package cn.swiftpass.enterprise.ui.fmt;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.bigkoo.pickerview.utils.ShapreUtils;
import com.bigkoo.pickerview.view.TimePickerViews;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.bill.BillOrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalInfo;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalItemInfo;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.print.PrintText;
import cn.swiftpass.enterprise.ui.activity.CommonBtnDialog;
import cn.swiftpass.enterprise.ui.activity.SummaryActivity;
import cn.swiftpass.enterprise.ui.activity.bill.ReportActivity;
import cn.swiftpass.enterprise.ui.activity.bill.UserListActivity;
import cn.swiftpass.enterprise.ui.activity.print.BluePrintUtil;
import cn.swiftpass.enterprise.ui.activity.print.BluetoothSettingActivity;
import cn.swiftpass.enterprise.ui.activity.spayMainTabActivity;
import cn.swiftpass.enterprise.ui.view.MyTextView;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.utils.DateTimeUtil;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

import static cn.swiftpass.enterprise.utils.DateUtil.getTime;

/**
 * Created by aijingya on 2018/11/22.
 *
 * @Package cn.swiftpass.enterprise.ui.fmt
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/11/22.15:39.
 */

public class FragmentTabSummary extends BaseFragment{
    private static final String TAG = FragmentTabSummary.class.getSimpleName();
    private FragmentActivity mActivity;

    private LinearLayout ll_select_date, ll_select_cashier;
    private TextView tv_choice_date,tv_choice_cashier;
    private ImageView iv_select_more_cashier;
    private MyTextView tv_total_money;
    private TextView  tv_total_pay_money, tv_order_pay_num, tv_refund_total_money, tv_refund_total_num;
    private LinearLayout  ll_summary_pay_type;
    private ListView listView_payType;

    private boolean loadNext = false;
    private ReportAdapter reportAdapter;
    private ViewHolder holder;
    private List<OrderTotalItemInfo> orderTotalItemInfo = new ArrayList<OrderTotalItemInfo>();
    private List<OrderTotalItemInfo> orderTotalItemInfoTwo = new ArrayList<>();

    private UserModel userModel;
    private OrderTotalInfo summaryOrderInfo;
    private ShapreUtils mSharePreUtils;
    List<String> dateList, thirtyDays;
    private String startTime, endTime, startPrint, endPrint;
    private TimePickerViews pvTime;
    private CommonBtnDialog commonBtnDialog;
    private LinearLayout ll_tip;
    private TextView tv_tip_amount;
    private TextView tv_tip_count;


    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_order_summary, container, false);

        if(isAdded()){
            //初始化控件
            initView(view);
            //设置listview的adapter
            if (reportAdapter == null) {
                reportAdapter = new ReportAdapter(mContext, orderTotalItemInfo);
                listView_payType.setAdapter(reportAdapter);
            }
        }

        setLister();
        initLabel();
        //从本地缓存里面取服务器地址
        PreferenceUtil.init(mContext);
        //新增时间控制缓存
        mSharePreUtils = new ShapreUtils(mContext);
        setDefaultTimeSelct();

        dateList = DateTimeUtil.getAllThirtyDays();
        thirtyDays = DateTimeUtil.getThirtyDays();

        return view;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        HandlerManager.registerHandler(HandlerManager.BILL_CHOICE_USER, handler);
        //注册handler 检查蓝牙打开和关闭的操作
        HandlerManager.registerHandler(HandlerManager.BLUE_CONNET_STUTS, handler);
        //注册handler ， 蓝牙打印日结数据
        HandlerManager.registerHandler(HandlerManager.CLICK_TITLE, handler);
        //注册handler ， 切换到当前tab页面，清除筛选数据
        HandlerManager.registerHandler(HandlerManager.SWITCH_TAB, handler);

        //去请求当天的数据信息
        loadDate(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00", DateUtil.formatTime(System.currentTimeMillis()), 2, null, true, null);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof spayMainTabActivity){
            mActivity=(spayMainTabActivity) activity;
        }else if(activity instanceof SummaryActivity){
            mActivity=(SummaryActivity) activity;
        }
    }

    boolean isOpen = true;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            if (msg.what == HandlerManager.BILL_CHOICE_USER) { //收银员
                try {
                    if (loadNext) { //
                        toastDialog(mActivity, R.string.tx_request_more, null);
                        return;
                    }

                    userModel = (UserModel) msg.obj;
                    if (userModel != null) {
                        iv_select_more_cashier.setVisibility(View.VISIBLE);
                        tv_choice_cashier.setVisibility(View.VISIBLE);
                        tv_choice_cashier.setText(userModel.getRealname());
                        if (!StringUtil.isEmptyOrNull(startTime) && !StringUtil.isEmptyOrNull(endTime)) {
                            loadDate(startTime, endTime, 2, userModel.getId() + "", true, null);
                        } else {
                            loadDate(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00", DateUtil.formatTime(System.currentTimeMillis()), 2, userModel.getId() + "", true, null);
                        }
                    } else {
                        iv_select_more_cashier.setVisibility(View.VISIBLE);
                        tv_choice_cashier.setVisibility(View.VISIBLE);
                        tv_choice_cashier.setText(R.string.tv_all_user);
                        if (!StringUtil.isEmptyOrNull(startTime) && !StringUtil.isEmptyOrNull(endTime)) {
                            loadDate(startTime, endTime, 2, null, true, null);
                        } else {
                            loadDate(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00", DateUtil.formatTime(System.currentTimeMillis()), 2, null, true, null);
                        }
                    }
                } catch (Exception e) {
                    iv_select_more_cashier.setVisibility(View.VISIBLE);
                    tv_choice_cashier.setVisibility(View.VISIBLE);
                    tv_choice_cashier.setText(R.string.tv_all_user);
                    if (!StringUtil.isEmptyOrNull(startTime) && !StringUtil.isEmptyOrNull(endTime)) {
                        loadDate(startTime, endTime, 2, null, true, null);
                    } else {
                        loadDate(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00", DateUtil.formatTime(System.currentTimeMillis()), 2, null, true, null);
                    }
                }
            } else if (msg.what == HandlerManager.BILL_CHOICE_TIME) {//选择时间
                if (loadNext) { //
                    toastDialog(mActivity, R.string.tx_request_more, null);
                    return;
                }
                String time = (String) msg.obj;
                if (!StringUtil.isEmptyOrNull(time)) {
                    String t[] = time.split("\\|");
                      /*  tv_date.setText(DateUtil.formartDateToYYMMDD(t[0]) + " " + getString(R.string.tv_least) + " " + DateUtil.formartDateToMMDD(t[1]));
                        startPrint = DateUtil.formartDateToYYMMDD(t[0]);
                        endPrint = DateUtil.formartDateToYYMMDD(t[1]);*/
                    startPrint = startTime;
                    endPrint = endTime;
                    if (userModel != null) {
                        startTime = t[0];
                        endTime = t[1];
                        loadDate(startTime, endTime, 2, userModel.getId() + "", true, null);
                    } else {
                        startTime = t[0];
                        endTime = t[1];
                        loadDate(startTime, endTime, 2, null, true, null);
                    }
                }
            } else if (msg.what == HandlerManager.BLUE_CONNET_STUTS) {
                if (isOpen) {
                    isOpen = false;
                }
            }else if(msg.what == HandlerManager.SUNMMARY_PRINT_DATA){
                if(isAdded()){
                    bluePrint();
                }
            }else if(msg.what == HandlerManager.SUMMARY_SWITCH_TAB){
                //设置默认选择显示当天的日期的数据
                if(isAdded()){
                    setDefaultTimeSelct();
                    tv_choice_date.setText(DateUtil.formatMD(System.currentTimeMillis()) + "(" + mActivity.getString(R.string.public_today) + ")");
                    //如果没有账单权限，则不能选择收银员
                    if (MainApplication.isOrderAuth.equals("0")) {
                        iv_select_more_cashier.setVisibility(View.GONE);
                        tv_choice_cashier.setText(MainApplication.realName);
                        ll_select_cashier.setEnabled(false);
                    }else{
                        iv_select_more_cashier.setVisibility(View.VISIBLE);
                        tv_choice_cashier.setText(R.string.tv_all_user);
                        if (userModel!=null) {
                            userModel.setRealname(mActivity.getString(R.string.tv_all_user));
                        }
                        userModel = null;
                        ll_select_cashier.setEnabled(true);
                    }

                }
                startTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00";
                endTime = DateUtil.formatTime(System.currentTimeMillis());
                startPrint = startTime;
                endPrint = endTime;
                //去请求当天的数据信息
                loadDate(startTime, endTime, 2, null, true, null);
            }
        }
    };

    private void initView(View view){
        ll_select_date = view.findViewById(R.id.ll_select_date);
        tv_choice_date = view.findViewById(R.id.tv_choice_date);
        ll_select_cashier = view.findViewById(R.id.ll_select_cashier);
        tv_choice_cashier = view.findViewById(R.id.tv_choice_cashier);

        iv_select_more_cashier = view.findViewById(R.id.iv_select_more_cashier);

        tv_total_money = view.findViewById(R.id.tv_total_money);

        tv_total_pay_money = view.findViewById(R.id.tv_total_pay_money);
        tv_order_pay_num = view.findViewById(R.id.tv_order_pay_num);
        tv_refund_total_money = view.findViewById(R.id.tv_refund_total_money);
        tv_refund_total_num = view.findViewById(R.id.tv_refund_total_num);

        ll_summary_pay_type = view.findViewById(R.id.ll_summary_pay_type);
        listView_payType =  view.findViewById(R.id.listView_payType);

        //设置默认选择显示当天的日期的数据
        tv_choice_date.setText(DateUtil.formatMD(System.currentTimeMillis()) + "(" + mActivity.getString(R.string.public_today) + ")");

        //如果没有账单权限，则不能选择收银员
        if (MainApplication.isOrderAuth.equals("0")) {
            iv_select_more_cashier.setVisibility(View.GONE);
            tv_choice_cashier.setText(MainApplication.realName);
            ll_select_cashier.setEnabled(false);
        }else{
            iv_select_more_cashier.setVisibility(View.VISIBLE);
            tv_choice_cashier.setText(R.string.tv_all_user);
            ll_select_cashier.setEnabled(true);
        }

        ll_tip=(LinearLayout) view.findViewById(R.id.ll_tip);
        tv_tip_amount=(TextView) view.findViewById(R.id.tv_tip_amount);
        tv_tip_count=(TextView) view.findViewById(R.id.tv_tip_count);
    }


    //去请求数据
    private void loadDate(final String startTime, final String endTime, final int countMethod,final String userId, final boolean isShowLoading, final String countDayKind) {
        BillOrderManager.getInstance().orderCount(startTime, endTime, countMethod, userId, countDayKind, new UINotifyListener<OrderTotalInfo>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                if (isShowLoading) {
                    loadDialog(mActivity, R.string.public_data_loading);
                }
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(Object object) {
                super.onError(object);
                dissDialog();
                if (checkSession()) {
                    return;
                }
                if (null != object) {
                    final String errorMsg = object.toString();
                    runOnUiThreadSafety(new Runnable() {
                        @Override
                        public void run() {
                            commonBtnDialog = new CommonBtnDialog(mActivity, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    commonBtnDialog.dismiss();
                                    //点击重试，重新按照当前条件请求网络
                                    loadDate(startTime, endTime, countMethod, userId, isShowLoading, countDayKind);
                                }
                            });
                            if(commonBtnDialog != null && !commonBtnDialog.isShowing()){
                                if(!StringUtil.isEmptyOrNull(errorMsg)){
                                    commonBtnDialog.setMessage(errorMsg);
                                }else{
                                    commonBtnDialog.setMessage(mActivity.getResources().getString(R.string.tx_load_fail));
                                }
                                commonBtnDialog.show();
                            }
                        }
                    });
                }
            }

            @Override
            public void onSucceed(OrderTotalInfo model) {
                dissDialog();
                if (null != model) {
                    //暂停
                    if (model.getReqFeqTime() > 0) {
                        loadNext = true;
                        sleep(model.getReqFeqTime());
                    }

                    if (MainApplication.isTipOpenFlag()){
                        ll_tip.setVisibility(View.VISIBLE);
                        tv_tip_amount.setText(DateUtil.formatMoneyUtils(model.getCountTotalTipFee()));
                        tv_tip_count.setText(String.valueOf(model.getCountTotalTipFeeCount()));
                    }else {
                        ll_tip.setVisibility(View.GONE);
                    }


                    //如果是汇总的数据
                    if (countMethod == 2) {
                        orderTotalItemInfo.clear();
                        orderTotalItemInfo.addAll(model.getOrderTotalItemInfo());

                        orderTotalItemInfoTwo.clear();
                        orderTotalItemInfoTwo.addAll(model.getOrderTotalItemInfo());

                        summaryOrderInfo = model;

                        if (reportAdapter != null) {
                            reportAdapter.notifyDataSetChanged();
                        }

                        if(isAdded()){
                            setValue(model);
                        }

                        if (model.getOrderTotalItemInfo().size() > 0) {
                            ll_summary_pay_type.setVisibility(View.VISIBLE);
                            listView_payType.setVisibility(View.VISIBLE);
                            setListViewHeightBasedOnChildren(listView_payType);
                        } else {
                            ll_summary_pay_type.setVisibility(View.GONE);
                            listView_payType.setVisibility(View.GONE);
                        }
                    }
                }
            }
        });
    }

     public void bluePrint() {
        runOnUiThreadSafety(new Runnable() {
            @Override
            public void run() {
                if (summaryOrderInfo != null) {
                    if (!StringUtil.isEmptyOrNull(startTime) && !StringUtil.isEmptyOrNull(endTime)) {
                        try {
                            summaryOrderInfo.setStartTime(startPrint);
                            summaryOrderInfo.setEndTime(endPrint);
                        } catch (Exception e) {
                            Log.e("hehui", "" + e);
                        }
                    } else {
                        summaryOrderInfo.setStartTime(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00");
                        summaryOrderInfo.setEndTime(DateUtil.formatTime(System.currentTimeMillis()));
                    }

                    if (userModel != null) {
                        summaryOrderInfo.setUserName(userModel.getRealname());
                    }

                    if (MainApplication.isAdmin.equals("0")) {
                        summaryOrderInfo.setUserName(MainApplication.realName);
                    }

                    PrintText.getInstance().init();
                    PrintText.getInstance().printSummary(mActivity, summaryOrderInfo);

                }
            }
        });
    }

    private void showDialog() {
        dialog = new DialogInfo(mContext, null, mActivity.getString(R.string.tx_blue_set), mActivity.getString(R.string.title_setting), mActivity.getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                dialog.cancel();
                dialog.dismiss();
                showPage(BluetoothSettingActivity.class);
            }

            @Override
            public void handleCancleBtn() {
                dialog.cancel();
            }
        }, null);

        DialogHelper.resize(mContext, dialog);
        dialog.show();
    }

    void setValue(OrderTotalInfo info) {
        long money = info.getCountTotalFee() - info.getCountTotalRefundFee();
        String totalvalues = MainApplication.feeFh+DateUtil.formatMoneyUtils(money);

        if (totalvalues.length() > 10){
            tv_total_money.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_25));
        }else{
            tv_total_money.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_42));
        }
        tv_total_money.setText(MainApplication.feeFh+DateUtil.formatMoneyUtils(money));

        tv_total_pay_money.setText(DateUtil.formatMoneyUtils(info.getCountTotalFee()));
        tv_order_pay_num.setText(info.getCountTotalCount() + "");
        tv_refund_total_money.setText(DateUtil.formatMoneyUtils(info.getCountTotalRefundFee()));
        tv_refund_total_num.setText(info.getCountTotalRefundCount() + "");
    }

    /**
     * 暂停 后 才去请求
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    void sleep(long time) {
        try {
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadNext = false;
                        }
                    });
                }
            };
            Timer timer = new Timer();
            timer.schedule(task, time * 1000);
        } catch (Exception e) {
            return;
        }
    }

    //设置开始年与结束年
    private Date tempSDate,tempEDate;
    private int EndYear = Calendar.getInstance().get(Calendar.YEAR);
    private void setLister() {
        ll_select_cashier.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (userModel != null) {
                    UserListActivity.startActivity(mContext, userModel,false,false);
                } else {
                    Intent intent = new Intent(mContext, UserListActivity.class);
                    intent.putExtra("isFromBillEnter",false);
                    intent.putExtra("isFromCodeListEnter",false);
                    startActivity(intent);
                }
            }
        });

        ll_select_date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                showPage(DateChoiceActivity.class);
                //时间选择器
                pvTime = new TimePickerViews.Builder(mContext, new TimePickerViews.OnTimeSelectListener() {
                    @Override
                    public void onTimeSelect(Date startdate, Date enddate,View v) {//选中事件回调
                        long starttime = startdate.getTime();
                        long endtime = enddate.getTime();
                        long tempstime = 0l;
                        long tempetime = 0l;
                        Log.e("JAMY","starttime: "+starttime+" endtime:"+endtime);
                        if (startdate.getTime() >= enddate.getTime()) {
                            mSharePreUtils.setSelect(false);
                            tempetime = mSharePreUtils.getTotime();
                            tempstime = mSharePreUtils.getFromtime();
                            Log.e("JAMY","tempetime: "+tempetime+" tempstime:"+tempstime);
                            if (0l != tempstime && 0l != tempetime){
                                Log.e("JAMY","etime: "+DateUtil.getAllNormalTime(tempetime)
                                        +" stime:"+DateUtil.getAllNormalTime(tempstime));
                                tempEDate = DateUtil.getStrToDate(DateUtil.getAllNormalTime(tempetime));
                                tempSDate = DateUtil.getStrToDate(DateUtil.getAllNormalTime(tempstime));
                                tv_choice_date.setText("     "+DateUtil.getNormalTime(getTime(tempSDate))+"\n"
                                        +mActivity.getResources().getString(R.string.data_to)+" "+DateUtil.getNormalTime(getTime(tempEDate)));
                            }else{
                                tv_choice_date.setText(DateUtil.formatMD(System.currentTimeMillis()) + "(" + mActivity.getString(R.string.public_today) + ")");
                            }
                            toastDialog(mActivity, R.string.show_endtime_than_starttime, null);
                            return;
                        }
                        startTime = DateUtil.getAllNormalTime(getTime(startdate));
                        endTime = DateUtil.getAllNormalTime(getTime(enddate));
                        mSharePreUtils.setSelect(true);
                        mSharePreUtils.setFromtime(getTime(startdate));
                        mSharePreUtils.setTotime(getTime(enddate));
                        String time = startTime + "|" + endTime;
                        tv_choice_date.setText("     "+DateUtil.getNormalTime(getTime(startdate))+"\n"
                                +mActivity.getResources().getString(R.string.data_to)+" "+DateUtil.getNormalTime(getTime(enddate)));
                        HandlerManager.notifyMessage(HandlerManager.BILL_CHOICE_USER, HandlerManager.BILL_CHOICE_TIME, time);
                    }
                })
                        .setCancelText(getStringById(R.string.data_select_cancel))//取消按钮文字
                        .setSubmitText(getStringById(R.string.data_select_done))//确认按钮文字
                        .setTitleText(getStringById(R.string.data_title))//标题文字
                        .setLabel(label_year,label_month,label_day,label_hours,label_mins,label_seconds)
                        .isCenterLabel(false)
                        .setLanguage(isEnglish())
                        .setRange(getStartYear(-89),EndYear)
                        .setRangDate(getStartDate(-89),Calendar.getInstance())
                        .setDate(Calendar.getInstance())
                        .build();
                pvTime.show();
            }
        });
    }

    class ReportAdapter extends BaseAdapter {
        private List<OrderTotalItemInfo> list;
        private Context context;

        private ReportAdapter(Context context, List<OrderTotalItemInfo> list) {
            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.activity_report_circle_list_item, null);
                holder = new ViewHolder();
                holder.v_line = convertView.findViewById(R.id.v_line);
                holder.tv_pay_type = convertView.findViewById(R.id.tv_pay_type);
                holder.tv_money = convertView.findViewById(R.id.tv_money);
                holder.tv_num = convertView.findViewById(R.id.tv_num);
                holder.iv_type = convertView.findViewById(R.id.iv_type);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            if ((position + 1) == list.size()) {
                holder.v_line.setVisibility(View.GONE);
            } else {
                holder.v_line.setVisibility(View.VISIBLE);
            }

            OrderTotalItemInfo orderTotalItemInfo = list.get(position);
            if (orderTotalItemInfo != null) {

                if (MainApplication.getPayTypeMap() != null && MainApplication.getPayTypeMap().size() > 0) {

                    String typeName = MainApplication.getPayTypeMap().get(String.valueOf(orderTotalItemInfo.getPayTypeId()));

                    holder.tv_pay_type.setText(typeName);
                }

                @SuppressWarnings("unchecked")
                Map<String, String> typePicMap = (Map<String, String>) SharedPreUtile.readProduct("payTypeIcon" + ApiConstant.bankCode + MainApplication.getMchId());
                if (typePicMap != null && typePicMap.size() > 0) {
                    String url = typePicMap.get(String.valueOf(orderTotalItemInfo.getPayTypeId()));
                    if (!StringUtil.isEmptyOrNull(url)) {
                        Bitmap bitmap = BitmapFactory.decodeResource(mActivity.getResources(), R.drawable.icon_general_receivables);
                        if(MainApplication.finalBitmap != null){
                            if (bitmap != null) {
                                MainApplication.finalBitmap.display(holder.iv_type, url, bitmap,bitmap);
                            } else {
                                MainApplication.finalBitmap.display(holder.iv_type, url);
                            }
                        }

                    }
                }
                holder.tv_money.setText(mActivity.getString(R.string.tx_money) + DateUtil.formatMoneyUtils(orderTotalItemInfo.getSuccessFee()));
                holder.tv_num.setText(mActivity.getString(R.string.tx_bill_stream_pay_type_num) + "：" + orderTotalItemInfo.getSuccessCount() );
            }
            return convertView;
        }
    }

    class ViewHolder {
        private View v_line;
        private ImageView iv_type;
        TextView tv_pay_type, tv_money, tv_num;
    }

    /**
     * 动态设置ListView的高度
     *
     * @param listView
     */
    public void setListViewHeightBasedOnChildren(ListView listView) {
        if (listView == null) return;
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    /**
     * 设置默认的选择时间
     */
    private void setDefaultTimeSelct(){
        if (null != mSharePreUtils){
            mSharePreUtils.setTotime(0L);
            mSharePreUtils.setFromtime(0L);
            mSharePreUtils.setSelect(false);
        }
    }

    /**
     * 初始化标签
     */

    private String label_year, label_month, label_day, label_hours, label_mins, label_seconds;
    private void initLabel(){
        label_year=getStringById(R.string.pickerview_year);
        label_month=getStringById(R.string.pickerview_month);
        label_day=getStringById(R.string.pickerview_day);
        label_hours=getStringById(R.string.pickerview_hours);
        label_mins=getStringById(R.string.pickerview_minutes);
        label_seconds=getStringById(R.string.pickerview_seconds);
    }

    /**
     *  获取前后日期 i为正数 向后推迟i天，负数时向前提前i天
     * @return
     */
    public Calendar getStartDate(int day)
    {
        Calendar beforeDate = Calendar.getInstance();
        beforeDate.add(Calendar.DATE, day);
        return beforeDate;
    }

    /**
     *  获取默认显示的日历
     * @return
     */
    public Calendar getDefaultFromDate()
    {
        Calendar beforeDate = Calendar.getInstance();
        beforeDate.set(Calendar.HOUR,00);
        beforeDate.set(Calendar.MINUTE,00);
        return beforeDate;
    }

    /**
     *  获取前90天前的年份
     * @return
     */
    public int getStartYear(int day)
    {
        Calendar beforeDate = Calendar.getInstance();
        beforeDate.add(Calendar.DATE, day);
        return beforeDate.get(Calendar.YEAR);
    }

}
