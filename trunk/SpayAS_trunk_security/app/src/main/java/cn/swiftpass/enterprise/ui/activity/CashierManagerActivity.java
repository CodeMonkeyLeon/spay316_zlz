package cn.swiftpass.enterprise.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnLoadListener;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnMenuItemClickListener;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnRefreshListener;
import com.example.common.sentry.SentryUtils;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.bussiness.model.UserModelList;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.CashierManagerContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.CashierManagerPresenter;
import cn.swiftpass.enterprise.ui.activity.user.CashierAddActivity;
import cn.swiftpass.enterprise.ui.activity.user.RefundManagerActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.DisplayUtil;

public class CashierManagerActivity extends BaseActivity<CashierManagerContract.Presenter> implements OnRefreshListener, OnLoadListener, CashierManagerContract.View {

    private static final String TAG = CashierManagerActivity.class.getSimpleName();
    private ViewHolder holder;
    private SwipeMenuListView listView;
    private List<UserModel> listUser;
    private Handler mHandler = new Handler();
    private CashierAdapter cashierAdapter;
    private int pageFulfil = 0;
    private int pageCount = 0;
    private EditText et_input;
    private DialogInfo dialogInfo;
    private TextView ly_cashier_no;

    @Override
    protected CashierManagerContract.Presenter createPresenter() {
        return new CashierManagerPresenter();
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cashier_manager);

        listUser = new ArrayList<UserModel>();
        cashierAdapter = new CashierAdapter(listUser);

        listView = getViewById(R.id.listView);
        listView.setAdapter(cashierAdapter);

        initView();

        et_input = getViewById(R.id.et_input);
        et_input.setFocusable(true);
        et_input.setFocusableInTouchMode(true);
        et_input.requestFocus();
        et_input.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Bundle b = new Bundle();
                    b.putBoolean("isFromOrderStreamSearch", true);
                    b.putInt("serchType", 2); //从收银员列表查询
                    showPage(RefundManagerActivity.class, b);
                }
                return true;
            }
        });


        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {
                try {
                    UserModel userModel = listUser.get(position - 1);
                    if (userModel != null) {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("userModel", userModel);
                        showPage(CashierAddActivity.class, bundle);
                    }

                } catch (Exception e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
        });

        pageFulfil = 0;
        loadData(pageFulfil, false, SwipeMenuListView.REFRESH);

    }

    private void initView() {
        ly_cashier_no = getViewById(R.id.ly_cashier_no);

        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem openItem = new SwipeMenuItem(getApplicationContext());
                openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9, 0xCE)));
                openItem.setWidth(DisplayUtil.dip2Px(CashierManagerActivity.this, 90));
                openItem.setTitle(R.string.bt_delete);
                openItem.setTitleSize(18);
                openItem.setTitleColor(Color.WHITE);
                openItem.setBackground(new ColorDrawable(Color.RED));
                menu.addMenuItem(openItem);
            }
        };

        listView.setMenuCreator(creator);
        listView.setOnRefreshListener(this);
        listView.setOnLoadListener(this);
        listView.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            @Override
            public void onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {
                    case 0:
                        UserModel userModel = listUser.get(position);
                        if (userModel != null) {
                            delete(userModel.getId(), position);
                        }
                        break;
                }
            }
        });
    }


    @Override
    public void cashierDeleteSuccess(boolean response, int position) {
        if (response) {
            toastDialog(CashierManagerActivity.this,
                    R.string.show_delete_succ,
                    new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            listUser.remove(position);
                            cashierAdapter.notifyDataSetChanged();
                        }
                    });
        }
    }

    @Override
    public void cashierDeleteFailed(@Nullable Object error) {
        if (error != null) {
            toastDialog(CashierManagerActivity.this, error.toString(), null);
        }
    }

    void delete(final long userId, final int position) {

        dialogInfo = new DialogInfo(CashierManagerActivity.this, getString(R.string.public_cozy_prompt),
                getString(R.string.dialog_delete), getString(R.string.btnOk), getString(R.string.btnCancel),
                DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {
            @Override
            public void handleOkBtn() {
                if (mPresenter != null) {
                    mPresenter.cashierDelete(userId, position);
                }
            }

            @Override
            public void handleCancelBtn() {
                dialogInfo.cancel();
            }
        }, null);

        DialogHelper.resize(CashierManagerActivity.this, dialogInfo);
        dialogInfo.show();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        //重新刷新界面
        pageFulfil = 0;
        loadData(pageFulfil, false, SwipeMenuListView.REFRESH);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

    }


    @Override
    public void queryCashierSuccess(@NonNull UserModelList response, int type, int page) {
        pageCount = response.pageCount;
        if (response.data != null && response.data.size() > 0) {
            listView.setVisibility(View.VISIBLE);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ly_cashier_no.setVisibility(View.GONE);
                    listView.setVisibility(View.VISIBLE);
                }
            });

            if (type == SwipeMenuListView.REFRESH) {
                listUser.clear();
                listUser.addAll(response.data);
                cashierAdapter.notifyDataSetChanged();
                listView.onRefreshComplete();
                listView.onLoadComplete();
            } else if (type == SwipeMenuListView.LOAD) {
                listUser.addAll(response.data);
                cashierAdapter.notifyDataSetChanged();
                listView.onRefreshComplete();
                listView.onLoadComplete();
            }
            if (page == 0) {
                listView.setSelection(0);
            }
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (listUser.size() == 0) {
                        ly_cashier_no.setVisibility(View.VISIBLE);
                        ly_cashier_no.setText(R.string.tv_user_list);
                        listView.setVisibility(View.GONE);
                        listView.setResultSize(listUser.size());
                    }
                }
            });
        }
    }

    @Override
    public void queryCashierFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }
        if (error != null) {
            toastDialog(CashierManagerActivity.this, error.toString(), new NewDialogInfo.HandleBtn() {

                @Override
                public void handleOkBtn() {
                    if (listUser.size() == 0) {
                        finish();
                    }
                }

            });
        }
    }

    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void loadData(final int page, final boolean isLoadMore, final int type) {
        if (mPresenter != null) {
            mPresenter.queryCashier(page, 10, null, type);
        }
    }


    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(R.string.tx_user_manager);
        titleBar.setLeftButtonVisible(true);
        titleBar.setRightButtonVisible(false);
        titleBar.setRightButLayVisible(true, 0);
        titleBar.setRightButLayBackground(R.drawable.icon_add);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onRightButLayClick() {
                showPage(CashierAddActivity.class);
            }

            @Override
            public void onLeftButtonClick() {
                finish();
            }
        });
    }

    @Override
    public void onLoad() {
        pageFulfil = pageFulfil + 1;
        if (pageFulfil < pageCount) {
            loadData(pageFulfil, true, SwipeMenuListView.LOAD);
        }
    }

    @Override
    public void onRefresh() {
        pageFulfil = 0;
        loadData(pageFulfil, false, SwipeMenuListView.REFRESH);
    }

    private class CashierAdapter extends BaseAdapter {

        private List<UserModel> userModels;

        public CashierAdapter() {
        }

        public CashierAdapter(List<UserModel> userModels) {
            this.userModels = userModels;
        }

        @Override
        public int getCount() {
            return userModels.size();
        }

        @Override
        public Object getItem(int position) {
            return userModels.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(CashierManagerActivity.this, R.layout.cashier_list, null);
                holder = new ViewHolder();
                holder.cashier_state = (TextView) convertView.findViewById(R.id.cashier_state);
                holder.cashier_store = (TextView) convertView.findViewById(R.id.cashier_store);
                holder.user_account = (TextView) convertView.findViewById(R.id.user_account);
                holder.userId = (TextView) convertView.findViewById(R.id.userId);
                holder.userName = (TextView) convertView.findViewById(R.id.userName);
                holder.usertel = (TextView) convertView.findViewById(R.id.usertel);
                holder.v_line = convertView.findViewById(R.id.v_line);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            UserModel userModel = userModels.get(position);

            if (position == (userModels.size() - 1)) {
                holder.v_line.setVisibility(View.GONE);
            } else {
                holder.v_line.setVisibility(View.VISIBLE);
            }

            if (userModel.getDeptname() != null) {
                holder.cashier_store.setText(userModel.getDeptname());
            }


            holder.userId.setText(getResources().getString(R.string.cash_number) + ": " + String.valueOf(userModel.getId()));
            if (userModel.getPhone() != null) {
                holder.usertel.setText(userModel.getPhone());
            }

            if (userModel.getRealname() != null) {
                holder.userName.setText(getResources().getString(R.string.cashier) + ": " + userModel.getRealname());
            }
            if (userModel.getUsername() != null) {
                holder.user_account.setText(getResources().getString(R.string.cashier_account) + ": " + userModel.getUsername());
            }

            return convertView;
        }
    }

    private class ViewHolder {
        private TextView userId, cashier_state, userName, usertel, cashier_store, user_account;

        private View v_line;

    }

}
