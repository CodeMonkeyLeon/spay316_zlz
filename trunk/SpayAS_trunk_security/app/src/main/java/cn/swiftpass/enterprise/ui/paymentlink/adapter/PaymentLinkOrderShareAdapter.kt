package cn.swiftpass.enterprise.ui.paymentlink.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.ui.paymentlink.entity.OrderShareEntity
import cn.swiftpass.enterprise.ui.paymentlink.interfaces.OnPaymentLinkShareTypeClickListener

class PaymentLinkOrderShareAdapter(val mContext: Context) :
    RecyclerView.Adapter<PaymentLinkOrderShareAdapter.ViewHolder>() {


    private val mList: ArrayList<OrderShareEntity> = arrayListOf()


    private var mItemClickListener: OnPaymentLinkShareTypeClickListener? = null

    fun setOnPaymentLinkShareTypeClickListener(listener: OnPaymentLinkShareTypeClickListener) {
        mItemClickListener = listener
    }


    val setData: (ArrayList<OrderShareEntity>) -> Unit = {
        mList.clear()
        mList.addAll(it)
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemLayout = itemView.findViewById<ConstraintLayout>(R.id.id_cy_item_share_type)
        val imgView = itemView.findViewById<ImageView>(R.id.id_icon)
        val textView = itemView.findViewById<TextView>(R.id.id_tv_text)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_dialog_share, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = mList[position]
        holder.safeAs<ViewHolder>()?.run {
            imgView.setImageDrawable(mContext.resources.getDrawable(data.imgId))
            textView.text = data.text
            itemLayout.setOnClickListener {
                mItemClickListener?.run {
                    onChoose(data.type)
                }
            }
        }
    }


    inline fun <reified T> Any.safeAs(): T? = this as? T


}