package cn.swiftpass.enterprise.mvp.presenter.fragment

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.RateInfo
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.contract.fragment.FragmentTabContract
import cn.swiftpass.enterprise.utils.JsonUtil
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/7
 *
 * @有客自远方来
 * @眉间有远方的风雨
 */
class FragmentTabPresenter : FragmentTabContract.Presenter {

    private var mView: FragmentTabContract.View? = null


    override fun getExchangeRate() {
        mView?.let { view ->


            AppClient.getExchangeRate(
                MainApplication.getInstance().getMchId(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<RateInfo>(RateInfo()) {

                    override fun onFailure(call: Call?, error: CommonResponse?) {
                        MainApplication.getInstance().setExchangeRate("1")
                        error?.let { e ->
                            view.getExchangeRateFailed(e.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        r?.let { response ->
                            val rateInfo = JsonUtil.jsonToBean(
                                response.message,
                                RateInfo::class.java
                            ) as RateInfo
                            if (!TextUtils.isEmpty(rateInfo.rate)) {
                                MainApplication.getInstance().setExchangeRate(rateInfo.rate)
                            }
                            if (!TextUtils.isEmpty(rateInfo.vatRate)) {
                                MainApplication.getInstance()
                                    .setVatRate(rateInfo.vatRate.toDouble())
                            }
                            if (!TextUtils.isEmpty(rateInfo.surchargeRate)) {
                                MainApplication.getInstance()
                                    .setSurchargeRate(rateInfo.surchargeRate.toDouble())
                            }
                            if (!TextUtils.isEmpty(rateInfo.taxRate)) {
                                MainApplication.getInstance()
                                    .setTaxRate(rateInfo.taxRate.toDouble())
                            }
                            if (!TextUtils.isEmpty(rateInfo.usdToRmbExchangeRate)) {
                                MainApplication.getInstance()
                                    .setUsdToRmbExchangeRate(rateInfo.usdToRmbExchangeRate.toDouble())
                            }
                            if (!TextUtils.isEmpty(rateInfo.sourceToUsdExchangeRate)) {
                                MainApplication.getInstance()
                                    .setSourceToUsdExchangeRate(rateInfo.sourceToUsdExchangeRate.toDouble())
                            }

                            if (!TextUtils.isEmpty(rateInfo.alipayPayRate)) {
                                MainApplication.getInstance()
                                    .setAlipayPayRate(rateInfo.alipayPayRate.toDouble())
                            }
                            view.getExchangeRateSuccess(true)
                        }
                    }
                }
            )
        }
    }

    override fun getVersionCode() {
//        mView?.let { view ->
//
//            AppClient.getVersionCode(
//                AppHelper.getUpdateVersionCode(MainApplication.getInstance()).toString() + "",
//                BuildConfig.bankCode,
//                System.currentTimeMillis().toString(),
//                object : CallBackUtil.CallBackCommonResponse<UpgradeInfo>(UpgradeInfo()) {
//
//                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
//                        commonResponse?.let {
//                            if (TextUtils.equals("400", it.result) || TextUtils.equals(
//                                    "402",
//                                    it.result
//                                )
//                            ) {
//                                view.getVersionCodeSuccess(null)
//                            } else {
//                                view.getVersionCodeFailed(it.result)
//                            }
//                        }
//                    }
//
//                    override fun onResponse(r: CommonResponse?) {
//                        r?.let { response ->
//                            if (TextUtils.equals(response.result, "200")) {
//                                try {
//                                    val upgradeInfoStr = response.message
//                                    val info = UpgradeInfo()
//                                    val json = JSONObject(upgradeInfoStr)
//                                    info.dateTime =
//                                        Utils.Long.tryParse(json.optString("updated_at"), 0)
//                                    info.message = json.optString("subDesc", "")
//                                    info.version =
//                                        Utils.Integer.tryParse(json.optString("verCode"), 0)
//                                    info.mustUpgrade = Utils.Integer.tryParse(
//                                        json.optString("isUpdate"),
//                                        0
//                                    ) == 0
//                                    info.versionName = json.optString("verName")
//                                    info.fileMd5 = json.optString("fileMd5")
//                                    MainApplication.getInstance().setFileMd5(info.fileMd5)
//                                    info.url = json.optString("filePath", "")
//                                    MainApplication.getInstance().setFileMd5(info.fileMd5)
//                                    view.getVersionCodeSuccess(info)
//                                } catch (e: Exception) {
//                                    SentryUtils.uploadNetInterfaceException(
//                                        e,
//                                        MainApplication.getInstance().baseUrl + "spay/upgrade/checkinVersion",
//                                        e.toString(),
//                                        ""
//                                    )
//                                }
//                            }
//                        }
//                    }
//                }
//            )
//
//        }
    }

    override fun attachView(view: FragmentTabContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}