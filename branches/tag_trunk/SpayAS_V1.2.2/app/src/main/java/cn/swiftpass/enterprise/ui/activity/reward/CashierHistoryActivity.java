package cn.swiftpass.enterprise.ui.activity.reward;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.order.HistoryRankingManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.AwardModel;
import cn.swiftpass.enterprise.bussiness.model.AwardModelList;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.adapter.CashierDetailsAdapter;
import cn.swiftpass.enterprise.ui.widget.PullDownListView;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;

public class CashierHistoryActivity extends TemplateActivity implements PullDownListView.OnRefreshListioner
{
    //    private ListView cashier_listview;
    
    private ListView listView;
    
    private CashierDetailsAdapter adapter;
    
    private List<String> list;
    
    private TimeCount time;
    
    private TextView tx_divide, tx_hour, tx_second, tx_day;
    
    private TextView tx_divide1, tx_hour1, tx_second1, tx_day1;
    
    private LinearLayout lay_title;
    
    private RelativeLayout ly_title_two, lay_activity, ly_title_one, ly_set_time;
    
    private TextView user_name, user_id, cashire_item_money_tv, cashire_item_money_scale_tv;
    
    private TextView my_valy, my_no_valy;
    
    private ImageView cashire_image;
    
    private RelativeLayout lay_time;
    
    private SharedPreferences sp = null;
    
    private PopupWindow pop = null;
    
    String activeId = "";
    
    private TextView tx_prompt, tx_title;
    
    private LinearLayout lay_title_time;
    
    private PullDownListView cashier_listview;
    
    private List<AwardModel> awardModelList;
    
    private CashierDetailsAdapter cashierDetailsAdapter;
    
    private Handler mHandler = new Handler();
    
    private int pageFulfil = 0;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        sp = getSharedPreferences(CashierHistoryActivity.class.getName(), 0);
        
        setContentView(R.layout.cashier_history_ranking);
        
        initObject();
        
        initViews();
        
        activeId = getIntent().getStringExtra("activeId");
        String activeName = getIntent().getStringExtra("activeName");
        String toashContent = getIntent().getStringExtra("content");
        if (TextUtils.isEmpty(toashContent))
        {
            toashContent = sp.getString(MainApplication.userName + "_activeId_" + activeId, null);
        }
        if (!TextUtils.isEmpty(activeId))
        {
            loadData(activeId, 0, false);
            if (!TextUtils.isEmpty(activeName))
            {
                titleBar.setTitle(activeName);
            }
            else
            {
                titleBar.setTitle(R.string.tx_history_rank);
            }
            titleBar.setRightButLayVisibleForTotal(false, getString(R.string.tx_history_rank));
        }
        else
        {
            loadData(null, 0, false);
        }
        if (null != toashContent && !"".equals(toashContent))
        {
            showWinToast(toashContent);
        }
    }
    
    private void showWinToast(String toashContent)
    {
        final View view = View.inflate(this, R.layout.view_text_pop, null);
        TextView textView = (TextView)view.findViewById(R.id.view_pop_text);
        textView.setText(toashContent);
        View closeButton = view.findViewById(R.id.closeToast);
        closeButton.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                
                sp.edit().clear().commit();
                if (pop != null)
                {
                    pop.dismiss();
                    pop = null;
                }
            }
        });
        if (!TextUtils.isEmpty(toashContent))
        {
            
            titleBar.post(new Runnable()
            {
                
                @Override
                public void run()
                {
                    
                    pop = new PopupWindow(view, titleBar.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
                    pop.setWidth(titleBar.getWidth());
                    pop.setAnimationStyle(R.style.popuwindow_in_out_style);
                    pop.update();
                    // 设置PopupWindow外部区域是否可触摸
                    pop.setBackgroundDrawable(getResources().getDrawable(android.R.color.transparent));
                    //                    pop.setFocusable(true); // 设置PopupWindow可获得焦点
                    //                    pop.setTouchable(true); // 设置PopupWindow可触摸
                    pop.setOutsideTouchable(false); // 设置非PopupWindow区域可触摸
                    pop.showAsDropDown(titleBar);
                    
                }
            });
            
        }
    }
    
    @Override
    protected void onResume()
    {
        // TODO Auto-generated method stub
        super.onResume();
    }
    
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (pop != null)
        {
            pop.dismiss();
            pop = null;
        }
        if (time != null)
        {
            time.cancel();
        }
    }
    
    /**
     *倒计时
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void Countdown(long times)
    {
        time = new TimeCount(times * 1000, 1000);
        time.start();
    }
    
    class TimeCount extends CountDownTimer
    {
        final long oneDay = 24 * 60 * 60 * 1000;
        
        final long oneHour = 60 * 60 * 1000;
        
        public TimeCount(long millisInFuture, long countDownInterval)
        {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }
        
        @Override
        public void onFinish()
        {//计时完毕时触发
            showToastInfo(R.string.show_active_end);
            lay_time.setVisibility(View.GONE);
            ly_set_time.setVisibility(View.GONE);
        }
        
        @Override
        public void onTick(long millisUntilFinished)
        {//计时过程显示
            long days = millisUntilFinished / oneDay;
            long temp = millisUntilFinished % oneDay; //时分钞
            long hour = temp / oneHour; //小时
            long temp1 = temp % oneHour; //分和秒
            long minute = temp1 / 60000; //分
            long temp2 = temp1 % 60000;
            long second = temp2 / 1000;//秒
            if (isTag)
            {
                lay_time.setVisibility(View.VISIBLE);
                lay_title_time.setVisibility(View.VISIBLE);
                tx_day1.setText(days + "");
                tx_hour1.setText(formatNumber(hour));
                tx_divide1.setText(formatNumber(minute));
                tx_second1.setText(formatNumber(second));
                
            }
            else
            {
                ly_set_time.setVisibility(View.VISIBLE);
                tx_day.setText(days + "");
                tx_hour.setText(formatNumber(hour));
                tx_divide.setText(formatNumber(minute));
                tx_second.setText(formatNumber(second));
            }
        }
    }
    
    /**
     * 格式化数字
     * @param n
     * @return
     * @see [类、类#方法、类#成员]
     */
    private String formatNumber(long n)
    {
        NumberFormat nf = NumberFormat.getInstance();
        //设置是否使用分组
        nf.setGroupingUsed(false);
        //设置最大整数位数
        nf.setMaximumIntegerDigits(2);
        //设置最小整数位数   
        nf.setMinimumIntegerDigits(2);
        return nf.format(n);
    }
    
    private void initViews()
    {
        lay_title_time = getViewById(R.id.lay_title_time);
        lay_title = getViewById(R.id.lay_title);
        ly_title_two = getViewById(R.id.ly_title_two);
        ly_title_one = getViewById(R.id.ly_title_one);
        tx_prompt = getViewById(R.id.tx_prompt);
        tx_title = getViewById(R.id.tx_title);
        ly_set_time = getViewById(R.id.ly_set_time);
        if (MainApplication.isAdmin.equals("1"))
        { //如果是商户
            ly_title_two.setVisibility(View.GONE);
            ly_title_one.setVisibility(View.GONE);
            titleBar.setRightButLayVisibleForTotal(false, getString(R.string.tx_history_rank));
            lay_title.setVisibility(View.GONE);
        }
        
        user_name = getViewById(R.id.user_name);
        my_no_valy = getViewById(R.id.my_no_valy);
        my_valy = getViewById(R.id.my_valy);
        cashire_item_money_scale_tv = getViewById(R.id.cashire_item_money_scale_tv);
        cashire_item_money_tv = getViewById(R.id.cashire_item_money_tv);
        cashire_image = getViewById(R.id.cashire_image);
        user_id = getViewById(R.id.user_id);
        
        cashier_listview = (PullDownListView)getViewById(R.id.cashier_list);
        cashier_listview.setAutoLoadMore(true);
        cashier_listview.setRefreshListioner(this);
        listView = cashier_listview.mListView;
        
        listView.setAdapter(cashierDetailsAdapter);
        
        tx_day = getViewById(R.id.tx_day);
        tx_divide = getViewById(R.id.tx_divide);
        tx_second = getViewById(R.id.tx_second);
        tx_hour = getViewById(R.id.tx_hour);
        
        tx_day1 = getViewById(R.id.tx_day1);
        tx_divide1 = getViewById(R.id.tx_divide1);
        tx_second1 = getViewById(R.id.tx_second1);
        tx_hour1 = getViewById(R.id.tx_hour1);
        lay_activity = getViewById(R.id.lay_activity);
        lay_time = getViewById(R.id.lay_time);
    }
    
    private void setMyView(AwardModel awardModel)
    {
        if (awardModel.getUserName() != null)
        {
            user_name.setText(awardModel.getUserName());
        }
        user_id.setText("(" + awardModel.getUserId() + ")");
        cashire_item_money_tv.setText(DateUtil.formatMoneyUtils(awardModel.getMoney()));
        setImage(cashire_image, awardModel.getActiveOrder());
        cashire_item_money_scale_tv.setText(awardModel.getActiveOrder() + "");
        my_no_valy.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT,
            Float.parseFloat(awardModel.getPercent())));
        my_valy.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT,
            (1 - Float.parseFloat(awardModel.getPercent()))));
    }
    
    private void setImage(ImageView view, long activeOrder)
    {
        if (activeOrder == 1)
        {
            view.setImageResource(R.drawable.icon_reward_special);
        }
        else if (activeOrder == 2)
        {
            view.setImageResource(R.drawable.icon_reward_01);
        }
        else if (activeOrder == 3)
        {
            view.setImageResource(R.drawable.icon_reward_02);
        }
        else
        {
            view.setImageResource(R.drawable.icon_reward_default);
        }
    }
    
    private boolean isTag = false;
    
    private void mySetListData(final PullDownListView pull, List<AwardModel> list, CashierDetailsAdapter adapter,
        List<AwardModel> result, boolean isLoadMore)
    {
        if (!isLoadMore)
        {
            pull.onfinish(getStringById(R.string.refresh_successfully));// 刷新完成
            
            mHandler.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    // 这里表示刷新处理完成后把上面的加载刷新界面隐藏
                    pull.onRefreshComplete();
                }
            }, 800);
            
            list.clear();
        }
        else
        {
            mHandler.postDelayed(new Runnable()
            {
                
                @Override
                public void run()
                {
                    pull.onLoadMoreComplete();
                    // pull.setMore(true);
                }
            }, 1000);
        }
        list.addAll(result);
        adapter.notifyDataSetChanged();
        
        if (list.size() > 0)
        {
            lay_activity.setVisibility(View.GONE);
            pull.setVisibility(View.VISIBLE);
        }
        else
        {
            lay_activity.setVisibility(View.VISIBLE);
            pull.setVisibility(View.GONE);
        }
    }
    
    private void loadData(final String activeId, int page, final boolean isLoadMore)
    {
        
        HistoryRankingManager.getInstance().queryActiveOrder(page, activeId, new UINotifyListener<AwardModelList>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                
                showNewLoading(true, getString(R.string.public_data_loading));
            }
            
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dismissLoading();
                if (checkSession())
                {
                    return;
                }
                if (object != null)
                {
                    //                    showToastInfo(object.toString());
                }
            }
            
            @Override
            public void onSucceed(AwardModelList result)
            {
                super.onSucceed(result);
                dismissLoading();
                
                if (result != null)
                {
                    if (result.getAwardModelList() != null && result.getAwardModelList().size() > 0)
                    {
                        if (null != result.getAwardModel())
                        {
                            cashierDetailsAdapter.setUserId(result.getAwardModel().getUserId());
                        }
                        //                        adapter =
                        //                            new CashierDetailsAdapter(CashierHistoryActivity.this, result.getAwardModelList(),
                        //                                result.getAwardModel().getUserId());
                        //                        listView.setAdapter(adapter);
                        
                        if (result.getAwardModel() != null)
                        {
                            lay_activity.setVisibility(View.GONE);
                            setMyView(result.getAwardModel());
                            if (!TextUtils.isEmpty(activeId))
                            {
                                
                                if (null != result.getAwardModel().getActiveState()
                                    && (result.getAwardModel().getActiveState() == 3 || result.getAwardModel()
                                        .getActiveState() == 4))
                                {
                                    tx_prompt.setVisibility(View.VISIBLE);
                                    if (!TextUtils.isEmpty(result.getAwardModel().getActiveOrderName()))
                                    {
                                        tx_prompt.setText(getString(R.string.tx_congrats)
                                            + result.getAwardModel().getActiveOrderName());
                                    }
                                    else
                                    {
                                        tx_prompt.setText(R.string.tx_come_on);
                                    }
                                }
                            }
                            
                        }
                        else
                        {
                            lay_title.setVisibility(View.GONE);
                            lay_activity.setVisibility(View.VISIBLE);
                            isTag = true;
                        }
                        if (awardModelList != null)
                        {
                            
                            mySetListData(cashier_listview,
                                awardModelList,
                                cashierDetailsAdapter,
                                result.getAwardModelList(),
                                isLoadMore);
                            if (isLoadMore)
                            {
                                pageFulfil += 1;
                            }
                            
                            cashier_listview.setMore(true);
                        }
                    }
                    else
                    {
                        //                        lay_activity.setVisibility(View.VISIBLE);
                        isTag = true;
                    }
                }
                else
                {
                    lay_activity.setVisibility(View.VISIBLE);
                    isTag = true;
                }
                
            }
            
        });
        if (TextUtils.isEmpty(activeId))
        {
            HistoryRankingManager.getInstance().queryActiveState(new UINotifyListener<AwardModel>()
            {
                @Override
                public void onPreExecute()
                {
                    super.onPreExecute();
                    
                    //                showLoading(false, "数据加载中...");
                }
                
                @Override
                public void onError(final Object object)
                {
                    super.onError(object);
                    //                dismissLoading();
                    if (checkSession())
                    {
                        return;
                    }
                    if (object != null)
                    {
                        Log.i("hehui", (object.toString()));
                    }
                }
                
                @Override
                public void onSucceed(AwardModel result)
                {
                    super.onSucceed(result);
                    if (result != null)
                    {
                        if (!TextUtils.isEmpty(result.getActiveTitle()))
                        {
                            tx_title.setText(R.string.tx_active_start);
                        }
                        
                        if (null == activeId || "".equals(activeId))
                        {
                            titleBar.setTitle(result.getActiveTitle());
                        }
                        //得到48小时的毫毛数
                        long totalM = 48 * 60 * 60;
                        if (result.getClosingDate() <= totalM && result.getClosingDate() > 0)
                        {//48小时以内就显示倒计时
                            if (TextUtils.isEmpty(activeId))
                            {
                                lay_time.setVisibility(View.VISIBLE);
                                Countdown(result.getClosingDate());
                            }
                            else
                            {
                                //                            tx_prompt.setVisibility(View.GONE);
                                lay_time.setVisibility(View.GONE);
                            }
                            
                        }
                    }
                    
                }
            });
        }
        
    }
    
    public static void startActivity(Context context, String activeId, String activeName)
    {
        Intent it = new Intent();
        it.setClass(context, CashierHistoryActivity.class);
        it.putExtra("activeId", activeId);
        it.putExtra("activeName", activeName);
        context.startActivity(it);
    }
    
    public static void startActivity(Context context, String activeId, String activeName, String content)
    {
        Intent it = new Intent();
        it.setClass(context, CashierHistoryActivity.class);
        it.putExtra("activeId", activeId);
        it.putExtra("activeName", activeName);
        it.putExtra("content", content);
        context.startActivity(it);
        
        SharedPreferences sp = context.getSharedPreferences(CashierHistoryActivity.class.getName(), 0);
        Editor editor = sp.edit();
        editor.putString(MainApplication.userName + "_activeId_" + activeId, content);
        editor.commit();
    }
    
    private void initObject()
    {
        
        awardModelList = new ArrayList<AwardModel>();
        cashierDetailsAdapter = new CashierDetailsAdapter(CashierHistoryActivity.this, awardModelList);
        //        cashierDetailsAdapter = new CashierDetailsAdapter(CashierHistoryActivity.this, list, userId)
        
        //        list = new ArrayList<String>();
        //        list.add("0.2");
        //        list.add("0.6");
        //        list.add("0.36");
        //        list.add("0.76");
        //        adapter = new CashierDetailsAdapter(CashierHistoryActivity.this, list);
        //        cashier_listview.setAdapter(adapter);
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        //        titleBar.setTitle("排名榜中午热是的");
        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.tx_history_rank));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                //历史排行榜
                showPage(HistoryActivity.class);
            }
            
            @Override
            public void onLeftButtonClick()
            {
                //                showPage(CashierManager.class);
                //finish();
                //                if (!TextUtils.isEmpty(activeId))
                //                {
                //                    MainActivity.startActivity(CashierHistoryActivity.this, "CashierHistoryActivity");
                //                }
                //                else
                //                {
                //                }
                //                finish();
                finish();
            }
        });
    }
    
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    @Override
    public void onRefresh()
    {
        pageFulfil = 0;
        loadData(activeId, pageFulfil, false);
    }
    
    @Override
    public void onLoadMore()
    {
        //        cashier_listview.setMore(true);
        loadData(activeId, pageFulfil + 1, true);
    }
    
}
