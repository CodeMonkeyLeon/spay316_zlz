package cn.swiftpass.enterprise.bussiness.logica.common;

import cn.swiftpass.enterprise.bussiness.model.CouponModel;
import cn.swiftpass.enterprise.bussiness.model.SelectListModel;

/**
 * 内容比较器
 * User: Alan
 * Date: 14-2-25
 * Time: 上午9:44
 */
public class Comparators {
    @SuppressWarnings("rawtypes")
    public static java.util.Comparator getComparator() {
        return new java.util.Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                if (o1 instanceof String) {
                    return compare((String) o1, (String) o2);
                } else if (o1 instanceof Integer) {
                    return compare((Integer) o1, (Integer) o2);
                } else if (o1 instanceof SelectListModel) {
                    return compare((SelectListModel) o1, (SelectListModel) o2);
                } else if (o1 instanceof CouponModel) {
                    return compare((CouponModel) o1, (CouponModel) o2);
                }  else {
                    System.err.println("未找到合适的比较器");
                    return 1;
                }
            }

            public int compare(String o1, String o2) {
                String s1 = (String) o1;
                String s2 = (String) o2;
                int len1 = s1.length();
                int len2 = s2.length();
                int n = Math.min(len1, len2);
                char v1[] = s1.toCharArray();
                char v2[] = s2.toCharArray();
                int pos = 0;
                while (n-- != 0) {
                    char c1 = v1[pos];
                    char c2 = v2[pos];
                    if (c1 != c2) { return c1 - c2; }
                    pos++;
                }
                return len1 - len2;
            }

            public int compare(Integer o1, Integer o2) {
                int val1 = o1.intValue();
                int val2 = o2.intValue();
                return (val1 < val2 ? -1 : (val1 == val2 ? 0 : 1));
            }

            public int compare(SelectListModel o1, SelectListModel o2) {
                int id1 = o1.id;
                int id2 = o2.id;
                return (compare(id1, id2));
            }
            public int compare(CouponModel o1, CouponModel o2) {
                int id1 = o1.couponId;
                int id2 = o2.couponId;
                return (compare(id1, id2));
            }

        };
    }

}
