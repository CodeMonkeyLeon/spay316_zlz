package cn.swiftpass.enterprise.ui.paymentlink.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.List;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkCustomer;

public class CustomerSelectAdapter extends BaseRecyclerAdapter<PaymentLinkCustomer> {

    private Context mContext;
    private OnItemClickListener click;

    public CustomerSelectAdapter(Context context, List<PaymentLinkCustomer> data) {
        super(R.layout.item_customer_select, data);
        mContext = context;
    }

    public void setOnItemClickListener(OnItemClickListener click) {
        this.click = click;
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder holder, PaymentLinkCustomer item, int position) {
        if (item == null) {
            return;
        }

        ConstraintLayout customerItem = holder.getView(R.id.cl_customer_select);
        TextView desc1 = holder.getView(R.id.tv_desc1);
        TextView desc2 = holder.getView(R.id.tv_desc2);
        TextView desc3 = holder.getView(R.id.tv_desc3);
        ImageView selectSymbol = holder.getView(R.id.iv_select_symbol);

        desc1.setText(item.custName);
        if (!TextUtils.isEmpty(item.custMobile)) {
            desc2.setText(mContext.getResources().getString(R.string.pl_customer_item_tel)
                    + item.custMobile);
        } else {
            desc2.setText("");
        }
        if (!TextUtils.isEmpty(item.custEmail)) {
            desc3.setText(mContext.getResources().getString(R.string.pl_customer_item_email)
                    + item.custEmail);
        } else {
            desc3.setText("");
        }
//        selectSymbol.setVisibility(data.isSelected ? View.VISIBLE : View.GONE);

        customerItem.setOnClickListener(null);
        customerItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                selectSymbol.setVisibility(View.VISIBLE);
                item.isSelected = true;
                if (click != null) {
                    click.onItemClick(position);
                }
            }
        });
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}
