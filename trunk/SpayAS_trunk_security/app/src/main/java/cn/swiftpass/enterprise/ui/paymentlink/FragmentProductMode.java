package cn.swiftpass.enterprise.ui.paymentlink;

import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.CREATE_ORDER_SUCCESS;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.CREATE_ORDER_SUCCESS_TAG;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.REQUEST_CODE_PICK_IMAGE;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.REQUEST_CODE_SELECT_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.REQUEST_CODE_SELECT_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_CREATE_ORDER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_CREATE_ORDER_SUCCESS;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_SELECT_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_SELECT_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.SUCCESS_TAG;
import static cn.swiftpass.enterprise.utils.DateUtil.getTime;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bigkoo.pickerview.TimePickerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.common.sentry.SentryUtils;
import com.example.common.utils.PngSetting;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.fragment.PaymentLinkContract;
import cn.swiftpass.enterprise.mvp.presenter.fragment.PaymentLinkPresenter;
import cn.swiftpass.enterprise.ui.fmt.BaseFragment;
import cn.swiftpass.enterprise.ui.paymentlink.adapter.OnListItemClickListener;
import cn.swiftpass.enterprise.ui.paymentlink.adapter.OtherFeeListAdapter;
import cn.swiftpass.enterprise.ui.paymentlink.adapter.ProductSelectedAdapter;
import cn.swiftpass.enterprise.ui.paymentlink.model.ImageUrl;
import cn.swiftpass.enterprise.ui.paymentlink.model.OrderConfig;
import cn.swiftpass.enterprise.ui.paymentlink.model.OtherFee;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkCustomer;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkOrder;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkProduct;
import cn.swiftpass.enterprise.ui.paymentlink.widget.SlideItemView1;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.ImageUtil;
import cn.swiftpass.enterprise.utils.PngUtils;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.permissionutils.OnPermissionCallback;
import cn.swiftpass.enterprise.utils.permissionutils.Permission;
import cn.swiftpass.enterprise.utils.permissionutils.PermissionDialogUtils;
import cn.swiftpass.enterprise.utils.permissionutils.PermissionsManage;

/**
 * Created by congwei.li on 2021/9/10.
 *
 * @Description:
 */
public class FragmentProductMode extends BaseFragment<PaymentLinkContract.Presenter> implements View.OnClickListener, PaymentLinkContract.View {

    public OrderConfig orderConfig;
    LinearLayout selectProductButton;
    LinearLayout selectedProduct;
    RecyclerView selectedProductList;
    TextView addProductMore;
    LinearLayout detailsTitle;
    LinearLayout detailsContent;
    LinearLayout addCustomer;
    LinearLayout addOtherFee;
    SlideItemView1 customerInfo;
    TextView uploadPhoto;
    FrameLayout flPhotoPreview;
    ImageView ivPhotoPreview;
    ImageView ivPhotoPreviewDelete;
    EditText notes;
    EditText periodValidity;
    TextView customerName;
    TextView customerPhone;
    TextView customerEmail;
    TextView tvAddOtherFee;
    TextView orderTotalAmount;
    TextView confirm;
    ImageView detailsTitleImage;
    RecyclerView rvOtherFee;
    Bitmap selectImage;
    PaymentLinkOrder orderData;
    OtherFeeListAdapter otherFeeListAdapter;
    ProductSelectedAdapter productSelectedAdapter;
    OrderCreateActivity mActivity;

    boolean isCreate;

    private String[] mPermissions;

    public FragmentProductMode(PaymentLinkOrder order, boolean isCreate) {
        this.isCreate = isCreate;
        orderData = order;
    }

    public FragmentProductMode(boolean isCreate) {
        this.isCreate = isCreate;
    }

    public void setOrderConfig(OrderConfig orderConfig) {
        this.orderConfig = orderConfig;
        if (!TextUtils.isEmpty(orderData.effectiveDate)) {
            return;
        }
        try {
            if (null != orderConfig && !TextUtils.isEmpty(orderConfig.orderExpireTime)
                    && !TextUtils.isEmpty(orderConfig.expireTimeUnit)) {
                long moreMill = 0;
                if (orderConfig.expireTimeUnit.equals("0")) { // 分钟
                    moreMill = Long.parseLong(orderConfig.orderExpireTime) * 60 * 1000;
                } else if (orderConfig.expireTimeUnit.equals("1")) {// 小时
                    moreMill = Long.parseLong(orderConfig.orderExpireTime) * 60 * 60 * 1000;
                } else if (orderConfig.expireTimeUnit.equals("2")) {// 天
                    moreMill = Long.parseLong(orderConfig.orderExpireTime) * 24 * 60 * 60 * 1000;
                }
                orderData.effectiveDate = DateUtil.formatTime(System.currentTimeMillis()
                        + moreMill);
            } else {
                orderData.effectiveDate = DateUtil.formatTime(System.currentTimeMillis()
                        + (24 * 60 * 60 * 1000));
            }
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            orderData.effectiveDate = DateUtil.formatTime(System.currentTimeMillis()
                    + (24 * 60 * 60 * 1000));
        }

        periodValidity.setText(orderData.effectiveDate);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OrderCreateActivity) {
            mActivity = (OrderCreateActivity) activity;
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_product_mode;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view == null) {
            return null;
        }
        if (null == orderData) {
            orderData = new PaymentLinkOrder();
            orderData.orderCostList = new ArrayList<>();
            orderData.orderRelateGoodsList = new ArrayList<>();
        } else {
            if (null == orderData.orderCostList) {
                orderData.orderCostList = new ArrayList<>();
            }
            if (null == orderData.orderRelateGoodsList) {
                orderData.orderRelateGoodsList = new ArrayList<>();
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            //android13
            mPermissions = Permission.Group.STORAGE_API_33;
        } else {
            mPermissions = Permission.Group.STORAGE;
        }

        initView(view);
        return view;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView(View view) {
        selectProductButton = view.findViewById(R.id.ll_select_product_button);
        selectedProduct = view.findViewById(R.id.ll_product_selected);
        selectedProductList = view.findViewById(R.id.rv_product_selected);
        addProductMore = view.findViewById(R.id.tv_add_product_more);
        detailsTitle = view.findViewById(R.id.ll_details_optional);
        detailsContent = view.findViewById(R.id.ll_details_content);
        addCustomer = view.findViewById(R.id.ll_add_customer);
        addOtherFee = view.findViewById(R.id.ll_add_other_fee);
        customerInfo = view.findViewById(R.id.ll_customer_item);
        uploadPhoto = view.findViewById(R.id.tv_upload_photo);
        flPhotoPreview = view.findViewById(R.id.fl_photo_preview);
        ivPhotoPreview = view.findViewById(R.id.im_photo_preview);
        ivPhotoPreviewDelete = view.findViewById(R.id.im_photo_preview_delete);
        notes = view.findViewById(R.id.et_notes);
        periodValidity = view.findViewById(R.id.et_validity);
        customerName = view.findViewById(R.id.tv_customer_name);
        customerPhone = view.findViewById(R.id.tv_customer_phone);
        customerEmail = view.findViewById(R.id.tv_customer_email);
        tvAddOtherFee = view.findViewById(R.id.tv_add_other_fee);
        orderTotalAmount = view.findViewById(R.id.tv_order_total_amount);
        confirm = view.findViewById(R.id.tv_add_order_confirm);
        detailsTitleImage = view.findViewById(R.id.iv_show_detail);
        rvOtherFee = view.findViewById(R.id.rv_other_fee);

        selectProductButton.setOnClickListener(this);
        addProductMore.setOnClickListener(this);
        showSelectedProductView();
        addOtherFee.setVisibility(View.VISIBLE);
        detailsContent.setVisibility(orderData.isHaveData() ? View.VISIBLE : View.GONE);
        confirm.setOnClickListener(this);
        confirm.setText(isCreate ? getStringById(R.string.payment_link_add_order_confirm_order) :
                getStringById(R.string.pl_edit_order_save));

        notes.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    //通知父控件不要干扰
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                    //通知父控件不要干扰
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    view.getParent().requestDisallowInterceptTouchEvent(false);
                }
                return false;
            }
        });

        customerInfo.setOnStateChangeListener(new SlideItemView1.OnStateChangeListener() {
            @Override
            public void onFuncShow(SlideItemView1 itemView, boolean isShow) {
            }

            @Override
            public void onFuncClick() {
                if (null != customerInfo && customerInfo.isExpansion()) {
                    customerInfo.reset();
                }
                orderData.customer = new PaymentLinkCustomer();
                orderData.custId = "";
                setCustomerView();
            }

            @Override
            public void onContentClick() {
            }
        });

        periodValidity.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(periodValidity.getText().toString().trim())) {
                    orderData.effectiveDate = periodValidity.getText().toString().trim();
                }
            }
        });
        periodValidity.setText(orderData.effectiveDate);
        periodValidity.setFocusable(false);
        periodValidity.setOnClickListener(this);

        if (null == orderData.orderCostList || orderData.orderCostList.size() < 5) {
            tvAddOtherFee.setVisibility(View.VISIBLE);
        } else {
            tvAddOtherFee.setVisibility(View.GONE);
        }
        otherFeeListAdapter = new OtherFeeListAdapter(getContext(), orderData.orderCostList);
        otherFeeListAdapter.setItemClickListener(new OnListItemClickListener() {
            @Override
            public void onDeleteClick(int position) {
                if (orderData.orderCostList.size() > position) {
                    orderData.orderCostList.remove(orderData.orderCostList.get(position));
                    otherFeeListAdapter.notifyDataSetChanged();
                    updateAmount();
                }
                if (orderData.orderCostList.size() < 5) {
                    tvAddOtherFee.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onItemClick(int position) {
            }

            @Override
            public void onDeleteShow(int position, boolean isShow) {
            }
        });
        otherFeeListAdapter.setTextChange(new OtherFeeListAdapter.TextChange() {
            @Override
            public void onNameEdit(int position, String name) {
                if (orderData.orderCostList.size() > position) {
                    orderData.orderCostList.get(position).extraCostDesc = name;
                }
            }

            @Override
            public void onAmountEdit(int position, String amount) {
                if (orderData.orderCostList.size() > position) {
                    if (!TextUtils.isEmpty(amount)) {
                        orderData.orderCostList.get(position).extraCost =
                                DateUtil.formatMoneyInt(amount);
                    } else {
                        orderData.orderCostList.get(position).extraCost = "";
                    }
                    updateAmount();
                }
            }
        });
        tvAddOtherFee.setOnClickListener(this);
        rvOtherFee.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));
        rvOtherFee.setAdapter(otherFeeListAdapter);

        // 设置可换行edittext，限制200字符
        notes.setSingleLine(false);
        notes.setMaxLines(10);
        notes.setHorizontallyScrolling(false);
        notes.setGravity(Gravity.TOP);
        notes.setFilters(new InputFilter[]{new InputFilter.LengthFilter(200)});
        notes.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(notes.getText().toString().trim())) {
                    return;
                }
                orderData.orderRemark = notes.getText().toString().trim();
            }
        });
        if (!TextUtils.isEmpty(orderData.orderRemark)) {
            notes.setText(orderData.orderRemark);
        }

        if (!TextUtils.isEmpty(orderData.custId)) {
            orderData.customer = new PaymentLinkCustomer();
            orderData.customer.custId = orderData.custId;
            orderData.customer.custName = orderData.custName;
            orderData.customer.custMobile = orderData.custMobile;
            orderData.customer.custEmail = orderData.custEmail;
        }
        setCustomerView();

        detailsTitleImage.setRotation(detailsContent.getVisibility() == View.VISIBLE ? 270f : 90f);

        selectedProductList.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));
        productSelectedAdapter = new ProductSelectedAdapter(getContext(), orderData.orderRelateGoodsList);
        productSelectedAdapter.setOnItemClickListener(new OnListItemClickListener() {
            @Override
            public void onDeleteClick(int position) {
                if (orderData.orderRelateGoodsList.size() > position) {
                    orderData.orderRelateGoodsList.remove(orderData.orderRelateGoodsList.get(position));
                    productSelectedAdapter.notifyDataSetChanged();
                }
                showSelectedProductView();
            }

            @Override
            public void onItemClick(int position) {
            }

            @Override
            public void onDeleteShow(int position, boolean isShow) {
            }
        });
        productSelectedAdapter.setOnProductNumChange(new ProductSelectedAdapter.OnProductNumChange() {
            @Override
            public void onProductChange(PaymentLinkProduct data) {
                for (int i = 0; i < orderData.orderRelateGoodsList.size(); i++) {
                    if (orderData.orderRelateGoodsList.get(i).goodsId.equals(data.goodsId)) {
                        orderData.orderRelateGoodsList.get(i).goodsNum = data.goodsNum;
                        updateAmount();
                    }
                }
            }
        });
        selectedProductList.setAdapter(productSelectedAdapter);

        detailsTitle.setOnClickListener(this);
        addCustomer.setOnClickListener(this);

        uploadPhoto.setOnClickListener(this);
        ivPhotoPreviewDelete.setOnClickListener(this);
        updateAmount();
        showSelectPhoto(!TextUtils.isEmpty(orderData.picUrl));
        if (!TextUtils.isEmpty(orderData.picUrl)) {
            Glide.with(mActivity)
                    .load(orderData.picUrl)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(ivPhotoPreview);
        }


        if (PngSetting.INSTANCE.isChangePng1()) {
            initPng(view);
        }
    }

    private void initPng(View v) {
        //id_img_add
        ImageView imgAdd = v.findViewById(R.id.id_img_add);
        PngUtils.INSTANCE.setPng1(getContext(), imgAdd, R.drawable.icon_me_add);
    }

    private void showSelectPhoto(boolean isShow) {
        uploadPhoto.setVisibility(isShow ? View.GONE : View.VISIBLE);
        flPhotoPreview.setVisibility(isShow ? View.VISIBLE : View.GONE);
        if (!isShow) {
            selectImage = null;
        }
    }

    private void toSelectImage() {
        if (!PermissionsManage.isGranted(mActivity, mPermissions)) {
            requestLocalPermission();
            return;
        }
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CODE_PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CREATE_ORDER_SUCCESS && resultCode == RESULT_CODE_CREATE_ORDER_SUCCESS) {
            Intent i = new Intent();
            mActivity.setResult(RESULT_CODE_CREATE_ORDER, i);
            if (!mActivity.isFinishing()) {
                mActivity.isFinishing();
            }
        }
        if (null != data && requestCode == REQUEST_CODE_PICK_IMAGE
                && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            Bitmap resultBimap = ImageUtil.getBitmapFromPath(mActivity, uri);
            if (resultBimap != null) {
                selectImage = ImageUtil.compressImage(resultBimap);
                showSelectPhoto(true);
                ivPhotoPreview.setImageBitmap(selectImage);
            }
        }
        if (null != data && requestCode == REQUEST_CODE_SELECT_CUSTOMER
                && resultCode == RESULT_CODE_SELECT_CUSTOMER) {
            orderData.customer = (PaymentLinkCustomer) data.getSerializableExtra("result");
            if (null != orderData.customer) {
                orderData.custId = orderData.customer.custId;
                orderData.custName = orderData.customer.custName;
                orderData.custMobile = orderData.customer.custMobile;
                orderData.custEmail = orderData.customer.custEmail;
            }
            setCustomerView();
        }
        if (null != data && requestCode == REQUEST_CODE_SELECT_PRODUCT
                && resultCode == RESULT_CODE_SELECT_PRODUCT) {
            ArrayList<PaymentLinkProduct> selectGoods =
                    (ArrayList<PaymentLinkProduct>) data.getSerializableExtra("result");
            orderData.orderRelateGoodsList.clear();
            orderData.orderRelateGoodsList.addAll(selectGoods);
            productSelectedAdapter.notifyDataSetChanged();
            showSelectedProductView();
        }
        if (requestCode == PermissionsManage.REQUEST_CODE) {
            if (PermissionsManage.isGranted(mActivity, mPermissions)) {
                toSelectImage();
            } else {
                PermissionDialogUtils.finishWithoutPermission(mActivity,
                        mPermissions, false);
            }
        }
    }

    private void updateAmount() {
        BigDecimal amount = new BigDecimal("0");
        for (int i = 0; i < orderData.orderRelateGoodsList.size(); i++) {
            try {
                BigDecimal productPrice = (new BigDecimal(orderData.orderRelateGoodsList.get(i).goodsPrice))
                        .multiply((new BigDecimal(orderData.orderRelateGoodsList.get(i).goodsNum)));
                amount = amount.add(productPrice);
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
            }
        }
        for (int i = 0; i < orderData.orderCostList.size(); i++) {
            try {
                BigDecimal otherFee = new BigDecimal(orderData.orderCostList.get(i).extraCost);
                amount = amount.add(otherFee);
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
            }
        }
        orderTotalAmount.setText(MainApplication.getInstance().getFeeFh() + " " +
                DateUtil.formatMoneyUtils(amount.toString()));
        orderData.totalFee = amount.toString();
        // 金额大于0且小于等于999999999
        confirm.setEnabled(null != orderData.orderRelateGoodsList &&
                orderData.orderRelateGoodsList.size() > 0 &&
                amount.compareTo(new BigDecimal("0")) > 0 &&
                amount.compareTo(new BigDecimal("100000000000")) < 0);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_select_product_button://选择商品按钮
            case R.id.tv_add_product_more://选择更多商品按钮
                openProductSelect();
                break;
            case R.id.tv_add_other_fee://添加其他费用
                if (!checkOtherFee()) {
                    showErrorDialog(getStringById(R.string.payment_link_add_order_add_other_fees_error));
                    return;
                }
                orderData.orderCostList.add(new OtherFee());
                otherFeeListAdapter.notifyDataSetChanged();
                if (orderData.orderCostList.size() >= 5) {
                    tvAddOtherFee.setVisibility(View.GONE);
                }
                break;
            case R.id.ll_details_optional://显示更多信息
                detailsContent.setVisibility(detailsContent.getVisibility() == View.VISIBLE ?
                        View.GONE : View.VISIBLE);
                detailsTitleImage.setRotation(detailsContent.getVisibility() == View.VISIBLE ?
                        270f : 90f);
                break;
            case R.id.ll_add_customer://选择顾客按钮
                openCustomerSelect();
                break;
            case R.id.et_validity://选择过期时间
                mActivity.showDateSelect(orderData.effectiveDate,
                        new TimePickerView.OnTimeSelectListener() {
                            @Override
                            public void onTimeSelect(Date date, View v) {
                                periodValidity.setText(
                                        DateUtil.getAllNormalTime(getTime(date)));
                            }
                        });
                break;
            case R.id.tv_upload_photo://选择要上传的图片
                if (mActivity == null) {
                    return;
                }
                toSelectImage();
                break;
            case R.id.im_photo_preview_delete://删除预览的图片
                showSelectPhoto(false);
                orderData.picUrl = "";
                break;
            case R.id.tv_add_order_confirm://确认新增订单
                if (!checkOtherFee()) {
                    showErrorDialog(getStringById(R.string.payment_link_add_order_error));
                    return;
                }
                if (mActivity != null) {
                    DialogOrderInfoConfirm dialogOrderInfoConfirm =
                            new DialogOrderInfoConfirm(mActivity, orderData);
                    dialogOrderInfoConfirm.setCurrentListener(
                            new DialogOrderInfoConfirm.DialogClickListener() {
                                @Override
                                public void onClickConfirm() {
                                    uploadImg();
                                }
                            });
                    if (!dialogOrderInfoConfirm.isShowing()) {
                        dialogOrderInfoConfirm.show();
                    }
                }
                break;
        }
    }

    private void requestLocalPermission() {
        PermissionDialogUtils.requestPermission(mActivity,
                mPermissions, false,
                new OnPermissionCallback() {
                    @Override
                    public void onGranted(List<String> permissions, boolean all) {
                        if (all) {
                            toSelectImage();
                        }
                    }
                });
    }

    @Override
    public void uploadImgSuccess(@NonNull ImageUrl response) {
        orderData.picUrl = response.picUrl;
        requestCreateOrder(orderData);
    }

    @Override
    public void uploadImgFailed(@Nullable Object error) {
        if (null != mActivity) {
            toastDialog(mActivity, error.toString(), null);
        }
    }

    private void uploadImg() {
        if (selectImage != null) {
            if (mPresenter != null) {
                mPresenter.uploadImg(ImageUtil.bitmapToBase64(selectImage), "jpg");
            }
        } else {
            requestCreateOrder(orderData);
        }
    }


    @Override
    public void addNewOrderSuccess(@NonNull PaymentLinkOrder response) {
        SharedPreUtils.saveObject(SUCCESS_TAG, CREATE_ORDER_SUCCESS_TAG);
        OrderDetailsActivity.startActivityForResult(mActivity, response, CREATE_ORDER_SUCCESS);
        if (!mActivity.isFinishing()) {
            mActivity.finish();
        }
    }

    @Override
    public void addNewOrderFailed(@Nullable Object error) {
        if (null != mActivity) {
            toastDialog(mActivity, error.toString(), null);
        }
    }


    @Override
    public void editOrderSuccess(@NonNull String response) {
        SharedPreUtils.saveObject(SUCCESS_TAG, CREATE_ORDER_SUCCESS_TAG);
        if (!mActivity.isFinishing()) {
            mActivity.finish();
        }
    }

    @Override
    public void editOrderFailed(@Nullable Object error) {
        if (null != mActivity) {
            toastDialog(mActivity, error.toString(), null);
        }
    }

    private void requestCreateOrder(PaymentLinkOrder orderData) {
        if (isCreate) {
            if (mPresenter != null) {
                mPresenter.addNewOrder(orderData);
            }
        } else {
            if (mPresenter != null) {
                mPresenter.editOrder(orderData);
            }
        }
    }


    private void openCustomerSelect() {
        CustAndProdSelectActivity.startActivityForResult(this, orderData.customer,
                REQUEST_CODE_SELECT_CUSTOMER);
    }

    private void openProductSelect() {
        CustAndProdSelectActivity.startActivityForResult(this, orderData.orderRelateGoodsList,
                REQUEST_CODE_SELECT_PRODUCT);
    }

    private void setCustomerView() {
        customerInfo.setVisibility(!TextUtils.isEmpty(orderData.custId) ? View.VISIBLE : View.GONE);
        if (!TextUtils.isEmpty(orderData.custId)) {
            customerName.setText(orderData.custName);
            if (!TextUtils.isEmpty(orderData.custMobile)) {
                customerPhone.setText(mContext.getResources().getString(R.string.pl_customer_item_tel) + " "
                        + orderData.custMobile);
            } else {
                customerPhone.setText("");
            }
            if (!TextUtils.isEmpty(orderData.custEmail)) {
                customerEmail.setText(mContext.getResources().getString(R.string.pl_customer_item_email) + " "
                        + orderData.custEmail);
            } else {
                customerEmail.setText("");
            }
        }
    }

    private boolean showSelectedProductView() {
        updateAmount();
        if (null == orderData.orderRelateGoodsList || orderData.orderRelateGoodsList.size() <= 0) {
            selectProductButton.setVisibility(View.VISIBLE);
            selectedProduct.setVisibility(View.GONE);
            return false;
        }
        selectProductButton.setVisibility(View.GONE);
        selectedProduct.setVisibility(View.VISIBLE);
        return true;
    }

    public void showErrorDialog(String error) {
        if (mActivity == null) {
            return;
        }
        toastDialog(mActivity, error, null);

    }

    private boolean checkOtherFee() {
        if (null == orderData || null == orderData.orderCostList || orderData.orderCostList.size() == 0) {
            return true;
        }
        for (OtherFee otherFee : orderData.orderCostList) {
            if (!otherFee.isAllFull()) {
                return false;
            }
        }
        return true;
    }


    @Override
    protected PaymentLinkContract.Presenter createPresenter() {
        return new PaymentLinkPresenter();
    }
}
