package cn.swiftpass.enterprise.broadcast;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.igexin.sdk.PushConsts;

import java.util.List;
import java.util.Map;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.WelcomeActivity;
import cn.swiftpass.enterprise.ui.activity.bill.BillMainActivity;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.TtsVoice;

public class PushReceiver extends BroadcastReceiver {

    /**
     * 应用未启动, 个推 service已经被唤醒,保存在该时间段内离线消息(此时 GetuiSdkDemoActivity.tLogView == null)
     */
    public static StringBuilder payloadData = new StringBuilder();

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        switch (bundle.getInt(PushConsts.CMD_ACTION)) {
            case PushConsts.GET_MSG_DATA:
                //  HistoryActivity.startActivity(context);
                // 获取透传数据
                String appid = bundle.getString("appid");
                byte[] payload = bundle.getByteArray("payload");
                // String taskid = bundle.getString("taskid");
                //String messageid = bundle.getString("messageid");
                if (payload != null && payload.length > 0) {
                    String data = new String(payload);
                    Gson gson = new Gson();
                    PushTransmissionModel pushTransmissionModel = null;
                    try {
                        pushTransmissionModel = gson.fromJson(data, PushTransmissionModel.class);
                    } catch (Exception e) {

                    }
                    if (pushTransmissionModel != null) {
                        try {
                            String voice = PreferenceUtil.getString("voice", "open");
                            if (!StringUtil.isEmptyOrNull(data)) {

                                if (voice.equalsIgnoreCase("open")) {
                                    if (pushTransmissionModel.getMessageKind() == PushTransmissionModel.MESSAGE_ORDER) { //收款成功 语音播放
                                        TtsVoice ttsVoice = new TtsVoice(context, pushTransmissionModel.getVoiceContent());
                                        ttsVoice.startVoice();
                                    }
                                }
                            }

                        } catch (Exception e) {

                        }

                        showNotify(context, pushTransmissionModel);
                    }

                }

                break;

            case PushConsts.GET_CLIENTID:
                // 获取ClientID(CID)
                // 第三方应用需要将CID上传到第三方服务器，并且将当前用户帐号和CID进行关联，以便日后通过用户帐号查找CID进行消息推送
                String cid = bundle.getString("clientid");
                //                if (GetuiSdkDemoActivity.tView != null) {
                //                    GetuiSdkDemoActivity.tView.setText(cid);
                //                }
                break;

            case PushConsts.THIRDPART_FEEDBACK:
                /*
                 * String appid = bundle.getString("appid"); String taskid =
                 * bundle.getString("taskid"); String actionid = bundle.getString("actionid");
                 * String result = bundle.getString("result"); long timestamp =
                 * bundle.getLong("timestamp");
                 * 
                 * Log.d("GetuiSdkDemo", "appid = " + appid); Log.d("GetuiSdkDemo", "taskid = " +
                 * taskid); Log.d("GetuiSdkDemo", "actionid = " + actionid); Log.d("GetuiSdkDemo",
                 * "result = " + result); Log.d("GetuiSdkDemo", "timestamp = " + timestamp);
                 */
                break;

            default:
                break;
        }
    }

    private void showNotify(Context context, PushTransmissionModel pushTransmissionModel) {
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        //        Intent intent = new Intent(context, MainActivity.class);
        //        intent.putExtra("registPromptStr", "OrderDetailsActivity");
        //        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        //        mBuilder.setContentIntent(pendingIntent);


        if (!isBackground(context)) {
            //如果当前APP在前台点击就跳转到账单页面、
            Intent intent = new Intent(context, BillMainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
            mBuilder.setContentIntent(pendingIntent);
        } else {
            //如果当前APP不在前台，点击消息通知就跳转到首页
            Intent intent = new Intent(context, WelcomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
            mBuilder.setContentIntent(pendingIntent);

        }

        mBuilder.setAutoCancel(true);
        mBuilder.setContentTitle(pushTransmissionModel.getTitle())// 设置通知栏标题
                .setContentText(pushTransmissionModel.getVoiceContent()).setOngoing(false)
                // ture，设置他为一个正在进行的通知。他们通常是用来表示一个后台任务,用户积极参与(如播放音乐)或以某种方式正在等待,因此占用设备(如一个文件下载,同步操作,主动网络连接)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                // 向通知添加声音、闪灯和振动效果的最简单、最一致的方式是使用当前的用户默认设置，使用defaults属性，可以组合
                // Notification.DEFAULT_ALL Notification.DEFAULT_SOUND 添加声音 // requires VIBRATE
                // permission
                .setSmallIcon(R.drawable.icon_app_logo);// 设置通知小ICON

        mNotificationManager.notify(0, mBuilder.build());
    }

    //用来检测当前APP是否在前台运行
    public static boolean isBackground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.processName.equals(context.getPackageName())) {
                /*
                BACKGROUND=400 EMPTY=500 FOREGROUND=100
                GONE=1000 PERCEPTIBLE=130 SERVICE=300 ISIBLE=200
                 */
                if (appProcess.importance != ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {

                    return true;
                } else {

                    return false;
                }
            }
        }
        return false;
    }


    private void doOrderPushMessage(Context context, PushTransmissionModel pushTransmissionModel) {
        if (pushTransmissionModel.getContentFormat() == PushTransmissionModel.CONTENT_FORMAT_JSON) {
            //            Gson gson = new Gson();
            //            
            //            Map<String, String> map = null;
            //            try
            //            {
            //                map = gson.fromJson(pushTransmissionModel.getContent(), new TypeToken<Map<String, Object>>()
            //                {
            //                }.getType());
            //                if (map != null)
            //                {
            //                    String activeId = (String)map.get("activeId");
            //                    String content = (String)map.get("content");
            //                    String activeName = (String)map.get("activeName");
            //                    Context lastContext = context;

            //            WelcomeActivity.startActivity(context, String.valueOf(activeId), activeName, content);

            //            MainActivity.startActivity(context, "OrderDetailsActivity");
            WelcomeActivity.startActivity(context);
            //            HandlerManager.notifyMessage(HandlerManager.SHOWMERCHANT, HandlerManager.SHOWMERCHANT);
            //                }
            //            }
            //            catch (Exception e)
            //            {
            //                Log.e("hehui", "fromJson error", e);
            //            }

        }
    }

    private void doCashierPushMessage(Context context, PushTransmissionModel pushTransmissionModel) {
        if (pushTransmissionModel.getContentFormat() == PushTransmissionModel.CONTENT_FORMAT_JSON) {
            Gson gson = new Gson();

            Map<String, String> map = null;
            try {
                map = gson.fromJson(pushTransmissionModel.getContent(), new TypeToken<Map<String, Object>>() {
                }.getType());
                if (map != null) {
                    String activeId = (String) map.get("activeId");
                    String content = (String) map.get("content");
                    String activeName = (String) map.get("activeName");
                    Context lastContext = context;
                    //                    boolean newTask = false;
                    //                    if (MainApplication.getContext() == null)
                    //                    {
                    //                        newTask = true;
                    //                    }
                    //                    else
                    //                    {
                    //                        WeakReference<Activity> wr = MainApplication.getContext().getLastActivityRef();
                    //                        if (wr != null && wr.get() != null)
                    //                        {
                    //                            lastContext = wr.get();
                    //                        }
                    //                    }

                    WelcomeActivity.startActivity(lastContext, String.valueOf(activeId), activeName, content);

                }
            } catch (Exception e) {

            }

        }
    }
}
