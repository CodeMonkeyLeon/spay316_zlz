/*
 * 文 件 名:  ReportActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-8
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.bill;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.refund.RefundManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.dialog.RefundConditonConfirmDialog;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 退款
 * 
 * @author  he_hui
 * @version  [版本号, 2016-11-8]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class OrderRefundActivity extends TemplateActivity
{
    
    private Order orderModel;
    
    private TextView tx_order, tv_money, tv_refunding;
    
    private EditText et_money;
    
    private Button btn_next_step;

    private RefundConditonConfirmDialog Dialog_Refund;
    private double minus = 1;
    
    public static void startActivity(Context context, Order orderModel)
    {
        Intent it = new Intent();
        it.setClass(context, OrderRefundActivity.class);
        it.putExtra("order", orderModel);
        context.startActivity(it);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_order_refund);
        MainApplication.listActivities.add(this);
        //根据当前的小数点的位数来判断应该除以多少
        for(int i = 0 ; i < MainApplication.numFixed ; i++ ){
            minus = minus * 10;
        }
        initview();
        CharSequence text = et_money.getText();
        //Debug.asserts(text instanceof Spannable);
        if (text instanceof Spannable)
        {
            Spannable spanText = (Spannable)text;
            Selection.setSelection(spanText, text.length());
        }
        setLister();
        btn_next_step.getBackground().setAlpha(102);
        showSoftInputFromWindow(OrderRefundActivity.this, et_money);
        orderModel = (Order)getIntent().getSerializableExtra("order");
        if (orderModel != null)
        {
            tx_order.setText(orderModel.getOrderNoMch());
            tv_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(orderModel.getMoney()));
            if (orderModel.getRfMoneyIng() > 0 || orderModel.getRefundMoney() > 0)
            {//部分退款
                tv_refunding.setText(R.string.tx_can_refund_money);
                long refMoney = orderModel.getMoney() - orderModel.getRfMoneyIng() - orderModel.getRefundMoney();
                tv_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(refMoney));
            }
            
        }
        
        setPricePoint(et_money);
    }
    
    private void setPricePoint(final EditText editText)
    {
        editText.addTextChangedListener(new TextWatcher()
        {
            
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(MainApplication.numFixed == 0){//即没有小数点的位数，最小币种单位为1
                    //如果币种的最小单位为1，则不让输入小数点
                    if (s.toString().contains(".")){
                        s = s.toString().subSequence(0, s.toString().indexOf("."));
                        editText.setText(s);
                        editText.setSelection(s.length());
                    }
                } else {//如果有小数点位数，则根据位数判断输入的截取
                    if (s.toString().contains("."))
                    {
                        if (s.length() - 1 - s.toString().indexOf(".") > MainApplication.numFixed)
                        {
                            s = s.toString().subSequence(0, s.toString().indexOf(".") + MainApplication.numFixed+1);
                            editText.setText(s);
                            editText.setSelection(s.length());
                        }
                    }
                    if (s.toString().trim().substring(0).equals("."))
                    {
                        s = "0" + s;
                        editText.setText(s);
                        editText.setSelection(2);
                    }

                    if (s.toString().startsWith("0") && s.toString().trim().length() > 1)
                    {
                        if (!s.toString().substring(1, 2).equals("."))
                        {
                            editText.setText(s.subSequence(0, 1));
                            editText.setSelection(1);
                            return;
                        }
                    }

                  /*  if (s.toString().startsWith("0") && s.toString().equals("0.00"))
                    {
                        editText.setText(s.subSequence(0, s.length() - 1));
                        editText.setSelection(s.length() - 1);
                        return;
                    }*/
                }

                //限制输入的最大金额不能超过可退金额
                if(s.length() > 0 ){
                    String moneyText = et_money.getText().toString();
                    Pattern p = Pattern.compile("[^0-9.]");//去掉当前字符串中除去0-9以及小数点的其他字符
                    Matcher m = p.matcher(moneyText);
                    moneyText = m.replaceAll("");

                    if(moneyText.equals(".")){//如果输入的是小数点则不走下面的逻辑
                        return;
                    }
                    long refound = 0;
                    if(!TextUtils.isEmpty(moneyText)){
                        BigDecimal bigDecimal = new BigDecimal(moneyText).multiply(new BigDecimal(minus));
                        refound = bigDecimal.longValue();
                    }

                    long canRefound = orderModel.getMoney();

                    if (orderModel.getRfMoneyIng() > 0 || orderModel.getRefundMoney() > 0)
                    {//部分退款
                        long refMoney = orderModel.getMoney() - orderModel.getRfMoneyIng() - orderModel.getRefundMoney();
                        canRefound = refMoney;
                    }
                    if(refound > canRefound){
                        et_money.setText(s.subSequence(0,s.length()-1));
                        et_money.setSelection(et_money.getText().length());
                    }
                }


              /*  if (s.toString().length() > 0
                        && !(s.toString().equals("0.0") || s.toString().equals("0.00") || s.toString().equals("0.")
                        || s.toString().equals("0")))
                {
                    String inputMoneyText = s.toString();
                    Pattern p = Pattern.compile("[^0-9.]");//去掉当前字符串中除去0-9以及小数点的其他字符
                    Matcher matcher = p.matcher(inputMoneyText);
                    inputMoneyText = matcher.replaceAll("");

                    BigDecimal bigDecimal = new BigDecimal(inputMoneyText);
                    double inputMoney = bigDecimal.doubleValue();

                    double m = 0;//订单金额
                    String tv_m = DateUtil.formatMoneyUtils(orderModel.getMoney());
                    if (tv_m.contains(","))
                    {
                        m = Double.parseDouble(tv_m.replace(",", ""));
                    }
                    else
                    {
                        m = Double.parseDouble(tv_m);
                    }

                    if (m >= inputMoney)
                    {
                        setButtonBg(btn_next_step, true, R.string.tx_bill_stream_refund_cirfom);
                    }

                } else {
                    setButtonBg(btn_next_step, false, R.string.tx_bill_stream_refund_cirfom);
                }*/
            }
            
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                
            }
            
            @Override
            public void afterTextChanged(Editable s)
            {
                String inputMoneyText = et_money.getText().toString();
                Pattern p = Pattern.compile("[^0-9.]");//去掉当前字符串中除去0-9以及小数点的其他字符
                Matcher matcher = p.matcher(inputMoneyText);
                inputMoneyText = matcher.replaceAll("");
                double inputMoney = 0;
                if(!TextUtils.isEmpty(inputMoneyText)){
                    BigDecimal bigDecimal = new BigDecimal(inputMoneyText);
                    inputMoney = bigDecimal.doubleValue();
                }

                if(s.toString().length() > 0 && inputMoney != 0){
                    setButtonBg(btn_next_step, true, R.string.tx_bill_stream_refund_cirfom);
                }else{
                    setButtonBg(btn_next_step, false, R.string.tx_bill_stream_refund_cirfom);
                }
            }
            
        });
        
    }

    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b,enable,res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_press_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }
    
    private void setLister()
    {
        btn_next_step.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                String input_money = et_money.getText().toString();
                if (TextUtils.isEmpty(input_money))
                {
                    
                    toastDialog(OrderRefundActivity.this, R.string.et_refund_moeny, null);
                    return;
                }
                if (input_money.contains(".") && input_money.substring(input_money.lastIndexOf('.')).equals("."))
                {
                    toastDialog(OrderRefundActivity.this, R.string.et_refund_format, null);
                    et_money.setFocusable(true);
                    return;
                }
                long editMoney = 0;
                if (input_money.contains("."))
                {
                    //                    double d = Double.parseDouble(input_money) * 100;
                    editMoney = Long.parseLong(OrderManager.getInstance().getMoney(input_money));
                }
                else
                {
                    editMoney = Long.parseLong(input_money) * (long) minus;
                }
                if (editMoney > orderModel.getMoney())
                {
                    toastDialog(OrderRefundActivity.this, R.string.et_refund_less_money, null);
                    et_money.setFocusable(true);
                    return;
                }

                //验证是否可以发起退款
                orderModel.setRefundFeel(editMoney);
                requestRefoundState();

            }
        });
    }

    private void requestRefoundState(){
        RefundManager.getInstant().checkCanRefundOrNot(orderModel.getOutTradeNo(),
                orderModel.getMoney(),
                orderModel.getRefundFeel(),
                new UINotifyListener<Boolean>(){
                    @Override
                    public void onError(Object object) {
                        super.onError(object);
                        dismissLoading();
                        if (object != null) {
                            if(object instanceof  Integer){
                                int resultCode = (Integer) object;
                                switch (resultCode) {
                                    case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                        ToastHelper.showToastUIThread( OrderRefundActivity.this ,ToastHelper.toStr(R.string.show_net_bad));
                                    case RequestResult.RESULT_TIMEOUT_ERROR:
                                        ToastHelper.showToastUIThread( OrderRefundActivity.this ,ToastHelper.toStr(R.string.show_net_timeout));
                                    case RequestResult.RESULT_READING_ERROR:
                                        ToastHelper.showToastUIThread( OrderRefundActivity.this ,ToastHelper.toStr(R.string.show_net_server_fail));
                                }
                            }else {
                                //如果请求失败则弹框提示当前的失败信息
                                final String message = object.toString();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Dialog_Refund = new RefundConditonConfirmDialog(OrderRefundActivity.this, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                //如果点击全额退款的处理，此处就要更改输入金额，然后跳转到确认身份的页面
                                                //如果点击全额退款，则要，先判断是否是部分退款的全额退款
                                                orderModel.setRefundFeel(orderModel.getMoney());
                                                et_money.setText(DateUtil.formatMoneyUtils(orderModel.getMoney()));
                                                //部分退款
                                                if (orderModel.getRfMoneyIng() > 0 || orderModel.getRefundMoney() > 0)
                                                {
                                                    long refMoney = orderModel.getMoney() - orderModel.getRfMoneyIng() - orderModel.getRefundMoney();
                                                    orderModel.setRefundFeel(refMoney);
                                                    et_money.setText(DateUtil.formatMoneyUtils(refMoney));
                                                }

                                                et_money.setSelection(et_money.length());//将光标追踪到内容的最后
                                                Dialog_Refund.dismiss();
                                                OrderRefundLoginConfirmActivity.startActivity(OrderRefundActivity.this, orderModel);
                                            }
                                        });

                                        if(Dialog_Refund != null && !Dialog_Refund.isShowing()){
                                            Dialog_Refund.setMessage(message);
                                            Dialog_Refund.show();
                                        }
                                    }
                                });
                            }

                        }
                    }

                    @Override
                    public void onPreExecute() {
                        super.onPreExecute();
                        loadDialog(OrderRefundActivity.this, "");
                    }

                    @Override
                    public void onPostExecute() {
                        super.onPostExecute();
                        dismissLoading();
                    }

                    @Override
                    public void onSucceed(Boolean result) {
                        super.onSucceed(result);
                        dismissLoading();
                        if(result){
                            //只要请求成功，就直接跳转到确认身份的页面
                            OrderRefundLoginConfirmActivity.startActivity(OrderRefundActivity.this, orderModel);
                        }
                    }
                });
    }
    
    private void initview()
    {
        tv_refunding = getViewById(R.id.tv_refunding);
        btn_next_step = getViewById(R.id.btn_next_step);
        tx_order = getViewById(R.id.tx_order);
        tv_money = getViewById(R.id.tv_money);
        et_money = getViewById(R.id.et_money);
        //要对输入金额做一个最大金额限制
       /* et_money.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() > 0 ){
                    String moneyText = et_money.getText().toString();
                    Pattern p = Pattern.compile("[^0-9.]");//去掉当前字符串中除去0-9以及小数点的其他字符
                    Matcher m = p.matcher(moneyText);
                    moneyText = m.replaceAll("");

                    if(moneyText.equals(".")){//如果输入的是小数点则不走下面的逻辑
                        return;
                    }

                    BigDecimal bigDecimal = new BigDecimal(moneyText).multiply(new BigDecimal(minus));
                    long refound = bigDecimal.longValue();
                    long canRefound = orderModel.getMoney();
                    if (orderModel.getRfMoneyIng() > 0 || orderModel.getRefundMoney() > 0)
                    {//部分退款
                        long refMoney = orderModel.getMoney() - orderModel.getRfMoneyIng() - orderModel.getRefundMoney();
                        canRefound = refMoney;
                    }
                    if(refound > canRefound){
                        et_money.setText(s.subSequence(0,s.length()-1));
                        et_money.setSelection(et_money.getText().length());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });*/
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setTitle(R.string.apply_refund);
        titleBar.setTitleChoice(false);
        titleBar.setLeftButtonVisible(true);
        titleBar.setLeftButtonIsVisible(true);
    }
    
}
