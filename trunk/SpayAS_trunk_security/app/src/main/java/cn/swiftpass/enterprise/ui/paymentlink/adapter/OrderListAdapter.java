package cn.swiftpass.enterprise.ui.paymentlink.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkOrder;
import cn.swiftpass.enterprise.ui.paymentlink.widget.SlideItemView1;
import cn.swiftpass.enterprise.utils.DateUtil;

public class OrderListAdapter extends BaseRecyclerAdapter<PaymentLinkOrder> {

    public OrderListAdapter(Context context, ArrayList<PaymentLinkOrder> data) {
        super(R.layout.item_pl_main_list_item, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder holder, PaymentLinkOrder item, int position) {
        if (item == null) {
            return;
        }

        TextView desc1 = holder.getView(R.id.tv_desc1);
        TextView desc2 = holder.getView(R.id.tv_desc2);
        TextView desc3 = holder.getView(R.id.tv_desc3);
        TextView desc4 = holder.getView(R.id.tv_desc4);
        TextView desc5 = holder.getView(R.id.tv_desc5);

        desc1.setText(item.orderNo);
        if (!TextUtils.isEmpty(item.custName)) {
            desc2.setText(item.custName);
        } else {
            desc2.setText("");
        }
        desc2.setEllipsize(TextUtils.TruncateAt.END);
        desc2.setMaxLines(1);
        desc3.setEllipsize(TextUtils.TruncateAt.END);
        desc3.setMaxLines(1);

        StringBuffer goodsName = new StringBuffer();
        for (int i = 0; i < item.orderRelateGoodsList.size(); i++) {
            if (i == (item.orderRelateGoodsList.size() - 1)) {
                goodsName.append(item.orderRelateGoodsList.get(i).goodsName);
            } else {
                goodsName.append(item.orderRelateGoodsList.get(i).goodsName + ",");
            }
        }
        if (!TextUtils.isEmpty(goodsName.toString())) {
            desc3.setText(goodsName.toString());
        } else {
            desc3.setText("");
        }
        desc4.setText(MainApplication.getInstance().getFeeFh() + " " + DateUtil.formatMoneyUtils(item.totalFee));
        desc5.setText(item.getOrderStatusText(mContext));
        desc5.setTextColor(item.getOrderStatusTextColor(mContext));

        SlideItemView1 view = (SlideItemView1) holder.itemView;
        view.setOnStateChangeListener(new SlideItemView1.OnStateChangeListener() {
            @Override
            public void onFuncShow(SlideItemView1 itemView, boolean isShow) {
            }

            @Override
            public void onFuncClick() {
            }

            @Override
            public void onContentClick() {
                if (getOnItemClickListener() != null) {
                    getOnItemClickListener().onItemClick(OrderListAdapter.this,
                            view, position);
                }
            }
        });
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // 禁止滑动
                return true;
            }
        });
    }
}
