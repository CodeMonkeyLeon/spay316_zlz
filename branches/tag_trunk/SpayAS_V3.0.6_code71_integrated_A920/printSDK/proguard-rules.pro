# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
##################################基本指令区，默认不用修改下面内容##########################################
-ignorewarnings                           #忽略所有警告
-optimizationpasses 5                    #指定代码的压缩级别
-dontusemixedcaseclassnames             #是否使用大小写混合
-dontskipnonpubliclibraryclasses       #是否混淆第三方jar， 指定不去忽略非公共的库的类
-dontskipnonpubliclibraryclassmembers #指定不去忽略非公共的库的类的成员
-dontpreverify                          #不做预校验的操作
-verbose                                 # 混淆时是否记录日志
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*,!code/allocation/variable # 混淆时所采用的算法
-printmapping proguardMapping.txt       #生成原类名和混淆后的类名的映射文件
-keepattributes *Annotation*            #不混淆Annotation
-keepattributes InnerClasses           #不混淆内部类
-keepattributes Signature              #不混淆泛型
-keepattributes Exceptions             #不混淆异常
-keepattributes EnclosingMethod        #不混淆反射
-keepattributes *JavascriptInterface*  #不混淆Android和JS交互
-keepattributes SourceFile,LineNumberTable #抛出异常时保留代码行号
###############################################################################################################

#zxingcore
-dontwarn com.google.zxing.**
-keep class com.google.zxing.** {*;}


#PAXGL
-dontwarn com.pax.gl.**
-keep class com.pax.gl.** {*;}

#PAXNEP
-dontwarn com.pax.**
-keep class com.pax.** {*;}
