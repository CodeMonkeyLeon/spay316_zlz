package cn.swiftpass.enterprise.print;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;

import com.landicorp.android.eptapi.device.Printer;
import com.landicorp.android.eptapi.exception.RequestException;
import com.landicorp.android.eptapi.utils.QrCode;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Locale;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * Created by aijingya on 2019/7/31.
 *
 * @Package cn.swiftpass.enterprise.print
 * @Description: ${TODO}(预授权的打印方法)
 * @date 2019/7/31.15:58.
 */
public abstract class PrinterPreAuth {
    private Context context;

    private StaticLayout mLayout = null;

    private TextPaint mPaint;

    private Canvas mCanvas;

    private String content = null;

    private Printer.Progress progress = new Printer.Progress() {

        @Override
        public void doPrint(Printer arg0) throws Exception {

        }

        @Override
        public void onFinish(int code) {
            if (code == Printer.ERROR_NONE) {
                displayPrinterInfo("PRINT SUCCESS END ");
            }
            /**
             * Has some error. Here is display it, but you may want to hanle the
             * error such as ERROR_OVERHEAT、ERROR_BUSY、ERROR_PAPERENDED to start
             * again in the right time later.
             */
            else {
                displayPrinterInfo("PRINT ERR - " + (code));
            }
        }

        @Override
        public void onCrash() {
            onDeviceServiceCrash();
        }

    };

    /**
     * Search card and show all track info
     */
    public void startPrint() {
        try {
            progress.start();
            // DeviceService.logout();
        } catch (RequestException e) {
            onDeviceServiceCrash();
        }
    }

    protected abstract void displayPrinterInfo(String info);

    protected abstract void onDeviceServiceCrash();



    /**
     * printType参数1：预授权成功---预授权小票
     * printType参数4：预授权订单详情---预授权小票
     * printType参数2：预授权解冻详情小票
     * printType参数3：预授权解冻完成小票
     * printType参数5：预授权成功---预授权小票---预授权反扫完下单一次成功
     */
    public PrinterPreAuth(Context context, final int printType, final Order orderModel)
    {
        this.context = context;

        progress.addStep(new Printer.Step()
        {

            @Override
            public void doPrint(Printer printer)
                    throws Exception
            {
              /*  printer.setAutoTrunc(true);

                // Default mode is real mode, now set it to virtual mode.
                printer.setMode(Printer.MODE_REAL);*/
                printer.setAutoTrunc(false);
                printer.setPageSpace(1500);

                /**
                 *  打印图片
                 */
            /*    if (null != bitmapLogo){
                    byte[] bytes = Bitmap2Bytes(bitmapLogo);
                    InputStream bitmapStream = BytesToInStream(bytes);
                    printer.printImage(0,300,40,bytes);
//                    printer.printImage(Alignment.CENTER,bitmapStream);
                }*/

                Printer.Format format = new Printer.Format();
                format.setAscSize(Printer.Format.ASC_DOT24x12);
                format.setAscScale(Printer.Format.ASC_SC1x2);
                format.setHzSize(Printer.Format.HZ_DOT24x24);
                format.setHzScale(Printer.Format.HZ_SC1x2);
                printer.setFormat(format);


                switch (printType){
                    case 1:
                    case 4:
                    case 5:
                        printer.printText(Printer.Alignment.CENTER,ToastHelper.toStr(R.string.pre_auth_receipt) + "\n");//预授权小票
                        break;

                    case 2:
                    case 3:
                        printer.printText(Printer.Alignment.CENTER,ToastHelper.toStr(R.string.pre_auth_unfreezed_receipt) + "\n");//预授权解冻小票
                        break;
                }

                format.setAscSize(Printer.Format.ASC_DOT24x12);
                format.setAscScale(Printer.Format.ASC_SC1x1);
                format.setHzSize(Printer.Format.HZ_DOT24x24);
                format.setHzScale(Printer.Format.HZ_SC1x1);
                printer.setFormat(format);

                printer.printText("\n" + orderModel.getPartner() +"\n" + ToastHelper.toStr(R.string.tv_pay_client_save));
                printer.printText("\n===============================\n");
                printer.printText(ToastHelper.toStr(R.string.shop_name) + "："+"\n"+ MainApplication.getMchName() + "\n");
                printer.printText(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "：" + MainApplication.getMchId() + "\n");

                if (printType==1) {
                    printer.printText(ToastHelper.toStr(R.string.pre_auth_time) + "：\n");
                    printer.printText(orderModel.getTradeTime() + "\n");
                }else if(printType==4){
                    printer.printText(ToastHelper.toStr(R.string.pre_auth_time) + "：\n");
                    printer.printText(orderModel.getTradeTimeNew() + "\n");
                }else if(printType==5){
                    printer.printText(ToastHelper.toStr(R.string.pre_auth_time) + "：\n");
                    printer.printText(orderModel.getTimeEnd() + "\n");
                }
                else if(printType==2){
                    printer.printText(ToastHelper.toStr(R.string.unfreezed_time) + "：\n");
                    printer.printText(orderModel.getOperateTime() + "\n");
                }else if(printType==3){
                    printer.printText(ToastHelper.toStr(R.string.unfreezed_time) + "：\n");
                    printer.printText(orderModel.getUnFreezeTime() + "\n");
                }
                if (printType==1 || printType == 4 || printType == 5){
                    printer.printText(ToastHelper.toStr(R.string.platform_pre_auth_order_id) + "：\n");//平台预授权订单号
                    printer.printText(orderModel.getAuthNo() + "\n");

                    String language = PreferenceUtil.getString("language","");
                    Locale locale = MainApplication.getContext().getResources().getConfiguration().locale; //zh-rHK
                    String lan = locale.getCountry();
                    if (!TextUtils.isEmpty(language)) {
                        if (language.equals(MainApplication.LANG_CODE_EN_US)) {
                            //支付宝单号
                            printer.printText(MainApplication.getPayTypeMap().get("2") + " "+ ToastHelper.toStr(R.string.tx_orderno) + "：\n");
                        } else {
                            //支付宝单号
                            printer.printText(MainApplication.getPayTypeMap().get("2") + ToastHelper.toStr(R.string.tx_orderno) + "：\n");
                        }
                    } else {
                        if (lan.equalsIgnoreCase("en")) {
                            //支付宝单号
                            printer.printText(MainApplication.getPayTypeMap().get("2") + " "+ ToastHelper.toStr(R.string.tx_orderno) + "：\n");
                        } else {
                            //支付宝单号
                            printer.printText(MainApplication.getPayTypeMap().get("2") + ToastHelper.toStr(R.string.tx_orderno) + "：\n");
                        }
                    }

                    if(printType==1 || printType == 5){
                        printer.printText(orderModel.getOutTransactionId() + "\n");
                    }

                    if(printType== 4){
                        printer.printText(orderModel.getTransactionId() + "\n");
                    }

                }else if(printType == 2 || printType== 3){
                    printer.printText(ToastHelper.toStr(R.string.unfreezed_order_id) + "：\n");//解冻订单号
                    printer.printText(orderModel.getOutRequestNo() + "\n");
                    printer.printText(ToastHelper.toStr(R.string.platform_pre_auth_order_id) + "：\n");//平台预授权订单号
                    printer.printText(orderModel.getAuthNo() + "\n");
                }
                if(printType == 5){
                    String tradename;
                    if (!isAbsoluteNullStr(orderModel.getTradeName())){//先判断大写的Name，再判断小写name
                        tradename=orderModel.getTradeName();
                    }else {
                        tradename=orderModel.getTradename();
                    }

                    printer.printText(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：" + "\n"+ tradename + "\n");//支付方式
                }else{
                    printer.printText(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：" + "\n"+ orderModel.getTradeName() + "\n");//支付方式
                }


                if (printType == 3) {
                    printer.printText(ToastHelper.toStr(R.string.tv_unfreezen_applicant) + "：\n");//办理人
                    printer.printText(orderModel.getUserName() + "\n");
                }

                printer.printText(ToastHelper.toStr(R.string.tx_bill_stream_statr) + "：\n");//订单状态
                String operationType = "";
                if(printType == 1 || printType == 4){
                    operationType = MainApplication.getPreAuthtradeStateMap().get(orderModel.getTradeState() + "");

                }else if(printType == 2){
                    switch (orderModel.getOperationType()){
                        case 2:
                            operationType = ToastHelper.toStr(R.string.freezen_success);
                            break;
                    }
                }else if(printType == 3){
                    operationType = ToastHelper.toStr(R.string.unfreezing);
                }else if (printType == 5){
                    operationType = ToastHelper.toStr(R.string.pre_auth_authorized);
                }

                printer.printText(operationType+  "\n");

                switch (printType) {
                    case 1:
                    case 4:
                    case 5:
                        if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
                            printer.printText(Printer.Alignment.LEFT,ToastHelper.toStr(R.string.pre_auth_amount) + ":"+ "\n");
                            printer.printText(Printer.Alignment.RIGHT, DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan)+ "\n");
                        } else {
                            long preAuthorizationMoney = 0;//用来接收printType判断授权金额来自反扫、正扫
                            if (printType == 1) {
                                preAuthorizationMoney = orderModel.getRestAmount();//反扫
                            } else if (printType == 5) {
                                preAuthorizationMoney = orderModel.getTotalFee();//正扫
                            } else if (printType == 4) {
                                preAuthorizationMoney = orderModel.getTotalFreezeAmount();//预授权详情
                            }
                            //预授权金额
                            printer.printText(Printer.Alignment.LEFT,ToastHelper.toStr(R.string.pre_auth_amount)+":"+ "\n");
                            printer.printText(Printer.Alignment.RIGHT,  MainApplication.feeType +" "+ DateUtil.formatMoneyUtils(preAuthorizationMoney)+ "\n");

                        }

                        printer.printText(" "+"\n\n");

                        //二维码
                        printer.printQrCode(Printer.Alignment.CENTER,
                                new QrCode(orderModel.getAuthNo(), QrCode.ECLEVEL_Q),
                                184);
                        //此二维码用于预授权收款、解冻操作
                        printer.printText(Printer.Alignment.CENTER,"\n"+ ToastHelper.toStr(R.string.scan_to_check)+ "\n");

                        printer.printText("-------------------------------\r\n");
                        printer.printText(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\r\n");
//                        printer.printText("\n"+ ToastHelper.toStr(R.string.tx_blue_print_sign) + "：\r\n");
                        printer.printText(" "+"\n\n");
                        break;
                    case 2:
                        if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
                            printer.printText(Printer.Alignment.LEFT,ToastHelper.toStr(R.string.unfreezing_amount) + ":"+ "\n");
                            printer.printText(Printer.Alignment.RIGHT,  DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan) + "\n");
                        } else {
                            printer.printText(Printer.Alignment.LEFT,ToastHelper.toStr(R.string.unfreezing_amount) + ":"+ "\n");
                            printer.printText(Printer.Alignment.RIGHT,  MainApplication.feeType +" "+ DateUtil.formatMoneyUtils(orderModel.getMoney()) + "\n");

                        }
                        //解冻将原路返回支付账户或银行卡，到账时间已第三方为准
                        printer.printText(ToastHelper.toStr(R.string.unfreeze)+  "\n");
                        printer.printText(" "+"\n\n");

                        printer.printText("-------------------------------\r\n");
                        printer.printText(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\r\n");
//                        printer.printText("\n"+ ToastHelper.toStr(R.string.tx_blue_print_sign) + "：\r\n");
                        printer.printText(" "+"\n\n");
                        break;
                    case 3:
                        //授权金额
                        if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
                            printer.printText(Printer.Alignment.LEFT,ToastHelper.toStr(R.string.pre_auth_amount) + ":"+ "\n");
                            printer.printText(Printer.Alignment.RIGHT,   DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan) + "\n");

                        } else {
                            printer.printText(Printer.Alignment.LEFT,ToastHelper.toStr(R.string.pre_auth_amount) + ":"+ "\n");
                            printer.printText(Printer.Alignment.RIGHT,   MainApplication.feeType+" " + DateUtil.formatMoneyUtils(orderModel.getTotalFreezeAmount()) + "\n");
                        }
                        //解冻金额
                        if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {

                            printer.printText(Printer.Alignment.LEFT,ToastHelper.toStr(R.string.unfreezing_amount) + ":"+ "\n");
                            printer.printText(Printer.Alignment.RIGHT,   DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan) + "\n");

                        } else {
                            printer.printText(Printer.Alignment.LEFT,ToastHelper.toStr(R.string.unfreezing_amount) + ":"+ "\n");
                            printer.printText(Printer.Alignment.RIGHT,   MainApplication.feeType +" "+ DateUtil.formatMoneyUtils(orderModel.getTotalFee()) + "\n");
                        }
                        //解冻将原路返回支付账户或银行卡，到账时间已第三方为准
                        printer.printText(ToastHelper.toStr(R.string.unfreeze)+  "\n");

                        printer.printText("-------------------------------\r\n");
                        printer.printText(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\r\n");
//                        printer.printText("\n"+ ToastHelper.toStr(R.string.tx_blue_print_sign) + "：\r\n");
                        printer.printText(" "+"\n\n");
                        break;
                    default:
                        break;
                }
                printer.printText("\r\n\n");
                printer.printText("\r\n\n");

            }
        });
    }


    /************************新增获取图片方法*******************************/
    public  byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos =new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);

        return baos.toByteArray();
    }

    public InputStream BytesToInStream(byte[] bytes)
    {
        InputStream is = new ByteArrayInputStream(bytes);
        return is;
    }


    public static boolean isAbsoluteNullStr(String str) {
        str = deleteBlank(str);
        return str == null || str.length() == 0 || "null".equalsIgnoreCase(str);

    }
    /**
     * 去前后空格和去换行符
     *
     * @param str
     * @return
     */
    public static   String deleteBlank(String str) {
        if (null != str && 0 < str.trim().length()) {
            char[] array = str.toCharArray();
            int start = 0, end = array.length - 1;
            while (array[start] == ' ') start++;
            while (array[end] == ' ') end--;
            return str.substring(start, end + 1).replaceAll("\n", "");

        } else {
            return "";
        }

    }

}
