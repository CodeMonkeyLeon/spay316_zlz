package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.bussiness.model.SettlementList
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.SettlementListContract
import cn.swiftpass.enterprise.utils.JsonUtil
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/2
 *
 * @走得突然
 * @我们来不及告别
 * @这样也好
 * @因为我们永远不告别
 */
class SettlementListPresenter : SettlementListContract.Presenter {


    private var mView: SettlementListContract.View? = null


    override fun queryDailySettlementList(startDate: String?, endDate: String?) {
        mView?.let { view ->

            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)

            AppClient.queryDailySettlementList(
                startDate,
                endDate,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<SettlementList>(SettlementList()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.queryDailySettlementListFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val bean = JsonUtil
                                .jsonToBean(
                                    response.message,
                                    SettlementList::class.java
                                ) as SettlementList
                            view.queryDailySettlementListSuccess(bean.data)
                        }

                    }
                }
            )
        }
    }

    override fun attachView(view: SettlementListContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}