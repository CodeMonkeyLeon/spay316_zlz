package cn.swiftpass.enterprise.ui.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.MaxCardManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DisplayUtil;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.ImageUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 固定二维码收款
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2015-10-14]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class StaticCodeActivity extends TemplateActivity {
    private static final String TAG = StaticCodeActivity.class.getSimpleName();
    // 固定二维码地址
    private String codeStrM = ApiConstant.BASE_URL_PORT + "spay/payMoneyNew?mchId=" + MainApplication.merchantId + "&userId=" + MainApplication.userId;


    private ImageView img;

    // 是否交易完成
    //private boolean isbussFinsh;

    private LinearLayout tv_content, lay_scan;

    //    private DialogInfoSdk dialogInfo;

    private TimeCount time;

    //private Handler mHandler;

    private String outTradeNo;

    private Button scan_confirm;

    private TextView tx_company, tv_scan_but, tv_scan, tx_money, tx_reMark;

    //private DialogInfo dialogInfos;

    private Button confirm;

    Bitmap bitmap;

    private Bitmap firstBitmap;

    private GridView gv_pay_type;

    private ViewPayTypeHolder payholder;

    private PayTypeAdape payTypeAdape;
    private LinearLayout ll_pay_way;
    private LinearLayout ly_lst_iocn;

    private LinearLayout ll_separate_code;

    private LinearLayout ll_wechat_pay;
    private ImageView iv_icon_paytype_wechat;
    private TextView tv_wechat_name;
    private ImageView iv_cover_wechat;

    private LinearLayout ll_Alipay_paytype;
    private ImageView iv_icon_paytype_alipay;
    private TextView tv_alipay_name;
    private ImageView iv_cover_alipay;

    private String codeShowMoney = null;

    private Handler handler = new Handler()

    {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == HandlerManager.SETCODEMONEY) {
                if (msg.obj != null) {
                    titleBar.setRightButLayVisibleForTotal(true, getString(R.string.tv_code_clean_moeny));

                    String coneten = (String) msg.obj;
                    if (coneten.contains("|")) {
                        tx_reMark.setVisibility(View.VISIBLE);
                        tx_money.setVisibility(View.VISIBLE);
                        String str[] = coneten.split("\\|");

                        double money = Double.parseDouble(str[0]);
                        //下面的代码是为了做到根据输入金额的长度修改字体的大小-----by aijingya on 2018/02/23
                        int length = str[0].length();
                        if(length >= 8){
                            tx_money.setTextSize(15);
                        }else{
                            tx_money.setTextSize(22);
                        }
                        tx_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtil(money));
                        tx_reMark.setText(str[1]);
                        createCode(str[0]);
                        codeShowMoney = str[0];
                    } else {
                        tx_money.setVisibility(View.VISIBLE);
                        double money = Double.parseDouble(coneten);
                        //下面的代码是为了做到根据输入金额的长度修改字体的大小-----by aijingya on 2018/02/23
                        int length = coneten.length();
                        if(length >= 8){
                            tx_money.setTextSize(15);
                        }else{
                            tx_money.setTextSize(22);
                        }
                        tx_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtil(money));
                        createCode(coneten);
                        codeShowMoney = coneten;
                    }

                }
            } else if (msg.what == HandlerManager.PAY_ACTIVE_SCAN) { //扫码激活
                scan_confirm.setVisibility(View.GONE);
            }
        }

        ;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_qrcode);

        initView();

        HandlerManager.registerHandler(HandlerManager.SETCODEMONEY, handler);

        HandlerManager.registerHandler(HandlerManager.PAY_ACTIVE_SCAN, handler);
        //        mHandler = new Handler();
        //        initDate();

        confirm = getViewById(R.id.confirm);
        setButBgAndFont(confirm);
        setButBgAndFont(scan_confirm);
    }

//    /**
//     * 支付2分钟倒计时
//     * <功能详细描述>
//     *
//     * @see [类、类#方法、类#成员]
//     */
//    private void Countdown() {
//        // 总共2分钟 间隔2秒
//        time = new TimeCount(1000 * 60 * 3, 2000);
//        time.start();
//    }

    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }

        @Override
        public void onFinish() {//计时完毕时触发

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //                    String mmsg = "支付请求已超时，请重新生成二维码";
                    //                    dialogInfo =
                    //                        new DialogInfoSdk(StaticCodeActivity.this,
                    //                            getResources().getString(Resourcemap.getById_title_prompt()), "" + mmsg, "确定",
                    //                            DialogInfoSdk.SUBMIT_FINISH, null);
                    //                    
                    //                    DialogHelper.resize(StaticCodeActivity.this, dialogInfo);
                    //                    dialogInfo.setOnKeyListener(new OnKeyListener()
                    //                    {
                    //                        
                    //                        @Override
                    //                        public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2)
                    //                        {
                    //                            
                    //                            if (keycode == KeyEvent.KEYCODE_BACK)
                    //                            {
                    //                                return true;
                    //                            }
                    //                            return false;
                    //                        }
                    //                    });
                    //                    dialogInfo.show();
                    //                    mHandler.post(myRunnable);
                }
            });
            //            time.cancel();
        }

        @Override
        public void onTick(long millisUntilFinished) {//计时过程显示
            //            if (!isMove)
            //            {
            if (null != outTradeNo && !"".equals(outTradeNo)) {
                queryOrderGetStuts(outTradeNo);
            }
            //            }
        }
    }

    boolean isMark = false;

    /**
     * 查询订单获取状态
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void queryOrderGetStuts(final String orderNo) {
        OrderManager.getInstance().queryOrderByOrderNo(orderNo, MainApplication.PAY_ZFB_QUERY, new UINotifyListener<Order>() {
            @Override
            public void onError(Object object) {
                super.onError(object);
            }

            @Override
            public void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(Order result) {
                super.onSucceed(result);
                if (result != null && isMark) {
                    if (result.state.equals("2")) {
                        isMark = true;
                        if (time != null) {
                            time.cancel();
                            time = null;
                        }
                        PayResultActivity.startActivity(StaticCodeActivity.this, result);
                        finish();

                    }
                }
            }
        });
    }

    /**
     * 从服务器取图片
     * http://bbs.3gstdy.com
     *
     * @param url
     * @return
     */
   /* private Bitmap getHttpBitmap(String url) {
        URL myFileUrl = null;
        Bitmap bitmap = null;
        try {
            myFileUrl = new URL(url);
        } catch (MalformedURLException e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }
        try {
            HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
            conn.setConnectTimeout(0);
            conn.setDoInput(true);
            conn.connect();
            InputStream is = conn.getInputStream();
            bitmap = BitmapFactory.decodeStream(is);
            is.close();
        } catch (IOException e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }
        return bitmap;
    }
*/

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        if (time != null) {
            time.cancel();
        }
    }

    //private long timeCount = 5;

    //    private Runnable myRunnable = new Runnable()
    //    {
    //        
    //        @Override
    //        public void run()
    //        {
    //            if (timeCount > 0 && dialogInfo != null)
    //            {
    //                dialogInfo.setBtnOkText("确定(" + timeCount + "秒后关闭)");
    //                timeCount -= 1;
    //                mHandler.postDelayed(this, 1000);
    //            }
    //            else
    //            {
    //                mHandler.removeCallbacks(this);
    //                if (dialogInfo != null && dialogInfo.isShowing())
    //                {
    //                    
    //                    dialogInfo.dismiss();
    //                }
    //                dialogInfo.setBtnOkText("确定(关闭)");
    //                StaticCodeActivity.this.finish();
    //            }
    //            
    //        }
    //    };

    String sign = null;

    boolean isTag = false;//默认是收款二维码

    private void createCode(String money) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("mchId", MainApplication.merchantId);
        params.put("userId", MainApplication.userId + "");
        params.put("client", MainApplication.CLIENT);
        if (null == MainApplication.body || "".equals(MainApplication.body)) {
            MainApplication.body = ToastHelper.toStr(R.string.tx_mobile_pay);
        }
        params.put("body", MainApplication.body);
        long mon = 0;
        if (money.contains(".")) {
            double m = Double.parseDouble(money);
            mon = Long.parseLong(DateUtil.formatMoneyInt(m));
            params.put("fixedMoney", String.valueOf(mon));
        } else {
            long m = Long.parseLong(money);
            mon = (m * 100);
            params.put("fixedMoney", String.valueOf(mon));
        }
        //        codeStrM = codeStrM + "&fixedMoney=" + (m * 100);

        sign = SignUtil.getInstance().createSign(params, MainApplication.getSignKey());
        try {

            //String language = PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN);

            //Locale locale = getResources().getConfiguration().locale; //zh-rHK
            //String lan = locale.getCountry();

//            String fp_lan;
//            if (!TextUtils.isEmpty(language)) {
//                if (language.equals(MainApplication.LANG_CODE_ZH_TW)) {
//                    fp_lan = MainApplication.LANG_CODE_ZH_TW;
//                } else {
//                    fp_lan = MainApplication.LANG_CODE_ZH_CN;
//                }
//            } else {
//                if (lan.equalsIgnoreCase("CN")) {
//                    fp_lan = MainApplication.LANG_CODE_ZH_CN;
//                } else {
//                    fp_lan = MainApplication.LANG_CODE_ZH_TW;
//                }
//            }
//            String codeMsg = codeStrM + "&client=" + MainApplication.CLIENT + "&body=" + "&fixedMoney=" + mon;
            String codeMsg = creatCodeMsg(true,mon); //根据聚合非聚合的情况生成不同的固定二维码的string

            //            String codeMsg =
            //                codeStrM + "&sign=" + sign + "&client=" + MainApplication.CLIENT + "&body="
            //                    + URLEncoder.encode(MainApplication.body, "utf-8") + "&fp-lang=" + fp_lan;
//            DynModel dynModel = (DynModel) SharedPreUtile.readProduct("dynModel" + ApiConstant.bankCode);
//            if (dynModel != null && !StringUtil.isEmptyOrNull(dynModel.getAndroidLogo())) {
//                Bitmap b = ImageUtil.decodeFile2(AppHelper.getImgCacheDir() + dynModel.getThemeMd5() + ".jpg");
//                if (b != null) {
//                    //                    bitmap = MaxCardManager.getInstance().create2DCode(StaticCodeActivity.this, codeMsg, 430, 430, b);
//
//                    bitmap = MaxCardManager.getInstance().create2DCode(codeMsg, DisplayUtil.dip2Px(StaticCodeActivity.this, 280), DisplayUtil.dip2Px(StaticCodeActivity.this, 280));
//                } else {
//                    //                    bitmap =
//                    //                        com.switfpass.pay.qrcode.MaxCardManager.getInstance().create2DCode(StaticCodeActivity.this,
//                    //                            codeMsg,
//                    //                            DisplayUtil.dip2Px(StaticCodeActivity.this, 280),
//                    //                            DisplayUtil.dip2Px(StaticCodeActivity.this, 280),
//                    //                            R.drawable.icon_app_logo);
//
//                    bitmap = MaxCardManager.getInstance().create2DCode(codeMsg, DisplayUtil.dip2Px(StaticCodeActivity.this, 280), DisplayUtil.dip2Px(StaticCodeActivity.this, 280));
//                }
//            } else {

                //                bitmap =
                //                    com.switfpass.pay.qrcode.MaxCardManager.getInstance().create2DCode(StaticCodeActivity.this,
                //                        codeMsg,
                //                        DisplayUtil.dip2Px(StaticCodeActivity.this, 280),
                //                        DisplayUtil.dip2Px(StaticCodeActivity.this, 280),
                //                        R.drawable.icon_app_logo);

                bitmap = MaxCardManager.getInstance().create2DCode(codeMsg, DisplayUtil.dip2Px(StaticCodeActivity.this, 280), DisplayUtil.dip2Px(StaticCodeActivity.this, 280));
//            }

            img.setImageBitmap(bitmap);
        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }

    }

    public void updatePayViewByFixCode(Object dynPayTypeStatic){
        if(MainApplication.isFixCode == 0){//为聚合码
            gv_pay_type.setVisibility(View.VISIBLE);
            ll_pay_way.setVisibility(View.VISIBLE);
            ll_separate_code.setVisibility(View.GONE);
        }else if(MainApplication.isFixCode == 1){//为非聚合码
            if(containsAlipay() && containsWeixinPay()){   //两种支付方式都有的时候默认选中微信的支付方式
                gv_pay_type.setVisibility(View.GONE);
                ll_pay_way.setVisibility(View.GONE);
                ll_separate_code.setVisibility(View.VISIBLE);
                //遮罩支付宝，去掉微信的遮罩
                iv_cover_wechat.setVisibility(View.GONE);
                iv_cover_alipay.setVisibility(View.VISIBLE);

                if (dynPayTypeStatic != null) {
                    List<DynModel> list = (List<DynModel>) dynPayTypeStatic;
                    for(int i = 0 ; i < list.size() ; i++){
                        if(list.get(i).getApiCode().equals("1")){ //weixin
                            if (!StringUtil.isEmptyOrNull(list.get(i).getSmallIconUrl())) {
                                if (MainApplication.finalBitmap != null){
                                    MainApplication.finalBitmap.display(iv_icon_paytype_wechat, list.get(i).getSmallIconUrl());
                                }
                            } else {
                                iv_icon_paytype_wechat.setImageResource(R.drawable.icon_general_receivables);
                            }
                            tv_wechat_name.setText(MainApplication.getPayTypeMap().get(String.valueOf(list.get(i).getApiCode())));

                        }else if(list.get(i).getApiCode().equals("2")){ //Alipay
                            if (!StringUtil.isEmptyOrNull(list.get(i).getSmallIconUrl())) {
                                if (MainApplication.finalBitmap != null){
                                    MainApplication.finalBitmap.display(iv_icon_paytype_alipay, list.get(i).getSmallIconUrl());
                                }
                            } else {
                                iv_icon_paytype_alipay.setImageResource(R.drawable.icon_general_receivables);
                            }
                            tv_alipay_name.setText(MainApplication.getPayTypeMap().get(String.valueOf(list.get(i).getApiCode())));
                        }
                    }
                }
            }else if(containsAlipay() || containsWeixinPay()) {//如果当前只有两种中间的一种支付方式
                //如果只有一种支付方式的时候显示成聚合码的形式
                gv_pay_type.setVisibility(View.VISIBLE);
                ll_pay_way.setVisibility(View.VISIBLE);
                ll_separate_code.setVisibility(View.GONE);

            }
        }
    }

    public String creatCodeMsg(boolean hasMoney,long money){
        String codeString = null;
        if(MainApplication.isFixCode == 0){ //为聚合码
            //是刚进来固定二维码页面，还是输入金额后回来固定二维码页面
            if(hasMoney){//输入过金额
                codeString = codeStrM + "&client=" + MainApplication.CLIENT + "&body=" + "&fixedMoney=" + money;
            }else {//没输入金额的固码页面
                codeString = codeStrM + "&client=" + MainApplication.CLIENT ;
            }
        }else if(MainApplication.isFixCode == 1){ //为非聚合码
            //先判断支付方式，是否是微信和支付宝两种支付方式都有，还是只有其中一种
            if(containsAlipay() && containsWeixinPay()){
                //两种支付方式都有的时候默认选中微信的支付方式
                //同时要判断当前的选中状态是那种支付方式，因为有可能选中了一种方式之后又去设置金额
                if(iv_cover_wechat.getVisibility() == View.VISIBLE){
                    if(hasMoney){//输入过金额
                        codeString = codeStrM + "&client=" + MainApplication.CLIENT + "&body=" + "&fixedMoney=" + money+"&accOrg=2";
                    }else {//没输入金额的固码页面
                        codeString = codeStrM + "&client=" + MainApplication.CLIENT +"&accOrg=2";
                    }
                }else if(iv_cover_alipay.getVisibility() == View.VISIBLE){
                    if(hasMoney){//输入过金额
                        codeString = codeStrM + "&client=" + MainApplication.CLIENT + "&body=" + "&fixedMoney=" + money;
                    }else {//没输入金额的固码页面
                        codeString = codeStrM + "&client=" + MainApplication.CLIENT ;
                    }
                }

            }else if(containsAlipay() || containsWeixinPay()){//如果当前只有两种中间的一种支付方式
                if(containsWeixinPay()){ //如果是微信
                    if(hasMoney){//输入过金额
                        codeString = codeStrM + "&client=" + MainApplication.CLIENT + "&body=" + "&fixedMoney=" + money;
                    }else {//没输入金额的固码页面
                        codeString = codeStrM + "&client=" + MainApplication.CLIENT ;
                    }
                }else if(containsAlipay()){//如果当前的支付方式是支付宝
                    if(hasMoney){//输入过金额
                        codeString = codeStrM + "&client=" + MainApplication.CLIENT + "&body=" + "&fixedMoney=" + money+"&accOrg=2";
                    }else {//没输入金额的固码页面
                        codeString = codeStrM + "&client=" + MainApplication.CLIENT +"&accOrg=2";
                    }
                }
            }
        }
        return codeString;
    }

    //判断是否包含支付宝支付
    public boolean containsAlipay(){
        Object dynPayTypeStatic = SharedPreUtile.readProduct("dynPayTypeStatic" + ApiConstant.bankCode + MainApplication.merchantId);
        if (dynPayTypeStatic != null) {
            List<DynModel> list = (List<DynModel>) dynPayTypeStatic;
            for(int i = 0 ; i < list.size() ; i++){
                if(list.get(i).getApiCode().equals("2")){
                    return true;
                }
            }
        }
        return false;
    }

    //判断是否包含微信支付
    public boolean containsWeixinPay(){
        Object dynPayTypeStatic = SharedPreUtile.readProduct("dynPayTypeStatic" + ApiConstant.bankCode + MainApplication.merchantId);
        if (dynPayTypeStatic != null) {
            List<DynModel> list = (List<DynModel>) dynPayTypeStatic;
            for(int i = 0 ; i < list.size() ; i++){
                if(list.get(i).getApiCode().equals("1")){
                    return true;
                }
            }
        }
        return false;
    }


    public void setListViewHeightBasedOnChildren(GridView listView) {
        // 获取listview的adapter  
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        // 固定列宽，有多少列  
        int col = 5;// listView.getNumColumns();  
        int totalHeight = 0;
        // i每次加4，相当于listAdapter.getCount()小于等于4时 循环一次，计算一次item的高度，  
        // listAdapter.getCount()小于等于8时计算两次高度相加  
        for (int i = 0; i < listAdapter.getCount(); i += col) {
            // 获取listview的每一个item  
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            // 获取item的高度和  
            totalHeight += listItem.getMeasuredHeight();
        }

        // 获取listview的布局参数  
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        // 设置高度  
        params.height = totalHeight + DisplayUtil.dip2Px(StaticCodeActivity.this, 10);
        // 设置margin  
        ((MarginLayoutParams) params).setMargins(10, 10, 10, 10);
        // 设置参数  
        listView.setLayoutParams(params);
    }

    class PayTypeAdape extends BaseAdapter {

        private List<DynModel> list;

        private Context context;

        private PayTypeAdape(Context context, List<DynModel> list) {
            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = View.inflate(context, R.layout.activity_static_paytype_list_item, null);
                //                AbsListView.LayoutParams params = new AbsListView.LayoutParams(viewWidth, viewHeight);
                //                convertView.setLayoutParams(params);
                payholder = new ViewPayTypeHolder();
                payholder.iv_icon = (ImageView) convertView.findViewById(R.id.iv_icon);

                convertView.setTag(payholder);
            } else {
                payholder = (ViewPayTypeHolder) convertView.getTag();
            }

            DynModel dynModel = list.get(position);
            if (dynModel != null && !StringUtil.isEmptyOrNull(dynModel.getSmallIconUrl())) {
                if (MainApplication.finalBitmap != null){
                    MainApplication.finalBitmap.display(payholder.iv_icon, dynModel.getSmallIconUrl());
                }
            } else {
                payholder.iv_icon.setImageResource(R.drawable.icon_scanqrcode_general_wechat);
            }

            return convertView;
        }
    }

    class ViewPayTypeHolder {
        ImageView iv_icon;
    }

    private void initView() {
        gv_pay_type = getViewById(R.id.gv_pay_type);
        ll_pay_way = getViewById(R.id.ll_pay_way);
        ll_separate_code = getViewById(R.id.ll_separate_code);

        ll_wechat_pay = getViewById(R.id.ll_wechat_pay);
        iv_icon_paytype_wechat = getViewById(R.id.iv_icon_paytype_wechat);
        tv_wechat_name = getViewById(R.id.tv_wechat_name);
        iv_cover_wechat = getViewById(R.id.iv_cover_wechat);

        ll_wechat_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(iv_cover_wechat.getVisibility() == View.VISIBLE){//如果当前是未选择微信支付，点击才有反应
                    iv_cover_wechat.setVisibility(View.GONE);
                    iv_cover_alipay.setVisibility(View.VISIBLE);

                    String codeMsg =  null;
                    long mon = 0;
                    if(codeShowMoney != null){
                        if (codeShowMoney.contains(".")) {
                            double m = Double.parseDouble(codeShowMoney);
                            mon = Long.parseLong(DateUtil.formatMoneyInt(m));
                        } else {
                            long m = Long.parseLong(codeShowMoney);
                            mon = (m * 100);
                        }
                    }

                    if(mon == 0){
                        codeMsg = creatCodeMsg(false,0);
                    }else{

                        codeMsg = creatCodeMsg(true,mon); //根据聚合非聚合的情况生成不同的固定二维码的string
                    }
                    try {
                        bitmap = MaxCardManager.getInstance().create2DCode(codeMsg, DisplayUtil.dip2Px(StaticCodeActivity.this, 280), DisplayUtil.dip2Px(StaticCodeActivity.this, 280));
                        img.setImageBitmap(bitmap);
                    }catch (Exception e){
                        Log.e(TAG,Log.getStackTraceString(e));
                    }
                }
            }
        });

        ll_Alipay_paytype = getViewById(R.id.ll_Alipay_paytype);
        iv_icon_paytype_alipay = getViewById(R.id.iv_icon_paytype_alipay);
        tv_alipay_name = getViewById(R.id.tv_alipay_name);
        iv_cover_alipay = getViewById(R.id.iv_cover_alipay);

        ll_Alipay_paytype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(iv_cover_alipay.getVisibility() == View.VISIBLE){//如果当前是未选择支付宝的状态，点击才有反应
                    iv_cover_alipay.setVisibility(View.GONE);
                    iv_cover_wechat.setVisibility(View.VISIBLE);

                    String codeMsg =  null;
                    long mon = 0;
                    if(codeShowMoney != null){
                        if (codeShowMoney.contains(".")) {
                            double m = Double.parseDouble(codeShowMoney);
                            mon = Long.parseLong(DateUtil.formatMoneyInt(m));
                        } else {
                            long m = Long.parseLong(codeShowMoney);
                            mon = (m * 100);
                        }
                    }

                    if(mon == 0){
                        codeMsg = creatCodeMsg(false,0);
                    }else{
                        codeMsg = creatCodeMsg(true,mon); //根据聚合非聚合的情况生成不同的固定二维码的string
                    }
                    try {
                        bitmap = MaxCardManager.getInstance().create2DCode(codeMsg, DisplayUtil.dip2Px(StaticCodeActivity.this, 280), DisplayUtil.dip2Px(StaticCodeActivity.this, 280));
                        img.setImageBitmap(bitmap);
                    }catch (Exception e){
                        Log.e(TAG,Log.getStackTraceString(e));
                    }

                }
            }
        });

        ly_lst_iocn = getViewById(R.id.ly_lst_iocn);
        Object dynPayTypeStatic = SharedPreUtile.readProduct("dynPayTypeStatic" + ApiConstant.bankCode + MainApplication.merchantId);
        if (dynPayTypeStatic != null) {
            ly_lst_iocn.setVisibility(View.GONE);
            List<DynModel> list = (List<DynModel>) dynPayTypeStatic;
            //为了要做居中显示的功能现在做特殊处理  2018/01/20 ---- aijingya
            if(list.size() <= 5){
                gv_pay_type.setVisibility(View.GONE);
                ll_pay_way.setVisibility(View.VISIBLE);
                ll_pay_way.removeAllViews();
                for(int i = 0 ;i < list.size() ; i++){
                    View ItemView = View.inflate(getApplicationContext(), R.layout.activity_static_paytype_list_item, null);
                    ImageView iv_icon = (ImageView) ItemView.findViewById(R.id.iv_icon);

                    DynModel dynModel = list.get(i);
                    if (dynModel != null && !StringUtil.isEmptyOrNull(dynModel.getSmallIconUrl())) {
                        if (MainApplication.finalBitmap != null){
                            MainApplication.finalBitmap.display(iv_icon, dynModel.getSmallIconUrl());
                        }
                    } else {
                        iv_icon.setImageResource(R.drawable.icon_general_receivables);
                    }
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    if(i == 0){
                        lp.setMargins(0,0,0,0);
                    }else{
                        lp.setMargins(25,0,0,0);
                    }

                    ll_pay_way.addView(ItemView,lp);
                }

            }else if(list.size() >5 ){//如果数量大于五个，则还是按照GridView的方式来显示
                gv_pay_type.setVisibility(View.VISIBLE);
                ll_pay_way.setVisibility(View.GONE);
                gv_pay_type.setNumColumns(5);
                payTypeAdape = new PayTypeAdape(StaticCodeActivity.this, list);
                gv_pay_type.setAdapter(payTypeAdape);
                setListViewHeightBasedOnChildren(gv_pay_type);
                payTypeAdape.notifyDataSetChanged();
            }
            /*if (list.size() <= 5) {
                gv_pay_type.setNumColumns(list.size());
                payTypeAdape = new PayTypeAdape(StaticCodeActivity.this, list);
                gv_pay_type.setAdapter(payTypeAdape);
            } else if (list.size() > 5) {
                gv_pay_type.setNumColumns(5);
                payTypeAdape = new PayTypeAdape(StaticCodeActivity.this, list);
                gv_pay_type.setAdapter(payTypeAdape);
                setListViewHeightBasedOnChildren(gv_pay_type);
                payTypeAdape.notifyDataSetChanged();
            }*/
            //根据支付方式更新View的显示
            updatePayViewByFixCode(dynPayTypeStatic);

            if(list.size() <= 5){
                gv_pay_type.setVisibility(View.GONE);
            }else if(list.size() >5 ){
                ll_pay_way.setVisibility(View.GONE);
            }

        } else {
            ly_lst_iocn.setVisibility(View.GONE);
            ll_pay_way.setVisibility(View.GONE);
            gv_pay_type.setVisibility(View.GONE);
            ll_separate_code.setVisibility(View.GONE);
        }


        tx_money = getViewById(R.id.tx_money);
        scan_confirm = getViewById(R.id.scan_confirm);
        scan_confirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                showPage(ActivateScanOneActivity.class);
            }
        });
        tx_reMark = getViewById(R.id.tx_reMark);
        lay_scan = getViewById(R.id.lay_scan);
        tv_scan = getViewById(R.id.tv_scan);
        tv_scan_but = getViewById(R.id.tv_scan_but);
        tv_content = getViewById(R.id.tv_content);
        img = getViewById(R.id.img);
        tx_company = getViewById(R.id.tx_company);
        tx_company.setText(MainApplication.mchName);
        Map<String, String> params = new HashMap<String, String>();
        params.put("mchId", MainApplication.merchantId);
        params.put("userId", MainApplication.userId + "");
        //        params.put("mchName", MainApplication.mchName);
        params.put("client", MainApplication.CLIENT);
        if (null == MainApplication.body || "".equals(MainApplication.body)) {
            MainApplication.body = ToastHelper.toStr(R.string.tx_mobile_pay);
        }
        params.put("body", MainApplication.body);
        sign = SignUtil.getInstance().createSign(params, MainApplication.getSignKey());
        try {

            //String language = PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN);

            //Locale locale = getResources().getConfiguration().locale; //zh-rHK
            //String lan = locale.getCountry();

            //String fp_lan;

//            if (!TextUtils.isEmpty(language)) {
//
//                if (language.equals(MainApplication.LANG_CODE_ZH_TW)) {
//                    fp_lan = MainApplication.LANG_CODE_ZH_TW;
//                } else {
//                    fp_lan = MainApplication.LANG_CODE_ZH_CN;
//                }
//            } else {
//                if (lan.equalsIgnoreCase("CN")) {
//                    fp_lan = MainApplication.LANG_CODE_ZH_CN;
//                } else {
//                    fp_lan = MainApplication.LANG_CODE_ZH_TW;
//                }
//            }
//            String codeMsg = codeStrM + "&client=" + MainApplication.CLIENT;
            String codeMsg = creatCodeMsg(false,0);

//            DynModel dynModel = (DynModel) SharedPreUtile.readProduct("dynModel" + ApiConstant.bankCode);
//            if (dynModel != null && !StringUtil.isEmptyOrNull(dynModel.getAndroidLogo())) {
//                Bitmap b = ImageUtil.decodeFile2(AppHelper.getImgCacheDir() + dynModel.getThemeMd5() + ".jpg");
//                if (b != null) {
//                    //                    bitmap = MaxCardManager.getInstance().create2DCode(StaticCodeActivity.this, codeMsg, 430, 430, b);
//
//                    firstBitmap = MaxCardManager.getInstance().create2DCode(codeMsg, DisplayUtil.dip2Px(StaticCodeActivity.this, 280), DisplayUtil.dip2Px(StaticCodeActivity.this, 280));
//                } else {

                    //                    bitmap =
                    //                        com.switfpass.pay.qrcode.MaxCardManager.getInstance().create2DCode(StaticCodeActivity.this,
                    //                            codeMsg,
                    //                            DisplayUtil.dip2Px(StaticCodeActivity.this, 280),
                    //                            DisplayUtil.dip2Px(StaticCodeActivity.this, 280),
                    //                            R.drawable.icon_app_logo);

//                    firstBitmap = MaxCardManager.getInstance().create2DCode(codeMsg, DisplayUtil.dip2Px(StaticCodeActivity.this, 280), DisplayUtil.dip2Px(StaticCodeActivity.this, 280));
//                }
//            } else {

                //                bitmap =
                //                    com.switfpass.pay.qrcode.MaxCardManager.getInstance().create2DCode(StaticCodeActivity.this,
                //                        codeMsg,
                //                        DisplayUtil.dip2Px(StaticCodeActivity.this, 280),
                //                        DisplayUtil.dip2Px(StaticCodeActivity.this, 280),
                //                        R.drawable.icon_app_logo);

                firstBitmap = MaxCardManager.getInstance().create2DCode(codeMsg, DisplayUtil.dip2Px(StaticCodeActivity.this, 280), DisplayUtil.dip2Px(StaticCodeActivity.this, 280));
//            }
            img.setImageBitmap(firstBitmap);
        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }

        tv_scan_but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!isTag) {
                    isTag = true;
                    lay_scan.setVisibility(View.VISIBLE);
                    tv_scan.setVisibility(View.VISIBLE);
                    tv_scan_but.setText(R.string.tx_stacit_switch_pay);
                } else {
                    isTag = false;
                    lay_scan.setVisibility(View.GONE);
                    tv_scan.setVisibility(View.GONE);
                    tv_scan_but.setText(R.string.tx_stacit_switch_scan);
                }
            }
        });

        if (MainApplication.isAdmin.equals("0")) {
            lay_scan.setVisibility(View.GONE);
        }

        if (MainApplication.isActive || MainApplication.isAdmin.equals("0")) {
            scan_confirm.setVisibility(View.GONE);
        }

    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(R.string.tv_scan_prompt3);
        titleBar.setRightButLayVisible(false, null);
        titleBar.setLeftButtonVisible(true);
        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.tv_code_set_moeny));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButLayClick() {
                if (!TextUtils.isEmpty(tx_money.getText().toString())) {
                    tx_money.setVisibility(View.GONE);
                    tx_reMark.setVisibility(View.GONE);
                    tx_money.setText("");
                    titleBar.setRightButLayVisibleForTotal(true, getString(R.string.tv_code_set_moeny));
                    if (firstBitmap != null) {
                        img.setImageBitmap(firstBitmap);
                    }
                } else {
                    showPage(SetCodeMoneyActivity.class);
                }

            }
        });
    }

    //    @Override
    //    public boolean onKeyDown(int keycode, KeyEvent event)
    //    {
    //        if (keycode == KeyEvent.KEYCODE_BACK)
    //        {
    //            if (!isbussFinsh)
    //            {
    //                DialogHelper.showDialog(getResources().getString(R.string.title_dialog),
    //                    getResources().getString(R.string.pay_unFinished),
    //                    getResources().getString(R.string.btnCancel),
    //                    getResources().getString(R.string.btnOk),
    //                    StaticCodeActivity.this,
    //                    new DialogInterface.OnClickListener()
    //                    {
    //                        @Override
    //                        public void onClick(DialogInterface dialogInterface, int i)
    //                        {
    //                            finish();
    //                        }
    //                    },
    //                    new DialogInterface.OnClickListener()
    //                    {
    //                        @Override
    //                        public void onClick(DialogInterface dialogInterface, int i)
    //                        {
    //                            
    //                        }
    //                    }).show();
    //            }
    //            return true;
    //        }
    //        return super.onKeyDown(keycode, event);
    //    }

    String viewPicSavePath;

    public void saveToSdcard(View v) {

        if (TextUtils.isEmpty(sign)) {
            return;
        }

        if (!TextUtils.isEmpty(viewPicSavePath)) {
            if (new File(viewPicSavePath).exists() && viewPicSavePath.contains(sign)) {
                showToastInfo(ToastHelper.toStr(R.string.save_qrcode_success));
                return;
            }
        }
        ScrollView scrollView = getViewById(R.id.static_qrcode_sv);
        scrollView.scrollTo(0, 0);
        final View view = getViewById(R.id.template_layout);
        final View leftView = view.findViewById(R.id.leftButtonLay);
        //        View RightView = view.findViewById(R.id.rightButtonLay);
        //        tv_content.setVisibility(View.GONE);
        //        RightView.setVisibility(View.GONE);
        //        tx_company.setVisibility(View.GONE);
        final View confirmView = getViewById(R.id.confirm);
        confirmView.setVisibility(View.GONE);
        tv_scan_but.setVisibility(View.GONE);
        scan_confirm.setVisibility(View.GONE);
        titleBar.setRightButLayVisibleForTotal(false, getString(R.string.tv_code_set_moeny));
        //        
        showLoading(false, ToastHelper.toStr(R.string.show_login_loading));
        leftView.setVisibility(View.GONE);
        titleBar.setLeftButtonVisible(false);
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            @Override
            public void run() {

                viewPicSavePath = ImageUtil.saveViewBitmapFile(sign, view);
                runOnUiThread(new Runnable() {
                    public void run() {
                        dismissMyLoading();
                        tx_company.setVisibility(View.VISIBLE);
                        tv_content.setVisibility(View.VISIBLE);
                        leftView.setVisibility(View.VISIBLE);
                        confirmView.setVisibility(View.VISIBLE);
                        //                        tv_scan_but.setVisibility(View.VISIBLE);
                        //                        scan_confirm.setVisibility(View.VISIBLE);
                        if (!TextUtils.isEmpty(tx_money.getText().toString())) {
                            titleBar.setRightButLayVisibleForTotal(true, getString(R.string.tv_code_clean_moeny));
                        } else {
                            titleBar.setRightButLayVisibleForTotal(true, getString(R.string.tv_code_set_moeny));
                        }
                    }
                });
                if (!TextUtils.isEmpty(viewPicSavePath)) {

                    showToastInfo(ToastHelper.toStr(R.string.save_qrcode_success) + ":" + viewPicSavePath);
                } else {
                    showToastInfo(ToastHelper.toStr(R.string.save_qrcode_fail));
                }

            }
        });
    }

}
