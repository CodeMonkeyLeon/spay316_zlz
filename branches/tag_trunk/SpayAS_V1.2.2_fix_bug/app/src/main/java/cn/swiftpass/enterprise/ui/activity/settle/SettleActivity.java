/*
 * 文 件 名:  SettleActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-5-31
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.settle;

import java.util.Date;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.SettleModel;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.user.RefundManagerActivity;
import cn.swiftpass.enterprise.ui.widget.DateTimePickDialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 结算汇总
 * 
 * @author  he_hui
 * @version  [版本号, 2016-5-31]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class SettleActivity extends TemplateActivity
{
    private RelativeLayout lay_end_time, lay_start_time;
    
    private TextView tv_start_time, tv_end_time;
    
    private TextView et_order_max_money, tv_succ_count, tv_refund_total, tv_refund_count, tv_wx_card_money, tv_wx_card,
        tv_total_count, tv_net_fee;
    
    private LinearLayout lay_cashier;
    
    private TextView tv_num, tv_name, tv_tel, tv_department;
    
    private UserModel userModel;
    
    private ImageView iv_clear;
    
    public static void startActivity(Context context, UserModel userModel)
    {
        Intent it = new Intent();
        it.setClass(context, SettleActivity.class);
        it.putExtra("userModel", userModel);
        context.startActivity(it);
    }
    
    Handler handler = new Handler()
    {
        
        public void handleMessage(android.os.Message msg)
        {
            switch (msg.what)
            {
                case HandlerManager.SETTLE_TYPE:
                    try
                    {
                        //                        userModel = (UserModel)msg.obj;
                        //                        if (null != userModel)
                        //                        {
                        //                            initValue(userModel);
                        //                            
                        //                            loadData();
                        //                        }
                        
                    }
                    catch (Exception e)
                    {
                        Log.e("hehui", "" + e);
                    }
                    
                    break;
                default:
                    break;
            }
        }
        
    };
    
    private void initValue(UserModel userModel)
    {
        lay_cashier.setVisibility(View.VISIBLE);
        tv_num.setText(userModel.getId() + "");
        tv_name.setText(userModel.getRealname());
        tv_tel.setText(userModel.getPhone());
        tv_department.setText(userModel.getDeptname());
        
    };
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settle);
        
        initView();
        initValue();
        setLister();
        userModel = (UserModel)getIntent().getSerializableExtra("userModel");
        loadData();
        
        HandlerManager.registerHandler(HandlerManager.SETTLE_TYPE, handler);
    }
    
    private void loadData()
    {
        String mobile = null;
        
        if (MainApplication.isAdmin.equals("0") && MainApplication.isOrderAuth.equals("0"))
        { //收银员且没有流水权限
            mobile = MainApplication.phone;
            titleBar.setRightButLayVisibleForTotal(false, getString(R.string.tx_user));
        }
        else
        {
            if (null != userModel)
            {
                mobile = userModel.getPhone();
            }
        }
        
        OrderManager.getInstance().getOrderTotal(tv_start_time.getText().toString().replaceAll("/", "-"),
            tv_end_time.getText().toString().replaceAll("/", "-"),
            mobile,
            new UINotifyListener<SettleModel>()
            {
                @Override
                public void onError(Object object)
                {
                    dismissLoading();
                    super.onError(object);
                    if (checkSession())
                    {
                        return;
                    }
                    if (object != null)
                    {
                        showToastInfo(object.toString());
                    }
                }
                
                @Override
                public void onPreExecute()
                {
                    super.onPreExecute();
                    
                    showNewLoading(true, ToastHelper.toStr(R.string.public_data_loading));
                }
                
                @Override
                public void onSucceed(SettleModel result)
                {
                    super.onSucceed(result);
                    dismissLoading();
                    if (result != null)
                    {
                        if (!StringUtil.isEmptyOrNull(result.getSuccess_total_fee()))
                        {
                            et_order_max_money.setText(MainApplication.getFeeFh() + result.getSuccess_total_fee() + "");
                            tv_succ_count.setText(result.getSuccess_total_count() + getString(R.string.stream_cases));
                        }
                        else
                        {
                            et_order_max_money.setText(MainApplication.getFeeFh() + "0.00");
                            tv_succ_count.setText("0 " + getString(R.string.stream_cases));
                        }
                        if (!StringUtil.isEmptyOrNull(result.getRefund_total_fee()))
                        {
                            tv_refund_total.setText(MainApplication.getFeeFh() + result.getRefund_total_fee() + "");
                            tv_refund_count.setText(result.getRefund_total_count() + getString(R.string.stream_cases));
                        }
                        else
                        {
                            tv_refund_total.setText(MainApplication.getFeeFh() + "0.00");
                            tv_refund_count.setText("0 " + getString(R.string.stream_cases));
                        }
                        if (!StringUtil.isEmptyOrNull(result.getCoupon_total_fee()))
                        {
                            
                            tv_wx_card_money.setText(MainApplication.getFeeFh() + result.getCoupon_total_fee() + "");
                            tv_wx_card.setText(result.getCoupon_total_count() + getString(R.string.stream_cases));
                        }
                        else
                        {
                            tv_wx_card_money.setText(MainApplication.getFeeFh() + "0.00");
                            tv_wx_card.setText("0 " + getString(R.string.stream_cases));
                        }
                        
                        if (!StringUtil.isEmptyOrNull(result.getTotal_net_fee()))
                        {
                            tv_net_fee.setText(MainApplication.getFeeFh() + result.getTotal_net_fee() + "");
                            tv_total_count.setText((StringUtil.paseStrToLong(result.getSuccess_total_count()) - StringUtil.paseStrToLong(result.getRefund_total_count()))
                                + getString(R.string.stream_cases));
                        }
                        else
                        {
                            tv_net_fee.setText(MainApplication.getFeeFh() + "0.00");
                            tv_total_count.setText("0 " + getString(R.string.stream_cases));
                        }
                        
                    }
                }
            });
    }
    
    private void initValue()
    {
        tv_start_time.setText(DateUtil.formatsYYYYMD(new Date().getTime()));
        
        tv_end_time.setText(DateUtil.formatNowTime(new Date().getTime()));
    }
    
    private void setLister()
    {
        iv_clear.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                lay_cashier.setVisibility(View.GONE);
                if (null != userModel)
                {
                    userModel = null;
                    
                    loadData();
                }
            }
        });
        
        lay_start_time.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                DateTimePickDialogInfo dateTimeDialog =
                    new DateTimePickDialogInfo(SettleActivity.this, DateUtil.paseTime(tv_start_time.getText()
                        .toString()), null, R.string.tv_settle_choice_start_time,
                        new DateTimePickDialogInfo.HandleBtn()
                        {
                            
                            @Override
                            public void handleOkBtn(String date)
                            {
                                tv_start_time.setText(date);
                            }
                            
                        }, null, false);
                
                DialogHelper.resize(SettleActivity.this, dateTimeDialog);
                dateTimeDialog.show();
            }
        });
        
        lay_end_time.setOnClickListener(new View.OnClickListener()//tv_settle_choice_end_time
        {
            
            @Override
            public void onClick(View v)
            {
                DateTimePickDialogInfo dateTimeDialog =
                    new DateTimePickDialogInfo(SettleActivity.this,
                        DateUtil.paseTime(tv_end_time.getText().toString()), tv_start_time.getText().toString(),
                        R.string.tv_settle_choice_end_time, new DateTimePickDialogInfo.HandleBtn()
                        {
                            
                            @Override
                            public void handleOkBtn(String date)
                            {
                                tv_end_time.setText(date);
                                
                                loadData();
                            }
                            
                        }, null, false);
                
                DialogHelper.resize(SettleActivity.this, dateTimeDialog);
                dateTimeDialog.show();
            }
        });
    }
    
    private void initView()
    {
        //tv_num,tv_name,tv_tel,tv_department
        iv_clear = getViewById(R.id.iv_clear);
        tv_num = getViewById(R.id.tv_num);
        tv_name = getViewById(R.id.tv_name);
        tv_tel = getViewById(R.id.tv_tel);
        tv_department = getViewById(R.id.tv_department);
        
        lay_cashier = getViewById(R.id.lay_cashier);
        
        lay_start_time = getViewById(R.id.lay_start_time);
        lay_end_time = getViewById(R.id.lay_end_time);
        tv_start_time = getViewById(R.id.tv_start_time);
        tv_end_time = getViewById(R.id.tv_end_time);
        
        et_order_max_money = getViewById(R.id.et_order_max_money);
        tv_succ_count = getViewById(R.id.tv_succ_count);
        tv_refund_total = getViewById(R.id.tv_refund_total);
        tv_refund_count = getViewById(R.id.tv_refund_count);
        tv_wx_card_money = getViewById(R.id.tv_wx_card_money);
        tv_wx_card = getViewById(R.id.tv_wx_card);
        tv_net_fee = getViewById(R.id.tv_net_fee);
        tv_total_count = getViewById(R.id.tv_total_count);
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tv_settle);
        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.tx_user));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
            }
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                //                showPage(SettleCashierManager.class);
                
                Bundle b = new Bundle();
                b.putBoolean("isFromOrderStreamSearch", true);
                b.putInt("serchType", 3); //从收银员列表查询
                showPage(RefundManagerActivity.class, b);
                
            }
        });
    }
}
