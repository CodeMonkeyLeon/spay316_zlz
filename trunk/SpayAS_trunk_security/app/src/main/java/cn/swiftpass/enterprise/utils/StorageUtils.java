package cn.swiftpass.enterprise.utils;

import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;

import com.example.common.sentry.SentryUtils;

import java.io.File;
import java.text.DecimalFormat;


public class StorageUtils {
	/**
	 * 默认下载存储路径
	 */
	public static final String defaultDownloadPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/jime/download/";
	
	private static final long LOW_STORAGE_THRESHOLD = 1024 * 1024 * 10;

	public static boolean isSdCardWrittenable() {

		if (TextUtils.equals(Environment.getExternalStorageState(),Environment.MEDIA_MOUNTED)) {
			return true;
		}
		return false;
	}

	public static long getAvailableStorage() {

		String storageDirectory = null;
		storageDirectory = Environment.getExternalStorageDirectory().toString();

		try {
			StatFs stat = new StatFs(storageDirectory);
			long avaliableSize = ((long) stat.getAvailableBlocks() * (long) stat
					.getBlockSize());
			return avaliableSize;
		} catch (RuntimeException ex) {
			SentryUtils.INSTANCE.uploadTryCatchException(
					ex,
					SentryUtils.INSTANCE.getClassNameAndMethodName()
			);
			return 0;
		}
	}

	public static boolean checkAvailableStorage() {

		if (getAvailableStorage() < LOW_STORAGE_THRESHOLD) {
			return false;
		}

		return true;
	}

//	@Override
//	public boolean equals(Object obj) {
//		return super.equals(obj);
//	}

	public static boolean isSDCardPresent() {

		return TextUtils.equals(Environment.getExternalStorageState(), Environment.MEDIA_MOUNTED);
	}

/*	public static void mkdir() throws IOException {

		File file = new File(defaultDownloadPath);
		if (!file.exists() || !file.isDirectory()){
			file.mkdirs();
			file.mkdir();
		}
	}*/


	public static String size(long size) {

		if (size / (1024 * 1024) > 0) {
			float tmpSize = (float) (size) / (float) (1024 * 1024);
			DecimalFormat df = new DecimalFormat("#.##");
			return "" + df.format(tmpSize) + "MB";
		} else if (size / 1024 > 0) {
			return "" + (size / (1024)) + "KB";
		} else{
			return "" + size + "B";
		}
	}



	public static boolean delete(File path) {

		boolean result = true;
		if (path.exists()) {
			if (path.isDirectory()) {
				for (File child : path.listFiles()) {
					result &= delete(child);
				}
				result &= path.delete(); // Delete empty directory.
			}
			if (path.isFile()) {
				result &= path.delete();
			}
//			if (!result) {
//
//			}
			return result;
		} else {

			return false;
		}
	}
}
