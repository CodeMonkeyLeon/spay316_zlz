package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bean.QRCodeBeanList
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.CodeListContract
import cn.swiftpass.enterprise.utils.JsonUtil
import com.example.common.entity.EmptyData
import com.example.common.sentry.SentryUtils.uploadNetInterfaceException
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @当我痛苦的站在你面前
 * @你不能说我一无所有
 * @也不能说我两手空空
 */
class CodeListPresenter : CodeListContract.Presenter {

    private var mView: CodeListContract.View? = null


    override fun getCodeList(page: Int, bindUserId: String?) {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
            AppClient.getCodeList(
                MainApplication.getInstance().getMchId(),
                MainApplication.getInstance().getUserId().toString(),
                bindUserId,
                page.toString(),
                "20",
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.getCodeListFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            try {
                                val order = JsonUtil.jsonToBean(
                                    response.message,
                                    QRCodeBeanList::class.java
                                ) as QRCodeBeanList
                                view.getCodeListSuccess(order.data)
                            } catch (e: Exception) {
                                uploadNetInterfaceException(
                                    e,
                                    "spay/qrCodeQuery",
                                    "QRCodeBeanList json解析异常",
                                    ""
                                )
                                view.getCodeListFailed(response.message)
                            }
                        }
                    }
                }
            )

        }
    }

    override fun attachView(view: CodeListContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}