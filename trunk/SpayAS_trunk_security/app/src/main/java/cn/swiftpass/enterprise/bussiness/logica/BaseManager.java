package cn.swiftpass.enterprise.bussiness.logica;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import cn.swiftpass.enterprise.bussiness.logica.threading.NotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;


public abstract class BaseManager {

	public static final int MSG_NOTIFY_ON_ERROR = 1;
	public static final int MSG_NOTIFY_ON_SUCCEED = 2;
	public static class NotifyHolder
	{
		public NotifyListener listener;
		public Object data;
	}
	
	public void callback(int what, NotifyListener listener, Object data)
	{
		if(listener instanceof UINotifyListener)
		{
			NotifyHolder holder = new NotifyHolder();
			holder.listener = listener;
			holder.data = data;
			Message msg = new Message();
			msg.what = what;
			msg.obj = holder;
			
			mainHandler.sendMessage(msg);
		}
		else
		{
			switch(what)
			{
			case MSG_NOTIFY_ON_ERROR:
			{
				listener.onError(data);
				break;
			}
			case MSG_NOTIFY_ON_SUCCEED:
			{
				listener.onSucceed(data);
				break;
			}
			}
		}
	}

	protected MainHandler mainHandler;
	protected class MainHandler extends Handler
	{
		public MainHandler()
		{
			super(Looper.getMainLooper());
		}
		
		public void handleMessage(Message msg)
		{
			NotifyHolder holder = (NotifyHolder)msg.obj;
			switch(msg.what)
			{
			case MSG_NOTIFY_ON_ERROR:
			{
				holder.listener.onError(holder.data);
				break;
			}
			case MSG_NOTIFY_ON_SUCCEED:
			{
				holder.listener.onSucceed(holder.data);
				break;
			}
			}
		};
	};
	
	
	
	public BaseManager()
	{
		mainHandler = new MainHandler();
	}
	
	/** 初始化 */
	public abstract void init();

	/** 销毁 */
	public abstract void destroy();
	
	/** 全局初始化*/
	public static void initAll()
	{
	    // TODO

		
	}
	
	/** 全局销毁*/
	public static void destoryAll()
	{
	    // TODO
	}


}
