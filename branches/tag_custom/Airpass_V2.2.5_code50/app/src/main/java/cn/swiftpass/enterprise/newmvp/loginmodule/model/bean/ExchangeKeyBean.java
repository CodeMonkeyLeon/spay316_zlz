package cn.swiftpass.enterprise.newmvp.loginmodule.model.bean;

public class ExchangeKeyBean {
    private String serPubKey;
    private String skey;

    public String getSerPubKey() {
        return serPubKey;
    }

    public void setSerPubKey(String serPubKey) {
        this.serPubKey = serPubKey;
    }

    public String getSkey() {
        return skey;
    }

    public void setSkey(String skey) {
        this.skey = skey;
    }
}
