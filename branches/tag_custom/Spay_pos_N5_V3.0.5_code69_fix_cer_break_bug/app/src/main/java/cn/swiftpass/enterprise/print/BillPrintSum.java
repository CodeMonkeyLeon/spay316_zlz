/*
 * 文 件 名:  BillPrint.java
 * 描    述:  <描述>
 * 修 改 人:  admin
 * 修改时间:  2017-3-23
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.print;

import com.nexgo.oaf.apiv3.device.printer.AlignEnum;
import com.nexgo.oaf.apiv3.device.printer.GrayLevelEnum;
import com.nexgo.oaf.apiv3.device.printer.OnPrintListener;
import com.nexgo.oaf.apiv3.device.printer.Printer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalInfo;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalItemInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  admin
 * @version  [版本号, 2017-3-23]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class BillPrintSum
{
    
    private final int FONT_SIZE_SMALL = 15;
    
    private final static int FONT_SIZE_NORMAL = 24;
    
    private final static int FONT_SIZE_BIG = 30;
    
    /** <一句话功能简述>
     * <打印日结>
     * @param printer
     * @see [类、类#方法、类#成员]
     */
    public static void printDateSum(OrderTotalInfo info, Printer printer)
    {
        printer.setGray(GrayLevelEnum.LEVEL_2);
        
        printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_data_sum), FONT_SIZE_BIG, AlignEnum.CENTER, false);
        printer.appendPrnStr(" ", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
        
        printer.appendPrnStr(ToastHelper.toStr(R.string.shop_name) + "：" , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        printer.appendPrnStr(MainApplication.mchName, FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "："+ " " + MainApplication.merchantId, FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

        if (StringUtil.isEmptyOrNull(info.getUserName())) {
            //            printBuffer.append("收银员：全部收银员\n");
        } else {
            printer.appendPrnStr(ToastHelper.toStr(R.string.tx_user) + "：" + " " + info.getUserName(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        }

        printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_start_time) + "：" + info.getStartTime() ,FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_end_time) + "：" + info.getEndTime() ,FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        printer.appendPrnStr("===========================", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);

        if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
            printer.appendPrnStr(ToastHelper.toStr(R.string.tx_pay_money) + "："+ DateUtil.formatMoneyUtils(info.getCountTotalFee()) + ToastHelper.toStr(R.string.pay_yuan) ,FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        } else {
            printer.appendPrnStr(ToastHelper.toStr(R.string.tx_pay_money) + "："+ MainApplication.getFeeType() + DateUtil.formatMoneyUtils(info.getCountTotalFee())  ,FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        }
        printer.appendPrnStr(ToastHelper.toStr(R.string.tx_bill_stream_pay_money_num) + "："+ info.getCountTotalCount()  ,FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

        if (MainApplication.feeFh.equalsIgnoreCase("¥")&& MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {

            printer.appendPrnStr(ToastHelper.toStr(R.string.tv_refund_dialog_refund_money)+ DateUtil.formatMoneyUtils(info.getCountTotalRefundFee()) + ToastHelper.toStr(R.string.pay_yuan) ,FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        } else {

            printer.appendPrnStr(ToastHelper.toStr(R.string.tv_refund_dialog_refund_money)+ MainApplication.getFeeType() + DateUtil.formatMoneyUtils(info.getCountTotalRefundFee()) ,FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        }

        printer.appendPrnStr(ToastHelper.toStr(R.string.tx_bill_reufun_total_num) + "：" + info.getCountTotalRefundCount()  ,FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

        if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {

            printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_pay_total) + "："+ DateUtil.formatMoneyUtils((info.getCountTotalFee() - info.getCountTotalRefundFee())) + ToastHelper.toStr(R.string.pay_yuan)  ,FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

        } else {

            printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_pay_total) + "："+ MainApplication.getFeeType() + DateUtil.formatMoneyUtils((info.getCountTotalFee() - info.getCountTotalRefundFee()))  ,FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        }
        printer.appendPrnStr("===========================", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
        /**
         * 是否开通小费功能，打印小费金额、总计
         */
        if (MainApplication.isTipOpenFlag()){
            // 小费金额
            printer.appendPrnStr(ToastHelper.toStr(R.string.tip_amount) + "：" + MainApplication.getFeeType() + DateUtil.formatMoneyUtils(info.getCountTotalTipFee()) ,FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            //小费笔数
            printer.appendPrnStr(ToastHelper.toStr(R.string.tip_count) + "：" + info.getCountTotalTipFeeCount() ,FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            printer.appendPrnStr("===========================", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
        }

        List<OrderTotalItemInfo> list = info.getOrderTotalItemInfo();

        Map<Integer, Object> map = new HashMap<Integer, Object>();

        for (OrderTotalItemInfo itemInfo : list) {

            switch (itemInfo.getPayTypeId()) {

                case 1:
                    map.put(0, itemInfo);
                    break;
                case 2:
                    map.put(1, itemInfo);
                    break;

                case 4:
                    map.put(2, itemInfo);
                    break;
                case 12:
                    map.put(3, itemInfo);
                    break;
                default:
                    map.put(itemInfo.getPayTypeId(), itemInfo);
                    break;
            }

        }

        for (Integer key : map.keySet()) {
            OrderTotalItemInfo itemInfo = (OrderTotalItemInfo) map.get(key);
            if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
                printer.appendPrnStr(MainApplication.getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId())) + ToastHelper.toStr(R.string.tx_money)+ DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + ToastHelper.toStr(R.string.pay_yuan) ,FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

            } else {
                printer.appendPrnStr(MainApplication.getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId())),FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                printer.appendPrnStr(ToastHelper.toStr(R.string.tx_money)+ MainApplication.getFeeType() + DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) ,FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            }
            printer.appendPrnStr(ToastHelper.toStr(R.string.tv_settle_count) + "："+ itemInfo.getSuccessCount(),FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

        }
        printer.appendPrnStr("------------------------------------------", FONT_SIZE_BIG, AlignEnum.CENTER, false);
        printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        printer.appendPrnStr(" ", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
        printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_sign) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        printer.appendPrnStr(" ", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
        
        printer.startPrint(true, new OnPrintListener()
        {
            @Override
            public void onPrintResult(final int retCode)
            {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        
                    }
                });
            }
            
            private void runOnUiThread(Runnable runnable)
            {
                
            }
        });
        
    }
}
