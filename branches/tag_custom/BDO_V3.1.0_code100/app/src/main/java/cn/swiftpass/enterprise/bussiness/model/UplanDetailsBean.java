package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created by aijingya on 2020/3/20.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: Uplan活动的基本数据类型(用一句话描述该文件做什么)
 * @date 2020/3/20.17:39.
 */
public class UplanDetailsBean implements Serializable {

    public String getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(String discountAmt) {
        this.discountAmt = discountAmt;
    }

    public String getDiscountNote() {
        return discountNote;
    }

    public void setDiscountNote(String discountNote) {
        this.discountNote = discountNote;
    }

    public String discountAmt; //"40.00"---金额
    public String discountNote; //"Uplan discount"---活动名称
}
