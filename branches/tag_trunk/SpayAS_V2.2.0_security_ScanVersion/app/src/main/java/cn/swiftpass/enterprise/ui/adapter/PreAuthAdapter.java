package cn.swiftpass.enterprise.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.PreAuthDetailsBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.PreAuthActivity;
import cn.swiftpass.enterprise.ui.activity.PreAuthThawActivity;
import cn.swiftpass.enterprise.ui.widget.BaseRecycleAdapter;

public class PreAuthAdapter extends BaseRecycleAdapter<PreAuthDetailsBean> {

    private List<PreAuthDetailsBean> mList=new ArrayList<>();
    private PreAuthDetailsBean mPreAuthDetailsBean;
    private int activityType;
    private Context mContext;
    private  Order mOrder;
    private String state="";

    public PreAuthAdapter(Context context, Order order,List<PreAuthDetailsBean> datas, int showType) {
        super(datas);
        mList=datas;
        if (mList!=null&&mList.size()>=3&&mList.get(3)!=null){
            state=mList.get(3).getMsg();
        }
        activityType=showType;
        mContext=context;
        mOrder=order;
    }

    @Override
    protected void bindData(BaseViewHolder holder, final int position) {
        mPreAuthDetailsBean=mList.get(position);

        RelativeLayout.LayoutParams params =
                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_VERTICAL);
        TextView tv_title=(TextView) holder.getView(R.id.tv_title);
       final TextView tv_tips=(TextView) holder.getView(R.id.tv_tips);
        TextView tv_content=(TextView)holder.getView(R.id.tv_content);
        View view=(View) holder.getView(R.id.view);

        String title=mPreAuthDetailsBean.getTitle()+"";
        String title_msg=mPreAuthDetailsBean.getTitle_msg()+"";
        String msg=mPreAuthDetailsBean.getMsg()+"";
        switch (activityType){
            case PreAuthActivity.TYPE_ONE:
                if (position==0||position==1){
                    tv_content.setText(title_msg);
                    tv_title.setText(title);
                    tv_tips.setTextColor(Color.parseColor("#4182E2"));
                    tv_tips.setText(msg);
                    String unauthorized=mContext.getString(R.string.unauthorized);
                    String bt_invoike=mContext.getString(R.string.bt_invoike);

                    if (state.contains(unauthorized)||state.contains(bt_invoike)){
                        tv_tips.setVisibility(View.GONE);

                    }else {
                        tv_tips.setVisibility(View.VISIBLE);
                        Drawable drawable = mContext.getResources().getDrawable(
                                R.drawable.icon_preauth_check);
                        drawable.setBounds(2, 2, drawable.getMinimumWidth(),
                                drawable.getMinimumHeight());
                        if(tv_tips != null){
                            tv_tips.setCompoundDrawables(null, null, drawable, null);
                        }
                    }

                    tv_tips.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if(listener!=null){
                                listener.onItemClick(view,position);
                            }

//                            if (position==0){
//                                PreAuthThawActivity.startActivity(mContext,mOrder,1,PreAuthActivity.TYPE_ONE);
//                            }else if (position==1){
//                                PreAuthThawActivity.startActivity(mContext,mOrder,2,PreAuthActivity.TYPE_ONE);
//                            }
                        }
                    });

                }else if (position==3){
                    tv_content.setVisibility(View.GONE);
                    tv_tips.setVisibility(View.VISIBLE);
                    tv_title.setLayoutParams(params);
                    tv_title.setText(title);
                    if (msg.equals(mContext.getString(R.string.pre_auth_authorized))) {
                        tv_tips.setTextColor(Color.parseColor("#23BF3F"));
                    }else {
                        tv_tips.setTextColor(Color.parseColor("#999999"));
                    }
                    tv_tips.setText(msg);
                }else {
                    tv_content.setVisibility(View.VISIBLE);
                    tv_tips.setVisibility(View.GONE);
                    tv_title.setText(title);
                    tv_content.setText(title_msg);

                }
                break;
            case PreAuthActivity.TYPE_TWO:
                if (position==0||position==1){
                    tv_content.setText(title_msg);
                    tv_title.setText(title);
                    tv_tips.setTextColor(Color.parseColor("#4182E2"));
                    tv_tips.setText(msg);

                    String unauthorized=mContext.getString(R.string.unauthorized);
                    String bt_invoike=mContext.getString(R.string.bt_invoike);

                    if (state.contains(unauthorized)||state.contains(bt_invoike)){
                        tv_tips.setVisibility(View.GONE);

                    }else {
                        tv_tips.setVisibility(View.VISIBLE);
                        Drawable drawable = mContext.getResources().getDrawable(
                                R.drawable.icon_preauth_check);
                        drawable.setBounds(2, 2, drawable.getMinimumWidth(),
                                drawable.getMinimumHeight());
                        if(tv_tips != null){
                            tv_tips.setCompoundDrawables(null, null, drawable, null);
                        }
                    }


                    tv_tips.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(listener!=null){
                                listener.onItemClick(view,position);
                            }
//                            if (position==0){
//                                PreAuthThawActivity.startActivity(mContext,mOrder,1,PreAuthActivity.TYPE_TWO);
//                            }else if (position==1){
//                                PreAuthThawActivity.startActivity(mContext,mOrder,2,PreAuthActivity.TYPE_TWO);
//                            }
                        }
                    });

                }else if (position==3){
                    tv_content.setVisibility(View.GONE);
                    tv_tips.setVisibility(View.VISIBLE);
                    tv_title.setLayoutParams(params);
                    tv_title.setText(title);
                    if (msg.equals(mContext.getString(R.string.pre_auth_authorized))) {
                        tv_tips.setTextColor(Color.parseColor("#23BF3F"));
                    }else {
                        tv_tips.setTextColor(Color.parseColor("#999999"));
                    }
                    tv_tips.setText(msg);
                }else {
                        tv_content.setVisibility(View.VISIBLE);
                        tv_tips.setVisibility(View.GONE);
                        tv_title.setText(title);
                        tv_content.setText(title_msg);

                }
                break;
                default:
                    break;
        }

    }

    @Override
    public int getLayoutId() {
        return R.layout.item_preauth_content;
    }

    /**
     * item点击事件接口
     */
    public  interface OnItemClickListener {
        void onItemClick(View view,int position);
    }
    private OnItemClickListener listener;
    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener=listener;
    }
}
