package cn.swiftpass.enterprise.ui.widget;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.PaymentChannelBean;
import cn.swiftpass.enterprise.bussiness.model.ReportTransactionDetailsBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.draw.PieChartView;
import cn.swiftpass.enterprise.utils.DateUtil;

/**
 * Created by aijingya on 2019/6/5.
 *
 * @Package cn.swiftpass.enterprise.ui.widget
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2019/6/5.17:09.
 */
public class PieContentView extends LinearLayout {
    private Context mContext;
    public String PageType;
    public ReportTransactionDetailsBean reportTransactionDetailsBean;

    public LinearLayout ll_pie_no_data,ll_pieChart;
    public PieChartView id_pie_chart;
    public LinearLayout ll_content_details;

    private String picColor[] = new String[]{"#4472C4", "#ED7D31", "#70AD47", "#FFC001", "#5B9BD5", "#A5A5A5",
            "#254478", "#9E480E", "#636363", "#997301", "#255E91", "#43682B",
            "#698ED0", "#F1975A", "#B7B7B7", "#FFCD33"};

    List<PaymentChannelBean> mReportTotalPies;

    public PieContentView(Context context,ReportTransactionDetailsBean transactionDetailsBean,String pageType) {
        super(context);
        this.mContext = context;
        this.reportTransactionDetailsBean = transactionDetailsBean;
        this.PageType = pageType;
        initView();
    }

    public PieContentView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initView();
    }

    public PieContentView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView();
    }

    public void initView(){
        //加载视图的布局
        LayoutInflater.from(mContext).inflate(R.layout.layout_channel_data, this);
        initViewLayout();
    }

    public void initViewLayout(){
        ll_pie_no_data = findViewById(R.id.ll_pie_no_data);
        ll_pieChart = findViewById(R.id.ll_pieChart);
        id_pie_chart  = findViewById(R.id.id_pie_chart);
        ll_content_details = findViewById(R.id.ll_content_details);

        if(reportTransactionDetailsBean != null && reportTransactionDetailsBean.getPayChannelStatistical()!= null
                && reportTransactionDetailsBean.getPayChannelStatistical().size() > 0){
            ll_pie_no_data.setVisibility(View.GONE);
            ll_pieChart.setVisibility(View.VISIBLE);
            createPie(reportTransactionDetailsBean.getPayChannelStatistical(),PageType);
            creatChannelList(reportTransactionDetailsBean.getPayChannelStatistical(),PageType);
        }else{
            ll_pie_no_data.setVisibility(View.VISIBLE);
            ll_pieChart.setVisibility(View.GONE);

        }
    }

    /**
     * 创建payment channel 饼图
     */
    public void createPie(List<PaymentChannelBean> reportTotalPies,String pageType) {
        if (reportTotalPies.size() > 0) {
            for (int i = 0; i < reportTotalPies.size(); i++) {
                int colors ;
                PaymentChannelBean pie;
                if(i > picColor.length-1){//如果通道大于16个，则循环取第一个
                    colors = Color.parseColor(picColor[i%picColor.length]);
                }else{
                    colors = Color.parseColor(picColor[i]);
                }
                pie = reportTotalPies.get(i);
                pie.setColorVal(colors);
            }

            id_pie_chart.setData(reportTotalPies,pageType);
            id_pie_chart.startAnima();
        }
    }

    /**
     * 创建payment channel 饼图右边的列表
     */
    public void creatChannelList(List<PaymentChannelBean> reportTotalPies,String pageType){
        ll_content_details.removeAllViews();
        for(int i = 0 ; i< reportTotalPies.size() ;i++){
            LayoutInflater flater = LayoutInflater.from(mContext);
            View view = flater.inflate(R.layout.item_channel_info, null);
            ImageView imageView_color = view.findViewById(R.id.iv_channel_color);
            TextView tv_channel = view.findViewById(R.id.tv_channel_name);
            TextView tv_channel_rate_count = view.findViewById(R.id.tv_channel_rate_count);

            int colors ;
            PaymentChannelBean pie;
            if(i > picColor.length-1){//如果通道大于16个，则循环取第一个
                colors = Color.parseColor(picColor[i%picColor.length]);
            }else{
                colors = Color.parseColor(picColor[i]);
            }
            pie = reportTotalPies.get(i);
            pie.setColorVal(colors);

            imageView_color.setBackgroundColor(colors);
            tv_channel.setText(MainApplication.getInstance().getPayTypeMap().get(pie.getChannelCode() + ""));
            if(PageType == "1"){
                tv_channel_rate_count.setText(DateUtil.formatPaseRMBMoney(pie.getAmountProportion() * 100.00f) + "%"
                        + "\n" + MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(pie.getTransactionAmount()));
            }else if(pageType == "2"){
                tv_channel_rate_count.setText(DateUtil.formatPaseRMBMoney(pie.getCountProportion()*100.00f)+"%"+  "   "
                        + pie.getTransactionCount());
            }
            ll_content_details.addView(view);
        }
    }
}
