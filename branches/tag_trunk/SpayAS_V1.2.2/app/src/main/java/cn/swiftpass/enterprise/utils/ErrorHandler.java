package cn.swiftpass.enterprise.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

import java.io.*;
import java.lang.Thread.UncaughtExceptionHandler;

/**
 * 捕获所有异常信息
 *
 */
public class ErrorHandler implements UncaughtExceptionHandler {
	
	private Context mContext;
	
	private static ErrorHandler errorHandler;
	
	private ErrorHandler(){}
	
	public static ErrorHandler getInstance(){
		if(errorHandler==null){
			errorHandler = new ErrorHandler();
		}
		return errorHandler;
	}
    /**删除1.3.2后的版本日志*/
    public void delete()
    {
        File file = FileUtils.getLogFile("log.txt");
        if(file.exists()){
            file.delete();
        }
    }
	public void init(Context context){
		mContext = context;
		Thread.setDefaultUncaughtExceptionHandler(this);  
	}

	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		
		String sdCardPath = null;
		try {
			sdCardPath = AppHelper.getCacheDir();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(null == sdCardPath){
			return ;
		}
		File logFile = FileUtils.getLogFile("error.txt");
		
		if(!logFile.exists()){
			try {
				
				logFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(new FileWriter(logFile,true));
			pw.println("--------------------");
			pw.println("时间 "+ DateUtil.getTodayDateTime());
			pw.println("版本 "+  mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), PackageManager.GET_CONFIGURATIONS).versionName);
			pw.println("--------------------");
			ex.printStackTrace(pw);
			ex.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(pw!=null){
				try {
					pw.close();
				} catch (Exception e) {}
			}
		}
	}

}
