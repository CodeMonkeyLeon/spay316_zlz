package cn.swiftpass.enterprise.base.mvp;

/**
 * 创建人：caoxiaoya
 * 时间：2019/4/28
 * 描述：数据传参回调，用于P
 * 备注：
 */

public interface Call<D> {
    /**
     * D泛型回调返回给P
     * @param data
     */
    void onSuccess(D data);

    /**
     * 异常，直接定义
     * @param fromRequest 告知来自哪个http请求
     * @param code
     * @param errorMsg
     */
    void onError(int fromRequest,String code,String errorMsg);
}
