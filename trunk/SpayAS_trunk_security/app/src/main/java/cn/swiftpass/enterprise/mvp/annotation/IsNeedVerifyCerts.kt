package cn.swiftpass.enterprise.mvp.annotation

@MustBeDocumented
@Target(AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.FUNCTION)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class IsNeedVerifyCerts(
    val value: Boolean
)
