package com.ziyeyouhu.library;

import android.content.Context;

public class Utils {
    /**
     * dp转换成px
     *
     * @param dp dp
     * @return px值
     */
    public static int dp2px(float dp, Context context) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }
}
