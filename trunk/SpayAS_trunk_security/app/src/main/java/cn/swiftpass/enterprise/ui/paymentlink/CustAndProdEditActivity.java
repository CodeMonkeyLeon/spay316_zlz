package cn.swiftpass.enterprise.ui.paymentlink;

import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.ADD_ORDER_CREATE_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.ADD_ORDER_CREATE_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.DATA_TAG;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.MANAGE_CREATE_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.MANAGE_CREATE_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.MANAGE_EDIT_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.MANAGE_EDIT_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.REQUEST_CODE_PICK_IMAGE;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_CREATE_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_CREATE_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_TAG;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.TYPE_TAG;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.CustAndProdEditContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.CustAndProdEditPresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.paymentlink.interfaces.OnImageDeleteViewClickListener;
import cn.swiftpass.enterprise.ui.paymentlink.model.ImageUrl;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkCustomer;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkProduct;
import cn.swiftpass.enterprise.ui.paymentlink.view.ImageCanDeleteView;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.EditTextDigitsFilter;
import cn.swiftpass.enterprise.utils.ImageUtil;
import cn.swiftpass.enterprise.utils.KeyBoardUtil;
import cn.swiftpass.enterprise.utils.KotlinUtils;
import cn.swiftpass.enterprise.utils.MoneyInputFilter;
import cn.swiftpass.enterprise.utils.RegexUtil;
import cn.swiftpass.enterprise.utils.permissionutils.OnPermissionCallback;
import cn.swiftpass.enterprise.utils.permissionutils.Permission;
import cn.swiftpass.enterprise.utils.permissionutils.PermissionDialogUtils;
import cn.swiftpass.enterprise.utils.permissionutils.PermissionsManage;

/**
 * Created by congwei.li on 2021/9/9.
 *
 * @Description: payment Link 中 customer 和 product 的编辑和新增页面
 */
public class CustAndProdEditActivity extends BaseActivity<CustAndProdEditContract.Presenter> implements CustAndProdEditContract.View {

    @BindView(R.id.ll_edit_root_view)
    LinearLayout llEditRootView;
    @BindView(R.id.ll_edit1)
    LinearLayout llEdit1;
    @BindView(R.id.ll_edit2)
    LinearLayout llEdit2;
    @BindView(R.id.ll_edit3)
    LinearLayout llEdit3;
    @BindView(R.id.ll_edit4)
    LinearLayout llEdit4;
    @BindView(R.id.tv_edit1)
    TextView tvEdit1;
    @BindView(R.id.tv_edit2)
    TextView tvEdit2;
    @BindView(R.id.tv_edit3)
    TextView tvEdit3;
    @BindView(R.id.tv_edit4)
    TextView tvEdit4;
    @BindView(R.id.et_edit1)
    EditText etEdit1;
    @BindView(R.id.et_edit2)
    EditText etEdit2;
    @BindView(R.id.et_edit3)
    EditText etEdit3;
    @BindView(R.id.et_edit4)
    EditText etEdit4;
    @BindView(R.id.line_4)
    View mLine4;
    @BindView(R.id.ll_edit5)
    LinearLayout mLlImage;
    @BindView(R.id.id_tv_upload_img)
    TextView mBtnUploadImage;
    @BindView(R.id.id_img_can_delete)
    ImageCanDeleteView mImageCanDeleteView;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;
    @BindView(R.id.line_3)
    View line3;
    @BindView(R.id.tv_fee)
    TextView tvFee;
    private int type; // 1:customer 2:product
    private PaymentLinkCustomer customer;
    private PaymentLinkProduct product;
    private PaymentLinkCustomer newCustomer;
    private PaymentLinkProduct newProduct;

    private String[] mPermissions;
    private Uri mImageUri;

    public static void startActivity(Context mContext, int type, Serializable data) {
        Intent intent = new Intent(mContext, CustAndProdEditActivity.class);
        intent.putExtra(TYPE_TAG, type);
        intent.putExtra(DATA_TAG, data);
        mContext.startActivity(intent);
    }

    public static void startActivityForResult(Activity mContext, int type, Serializable data) {
        Intent intent = new Intent(mContext, CustAndProdEditActivity.class);
        intent.putExtra(TYPE_TAG, type);
        intent.putExtra(DATA_TAG, data);
        mContext.startActivityForResult(intent, type);
    }

    @Override
    protected CustAndProdEditContract.Presenter createPresenter() {
        return new CustAndProdEditPresenter();
    }

    private void checkConfirmState() {
        if (type == MANAGE_CREATE_CUSTOMER || type == MANAGE_EDIT_CUSTOMER
                || type == ADD_ORDER_CREATE_CUSTOMER) {
            tvConfirm.setEnabled(newCustomer.isAllFilled() && !newCustomer.isEqual(customer));
        } else if (type == MANAGE_CREATE_PRODUCT || type == MANAGE_EDIT_PRODUCT
                || type == ADD_ORDER_CREATE_PRODUCT) {
            tvConfirm.setEnabled(newProduct.isAllFilled() && !newProduct.isEqual(product));
        }
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cust_prod_edit);
        ButterKnife.bind(this);

        type = getIntent().getIntExtra(TYPE_TAG, MANAGE_CREATE_CUSTOMER);
        initView();
    }

    private void initView() {
        titleBar.setLeftButton(R.drawable.icon_general_top_back_default);
        if (type == MANAGE_CREATE_CUSTOMER || type == MANAGE_EDIT_CUSTOMER
                || type == ADD_ORDER_CREATE_CUSTOMER) {
            newCustomer = new PaymentLinkCustomer();
            customer = (PaymentLinkCustomer) getIntent().getSerializableExtra(DATA_TAG);
            if (customer == null) {
                customer = new PaymentLinkCustomer();
            }
            newCustomer.copyFrom(customer);
            showCustomerEdit();
        } else if (type == MANAGE_CREATE_PRODUCT || type == MANAGE_EDIT_PRODUCT
                || type == ADD_ORDER_CREATE_PRODUCT) {
            newProduct = new PaymentLinkProduct();
            product = (PaymentLinkProduct) getIntent().getSerializableExtra(DATA_TAG);
            if (product == null) {
                product = new PaymentLinkProduct();

            }
            newProduct.copyFrom(product);
            showProductEdit();
        }
    }

    private void showProductEdit() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            //android13
            mPermissions = Permission.Group.STORAGE_API_33;
        } else {
            mPermissions = Permission.Group.STORAGE;
        }

        titleBar.setTitleText(type == MANAGE_EDIT_PRODUCT ? R.string.pl_edit_product_title :
                R.string.payment_link_add_product);
        llEdit4.setVisibility(View.VISIBLE);
        tvFee.setVisibility(View.VISIBLE);
        line3.setVisibility(View.VISIBLE);


        //显示上传图片
        mLine4.setVisibility(View.VISIBLE);
        mLlImage.setVisibility(View.VISIBLE);

        if (TextUtils.isEmpty(newProduct.goodsPic)) {
            showSelectPhoto(false);
        } else {
            mImageUri = Uri.parse(newProduct.goodsPic);
            showSelectPhoto(true);
        }


        tvEdit1.setText(getStringById(R.string.payment_link_add_product_name));
        tvEdit2.setText(getStringById(R.string.payment_link_add_product_phone_code));
        tvEdit3.setText(getStringById(R.string.payment_link_add_product_price));
        tvEdit4.setText(getStringById(R.string.payment_link_add_product_description));
        etEdit1.setHint(getStringById(R.string.payment_link_add_product_name_hint));
        etEdit2.setHint(getStringById(R.string.payment_link_add_product_phone_code_hint));
        etEdit3.setHint(getStringById(R.string.pl_create_product_price_hint));
        etEdit4.setHint(getStringById(R.string.payment_link_add_product_description_hint));

        tvConfirm.setText(getStringById(R.string.payment_link_add_customer_done));
        etEdit1.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});
        etEdit2.setFilters(new InputFilter[]{new InputFilter.LengthFilter(30)});
        etEdit3.setFilters(new InputFilter[]{new MoneyInputFilter(), new InputFilter.LengthFilter(20)});
        etEdit4.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
        etEdit3.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);

        if (!TextUtils.isEmpty(product.goodsPrice)) {
            tvFee.setTextColor(CustAndProdEditActivity.this.getResources()
                    .getColor(R.color.black));
        }
        tvFee.setText(MainApplication.getInstance().getFeeFh() + " ");

        etEdit1.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                newProduct.goodsName = etEdit1.getText().toString().trim();
                checkConfirmState();
            }
        });
        etEdit2.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                newProduct.goodsCode = etEdit2.getText().toString().trim();
                checkConfirmState();
            }
        });
        etEdit3.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                newProduct.goodsPrice = DateUtil.formatMoneyInt(etEdit3.getText().toString().trim());
                checkConfirmState();
            }
        });
        etEdit3.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus || !TextUtils.isEmpty(newProduct.goodsPrice)) {
                    tvFee.setTextColor(CustAndProdEditActivity.this.getResources()
                            .getColor(R.color.black));
                } else {
                    tvFee.setTextColor(CustAndProdEditActivity.this.getResources()
                            .getColor(R.color.color_A3A3A3));
                }
            }
        });
        etEdit4.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                newProduct.goodsDesc = etEdit4.getText().toString().trim();
                checkConfirmState();
            }
        });

        if (!TextUtils.isEmpty(newProduct.goodsId)) {
            if (mPresenter != null) {
                mPresenter.getProductDetail(newProduct);
            }
        } else {
            llEditRootView.setVisibility(View.VISIBLE);
        }


        //点击上传图片
        mBtnUploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toSelectImage();
            }
        });

        mImageCanDeleteView.setOnClickListener(new OnImageDeleteViewClickListener() {
            @Override
            public void onImageClick(@Nullable ImageView imageView) {
                BigImageDisplayActivity.Companion.startBigImageDisplayActivity(
                        CustAndProdEditActivity.this, mImageUri
                );
            }

            @Override
            public void onDeleteClick() {
                showDeleteConfirmDialog();
            }
        });
    }


    private void showDeleteConfirmDialog() {
        DialogInfo deleteDialog = new DialogInfo(
                CustAndProdEditActivity.this,
                getString(R.string.string_payment_link_delete_photo_dialog_title),
                getString(R.string.string_payment_link_delete_photo_dialog_content),
                getString(R.string.string_payment_link_delete_photo_dialog_delete),
                getString(R.string.string_payment_link_delete_photo_dialog_cancel),
                DialogInfo.UNIFED_DIALOG,
                new DialogInfo.HandleBtn() {

                    @Override
                    public void handleOkBtn() {
                        showSelectPhoto(false);
                    }

                    @Override
                    public void handleCancelBtn() {

                    }
                },
                null
        );
        deleteDialog.setBtnOkTextColor(getResources().getColor(R.color.cashier_add));
        deleteDialog.setBtnCancelTextColor(getResources().getColor(R.color.cashier_add));
        deleteDialog.setBtnCancelTextBold();
        DialogHelper.resize(CustAndProdEditActivity.this, deleteDialog);
        deleteDialog.show();
    }


    private void showSelectPhoto(boolean isShow) {
        mBtnUploadImage.setVisibility(isShow ? View.GONE : View.VISIBLE);
        mImageCanDeleteView.setVisibility(isShow ? View.VISIBLE : View.GONE);
        if (isShow) {
            newProduct.goodsPic = mImageUri.getPath();
            mImageCanDeleteView.setImage(mImageUri);
        } else {
            newProduct.goodsPic = "";
            mImageUri = null;
        }
        checkConfirmState();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PICK_IMAGE
                && resultCode == Activity.RESULT_OK
                && data != null) {
            mImageUri = data.getData();
            if (mImageUri != null) {
                showSelectPhoto(true);
            }
        } else if (requestCode == BigImageDisplayActivity.REQUEST_CODE
                && resultCode == BigImageDisplayActivity.RESULT_CODE
                && data != null) {
            boolean isDeletePhoto = data.getBooleanExtra(BigImageDisplayActivity.IS_DELETE_PHOTO, false);
            if (isDeletePhoto) {
                showSelectPhoto(false);
            }
        }
    }

    private void requestLocalPermission() {
        PermissionDialogUtils.requestPermission(this,
                mPermissions, false,
                new OnPermissionCallback() {
                    @Override
                    public void onGranted(List<String> permissions, boolean all) {
                        if (all) {
                            toSelectImage();
                        }
                    }
                });
    }


    private void toSelectImage() {
        if (!PermissionsManage.isGranted(this, mPermissions)) {
            requestLocalPermission();
            return;
        }
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CODE_PICK_IMAGE);
    }


    @Override
    public void getProductDetailSuccess(@Nullable PaymentLinkProduct response) {
        newProduct = response;
        etEdit1.setText(newProduct.goodsName);
        etEdit2.setText(newProduct.goodsCode);
        etEdit3.setText(DateUtil.formatMoneyUtils(newProduct.goodsPrice));
        etEdit4.setText(newProduct.goodsDesc);
        llEditRootView.setVisibility(View.VISIBLE);
    }

    @Override
    public void getProductDetailFailed(@Nullable Object error) {
        toastDialog(CustAndProdEditActivity.this,
                error.toString(), new NewDialogInfo.HandleBtn() {
                    @Override
                    public void handleOkBtn() {
                        if (!CustAndProdEditActivity.this.isFinishing()) {
                            CustAndProdEditActivity.this.finish();
                        }
                    }
                });
    }

    private void showCustomerEdit() {
        titleBar.setTitleText(type == MANAGE_EDIT_CUSTOMER ? R.string.pl_edit_customer_title :
                R.string.payment_link_add_customer);
        llEdit4.setVisibility(View.GONE);
        tvFee.setVisibility(View.GONE);
        line3.setVisibility(View.GONE);

        //隐藏上传图片
        mLine4.setVisibility(View.GONE);
        mLlImage.setVisibility(View.GONE);

        tvEdit1.setText(getStringById(R.string.payment_link_add_customer_name));
        tvEdit2.setText(getStringById(R.string.payment_link_add_customer_phone_number));
        tvEdit3.setText(getStringById(R.string.payment_link_add_customer_email));
        etEdit1.setHint(getStringById(R.string.payment_link_add_customer_name_hint));
        etEdit2.setHint(getStringById(R.string.payment_link_add_customer_phone_number_hint));
        etEdit3.setHint(getStringById(R.string.payment_link_add_customer_email_hint));
        tvConfirm.setText(getStringById(R.string.payment_link_add_customer_done));

        etEdit1.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
        etEdit2.setFilters(new InputFilter[]{new EditTextDigitsFilter("0123456789"), new InputFilter.LengthFilter(50)});
        etEdit3.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});

        etEdit1.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                newCustomer.custName = etEdit1.getText().toString().trim();
                checkConfirmState();
            }
        });
        etEdit2.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                newCustomer.custMobile = etEdit2.getText().toString().trim();
                checkConfirmState();
            }
        });
        etEdit3.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                newCustomer.custEmail = etEdit3.getText().toString().trim();
                checkConfirmState();
            }
        });
        if (!TextUtils.isEmpty(newCustomer.custId)) {
            if (mPresenter != null) {
                mPresenter.getCustomerDetail(newCustomer);
            }
        } else {
            llEditRootView.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void getCustomerDetailSuccess(@Nullable PaymentLinkCustomer response) {
        newCustomer = response;
        etEdit1.setText(newCustomer.custName);
        etEdit2.setText(newCustomer.custMobile);
        etEdit3.setText(newCustomer.custEmail);
        llEditRootView.setVisibility(View.VISIBLE);
    }

    @Override
    public void getCustomerDetailFailed(@Nullable Object error) {
        toastDialog(CustAndProdEditActivity.this,
                error.toString(), new NewDialogInfo.HandleBtn() {
                    @Override
                    public void handleOkBtn() {
                        if (!CustAndProdEditActivity.this.isFinishing()) {
                            CustAndProdEditActivity.this.finish();
                        }
                    }
                });
    }

    @OnClick({R.id.tv_confirm})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_confirm:
                saveInfo();
                break;
        }
    }


    private void saveInfo() {
        if (type == MANAGE_CREATE_CUSTOMER || type == ADD_ORDER_CREATE_CUSTOMER) {
            saveCustomer();
        } else if (type == MANAGE_EDIT_CUSTOMER) {
            editCustomer();
        } else if (type == MANAGE_CREATE_PRODUCT || type == ADD_ORDER_CREATE_PRODUCT) {
            uploadProductImage();
        } else if (type == MANAGE_EDIT_PRODUCT) {
            uploadProductImage();
        }
    }


    private void uploadProductImage() {
        if (mImageUri != null) {
            //上传图片
            if (mPresenter != null) {
//                Bitmap resultBimap = ImageUtil.getBitmapFromPath(getActivity(), mImageUri);
                Bitmap resultBimap = KotlinUtils.INSTANCE.displayImageWithOrientation(mImageUri, getActivity());
                if (resultBimap != null) {
                    Bitmap selectImage = ImageUtil.compressImage(resultBimap);
                    mPresenter.uploadImg(ImageUtil.bitmapToBase64(selectImage), "jpg");
                }
            }
        } else {
            //不上传图片
            if (type == MANAGE_CREATE_PRODUCT || type == ADD_ORDER_CREATE_PRODUCT) {
                saveProduct();
            } else if (type == MANAGE_EDIT_PRODUCT) {
                editProduct();
            }
        }
    }


    @Override
    public void uploadImgSuccess(@NonNull ImageUrl response) {
        String imgUrl = response.picUrl;
        newProduct.goodsPic = imgUrl;
        if (type == MANAGE_CREATE_PRODUCT || type == ADD_ORDER_CREATE_PRODUCT) {
            saveProduct();
        } else if (type == MANAGE_EDIT_PRODUCT) {
            editProduct();
        }
    }

    @Override
    public void uploadImgFailed(@Nullable Object error) {
        if (null != getActivity()) {
            toastDialog(getActivity(), error.toString(), null);
        }
    }

    @Override
    public void addNewProductSuccess(@Nullable PaymentLinkProduct response) {
        newProduct = response;
        saveInfoSuccess();
    }

    @Override
    public void addNewProductFailed(@Nullable Object error) {
        toastDialog(CustAndProdEditActivity.this, error.toString(), null);
    }

    private void saveProduct() {
        if (mPresenter != null) {
            mPresenter.addNewProduct(newProduct);
        }
    }


    @Override
    public void editProductDetailSuccess(@Nullable String response) {
        saveInfoSuccess();
    }

    @Override
    public void editProductDetailFailed(@Nullable Object error) {
        toastDialog(CustAndProdEditActivity.this, error.toString(), null);
    }

    private void editProduct() {
        if (mPresenter != null) {
            mPresenter.editProductDetail(newProduct);
        }
    }


    @Override
    public void addNewCustomerSuccess(@Nullable PaymentLinkCustomer response) {
        newCustomer = response;
        saveInfoSuccess();
    }

    @Override
    public void addNewCustomerFailed(@Nullable Object error) {
        toastDialog(CustAndProdEditActivity.this, error.toString(), null);
    }

    private void saveCustomer() {
        if (!TextUtils.isEmpty(newCustomer.custEmail) && !RegexUtil.isEmail(newCustomer.custEmail)) {
            toastDialog(CustAndProdEditActivity.this,
                    getStringById(R.string.payment_link_add_customer_email_error), null);
        } else {
            if (mPresenter != null) {
                mPresenter.addNewCustomer(newCustomer);
            }
        }
    }


    @Override
    public void editCustomerDetailSuccess(@Nullable String response) {
        saveInfoSuccess();
    }

    @Override
    public void editCustomerDetailFailed(@Nullable Object error) {
        toastDialog(CustAndProdEditActivity.this, error.toString(), null);
    }

    private void editCustomer() {
        if (!TextUtils.isEmpty(newCustomer.custEmail) && !RegexUtil.isEmail(newCustomer.custEmail)) {
            toastDialog(CustAndProdEditActivity.this,
                    getStringById(R.string.payment_link_add_customer_email_error), null);
        } else {
            if (mPresenter != null) {
                mPresenter.editCustomerDetail(newCustomer);
            }
        }
    }

    private void saveInfoSuccess() {
        Toast.makeText(this, getResources().getString(R.string.save_success),
                Toast.LENGTH_SHORT).show();
        if (type == MANAGE_CREATE_CUSTOMER || type == MANAGE_EDIT_CUSTOMER
                || type == ADD_ORDER_CREATE_CUSTOMER) {
            Intent i = new Intent();
            i.putExtra(RESULT_TAG, newCustomer);
            setResult(RESULT_CODE_CREATE_CUSTOMER, i);
            finish();
        } else if (type == MANAGE_CREATE_PRODUCT || type == MANAGE_EDIT_PRODUCT
                || type == ADD_ORDER_CREATE_PRODUCT) {
            Intent i = new Intent();
            i.putExtra(RESULT_TAG, newProduct);
            setResult(RESULT_CODE_CREATE_PRODUCT, i);
            finish();
        } else {
            finish();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = this.getCurrentFocus();
        if (KeyBoardUtil.isShouldHideInput(v, ev)) {//点击editText控件外部
            InputMethodManager imm = (InputMethodManager) this
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                KeyBoardUtil.hideKeyboard(v);//软键盘工具类
                if (llEditRootView != null) {
                    llEditRootView.clearFocus();
                }
            }
        }
        return getWindow().superDispatchTouchEvent(ev) || onTouchEvent(ev);
    }


}
