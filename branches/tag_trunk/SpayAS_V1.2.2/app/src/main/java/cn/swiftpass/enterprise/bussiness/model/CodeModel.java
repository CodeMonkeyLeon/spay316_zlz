package cn.swiftpass.enterprise.bussiness.model;

import cn.swiftpass.enterprise.io.database.table.CodeTable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-10-31
 * Time: 下午12:15
 * To change this template use File | Settings | File Templates.
 */
@DatabaseTable(tableName = CodeTable.TABLE_NAME)
public class CodeModel implements Serializable {
    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField(columnName = CodeTable.CODEVALUE)
    public int value;
}
