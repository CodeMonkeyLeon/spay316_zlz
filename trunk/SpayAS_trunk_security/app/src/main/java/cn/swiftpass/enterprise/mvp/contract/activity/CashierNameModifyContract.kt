package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.CashierAddBean
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo
import cn.swiftpass.enterprise.bussiness.model.UserModel
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView


/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @我相信
 * @那一切都是种子
 * @只有经过埋葬
 * @才有生机
 */
class CashierNameModifyContract {

    interface View : BaseView {


        fun cashierAddSuccess(response: CashierAddBean?)

        fun cashierAddFailed(error: Any?)


        fun ecdhKeyExchangeSuccess(
            response: ECDHInfo?,
            userModel: UserModel?
        )

        fun ecdhKeyExchangeFailed(error: Any?)
    }

    interface Presenter : BasePresenter<View> {


        fun ecdhKeyExchange(
            publicKey: String?,
            userModel: UserModel?
        )


        fun cashierAdd(
            userModel: UserModel?,
            isAddCashier: Boolean
        )
    }

}