package cn.swiftplus.enterprise.printsdk.print;
import android.content.Context;
import android.graphics.Bitmap;

import com.nexgo.oaf.apiv3.device.printer.AlignEnum;
import com.pax.gl.imgprocessing.IImgProcessing;

import cn.swiftplus.enterprise.printsdk.print.A8.POSA8Client;
import cn.swiftplus.enterprise.printsdk.print.A8.printUtils.PrintA8Bean;
import cn.swiftplus.enterprise.printsdk.print.A8.printUtils.PrintA8Text;
import cn.swiftplus.enterprise.printsdk.print.A920.POSA920Client;
import cn.swiftplus.enterprise.printsdk.print.A920.printUtils.PrintA920Text;
import cn.swiftplus.enterprise.printsdk.print.N5.POSN5Client;
import cn.swiftplus.enterprise.printsdk.print.N5.printUtils.PrintN5Text;
import cn.swiftplus.enterprise.printsdk.print.Sunmi.POSSunmiClient;
import cn.swiftplus.enterprise.printsdk.print.Sunmi.printUtils.PrintSunmiText;
import cn.swiftplus.enterprise.printsdk.print.TradelinkA8.POSTradelinkA8Client;
import cn.swiftplus.enterprise.printsdk.print.TradelinkA8.printUtils.PrintTradelinkA8Text;
import hk.com.tradelink.tess.ecr.export.Printing;

import static cn.swiftplus.enterprise.printsdk.print.N5.printUtils.PrintN5Text.FONT_SIZE_NORMAL;


/**
 * Created by aijingya on 2020/8/7.
 *
 * @Package cn.swiftplus.enterprise.printsdk.print
 * @Description:
 * @date 2020/8/7.10:50.
 */
public class PrintClient {
    private static PrintClient sPrintClient;
    private static String sPosTerminal;
    private Context mAppContext;
    private long printDelay;

    public static void initClient(Context context){
        if (sPrintClient != null) {
            throw new RuntimeException("PrintClient Already initialized");
        }
        sPrintClient = new PrintClient(context.getApplicationContext());
    }

    public static PrintClient getInstance() {
        if (sPrintClient == null) {
            throw new RuntimeException("PrintClient is not initialized");
        }
        return sPrintClient;
    }

    private PrintClient(Context context ){
        mAppContext = context;
    }

    public void initPrintSDK(String posTerminal){
        sPosTerminal = posTerminal;
        //根据不同的POS机型号，初始化不同的打印机SDK
        if(sPosTerminal.equalsIgnoreCase("A920")){
            POSA920Client.initA920(mAppContext);
            setPrintDelay(5000L);
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            POSA8Client.initA8(mAppContext);
            setPrintDelay(4000L);
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){
            POSSunmiClient.initSunmi(mAppContext);
            setPrintDelay(4000L);
        }else if(sPosTerminal.equalsIgnoreCase("N5")){
            POSN5Client.initN5(mAppContext);
            setPrintDelay(5000L);
        }else if(sPosTerminal.equalsIgnoreCase("tradelinkA8")){
            POSTradelinkA8Client.initTradelinkA8(mAppContext);
            setPrintDelay(6000L);
        }
    }

    public long getPrintDelay() {
        return printDelay;
    }

    public void setPrintDelay(long printDelay) {
        this.printDelay = printDelay;
    }

    public void printTitle(String text){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().init(mAppContext);
            PrintA920Text.getInstance().printTitle(text);
            PrintA920Text.getInstance().printEmptyLine();
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.TITLE,text);
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){
            PrintSunmiText.getInstance().init(mAppContext);
            PrintSunmiText.getInstance().printTitle(text);
        }else if(sPosTerminal.equalsIgnoreCase("N5")){
            PrintN5Text.getInstance().init(mAppContext);
            PrintN5Text.getInstance().printTitle(text);
        }else if(sPosTerminal.equalsIgnoreCase("tradelinkA8")){
            PrintTradelinkA8Text.getInstance().init(mAppContext);
            PrintTradelinkA8Text.getInstance().printTitle(text);
        }
    }

    public void printTextLeft(String text){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().printTextLine(text,24, IImgProcessing.IPage.EAlign.LEFT);
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.TEXT_LINE_LEFT,text);
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){
            PrintSunmiText.getInstance().printTextLine(text,24,0);
        }else if(sPosTerminal.equalsIgnoreCase("N5")){
            PrintN5Text.getInstance().printTextLine(text,FONT_SIZE_NORMAL, AlignEnum.LEFT);
        }else if(sPosTerminal.equalsIgnoreCase("tradelinkA8")){
            PrintTradelinkA8Text.getInstance().printTextLine(text, Printing.ALIGN_START );
        }
    }

    public void  printTextRight(String text){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().printTextLine(text,24, IImgProcessing.IPage.EAlign.RIGHT);
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.TEXT_LINE_RIGHT,text);
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){
            PrintSunmiText.getInstance().printTextLine(text,24,2);
        }else if(sPosTerminal.equalsIgnoreCase("N5")){
            PrintN5Text.getInstance().printTextLine(text,FONT_SIZE_NORMAL, AlignEnum.RIGHT);
        }else if(sPosTerminal.equalsIgnoreCase("tradelinkA8")){
            PrintTradelinkA8Text.getInstance().printTextLine(text, Printing.ALIGN_END );
        }
    }

    public void  printTextCenter(String text){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().printTextLine(text,24, IImgProcessing.IPage.EAlign.CENTER);
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.TEXT_LINE_CENTER,text);
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){
            PrintSunmiText.getInstance().printTextLine(text,24,1);
        }else if(sPosTerminal.equalsIgnoreCase("N5")){
            PrintN5Text.getInstance().printTextLine(text,FONT_SIZE_NORMAL, AlignEnum.CENTER);
        }else if(sPosTerminal.equalsIgnoreCase("tradelinkA8")){
            PrintTradelinkA8Text.getInstance().printTextLine(text, Printing.ALIGN_CENTER );
        }
    }

    public void printMultiLines(String text){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().printTextLine(text,24, IImgProcessing.IPage.EAlign.LEFT);
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.TEXT_MULTI_LINES,text);
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){
            PrintSunmiText.getInstance().printTextLine(text,24,0);
        }else if(sPosTerminal.equalsIgnoreCase("N5")){
            PrintN5Text.getInstance().printTextLine(text,FONT_SIZE_NORMAL, AlignEnum.LEFT);
        }else if(sPosTerminal.equalsIgnoreCase("tradelinkA8")){
            PrintTradelinkA8Text.getInstance().printMultiLines(text);
        }
    }

    public void printBitmapLogo(Bitmap bitmap){
        if(sPosTerminal.equalsIgnoreCase("A920")){
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){
            PrintSunmiText.getInstance().printBitmap(bitmap);
        }else if(sPosTerminal.equalsIgnoreCase("N5")){
            PrintN5Text.getInstance().printBitmap(bitmap);
        }else if(sPosTerminal.equalsIgnoreCase("tradelinkA8")){
        }
    }

    public void  printQRCode(String orderNoMch){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().printQRCode(orderNoMch);
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.QR_CODE,orderNoMch);
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){
            PrintSunmiText.getInstance().printQRCode(orderNoMch);
        }else if(sPosTerminal.equalsIgnoreCase("N5")){
            PrintN5Text.getInstance().printQRCode(orderNoMch);
        }else if(sPosTerminal.equalsIgnoreCase("tradelinkA8")){
            PrintTradelinkA8Text.getInstance().printQRCode(orderNoMch);
        }
    }

    public void printDoubleLine(){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().printDoubleLine();
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.DOUBLE_LINE,"");
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){
            PrintSunmiText.getInstance().printDoubleLine();
        }else if(sPosTerminal.equalsIgnoreCase("N5")){
            PrintN5Text.getInstance().printDoubleLine();
        }else if(sPosTerminal.equalsIgnoreCase("tradelinkA8")){
            PrintTradelinkA8Text.getInstance().printDoubleLine();
        }
    }

    public void pintSingleLine(){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().pintSingleLine();
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.SINGLE_LINE,"");
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){
            PrintSunmiText.getInstance().pintSingleLine();
        }else if(sPosTerminal.equalsIgnoreCase("N5")){
            PrintN5Text.getInstance().pintSingleLine();
        }else if(sPosTerminal.equalsIgnoreCase("tradelinkA8")){
            PrintTradelinkA8Text.getInstance().printSingleLine();
        }
    }

    public void printEmptyLine(){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().printEmptyLine();
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.EMPTY_LINE,"");
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){
            PrintSunmiText.getInstance().printEmptyLine();
        }else if(sPosTerminal.equalsIgnoreCase("N5")){
            PrintN5Text.getInstance().printEmptyLine();
        }else if(sPosTerminal.equalsIgnoreCase("tradelinkA8")){
            PrintTradelinkA8Text.getInstance().printEmptyLine();
        }
    }

    public void startPrint(){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().startPrint();
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().init(mAppContext);
            PrintA8Text.getInstance().startPrint();
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){
        }else if(sPosTerminal.equalsIgnoreCase("N5")){
            PrintN5Text.getInstance().startPrint();
        }else if(sPosTerminal.equalsIgnoreCase("tradelinkA8")){
            PrintTradelinkA8Text.getInstance().startPrint();
        }
    }

    public void bindDeviceService(){
        if(sPosTerminal.equalsIgnoreCase("A920")){
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().bindDeviceService();
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){
        }else if(sPosTerminal.equalsIgnoreCase("N5")){
        }else if(sPosTerminal.equalsIgnoreCase("tradelinkA8")){
            PrintTradelinkA8Text.getInstance().bindDeviceService(mAppContext);
        }
    }


    public void unbindDeviceService(){
        if(sPosTerminal.equalsIgnoreCase("A920")){
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().unbindDeviceService();
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){
        }else if(sPosTerminal.equalsIgnoreCase("N5")){
        }else if(sPosTerminal.equalsIgnoreCase("tradelinkA8")){
            PrintTradelinkA8Text.getInstance().unbindDeviceService();
        }
    }

}