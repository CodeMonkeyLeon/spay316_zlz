package cn.swiftpass.enterprise.io.database.table;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-10-27
 * Time: 下午7:40
 * To change this template use File | Settings | File Templates.
 */
public class AdsTable  extends TableBase{

    public static final String TABLE_NAME="tb_adsInfo";
    public static final String IMG_NAME="img_name";


    @Override
    public void createTable(SQLiteDatabase db) {
        final String SQL_CREATE_TABLE = "create table "+TABLE_NAME+"("
                + COLUMN_ID + " INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT  ," +
                IMG_NAME + " VARCHAR(32)"+")";
        db.execSQL(SQL_CREATE_TABLE);
    }
}
