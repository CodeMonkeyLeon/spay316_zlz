package cn.swiftpass.enterprise.print;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalInfo;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalItemInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;
import hk.com.tradelink.tess.android.Printable;
import hk.com.tradelink.tess.ecr.export.Printing;

import static cn.swiftpass.enterprise.MainApplication.getContext;
import static cn.swiftpass.enterprise.utils.ImageUtil.getSDcardPath;

/**
 * Created by aijingya on 2019/1/30.
 *
 * @Package cn.swiftpass.enterprise.print
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2019/1/30.16:09.
 */

public class HasePrintUtils {
    private static final String TAG = HasePrintUtils.class.getSimpleName();


    public static Printable createDateSummaryPrintable(OrderTotalInfo info) {

        List<Printable.Step> list = new ArrayList<>();
        list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.tx_blue_print_data_sum)}, Printing.ALIGN_CENTER |Printing.SIZE_MEDIUM));
        list.add(new Printable.Lines(1));
        list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.shop_name) + "："}, Printing.ALIGN_START ));
        list.add(new Printable.Strings(new String[]{MainApplication.getMchName()}, Printing.ALIGN_START ));


        if (StringUtil.isEmptyOrNull(info.getUserName())) {
            //            printBuffer.append("收银员：全部收银员\n");
        } else {
            list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.tx_user) + "：" + info.getUserName()}, Printing.ALIGN_START ));
        }
        list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.tx_blue_print_start_time) + "：" + info.getStartTime()}, Printing.ALIGN_START ));
        list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.tx_blue_print_end_time) + "：" + info.getEndTime()}, Printing.ALIGN_START ));

        list.add(new Printable.Lines(1, '='));

        if (MainApplication.feeFh.equalsIgnoreCase("¥")) {
            list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.tx_pay_money) + "：", DateUtil.formatMoneyUtils(info.getCountTotalFee()) + ToastHelper.toStr(R.string.pay_yuan)));
        } else {
            list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.tx_pay_money) + "：", MainApplication.getFeeType() + DateUtil.formatMoneyUtils(info.getCountTotalFee())));
        }
        list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.tx_bill_stream_pay_money_num) + "：", info.getCountTotalCount().toString()));

        if (MainApplication.feeFh.equalsIgnoreCase("¥")) {
            list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.tv_refund_dialog_refund_money), DateUtil.formatMoneyUtils(info.getCountTotalRefundFee()) + ToastHelper.toStr(R.string.pay_yuan)));
        } else {
            list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.tv_refund_dialog_refund_money), MainApplication.getFeeType() + DateUtil.formatMoneyUtils(info.getCountTotalRefundFee())));
        }
        list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.tx_bill_reufun_total_num) + "：",info.getCountTotalRefundCount().toString()));

        if (MainApplication.feeFh.equalsIgnoreCase("¥")) {
            list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.tx_blue_print_pay_total) + "：",
                    DateUtil.formatMoneyUtils((info.getCountTotalFee() - info.getCountTotalRefundFee())) + ToastHelper.toStr(R.string.pay_yuan)));
        } else {
            list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.tx_blue_print_pay_total) + "：",
                    MainApplication.getFeeType() + DateUtil.formatMoneyUtils((info.getCountTotalFee() - info.getCountTotalRefundFee()))));
        }
        list.add(new Printable.Lines(1, '='));

        List<OrderTotalItemInfo> listOrderTotal = info.getOrderTotalItemInfo();

        Map<Integer, Object> map = new HashMap<Integer, Object>();

        for (OrderTotalItemInfo itemInfo : listOrderTotal) {

            switch (itemInfo.getPayTypeId()) {

                case 1:
                    map.put(0, itemInfo);
                    break;
                case 2:
                    map.put(1, itemInfo);
                    break;

                case 4:
                    map.put(2, itemInfo);
                    break;
                case 12:
                    map.put(3, itemInfo);
                    break;
                default:
                    map.put(itemInfo.getPayTypeId(), itemInfo);
                    break;
            }

        }

        for (Integer key : map.keySet()) {
            OrderTotalItemInfo itemInfo = (OrderTotalItemInfo) map.get(key);
            if (MainApplication.feeFh.equalsIgnoreCase("¥")) {
                list.add(new Printable.KeyValue(MainApplication.getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId())) + ToastHelper.toStr(R.string.tx_money),
                        DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + ToastHelper.toStr(R.string.pay_yuan)));
            } else {
                list.add(new Printable.Strings(new String[]{MainApplication.getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId()))+""}, Printing.ALIGN_START ));
                list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.tx_money), MainApplication.getFeeType() + DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) ));
            }
            list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.tv_settle_count) + "：", itemInfo.getSuccessCount()+""));
        }
        list.add(new Printable.Lines(1, '-'));
        list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis())}, Printing.ALIGN_START ));
//        list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.tx_blue_print_sign) + "："}, Printing.ALIGN_START ));
        list.add(new Printable.Lines(1));
        list.add(new Printable.Lines(1));

        return new Printable(list.toArray(new Printable.Step[list.size()]));
    }


    public static Printable creatOrderInfoPrintable(boolean isCashierShow, Order orderModel) {
        List<Printable.Step> list = new ArrayList<>();
        if (!orderModel.isPay()) {//退款
            list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.tx_blue_print_refund_note)}, Printing.ALIGN_CENTER |Printing.SIZE_MEDIUM));
        } else {
            list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.tx_blue_print_pay_note)}, Printing.ALIGN_CENTER |Printing.SIZE_MEDIUM));
        }
        list.add(new Printable.Lines(1));
        list.add(new Printable.Strings(new String[]{orderModel.getPartner()}, Printing.ALIGN_START ));
        list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.tv_pay_client_save)}, Printing.AUTO_WRAP|Printing.ALIGN_START ));
        list.add(new Printable.Lines(1, '='));

        list.add(new Printable.Strings(new String[]{ ToastHelper.toStr(R.string.shop_name) + "："}, Printing.ALIGN_START ));
        list.add(new Printable.Strings(new String[]{ MainApplication.getMchName()}, Printing.ALIGN_START ));
        list.add(new Printable.Strings(new String[]{ ToastHelper.toStr( R.string.tx_blue_print_merchant_no) + "：" + MainApplication.getMchId()}, Printing.ALIGN_START ));

        if (!orderModel.isPay()) {//退款
            list.add(new Printable.Strings(new String[]{ ToastHelper.toStr(R.string.tx_blue_print_refund_time) + "："}, Printing.ALIGN_START ));
            list.add(new Printable.Strings(new String[]{orderModel.getAddTimeNew()}, Printing.ALIGN_START ));
            list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.refund_odd_numbers) + ":"}, Printing.ALIGN_START ));
            list.add(new Printable.Strings(new String[]{ orderModel.getRefundNo()}, Printing.ALIGN_START ));
        } else {
            if (isCashierShow){
                list.add(new Printable.Strings(new String[]{ ToastHelper.toStr(R.string.tx_user) + "："}, Printing.ALIGN_START ));
                list.add(new Printable.Strings(new String[]{  orderModel.getUserName()}, Printing.ALIGN_START ));
            }
            list.add(new Printable.Strings(new String[]{ ToastHelper.toStr(R.string.tx_bill_stream_time) + "："}, Printing.ALIGN_START ));
            list.add(new Printable.Strings(new String[]{  orderModel.getAddTimeNew()}, Printing.ALIGN_START ));

        }
        list.add(new Printable.Strings(new String[]{ ToastHelper.toStr(R.string.tx_order_no) + "："}, Printing.ALIGN_START ));
        list.add(new Printable.Strings(new String[]{  orderModel.getOrderNoMch()}, Printing.ALIGN_START ));


        if (orderModel.isPay()) {
            if (MainApplication.getPayTypeMap() != null && MainApplication.getPayTypeMap().size() > 0) {
                list.add(new Printable.Strings(new String[]{  MainApplication.getPayTypeMap().get(orderModel.getApiCode())+ " " + ToastHelper.toStr(R.string.tx_orderno) + ":"}, Printing.ALIGN_START ));
            }
            list.add(new Printable.Strings(new String[]{ orderModel.getTransactionId()}, Printing.ALIGN_START ));
            list.add(new Printable.Strings(new String[]{ ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "："}, Printing.ALIGN_START ));
            list.add(new Printable.Strings(new String[]{  orderModel.getTradeName()}, Printing.ALIGN_START ));


            list.add(new Printable.Strings(new String[]{   ToastHelper.toStr(R.string.tx_bill_stream_statr) + "："}, Printing.ALIGN_START ));
            list.add(new Printable.Strings(new String[]{  MainApplication.getTradeTypeMap().get(orderModel.getTradeState() + "")}, Printing.ALIGN_START ));

            if (!StringUtil.isEmptyOrNull(orderModel.getAttach())) {
                list.add(new Printable.Strings(new String[]{ ToastHelper.toStr(R.string.tx_bill_stream_attach) + "：" + orderModel.getAttach()}, Printing.ALIGN_START ));
            }

            if (MainApplication.feeFh.equalsIgnoreCase("¥")) {
                if (orderModel.getDaMoney() > 0) {
                    long actualMoney = orderModel.getDaMoney() + orderModel.getOrderFee();
                    list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.tx_blue_print_money) + "：",
                            DateUtil.formatRMBMoneyUtils(actualMoney) + ToastHelper.toStr(R.string.pay_yuan)));
                } else {
                    list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.tx_blue_print_money) + "：" ,
                            DateUtil.formatRMBMoneyUtils(orderModel.getOrderFee()) + ToastHelper.toStr(R.string.pay_yuan)));
                }
            } else {

                if (MainApplication.isSurchargeOpen()) {
                    if(!orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") &&  !orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")){

                        list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.tx_blue_print_money) + "：", MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getOrderFee())));
                        list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.tx_surcharge) + "：", MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getSurcharge())));
                    }

                }

                list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.tv_charge_total) + "：", MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee())));

                if (orderModel.getCashFeel() > 0) {
                    list.add(new Printable.KeyValue("", ToastHelper.toStr(R.string.pay_yuan)+ DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel())));
                }
            }
        } else {
            list.add(new Printable.Strings(new String[]{ ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "："}, Printing.ALIGN_START ));
            list.add(new Printable.Strings(new String[]{ orderModel.getTradeName()}, Printing.ALIGN_START ));

            if (!StringUtil.isEmptyOrNull(orderModel.getUserName())) {
                list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.tv_refund_peop) + "："}, Printing.ALIGN_START ));
                list.add(new Printable.Strings(new String[]{orderModel.getUserName()}, Printing.ALIGN_START ));
            }
            if (MainApplication.getRefundStateMap() != null && MainApplication.getRefundStateMap().size() > 0) {
                list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.tv_refund_state) + "："}, Printing.ALIGN_START ));
                list.add(new Printable.Strings(new String[]{MainApplication.getRefundStateMap().get(orderModel.getRefundState() + "")}, Printing.ALIGN_START ));
            }
            if (MainApplication.feeFh.equalsIgnoreCase("¥")) {

                list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.tx_blue_print_money) + "：", DateUtil.formatRMBMoneyUtils(orderModel.getTotalFee()) + ToastHelper.toStr(R.string.pay_yuan)));
                list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "：", DateUtil.formatRMBMoneyUtils(orderModel.getRefundMoney()) + ToastHelper.toStr(R.string.pay_yuan)));

            } else {
                list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.tv_charge_total) + "：", MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee())));
                list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "：", MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getRefundMoney())));

                if(orderModel.getCashFeel() > 0){
                    list.add(new Printable.KeyValue("", ToastHelper.toStr(R.string.pay_yuan)+ DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel())));
                }
            }
            if (!StringUtil.isEmptyOrNull(orderModel.getPrintInfo())) {
                list.add(new Printable.Strings(new String[]{orderModel.getPrintInfo()}, Printing.AUTO_WRAP|Printing.ALIGN_START ));
            }
        }

        list.add(new Printable.Lines(1));

        if(!StringUtil.isEmptyOrNull(orderModel.getOrderNoMch()) && orderModel.isPay()){
            list.add(new Printable.Strings(new String[]{orderModel.getOrderNoMch()}, Printing.CODE_QR | Printing.ALIGN_CENTER));

            list.add(new Printable.Strings(new String[]{ ToastHelper.toStr(R.string.refound_QR_code) }, Printing.ALIGN_CENTER ));
            list.add(new Printable.Strings(new String[]{ ToastHelper.toStr(R.string.refound_QR_code_thanks) }, Printing.ALIGN_CENTER ));
        }

        list.add(new Printable.Lines(1, '-'));
        list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis())}, Printing.ALIGN_START ));

      /*  if (orderModel.getPartner().equalsIgnoreCase(ToastHelper.toStr(R.string.tv_pay_mch_stub))
                || orderModel.getPartner().equalsIgnoreCase(ToastHelper.toStr(R.string.tv_pay_user_stub))) {
            list.add(new Printable.Strings(new String[]{ ToastHelper.toStr(R.string.tx_blue_print_sign) + "："}, Printing.ALIGN_START ));
        }*/

        list.add(new Printable.Lines(1));
        list.add(new Printable.Lines(1));


        return new Printable(list.toArray(new Printable.Step[list.size()]));

    }


    /**
     * printType参数1：预授权成功---预授权小票
     * printType参数4：预授权订单详情---预授权小票
     * printType参数2：预授权解冻详情小票
     * printType参数3：预授权解冻完成小票
     */
    public static Printable CreatePreAutPrintable(int printType, Order orderModel){
        int minus=1;
        //根据当前的小数点的位数来判断应该除以或者乘以多少
        for(int i = 0; i < MainApplication.numFixed ; i++ ){
            minus = minus * 10;
        }
        List<Printable.Step> list = new ArrayList<>();
        switch (printType){
            case 1:
            case 4:
            case 5:
                list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.pre_auth_receipt)}, Printing.ALIGN_CENTER |Printing.SIZE_MEDIUM));
                list.add(new Printable.Lines(1));
                list.add(new Printable.Strings(new String[]{  orderModel.getPartner()}, Printing.ALIGN_START ));
                list.add(new Printable.Strings(new String[]{ ToastHelper.toStr(R.string.tv_pay_client_save)}, Printing.ALIGN_START ));
                break;

            case 2:
            case 3:
                list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.pre_auth_unfreezed_receipt)}, Printing.ALIGN_CENTER |Printing.SIZE_MEDIUM));
                list.add(new Printable.Lines(1));
                list.add(new Printable.Strings(new String[]{  orderModel.getPartner()}, Printing.ALIGN_START ));
                list.add(new Printable.Strings(new String[]{ ToastHelper.toStr(R.string.tv_pay_client_save)}, Printing.ALIGN_START ));
                break;
        }
        list.add(new Printable.Lines(1, '='));
        list.add(new Printable.Strings(new String[]{  ToastHelper.toStr(R.string.shop_name) + "："}, Printing.ALIGN_START ));
        list.add(new Printable.Strings(new String[]{  MainApplication.getMchName()}, Printing.ALIGN_START ));

        list.add(new Printable.Strings(new String[]{  ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "："}, Printing.ALIGN_START ));
        list.add(new Printable.Strings(new String[]{ MainApplication.getMchId()}, Printing.ALIGN_START ));

        if (printType==1) {
            list.add(new Printable.Strings(new String[]{  ToastHelper.toStr(R.string.tx_bill_stream_time) + "："}, Printing.ALIGN_START ));
            list.add(new Printable.Strings(new String[]{ orderModel.getTradeTime()}, Printing.ALIGN_START ));
        }else if(printType==4){
            list.add(new Printable.Strings(new String[]{  ToastHelper.toStr(R.string.tx_bill_stream_time) + "："}, Printing.ALIGN_START ));
            list.add(new Printable.Strings(new String[]{ orderModel.getTradeTimeNew()}, Printing.ALIGN_START ));
        }else if(printType==5){
            list.add(new Printable.Strings(new String[]{  ToastHelper.toStr(R.string.tx_bill_stream_time) + "："}, Printing.ALIGN_START ));
            list.add(new Printable.Strings(new String[]{ orderModel.getTimeEnd()}, Printing.ALIGN_START ));
        } else if(printType==2){
            list.add(new Printable.Strings(new String[]{  ToastHelper.toStr(R.string.unfreezed_time) + "："}, Printing.ALIGN_START ));
            list.add(new Printable.Strings(new String[]{ orderModel.getOperateTime()}, Printing.ALIGN_START ));
        }else if(printType==3){
            list.add(new Printable.Strings(new String[]{  ToastHelper.toStr(R.string.unfreezed_time) + "："}, Printing.ALIGN_START ));
            list.add(new Printable.Strings(new String[]{ orderModel.getUnFreezeTime()}, Printing.ALIGN_START ));
        }

        if (printType==1 || printType == 4 || printType == 5){
            list.add(new Printable.Strings(new String[]{  ToastHelper.toStr(R.string.platform_pre_auth_order_id) + "："}, Printing.ALIGN_START ));
            list.add(new Printable.Strings(new String[]{ orderModel.getAuthNo()}, Printing.ALIGN_START ));


            String language = PreferenceUtil.getString("language","");
            Locale locale = MainApplication.getContext().getResources().getConfiguration().locale; //zh-rHK
            String lan = Locale.getDefault().toString();
            if (!TextUtils.isEmpty(language)) {
                if (language.equals(MainApplication.LANG_CODE_EN_US)) {
                    list.add(new Printable.Strings(new String[]{  MainApplication.getPayTypeMap().get("2") + " "+ ToastHelper.toStr(R.string.tx_orderno) + "："}, Printing.ALIGN_START ));
                } else {
                    list.add(new Printable.Strings(new String[]{MainApplication.getPayTypeMap().get("2") + ToastHelper.toStr(R.string.tx_orderno) + "："}, Printing.ALIGN_START ));
                }
            } else {
                if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_EN_US)) {
                    list.add(new Printable.Strings(new String[]{ MainApplication.getPayTypeMap().get("2") + " "+ ToastHelper.toStr(R.string.tx_orderno) + "："}, Printing.ALIGN_START ));
                } else {
                    list.add(new Printable.Strings(new String[]{  MainApplication.getPayTypeMap().get("2") + ToastHelper.toStr(R.string.tx_orderno) + "："}, Printing.ALIGN_START ));
                }
            }

            if(printType==1 || printType == 5){
                list.add(new Printable.Strings(new String[]{orderModel.getOutTransactionId()}, Printing.ALIGN_START ));
            }

            if(printType== 4){
                list.add(new Printable.Strings(new String[]{orderModel.getTransactionId()}, Printing.ALIGN_START ));
            }

        }else if(printType == 2 || printType== 3){
            list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.unfreezed_order_id) + "："}, Printing.ALIGN_START ));
            list.add(new Printable.Strings(new String[]{orderModel.getOutRequestNo()}, Printing.ALIGN_START ));
            list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.platform_pre_auth_order_id) + "："}, Printing.ALIGN_START ));
            list.add(new Printable.Strings(new String[]{orderModel.getAuthNo()}, Printing.ALIGN_START ));
        }

        if(printType == 5){
            String tradename;
            if (!isAbsoluteNullStr(orderModel.getTradeName())){//先判断大写的Name，再判断小写name
                tradename=orderModel.getTradeName();
            }else {
                tradename=orderModel.getTradename();
            }
            list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "："}, Printing.ALIGN_START ));
            list.add(new Printable.Strings(new String[]{tradename}, Printing.ALIGN_START ));

        }else{
            list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "："}, Printing.ALIGN_START ));
            list.add(new Printable.Strings(new String[]{orderModel.getTradeName()}, Printing.ALIGN_START ));
        }


        if (printType == 3) {
            list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.tv_refund_peop) + "："}, Printing.ALIGN_START ));
            list.add(new Printable.Strings(new String[]{orderModel.getUserName()}, Printing.ALIGN_START ));
        }

        list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.tx_bill_stream_statr) + "："}, Printing.ALIGN_START ));

        String operationType = "";
        if(printType == 1 || printType == 4){
            operationType = MainApplication.getPreAuthtradeStateMap().get(orderModel.getTradeState() + "");

        }else if(printType == 2){
            switch (orderModel.getOperationType()){
                case 2:
                    operationType = ToastHelper.toStr(R.string.freezen_success);
                    break;
            }
        }else if(printType == 3){
            operationType = ToastHelper.toStr(R.string.unfreezing);
        }else if (printType == 5){
            operationType = ToastHelper.toStr(R.string.pre_auth_authorized);
        }
        list.add(new Printable.Strings(new String[]{operationType}, Printing.ALIGN_START ));

        switch (printType){
            case 1:
            case 4:
            case 5:
                if (MainApplication.feeFh.equalsIgnoreCase("¥")){
                    list.add(new Printable.KeyValue((ToastHelper.toStr(R.string.pre_auth_amount)+":"),
                            DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan)));
                }else{
                    long preAuthorizationMoney=0;//用来接收printType判断授权金额来自反扫、正扫
                    if (printType==1){
                        preAuthorizationMoney=orderModel.getRestAmount();//反扫
                    }else if (printType==5){
                        preAuthorizationMoney=orderModel.getTotalFee();//正扫
                    }
                    list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.pre_auth_amount)+":",
                            MainApplication.feeType + DateUtil.formatMoneyUtils(preAuthorizationMoney)));
                }

                list.add(new Printable.Lines(1));

                list.add(new Printable.Strings(new String[]{orderModel.getAuthNo()}, Printing.CODE_QR | Printing.ALIGN_CENTER));

                list.add(new Printable.Strings(new String[]{ ToastHelper.toStr(R.string.refound_QR_code) + "："}, Printing.ALIGN_CENTER ));
                list.add(new Printable.Strings(new String[]{ ToastHelper.toStr(R.string.refound_QR_code_thanks) + "："}, Printing.ALIGN_CENTER ));

                list.add(new Printable.Lines(1));
                list.add(new Printable.Lines(1, '-'));
                list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis())}, Printing.ALIGN_START ));
//                list.add(new Printable.Strings(new String[]{ ToastHelper.toStr(R.string.tx_blue_print_sign) + "："}, Printing.ALIGN_START ));
                list.add(new Printable.Lines(1));
                break;
            case 2:
                if (MainApplication.feeFh.equalsIgnoreCase("¥")){
                    list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.unfreezing_amount)+":",
                            DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan)));

                }else{

                    list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.unfreezing_amount)+":",
                            MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getMoney())));
                }

                list.add(new Printable.Strings(new String[]{ ToastHelper.toStr(R.string.unfreeze) }, Printing.ALIGN_START ));
                list.add(new Printable.Lines(1));

                list.add(new Printable.Lines(1));
                list.add(new Printable.Lines(1, '-'));
                list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis())}, Printing.ALIGN_START ));
//                list.add(new Printable.Strings(new String[]{ ToastHelper.toStr(R.string.tx_blue_print_sign) + "："}, Printing.ALIGN_START ));
                list.add(new Printable.Lines(1));
                break;
            case 3:
                //授权金额
                if (MainApplication.feeFh.equalsIgnoreCase("¥")){

                    list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.pre_auth_amount)+":",
                            DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan)));
                }else{
                    list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.pre_auth_amount)+":",
                            MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFreezeAmount())));
                }
                //解冻金额
                if (MainApplication.feeFh.equalsIgnoreCase("¥")){
                    list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.unfreezing_amount)+":",
                            DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan)));
                }else{
                    list.add(new Printable.KeyValue(ToastHelper.toStr(R.string.unfreezing_amount)+":",
                            MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee())));
                }
                list.add(new Printable.Strings(new String[]{ ToastHelper.toStr(R.string.unfreeze) }, Printing.ALIGN_START ));
                list.add(new Printable.Lines(1));

                list.add(new Printable.Lines(1));
                list.add(new Printable.Lines(1, '-'));
                list.add(new Printable.Strings(new String[]{ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis())}, Printing.ALIGN_START ));
//                list.add(new Printable.Strings(new String[]{ ToastHelper.toStr(R.string.tx_blue_print_sign) + "："}, Printing.ALIGN_START ));
                list.add(new Printable.Lines(1));

                break;
            default:
                break;
        }

        return new Printable(list.toArray(new Printable.Step[list.size()]));
    }

    public static boolean isAbsoluteNullStr(String str) {
        str = deleteBlank(str);
        if (str == null || str.length() == 0 || "null".equalsIgnoreCase(str)) {
            return true;
        }

        return false;
    }
    /**
     * 去前后空格和去换行符
     *
     * @param str
     * @return
     */
    public static String deleteBlank(String str) {
        if (null != str && 0 < str.trim().length()) {
            char[] array = str.toCharArray();
            int start = 0, end = array.length - 1;
            while (array[start] == ' ') start++;
            while (array[end] == ' ') end--;
            return str.substring(start, end + 1).replaceAll("\n", "");

        } else {
            return "";
        }

    }


    /**
     *将二维码保存到本地
     */
    // 二维码图片存放路径

    public static String saveViewBitmapFile(Bitmap bitmap) {
        File file;
        FileOutputStream out = null;
        try {
            File dir = new File(getSDcardPath() + "/swiftpass_pay", "receiptQrCode");
            if (!dir.exists()) {
                boolean isSuccess = dir.mkdirs();
                if (isSuccess) {

                } else {

                }
            }
            file = new File(dir, "Receipt_qrcode_image "+ ".jpg");
            out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
            MediaScannerConnection.scanFile(MainApplication.getContext(), new String[]{file.getPath()}, null, null);

            return file.getPath();
        } catch (FileNotFoundException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        } catch (IOException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ioe) {
                    Log.e(TAG, Log.getStackTraceString(ioe));
                }
            }
        }
        return null;
    }

    private static File dir;
    private static File file;
    private static Uri uriIntFileLogo;
    private static void prepareQRcodeImage() {
        dir =new File(getSDcardPath() + "/swiftpass_pay", "receiptQrCode");
        file = new File(dir, "Receipt_qrcode_image "+ ".jpg");
        // 判断是否是7.0
        if(Build.VERSION.SDK_INT  >= Build.VERSION_CODES.N){
            uriIntFileLogo = FileProvider.getUriForFile(getContext(), "cn.swiftpass.enterprise.intl.files", file);
        }else{
            uriIntFileLogo = Uri.fromFile(file);
        }

        //  Grant TESS app permission to access your picture file
        getContext().grantUriPermission("cn.swiftpass.enterprise", uriIntFileLogo, Intent.FLAG_GRANT_READ_URI_PERMISSION);
    }

}
