package cn.swiftpass.enterprise.bussiness.logica.ads;

import android.graphics.Bitmap;
import cn.swiftpass.enterprise.bussiness.logica.BaseManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.NotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.AdsInfo;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.io.database.access.AdsInfoDB;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.NetHelper;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.ImageUtil;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 广告管理
 * User: Administrator
 * Date: 13-10-25
 * Time: 下午4:10
 */
public class AdsManager  extends BaseManager{

    private  AdsManager(){}
    private static AdsManager instance;
    public static AdsManager getInstance()
    {
        if(instance == null){
            instance = new AdsManager();
        }
        return instance;
    }

    @Override
    public void init() {

    }

    @Override
    public void destory() {

    }

    public void save(final String ids)
    {
        ThreadHelper.executeWithCallback(new Executable<Void>() {
            @Override
            public Void execute() throws Exception {

                 return null;
            }
        },null);
    }


    public void loadAdsInfo(final UINotifyListener<String> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<String>() {
            @Override
            public String execute() throws Exception {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("clientType",ApiConstant.pad+"");
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT+ApiConstant.ADS_QUERY,jsonObject);
                if (!result.hasError()) {

                    String ids =  result.data.getString("ids");
                    String[] idstr = ids.split(",");
                    for(String  s: idstr)
                    {
                        AdsInfo adsInfo = new AdsInfo();
                        adsInfo.name = s;
                        AdsInfoDB.getInstance().save(adsInfo);
                    }
                    return "";

                }else
                {
                    listener.onError("对不起，数据错误");
                }
                return null;
            }
        },listener);
    }


    /**
     * 加载app 图片
     * ids
     * */
    public void loadAdsImg( final NotifyListener<List<Bitmap>> listener) {
        Executable<List<Bitmap>> executable = new Executable<List<Bitmap>>() {

            @Override
            public List<Bitmap> execute() throws Exception {
                try {
                    List<AdsInfo> adss = AdsInfoDB.getInstance().query();
                    if(adss == null){
                        return null;
                    }
                    List<Bitmap> bits = new ArrayList<Bitmap>();
                    for (AdsInfo ads : adss) {
                        Bitmap bitmap = null;
                        String imgFile = AppHelper.getImgCacheDir() + ads.name
                                + ".jpg";
                        File f = new File(imgFile);
                        if (f.exists() && f.length() != 0) {
                            bitmap = ImageUtil.decodeFile(f, false, 0);
                        } else {
                            bitmap = NetHelper.getBitmap(ApiConstant.DOWNLOAD_IMG_URL);
                            // 保存 ico
                            try {
                                imgFile = AppHelper.getImgCacheDir() + ads.name + ".jpg";
                                ImageUtil.saveSimpleImag(imgFile, bitmap, Bitmap.CompressFormat.JPEG);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        bits.add(bitmap);
                    }
                    callback(MSG_NOTIFY_ON_SUCCEED, listener, bits);
                    return bits;
                } catch (Exception e1) {
                    e1.printStackTrace();
                    callback(MSG_NOTIFY_ON_ERROR, listener, "read image error");
                }

                return null;
            }

        };
        ThreadHelper.executeWithCallback(executable, listener);

    }
    /** 加载app 图片 */
    public Bitmap loadAppImage(final long icoId,final NotifyListener<Bitmap> listener) {
            return null;
    }
}
