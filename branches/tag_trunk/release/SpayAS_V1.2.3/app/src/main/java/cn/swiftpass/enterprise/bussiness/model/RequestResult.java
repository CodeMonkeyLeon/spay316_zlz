package cn.swiftpass.enterprise.bussiness.model;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.utils.Utils;

public class RequestResult
{
    /**
     * 请求结果
     */
    public JSONObject data;
    
    public JSONArray arr;
    
    /**
     * 请求结果号
     */
    public int resultCode;
    
    /**
     * 服务器返回消息
     */
    private String message;
    
    /**
     * 其它数据
     */
    public Object tag;
    
    /**
     * 默认无错误
     */
    public RequestResult()
    {
        resultCode = RESULT_NO_ERROR;
    }
    
    /**
     * 设置消息
     * @param message
     */
    public void setMessage(String message)
    {
        this.message = message;
    }
    
    /**
     * 是否有错误，不为RESULT_NO_ERROR的结果都算作错误
     * @return
     */
    public boolean hasError()
    {
        if (uiNotifyListener != null)
        {
            switch (resultCode)
            {
            //                case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
            //                    uiNotifyListener.onError("网络连接不可用，请检查你网络连接!");
            //                    break;
            //                case RequestResult.RESULT_TIMEOUT_ERROR:
            //                    uiNotifyListener.onError("请求连接超时，请稍候再试!");
            //                    break;
            //                case RequestResult.RESULT_READING_ERROR:
            //                    uiNotifyListener.onError("请求服务连接超时，请稍候再试!");
            //                    break;
                case RequestResult.RESULT_NEED_LOGIN:
                    try
                    {
                        String message = MainApplication.getContext().getString(R.string.msg_need_login);
                        uiNotifyListener.onError(message);
                    }
                    catch (Exception e)
                    {
                    }
                    break;
            //                default:
            //                    if (resultCode != RESULT_NO_ERROR)
            //                    {
            //                        uiNotifyListener.onError(MainApplication.getContext().getString(R.string.unknow_error)
            //                            + " code: " + resultCode);
            //                    }
            //                    break;
            
            }
        }
        
        return resultCode != RESULT_NO_ERROR || !TextUtils.isEmpty(message);
    }
    
    public static boolean hasError(int resultCode)
    {
        return resultCode != RESULT_NO_ERROR;
    }
    
    public void returnErrorMessage()
        throws JSONException
    {
        if (uiNotifyListener != null)
        {
            String message = data.getString("message");
            if (!TextUtils.isEmpty(message))
            {
                uiNotifyListener.onError(message);
            }
            else
            {
                int res = Utils.Integer.tryParse(data.getString("result"), -1);
                uiNotifyListener.onError(MainApplication.getContext().getString(R.string.unknow_error) + " code: "
                    + res);
            }
        }
    }
    
    /**
     * 服务器返回的错误通常是不可重试的
     * 而系统错误允许重试
     * @return
     */
    public boolean isRetryable()
    {
        return resultCode < 0;
    }
    
    /**
     * 如果预设的系统错误里可以找到描述，则直接返回
     * 否则，表示是一个服务器返回的错误
     * @return
     */
    public String getMessage()
    {
        if (!TextUtils.isEmpty(message))
        {
            return message;
        }
        else
        {
            return errorMap.get(resultCode);
        }
    }
    
    /**
     * 没有错误
     */
    public static final int RESULT_NO_ERROR = 0;
    
    /**
     * 网络不可用
     */
    public static final int RESULT_BAD_NETWORK = -1;
    
    /**
     * 内部错误
     */
    public static final int RESULT_INTERNAL_ERROR = -2;
    
    /**
     * 无法链接服务器，服务器内部错误！
     */
    public static final int RESULT_READING_ERROR = -3;
    
    /**
     * 读取数据超时
     */
    public static final int RESULT_TIMEOUT_ERROR = -4;
    
    /**
     * 服务器错误
     */
    public static final int RESULT_SERVICE_ERROR = -5;
    
    /**
     * 验证数据失败
     */
    public static final int RESULT_CHANK_DATA_ERROR = -6;
    
    //////////////////////////////////////////注册错误
    /**用户名不存在*/
    public static final int RESULT_USERNAME_EXIST = 460;
    
    /**密码错误*/
    public static final int RESULT_RECOMMEND_NOT_EXIST = 461;
    
    /**用户被冻结*/
    public static final int RESULT_INVALID_RECOMMEND = 462;
    
    /////////////////////////////////////软件评论
    public static final int RESULT_SOFT_COMMENT = 460;
    
    /**
     * 会话已过期
     */
    public static final int RESULT_NEED_LOGIN = 401;
    
    public static Map<Integer, String> errorMap;
    
    private UINotifyListener uiNotifyListener;
    
    public void setNotifyListener(UINotifyListener uiNotifyListener)
    {
        this.uiNotifyListener = uiNotifyListener;
    }
    
    static
    {
        errorMap = new HashMap<Integer, String>();
        
        // 系统错误
        errorMap.put(RESULT_BAD_NETWORK, "网络不可用");
        errorMap.put(RESULT_INTERNAL_ERROR, "发生未知错误");
        errorMap.put(RESULT_READING_ERROR, "网络繁忙，请稍候再试！");
        errorMap.put(RESULT_TIMEOUT_ERROR, "读取数据超时");
        errorMap.put(RESULT_SERVICE_ERROR, "网络连接错误");
        errorMap.put(RESULT_CHANK_DATA_ERROR, "验证数据失败");
        
        // 注册错误
        errorMap.put(RESULT_USERNAME_EXIST, "用户名已经存在");
        errorMap.put(RESULT_RECOMMEND_NOT_EXIST, "推荐人不存在");
        errorMap.put(RESULT_INVALID_RECOMMEND, "推荐人不能推荐了");
        //软件评论
        errorMap.put(RESULT_SOFT_COMMENT, "应用不存在");
        
    }
    
    public static String getErrorMsg(int resultCode)
    {
        return errorMap.get(resultCode);
    }
}
