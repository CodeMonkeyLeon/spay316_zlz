package cn.swiftpass.enterprise.ui.widget;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/***
 * 退款检查对话框
 */
public class ScanCodeCheckDialog extends Dialog implements View.OnClickListener {
    private static final String TAG = ScanCodeCheckDialog.class.getSimpleName();
    //private ViewGroup mRootView;

    private ButtonM btnOk;

    private Button btnCancel;

    private ConfirmListener btnListener;

    private EditText etPwd;

    private TextView tvUserName, tv_order_no, tv_order_tx, tv_money_tx;

    private Activity mContext;

    //private UserModel userModel;

    public static final int REFUND = 0;

    public static final int PAY = 1;

    public static final int REVERS = 2; // 冲正

    private LinearLayout ly_order, ly_money, lay_pass;

    private int flag;

    /**
     * 按钮自动设置颜色背景
     * <功能详细描述>
     *
     * @param b
     * @see [类、类#方法、类#成员]
     */
    private void setButBg(ButtonM b) {
        //定制化版本 初始化
        DynModel dynModel = (DynModel) SharedPreUtile.readProduct("dynModel" + ApiConstant.bankCode);
        if (null != dynModel) {
            try {
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonColor())) {
                    //设置是否为圆角
                    b.setFillet(true);
                    //设置圆角的半径大小
                    b.setRadius(38);
                    b.setBackColor(Color.parseColor(dynModel.getButtonColor()));
                    b.setTextColors(ToastHelper.toStr(R.color.white));

                } else {
                    if (b.getId() == R.id.ok) {
                        b.setBackGroundImage(R.drawable.btn_scan);
                    } else {
                        b.setBackGroundImage(R.drawable.btn_scan_cancle);
                    }

                }
                b.setTextColors(ToastHelper.toStr(R.color.white));
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonFontColor())) {
                    b.setTextColors(dynModel.getButtonFontColor());
                }

            } catch (Exception e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }

        }
    }

    public ScanCodeCheckDialog(Activity context, int flag, String title, String type, String tv_moneyStr, String order_no, String money, ConfirmListener btnListener) {
        super(context);
        mContext = context;
        this.btnListener = btnListener;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.scan_code_dialog);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        this.setCanceledOnTouchOutside(false);
        this.flag = flag;
        TextView tvTitle = (TextView) findViewById(R.id.title);
        if (title != null) {
            tvTitle.setText(title);
        }
        etPwd = (EditText) findViewById(R.id.edit_input);
        etPwd.setVisibility(View.GONE);
        tvUserName = (TextView) findViewById(R.id.tv_userName);
        //userModel = LocalAccountManager.getInstance().getLoggedUser();
        btnOk = (ButtonM) findViewById(R.id.ok);
        btnCancel = (ButtonM) findViewById(R.id.cancel);
        setButBg(btnOk);
        //        setButBg(btnCancel);
        tv_order_no = (TextView) findViewById(R.id.tv_order_no);
        tv_money_tx = (TextView) findViewById(R.id.tv_money_tx);
        lay_pass = (LinearLayout) findViewById(R.id.lay_pass);
        lay_pass.setVisibility(View.GONE);
        if (type != null) {
            if (type.equals(MainApplication.PAY_TYPE_WRITE_OFF)) // 扫码卡券
            {
                etPwd.setHint(R.string.pay_scan_input_kj);
            }
            if (type.equals(MainApplication.PAY_TYPE_REFUND)) {
                etPwd.setHint(R.string.pay_scan_input_refund);
            }

        }

        if (null == order_no) {
            tv_order_no.setVisibility(View.GONE);
            tv_order_tx = (TextView) findViewById(R.id.tv_order_tx);
            tv_order_tx.setVisibility(View.GONE);
            ly_order = (LinearLayout) findViewById(R.id.ly_order);
            ly_order.setVisibility(View.GONE);
        } else {
            tv_order_no.setText(order_no);
            tv_order_no.setVisibility(View.VISIBLE);
        }

        if (money == null) {
            ly_money = (LinearLayout) findViewById(R.id.ly_money);
            ly_money.setVisibility(View.GONE);
            tvUserName.setVisibility(View.GONE);
            tv_money_tx.setVisibility(View.GONE);
        } else {
            tvUserName.setVisibility(View.VISIBLE);
            tvUserName.setText("¥ " + DateUtil.formatMoneyUtil(Double.parseDouble(money) / 100d));
            tv_money_tx.setText(tv_moneyStr);
        }
        btnOk.setText(context.getString(R.string.revers_tx));
        btnCancel.setText(context.getString(R.string.query_tx));
        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    public ScanCodeCheckDialog(Activity context, int flag, String title, String type, String order_no, String money, ConfirmListener btnListener) {
        super(context);
        mContext = context;
        this.btnListener = btnListener;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.scan_code_dialog);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        this.setCanceledOnTouchOutside(false);
        this.flag = flag;
        //TextView tvTitle = (TextView) findViewById(R.id.title);
        etPwd = (EditText) findViewById(R.id.edit_input);
        if (title != null) {
            etPwd.setHint(title);
        }
        tvUserName = (TextView) findViewById(R.id.tv_userName);
        //userModel = LocalAccountManager.getInstance().getLoggedUser();
        btnOk = (ButtonM) findViewById(R.id.ok);
        btnCancel = (ButtonM) findViewById(R.id.cancel);
        setButBg(btnOk);
        //        setButBg(btnCancel);
        tv_order_no = (TextView) findViewById(R.id.tv_order_no);

        if (null == order_no) {
            tv_order_no.setVisibility(View.GONE);
            tv_order_tx = (TextView) findViewById(R.id.tv_order_tx);
            tv_order_tx.setVisibility(View.GONE);
            ly_order = (LinearLayout) findViewById(R.id.ly_order);
            ly_order.setVisibility(View.GONE);
        } else {
            tv_order_no.setText(order_no);
            tv_order_no.setVisibility(View.VISIBLE);
        }

        if (type != null) {
            if (type.equals(MainApplication.PAY_TYPE_WRITE_OFF)) // 扫码卡券
            {
                etPwd.setHint(R.string.pay_scan_input_kj);
            }
            if (type.equals(MainApplication.PAY_TYPE_REFUND)) {
                etPwd.setHint(R.string.pay_scan_input_refund);
            }

        }

        if (money == null) {
            ly_money = (LinearLayout) findViewById(R.id.ly_money);
            ly_money.setVisibility(View.GONE);
            tvUserName.setVisibility(View.GONE);
            tv_money_tx = (TextView) findViewById(R.id.tv_money_tx);
            tv_money_tx.setVisibility(View.GONE);
        } else {
            tvUserName.setVisibility(View.VISIBLE);
            tvUserName.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtil(Double.parseDouble(money) / 100d));
        }

        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    public void setInputType(int type) {
        if (etPwd != null) {
            etPwd.setInputType(type);
        }
    }

    //    public static void show(Activity context, String title, String order_no, String money, ConfirmListener btnListener)
    //    {
    //        RefundCheckDialog dia = new RefundCheckDialog(context, title, order_no, money, btnListener);
    //        dia.show();
    //    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ok:
                switch (flag) {
                    case ScanCodeCheckDialog.REFUND:
                        if (TextUtils.isEmpty(etPwd.getText())) {
                            MyToast toast = new MyToast();
                            toast.showToast(mContext, "请输入密码");
                        } else {
                            checkUser();
                        }
                        break;
                    case ScanCodeCheckDialog.PAY:
                        btnListener.ok(etPwd.getText().toString());
                        break;
                    case REVERS:

                        btnListener.ok("REVERS");
                        //                        dismiss();
                        break;

                }

                break;
            case R.id.cancel:
                if (btnListener != null) {
                    btnListener.cancel();
                    dismiss();
                }
                break;
            default:
                break;
        }

    }

    /**
     * 验证用户
     */
    private void checkUser() {
        String et_string = etPwd.getText().toString();
        LocalAccountManager.getInstance().loginAsync(null, MainApplication.getMchId(), MainApplication.getMchName(),et_string , AppHelper.getIMEI(), AppHelper.getIMSI(), false, new UINotifyListener<Boolean>() {
            @Override
            public void onError(final Object object) {
                super.onError(object);
                if (object != null) {
                    // ToastHelper.showInfo(object.toString());
                    mContext.runOnUiThread(new Runnable() {
                        public void run() {
                            MyToast myToast = new MyToast();
                            myToast.showToast(mContext, object.toString());
                        }
                    });
                }

            }

            @Override
            public void onSucceed(Boolean result) {
                super.onSucceed(result);
                if (result) {
                    if (btnListener != null) {
                        btnListener.ok(null);
                        dismiss();
                    }
                }/*else{
                        mContext.runOnUiThread(new Runnable() {
                            public void run() {
                                ToastHelper.showInfo("对不起，密码错误!");
                            }
                        });
                     }*/
            }

            @Override
            public void onPreExecute() {
                super.onPreExecute();
                showLoading("请稍候...");
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
                dismissMyLoading();
            }
        });
    }

    ProgressDialog loadingDialog;

    public void showLoading(String str) {
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(mContext);
            loadingDialog.setCancelable(true);

        }
        loadingDialog.show();
        loadingDialog.setMessage(str);
    }

    public void dismissMyLoading() {
        if (loadingDialog != null) {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }

    public interface ConfirmListener {
        public void ok(String code);

        public void cancel();
    }
}
