package cn.swiftpass.enterprise.ui.activity;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.enums.OrderStatusEnum;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.common.DataObserver;
import cn.swiftpass.enterprise.bussiness.logica.common.DataObserverManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.model.QRcodeInfo;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.AbstractBus;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.MD5;
import cn.swiftpass.enterprise.utils.MessageBus;
import cn.swiftpass.enterprise.utils.MessageBus.MMessage;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-8-20
 * Time: 下午3:32
 * 微信二维码
 */
public class WxPayActivity extends TemplateActivity
{
    
    private WebView wb;
    
    private Context context;
    
    private View v1;
    
    private LinearLayout v2;
    
    private ProgressDialog dialog;
    
    private String tag = "WxPayActivity";
    
    /** 循环检查 */
    public static final int MSG_START_CHECKIN = 0x8804;
    
    /** 停止检查*/
    public static final int MSG_STOP_CHECKIN = 0x8804;
    
    /** 订单状态 成功*/
    public static final int MSG_SUCCESS = 0x8801;
    
    /**订单状态 失败*/
    public static final int MSG_FAIL = 0x8802;
    
    /** 订单状态 未支付*/
    public static final int MSG_DEFAULT = 0x8803;
    
    private QRcodeInfo qrcodeInfo;
    
    @SuppressLint({"JavascriptInterface", "NewApi"})
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wxpay);
        Log.i(tag, "onCreate");
        v1 = findViewById(R.id.ll);
        v2 = getViewById(R.id.ll2);
        img_lastPager_left = (ImageView)findViewById(R.id.img_lastPager_left1);
        img_lastPager_right = (ImageView)findViewById(R.id.img_lastPager_right1);
        context = this;
        
        qrcodeInfo = (QRcodeInfo)getIntent().getSerializableExtra("qrcodeInfo");
        
        checkNetwork();
        Log.i(tag, "checkNetwork");
        wb = (WebView)findViewById(R.id.webview);
//        wb.addJavascriptInterface(new InJavaScriptLocalObj(), "local_obj");
        wb.getSettings().setJavaScriptEnabled(true);
        wb.getSettings().setAllowFileAccessFromFileURLs(false);
        wb.getSettings().setAllowUniversalAccessFromFileURLs(false);
        //api 17
        WebSettings settings = wb.getSettings();
        if (AppHelper.getAndroidSDKVersion() == 17)
        {
            settings.setDisplayZoomControls(false);
        }
        settings.setLoadWithOverviewMode(true);
        //settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        //各种分辨率适应
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int mDensity = metrics.densityDpi;
        if (mDensity == 120)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        }
        else if (mDensity == 160)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        }
        else if (mDensity == 240)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        }
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        
        wb.setWebChromeClient(new myWebChromeClien());
        wb.setWebViewClient(new MyWebViewClient());
        String url = createUrl();
        System.out.println("url:" + url);
        Log.i("hehui", "qrcodeInfo.uuId-->" + qrcodeInfo.uuId);
        wb.loadUrl(qrcodeInfo.uuId);
        init();
        initData();
        
        // bottomBar.setVisibility(View.GONE);
    }
    
    String name = "";
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(getStringById(R.string.qq_pay_type));
        try
        {
            name = LocalAccountManager.getInstance().getLoggedUser().merchantName;
            if (name != null)
            {
                titleBar.setTitle(name + "");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Logger.i(e);
        }
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightLayClick()
            {
                // TODO Auto-generated method stub
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                // TODO Auto-generated method stub
                
            }
        });
    }
    
    private Handler handler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            super.handleMessage(msg);
            switch (msg.what)
            {
                case MSG_START_CHECKIN:
                    OrderManager.getInstance().findOrderState(orderCode);
                    break;
            
            }
            
        }
    };
    
    private void initData()
    {
        MessageBus.getBusFactory().register(MSG_SUCCESS, receiver);
        MessageBus.getBusFactory().register(MSG_FAIL, receiver);
        MessageBus.getBusFactory().register(MSG_DEFAULT, receiver);
    }
    
    /** 消息到达接收器 */
    AbstractBus.Receiver<MessageBus.MMessage> receiver = new MessageBus.UIReceiver()
    {
        private static final long serialVersionUID = 8434181217182948405L;
        
        @Override
        public void onReceive(MMessage message)
        {
            switch (message.what)
            {
                case MSG_SUCCESS:
                    titleBar.setTitle(getStringById(R.string.order_status)
                        + OrderStatusEnum.PAY_SUCCESS.getDisplayName());
                    Logger.d("订单状态：" + OrderStatusEnum.PAY_SUCCESS.getDisplayName());
                    handler.removeMessages(WxPayActivity.MSG_START_CHECKIN);
                    titleBar.setRightLodingVisible(false);
                    OrderManager.getInstance().updateOrderState(orderCode, OrderStatusEnum.PAY_SUCCESS);
                    break;
                case MSG_FAIL:
                    titleBar.setTitle(getStringById(R.string.order_status) + OrderStatusEnum.PAY_FAILL.getDisplayName());
                    Logger.d("订单状态：" + OrderStatusEnum.PAY_FAILL.getDisplayName());
                    handler.removeMessages(WxPayActivity.MSG_START_CHECKIN);
                    titleBar.setRightLodingVisible(false);
                    OrderManager.getInstance().updateOrderState(orderCode, OrderStatusEnum.PAY_FAILL);
                    break;
                case MSG_DEFAULT:
                    titleBar.setTitle(getStringById(R.string.order_status) + OrderStatusEnum.NO_PAY.getDisplayName());
                    Logger.d("订单状态：" + OrderStatusEnum.NO_PAY.getDisplayName());
                    handler.removeMessages(WxPayActivity.MSG_START_CHECKIN);
                    titleBar.setRightLodingVisible(false);
                    OrderManager.getInstance().updateOrderState(orderCode, OrderStatusEnum.NO_PAY);
                    break;
            }
        }
    };
    
    private void checkNetwork()
    {
        // 网络状态 通知
        DataObserverManager.getInstance().registerObserver(new DataObserver(WxPayActivity.class.getName(),
            DataObserver.EVENT_NETWORK_CHANGER_DESTORY)
        {
            
            @Override
            public void onChange()
            {
                ToastHelper.showError(getStringById(R.string.network_exception));
            }
        });
    }
    
    String orderCode, orderTime;
    
    private String createUrl()
    {
        String strTotalMoney = getIntent().getStringExtra("memoy");
        orderCode = getIntent().getStringExtra("orderCode");
        orderTime = getIntent().getStringExtra("orderTime");//必须保证这个时间
        //签名的时候不用编码 name +"订单号：" + orderCode
        String productName = name + getStringById(R.string.order_number) + orderCode;
        String signPara = executPara(productName, orderCode, strTotalMoney, false);
        String sign;
        if (ApiConstant.ISOVERSEASY)
        {
            sign = MD5.md5s(signPara + "&" + ApiConstant.P_KEY + "=" + ApiConstant.KEY_ISOVERSEASYURL).toUpperCase();//  ApiConstant.KEY 测试
        }
        else
        {
            sign = MD5.md5s(signPara + "&" + ApiConstant.P_KEY + "=" + ApiConstant.KEY).toUpperCase();
        }
        //url 特殊字符进行编码
        String para = executPara(productName, orderCode, strTotalMoney, true);
        if (ApiConstant.ISOVERSEASY)
        {
            return ApiConstant.CFT_BASE_PAY_URL_ISOVERSEASYURL + para + "&" + ApiConstant.P_SIGN + "=" + sign;
        }
        return ApiConstant.CFT_BASE_PAY_URL + para + "&" + ApiConstant.P_SIGN + "=" + sign;
    }
    
    public String executPara(String body, String tradeNo, String total, boolean isEncode)
    {
        String s = "";
        if (isEncode)
        {
            try
            {
                s = URLEncoder.encode(body, ApiConstant.INPUT_CHARTE);
            }
            catch (UnsupportedEncodingException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            s = body;
        }
        String para;
        if (ApiConstant.ISOVERSEASY)
        {
            para =
                "bank_type=WX&body=" + s + "&fee_type=" + ApiConstant.FEE_TYPE + "&input_charset="
                    + ApiConstant.INPUT_CHARTE + "&notify_url=" + ApiConstant.NOTIFY_URL + "&out_trade_no=" + tradeNo
                    + "&partner=" + ApiConstant.PARTNET_ISOVERSEASYURL + "&return_url=" + ApiConstant.RETURN_URL
                    + "&spbill_create_ip=" + ApiConstant.MY_IP + "&time_start=" + orderTime + "&total_fee=" + total;
        }
        else
        {
            para =
                "bank_type=WX&body=" + s + "&fee_type=1" + "&input_charset=" + ApiConstant.INPUT_CHARTE
                    + "&notify_url=" + ApiConstant.NOTIFY_URL + "&out_trade_no=" + tradeNo + "&partner="
                    + ApiConstant.PARTNET + "&return_url=" + ApiConstant.RETURN_URL + "&spbill_create_ip="
                    + ApiConstant.MY_IP + "&time_start=" + orderTime + "&total_fee=" + total;
        }
        return para;
    }
    
    final class InJavaScriptLocalObj
    {
        public void showSource(String html)
        {
            Logger.d("HTML", html);
        }
    }
    
    class myWebChromeClien extends WebChromeClient
    {
        @Override
        public void onProgressChanged(WebView view, int newProgress)
        {
            super.onProgressChanged(view, newProgress);
            Log.i(tag, "onProgressChanged");
            if (newProgress == 100)
            {
                Logger.d("WebView", "onProgressChanged newProgress:" + newProgress + " count " + count);
            }
        }
    }
    
    int count = 0;
    
    final class MyWebViewClient extends WebViewClient
    {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            // Logger.i(tag, "shouldOverrideUrlLoading url:"+url);
            view.loadUrl(url);
            return true;
        }
        
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            //Logger.i(tag, "onPageStarted url:" + url);
            super.onPageStarted(view, url, favicon);
            
            //            String msg = "请稍候";
            //            if (count <= 1)
            //            {
            //                msg = "正在请求手Q二维码，请稍候..";
            //            }
            //            else if (count > 3)
            //            {
            //                msg = "请稍候";
            //            }
            //            count += 1;
            //            dialog = ProgressDialog.show(context, null, msg);
            //            dialog.setCancelable(true);
        }
        
        public void onPageFinished(WebView view, String url)
        {
            // Log.i(tag,"onPageFinished url:"+url);
            //view.loadUrl("javascript:window.local_obj.showSource('<head>'+" +"document.getElementsByTagName('html')[0].innerHTML+'</head>');");
            super.onPageFinished(view, url);
            i = i + 1;
            if (i > 1)
            {
                //加载页面二维码成功才进行请求订单状态
                if (ApiConstant.isContentNetwork)
                {
                    OrderManager.getInstance().checkOrderState(handler);
                }
            }
            Logger.i(tag, "onPageFinished i = " + i);
            if (dialog != null)
                dialog.dismiss();
        }
        
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
        {
            super.onReceivedError(view, errorCode, description, failingUrl);
            // Log.i(tag,"onReceivedError description:"+description+" errorCode:"+errorCode+" failingUrl: "+failingUrl);
            if (dialog != null)
                dialog.dismiss();
            wb.loadUrl("file:///android_asset/error.htm");
            
        }
    }
    
    int i = 0;
    
    private ImageView img_lastPager_left;
    
    private ImageView img_lastPager_right;
    
    private Animation anim_left;
    
    private Animation anim_right;
    
    private void startAnim()
    {
        
        anim_left.start();
        anim_right.start();
    }
    
    private void init()
    {
        // 装载补间动画文件
        anim_left = AnimationUtils.loadAnimation(context, R.anim.translate_left);
        // 装载补间动画文件
        anim_right = AnimationUtils.loadAnimation(context, R.anim.translate_right);
        
        //设置动画结束后停留在结束状态
        anim_left.setFillEnabled(true);
        anim_left.setFillAfter(true);
        
        anim_right.setFillEnabled(true);
        anim_right.setFillAfter(true);
        startAnimImg();
        startAnim();
        
    }
    
    private void startAnimImg()
    {
        img_lastPager_left.setAnimation(anim_left);
        img_lastPager_right.setAnimation(anim_right);
        
        anim_left.setAnimationListener(new Animation.AnimationListener()
        {
            
            @Override
            public void onAnimationStart(Animation animation)
            {
            }
            
            @Override
            public void onAnimationRepeat(Animation animation)
            {
            }
            
            @Override
            public void onAnimationEnd(Animation animation)
            {
                v1.setVisibility(View.GONE);
                v2.setVisibility(View.VISIBLE);
            }
        });
    }
    
    public static void startActivity(QRcodeInfo qrcodeInfo, Context context)
    {
        Intent it = new Intent();
        //        it.setClass(context, WxPayActivity.class);
        //        //        it.putExtra("memoy", total);
        //        //        it.putExtra("orderCode", orderCode);
        //        //        it.putExtra("orderTime", orderTime);
        //        
        //        it.putExtra("qrcodeInfo", qrcodeInfo);
        //        it.setClass(context, ShowQRcodeActivity.class);
        //        context.startActivity(it);
        
        it.putExtra("qrcodeInfo", qrcodeInfo);
        it.setClass(context, WxPayActivity.class);
        context.startActivity(it);
        
        //        context.startActivity(it);
        
    }
    
    //每次交易成功后 请求下订单号
    public boolean onKeyDown(int keycode, KeyEvent event)
    {
        if (keycode == KeyEvent.KEYCODE_BACK)
        {
            if (handler != null)
            {
                handler.removeMessages(WxPayActivity.MSG_START_CHECKIN);
            }
        }
        return super.onKeyDown(keycode, event);
    }
    
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (wb != null)
        {
            v2.removeView(wb);
            wb.removeAllViews();
            wb.clearHistory();
            wb.destroy();
        }
    }
}
