/*
 * 文 件 名:  SharedPreUtils.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-9-2
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.utils;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import android.util.Log;

import com.example.common.sentry.SentryUtils;

import org.apache.commons.codec.binary.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;

import cn.swiftpass.enterprise.MainApplication;

/**
 * <一句话功能简述>
 *
 * @author he_hui
 * @version [版本号, 2016-9-2]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class SharedPreUtils {
    private static final String TAG = SharedPreUtils.class.getSimpleName();

    /**
     * 读取数据
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    public static Object readProduct(String key) {
        SharedPreferences preferences =
                MainApplication.getInstance().getSharedPreferences("base64",
                        MainApplication.getInstance().MODE_PRIVATE);
        String productBase64 = preferences.getString(key, "");
        if (TextUtils.isEmpty(productBase64)) {
            return null;
        }

        //读取字节
        byte[] base64 = Base64.decodeBase64(productBase64.getBytes());

        //封装到字节流
        ByteArrayInputStream bais = new ByteArrayInputStream(base64);
        SecureObjectInputStream bis = null;
        try {
            bis = new SecureObjectInputStream(bais);
            //再次封装
            try {
                //读取对象
                return bis.readObject();
            } catch (ClassNotFoundException e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                Log.e(TAG, Log.getStackTraceString(e));
            }
        } catch (StreamCorruptedException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
        } catch (IOException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
            if (bais != null) {
                try {
                    bais.close();
                } catch (IOException e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
        }

        return null;
    }

    /**
     * 保存对象
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    public static void saveObject(Object prodect, String key) {
        SharedPreferences preferences =
                MainApplication.getInstance().getSharedPreferences("base64",
                        MainApplication.getInstance().MODE_PRIVATE);

        //创建字节输出流
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = null;
        try {
            //创建对象输出流，并封装字节流
            oos = new ObjectOutputStream(baos);

            //将对象写入字节流
            oos.writeObject(prodect);

            //将字节流编码成base64的字符窜
            String productBase64 = new String(Base64.encodeBase64(baos.toByteArray())
                    , "UTF-8");

            Editor editor = preferences.edit();
            //            editor.putString(key, AESHelper.encrypt("SPAY", productBase64));

            editor.putString(key, productBase64);

            editor.commit();
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
        } finally {
            if (baos != null) {
                try {
                    baos.close();
                } catch (IOException e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    // TODO Auto-generated catch block
                    Log.e(TAG, Log.getStackTraceString(e));

                }
            }

            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    // TODO Auto-generated catch block
                    Log.e(TAG, Log.getStackTraceString(e));

                }
            }

        }

    }

    public static void removeKey(String key) {
        SharedPreferences preferences =
                MainApplication.getInstance().getSharedPreferences("base64",
                        MainApplication.getInstance().MODE_PRIVATE);
        preferences.edit().remove(key);
        preferences.edit().commit();
    }

}
