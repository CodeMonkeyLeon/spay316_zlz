package cn.swiftpass.enterprise.ui.widget;

import java.io.File;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.enums.CouponStatusEnum;
import cn.swiftpass.enterprise.bussiness.enums.CouponType;
import cn.swiftpass.enterprise.bussiness.model.CouponModel;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.widget.ImageCache.ImageLoader;
import cn.swiftpass.enterprise.ui.widget.ImageCache.ImageLoader.ImageCallback;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.ImageUtil;
import cn.swiftpass.enterprise.utils.Logger;

/**
 * 适配 User: Alan Date: 13-12-23 Time: 下午4:35 To change this template use File |
 * Settings | File Templates.
 */
public class ItemCoupon extends LinearLayout
{
    private TextView tvCouponName, tvIssueXY, tvCouponMoney, tvNextXY;
    
    public ImageView ivIco, ivCouponCheck;
    
    public Button btnDel;
    
    public TextView tvAddress;
    
    public ItemCoupon(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
    
    public ItemCoupon(Context context, View view)
    {
        super(context);
        tvCouponName = (TextView)view.findViewById(R.id.tv_couponName);
        tvIssueXY = (TextView)view.findViewById(R.id.tv_issueXY);
        tvAddress = (TextView)view.findViewById(R.id.tv_address);
        ivIco = (ImageView)view.findViewById(R.id.iv_logo);
        ivCouponCheck = (ImageView)view.findViewById(R.id.iv_coupon_check);
        tvCouponMoney = (TextView)view.findViewById(R.id.tv_couponMoney);
        tvNextXY = (TextView)view.findViewById(R.id.tv_nextXY);
        
    }
    
    @Override
    protected void onFinishInflate()
    {
        // initViews(v);
    }
    
    public void setData(CouponModel couponModel, final ListView mListView, ImageLoader mImageLoader)
    {
        try
        {
            if (null != couponModel.couponPic)
            {
                Bitmap bitmap =
                    mImageLoader.loadImage(ApiConstant.BASE_URL_PORT + ApiConstant.DONLOAD_COU_IMAGE
                        + couponModel.couponPic, 1,
                    // 回调对象
                        new ImageCallback()
                        {
                            @Override
                            public void loadedImage(String path, Bitmap bitmap)
                            {
                                // 根据回传的路径，查找listview中的imageview
                                ImageView iv = (ImageView)mListView.findViewWithTag(path);
                                if (iv != null && bitmap != null)
                                {
                                    iv.setImageBitmap(bitmap);
                                }
                                else
                                {
                                    if (iv != null)
                                    {
                                        iv.setImageResource(R.drawable.n_logo_01);
                                    }
                                }
                            }
                        },
                        1);
                if (bitmap != null)
                {
                    ivIco.setImageBitmap(bitmap);
                }
                else
                {
                    File file = new File(AppHelper.getImgCacheDir() + couponModel.couponPic);
                    if (file.exists())
                    {
                        Bitmap b = ImageUtil.decodeFile(file, true, 90);
                        ivIco.setImageBitmap(b);
                    }
                    else
                    {
                        if (couponModel.issueReceive == CouponType.COUPON_SALE.getValue())
                        {
                            ivIco.setImageResource(R.drawable.n_logo_01);
                        }
                        else
                        {
                            ivIco.setImageResource(R.drawable.n_logo_02);
                        }
                    }
                }
            }
            else
            {
                if (couponModel.issueReceive == CouponType.COUPON_SALE.getValue())
                {
                    ivIco.setImageResource(R.drawable.n_logo_01);
                }
                else
                {
                    ivIco.setImageResource(R.drawable.n_logo_02);
                }
            }
            tvCouponName.setText(couponModel.couponName);
            tvCouponMoney.setText("￥" + DateUtil.formatMoneyByInt(couponModel.discount) + "元");
            if (couponModel.issueReceive == CouponType.COUPON_SALE.getValue())
            {
                tvIssueXY.setText("消费满" + DateUtil.formatMoneyByInt(couponModel.minAmount) + "元可使用1张");
            }
            else
            {
                tvNextXY.setVisibility(View.VISIBLE);
                if (couponModel.issueY != null)
                {
                    tvIssueXY.setText("满" + DateUtil.formatMoneyByInt(couponModel.issueX) + "元送 " + couponModel.issueY
                        + "张");
                }
                else
                {
                    tvIssueXY.setText("满" + DateUtil.formatMoneyByInt(couponModel.issueX) + "元送 " + 0 + "张");
                }
                tvNextXY.setText("下次消费" + DateUtil.formatMoneyByInt(couponModel.minAmount) + "元可使用1张");
            }
            tvAddress.setText("已有" + couponModel.collectCount + "人领用");
            if (couponModel.couponStatus == CouponStatusEnum.WAIT.getValue())
            {
                ivCouponCheck.setVisibility(VISIBLE);
                ivCouponCheck.setImageResource(R.drawable.n_icon_ok);
            }
            else if (couponModel.couponStatus == CouponStatusEnum.NOPASS.getValue())
            {
                ivCouponCheck.setVisibility(VISIBLE);
                ivCouponCheck.setImageResource(R.drawable.n_icon_not);
            }
            else if (couponModel.couponStatus == CouponStatusEnum.ISSUE.getValue())
            {
                // ivCouponCheck.setVisibility(GONE);
                ivCouponCheck.setVisibility(VISIBLE);
                ivCouponCheck.setImageResource(R.drawable.n_icon_release);
            }
            else if (couponModel.couponStatus == CouponStatusEnum.OVERDUE.getValue())
            {
                ivCouponCheck.setVisibility(VISIBLE);
                ivCouponCheck.setImageResource(R.drawable.n_icon_failure);
            }
            
        }
        catch (Exception e)
        {
            Logger.i(e);
        }
    }
    
}
