package cn.swiftpass.enterprise.bussiness.enums;

public enum DownloadStatus
{
    /** 未知*/
    STATUS_DOWN_UNKNOWN(0),
    /** 准备下载，即还在下载队列没开始下载 1 */
    STATUS_DOWN_READY(1),
    /**下载中2*/
    STATUS_DOWN_DOWNING(2),
    /**下载完成3*/
    STATUS_DOWN_DOWNCOMPLETE(3),
    /**下载暂停4*/
    STATUS_DOWN_PAUSE(4),
    /**下载失败5*/
    STATUS_DOWN_DOWNFAIL(5);
    
    private DownloadStatus(int val)
    {
        this.value = val;
    }
    
    private int value;
    
    public int getValue()
    {
        return value;
    }
    
    public static DownloadStatus toEmun(int val)
    {
        if (val == STATUS_DOWN_UNKNOWN.value)
        {
            return STATUS_DOWN_UNKNOWN;
        }
        if (val == STATUS_DOWN_READY.value)
        {
            return STATUS_DOWN_READY;
        }
        if (val == STATUS_DOWN_DOWNING.value)
        {
            return STATUS_DOWN_DOWNING;
        }
        if (val == STATUS_DOWN_DOWNCOMPLETE.value)
        {
            return STATUS_DOWN_DOWNCOMPLETE;
        }
        if (val == STATUS_DOWN_PAUSE.value)
        {
            return STATUS_DOWN_PAUSE;
        }
        if (val == STATUS_DOWN_DOWNFAIL.value)
        {
            return STATUS_DOWN_DOWNFAIL;
        }
        return STATUS_DOWN_UNKNOWN;
        
    }
}
