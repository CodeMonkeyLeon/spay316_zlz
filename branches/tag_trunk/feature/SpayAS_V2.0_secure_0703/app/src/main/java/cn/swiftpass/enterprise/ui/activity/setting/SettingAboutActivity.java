package cn.swiftpass.enterprise.ui.activity.setting;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.ContentTextActivity;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.UpgradeDailog;
import cn.swiftpass.enterprise.ui.activity.user.TuiKuanPwdSettingActivity;
import cn.swiftpass.enterprise.ui.widget.CommonConfirmDialog;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.PreferenceUtil;

/**
 * 设置 会自动检查版本 User: ALAN Date: 13-10-28 Time: 下午5:11 To change this template use
 * File | Settings | File Templates.
 */
public class SettingAboutActivity extends TemplateActivity implements View.OnClickListener
{
    
    private Context mContext;
    
    private TextView tvVersion;
    
    // private TextView tvUser;
    
    //private LinearLayout llChangePwd, llShopInfo, refundLay, empManager, llCoupon, replaceTel, lay_scan;
    
    //private View lineChangePwd;
    
    //private ImageView ivNewVersion;
    
    //private TextView tv_debug, textView1, textView2;
    
    //private LayoutInflater inflater;
    
    //private View view, sum_id, ll_shopinfo, bodyLay_id, empManager_id, refundLay_id, ll_shopinfo_id, view_manager, view_last;
    
    //private Button ll_logout;
    
    //private SharedPreferences sp;
    
    //private LinearLayout bodyLay, write_off, ll_static_code, lay_prize;
    
    //private View marketing_id;
    
    //private LinearLayout marketingLay, lay_choise, sumLay, ly_pay_method;
    
    //private ImageView iv_voice, iv_point;
    
    //private View vi_pay_method;
    
    //private LinearLayout device_lay, lay_notify, lay_choise_lanu;
    
    //private View device_view;
    
    private TextView tv_ver;

    //private TextView tv_version_info,
    
    //private ImageView iv_about;
    
    private LinearLayout ly_protocol;
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_aboutus);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
            }
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                
            }
        });
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        initViews();
        
    }
    
    @Override
    protected void onResume()
    {
        // TODO Auto-generated method stub
        super.onResume();
        
    }
    
    @Override
    protected void onStop()
    {
        super.onStop();
    }
    
    private void initViews()
    {
        setContentView(R.layout.activity_setting_about);
        mContext = this;
        //iv_about = getViewById(R.id.iv_about);
        //tv_version_info = getViewById(R.id.tv_version_info);
        tv_ver = getViewById(R.id.tv_ver);
        tvVersion = getViewById(R.id.tv_version);
        tvVersion.setText("V" + AppHelper.getVerName(mContext));
        tv_ver.setText(getString(R.string.app_name) + " " + "V" +AppHelper.getVerName(mContext));
        //ivNewVersion = getViewById(R.id.iv_newVersion);
        checkVersion(true);
        ly_protocol = getViewById(R.id.ly_protocol);
        
        ly_protocol.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                //                ContentTextActivity.startActivity(SettingAboutActivity.this,
                //                    ApiConstant.pushMoneyUrl + "spay/qreProtocol/scanAgreement?mchId="
                //                        + getString(R.string.qustion_company_phone),
                //                    R.string.title_common_question2);
                
                ContentTextActivity.startActivity(SettingAboutActivity.this,
                    ApiConstant.pushMoneyUrl + "spay/qreProtocol/scanAgreement?mchId=" + MainApplication.getMchId(),
                    R.string.title_common_question2);
            }
        });
        
//        DynModel dynModel = (DynModel)SharedPreUtile.readProduct("dynModel" + ApiConstant.bankCode);
//        if (dynModel != null && !StringUtil.isEmptyOrNull(dynModel.getAndroidLogo()))
//        {
//            try
//            {
//                Bitmap b = ImageUtil.decodeFile2(AppHelper.getImgCacheDir() + dynModel.getThemeMd5() + ".jpg");
//                if (b != null)
//                {
//                    iv_about.setImageBitmap(b);
//                }
//                else
//                {
//                    MainApplication.finalBitmap.display(iv_about,
//                        dynModel.getAndroidLogo(),
//                        BitmapFactory.decodeResource(getResources(), R.drawable.my_process_loading));
//                }
//
//            }
//            catch (Exception e)
//            {
//
//            }
//        }
    }
    
    //private UpgradeInfo upVerInfo;
    
    /** 检查版本显示提示图标 */
    public void checkVersion(final boolean isDisplayIcon)
    {
        // 不用太平凡的检测升级 所以使用时间间隔区分
        /*UpgradeManager.getInstance().getVersonCode(new UINotifyListener<UpgradeInfo>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                // titleBar.setRightLodingVisible(true);
                showLoading(false, R.string.show_new_version_loading);
            }
            
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                dismissLoading();
            }
            
            @Override
            public void onSucceed(UpgradeInfo result)
            {
                super.onSucceed(result);
                dismissLoading();
                if (null != result)
                {
                    if (isDisplayIcon)
                    {
                        upVerInfo = result;
                        ivNewVersion.setVisibility(View.VISIBLE);
                        tv_version_info.setText(R.string.tv_version_info);
                    }
                    else
                    {
                        result.isMoreSetting = true;
                        showUpgradeInfoDialog(result, new ComDialogListener(result));
                    }
                }
                else
                {
                    if (!isDisplayIcon)
                    {
                        toastDialog(SettingAboutActivity.this, R.string.show_new_version, null);
                    }
                }
            }
        });*/
    }
    
    @Override
    public void onClick(View view)
    {
//        switch (view.getId())
//        {
//
//        }
    }
    
    /** 检查版本 */
    public void onCheckVersion(View v)
    {
        
        // 进行网络判断
       /* if (!NetworkUtils.isNetworkAvailable(this))
        {
            toastDialog(SettingAboutActivity.this, R.string.network_exception, null);
            return;
        }
        //检查过了 有数据了直接跳出询问用户是否需要升级 暂时屏蔽掉
        if (upVerInfo != null)
        {
            upVerInfo.isMoreSetting = true;
            showUpgradeInfoDialog(upVerInfo, new ComDialogListener(upVerInfo));
        }
        else
        {
            checkVersion(false);
        }*/
    }
    
    /** 升级点击事件 */
    class ComDialogListener implements CommonConfirmDialog.ConfirmListener
    {
        private UpgradeInfo result;
        
        public ComDialogListener(UpgradeInfo result)
        {
            this.result = result;
        }
        
        @Override
        public void ok()
        {
            new UpgradeDailog(SettingAboutActivity.this, result, new UpgradeDailog.UpdateListener()
            {
                @Override
                public void cancel()
                {
                    
                }
            }).show();
        }
        
        @Override
        public void cancel()
        {
            PreferenceUtil.commitInt("update", result.version); // 保存更新，下次进来不更新
        }
        
    }

    
    boolean isAuth = false;
    
    public void existAuth()
    {
        UserManager.existAuth(MainApplication.userId, 2, new UINotifyListener<Integer>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
            }
            
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                if (object != null)
                {
                    showToastInfo(object.toString());
                }
            }
            
            @Override
            public void onSucceed(Integer result)
            {
                super.onSucceed(result);
                dismissMyLoading();
                
                switch (result)
                {
                // 资料未审核
                    case 4:
                        isAuth = false;
                        break;
                    // 资料已审核
                    case 5:
                        isAuth = true;
                        break;
                }
                
            }
        });
    }

    
    public void changeRefundPwd(View view)
    {
        TuiKuanPwdSettingActivity.startActivity(mContext);
    }
    
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
}