/*
 * 文 件 名:  HelpUI.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.utils;

import java.lang.ref.WeakReference;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.City;
import cn.swiftpass.enterprise.ui.activity.shop.Personal_DataAdapter;

/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-11]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class HelpUI
{
    
    public static PopupWindow popupWindow;
    
    // 省 
    public static final int TAG_PROVI = 0;
    
    // 市
    public static final int TAG_CITY = 1;
    
    public static final int CITYS = 5;
    
    public static final int BANK_CITY = 2;
    
    // 商户行业数据
    public static final int SHOP_DATA = 3;
    
    public static final int SHOP_BANK = 4;
    
    //商铺类型
    public static final int ShOP_TYPE = 6;
    
    //行业类型
    public static final int TAG_MERCHANT = 7;
    
    // 支付类型
    public static final int PAY_TYPE = 8;
    
    public static final int PAY_TYPE_REFUND = 9;
    
    private static SharedPreferences sharedPreferences;
    
    //    public static void initPopWindow(Context context, final List<City> dataStrs, final LinearLayout lay, int tag,
    //        final Handler handler)
    //    {
    //        sharedPreferences = context.getSharedPreferences("dataOne", 0);
    //        
    //        View contentView = LayoutInflater.from(context).inflate(R.layout.person_data_pop, null);
    //        //= chairman.getChPopupWindow(contentView, 200, 300);
    //        
    //        int popWidth;
    //        
    //        if (lay.getId() == R.id.citys)
    //        {
    //            popWidth = lay.getWidth() - 40;
    //        }
    //        else
    //        {
    //            popWidth = lay.getWidth() - 35;
    //        }
    //        int popHeight = (int)context.getResources().getDimensionPixelSize(R.dimen.shop_keeper_popwindow_height);
    //        
    //        popupWindow = new PopupWindow(contentView, popWidth, popHeight);
    //        popupWindow.setContentView(contentView);
    //        ListView listView = (ListView)contentView.findViewById(R.id.name_list);
    //        Personal_DataAdapter adapter = new Personal_DataAdapter(context, dataStrs);
    //        listView.setAdapter(adapter);
    //        //        listView.setOnItemClickListener(shopkeeperActivity);
    //        switch (tag)
    //        {
    //            case SHOP_BANK:
    //                listView.setOnItemClickListener(new OnItemClickListener()
    //                {
    //                    @Override
    //                    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    //                    {
    //                        City city = dataStrs.get(position);
    //                        province.setText(city.getCity());
    //                        sharedPreferences.edit().putString("bank_name", city.getCity()).commit();
    //                        HandlerManager.notifyMessage(HandlerManager.SHOPKEEPID, 3, city.getNumber());
    //                        if (null != popupWindow)
    //                        {
    //                            popupWindow.dismiss();
    //                        }
    //                    }
    //                });
    //                break;
    //            
    //            default:
    //                break;
    //        }
    //        
    //        //        
    //        popupWindow.setBackgroundDrawable(new BitmapDrawable());
    //        // 设置焦点，必须设置，否则listView无法响应 
    //        popupWindow.setFocusable(true);
    //        // 设置点击其他地方 popupWindow消失 
    //        popupWindow.setOutsideTouchable(true);
    //        popupWindow.showAsDropDown(province, 23, 5);
    //        
    //    }
    
    /**
     * 初始化
     * <功能详细描述>
     * @param myContact
     * @see [类、类#方法、类#成员]
     */
    public static void initPopWindow(Context context, final List<City> dataStrs, final TextView province, final View v,
        final int tag, final Handler handler)
    {
        sharedPreferences = context.getSharedPreferences("dataOne", 0);
        
        View contentView = LayoutInflater.from(context).inflate(R.layout.person_data_pop, null);
        //= chairman.getChPopupWindow(contentView, 200, 300);
        
        int popWidth;
        int popHeight = 0;
        if (tag == PAY_TYPE)
        {
            popWidth = DisplayUtil.dip2Px(context, 85);
            popHeight = DisplayUtil.dip2Px(context, 235);
            
        }
        else if (tag == PAY_TYPE_REFUND)
        {
            popWidth = DisplayUtil.dip2Px(context, 100);
            popHeight = DisplayUtil.dip2Px(context, 140);
        }
        else
        {
            if (province.getId() == R.id.citys)
            {
                popWidth = province.getWidth() - 40;
            }
            else
            {
                popWidth = province.getWidth() - 35;
            }
            popHeight = (int)context.getResources().getDimensionPixelSize(R.dimen.shop_keeper_popwindow_height);
        }
        
        popupWindow = new PopupWindow(contentView, popWidth, popHeight);
        popupWindow.setContentView(contentView);
        ListView listView = (ListView)contentView.findViewById(R.id.name_list);
        Personal_DataAdapter adapter = new Personal_DataAdapter(context, dataStrs);
        listView.setAdapter(adapter);
        //        listView.setOnItemClickListener(shopkeeperActivity);
        switch (tag)
        {
            case PAY_TYPE://支付类型弹窗框
                listView.setOnItemClickListener(new OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                    {
                        City city = dataStrs.get(position);
                        province.setText(city.getCity());
                        
                        HandlerManager.notifyMessage(HandlerManager.PAY_TYPE, HandlerManager.PAY_TYPE, city.getNumber());
                        if (null != popupWindow)
                        {
                            popupWindow.dismiss();
                        }
                    }
                });
                break;
            case PAY_TYPE_REFUND://支付类型弹窗框
                listView.setOnItemClickListener(new OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                    {
                        City city = dataStrs.get(position);
                        province.setText(city.getCity());
                        HandlerManager.notifyMessage(HandlerManager.PAY_TYPE_REFUND,
                            HandlerManager.PAY_TYPE_REFUND,
                            city.getNumber());
                        if (null != popupWindow)
                        {
                            popupWindow.dismiss();
                        }
                    }
                });
                break;
        }
        //        
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        // 设置焦点，必须设置，否则listView无法响应 
        popupWindow.setFocusable(true);
        // 设置点击其他地方 popupWindow消失 
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAsDropDown(v, 0, 5);
        
    }
    
    /**
     * 初始化
     * <功能详细描述>
     * @param myContact
     * @param type 1:统计 2，交易分布
     * @see [类、类#方法、类#成员]
     */
    public static void initTotalPopWindow(Context context, final List<City> dataStrs, final int type, final View v,
        final Handler handler)
    {
        View contentView = LayoutInflater.from(context).inflate(R.layout.total_pop_list, null);
        
        int popWidth = 0;
        int popHeight = 0;
        switch (type)
        {
            case 1:
                popWidth = DisplayUtil.dip2Px(context, 100);
                popHeight = DisplayUtil.dip2Px(context, 98);
                break;
            case 2:
                popWidth = DisplayUtil.dip2Px(context, 83);
                popHeight = DisplayUtil.dip2Px(context, 98);
                break;
            default:
                break;
        }
        
        popupWindow = new PopupWindow(contentView, popWidth, popHeight);
        popupWindow.setContentView(contentView);
        ListView listView = (ListView)contentView.findViewById(R.id.name_list);
        Personal_DataAdapter adapter = new Personal_DataAdapter(context, dataStrs);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                City city = dataStrs.get(position);
                switch (type)
                {
                    case 1: //统计
                        HandlerManager.notifyMessage(HandlerManager.PAY_TOTAL_TYPE,
                            HandlerManager.PAY_TOTAL_TYPE,
                            city.getNumber());
                        break;
                    case 2: //交易分布
                        HandlerManager.notifyMessage(HandlerManager.PAY_TOTAL_TYPE_DISTRIBUTION,
                            HandlerManager.PAY_TOTAL_TYPE_DISTRIBUTION,
                            city.getNumber());
                        break;
                    default:
                        break;
                }
                if (null != popupWindow)
                {
                    popupWindow.dismiss();
                }
            }
        });
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        // 设置焦点，必须设置，否则listView无法响应 
        popupWindow.setFocusable(true);
        // 设置点击其他地方 popupWindow消失 
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAsDropDown(v, 0, DisplayUtil.dip2Px(context, 7));
        
    }
    
    /**
     * 初始化
     * <功能详细描述>
     * @param myContact
     * @see [类、类#方法、类#成员]
     */
    public static void initPopWindow(Context context, final List<City> dataStrs, final TextView province, int tag,
        final Handler handler)
    {
        sharedPreferences = context.getSharedPreferences("dataOne", 0);
        
        View contentView = LayoutInflater.from(context).inflate(R.layout.person_data_pop, null);
        //= chairman.getChPopupWindow(contentView, 200, 300);
        
        int popWidth;
        int popHeight = 0;
        if (tag == PAY_TYPE)
        {
            popWidth = DisplayUtil.dip2Px(context, 80);
            popHeight = DisplayUtil.dip2Px(context, 190);
            
        }
        else
        {
            if (province.getId() == R.id.citys)
            {
                popWidth = province.getWidth() - 40;
            }
            else
            {
                popWidth = province.getWidth() - 35;
            }
            popHeight = (int)context.getResources().getDimensionPixelSize(R.dimen.shop_keeper_popwindow_height);
        }
        
        popupWindow = new PopupWindow(contentView, popWidth, popHeight);
        popupWindow.setContentView(contentView);
        ListView listView = (ListView)contentView.findViewById(R.id.name_list);
        Personal_DataAdapter adapter = new Personal_DataAdapter(context, dataStrs);
        listView.setAdapter(adapter);
        //        listView.setOnItemClickListener(shopkeeperActivity);
        switch (tag)
        {
            case PAY_TYPE://支付类型弹窗框
                listView.setOnItemClickListener(new OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                    {
                        City city = dataStrs.get(position);
                        province.setText(city.getCity());
                        HandlerManager.notifyMessage(HandlerManager.PAY_TYPE, HandlerManager.PAY_TYPE, city.getNumber());
                        if (null != popupWindow)
                        {
                            popupWindow.dismiss();
                        }
                    }
                });
                break;
            case SHOP_BANK:
                listView.setOnItemClickListener(new OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                    {
                        City city = dataStrs.get(position);
                        province.setText(city.getCity());
                        sharedPreferences.edit().putString("bank_name", city.getCity()).commit();
                        HandlerManager.notifyMessage(HandlerManager.SHOPKEEPID, 3, city.getNumber());
                        if (null != popupWindow)
                        {
                            popupWindow.dismiss();
                        }
                    }
                });
                break;
            // 省
            case TAG_PROVI:
                listView.setOnItemClickListener(new OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                    {
                        City city = dataStrs.get(position);
                        province.setText(city.getCity());
                        sharedPreferences.edit().putString("province", province.getText().toString()).commit();
                        
                        HandlerManager.notifyMessage(HandlerManager.SHOPKEEPID, 0, city.getNumber());
                        
                        if (null != popupWindow)
                        {
                            popupWindow.dismiss();
                        }
                    }
                });
                
                break;
            case BANK_CITY:
                listView.setOnItemClickListener(new OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                    {
                        City city = dataStrs.get(position);
                        province.setText(city.getCity());
                        sharedPreferences.edit().putString("bank_province", province.getText().toString()).commit();
                        HandlerManager.notifyMessage(HandlerManager.SHOPKEEPID, 1, city.getNumber());
                        
                        if (null != popupWindow)
                        {
                            popupWindow.dismiss();
                        }
                    }
                });
                
                break;
            case TAG_CITY:
                listView.setOnItemClickListener(new OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                    {
                        City city = dataStrs.get(position);
                        province.setText(city.getCity());
                        if (null != popupWindow)
                        {
                            popupWindow.dismiss();
                        }
                    }
                });
                break;
            
            case TAG_MERCHANT:
                listView.setOnItemClickListener(new OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                    {
                        City city = dataStrs.get(position);
                        province.setText(city.getCity());
                        handler.sendMessage(handler.obtainMessage(6, city.getNumber())); //发送数据给handler 告诉它小类型已经选 好！ 
                        if (null != popupWindow)
                        {
                            popupWindow.dismiss();
                        }
                    }
                });
                break;
            
            case CITYS:
                listView.setOnItemClickListener(new OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                    {
                        City city = dataStrs.get(position);
                        province.setText(city.getCity());
                        Message msg = handler.obtainMessage(4, city.getCity());
                        handler.sendMessage(msg);
                        
                        sharedPreferences.edit().putString("citys", city.getCity().toString()).commit();
                        
                        if (null != popupWindow)
                        {
                            popupWindow.dismiss();
                        }
                    }
                });
                break;
            
            case SHOP_DATA:
                listView.setOnItemClickListener(new OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                    {
                        City city = dataStrs.get(position);
                        province.setText(city.getCity());
                        
                        HandlerManager.notifyMessage(HandlerManager.SHOPKEEPID, 2, city.getCity().toString());
                        
                        sharedPreferences.edit().putString("bunisOne", city.getCity().toString()).commit();
                        
                        if (null != popupWindow)
                        {
                            popupWindow.dismiss();
                        }
                    }
                });
                break;
            default:
                break;
        }
        
        //        
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        // 设置焦点，必须设置，否则listView无法响应 
        popupWindow.setFocusable(true);
        // 设置点击其他地方 popupWindow消失 
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAsDropDown(province, 0, 5);
        
    }
    
    /**
     * 商户 行业数据
     * <功能详细描述>
     * @param myContact
     * @see [类、类#方法、类#成员]
     */
    public static void initSShopDataPopWindow(Context context, final List<City> dataStrs, final TextView province,
        int tag)
    {
        Log.i("hehui", " province.getWidth()-->" + province.getWidth());
        
        sharedPreferences = context.getSharedPreferences("dataOne", 0);
        
        View contentView = LayoutInflater.from(context).inflate(R.layout.person_data_pop, null);
        //= chairman.getChPopupWindow(contentView, 200, 300);
        
        int popWidth;
        
        if (province.getId() == R.id.citys)
        {
            popWidth = province.getWidth() - 40;
        }
        else
        {
            popWidth = province.getWidth() - 35;
        }
        int popHeight = (int)context.getResources().getDimensionPixelSize(R.dimen.shop_keeper_popwindow_height);
        popupWindow = new PopupWindow(contentView, popWidth, popHeight);
        popupWindow.setContentView(contentView);
        
        ListView listView = (ListView)contentView.findViewById(R.id.name_list);
        Personal_DataAdapter adapter = new Personal_DataAdapter(context, dataStrs);
        listView.setAdapter(adapter);
        //        listView.setOnItemClickListener(shopkeeperActivity);
        switch (tag)
        {
        // 省
            case TAG_PROVI:
                listView.setOnItemClickListener(new OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                    {
                        City city = dataStrs.get(position);
                        province.setText(city.getCity());
                        sharedPreferences.edit().putString("province", province.getText().toString()).commit();
                        
                        HandlerManager.notifyMessage(HandlerManager.SHOPKEEPID, 0, city.getNumber());
                        
                        if (null != popupWindow)
                        {
                            popupWindow.dismiss();
                        }
                    }
                });
                
                break;
            case BANK_CITY:
                listView.setOnItemClickListener(new OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                    {
                        City city = dataStrs.get(position);
                        province.setText(city.getCity());
                        sharedPreferences.edit().putString("bank_province", province.getText().toString()).commit();
                        HandlerManager.notifyMessage(HandlerManager.SHOPKEEPID, 1, city.getNumber());
                        
                        if (null != popupWindow)
                        {
                            popupWindow.dismiss();
                        }
                    }
                });
                
                break;
            case TAG_CITY:
                listView.setOnItemClickListener(new OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                    {
                        City city = dataStrs.get(position);
                        province.setText(city.getCity());
                        if (null != popupWindow)
                        {
                            popupWindow.dismiss();
                        }
                    }
                });
                break;
            default:
                break;
        }
        
        //        
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        // 设置焦点，必须设置，否则listView无法响应 
        popupWindow.setFocusable(true);
        // 设置点击其他地方 popupWindow消失 
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAsDropDown(province, 23, 5);
        
    }
    
    /**
     * <一句话功能简述>商铺类型选择popwindow
     * <功能详细描述>
     * @param context
     * @param arrays 
     * @param etShopType
     * @param shopType
     * @param handler
     * @see [类、类#方法、类#成员]
     */
    public static void initPopWindow(Context context, final String[] arrays, final TextView etShopType, int shopType,
        Handler handler)
    {
        View contentView = LayoutInflater.from(context).inflate(R.layout.person_data_pop, null);
        
        int popWidth;
        popWidth = etShopType.getWidth() - 30;
        int popHeight = (int)context.getResources().getDimensionPixelSize(R.dimen.shop_keeper_popwindow_height);
        
        popupWindow = new PopupWindow(contentView, popWidth, popHeight);
        popupWindow.setContentView(contentView);
        
        ListView listView = (ListView)contentView.findViewById(R.id.name_list);
        //        Personal_DataAdapter adapter = new Personal_DataAdapter(context, arrays);
        ArrayAdapter<String> adapter =
            new ArrayAdapter<String>(context, R.layout.personal_pop_list_item, R.id.username_spinner_item, arrays);
        listView.setAdapter(adapter);
        
        switch (shopType)
        {
            case HelpUI.ShOP_TYPE:
                listView.setOnItemClickListener(new OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
                    {
                        etShopType.setText(arrays[arg2]);
                        popupWindow.dismiss();
                    }
                });
                break;
        
        }
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        // 设置焦点，必须设置，否则listView无法响应 
        popupWindow.setFocusable(true);
        // 设置点击其他地方 popupWindow消失 
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAsDropDown(etShopType, 18, 5);
    }
    
    public static class PopModel
    {
        public String id;
        
        public String label;
        
        public WeakReference<Object> obWf;
    }
    
    public static interface onSelectItemListener
    {
        public void onSelectItemEvent(int selectIdex, PopModel selectModel);
    }
    
    public static void showRegPopWindow(Context context, int popWindowWidth, final List<PopModel> dataList,
        final TextView actionTextView, final onSelectItemListener listener)
    {
        View contentView = LayoutInflater.from(context).inflate(R.layout.person_data_pop, null);
        int popHeight = (int)context.getResources().getDimensionPixelSize(R.dimen.shop_keeper_popwindow_height);
        
        popupWindow = new PopupWindow(contentView, popWindowWidth, popHeight);
        popupWindow.setContentView(contentView);
        ListView listView = (ListView)contentView.findViewById(R.id.name_list);
        listView.setOnItemClickListener(new OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                PopModel selectedModel = dataList.get(position);
                actionTextView.setText(selectedModel.label);
                actionTextView.setTag(selectedModel.id);
                
                if (null != popupWindow)
                {
                    popupWindow.dismiss();
                }
                
                if (listener != null)
                {
                    
                    listener.onSelectItemEvent(position, selectedModel);
                }
                
            }
        });
        PopWindowAdapter adapter = new PopWindowAdapter(context, dataList);
        listView.setAdapter(adapter);
        
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        // 设置焦点，必须设置，否则listView无法响应 
        popupWindow.setFocusable(true);
        // 设置点击其他地方 popupWindow消失 
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAsDropDown(actionTextView, 23, 5);
    }
    
    static class PopWindowAdapter extends BaseAdapter
    {
        private LayoutInflater inflater;
        
        private Context context;
        
        private List<PopModel> dataList;
        
        public PopWindowAdapter(Context context, List<PopModel> dataList)
        {
            this.context = context;
            this.dataList = dataList;
            this.inflater = LayoutInflater.from(context);
            
        }
        
        @Override
        public int getCount()
        {
            return dataList.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            return dataList.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            final ViewHolder holder;
            if (convertView == null)
            {
                convertView = inflater.inflate(R.layout.personal_pop_list_item, null);
                holder = new ViewHolder();
                holder.name = (TextView)convertView.findViewById(R.id.username_spinner_item);
                
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            PopModel popModel = dataList.get(position);
            holder.name.setText(popModel.label);
            holder.name.setTag(popModel.id);
            return convertView;
            
        }
        
        public class ViewHolder
        {
            public TextView name;
        }
    }
}
