/*
 * 文 件 名:  NoticeModel.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-9-21
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * 公告
 * 
 * @author  he_hui
 * @version  [版本号, 2016-9-21]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class NoticeModel implements Serializable
{
    
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    private String title; //标题
    
    private String startTime;
    
    private String endTime;
    
    private String noticeContentUrl;
    
    private long noticeId;
    
    private long id;
    
    private String createTime;
    
    /**
     * @return 返回 createTime
     */
    public String getCreateTime()
    {
        return createTime;
    }
    
    /**
     * @param 对createTime进行赋值
     */
    public void setCreateTime(String createTime)
    {
        this.createTime = createTime;
    }
    
    /**
     * @return 返回 id
     */
    public long getId()
    {
        return id;
    }
    
    /**
     * @param 对id进行赋值
     */
    public void setId(long id)
    {
        this.id = id;
    }
    
    /**
     * @return 返回 noticeId
     */
    public long getNoticeId()
    {
        return noticeId;
    }
    
    /**
     * @param 对noticeId进行赋值
     */
    public void setNoticeId(long noticeId)
    {
        this.noticeId = noticeId;
    }
    
    /**
     * @return 返回 title
     */
    public String getTitle()
    {
        return title;
    }
    
    /**
     * @param 对title进行赋值
     */
    public void setTitle(String title)
    {
        this.title = title;
    }
    
    /**
     * @return 返回 startTime
     */
    public String getStartTime()
    {
        return startTime;
    }
    
    /**
     * @param 对startTime进行赋值
     */
    public void setStartTime(String startTime)
    {
        this.startTime = startTime;
    }
    
    /**
     * @return 返回 endTime
     */
    public String getEndTime()
    {
        return endTime;
    }
    
    /**
     * @param 对endTime进行赋值
     */
    public void setEndTime(String endTime)
    {
        this.endTime = endTime;
    }
    
    /**
     * @return 返回 noticeContentUrl
     */
    public String getNoticeContentUrl()
    {
        return noticeContentUrl;
    }
    
    /**
     * @param 对noticeContentUrl进行赋值
     */
    public void setNoticeContentUrl(String noticeContentUrl)
    {
        this.noticeContentUrl = noticeContentUrl;
    }
}
