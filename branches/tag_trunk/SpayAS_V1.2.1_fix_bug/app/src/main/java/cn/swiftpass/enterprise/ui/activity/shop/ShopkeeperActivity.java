/*
 * 文 件 名:  ShopkeeperActivity.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.shop;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.tsz.afinal.FinalBitmap;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.shop.PersonalManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.City;
import cn.swiftpass.enterprise.bussiness.model.MerchantTempDataModel;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.bussiness.model.ShopBankBaseInfo;
import cn.swiftpass.enterprise.bussiness.model.ShopBaseDataInfo;
import cn.swiftpass.enterprise.io.database.CityDao;
import cn.swiftpass.enterprise.io.database.access.ShopBankDataDB;
import cn.swiftpass.enterprise.io.database.access.ShopBaseDataDB;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.SelectTowDialog;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.CleanManager;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.HelpUI;
import cn.swiftpass.enterprise.utils.VerifyUtil;

/**
 * 店主资料完善 <功能详细描述>
 * 
 * @author he_hui
 * @version [版本号, 2013-3-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class ShopkeeperActivity extends TemplateActivity
{
    
    private Button btn_next;
    
    // 省
    private TextView province;
    
    private TextView bank_province;
    
    private TextView bank_city;
    
    /** 市区 */
    private TextView citys;
    
    private List<City> provinces = new ArrayList<City>();
    
    private List<City> shopCityArrs = new ArrayList<City>();
    
    private List<City> bankCityArrs = new ArrayList<City>();
    
    private List<City> shopBaseTwo = null;
    
    private String region_id;
    
    private CityDao dao;
    
    // 商品名称
    private EditText shop_name;
    
    // 商家地址
    private EditText shop_address;
    
    // 开户行名称
    private TextView bank_name;
    
    // 营业网点
    private EditText branch;
    
    // 收款人姓名
    private EditText bayee_name;
    
    // 银行卡号
    private EditText bank_num;
    
    // 固话
    private EditText tel;
    
    // 身份证号码
    private EditText personal_id;
    
    private SharedPreferences sharedPreferences;
    
    private RelativeLayout bunisLayout;
    
    private RelativeLayout bankLayout;
    
    private TextView bunisOne;
    
    private RelativeLayout towLayout;
    
    private TextView twoText, statuImg, statusTxt;
    
    private MerchantTempDataModel dataModel;
    
    private TextView twoText_iv, bunisOne_iv;
    
    private ImageView iv_shopname_promt, iv_shopaddress_promt, iv_shophone_promt, iv_shopbranch_promt,
        iv_bayeeName_promt, iv_bankNum_promt, iv_personalId_promt;
    
    private ImageView iv_shop_name, iv_shop_address, iv_tel, iv_branch, iv_bayee_name, iv_bank_num, iv_personal_id;
    
    private ImageView id_people_photo, id_down_photo, id_code;
    
    private File tempFile;
    
    /** 拍照保存照片的uri */
    private Uri originalUri = null;
    
    private SelectTowDialog takImgDialog;
    
    private Integer imageID;
    
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x1001;
    
    public static final int REQUEST_CODE_CAPTURE_PICTURE = 0x1002;
    
    public static boolean cancel_perview = true;
    
    private TextView id_people_photo_look, id_down_photo_look, id_code_look;
    
    private FinalBitmap finalBitmap;
    
    private TextView et_tel, tv_mch;
    
    private TextView tv_mch_prompt;
    
    // 这个是判断是否激活过。。。。
    
    Handler handler = new Handler()
    {
        public void handleMessage(android.os.Message msg)
        {
            switch (msg.what)
            {
            // 选择省，自动连动式级数据
                case 0:
                    region_id = (String)msg.obj;
                    
                    // 截取region_id 前两位
                    String interStr = region_id.substring(0, 2);
                    
                    shopCityArrs = dao.getAllCityByID(interStr);
                    
                    if (shopCityArrs.size() > 0)
                    {
                        citys.setText(shopCityArrs.get(0).getCity());
                        sharedPreferences.edit().putString("citys", citys.getText().toString()).commit();
                        sharedPreferences.edit().putString("province", province.getText().toString()).commit();
                    }
                    
                    break;
                // 银行 选择省，自动连动式级数据
                case 1:
                    region_id = (String)msg.obj;
                    
                    // 截取region_id 前两位
                    bankCityArrs = dao.getAllCityByID(region_id.substring(0, 2));
                    
                    if (bankCityArrs.size() > 0)
                    {
                        bank_city.setText(bankCityArrs.get(0).getCity());
                        sharedPreferences.edit().putString("bank_city", bank_city.getText().toString()).commit();
                    }
                    break;
                case 2: // 商户资料基本信息
                    String parent_id = (String)msg.obj;
                    bunisOne.setText(parent_id);
                    sharedPreferences.edit().putString("bunisOne", parent_id).commit();
                    //
                    // List<ShopBaseDataInfo> lst =
                    // ShopBaseDataDB.getInstance().querySonData(parent_id);
                    //
                    // shopBaseTwo = new ArrayList<City>();
                    // for (ShopBaseDataInfo info : lst)
                    // {
                    // City city = new City();
                    // city.setNumber(info.getMerchantTypeId());
                    // city.setCity(info.typeName);
                    //
                    // shopBaseTwo.add(city);
                    // }
                    // if (lst != null && lst.size() > 0)
                    // {
                    // ShopBaseDataInfo info = lst.get(0);
                    // twoText.setText(info.typeName);
                    // // 保存 商户 行业的id
                    // sharedPreferences.edit().putString("bunisOne",
                    // info.merchantTypeId).commit();
                    // }
                    break;
                
                case 3: // 银行类型
                    region_id = (String)msg.obj;
                    if (!"".equals(region_id) && region_id != null)
                    {
                        sharedPreferences.edit().putString("bank_id", region_id).commit();
                    }
                    break;
                case 4:
                    // 选择完市后系统自动填充省市地址 clarence
                    String info = province.getText().toString().trim() + (String)msg.obj;
                    shop_address.setText(info);
                    break;
                case 5:
                    initPromt();
                    break;
                case 6: // 行业
                    String merchantTypeSonId = (String)msg.obj; // 获取子类型
                    sharedPreferences.edit().putString("bunisOne", merchantTypeSonId).commit();
                    break;
                default:
                    break;
            }
        };
    };
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.personal_data);
        
        dao = CityDao.getInstance(ShopkeeperActivity.this);
        
        sharedPreferences = this.getSharedPreferences("dataOnes", 0);
        
        finalBitmap = FinalBitmap.create(ShopkeeperActivity.this);//
        try
        {
            finalBitmap.configDiskCachePath(AppHelper.getImgCacheDir());
        }
        catch (Exception e)
        {
            Log.e("hehui", "configDiskCachePath failed ");
        }
        
        initView();
        
        setLister();
        
        initValue();
        
        HandlerManager.registerHandler(HandlerManager.SHOPKEEPID, handler);
        
        Bundle bundle = this.getIntent().getBundleExtra("bundle");
        // 判断服务器端 有没有在激活时 填写商户类型与行业类型 如果有直接从服务器端返回过来填写到前端
        // Integer merchantType = this.getIntent().getIntExtra("merchantType",
        // -1);
        // if (merchantType != -1)
        // {
        // List<ShopBaseDataInfo> menrchList =
        // ShopBaseDataDB.getInstance().queryByParentID(String.valueOf(merchantType));
        // if (menrchList != null && menrchList.size() > 0)
        // {
        // ShopBaseDataInfo bankBaseInfo = menrchList.get(0);
        // twoText.setText(bankBaseInfo.getTypeName());
        // List<ShopBaseDataInfo> menrchListParent =
        // ShopBaseDataDB.getInstance().queryByParentID(bankBaseInfo.getParentId());
        // if (menrchListParent != null && menrchListParent.size() > 0)
        // {
        // bunisOne.setText(menrchListParent.get(0).getTypeName());
        // }
        // }
        // bunisLayout.setClickable(false);
        // bunisLayout.setEnabled(false);
        // towLayout.setClickable(false);
        // towLayout.setEnabled(false);
        // twoText_iv.setVisibility(View.GONE);
        // bunisOne_iv.setVisibility(View.GONE);
        // }
        
        // 从商户信息 跳转到资料完善
        if (bundle != null)
        {
            dataModel = (MerchantTempDataModel)bundle.get("merchantData");
            if (dataModel != null)
            {
                // 进行激活验证
                loadData(dataModel);
                
                //
                setViewEnable();
            }
            
        }
        handler.sendEmptyMessage(5);
    }
    
    /**
     * <一句话功能简述> <功能详细描述>
     * 
     * @see [类、类#方法、类#成员]
     */
    private void setViewEnable()
    {
        
        switch (Integer.parseInt(MainApplication.remark))
        {
            case 1:
                statusTxt.setText(R.string.tx_mch_prompt_succ);
                statuImg.setBackgroundResource(R.drawable.picture_03);
                setEnbaled(false);
                btn_next.setVisibility(View.GONE);
                break;
            case -1: // 测试账号
                if (dataModel != null)
                {
                    btn_next.setVisibility(View.GONE);
                    setEnbaled(false);
                }
                break;
            case 0: // 审核通过
                statuImg.setBackgroundResource(R.drawable.picture_02);
                statusTxt.setText(R.string.tx_mch_prompt_pass);
                btn_next.setVisibility(View.GONE);
                setEnbaled(false);
                break;
            case -3:// 审核未通过
                statuImg.setBackgroundResource(R.drawable.picture_02);
                statusTxt.setText(R.string.tx_mch_prompt_nopass);
                break;
            case -2:// 审核中
                statuImg.setBackgroundResource(R.drawable.picture_02);
                statusTxt.setText(R.string.tx_mch_prompt_review);
                btn_next.setVisibility(View.GONE);
                setEnbaled(false);
                break;
            default:
                // statuImg.setBackgroundResource(R.drawable.picture_framework_02);
                setEnbaled(false);
                btn_next.setVisibility(View.GONE);
                break;
        }
    }
    
    private void setEnbaled(boolean b)
    {
        shop_name.setEnabled(b);
        shop_address.setEnabled(b);
        bank_name.setEnabled(b);
        branch.setEnabled(b);
        bank_num.setEnabled(b);
        bayee_name.setEnabled(b);
        personal_id.setEnabled(b);
        tel.setEnabled(b);
        bunisOne.setEnabled(b);
        id_people_photo.setEnabled(b);
        id_down_photo.setEnabled(b);
        bunisLayout.setEnabled(b);
        btn_next.setEnabled(b);
        province.setEnabled(b);
        citys.setEnabled(b);
        bankLayout.setEnabled(b);
        id_code.setEnabled(b);
    }
    
    /**
     * 检查是否授权 和 资料是否完善 <功能详细描述>
     * 
     * @see [类、类#方法、类#成员]
     */
    private void checkExitAuth()
    {
        // 授权验证
        if (MainApplication.userId != 0)
        {
            UserManager.existAuth(MainApplication.userId, 0, new UINotifyListener<Integer>()
            {
                @Override
                public void onPreExecute()
                {
                    super.onPreExecute();
                    showLoading(false, "授权激活检查中...");
                }
                
                @Override
                public void onError(Object object)
                {
                    super.onError(object);
                    if (object != null)
                    {
                        showToastInfo(object.toString());
                    }
                }
                
                @Override
                public void onSucceed(Integer result)
                {
                    super.onSucceed(result);
                    dismissLoading();
                    // DialogInfo dialogInfo = null;
                    switch (result)
                    {
                    // 已激活
                        case 0:
                            // 使行业类型不能选择
                            bunisLayout.setClickable(false);
                            bunisLayout.setEnabled(false);
                            towLayout.setClickable(false);
                            towLayout.setEnabled(false);
                            twoText_iv.setVisibility(View.GONE);
                            bunisOne_iv.setVisibility(View.GONE);
                            break;
                        // 出错
                        case 11:
                            showToastInfo("授权激活失败，请稍微再试!");
                            return;
                            // 未激活
                        case 1:
                            
                            // 不作任何修改
                            break;
                        case RequestResult.RESULT_TIMEOUT_ERROR: // 请求超时
                            showToastInfo("连接超时，请稍微再试!!");
                            return;
                            
                        default:
                            break;
                    }
                    
                }
            });
        }
        else
        {
            // 用户id为空
            showToastInfo("连接已断开，请重新再登录!!");
            return;
        }
    }
    
    /**
     * 加载 填充数据 <功能详细描述>
     * 
     * @see [类、类#方法、类#成员]
     */
    private void loadData(MerchantTempDataModel dataModel)
    {
        shop_name.setText(dataModel.getMerchantName());
        
        shop_address.setText(dataModel.getAddress());
        
        // if (!"".equals(dataModel.getBankTypeName()) &&
        // dataModel.getBankTypeName() != null)
        // {
        bank_name.setText(dataModel.getBank());
        // }
        // else
        // {
        // int bankId = dataModel.getBankType();
        // List<ShopBankBaseInfo> bankList =
        // ShopBankDataDB.getInstance().queryByID(String.valueOf(bankId));
        // bank_name.setText(bankList.get(0).bankName);
        // }
        
        branch.setText(dataModel.getBranchBankName());
        et_tel.setText(dataModel.getPrincipalPhone());
        bayee_name.setText(dataModel.getPrincipal());
        bank_num.setText(dataModel.getBank());
        String identityNo = dataModel.getIdentityNo();
        citys.setText(dataModel.getCity());
        personal_id.setText(getIdentityNo(identityNo));
        province.setText(dataModel.getProvince() + dataModel.getCity());
        if (dataModel.getEmil() != null)
        {
            tel.setText(dataModel.getEmil());
        }
        bunisOne.setText(dataModel.getMerchantTypeName());
        // mapPicUrls.put("one", merchant.getIdCardJustPic());
        // mapPicUrls.put("two", merchant.getLicensePic());
        String url = ApiConstant.BASE_URL_PORT + ApiConstant.DONLOAD_IMAGE;
        finalBitmap.display(id_people_photo, url + dataModel.getIdCardJustPic());
        Log.i("hehui", "url-->" + url + dataModel.getIdCardJustPic());
        
        finalBitmap.display(id_down_photo, url + dataModel.getLicensePic());
        
        finalBitmap.display(id_code, url + dataModel.getIdCode());
        
        // int bunisOneID = dataModel.getMerchantType();
        
        // 商业 基本数据，根据id查询名称
        // List<ShopBaseDataInfo> menrchList =
        // ShopBaseDataDB.getInstance().queryByParentID(String.valueOf(bunisOneID));
        // 根据子类查询父类
        // ShopBaseDataInfo shopBaseDataInfo =
        // ShopBaseDataDB.getInstance().queryBySonId(String.valueOf(bunisOneID));
        // bunisOne.setText(shopBaseDataInfo.getTypeName());
        // if (menrchList != null && menrchList.size() > 0)
        // {
        // bunisOne.setText(menrchList.get(0).getTypeName());
        // }
        
        // twoText.setText(dataModel.getMerchantTypeName());
        
    }
    
    /** 将身份证中间16位用*号代替
     * <功能详细描述>
     * @param identityNo
     * @return
     * @see [类、类#方法、类#成员]
     */
    private StringBuilder getIdentityNo(String identityNo)
    {
        StringBuilder result = new StringBuilder();
        if (identityNo != null && identityNo.length() > 15)
        {
            StringBuilder temp = new StringBuilder();
            for (int a = 0; a < identityNo.length() - 2; a++)
            {
                temp.append("*");
            }
            result.append(identityNo.charAt(0)).append(temp).append(identityNo.charAt(identityNo.length() - 1));
        }
        return result;
    }
    
    /**
     * 记录输入的值 <功能详细描述>
     * 
     * @see [类、类#方法、类#成员]
     */
    private void initValue()
    {
        shop_name.setText(sharedPreferences.getString("shop_name", ""));
        shop_address.setText(sharedPreferences.getString("shop_address", ""));
        bank_name.setText(sharedPreferences.getString("bank_name", ""));
        branch.setText(sharedPreferences.getString("branch", ""));
        bayee_name.setText(sharedPreferences.getString("bayee_name", ""));
        bank_num.setText(sharedPreferences.getString("bank_num", ""));
        personal_id.setText(sharedPreferences.getString("personal_id", ""));
        
        bunisOne.setText(sharedPreferences.getString("bunisOne", ""));
        
        citys.setText(sharedPreferences.getString("citys", ""));
        
        bank_city.setText(sharedPreferences.getString("bank_city", ""));
        
        province.setText(sharedPreferences.getString("province", ""));
        
        bank_province.setText(sharedPreferences.getString("bank_province", ""));
        
    }
    
    // 检查数据的合法性
    private boolean checkDataValid()
    {
        if (!VerifyUtil.verifyValid(tel.getText().toString().trim(), GlobalConstant.EMAIL))
        {
            showToastInfo(R.string.tx_email_detai);
            tel.setFocusable(true);
            return false;
        }
        // if (!VerifyUtil.verifyValid(bank_num.getText().toString().trim(),
        // GlobalConstant.BANKNUM))
        // {
        // ToastHelper.showInfo("请输入19位银行卡号");
        // // bank_num.setText("");
        // bank_num.setFocusable(true);
        // return false;
        // }
        if (!VerifyUtil.verifyValid(personal_id.getText().toString().trim(), GlobalConstant.IDCARD))
        {
            showToastInfo(R.string.tx_id_fomart);
            // personal_id.setText("");
            personal_id.setFocusable(true);
            return false;
        }
        return true;
        
    }
    
    // 检查为空
    private boolean checkExitisNULl()
    {
        if (isAbsoluteNullStr(bunisOne.getText().toString()))
        {
            showToastInfo(R.string.account_info);
            return false;
        }
        
        if (isAbsoluteNullStr(shop_name.getText().toString()))
        {
            showToastInfo(R.string.tx_shop_name);
            shop_name.setFocusable(true);
            return false;
        }
        
        if (isAbsoluteNullStr(sharedPreferences.getString("province", "")) && !province.getText().toString().equals(""))
        {
            showToastInfo(R.string.tx_province);
            return false;
        }
        
        if (isAbsoluteNullStr(shop_address.getText().toString()))
        {
            showToastInfo(R.string.tx_shop_address);
            shop_address.setFocusable(true);
            return false;
        }
        
        if (isAbsoluteNullStr(tel.getText().toString()))
        {
            showToastInfo(R.string.tx_email);
            tel.setFocusable(true);
            return false;
        }
        
        if (isAbsoluteNullStr(bank_name.getText().toString()))
        {
            showToastInfo(R.string.tx_bank_name);
            bank_name.setFocusable(true);
            return false;
        }
        
        if (isAbsoluteNullStr(branch.getText().toString()))
        {
            showToastInfo(R.string.tx_branch);
            branch.setFocusable(true);
            return false;
        }
        
        if (isAbsoluteNullStr(bayee_name.getText().toString()))
        {
            showToastInfo(R.string.tx_bayee_name);
            bayee_name.setFocusable(true);
            return false;
        }
        
        // if (isAbsoluteNullStr(bank_num.getText().toString()))
        // {
        // showToastInfo("银行卡账号不能为空");
        // bank_num.setFocusable(true);
        // return false;
        // }
        
        if (isAbsoluteNullStr(personal_id.getText().toString()))
        {
            showToastInfo(R.string.tx_personal_id);
            personal_id.setFocusable(true);
            return false;
        }
        
        // mapPicUrls.put("one", sharedPreferences.getString("id_people_photo",
        // ""));
        // mapPicUrls.put("two", sharedPreferences.getString("id_down_photo",
        // "")); // 运营照片
        // mapPicUrls.put("three", sharedPreferences.getString("id_code", ""));
        
        if (sharedPreferences.getString("id_people_photo", "").equals("")
            || sharedPreferences.getString("id_people_photo", "") == null)
        {
            showToastInfo(R.string.tx_id_people_photo);
            return false;
        }
        
        if (sharedPreferences.getString("id_down_photo", "").equals("")
            || sharedPreferences.getString("id_down_photo", "") == null)
        {
            showToastInfo(R.string.tx_id_down_photo);
            return false;
        }
        
        if (sharedPreferences.getString("id_code", "").equals("") || sharedPreferences.getString("id_code", "") == null)
        {
            showToastInfo(R.string.tx_id_code);
            return false;
        }
        
        return true;
        
    }
    
    // private final OnFocusChangeListener listener = new
    // OnFocusChangeListener()
    // {
    // @Override
    // public void onFocusChange(View v, boolean hasFocus)
    // {
    // switch (v.getId())
    // {
    // case R.id.shop_name:
    // sharedPreferences.edit().putString("shop_name",
    // shop_name.getText().toString()).commit();
    // break;
    // case R.id.shop_address:
    // sharedPreferences.edit().putString("shop_address",
    // shop_address.getText().toString()).commit();
    // break;
    // case R.id.bank_name:
    // sharedPreferences.edit().putString("bank_name",
    // bank_name.getText().toString()).commit();
    // break;
    // case R.id.branch:
    // sharedPreferences.edit().putString("branch",
    // branch.getText().toString()).commit();
    // break;
    // case R.id.bayee_name:
    // sharedPreferences.edit().putString("bayee_name",
    // bayee_name.getText().toString()).commit();
    // break;
    // case R.id.bank_num:
    // sharedPreferences.edit().putString("bank_num",
    // bank_num.getText().toString()).commit();
    // break;
    // case R.id.personal_id:
    // sharedPreferences.edit().putString("personal_id",
    // personal_id.getText().toString()).commit();
    // break;
    // case R.id.tel:
    // sharedPreferences.edit().putString("tel",
    // tel.getText().toString()).commit();
    // break;
    // }
    //
    // }
    // };
    
    // 点击清空内容
    private OnClickListener clearImage = new OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            switch (v.getId())
            {
                case R.id.iv_shop_name:
                    shop_name.setText("");
                    break;
                case R.id.iv_shop_address:
                    shop_address.setText("");
                    break;
                case R.id.iv_tel:
                    tel.setText("");
                    break;
                case R.id.iv_branch:
                    branch.setText("");
                    break;
                case R.id.iv_bayee_name:
                    bayee_name.setText("");
                    break;
                case R.id.iv_bank_num:
                    bank_num.setText("");
                    break;
                case R.id.iv_personal_id:
                    personal_id.setText("");
                    break;
            }
        }
    };
    
    /**
     * 上传图片 <功能详细描述>
     * 
     * @param view
     * @see [类、类#方法、类#成员]
     */
    public void onUploadImg(final View view)
    {
        view.setEnabled(false);
        // 从相册选择照片
        final String status = Environment.getExternalStorageState();
        takImgDialog =
            new SelectTowDialog(ShopkeeperActivity.this, getString(R.string.dialog_choice_photo), new String[] {
                getString(R.string.tx_album), getString(R.string.tx_take_pic)},
                new SelectTowDialog.OnSelectTowListener()
                {
                    @Override
                    public void onTow()
                    {
                        try
                        {
                            // 是否有SD卡
                            if (status.equals(Environment.MEDIA_MOUNTED))
                            {
                                takeImg(view);
                            }
                            else
                            {
                                Toast.makeText(ShopkeeperActivity.this,
                                    getString(R.string.tx_sd_pic),
                                    Toast.LENGTH_SHORT).show();
                            }
                            view.setEnabled(true);
                            takImgDialog.dismiss();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                    
                    @Override
                    public void onOne()
                    {
                        try
                        {
                            if (status.equals(Environment.MEDIA_MOUNTED))
                            {
                                startCamrae(view);
                            }
                            else
                            {
                                Toast.makeText(ShopkeeperActivity.this,
                                    getString(R.string.tx_sd_pic),
                                    Toast.LENGTH_SHORT).show();
                            }
                            view.setEnabled(true);
                            takImgDialog.dismiss();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                });
        DialogHelper.resize(this, takImgDialog);
        takImgDialog.show();
        takImgDialog.setCanceledOnTouchOutside(true);
        view.setEnabled(true);
        
    }
    
    /**
     * 获取相册图片
     * 
     * @throws Exception
     */
    public void takeImg(View view)
        throws Exception
    {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setDataAndType(MediaStore.Images.Media.INTERNAL_CONTENT_URI, "image/*");
        // intent.setType("image/*");
        // String root = AppHelper.getImgCacheDir();
        // imageUrl = root + String.valueOf(new Date().getTime()) + ".jpg";
        // tempFile = new File(imageUrl);
        
        ImageView imageView = (ImageView)view;
        
        imageID = imageView.getId();
        HandlerManager.notifyMessage(HandlerManager.SHOWPIC, HandlerManager.SHOWPIC, imageView.getId());
        
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
    }
    
    String imageUrl;
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
        {
            switch (requestCode)
            {
            // 相册获取图片
                case REQUEST_CODE_TAKE_PICTURE:
                    if (ShopkeeperNextActivity.IMAGE_PREVIEW)
                    {
                        originalUri = data.getData();
                        Intent intent = new Intent();
                        intent.setData(originalUri);
                        intent.setClass(this, ImagePreviewActivity.class);
                        
                        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
                        return;
                    }
                    else
                    {
                        ShopkeeperNextActivity.IMAGE_PREVIEW = true;
                    }
                    
                    if (originalUri == null)
                    {
                        break;
                    }
                    // 取消
                    if (!cancel_perview)
                    {
                        cancel_perview = true;
                        break;
                    }
                    ImageView imageView = (ImageView)findViewById(imageID);
                    
                    String path = getPicPath(originalUri);
                    originalUri = null;
                    Bitmap bitmap = null;
                    
                    try
                    {
                        
                        // File file = new File(path);
                        // String newUrlStr = path.substring(0,
                        // path.lastIndexOf('/'));
                        // newPath = newUrlStr + "/" + String.valueOf(new
                        // Date().getTime()) + ".jpg";
                        // if (file.exists())
                        // {
                        // newFile = new File(newPath);
                        // file.renameTo(newFile);
                        // }
                        // fos = new FileOutputStream(newFile);
                        if (path != null)
                        {
                            File file = new File(path);
                            if (file.exists() && file.length() / 1024 > 100)
                            {
                                bitmap = ImagePase.createBitmap(path, ImagePase.bitmapSize_Image);
                            }
                            else
                            {
                                // bitmap = BitmapFactory.decodeFile(path);
                                bitmap = ImagePase.readBitmapFromStream(path);
                            }
                            // bitmap = BitmapFactory.decodeFile(path);
                        }
                    }
                    catch (Exception e1)
                    {
                        e1.printStackTrace();
                    }
                    
                    if (bitmap != null)
                    {
                        imageView.setImageBitmap(bitmap);
                    }
                    switch (imageID)
                    {
                        case R.id.id_down_photo:
                            sharedPreferences.edit().putString("id_down_photo", path).commit();
                            break;
                        case R.id.id_people_photo:
                            sharedPreferences.edit().putString("id_people_photo", path).commit();
                            break;
                        case R.id.id_code:
                            sharedPreferences.edit().putString("id_code", path).commit();
                            break;
                        default:
                            break;
                    }
                    takImgDialog.dismiss();
                    break;
                
                // 拍照
                case REQUEST_CODE_CAPTURE_PICTURE:
                    
                    ImageView image = (ImageView)findViewById(imageID);
                    String pathPhoto = getPicPath(originalUri);
                    Bitmap bitmap_pci = null;
                    // 得到缩略图的路径
                    try
                    {
                        
                        File file = new File(pathPhoto);
                        if (file.exists() && file.length() / 1024 > 100)
                        {
                            bitmap_pci = ImagePase.createBitmap(pathPhoto, ImagePase.bitmapSize_Image);
                        }
                        else
                        {
                            bitmap_pci = ImagePase.readBitmapFromStream(pathPhoto);
                        }
                        
                        if (bitmap_pci != null)
                        {
                            image.setImageBitmap(bitmap_pci);
                        }
                        else
                        {
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    switch (imageID)
                    {
                        case R.id.id_down_photo:
                            sharedPreferences.edit().putString("id_down_photo", pathPhoto).commit();
                            break;
                        case R.id.id_people_photo:
                            sharedPreferences.edit().putString("id_people_photo", pathPhoto).commit();
                            break;
                        case R.id.id_code:
                            sharedPreferences.edit().putString("id_code", pathPhoto).commit();
                            break;
                        default:
                            break;
                    }
                    takImgDialog.dismiss();
                    break;
                default:
                    break;
            }
            
        }
        else
        {
            // Toast.makeText(this, "获取照片失败,重新获取！", Toast.LENGTH_LONG).show();
            return;
        }
    }
    
    /**
     * 
     * 返回图片地址
     * 
     * @param originalUri
     * @return String
     */
    @SuppressLint("NewApi")
    public String getPicPath(Uri originalUri)
    {
        ContentResolver mContentResolver = ShopkeeperActivity.this.getContentResolver();
        String originalPath = null;
        if ((originalUri + "").contains("/sdcard"))
        {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/sdcard"));
        }
        else if ((originalUri + "").contains("/data"))
        {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/data"));
        }
        else if ((originalUri + "").contains("/storage")) // 小米 手机 适配
        {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/storage"));
        }
        else
        {
            
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT
                && originalUri.toString().contains("documents"))
            {
                String wholeID = DocumentsContract.getDocumentId(originalUri);
                String id = wholeID.split(":")[1];
                String[] column = {MediaStore.Images.Media.DATA};
                String sel = MediaStore.Images.Media._ID + " = ?";
                Cursor cursor =
                    mContentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column,
                        sel,
                        new String[] {id},
                        null);
                int columnIndex = cursor.getColumnIndex(column[0]);
                if (cursor.moveToFirst())
                {
                    originalPath = cursor.getString(columnIndex);
                }
                cursor.close();
            }
            else
            {
                
                Cursor cursor = mContentResolver.query(originalUri, null, null, null, null);
                if (cursor != null)
                {
                    cursor.moveToFirst();
                    originalPath = cursor.getString(1);
                    cursor.close();
                }
            }
            
        }
        
        return originalPath;
    }
    
    /**
     * 拍照 <功能详细描述>
     * 
     * @throws Exception
     * @see [类、类#方法、类#成员]
     */
    private void startCamrae(View view)
        throws Exception
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String root = AppHelper.getImgCacheDir();
        imageUrl = root + String.valueOf(new Date().getTime()) + ".jpg";
        tempFile = new File(imageUrl);
        originalUri = Uri.fromFile(tempFile);
        
        ImageView imageView = (ImageView)view;
        
        imageID = imageView.getId();
        
        HandlerManager.notifyMessage(HandlerManager.SHOWPIC, HandlerManager.SHOWPIC, imageView.getId());
        intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, originalUri);
        startActivityForResult(intent, REQUEST_CODE_CAPTURE_PICTURE);
    }
    
    /**
     * 设置监听 <功能详细描述>
     * 
     * @see [类、类#方法、类#成员]
     */
    private void setLister()
    {
        
        // shop_name.setOnFocusChangeListener(listener);
        // shop_address.setOnFocusChangeListener(listener);
        // bank_name.setOnFocusChangeListener(listener);
        // branch.setOnFocusChangeListener(listener);
        // bayee_name.setOnFocusChangeListener(listener);
        // bank_num.setOnFocusChangeListener(listener);
        // personal_id.setOnFocusChangeListener(listener);
        // tel.setOnFocusChangeListener(listener);
        
        id_down_photo_look.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                
                String down_photo = sharedPreferences.getString("id_down_photo", "");
                
                if (down_photo != null && !down_photo.equals(""))
                {
                    showPage(ShowPciActivity.class, down_photo);
                }
                else
                {
                    if (dataModel != null && !dataModel.getLicensePic().equals("null")
                        && !dataModel.getLicensePic().equals(""))
                    {
                        showPage(ShowPciActivity.class, dataModel.getLicensePic());
                    }
                }
                
            }
        });
        
        id_people_photo_look.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                
                String people_photo = sharedPreferences.getString("id_people_photo", "");
                if (people_photo != null && !people_photo.equals(""))
                {
                    showPage(ShowPciActivity.class, people_photo);
                }
                else
                {
                    if (dataModel != null && !dataModel.getIdCardJustPic().equals("null"))
                    {
                        showPage(ShowPciActivity.class, dataModel.getIdCardJustPic());
                    }
                }
            }
        });
        
        id_code_look.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                String people_photo = sharedPreferences.getString("id_code", "");
                if (people_photo != null && !people_photo.equals(""))
                {
                    showPage(ShowPciActivity.class, people_photo);
                }
                else
                {
                    if (dataModel != null && !dataModel.getIdCode().equals("null"))
                    {
                        showPage(ShowPciActivity.class, dataModel.getIdCode());
                    }
                }
            }
        });
        
        id_code.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                try
                {
                    onUploadImg(v);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    Log.e("hehui", "takeImg-->" + e);
                }
            }
        });
        
        id_down_photo.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                try
                {
                    onUploadImg(v);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    Log.e("hehui", "takeImg-->" + e);
                }
                
            }
        });
        
        id_people_photo.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                try
                {
                    onUploadImg(v);
                }
                catch (Exception e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Log.e("hehui", "id_people_photo takeImg --> " + e);
                }
            }
        });
        
        // 银行数据
        bankLayout.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                List<ShopBankBaseInfo> bankList = ShopBankDataDB.getInstance().query();
                List<City> bankCities = new ArrayList<City>();
                // 转换成对应的 city对象，同一复用
                for (ShopBankBaseInfo info : bankList)
                {
                    City city = new City();
                    city.setCity(info.bankName);
                    city.setNumber(info.bankId);
                    bankCities.add(city);
                }
                
                HelpUI.initPopWindow(getApplicationContext(), bankCities, bank_name, HelpUI.SHOP_BANK, handler);
                
            }
        });
        
        // 商户行业数据
        bunisLayout.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                List<ShopBaseDataInfo> baseInfo = ShopBaseDataDB.getInstance().query();
                List<City> bunisArrs = new ArrayList<City>();
                // 转换成对应的 city对象，同一复用
                for (ShopBaseDataInfo info : baseInfo)
                {
                    City city = new City();
                    city.setNumber(info.getTypeno());
                    city.setCity(info.getName());
                    
                    bunisArrs.add(city);
                    
                }
                HelpUI.initPopWindow(getApplicationContext(), bunisArrs, bunisOne, HelpUI.SHOP_DATA, handler);
                
            }
        });
        
        towLayout.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (shopBaseTwo != null && shopBaseTwo.size() > 0)
                {
                    HelpUI.initPopWindow(getApplicationContext(), shopBaseTwo, twoText, HelpUI.TAG_MERCHANT, handler);
                }
                sharedPreferences.edit().putString("twoText", twoText.getText().toString()).commit();
            }
        });
        
        btn_next.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                // 检查是否为空
                if (!checkExitisNULl())
                    return;
                if (!checkDataValid())
                    return;
                
                // 下一步跳转到 完善资料二界面 保存数据到sharedPreferences
                final MerchantTempDataModel merchant = saveData();
                
                DialogInfo dialogInfo =
                    new DialogInfo(ShopkeeperActivity.this, "温馨提示", "您的资料已填写完成，是否确认并提交审核！", "确定",
                        DialogInfo.SUBMIT_SHOP, new DialogInfo.HandleBtn()
                        {
                            
                            @Override
                            public void handleOkBtn()
                            {
                                submitData(merchant, MainApplication.userId);
                            }
                            
                            @Override
                            public void handleCancleBtn()
                            {
                                // TODO Auto-generated method stub
                                
                            }
                        }, null);
                DialogHelper.resize(ShopkeeperActivity.this, dialogInfo);
                dialogInfo.show();
                
                // if (dataModel != null)
                // {
                // Bundle bundle = new Bundle();
                // bundle.putSerializable("merchantDataModel", dataModel);
                //
                // showPage(ShopkeeperNextActivity.class, bundle);
                // }
                // else
                // {
                // showPage(ShopkeeperNextActivity.class);
                // }
                //
                // MainApplication.listActivities.add(ShopkeeperActivity.this);
            }
        });
        // 选择省
        
        province.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                CityDao dao = CityDao.getInstance(getApplicationContext());
                provinces = dao.getAllProvince();
                HelpUI.initPopWindow(getApplicationContext(), provinces, province, HelpUI.TAG_PROVI, handler);
            }
            
        });
        // // 选择市级
        citys.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (!"".equals(region_id) && null != region_id)
                {
                    HelpUI.initPopWindow(getApplicationContext(), shopCityArrs, citys, HelpUI.CITYS, handler);
                }
                //                else
                //                // 默认是广东内的城市
                //                {
                //                    // 截取region_id 前两位
                //                    
                //                    shopCityArrs = dao.getAllCityByID("19");
                //                    
                //                    HelpUI.initPopWindow(getApplicationContext(), shopCityArrs, citys, HelpUI.CITYS, handler);
                //                }
                
            }
            
        });
        
        // 银行选择身份
        bank_province.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                provinces = dao.getAllProvince();
                HelpUI.initPopWindow(getApplicationContext(), provinces, bank_province, HelpUI.BANK_CITY, handler);
                
            }
        });
        
        // 银行 选择市级城市
        bank_city.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                
                if (!"".equals(region_id) && null != region_id)
                {
                    
                    HelpUI.initPopWindow(getApplicationContext(), bankCityArrs, bank_city, HelpUI.TAG_CITY, handler);
                }
                else
                // 默认是广东内的城市
                {
                    
                    bankCityArrs = dao.getAllCityByID("19");
                    
                    HelpUI.initPopWindow(getApplicationContext(), bankCityArrs, bank_city, HelpUI.TAG_CITY, handler);
                }
            }
        });
        
    }
    
    /**
     * 提交资料完善信息 <功能详细描述>
     * 
     * @param merchant
     * @param userID
     * @see [类、类#方法、类#成员]
     */
    private void submitData(final MerchantTempDataModel merchant, long userID)
    {
        // if (userID == 0)
        // {
        // return;
        // }
        
        // 提交完善资料
        PersonalManager.shopAddData(String.valueOf(userID), merchant, new UINotifyListener<Boolean>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                
                showLoading(false, getString(R.string.public_submitting));
            }
            
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dismissLoading();
                if (object != null)
                {
                    // showToastInfo(object.toString());
                    
                    ShopkeeperActivity.this.runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            //                            DialogInfoSdk dialogInfo =
                            //                                new DialogInfoSdk(ShopkeeperActivity.this, ShopkeeperActivity.this.getResources()
                            //                                    .getString(Resourcemap.getById_title_prompt()), "" + object.toString(), "确定",
                            //                                    DialogInfoSdk.FLAG, null);
                            
                            //                            DialogHelper.resize(ShopkeeperActivity.this, dialogInfo);
                            //                            dialogInfo.show();
                        }
                    });
                }
            }
            
            @Override
            public void onSucceed(Boolean result)
            {
                super.onSucceed(result);
                dismissLoading();
                if (result)
                {
                    
                    showToastInfo(R.string.tx_submit_succ);
                    // 把数据更给 商户添加界面
                    
                    // 更新本地数据库记录
                    // UserModel userModel;
                    // try
                    // {
                    // // sharedPreferencesOne.edit().clear().commit();
                    // userModel =
                    // UserInfoDB.getInstance().queryUserByName(MainApplication.userName);
                    // userModel.setExistData(1);
                    // UserInfoDB.getInstance().update(userModel);
                    // /** 把图片用map给存储进去*/
                    Map<String, String> mapPicUrls = new HashMap<String, String>();
                    
                    // merchant.setLicensePic(sharedPreferences.getString("id_down_photo",
                    // ""));
                    // //
                    // merchant.setIdCardJustPic(sharedPreferences.getString("id_people_photo",
                    // ""));
                    
                    mapPicUrls.put("one", sharedPreferences.getString("id_people_photo", ""));
                    mapPicUrls.put("two", sharedPreferences.getString("id_down_photo", "")); // 运营照片
                    mapPicUrls.put("three", sharedPreferences.getString("id_code", ""));
                    // // 上传图片
                    if (mapPicUrls.size() > 0)
                    {
                        uploadPic(mapPicUrls, merchant);
                    }
                    
                    // 清除sharedPreferences缓存信息
                    CleanManager.cleanSharedPreference(ShopkeeperActivity.this, "dataOnes");
                    
                    Editor editor = sharedPreferences.edit();
                    editor.clear();
                    editor.commit();
                    
                    btn_next.setVisibility(View.GONE);
                    setEnbaled(false);
                    
                    // }
                    // catch (SQLException e)
                    // {
                    // Log.e("hehui", "update local data failed -->" +
                    // e.getMessage());
                    // return;
                    // }
                }
                
            }
        });
    }
    
    /**
     * 上传图片 <功能详细描述>
     * 
     * @see [类、类#方法、类#成员]
     */
    private void uploadPic(Map<String, String> mapPicUrls, MerchantTempDataModel merchant)
    {
        // int len = gridList.size();
        // for (int i = 0; i < len; i++)
        // {
        // GridViewBean bean = gridList.get(i);
        PersonalManager.getInstance().uploadPhoto(mapPicUrls, merchant, new UINotifyListener<Boolean>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                // titleBar.setRightLodingVisible(true);
            }
            
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                if (object != null)
                {
                    showToastInfo(object.toString());
                }
            }
            
            @Override
            public void onSucceed(Boolean result)
            {
                super.onSucceed(result);
                if (result)
                {
                    // titleBar.setRightLodingVisible(false, false);
                }
                
            }
        });
        // }
        
    }
    
    /**
     * <一句话功能简述>保存数据 <功能详细描述>
     * 
     * @see [类、类#方法、类#成员]
     */
    protected MerchantTempDataModel saveData()
    {
        Editor editor = sharedPreferences.edit();
        editor.putString("personal_id", personal_id.getText().toString()).commit();
        editor.putString("shop_name", shop_name.getText().toString()).commit();
        editor.putString("shop_address", shop_address.getText().toString()).commit();
        editor.putString("bank_name", bank_name.getText().toString()).commit();
        editor.putString("branch", branch.getText().toString()).commit();
        editor.putString("bayee_name", bayee_name.getText().toString()).commit();
        editor.putString("bank_num", bank_num.getText().toString()).commit();
        editor.putString("tel", tel.getText().toString()).commit();
        editor.putString("twoText", twoText.getText().toString()).commit();
        
        MerchantTempDataModel merchant = new MerchantTempDataModel();
        if (dataModel != null)
        {
            merchant.setProvince(province.getText().toString());
            merchant.setCity(citys.getText().toString());
            merchant.setMerchantTypeName(bunisOne.getText().toString());
            // merchant.setLicensePic(dataModel.getLicensePic());
            // merchant.setIdCardJustPic(dataModel.getIdCardJustPic());
        }
        else
        {
            merchant.setProvince(sharedPreferences.getString("province", ""));
            merchant.setCity(sharedPreferences.getString("citys", ""));
            merchant.setMerchantTypeName(sharedPreferences.getString("bunisOne", ""));
            // merchant.setLicensePic(sharedPreferences.getString("id_down_photo",
            // ""));
            // merchant.setIdCardJustPic(sharedPreferences.getString("id_people_photo",
            // ""));
        }
        merchant.setMerchantName(shop_name.getText().toString());
        merchant.setAddress(shop_address.getText().toString());
        merchant.setEmil(tel.getText().toString());
        merchant.setBank(sharedPreferences.getString("bank_name", ""));
        merchant.setBranchBankName(branch.getText().toString());
        merchant.setBankUserName(bayee_name.getText().toString());
        merchant.setBankNum(bank_num.getText().toString());
        merchant.setIdentityNo(personal_id.getText().toString());
        
        merchant.setMerchantId(MainApplication.getMchId());
        
        return merchant;
    }
    
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        // saveData(); //保存数据
    }
    
    /**
     * 页面参数初始化 <功能详细描述>
     * 
     * @see [类、类#方法、类#成员]
     */
    private void initView()
    {
        tv_mch_prompt = getViewById(R.id.tv_prompt);
        //        tv_mch_prompt.setPadding(0,
        //            DisplayUtil.dip2Px(ShopkeeperActivity.this, 70),
        //            0,
        //            DisplayUtil.dip2Px(ShopkeeperActivity.this, 10));
        et_tel = getViewById(R.id.tv_tel);
        //        et_tel.setText(MainApplication.phone);
        tv_mch = getViewById(R.id.tv_mch);
        tv_mch.setText(MainApplication.getMchId());
        
        id_code_look = getViewById(R.id.id_code_look);
        
        id_code = getViewById(R.id.id_code);
        
        statusTxt = getViewById(R.id.statusTxt);
        
        statuImg = getViewById(R.id.statuImg);
        
        id_down_photo_look = getViewById(R.id.id_down_photo_look);
        
        id_people_photo_look = getViewById(R.id.id_people_photo_look);
        
        id_people_photo = getViewById(R.id.id_people_photo);
        
        id_down_photo = getViewById(R.id.id_down_photo);
        
        btn_next = (Button)findViewById(R.id.btn_next);
        
        province = (TextView)findViewById(R.id.province);
        
        citys = (TextView)findViewById(R.id.citys);
        
        bank_city = (TextView)findViewById(R.id.bank_city);
        
        bank_province = (TextView)findViewById(R.id.bank_province);
        
        shop_name = (EditText)findViewById(R.id.shop_name);
        
        shop_address = (EditText)findViewById(R.id.shop_address);
        
        bank_name = (TextView)findViewById(R.id.bank_name);
        
        branch = (EditText)findViewById(R.id.branch);
        
        bayee_name = (EditText)findViewById(R.id.bayee_name);
        
        bank_num = (EditText)findViewById(R.id.bank_num);
        
        personal_id = (EditText)findViewById(R.id.personal_id);
        
        tel = (EditText)findViewById(R.id.tel);
        
        bunisOne = getViewById(R.id.bunisOne);
        
        bunisLayout = getViewById(R.id.bunisLayout);
        
        towLayout = getViewById(R.id.towLayout);
        
        twoText = getViewById(R.id.twoText);
        
        bankLayout = getViewById(R.id.bankLayout);
        
        twoText_iv = getViewById(R.id.twoText_iv);
        bunisOne_iv = getViewById(R.id.bunisOne_iv);
        
        iv_shopname_promt = getViewById(R.id.iv_shopname_promt);
        iv_shopaddress_promt = getViewById(R.id.iv_shopaddress_promt);
        iv_shophone_promt = getViewById(R.id.iv_shophone_promt);
        iv_shopbranch_promt = getViewById(R.id.iv_shopbranch_promt);
        iv_bayeeName_promt = getViewById(R.id.iv_bayeeName_promt);
        iv_bankNum_promt = getViewById(R.id.iv_bankNum_promt);
        iv_personalId_promt = getViewById(R.id.iv_personalId_promt);
        
        iv_shop_name = getViewById(R.id.iv_shop_name);
        iv_shop_address = getViewById(R.id.iv_shop_address);
        iv_tel = getViewById(R.id.iv_tel);
        iv_branch = getViewById(R.id.iv_branch);
        iv_bayee_name = getViewById(R.id.iv_bayee_name);
        iv_bank_num = getViewById(R.id.iv_bank_num);
        iv_personal_id = getViewById(R.id.iv_personal_id);
        
        iv_shop_name.setOnClickListener(clearImage);
        iv_shop_address.setOnClickListener(clearImage);
        iv_tel.setOnClickListener(clearImage);
        iv_branch.setOnClickListener(clearImage);
        iv_bayee_name.setOnClickListener(clearImage);
        iv_bank_num.setOnClickListener(clearImage);
        iv_personal_id.setOnClickListener(clearImage);
        
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        {
            
            @Override
            public void onExecute(CharSequence s, int start, int before, int count)
            {
            }
            
            @Override
            public void onAfterTextChanged(Editable s)
            {
                if (shop_name.isFocused())
                {
                    if (shop_name.getText().toString().length() > 0)
                    {
                        iv_shop_name.setVisibility(View.VISIBLE);
                        // iv_shopname_promt.setVisibility(View.GONE);
                    }
                    else
                    {
                        iv_shop_name.setVisibility(View.GONE);
                        // iv_shopname_promt.setVisibility(View.VISIBLE);
                    }
                }
                if (shop_address.isFocused())
                {
                    if (shop_address.getText().toString().length() > 0)
                    {
                        iv_shop_address.setVisibility(View.VISIBLE);
                        //iv_shopaddress_promt.setVisibility(View.GONE);
                    }
                    else
                    {
                        iv_shop_address.setVisibility(View.GONE);
                        //iv_shopaddress_promt.setVisibility(View.VISIBLE);
                    }
                }
                if (tel.isFocused())
                {
                    if (tel.getText().toString().length() > 0)
                    {
                        iv_tel.setVisibility(View.VISIBLE);
                        //iv_shophone_promt.setVisibility(View.GONE);
                    }
                    else
                    {
                        iv_tel.setVisibility(View.GONE);
                        //iv_shophone_promt.setVisibility(View.VISIBLE);
                    }
                }
                if (branch.isFocused())
                {
                    if (branch.getText().toString().length() > 0)
                    {
                        iv_branch.setVisibility(View.VISIBLE);
                        //iv_shopbranch_promt.setVisibility(View.GONE);
                    }
                    else
                    {
                        iv_branch.setVisibility(View.GONE);
                        //iv_shopbranch_promt.setVisibility(View.VISIBLE);
                    }
                }
                if (bayee_name.isFocused())
                {
                    if (bayee_name.getText().toString().length() > 0)
                    {
                        iv_bayee_name.setVisibility(View.VISIBLE);
                        //iv_bayeeName_promt.setVisibility(View.GONE);
                    }
                    else
                    {
                        iv_bayee_name.setVisibility(View.GONE);
                        //iv_bayeeName_promt.setVisibility(View.VISIBLE);
                    }
                }
                if (bank_num.isFocused())
                {
                    if (bank_num.getText().toString().length() > 0)
                    {
                        iv_bank_num.setVisibility(View.VISIBLE);
                        iv_bankNum_promt.setVisibility(View.GONE);
                    }
                    else
                    {
                        iv_bank_num.setVisibility(View.GONE);
                        iv_bankNum_promt.setVisibility(View.VISIBLE);
                    }
                }
                if (personal_id.isFocused())
                {
                    if (personal_id.getText().toString().length() > 0)
                    {
                        iv_personal_id.setVisibility(View.VISIBLE);
                        //iv_personalId_promt.setVisibility(View.GONE);
                    }
                    else
                    {
                        iv_personal_id.setVisibility(View.GONE);
                        //iv_personalId_promt.setVisibility(View.VISIBLE);
                    }
                }
                
            }
            
        });
        
        shop_name.addTextChangedListener(editTextWatcher);
        shop_address.addTextChangedListener(editTextWatcher);
        tel.addTextChangedListener(editTextWatcher);
        branch.addTextChangedListener(editTextWatcher);
        bayee_name.addTextChangedListener(editTextWatcher);
        bank_num.addTextChangedListener(editTextWatcher);
        personal_id.addTextChangedListener(editTextWatcher);
        
    }
    
    /**
     * <一句话功能简述>初始化必填项图标 <功能详细描述>
     * 
     * @see [类、类#方法、类#成员]
     */
    private void initPromt()
    {
        if ("".equals(shop_name.getText().toString().trim()))
        {
            //iv_shopname_promt.setVisibility(View.VISIBLE);
        }
        else
        {
            //iv_shopname_promt.setVisibility(View.GONE);
        }
        
        if ("".equals(shop_address.getText().toString().trim()))
        {
            
            //iv_shopaddress_promt.setVisibility(View.VISIBLE);
        }
        else
        {
            
            //iv_shopaddress_promt.setVisibility(View.GONE);
        }
        
        if ("".equals(branch.getText().toString().trim()))
        {
            
            //iv_shopbranch_promt.setVisibility(View.VISIBLE);
        }
        else
        {
            
            //iv_shopbranch_promt.setVisibility(View.GONE);
        }
        
        if ("".equals(bayee_name.getText().toString().trim()))
        {
            
            //iv_bayeeName_promt.setVisibility(View.VISIBLE);
        }
        else
        {
            
            //iv_bayeeName_promt.setVisibility(View.GONE);
        }
        
        if ("".equals(bank_num.getText().toString().trim()))
        {
            
            iv_bankNum_promt.setVisibility(View.VISIBLE);
        }
        else
        {
            
            iv_bankNum_promt.setVisibility(View.GONE);
        }
        
        if ("".equals(tel.getText().toString().trim()))
        {
            //iv_shophone_promt.setVisibility(View.VISIBLE);//
        }
        
        else
        {
            
            //iv_shophone_promt.setVisibility(View.GONE);
        }
        
        if ("".equals(personal_id.getText().toString().trim()))
        {
            
            //iv_personalId_promt.setVisibility(View.VISIBLE);
        }
        else
        {
            
            //iv_personalId_promt.setVisibility(View.GONE);
        }
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_add_shop);
    }
}
