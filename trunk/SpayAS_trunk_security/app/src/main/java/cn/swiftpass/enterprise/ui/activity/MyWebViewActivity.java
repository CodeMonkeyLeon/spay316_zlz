package cn.swiftpass.enterprise.ui.activity;

import static android.webkit.WebView.enableSlowWholeDocumentDraw;
import static cn.swiftpass.enterprise.camera.CameraActivity.KEY_IMAGE_PATH;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.annotation.Nullable;

import com.example.common.sentry.SentryUtils;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.camera.CameraActivity;
import cn.swiftpass.enterprise.common.Constant;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants;
import cn.swiftpass.enterprise.ui.widget.ProgressWebView;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.Base64;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.ImageUtil;
import cn.swiftpass.enterprise.utils.LocaleUtils;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;
import cn.swiftpass.enterprise.utils.permissionutils.OnPermissionCallback;
import cn.swiftpass.enterprise.utils.permissionutils.Permission;
import cn.swiftpass.enterprise.utils.permissionutils.PermissionDialogUtils;

/**
 * Created by aijingya on 2020/9/17.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description:
 * @date 2020/9/17.14:52.
 */
public class MyWebViewActivity extends BaseActivity {

    public final static int REQUEST_CODE_CAMERA_TAKE_PICTURE = 2005;
    private ProgressWebView mWebView;
    private String loadUrl;//加载的url
    //登录态信息（如果请求需要带登录态的话）
    private String login_skey;
    private String login_sauthid;
    private String photoName;//拍照生成的图片名
    private Uri photoUri;//拍照生成的图片Uri
    private int mApiCode;//1：常规相机 2：特殊相机 3：注册成功，返回登录页 4：截图当前屏幕所有内容
    private int mCamereType;//1：拍正面时：Take a photo of the front of your ID. 2：拍反面时：Take a photo of the back of your ID.
    private String mCallbackName;//执行js后，回调方法，
    private String PicSign = null;   //图片名称的唯一标识，根据图片的内容生成
    private String viewPicSavePath; //保存的图片的完整路径名称

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }


    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //在activity的onCreate方法中的setContentView前加上一句enableSlowWholeDocumentDraw();
        // 意思为取消系统的智能绘制，当然这样之后性能会有所下降，是系统在5.0+版本上，Android对webview做了优化，
        // 为了减少内存占用以提高性能，因此在默认情况下会智能的绘制html中需要绘制的部分，其实就是当前屏幕展示的html内容，因此会出现未显示的图像是空白的
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            enableSlowWholeDocumentDraw();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_webview);
        loadUrl = getIntent().getStringExtra("loadUrl");
        login_skey = getIntent().getStringExtra("login_skey");
        login_sauthid = getIntent().getStringExtra("login_sauthid");

        initViews();
    }

    public void initViews() {
        mWebView = findViewById(R.id.web_view);
        //设置ProgressBar样式
        mWebView.setProgressbarDrawable(getResources().getDrawable(R.drawable.web_view_progress));
        //设置ProgressBar高度
        mWebView.setProgressbarHeight(5);
        mWebView.setDrawingCacheEnabled(true);

        //加载url网页
        setWebView(loadUrl);
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*if(mWebView != null){
            mWebView.onResume();
        }*/
    }

    @Override
    protected void onPause() {
       /* if(mWebView!=null){
            mWebView.pauseTimers();
            mWebView.onPause();
        }*/
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        //将webview清空，防止内存泄漏
        if (mWebView != null) {
            mWebView.clearCache(true);
            mWebView.stopLoading();
            mWebView.removeAllViews();
            mWebView.setWebViewClient(null);
            mWebView.setWebChromeClient(null);
            unregisterForContextMenu(mWebView);
            mWebView.destroy();
            mWebView = null;
        }
        super.onDestroy();
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setVisibility(View.GONE);
    }

    private void setWebView(String Url) {
        WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true); //开启JavaScript支持
        settings.setJavaScriptCanOpenWindowsAutomatically(true); // 支持通过JS打开新窗口
        settings.setAllowFileAccess(true); // 是否可访问本地文件，默认值 true
        settings.setSupportMultipleWindows(true);// 是否支持多窗口，默认值false
        settings.setAllowContentAccess(true); // 是否可访问Content Provider的资源，默认值 true
        settings.setAllowFileAccessFromFileURLs(false);//是否允许通过file url加载的Javascript读取本地文件，默认值 false
        settings.setAllowUniversalAccessFromFileURLs(false);//是否允许通过file url加载的Javascript读取全部资源(包括文件,http,https)，默认值 false
        settings.setSupportZoom(true); // 支持缩放
        settings.setCacheMode(WebSettings.LOAD_NORMAL);// 不使用缓存
        settings.setDefaultTextEncodingName("UTF-8");
        settings.setDomStorageEnabled(true);
        settings.setBuiltInZoomControls(true); // 支持缩放
        settings.setLoadWithOverviewMode(true); // 初始加载时，是web页面自适应屏幕
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // 5.0以上允许加载http和https混合的页面(5.0以下默认允许，5.0+默认禁止)
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        //各种分辨率适应
        int screenDensity = getResources().getDisplayMetrics().densityDpi;
        WebSettings.ZoomDensity zoomDensity = WebSettings.ZoomDensity.MEDIUM;
        switch (screenDensity) {
            case DisplayMetrics.DENSITY_LOW:
                zoomDensity = WebSettings.ZoomDensity.CLOSE;
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                zoomDensity = WebSettings.ZoomDensity.MEDIUM;
                break;
            case DisplayMetrics.DENSITY_HIGH:
                zoomDensity = WebSettings.ZoomDensity.FAR;
                break;
        }
        settings.setDefaultZoom(zoomDensity);
        settings.setUseWideViewPort(true);

        mWebView.requestFocus();
        mWebView.requestFocusFromTouch();
        //设置监听
        mWebView.setOnReceivedStateListener(new ProgressWebView.OnReceivedStateListener() {
            @Override
            public void onPageStart() {
                showLoading(R.string.public_loading, ParamsConstants.COMMON_LOADING);
            }

            @Override
            public void onOverrideUrlLoading(WebView view, String url) {
                //调用拨号程序
                if (url.startsWith("mailto:") || url.startsWith("geo:") || url.startsWith("tel:")) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                } else {
                    view.loadUrl(url);
                }
            }

            @Override
            public void onTitleReceived(String title) {
                dismissLoading(ParamsConstants.COMMON_LOADING);
              /*  if (title != null) {
                    titleBar.setTitle(title);
                }*/
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                dismissLoading(ParamsConstants.COMMON_LOADING);
//                titleBar.setTitle("");
            }
        });

        //添加header信息
        Map<String, String> heards = new HashMap<String, String>();
        heards = initHeaderInfo();
        mWebView.loadUrl(Url, heards);

        mWebView.addJavascriptInterface(new MyWebviewAndJsMutual(this), "spay_android");
    }

    public Map<String, String> initHeaderInfo() {
        Map<String, String> heads = new HashMap<>();

        String language = LocaleUtils.getLocaleLanguage();
        if (language.equalsIgnoreCase(Constant.LANG_CODE_JA_JP)) {//如果是日语，默认设置成英文
            language = Constant.LANG_CODE_EN_US;
        }
        heads.put("fp-lang", language);
        heads.put("SKEY", login_skey);
        heads.put("SAUTHID", login_sauthid);
        String key = "";
        try {
            key = "sp-" + "Android" + "-" + AppHelper.getUUID();
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }

        String value = AppHelper.getUpdateVersionCode(MainApplication.getInstance()) + ","
                + BuildConfig.bankCode + "," + language + ","
                + AppHelper.getAndroidSDKVersionName() + ","
                + DateUtil.formatTime(System.currentTimeMillis()) + ","
                + "Android";
        heads.put(key, value);

        return heads;
    }

    @Override
    public void onBackPressed() {
        //返回键处理
        if (mWebView != null && mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            finish();
        }
    }

    //Android调用js方法:第一种通过loadUrl 第二种通过evaluateJavascript（此方法只可用在4.4及以上版本）
    public void AndroidCallJs_Upload_Image(String callbackName, byte[] image) {
        String imageData64String = new String(Base64.encode(image));
        JSONObject json = new JSONObject();
        try {
            json.put("image", imageData64String);
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }

        if (callbackName != null) {
            String url = "javascript:" + callbackName + "(" + json + ")";
            // 因为该方法在 Android 4.4 版本才可使用，所以使用时需进行版本判断
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                //第一种方法
                mWebView.loadUrl(url);
                mWebView.refreshDrawableState();
            } else {
                //第二种方法
                mWebView.evaluateJavascript(url, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String value) {
                        //value js 的返回值
//                            Toast.makeText(MyWebViewActicity.this,value,Toast.LENGTH_SHORT).show();
                    }
                });
                mWebView.refreshDrawableState();
            }
        }
    }

    /**
     * 调用自定义的camera，可以移植到任何地方
     */
    public void gotoCamera(int apiCode, int camereType) {
        PermissionDialogUtils.requestPermission(MyWebViewActivity.this,
                Permission.Group.CAMERA, false,
                new OnPermissionCallback() {
                    @Override
                    public void onGranted(List<String> permissions, boolean all) {
                        if (all) {
                            CameraActivity.startCamera(MyWebViewActivity.this,
                                    REQUEST_CODE_CAMERA_TAKE_PICTURE, apiCode, camereType);
                        }
                    }
                });
    }


    //跳转到登录页
    public void startWelcomeActivity() {
        //结束掉webview就可以，因为是从登录页直接打开的webview流程
        finish();
    }

    //截取当前webview屏幕的所有内容并生成图片
    public void getCurrentPageBitmap() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        Map<String, String> params = new HashMap<String, String>();
        params.put("pic", "bitmap" + timeStamp);
        PicSign = SignUtil.getInstance().createSign(params, MainApplication.getInstance().getSignKey());

        if (TextUtils.isEmpty(PicSign)) {
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            // 适配android 10
            if (ImageUtil.isContainsCurrentImageFile(MyWebViewActivity.this, PicSign)) {
                showToastInfo(ToastHelper.toStr(R.string.instapay_save_success));
                return;
            }

        } else {
            // 适配android 9.0以及以下
            if (!TextUtils.isEmpty(viewPicSavePath)) {
                if (new File(viewPicSavePath).exists() && viewPicSavePath.contains(PicSign)) {
                    showToastInfo(ToastHelper.toStr(R.string.instapay_save_success));
                    return;
                }
            }
        }
        showLoading(R.string.loading, ParamsConstants.COMMON_LOADING);

        Executors.newSingleThreadExecutor().submit(new Runnable() {
            @Override
            public void run() {
                viewPicSavePath = ImageUtil.saveWebViewBitmapFile(PicSign, mWebView, MyWebViewActivity.this);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dismissLoading(ParamsConstants.COMMON_LOADING);
                    }
                });
                if (!TextUtils.isEmpty(viewPicSavePath)) {
                    showToastInfo(ToastHelper.toStr(R.string.instapay_save_success) + ":" + viewPicSavePath);
                } else {
                    showToastInfo(ToastHelper.toStr(R.string.instapay_save_failed));
                }
            }
        });
    }

    //调用自定义相机的回调代码----测试用，可以移植到任何地方
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_CAMERA_TAKE_PICTURE
                && null != data) {
            String path = data.getStringExtra(KEY_IMAGE_PATH);
            Uri photoUri = Uri.parse(path);
            if (photoUri != null) {
                //若生成了uri，则表示该文件添加成功
                //使用流将该uri中的内容写入字节数组即可
                byte[] image = null;
                try {
                    InputStream inputStream = getContentResolver().openInputStream(photoUri);
                    if (null == inputStream || 0 == inputStream.available()) {
                        return;
                    }
                    image = new byte[inputStream.available()];
                    inputStream.read(image);
                    inputStream.close();

                    if (image != null && image.length > 0) {
                        //如果上传的拖大于5M，则弹框提示不让上传
                        if (image.length > 5 * 1024 * 1024) {
                            showToastInfo(getStringById(R.string.bdo_check_image_size));
                        } else {
                            //拍照成功上传数据给H5
                            if (mCallbackName != null) {
                                AndroidCallJs_Upload_Image(mCallbackName, image);
                            }
                        }
                    }
                } catch (IOException e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                } catch (Exception e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                }
            }
        } else if (resultCode == Activity.RESULT_FIRST_USER && requestCode == REQUEST_CODE_CAMERA_TAKE_PICTURE) {
            showToastInfo(getStringById(R.string.instapay_save_failed));
        }

    }

    public class MyWebviewAndJsMutual {
        private Context context;

        public MyWebviewAndJsMutual(Context context) {
            this.context = context;
        }

        /**
         * 前端代码嵌入js：
         * takePhoto 名应和js函数方法名一致
         * 定义JS需要调用的方法，被JS调用的方法必须加入@JavascriptInterface注解
         */
        @JavascriptInterface
        public void JsCallAndroidFunction(int apiCode, int camereType, String callbackName) {
            mApiCode = apiCode;
            mCamereType = camereType;
            mCallbackName = callbackName;

            ;//1：常规相机 2：特殊相机 3：注册成功，返回登录页 4：截图当前屏幕所有内容
            switch (mApiCode) {
                case 1:
                    //1：常规相机
                    //调用系统相机的代码--可以移植到任何地方
                  /* String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                   photoName = "camera" + timeStamp;
                   photoUri = ImageUtil.openSystemCamera(MyWebViewActicity.this ,photoName);*/

                    //调用自定义相机的代码----可以移植到任何地方
                    gotoCamera(mApiCode, mCamereType);

                    break;
                case 2:
                    //2：特殊相机
                    //调用自定义相机的代码----可以移植到任何地方
                    gotoCamera(mApiCode, mCamereType);

                    break;
                case 3:
                    //3：注册成功，返回登录页
                    startWelcomeActivity();

                    break;
                case 4:
                    //4：截图当前屏幕所有内容,保存webview的长图
                    getCurrentPageBitmap();

                    break;
            }
        }
    }


    //调用系统相机的回调代码----测试用，可以移植到任何地方
   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        //如果是拍照返回的结果
        //如果调用相机用的这种方法指定Uri intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        //onActivityResult 方法中的data 返回为空（数据表明，93%的机型的data
        //将会是Null，所以如果我们指定了路径(Uri)，就不要使用data 来获取照片，起码在使用前要做空判断）
        //如果不指定图片的uri，则可能会返回data(照相机有自己默认的存储路径，拍摄的照片将返回一个缩略图。
        //可以通过data.getParcelableExtra("data")获得图片)
        //使用 onActivityResult 中的 intent(data)前要做空判断。！！！很重要
        if (resultCode == Activity.RESULT_OK && requestCode == ImageUtil.REQUESTCODE_TAKE_PICTURE) {

             if (photoUri != null) {
                //若生成了uri，则表示该文件添加成功
                //使用流将该uri中的内容写入字节数组即可
               byte[] image;
               try{
                   InputStream inputStream = getContentResolver().openInputStream(photoUri);
                   if (null == inputStream || 0 == inputStream.available()) {
                       return;
                   }
                   image = new byte[inputStream.available()];
                   inputStream.read(image);
                   inputStream.close();

                   //拍照成功上传数据给H5
                   if(mCallbackName != null){
                       AndroidCallJs_Upload_Image(mCallbackName,image);
                   }

               }catch (IOException e){

               }

                sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, photoUri));

            }

        }else {
            if(photoUri != null){
                //如果是Android 10.0 点击确定之后，又重试不保存之前确定的图，故应该先查询，如果存在就删除
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
                    //因为insert 如果uri存在的话，会返回null 所以需要先delete
                    String status = Environment.getExternalStorageState();
                    // 判断是否有SD卡,优先使用SD卡存储,当没有SD卡时使用手机存储
                    Uri uri ;
                    if (status.equals(Environment.MEDIA_MOUNTED)) {
                        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else {
                        uri =MediaStore.Images.Media.INTERNAL_CONTENT_URI;
                    }
                    Cursor query = getContentResolver().query(uri,
                            new String[]{MediaStore.Images.Media.DISPLAY_NAME},
                            MediaStore.Images.Media.DISPLAY_NAME + "=?",
                            new String[]{photoName+".jpg"},
                            null
                    );
                    if (query != null && query.getCount() > 0) {
                        getContentResolver().delete(uri,
                                MediaStore.Images.Media.DISPLAY_NAME + "=?",
                                new String[]{photoName+".jpg"});

                    }
                }

            };
        }
    }*/


}
