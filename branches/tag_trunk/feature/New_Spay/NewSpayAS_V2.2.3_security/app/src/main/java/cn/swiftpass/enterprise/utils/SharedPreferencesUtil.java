package cn.swiftpass.enterprise.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;
import java.util.Set;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.newmvp.config.SPConfig;


/**
 * 创建人：caoxiaoya
 * 时间：2018/6/13
 * 描述：SharedPreferences缓存管理
 * 备注：
 */
public class SharedPreferencesUtil {

    private  SharedPreferences.Editor mEditor;
    private SharedPreferences mSharedPreferences;
    private static SharedPreferencesUtil mSharedPreferencesUtil;

    public SharedPreferencesUtil(SharedPreferences sharedPreferences) {
        this.mSharedPreferences = sharedPreferences;
    }
    //构造方法
    public SharedPreferencesUtil(Context context) {
        this.mSharedPreferences = context.getSharedPreferences(SPConfig.SP_NAME, Context.MODE_PRIVATE);
        //getDefaultSharedPreferences
        this.mSharedPreferences = android.preference.PreferenceManager.getDefaultSharedPreferences(MainApplication.getContext());
    }

    //单例模式
    public static SharedPreferencesUtil getInstance() {
        if (mSharedPreferencesUtil == null) {
            synchronized (SharedPreferencesUtil.class) {
                if (mSharedPreferencesUtil == null) {
                    mSharedPreferencesUtil = new SharedPreferencesUtil(MainApplication.getContext());
                }
            }
        }
        return  mSharedPreferencesUtil;
    }

    public void put(String key, String value) {
        if (mSharedPreferences == null) {
            return;
        }
        mSharedPreferences.edit().putString(key, value).apply();
    }

    public void put(String key, boolean value) {
        if (mSharedPreferences == null) {
            return;
        }
        mSharedPreferences.edit().putBoolean(key, value).apply();
    }

    public void put(String key, float value) {
        if (mSharedPreferences == null) {
            return;
        }
        mSharedPreferences.edit().putFloat(key, value).apply();
    }

    public void put(String key, int value) {
        if (mSharedPreferences == null) {
            return;
        }
        mSharedPreferences.edit().putInt(key, value).apply();
    }

    public void put(String key, long value) {
        if (mSharedPreferences == null) {
            return;
        }
        mSharedPreferences.edit().putLong(key, value).apply();
    }

    public void put(String key, Set<String> value) {
        if (mSharedPreferences == null) {
            return;
        }
        mSharedPreferences.edit().putStringSet(key, value).apply();
    }

    public String getString(String key, String defValue) {
        return mSharedPreferences == null ? null : mSharedPreferences.getString(key, defValue);
    }
    public String getString(String key) {
        return mSharedPreferences == null ? null : mSharedPreferences.getString(key, "");
    }
    public boolean getBoolean(String key, boolean defValue) {
        return mSharedPreferences == null ? defValue : mSharedPreferences.getBoolean(key, defValue);
    }

    public float getFloat(String key, float defValue) {
        return mSharedPreferences == null ? defValue : mSharedPreferences.getFloat(key, defValue);
    }

    public int getInt(String key, int defValue) {
        return mSharedPreferences == null ? defValue : mSharedPreferences.getInt(key, defValue);
    }

    public long getLong(String key, long defValue) {
        return mSharedPreferences == null ? defValue : mSharedPreferences.getLong(key, defValue);
    }

    public Set<String> getStringSet(String key, Set<String> defValue) {
        return mSharedPreferences == null ? defValue : mSharedPreferences.getStringSet(key, defValue);
    }

    public void remove(String key) {
        if (mSharedPreferences == null) {
            return;
        }
        mSharedPreferences.edit().remove(key).apply();
    }

    public Map<String, ?> getAll() {
        return mSharedPreferences == null ? null : mSharedPreferences.getAll();
    }

    public void clear() {
        mSharedPreferences.edit().clear().apply();
    }
}
