package cn.swiftpass.enterprise.ui.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.bill.BillOrderManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.refund.RefundManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.WxCard;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.list.NewPullDownListView;
import cn.swiftpass.enterprise.ui.activity.list.PullToRefreshBase;
import cn.swiftpass.enterprise.ui.activity.list.PullToRefreshListView;
import cn.swiftpass.enterprise.ui.activity.user.RefundRecordOrderDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.user.VerificationDetails;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabBill;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 *单次退款    退款详情
 *多次退款   退款列表
 *无退款    空页面
 * User: jamy
 */
public class RefundOrderListActivity extends TemplateActivity implements NewPullDownListView.OnRefreshListioner, NewPullDownListView.OnLoadDateRefreshListioner{
    private static final String TAG = FragmentTabBill.class.getSimpleName();


    private ViewHolder holder;

    private ListView listView;

    private List<Order> orderList = new ArrayList<Order>();

    private Handler mHandler = new Handler();

    private BillStreamAdapter billStreamAdapter;

    private int pageFulfil = 0;

    private TextView tv_prompt;

    //0流水，1，退款，2 卡券
    private int isRef = 1;

    private PullToRefreshListView mPullRefreshListView;
    //刷新反正多次刷新
    private boolean isMoerRefresh = true;

    private boolean loadMore = true;

    private Integer reqFeqTime = 0;

    private boolean loadNext = false;

    private Context mContext;
    private String orderNo = null;
    private int size = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list_activity);
        mContext = RefundOrderListActivity.this;
        initView();
        setLister();
        if (null != getIntent()){
            size = Integer.parseInt(getIntent().getStringExtra("order_size"));
            orderNo = getIntent().getStringExtra("orderNo");
        }
        if (0 == size){
            mPullRefreshListView.setVisibility(View.GONE);
            tv_prompt.setVisibility(View.VISIBLE);
        }else{
            tv_prompt.setVisibility(View.GONE);
            mPullRefreshListView.setVisibility(View.VISIBLE);
            loadDateTask(1, 1, true, true);
        }
    }


    private void initView() {
        tv_prompt =getViewById(R.id.tv_prompt);
        tv_prompt.setText(R.string.tv_bill);
        mPullRefreshListView =getViewById(R.id.pullrefresh);
        listView = mPullRefreshListView.getRefreshableView();
        billStreamAdapter = new BillStreamAdapter(orderList);
        listView.setAdapter(billStreamAdapter);
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.refund_detail_list);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {
                // TODO Auto-generated method stub

            }

            @Override
            public void onRightButLayClick() {
                // TODO Auto-generated method stub

            }
        });
    }

    void loadDate(final int page, final boolean isLoadMore, int isRefund, String startDate,String orderNo) {
        if (loadNext) {
            mPullRefreshListView.onRefreshComplete();
            toastDialog(RefundOrderListActivity.this, R.string.tx_request_more, null);
            return;
        }

        BillOrderManager.getInstance().querySpayOrder(null,reqFeqTime, null, null, isRefund, page, orderNo, startDate, new UINotifyListener<List<Order>>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                reqFeqTime = 0;
                if (page == 1) {
                    isMoerRefresh = false;
                }
                if (isLoadMore) {
                    loadDialog(RefundOrderListActivity.this, R.string.public_data_loading);
                }
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                dissDialog();
                isMoerRefresh = true;
                if (checkSession()) {
                    return;
                }
                if (null != object) {
                    // 服务器返回有这个参数，说明服务器有压力，需要暂停reqFeqTime=对应的秒数才去请求返回
                    if (object.toString().startsWith("reqFeqTime")) {
                        String time = object.toString().substring(object.toString().lastIndexOf("=") + 1);
                        if (!StringUtil.isEmptyOrNull(time)) {
                            Integer count = Integer.parseInt(time);
                            loadNext = true;
                            sleep(count);
                        } else {
                            dissDialog();
                        }
                    } else {
                        dissDialog();
                        RefundOrderListActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                if (!StringUtil.isEmptyOrNull(object.toString())) {
                                    toastDialog(RefundOrderListActivity.this, object.toString(), null);
                                } else {
                                    toastDialog(RefundOrderListActivity.this, R.string.tx_load_fail, null);
                                }

                                if (orderList.size() == 0) {
                                    tv_prompt.setVisibility(View.VISIBLE);
                                    mPullRefreshListView.setVisibility(View.GONE);
                                }
                            }
                        });
                    }

                }
            }

            @Override
            public void onSucceed(List<Order> model) {
                dissDialog();
                isMoerRefresh = true;
                if (null != model && model.size() > 0) {
                    tv_prompt.setVisibility(View.GONE);
                    if (page == 1) {
                        orderList.clear();
                    }

                    //pageCount = model.get(0).getPageCount();

                    reqFeqTime = model.get(0).getReqFeqTime();

                    setListData(mPullRefreshListView, orderList, billStreamAdapter, model, isLoadMore, page);
                    RefundOrderListActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            mPullRefreshListView.setVisibility(View.VISIBLE);
                        }
                    });
                    //暂停
                    if (reqFeqTime > 0) {
                        loadNext = true;
                        sleep(reqFeqTime);
                    }
                    // mySetListData(bill_list, orderList, billStreamAdapter, model, isLoadMore);
                } else {
                    loadMore = false;
                    if (orderList.size() == 0) {
                        RefundOrderListActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                tv_prompt.setVisibility(View.VISIBLE);
                                mPullRefreshListView.setVisibility(View.GONE);
                            }
                        });
                    } else {
                        mPullRefreshListView.onRefreshComplete();
                    }
                }
            }
        });
    } /**
     * 暂停 后 才去请求
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void sleep(long time) {
        try {
            //            Thread.sleep(time * 1000);

            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    RefundOrderListActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Logger.i("hehui", "sleep-->");
                            loadNext = false;
                        }
                    });

                }

            };
            Timer timer = new Timer();
            timer.schedule(task, time * 1000);
        } catch (Exception e) {
            return;
        }
    }


    void loadDateTask(int page, int pullType, final boolean isLoadMore, boolean isRefund) {

        switch (pullType) {
            case 1:
                loadMore = true;
                pageFulfil = 1;
                loadDate(pageFulfil, isLoadMore, isRef, null,orderNo);
                break;
            case 2:
                mPullRefreshListView.onRefreshComplete();
                break;
            case 3:
                pageFulfil = pageFulfil + 1;
                if (loadMore) {
                    loadDate(pageFulfil, isLoadMore, isRef, null,orderNo);
                } else {
                    mPullRefreshListView.onRefreshComplete();
                }
                break;
            default:
                break;
        }
    }

    private void setLister() {

        PullToRefreshBase.OnRefreshListener mOnrefreshListener = new PullToRefreshBase.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadDateTask(1, mPullRefreshListView.getRefreshType(), false, false);
            }
        };
        mPullRefreshListView.setOnRefreshListener(mOnrefreshListener);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int lastItemIndex;//当前ListView中最后一个Item的索引

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                lastItemIndex = firstVisibleItem + visibleItemCount - 1 - 1;
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (lastItemIndex >= billStreamAdapter.getCount() - 2) {
                    loadDateTask(1, 3, true, false);
                }

            }

        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Order order = orderList.get(position - 1);
                if (null != order) {
                    //isRefresh = false;
                    if (isRef == 1) {
                        RefundManager.getInstant().queryRefundDetail(order.getOutRefundNo(), MainApplication.getMchId(), new UINotifyListener<Order>() {

                            @Override
                            public void onError(Object object) {
                                super.onError(object);
                                dismissLoading();
                                if (checkSession()) {
                                    return;
                                }
                                if (object != null) {
                                    toastDialog(RefundOrderListActivity.this, object.toString(), null);
                                }
                            }

                            @Override
                            public void onPreExecute() {
                                super.onPreExecute();

                                loadDialog(RefundOrderListActivity.this, getStringById(R.string.public_data_loading));
                            }

                            @Override
                            public void onSucceed(Order result) {
                                super.onSucceed(result);
                                dismissLoading();
                                if (result != null) {
                                    RefundRecordOrderDetailsActivity.startActivity(RefundOrderListActivity.this, result);
                                }
                            }

                        });
                    } else if (isRef == 0) {
                        OrderManager.getInstance().queryOrderDetail(order.getOutTradeNo(), MainApplication.getMchId(), true, new UINotifyListener<Order>() {

                            @Override
                            public void onError(Object object) {
                                super.onError(object);
                                dismissLoading();
                                if (checkSession()) {
                                    return;
                                }
                                if (object != null) {
                                    toastDialog(RefundOrderListActivity.this, object.toString(), null);
                                }
                            }

                            @Override
                            public void onPreExecute() {
                                super.onPreExecute();

                                loadDialog(RefundOrderListActivity.this, getStringById(R.string.public_data_loading));

                            }

                            @Override
                            public void onSucceed(Order result) {

                                super.onSucceed(result);
                                dismissLoading();
                                if (result != null) {
                                    OrderDetailsActivity.startActivity(RefundOrderListActivity.this, result);
                                }
                            }

                        });
                    } else {
                        OrderManager.getInstance().queryVardDetail(order.getCardId(), order.getCardCode(), new UINotifyListener<WxCard>() {

                            @Override
                            public void onError(Object object) {
                                super.onError(object);
                                dismissLoading();
                                if (object != null) {
                                    toastDialog(RefundOrderListActivity.this, object.toString(), null);
                                }
                            }

                            @Override
                            public void onPreExecute() {
                                super.onPreExecute();

                                loadDialog(RefundOrderListActivity.this, R.string.public_data_loading);
                            }

                            @Override
                            public void onSucceed(WxCard result) {
                                super.onSucceed(result);
                                dismissLoading();
                                if (result != null) {
                                    VerificationDetails.startActivity(RefundOrderListActivity.this, result);
                                }
                            }

                        });
                    }

                }
            }
        });


    }

    private void setListData(final PullToRefreshListView pull, List<Order> list, BillStreamAdapter adapter, List<Order> result, boolean isLoadMore, int page) {
        if (!isLoadMore) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {

                }
            }, 800);

            list.clear();
        } else {
            mHandler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    //                    pull.onLoadMoreComplete();
                    // pull.setMore(true);
                    pull.onRefreshComplete();
                }
            }, 1000);
        }
        list.addAll(result);
        pull.onRefreshComplete();
        adapter.notifyDataSetChanged();
        if (page == 1) {
            listView.setSelection(0);
        }
    }

    private class BillStreamAdapter extends BaseAdapter
    {

        private List<Order> orders;

        public BillStreamAdapter() {
        }

        public BillStreamAdapter(List<Order> orders) {
            this.orders = orders;
        }

        @Override
        public int getCount() {
            return orders.size();
        }

        @Override
        public Object getItem(int position) {
            return orders.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(mContext, R.layout.activity_bill_stream_list_item, null);
                holder = new ViewHolder();
                holder.tv_pay_type = (TextView) convertView.findViewById(R.id.tv_pay_type);
                holder.tv_time = (TextView) convertView.findViewById(R.id.tv_time);
                holder.tv_money = (TextView) convertView.findViewById(R.id.tv_money);
                holder.tv_state = (TextView) convertView.findViewById(R.id.tv_state);
                holder.tv_total = (TextView) convertView.findViewById(R.id.tv_total);
//                holder.tv_date = (TextView) convertView.findViewById(R.id.tv_date);
                holder.iv_type = (ImageView) convertView.findViewById(R.id.iv_type);
//                holder.ly_title = (RelativeLayout) convertView.findViewById(R.id.ly_title);//没有引用，注释掉
                holder.v_iv = (View) convertView.findViewById(R.id.v_iv);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            Order order = orderList.get(position);
            if (null != order) {
                if (order.getMoney() > 0) {
                    holder.tv_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(order.getMoney()));
                }
                holder.tv_money.setVisibility(View.VISIBLE);
                if (isRef == 1) {
//                    String langeage = PreferenceUtil.getString("language", "");
//                    if (langeage.equals("en_us")) {
//                        holder.tv_pay_type.setText(MainApplication.getPayTypeMap().get(String.valueOf(order.getApiProvider())) + " " + getString(R.string.title_order_refund));
//                    } else {
//                        holder.tv_pay_type.setText(MainApplication.getPayTypeMap().get(String.valueOf(order.getApiProvider())) + getString(R.string.title_order_refund));
//                    }

                    if (!TextUtils.isEmpty(order.getRefundNo())&&order.getRefundNo().length()>4){
                        String frontStr=order.getRefundNo();
                        String behindStr=order.getRefundNo();
                        holder.tv_pay_type.setText(frontStr.substring(0,4) +"****"+behindStr.substring(behindStr.length()-4,behindStr.length()));
                    }

                    try {
                        holder.tv_time.setText(order.getRefundTimeNew());
                        if (isMove(position)) {
                            holder.v_iv.setVisibility(View.GONE);
                        } else {
                            holder.v_iv.setVisibility(View.VISIBLE);
                        }

                        holder.tv_state.setText(MainApplication.getRefundStateMap().get(order.getRefundState() + ""));
                        switch (order.getRefundState()) {
                            case 0:
                                //审核中
                                holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.paytype_title_reset));
                                holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.black));
                                break;
                            case 1:
                            case 3:
                            case 5:
                                //退款成功
                                holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.pay_fail));
                                holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.black));
                                break;
                            case 2:
                                //退款失败
                                holder.tv_state.setText(R.string.refund_failure);
                                holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                break;
                        }
                    } catch (Exception e) {
                        Log.e(TAG, Log.getStackTraceString(e));
                    }
                } else if (isRef == 0) {
                    holder.tv_pay_type.setText(MainApplication.getPayTypeMap().get(String.valueOf(order.getApiProvider())));

                    if (!StringUtil.isEmptyOrNull(order.getTradeTimeNew())) {
                        try {
                            holder.tv_time.setText(order.getTradeTimeNew());
                            if (isMove(position)) {
                                holder.v_iv.setVisibility(View.GONE);
                            } else {
                                holder.v_iv.setVisibility(View.VISIBLE);
                            }

                            holder.tv_state.setText(MainApplication.getTradeTypeMap().get(order.getTradeState() + ""));
                            switch (order.getTradeState()) {
                                case 2:
                                    //支付成功
                                    holder.tv_state.setTextColor(Color.parseColor("#03C700"));
                                    holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.black));
                                    break;
                                case 1:
                                    //未支付
                                    holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                    holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                    break;
                                case 3:
                                    //关闭
                                    holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                    holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                    break;
                                case 4:
                                    //转入退款
                                    holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.pay_fail));
                                    holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.black));
                                    break;
                                case 8:
                                    //已撤销
                                    holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                    holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                    break;
                            }

                        } catch (Exception e) {
                            Log.e(TAG,Log.getStackTraceString(e));
                        }
                    }
                } else {
                    holder.tv_time.setText(order.getUseTimeNew());
                    holder.tv_pay_type.setText(order.getTitle());
                    holder.tv_money.setVisibility(View.INVISIBLE);
                    holder.iv_type.setImageResource(R.drawable.icon_pop_coupon);
                    //String value = dayCountMap.get(order.getFromatCard());
                    if (isMove(position)) {
                        holder.v_iv.setVisibility(View.GONE);
                    } else {
                        holder.v_iv.setVisibility(View.VISIBLE);
                    }
                    //卡券
                    holder.tv_state.setText(R.string.tx_affirm_succ);
                    holder.tv_state.setTextColor(Color.parseColor("#666666"));
                }

                if (isRef == 0 || isRef == 1) {

                    Object object = SharedPreUtile.readProduct("payTypeIcon" + ApiConstant.bankCode + MainApplication.getMchId());
                    if (object != null) {
                        try {

                            Map<String, String> typePicMap = (Map<String, String>) object;
                            if (typePicMap != null && typePicMap.size() > 0) {
                                String picUrl = typePicMap.get(order.getApiProvider() + "");
                                if (!StringUtil.isEmptyOrNull(picUrl)) {
                                    Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.icon_general_receivables);
                                    if (bitmap != null) {
                                        MainApplication.finalBitmap.display(holder.iv_type, picUrl, bitmap);
                                    } else {
                                        MainApplication.finalBitmap.display(holder.iv_type, picUrl);
                                    }
                                } else {
                                    holder.iv_type.setImageResource(R.drawable.icon_general_receivables);
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG,Log.getStackTraceString(e));
                        }
                    } else {

                        switch (order.getApiProvider()) {
                            case 1: //微信
                                holder.iv_type.setImageResource(R.drawable.icon_general_wechat);
                                break;
                            case 2: //支付宝
                                holder.iv_type.setImageResource(R.drawable.icon_general_pay);
                                break;
                            case 12: //京东
                                holder.iv_type.setImageResource(R.drawable.icon_general_jingdong);
                                break;
                            case 4: //qq
                                holder.iv_type.setImageResource(R.drawable.icon_general_qq);
                                break;
                            default:
                                holder.iv_type.setImageResource(R.drawable.icon_general_receivables);
                                break;
                        }
                    }
                }

            }
            return convertView;
        }
    }

    private boolean isMove(int position) {
        // 获取当前与下一项
        //        ItemEntity currentEntity = (ItemEntity)getItem(position);
        //          ItemEntity nextEntity = (ItemEntity)getItem(position + 1);
        try {
            Order order = orderList.get(position);
            Order order1 = orderList.get(position + 1);

            if (null == order || null == order1) {
                return true;
            }

            if (isRef == 0) {
                // 获取两项header内容
                //                String currentTitle = DateUtil.formartDateYYMMDD(order.getTradeTimeNew());
                //                String nextTitle = DateUtil.formartDateYYMMDD(order1.getTradeTimeNew());

                String currentTitle = order.getFormatTimePay();
                String nextTitle = order1.getFormatTimePay();
                if (null == currentTitle || null == nextTitle) {
                    return false;
                }

                // 当前不等于下一项header，当前项需要移动了
                if (!currentTitle.equals(nextTitle)) {
                    return true;
                }

            } else if (isRef == 1) { //退款
                String currentTitle = order.getFormatRefund();
                String nextTitle = order1.getFormatRefund();
                if (currentTitle == null || nextTitle == null) {
                    return false;
                }

                if (!currentTitle.equalsIgnoreCase(nextTitle)) {
                    return true;
                }
            } else {
                String currentTitle = order.getFromatCard();
                String nextTitle = order1.getFromatCard();
                if (currentTitle == null || nextTitle == null) {
                    return false;
                }

                if (!currentTitle.equalsIgnoreCase(nextTitle)) {
                    return false;
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
            return true;
        }

        return false;
    }

    private class ViewHolder {
        private TextView tv_total, tv_time, tv_pay_type, tv_money, tv_state, tv_date;

        private ImageView iv_type;

//        private RelativeLayout ly_title;

        private View v_iv;
    }

    @Override
    public void onLoadMoreDate() {

    }

    @Override
    public void onRefresh() {
        Logger.i("hehui", "onRefresh()");
        pageFulfil = 1;
        if (isMoerRefresh) {
            loadDate(pageFulfil, false, isRef, null,orderNo);
        }
    }

    @Override
    public void onLoadMore() {
        Logger.i("hehui", "onLoadMore()");
        pageFulfil = pageFulfil + 1;
        loadDate(pageFulfil, true, isRef, null,orderNo);
    }

}
