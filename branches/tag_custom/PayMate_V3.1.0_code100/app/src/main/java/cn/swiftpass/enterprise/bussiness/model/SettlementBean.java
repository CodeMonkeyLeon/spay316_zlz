package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created by aijingya on 2019/5/20.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2019/5/20.17:32.
 */
public class SettlementBean implements Serializable {
    private Long transactionAmount;//交易金额
    private Long  refundAmount; //退款金额
    private Long merchantCharge;//商户手续费
    private Long  preChargeTax; //预扣税费
    private Long additionalTax;//附加税费
    private Long  tip; //小费
    private Long settlementAmount;//结算金额
    private String settlementDate;//结算日期（北京时间）
    private String organizationBeginTime;//机构开始时间
    private String organizationEndTime;//机构结束时间

    public String getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(String settlementDate) {
        this.settlementDate = settlementDate;
    }

    public String getOrganizationBeginTime() {
        return organizationBeginTime;
    }

    public void setOrganizationBeginTime(String organizationBeginTime) {
        this.organizationBeginTime = organizationBeginTime;
    }

    public String getOrganizationEndTime() {
        return organizationEndTime;
    }

    public void setOrganizationEndTime(String organizationEndTime) {
        this.organizationEndTime = organizationEndTime;
    }



    public Long getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Long transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public Long getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(Long refundAmount) {
        this.refundAmount = refundAmount;
    }

    public Long getMerchantCharge() {
        return merchantCharge;
    }

    public void setMerchantCharge(Long merchantCharge) {
        this.merchantCharge = merchantCharge;
    }

    public Long getPreChargeTax() {
        return preChargeTax;
    }

    public void setPreChargeTax(Long preChargeTax) {
        this.preChargeTax = preChargeTax;
    }

    public Long getAdditionalTax() {
        return additionalTax;
    }

    public void setAdditionalTax(Long additionalTax) {
        this.additionalTax = additionalTax;
    }

    public Long getTip() {
        return tip;
    }

    public void setTip(Long tip) {
        this.tip = tip;
    }

    public Long getSettlementAmount() {
        return settlementAmount;
    }

    public void setSettlementAmount(Long settlementAmount) {
        this.settlementAmount = settlementAmount;
    }





}
