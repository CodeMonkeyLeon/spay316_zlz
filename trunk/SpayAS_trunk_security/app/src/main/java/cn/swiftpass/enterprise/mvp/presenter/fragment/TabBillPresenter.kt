package cn.swiftpass.enterprise.mvp.presenter.fragment

import android.text.TextUtils
import android.util.Log
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.DailyStatisticsBean
import cn.swiftpass.enterprise.bussiness.model.MasterCardDailyStatisticsBean
import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.bussiness.model.OrderList
import cn.swiftpass.enterprise.bussiness.model.PreAuthDailyStatisticsBean
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.fragment.TabBillContract
import cn.swiftpass.enterprise.utils.DateUtil
import cn.swiftpass.enterprise.utils.JsonUtil
import cn.swiftpass.enterprise.utils.SignUtil
import cn.swiftpass.enterprise.utils.StringUtil
import com.example.common.sentry.SentryUtils.uploadNetInterfaceException
import com.example.common.sp.PreferenceUtil
import okhttp3.Call
import java.text.ParseException

/**
 * @author lizheng.zhao
 * @date 2022/11/15
 *
 * @你应该是一场梦我应该是一阵风
 */
class TabBillPresenter : TabBillContract.Presenter {

    companion object {
        const val TAG = "TabBillPresenter"
    }

    private var mView: TabBillContract.View? = null


    override fun queryMasterCardDailyStatics() {

        mView?.run {
            showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)


            AppClient.queryDailyStatics(
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<MasterCardDailyStatisticsBean>(
                    MasterCardDailyStatisticsBean()
                ) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {

                        dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { res ->
                            queryMasterCardDailyStaticsFailed(res.message)
                        }

                    }


                    override fun onResponse(res: CommonResponse?) {

                        dismissLoading(ParamsConstants.COMMON_LOADING)
                        res?.let { response ->
                            val bean = JsonUtil.jsonToBean(
                                response.message, MasterCardDailyStatisticsBean::class.java
                            ) as MasterCardDailyStatisticsBean
                            queryMasterCardDailyStaticsSuccess(bean)
                        }

                    }
                }
            )
        }


    }

    override fun queryPreAuthDailyStatics() {


        mView?.run {
            showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)


            AppClient.queryPreAuthDailyStatics(
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<PreAuthDailyStatisticsBean>(
                    PreAuthDailyStatisticsBean()
                ) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {

                        dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { res ->
                            queryPreAuthDailyStaticsFailed(res.message)
                        }

                    }

                    override fun onResponse(res: CommonResponse?) {

                        dismissLoading(ParamsConstants.COMMON_LOADING)
                        res?.let { response ->
                            val bean = JsonUtil.jsonToBean(
                                response.message, PreAuthDailyStatisticsBean::class.java
                            ) as PreAuthDailyStatisticsBean
                            queryPreAuthDailyStaticsSuccess(bean)
                        }

                    }
                }
            )
        }

    }

    override fun queryDailyStatics() {


        mView?.run {
            showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)

            AppClient.queryDailyStatics(
                System.currentTimeMillis().toString(),
                object :
                    CallBackUtil.CallBackCommonResponse<DailyStatisticsBean>(DailyStatisticsBean()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {

                        dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { res ->
                            queryDailyStaticsFailed(res.message)
                        }

                    }

                    override fun onResponse(res: CommonResponse?) {

                        dismissLoading(ParamsConstants.COMMON_LOADING)
                        res?.let { response ->
                            val bean = JsonUtil.jsonToBean(
                                response.message, DailyStatisticsBean::class.java
                            ) as DailyStatisticsBean
                            queryDailyStaticsSuccess(bean)
                        }

                    }

                }
            )
        }


    }

    override fun queryOrderDetail(
        orderNo: String?,
        mchId: String?,
        isMark: Boolean
    ) {

        mView?.run {
            showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)

            AppClient.queryOrderDetail(
                orderNo,
                orderNo,
                orderNo,
                mchId,
                MainApplication.getInstance().getUserId().toString(),
                System.currentTimeMillis().toString(),
                isMark,
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {

                        dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { res ->
                            queryOrderDetailFailed(res.message)
                        }

                    }


                    override fun onResponse(res: CommonResponse?) {
                        //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组

                        dismissLoading(ParamsConstants.COMMON_LOADING)
                        res?.let { response ->
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"", "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message, Order::class.java
                            ) as Order
                            order.add_time =
                                if (TextUtils.isEmpty(order.addTime)) 0 else order.addTime.toLong()
                            order.useId = order.userId.toString()
                            order.canAffim = order.canAffirm
                            order.setUplanDetailsBeans(order.discountDetail)
                            queryOrderDetailSuccess(order)
                        }

                    }
                }
            )
        }


    }

    override fun queryRefundDetail(
        orderNo: String?,
        mchId: String?
    ) {

        mView?.run {
            showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)



            AppClient.queryRefundDetail(
                orderNo,
                mchId,
                MainApplication.getInstance().getUserId().toString(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {

                        dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { response ->
                            queryRefundDetailFailed(response.message)
                        }

                    }


                    override fun onResponse(res: CommonResponse?) {

                        dismissLoading(ParamsConstants.COMMON_LOADING)
                        res?.let { response ->
                            //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"", "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message, Order::class.java
                            ) as Order
                            order.useId = order.userId.toString()
                            order.orderNoMch = order.orderNo
                            order.cashFeel =
                                if (TextUtils.isEmpty(order.cashFee)) 0 else order.cashFee.toLong()
                            queryRefundDetailSuccess(order)
                        }


                    }
                }
            )
        }

    }

    override fun queryCardPaymentOrderDetails(
        outTradeNo: String?,
        orderNoMch: String?
    ) {

        mView?.run {

            showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)

            AppClient.queryCardPaymentOrderDetails(
                outTradeNo,
                orderNoMch,
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {

                        dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { response ->
                            queryCardPaymentOrderDetailsFailed(response.message)
                        }

                    }


                    override fun onResponse(res: CommonResponse?) {

                        dismissLoading(ParamsConstants.COMMON_LOADING)
                        res?.let { response ->
                            response.message = response.message.replace(
                                "\"data\":\"\"", "\"data\":[]"
                            )
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"", "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message, Order::class.java
                            ) as Order
                            order.useId = order.userId.toString()
                            order.cashFeel =
                                if (TextUtils.isEmpty(order.cashFee)) 0 else order.cashFee.toLong()
                            queryCardPaymentOrderDetailsSuccess(order)
                        }

                    }
                }
            )
        }


    }

    override fun loadCardPaymentDate(
        apiProviderList: List<Integer>?,
        tradeTypeList: List<Integer>?,
        tradeStateList: List<Integer>?,
        startTime: String?,
        page: Int,
        isSearch: Boolean,
        orderNoMch: String?,
        isLoadMore: Boolean
    ) {

        mView?.run {
            if (isLoadMore) {
                showLoading(
                    R.string.public_data_loading,
                    ParamsConstants.COMMON_LOADING
                )
            }


            val param: MutableMap<String, Any> = java.util.HashMap()
            if (!isSearch) {
                if (!StringUtil.isEmptyOrNull(startTime)) {
                    param[ParamsConstants.START_DATE] = "$startTime 00:00:00"
                    param[ParamsConstants.END_DATE] = "$startTime 23:59:59"
                } else {
                    param[ParamsConstants.START_DATE] =
                        DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00"
                    param[ParamsConstants.END_DATE] =
                        DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59"
                }
            }

            param[ParamsConstants.PAGE_SIZE] = "20"
            param[ParamsConstants.PAGE] = page.toString()


            orderNoMch?.let {
                param[ParamsConstants.ORDER_NO_MCH] = it
            }

            if (!TextUtils.isEmpty(MainApplication.getInstance().getSignKey())) {
                param[ParamsConstants.SIGN] = SignUtil.getInstance().createSign(
                    JsonUtil.jsonToMap(JsonUtil.objectToJson(param)),
                    MainApplication.getInstance().getSignKey()
                )
            }

            val spayRs = System.currentTimeMillis().toString()
            //加签名
            if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getNewSignKey())) {
                param[ParamsConstants.SPAY_RS] = spayRs
                param[ParamsConstants.NNS] = SignUtil.getInstance().createSign(
                    JsonUtil.jsonToMap(JsonUtil.objectToJson(param)),
                    MainApplication.getInstance().getNewSignKey()
                )
            }

            if (tradeTypeList != null && tradeTypeList.size > 0) {
                param[ParamsConstants.TRADE_TYPE_LIST] = tradeTypeList
            }
            if (apiProviderList != null && apiProviderList.size > 0) {
                param[ParamsConstants.API_PROVIDER_LIST] = apiProviderList
            }
            if (tradeStateList != null && tradeStateList.size > 0) {
                param[ParamsConstants.TRADE_STATE_LIST] = tradeStateList
            }

            AppClient.loadCardPaymentDate(
                spayRs,
                JsonUtil.mapToJsons(param),
                object : CallBackUtil.CallBackCommonResponse<OrderList>(OrderList()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {

                        dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { res ->
                            loadCardPaymentDateFailed(res.message)
                        }

                    }


                    override fun onResponse(res: CommonResponse?) {

                        dismissLoading(ParamsConstants.COMMON_LOADING)
                        res?.let { response ->
                            response.message = response.message.replace(
                                "\"data\":\"\"", "\"data\":[]"
                            )
                            val orderList = JsonUtil.jsonToBean(
                                response.message, OrderList::class.java
                            ) as OrderList
                            for (order in orderList.data) {
                                val time = order.tradeTimeNew
                                if (!StringUtil.isEmptyOrNull(time)) {
                                    try {
                                        order.setFormatTimePay(DateUtil.formartDateYYMMDD(time))
                                        order.setFormartYYMM(DateUtil.formartDateYYMMDDTo(time))
                                        order.setTradeTimeNew(DateUtil.formartDateToHHMMSS(time))
                                    } catch (e: ParseException) {
                                        uploadNetInterfaceException(
                                            e, "spay/card/transList", "", ""
                                        )
                                        Log.e(TAG, Log.getStackTraceString(e))
                                    }
                                }
                            }
                            val reqFeqTime: Int =
                                if (TextUtils.isEmpty(orderList.reqFeqTime)) 0 else orderList.reqFeqTime.toInt()
                            if (null != orderList.data && orderList.data.size > 0 && orderList.data[0] != null) {
                                orderList.data[0].setReqFeqTime(reqFeqTime)
                            } else {
                                if (reqFeqTime > 0) {
                                    loadCardPaymentDateFailed("reqFeqTime=$reqFeqTime")
                                }
                            }
                            loadCardPaymentDateSuccess(orderList.data, page, isLoadMore)
                        }


                    }
                }
            )
        }
    }

    override fun querySpayOrderNew(
        reqFeqTime: Int,
        listStr: List<String>?,
        payTypeList: List<String>?,
        isRefund: Int,
        page: Int,
        order: String?,
        isUnfrozen: Int,
        userId: String?,
        startDate: String?,
        isLoadMore: Boolean
    ) {


        mView?.run {
            if (isLoadMore) {
                showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)

            }
            val mapParam: MutableMap<String?, Any?> = HashMap<String?, Any?>()

            mapParam[ParamsConstants.PAGE_SIZE] = "20"
            mapParam[ParamsConstants.PAGE] = page.toString()
            mapParam[ParamsConstants.MCH_ID] = MainApplication.getInstance().getMchId()

            if (isUnfrozen == 1) {
                mapParam[ParamsConstants.OPERATION_TYPE] = 2.toString()
            }

            //如果是收银员登录的同时收银员也没有账单权限
            if (MainApplication.getInstance().isAdmin(0) && MainApplication.getInstance()
                    .isOrderAuth("0")
            ) {
                mapParam[ParamsConstants.USER_ID] =
                    MainApplication.getInstance().getUserId().toString()
            }

            if (!StringUtil.isEmptyOrNull(userId)) {
                mapParam[ParamsConstants.USER_ID] = userId
            }

            if (!StringUtil.isEmptyOrNull(startDate)) {
                if (isRefund == 1) { //退款不用时分秒
                    mapParam[ParamsConstants.START_DATE] = startDate
                    mapParam[ParamsConstants.END_DATE] = startDate
                } else {
                    mapParam[ParamsConstants.START_DATE] = "$startDate 00:00:00"
                    mapParam[ParamsConstants.END_DATE] = "$startDate 23:59:59"
                }
            } else {
                if (isRefund == 1) { //退款不用时分秒
                    mapParam[ParamsConstants.START_DATE] =
                        DateUtil.formatYYMD(System.currentTimeMillis())
                    mapParam[ParamsConstants.END_DATE] =
                        DateUtil.formatYYMD(System.currentTimeMillis())
                } else {
                    mapParam[ParamsConstants.START_DATE] =
                        DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00"
                    mapParam[ParamsConstants.END_DATE] =
                        DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59"
                }
            }

            if (MainApplication.getInstance().userInfo.isAuthFreezeOpen == 1 && TextUtils.equals(
                    PreferenceUtil.getString(
                        ParamsConstants.BILL_LIST_CHOOSE_PAY_OR_PRE_AUTH,
                        "sale"
                    ),
                    "pre_auth"
                )
            ) {
                if (!StringUtil.isEmptyOrNull(order)) {
                    mapParam[ParamsConstants.AUTH_NO] = order
                }
            } else {
                if (!StringUtil.isEmptyOrNull(order)) {
                    mapParam[ParamsConstants.ORDER_NO_MCH] = order
                }
            }

            val spayRs = System.currentTimeMillis()
            //加签名
            if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getNewSignKey())) {
                mapParam[ParamsConstants.SPAY_RS] = spayRs.toString()
                mapParam[ParamsConstants.NNS] = SignUtil.getInstance().createSign(
                    JsonUtil.jsonToMap(JsonUtil.objectToJson(mapParam)),
                    MainApplication.getInstance().getNewSignKey()
                )
            }

            if (payTypeList != null && payTypeList.size > 0) {
                mapParam[ParamsConstants.API_PROVIDER_LIST] = payTypeList
            }
            val tradeType: ArrayList<Int> = ArrayList()
            val payType: ArrayList<Int> = ArrayList()
            parseToJson(listStr, tradeType, payType)
            if (tradeType.size > 0) {
                mapParam[ParamsConstants.API_PROVIDER_LIST] = tradeType
            }
            if (payType.size > 0) {
                mapParam[ParamsConstants.TRADE_STATE_LIST] = payType
            }

            AppClient.querySpayOrderNew(
                spayRs.toString(),
                JsonUtil.mapToJsons(mapParam),
                isRefund,
                isUnfrozen,
                object : CallBackUtil.CallBackCommonResponse<OrderList>(OrderList()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {

                        dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { response ->
                            querySpayOrderNewFailed(response.message)
                        }

                    }

                    override fun onResponse(r: CommonResponse?) {

                        dismissLoading(ParamsConstants.COMMON_LOADING)

                        r?.let { response ->
                            val sentryUrl =
                                if (TextUtils.isEmpty(response.url)) "" else response.url.replace(
                                    MainApplication.getInstance().baseUrl,
                                    ""
                                )
                            val dynModels = JsonUtil.jsonToBean(
                                response.message, OrderList::class.java
                            ) as OrderList
                            for (orderInfo in dynModels.data) {
                                if (!StringUtil.isEmptyOrNull(orderInfo.operateTime)) {
                                    try {
                                        orderInfo.setOperateTimeFormat(
                                            DateUtil.formartDateToHHMMSS(orderInfo.operateTime)
                                        )
                                    } catch (e: ParseException) {
                                        uploadNetInterfaceException(
                                            e, sentryUrl, "", ""
                                        )
                                    }
                                }
                                if (isRefund == 0) {
                                    val tradeTimeNew = orderInfo.tradeTimeNew
                                    if (!StringUtil.isEmptyOrNull(tradeTimeNew)) {
                                        try {
                                            orderInfo.setFormatTimePay(
                                                DateUtil.formartDateYYMMDD(
                                                    tradeTimeNew
                                                )
                                            )
                                            orderInfo.setFormartYYMM(
                                                DateUtil.formartDateYYMMDDTo(
                                                    tradeTimeNew
                                                )
                                            )
                                            orderInfo.setTradeTimeNew(
                                                DateUtil.formartDateToHHMMSS(
                                                    tradeTimeNew
                                                )
                                            )
                                        } catch (e: ParseException) {
                                            uploadNetInterfaceException(
                                                e, sentryUrl, "", ""
                                            )
                                        }
                                    }
                                } else if (isRefund == 1) {
                                    val time = orderInfo.addTimeNew
                                    orderInfo.setMoney(orderInfo.refundFee)
                                    if (!StringUtil.isEmptyOrNull(time)) {
                                        try {
                                            orderInfo.setFormatRefund(
                                                DateUtil.formartDateYYMMDD(
                                                    time
                                                )
                                            )
                                            orderInfo.setFormartYYMM(
                                                DateUtil.formartDateYYMMDDTo(
                                                    time
                                                )
                                            )
                                            orderInfo.setAddTimeNew(
                                                DateUtil.formartDateToHHMMSS(
                                                    time
                                                )
                                            )
                                        } catch (e: ParseException) {
                                            uploadNetInterfaceException(
                                                e, sentryUrl, "", ""
                                            )
                                            Log.e(TAG, Log.getStackTraceString(e))
                                        }
                                    }
                                } else {
                                    val useTimeNew = orderInfo.useTimeNew
                                    if (!StringUtil.isEmptyOrNull(useTimeNew)) {
                                        try {
                                            orderInfo.setFromatCard(
                                                DateUtil.formartDateYYMMDD(
                                                    useTimeNew
                                                )
                                            )
                                            orderInfo.setFormartYYMM(
                                                DateUtil.formartDateYYMMDDTo(
                                                    useTimeNew
                                                )
                                            )
                                            orderInfo.setUseTimeNew(
                                                DateUtil.formartDateToHHMMSS(
                                                    useTimeNew
                                                )
                                            )
                                        } catch (e: ParseException) {
                                            uploadNetInterfaceException(
                                                e, sentryUrl, "", ""
                                            )
                                        }
                                    }
                                }
                            }
                            try {
//                        Integer reqFeqTime = Integer.parseInt(TextUtils.isEmpty(dynModels.reqFeqTime)
//                        ? "0" : dynModels.reqFeqTime);

                                val reqFeqTime: Int =
                                    if (TextUtils.isEmpty(dynModels.reqFeqTime)) 0 else dynModels.reqFeqTime.toInt()
                                if (dynModels.data.size > 0 && dynModels.data[0] != null) {
                                    dynModels.data[0].setReqFeqTime(reqFeqTime)
                                } else {
                                    if (reqFeqTime > 0) {
                                        uploadNetInterfaceException(
                                            Exception("时间错误 reqFeqTime > 0"), sentryUrl, "", ""
                                        )

                                        querySpayOrderNewFailed("reqFeqTime=$reqFeqTime")

                                    }
                                }
                            } catch (e: Exception) {
                                uploadNetInterfaceException(
                                    e, sentryUrl, "", ""
                                )
                            }

                            querySpayOrderNewSuccess(dynModels.data, page, isLoadMore)

                        }
                    }
                }
            )
        }


    }


    private fun parseToJson(
        listStr: List<String>?, tradeType: ArrayList<Int>, payType: ArrayList<Int>
    ) {
        if (listStr != null && listStr.size > 0) {
            for (s in listStr) {
                if (s == "wx") {
                    tradeType.add(1)
                } else if (s == "zfb") {
                    tradeType.add(2)
                } else if (s == "jd") {
                    tradeType.add(12)
                } else if (s == "qq") {
                    tradeType.add(4)
                } else if (s == "2") {
                    payType.add(2)
                } else if (s == "1") {
                    payType.add(1)
                } else if (s == "3") {
                    payType.add(3)
                } else if (s == "4") {
                    payType.add(4)
                } else if (s == "8") {
                    payType.add(8)
                }
            }
        }
    }


    override fun attachView(view: TabBillContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}