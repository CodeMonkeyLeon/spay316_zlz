package cn.swiftpass.enterprise.utils;

import android.content.Context;
import android.util.Log;

import com.example.common.sentry.SentryUtils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

/**
 * Created by aijingya on 2020/3/10.
 *
 * @Package cn.swiftpass.enterprise.utils
 * @Description: 服务端证书校验工具类(用一句话描述该文件做什么)
 * @date 2020/3/10.15:53.
 */
public class CertificateUtils {

    //存储本地的在服务端证书列表
    private static ArrayList<Certificate> cerList = new ArrayList<Certificate>();

    //把本地的服务器证书列表读取出来，有几个读几个，全添加在列表里，暂时只有一个
    public static ArrayList<Certificate> getCertificateList() {
        cerList.clear();
        try {
            cerList.addAll(KotlinUtils.INSTANCE.getCertificatesFromAssets());
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
        return cerList;
    }


    //检查证书列表里的全部证书，是否都过期，只要有一个没过期就返回true，否则返回false
    public static boolean checkCertificatesListValid() {
        //如果本地没有证书，则直接返回false
        if (cerList == null || cerList.size() == 0) return false;
        for (int i = 0; i < cerList.size(); i++) {
            String message = "";
            Certificate c = cerList.get(i);
            if (c != null && c instanceof X509Certificate) {
                try {
                    ((X509Certificate) c).checkValidity();
                } catch (CertificateExpiredException Expired) {
                    message = "CertificateExpiredException";
                    continue;
                } catch (CertificateNotYetValidException e) {
                    message = "CertificateNotYetValidException";
                    continue;
                }
                Log.i("TAG_ZLZ", "本地证书至少有一个未过期");
                return true;
            }
        }
        Log.i("TAG_ZLZ", "本地证书全部过期");
        return false;
    }

//    /**
//     * 检查本地证书有效性
//     *
//     * @return 只要有一个没过期就返回true，否则返回false
//     */
//    public static final boolean checkLocalCertsListValidity() {
//        if (cerList == null || cerList.size() == 0) {
//            //如果本地没有证书，则直接返回false
//            return false;
//        }
//        int errorCertsCnt = 0;
//        for (int i = 0; i < cerList.size(); i++) {
//            Certificate c = cerList.get(i);
//            if (c != null && c instanceof X509Certificate) {
//                try {
//                    ((X509Certificate) c).checkValidity();
//                } catch (CertificateExpiredException Expired) {
//                    errorCertsCnt++;
//                } catch (CertificateNotYetValidException e) {
//                    errorCertsCnt++;
//                }
//            }
//        }
//        if (errorCertsCnt == cerList.size()) {
//            //全部过期
//            return false;
//        } else {
//            //有没过期的证书
//            return true;
//        }
//    }


    public static X509Certificate getX509CerCate(X509Certificate x509Certificate) throws Exception {
        Logger.i("读取Cer证书信息...");
        Logger.i("x509Certificate_SerialNumber_序列号___:" + x509Certificate.getSerialNumber());
        Logger.i("序列号:" + x509Certificate.getSerialNumber());
        Logger.i("x509Certificate_getIssuerDN_发布方标识名___:" + x509Certificate.getIssuerDN());
        Logger.i("x509Certificate_getSubjectDN_主体标识___:" + x509Certificate.getSubjectDN());
        Logger.i("x509Certificate_getSigAlgOID_证书算法OID字符串___:" + x509Certificate.getSigAlgOID());
        Logger.i("x509Certificate_getNotBefore_证书有效期___:" + x509Certificate.getNotBefore());
        Logger.i("x509Certificate_getNotAfter_证书有效期___:" + x509Certificate.getNotAfter());
        Logger.i("x509Certificate_getSigAlgName_签名算法___:" + x509Certificate.getSigAlgName());
        Logger.i("x509Certificate_getVersion_版本号___:" + x509Certificate.getVersion());
        Logger.i("x509Certificate_getPublicKey_公钥___:" + x509Certificate.getPublicKey());
        return x509Certificate;
    }


    public static X509Certificate getX509CerCateUt(Context context) throws Exception {
        InputStream input = context.getAssets().open("certs/swiftpassNew.cer");// 下载的证书放到项目中的assets目录中
        CertificateFactory cerFactory = CertificateFactory.getInstance("X.509");
        Certificate cer = cerFactory.generateCertificate(input);   //其中cer是APP中预埋的服务器端公钥证书

        X509Certificate x509Certificate = (X509Certificate) cer;

        String test = "";
        byte[] publicKeyString = Base64.decode(test);
        Certificate cerTest = cerFactory.generateCertificate(new ByteArrayInputStream(publicKeyString));
        String finalString = Base64.encode(cerTest.getPublicKey().getEncoded());

        Logger.i("读取Cer证书信息...");
        Logger.i("x509Certificate_SerialNumber_序列号___:" + x509Certificate.getSerialNumber());
        Logger.i("序列号:" + x509Certificate.getSerialNumber());
        Logger.i("x509Certificate_getIssuerDN_发布方标识名___:" + x509Certificate.getIssuerDN());
        Logger.i("x509Certificate_getSubjectDN_主体标识___:" + x509Certificate.getSubjectDN());
        Logger.i("x509Certificate_getSigAlgOID_证书算法OID字符串___:" + x509Certificate.getSigAlgOID());
        Logger.i("x509Certificate_getNotBefore_证书有效期___:" + x509Certificate.getNotBefore());
        Logger.i("x509Certificate_getNotAfter_证书有效期___:" + x509Certificate.getNotAfter());
        Logger.i("x509Certificate_getSigAlgName_签名算法___:" + x509Certificate.getSigAlgName());
        Logger.i("x509Certificate_getVersion_版本号___:" + x509Certificate.getVersion());
        Logger.i("x509Certificate_getPublicKey_公钥___:" + x509Certificate.getPublicKey());
        return x509Certificate;
    }

}
