package cn.swiftpass.enterprise.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.TextUtils;
import android.util.Log;

import com.example.common.sentry.SentryUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Thread.UncaughtExceptionHandler;


/**
 * 捕获所有异常信息
 */
public class ErrorHandler implements UncaughtExceptionHandler {
    private static final String TAG = ErrorHandler.class.getSimpleName();
    private static ErrorHandler errorHandler;
    private Context mContext;

    private ErrorHandler() {
    }

    public static ErrorHandler getInstance() {
        if (errorHandler == null) {
            errorHandler = new ErrorHandler();
        }
        return errorHandler;
    }

    /**
     * 删除1.3.2后的版本日志
     */
    public void delete() {
        File file = FileUtils.getLogFile("log.txt");
        if (file.exists()) {
            boolean isSuccess = file.delete();
            Log.i(TAG, "delete file " + isSuccess);
        }
    }

    public void init(Context context) {
        mContext = context;
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {

        String sdCardPath = null;
        sdCardPath = AppHelper.getCacheDir();
        if (TextUtils.isEmpty(sdCardPath)) {
            return;
        }
        File logFile = FileUtils.getLogFile("error.txt");

        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch blo
                Log.e(TAG, Log.getStackTraceString(e));
            }
        }
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(new FileWriter(logFile, true));
            pw.println("--------------------");
            pw.println("时间 " + DateUtil.getTodayDateTime());
            pw.println("版本 " + mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), PackageManager.GET_CONFIGURATIONS).versionName);
            pw.println("--------------------");
            Log.e(TAG, Log.getStackTraceString(ex));
        } catch (FileNotFoundException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        } catch (IOException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        } catch (NameNotFoundException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        } finally {
            if (pw != null) {
                try {
                    pw.close();
                } catch (Exception e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }

            if (ex != null) {
                Log.e("TAG_SERVICE", ex.getMessage());
                SentryUtils.INSTANCE.uploadUncaughtThrowable(ex,
                        SentryUtils.INSTANCE.getClassNameAndMethodName());
            }
        }
    }

}
