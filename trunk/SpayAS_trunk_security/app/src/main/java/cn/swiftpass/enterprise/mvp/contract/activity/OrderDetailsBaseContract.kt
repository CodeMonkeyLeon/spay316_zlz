package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkOrder

/**
 * @author lizheng.zhao
 * @date 2022/11/23
 *
 * @你在雾海中航行
 * @没有帆
 * @你在月夜下漂泊
 * @没有锚
 * @路从这里消失
 * @夜从这里消失
 */
class OrderDetailsBaseContract {


    interface View : BaseView {

        fun updateOrderStatusSuccess(response: PaymentLinkOrder)

        fun updateOrderStatusFailed(error: Any?)

        fun getOrderDetailSuccess(response: PaymentLinkOrder)

        fun getOrderDetailFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {

        fun getOrderDetail(order: PaymentLinkOrder?)

        fun updateOrderStatus(order: PaymentLinkOrder?)
    }

}