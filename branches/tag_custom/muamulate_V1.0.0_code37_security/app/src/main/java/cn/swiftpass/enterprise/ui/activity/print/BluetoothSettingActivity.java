/*
 * 文 件 名:  BluetoothSettingActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2017-4-13
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.print;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;



import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.ContentTextActivity;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.marketing.MarketListView;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 蓝牙打印设置列表
 *
 * @author he_hui
 * @version [版本号, 2017-4-13]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@SuppressLint("NewApi")
public class BluetoothSettingActivity extends TemplateActivity {
    private static final String TAG = BluetoothSettingActivity.class.getSimpleName();

    private MyAdape myAdape;

    private ViewPayTypeHolder holder;

    private LinearLayout lay_choise;

    private BluetoothAdapter mBtAdapter;

    private List<BluetoothDevice> deviceList;

    private MarketListView pairedListView;

    private ImageView iv_voice, iv_print;

    private TextView tv_null_info;

    boolean connState;// 连接状态

    private DialogInfo dialog;

    private TextView tv_blue_scaning, tv_blue_help, tv_blue_help_two;

    private ImageView iView;

    private LinearLayout lay_id_print;

    private Handler handler = new Handler() {
        @SuppressLint("NewApi")
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:// 连接失败
                    dissDialog();
                    toastDialog(BluetoothSettingActivity.this, R.string.tx_blue_conn_fail, null);
                    break;
                case 2:// 蓝牙可以被搜索
                    break;
                case 3:// 设备已经接入
                    break;
                case 4:// 已连接某个设备
                    dismissLoading();
                    if (iView != null) {
                        iView.setVisibility(View.VISIBLE);
                    }
                    HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET, HandlerManager.BLUE_CONNET);
                    toastDialog(BluetoothSettingActivity.this, R.string.tx_blue_conn_succ, new NewDialogInfo.HandleBtn() {
                        @Override
                        public void handleOkBtn() {
                            finish();
                        }

                    });
                    break;

                case HandlerManager.BLUE_CONNET_STUTS_CLOSED:// 蓝牙关闭
                    pairedListView.setVisibility(View.GONE);
                    tv_blue_scaning.setVisibility(View.GONE);
                    ly_sv.setVisibility(View.GONE);
                    tv_null_info.setVisibility(View.VISIBLE);
                    ly_colse.setVisibility(View.VISIBLE);
                    tv_null_info.setText(R.string.tx_blue_closed);
                    break;
                case HandlerManager.BLUE_CONNET_STUTS:// 蓝牙已连接上后
                    if (MainApplication.getBluePrintSetting()) {
                        pairedListView.setVisibility(View.VISIBLE);
                        tv_blue_scaning.setVisibility(View.VISIBLE);
                        ly_sv.setVisibility(View.VISIBLE);
                        //                        tv_null_info.setText(R.string.tx_blue_close);
                        tv_null_info.setVisibility(View.GONE);
                        ly_colse.setVisibility(View.GONE);
                        tv_blue_scaning.setVisibility(View.VISIBLE);
                        doDiscovery();
                    }
                    break;
            }
        }
    };

    /**
     * 蓝牙UUID
     */
    public static final UUID SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    /**
     * 配对成功后的蓝牙套接字
     */
    private BluetoothSocket mBluetoothSocket;

    /**
     * @return 返回 mBluetoothSocket
     */
    public BluetoothSocket getmBluetoothSocket() {
        return mBluetoothSocket;
    }

    /**
     * @param
     */
    public void setmBluetoothSocket(BluetoothSocket mBluetoothSocket) {
        this.mBluetoothSocket = mBluetoothSocket;
    }

    /**
     * @return 返回 mBtAdapter
     */
    public BluetoothAdapter getmBtAdapter() {
        return mBtAdapter;
    }

    private ScrollView ly_sv;

    private LinearLayout ly_colse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_setting);

        initView();

        setLister();

        initValue();

        // 注册handler 检查蓝牙打开和关闭的操作
        HandlerManager.registerHandler(HandlerManager.BLUE_CONNET_STUTS, handler);

    }

//    @Override
//    public boolean equals(Object obj) {
//        return super.equals(obj);
//    }

    private void initValue() {
        deviceList = new ArrayList<BluetoothDevice>();
        myAdape = new MyAdape(BluetoothSettingActivity.this, deviceList);
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();

        pairedListView.setAdapter(myAdape);

        connState = PreferenceUtil.getBoolean("connState" + ApiConstant.bankCode, true);
        if (MainApplication.getBluePrintSetting()) {
            iv_voice.setImageResource(R.drawable.button_configure_switch_default);
            pairedListView.setVisibility(View.VISIBLE);
            tv_null_info.setVisibility(View.GONE);
            ly_sv.setVisibility(View.VISIBLE);
            ly_colse.setVisibility(View.GONE);
            if (!StringUtil.isEmptyOrNull(MainApplication.getBlueDeviceName())) {
                Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();
                if (pairedDevices.size() > 0) {
                    for (BluetoothDevice device : pairedDevices) {
                        if (MainApplication.getBlueDeviceAddress().equals(device.getAddress())) {
                            deviceList.add(device);
                        }
                    }

                    myAdape.notifyDataSetChanged();
                    tv_blue_scaning.setVisibility(View.GONE);
                } else {
                    if (!mBtAdapter.isEnabled()) { // 蓝牙没有打开
                        openBlue(true);
                    } else {
                        doDiscovery();
                    }
                }
            } else {
                // 默认去加载收索附件的蓝牙设备
                if (mBtAdapter == null) {// 说明手机不带蓝牙模块
                    blueCloseState();
                    toastDialog(BluetoothSettingActivity.this, R.string.tx_blue_no_device, new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            BluetoothSettingActivity.this.finish();
                        }

                    });

                    return;
                }

                if (!mBtAdapter.isEnabled()) { // 蓝牙没有打开
                    openBlue(false);
                } else {
                    doDiscovery();
                }
            }
        } else {
            tv_blue_scaning.setVisibility(View.GONE);
            blueCloseState();
        }

        //

    }

    /**
     * 用户打开蓝牙时，选择的返回处理
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                doDiscovery();
            } else if (resultCode == RESULT_CANCELED) {
                toastDialog(BluetoothSettingActivity.this, R.string.tx_blue_open_fail, new NewDialogInfo.HandleBtn() {

                    @Override
                    public void handleOkBtn() {
                        BluetoothSettingActivity.this.finish();
                    }

                });
            }
        }
    }

    void openBlue(final boolean isFinish) {
        dialog = new DialogInfo(BluetoothSettingActivity.this, null, getString(R.string.tx_blue_no_open), getString(R.string.to_open), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                tv_blue_scaning.setVisibility(View.VISIBLE);
                dialog.cancel();
                dialog.dismiss();
                Intent mIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(mIntent, 1);

            }

            @Override
            public void handleCancleBtn() {
                dialog.cancel();
                if (isFinish) {
                    BluetoothSettingActivity.this.finish();
                }
            }
        }, null);

        DialogHelper.resize(BluetoothSettingActivity.this, dialog);
        dialog.show();
    }

    /**
     * Start device discover with the BluetoothAdapter
     */
    private void doDiscovery() {
        tv_blue_scaning.setVisibility(View.VISIBLE);
        if (mBtAdapter.isDiscovering()) {
            mBtAdapter.cancelDiscovery();
        }

        deviceList.clear();
        mBtAdapter.startDiscovery();
    }

    void blueCloseState() {
        iv_voice.setImageResource(R.drawable.button_configure_switch_close);
        pairedListView.setVisibility(View.GONE);
        tv_null_info.setVisibility(View.VISIBLE);
        ly_sv.setVisibility(View.GONE);
        ly_colse.setVisibility(View.VISIBLE);
    }

    /**
     * 尝试配对和连接
     *
     * @param btDev
     */
    public void createBond(BluetoothDevice btDev, Handler handler) {
        BluetoothSettingActivity.this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                loadDialog(BluetoothSettingActivity.this, R.string.tx_blue_conn_deviceing);
            }
        });
        connect(btDev, handler);
    }

    /**
     * 尝试连接一个设备，子线程中完成，因为会线程阻塞
     *
     * @param btDev   蓝牙设备对象
     * @param handler 结果回调事件
     * @return
     */
    private void connect(BluetoothDevice btDev, Handler handler) {
        Message message = new Message();
        try {
            // 通过和服务器协商的uuid来进行连接
            mBluetoothSocket = btDev.createRfcommSocketToServiceRecord(SPP_UUID);
            // 全局只有一个bluetooth，所以我们可以将这个socket对象保存在appliaction中
            // BltAppliaction.bluetoothSocket = mBluetoothSocket;
            // 通过反射得到bltSocket对象，与uuid进行连接得到的结果一样，但这里不提倡用反射的方法
            // mBluetoothSocket =
            // (BluetoothSocket)btDev.getClass()
            // .getMethod("createRfcommSocket", new Class[] {int.class})
            // .invoke(btDev, 1);
            if (mBluetoothSocket != null) MainApplication.bluetoothSocket = mBluetoothSocket;
            // 在建立之前调用
            if (getmBtAdapter().isDiscovering())
                // 停止搜索
                getmBtAdapter().cancelDiscovery();
            // 如果当前socket处于非连接状态则调用连接
            if (getmBluetoothSocket() != null) {

                if (!getmBluetoothSocket().isConnected()) {
                    // 你应当确保在调用connect()时设备没有执行搜索设备的操作。
                    // 如果搜索设备也在同时进行，那么将会显著地降低连接速率，并很大程度上会连接失败。
                    getmBluetoothSocket().connect();
                }
            } else {
                return;
            }
            Logger.i("hehui", "已经链接"+Thread.currentThread().getName());
            if (handler == null) return;
            // 结果回调
            MainApplication.setBlueDeviceName(btDev.getName());
            MainApplication.setBlueDeviceNameAddress(btDev.getAddress());
            MainApplication.setBlueState(true);
            message.what = 4;
            message.obj = btDev;
            handler.sendMessage(message);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //连接成功要发送一个广播，通知前一个activity-----settingMoreActivity更新蓝牙状态
                    Intent intent=new Intent("action_bluetooth_connect_is_success");
                    sendBroadcast(intent,MainApplication.permission);
                }
            });


        } catch (Exception e) {
            try {
                message.what = 1;
                handler.sendMessage(message);
                getmBluetoothSocket().close();
            } catch (IOException e1) {
                Log.e(TAG,Log.getStackTraceString(e1));
            }
            Log.e(TAG,Log.getStackTraceString(e));
        }
    }

    /**
     * 按钮设置监听 <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void setLister() {
        lay_id_print.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (MainApplication.getBluePrintSetting()) {// 打印机处于可以打印的状态，才可以设置是否自动打印
                    if (MainApplication.getAutoBluePrintSetting()) {
                        MainApplication.setAutoBluePrintSetting(false);
                        iv_print.setImageResource(R.drawable.button_configure_switch_close);

                    } else {
                        iv_print.setImageResource(R.drawable.button_configure_switch_default);
                        MainApplication.setAutoBluePrintSetting(true);

                    }
                }
            }
        });

        tv_blue_help_two.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String language = PreferenceUtil.getString("language", "");
                if (!TextUtils.isEmpty(language)) {
                    ContentTextActivity.startActivity(BluetoothSettingActivity.this, "https://app.wepayez.com/web/help/print-all.html?language=" + language, R.string.title_help);
                } else {
                    String lan = Locale.getDefault().toString();
                    if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_HANS)) {
                        ContentTextActivity.startActivity(BluetoothSettingActivity.this, "https://app.wepayez.com/web/help/print-all.html?language=zh_cn", R.string.title_help);
                    } else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO) ||
                            lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_HANT)) {
                        ContentTextActivity.startActivity(BluetoothSettingActivity.this, "https://app.wepayez.com/web/help/print-all.html?language=zh_tw", R.string.title_help);
                    } else {
                        ContentTextActivity.startActivity(BluetoothSettingActivity.this, "https://app.wepayez.com/web/help/print-all.html?language=en_us", R.string.title_help);
                    }
                }
            }
        });
        tv_blue_help.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String language = PreferenceUtil.getString("language", "");
                if (!TextUtils.isEmpty(language)) {
                    ContentTextActivity.startActivity(BluetoothSettingActivity.this, "https://app.wepayez.com/web/help/print-all.html?language=" + language, R.string.title_help);
                } else {
                    String lan = Locale.getDefault().toString();
                    if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_HANS)) {
                        ContentTextActivity.startActivity(BluetoothSettingActivity.this, "https://app.wepayez.com/web/help/print-all.html?language=zh_cn", R.string.title_help);
                    } else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO) ||
                            lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_HANT)) {
                        ContentTextActivity.startActivity(BluetoothSettingActivity.this, "https://app.wepayez.com/web/help/print-all.html?language=zh_tw", R.string.title_help);
                    } else {
                        ContentTextActivity.startActivity(BluetoothSettingActivity.this, "https://app.wepayez.com/web/help/print-all.html?language=en_us", R.string.title_help);
                    }
                }
            }
        });

        pairedListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, final View view, int position, long arg3) {
                final BluetoothDevice bluetoothDevice = deviceList.get(position);
                loadDialog(BluetoothSettingActivity.this, R.string.tx_blue_conn_deviceing);
                iView = (ImageView) view.findViewById(R.id.iv_choice);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        createBond(bluetoothDevice, handler);

                    }
                }).start();
            }
        });

        lay_choise.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (MainApplication.getBluePrintSetting()) {
                    iv_voice.setImageResource(R.drawable.button_configure_switch_close);
                    tv_null_info.setVisibility(View.VISIBLE);
                    ly_colse.setVisibility(View.VISIBLE);
                    connState = false;
                    deviceList.clear();
                    MainApplication.setBluePrintSetting(false);
                    // PreferenceUtil.removeKey("blueDeviceAddress" + MainApplication.getMchId());
                    // PreferenceUtil.removeKey("blueDevice" + MainApplication.getMchId());
                    // // PreferenceUtil.commitBoolean("connState" + ApiConstant.bankCode, false);//默认连接打印机打开
                    MainApplication.setBlueDeviceName("");
                    MainApplication.setBlueDeviceNameAddress("");
                    MainApplication.setBlueState(false);
                    MainApplication.bluetoothSocket = null;
                    HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET, HandlerManager.BLUE_CONNET);
                    pairedListView.setVisibility(View.GONE);
                    tv_blue_scaning.setVisibility(View.GONE);
                    ly_sv.setVisibility(View.GONE);

                    //如果切换蓝牙为连接的状态要同步广播去更新settingMoreActivity的状态
                    Intent intent=new Intent("action_bluetooth_connect_is_cancel");
                    sendBroadcast(intent,MainApplication.permission);
                } else {
                    connState = true;
                    deviceList.clear();
                    MainApplication.setBluePrintSetting(true);
                    ly_sv.setVisibility(View.VISIBLE);
                    iv_voice.setImageResource(R.drawable.button_configure_switch_default);
                    pairedListView.setVisibility(View.VISIBLE);
                    tv_null_info.setVisibility(View.GONE);
                    ly_colse.setVisibility(View.GONE);
                    // PreferenceUtil.commitBoolean("connState" + ApiConstant.bankCode, true);//默认连接打印机打开
                    if (!mBtAdapter.isEnabled()) { // 蓝牙没有打开
                        openBlue(false);
                    } else {
                        tv_blue_scaning.setVisibility(View.VISIBLE);
                        doDiscovery();
                    }
                }

            }
        });
    }

    @Override
    protected void onStop() {
        if (mBtAdapter != null && mBtAdapter.isDiscovering()) {
            mBtAdapter.cancelDiscovery();
        }
        this.unregisterReceiver(mReceiver);
        super.onStop();
    }

    @Override
    protected void onResume() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(mReceiver, filter);

        super.onResume();
    }

    Map<String, BluetoothDevice> deviceMap = new HashMap<String, BluetoothDevice>();

    /**
     * 打开蓝牙
     */
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                try {

                    if (device != null) { // 蓝牙打印设备类似
                        for (int i = 0; i < deviceList.size(); i++) {
                            BluetoothDevice info = deviceList.get(i);
                            if (StringUtil.isEmptyOrNull(device.getName()) || info.getAddress().equalsIgnoreCase(device.getAddress())) {
                                return;
                            }
                        }

                        if (MainApplication.getBluePrintSetting()) {
                            if (!StringUtil.isEmptyOrNull(device.getName())) {
                                deviceList.add(device);
                            }
                            myAdape.notifyDataSetChanged();
                            myAdape.notifyDataSetInvalidated();
                        }
                    }

                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
                // ListViewUtil.setListViewHeightBasedOnChildren(pairedListView, 0);
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                tv_blue_scaning.setVisibility(View.GONE);
                dismissLoading();
            }
        }
    };

    /**
     * 页面参数 初始化 <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initView() {
        iv_print = getViewById(R.id.iv_print);

        if (MainApplication.getAutoBluePrintSetting()) {
            iv_print.setImageResource(R.drawable.button_configure_switch_default);
        } else {
            iv_print.setImageResource(R.drawable.button_configure_switch_close);
        }

        lay_id_print = getViewById(R.id.lay_id_print);
        tv_blue_help_two = getViewById(R.id.tv_blue_help_two);
        tv_blue_help = getViewById(R.id.tv_blue_help);
        tv_blue_scaning = getViewById(R.id.tv_blue_scaning);
        ly_colse = getViewById(R.id.ly_colse);
        ly_sv = getViewById(R.id.ly_sv);
        tv_null_info = getViewById(R.id.tv_null_info);
        iv_voice = getViewById(R.id.iv_voice);
        lay_choise = getViewById(R.id.lay_choise);
        pairedListView = getViewById(R.id.paired_devices);

    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tx_print_title_info);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButLayClick() {

            }
        });
    }

    class MyAdape extends BaseAdapter {

        private List<BluetoothDevice> list;

        private Context context;

        private MyAdape(Context context, List<BluetoothDevice> list) {
            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = View.inflate(context, R.layout.buletooth_list_item, null);
                holder = new ViewPayTypeHolder();
                holder.iv_choice = (ImageView) convertView.findViewById(R.id.iv_choice);
                holder.userName = (TextView) convertView.findViewById(R.id.userName);
                convertView.setTag(holder);
            } else {
                holder = (ViewPayTypeHolder) convertView.getTag();
            }
            BluetoothDevice deviceInfo = list.get(position);
            if (!StringUtil.isEmptyOrNull(deviceInfo.getName())) {
                holder.userName.setText(deviceInfo.getName());
            }
            String name = MainApplication.getBlueDeviceAddress();
            if (!StringUtil.isEmptyOrNull(name) && name.equalsIgnoreCase(deviceInfo.getAddress())) {// 判断是否连接
                holder.iv_choice.setVisibility(View.VISIBLE);
            } else {
                holder.iv_choice.setVisibility(View.GONE);
            }
            return convertView;
        }
    }

    class ViewPayTypeHolder {
        ImageView iv_choice;

        private TextView userName;
    }
}
