package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created by aijingya on 2021/3/12.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description:
 * @date 2021/3/12.13:42.
 */
public class MasterCardOrderInfo implements Serializable {

    public int tradeState;//交易状态：(1.初始化，2：支付成功，3：支付失败)
    public String transactionId;//第三方订单号
    public String outTradeNo;//商户订单号
    public int money;//消费金额
    public String currency;//币种
    public String body;//附加信息
    public String attach;//备注
    public String tradeTime;//交易时间
    public String service;//支付类型
    public String paymentType;//订单所属类型 DB：消费 RF：退款
    public String cardBin;//卡号前6位
    public String last4Digits;//卡号后四位
    public String paymentBrand;//卡所属的卡组织
    public String userName;//收银员名称
    public int orderAmount;//商品金额
    public int tipFee;//小费
    public int surcharge;//附加费
    public String tradeName;//支付名称
    public String orderNoMch;//平台订单号
    public String apiCode;//接口提供方

    public int getTradeState() {
        return tradeState;
    }

    public void setTradeState(int tradeState) {
        this.tradeState = tradeState;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getTradeTime() {
        return tradeTime;
    }

    public void setTradeTime(String tradeTime) {
        this.tradeTime = tradeTime;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getCardBin() {
        return cardBin;
    }

    public void setCardBin(String cardBin) {
        this.cardBin = cardBin;
    }

    public String getLast4Digits() {
        return last4Digits;
    }

    public void setLast4Digits(String last4Digits) {
        this.last4Digits = last4Digits;
    }

    public String getPaymentBrand() {
        return paymentBrand;
    }

    public void setPaymentBrand(String paymentBrand) {
        this.paymentBrand = paymentBrand;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(int orderAmount) {
        this.orderAmount = orderAmount;
    }

    public int getTipFee() {
        return tipFee;
    }

    public void setTipFee(int tipFee) {
        this.tipFee = tipFee;
    }

    public int getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(int surcharge) {
        this.surcharge = surcharge;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getOrderNoMch() {
        return orderNoMch;
    }

    public void setOrderNoMch(String orderNoMch) {
        this.orderNoMch = orderNoMch;
    }

    public String getApiCode() {
        return apiCode;
    }

    public void setApiCode(String apiCode) {
        this.apiCode = apiCode;
    }
}
