package cn.swiftpass.enterprise.utils;

import java.util.regex.Pattern;

/**
 * Created by congwei.li on 2021/9/10.
 *
 * @Description:
 */
public class RegexUtil {
    public static boolean isEmail(String email) {
        String regex = "^[0-9a-zA-Z][a-zA-Z0-9\\._-]{1,}@[a-zA-Z0-9-]{1,}[a-zA-Z0-9](\\.[a-zA-Z]{1,})+$";
        return Pattern.matches(regex, email);
    }
}
