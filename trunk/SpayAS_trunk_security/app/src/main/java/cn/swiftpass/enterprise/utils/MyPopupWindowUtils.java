package cn.swiftpass.enterprise.utils;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;

import cn.swiftpass.enterprise.common.Constant;
import cn.swiftpass.enterprise.intl.R;


public class MyPopupWindowUtils implements OnClickListener {

    private OnPopuWindowItemClickListener listener;

    private Context context;

    public interface OnPopuWindowItemClickListener {
        void onPopuWindowItemClick(View v);
    }

    public MyPopupWindowUtils(Context context, OnPopuWindowItemClickListener listener) {
        this.context = context;
        this.listener = listener;
    }
    
   /* public PopupWindow getDealPageChangeDialog(int width, String[] data, final Handler handler)
    {
        View view = View.inflate(context, R.layout.deal_scatter_popwindow_listview, null);
        //       String[] data = context.getResources().getStringArray(R.array.deal_scatter_type);
        final PopupWindow pop = new PopupWindow(view, width, ViewGroup.LayoutParams.MATCH_PARENT);
        ListView listview = (ListView)view.findViewById(R.id.listview_popwindow);
        listview.setAdapter(new ArrayAdapter<String>(context, R.layout.deal_scatter_type_item,
            R.id.deal_scatter_type_item_name_tv, data));
        LinearLayout ll = (LinearLayout)view.findViewById(R.id.listview_popwindow_bak);
        ll.setOnClickListener(new OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                pop.dismiss();
            }
        });
        
        listview.setOnItemClickListener(new OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                view.findViewById(R.id.deal_scatter_type_item_selected_ico).setVisibility(View.VISIBLE);
                TextView t = (TextView)view.findViewById(R.id.deal_scatter_type_item_name_tv);
                t.setTextColor(Color.parseColor("#ff7f00"));
                if (handler != null)
                {
                    Message msg = Message.obtain();
                    msg.what = DealScatterFragmentActivity.POPWINDOWN_CLICK;
                    msg.arg1 = position;
                    //msg.obj = data[position];
                    handler.sendMessageDelayed(msg, 90);
                }
            }
        });
        pop.setWidth(width);
        pop.setAnimationStyle(R.style.popuwindow_in_out_style);
        pop.update();
        // 设置PopupWindow外部区域是否可触摸
        pop.setBackgroundDrawable(context.getResources().getDrawable(android.R.color.transparent));
        pop.setFocusable(true); // 设置PopupWindow可获得焦点
        pop.setTouchable(true); // 设置PopupWindow可触摸
        pop.setOutsideTouchable(false); // 设置非PopupWindow区域可触摸
        return pop;
    }*/

    /**
     * {@inheritDoc}
     */

    @Override
    public void onClick(View v) {
        if (listener != null) {
            listener.onPopuWindowItemClick(v);
        }
    }

    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @return
     * @see [类、类#方法、类#成员]
     */
    public PopupWindow getPayHelpDialog(String payType, int width) {
        View v = View.inflate(context, R.layout.micropay_help_layout, null);
        final PopupWindow pop = new PopupWindow(v, width, ViewGroup.LayoutParams.MATCH_PARENT);
        ImageView micropay_help_img = (ImageView) v.findViewById(R.id.micropay_help_img);
        //支付帮助
        if (payType.equals(Constant.PAY_WX_MICROPAY)) {
            //微信扫码
            micropay_help_img.setImageDrawable(context.getResources().getDrawable(R.drawable.micropay_help_weixin_fs));
        } else if (payType.equals(Constant.PAY_ZFB_MICROPAY)) {
            //支付宝扫码
            micropay_help_img.setImageDrawable(context.getResources().getDrawable(R.drawable.micropay_help_zfb_fs));
        }
        micropay_help_img.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                pop.dismiss();
            }
        });
        pop.setWidth(width);
        pop.setAnimationStyle(android.R.style.Animation_Dialog);
        pop.update();
        // 设置PopupWindow外部区域是否可触摸
        pop.setBackgroundDrawable(context.getResources().getDrawable(android.R.color.transparent));
        pop.setFocusable(true); // 设置PopupWindow可获得焦点
        pop.setTouchable(true); // 设置PopupWindow可触摸
        pop.setOutsideTouchable(false); // 设置非PopupWindow区域可触摸
        return pop;
    }
}
