package cn.swiftpass.enterprise.mvp.contract.dialog

import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/12/7
 *
 * @所谓最难忘的
 * @就是从来不曾想起
 * @却永远也不会忘记
 */
class ScanCodeCheckContract {


    interface View : BaseView {

        fun loginAsyncSuccess(response: Boolean)

        fun loginAsyncFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {

        fun loginAsync(
            code: String?,
            username: String?,
            password: String?
        )

    }


}