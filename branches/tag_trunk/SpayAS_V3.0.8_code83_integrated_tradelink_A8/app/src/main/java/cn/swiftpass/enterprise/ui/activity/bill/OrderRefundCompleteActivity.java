/*
 * 文 件 名:  ReportActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-8
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.bill;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.print.PrintOrder;
import cn.swiftpass.enterprise.ui.activity.PayResultActivity;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.print.BluePrintUtil;
import cn.swiftpass.enterprise.ui.activity.print.BluetoothSettingActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftplus.enterprise.printsdk.print.PrintClient;

import static cn.swiftpass.enterprise.intl.BuildConfig.IS_POS_VERSION;

/**
 * 退款中
 *
 * @author he_hui
 * @version [版本号, 2016-11-8]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class OrderRefundCompleteActivity extends TemplateActivity {
    private static final String TAG = OrderRefundCompleteActivity.class.getSimpleName();
    private Order orderModel;

    private TextView tx_order, id_total;

    //private TextView tv_money;

    private TextView et_money, tx_time, tv_user, tx_surcharge, tx_withholding;

    private LinearLayout ly_user, id_lin_withholding, id_lin_surcharge,id_order_line;

    private Button btn_next_step;

    public static void startActivity(Context context, Order orderModel) {
        Intent it = new Intent();
        it.setClass(context, OrderRefundCompleteActivity.class);
        it.putExtra("order", orderModel);
        context.startActivity(it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_order_refund_complete);
        MainApplication.listActivities.add(this);
        initview();
        setLister();
        orderModel = (Order) getIntent().getSerializableExtra("order");
        //Mark ---- 此处因为在前一个的界面已经把orderModel的setMoney改为了RefundMoney，故此处设置的退款金额直接从前面的界面取的
        if (orderModel != null) {
            orderModel.setRefundMoney(orderModel.getMoney());
            id_total.setText(MainApplication.feeFh + DateUtil.formatMoneyUtils(orderModel.getOrderFee()));
            tx_order.setText(orderModel.getRefundNo());
            et_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(orderModel.getMoney()));
            tx_time.setText(orderModel.getAddTimeNew());
        }
        if (MainApplication.isAdmin.equals("1")) {
            //            ly_user.setVisibility(View.GONE);
            ly_user.setVisibility(View.VISIBLE);
            tv_user.setText(MainApplication.mchName);
            //此时要在打印小票的时候更新申请人
            if (orderModel != null) {
                orderModel.setUserName(MainApplication.mchName);
            }
        } else {
            ly_user.setVisibility(View.VISIBLE);
            tv_user.setText(MainApplication.realName);
            //此时要在打印小票的时候更新申请人
            if (orderModel != null) {
                orderModel.setUserName(MainApplication.realName);
            }
        }
//        orderModel.setTotalFee(orderModel.getMoney());
        if (!MainApplication.isSurchargeOpen()) {
            id_lin_surcharge.setVisibility(View.GONE);
            id_order_line.setVisibility(View.GONE);
        } else {
            if (orderModel != null) {
                if(orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") || orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")){
                    id_lin_surcharge.setVisibility(View.GONE);
                    id_order_line.setVisibility(View.GONE);
                }else{
                    tx_surcharge.setText(MainApplication.feeFh + DateUtil.formatMoneyUtils(orderModel.getSurcharge()));
                }
            }
        }

       /* if (!MainApplication.isTaxRateOpen()) {
            id_lin_withholding.setVisibility(View.GONE);
        } else {
            if (orderModel != null) {
                tx_withholding.setText(MainApplication.feeFh + DateUtil.formatMoneyUtils(orderModel.getWithholdingTax()));
            }
        }*/
        id_lin_withholding.setVisibility(View.GONE);

        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印
        if(IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)){
            //设置自动打印
            if(orderModel != null){
                print(orderModel);
            }
        }else{
            if (MainApplication.getAutoBluePrintSetting()) {//设置自动打印
                if (orderModel != null) {
                    OrderRefundCompleteActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            print(orderModel);
                        }
                    });
                }
            }
        }


    }

    private void setLister() {
        btn_next_step.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                for (Activity a : MainApplication.listActivities) {
                    a.finish();
                }
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印的绑定操作
        if(IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)){
            PrintClient.getInstance().bindDeviceService();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印的解绑操作
        if(IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)){
            PrintClient.getInstance().unbindDeviceService();
        }
    }


    private void print(final Order orderModel) {
        //调用打印机打印
        orderModel.setPay(false);
        orderModel.setPrintInfo(getString(R.string.tx_print_refund_info));
        orderModel.setPartner(getString(R.string.tv_pay_user_stub));

        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印
        if(IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)){
            if(BuildConfig.posTerminal.equalsIgnoreCase("A8")){
                orderModel.setPrintInfo(getString(R.string.tx_print_refund_info_A8));
            }
            PrintOrder.printOrderDetails(OrderRefundCompleteActivity.this,false, orderModel);
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    try {
                        orderModel.setPay(false);
                        orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
                        PrintOrder.printOrderDetails(OrderRefundCompleteActivity.this, false,orderModel);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            Timer timer = new Timer();
            timer.schedule(task, PrintClient.getInstance().getPrintDelay());
        }else{
            if (!MainApplication.getBluePrintSetting()) {//关闭蓝牙打印
                showDialog();
                return;
            }
            if (MainApplication.bluetoothSocket != null && MainApplication.bluetoothSocket.isConnected()) {
                OrderRefundCompleteActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            try {
                                BluePrintUtil.print(false,orderModel);
                                //打印第二联
                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
                                        BluePrintUtil.print(false,orderModel);

                                    }
                                }, 3000);
                            } catch (Exception e) {
                                Log.e(TAG, Log.getStackTraceString(e));
                            }
                        } catch (Exception e) {
                            Log.e(TAG, Log.getStackTraceString(e));
                            Logger.e("hehui", "print error " + e);
                        }
                    }
                });
            } else {
                String bluetoothDeviceAddress = MainApplication.getBlueDeviceAddress();
                if (!StringUtil.isEmptyOrNull(bluetoothDeviceAddress)) {
                    boolean isSucc = BluePrintUtil.blueConnent(bluetoothDeviceAddress, OrderRefundCompleteActivity.this);
                    if (isSucc) {
                        try {
                            BluePrintUtil.print(false,orderModel);
                            new Handler().postDelayed(new Runnable() {

                                @Override
                                public void run() {
                                    orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
                                    BluePrintUtil.print(false,orderModel);

                                }
                            }, 3000);
                        } catch (Exception e) {
                            Log.e(TAG, Log.getStackTraceString(e));
                        }
                    }
                } else {
                    showDialog();
                }
            }
        }

    }

    void showDialog() {
        dialog = new DialogInfo(OrderRefundCompleteActivity.this, null, getString(R.string.tx_blue_set), getString(R.string.title_setting), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                showPage(BluetoothSettingActivity.class);
                dialog.cancel();
                dialog.dismiss();
            }

            @Override
            public void handleCancleBtn() {
                dialog.cancel();
            }
        }, null);

        DialogHelper.resize(OrderRefundCompleteActivity.this, dialog);
        dialog.show();
    }

    private void initview() {
        ly_user = getViewById(R.id.ly_user);
        tv_user = getViewById(R.id.tv_user);
        tx_time = getViewById(R.id.tx_time);
        btn_next_step = getViewById(R.id.btn_next_step);
        tx_order = getViewById(R.id.tx_order);
        id_total = getViewById(R.id.id_total);
        et_money = getViewById(R.id.et_money);
        id_lin_surcharge = getViewById(R.id.id_lin_surcharge);
        id_lin_withholding = getViewById(R.id.id_lin_withholding);
        tx_surcharge = getViewById(R.id.tx_surcharge);
        tx_withholding = getViewById(R.id.tx_withholding);
        id_order_line = getViewById(R.id.id_order_line);
    }

    @Override
    public boolean onKeyDown(int keycode, KeyEvent event) {
        if (keycode == KeyEvent.KEYCODE_BACK) {
            for (Activity a : MainApplication.listActivities) {
                a.finish();
            }
            finish();
        }
        return super.onKeyDown(keycode, event);
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(R.string.refund_auditing);
        titleBar.setTitleChoice(false);
        titleBar.setLeftButtonVisible(true);
        titleBar.setLeftButtonIsVisible(true);
        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.print));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                for (Activity a : MainApplication.listActivities) {
                    a.finish();
                }
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {
                if (orderModel != null) {
                    print(orderModel);
                }
            }

            @Override
            public void onRightButLayClick() {
                if (orderModel != null) {
                    print(orderModel);
                }
            }
        });
    }

}
