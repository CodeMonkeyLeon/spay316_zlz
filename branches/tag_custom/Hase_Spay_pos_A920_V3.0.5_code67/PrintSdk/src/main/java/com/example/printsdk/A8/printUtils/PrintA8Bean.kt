package com.example.printsdk.A8.printUtils

class PrintA8Bean {
    var printText: String? = null
    var functionName: PRINT_FUNCTION_NAME? = null

    enum class PRINT_FUNCTION_NAME {
        TITLE, TEXT_LINE_LEFT, TEXT_LINE_RIGHT, TEXT_LINE_CENTER, TEXT_MULTI_LINES, SINGLE_LINE, DOUBLE_LINE, EMPTY_LINE, QR_CODE
    }
}