package org.xclcharts.common;

/**
 * @InterfaceName IFormatterTextCallBack
 * @Description  用于文本的回调接口
 * @author XiongChuanLiang<br/>(xcl_168@aliyun.com)
 *  * MODIFIED    YYYY-MM-DD   REASON
 */


public interface IFormatterTextCallBack {
	public String textFormatter(String value);
}
