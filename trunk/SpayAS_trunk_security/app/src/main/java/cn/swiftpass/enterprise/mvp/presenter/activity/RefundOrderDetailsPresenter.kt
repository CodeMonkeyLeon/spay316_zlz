package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.common.Constant
import cn.swiftpass.enterprise.intl.BuildConfig
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.contract.activity.RefundOrderDetailsContract
import com.example.common.entity.EmptyData
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @穿过长满冰霜的玻璃
 * @晨光已铺满大地
 * @万盏灯火结束了工作
 * @明亮的太阳正在升起。
 */
class RefundOrderDetailsPresenter : RefundOrderDetailsContract.Presenter {

    private var mView: RefundOrderDetailsContract.View? = null


    override fun regisRefund(orderNo: String?, money: Long, refundMoney: Long) {
        mView?.let { view ->

            val userName = if (MainApplication.getInstance().isAdmin(0)) {
                // 普通用户才需要查自己的数据，管理员可以查询全部
                MainApplication.getInstance().userInfo.realname // 收银员名称
            } else {
                MainApplication.getInstance().userInfo.mchName // 商户名称
            }

            val body =
                if (!TextUtils.isEmpty(MainApplication.getInstance().userInfo.tradeName)) MainApplication.getInstance().userInfo.tradeName else BuildConfig.body


            AppClient.regisRefund(
                orderNo,
                Constant.CLIENT,
                refundMoney.toString(),
                money.toString(),
                MainApplication.getInstance().getMchId(),
                MainApplication.getInstance().getUserId().toString(),
                userName,
                body,
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        commonResponse?.let {
                            view.regisRefundFailed(it.message)
                        }
                    }


                    override fun onResponse(response: CommonResponse?) {
                        view.regisRefundSuccess(true)
                    }
                }
            )
        }
    }

    override fun attachView(view: RefundOrderDetailsContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}