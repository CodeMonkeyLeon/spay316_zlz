package cn.swiftpass.enterprise.mvp.contract.fragment

import cn.swiftpass.enterprise.bussiness.model.TransactionReportBean
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/11/16
 *
 * @你应该是一场梦我应该是一阵风
 */
class TransactionReportContract {

    interface View : BaseView {

        fun queryTransactionReportSuccess(response: ArrayList<TransactionReportBean>)


        fun queryTransactionReportFailed(error: Any?)

    }


    interface Presenter : BasePresenter<View> {

        fun queryTransactionReport(reportType: String?)
    }


}