package cn.swiftpass.enterprise.mvp.contract.fragment

import cn.swiftpass.enterprise.bean.QRCodeBean
import cn.swiftpass.enterprise.bussiness.model.MerchantTempDataModel
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView


/**
 * @author lizheng.zhao
 * @date 2022/11/16
 *
 * @你应该是一场梦我应该是一阵风
 */
class TabSettingContract {


    interface View : BaseView {

        fun getVersionCodeSuccess(response: UpgradeInfo?, isDisplayIcon: Boolean)

        fun getVersionCodeFailed(error: Any?)

        fun queryMerchantDataByTelSuccess(response: MerchantTempDataModel)

        fun queryMerchantDataByTelFailed(error: Any?)

        fun getCodeListSuccess(response: ArrayList<QRCodeBean>)

        fun getCodeListFailed(error: Any?)

    }

    interface Presenter : BasePresenter<View> {

        fun getCodeList(
            page: Int,
            bindUserId: String?,
        )


        fun queryMerchantDataByTel()


        fun getVersionCode(isDisplayIcon: Boolean)
    }


}