package cn.swiftpass.enterprise.ui.activity.draw;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import org.xclcharts.common.DensityUtil;

import java.util.List;

import cn.swiftpass.enterprise.bussiness.model.PaymentChannelBean;

/**
 * Created by aijingya on 2019/5/16.
 *
 * @Package cn.swiftpass.enterprise.ui.activity.draw
 * @Description: ${TODO}(自定义的饼状图)
 * @date 2019/5/16.10:40.
 */
public class PieChartView extends View {
    private final Context context;
    private Paint mChartPaint;

    private Paint mCirclePaint;// 中心圆

    private RectF mRectF;

    private int padding;

    private List<PaymentChannelBean> mPieModelList;

    private String mPageType;

    private float	mAnimaAngle;
    private int lineCount;
    private float LineAngle = 2;

    private RectF mSelectedRectF	= new RectF();

    public PieChartView(Context context) {
        this(context, null);
    }

    public PieChartView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PieChartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();
    }

    private void init() {
        mChartPaint = new Paint();
        mChartPaint.setAntiAlias(true);
        mChartPaint.setDither(true);
        mChartPaint.setStrokeWidth(DensityUtil.dip2px(context, 100));
        mChartPaint.setStyle(Paint.Style.FILL);

        mCirclePaint = new Paint();
        mCirclePaint.setAntiAlias(true);
        mCirclePaint.setStyle(Paint.Style.FILL);
        mCirclePaint.setColor(Color.WHITE);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mPieModelList == null || mPieModelList.isEmpty()) {
            return;
        }
        for (int i = 0; i < mPieModelList.size(); i++) {
            if(mPageType.equalsIgnoreCase("1")){
                if (mPieModelList.get(i).getAmountProportion() > 0) {
                    if (mAnimaAngle >= mPieModelList.get(i).startAngleAmount &&
                            mAnimaAngle <= (mPieModelList.get(i).startAngleAmount + mPieModelList.get(i).sweepAngleAmount)) {

                        drawColor(canvas, mPieModelList.get(i).getColorVal(), mPieModelList.get(i).startAngleAmount, mAnimaAngle - mPieModelList.get(i).startAngleAmount);

                    } else if (mAnimaAngle >= (mPieModelList.get(i).startAngleAmount + mPieModelList.get(i).sweepAngleAmount)) {
                        drawColor(canvas, mPieModelList.get(i).getColorVal(), mPieModelList.get(i).startAngleAmount, mPieModelList.get(i).sweepAngleAmount);
                    }
              /*      if (mPieModelList.get(i).selected) {
                        drawSelectedView(canvas, mPieModelList.get(i).getColorVal(), mPieModelList.get(i).startAngleAmount, mPieModelList.get(i).sweepAngleAmount);
                    }*/
                }
            }else if(mPageType.equalsIgnoreCase("2")){
                if (mPieModelList.get(i).getCountProportion() > 0) {
                    if (mAnimaAngle >= mPieModelList.get(i).startAngleCount &&
                            mAnimaAngle <= (mPieModelList.get(i).startAngleCount + mPieModelList.get(i).sweepAngleCount)) {

                        drawColor(canvas, mPieModelList.get(i).getColorVal(), mPieModelList.get(i).startAngleCount, mAnimaAngle - mPieModelList.get(i).startAngleCount);

                    } else if (mAnimaAngle >= (mPieModelList.get(i).startAngleCount + mPieModelList.get(i).sweepAngleCount)) {
                        drawColor(canvas, mPieModelList.get(i).getColorVal(), mPieModelList.get(i).startAngleCount, mPieModelList.get(i).sweepAngleCount);
                    }
                   /* if (mPieModelList.get(i).selected) {
                        drawSelectedView(canvas, mPieModelList.get(i).getColorVal(), mPieModelList.get(i).startAngleCount, mPieModelList.get(i).sweepAngleCount);
                    }*/
                }
            }

        }
        canvas.drawCircle(getMeasuredWidth() / 2, getMeasuredWidth() / 2, padding + DensityUtil.dip2px(context, 12), mCirclePaint);
    }

    private void drawColor(Canvas canvas, int color, float startAngle, float sweepAngle) {
        mChartPaint.setColor(color);
        mChartPaint.setAlpha(255);
        canvas.drawArc(mRectF, startAngle, sweepAngle, true, mChartPaint);
    }

    private void drawSelectedView(Canvas canvas, int color, float startAngle, float sweepAngle) {
        mChartPaint.setColor(color);
        mChartPaint.setAlpha(150);
        canvas.drawArc(mSelectedRectF, startAngle, sweepAngle, true, mChartPaint);
    }

    public void startAnima() {
        final ValueAnimator mValueAnimator = ValueAnimator.ofFloat(0f, 360f);
        mValueAnimator.setDuration(1 * 1000);

        mValueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override public void onAnimationUpdate(ValueAnimator animation) {
                mAnimaAngle = (float) animation.getAnimatedValue();
                invalidate();
            }
        });
        mValueAnimator.start();
    }

    public void setData(List<PaymentChannelBean> pieModelList,String pageType) {
        this.mPieModelList = pieModelList;
        this.mPageType = pageType;
        if(mPieModelList.size() == 1){
            lineCount = 0;
        }else{
            lineCount = mPieModelList.size();
        }

        for (int i = 0; i < mPieModelList.size(); i++) {
            PaymentChannelBean model = mPieModelList.get(i);
            if(mPageType.equalsIgnoreCase("1")){
                if (i == 0) {
                    model.startAngleAmount = -90;
                } else {
                    model.startAngleAmount = mPieModelList.get(i - 1).startAngleAmount + mPieModelList.get(i - 1).sweepAngleAmount+LineAngle;
                }
                model.sweepAngleAmount = (float) (model.getAmountProportion() * (360-lineCount*LineAngle));
            }else if(mPageType.equalsIgnoreCase("2")){
                if (i == 0) {
                    model.startAngleCount = -90;
                } else {
                    model.startAngleCount = mPieModelList.get(i - 1).startAngleCount + mPieModelList.get(i - 1).sweepAngleCount+LineAngle;
                }
                model.sweepAngleCount = (float) (model.getCountProportion() * (360-lineCount*LineAngle));
            }
        }
    }


    @Override protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        padding = w / 6;
        mRectF = new RectF(padding, padding, w - padding, w - padding);
        mSelectedRectF.set(mRectF);
        mSelectedRectF.inset(-30, -30);
    }

}
