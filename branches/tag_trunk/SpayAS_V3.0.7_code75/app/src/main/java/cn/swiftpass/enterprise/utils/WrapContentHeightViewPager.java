package cn.swiftpass.enterprise.utils;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by aijingya on 2019/6/4.
 *
 * @Package cn.swiftpass.enterprise.utils
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2019/6/4.16:42.
 */
public class WrapContentHeightViewPager extends ViewPager {
    private int height = 0;
    public WrapContentHeightViewPager(Context context) {
        super(context);
    }

    public WrapContentHeightViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);
            //1.测量子View的宽高
            child.measure(widthMeasureSpec,MeasureSpec.makeMeasureSpec(0,MeasureSpec.UNSPECIFIED));
            //2.获取view的高度
            int measuredHeight = child.getMeasuredHeight();
            //3.取所有的子View中的高度最高的那个
            if (measuredHeight>height){
                height =measuredHeight;
            }

        }
        //4、最后设置高度的测量模式为EXACTLY
        heightMeasureSpec= MeasureSpec.makeMeasureSpec(height,MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
