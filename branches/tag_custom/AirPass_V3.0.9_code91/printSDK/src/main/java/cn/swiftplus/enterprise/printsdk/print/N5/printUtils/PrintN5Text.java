package cn.swiftplus.enterprise.printsdk.print.N5.printUtils;

import android.content.Context;
import android.graphics.Bitmap;

import com.nexgo.oaf.apiv3.device.printer.AlignEnum;
import com.nexgo.oaf.apiv3.device.printer.GrayLevelEnum;
import com.nexgo.oaf.apiv3.device.printer.OnPrintListener;
import com.nexgo.oaf.apiv3.device.printer.Printer;
import cn.swiftplus.enterprise.printsdk.print.N5.POSN5Client;

/**
 * Created by aijingya on 2020/10/27.
 *
 * @Package cn.swiftplus.enterprise.printsdk.print.N5.printUtils
 * @Description:
 * @date 2020/10/27.11:11.
 */
public class PrintN5Text {

    private static PrintN5Text mPrintN5Text;
    private Printer printer;
    private Context mContext;

    public final int FONT_SIZE_SMALL = 15;
    public final static int FONT_SIZE_NORMAL = 24;
    public final static int FONT_SIZE_BIG = 30;

    private PrintN5Text(){
        printer = POSN5Client.getDeviceEngine().getPrinter();
    }

    public static PrintN5Text getInstance(){
        if(mPrintN5Text == null){
            mPrintN5Text = new PrintN5Text();
        }
        return mPrintN5Text;
    }

    public void init(Context context){
        this.mContext = context;
        printer.initPrinter();
    }


    public void printTitle(String text){
        printer.setGray(GrayLevelEnum.LEVEL_2);
        printer.appendPrnStr(text, FONT_SIZE_BIG, AlignEnum.CENTER, false);
        printer.appendPrnStr(" ", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
    }

    public void printTextLine(String text,int TextSize ,AlignEnum align){
        printer.appendPrnStr(text, TextSize, align, false);
    }

    public void printMultiLines(String text,int TextSize ,AlignEnum align){
        printer.appendPrnStr(text, TextSize, align, false);
    }

    public void printEmptyLine(){
        printer.appendPrnStr(" ", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
    }

    public void printDoubleLine(){
        printer.appendPrnStr("===========================", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
    }

    public void printSingleLine(){
        printer.appendPrnStr("------------------------------------------", FONT_SIZE_BIG, AlignEnum.CENTER, false);
    }

    public void printQRCode(String orderNoMch){
        //打印小票的代码
        final Bitmap bitmap = CreateOneDiCodeUtilPrint.createCode(orderNoMch, Integer.valueOf(260), Integer.valueOf(260));
        if (bitmap != null) {
            printer.appendImage(bitmap, AlignEnum.CENTER);
            printer.appendPrnStr(" ", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
        }
    }

    /**
     *  打印图片
     */
    public void printBitmap(Bitmap logoBitmap){
        try {
            if (logoBitmap != null) {
                printer.appendImage(logoBitmap, AlignEnum.CENTER);
                printer.appendPrnStr(" ", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
            }
        }
        catch (Exception e) {
        }
    }

    public void startPrint(){
        printer.startPrint(true, new OnPrintListener() {
            @Override
            public void onPrintResult(final int retCode) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    }
                });
            }

            private void runOnUiThread(Runnable runnable) {
            }
        });
    }

}
