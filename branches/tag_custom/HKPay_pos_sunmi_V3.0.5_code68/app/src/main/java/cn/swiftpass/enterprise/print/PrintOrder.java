package cn.swiftpass.enterprise.print;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * Created by aijingya on 2020/8/18.
 *
 * @Package cn.swiftpass.enterprise.print
 * @Description:
 * @date 2020/8/18.16:18.
 */
public class PrintOrder {

    public static void printOrderDetail(boolean isCashierShow, final Order orderModel){
        SunmiPrintHelper.getInstance().setAlign(1);

        Bitmap logoBitmap = null;
        if (!TextUtils.isEmpty(orderModel.getApiCode())){
            if (orderModel.getApiCode().equals("1")){//微信
                logoBitmap = BitmapFactory.decodeResource(MainApplication.getContext().getResources(), R.drawable.wechat_pint, null);
            }else if (orderModel.getApiCode().equals("2")){//支付宝
                logoBitmap = BitmapFactory.decodeResource(MainApplication.getContext().getResources(), R.drawable.alipay_pint, null);
            }
        }
        if (logoBitmap != null){
            SunmiPrintHelper.getInstance().printBitmap(logoBitmap);
            SunmiPrintHelper.getInstance().printText("\n", 28, false, false);
            SunmiPrintHelper.getInstance().printText("\n", 28, false, false);
        }else {
            SunmiPrintHelper.getInstance().printText("\n", 28, false, false);
            SunmiPrintHelper.getInstance().printText("\n", 28, false, false);
        }

        if(!orderModel.isPay()){
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_blue_print_refund_note)+ "\n", 36, true, false);
        }
        else{
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_blue_print_pay_note)+ "\n", 36, true, false);
        }

        SunmiPrintHelper.getInstance().printText("\n", 28, false, false);

        SunmiPrintHelper.getInstance().setAlign(0);
        SunmiPrintHelper.getInstance().printText(orderModel.getPartner() + "\n", 24, false, false);
        SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tv_pay_client_save)+ "\n", 24, false, false);

        SunmiPrintHelper.getInstance().printText("===========================\n", 28, false, false);

        if (!StringUtil.isEmptyOrNull(MainApplication.getMchName())) {
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.shop_name)+ "：" + "\n"+ MainApplication.getMchName() + "\n", 24, false, false);
        }
        if (!StringUtil.isEmptyOrNull(MainApplication.getMchId())) {
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_blue_print_merchant_no)+ "：" + MainApplication.getMchId() + "\n", 24, false, false);
        }
        if(orderModel.isPay()){
            if (MainApplication.isAdmin.equals("0") && !isCashierShow) { //收银员
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_user) + "：" + MainApplication.realName + "\n", 24, false, false);
            }
            if (isCashierShow){
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_user) + "："+ orderModel.getUserName() + "\n", 24, false, false);

            }
        }
        if(!orderModel.isPay()){//退款
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_blue_print_refund_time) + "：" + "\n"+ orderModel.getAddTimeNew() + "\n", 24, false, false);
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.refund_odd_numbers) + "：\n", 24, false, false);
            SunmiPrintHelper.getInstance().printText(orderModel.getRefundNo() + "\n", 24, false, false);
        }else{
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_bill_stream_time) + "："+ "\n" + orderModel.getAddTimeNew() + "\n", 24, false, false);
        }
        SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_order_no) + ": " + "\n", 24, false, false);
        SunmiPrintHelper.getInstance().printText(orderModel.getOrderNoMch() + "\n", 24, false, false);

        if (orderModel.isPay()){
            if (MainApplication.getPayTypeMap() != null && MainApplication.getPayTypeMap().size() > 0) {
                SunmiPrintHelper.getInstance().printText(MainApplication.getPayTypeMap().get(orderModel.getApiCode())+ " " + ToastHelper.toStr(R.string.tx_orderno) + ":\n", 24, false, false);
            }
            SunmiPrintHelper.getInstance().printText(orderModel.getTransactionId() + "\n", 24, false, false);

            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：" + "\n"+ orderModel.getTradeName() + "\n", 24, false, false);
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_bill_stream_statr) + "：" + "\n"+ MainApplication.getTradeTypeMap().get(orderModel.getTradeState() + "") + "\n", 24, false, false);

            if (!StringUtil.isEmptyOrNull(orderModel.getAttach())) {
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_bill_stream_attach) + "：" + " " + orderModel.getAttach() + "\n", 24, false, false);
            }

            if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
                if (orderModel.getDaMoney() > 0) {
                    long actualMoney = orderModel.getDaMoney() + orderModel.getOrderFee();

                    SunmiPrintHelper.getInstance().setAlign(0);
                    SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_blue_print_money) + "："+ "\n", 24, false, false);

                    SunmiPrintHelper.getInstance().setAlign(2);
                    SunmiPrintHelper.getInstance().printText( DateUtil.formatRMBMoneyUtils(actualMoney) + ToastHelper.toStr(R.string.pay_yuan)+ "\n", 24, false, false);
                } else {

                    SunmiPrintHelper.getInstance().setAlign(0);
                    SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_blue_print_money) + "：" + "\n", 24, false, false);

                    SunmiPrintHelper.getInstance().setAlign(2);
                    SunmiPrintHelper.getInstance().printText(DateUtil.formatRMBMoneyUtils(orderModel.getOrderFee()) + ToastHelper.toStr(R.string.pay_yuan)+ "\n", 24, false, false);
                }
            }else{
                if (MainApplication.isSurchargeOpen()) {
                    if (!orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") && !orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")) {
                        SunmiPrintHelper.getInstance().setAlign(0);
                        SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_blue_print_money) + "：" + "\n", 24, false, false);

                        SunmiPrintHelper.getInstance().setAlign(2);
                        SunmiPrintHelper.getInstance().printText(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getOrderFee()) + "\n", 24, false, false);

                        SunmiPrintHelper.getInstance().setAlign(0);
                        SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_surcharge) + "：" + "\n", 24, false, false);

                        SunmiPrintHelper.getInstance().setAlign(2);
                        SunmiPrintHelper.getInstance().printText(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getSurcharge()) + "\n", 24, false, false);
                    }
                }

                    //Uplan ---V3.0.5迭代新增

                    //如果为预授权的支付类型，则不展示任何优惠相关的信息
                    /*因目前SPAY與網關支持的預授權接口僅為‘支付寶’預授權，
                    遂與銀聯Uplan不衝突，遂預授權頁面內，無需處理該優惠信息。*/
                    if(!TextUtils.isEmpty(orderModel.getTradeType()) && !orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") &&  !orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")){
                        // 优惠详情 --- 如果列表有数据，则展示，有多少条，展示多少条
                        if(orderModel.getUplanDetailsBeans() != null && orderModel.getUplanDetailsBeans().size() > 0){
                            for(int i =0 ; i<orderModel.getUplanDetailsBeans().size() ; i++){
                                String string_Uplan_details;
                                //如果是Uplan Discount或者Instant Discount，则采用本地词条，带不同语言的翻译
                                if(orderModel.getUplanDetailsBeans().get(i).getDiscountNote().equalsIgnoreCase("Uplan discount")){
                                    string_Uplan_details = ToastHelper.toStr(R.string.uplan_Uplan_discount) + "：";
                                }else if(orderModel.getUplanDetailsBeans().get(i).getDiscountNote().equalsIgnoreCase("Instant Discount")){
                                    string_Uplan_details = ToastHelper.toStr(R.string.uplan_Uplan_instant_discount) + "：";
                                } else{//否则，后台传什么展示什么
                                    string_Uplan_details = orderModel.getUplanDetailsBeans().get(i).getDiscountNote()+ "：";
                                }
                                SunmiPrintHelper.getInstance().setAlign(0);
                                SunmiPrintHelper.getInstance().printText(string_Uplan_details+ "\n", 24, false, false);

                                SunmiPrintHelper.getInstance().setAlign(2);
                                SunmiPrintHelper.getInstance().printText(MainApplication.feeType+ " "+ "-" + orderModel.getUplanDetailsBeans().get(i).getDiscountAmt()+ "\n", 24, false, false);

                            }
                        }
                        // 消费者实付金额 --- 如果不为0 ，就展示
                        if(orderModel.getCostFee() != 0){
                            SunmiPrintHelper.getInstance().setAlign(0);
                            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.uplan_Uplan_actual_paid_amount) + "："+ "\n", 24, false, false);

                            SunmiPrintHelper.getInstance().setAlign(2);
                            SunmiPrintHelper.getInstance().printText(MainApplication.feeType + " "+ DateUtil.formatMoneyUtils(orderModel.getCostFee())+ "\n", 24, false, false);

                        }
                    }

                SunmiPrintHelper.getInstance().setAlign(0);
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tv_charge_total) + "："+ "\n", 36, false, false);

                SunmiPrintHelper.getInstance().setAlign(2);
                SunmiPrintHelper.getInstance().printText(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee())+ "\n", 36, false, false);

                SunmiPrintHelper.getInstance().printText("\n", 28, false, false);
                SunmiPrintHelper.getInstance().setAlign(0);
            }

        }else{
            SunmiPrintHelper.getInstance().setAlign(0);
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "："+ "\n" + orderModel.getTradeName() + "\n", 24, false, false);

            if (!StringUtil.isEmptyOrNull(orderModel.getUserName())) {
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tv_refund_peop) + "："+ "\n" + orderModel.getUserName() + "\n", 24, false, false);
            }
            if (MainApplication.getRefundStateMap() != null && MainApplication.getRefundStateMap().size() > 0) {
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tv_refund_state) + "："+ "\n" + MainApplication.getRefundStateMap().get(orderModel.getRefundState() + "") + "\n", 24, false, false);
            }
            if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {

                SunmiPrintHelper.getInstance().setAlign(0);
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_blue_print_money) + "："+ "\n", 24, false, false);

                SunmiPrintHelper.getInstance().setAlign(2);
                SunmiPrintHelper.getInstance().printText( DateUtil.formatRMBMoneyUtils(orderModel.getTotalFee()) + ToastHelper.toStr(R.string.pay_yuan)+ "\n", 24, false, false);

                SunmiPrintHelper.getInstance().setAlign(0);
                SunmiPrintHelper.getInstance().printText( ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "："+ "\n", 24, false, false);

                SunmiPrintHelper.getInstance().setAlign(2);
                SunmiPrintHelper.getInstance().printText( DateUtil.formatRMBMoneyUtils(orderModel.getRefundMoney()) + ToastHelper.toStr(R.string.pay_yuan)+ "\n", 24, false, false);

            }else{

                SunmiPrintHelper.getInstance().setAlign(0);
                SunmiPrintHelper.getInstance().printText( ToastHelper.toStr(R.string.tv_charge_total) + "："+ "\n", 36, false, false);

                SunmiPrintHelper.getInstance().setAlign(2);
                SunmiPrintHelper.getInstance().printText(  MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee())+ "\n", 36, false, false);

                SunmiPrintHelper.getInstance().setAlign(0);
                SunmiPrintHelper.getInstance().printText( ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "："+ "\n", 36, false, false);

                SunmiPrintHelper.getInstance().setAlign(2);
                SunmiPrintHelper.getInstance().printText( MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getRefundMoney())+ "\n", 36, false, false);

            }
            if (!StringUtil.isEmptyOrNull(orderModel.getPrintInfo())) {
                SunmiPrintHelper.getInstance().setAlign(0);
                SunmiPrintHelper.getInstance().printText( orderModel.getPrintInfo() + "\n", 24, false, false);
            }
        }

        if(!StringUtil.isEmptyOrNull(orderModel.getOrderNoMch()) && orderModel.isPay()){
            SunmiPrintHelper.getInstance().setAlign(1);
            SunmiPrintHelper.getInstance().printText("\n", 28, false, false);

            SunmiPrintHelper.getInstance().printQr(orderModel.getOrderNoMch(), 7, 2);
            SunmiPrintHelper.getInstance().printText( "\n"+ ToastHelper.toStr(R.string.refound_QR_code)+ "\n", 24, false, false);
        }
        SunmiPrintHelper.getInstance().printText("---------------------------\n", 28, false, false);
        SunmiPrintHelper.getInstance().setAlign(0);
        SunmiPrintHelper.getInstance().printText( ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n", 24, false, false);
        SunmiPrintHelper.getInstance().print3Line();
    }
}
