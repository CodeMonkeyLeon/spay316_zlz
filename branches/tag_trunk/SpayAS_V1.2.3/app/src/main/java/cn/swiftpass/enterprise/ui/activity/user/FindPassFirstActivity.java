/*
 * 文 件 名:  FindPassWordActivity.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-14
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.user;

import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 忘记密码
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-14]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class FindPassFirstActivity extends TemplateActivity
{
    
    private EditText et_id;
    
    private ImageView iv_clean_input;
    
    private Button btn_next_step;
    
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_pass_first);
        
        initView();
        btn_next_step.getBackground().setAlpha(102);
        //        btn_next_step.sett
        setLister();
        
    }
    
    private void initView()
    {
        et_id = getViewById(R.id.et_id);
        iv_clean_input = getViewById(R.id.iv_clean_input);
        btn_next_step = getViewById(R.id.btn_next_step);
        
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        {
            
            @Override
            public void onExecute(CharSequence s, int start, int before, int count)
            {
            }
            
            @Override
            public void onAfterTextChanged(Editable s)
            {
                if (et_id.isFocused())
                {
                    if (et_id.getText().toString().length() > 0)
                    {
                        iv_clean_input.setVisibility(View.VISIBLE);
                        setButtonBg(btn_next_step, true, R.string.reg_next_step);
                    }
                    else
                    {
                        iv_clean_input.setVisibility(View.GONE);
                        setButtonBg(btn_next_step, false, R.string.reg_next_step);
                    }
                }
            }
        });
        et_id.addTextChangedListener(editTextWatcher);
        
        showSoftInputFromWindow(FindPassFirstActivity.this, et_id);
    }
    
    private void setLister()
    {
        btn_next_step.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (StringUtil.isEmptyOrNull(et_id.getText().toString()))
                {
                    toastDialog(FindPassFirstActivity.this, R.string.show_user_name, null);
                    et_id.setFocusable(true);
                    return;
                }
                
                UserManager.checkForgetPwdInput(et_id.getText().toString(), new UINotifyListener<UserModel>()
                {
                    @Override
                    public void onError(final Object object)
                    {
                        // TODO Auto-generated method stub
                        super.onError(object);
                        dismissLoading();
                        FindPassFirstActivity.this.runOnUiThread(new Runnable()
                        {
                            
                            @Override
                            public void run()
                            {
                                setButtonBg(btn_next_step, true, R.string.reg_next_step);
                                if (null != object)
                                {
                                    toastDialog(FindPassFirstActivity.this, object.toString(), null);
                                }
                            }
                        });
                    }
                    
                    public void onPreExecute()
                    {
                        super.onPostExecute();
                        
                        loadDialog(FindPassFirstActivity.this, R.string.public_data_loading);
                        
                        FindPassFirstActivity.this.runOnUiThread(new Runnable()
                        {
                            
                            @Override
                            public void run()
                            {
                                setButtonBg(btn_next_step, false, R.string.reg_next_step_loading);
                            }
                        });
                    };
                    
                    @Override
                    public void onSucceed(UserModel result)
                    {
                        super.onSucceed(result);
                        dismissLoading();
                        setButtonBg(btn_next_step, true, R.string.reg_next_step);
                        if (result != null)
                        {
                            result.setTel(et_id.getText().toString());
                            MainApplication.listActivities.add(FindPassFirstActivity.this);
                            
                            FindPassSecondActivity.startActivity(FindPassFirstActivity.this, result);
                        }
                    }
                });
            }
        });
        
        iv_clean_input.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                et_id.setText("");
                iv_clean_input.setVisibility(View.GONE);
            }
        });
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tv_find_pass_title);
    }
    
}
