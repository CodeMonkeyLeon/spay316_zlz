package cn.swiftpass.enterprise.ui.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.PreferenceUtil;

/**
 * 内容显示
 * Date: 13-10-29
 * Time: 下午6:22
 */
@SuppressLint({"SetJavaScriptEnabled", "NewApi"})
public class ContentTextActivity extends TemplateActivity {

    private static final String TAG = ContentTextActivity.class.getSimpleName();
    private WebView wb;

    private boolean isDisplayShared;

    //private Context mContext;

    // private SharedDialog sharedDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contenttext);
        initViews();
    }

    @SuppressLint("JavascriptInterface")
    private void initViews() {
        //mContext = this;
        wb = (WebView) findViewById(R.id.webview);
        //        wb.addJavascriptInterface(new InJavaScriptLocalObj(), "local_obj");
        wb.getSettings().setJavaScriptEnabled(true);
        wb.getSettings().setAllowFileAccessFromFileURLs(false);
        wb.getSettings().setAllowUniversalAccessFromFileURLs(false);

        WebSettings settings = wb.getSettings();
        if (AppHelper.getAndroidSDKVersion() == 17) {
            settings.setDisplayZoomControls(false);
        }
        settings.setLoadWithOverviewMode(true);
        //settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        //各种分辨率适应
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int mDensity = metrics.densityDpi;
        if (mDensity == 120) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        } else if (mDensity == 160) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        } else if (mDensity == 240) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        }
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);

        wb.setWebChromeClient(new myWebChromeClien());
        wb.setWebViewClient(new MyWebViewClient());
        isDisplayShared = getIntent().getBooleanExtra("isShared", false);
       /* try {
            int title = getIntent().getIntExtra("title", R.string.title_context_text);

            titleBar.setTitle(title);
        } catch (Exception e) {
            String title = getIntent().getStringExtra("title");

            titleBar.setTitle(title);
        }*/
        String url = getIntent().getStringExtra("url");

        String login_skey = getIntent().getStringExtra("login_skey");
        String login_sauthid = getIntent().getStringExtra("login_sauthid");

        wb.setWebViewClient(new MyWebViewClient());

        String language = PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN);
        Map<String, String> heards = new HashMap<String, String>();
        Locale locale = getResources().getConfiguration().locale; //zh-rHK
        String lan = locale.getCountry();
        String langu = "zh_cn";
        if (!TextUtils.isEmpty(language)) {
            heards.put("fp-lang", PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN));
            langu = PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN);
        } else {
            if (lan.equalsIgnoreCase("CN")) {
                heards.put("fp-lang", MainApplication.LANG_CODE_ZH_CN);
                langu = MainApplication.LANG_CODE_ZH_CN;
            } else {
                heards.put("fp-lang", MainApplication.LANG_CODE_ZH_TW);
                langu = MainApplication.LANG_CODE_ZH_TW;
            }

        }
//        heards.put("User-Agent", String.format("%s/%s (Linux; Android %s; %s Build/%s)", ApiConstant.APK_NAME, "3.5", Build.VERSION.RELEASE, Build.MANUFACTURER, Build.ID));

        heards.put("SKEY", login_skey);
        heards.put("SAUTHID", login_sauthid);

        String key = "";
        try {
            key = "sp-" + "Android" + "-" + AppHelper.getImei(MainApplication.getContext());
        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }

        String value =
                AppHelper.getVerCode(MainApplication.getContext()) + "," + ApiConstant.bankCode + "," + langu + ","
                        + AppHelper.getAndroidSDKVersionName() + "," + DateUtil.formatTime(System.currentTimeMillis()) + ","
                        + "Android";

        heards.put(key, value);

        wb.loadUrl(url, heards);

        // bottomBar.setVisibility(View.GONE);
    }

    final class InJavaScriptLocalObj {
        public void showSource(String html) {

        }
    }

    final class MyWebViewClient extends WebViewClient {
        public MyWebViewClient() {
            super();
        }

        /**
         * {@inheritDoc}
         */

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            try {
                showNewLoading(true, getString(R.string.public_loading));
            } catch (Exception e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            showNewLoading(true, getString(R.string.public_loading));
            //调用拨号程序
            if (url.startsWith("mailto:") || url.startsWith("geo:") || url.startsWith("tel:")) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                return true;
            }
            return super.shouldOverrideUrlLoading(view, url);

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            titleBar.setRightLodingVisible(false, true);
            //            dialog.dismiss();
            //            if (isDisplayShared)
            //                titleBar.setRightButtonVisible(true, "分享");

            dismissLoading();
            super.onPageFinished(view, url);
            titleBar.setTitle(view.getTitle());
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            titleBar.setRightLodingVisible(false, false);
            titleBar.setTitle("");
            //            if (dialog != null)
            //                dialog.dismiss();

            dismissLoading();
            //            wb.loadUrl("file:///android_asset/error.html");

        }
    }

    class myWebChromeClien extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);

        }
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setRightLodingVisible(false);
        titleBar.setLeftButtonVisible(true);
        titleBar.setRightButtonVisible(false);
        if (isDisplayShared)
            titleBar.setRightButtonVisible(true, "分享");
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                if(wb.canGoBack()){
                    wb.goBack();
                }else {
                    finish();
                }
            }

            @Override
            public void onRightButtonClick() {
                //                if (isDisplayShared)
                //                {
                //                    // showSharedDialog();
                //                }
            }

            @Override
            public void onRightLayClick() {
                // TODO Auto-generated method stub

            }

            @Override
            public void onRightButLayClick() {
                // TODO Auto-generated method stub

            }
        });
    }

    /*
        private void showSharedDialog() {
            if (sharedDialog != null && sharedDialog.isShowing()) {
                sharedDialog.dismiss();
            } else {
                sharedDialog = new SharedDialog(mContext, R.style.MyDialogStyleTop);
                sharedDialog.show();
            }
        }*/
    @Override
    protected boolean isLoginRequired() {
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    public static void startActivity(Context context, String url, int title) {
        Intent it = new Intent();
        it.putExtra("url", url);
        it.putExtra("title", title);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        it.setClass(context, ContentTextActivity.class);
        context.startActivity(it);
    }

    public static void startActivity(Context context, String url, String title) {
        Intent it = new Intent();
        it.putExtra("url", url);
        it.putExtra("title", title);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        it.setClass(context, ContentTextActivity.class);
        context.startActivity(it);
    }

    public static void startActivity(Context context, String url, int title, boolean isShared) {
        Intent it = new Intent();
        it.putExtra("url", url);
        it.putExtra("title", title);
        it.putExtra("isShared", isShared);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        it.setClass(context, ContentTextActivity.class);
        context.startActivity(it);
    }

    public static void startActivityWithCookie(Context context, String url, int title, String login_skey,String login_sauthid){
        Intent it = new Intent();
        it.putExtra("url", url);
        it.putExtra("title", title);
        it.putExtra("login_skey",login_skey);
        it.putExtra("login_sauthid",login_sauthid);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        it.setClass(context, ContentTextActivity.class);
        context.startActivity(it);

    }
}
