package cn.swiftpass.enterprise.ui.widget;

import android.content.Context;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import cn.swiftpass.enterprise.intl.R;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-11-27
 * Time: 下午9:17
 * To change this template use File | Settings | File Templates.
 */
public class MyToast
{
    
    private static MyToast mToast;
    
    private static Handler mHandler = new Handler();
    
    LayoutInflater mInflater;
    
    private static Runnable r = new Runnable()
    {
        public void run()
        {
            //mToast.cancel();
        }
    };
    
    public void showToast(Context context, String msg)
    {
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View toastRoot = mInflater.inflate(R.layout.view_toast, null);
        TextView message = (TextView)toastRoot.findViewById(R.id.tv_content);
        message.setText(msg);
        Toast toastStart = new Toast(context);
        toastStart.setGravity(Gravity.CENTER, 0, 0);
        
        toastStart.setDuration(Toast.LENGTH_SHORT);
        toastStart.setView(toastRoot);
        toastStart.show();
    }
    
}
