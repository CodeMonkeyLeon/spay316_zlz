package cn.swiftpass.enterprise.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.ui.widget.ItemOrder;

/**
 * 订单.
 * User: Alan
 * Date: 13-9-28
 * Time: 上午10:02
 */
public class OrderAdapter extends ArrayListAdapter<Order>
{
    
    public OrderAdapter(Context context)
    {
        super(context);
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ItemOrder itemOrder = null;
        if (convertView != null && convertView instanceof ItemOrder)
        {
            itemOrder = (ItemOrder)convertView;
        }
        Order orderModel = (Order)getItem(position);
        if (convertView == null)
        {
            itemOrder = (ItemOrder)LayoutInflater.from(mContext).inflate(R.layout.listitem_order, null);
        }
        
        itemOrder.setData(orderModel);
        return itemOrder;
    }
    
}
