package cn.swiftpass.enterprise.ui.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.example.common.sp.PreferenceUtil;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.WalletListBean;
import cn.swiftpass.enterprise.common.Constant;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.LocaleUtils;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

public class SelectPicPopupWindow extends PopupWindow {

    //private TextView tv_ok;
    private TextView tv_cancel, tv_wx_pay, tv_zfb_pay, tv_qq_pay, tv_jd_pay;

    private View mMenuView;

    //private String type;

    private TextView tv_scan;

    private ViewPayTypeHolder payholder;

    private ListView listView;

    private TextView title;

    private String choice;

    private TextView tv_cel;

    private ImageView iv_bank;

    private Button tv_wx, tv_zfb, tv_succ, tv_fail, tv_revers, tv_refund, tv_close, tv_tyep_refund;
    private TextView tv_reset;

    private TextView ly_ok;

    private List<Button> listBut = new ArrayList<Button>();

    //    private Map<String, String> payTypeMap = new HashMap<String, String>();

    private SelectPicPopupWindow.HandleBtn handleBtn;

    private Activity activity;

    private boolean isTag = false;

    //private SelectPicPopupWindow.HandleTv handleTv;

    private View v_wx, v_zfb, v_jd, v_qq;

    private GridViewForScrollView gv_pay_type;

    private PayTypeAdape payTypeAdape;

    private LinearLayout ly_pay_type, ly_second_pay_type;

    private List<DynModel> list;

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    //    private Map<String, String> typeMap = new HashMap<String, String>();
    //
    public interface HandleBtn {
        void handleOkBtn(List<String> choiceType, List<String> payTypeList, boolean ishandled);
    }

    public SelectPicPopupWindow(Activity context, List<String> payList, final List<String> payTypeList, SelectPicPopupWindow.HandleBtn handleBtn) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.pay_type_choice_dialog_new, null);
        this.handleBtn = handleBtn;
        this.activity = context;
        this.payTypeList = payTypeList;
        initview(mMenuView);
        setDate(payList);
        setLisert();
        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        for (Button b : listBut) {
                            resetBut(b);
                        }
                        if (choiceType.size() > 0) {
                            isTag = true;
                        }
                        tv_tyep_refund.setEnabled(true);
                        choiceType.clear();

                        if (list != null && list.size() > 0) {
                            for (DynModel d : list) {
                                d.setEnaled(0);
                            }
                            payTypeAdape.notifyDataSetChanged();
                        }

                        payTypeList.clear();
                        dismiss();
                    }
                }
                return true;
            }
        });
    }

    private List<String> choiceType = new ArrayList<String>();

    List<String> payTypeList = new ArrayList<>();

    private boolean ishadhandle;

    public SelectPicPopupWindow(boolean isHandled, Activity context, List<String> payList,
                                final List<String> payTypeList,
                                SelectPicPopupWindow.HandleBtn handleBtn) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.pay_type_choice_dialog_new, null);
        this.handleBtn = handleBtn;
        this.activity = context;
        this.payTypeList = payTypeList;
        ishadhandle = isHandled;
        initview(mMenuView);
        setDate(payList);
        setLisert();
        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        for (Button b : listBut) {
                            resetBut(b);
                        }
                        if (choiceType.size() > 0) {
                            isTag = true;
                        }
                        tv_tyep_refund.setEnabled(true);
                        choiceType.clear();

                        if (list != null && list.size() > 0) {
                            for (DynModel d : list) {
                                d.setEnaled(0);
                            }
                            payTypeAdape.notifyDataSetChanged();
                        }

//                        payTypeList.clear();
                        dismiss();
                    }
                }
                return true;
            }
        });
    }

    private void setDate(List<String> payList) {
        if (null != payList && payList.size() > 0) {
            for (String s : payList) {
                if (s.equals(tv_wx.getTag().toString())) {
                    updatepayTypeButBg(tv_wx, "1");
                } else if (s.equals(tv_zfb.getTag().toString())) {
                    updatepayTypeButBg(tv_zfb, "1");
                } else if (s.equals(tv_succ.getTag().toString())) {
                    updatepayTypeButBg(tv_succ, "1");
                    setRefundDisabled();
                } else if (s.equals(tv_fail.getTag().toString())) {
                    updatepayTypeButBg(tv_fail, "1");
                    setRefundDisabled();
                } else if (s.equals(tv_revers.getTag().toString())) {
                    updatepayTypeButBg(tv_revers, "1");
                    setRefundDisabled();
                } else if (s.equals(tv_refund.getTag().toString())) {
                    updatepayTypeButBg(tv_refund, "1");
                    setRefundDisabled();
                } else if (s.equals(tv_close.getTag().toString())) {
                    updatepayTypeButBg(tv_close, "1");
                    setRefundDisabled();
                } else if (s.equals(tv_tyep_refund.getTag().toString())) {
                    updateRefundTypeButtonBg(tv_tyep_refund, "4");
                }
            }
        }
    }

    void updatepayTypeButBg(Button b, String type) {
        isTag = false;

        if (choiceType.contains(b.getTag().toString())) {
            if (!choiceType.contains(tv_tyep_refund.getTag().toString())) {
                setRefundEnabled();
            }
            //未选中的状态
            b.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            b.setTextColor(Color.parseColor("#535353"));
            choiceType.remove(b.getTag().toString());
        } else {
            //选中的状态
            b.setBackgroundResource(R.drawable.button_shape);
            b.setTextColor(Color.WHITE);
            choiceType.add(b.getTag().toString());
        }
    }

    boolean isContainsPayTypeAndState() {
        if (choiceType.contains(tv_wx.getTag().toString())
                || choiceType.contains(tv_zfb.getTag().toString())
                || choiceType.contains(tv_fail.getTag().toString())
                || choiceType.contains(tv_revers.getTag().toString())
                || choiceType.contains(tv_refund.getTag().toString())
                || choiceType.contains(tv_close.getTag().toString())) {
            return true;
        }

        return false;
    }

    boolean isContainsPayState() {
        if (choiceType.contains(tv_fail.getTag().toString())
                || choiceType.contains(tv_revers.getTag().toString())
                || choiceType.contains(tv_refund.getTag().toString())
                || choiceType.contains(tv_close.getTag().toString())) {
            return true;
        }
        return false;
    }


    void updatePayTypeSetEnabled(boolean enable) {
        tv_wx.setEnabled(enable);
        tv_zfb.setEnabled(enable);
        tv_succ.setEnabled(enable);
        tv_fail.setEnabled(enable);
        tv_revers.setEnabled(enable);
        tv_refund.setEnabled(enable);
        tv_close.setEnabled(enable);
        tv_tyep_refund.setEnabled(enable);
    }

    void setPayTypeSetEnabled(boolean enable) {

        tv_succ.setEnabled(enable);
        tv_fail.setEnabled(enable);
        tv_revers.setEnabled(enable);
        tv_refund.setEnabled(enable);
        tv_close.setEnabled(enable);
        if (enable) {

            tv_succ.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            tv_succ.setTextColor(Color.parseColor("#535353"));

            tv_fail.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            tv_fail.setTextColor(Color.parseColor("#535353"));

            tv_revers.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            tv_revers.setTextColor(Color.parseColor("#535353"));

            tv_refund.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            tv_refund.setTextColor(Color.parseColor("#535353"));

            tv_close.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            tv_close.setTextColor(Color.parseColor("#535353"));

        } else {
            tv_succ.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            tv_succ.setTextColor(Color.parseColor("#E3E3E3"));

            tv_fail.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            tv_fail.setTextColor(Color.parseColor("#E3E3E3"));

            tv_revers.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            tv_revers.setTextColor(Color.parseColor("#E3E3E3"));

            tv_refund.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            tv_refund.setTextColor(Color.parseColor("#E3E3E3"));

            tv_close.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            tv_close.setTextColor(Color.parseColor("#E3E3E3"));

        }
    }


    void setRefundDisabled() {
        tv_tyep_refund.setEnabled(false);
        tv_tyep_refund.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
        tv_tyep_refund.setTextColor(Color.parseColor("#E3E3E3"));
    }

    void setRefundEnabled() {
        tv_tyep_refund.setEnabled(true);
        tv_tyep_refund.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
        tv_tyep_refund.setTextColor(Color.parseColor("#535353"));

    }


    void updatepayStateButBg(Button b) {
        isTag = false;

        if (choiceType.contains(b.getTag().toString())) {
            //未选中的状态
            b.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            choiceType.remove(b.getTag().toString());
            b.setTextColor(Color.parseColor("#535353"));
            if (!isContainsPayState()) {
                setRefundEnabled();
            }

        } else {
            //选中的状态
            b.setBackgroundResource(R.drawable.button_shape);
            choiceType.add(b.getTag().toString());
            b.setTextColor(Color.WHITE);
        }
    }

    void updateRefundTypeButtonBg(Button b, String type) {
        if (choiceType.contains(b.getTag().toString())) {
            //设置成未选中的状态
            b.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            choiceType.remove(b.getTag().toString());
            b.setTextColor(Color.parseColor("#535353"));
            b.setEnabled(true);
            setPayTypeSetEnabled(true);
        } else {
            setPayTypeSetEnabled(false);

            //设置成选中的状态
            b.setBackgroundResource(R.drawable.button_shape);
            choiceType.add(b.getTag().toString());
            b.setTextColor(Color.WHITE);
        }
    }

    //tv_wx, tv_zfb, tv_qq, tv_jd, tv_succ, tv_fail, tv_revers, tv_refund, tv_close, tv_tyep_refund,
    //    tv_card;
    private void setLisert() {
        gv_pay_type.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int postion, long arg3) {
                DynModel dynModel = list.get(postion);
                if (dynModel != null) {
                    TextView tv = (TextView) v.findViewById(R.id.tv_content);
                    if (payTypeList.contains(dynModel.getApiCode())) {
                        payTypeList.remove(dynModel.getApiCode());

                        //设置成未选中的状态
                        tv.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
                        tv.setTextColor(Color.parseColor("#535353"));
                        tv.setEnabled(true);

                    } else {
                        payTypeList.add(dynModel.getApiCode());
                        //变成可点击
                        tv.setBackgroundResource(R.drawable.button_shape);
                        tv.setTextColor(Color.WHITE);
                        tv.setEnabled(false);

                    }
                }
            }
        });

        tv_wx.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                updatepayTypeButBg(tv_wx, "1");
            }
        });

        tv_zfb.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                updatepayTypeButBg(tv_zfb, "2");
            }
        });
        tv_fail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setButtonGrey(tv_tyep_refund);
                updatepayStateButBg(tv_fail);
            }
        });
        tv_succ.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setButtonGrey(tv_tyep_refund);
                updatepayStateButBg(tv_succ);
            }
        });
        tv_revers.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setButtonGrey(tv_tyep_refund);
                updatepayStateButBg(tv_revers);
            }
        });
        tv_refund.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setButtonGrey(tv_tyep_refund);
                updatepayStateButBg(tv_refund);
            }
        });
        tv_close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setButtonGrey(tv_tyep_refund);
                updatepayStateButBg(tv_close);
                //                choiceType.clear();

            }
        });// tv_tyep_refund,
        //      tv_card;
        tv_tyep_refund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateRefundTypeButtonBg(tv_tyep_refund, "4");
            }
        });


        ly_ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (choiceType.size() == 0 && isTag) {
                    ToastHelper.showInfo(activity, ToastHelper.toStr(R.string.tx_bill_stream_chioce_not_null));
                    return;
                } else {
                    ishadhandle = true;
                    handleBtn.handleOkBtn(choiceType, payTypeList, true);
                    dismiss();
                }
            }
        });

        tv_reset.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                for (Button b : listBut) {
                    resetBut(b);
                }
                if (choiceType.size() > 0) {
                    isTag = true;
                }
                tv_tyep_refund.setEnabled(true);
                choiceType.clear();

                if (list != null && list.size() > 0) {
                    for (DynModel d : list) {
                        d.setEnaled(0);
                    }
                    payTypeAdape.notifyDataSetChanged();
                }

                payTypeList.clear();
            }
        });
    }

    private void initview(View v) {
        gv_pay_type = (GridViewForScrollView) v.findViewById(R.id.gv_pay_type);
        ly_pay_type = (LinearLayout) v.findViewById(R.id.ly_pay_type);
        gv_pay_type.setSelector(new ColorDrawable(Color.TRANSPARENT));
        gv_pay_type.setFocusable(true);
        gv_pay_type.setFocusableInTouchMode(true);
        gv_pay_type.requestFocus();

        tv_wx = (Button) v.findViewById(R.id.tv_wx);
        tv_zfb = (Button) v.findViewById(R.id.tv_zfb);
        tv_succ = (Button) v.findViewById(R.id.tv_succ);
        tv_fail = (Button) v.findViewById(R.id.tv_fail);
        tv_revers = (Button) v.findViewById(R.id.tv_revers);
        tv_refund = (Button) v.findViewById(R.id.tv_refund);
        tv_close = (Button) v.findViewById(R.id.tv_close);
        tv_tyep_refund = (Button) v.findViewById(R.id.tv_tyep_refund);

        tv_reset = (TextView) v.findViewById(R.id.tv_reset);
        ly_ok = (TextView) v.findViewById(R.id.ly_ok);
        listBut.add(tv_wx);
        listBut.add(tv_zfb);

        listBut.add(tv_succ);
        listBut.add(tv_fail);
        listBut.add(tv_revers);
        listBut.add(tv_refund);
        listBut.add(tv_close);
        listBut.add(tv_tyep_refund);

        list = new ArrayList<DynModel>();
        //获取订单的serviceType匹配到的类型去筛选流水
        Object object = SharedPreUtils.readProduct("dynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());

        if (object != null) {
            list = (List<DynModel>) object;
            if (null != list && list.size() > 0) {
                if (payTypeList.size() > 0) {
                    for (String s : payTypeList) {
                        for (int i = 0; i < list.size(); i++) {
                            DynModel dynModel = list.get(i);

                            if (s.equals(dynModel.getApiCode())) {
                                dynModel.setEnaled(1);
                                list.set(i, dynModel);
                            }
                        }
                    }
                }
                ly_pay_type.setVisibility(View.GONE);
                gv_pay_type.setVisibility(View.VISIBLE);
                payTypeAdape = new PayTypeAdape(activity, list);
                gv_pay_type.setAdapter(payTypeAdape);

            } else {
                ly_pay_type.setVisibility(View.VISIBLE);
                gv_pay_type.setVisibility(View.GONE);
            }
        } else {
            gv_pay_type.setVisibility(View.GONE);
            ly_pay_type.setVisibility(View.VISIBLE);
        }
    }

    class PayTypeAdape extends BaseAdapter {

        private List<DynModel> list;

        private Context context;

        private PayTypeAdape(Context context, List<DynModel> list) {
            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.activity_bill_paytype_list_item, null);
                payholder = new ViewPayTypeHolder();
                payholder.tv_content = (TextView) convertView.findViewById(R.id.tv_content);
                convertView.setTag(payholder);
            } else {
                payholder = (ViewPayTypeHolder) convertView.getTag();
            }

            DynModel dynModel = list.get(position);
            if (dynModel != null && !StringUtil.isEmptyOrNull(dynModel.getProviderName())) {
                payholder.tv_content.setText(MainApplication.getInstance().getPayTypeMap().get(dynModel.getApiCode()));
                switch (dynModel.getEnabled()) {
                    case 0://默认状态
                        payholder.tv_content.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
                        payholder.tv_content.setTextColor(Color.parseColor("#535353"));
                        payholder.tv_content.setEnabled(true);
                        break;
                    case 1://选中
                        payholder.tv_content.setBackgroundResource(R.drawable.button_shape);
                        payholder.tv_content.setTextColor(Color.WHITE);
                        payholder.tv_content.setEnabled(true);
                        break;
                    case 2://置灰
                        payholder.tv_content.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
                        payholder.tv_content.setTextColor(Color.parseColor("#E3E3E3"));
                        payholder.tv_content.setEnabled(false);
                        break;

                    default:
                        break;
                }

            }

            return convertView;
        }
    }

    class ViewPayTypeHolder {
        TextView tv_content;
    }

    void resetBut(Button b) {
        //设置成未选中的状态
        b.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
        choiceType.remove(b.getTag().toString());
        b.setTextColor(Color.parseColor("#535353"));
        b.setEnabled(true);
    }

    void setButtonGrey(Button b) {
        //设置成灰色
        b.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
        choiceType.remove(b.getTag().toString());
        b.setTextColor(Color.parseColor("#E3E3E3"));
        b.setEnabled(false);
    }


    private ListView lv_pay_type;

    List<DynModel> listQRNative = new ArrayList<DynModel>();
    List<DynModel> listCardPaymentNative = new ArrayList<DynModel>();
    List<DynModel> listBothAll = new ArrayList<DynModel>();

    ViewHolderNative viewHolderNative;

    SelectPicPopupWindow.HandleNative handleNative;

    private class ViewHolderNative {
        private TextView tv_pay_name;

        private View v_line;
    }

    public interface HandleNative {
        void toPay(String type, String secondType, boolean isSecondPay, WalletListBean bean);
    }

    private class NativePayTypeAdpter extends BaseAdapter {
        private Context context;

        List<DynModel> list;

        public NativePayTypeAdpter(Context context, List<DynModel> model) {
            this.list = model;
            this.context = context;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.native_pay_list_item, null);
                viewHolderNative = new ViewHolderNative();
                viewHolderNative.tv_pay_name = (TextView) convertView.findViewById(R.id.tv_pay_name);
                convertView.setTag(viewHolderNative);
            } else {
                viewHolderNative = (ViewHolderNative) convertView.getTag();
            }

            DynModel dynModel = list.get(position);
            if (dynModel != null) {
                String language = LocaleUtils.getLocaleLanguage();
                if (language.equalsIgnoreCase(Constant.LANG_CODE_ZH_CN)) {

                    if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                        viewHolderNative.tv_pay_name.setText(MainApplication.getInstance().getPayTypeMap().get(dynModel.getApiCode()) + context.getString(R.string.choose_pre_auth));
                    } else {
                        viewHolderNative.tv_pay_name.setText(MainApplication.getInstance().getPayTypeMap().get(dynModel.getApiCode()));
                    }

                } else if (language.equalsIgnoreCase(Constant.LANG_CODE_ZH_TW)) {
                    if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                        viewHolderNative.tv_pay_name.setText(MainApplication.getInstance().getPayTypeMap().get(dynModel.getApiCode()) + context.getString(R.string.choose_pre_auth));
                    } else {
                        viewHolderNative.tv_pay_name.setText(MainApplication.getInstance().getPayTypeMap().get(dynModel.getApiCode()));
                    }
                } else {
                    if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                        viewHolderNative.tv_pay_name.setText(MainApplication.getInstance().getPayTypeMap().get(dynModel.getApiCode()) + " " + context.getString(R.string.choose_pre_auth));
                    } else {
                        viewHolderNative.tv_pay_name.setText(MainApplication.getInstance().getPayTypeMap().get(dynModel.getApiCode()));
                    }
                }
            }

            return convertView;
        }
    }

    public SelectPicPopupWindow(Activity context, final SelectPicPopupWindow.HandleNative handleNative) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.alert_dialog, null);
        tv_cancel = (TextView) mMenuView.findViewById(R.id.tv_cel);
        lv_pay_type = (ListView) mMenuView.findViewById(R.id.lv_pay_type);
        this.handleNative = handleNative;

        Object object = null;
        Object object_CardPayment = null;
        //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth", "sale");
        if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(saleOrPreauth, "pre_auth")) {
            object = SharedPreUtils.readProduct("dynPayType_pre_auth" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
        } else {
            object = SharedPreUtils.readProduct("ActiveDynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            object_CardPayment = SharedPreUtils.readProduct("cardPayment_ActiveDynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
        }

        if (object != null) {
            listQRNative = (List<DynModel>) object;
        }
        if (object_CardPayment != null) {
            listCardPaymentNative = (List<DynModel>) object_CardPayment;
        }
        if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(saleOrPreauth, "pre_auth")) {
            //如果是预授权，则只添加QR数据
            if (listBothAll != null && listQRNative != null && listQRNative.size() > 0) {
                listBothAll.addAll(listQRNative);
            }
        } else {
            //把两个数组拼接起来
            if (listBothAll != null && listQRNative != null && listQRNative.size() > 0) {
                listBothAll.addAll(listQRNative);
            }
            if (listBothAll != null && listCardPaymentNative != null && listCardPaymentNative.size() > 0) {
                listBothAll.addAll(listCardPaymentNative);
            }
        }

        if (listBothAll != null && listBothAll.size() > 0) {
            //列表数据重排列----如果是AUB的话
            if (BuildConfig.bankCode.contains("aub")) {
                if (containsInstapay(listBothAll) && getInstapayIndex(listBothAll) != -1 && containsInstapayQRPayment(listBothAll) && getInstapayQRPaymentIndex(listBothAll) != -1) {
                    List<DynModel> list_temp = new ArrayList<>();
                    list_temp.clear();
                    list_temp.addAll(listBothAll);

                    //取得instapay的支付方式的model
                    DynModel dynModel = list_temp.get(getInstapayIndex(list_temp));
                    //先移除第一个，重新计算index，再在新的列表里删除第二个
                    list_temp.remove(getInstapayIndex(list_temp));

                    //取得QR Ph Payment的支付方式的model
                    DynModel dynModel_QRPayment = list_temp.get(getInstapayQRPaymentIndex(list_temp));
                    list_temp.remove(getInstapayQRPaymentIndex(list_temp));

                    listBothAll.clear();
                    //先添加QR Ph Payment
                    listBothAll.add(dynModel_QRPayment);
                    //再添加instapay
                    listBothAll.add(dynModel);

                    listBothAll.addAll(list_temp);

                } else if (containsInstapay(listBothAll) && getInstapayIndex(listBothAll) != -1 && !containsInstapayQRPayment(listBothAll)) {//只有instapay
                    List<DynModel> list_temp = new ArrayList<>();
                    list_temp.clear();
                    list_temp.addAll(listBothAll);

                    DynModel dynModel = list_temp.get(getInstapayIndex(list_temp));
                    list_temp.remove(getInstapayIndex(list_temp));

                    listBothAll.clear();
                    listBothAll.add(dynModel);
                    listBothAll.addAll(list_temp);

                } else if (containsInstapayQRPayment(listBothAll) && getInstapayQRPaymentIndex(listBothAll) != -1 && !containsInstapay(listBothAll)) {//只有QR Ph Payment
                    List<DynModel> list_temp = new ArrayList<>();
                    list_temp.clear();
                    list_temp.addAll(listBothAll);

                    //取得QR Ph Payment的支付方式的model
                    DynModel dynModel_QRPayment = list_temp.get(getInstapayQRPaymentIndex(list_temp));
                    list_temp.remove(getInstapayQRPaymentIndex(list_temp));

                    listBothAll.clear();
                    //再添加QR Ph Payment
                    listBothAll.add(dynModel_QRPayment);
                    listBothAll.addAll(list_temp);
                }
            }

            if (null != listBothAll && listBothAll.size() > 0) {
                lv_pay_type.setVisibility(View.VISIBLE);
                NativePayTypeAdpter nativePayTypeAdpter = new NativePayTypeAdpter(context, listBothAll);
                lv_pay_type.setAdapter(nativePayTypeAdpter);
            }
        } else {
            lv_pay_type.setVisibility(View.GONE);
        }

        lv_pay_type.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int postion, long arg3) {
                DynModel dynModel = listBothAll.get(postion);
                if (dynModel != null) {
                    dismiss();
                    handleNative.toPay(dynModel.getApiCode(), null, false, null);
                }
            }
        });

        //取消按钮
        tv_cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //销毁弹出框
                dismiss();
            }
        });

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });

    }

    //判断当前的支付列表中是否含有 instapay的支付通道
    public boolean containsInstapay(List<DynModel> list) {
        for (int i = 0; i < list.size(); i++) {
            if (getNativePayType(list.get(i).getApiCode()).equalsIgnoreCase(Constant.NATIVE_INSTAPAY)) {
                return true;
            }
        }
        return false;
    }

    //判断当前的支付列表中是否含有 instapay的QR Ph Payment支付通道
    public boolean containsInstapayQRPayment(List<DynModel> list) {
        for (int i = 0; i < list.size(); i++) {
            if (getNativePayType(list.get(i).getApiCode()).equalsIgnoreCase(Constant.NATIVE_INSTAPAY_V2)) {
                return true;
            }
        }
        return false;
    }


    //获取instapay的通道在列表中的位置
    public int getInstapayIndex(List<DynModel> list) {
        for (int i = 0; i < list.size(); i++) {
            if (getNativePayType(list.get(i).getApiCode()).equalsIgnoreCase(Constant.NATIVE_INSTAPAY)) {
                return i;
            }
        }
        return -1;
    }

    //获取instapay的QR Ph Payment的通道在列表中的位置
    public int getInstapayQRPaymentIndex(List<DynModel> list) {
        for (int i = 0; i < list.size(); i++) {
            if (getNativePayType(list.get(i).getApiCode()).equalsIgnoreCase(Constant.NATIVE_INSTAPAY_V2)) {
                return i;
            }
        }
        return -1;
    }

    //通过ApiCode得到当前支付类型的NativePayType
    public String getNativePayType(String ApiCode) {
        //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth", "sale");
        if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(saleOrPreauth, "pre_auth")) {
            Object object = SharedPreUtils.readProduct("dynPayType_pre_auth" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            if (object != null) {
                List<DynModel> list = (List<DynModel>) object;
                if (null != list && list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getApiCode().equalsIgnoreCase(ApiCode)) {
                            return list.get(i).getNativeTradeType();
                        }
                    }
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            Object object = SharedPreUtils.readProduct("ActiveDynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            Object object_CardPayment = SharedPreUtils.readProduct("cardPayment_ActiveDynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());

            List<DynModel> list = new ArrayList<>();
            List<DynModel> list_CardPayment = new ArrayList<>();
            if (object != null) {
                list = (List<DynModel>) object;
            }
            if (object_CardPayment != null) {
                list_CardPayment = (List<DynModel>) object_CardPayment;
            }

            //把两个数组拼接起来
            List<DynModel> listAll = new ArrayList<>();
            if (list != null) {
                listAll.addAll(list);
            }
            if (list_CardPayment != null) {
                listAll.addAll(list_CardPayment);
            }

            if (null != listAll && listAll.size() > 0) {
                for (int i = 0; i < listAll.size(); i++) {
                    if (listAll.get(i).getApiCode().equalsIgnoreCase(ApiCode)) {
                        return listAll.get(i).getNativeTradeType();
                    }
                }
            } else {
                return null;
            }
        }
        return null;
    }


    private ListView lv_second_pay_type;
    private LiquidNativePayTypeAdpter liquidNativePayTypeAdpter;

    public SelectPicPopupWindow(Activity context, String type, boolean isSecond, final SelectPicPopupWindow.HandleNative handleNative) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.alert_dialog, null);
        tv_cancel = (TextView) mMenuView.findViewById(R.id.tv_cel);
        lv_second_pay_type = (ListView) mMenuView.findViewById(R.id.lv_pay_type);
        this.handleNative = handleNative;

        //如果获取过liquid pay小钱包数据信息
        if (MainApplication.getInstance().getWalletListBeans() != null && MainApplication.getInstance().getWalletListBeans().size() > 0) {
            liquidNativePayTypeAdpter = new LiquidNativePayTypeAdpter(context, MainApplication.getInstance().getWalletListBeans());
            lv_second_pay_type.setAdapter(liquidNativePayTypeAdpter);

            lv_second_pay_type.setVisibility(View.VISIBLE);

        } else {
            lv_second_pay_type.setVisibility(View.GONE);
        }


        lv_second_pay_type.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int postion, long arg3) {

                WalletListBean walletListBean = MainApplication.getInstance().getWalletListBeans().get(postion);
                if (walletListBean != null) {
                    dismiss();
                    handleNative.toPay(type, walletListBean.getPayloadCode(), isSecond, walletListBean);
                }
            }
        });

        //取消按钮
        tv_cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //销毁弹出框
                dismiss();
            }
        });

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });

    }

    private class LiquidNativePayTypeAdpter extends BaseAdapter {
        private Context context;
        private List<WalletListBean> Walletlist;

        public LiquidNativePayTypeAdpter(Context context, List<WalletListBean> model) {
            this.Walletlist = model;
            this.context = context;
        }

        @Override
        public int getCount() {
            return Walletlist.size();
        }

        @Override
        public Object getItem(int position) {
            return Walletlist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.native_pay_list_item, null);
                viewHolderNative = new ViewHolderNative();
                viewHolderNative.tv_pay_name = (TextView) convertView.findViewById(R.id.tv_pay_name);
                convertView.setTag(viewHolderNative);
            } else {
                viewHolderNative = (ViewHolderNative) convertView.getTag();
            }

            WalletListBean walletModel = Walletlist.get(position);
            if (walletModel != null) {
                String language = LocaleUtils.getLocaleLanguage();
                if (language.equalsIgnoreCase(Constant.LANG_CODE_ZH_CN)) {

                    if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                        viewHolderNative.tv_pay_name.setText(walletModel.getName() + context.getString(R.string.choose_pre_auth));
                    } else {
                        viewHolderNative.tv_pay_name.setText(walletModel.getName());
                    }

                } else if (language.equalsIgnoreCase(Constant.LANG_CODE_ZH_TW)) {
                    if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                        viewHolderNative.tv_pay_name.setText(walletModel.getName() + context.getString(R.string.choose_pre_auth));
                    } else {
                        viewHolderNative.tv_pay_name.setText(walletModel.getName());
                    }
                } else {
                    if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                        viewHolderNative.tv_pay_name.setText(walletModel.getName() + " " + context.getString(R.string.choose_pre_auth));
                    } else {
                        viewHolderNative.tv_pay_name.setText(walletModel.getName());
                    }
                }
            }

            return convertView;
        }
    }

}
