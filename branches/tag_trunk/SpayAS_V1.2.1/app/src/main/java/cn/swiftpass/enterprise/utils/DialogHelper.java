/**************************************************************
 * 作者: 			huangdhua
 * 创建时间:		2012-11-14  下午05:20:57
 * 功能描述:		针对diallog 显示处理 
 *
 * 版权声明:本程序版权归 深圳市时代纬科技有限公司所有 Copy right 2010-2012
 **************************************************************/
package cn.swiftpass.enterprise.utils;

import java.lang.reflect.Field;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class DialogHelper
{
    /**
     * 多个条目显示
     * 
     * @param title
     * @param mContext
     * @param dialogItem
     * @return
     */
    public static AlertDialog.Builder createListDialog(String title, final Context mContext,
        final List<DialogItem> dialogItem)
    {
        CharSequence[] data = new CharSequence[dialogItem.size()];
        int i = 0;
        for (DialogItem d : dialogItem)
        {
            data[i] = d.getName();
            i++;
        }
        return new AlertDialog.Builder(mContext).setTitle(title).setItems(data, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                if (dialogItem.get(which).getRequestCode() != 0)
                {
                    dialogItem.get(which).activity.startActivityForResult(dialogItem.get(which).link,
                        dialogItem.get(which).getRequestCode());
                }
                else
                {
                    mContext.startActivity(dialogItem.get(which).link);
                }
            }
        });
    }
    
    public static class DialogItem
    {
        public String name;
        
        public Intent link;
        
        public int requestCode = 0;
        
        public Activity activity;
        
        public DialogItem()
        {
        }
        
        public DialogItem(String name, Intent link)
        {
            this.name = name;
            this.link = link;
        }
        
        public DialogItem(String name, Intent link, int requestCode, Activity activity)
        {
            this.name = name;
            this.link = link;
            this.requestCode = requestCode;
            this.activity = activity;
        }
        
        public int getRequestCode()
        {
            return requestCode;
        }
        
        public void setRequestCode(int requestCode)
        {
            this.requestCode = requestCode;
        }
        
        public String getName()
        {
            return name;
        }
        
        public void setName(String name)
        {
            this.name = name;
        }
        
        public Intent getLink()
        {
            return link;
        }
        
        public void setLink(Intent link)
        {
            this.link = link;
        }
    }
    
    /***
     * 确认对话框
     * @param title
     * @param msg
     * @param okbtnName
     * @param context
     * @return
     */
    public static Dialog showDialog(String title, String msg, int okbtnName, Context context)
    {
        Dialog dialog =
            new AlertDialog.Builder(context).setTitle(title)
                .setMessage(msg)
                .setPositiveButton(okbtnName, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                        return;
                    }
                })
                .create();
        return dialog;
    }
    
    /**
     * 提示框
     * 
     * @param title
     * @param msg
     * @param cancelbtnName
     * @param okbtnName
     *            按钮名字
     * @param context
     * @param okbtnListener
     * @return
     */
    public static Dialog showDialog(String title, String msg, int cancelbtnName, int okbtnName, Context context,
        DialogInterface.OnClickListener okbtnListener)
    {
        Dialog dialog =
            new AlertDialog.Builder(context).setTitle(title)
                .setMessage(msg)
                .setNegativeButton(cancelbtnName, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                        return;
                    }
                })
                .setPositiveButton(okbtnName, okbtnListener)
                .create();
        return dialog;
    }
    
    public static Dialog showDialog(String title, String msg, int okbtnName, Context context,
        DialogInterface.OnClickListener okbtnListener)
    {
        Dialog dialog =
            new AlertDialog.Builder(context).setTitle(title)
                .setMessage(msg)
                .setPositiveButton(okbtnName, okbtnListener)
                
                .create();
        
        return dialog;
    }
    
    public static Dialog showDialog(String title, String msg, String cancelbtnName, String okbtnName, Context context,
        DialogInterface.OnClickListener okbtnListener, int theme)
    {
        return showDialog(title,
            msg,
            cancelbtnName,
            okbtnName,
            context,
            okbtnListener,
            new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    dialog.dismiss();
                    return;
                }
            });
    }
    
    public static Dialog showDialog(String title, View v, Context context)
    {
        Dialog dialog = new AlertDialog.Builder(context).setTitle(title).setView(v).create();
        return dialog;
    }
    
    public static Dialog showDialog(String title, String msg, String cancelbtnName, String okbtnName, Context context,
        DialogInterface.OnClickListener okbtnListener, DialogInterface.OnClickListener cancelbtnListener)
    {
        
        Dialog dialog =
            new AlertDialog.Builder(context).setTitle(title)
                
                .setMessage(msg)
                .setNegativeButton(cancelbtnName, cancelbtnListener)
                .setPositiveButton(okbtnName, okbtnListener)
                .create();
        
        return dialog;
    }
    
    /**
     * 显示视图dialog
     * @param title
     * @param v
     * @param cancelbtnName
     * @param okbtnName
     * @param context
     * @param okbtnListener
     * @return
     */
    public static Dialog showDialog(String title, View v, int cancelbtnName, int okbtnName, Context context,
        DialogInterface.OnClickListener okbtnListener)
    {
        Dialog dialog =
            new AlertDialog.Builder(context).setTitle(title)
                .setView(v)
                .setNegativeButton(cancelbtnName, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                        distoryDialog(dialog);
                        return;
                    }
                })
                .setPositiveButton(okbtnName, okbtnListener)
                .create();
        return dialog;
    }
    
    public static void keepDialog(DialogInterface dialog)
    {
        try
        {
            if (dialog == null)
            {
                return;
            }
            Field field = dialog.getClass().getSuperclass().getDeclaredField("mShowing");
            field.setAccessible(true);
            field.set(dialog, false);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public static void distoryDialog(DialogInterface dialog)
    {
        
        try
        {
            if (dialog == null)
            {
                return;
            }
            Field field = dialog.getClass().getSuperclass().getDeclaredField("mShowing");
            field.setAccessible(true);
            field.set(dialog, true);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    private static ProgressDialog loadingDialog = null;
    
    /**
     * 显示一个正在加载对话框
     * @param msg　要显示的文本内容
     */
    public static void showLoading(String msg, Context context)
    {
        if (loadingDialog == null)
        {
            loadingDialog = new ProgressDialog(context);
        }
        
        loadingDialog.setMessage(msg);
        loadingDialog.show();
    }
    
    /**
     * 关闭加载对话框
     */
    public static void dismissLoading()
    {
        if (loadingDialog != null)
        {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }
    
    /**重设dialog 大小 为屏幕的80%*/
    public static void resize(Activity activity, Dialog dialog)
    {
        
        WindowManager m = activity.getWindowManager();
        Display display = m.getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        Window window = dialog.getWindow();
        WindowManager.LayoutParams layoutParams = window.getAttributes();
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;//(int) (height * 0.5);
        layoutParams.width = (int)(width * 0.85);
        window.setAttributes(layoutParams);
        
    }
    
    /**重设dialog 大小 为屏幕的80%*/
    public static void resize(Context activity, Dialog dialog)
    {
        
        WindowManager m = (WindowManager)activity.getSystemService(Context.WINDOW_SERVICE);
        Display display = m.getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        Window window = dialog.getWindow();
        WindowManager.LayoutParams layoutParams = window.getAttributes();
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;//(int) (height * 0.5);
        layoutParams.width = (int)(width * 0.85);
        window.setAttributes(layoutParams);
        
    }
    
    /**重设dialog 大小 为屏幕的80%*/
    public static void resizeNew(Activity activity, Dialog dialog)
    {
        
        WindowManager m = activity.getWindowManager();
        Display display = m.getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        Window window = dialog.getWindow();
        WindowManager.LayoutParams layoutParams = window.getAttributes();
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;//(int) (height * 0.5);
        layoutParams.width = DisplayUtil.dip2Px(activity, 130);
        
    }
    
}
