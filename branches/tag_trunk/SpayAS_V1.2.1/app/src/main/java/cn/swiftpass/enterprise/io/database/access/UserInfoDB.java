package cn.swiftpass.enterprise.io.database.access;

import java.sql.SQLException;
import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.io.database.table.UserInfoTable;
import cn.swiftpass.enterprise.utils.MD5;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

/**
 *
 * User: Administrator
 * Date: 13-10-30
 * Time: 下午6:59
 */
public class UserInfoDB
{
    private static Dao<UserModel, Integer> userDao;
    
    private UserInfoDB()
    {
        try
        {
            userDao = MainApplication.getContext().getHelper().getDao(UserModel.class);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    
    private static UserInfoDB instance;
    
    public static UserInfoDB getInstance()
    {
        if (instance == null)
        {
            instance = new UserInfoDB();
        }
        return instance;
    }
    
    public void save(UserModel userModel)
        throws SQLException
    {
        userModel.addTime = System.currentTimeMillis();
        userModel.loginTime = System.currentTimeMillis();
        //        // 保存商户号，用于区分    是否激活 等验证
        //        userModel.merchantId = userModel.merchantId;
        //        String pwd = MD5.md5s(userModel.pwd);
        //        userModel.pwd = pwd;
        userDao.create(userModel);
    }
    
    /**
     * 修改更新商户号
     * @throws SQLException 
     */
    public void update(UserModel userModel)
        throws SQLException
    {
        userDao.update(userModel);
    }
    
    /**获取登录最近的一个用户*/
    public UserModel queryByLastTime()
        throws SQLException
    {
        QueryBuilder<UserModel, Integer> builder = userDao.queryBuilder();
        GenericRawResults<String[]> rawResults =
            userDao.queryRaw("select " + UserInfoTable.COLUMN_ID + "," + UserInfoTable.COLUMN_USER_NAME + " , "
                + UserInfoTable.COLUMN_USER_PWD + " , " + UserInfoTable.COLUMN_REAL_NAME + " , "
                + UserInfoTable.COLUMN_M_ID + " , " + UserInfoTable.COLUMN_M_NAME + " , " + UserInfoTable.COLUMN_M_NAME
                + " from " + UserInfoTable.TABLE_NAME + " where " + UserInfoTable.COLUMN_LOGIN_TIME + " = (select max("
                + UserInfoTable.COLUMN_LOGIN_TIME + ") from " + UserInfoTable.TABLE_NAME + "order by "
                + UserInfoTable.COLUMN_LOGIN_TIME + " desc   limit 0,1");
        
        List<String[]> list = rawResults.getResults();
        UserModel userModel = new UserModel();
        //userModel.id = list.get(UserInfoTable.COLUMN_ID);
        String[] str = list.get(0);
        userModel.name = str[1];
        userModel.pwd = str[2];
        userModel.realname = str[3];
        userModel.merchantId = str[4];
        userModel.merchantName = str[5];
        return userModel;
        
    }
    
    /**获取最近登录的用户*/
    public List<UserModel> query(String mId)
        throws SQLException
    {
        QueryBuilder<UserModel, Integer> builder = userDao.queryBuilder();
        Where<UserModel, Integer> where = builder.where();
        if (null != mId && !"".equals(mId))
        {
            where.eq(UserInfoTable.COLUMN_M_ID, mId);
        }
        builder.orderBy(UserInfoTable.COLUMN_LOGIN_TIME, false);
        List<UserModel> list = userDao.query(builder.prepare());
        return list;
        
    }
    
    /**
     * 根据用户名查询本地数据
     * <功能详细描述>
     * @return
     * @throws SQLException 
     * @see [类、类#方法、类#成员]
     */
    public UserModel queryUserByName(String userName)
        throws SQLException
    {
        QueryBuilder<UserModel, Integer> builder = userDao.queryBuilder();
        Where<UserModel, Integer> where = builder.where();
        where.eq(UserInfoTable.COLUMN_USER_NAME, userName);
        where.and();
        List<UserModel> list = userDao.query(builder.prepare());
        if (list != null && list.size() > 0)
        {
            return list.get(0);
        }
        return null;
    }
    
    public UserModel queryUser(long id, String userName, String pwd, String mId)
        throws SQLException
    {
        QueryBuilder<UserModel, Integer> builder = userDao.queryBuilder();
        Where<UserModel, Integer> where = builder.where();
        if (id != 0)
        {
            where.eq(UserInfoTable.COLUMN_U_ID, id);
            where.and();
        }
        where.eq(UserInfoTable.COLUMN_USER_NAME, userName);
        where.and();
        String pwd2 = MD5.md5s(pwd);
        where.eq(UserInfoTable.COLUMN_USER_PWD, pwd2);
        if (mId != null)
        {
            where.and();
            where.eq(UserInfoTable.COLUMN_M_ID, mId);
        }
        //builder.orderBy(UserInfoTable.COLUMN_ADD_TIME,false);
        List<UserModel> list = userDao.query(builder.prepare());
        if (list != null && list.size() > 0)
        {
            return list.get(0);
        }
        return null;
    }
    
}
