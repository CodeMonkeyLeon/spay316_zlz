package cn.swiftpass.enterprise.bussiness.model;

public class ShopBankInfo extends ShopModel
{
    
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    public String shopName;
    
    public String shopAddress;
    
    //spay新版本注册 
    private String merchantType; //商户小类型   
    
    private String merchantTypeName; //商户类型名称
    
    // 银行类型
    private Integer bankType;
    
    /**
     * @return 返回 bankType
     */
    public Integer getBankType()
    {
        return bankType;
    }
    
    /**
     * @param 对bankType进行赋值
     */
    public void setBankType(Integer bankType)
    {
        this.bankType = bankType;
    }
    
    // 固话
    private String tel;
    
    private String shop_bank;
    
    private String shop_branch;
    
    private String branch_name;
    
    private String bank_num;
    
    private String personal_id;
    
    private String id_up_photo_Ulr;
    
    private String id_down_photo_Ulr;
    
    private String id_people_photo_Ulr;
    
    private String id_ird_Ulr;
    
    private String id_code_Ulr;
    
    // 收款人姓名
    private String bayee_name;
    
    private String bankTypeName; //银行类型
    
    /**
     * @return 返回 bankTypeName
     */
    public String getBankTypeName()
    {
        return bankTypeName;
    }
    
    /**
     * @param 对bankTypeName进行赋值
     */
    public void setBankTypeName(String bankTypeName)
    {
        this.bankTypeName = bankTypeName;
    }
    
    /**
     * @return 返回 tel
     */
    public String getTel()
    {
        return tel;
    }
    
    /**
     * @return 返回 merchantType
     */
    public String getMerchantType()
    {
        return merchantType;
    }
    
    /**
     * @param 对merchantType进行赋值
     */
    public void setMerchantType(String merchantType)
    {
        this.merchantType = merchantType;
    }
    
    /**
     * @return 返回 merchantTypeName
     */
    public String getMerchantTypeName()
    {
        return merchantTypeName;
    }
    
    /**
     * @param 对merchantTypeName进行赋值
     */
    public void setMerchantTypeName(String merchantTypeName)
    {
        this.merchantTypeName = merchantTypeName;
    }
    
    /**
     * @return 返回 serialversionuid
     */
    public static long getSerialversionuid()
    {
        return serialVersionUID;
    }
    
    /**
     * @param 对tel进行赋值
     */
    public void setTel(String tel)
    {
        this.tel = tel;
    }
    
    /**
     * @return 返回 shopName
     */
    public String getShopName()
    {
        return shopName;
    }
    
    /**
     * @param 对shopName进行赋值
     */
    public void setShopName(String shopName)
    {
        this.shopName = shopName;
    }
    
    /**
     * @return 返回 shopAddress
     */
    public String getShopAddress()
    {
        return shopAddress;
    }
    
    /**
     * @param 对shopAddress进行赋值
     */
    public void setShopAddress(String shopAddress)
    {
        this.shopAddress = shopAddress;
    }
    
    /**
     * @return 返回 bayee_name
     */
    public String getBayee_name()
    {
        return bayee_name;
    }
    
    /**
     * @param 对bayee_name进行赋值
     */
    public void setBayee_name(String bayee_name)
    {
        this.bayee_name = bayee_name;
    }
    
    /**
     * @return 返回 id_up_photo_Ulr
     */
    public String getId_up_photo_Ulr()
    {
        return id_up_photo_Ulr;
    }
    
    /**
     * @param 对id_up_photo_Ulr进行赋值
     */
    public void setId_up_photo_Ulr(String id_up_photo_Ulr)
    {
        this.id_up_photo_Ulr = id_up_photo_Ulr;
    }
    
    /**
     * @return 返回 id_down_photo_Ulr
     */
    public String getId_down_photo_Ulr()
    {
        return id_down_photo_Ulr;
    }
    
    /**
     * @param 对id_down_photo_Ulr进行赋值
     */
    public void setId_down_photo_Ulr(String id_down_photo_Ulr)
    {
        this.id_down_photo_Ulr = id_down_photo_Ulr;
    }
    
    /**
     * @return 返回 id_people_photo_Ulr
     */
    public String getId_people_photo_Ulr()
    {
        return id_people_photo_Ulr;
    }
    
    /**
     * @param 对id_people_photo_Ulr进行赋值
     */
    public void setId_people_photo_Ulr(String id_people_photo_Ulr)
    {
        this.id_people_photo_Ulr = id_people_photo_Ulr;
    }
    
    /**
     * @return 返回 id_ird_Ulr
     */
    public String getId_ird_Ulr()
    {
        return id_ird_Ulr;
    }
    
    /**
     * @param 对id_ird_Ulr进行赋值
     */
    public void setId_ird_Ulr(String id_ird_Ulr)
    {
        this.id_ird_Ulr = id_ird_Ulr;
    }
    
    /**
     * @return 返回 id_code_Ulr
     */
    public String getId_code_Ulr()
    {
        return id_code_Ulr;
    }
    
    /**
     * @param 对id_code_Ulr进行赋值
     */
    public void setId_code_Ulr(String id_code_Ulr)
    {
        this.id_code_Ulr = id_code_Ulr;
    }
    
    /**
     * @return 返回 shop_bank
     */
    public String getShop_bank()
    {
        return shop_bank;
    }
    
    /**
     * @param 对shop_bank进行赋值
     */
    public void setShop_bank(String shop_bank)
    {
        this.shop_bank = shop_bank;
    }
    
    /**
     * @return 返回 shop_branch
     */
    public String getShop_branch()
    {
        return shop_branch;
    }
    
    /**
     * @param 对shop_branch进行赋值
     */
    public void setShop_branch(String shop_branch)
    {
        this.shop_branch = shop_branch;
    }
    
    /**
     * @return 返回 branch_name
     */
    public String getBranch_name()
    {
        return branch_name;
    }
    
    /**
     * @param 对branch_name进行赋值
     */
    public void setBranch_name(String branch_name)
    {
        this.branch_name = branch_name;
    }
    
    /**
     * @return 返回 bank_num
     */
    public String getBank_num()
    {
        return bank_num;
    }
    
    /**
     * @param 对bank_num进行赋值
     */
    public void setBank_num(String bank_num)
    {
        this.bank_num = bank_num;
    }
    
    /**
     * @return 返回 personal_id
     */
    public String getPersonal_id()
    {
        return personal_id;
    }
    
    /**
     * @param 对personal_id进行赋值
     */
    public void setPersonal_id(String personal_id)
    {
        this.personal_id = personal_id;
    }
    
}
