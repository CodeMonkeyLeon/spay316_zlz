package cn.swiftpass.enterprise.ui.paymentlink.adapter;

import android.content.Context;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import org.xclcharts.common.DensityUtil;

import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.paymentlink.model.OtherFee;
import cn.swiftpass.enterprise.ui.widget.BaseRecycleAdapter;
import cn.swiftpass.enterprise.utils.DateUtil;

public class OrderDetailOtherFeeListAdapter extends BaseRecycleAdapter<OtherFee> {

    private Context mContext;

    public OrderDetailOtherFeeListAdapter(Context context, List<OtherFee> data) {
        super(data);
        mContext = context;
    }

    @Override
    protected void bindData(BaseViewHolder holder, int position) {
        OtherFee data = datas.get(position);

        TextView desc1 = (TextView) holder.getView(R.id.tv_desc1);
        TextView desc2 = (TextView) holder.getView(R.id.tv_desc2);

        ConstraintLayout.LayoutParams layoutParams1 = (ConstraintLayout.LayoutParams) desc1.getLayoutParams();
        layoutParams1.setMargins(DensityUtil.dip2px(mContext, 10),
                DensityUtil.dip2px(mContext, 6),
                DensityUtil.dip2px(mContext, 10),
                DensityUtil.dip2px(mContext, 6));
        desc1.setLayoutParams(layoutParams1);

        ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) desc2.getLayoutParams();
        layoutParams2.setMargins(DensityUtil.dip2px(mContext, 10),
                DensityUtil.dip2px(mContext, 6),
                DensityUtil.dip2px(mContext, 10),
                DensityUtil.dip2px(mContext, 6));
        desc2.setLayoutParams(layoutParams2);

        desc1.setText(data.extraCostDesc);
        desc2.setText(MainApplication.getInstance().getFeeFh() + " " +
                DateUtil.formatMoneyUtils(data.extraCost));
    }

    @Override
    public int getLayoutId() {
        return R.layout.simple_item_view;
    }
}
