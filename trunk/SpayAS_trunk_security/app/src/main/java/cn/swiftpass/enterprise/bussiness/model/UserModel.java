package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

import cn.swiftpass.enterprise.bussiness.enums.UserRole;
import cn.swiftpass.enterprise.io.database.table.UserInfoTable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 *
 * User: Alan
 * Date: 13-9-21
 * Time: 下午1:57
 *
 */
@DatabaseTable(tableName = UserInfoTable.TABLE_NAME)
public class UserModel implements Serializable
{
    @DatabaseField(generatedId = true)
    public long id;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public int pageCount = 0;

    public int getPageCount()
    {
        return pageCount;
    }

    public void setPageCount(int pageCount)
    {
        this.pageCount = pageCount;
    }
    
    @DatabaseField(columnName = UserInfoTable.COLUMN_U_ID)
    public long uId;
    
    @DatabaseField(columnName = UserInfoTable.COLUMN_USER_NAME)
    public String name;
    
    @DatabaseField(columnName = UserInfoTable.COLUMN_USER_PWD)
    public String pwd;
    
    @DatabaseField(columnName = UserInfoTable.COLUMN_REAL_NAME)
    public String realname;
    
    public String mchId;
    
    public int isMch;
    
    public String tel7;
    
    public String tel;
    
    public String userName;
    
    public String password;
    
    public String username;
    
    public String ee;
    
    public String emailCode;

    public String refundLimit;

    public boolean isselect;

    public void setIsselect(boolean isselect) {
        this.isselect = isselect;
    }

    public boolean isIsselect() {
        return isselect;
    }


    public String getRefundLimit() {
        return refundLimit;
    }

    public void setRefundLimit(String refundLimit) {
        this.refundLimit = refundLimit;
    }

    public String getEmailCode()
    {
        return emailCode;
    }

    public void setEmailCode(String emailCode)
    {
        this.emailCode = emailCode;
    }

    public String getEe()
    {
        return ee;
    }

    public void setEe(String ee)
    {
        this.ee = ee;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getTel()
    {
        return tel;
    }

    public void setTel(String tel)
    {
        this.tel = tel;
    }

    public int getIsMch()
    {
        return isMch;
    }

    public void setIsMch(int isMch)
    {
        this.isMch = isMch;
    }

    public String getTel7()
    {
        return tel7;
    }

    public void setTel7(String tel7)
    {
        this.tel7 = tel7;
    }

    public String getMchId()
    {
        return mchId;
    }

    public void setMchId(String mchId)
    {
        this.mchId = mchId;
    }

    public String getRealname()
    {
        return realname;
    }

    public void setRealname(String realname)
    {
        this.realname = realname;
    }
    
    // 资料是否完善标志0:为完善资料，1：已完善资料
    @DatabaseField(columnName = UserInfoTable.COLUMN_EXIT)
    public int existData = 0;
    
    public String code;
    
    public String deptname;
    
    public String isRefundAuth;
    
    public String isOrderAuth;
    
    public boolean enabled;
    
    public String isActivityAuth;

    public String isUnfreezeAuth;

    public String getIsUnfreezeAuth() {
        return isUnfreezeAuth;
    }

    public void setIsUnfreezeAuth(String isUnfreezeAuth) {
        this.isUnfreezeAuth = isUnfreezeAuth;
    }

    public String getIsActivityAuth()
    {
        return isActivityAuth;
    }

    public void setIsActivityAuth(String isActivityAuth)
    {
        this.isActivityAuth = isActivityAuth;
    }

    public String getDeptname()
    {
        return deptname;
    }

    public void setDeptname(String deptname)
    {
        this.deptname = deptname;
    }

    public String getIsRefundAuth()
    {
        return isRefundAuth;
    }

    public void setIsRefundAuth(String isRefundAuth)
    {
        this.isRefundAuth = isRefundAuth;
    }

    public String getIsOrderAuth()
    {
        return isOrderAuth;
    }

    public void setIsOrderAuth(String isOrderAuth)
    {
        this.isOrderAuth = isOrderAuth;
    }

    public boolean getEnabled()
    {
        return enabled;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public long getAddTime()
    {
        return addTime;
    }

    public void setAddTime(long addTime)
    {
        this.addTime = addTime;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getPwd()
    {
        return pwd;
    }

    public int isExistData()
    {
        return existData;
    }

    public void setExistData(int existData)
    {
        this.existData = existData;
    }

    public void setPwd(String pwd)
    {
        this.pwd = pwd;
    }
    
    /** 商户号id*/
    @DatabaseField(columnName = UserInfoTable.COLUMN_M_ID)
    public String merchantId;
    
    @DatabaseField(columnName = UserInfoTable.COLUMN_M_NAME)
    public String merchantName;
    
    /**是否 true 管理账号  false 审核账号*/
    @DatabaseField(columnName = UserInfoTable.COLUMN_USER_OPTION)
    public int userOptionType;
    
    /**用户类型 普通用户 商户管理员*/
    @DatabaseField(columnName = UserInfoTable.COLUMN_USER_TYPE)
    public Integer userType;
    
    @DatabaseField(columnName = UserInfoTable.COLUMN_LOGIN_TIME)
    public long loginTime;
    
    /**设备ID */
    @DatabaseField(columnName = UserInfoTable.COLUMN_DERVICE_NO)
    public String derviceNo;
    
    @DatabaseField(columnName = UserInfoTable.COLUMN_ADD_TIME)
    public long addTime;
    
    public Integer merId;
    
    public String partent;
    
    public String key;
    
    // 省份证号码
    public String IDNumber;

    public String getIDNumber()
    {
        return IDNumber;
    }

    public void setIDNumber(String iDNumber)
    {
        IDNumber = iDNumber;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }
    
    /**设备是否 授权*/
    public boolean isMake;
    
    /**用户类型 普通用户 商户管理员 商户审核员 三种角色*/
    public UserRole role;
    
    public String email;

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }
    
    public String phone;
    
    public String ipAddr;

    public String isTotalAuth;

    public String telephone;

    public String getTelephone()
    {
        return telephone;
    }

    public void setTelephone(String telephone)
    {
        this.telephone = telephone;
    }

    public String getIsTotalAuth()
    {
        return isTotalAuth;
    }

    public void setIsTotalAuth(String isTotalAuth)
    {
        this.isTotalAuth = isTotalAuth;
    }
    
    //店铺ID
    public Integer shopId;

    public void initRole()
    {
        if (userType == 101 || userType == 102)
        {
            role = UserRole.approve;
        }
        else if (userType == 103)
        {
            role = UserRole.general;
        }
    }
    
    public boolean isNomal()
    {
        if (role == UserRole.general)
        {
            return true;
        }
        else if (role == UserRole.approve)
        {
            return false;
        }
        else if (role == UserRole.manager)
        {
            return false;
        }
        return false;
    }
    
}
