package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.GoodsMode
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.SettingMoreContract
import cn.swiftpass.enterprise.utils.JsonUtil
import com.example.common.entity.EmptyData
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @我们始终没有想出
 * @太阳却已悄悄安息
 */
class SettingMorePresenter : SettingMoreContract.Presenter {

    private var mView: SettingMoreContract.View? = null


    override fun logout() {
        mView?.let { view ->

            view.showLoading(R.string.public_loading, ParamsConstants.COMMON_LOADING)

            AppClient.logout(
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.logoutFailed(it.message)
                        }
                    }

                    override fun onResponse(response: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        view.logoutSuccess(true)
                    }
                }
            )
        }
    }

    override fun getVersionCode(isDisplayIcon: Boolean) {
//        mView?.let { view ->
//
//            view.showLoading(
//                R.string.show_new_version_loading,
//                ParamsConstants.COMMON_LOADING
//            )
//
//            AppClient.getVersionCode(
//                AppHelper.getUpdateVersionCode(MainApplication.getInstance()).toString(),
//                BuildConfig.bankCode,
//                System.currentTimeMillis().toString(),
//                object : CallBackUtil.CallBackCommonResponse<UpgradeInfo>(UpgradeInfo()) {
//
//                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
//                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
//                        commonResponse?.let {
//
//                            if (TextUtils.equals("400", it.result) || TextUtils.equals(
//                                    "402",
//                                    it.result
//                                )
//                            ) {
//                                view.getVersionCodeSuccess(null, isDisplayIcon)
//                            } else {
//                                view.getVersionCodeFailed(it.message)
//                            }
//                        }
//                    }
//
//                    override fun onResponse(r: CommonResponse?) {
//                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
//                        r?.let { response ->
//
//                            if (TextUtils.equals(response.result, "200")) {
//                                try {
//                                    val upgradeInfoStr = response.message
//                                    val info = UpgradeInfo()
//                                    val json = JSONObject(upgradeInfoStr)
//                                    info.dateTime =
//                                        Utils.Long.tryParse(json.optString("updated_at"), 0)
//                                    info.message = json.optString("subDesc", "")
//                                    info.version =
//                                        Utils.Integer.tryParse(json.optString("verCode"), 0)
//                                    info.mustUpgrade = Utils.Integer.tryParse(
//                                        json.optString("isUpdate"),
//                                        0
//                                    ) == 0
//                                    info.versionName = json.optString("verName")
//                                    info.fileMd5 = json.optString("fileMd5")
//                                    MainApplication.getInstance().setFileMd5(info.fileMd5)
//                                    info.url = json.optString("filePath", "")
//                                    MainApplication.getInstance().setFileMd5(info.fileMd5)
//                                    view.getVersionCodeSuccess(info, isDisplayIcon)
//                                } catch (e: Exception) {
//                                    uploadNetInterfaceException(
//                                        e,
//                                        MainApplication.getInstance().baseUrl + "spay/upgrade/checkinVersion",
//                                        e.toString(),
//                                        ""
//                                    )
//                                }
//                            }
//                        }
//                    }
//                }
//            )
//
//        }
    }

    override fun queryBody() {
        mView?.let { view ->
            AppClient.queryBody(
                MainApplication.getInstance().getMchId(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<GoodsMode>(GoodsMode()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        commonResponse?.let {
                            if (TextUtils.equals("405", it.result)) {
                                view.queryBodyFailed("405${it.message}")
                            } else {
                                view.queryBodyFailed(it.message)
                            }
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        r?.let { response ->
                            val goodsMode = JsonUtil.jsonToBean(
                                response.message,
                                GoodsMode::class.java
                            ) as GoodsMode
                            view.queryBodySuccess(goodsMode)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: SettingMoreContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}