package cn.swiftpass.enterprise.partyking.entity

import com.chad.library.adapter.base.entity.MultiItemEntity
import java.io.Serializable


/**
 * @author leon.zhao
 * @date 2022/06/02
 *
 *
 * type                           item 的类型 : 顶部/底部/中部(纵向/横向)
 * topTitle                       顶部样式 title
 * topIconType                    顶部样式图标
 * midTitle                       中部样式 title
 * midContent                     中部样式 value
 * midContentHorizontalColor      横向中部样式字体颜色
 * bottomType                     底部样式 ：含有蓝色按钮/含有白色按钮...
 * bottomGuideText                底部引导文字内容
 * bottomButtonText               底部按钮文字内容
 * bottomPageType                       页面类型 : 核销成功界面/核销失败界面/核销超时界面...
 */
data class VoucherInfoItemEntity(
    var type: Int = 0,
    var topTitle: String = "",
    var topIconType: Int = 0,
    var midTitle: String = "",
    var midContent: String = "",
    var midContentHorizontalColor: Int = 0,
    var bottomType: Int = 0,
    var bottomGuideText: String = "",
    var bottomButtonText: String = "",
    var bottomPageType: Int = 0
) : Serializable, MultiItemEntity {
    companion object {
        /**
         * item类型
         *
         * ITEM_TYPE_TOP              顶部样式
         * ITEM_TYPE_MID_HORIZONTAL   中间样式(横向)
         * ITEM_TYPE_MID_VERTICAL     中间样式(纵向)
         * ITEM_TYPE_BOTTOM           底部样式
         *
         */
        const val ITEM_TYPE_TOP = 1001
        const val ITEM_TYPE_MID_HORIZONTAL = 1002
        const val ITEM_TYPE_MID_VERTICAL = 1003
        const val ITEM_TYPE_BOTTOM = 1004

        /**
         * 顶部图标类型
         *
         * TOP_ICON_SUCCESS    核销成功
         * TOP_ICON_TIMEOUT    核销超时
         * TOP_ICON_FAIL       核销失败
         * TOP_ICON_ROLLBACK   撤回成功
         */
        const val TOP_ICON_SUCCESS = 2001
        const val TOP_ICON_TIMEOUT = 2002
        const val TOP_ICON_FAIL = 2003
        const val TOP_ICON_ROLLBACK = 2004


        /**
         * 内容横向样式 内容字体颜色
         */
        const val COLOR_MID_CONTENT_SUCCESS = 3001
        const val COLOR_MID_CONTENT_FAIL = 3002
        const val COLOR_MID_CONTENT_TIMEOUT = 3003


        /**
         * 底部样式
         *
         * BOTTOM_NO_BUTTON                无按钮(只有引导箭头)
         * BOTTOM_BLUE_BUTTON_AND_GUIDE    蓝色按钮 + 引导箭头
         * BOTTOM_WHITE_BUTTON_AND_GUIDE   白色按钮 + 引导箭头
         * BOTTOM_BLUE_BUTTON              蓝色按钮(无引导箭头)
         * BOTTOM_WHITE_BUTTON             白色按钮(无引导箭头)
         */
        const val BOTTOM_NO_BUTTON = 4001
        const val BOTTOM_BLUE_BUTTON_AND_GUIDE = 4002
        const val BOTTOM_WHITE_BUTTON_AND_GUIDE = 4003
        const val BOTTOM_BLUE_BUTTON = 4004
        const val BOTTOM_WHITE_BUTTON = 4005


        /**
         *
         * BOTTOM_PAGE_TYPE_WRITE_OFF_CONFIRM               核销确认
         * BOTTOM_PAGE_TYPE_WRITE_OFF_CAN_NOT_WRITE_OFF     不可核销
         * BOTTOM_PAGE_TYPE_WRITE_OFF_WRITE_OFF_SUCCESS     核销成功
         * BOTTOM_PAGE_TYPE_WRITE_OFF_WRITE_OFF_FAIL        核销失败
         * BOTTOM_PAGE_TYPE_WRITE_OFF_WRITE_OFF_TIMEOUT     核销超时
         * BOTTOM_PAGE_TYPE_ROLLBACK_SUCCESS                撤回成功
         * BOTTOM_PAGE_TYPE_WRITE_OFF_RECORD_SUCCESS        核销记录成功
         * BOTTOM_PAGE_TYPE_WRITE_OFF_RECORD_FAIL           核销记录失败
         * BOTTOM_PAGE_TYPE_WRITE_OFF_RECORD_TIMEOUT        核销记录超时
         *
         */
        const val BOTTOM_PAGE_TYPE_WRITE_OFF_CONFIRM = 5001
        const val BOTTOM_PAGE_TYPE_WRITE_OFF_CAN_NOT_WRITE_OFF = 5002
        const val BOTTOM_PAGE_TYPE_WRITE_OFF_WRITE_OFF_SUCCESS = 5003
        const val BOTTOM_PAGE_TYPE_WRITE_OFF_WRITE_OFF_FAIL = 5004
        const val BOTTOM_PAGE_TYPE_WRITE_OFF_WRITE_OFF_TIMEOUT = 5005
        const val BOTTOM_PAGE_TYPE_ROLLBACK_SUCCESS = 5006
        const val BOTTOM_PAGE_TYPE_WRITE_OFF_RECORD_SUCCESS = 5007
        const val BOTTOM_PAGE_TYPE_WRITE_OFF_RECORD_FAIL = 5008
        const val BOTTOM_PAGE_TYPE_WRITE_OFF_RECORD_TIMEOUT = 5009


    }

    override fun getItemType() = type
}
