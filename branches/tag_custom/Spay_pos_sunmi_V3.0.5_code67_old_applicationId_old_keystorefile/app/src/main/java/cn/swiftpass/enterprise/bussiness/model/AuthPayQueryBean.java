package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

public class AuthPayQueryBean implements Serializable {

    /**
     * authNo : 100570000058201812111076589676
     * tradeName :
     * body : mobile payment
     * outTradeNo : 100570000058274674098667775
     * mchCreateIp :
     * timeStart :
     * centerId :
     * tradeState : 2
     * transactionId :
     * orderNoMch : 100570000058201812111076589680
     * money :
     * totalFee :
     * subOpenID :
     * tradeTime :
     * notifyTime :
     * daMoney : 0
     * state :
     * need_query :
     */

    private String authNo;
    private String tradeName;
    private String body;
    private String outTradeNo;
    private String mchCreateIp;
    private String timeStart;
    private String centerId;
    private String tradeState;
    private String transactionId;
    private String orderNoMch;
    private String money;
    private String totalFee;
    private String subOpenID;
    private String tradeTime;
    private String notifyTime;
    private String daMoney;
    private String state;
    private String need_query;

    public String getAuthNo() {
        return authNo;
    }

    public void setAuthNo(String authNo) {
        this.authNo = authNo;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getMchCreateIp() {
        return mchCreateIp;
    }

    public void setMchCreateIp(String mchCreateIp) {
        this.mchCreateIp = mchCreateIp;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }

    public String getTradeState() {
        return tradeState;
    }

    public void setTradeState(String tradeState) {
        this.tradeState = tradeState;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getOrderNoMch() {
        return orderNoMch;
    }

    public void setOrderNoMch(String orderNoMch) {
        this.orderNoMch = orderNoMch;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getSubOpenID() {
        return subOpenID;
    }

    public void setSubOpenID(String subOpenID) {
        this.subOpenID = subOpenID;
    }

    public String getTradeTime() {
        return tradeTime;
    }

    public void setTradeTime(String tradeTime) {
        this.tradeTime = tradeTime;
    }

    public String getNotifyTime() {
        return notifyTime;
    }

    public void setNotifyTime(String notifyTime) {
        this.notifyTime = notifyTime;
    }

    public String getDaMoney() {
        return daMoney;
    }

    public void setDaMoney(String daMoney) {
        this.daMoney = daMoney;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getNeed_query() {
        return need_query;
    }

    public void setNeed_query(String need_query) {
        this.need_query = need_query;
    }
}
