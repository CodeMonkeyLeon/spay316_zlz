/*
 * 文 件 名:  UserManager.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-17
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.logica.user;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.GoodsMode;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.NetHelper;
import cn.swiftpass.enterprise.utils.AESHelper;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.MD5;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;
import cn.swiftpass.enterprise.utils.Utils;

/**
 * 注册，忘记密码，等操作业务类
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-17]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class UserManager {


    private final static String TAG = UserManager.class.getCanonicalName();

    // 注册url
    private static String registerUrl = ApiConstant.BASE_URL_PORT + ApiConstant.REGISTER;

    // 忘记密码检查 
    private static String chackData = ApiConstant.BASE_URL_PORT + "spay/user/setPasswordForEmail";

    // 重置修改密码 
    private static String updatePwd = ApiConstant.BASE_URL_PORT + ApiConstant.UPDATEPWD;

    // 注册验证码
    private static String registerCode = ApiConstant.BASE_URL_PORT + ApiConstant.REGISTERCODE;

    private static String bodyAdd = ApiConstant.BASE_URL_PORT + "spay/addTradeRemark";

    private static String bodyQuery = ApiConstant.BASE_URL_PORT + "spay/findTradeRemark";

    // 是否授权激活，是否完善资料
    private static String authorize = ApiConstant.BASE_URL_PORT + ApiConstant.AUTHORIZE;

    // 获取代理商电话
    private static String getAgentTel = ApiConstant.BASE_URL_PORT + ApiConstant.AGENTTEL;

//    private static String cashierAdd = ;

    //    private static String cashierQuery = ;

    //    private static String cashierDelete = ;

    private static String replacePhone = ApiConstant.BASE_URL_PORT + "spay/user/updatePhone";

    // 我要申请
    //private static String addApply = ApiConstant.BASE_URL_PORT + "agentApplyContact/AgentApplyContactAction_doAdd";

    /**
     * 注册
     * <功能详细描述>
     *
     * @param telNum    商户手机号码
     * @param shop_name 商户名称
     * @param code      验证码
     * @see [类、类#方法、类#成员]
     */
    public static void register(final String telNum, final String password, final String commit_password, final String requestcode, final String code, final UINotifyListener<UserModel> listener) {
        ThreadHelper.executeWithCallback(new Executable() {
            @Override
            public UserModel execute()

            {
                UserModel userModel = null;
                try {
                    JSONObject json = new JSONObject();
                    //                    json.put("clientType ", ApiConstant.SPAY);
                    // 为空，默认为手机号码
                    //                    if ("".equals(shop_name) || shop_name == null)
                    //                    {
                    //                        json.put("userName", telNum);
                    //                    }
                    //                    else
                    //                    {
                    //                        json.put("userName", shop_name);
                    //                    }

                    json.put("telephone", telNum);
                    json.put("phoneCode", code);
                    json.put("password", password);
                    json.put("repPassword", commit_password);
                    json.put("channelId", requestcode);

                    RequestResult result = NetHelper.httpsPost(registerUrl, json);
                    result.setNotifyListener(listener);
                    userModel = new UserModel();
                    if (!result.hasError()) {

                        if (result.data == null) {
                            return null;
                        }

                        int resCode = Integer.parseInt(result.data.getString("result"));

                        if (resCode == 200) {
                            //                            switch (resCode)
                            //                            {
                            //                                case 0:
                            //                                    paseJson(result.data.getString("userModel"), userModel);
                            //                                    LocalAccountManager.getInstance().setUser(userModel);
                            //                                    return userModel;
                            //                                    // 手机号码已注册
                            //                                case 1:
                            //                                    listener.onError("该手机号码已注册，请重新输入!");
                            //                                    return null;
                            //                                    //验证码错误
                            //                                case 2:
                            //                                    listener.onError("验证码输入错误，请重新输入!");
                            //                                    return null;
                            //                                case 4: // 验证码过期
                            //                                    listener.onError("验证码已过期，请重新获取验证码!");
                            //                                    return null;
                            //                                    
                            //                                default:
                            //                                    break;
                            //                            }
                            MainApplication.channelId = requestcode;
                            JSONObject jsonObject = new JSONObject(result.data.getString("message"));
                            userModel.merchantId = jsonObject.optString("mchId", "");
                            MainApplication.merchantId = userModel.merchantId;
                            MainApplication.remark = jsonObject.optString("remark", "");
                            MainApplication.phone = jsonObject.optString("phone", "");
                            MainApplication.isAdmin = "1";
                            userModel.setPhone(MainApplication.phone);
                            return userModel;
                        } else {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }

                    }

                } catch (JSONException e) {
                    listener.onError("注册失败，请稍后在试!!");
                    return null;
                }
                return null;

            }

        }, listener);
    }

    /**
     * 查询收银员列表
     * <功能详细描述>
     *
     * @param telNum    商户手机号码
     * @param shop_name 商户名称
     * @param code      验证码
     * @see [类、类#方法、类#成员]
     */
    public static void queryCashier(final int page, final int pageSize, final String input, final UINotifyListener<List<UserModel>> listener) {
        ThreadHelper.executeWithCallback(new Executable() {
            @Override
            public List<UserModel> execute()

            {
                try {
                    JSONObject json = new JSONObject();
                    Map<String, String> params = new HashMap<String, String>();

                    if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                        json.put("mchId", MainApplication.merchantId);
                        params.put("mchId", MainApplication.merchantId);
                    } else {
                        json.put("mchId", MainApplication.getMchId());
                        params.put("mchId", MainApplication.getMchId());

                    }
                    json.put("page", page);
                    params.put("page", page + "");
                    if (null != input) {
                        json.put("searchContent", input);
                        params.put("searchContent", input);
                    }
                    if (pageSize > 0) {

                        params.put("pageSize", pageSize + "");
                        json.put("pageSize", pageSize);
                    }
                    if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey())) {
                        json.put("sign", SignUtil.getInstance().createSign(params, MainApplication.getSignKey()));

                        //                        Log.i("hehui", "sign-->" + SignUtil.getInstance().createSign(params, MainApplication.signKey));
                    }

                    long spayRs = System.currentTimeMillis();
                    //                    //加签名
                    if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                        json.put("spayRs", spayRs);
                        json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                    }

                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/queryPageCashier", json, String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);

                    //                    Log.i("hehui", "queryCashier-->" + result.data);
                    if (!result.hasError()) {

                        if (result.data == null) {
                            return null;
                        }

                        int resCode = Integer.parseInt(result.data.getString("result"));

                        if (resCode == 200) {
                            JSONObject object = new JSONObject(result.data.getString("message"));

                            int pageCount = object.getInt("pageCount");
                            Gson gson = new Gson();
                            List<UserModel> userModels = gson.fromJson(object.getString("data"), new TypeToken<List<UserModel>>() {
                            }.getType());
                            if (userModels != null && userModels.size() > 0) {
                                UserModel userModel = userModels.get(0);
                                userModel.setPageCount(pageCount);
                                userModels.set(0, userModel);
                            } else {
                                listener.onSucceed(null);
                            }
                            return userModels;
                        } else {
                            listener.onError(result.data.getString("message"));
                        }
                    }

                } catch (JSONException e) {
                    listener.onError("收银员列表加载失败，请稍后在试!");
                    return null;
                }
                return null;

            }

        }, listener);
    }

    /**
     * 收银员删除
     * <功能详细描述>
     *
     * @param telNum    商户手机号码
     * @param shop_name 商户名称
     * @param code      验证码
     * @see [类、类#方法、类#成员]
     */
    public static void cashierDelete(final long userId, final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable() {
            @Override
            public Boolean execute()

            {
                try {
                    JSONObject json = new JSONObject();
                    Map<String, String> params = new HashMap<String, String>();
                    json.put("id", userId + "");
                    params.put("id", userId + "");

                    if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                        params.put("mchId", MainApplication.merchantId);
                        json.put("mchId", MainApplication.merchantId);
                    } else {
                        json.put("mchId", MainApplication.getMchId());
                        params.put("mchId", MainApplication.getMchId());
                    }

                    if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey())) {
                        json.put("sign", SignUtil.getInstance().createSign(params, MainApplication.getSignKey()));

                    }

                    long spayRs = System.currentTimeMillis();
                    //                    //加签名
                    if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                        json.put("spayRs", spayRs);
                        json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                    }

                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/deleteSpayUser", json, String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    if (!result.hasError()) {

                        if (result.data == null) {
                            return false;
                        }

                        int resCode = Integer.parseInt(result.data.getString("result"));

                        if (resCode == 200) {
                            return true;
                        } else {
                            listener.onError(result.data.getString("message"));
                        }
                    }

                } catch (JSONException e) {
                    listener.onError("添加收银员失败，请稍后在试!");
                    return false;
                }
                return false;

            }

        }, listener);
    }

    /**
     * 收银员添加
     * <功能详细描述>
     *
     * @param telNum    商户手机号码
     * @param shop_name 商户名称
     * @param code      验证码
     * @see [类、类#方法、类#成员]
     */
    public static void cashierAdd(final UserModel userModel, final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable() {
            @Override
            public Boolean execute()

            {
                try {
                    JSONObject json = new JSONObject();
                    Map<String, String> params = new HashMap<String, String>();

                    json.put("email", userModel.getPhone());

                    json.put("realname", userModel.getRealname());

                    //                    json.put("deptname", userModel.getDeptname());

                    json.put("isRefundAuth", userModel.getIsRefundAuth());

                    json.put("isOrderAuth", userModel.getIsOrderAuth());

                    json.put("enabled", userModel.getEnabled());

                    json.put("isTotalAuth", userModel.getIsTotalAuth());

                    //                    json.put("isActivityAuth", userModel.getIsActivityAuth() + "");

                    params.put("email", userModel.getPhone());

                    params.put("realname", userModel.getRealname());

                    json.put("username", userModel.getUserName());
                    long spayRs = System.currentTimeMillis();
                    String paseStr = MD5.md5s(spayRs + "2017SWIFTpassSPAYCCC").toUpperCase();
                    try {
                        json.put("password", AESHelper.aesEncrypt(userModel.getPassword(), paseStr.substring(8, 24)));
                    } catch (Exception e) {

                        Log.e(TAG,Log.getStackTraceString(e));

                    }
                    //                    params.put("username", userModel.getUserName());
                    //                    params.put("password", userModel.getPassword());

                    if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                        params.put("mchId", MainApplication.merchantId);
                        json.put("mchId", MainApplication.merchantId);
                    } else {
                        json.put("mchId", MainApplication.getMchId());
                        params.put("mchId", MainApplication.getMchId());
                    }

                    params.put("isRefundAuth", userModel.getIsRefundAuth());

                    params.put("isOrderAuth", userModel.getIsOrderAuth());
                    params.put("enabled", userModel.getEnabled() + "");
                    params.put("isTotalAuth", userModel.getIsTotalAuth() + "");
                    params.put("isActivityAuth", userModel.getIsActivityAuth() + "");

                    if (userModel.getId() != 0) {
                        json.put("id", userModel.getId() + "");
                        params.put("id", userModel.getId() + "");
                    }

                    if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey())) {
                        json.put("sign", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getSignKey()));

                        //                        json.put("sign", SignUtil.getInstance().createSign(params, MainApplication.signKey));

                    }

                    //                    //加签名
                    if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                        json.put("spayRs", spayRs);
                        json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                    }

                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.CASHIERADD, json, String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    if (!result.hasError()) {
                        if (result.data == null) {
                            return false;
                        }

                        int resCode = Integer.parseInt(result.data.getString("result"));

                        if (resCode == 200) {
                            return true;
                        } else {
                            listener.onError(result.data.getString("message"));
                        }
                    }

                } catch (JSONException e) {
                    listener.onError("添加收银员失败，请稍后在试!");
                    return false;
                }
                return false;

            }

        }, listener);
    }

    /**
     * 获取代理电话
     * <功能详细描述>
     *
     * @param telNum    商户手机号码
     * @param shop_name 商户名称
     * @param code      验证码
     * @see [类、类#方法、类#成员]
     */
    public static void getAgentPhone(final String city, final UINotifyListener<String> listener) {
        ThreadHelper.executeWithCallback(new Executable() {
            @Override
            public String execute()

            {
                try {
                    JSONObject json = new JSONObject();
                    json.put("clientType ", ApiConstant.SPAY_SUB);
                    // 为空，默认为手机号码

                    json.put("city", city);

                    RequestResult result = NetHelper.httpsPost(getAgentTel, json);
                    result.setNotifyListener(listener);
                    if (!result.hasError()) {

                        if (result.data == null) {
                            return null;
                        }

                        int resCode = Integer.parseInt(result.data.getString("result"));

                        if (result.resultCode == 0) {
                            switch (resCode) {
                                case 0:
                                    // 手机号码已注册

                                    String city = result.data.getString("city");
                                    return city;
                                case 11: // 验证码过期
                                    //                                    listener.onError("服务器内部错误，请稍后在试!");
                                    return null;

                                default:
                                    break;
                            }
                        } else {
                            //                            listener.onError("请求失败，请稍后在试!");
                            return null;
                        }

                    }

                } catch (JSONException e) {
                    listener.onError("注册失败，请稍后在试!!");
                    return null;
                }
                return null;

            }

        }, listener);
    }



//    /**
//     * 解析json
//     * <功能详细描述>
//     *
//     * @param string
//     * @throws JSONException
//     * @see [类、类#方法、类#成员]
//     */
//    private static void paseJson(String json, UserModel userModel) throws JSONException {
//        /** 根据json格式转成JSONObject对象 */
//
//        JSONObject jsonObject = new JSONObject(json);
//
//        userModel.id = Long.parseLong(jsonObject.optString("userId", "0"));
//        userModel.uId = Utils.Integer.tryParse(jsonObject.optString("userId", "0"), 0);
//
//        MainApplication.userId = Long.parseLong(jsonObject.optString("userId", "0"));
//        userModel.name = jsonObject.optString("userName", "");
//        // 注册成功把用户保存
//        MainApplication.userName = jsonObject.optString("userName", "");
//        userModel.pwd = jsonObject.optString("pwd", "");
//        //        userModel.realName = jsonObject.optString("realName", "");
//
//        userModel.merId = Utils.Integer.tryParse(jsonObject.optString("merId", "0"), 0);
//
//        userModel.userType = Utils.Integer.tryParse(jsonObject.getString("userType"), 103);
//
//        userModel.merchantId = jsonObject.optString("merchantId", "");
//        //        MainApplication.merchantId = jsonObject.optString("merchantId", "");
//        userModel.merchantName = jsonObject.optString("merchantName", "");
//
//        userModel.initRole();
//    }

    /**
     * <功能详细描述>
     *
     * @param code
     * @param listener
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static void replasePhone(final String oldPhone, final String newPhone, final String pwd, final String code, final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable() {

            @Override
            public Boolean execute() {
                JSONObject json = new JSONObject();
                try {
                    json.put("phoneCode", code);
                    json.put("telephone", oldPhone);
                    json.put("newTelephone", newPhone);
                    json.put("password", pwd);

                    RequestResult result = NetHelper.httpsPost(replacePhone, json);
                    result.setNotifyListener(listener);
                    if (!result.hasError()) {

                        if (result.data == null) {
                            return false;
                        }
                        int resCode = Integer.parseInt(result.data.getString("result"));
                        switch (resCode) {
                            case 200://成功
                                return true;
                            default:
                                listener.onError(result.data.getString("message"));
                                break;
                        }
                    }
                } catch (JSONException e) {

                    return false;
                }

                return false;
            }

        }, listener);

    }

    /**
     * 检查是商户还是手机号码
     * <功能详细描述>
     *
     * @param userModel
     * @param code
     * @param listener
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static void checkForgetPwdInput(final String inupt, final UINotifyListener<UserModel> listener) {
        ThreadHelper.executeWithCallback(new Executable() {

            @Override
            public UserModel execute() {
                JSONObject json = new JSONObject();
                try {
                    json.put("email", inupt);

                    long spayRs = System.currentTimeMillis();
                    //                    //加签名
                    json.put("spayRs", String.valueOf(spayRs));
                    json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(String.valueOf(spayRs))));

                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/checkForgetPwdInput", json, String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    if (!result.hasError()) {

                        if (result.data == null) {
                            return null;
                        }
                        int resCode = Integer.parseInt(result.data.getString("result"));

                        switch (resCode) {
                            case 200://成功
                                return (UserModel) JsonUtil.jsonToBean(result.data.getString("message"), UserModel.class);
                            default:
                                listener.onError(result.data.getString("message"));
                                return null;
                        }
                    } else {

                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return null;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return null;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return null;
                        }
                        listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                    }
                    return null;
                } catch (JSONException e) {

                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                    return null;
                }

            }

        }, listener);

    }

    /**
     * 检查验证码是否正确
     * <功能详细描述>
     *
     * @param userModel
     * @param code
     * @param listener
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static void checkPhoneCode(final String telephone, final String code, final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable() {

            @Override
            public Boolean execute() {
                JSONObject json = new JSONObject();
                try {
                    json.put("email", telephone);
                    json.put("emailCode", code);

                    long spayRs = System.currentTimeMillis();
                    //                    //加签名
                    json.put("spayRs", String.valueOf(spayRs));
                    json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(String.valueOf(spayRs))));


                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/checkEmailCode", json, String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    if (!result.hasError()) {

                        if (result.data == null) {
                            return false;
                        }
                        int resCode = Integer.parseInt(result.data.getString("result"));
                        switch (resCode) {
                            case 200://成功
                                return true;
                            default:
                                result.returnErrorMessage();
                                break;
                        }
                    }
                } catch (JSONException e) {

                    return false;
                }

                return null;
            }

        }, listener);

    }

    /**
     * 忘记密码检查
     * <功能详细描述>
     *
     * @param userModel
     * @param code
     * @param listener
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static void checkData(final UserModel userModel, final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable() {

            @Override
            public Boolean execute() throws Exception {
                JSONObject json = new JSONObject();
                try {
                    long spayRs = System.currentTimeMillis();
                    String paseStr = MD5.md5s(spayRs + "2017SWIFTpassSPAYCCC").toUpperCase();
                    json.put("password", AESHelper.aesEncrypt(userModel.getIDNumber(), paseStr.substring(8, 24)));
                    json.put("repPassword", AESHelper.aesEncrypt(userModel.getIDNumber(), paseStr.substring(8, 24)));
                    json.put("emailCode", userModel.getEmailCode());
                    if (userModel.getIsMch() == 1) {
                        json.put("mchId", userModel.getTel());
                        //                        json.put("telephone", userModel.getTelephone());
                    }
                    json.put("email", userModel.getEmail());

                    //                    //加签名
                    json.put("spayRs", String.valueOf(spayRs));
                    json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(String.valueOf(spayRs))));

                    RequestResult result = NetHelper.httpsPost(chackData, json, String.valueOf(spayRs), null);

                    result.setNotifyListener(listener);
                    if (!result.hasError()) {

                        if (result.data == null) {
                            return false;
                        }
                        int resCode = Integer.parseInt(result.data.getString("result"));
                        switch (resCode) {
                            case 200://成功
                                return true;
                            default:
                                result.returnErrorMessage();
                                break;
                        }
                    }
                } catch (JSONException e) {

                    return false;
                }

                return null;
            }

        }, listener);

    }

    /**
     * 是否授权
     * <功能详细描述>
     *
     * @param userId   用户id
     * @param listener
     * @param dataType 0为授权激活，1为资料完善
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static void existAuth(final Long userId, final int dataType, final UINotifyListener<Integer> listener) {
        ThreadHelper.executeWithCallback(new Executable() {

            @Override
            public Integer execute()

            {
                JSONObject json = new JSONObject();
                int resCode = 0;
                try {
                    json.put("clientType ", ApiConstant.SPAY);
                    json.put("userId", String.valueOf(userId));
                    json.put("dataType", String.valueOf(dataType));


                    RequestResult result = NetHelper.httpsPost(authorize, json);

                    switch (result.resultCode) {
                        case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                            listener.onError("网络连接不可用，请检查你网络连接!");
                            return RequestResult.RESULT_BAD_NETWORK;
                        case RequestResult.RESULT_TIMEOUT_ERROR:
                            listener.onError("请求连接超时，请稍候再试!");
                            return RequestResult.RESULT_TIMEOUT_ERROR;
                        case RequestResult.RESULT_READING_ERROR:
                            listener.onError("请求服务连接超时，请稍候再试!");
                            return RequestResult.RESULT_READING_ERROR;
                    }

                    if (result.data != null) {
                        resCode = Integer.parseInt(result.data.getString("result"));
                    }
                } catch (JSONException e) {

                    return 11;
                }

                return resCode;
            }

        }, listener);
    }

    /**
     * 查询设备列表
     * <功能详细描述>
     *
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public static void queryDeviceList(final UINotifyListener<List<String>> listener) {
        ThreadHelper.executeWithCallback(new Executable() {

            @Override
            public List<String> execute() throws Exception {
                JSONObject json = new JSONObject();

                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);
                } else {
                    json.put("mchId", MainApplication.getMchId());
                }

                long spayRs = System.currentTimeMillis();
                //                    //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                    json.put("spayRs", spayRs);
                    json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                    json.remove("spayRs");
                }

                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/findEquipmentNumberList", json, String.valueOf(spayRs), null);
                result.setNotifyListener(listener);
                if (!result.hasError()) {

                    if (result.data == null) {
                        return null;
                    }
                    int resCode = Integer.parseInt(result.data.getString("result"));

                    if (resCode == 200) {
                        List<String> deviceList = new ArrayList<String>();

                        //JSONObject jsonObject = new JSONObject(result.data.getString("message"));
                        return deviceList;
                    } else {
                        listener.onError(result.data.getString("message"));
                    }

                }
                return null;
            }

        }, listener);

    }

    /**
     * 修改增加商品名称
     * <功能详细描述>
     *
     * @param telNumber
     * @param dataType
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public static void queryBody(final UINotifyListener<GoodsMode> listener) {
        ThreadHelper.executeWithCallback(new Executable() {

            @Override
            public GoodsMode execute() throws Exception {
                JSONObject json = new JSONObject();

                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);
                } else {
                    json.put("mchId", MainApplication.getMchId());
                }

                long spayRs = System.currentTimeMillis();
                //                    //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                    json.put("spayRs", spayRs);
                    json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                }

                GoodsMode goodsMode = new GoodsMode();
                RequestResult result = NetHelper.httpsPost(bodyQuery, json, String.valueOf(spayRs), null);
                result.setNotifyListener(listener);
                if (!result.hasError()) {

                    if (result.data == null) {
                        return null;
                    }
                    int resCode = Integer.parseInt(result.data.getString("result"));

                    if (resCode == 200) {
                        JSONObject jsonObject = new JSONObject(result.data.getString("message"));
                        goodsMode.setBody(jsonObject.optString("commodityName", ""));
                        goodsMode.setCount(jsonObject.optString("commodityNum", ""));
                        return goodsMode;
                    } else {
                        listener.onError(result.data.getString("message"));
                    }

                } else {

                    switch (result.resultCode) {
                        case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                            listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                        case RequestResult.RESULT_TIMEOUT_ERROR:
                            listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                        case RequestResult.RESULT_READING_ERROR:
                            listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                    }
                }
                return null;
            }

        }, listener);

    }

    /**
     * 查询设备号
     * <功能详细描述>
     *
     * @param telNumber
     * @param dataType
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public static void queryMchDevice(final UINotifyListener<String> listener) {
        ThreadHelper.executeWithCallback(new Executable() {

            @Override
            public String execute() throws Exception {
                JSONObject json = new JSONObject();

                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);
                } else {
                    json.put("mchId", MainApplication.getMchId());
                }

                long spayRs = System.currentTimeMillis();
                //                    //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                    json.put("spayRs", String.valueOf(spayRs));
                    json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                }

                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/findEquipmentNumber", json, String.valueOf(spayRs), null);
                result.setNotifyListener(listener);
                if (!result.hasError()) {

                    if (result.data == null) {
                        return null;
                    }
                    int resCode = Integer.parseInt(result.data.getString("result"));

                    if (resCode == 200) {
                        JSONObject jsonObject = new JSONObject(result.data.getString("message"));
                        return jsonObject.optString("equipmentCode", "");
                    } else {
                        listener.onError(result.data.getString("message"));
                    }

                } else {

                    switch (result.resultCode) {
                        case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                            listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                        case RequestResult.RESULT_TIMEOUT_ERROR:
                            listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                        case RequestResult.RESULT_READING_ERROR:
                            listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                    }
                }
                return null;
            }

        }, listener);

    }

    /**
     * 设备号添加和修改
     * <功能详细描述>
     *
     * @param body
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public static void updateOrAddDevice(final String device, final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable() {

            @Override
            public Boolean execute() throws Exception {
                JSONObject json = new JSONObject();

                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);
                } else {
                    json.put("mchId", MainApplication.getMchId());
                }
                json.put("equipmentCode", device);

                long spayRs = System.currentTimeMillis();
                //                    //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                    json.put("spayRs", spayRs);
                    json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                }

                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/addEquipmentNumber", json);
                result.setNotifyListener(listener);
                if (!result.hasError()) {

                    if (result.data == null) {
                        return false;
                    }
                    int resCode = Integer.parseInt(result.data.getString("result"));

                    if (resCode == 200) {
                        return true;
                    } else {
                        listener.onError(result.data.getString("message"));
                    }
                    return false;

                } else {

                    switch (result.resultCode) {
                        case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                            listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                        case RequestResult.RESULT_TIMEOUT_ERROR:
                            listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                        case RequestResult.RESULT_READING_ERROR:
                            listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                    }
                }
                return false;
            }

        }, listener);

    }

    /**
     * 修改增加商品名称
     * <功能详细描述>
     *
     * @param telNumber
     * @param dataType
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public static void updateOrAddBody(final String body, final String count, final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable() {

            @Override
            public Boolean execute() throws Exception {
                JSONObject json = new JSONObject();

                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);
                } else {
                    json.put("mchId", MainApplication.getMchId());
                }
                json.put("commodityName", body);
                json.put("commodityNum", count);
                long spayRs = System.currentTimeMillis();
                //                    //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                    json.put("spayRs", spayRs);
                    json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                }

                RequestResult result = NetHelper.httpsPost(bodyAdd, json, String.valueOf(spayRs), null);
                result.setNotifyListener(listener);
                if (!result.hasError()) {

                    if (result.data == null) {
                        return false;
                    }
                    int resCode = Integer.parseInt(result.data.getString("result"));

                    if (resCode == 200) {
                        return true;
                    } else {
                        listener.onError(result.data.getString("message"));
                    }
                    return false;

                }
                switch (result.resultCode) {
                    case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                        listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                    case RequestResult.RESULT_TIMEOUT_ERROR:
                        listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                    case RequestResult.RESULT_READING_ERROR:
                        listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                }
                return false;
            }

        }, listener);

    }

    /**
     * 获取验证码
     * <功能详细描述>
     *
     * @param telNumber 手机号码
     * @param listener
     * @param dataType  1、注册，2、忘记密码 等获取密码
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static void getCode(final String mch, final String telNumber, final UINotifyListener<String> listener) {
        ThreadHelper.executeWithCallback(new Executable() {

            @Override
            public String execute() throws Exception {
                JSONObject json = new JSONObject();
                //                json.put("clientType ", ApiConstant.SPAY);
                json.put("email", mch);
                //                if (!StringUtil.isEmptyOrNull(telNumber))
                //                {
                //                    
                //                    json.put("last4Tel", telNumber);
                //                }

                long spayRs = System.currentTimeMillis();
                //                    //加签名
                json.put("spayRs", String.valueOf(spayRs));
                json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(String.valueOf(spayRs))));


                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/getEmailCode", json, String.valueOf(spayRs), null);
                //                result.setNotifyListener(listener);
                if (!result.hasError()) {
                    if (result.data == null) {
                        return null;
                    }
                    int resCode = Integer.parseInt(result.data.getString("result"));

                    switch (resCode) {
                        case 200:

                            JSONObject object = new JSONObject(result.data.optString("message", ""));
                            String tel = object.optString("telephone", "");
                            if (StringUtil.isEmptyOrNull(tel)) {
                                return object.optString("email", "");
                            }
                            return tel;
                        default:
                            listener.onError(result.data.getString("message"));
                            break;
                    }
                    return null;

                } else {
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }
                return null;
            }

        }, listener);

    }

    /**
     * 获取验证码
     * <功能详细描述>
     *
     * @param telNumber 手机号码
     * @param listener
     * @param dataType  1、注册，2、忘记密码 等获取密码
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static void getRegisterCode(final String telNumber, final String dataType, final UINotifyListener<Integer> listener) {
        ThreadHelper.executeWithCallback(new Executable() {

            @Override
            public Integer execute() throws Exception {
                JSONObject json = new JSONObject();
                //                json.put("clientType ", ApiConstant.SPAY);
                json.put("telephone", telNumber);
                json.put("key", dataType);
                //                json.put("dataType", dataType);

                RequestResult result = NetHelper.httpsPost(registerCode, json);
                result.setNotifyListener(listener);
                if (!result.hasError()) {
                    if (result.data == null) {
                        return 2;
                    }
                    int resCode = Integer.parseInt(result.data.getString("result"));
                    //                    switch (resCode)
                    //                    {
                    // 成功

                    switch (resCode) {
                        case 200:
                            return 0;

                        default:
                            listener.onError(result.data.getString("message"));
                            break;
                    }
                    return 2;

                }
                return 2;
            }

        }, listener);

    }

    /**
     * 重置修改密码
     * <功能详细描述>
     *
     * @param oldPwd   旧密码
     * @param newPwd   新密码
     * @param listener
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static void updatePdw(final String oldPwd, final String userId, final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable() {

            @Override
            public Boolean execute() throws Exception {
                JSONObject json = new JSONObject();
                json.put("clientType ", ApiConstant.SPAY);
                json.put("password", oldPwd);
                json.put("userId", userId);

                RequestResult result = NetHelper.httpsPost(updatePwd, json);
                result.setNotifyListener(listener);
                if (!result.hasError()) {

                    if (result.data == null) {
                        listener.onError("修改密码失败!");
                        return false;
                    }

                    int code = Integer.parseInt(result.data.getString("result"));
                    switch (code) {
                        case 0: // 成功
                            listener.onError("重置密码成功!");
                            return true;
                        case 1: // 失败
                            listener.onError("修改密码失败!");
                            return false;
                        case RequestResult.RESULT_TIMEOUT_ERROR: // 请求超时
                            listener.onError("连接超时，请稍微再试!");
                            return false;

                        default:
                            break;
                    }

                }

                return false;
            }

        }, listener);

    }
}
