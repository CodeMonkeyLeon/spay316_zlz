package cn.swiftpass.enterprise.partyking.view

import android.app.Activity
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.partyking.adapter.PartyKingSummaryAdapter
import cn.swiftpass.enterprise.partyking.entity.PtkSummaryItemEntity
import cn.swiftpass.enterprise.partyking.interfaces.OnPtkSummaryItemClickListener
import cn.swiftpass.enterprise.partyking.view.refresh.PtkRefreshFooter
import cn.swiftpass.enterprise.partyking.view.refresh.PtkRefreshHeader
import cn.swiftpass.enterprise.ui.activity.TemplateActivity
import cn.swiftpass.enterprise.ui.widget.SelectDatePopupWindow
import cn.swiftpass.enterprise.ui.widget.TitleBar
import cn.swiftpass.enterprise.utils.ActivitySkipUtil
import cn.swiftpass.enterprise.utils.DateUtil
import cn.swiftpass.enterprise.utils.KotlinUtils
import cn.swiftpass.enterprise.utils.Logger
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener
import java.util.*

class PartyKingSummaryActivity : TemplateActivity(), View.OnClickListener {

    private fun getLayoutId() = R.layout.act_ptk_summary


    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mAdapter: PartyKingSummaryAdapter
    private val mList = mutableListOf<PtkSummaryItemEntity>()
    private lateinit var mLinearLayoutDateChoose: LinearLayout
    private lateinit var mLinearLayoutStatusChoose: LinearLayout
    private lateinit var mSmartRefreshLayout: SmartRefreshLayout
    private lateinit var mPtkRefreshHeader: PtkRefreshHeader
    private lateinit var mPtkRefreshFooter: PtkRefreshFooter


    private lateinit var mTvTime: TextView
    private var mTime: String = ""
    private lateinit var mSelectDatePopupWindow: SelectDatePopupWindow


    companion object {
        const val TAG = "PtkSummaryActivity"
        const val PTK_SUMMARY_DATA = "PTK_SUMMARY_DATA"


        fun startPtkSummaryActivity(fromActivity: Activity, data: ArrayList<PtkSummaryItemEntity>) {
            val hashMap = HashMap<String, ArrayList<PtkSummaryItemEntity>>()
            hashMap[PTK_SUMMARY_DATA] = data
            ActivitySkipUtil.startAnotherActivity(
                fromActivity,
                PartyKingSummaryActivity::class.java,
                hashMap
            )
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        mList.clear()
        intent?.let {
            val list = it.getSerializableExtra(PTK_SUMMARY_DATA) as ArrayList<PtkSummaryItemEntity>
            mList.addAll(list)
        }
        initView()
    }


    fun initView() {
        mRecyclerView = findViewById(R.id.id_recycle_view_ptk_summary)
        mSmartRefreshLayout = findViewById(R.id.id_ptk_summary_smart_refresh)
        mPtkRefreshHeader = findViewById(R.id.id_ptk_summary_refresh_header)
        mPtkRefreshFooter = findViewById(R.id.id_ptk_summary_refresh_footer)
        mLinearLayoutDateChoose = findViewById(R.id.ll_summary_choice_date)
        mLinearLayoutStatusChoose = findViewById(R.id.ll_summary_choice_status)
        mTvTime = findViewById(R.id.tv_time)


        mLinearLayoutDateChoose.setOnClickListener(this)
        mLinearLayoutStatusChoose.setOnClickListener(this)

        initRecyclerView()
        initRefreshLayout()
    }


    /**
     * 初始化 RecyclerView
     */
    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(activity)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        mRecyclerView.layoutManager = layoutManager

        mAdapter = PartyKingSummaryAdapter(activity, mList)
        mAdapter.setOnPtkSummaryItemClickListener(object : OnPtkSummaryItemClickListener {
            override fun onPtkSummaryItemClick(view: View?, position: Int) {
                Logger.d(TAG, "---position : $position 点击了 ptk summary item")
            }
        })
        mRecyclerView.adapter = mAdapter
    }


    /**
     * 初始化刷新布局
     */
    private fun initRefreshLayout() {
        mPtkRefreshHeader.setRefreshText(
            getString(R.string.drop_dowm),
            getString(R.string.release_update),
            getString(R.string.loading)
        )

//        mPtkRefreshFooter.setLoadText(
//            getString()
//        )


        mSmartRefreshLayout.setOnRefreshLoadMoreListener(object : OnRefreshLoadMoreListener {
            override fun onRefresh(refreshLayout: RefreshLayout) {
                loadDialog(activity, getString(R.string.public_data_loading))
                //下拉刷新
                Handler().postDelayed(Runnable {
                    dismissLoading()
                    Logger.d(TAG, "ptk 下拉刷新1s后 finish()")
                    mSmartRefreshLayout.finishRefresh()
                }, 1000)
            }

            override fun onLoadMore(refreshLayout: RefreshLayout) {
                //上拉加载
                loadDialog(activity, getString(R.string.public_data_loading))
                Handler().postDelayed(Runnable {
                    dismissLoading()
                    Logger.d(TAG, "ptk 上拉加载1s后 finish()")
                    mSmartRefreshLayout.finishLoadMore()
                }, 1000)
            }
        })
    }


    override fun onClick(v: View?) {
        v?.let { view ->
            when (view.id) {
                R.id.ll_summary_choice_date -> {
                    //时间筛选
                    Logger.d(TAG, "--- 点击了 summary date")
                    clickSelectData(mLinearLayoutDateChoose)
                }
                R.id.ll_summary_choice_status -> {
                    //状态筛选
                    Logger.d(TAG, "--- 点击了 summary status")
                }

            }
        }

    }


    /**
     * 点击时间 进行选择
     */
    private fun clickSelectData(view: View) {
        mSelectDatePopupWindow =
            SelectDatePopupWindow(activity, false, mTime) { time ->
                var tempTime = time
                if (time == 0L) {
                    //SelectDatePopupWindow的 time==0
                    if (TextUtils.isEmpty(mTime)) {
                        //mTime为空
                        tempTime = System.currentTimeMillis()
                    } else {
                        //mTime不空
                        if (KotlinUtils.isToday(mTime)) {
                            //日期为今天
                            mTvTime.text = getString(R.string.tx_today)
                            mTvTime.textSize = 16f
                        } else {
                            //日期不是今天
                            mTvTime.text = mTime
                            mTvTime.textSize = 16f
                        }
                        loadData()
                    }
                }


                val nowTime = System.currentTimeMillis()
                if (tempTime > nowTime) {
                    //选择的日期不能超过今天
                    toastDialog(activity, R.string.tx_date, null)
                    return@SelectDatePopupWindow
                }

                val calendar = Calendar.getInstance()
                calendar.add(Calendar.DAY_OF_MONTH, -90)

                if (tempTime <= calendar.timeInMillis) {
                    //选择时间不能超过90天
                    toastDialog(activity, R.string.tx_choice_date, null)
                    return@SelectDatePopupWindow
                }

                setTime(tempTime)
                mTime = DateUtil.formatYYMD(time)
                loadData()
            }


        mSelectDatePopupWindow.showAtLocation(
            view,
            Gravity.BOTTOM or Gravity.CENTER_HORIZONTAL,
            0,
            0
        )
    }


    /**
     * 模拟加载数据
     */
    private fun loadData() {
        loadDialog(activity, getString(R.string.public_data_loading))
        Handler().postDelayed(Runnable {
            dismissLoading()
            mSelectDatePopupWindow?.let {
                it.dismiss()
            }
        }, 1000)
    }


    /**
     * 设置条目栏中的时间显示内容
     */
    private fun setTime(time: Long) {
        val newDate = DateUtil.formatYYMD(System.currentTimeMillis())
        val date = DateUtil.formatYYMD(time)
        val arr1 = newDate.split("-").toTypedArray()
        val arr2 = date.split("-").toTypedArray()
        if (arr1[0] == arr2[0] && arr1[1] == arr2[1] && arr1[2] == arr2[2]) {
            mTvTime.text = getString(R.string.tx_today)
            mTvTime.textSize = 16f
        } else {
            mTvTime.text = DateUtil.formatYYMD(time)
            mTvTime.textSize = 14f
        }
    }


    override fun setupTitleBar() {
        super.setupTitleBar()
        titleBar.setLeftButtonVisible(true)
        titleBar.setRightButLayVisible(false, "")
        titleBar.setTitle(getString(R.string.string_pk_summary_title))
        titleBar.setOnTitleBarClickListener(object : TitleBar.OnTitleBarClickListener {
            override fun onLeftButtonClick() {
                //返回
                finish()
            }

            override fun onRightButtonClick() {

            }

            override fun onRightLayClick() {

            }

            override fun onRightButLayClick() {

            }

        })
    }
}