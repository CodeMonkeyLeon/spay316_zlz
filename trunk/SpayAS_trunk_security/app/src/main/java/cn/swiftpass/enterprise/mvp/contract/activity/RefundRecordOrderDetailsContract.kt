package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @当你的情人已改名玛丽
 * @你怎能送她一首菩萨蛮
 */
class RefundRecordOrderDetailsContract {


    interface View : BaseView {

        fun queryRefundDetailSuccess(response: Order?)

        fun queryRefundDetailFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {

        fun queryRefundDetail(
            orderNo: String?,
            mchId: String?
        )

    }


}