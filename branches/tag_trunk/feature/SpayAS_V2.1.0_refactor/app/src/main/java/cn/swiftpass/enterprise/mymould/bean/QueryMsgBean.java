package cn.swiftpass.enterprise.mymould.bean;

import java.util.List;

public class QueryMsgBean{
    private int currentPage;
    private int perPage;
    private int pageCount;
    private int totalRows;
    private String reqFeqTime;
    private List<DataBean> data;

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public String getReqFeqTime() {
        return reqFeqTime;
    }

    public void setReqFeqTime(String reqFeqTime) {
        this.reqFeqTime = reqFeqTime;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 30010296
         * empId :
         * username : 1357924680
         * realname : OliviaJiang06
         * deptname :
         * remark :
         * isAdmin :
         * enabled : true
         * mchId :
         * isSysAdmin :
         * isRefundAuth : 0
         * isOrderAuth : 0
         * isTotalAuth : 0
         * isActivityAuth : 0
         * showEwallet :
         * mchName :
         * mchLogo :
         * tradeName :
         * serviceType :
         * signKey :
         * cookieMap :
         * roleType :
         * delete : false
         * bankCode :
         * feeType :
         * feeFh :
         * merchantShortName :
         * themeMd5 :
         * apiShowListMd5 :
         * apiProviderMap : {}
         * refundStateMap : {}
         * tradeStateMap : {}
         * payTypeMap : {}
         * isSurchargeOpen : 0
         * isTaxRateOpen : 0
         * isTipOpen : 0
         * email : yumeng.jiang@swiftpass.cn
         * timeZone :
         * rate :
         * isFixCode :
         * rateTime :
         * surchargeRate :
         * taxRate :
         * vatRate :
         * numFixed :
         * usdToRmbExchangeRate :
         * sourceToUsdExchangeRate :
         * isCardOpen : 0
         * alipayPayRate :
         * token :
         * appId :
         * machineId :
         * createdAt :
         */

        private String id;
        private String empId;
        private String username;
        private String realname;
        private String deptname;
        private String remark;
        private String isAdmin;
        private boolean enabled;
        private String mchId;
        private String isSysAdmin;
        private String isRefundAuth;
        private String isOrderAuth;
        private String isTotalAuth;
        private String isActivityAuth;
        private String showEwallet;
        private String mchName;
        private String mchLogo;
        private String tradeName;
        private String serviceType;
        private String signKey;
        private String cookieMap;
        private String roleType;
        private boolean delete;
        private String bankCode;
        private String feeType;
        private String feeFh;
        private String merchantShortName;
        private String themeMd5;
        private String apiShowListMd5;
        private ApiProviderMapBean apiProviderMap;
        private RefundStateMapBean refundStateMap;
        private TradeStateMapBean tradeStateMap;
        private PayTypeMapBean payTypeMap;
        private int isSurchargeOpen;
        private int isTaxRateOpen;
        private int isTipOpen;
        private String email;
        private String timeZone;
        private String rate;
        private String isFixCode;
        private String rateTime;
        private String surchargeRate;
        private String taxRate;
        private String vatRate;
        private String numFixed;
        private String usdToRmbExchangeRate;
        private String sourceToUsdExchangeRate;
        private int isCardOpen;
        private String alipayPayRate;
        private String token;
        private String appId;
        private String machineId;
        private String createdAt;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getEmpId() {
            return empId;
        }

        public void setEmpId(String empId) {
            this.empId = empId;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getRealname() {
            return realname;
        }

        public void setRealname(String realname) {
            this.realname = realname;
        }

        public String getDeptname() {
            return deptname;
        }

        public void setDeptname(String deptname) {
            this.deptname = deptname;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getIsAdmin() {
            return isAdmin;
        }

        public void setIsAdmin(String isAdmin) {
            this.isAdmin = isAdmin;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public String getMchId() {
            return mchId;
        }

        public void setMchId(String mchId) {
            this.mchId = mchId;
        }

        public String getIsSysAdmin() {
            return isSysAdmin;
        }

        public void setIsSysAdmin(String isSysAdmin) {
            this.isSysAdmin = isSysAdmin;
        }

        public String getIsRefundAuth() {
            return isRefundAuth;
        }

        public void setIsRefundAuth(String isRefundAuth) {
            this.isRefundAuth = isRefundAuth;
        }

        public String getIsOrderAuth() {
            return isOrderAuth;
        }

        public void setIsOrderAuth(String isOrderAuth) {
            this.isOrderAuth = isOrderAuth;
        }

        public String getIsTotalAuth() {
            return isTotalAuth;
        }

        public void setIsTotalAuth(String isTotalAuth) {
            this.isTotalAuth = isTotalAuth;
        }

        public String getIsActivityAuth() {
            return isActivityAuth;
        }

        public void setIsActivityAuth(String isActivityAuth) {
            this.isActivityAuth = isActivityAuth;
        }

        public String getShowEwallet() {
            return showEwallet;
        }

        public void setShowEwallet(String showEwallet) {
            this.showEwallet = showEwallet;
        }

        public String getMchName() {
            return mchName;
        }

        public void setMchName(String mchName) {
            this.mchName = mchName;
        }

        public String getMchLogo() {
            return mchLogo;
        }

        public void setMchLogo(String mchLogo) {
            this.mchLogo = mchLogo;
        }

        public String getTradeName() {
            return tradeName;
        }

        public void setTradeName(String tradeName) {
            this.tradeName = tradeName;
        }

        public String getServiceType() {
            return serviceType;
        }

        public void setServiceType(String serviceType) {
            this.serviceType = serviceType;
        }

        public String getSignKey() {
            return signKey;
        }

        public void setSignKey(String signKey) {
            this.signKey = signKey;
        }

        public String getCookieMap() {
            return cookieMap;
        }

        public void setCookieMap(String cookieMap) {
            this.cookieMap = cookieMap;
        }

        public String getRoleType() {
            return roleType;
        }

        public void setRoleType(String roleType) {
            this.roleType = roleType;
        }

        public boolean isDelete() {
            return delete;
        }

        public void setDelete(boolean delete) {
            this.delete = delete;
        }

        public String getBankCode() {
            return bankCode;
        }

        public void setBankCode(String bankCode) {
            this.bankCode = bankCode;
        }

        public String getFeeType() {
            return feeType;
        }

        public void setFeeType(String feeType) {
            this.feeType = feeType;
        }

        public String getFeeFh() {
            return feeFh;
        }

        public void setFeeFh(String feeFh) {
            this.feeFh = feeFh;
        }

        public String getMerchantShortName() {
            return merchantShortName;
        }

        public void setMerchantShortName(String merchantShortName) {
            this.merchantShortName = merchantShortName;
        }

        public String getThemeMd5() {
            return themeMd5;
        }

        public void setThemeMd5(String themeMd5) {
            this.themeMd5 = themeMd5;
        }

        public String getApiShowListMd5() {
            return apiShowListMd5;
        }

        public void setApiShowListMd5(String apiShowListMd5) {
            this.apiShowListMd5 = apiShowListMd5;
        }

        public ApiProviderMapBean getApiProviderMap() {
            return apiProviderMap;
        }

        public void setApiProviderMap(ApiProviderMapBean apiProviderMap) {
            this.apiProviderMap = apiProviderMap;
        }

        public RefundStateMapBean getRefundStateMap() {
            return refundStateMap;
        }

        public void setRefundStateMap(RefundStateMapBean refundStateMap) {
            this.refundStateMap = refundStateMap;
        }

        public TradeStateMapBean getTradeStateMap() {
            return tradeStateMap;
        }

        public void setTradeStateMap(TradeStateMapBean tradeStateMap) {
            this.tradeStateMap = tradeStateMap;
        }

        public PayTypeMapBean getPayTypeMap() {
            return payTypeMap;
        }

        public void setPayTypeMap(PayTypeMapBean payTypeMap) {
            this.payTypeMap = payTypeMap;
        }

        public int getIsSurchargeOpen() {
            return isSurchargeOpen;
        }

        public void setIsSurchargeOpen(int isSurchargeOpen) {
            this.isSurchargeOpen = isSurchargeOpen;
        }

        public int getIsTaxRateOpen() {
            return isTaxRateOpen;
        }

        public void setIsTaxRateOpen(int isTaxRateOpen) {
            this.isTaxRateOpen = isTaxRateOpen;
        }

        public int getIsTipOpen() {
            return isTipOpen;
        }

        public void setIsTipOpen(int isTipOpen) {
            this.isTipOpen = isTipOpen;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getTimeZone() {
            return timeZone;
        }

        public void setTimeZone(String timeZone) {
            this.timeZone = timeZone;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public String getIsFixCode() {
            return isFixCode;
        }

        public void setIsFixCode(String isFixCode) {
            this.isFixCode = isFixCode;
        }

        public String getRateTime() {
            return rateTime;
        }

        public void setRateTime(String rateTime) {
            this.rateTime = rateTime;
        }

        public String getSurchargeRate() {
            return surchargeRate;
        }

        public void setSurchargeRate(String surchargeRate) {
            this.surchargeRate = surchargeRate;
        }

        public String getTaxRate() {
            return taxRate;
        }

        public void setTaxRate(String taxRate) {
            this.taxRate = taxRate;
        }

        public String getVatRate() {
            return vatRate;
        }

        public void setVatRate(String vatRate) {
            this.vatRate = vatRate;
        }

        public String getNumFixed() {
            return numFixed;
        }

        public void setNumFixed(String numFixed) {
            this.numFixed = numFixed;
        }

        public String getUsdToRmbExchangeRate() {
            return usdToRmbExchangeRate;
        }

        public void setUsdToRmbExchangeRate(String usdToRmbExchangeRate) {
            this.usdToRmbExchangeRate = usdToRmbExchangeRate;
        }

        public String getSourceToUsdExchangeRate() {
            return sourceToUsdExchangeRate;
        }

        public void setSourceToUsdExchangeRate(String sourceToUsdExchangeRate) {
            this.sourceToUsdExchangeRate = sourceToUsdExchangeRate;
        }

        public int getIsCardOpen() {
            return isCardOpen;
        }

        public void setIsCardOpen(int isCardOpen) {
            this.isCardOpen = isCardOpen;
        }

        public String getAlipayPayRate() {
            return alipayPayRate;
        }

        public void setAlipayPayRate(String alipayPayRate) {
            this.alipayPayRate = alipayPayRate;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getAppId() {
            return appId;
        }

        public void setAppId(String appId) {
            this.appId = appId;
        }

        public String getMachineId() {
            return machineId;
        }

        public void setMachineId(String machineId) {
            this.machineId = machineId;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public static class ApiProviderMapBean {
        }

        public static class RefundStateMapBean {
        }

        public static class TradeStateMapBean {
        }

        public static class PayTypeMapBean {
        }
    }
}
