package cn.swiftpass.enterprise.ui.activity.setting;

import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.MainActivity;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 设置 会自动检查版本 User: ALAN Date: 13-10-28 Time: 下午5:11 To change this template use
 * File | Settings | File Templates.
 */
public class SettingSwitchLanActivity extends TemplateActivity
{
    
    private Context mContext;
    
    private LinearLayout ly_close, ly_open, ly_china;
    
    private ImageView iv_open, iv_close, iv_china;
    
    private TextView tv_en, tv_tw, tv_ch;
    
    void getBase()
    {
        LocalAccountManager.getInstance().getBase(new UINotifyListener<Boolean>()
        {
            @Override
            public void onError(Object object)
            {
                // TODO Auto-generated method stub
                super.onError(object);
                dismissLoading();
                if (checkSession())
                {
                    return;
                }
                
                MainActivity.startActivity(SettingSwitchLanActivity.this, "SettingActivity");
                finish();
            }
            
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                showLoading(false, R.string.public_loading);
            }
            
            @Override
            public void onSucceed(Boolean result)
            {
                // TODO Auto-generated method stub
                super.onSucceed(result);
                dismissLoading();
                MainActivity.startActivity(SettingSwitchLanActivity.this, "SettingActivity");
                for (Activity a : MainApplication.allActivities)
                {
                    a.finish();
                }
                finish();
            }
            
        });
        
    }
    
    private void initViews()
    {
        setContentView(R.layout.activity_setting_switch_lan);
        mContext = this;
        tv_tw = getViewById(R.id.tv_tw);
        tv_en = getViewById(R.id.tv_en);
        tv_ch = getViewById(R.id.tv_ch);
        
        tv_ch.setText("中文(简体)");
        tv_tw.setText("中文(繁體)");
        tv_en.setText("English");
        iv_china = getViewById(R.id.iv_china);
        ly_china = getViewById(R.id.ly_china);
        iv_open = getViewById(R.id.iv_open);
        iv_close = getViewById(R.id.iv_close);
        ly_close = getViewById(R.id.ly_close);
        ly_open = getViewById(R.id.ly_open);
        
        String voice = PreferenceUtil.getString("language", "");
        
        //        Locale locale = MainApplication.getContext().getResources().getConfiguration().locale; //zh-rHK
        //        String lan = locale.getCountry();
        
        if (!StringUtil.isEmptyOrNull(voice))
        {
            if (voice.equals(MainApplication.LANG_CODE_ZH_TW))
            {
                iv_open.setVisibility(View.GONE);
                iv_china.setVisibility(View.GONE);
                iv_close.setVisibility(View.VISIBLE);
            }
            else if (voice.equals(MainApplication.LANG_CODE_ZH_CN))
            {
                iv_open.setVisibility(View.GONE);
                iv_close.setVisibility(View.GONE);
                iv_china.setVisibility(View.VISIBLE);
            }
            else
            {
                iv_open.setVisibility(View.VISIBLE);
                iv_close.setVisibility(View.GONE);
                iv_china.setVisibility(View.GONE);
            }
        }
        else
        {
            String lan = Locale.getDefault().toString();
            if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK)
                || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO)
                | lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW))
            {
                //                switchLanguage(MainApplication.LANG_CODE_EN_US);
                
                iv_open.setVisibility(View.GONE);
                iv_china.setVisibility(View.GONE);
                iv_close.setVisibility(View.VISIBLE);
            }
            else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN))
            {
                //                switchLanguage(MainApplication.LANG_CODE_ZH_CN);
                iv_open.setVisibility(View.GONE);
                iv_close.setVisibility(View.GONE);
                iv_china.setVisibility(View.VISIBLE);
            }
            else
            {
                //                switchLanguage(MainApplication.LANG_CODE_ZH_TW);
                iv_open.setVisibility(View.VISIBLE);
                iv_close.setVisibility(View.GONE);
                iv_china.setVisibility(View.GONE);
            }
            
        }
        
        ly_china.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                switchLanguage(MainApplication.LANG_CODE_ZH_CN);
                iv_china.setVisibility(View.VISIBLE);
                iv_open.setVisibility(View.GONE);
                iv_close.setVisibility(View.GONE);
                getBase();
            }
        });
        
        ly_open.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                iv_open.setVisibility(View.VISIBLE);
                iv_close.setVisibility(View.GONE);
                iv_china.setVisibility(View.GONE);
                switchLanguage(MainApplication.LANG_CODE_EN_US);
                
                getBase();
                
                //                PreferenceUtil.commitString("voice", "open");
                //                HandlerManager.notifyMessage(HandlerManager.VICE_SWITCH, HandlerManager.VICE_SWITCH);
            }
        });
        
        ly_close.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                iv_open.setVisibility(View.GONE);
                iv_close.setVisibility(View.VISIBLE);
                iv_china.setVisibility(View.GONE);
                //PreferenceUtil.commitString("voice", "close");
                //HandlerManager.notifyMessage(HandlerManager.VICE_SWITCH, HandlerManager.VICE_SWITCH);
                switchLanguage(MainApplication.LANG_CODE_ZH_TW);
                //                MainActivity.startActivity(SettingSwitchLanActivity.this, "SettingActivity");
                //                finish();
                getBase();
            }
        });
        
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        initViews();
        
    }
    
    @Override
    protected void onResume()
    {
        super.onResume();
        
    }
    
    @Override
    protected void onStop()
    {
        super.onStop();
    }
    
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_common_yuyan);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
            }
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                
            }
        });
    }
}