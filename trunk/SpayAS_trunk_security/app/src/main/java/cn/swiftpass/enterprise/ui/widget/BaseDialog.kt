package cn.swiftpass.enterprise.ui.widget

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView
import cn.swiftpass.enterprise.utils.ToastHelper

/**
 * @author lizheng.zhao
 * @date 2022/12/7
 *
 * @没有谁会喜欢孤独
 * @只是害怕失望
 */
abstract class BaseDialog<V : BaseView, P : BasePresenter<V>> : Dialog, BaseView {

    protected var mContext: Context? = null

    protected var mPresenter: P? = null

    private var loadingDialog: ProgressDialog? = null


    protected abstract fun createPresenter(): P

    protected abstract fun bindView(p: P)


    constructor(context: Context) : super(context) {
        mContext = context
        mPresenter = createPresenter()
        mPresenter?.let {
            bindView(it)
        }
    }

    override fun dismiss() {
        super.dismiss()
        mPresenter?.let {
            it.detachView()
        }
    }


    private fun showMyLoading(str: String?) {
        if (loadingDialog == null) {
            loadingDialog = ProgressDialog(mContext)
            loadingDialog!!.setCancelable(true)
        }
        loadingDialog!!.show()
        loadingDialog!!.setMessage(str)
    }

    private fun dismissMyLoading() {
        if (loadingDialog != null) {
            loadingDialog!!.dismiss()
            loadingDialog = null
        }
    }


    override fun showLoading(id: Int, type: Int) {
        showMyLoading(ToastHelper.toStr(id))
    }

    override fun dismissLoading(type: Int) {
        dismissMyLoading()
    }


}