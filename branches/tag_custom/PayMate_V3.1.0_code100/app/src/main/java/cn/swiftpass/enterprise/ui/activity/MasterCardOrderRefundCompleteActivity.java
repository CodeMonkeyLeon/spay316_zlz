package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.print.PrintOrder;
import cn.swiftpass.enterprise.ui.activity.print.BluePrintUtil;
import cn.swiftpass.enterprise.ui.activity.print.BluetoothSettingActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftplus.enterprise.printsdk.print.PrintClient;

import static cn.swiftpass.enterprise.intl.BuildConfig.IS_POS_VERSION;


/**
 * Created by aijingya on 2021/3/23.
 *
 * @Package cn.swiftpass.enterprise.ui
 * @Description:
 * @date 2021/3/23.17:22.
 */
public class MasterCardOrderRefundCompleteActivity extends TemplateActivity {
    private static final String TAG = MasterCardOrderRefundCompleteActivity.class.getSimpleName();
    private LinearLayout ll_refund_money,ll_order_amount,ll_surcharge,ll_refund_time,ll_refund_user,ll_refund_orderNoMch,ll_refund_order_id;
    private TextView tv_money,tv_amount_title,tv_total_amount,tv_surcharge,tv_order_refund_time,tv_refund_time,tv_refund_user,tv_refund_orderNoMch,tv_refund_order_id;
    private View view_line_refund_user,view_line_refund_orderNoMch;
    private Button btn_next_step;

    private Order orderModel;

    public static void startActivity(Context context, Order orderModel) {
        Intent it = new Intent();
        it.setClass(context, MasterCardOrderRefundCompleteActivity.class);
        it.putExtra("order", orderModel);
        context.startActivity(it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_payment_refund_complete);
        MainApplication.listActivities.add(this);

        orderModel = (Order) getIntent().getSerializableExtra("order");

        initView();
        initData();
        setListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印的绑定操作
        if(IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)){
            PrintClient.getInstance().bindDeviceService();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印的解绑操作
        if(IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)){
            PrintClient.getInstance().unbindDeviceService();
        }
    }

    public void initView(){
        ll_refund_money = findViewById(R.id.ll_refund_money);
        ll_order_amount = findViewById(R.id.ll_order_amount);
        ll_surcharge = findViewById(R.id.ll_surcharge);
        ll_refund_time = findViewById(R.id.ll_refund_time);
        ll_refund_user = findViewById(R.id.ll_refund_user);
        ll_refund_order_id = findViewById(R.id.ll_refund_order_id);
        tv_money = findViewById(R.id.tv_money);
        tv_amount_title = findViewById(R.id.tv_amount_title);
        tv_total_amount = findViewById(R.id.tv_total_amount);
        tv_surcharge = findViewById(R.id.tv_surcharge);
        tv_order_refund_time = findViewById(R.id.tv_order_refund_time);
        tv_refund_time = findViewById(R.id.tv_refund_time);
        tv_refund_user = findViewById(R.id.tv_refund_user);
        ll_refund_orderNoMch =findViewById(R.id.ll_refund_orderNoMch);
        tv_refund_orderNoMch =findViewById(R.id.tv_refund_orderNoMch);
        view_line_refund_orderNoMch = findViewById(R.id.view_line_refund_orderNoMch);
        tv_refund_order_id = findViewById(R.id.tv_refund_order_id);
        view_line_refund_user = findViewById(R.id.view_line_refund_user);
        btn_next_step = findViewById(R.id.btn_next_step);
    }

    public void initData(){
        //Mark ---- 此处因为在前一个的界面已经把orderModel的setMoney改为了RefundMoney，故此处设置的退款金额直接从前面的界面取的
        if (orderModel != null) {
            tv_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(orderModel.getRefundMoney()));
            tv_total_amount.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(orderModel.getTotalFee()));
            tv_refund_time.setText(orderModel.getTradeTimeNew());
            tv_refund_order_id.setText(orderModel.getOutRefundNo());

            if(!TextUtils.isEmpty(orderModel.getOrderNoMch())){
                ll_refund_orderNoMch.setVisibility(View.VISIBLE);
                view_line_refund_orderNoMch.setVisibility(View.VISIBLE);

                tv_refund_orderNoMch.setText(orderModel.getOrderNoMch());
            }else{
                ll_refund_orderNoMch.setVisibility(View.GONE);
                view_line_refund_orderNoMch.setVisibility(View.GONE);
            }
        }

        if (MainApplication.isAdmin.equals("1")) {
            ll_refund_user.setVisibility(View.VISIBLE);
            view_line_refund_user.setVisibility(View.VISIBLE);
            tv_refund_user.setText(MainApplication.mchName);
            //此时要在打印小票的时候更新申请人
            if (orderModel != null) {
                orderModel.setUserName(MainApplication.mchName);
            }
        } else {
            ll_refund_user.setVisibility(View.VISIBLE);
            view_line_refund_user.setVisibility(View.VISIBLE);
            tv_refund_user.setText(MainApplication.realName);
            //此时要在打印小票的时候更新申请人
            if (orderModel != null) {
                orderModel.setUserName(MainApplication.realName);
            }
        }

        //经产品确认，发起退款的页面，即使开通surcharge也不展示退款的surcharge，同理，小票上也不展示
        ll_surcharge.setVisibility(View.GONE);
        ll_order_amount.setVisibility(View.GONE);

       /* if (!MainApplication.isSurchargeOpen()) {
            ll_surcharge.setVisibility(View.GONE);
            ll_order_amount.setVisibility(View.GONE);
        } else {
            if (orderModel != null) {
                ll_surcharge.setVisibility(View.VISIBLE);
                ll_order_amount.setVisibility(View.VISIBLE);
                tv_surcharge.setText(MainApplication.feeFh + DateUtil.formatMoneyUtils(orderModel.getSurcharge()));
            }
        }*/

        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印
        if(IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)){
            //设置自动打印
            if(orderModel != null){
                print(orderModel);
            }
        }else{
            if (MainApplication.getAutoBluePrintSetting()) {//设置自动打印
                if (orderModel != null) {
                    MasterCardOrderRefundCompleteActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            print(orderModel);
                        }
                    });
                }
            }
        }
    }

    public void setListener(){
        btn_next_step.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (Activity a : MainApplication.listActivities) {
                    if(!(a instanceof spayMainTabActivity)){
                        a.finish();
                    }
                }
                finish();
            }
        });
    }


    private void print(final Order orderModel) {
        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印
        orderModel.setPrintInfo(getString(R.string.tv_refunding_info));
        if(IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)){
            if(BuildConfig.posTerminal.equalsIgnoreCase("A8")){
                orderModel.setPrintInfo(getString(R.string.tv_refunding_info_A8));
            }
            PrintOrder.printMasterCardOrderDetails(MasterCardOrderRefundCompleteActivity.this, false,false,false,
                    getString(R.string.tv_pay_user_stub),orderModel);
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    try {
                        PrintOrder.printMasterCardOrderDetails(MasterCardOrderRefundCompleteActivity.this, false,false,false,
                                getString(R.string.tv_pay_mch_stub),orderModel);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            Timer timer = new Timer();
            timer.schedule(task, PrintClient.getInstance().getPrintDelay());
        }else{
            if (!MainApplication.getBluePrintSetting()) {//关闭蓝牙打印
                showDialog();
                return;
            }
            if (MainApplication.bluetoothSocket != null && MainApplication.bluetoothSocket.isConnected()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            try {
                                BluePrintUtil.printMasterCardOrder(false,false,false,getString(R.string.tv_pay_user_stub),orderModel);
                                //打印第二联
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        BluePrintUtil.printMasterCardOrder(false,false,false,getString(R.string.tv_pay_mch_stub),orderModel);
                                    }
                                }, 3000);
                            } catch (Exception e) {
                                Log.e(TAG, Log.getStackTraceString(e));
                            }
                        } catch (Exception e) {
                            Log.e(TAG, Log.getStackTraceString(e));
                        }
                    }
                });
            } else {
                String bluetoothDeviceAddress = MainApplication.getBlueDeviceAddress();
                if (!StringUtil.isEmptyOrNull(bluetoothDeviceAddress)) {
                    boolean isSucc = BluePrintUtil.blueConnent(bluetoothDeviceAddress, MasterCardOrderRefundCompleteActivity.this);
                    if (isSucc) {
                        try {
                            BluePrintUtil.printMasterCardOrder(false,false,false,getString(R.string.tv_pay_user_stub),orderModel);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    BluePrintUtil.printMasterCardOrder(false,false,false,getString(R.string.tv_pay_mch_stub),orderModel);
                                }
                            }, 3000);
                        } catch (Exception e) {
                            Log.e(TAG, Log.getStackTraceString(e));
                        }
                    }
                } else {
                    showDialog();
                }
            }
        }

    }

    void showDialog() {
        dialog = new DialogInfo(MasterCardOrderRefundCompleteActivity.this, null, getString(R.string.tx_blue_set), getString(R.string.title_setting), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                showPage(BluetoothSettingActivity.class);
                dialog.cancel();
                dialog.dismiss();
            }

            @Override
            public void handleCancleBtn() {
                dialog.cancel();
            }
        }, null);

        DialogHelper.resize(MasterCardOrderRefundCompleteActivity.this, dialog);
        dialog.show();
    }

    @Override
    public boolean onKeyDown(int keycode, KeyEvent event) {
        if (keycode == KeyEvent.KEYCODE_BACK) {
            for (Activity a : MainApplication.listActivities) {
                if(!(a instanceof spayMainTabActivity)){
                    a.finish();
                }
            }
            finish();
        }
        return super.onKeyDown(keycode, event);
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(R.string.refund_auditing);
        titleBar.setTitleChoice(false);
        titleBar.setLeftButtonVisible(true);
        titleBar.setLeftButtonIsVisible(true);
        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.print));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                for (Activity a : MainApplication.listActivities) {
                    if(!(a instanceof spayMainTabActivity)){
                        a.finish();
                    }
                }
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {
                if (orderModel != null) {
                    print(orderModel);
                }
            }

            @Override
            public void onRightButLayClick() {
                if (orderModel != null) {
                    print(orderModel);
                }
            }
        });
    }
}
