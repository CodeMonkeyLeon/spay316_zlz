package cn.swiftpass.enterprise.ui.activity.chart;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;

public class HorizontalScrollViewNew extends HorizontalScrollView
{
    
    public HorizontalScrollViewNew(Context context)
    {
        super(context);
    }
    
    private boolean mCanScroll = true;
    
    private float mDownX;
    
    @Override
    public boolean onTouchEvent(MotionEvent ev)
    {
        if (ev.getAction() == MotionEvent.ACTION_DOWN)
        {
            mDownX = ev.getX();
        }
        
        if (ev.getAction() == MotionEvent.ACTION_MOVE)
        {
            int scrollx = getScrollX();
            if ((scrollx == 0 && mDownX - ev.getX() <= -10)
                || (getChildAt(0).getMeasuredWidth() <= (scrollx + 500) && mDownX - ev.getX() >= 10))
            {
                mCanScroll = false;
            }
            
        }
        
        if (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_CANCEL)
        {
            mCanScroll = true;
        }
        
        if (this.mCanScroll)
        {
            //此句代码是为了通知他的父ViewPager现在进行的是本控件的操作，不要对我的操作进行干扰
            getParent().requestDisallowInterceptTouchEvent(true);
            return super.onTouchEvent(ev);
        }
        else
        {
            getParent().requestDisallowInterceptTouchEvent(false);
            return false;
        }
    }
}
