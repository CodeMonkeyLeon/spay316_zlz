package cn.swiftpass.enterprise.ui.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import cn.swiftpass.enterprise.intl.R;


// CustomDialog.Builder builder=new CustomDialog.Builder(this);
//  CustomDialog dialog = builder
//        .style(R.style.Dialog)
//        .heightDimenRes(R.dimen.dilog_identitychange_height)  WindowManager.LayoutParams对象类的height和width。
//        .widthDimenRes(R.dimen.dilog_identitychange_width)
//        .cancelTouchOut(false)
//        .view(R.layout.dialog_identitychange)
//        .addViewOnclick(R.id.jbperson,listener)
//        .addViewOnclick(R.id.spperson,listener)
//        .addViewOnclick(R.id.confirmbtn,listener)
//        .build();
//
// dialog.show();

public class CustomDialog extends Dialog {
    private Context context;
    private int height, width, cGravity;

    private boolean cancelTouchout = true;
    private View cView;

    private CustomDialog(Builder builder) {
        super(builder.context);
        context = builder.context;
        height = builder.height;
        width = builder.width;
        cancelTouchout = builder.cancelTouchOut;
        cView = builder.mView;
    }


    private CustomDialog(Builder builder, int resStyle) {
        super(builder.context, resStyle);
        context = builder.context;
        height = builder.height;
        width = builder.width;
        cancelTouchout = builder.cancelTouchOut;
        cView = builder.mView;
        cGravity = builder.mGravity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(cView);

        setCanceledOnTouchOutside(cancelTouchout);

        Window win = getWindow();
        WindowManager.LayoutParams lp = win.getAttributes();
        lp.dimAmount = 0.2f;
        lp.gravity = cGravity;
        lp.height = height;
        lp.width = width;

        win.setAttributes(lp);
        win.setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND, WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    public static final class Builder {

        private Context context;
        private int height = WindowManager.LayoutParams.WRAP_CONTENT;
        private int width = WindowManager.LayoutParams.WRAP_CONTENT;     // 默认宽度250dp
        private boolean cancelTouchOut = true;
        private View mView;
        private int resStyle = R.style.dialog;
        private int mGravity = Gravity.CENTER;

        public Builder(Context context) {
            this.context = context;
            width =dip2px(context, 250);
        }

        public Builder view(int resView) {
            mView = LayoutInflater.from(context).inflate(resView, null);
            return this;
        }

        public Builder gravity(int val) {
            mGravity = val;
            return this;
        }

        public Builder heightpx(int val) {
            height = val;
            return this;
        }

        public Builder widthpx(int val) {
            width = val;
            return this;
        }

        public Builder heightdp(int val) {
            height =dip2px(context, val);
            return this;
        }

        public Builder widthdp(int val) {
            width = dip2px(context, val);
            return this;
        }

        public Builder heightDimenRes(int dimenRes) {
            height = context.getResources().getDimensionPixelOffset(dimenRes);
            return this;
        }

        public Builder widthDimenRes(int dimenRes) {
            width = context.getResources().getDimensionPixelOffset(dimenRes);
            return this;
        }

        public Builder style(int resStyle) {
            this.resStyle = resStyle;
            return this;
        }

        public Builder cancelTouchout(boolean val) {
            cancelTouchOut = val;
            return this;
        }

        public Builder addViewOnclick(int viewRes, View.OnClickListener listener) {
            mView.findViewById(viewRes).setOnClickListener(listener);
            return this;
        }


        public CustomDialog build() {
            if (resStyle != -1) {
                return new CustomDialog(this, resStyle);
            } else {
                return new CustomDialog(this);
            }
        }
    }
    /**
     * dp转px
     */
    public static int dip2px(Context context, float dpVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dpVal, context.getResources().getDisplayMetrics());
    }
}