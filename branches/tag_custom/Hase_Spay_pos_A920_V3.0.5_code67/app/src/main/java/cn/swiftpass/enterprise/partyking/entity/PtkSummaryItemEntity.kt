package cn.swiftpass.enterprise.partyking.entity

import com.chad.library.adapter.base.entity.MultiItemEntity
import java.io.Serializable

data class PtkSummaryItemEntity(
    var title: String = "",
    var time: String = "",
    var status: String = "",
    var statusType: Int = 0
) : Serializable, MultiItemEntity {

    companion object {
        const val TYPE_PTK_SUMMARY_DEFAULT = 0

        const val STATUS_TYPE_SUCCEED = 1100
        const val STATUS_TYPE_FAILED = 1101
        const val STATUS_TYPE_TIMEOUT = 1102
        const val STATUS_TYPE_ROLLBACK = 1103
    }


    override fun getItemType() = TYPE_PTK_SUMMARY_DEFAULT
}