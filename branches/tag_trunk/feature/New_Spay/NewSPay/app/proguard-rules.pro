##################################基本指令区，默认不用修改下面内容##########################################
-ignorewarnings                           #忽略所有警告
-optimizationpasses 5                    #指定代码的压缩级别
-dontusemixedcaseclassnames             #是否使用大小写混合
-dontskipnonpubliclibraryclasses       #是否混淆第三方jar， 指定不去忽略非公共的库的类
-dontskipnonpubliclibraryclassmembers #指定不去忽略非公共的库的类的成员
-dontpreverify                          #不做预校验的操作
-verbose                                 # 混淆时是否记录日志
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*,!code/allocation/variable # 混淆时所采用的算法
-printmapping proguardMapping.txt       #生成原类名和混淆后的类名的映射文件
-keepattributes *Annotation*            #不混淆Annotation
-keepattributes InnerClasses           #不混淆内部类
-keepattributes Signature              #不混淆泛型
-keepattributes Exceptions             #不混淆异常
-keepattributes EnclosingMethod        #不混淆反射
-keepattributes *JavascriptInterface*  #不混淆Android和JS交互
-keepattributes SourceFile,LineNumberTable #抛出异常时保留代码行号
###############################################################################################################

##############################################基本组件白名单，默认不用修改以下内容----Start#######################################
#-keep public class * extends android.app.Fragment
#-keep public class * extends android.app.Activity
#-keep public class * extends android.app.Application
#-keep public class * extends android.app.Service
#-keep public class * extends android.content.BroadcastReceiver
#-keep public class * extends android.content.ContentProvider
#-keep public class * extends android.app.backup.BackupAgentHelper
#-keep public class * extends android.preference.Preference
#-keep public class * extends android.view.View
#保留Google原生服务需要的类
#-keep public class com.android.vending.licensing.ILicensingService
#-keep public class com.google.vending.licensing.ILicensingService
#XML映射问题,如果你遇到一些控件无法Inflate，报NullPointException，比如ListView，NavigationView等等
#-keep class org.xmlpull.v1.** {*;}

#保留自定义View,如"属性动画"中的set/get方法
-keepclassmembers public class * extends android.view.View{
    *** get*();
    void set*(***);
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
}
#保留Activity中参数是View的方法，如XML中配置android:onClick=”buttonClick”属性，Activity中调用的buttonClick(View view)方法
-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
}
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}
#Parcelable实现类中的CREATOR字段是绝对不能改变的，包括大小写
-keepclassmembers class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}
#保留混淆枚举中的values()和valueOf()方法
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
#保留R下面的资源
-keep class **.R$* {*;}
#R文件中的所有记录资源id的静态字段
-keepclassmembers class **.R$* {
    public static <fields>;
}
# 保留native方法的类名和方法名
-keepclasseswithmembernames class * {
    native <methods>;
}
# -->某些构造方法不能去混淆
-keepclasseswithmembers class * {
    public <init>(android.content.Context);
}
-keepclassmembers class * {
   public <init>(org.json.JSONObject);
}
#对于带有回调函数的onXXEvent的，不能被混淆
-keepclassmembers class * {
    void *(**On*Event);
}
##############################################基本组件白名单----End#######################################

