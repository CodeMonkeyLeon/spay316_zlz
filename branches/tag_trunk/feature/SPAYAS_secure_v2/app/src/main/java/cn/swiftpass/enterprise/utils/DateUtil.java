/*******************************************************
 * @作者: huangdh
 * @日期: 2012-6-5
 * @描述: TODO
 * @声明: copyrights reserved by Petfone 2007-2011
 *******************************************************/
package cn.swiftpass.enterprise.utils;

import android.annotation.SuppressLint;
import android.util.Log;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author : huangdh
 * @version :13-7-12
 * @Copyright : copyrights reserved by personal 20012-2013
 * @see : 日期工具栏
 */
@SuppressLint("SimpleDateFormat")
public class DateUtil
{
    public static String formartDateToYYMMDD(String date)
            throws ParseException
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(df.parse(date));
    }

    private static final String TAG = "DateUtil";

    private static final String DATA_FORMAT = "yyyy-MM-dd";

    //将指定日期转化为秒数
    public static long getSecondsFromDate(String expireDate)
    {
        if (expireDate == null || expireDate.trim().equals(""))
            return 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try
        {
            date = sdf.parse(expireDate);
            return date.getTime();
        }
        catch (ParseException e)
        {
            Log.e(TAG,Log.getStackTraceString(e));
            return 0;
        }
    }

    public static String[] paseTime(String time)
    {
        String[] times = new String[5];

        String[] a = time.split(" ");

        String[] c = a[0].split("/");
        //月，日
        times[0] = c[1];
        times[1] = c[2];
        if (time.contains(":"))
        {
            String[] d = a[1].split(":");
            times[2] = d[0];
            times[3] = d[1];
            times[4] = d[2];
        }

        return times;
    }

    //将指定日期转化为date
    public static Date getStrFromDate(String expireDate)
    {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        try
        {
            return sdf.parse(expireDate);
        }
        catch (ParseException e)
        {
            Log.e(TAG,Log.getStackTraceString(e));
            return null;
        }
    }

    //将指定日期转化为date
    public static Date getStrToDate(String expireDate)
    {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try
        {
            return sdf.parse(expireDate);
        }
        catch (ParseException e)
        {
            Log.e(TAG,Log.getStackTraceString(e));
            return null;
        }
    }

    //将指定日期转化为date
    public static Date getStrFromDateYYMMDD(String expireDate)
    {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        try
        {
            return sdf.parse(expireDate);
        }
        catch (ParseException e)
        {
            Log.e(TAG,Log.getStackTraceString(e));
            return null;
        }
    }

    /**
     * 获取两个日期之间的间隔天数 
     * @return
     */
    public static int getGapCount(Date startDate, Date endDate)
    {
        Calendar fromCalendar = Calendar.getInstance();
        fromCalendar.setTime(startDate);
        fromCalendar.set(Calendar.HOUR_OF_DAY, 0);
        fromCalendar.set(Calendar.MINUTE, 0);
        fromCalendar.set(Calendar.SECOND, 0);
        fromCalendar.set(Calendar.MILLISECOND, 0);

        Calendar toCalendar = Calendar.getInstance();
        toCalendar.setTime(endDate);
        toCalendar.set(Calendar.HOUR_OF_DAY, 0);
        toCalendar.set(Calendar.MINUTE, 0);
        toCalendar.set(Calendar.SECOND, 0);
        toCalendar.set(Calendar.MILLISECOND, 0);

        return (int)((toCalendar.getTime().getTime() - fromCalendar.getTime().getTime()) / (1000 * 60 * 60 * 24));
    }

    /**
     * 获取两个日期之间的间隔天数 
     * @return
     */
    public static int getGapCounts(Date startDate, Date endDate)
    {
        Calendar fromCalendar = Calendar.getInstance();
        fromCalendar.setTime(startDate);
        fromCalendar.set(Calendar.HOUR_OF_DAY, 0);
        //        fromCalendar.set(Calendar.MINUTE, 0);
        //        fromCalendar.set(Calendar.SECOND, 0);
        //        fromCalendar.set(Calendar.MILLISECOND, 0);

        Calendar toCalendar = Calendar.getInstance();
        toCalendar.setTime(endDate);
        toCalendar.set(Calendar.HOUR_OF_DAY, 0);
        //        toCalendar.set(Calendar.MINUTE, 0);
        //        toCalendar.set(Calendar.SECOND, 0);
        //        toCalendar.set(Calendar.MILLISECOND, 0);

        return (int)((toCalendar.getTime().getTime() - fromCalendar.getTime().getTime()) / (1000 * 60 * 60 * 24));
    }

    /**
     * 比较是否通一天
     *
     * @param localTime
     *            本地时间
     * @param remoteTime
     *            服务器或者其他地方获取的时间
     * @return
     */
    public static boolean isSameDay(long localTime, long remoteTime)
    {
        Calendar todayC = Calendar.getInstance(Locale.CHINA);
        todayC.setTimeInMillis(localTime);
        int todayYear = todayC.get(Calendar.YEAR);
        int todayMonth = todayC.get(Calendar.MONTH) + 1;
        int todayDay = todayC.get(Calendar.DAY_OF_MONTH);

        Logger.d(TAG, "start:" + todayYear + "-" + todayMonth + "-" + todayDay);

        Calendar compareTime = Calendar.getInstance();
        compareTime.setTimeInMillis(remoteTime);
        int year = compareTime.get(Calendar.YEAR);
        int month = compareTime.get(Calendar.MONTH) + 1;
        int day = compareTime.get(Calendar.DAY_OF_MONTH);

        Logger.d(TAG, "end:" + year + "-" + month + "-" + day);

        if (year == todayYear && month == todayMonth && day == todayDay)
        {
            return true;
        }
        return false;
    }

    /**
     * 获取当天日期
     *
     * @return yyyy-mm-dd 00：00：00
     */
    public static String getTodayDate()
    {
        Calendar todayC = Calendar.getInstance(Locale.CHINA);
        todayC.setTimeInMillis(System.currentTimeMillis());
        int todayYear = todayC.get(Calendar.YEAR);
        int todayMonth = todayC.get(Calendar.MONTH) + 1;
        int todayDay = todayC.get(Calendar.DAY_OF_MONTH);
        todayC.get(Calendar.HOUR);
        todayC.get(Calendar.MINUTE);
        todayC.get(Calendar.SECOND);
        return todayYear + "/" + todayMonth + "/" + todayDay + " 00:00:00";
    }

    public static String getTodayDateTime()
    {
        Calendar todayC = Calendar.getInstance(Locale.CHINA);
        todayC.setTimeInMillis(System.currentTimeMillis());
        int todayYear = todayC.get(Calendar.YEAR);
        int todayMonth = todayC.get(Calendar.MONTH) + 1;
        int todayDay = todayC.get(Calendar.DAY_OF_MONTH);
        int hour = todayC.get(Calendar.HOUR);
        int minute = todayC.get(Calendar.MINUTE);
        int second = todayC.get(Calendar.SECOND);
        return todayYear + "/" + todayMonth + "/" + todayDay + " " + hour + ":" + minute + ":" + second;
    }

    /**
     * 获取指定时间
     *
     * @param date
     * @return
     */
    public static boolean isToday(String date)
    {
        int year, month, day;
        String[] value = date.split("-");
        year = Integer.parseInt(value[0]);
        month = Integer.parseInt(value[1]);
        day = Integer.parseInt(value[2]);

        Calendar todayC = Calendar.getInstance(Locale.CHINA);
        todayC.setTimeInMillis(System.currentTimeMillis());
        int todayYear = todayC.get(Calendar.YEAR);
        int todayMonth = todayC.get(Calendar.MONTH) + 1;
        int todayDay = todayC.get(Calendar.DAY_OF_MONTH);
        if (year == todayYear && month == todayMonth && day == todayDay)
        {
            return true;
        }
        return false;
    }

    /**
     * 保留小数点后几位
     *
     * @param f
     * @return
     */
    public static String subDecimal(double f, int p)
    {
        String s = String.valueOf(f);
        int pointIdx = s.indexOf(".");
        String result = null;
        int end = pointIdx + 1 + p;
        if (s.length() >= end)
        {
            result = s.substring(0, end);
        }
        else
        {
            result = s;
        }
        return result;
    }

    /** 是否是电话号码 */
    public static boolean isPhoneNumberValid(String phoneNumber)
    {
        // (^(\d{2,4}[-_－—]?)?\d{3,8}([-_－—]?\d{3,8})?([-_－—]?\d{1,7})?$)|(^0?1[35]\d{9}$)
        String expression1 =
                "((^(13|15|18)[0-9]{9}$)|(^0[1,2]{1}\\d{1}-?\\d{8}$)|(^0[3-9] {1}\\d{2}-?\\d{7,8}$)|(^0[1,2]{1}\\d{1}-?\\d{8}-(\\d{1,4})$)|(^0[3-9]{1}\\d{2}-? \\d{7,8}-(\\d{1,4})$))";
        String expression2 =
                "((\\d{11})|^((\\d{7,8})|(\\d{4}|\\d{3})-(\\d{7,8})|(\\d{4}|\\d{3})-(\\d{7,8})-(\\d{4}|\\d{3}|\\d{2}|\\d{1})|(\\d{7,8})-(\\d{4}|\\d{3}|\\d{2}|\\d{1}))$)";
        CharSequence inputStr = phoneNumber;
        Pattern pattern1 = Pattern.compile(expression1);
        Matcher matcher1 = pattern1.matcher(inputStr);
        Pattern pattern2 = Pattern.compile(expression2);
        Matcher matcher2 = pattern2.matcher(inputStr);
        return matcher1.matches() || matcher2.matches();
    }

    /** 格式化为yyyy年MM月dd日 */
    public static String formatYMD(long timestamp)
    {
        SimpleDateFormat sdf = new SimpleDateFormat(DATA_FORMAT);
        return sdf.format(new Date(timestamp));
    }

    /** 格式化为yyyy年MM月 */
    public static String formatYM(long timestamp)
    {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy年MM月");
        return sdf1.format(new Date(timestamp));
    }

    /** 格式化为MM月 dd日 */
    public static String formatMD(long timestamp)
    {
        SimpleDateFormat sdf1 = new SimpleDateFormat("MM-dd");
        return sdf1.format(timestamp);
    }

    /** 格式化为HH:mm:ss */
    public static String formatHHmm(long timestamp)
    {
        SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");
        return sdf1.format(timestamp);
    }

    /** 格式化为YYYYMMdd */
    public static String formatYYYYMD(long time)
    {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd");
        return sdf1.format(time);
    }

    /** 格式化为yyyy/MM/dd */
    public static String formatsYYYYMD(long time)
    {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd");
        return sdf1.format(time) + " 00:00:00";
    }

    /** 格式化为yyyy/MM/dd HH:mm:ss */
    public static String formatNowTime(long time)
    {
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return df.format(new Date(time));
    }

    /** 格式化为YYYYMMdd */
    public static String formatByYYMD(long time)
    {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyMMdd");
        return sdf1.format(time);
    }

    /** 格式化为MM月 dd日 */
    public static String formatYYMD(long timestamp)
    {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        return sdf1.format(new Date(timestamp));
    }

    /** 格式化为MM月 dd日 */
    public static String formatMMDDNew(long timestamp)
    {
        SimpleDateFormat sdf1 = new SimpleDateFormat("MM月dd日");
        return sdf1.format(new Date(timestamp));
    }

    /** 格式化为yyyy-MM-dd HH:mm:ss */
    public static String formatTime(long time)
    {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(new Date(time));
    }

    /** 格式化为yyyy/MM/dd HH:mm:ss */
    public static String formatTimeUtil(long time)
    {
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return df.format(new Date(time));
    }

    /** 格式化为yyyy-MM-dd HH:mm:ss */
    public static String formatTime(long time, String format)
    {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(new Date(time));
    }

    /** 格式化为yyyy-MM-dd HH:mm:ss */
    public static String formatTime2(long time)
    {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        return df.format(new Date(time));
    }

    /** 格式化为yyyy年MM月dd日 */
    public static String formatYMD(Date timestamp)
    {
        SimpleDateFormat sdf = new SimpleDateFormat(DATA_FORMAT);
        return sdf.format(timestamp);
    }

    /** 获取相差 小时 */
    public static long isdiffDay(long backTime)
    {

        long l = System.currentTimeMillis() - backTime;
        long day = l / (24 * 60 * 60 * 1000);
        long hour = (l / (60 * 60 * 1000) - day * 24);
        long min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
        long s = (l / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        Log.i("DateUtil", "" + day + "天" + hour + "小时" + min + "分" + s + "秒");
        return hour;
    }

    public static void isdiffDay()
    {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now;
        try
        {
            now = df.parse("2004-03-26 13:31:40");

            Date date = df.parse("2004-03-01 11:30:24");
            long l = now.getTime() - date.getTime();
            long day = l / (24 * 60 * 60 * 1000);
            long hour = (l / (60 * 60 * 1000) - day * 24);
            long min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
            long s = (l / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
            Log.i(TAG,"" + day + "天" + hour + "小时" + min + "分" + s + "秒");
        }
        catch (ParseException e)
        {
            // TODO Auto-generated catch block
            Log.e(TAG,Log.getStackTraceString(e));

        }
    }

    public static String formatAM(long time)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        return formatter.format(time);
    }

    public static String formatTimeMMSS(long time)
    {
        SimpleDateFormat sdf2 = new SimpleDateFormat(DATA_FORMAT_2, Locale.CHINESE);
        return sdf2.format(time);
    }

    public static String formatTimeHHMM(long time)
    {
        SimpleDateFormat sdf2 = new SimpleDateFormat(DATA_FORMAT_2, Locale.CHINESE);
        return sdf2.format(time);
    }

    private static final String DATA_FORMAT_2 = "HH:mm:ss";

    public static String converTime(long timestamp) {
        long currentSeconds = System.currentTimeMillis() / 1000;
        long timeGap = currentSeconds - timestamp;// 与现在时间相差秒数
        if (timeGap > 24 * 60 * 60) {// 1天以上
            return SimpleDateFormatUtils.format(new Date(timestamp),DATA_FORMAT);
        } else if (timeGap > 60) {// 1分钟以上
            return formatAM(timestamp);// //上，中 下 午
        } else {// 1分钟以下
            return SimpleDateFormatUtils.format(new Date(timestamp),DATA_FORMAT);
        }
    }

    public static String getEasyTime(long t)
    {
        Date date = new Date(t);
        String todySDF = "HH:mm:ss";
        //String yesterDaySDF = "昨天 HH:mm";
        String otherSDF = "yyyy/MM/dd";
        SimpleDateFormat sfd = null;
        String time = "";
        Calendar dateCalendar = Calendar.getInstance();
        dateCalendar.setTime(date);
        Date now = new Date();
        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTime(now);
        targetCalendar.set(Calendar.HOUR_OF_DAY, 0);
        targetCalendar.set(Calendar.MINUTE, 0);
        if (dateCalendar.after(targetCalendar))
        {
            sfd = new SimpleDateFormat(todySDF);
            time = sfd.format(date);
            return time;
        }
        //        else
        //        {
        //            targetCalendar.add(Calendar.DATE, -1);
        //            if (dateCalendar.after(targetCalendar))
        //            {
        //                sfd = new SimpleDateFormat(yesterDaySDF);
        //                time = sfd.format(date);
        //                return time;
        //            }
        //        }
        sfd = new SimpleDateFormat(otherSDF);
        time = sfd.format(date);
        return time;
    }

    /** 将传入时间与当前时间进行对比，是否今天昨天 */
    public static String getStrTime(long t)
    {
        Date date = new Date(t);
        String todySDF = "a HH:mm";
        String yesterDaySDF = "昨天 HH:mm";
        String otherSDF = "M月d日 HH:mm";
        SimpleDateFormat sfd = null;
        String time = "";
        Calendar dateCalendar = Calendar.getInstance();
        dateCalendar.setTime(date);
        Date now = new Date();
        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTime(now);
        targetCalendar.set(Calendar.HOUR_OF_DAY, 0);
        targetCalendar.set(Calendar.MINUTE, 0);
        if (dateCalendar.after(targetCalendar))
        {
            sfd = new SimpleDateFormat(todySDF);
            time = sfd.format(date);
            return time;
        }
        else
        {
            targetCalendar.add(Calendar.DATE, -1);
            if (dateCalendar.after(targetCalendar))
            {
                sfd = new SimpleDateFormat(yesterDaySDF);
                time = sfd.format(date);
                return time;
            }
        }
        sfd = new SimpleDateFormat(otherSDF);
        time = sfd.format(date);
        return time;
    }

    /**
     * 将毫秒转换为类似于：1小时5分1秒的字符串
     */
    public static String getTimeDesc(long ms)
    {
        long hour = ms / 3600000;
        long rest = ms % 3600000;
        long min = rest / 60000;
        rest = rest % 60000;
        long sec = rest / 1000;

        String ret = "";
        if (hour > 0)
        {
            ret = hour + "小时";
        }
        if (min > 0)
        {
            ret = ret + min + "'";
        }
        ret = ret + sec + "''";

        return ret;
    }

    /**
     * 判断是否为合法的日期时间字符串
     *
     * @param str_input
     * @param str_input
     * @return boolean;符合为true,不符合为false
     */
    public static boolean isDate(String str_input, String rDateFormat)
    {
        if (!isNull(str_input))
        {
            SimpleDateFormat formatter = new SimpleDateFormat(rDateFormat);
            formatter.setLenient(false);
            try
            {
                formatter.format(formatter.parse(str_input));
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean isNull(String str)
    {
        if (str == null)
            return true;
        else
            return false;
    }

    /**
     * 得到一个年,月,日 的Date对象
     *
     * @param y
     * @param m
     * @param d
     * @return
     */
    static public Date date(int y, int m, int d)
    {
        return date(y, m - 1, d, 0, 0, 0);
    }

    static public Date date(int y, int m, int d, int h, int ms, int s)
    {
        Calendar cal = Calendar.getInstance();
        cal.set(y, m, d, h, ms, s);
        return cal.getTime();
    }

    public static long getTime(Date date)
    {
        return date.getTime();
    }

    /******************* 本周 上周 本月 上月 ******************************/
    public static String formatDate(Date date, String pattern)
    {
        return (new SimpleDateFormat(pattern)).format(date);
    }

    /**
     * 得到上周
     *
     * @return
     */
    public static String[] calcLastWeek()
    {
        GregorianCalendar calendar = new GregorianCalendar();
        int minus = calendar.get(GregorianCalendar.DAY_OF_WEEK) + 1;
        calendar.add(GregorianCalendar.DATE, -minus);
        String end = new java.sql.Date(calendar.getTime().getTime()).toString();
        calendar.add(GregorianCalendar.DATE, -4);
        String begin = new java.sql.Date(calendar.getTime().getTime()).toString();
        return new String[] {begin, end};
    }

    /**
     * 得到上周
     *
     * @return
     */
    public static String[] getLastWeek()
    {
        // 上周一
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance(); // n为推迟的周数，1本周，-1向前推迟一周，2下周，依次类推
        int n = -1;
        cal.add(Calendar.DATE, n * 7); // 想周几，这里就传几Calendar.MONDAY（TUESDAY...）
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        String begin = format.format(cal.getTime());

        // 上周日
        Calendar c = Calendar.getInstance();
        // 减去一个星期
        c.add(Calendar.WEEK_OF_MONTH, -1);
        // 上个星期的今天是第几天,星期天是1,所以要减去1
        int d = c.get(Calendar.DAY_OF_WEEK) - 1;
        // 添加余下的天数
        c.add(Calendar.DAY_OF_WEEK, 7 - d);
        Log.i(TAG,"last sunday: " + new java.sql.Date(c.getTime().getTime()).toString());
        String end = format.format(c.getTime());
        return new String[] {begin, end};
    }

    /**
     * 得到本月的最后一天和第一天
     *
     * @return
     */
    public static String[] getThisMonth()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        String begin = formatDate(calendar.getTime(), "yyyy-MM-dd");
        String end = getMonthLastDay();
        return new String[] {begin, end};
    }

    /**
     * 得到本月的第一天
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static String getMonthFirst()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        String begin = formatDate(calendar.getTime(), "yyyy-MM-dd HH:mm:ss");

        return begin;
    }

    /**
     * 得到本月的最后一天
     *
     * @return
     */
    public static String getMonthLastDay()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return formatDate(calendar.getTime(), "yyyy-MM-dd");
    }

    /**
     * 上月
     *
     * @return
     */
    public static String[] getcLastMonth()
    {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.set(calendar.get(GregorianCalendar.YEAR), calendar.get(GregorianCalendar.MONTH), 1);
        calendar.add(GregorianCalendar.DATE, -1);
        String end = new java.sql.Date(calendar.getTime().getTime()).toString();

        int month = calendar.get(GregorianCalendar.MONTH) + 1;
        String begin = calendar.get(GregorianCalendar.YEAR) + "-" + month + "-01 ";
        return new String[] {begin, end};
    }

    /**
     * 得到本周周一
     *
     * @return yyyy-MM-dd
     */
    public static String getMondayOfThisWeek()
    {
        Calendar c = Calendar.getInstance();
        int dayofweek = c.get(Calendar.DAY_OF_WEEK) - 1;
        if (dayofweek == 0)
            dayofweek = 7;
        c.add(Calendar.DATE, -dayofweek + 1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(c.getTime());
    }

    /**
     * 得到本周周日
     *
     * @return yyyy-MM-dd
     */
    public static String getSundayOfThisWeek()
    {
        Calendar c = Calendar.getInstance();
        int dayofweek = c.get(Calendar.DAY_OF_WEEK) - 1;
        if (dayofweek == 0)
            dayofweek = 7;
        c.add(Calendar.DATE, -dayofweek + 7);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(c.getTime());
    }

    /** 格式金额 */
    public static String formatMoney(double d)
    {
        DecimalFormat df = new DecimalFormat("0.00");
        return df.format(d);
    }

    /**
     * <一句话功能简述> <功能详细描述>
     *
     * @param d
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static String formatMoneyUtil(double d)
    {

        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.CHINA);
        numberFormat.setMinimumFractionDigits(2);
        return numberFormat.format(d);
    }

    /**
     * <一句话功能简述> <功能详细描述>
     *
     * @param d
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static String formatPaseMoneyUtil(Object d)
    {

        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.CHINA);
        //        numberFormat.setMinimumFractionDigits(2);
        return numberFormat.format(d);
    }

    /**
     * <一句话功能简述> <功能详细描述>
     *
     * @param d
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static String formatPaseMoney(Object d)
    {

        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.CHINA);
        numberFormat.setMinimumFractionDigits(2);

        return numberFormat.format(d);
    }

    /** 金额格式 */
    public static String formatMoneyInt(double money)
    {
        double t = money * 100;
        DecimalFormat df = new DecimalFormat("0");
        return df.format(t);
    }

    public static String formatMoneyByInt(int money)
    {
        double t = money / 100d;
        /*
         * DecimalFormat df = new DecimalFormat("0.00"); String m =
         * df.format(t);
         */
        String m = t + "";
        int idx = m.lastIndexOf(".");
        String sub = m.substring(idx + 1, m.length());
        if (Integer.parseInt(sub) == 0)
        {
            if (sub.length() > 1)
            {
                return m.substring(0, m.length() - 3);
            }
            else
            {
                return m.substring(0, m.length() - 2);
            }
        }
        else
        {
            return m;
        }
    }

    /**
     * 得到当前的前一天
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static Date getBeforeDate()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -1);

        return calendar.getTime();
    }

    /** 格式化为MM月 dd日 */
    public static String formatMMDD(long timestamp)
    {
        SimpleDateFormat sdf1 = new SimpleDateFormat("MM.dd");
        return sdf1.format(timestamp);
    }

    /**
     *
     * @param d
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static String formatMoneyUtils(double d)
    {
        d = d / 100;
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.CHINA);
        numberFormat.setMinimumFractionDigits(2);
        return numberFormat.format(d);
    }

    /**
     *
     * @param d
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static String formatMoneyUtilDigist(double d)
    {
        d = d / 100;
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.CHINA);
        numberFormat.setMinimumFractionDigits(2);
        return numberFormat.format(d);
    }

    public static String formartDateYYMMDDTo(Long date)
            throws ParseException
    {
        SimpleDateFormat sdf = new SimpleDateFormat("MM月dd日");
        return sdf.format(date);
    }

    public static String formartDateYYMMDDTo(String date)
            throws ParseException
    {
        SimpleDateFormat sdf = new SimpleDateFormat("MM月dd日");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(df.parse(date));
    }

    public static String formartDateYYMMDD(String date)
            throws ParseException
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(df.parse(date));
    }

    public static String formartDateToHHMMSS(String date)
            throws ParseException
    {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(df.parse(date));
    }

    public static String formartDateToMMDD(String date)
            throws ParseException
    {
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(df.parse(date));
    }

    /**
     * 根据当前时间得到6个月前的
     * @param date
     * @return
     * @throws ParseException
     * @see [类、类#方法、类#成员]
     */
    public static String getSixMonthodDate()
            throws ParseException
    {
        Calendar ca = Calendar.getInstance();//得到一个Calendar的实例 
        ca.add(Calendar.MONTH, -5); //月份减1 
        ca.add(Calendar.DAY_OF_MONTH, -1);
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        return sf.format(ca.getTime());
    }

    /**
     * 将long类型转换成年月日时分形式
     * @param value
     * @return
     */
    public static String getNormalTime(long value) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm") ;
        String time = format.format(new Date(value)) ;
        return time;
    }

    /**
     * 将long类型转换成年月日时分秒形式
     * @param value
     * @return
     */
    public static String getAllNormalTime(long value) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss") ;
        String time = format.format(new Date(value)) ;
        return time;
    }



}
