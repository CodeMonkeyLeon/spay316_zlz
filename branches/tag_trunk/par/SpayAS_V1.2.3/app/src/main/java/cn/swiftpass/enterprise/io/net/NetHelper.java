package cn.swiftpass.enterprise.io.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.FileUtils;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.TimeZoneUtil;

/**
 * Created with IntelliJ IDEA.
 * User: Alan
 * Date: 13-9-20
 * Time: 下午7:35
 * To change this template use File | Settings | File Templates.
 */
public class NetHelper {

    public static final String TAG = "NetHelper";

    private static final CookieStore COOKIE_STORE = new BasicCookieStore();

    public static RequestResult post(String url, JSONObject postData) {
        RequestResult result = post(url, postData, null, false);
        MainApplication.getContext().filterResponse(result);
        return result;
    }

    public static RequestResult post(String url, JSONObject postData, JSONObject getData) {
        RequestResult result = post(url, postData, getData, false);
        MainApplication.getContext().filterResponse(result);
        return result;
    }

    private static RequestResult post(String url, JSONObject postData, JSONObject getData, boolean returnArray) {
        RequestResult result = new RequestResult();
        if (!chkStatus()) {
            result.resultCode = RequestResult.RESULT_BAD_NETWORK;
            return result;
        }
        StringBuilder sb = new StringBuilder();
        if (!url.contains("?")) {
            sb.append("?client_type=android");
        }
        if (getData != null) {
            Iterator it = getData.keys();
            while (it != null && it.hasNext()) {
                sb.append("&");
                String key = (String) it.next();
                sb.append(key);
                sb.append("=");
                sb.append(getData.opt(key));
            }
        }
        url += sb.toString();
        HttpPost request = new HttpPost(url);
        // 添加headers
        request.setHeader("User-Info", "imei=123456;imsi=456;netType=3G;android:2.2");
        request.setHeader("Authorization", "我是密码");

        if (postData != null) {
            if (GlobalConstant.isDebug) {
                Logger.i(TAG, "Data:" + postData.toString());
            }
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            Iterator<String> it = postData.keys();
            while (it.hasNext()) {
                String key = it.next();
                String value = postData.optString(key);
                params.add(new BasicNameValuePair(key, value));
            }
            try {
                request.setEntity(new UrlEncodedFormEntity(params, "utf-8"));
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }
        }
        DefaultHttpClient client = getNewHttpClient();
        client.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, GlobalConstant.DEFAULT_TIMEOUT);
        try {
            HttpResponse response = client.execute(request);
            getCookie(response);
            // Set-Cookie:sessionid=BAh7BjoPc2Vzc2lv
            // (这里设置了一个Cookie,以后所有请求都必须携带这个值
            if (response == null || response.getStatusLine().getStatusCode() != 200) {
                result.resultCode = RequestResult.RESULT_READING_ERROR;
                if (response != null) {
                    result.setMessage("Server error:" + response.getStatusLine().getStatusCode());
                }
                return result;
            }
            String str = EntityUtils.toString(response.getEntity());

            if (TextUtils.isEmpty(str)) {
                return result;
            }
            try {
                if (returnArray) {
                    result.arr = new JSONArray(str);
                } else {
                    try {
                        result.data = new JSONObject(str);
                        int status = result.data.optInt("Ret");
                        if (status != 1) {
                            result.setMessage(result.data.optString("ErrInfo"));
                        }
                    } catch (Exception e) {
                        Log.e(TAG,Log.getStackTraceString(e));
                    }
                }
            } catch (JSONException e) {
                Log.e(TAG,Log.getStackTraceString(e));
                result.resultCode = RequestResult.RESULT_INTERNAL_ERROR;
            }
            return result;
        } catch (IOException e) {
            Log.e(TAG,Log.getStackTraceString(e));
            result.resultCode = RequestResult.RESULT_READING_ERROR;
        }
        return result;
    }

    public static RequestResult get(String url, JSONObject data) {
        RequestResult result = get(url, data, false);
        MainApplication.getContext().filterResponse(result);
        return result;
    }


    public static RequestResult get(String url, JSONObject data, boolean returnArray) {
        RequestResult result = new RequestResult();
        if (!chkStatus()) {
            result.resultCode = RequestResult.RESULT_BAD_NETWORK;
            return result;
        }
        StringBuilder sb = new StringBuilder();
        if (!url.contains("?")) {
            // sb.append("?client_type=android");
            String param;
            try {
                param = URLEncoder.encode(data.toString(), "utf-8");
                sb.append("?data=" + param);
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                Log.e(TAG,Log.getStackTraceString(e));
            }

        }
        if (data != null) {
            Iterator it = data.keys();
            while (it != null && it.hasNext()) {
                sb.append("&");
                String key = (String) it.next();
                sb.append(key);
                sb.append("=");
                sb.append(data.opt(key));
            }
        }
        url += sb.toString();

        HttpGet request = new HttpGet(url);
        // 添加headers
        //        request.setHeader("User-Info", "imei=123456;imsi=456;netType=3G;android:2.2");
        //        request.setHeader("Authorization", "我是密码");
        //        if (GlobalConstant.cookitValue != null)
        //        {
        //            // 返回协议中带这个参数才需要设置
        //            request.setHeader("Cookie", GlobalConstant.cookitValue);
        //        }
        DefaultHttpClient client = getNewHttpClient();
        client.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, GlobalConstant.DEFAULT_TIMEOUT);
        try {
            HttpResponse response = client.execute(request);
            if (response == null || response.getStatusLine().getStatusCode() != 200) {
                result.resultCode = RequestResult.RESULT_READING_ERROR;
                return result;
            }
            //            getCookie(response);
            String str = EntityUtils.toString(response.getEntity());
            if (TextUtils.isEmpty(str)) {
                return result;
            }
            try {

                if (returnArray) {
                    result.arr = new JSONArray(str);
                } else {
                    try {
                        JSONObject d = new JSONObject(str);
                        int status = d.optInt("Ret");
                        result.data = d.optJSONObject("returnData");
                        if (status != 1) {
                            result.setMessage(result.data.optString("ErrInfo"));
                        }
                    } catch (Exception e) {
                        Log.e(TAG,Log.getStackTraceString(e));
                    }
                }
            } catch (JSONException e) {
                Log.e(TAG,Log.getStackTraceString(e));
                result.resultCode = RequestResult.RESULT_INTERNAL_ERROR;
            }
            return result;
        } catch (ConnectTimeoutException timeout) {
            result.resultCode = RequestResult.RESULT_TIMEOUT_ERROR;
        } catch (IOException e) {
            Log.e(TAG,Log.getStackTraceString(e));
            result.resultCode = RequestResult.RESULT_READING_ERROR;
        }
        return result;
    }

    public static RequestResult httpsGet(String url, JSONObject data, boolean returnArray) {
        RequestResult result = new RequestResult();
        if (!chkStatus()) {
            result.resultCode = RequestResult.RESULT_BAD_NETWORK;
            return result;
        }
        StringBuilder sb = new StringBuilder();
        if (!url.contains("?")) {
            sb.append("?client_type=android");
        }
        if (data != null) {
            Iterator it = data.keys();
            while (it != null && it.hasNext()) {
                sb.append("&");
                String key = (String) it.next();
                sb.append(key);
                sb.append("=");
                sb.append(data.opt(key));
            }
        }
        url += sb.toString();

        HttpGet request = new HttpGet(url);

        if (GlobalConstant.cookitValue != null) {
            // 返回协议中带这个参数才需要设置
            request.setHeader("Cookie", GlobalConstant.cookitValue);
        }

        DefaultHttpClient client = getNewHttpClient();

        client.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, GlobalConstant.DEFAULT_TIMEOUT);

        try {

            HttpResponse response = client.execute(request);

            if (response == null || response.getStatusLine().getStatusCode() != 200) {

                result.resultCode = RequestResult.RESULT_READING_ERROR;
                return result;
            }
            getCookie(response);
            String str = EntityUtils.toString(response.getEntity(), "UTF-8");
            if (TextUtils.isEmpty(str)) {
                return result;
            }
            try {

                if (returnArray) {
                    result.arr = new JSONArray(str);
                } else {
                    try {
                        JSONObject d = new JSONObject(str);
                        int status = d.optInt("Ret");
                        result.data = d.optJSONObject("returnData");
                        if (status != 1) {
                            result.setMessage(result.data.optString("ErrInfo"));
                        }
                    } catch (Exception e) {
                        //  Log.e(TAG,Log.getStackTraceString(e));
                        result.tag = str;
                    }
                }
            } catch (JSONException e) {
                Log.e(TAG,Log.getStackTraceString(e));
                result.resultCode = RequestResult.RESULT_INTERNAL_ERROR;
            }
            return result;
        } catch (ConnectTimeoutException timeout) {
            result.resultCode = RequestResult.RESULT_TIMEOUT_ERROR;
        } catch (IOException e) {
            Log.e(TAG,Log.getStackTraceString(e));
            result.resultCode = RequestResult.RESULT_READING_ERROR;
        }
        return result;
    }

    public static String httpsGet(String url) {
        RequestResult result = new RequestResult();
        if (!chkStatus()) {
            result.resultCode = RequestResult.RESULT_BAD_NETWORK;
            return "";
        }
        StringBuilder sb = new StringBuilder();

        //        if (data != null)
        //        {
        //            Iterator it = data.keys();
        //            while (it != null && it.hasNext())
        //            {
        //                sb.append("&");
        //                String key = (String)it.next();
        //                sb.append(key);
        //                sb.append("=");
        //                sb.append(data.opt(key));
        //            }
        //        }
        //        if (url.indexOf("?") > 0)
        //        {
        //            url += sb.toString();
        //        }
        //        else
        //        {
        //            sb.setCharAt(0, '?');
        //            url += sb.toString();
        //        }

        HttpGet request = new HttpGet(url);
        DefaultHttpClient client = getNewHttpClient();

        client.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, GlobalConstant.DEFAULT_TIMEOUT);

        try {

            HttpResponse response = client.execute(request);

            if (response == null || response.getStatusLine().getStatusCode() != 200) {

                result.resultCode = RequestResult.RESULT_READING_ERROR;
                return null;
            }
            //            getCookie(response);
            String str = EntityUtils.toString(response.getEntity());
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            //            result.tag = str;
            return str;
        } catch (ConnectTimeoutException timeout) {
            result.resultCode = RequestResult.RESULT_TIMEOUT_ERROR;
        } catch (IOException e) {
            Log.e(TAG,Log.getStackTraceString(e));
            result.resultCode = RequestResult.RESULT_READING_ERROR;
        }
        return null;
    }

    public static RequestResult upload(String fileKey, String filePath, String url, JSONObject data) {
        RequestResult result = upload(fileKey, filePath, url, data, false);
        MainApplication.getContext().filterResponse(result);
        return result;
    }

    private static DefaultHttpClient getNewHttpClient() {
        DefaultHttpClient httpClient = null;
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);
            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            //            sf.setHostnameVerifier(SSLSocketFactory.STRICT_HOSTNAME_VERIFIER);
            sf.setHostnameVerifier(SSLSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));
            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);
            //ccm.closeExpiredConnections();
            httpClient = new DefaultHttpClient(ccm, params);

        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
            httpClient = new DefaultHttpClient();
        }
        httpClient.setCookieStore(COOKIE_STORE);
        return httpClient;
    }

    public static class MySSLSocketFactory extends SSLSocketFactory {
        SSLContext sslContext = SSLContext.getInstance("TLS");

        public MySSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
            super(truststore);
            TrustManager tm = new X509TrustManager() {

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                    try {
                        chain[0].checkValidity();
                    } catch (Exception e) {
                        throw new CertificateException("Certificate not valid or trusted.");
                    }
                }
            };
            sslContext.init(null, new TrustManager[]{tm}, null);
        }

        @Override
        public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
            return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
        }

        @Override
        public Socket createSocket() throws IOException {
            return sslContext.getSocketFactory().createSocket();
        }
    }

    public static RequestResult httpsPost(String url, JSONObject postData) {
        return httpsPost(url, postData, null, null);
    }

    public static RequestResult httpsPostBanckArrJson(String url, JSONObject postData, String fileName, String filePath) {
        RequestResult result = new RequestResult();
        if (!chkStatus()) {
            result.resultCode = RequestResult.RESULT_BAD_NETWORK;
            return result;
        }
        HttpPost request = new HttpPost(url);

        // Logger.i("url:"+url);
        //request.addHeader("User-Info", getDeviceInfo());
        MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
        if (postData != null) {
            try {
                entity.addPart("data", new StringBody(postData.toString(), Charset.forName("utf-8")));
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }
        }
        if (!TextUtils.isEmpty(filePath)) {
            File fileToUpload = new File(filePath);
            FileBody fileBody = null;
            // 文件不存在
            if (!fileToUpload.exists()) {
                return null;
            } else {
                fileBody = new FileBody(fileToUpload, "image/jpeg");
            }
            entity.addPart("fileData", fileBody);
        }
        request.setEntity(entity);

        DefaultHttpClient client = getNewHttpClient();
        try {
            client.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, GlobalConstant.DEFAULT_TIMEOUT);
            client.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 30000);
            HttpResponse response = null;
            try {
                // 超时捕获
                response = client.execute(request);
            } catch (SocketTimeoutException e) // 超时异常
            {
                result.resultCode = RequestResult.RESULT_TIMEOUT_ERROR;

                return result;
            } catch (UnknownHostException e1) // 连接服务器超时
            {
                result.resultCode = RequestResult.RESULT_READING_ERROR;

                return result;
            } catch (ConnectTimeoutException e) {
                result.resultCode = RequestResult.RESULT_TIMEOUT_ERROR;

                return result;
            }

            if (response == null) {
                result.resultCode = RequestResult.RESULT_READING_ERROR;
                return result;
            }
            int statusCode = response.getStatusLine().getStatusCode();

            String str = EntityUtils.toString(response.getEntity());

            if (statusCode >= 400 && statusCode <= 499) {
                result.resultCode = statusCode;
                return result;
            } else if (statusCode >= 500 && statusCode <= 599) {
                result.resultCode = statusCode;
                return result;
            }
            if (TextUtils.isEmpty(str)) {
                return result;
            }
            //getCookie(response);
            try {
                result.arr = new JSONArray(str);
            } catch (JSONException e) {
                result.resultCode = RequestResult.RESULT_INTERNAL_ERROR;
            }
            return result;
        } catch (IOException e) {
            //            Log.e(TAG,Log.getStackTraceString(e));
            result.resultCode = RequestResult.RESULT_READING_ERROR;
        }
        return result;
    }


    public static RequestResult httpsPost(String url, JSONObject postData, String spayRs, String filePath) {
        RequestResult result = new RequestResult();
        if (!chkStatus()) {
            result.resultCode = RequestResult.RESULT_BAD_NETWORK;
            return result;
        }
        HttpPost request = new HttpPost(url);

        MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
        if (postData != null) {
            try {
                entity.addPart("data", new StringBody(postData.toString(), Charset.forName("utf-8")));
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }
        }
        if (!TextUtils.isEmpty(filePath)) {
            File fileToUpload = new File(filePath);
            FileBody fileBody = null;
            // 文件不存在
            if (!fileToUpload.exists()) {
                return null;
            } else {
                fileBody = new FileBody(fileToUpload, "image/jpeg");
            }
            entity.addPart("fileData", fileBody);
            //            request.setHeader("Content-Type", "multipart/form-data");
        }

        request.setHeader("User-Agent", String.format("%s/%s (Linux; Android %s; %s Build/%s)", ApiConstant.APK_NAME, "3.5", Build.VERSION.RELEASE, Build.MANUFACTURER, Build.ID));
        // 设置这个属性fp-lang  表示语言

        String language = PreferenceUtil.getString("language", "");

        //        Locale locale = MainApplication.getContext().getResources().getConfiguration().locale; //zh-rHK
        //        String lan = locale.getCountry();
        String langu = "en_us";
        if (!StringUtil.isEmptyOrNull(language)) {
            request.setHeader("fp-lang", PreferenceUtil.getString("language", MainApplication.LANG_CODE_EN_US));
            langu = PreferenceUtil.getString("language", MainApplication.LANG_CODE_EN_US);
        } else {
            String lan = Locale.getDefault().toString();

            if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN)) {
                request.setHeader("fp-lang", MainApplication.LANG_CODE_ZH_CN);
                langu = MainApplication.LANG_CODE_ZH_CN;
            } else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO) | lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW)) {
                request.setHeader("fp-lang", MainApplication.LANG_CODE_ZH_TW);
                langu = MainApplication.LANG_CODE_ZH_TW;
            } else {
                request.setHeader("fp-lang", MainApplication.LANG_CODE_EN_US);
                langu = MainApplication.LANG_CODE_EN_US;
            }
        }
        try {

            String key = "sp-" + "Android" + "-" + AppHelper.getImei(MainApplication.getContext());
            String value = AppHelper.getVerCode(MainApplication.getContext()) + "," + ApiConstant.bankCode + "," + langu + "," + AppHelper.getAndroidSDKVersionName() + "," + DateUtil.formatTime(System.currentTimeMillis()) + "," + "Android";
            request.setHeader(key, value);
        } catch (Exception e5) {

        }

        request.setHeader("Accept-Encoding", "gzip");
        //增加版本号
        request.setHeader("interface-version", "2");
        //带上手机默认时区
        request.setHeader("timezone", TimeZoneUtil.getCurrentTimeZone());
        //添加cookic
        String login_skey = PreferenceUtil.getString("login_skey", "");
        String login_sauthid = PreferenceUtil.getString("login_sauthid", "");
        if (!StringUtil.isEmptyOrNull(login_skey) && !StringUtil.isEmptyOrNull(login_sauthid)) {
            request.setHeader("SKEY", login_skey);
            request.setHeader("SAUTHID", login_sauthid);
            request.setHeader("ELETYPE", "terminal");
        }
        if (!StringUtil.isEmptyOrNull(spayRs)) {
            request.setHeader("spayRs", spayRs);
        }
        request.setEntity(entity);
        DefaultHttpClient client = getNewHttpClient();
        try {
            client.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 10000);
            client.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 35000);

            HttpResponse response = null;
            try {
                // 超时捕获
                response = client.execute(request);
            } catch (SocketTimeoutException e) // 超时异常
            {
                result.resultCode = RequestResult.RESULT_TIMEOUT_ERROR;

                return result;
            } catch (UnknownHostException e1) // 连接服务器超时
            {
                result.resultCode = RequestResult.RESULT_READING_ERROR;

                return result;
            } catch (ConnectTimeoutException e) {
                result.resultCode = RequestResult.RESULT_TIMEOUT_ERROR;

                return result;
            } catch (Exception e) {
                result.resultCode = RequestResult.RESULT_READING_ERROR;

                return result;
            }

            if (response == null) {
                result.resultCode = RequestResult.RESULT_READING_ERROR;
                return result;
            }
            int statusCode = response.getStatusLine().getStatusCode();

            String str = "";
            if (statusCode == 200) {
                boolean isGzip = false;
                Header[] headers = response.getHeaders("Content-Encoding");
                for (Header header : headers) {
                    if ("gzip".equals(header.getValue())) {
                        isGzip = true;
                        break;
                    }
                }
                /*if (isGzip) {
                    // 解压缩
                    try {
                        str = parseGzip(response.getEntity());
                    } catch (Exception e) {
                        Log.e(TAG,Log.getStackTraceString(e));
                    }
                } else {
                    str = EntityUtils.toString(response.getEntity(), "utf-8");
                }*/
                str = EntityUtils.toString(response.getEntity(), "utf-8");
                Logger.i("hehui", "response-->" + str + ",url-->" + url);
            }

            if (statusCode >= 400 && statusCode <= 499) {
                result.resultCode = statusCode;
                return result;
            } else if (statusCode >= 500 && statusCode <= 599) {
                result.resultCode = statusCode;
                return result;
            }
            if (TextUtils.isEmpty(str)) {
                return result;
            }
            try {
                result.data = new JSONObject(str);
                String resultCode = result.data.optString("result");
                if (resultCode.equals(String.valueOf(RequestResult.RESULT_NEED_LOGIN))) {
                    Logger.i("hehui", "resultCode-->" + resultCode);
                    MainApplication.getContext().setNeedLogin(true);
                    result.resultCode = RequestResult.RESULT_NEED_LOGIN;
                    return result;
                }

                //                int status = result.data.optInt("Ret");
                //                if (status != 1)
                //                {
                //                    result.setMessage(result.data.optString("ErrInfo"));
                //                }

                //
            } catch (JSONException e) {

                result.resultCode = RequestResult.RESULT_INTERNAL_ERROR;
            }
            return result;
        } catch (IOException e) {
            //            Log.e(TAG,Log.getStackTraceString(e));
            result.resultCode = RequestResult.RESULT_READING_ERROR;
        }
        return result;
    }

    public static RequestResult httpPostLoadPic(String url, JSONObject postData, String fileOne, String fileTwo) {
        RequestResult result = new RequestResult();
        if (!chkStatus()) {
            result.resultCode = RequestResult.RESULT_BAD_NETWORK;
            return result;
        }
        HttpPost request = new HttpPost(url);

        MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
        if (postData != null) {
            try {
                entity.addPart("data", new StringBody(postData.toString(), Charset.forName("utf-8")));
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }
        }
        if (!TextUtils.isEmpty(fileOne)) {
            File fileToUpload = new File(fileOne);
            FileBody fileBody = null;
            // 文件不存在
            if (!fileToUpload.exists()) {
                return null;
            } else {
                fileBody = new FileBody(fileToUpload, "image/jpeg");
            }
            entity.addPart("picfilezm", fileBody);
            entity.addPart("picfilefm", new FileBody(new File(fileTwo), "image/jpeg"));
        }

        request.setHeader("User-Agent", String.format("%s/%s (Linux; Android %s; %s Build/%s)", ApiConstant.APK_NAME, "3.5", Build.VERSION.RELEASE, Build.MANUFACTURER, Build.ID));
        // 设置这个属性fp-lang  表示语言

        String language = PreferenceUtil.getString("language", "");

        //        Locale locale = MainApplication.getContext().getResources().getConfiguration().locale; //zh-rHK
        //        String lan = locale.getCountry();
        String langu = "en_us";
        if (!StringUtil.isEmptyOrNull(language)) {
            request.setHeader("fp-lang", PreferenceUtil.getString("language", MainApplication.LANG_CODE_EN_US));
            langu = PreferenceUtil.getString("language", MainApplication.LANG_CODE_EN_US);
        } else {
            String lan = Locale.getDefault().toString();
            if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN)) {
                request.setHeader("fp-lang", MainApplication.LANG_CODE_ZH_CN);
                langu = MainApplication.LANG_CODE_ZH_CN;
            } else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO) | lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW)) {
                request.setHeader("fp-lang", MainApplication.LANG_CODE_ZH_TW);
                langu = MainApplication.LANG_CODE_ZH_TW;
            } else {
                request.setHeader("fp-lang", MainApplication.LANG_CODE_EN_US);
                langu = MainApplication.LANG_CODE_EN_US;
            }
        }
        try {

            String key = "sp-" + "Android" + "-" + AppHelper.getImei(MainApplication.getContext());

            String value = AppHelper.getVerCode(MainApplication.getContext()) + "," + ApiConstant.bankCode + "," + langu + "," + AppHelper.getAndroidSDKVersionName() + "," + DateUtil.formatTime(System.currentTimeMillis()) + "," + "Android";

            request.setHeader(key, value);

            String login_skey = PreferenceUtil.getString("login_skey", "");
            String login_sauthid = PreferenceUtil.getString("login_sauthid", "");

            if (!StringUtil.isEmptyOrNull(login_skey) && !StringUtil.isEmptyOrNull(login_sauthid)) {
                request.setHeader("SKEY", login_sauthid);
                request.setHeader("SAUTHID", login_sauthid);
                request.setHeader("ELETYPE", "terminal");
            }
        } catch (Exception e5) {

        }
        request.setEntity(entity);
        DefaultHttpClient client = getNewHttpClient();
        try {
            client.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 10000);
            client.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 30000);

            HttpResponse response = null;
            try {
                // 超时捕获
                response = client.execute(request);
            } catch (SocketTimeoutException e) // 超时异常
            {
                result.resultCode = RequestResult.RESULT_TIMEOUT_ERROR;

                return result;
            } catch (UnknownHostException e1) // 连接服务器超时
            {
                result.resultCode = RequestResult.RESULT_READING_ERROR;

                return result;
            } catch (ConnectTimeoutException e) {
                result.resultCode = RequestResult.RESULT_TIMEOUT_ERROR;

                return result;
            } catch (Exception e) {
                result.resultCode = RequestResult.RESULT_READING_ERROR;

                return result;
            }

            if (response == null) {
                result.resultCode = RequestResult.RESULT_READING_ERROR;
                return result;
            }
            int statusCode = response.getStatusLine().getStatusCode();

            String str = EntityUtils.toString(response.getEntity());

            Logger.i("hehui", "response-->" + str + ",url-->" + url);
            if (statusCode >= 400 && statusCode <= 499) {
                result.resultCode = statusCode;
                return result;
            } else if (statusCode >= 500 && statusCode <= 599) {
                result.resultCode = statusCode;
                return result;
            }
            if (TextUtils.isEmpty(str)) {
                return result;
            }
            try {
                result.data = new JSONObject(str);
                String resultCode = result.data.optString("result");
                if (resultCode.equals(String.valueOf(RequestResult.RESULT_NEED_LOGIN))) {

                    MainApplication.getContext().setNeedLogin(true);
                    result.resultCode = RequestResult.RESULT_NEED_LOGIN;
                    return result;
                }

                //                int status = result.data.optInt("Ret");
                //                if (status != 1)
                //                {
                //                    result.setMessage(result.data.optString("ErrInfo"));
                //                }

                //
            } catch (JSONException e) {

                result.resultCode = RequestResult.RESULT_INTERNAL_ERROR;
            }
            return result;
        } catch (IOException e) {
            //            Log.e(TAG,Log.getStackTraceString(e));

            result.resultCode = RequestResult.RESULT_READING_ERROR;
        }
        return result;
    }

    /**
     * 解Gzip压缩
     *
     * @throws IOException
     * @throws IllegalStateException
     */
   /* private static String parseGzip(HttpEntity entity) throws Exception {
        InputStream in = entity.getContent();
        GZIPInputStream gzipInputStream = new GZIPInputStream(in);
        BufferedReader reader = new BufferedReader(new InputStreamReader(gzipInputStream, HTTP.UTF_8));
        String line = null;
        StringBuffer sb = new StringBuffer();
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        return sb.toString();
    }*/

    public static RequestResult httpsPost(String url, String postData, String spayRs, String filePath) {
        RequestResult result = new RequestResult();
        if (!chkStatus()) {
            result.resultCode = RequestResult.RESULT_BAD_NETWORK;
            return result;
        }
        HttpPost request = new HttpPost(url);

        MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
        if (!StringUtil.isEmptyOrNull(postData)) {
            try {
                entity.addPart("data", new StringBody(postData, Charset.forName("utf-8")));
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }
        }
        if (!TextUtils.isEmpty(filePath)) {
            File fileToUpload = new File(filePath);
            FileBody fileBody = null;
            // 文件不存在
            if (!fileToUpload.exists()) {
                return null;
            } else {
                fileBody = new FileBody(fileToUpload, "image/jpeg");
            }
            entity.addPart("fileData", fileBody);
        }
        request.setHeader("User-Agent", String.format("%s/%s (Linux; Android %s; %s Build/%s)", ApiConstant.APK_NAME, "3.5", Build.VERSION.RELEASE, Build.MANUFACTURER, Build.ID));
        // 设置这个属性fp-lang  表示语言

        String language = PreferenceUtil.getString("language", "");

        String langu = "en_us";
        if (!StringUtil.isEmptyOrNull(language)) {
            request.setHeader("fp-lang", PreferenceUtil.getString("language", MainApplication.LANG_CODE_EN_US));
            langu = PreferenceUtil.getString("language", MainApplication.LANG_CODE_EN_US);
        } else {
            String lan = Locale.getDefault().toString();
            if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN)) {
                request.setHeader("fp-lang", MainApplication.LANG_CODE_ZH_CN);
                langu = MainApplication.LANG_CODE_ZH_CN;
            } else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO) | lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW)) {
                request.setHeader("fp-lang", MainApplication.LANG_CODE_ZH_TW);
                langu = MainApplication.LANG_CODE_ZH_TW;
            } else {
                request.setHeader("fp-lang", MainApplication.LANG_CODE_EN_US);
                langu = MainApplication.LANG_CODE_EN_US;
            }
        }
        try {

            String key = "sp-" + "Android" + "-" + AppHelper.getImei(MainApplication.getContext());
            String value = AppHelper.getVerCode(MainApplication.getContext()) + "," + ApiConstant.bankCode + "," + langu + "," + AppHelper.getAndroidSDKVersionName() + "," + DateUtil.formatTime(System.currentTimeMillis()) + "," + "Android";
            request.setHeader(key, value);
        } catch (Exception e5) {

        }
        request.setHeader("Accept-Encoding", "gzip");
        request.setHeader("timezone", TimeZoneUtil.getCurrentTimeZone());
        //添加cookic
        String login_skey = PreferenceUtil.getString("login_skey", "");
        String login_sauthid = PreferenceUtil.getString("login_sauthid", "");

        if (!StringUtil.isEmptyOrNull(login_skey) && !StringUtil.isEmptyOrNull(login_sauthid)) {
            request.setHeader("SKEY", login_skey);
            request.setHeader("SAUTHID", login_sauthid);
            request.setHeader("ELETYPE", "terminal");
        }

        if (!StringUtil.isEmptyOrNull(spayRs)) {
            request.setHeader("spayRs", spayRs);
        }
        request.setEntity(entity);
        DefaultHttpClient client = getNewHttpClient();
        try {
            client.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 10000);
            client.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 30000);

            HttpResponse response = null;
            try {
                // 超时捕获
                response = client.execute(request);

                //                response.getHeaders("Content-Encoding");
            } catch (SocketTimeoutException e) // 超时异常
            {
                result.resultCode = RequestResult.RESULT_TIMEOUT_ERROR;

                return result;
            } catch (UnknownHostException e1) // 连接服务器超时
            {
                result.resultCode = RequestResult.RESULT_READING_ERROR;

                return result;
            } catch (ConnectTimeoutException e) {
                result.resultCode = RequestResult.RESULT_TIMEOUT_ERROR;

                return result;
            } catch (Exception e) {
                result.resultCode = RequestResult.RESULT_READING_ERROR;

                return result;
            }

            if (response == null) {
                result.resultCode = RequestResult.RESULT_READING_ERROR;
                return result;
            }
            int statusCode = response.getStatusLine().getStatusCode();
            String str = "";
            if (statusCode == 200) {
                boolean isGzip = false;
                Header[] headers = response.getHeaders("Content-Encoding");
                for (Header header : headers) {
                    if ("gzip".equals(header.getValue())) {
                        isGzip = true;
                        break;
                    }
                }
                /*  if (isGzip) {
                  // 解压缩
                    try {
                        str = parseGzip(response.getEntity());
                    } catch (Exception e) {
                        Log.e(TAG,Log.getStackTraceString(e));
                    }
                } else {
                    str = EntityUtils.toString(response.getEntity(), "utf-8");
                }*/
                str = EntityUtils.toString(response.getEntity(), "utf-8");

                Logger.i("hehui", "response-->" + str + ",url-->" + url);
            }
            if (statusCode >= 400 && statusCode <= 499) {
                result.resultCode = statusCode;
                return result;
            } else if (statusCode >= 500 && statusCode <= 599) {
                result.resultCode = statusCode;
                return result;
            }
            if (TextUtils.isEmpty(str)) {
                return result;
            }
            try {
                result.data = new JSONObject(str);
                String resultCode = result.data.optString("result");
                if (resultCode.equals(String.valueOf(RequestResult.RESULT_NEED_LOGIN))) {

                    MainApplication.getContext().setNeedLogin(true);
                    result.resultCode = RequestResult.RESULT_NEED_LOGIN;
                    return result;
                }

                //                int status = result.data.optInt("Ret");
                //                if (status != 1)
                //                {
                //                    result.setMessage(result.data.optString("ErrInfo"));
                //                }

                //
            } catch (JSONException e) {

                result.resultCode = RequestResult.RESULT_INTERNAL_ERROR;
            }
            return result;
        } catch (IOException e) {
            //            Log.e(TAG,Log.getStackTraceString(e));
            result.resultCode = RequestResult.RESULT_READING_ERROR;
        }
        return result;
    }

    private static RequestResult upload(String fileKey, String filePath, String url, JSONObject data, boolean invalid) {
        RequestResult result = new RequestResult();
        if (!chkStatus()) {
            result.resultCode = RequestResult.RESULT_BAD_NETWORK;
            return result;
        }
        HttpPost request = new HttpPost(url);

        MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
        if (data != null) {

            Iterator<String> it = data.keys();
            while (it.hasNext()) {
                String key = it.next();
                String value = data.optString(key);
                if (TextUtils.isEmpty(value)) {
                    continue;
                }
                try {
                    entity.addPart(key, new StringBody(value));
                } catch (UnsupportedEncodingException e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
            }
        }
        File fileToUpload = new File(filePath);
        FileBody fileBody = new FileBody(fileToUpload, "application/octet-stream");
        entity.addPart(fileKey, fileBody);
        request.setEntity(entity);
        DefaultHttpClient client = new DefaultHttpClient();
        client.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, GlobalConstant.DEFAULT_TIMEOUT);
        try {
            HttpResponse response = client.execute(request);
            if (response == null || response.getStatusLine().getStatusCode() != 200) {
                result.resultCode = RequestResult.RESULT_READING_ERROR;
                return result;
            }
            String str = EntityUtils.toString(response.getEntity());

            if (TextUtils.isEmpty(str)) {
                return result;
            }
            try {
                result.data = new JSONObject(str);
                int status = result.data.optInt("Ret");
                if (status != 1) {
                    result.setMessage(result.data.optString("ErrInfo"));
                }
            } catch (JSONException e) {
                Log.e(TAG,Log.getStackTraceString(e));
                result.resultCode = RequestResult.RESULT_INTERNAL_ERROR;
            }
            return result;
        } catch (IOException e) {
            Log.e(TAG,Log.getStackTraceString(e));
            result.resultCode = RequestResult.RESULT_READING_ERROR;
        }
        return result;
    }

    /*public static RequestResult upload(String addr, String file, String u, String p) {
        addr = addr + "?apiCode=";// +GlobalConstant.apiCode + "&client=" +
        // GlobalConstant.CLIENT + "&account="+u +
        // "&password="+p;

        RequestResult result = new RequestResult();
        HttpURLConnection connection = null;
        DataOutputStream outputStream = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        try {

            FileInputStream fileInputStream = new FileInputStream(new File(file));
            URL url = new URL(addr);
            connection = (HttpURLConnection) url.openConnection();
            // Allow Inputs & Outputs
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            // Enable POST method
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);
            outputStream.writeBytes("Content-Disposition: form-data; name=\"fileField\";filename=\"" + file + "\"" + lineEnd);
            outputStream.writeBytes(lineEnd);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];
            // Read file
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                outputStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }
            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
            // Responses from the server (code and message)
            String serverResponseCode = Integer.toString(connection.getResponseCode());
            String serverResponseMessage = connection.getResponseMessage();
            int responseCode = Utils.Integer.tryParse(serverResponseCode, 0);
            if (responseCode != 200) {
                result.resultCode = RequestResult.RESULT_INTERNAL_ERROR;
                result.setMessage(serverResponseMessage);
            }
            *//*  if (GlobalConstant.isDebug) {
                  Logger.i(TAG, "Uploaded: ");
                  Logger.i(TAG, serverResponseCode + "  " + serverResponseMessage);
              }*//*
            InputStreamReader isr = new InputStreamReader(connection.getInputStream());
            BufferedReader br = new BufferedReader(isr);
            StringBuffer sb = new StringBuffer();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            isr.close();
            br.close();
            fileInputStream.close();
            outputStream.flush();
            outputStream.close();

            result.data = new JSONObject(sb.toString());
            result.resultCode = result.data.optInt("error_code");
            result.setMessage(result.data.optString("error"));
        } catch (Exception ex) {
            Log.e(TAG,Log.getStackTraceString(ex));
            result.resultCode = RequestResult.RESULT_INTERNAL_ERROR;
        }
        return result;
    }*/

   /* public static byte[] getImage(String path) throws Exception {
        URL url = new URL(path);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setConnectTimeout(5 * 1000);
        conn.setRequestMethod("GET");
        InputStream inStream = conn.getInputStream();
        if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
            return readStream(inStream);
        }
        return null;
    }*/

    // 下载图片
    /*public static Bitmap getBitmap(String path) throws Exception {
        byte[] data = getImage(path);
        if (data == null) {
            return null;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true; //首先设置.inJustDecodeBounds为true
        BitmapFactory.decodeByteArray(data, 0, data.length, options);
        options.inJustDecodeBounds = false; // 得到图片有宽和高的options对象后，设置.inJustDecodeBounds为false。
        int be = (int) (options.outHeight / (float) 200);
        if (be <= 0) be = 1;
        options.inSampleSize = be;
        return BitmapFactory.decodeByteArray(data, 0, data.length, options); //这时获取到的bitmap是null的，尚未调用系统内存资源
    }*/

    public static byte[] readStream(InputStream inStream) {
        ByteArrayOutputStream outStream = null;
        try {
            outStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int len = 0;
            while ((len = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, len);
            }
            return outStream.toByteArray();
        } catch (Exception e) {
            return null;
        } finally {
            FileUtils.closeIO(outStream, inStream);
        }
    }

    public static boolean chkStatus() {
        final ConnectivityManager connMgr = (ConnectivityManager) MainApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (!wifi.isAvailable() && !mobile.isAvailable()) {
            return false;
        }
        return true;
    }

    private static void getCookie(HttpResponse response) {
        Header[] headers = response.getHeaders("Set-Cookie");
        if (null != headers && headers.length > 0) {
            Header header = headers[0];
            Logger.i(TAG, "Response:header" + header.getName() + " value:" + header.getValue());
            String strCookie = header.getValue();

            String[] strs = strCookie.split(";");
            GlobalConstant.cookitValue = strs[0];
        }
    }

    //    private static void addCookie(HttpRequest request)
    //    {
    //        if (MainApplication.cookieMap != null)
    //        {
    //            String value = null;
    //            
    //            for (String key : MainApplication.cookieMap.keySet())
    //            {
    //                value = key + "=" + MainApplication.cookieMap.get(key);
    //                BasicHeader basicHeader = new BasicHeader("Cookie", value);
    //                request.addHeader(basicHeader);
    //            }
    //        }
    //        
    //    }
}
