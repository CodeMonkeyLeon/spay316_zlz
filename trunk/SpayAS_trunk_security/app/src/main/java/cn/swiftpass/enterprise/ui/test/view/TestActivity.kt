package cn.swiftpass.enterprise.ui.test.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.common.Constant
import cn.swiftpass.enterprise.intl.BuildConfig
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.ui.activity.BaseActivity
import cn.swiftpass.enterprise.ui.test.adapter.DomainAdapter
import cn.swiftpass.enterprise.ui.test.entity.DomainEntity
import cn.swiftpass.enterprise.ui.test.interfaces.OnDomainItemClickListener
import com.example.common.sp.PreferenceUtil

class TestActivity : BaseActivity<BasePresenter<*>>(), View.OnClickListener {

    override fun createPresenter() = null

    companion object {
        const val HTTP = "http://"
        const val HTTPS = "https://"

        fun startTestActivity(fromActivity: Activity) {
            fromActivity.startActivity(Intent(fromActivity, TestActivity::class.java))
        }
    }


    private lateinit var mBtnBack: TextView
    private lateinit var mBtnOk: TextView

    private lateinit var mTvCurrentDomain: TextView

    private lateinit var mEtDomain: EditText

    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mAdapter: DomainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_test)
        initView()
    }


    private fun initView() {
        mBtnBack = findViewById(R.id.id_btn_back)
        mBtnOk = findViewById(R.id.id_btn_ok)
        mTvCurrentDomain = findViewById(R.id.id_tv_domain_current)
        mEtDomain = findViewById(R.id.id_et_domain)
        mRecyclerView = findViewById(R.id.id_recycler_view)

        mBtnBack.setOnClickListener(this)
        mBtnOk.setOnClickListener(this)
        mTvCurrentDomain.setOnClickListener(this)


        mTvCurrentDomain.text = MainApplication.getInstance().baseUrl

        initRecyclerView()
        initSelfDomain()

    }

    private fun initSelfDomain() {
        val selfDomain = PreferenceUtil.getString(Constant.SERVER_ADDRESS_SELF, "")
        if (!TextUtils.isEmpty(selfDomain)) {
            mEtDomain.setText(selfDomain.substring(0, selfDomain.length - 1))
        }
    }


    private fun initRecyclerView() {

        val manager = LinearLayoutManager(this)
        manager.orientation = LinearLayoutManager.VERTICAL
        mRecyclerView.layoutManager = manager


        mAdapter = DomainAdapter(this, getDomainList())
        mRecyclerView.adapter = mAdapter
        mAdapter.notifyDataSetChanged()


        mAdapter.setOnDomainItemClickListener(object : OnDomainItemClickListener {
            override fun onItemClick(position: Int, entity: DomainEntity, view: View) {
                mEtDomain.setText(entity.domain)
            }
        })
    }


    private fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }


    private fun getDomainList(): ArrayList<DomainEntity> {
        val list = ArrayList<DomainEntity>()
        list.add(DomainEntity(BuildConfig.serverAddr))
        list.add(DomainEntity(BuildConfig.serverAddrBack))
        list.add(DomainEntity(BuildConfig.serverAddrCheckoutDev))
        list.add(DomainEntity(BuildConfig.serverAddrOverseas61))
        list.add(DomainEntity(BuildConfig.serverAddrOverseas63))
        list.add(DomainEntity(BuildConfig.serverAddrDev))
        list.add(DomainEntity(BuildConfig.serverAddrTest123))
        list.add(DomainEntity(BuildConfig.serverAddrTest61))
        list.add(DomainEntity(BuildConfig.serverAddrTest63))
        list.add(DomainEntity(BuildConfig.serverAddrTestUAT))
        list.add(DomainEntity(BuildConfig.serverAddrPrd))
        list.add(DomainEntity(BuildConfig.serverAddrDevJH))
        list.forEach {
            it.domain = it.domain.substring(0, it.domain.length - 1)
        }
        return list
    }


    private fun saveChooseDomain(domain: String) {
        when (domain) {
            BuildConfig.serverAddr -> {
                //线上环境
                //cdn开启
                MainApplication.getInstance().baseUrl = BuildConfig.serverAddr
                PreferenceUtil.removeKey(Constant.SERVER_CONFIG)
            }

            BuildConfig.serverAddrBack -> {
                //线上环境
                //cdn关闭
                MainApplication.getInstance().baseUrl = BuildConfig.serverAddrBack
                PreferenceUtil.removeKey(Constant.SERVER_CONFIG)
            }

            BuildConfig.serverAddrCheckoutDev -> {
                //checkout-dev
                MainApplication.getInstance().baseUrl =
                    BuildConfig.serverAddrCheckoutDev
                PreferenceUtil.commitString(
                    Constant.SERVER_CONFIG,
                    Constant.SERVER_ADDRESS_CHECKOUT_DEV
                )
            }

            BuildConfig.serverAddrOverseas61 -> {
                //61容器化
                MainApplication.getInstance().baseUrl =
                    BuildConfig.serverAddrOverseas61
                PreferenceUtil.commitString(
                    Constant.SERVER_CONFIG,
                    Constant.SERVER_ADDRESS_OVERSEAS_61
                )
            }

            BuildConfig.serverAddrOverseas63 -> {
                //63容器化
                MainApplication.getInstance().baseUrl =
                    BuildConfig.serverAddrOverseas63
                PreferenceUtil.commitString(
                    Constant.SERVER_CONFIG,
                    Constant.SERVER_ADDRESS_OVERSEAS_63
                )
            }

            BuildConfig.serverAddrDev -> {
                //dev
                MainApplication.getInstance().baseUrl = BuildConfig.serverAddrDev
                PreferenceUtil.commitString(
                    Constant.SERVER_CONFIG,
                    Constant.SERVER_ADDRESS_DEV
                )
            }

            BuildConfig.serverAddrTest123 -> {
                //test123
                MainApplication.getInstance().baseUrl = BuildConfig.serverAddrTest123
                PreferenceUtil.commitString(
                    Constant.SERVER_CONFIG,
                    Constant.SERVER_ADDRESS_TEST_123
                )
            }

            BuildConfig.serverAddrTest61 -> {
                //test61
                MainApplication.getInstance().baseUrl = BuildConfig.serverAddrTest61
                PreferenceUtil.commitString(
                    Constant.SERVER_CONFIG,
                    Constant.SERVER_ADDRESS_TEST_61
                )
            }

            BuildConfig.serverAddrTest63 -> {
                //test63
                MainApplication.getInstance().baseUrl = BuildConfig.serverAddrTest63
                PreferenceUtil.commitString(
                    Constant.SERVER_CONFIG,
                    Constant.SERVER_ADDRESS_TEST_63
                )
            }

            BuildConfig.serverAddrTestUAT -> {
                //testUAT
                MainApplication.getInstance().baseUrl = BuildConfig.serverAddrTestUAT
                PreferenceUtil.commitString(
                    Constant.SERVER_CONFIG,
                    Constant.SERVER_ADDRESS_TEST_UAT
                )
            }

            BuildConfig.serverAddrPrd -> {
                //prd
                MainApplication.getInstance().baseUrl = BuildConfig.serverAddrPrd
                PreferenceUtil.commitString(
                    Constant.SERVER_CONFIG,
                    Constant.SERVER_ADDRESS_PRD
                )
            }

            BuildConfig.serverAddrDevJH -> {
                //dev-jh
                MainApplication.getInstance().baseUrl = BuildConfig.serverAddrDevJH
                PreferenceUtil.commitString(
                    Constant.SERVER_CONFIG,
                    Constant.SERVER_ADDRESS_DEV_JH
                )
            }

            else -> {
                //自定义域名
                MainApplication.getInstance().baseUrl = domain
                PreferenceUtil.commitString(
                    Constant.SERVER_CONFIG,
                    Constant.SERVER_ADDRESS_SELF
                )
                PreferenceUtil.commitString(
                    Constant.SERVER_ADDRESS_SELF,
                    domain
                )
            }
        }
    }


    override fun onClick(v: View?) {
        v?.let { view ->
            when (view.id) {
                R.id.id_btn_back -> {
                    //返回
                    finish()
                }

                R.id.id_btn_ok -> {
                    //确认
                    val domain = mEtDomain.text.trim().toString()
                    if (TextUtils.isEmpty(domain)) {
                        showToast("输入的域名不能为空")
                    } else {
                        if (domain.startsWith(HTTP) || domain.startsWith(HTTPS)) {
                            PreferenceUtil.commitString(
                                Constant.SERVER_ADDRESS_SELF,
                                ""
                            )
                            val d = if (domain.endsWith('/')) domain else "$domain/"
                            saveChooseDomain(d)
                            finish()
                        } else {
                            showToast("请以${HTTP}或${HTTPS}开头")
                        }
                    }
                }

            }
        }
    }
}