/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.widget;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;

import java.util.Calendar;
import java.util.Date;

import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 弹出提示框
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-11]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class DateTimePickDialogInfo extends Dialog implements OnTimeChangedListener, OnDateChangedListener
{
    
    private Context context;
    
    private TextView content;
    
    private Button btnOk, btCancel;
    
    private ViewGroup mRootView;
    
    private DateTimePickDialogInfo.HandleBtn handleBtn;
    
    private DateTimePickDialogInfo.HandleBtnCancle cancleBtn;
    
    private DateTimePickDialogInfo.HandleBtnrtw rTWBtn;
    
    private View line_img, line_img_bt;
    
    // 更多
    public static final int FLAG = 0;
    
    // 最终提交
    public static final int SUBMIT = 1;
    
    // 注册成功
    public static final int REGISTFLAG = 2;
    
    // 微信支付 是否授权激活
    public static final int EXITAUTH = 3;
    
    // 微信支付 授权激活 重新登录
    public static final int EXITAUTHLOGIN = 4;
    
    //提交商户信息
    public static final int SUBMITSHOPINFO = 5;
    
    //判断网络连接状态
    public static final int NETWORKSTATUE = 8;
    
    //优惠卷列表，长按提示删除提示框
    public static final int SUBMIT_DEL_COUPON_INFO = 6;
    
    //优惠卷发布
    public static final int SUBMIT_COUPON_INFO = 7;
    
    public static final int SUBMIT_SHOP = 9;
    
    public static final int SCAN_PAY = 10;
    
    public static final int VARD = 11;
    
    public static final int UNIFED_DIALOG = 12;
    
    private OnItemLongDelListener mDelListener;
    
    private int position;
    
    private OnSubmitCouponListener mOnSubmitCouponListener;
    
    private ImageView radioButton1, radioButton2;
    
    private RelativeLayout lay_zh, lay_rTw;
    
    private boolean lang = true; // 默认是中文
    
    private DatePicker datePicker;
    
    private TimePicker timePicker;
    
    private int title;
    
    private TextView tvTitle;
    
    private String dateTime;
    
    private String startTime;
    
    private boolean isTag = false;
    
    private String[] tiems;
    
    /**
     * title 
     * content 提示内容
     * <默认构造函数>
     */
    public DateTimePickDialogInfo(Context context, String[] tiems, String startTime, int title,
        DateTimePickDialogInfo.HandleBtn handleBtn, DateTimePickDialogInfo.HandleBtnCancle cancleBtn, boolean isTag)
    {
        super(context);
        this.title = title;
        this.startTime = startTime;
        this.isTag = isTag;
        this.tiems = tiems;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.date_time_pick_dialog, null);
        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);
        
        this.context = context;
        this.handleBtn = handleBtn;
        this.cancleBtn = cancleBtn;
        
        initView();
        
        initDateTimePick();
        
        setLinster();
        
        /**
         * 可配置设置
         */
        DynModel dynModel = (DynModel)SharedPreUtile.readProduct("dynModel" + ApiConstant.bankCode);
        if (null != dynModel)
        {
            try
            {
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonColor()))
                {
                    btnOk.setBackgroundColor(Color.parseColor(dynModel.getButtonColor()));
                    btCancel.setBackgroundColor(Color.parseColor(dynModel.getButtonColor()));
                }
                
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonFontColor()))
                {
                    btnOk.setTextColor(Color.parseColor(dynModel.getButtonFontColor()));
                    btCancel.setTextColor(Color.parseColor(dynModel.getButtonFontColor()));
                }
            }
            catch (Exception e)
            {
            }
        }
        
    }
    
    private void initDateTimePick()
    {
        Calendar calendar = Calendar.getInstance();
        if (isTag)
        {
            calendar.set(calendar.get(Calendar.YEAR),
                Integer.parseInt(tiems[0]),
                Integer.parseInt(tiems[1]),
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                calendar.get(Calendar.MILLISECOND));
        }
        else
        {
            calendar.set(calendar.get(Calendar.YEAR),
                Integer.parseInt(tiems[0]),
                Integer.parseInt(tiems[1]),
                Integer.parseInt(tiems[2]),
                Integer.parseInt(tiems[3]),
                Integer.parseInt(tiems[4]));
        }
        
        datePicker.init(calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH) - 1,
            calendar.get(Calendar.DAY_OF_MONTH),
            this);
        timePicker.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));
        timePicker.setCurrentMinute(calendar.get(Calendar.MINUTE));
        timePicker.setIs24HourView(true);
        timePicker.setOnTimeChangedListener(this);
    }
    
    public void setBtnOkText(String string)
    {
        if (btnOk != null)
        {
            btnOk.setText(string);
        }
    }
    
    public void setMessage(String msg)
    {
        this.content.setText(msg);
    }
    
    public void setmDelListener(OnItemLongDelListener mDelListener, int position)
    {
        this.mDelListener = mDelListener;
        this.position = position;
    }
    
    public void setmOnSubmitCouponListener(OnSubmitCouponListener mOnSubmitCouponListener)
    {
        this.mOnSubmitCouponListener = mOnSubmitCouponListener;
    }
    
    /**
     * 设置监听
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void setLinster()
    {
        
        btCancel.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                dismiss();
                cancel(); //Intl
            }
            
        });
        btnOk.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                
                onDateChanged(null, 0, 0, 0);
                if (!StringUtil.isEmptyOrNull(startTime))
                {//判断开始时间和结束时间不能超过30天
                    Date startDate, endDate;
                    int day;
                    if (isTag)
                    {
                        startDate = DateUtil.getStrFromDateYYMMDD(startTime);
                        endDate = DateUtil.getStrFromDateYYMMDD(dateTime);
                        day = DateUtil.getGapCounts(startDate, endDate);
                    }
                    else
                    {
                        startDate = DateUtil.getStrFromDate(startTime);
                        endDate = DateUtil.getStrFromDate(dateTime);
                        day = DateUtil.getGapCount(startDate, endDate);
                    }
                    
                    MyToast toast = new MyToast();
                    
                    if (day < 0)
                    { //
                        toast.showToast(context, ToastHelper.toStr(R.string.show_endtime_than_starttime));
                        return;
                    }
                    if (day > 30)
                    {
                        toast.showToast(context, ToastHelper.toStr(R.string.tv_settle_choice_pass_time));
                        return;
                    }
                }
                dismiss();
                cancel();
                handleBtn.handleOkBtn(dateTime);
            }
        });
    }
    
    /**
     * 初始化
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView()
    {
        tvTitle = (TextView)findViewById(R.id.title);
        tvTitle.setText(title);
        btnOk = (Button)findViewById(R.id.btnOk);
        btCancel = (Button)findViewById(R.id.btCancel);
        datePicker = (DatePicker)findViewById(R.id.datepicker);
        timePicker = (TimePicker)findViewById(R.id.timepicker);
        
        if (isTag)
        { // 为true，说明不需要设置分，秒。
            timePicker.setVisibility(View.GONE);
        }
    }
    
    public void showPage(Class clazz)
    {
        Intent intent = new Intent(context, clazz);
        context.startActivity(intent);
    }
    
    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作  
     * @author Administrator  
     *
     */
    public interface HandleBtn
    {
        void handleOkBtn(String date);
        
        //        void handleCancleBtn();
    }
    
    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作  
     * @author Administrator  
     *
     */
    public interface HandleBtnCancle
    {
        void handleCancleBtn();
    }
    
    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作  
     * @author Administrator  
     *
     */
    public interface HandleBtnrtw
    {
        void handleCancleBtn();
    }
    
    /***
     * 
     * 长按删除优惠
     * 
     * @author  wang_lin
     * @version  [2.1.0, 2014-4-23]
     */
    public interface OnItemLongDelListener
    {
        public void onItemLongDelMessage(int position);
    }
    
    /***
     * 
     * 发布和修改优惠价提示
     * @author  wang_lin
     * @version  [2.1.0, 2014-4-23]
     */
    public interface OnSubmitCouponListener
    {
        public void onSubmitCouponListenerOk();
        
        public void onSubmitCouponListenerCancel();
    }
    
    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute)
    {
        onDateChanged(null, 0, 0, 0);
    }
    
    @Override
    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth)
    {
        Calendar calendar = Calendar.getInstance();
        
        calendar.set(datePicker.getYear(),
            datePicker.getMonth(),
            datePicker.getDayOfMonth(),
            timePicker.getCurrentHour(),
            timePicker.getCurrentMinute());
        if (isTag)
        {
            dateTime = DateUtil.formatYYYYMD(calendar.getTime().getTime());
        }
        else
        {
            dateTime = DateUtil.formatNowTime(calendar.getTime().getTime());
        }
    }
}
