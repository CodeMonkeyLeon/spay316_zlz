package cn.swiftpass.enterprise.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.enterprise.R;

/**
 * 创建人：caoxiaoya
 * 时间：2019/4/8
 * 描述：自定义底部Tab
 * 备注：
 */
public class BottomBarTab extends FrameLayout {
    private ImageView mIcon;
    private TextView mTvTitle;
    private Context mContext;
    private int mTabPosition = -1;


    public BottomBarTab(Context context, @DrawableRes int icon, CharSequence title) {
        this(context, null, icon, title);
    }

    public BottomBarTab(Context context, AttributeSet attrs, int icon, CharSequence title) {
        this(context, attrs, 0, icon, title);
    }

    public BottomBarTab(Context context, AttributeSet attrs, int defStyleAttr, int icon, CharSequence title) {
        super(context, attrs, defStyleAttr);
        init(context, icon, title);
    }

    private void init(Context context, int icon, CharSequence title) {
        mContext = context;
        TypedArray typedArray = context.obtainStyledAttributes(new int[]{R.attr.selectableItemBackgroundBorderless});
        Drawable drawable = typedArray.getDrawable(0);
        setBackgroundDrawable(drawable);
        typedArray.recycle();

        LinearLayout lLContainer = new LinearLayout(context);
        lLContainer.setOrientation(LinearLayout.VERTICAL);
        lLContainer.setGravity(Gravity.CENTER);
        LayoutParams paramsContainer = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsContainer.gravity = Gravity.CENTER;
        lLContainer.setLayoutParams(paramsContainer);

        mIcon = new ImageView(context);
        int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 27, getResources().getDisplayMetrics());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size, size);
        mIcon.setImageResource(icon);
        mIcon.setLayoutParams(params);
        lLContainer.addView(mIcon);

        mTvTitle = new TextView(context);
        mTvTitle.setText(title);
        LinearLayout.LayoutParams paramsTv = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsTv.topMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics());
        mTvTitle.setTextSize(11);
        mTvTitle.setTextColor(ContextCompat.getColor(context, R.color.clr_999999));
        mTvTitle.setLayoutParams(paramsTv);
        lLContainer.addView(mTvTitle);

        addView(lLContainer);

    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        if (selected) {
            switch (getTabPosition()){
                case 0:
                    mIcon.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.icon_tab_charge_sel));
                    break;
                case 1:
                    mIcon.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.icon_tab_transaction_sel));
                    break;
                case 2:
                    mIcon.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.icon_summary));
                    break;
                case 3:
                    mIcon.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.icon_tab_me_sel));
                    break;
            }
            mTvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.clr_5389FF));
        } else {
            switch (getTabPosition()){
                case 0:
                    mIcon.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.icon_tab_charge_nor));
                    break;
                case 1:
                    mIcon.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.icon_tab_transaction_nor));
                    break;
                case 2:
                    mIcon.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.icon_summary_nor));
                    break;
                case 3:
                    mIcon.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.icon_tab_me_nor));
                    break;
            }
            mTvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.clr_999999));
        }
    }

    public void setTabPosition(int position) {
        mTabPosition = position;
        if (position == 0) {
            setSelected(true);
        }
    }

    public int getTabPosition() {
        return mTabPosition;
    }

}
