package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.app.LocalActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.HashMap;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.bill.BillMainActivity;
import cn.swiftpass.enterprise.ui.activity.wallet.WalletApplyMainActivity;
import cn.swiftpass.enterprise.ui.widget.BottomButtonView;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;


public class MainActivity extends TemplateActivity {

    public static final int TAB_PAY = 0;// 支付

    public static final int TAB_ORDER = 1;// 流水

    public static final int TAB_SEARCH = 2;// 统计

    public static final int TAB_SETTING = 3;// 设置

    private String registPromptStr;


    private TabHost tabHost;

    private Context mContext;

    public static HashMap<Integer, Intent> mIntents = new HashMap<Integer, Intent>();

    public int nCurrentTabIndex = 0;

    private boolean isRefund; // 退款跳转

    private ViewPager my_viewpage;

    private ImageView img_pay, img_order, img_search, img_settings;

    private TextView tvPay, tvOrder, tvSearch, tvSetting;

    private String textColor = "#999999";//W.l 修改字体颜色

    private String textDownClolor = ToastHelper.toStr(R.color.title_bg_new); //W.l 添加按下颜色

    @SuppressWarnings("deprecation") private LocalActivityManager mActivityManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setupInitViews();

        registerBlueReceiver();
    }

    private void registerBlueReceiver() {

        IntentFilter filterBlue = new IntentFilter();
        filterBlue.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filterBlue.addAction(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
        try {
            registerReceiver(blueReceiver, filterBlue);
        } catch (Exception e) {
            Logger.e("hehui", "" + e);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (blueReceiver != null) {
            unregisterReceiver(blueReceiver);
        }
    }

    /**
     * 实时监听蓝牙广播
     */
    private final BroadcastReceiver blueReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
//            Logger.i(TAG, "state-->" + state + "," + intent.getAction());
            switch (state) {
                case BluetoothAdapter.STATE_OFF:
                    MainApplication.setBlueState(false);
                    if (isClose) {
                        isClose = false;
                    }
                    MainApplication.bluetoothSocket = null;
                    HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET_STUTS, HandlerManager.BLUE_CONNET_STUTS_CLOSED);
                    break;
                case BluetoothAdapter.STATE_TURNING_OFF:
                    MainApplication.setBlueState(false);
                    MainApplication.bluetoothSocket = null;
                    if (isCloseing) {
                        isCloseing = false;

                    }
                    HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET_STUTS, HandlerManager.BLUE_CONNET_STUTS_CLOSED);
                    break;
                case BluetoothAdapter.STATE_ON:
                    MainApplication.setBlueState(true);
                    if (isOpen) {
                        isOpen = false;
                    }
                    try {
                        connentBlue();
                    } catch (Exception e) {
//                        Logger.e(TAG, "" + e);
                    }
                    HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET_STUTS, HandlerManager.BLUE_CONNET_STUTS);
                    break;
                case BluetoothAdapter.STATE_TURNING_ON:
                    if (isOpening) {
                        isOpening = false;

                    }
                    break;
            }

        }

    };



    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            //            if (msg.what == 1)
            //            {
            //                initPromt();
            //            }
            //            viewPager.setCurrentItem(currentItem);// 切换当前显示的图片
            if (msg.what == 2) {
                setTabView(TAB_ORDER);
                setView(bottomBar);
            }
        }

        ;
    };

    @SuppressWarnings("deprecation")
    private void setupInitViews() {
        setContentView(R.layout.activity_main);
        registPromptStr = getIntent().getStringExtra("registPromptStr");
        tabHost = getViewById(android.R.id.tabhost);
        mActivityManager = getLocalActivityManager();
        // 文本显示
        initTabHost();
        HandlerManager.registerHandler(HandlerManager.MAINACTIVITY, handler);
    }

    /**
     * @description: 初始化切换卡
     */
    private void initTabHost() {
        tabHost.setup(mActivityManager);
        setTabIntent(TAB_PAY, new Intent(this, PayActivity.class));

        setTabIntent(TAB_ORDER, new Intent(this, BillMainActivity.class));

        //        setTabIntent(TAB_ORDER, new Intent(this, OrderStreamActivity.class));

        //        setTabIntent(TAB_ORDER, new Intent(this, StreamActivity.class));
        //        setTabIntent(TAB_SEARCH, new Intent(this, OrderTotalNewActivity.class));
        //        if (MainApplication.isAdmin.equals("1"))
        //        {
        //            
        //        setTabIntent(TAB_SEARCH, new Intent(this, OrderTotalActivity.class));
        //        setTabIntent(TAB_SEARCH, new Intent(this, TotalActivity.class));
        //        }
        //        else
        //        {
        //            if (MainApplication.isTotalAuth == 1)
        //            {
        //        setTabIntent(TAB_SEARCH, new Intent(this, OrderTotalNewActivity.class));

        //最新统计
        //        setTabIntent(TAB_SEARCH, new Intent(this, OrderTotalNewestActivity.class));

        //申请开通钱包

        setTabIntent(TAB_SEARCH, new Intent(this, WalletApplyMainActivity.class));
        //        if (MainApplication.isHasEwallet == -1 || MainApplication.isHasEwallet == 0)
        //        { //-1 0都是未开通
        //        
        //            setTabIntent(TAB_SEARCH, new Intent(this, WalletApplyMainActivity.class));
        //        }
        //        else
        //        {
        //            //余额转出
        //            setTabIntent(TAB_SEARCH, new Intent(this, WalletPushMainActivity.class));
        //        }
        //            }
        //        }
        setTabIntent(TAB_SETTING, new Intent(this, SettingActivity.class));
        if (!StringUtil.isEmptyOrNull(registPromptStr)) {

            if (registPromptStr.equals("OrderDetailsActivity")) // 退款跳转
            {
                setTabView(TAB_ORDER);
                nCurrentTabIndex = TAB_ORDER;
            } else if (registPromptStr.equals("SettingActivity")) {
                //                setTabView(TAB_SETTING);
                //                nCurrentTabIndex = TAB_SETTING;
                //                
                //                setViewSetting(bottomBar);
                setTabView(TAB_PAY);
                nCurrentTabIndex = TAB_PAY;
                setViewSetting(bottomBar);
            } else {
                setTabView(TAB_PAY);
            }
        } else {
            setTabView(TAB_PAY);
        }
    }

    /**
     * @param tabIndex
     * @description: 显示第tabIndex个切换卡界面
     */
    public void setTabView(int tabIndex) {
        Intent i = mIntents.get(tabIndex);
        //        if (mIntents.size() == 0)
        //        {
        //            setTabIntent(TAB_PAY, new Intent(MainActivity.this, PayActivity.class));
        //            setTabIntent(TAB_ORDER, new Intent(MainActivity.this, OrderStreamNewActivity.class));
        //            setTabIntent(TAB_SEARCH, new Intent(MainActivity.this, OrderTotalNewActivity.class));
        //            setTabIntent(TAB_SETTING, new Intent(MainActivity.this, SettingActivity.class));
        //            setTabView(TAB_ORDER);
        //            return;
        //        }
        if (i != null) {
            nCurrentTabIndex = tabIndex;
            if (!StringUtil.isEmptyOrNull(registPromptStr)) {
                if (tabIndex == 0 && registPromptStr.endsWith("RegisterActivity")) {
                    i.putExtra("registPromptStr", registPromptStr);
                }
            }
            View v = mActivityManager.startActivity("tab" + tabIndex, i).getDecorView();
            tabHost.removeAllViews();
            tabHost.setCurrentTab(tabIndex);
            tabHost.addView(v);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    /**
     * @param tabIndex
     * @param intent
     * @description: 设置第tabIndex个Tab的Intent，即设置第tabIndex个切换卡要显示的界面
     */
    public void setTabIntent(int tabIndex, Intent intent) {
        mIntents.put(tabIndex, intent);
    }

    public static void startActivity(Context context, String registPromptStr) {
        Intent it = new Intent(context, MainActivity.class);
        if ((context instanceof Activity)) {
            //it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            it.putExtra("registPromptStr", registPromptStr);
        }
        context.startActivity(it);
    }

    @Override
    protected void setupBottomBar() {
        super.setupBottomBar();
        BottomButtonView bottomView = new BottomButtonView(mContext);
        bottomView.setActivity(MainActivity.this);

        WelcomeActivity welcomeActivity = new WelcomeActivity();
        welcomeActivity.setActivity(MainActivity.this);
        bottomBar.addView(bottomView);
    }

    private void setView(LinearLayout layout) {

        img_pay = (ImageView) layout.findViewById(R.id.img_pay);
        tvPay = (TextView) findViewById(R.id.tv_pay);
        img_order = (ImageView) layout.findViewById(R.id.img_order);
        tvOrder = (TextView) layout.findViewById(R.id.tv_order);
        img_search = (ImageView) layout.findViewById(R.id.img_search);
        tvSearch = (TextView) layout.findViewById(R.id.tv_search);
        img_settings = (ImageView) layout.findViewById(R.id.img_settings);
        tvSetting = (TextView) layout.findViewById(R.id.tv_settings);

        img_pay.setImageResource(R.drawable.icon_tab_collection_default);
        tvPay.setTextColor(Color.parseColor(textColor));
        img_order.setImageResource(R.drawable.icon_tab_bill_choose);
        tvOrder.setTextColor(Color.parseColor(textDownClolor));
        img_search.setImageResource(R.drawable.icon_tab_wallet_default);
        tvSearch.setTextColor(Color.parseColor(textColor));
        img_settings.setImageResource(R.drawable.icon_tab_me_default);
        tvSetting.setTextColor(Color.parseColor(textColor));
    }

    private void setViewSetting(LinearLayout layout) {

        img_pay = (ImageView) layout.findViewById(R.id.img_pay);
        tvPay = (TextView) findViewById(R.id.tv_pay);
        img_order = (ImageView) layout.findViewById(R.id.img_order);
        tvOrder = (TextView) layout.findViewById(R.id.tv_order);
        img_search = (ImageView) layout.findViewById(R.id.img_search);
        tvSearch = (TextView) layout.findViewById(R.id.tv_search);
        img_settings = (ImageView) layout.findViewById(R.id.img_settings);
        tvSetting = (TextView) layout.findViewById(R.id.tv_settings);

        img_pay.setImageResource(R.drawable.icon_tab_collection_choose);
        tvPay.setTextColor(Color.parseColor(textDownClolor));
        img_order.setImageResource(R.drawable.icon_tab_bill_default);
        tvOrder.setTextColor(Color.parseColor(textColor));
        img_search.setImageResource(R.drawable.icon_tab_wallet_default);
        tvSearch.setTextColor(Color.parseColor(textColor));
        img_settings.setImageResource(R.drawable.icon_tab_me_default);
        tvSetting.setTextColor(Color.parseColor(textColor));
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        findViewById(R.id.titleBar).setVisibility(View.GONE);
    }

    @Override
    public boolean onKeyDown(int keycode, KeyEvent event) {
        if (keycode == KeyEvent.KEYCODE_BACK) {
            showExitDialog(this);
        }
        return super.onKeyDown(keycode, event);
    }
}
