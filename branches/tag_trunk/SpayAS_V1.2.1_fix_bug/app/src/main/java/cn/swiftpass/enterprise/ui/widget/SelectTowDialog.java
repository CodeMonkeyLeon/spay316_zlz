package cn.swiftpass.enterprise.ui.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;

/**只有两个选择对话框*/
public class SelectTowDialog extends Dialog implements OnClickListener
{
    
    private OnSelectTowListener listener;
    
    private LinearLayout llOne, llTow;
    
    private TextView tvOne, tvTow, tvTitle;
    
    private String title;
    
    private String[] itemText;
    
    private Context context;
    
    public SelectTowDialog(Context context, String title, String[] itemText, OnSelectTowListener listener)
    {
        super(context);
        this.context = context;
        this.listener = listener;
        this.title = title;
        this.itemText = itemText;
        initViews();
    }
    
    private void initViews()
    {
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_select_tow);
        //        LinearLayout ll = (LinearLayout)this.findViewById(R.id.ll_root);
        //        Display mDisplay = ((Activity)context).getWindowManager().getDefaultDisplay();
        //        ll.set
        llOne = (LinearLayout)findViewById(R.id.ll_one);
        llTow = (LinearLayout)findViewById(R.id.ll_tow);
        llOne.setOnClickListener(this);
        llTow.setOnClickListener(this);
        
        tvTitle = (TextView)findViewById(R.id.tv_title);
        tvOne = (TextView)findViewById(R.id.tv_textview1);
        tvTow = (TextView)findViewById(R.id.tv_textview2);
        
        tvTitle.setText(title);
        tvTow.setText(itemText[0]);
        tvOne.setText(itemText[1]);
    }
    
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.ll_one:
                listener.onOne();
                break;
            case R.id.ll_tow:
                listener.onTow();
                break;
        }
    }
    
    public interface OnSelectTowListener
    {
        public void onOne();
        
        public void onTow();
    }
}
