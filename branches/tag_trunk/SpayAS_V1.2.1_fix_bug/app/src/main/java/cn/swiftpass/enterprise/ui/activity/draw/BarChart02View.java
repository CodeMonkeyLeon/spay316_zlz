/**
 * Copyright 2014  XCL-Charts
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 	
 * @Project XCL-Charts 
 * @Description Android图表基类库
 * @author XiongChuanLiang<br/>(xcl_168@aliyun.com)
 * @Copyright Copyright (c) 2014 XCL-Charts (www.xclcharts.com)
 * @license http://www.apache.org/licenses/  Apache v2 License
 * @version 1.0
 */
package cn.swiftpass.enterprise.ui.activity.draw;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.xclcharts.chart.BarChart;
import org.xclcharts.chart.BarData;
import org.xclcharts.common.DensityUtil;
import org.xclcharts.common.IFormatterDoubleCallBack;
import org.xclcharts.common.IFormatterTextCallBack;
import org.xclcharts.event.click.BarPosition;
import org.xclcharts.renderer.XEnum;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalItemInfo;
import cn.swiftpass.enterprise.utils.DateTimeUtil;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * @ClassName BarChart02View
 * @Description  柱形图例子(横向)
 * @author XiongChuanLiang<br/>(xcl_168@aliyun.com)
 */
public class BarChart02View extends DemoView
{
    
    private static final String TAG = "BarChart02View";
    
    private BarChart chart = new BarChart();
    
    //标签轴
    private List<String> chartLabels = new LinkedList<String>();
    
    private List<BarData> chartData = new LinkedList<BarData>();
    
    private List<String> sixMonthsLst;
    
    private List<OrderTotalItemInfo> infoList;
    
    private List<Double> moneyList = new ArrayList<Double>();
    
    private List<Double> numList = new ArrayList<Double>();
    
    private double evaluate; //平均值
    
    private TextView tv_months, tv_moeny;
    
    private boolean isNum;
    
    public BarChart02View(Context context, List<String> sixMonthsLst, List<OrderTotalItemInfo> infoList,
        TextView tv_months, TextView tv_moeny, boolean isNum)
    {
        super(context);
        // TODO Auto-generated constructor stub
        this.sixMonthsLst = sixMonthsLst;
        this.infoList = infoList;
        this.tv_months = tv_months;
        this.tv_moeny = tv_moeny;
        this.isNum = isNum;
        long m = 0;
        for (OrderTotalItemInfo info : infoList)
        {
            if (isNum)
            {
                numList.add((double)info.getSuccessCount());
                m = m + (info.getSuccessCount() / 100);
                
            }
            else
            {
                //                if (info.getSuccessFee() > 10000)
                //                {
                //                    long succFee = info.getSuccessFee() / 100;
                //                    moneyList.add((double)succFee);
                //                }
                //                else
                //                {
                //                double succFee = info.getSuccessFee() / 100;
                double succFee = Double.parseDouble(DateTimeUtil.formatMonetToStr(info.getSuccessFee(), 100));
                moneyList.add(succFee);
                //                }
                m = m + (info.getSuccessFee() / 100);
                
            }
        }
        
        //        if (isNum)
        //        {
        //            Collections.reverse(numList);
        //        }
        //        else
        //        {
        //            Collections.reverse(moneyList);
        //        }
        
        evaluate = (m / 6) * 100;
        initView();
    }
    
    public BarChart02View(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initView();
    }
    
    public BarChart02View(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        initView();
    }
    
    private void initView()
    {
        //        DrawHelper.isTag = true;
        chartLabels();
        chartDataSet();
        chartRender();
        
        //綁定手势滑动事件
        this.bindTouch(this, chart);
    }
    
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh)
    {
        super.onSizeChanged(w, h, oldw, oldh);
        //图所占范围大小
        if (null != chart)
            chart.setChartRange(w, h);
    }
    
    private void chartRender()
    {
        try
        {
            //设置绘图区默认缩进px值,留置空间显示Axis,Axistitle....	
            int[] ltrb = getBarLnDefaultSpadding();
            chart.setPadding(DensityUtil.dip2px(getContext(), 50), ltrb[1], ltrb[2], ltrb[3]);
            
            //            chart.setTitle("每日收益情况");
            //            chart.addSubtitle("(XCL-Charts Demo)");
            chart.setTitleVerticalAlign(XEnum.VerticalAlign.MIDDLE);
            chart.setTitleAlign(XEnum.HorizontalAlign.LEFT);
            //设置默认横坐标最大值
            //            chart.getDataAxis().defaultX = sixMonthsLst.get(sixMonthsLst.size() - 1);
            //数据源
            chart.setDataSource(chartData);
            chart.setCategories(chartLabels);
            
            //轴标题
            //			chart.getAxisTitle().setLeftTitle("所售商品");
            //			chart.getAxisTitle().setLowerTitle("纯利润(天)");
            //			chart.getAxisTitle().setRightTitle("生意兴隆通四海,财源茂盛达三江。");
            
            //数据轴
            
            if (isNum)
            {
                double minNum = Collections.min(numList);
                double maxNum = Collections.max(numList);
                setAxisStepsLen(minNum, maxNum);
                if (numList.size() > 0 && maxNum > 0f)
                {
                    chart.getDataAxis().setAxisMax(maxNum);
                }
                else
                {
                    chart.getDataAxis().setAxisMax(9);
                }
            }
            else
            {
                double min = Collections.min(moneyList);
                double max = Collections.max(moneyList);
                if (moneyList.size() > 0 && max > 0f)
                {
                    //                    max = max / 100;
                    chart.getDataAxis().setAxisMax(max);
                }
                else
                {
                    chart.getDataAxis().setAxisMax(9);
                }
                
                setAxisStepsLen(min, max);
            }
            chart.getDataAxis().setAxisMin(0);
            
            chart.getDataAxis().getTickLabelPaint().setColor(Color.parseColor("#666666"));
            
            chart.getDataAxis().setLabelFormatter(new IFormatterTextCallBack()
            {
                
                @Override
                public String textFormatter(String value)
                {
                    double val = Double.parseDouble(value);
                    double axisStept = chart.getDataAxis().getAxisSteps();
                    Log.i("hehui", "value-->" + value + ",axisStept-->" + axisStept);
                    
                    //                    if (val >= 100 && val < 1000)
                    //                    {
                    //                        //                        chart.getDataAxis().setAxisSteps(100);
                    //                        return (val / 100) + "";
                    //                    }
                    //                    else if (val >= 1000 && val <= 10000)
                    //                    {
                    //                        //                        chart.getDataAxis().setAxisSteps(1000);
                    //                        return (val / 1000) + "";
                    //                    }
                    //                    else if (val > 10000 && val <= 100000)
                    //                    {
                    //                        //                        chart.getDataAxis().setAxisSteps(10000);
                    //                        return (val / 10000) + "";
                    //                    }
                    //                    else if (val > 100000 && val < 1000000)
                    //                    {
                    //                        //                        chart.getDataAxis().setAxisSteps(100000);
                    //                        return (val / 100000) + "";
                    //                    }
                    //                    else if (val > 1000000 && val <= 10000000)
                    //                    {
                    //                        //                        chart.getDataAxis().setAxisSteps(1000000);
                    //                        return (val / 1000000) + "";
                    //                    }
                    //                    else if (val > 10000000 && val <= 10000000)
                    //                    {
                    //                        //                        chart.getDataAxis().setAxisSteps(10000000);
                    //                        return (val / 10000000) + "";
                    //                    }
                    //                    else if (val >= 100000000)
                    //                    {
                    //                        //                        chart.getDataAxis().setAxisSteps(10000000);
                    //                        return (val / 100000000) + "";
                    //                    }
                    //                    else if (val < 100 && val >= 50)
                    //                    {
                    //                        //                        chart.getDataAxis().setAxisSteps(10);
                    //                        return (val / 100) + "";
                    //                    }
                    //                    else if (val >= 10 && val <= 50)
                    //                    {
                    //                        //                        chart.getDataAxis().setAxisSteps(10);
                    //                        return (val / 100) + "";
                    //                    }
                    //                    else if (val > 0 && val < 10)
                    //                    {
                    //                        chart.getDataAxis().setAxisSteps(1);
                    //                    }
                    //                    else
                    //                    {
                    //                        chart.getDataAxis().setAxisSteps(1);
                    //                    }
                    return val / axisStept + "";
                }
                
            });
            
            //网格
            chart.getPlotGrid().showHorizontalLines();
            chart.getPlotGrid().hideVerticalLines();
            chart.getPlotGrid().getHorizontalLinePaint().setColor(Color.parseColor("#cccccc"));
            chart.getPlotGrid().getHorizontalLinePaint().setStrokeWidth(0.5f);
            //            chart.getPlotGrid().getHorizontalLinePaint().setAlpha(200);
            //            chart.getPlotGrid().showEvenRowBgColor();
            
            //标签轴文字旋转-45度
            //            chart.getCategoryAxis().setTickLabelRotateAngle(-45f);
            //横向显示柱形
            chart.setChartDirection(XEnum.Direction.VERTICAL);
            //在柱形顶部显示值
            chart.getBar().setItemLabelVisible(true);
            chart.getBar().getItemLabelPaint().setTextSize(22);
            chart.setPlotPanMode(XEnum.PanMode.HORIZONTAL);
            chart.getCategoryAxis().hideTickMarks();
            chart.getDataAxis().hideTickMarks();
            //            chart.getDataAxis().hideAxisLine();
            chart.getDataAxis().show();
            chart.getDataAxis().getAxisPaint().setColor(Color.parseColor("#cccccc"));
            chart.getDataAxis().getAxisPaint().setStrokeWidth(DensityUtil.dip2px(getContext(), 1));
            
            chart.disableScale();
            chart.getDataAxis().setTickLabelMargin(DensityUtil.dip2px(getContext(), 15));
            chart.getDataAxis().getTickLabelPaint().setTextSize(DensityUtil.dip2px(getContext(), 11));
            
            chart.getCategoryAxis().setTickLabelMargin(DensityUtil.dip2px(getContext(), 10));
            chart.getCategoryAxis().getAxisPaint().setColor(Color.parseColor("#cccccc"));
            chart.getCategoryAxis().getAxisPaint().setStrokeWidth(DensityUtil.dip2px(getContext(), 1));
            chart.getCategoryAxis().getTickLabelPaint().setTextSize(DensityUtil.dip2px(getContext(), 11));
            //            chart.getCategoryAxis().getTickLabelPaint().setColor(Color.RED);
            chart.setItemLabelFormatter(new IFormatterDoubleCallBack()
            {
                @Override
                public String doubleFormatter(Double value)
                {
                    // TODO Auto-generated method stub
                    DecimalFormat df = new DecimalFormat("[#0]");
                    String label = df.format(value).toString();
                    return label;
                }
            });
            
            //激活点击监听
            chart.ActiveListenItemClick();
            chart.showClikedFocus();
            chart.setBarCenterStyle(XEnum.BarCenterStyle.TICKMARKS);
            chart.disablePanMode();
            /*
            chart.setDataAxisPosition(XEnum.DataAxisPosition.BOTTOM);
            chart.getDataAxis().setVerticalTickPosition(XEnum.VerticalAlign.BOTTOM);
            
            chart.setCategoryAxisPosition(XEnum.CategoryAxisPosition.LEFT);
            chart.getCategoryAxis().setHorizontalTickAlign(Align.LEFT);
            */
            
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        }
        
    }
    
    void setAxisStepsLen(double min, double max)
    {
        //        if (min >= 100)
        //        {
        //            chart.getDataAxis().setAxisSteps(100);
        //        }
        //        else if (min >= 1000)
        //        {
        //            chart.getDataAxis().setAxisSteps(1000);
        //        }
        //        else if (min >= 10000)
        //        {
        //            chart.getDataAxis().setAxisSteps(10000);
        //        }
        //        else if (min >= 100000)
        //        {
        //            chart.getDataAxis().setAxisSteps(100000);
        //        }
        //        else if (min >= 1000000)
        //        {
        //            chart.getDataAxis().setAxisSteps(1000000);
        //        }
        //        else if (min >= 10000000)
        //        {
        //            chart.getDataAxis().setAxisSteps(10000000);
        //        }
        //        else if (min >= 100000000)
        //        {
        //            chart.getDataAxis().setAxisSteps(100000000);
        //        }
        //        else if (min >= 1000000000)
        //        {
        //            chart.getDataAxis().setAxisSteps(1000000000);
        //        }
        //        else if (max >= 100 && max < 200)
        //        {
        //            chart.getDataAxis().setAxisSteps(20);
        //        }
        
        if (max >= 100 && max < 1000)
        {
            chart.getDataAxis().setAxisSteps(100);
        }
        else if (max >= 1000 && max < 10000)
        {
            chart.getDataAxis().setAxisSteps(1000);
        }
        else if (max >= 10000 && max < 100000)
        {
            chart.getDataAxis().setAxisSteps(10000);
        }
        else if (max >= 100000 && max < 1000000)
        {
            chart.getDataAxis().setAxisSteps(100000);
        }
        else if (max >= 1000000 && max < 10000000)
        {
            chart.getDataAxis().setAxisSteps(1000000);
        }
        else if (max >= 10000000 && max < 10000000)
        {
            chart.getDataAxis().setAxisSteps(10000000);
        }
        else if (max >= 100000000)
        {
            chart.getDataAxis().setAxisSteps(10000000);
        }
        else if (max < 100 && max >= 50)
        {
            chart.getDataAxis().setAxisSteps(10);
        }
        else if (max >= 10 && max <= 50)
        {
            chart.getDataAxis().setAxisSteps(10);
        }
        else if (max > 0 && max < 10)
        {
            chart.getDataAxis().setAxisSteps(1);
        }
        else
        {
            chart.getDataAxis().setAxisSteps(1);
        }
        
        //        if (max >= 2000 && max < 3000)
        //        {
        //            chart.getDataAxis().setAxisSteps(500);
        //        }
        //        else if (max > 3000 && max < 10000)
        //        {
        //            chart.getDataAxis().setAxisSteps(1000);
        //        }
        //        else if (max >= 10000 && max < 50000)
        //        {
        //            chart.getDataAxis().setAxisSteps(1000);
        //        }
        //        else if (max >= 50000 && max < 100000)
        //        {
        //            chart.getDataAxis().setAxisSteps(5000);
        //        }
        //        else if (max >= 100000 && max < 1000000)
        //        {
        //            chart.getDataAxis().setAxisSteps(100000);
        //        }
        //        else if (max >= 1000000 && max < 10000000)
        //        {
        //            chart.getDataAxis().setAxisSteps(1000000);
        //        }
        //        else if (max >= 10000000)
        //        {
        //            chart.getDataAxis().setAxisSteps(1000000);
        //        }
        //        else if (max >= 200 && max < 500)
        //        {
        //            chart.getDataAxis().setAxisSteps(50);
        //        }
        //        else if (max >= 500 && max < 1000)
        //        {
        //            chart.getDataAxis().setAxisSteps(100);
        //        }
        //        else if (max >= 1000 && max < 2000)
        //        {
        //            chart.getDataAxis().setAxisSteps(200);
        //        }
        //        else if (max > 2000)
        //        {
        //            chart.getDataAxis().setAxisSteps(500);
        //        }
        //        else if (max < 100 && max >= 50)
        //        {
        //            chart.getDataAxis().setAxisSteps(10);
        //        }
        //        else if (max > 10 && max <= 50)
        //        {
        //            chart.getDataAxis().setAxisSteps(5);
        //        }
        //        else if (max >= 0 && max <= 10)
        //        {
        //            chart.getDataAxis().setAxisSteps(1);
        //        }
        //        else
        //        {
        //            chart.getDataAxis().setAxisSteps(1);
        //        }
    }
    
    private void chartDataSet()
    {
        //标签对应的柱形数据集
        List<Double> dataSeriesA = new LinkedList<Double>();
        //        dataSeriesA.add((double)200);
        //        dataSeriesA.add((double)250);
        //        dataSeriesA.add((double)400);
        //        dataSeriesA.add((double)500);
        //        dataSeriesA.add((double)700);
        //        dataSeriesA.add((double)200);
        dataSeriesA.clear();
        if (isNum)
        {
            dataSeriesA.addAll(numList);
            //            dataSeriesA.add(numList.size() - 2, evaluate);
            
        }
        else
        {
            dataSeriesA.addAll(moneyList);
            //            dataSeriesA.add(moneyList.size() - 2, evaluate);
        }
        //        Collections.reverse(dataSeriesA);
        BarData BarDataA = new BarData("", dataSeriesA, Color.parseColor("#4182E2"));
        
        //        for (Double d : dataSeriesA)
        //        {
        //            Log.i("hehui", "dataSeriesA-->" + d);
        //        }
        //        List<Double> dataSeriesB = new LinkedList<Double>();
        //        dataSeriesB.add((double)300);
        //        dataSeriesB.add((double)150);
        //        dataSeriesB.add((double)450);
        //        BarData BarDataB = new BarData("", dataSeriesB, Color.rgb(255, 0, 0));
        //        
        chartData.add(BarDataA);
        //        chartData.add(BarDataB);
    }
    
    private void chartLabels()
    {
        //        chartLabels.add("5月");
        //        chartLabels.add("6月");
        //        chartLabels.add("7月");
        //        chartLabels.add("8月");
        //        chartLabels.add("9月");
        //        chartLabels.add("10月");
        chartLabels.clear();
        chartLabels.addAll(sixMonthsLst);
        //        for (String s : chartLabels)
        //        {
        //            Log.i("hehui", "sixMonthsLst-->" + s);
        //            
        //        }
        
    }
    
    @Override
    public void render(Canvas canvas)
    {
        try
        {
            chart.render(canvas);
        }
        catch (Exception e)
        {
            Log.e(TAG, e.toString());
        }
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        // TODO Auto-generated method stub		
        super.onTouchEvent(event);
        if (event.getAction() == MotionEvent.ACTION_UP)
        {
            triggerClick(event.getX(), event.getY());
        }
        return true;
    }
    
    //触发监听
    private void triggerClick(float x, float y)
    {
        BarPosition record = chart.getPositionRecord(x, y);
        if (null == record)
            return;
        
        BarData bData = chartData.get(record.getDataID());
        Double bValue = bData.getDataSet().get(record.getDataChildID());
        
        //        Toast.makeText(this.getContext(),
        //            "info:" + record.getRectInfo() + " Key:" + bData.getKey() + " Current Value:" + Double.toString(bValue),
        //            Toast.LENGTH_SHORT).show();
        //        if (record.getDataChildID() == (sixMonthsLst.size() - 1))
        //        {
        //            return;
        //        }
        //        else
        //        {
        if (isNum)
        {
            double d = numList.get(record.getDataChildID());
            tv_moeny.setText((long)(d) + ToastHelper.toStr(R.string.stream_cases));
        }
        else
        {
            tv_moeny.setText(DateUtil.formatMoneyUtil(moneyList.get(record.getDataChildID()))
                + ToastHelper.toStr(R.string.pay_yuan));
        }
        tv_months.setText(sixMonthsLst.get(record.getDataChildID()).toString());
        //        }
        
        //        if ((chart.lastX == record.getPosition().x) && (chart.lastY == record.getPosition().y))
        //        {
        //            chart.getToolTip().setAlign(Align.LEFT);
        //        }
        
        chart.showFocusRectF(record.getRectF());
        //        chart.getFocusPaint().setStyle(Style.STROKE);
        //        chart.getFocusPaint().setStrokeWidth(3);
        //        chart.getFocusPaint().setColor(Color.GREEN);
        this.invalidate();
    }
    
}
