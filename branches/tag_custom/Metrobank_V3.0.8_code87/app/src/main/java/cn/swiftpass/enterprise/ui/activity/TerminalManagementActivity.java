package cn.swiftpass.enterprise.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.print.BluetoothSettingActivity;

/**
 * Created by aijingya on 2019/8/13.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(设备管理的界面)
 * @date 2019/8/13.17:26.
 */
public class TerminalManagementActivity extends TemplateActivity implements View.OnClickListener {
    private LinearLayout ll_Speaker,ll_Printer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terminal_management_layout);
        initView();
    }

    public void initView(){
        ll_Speaker = findViewById(R.id.ll_Speaker);
        ll_Speaker.setOnClickListener(this);

        //屏蔽云播报
        ll_Speaker.setVisibility(View.GONE);

        ll_Printer = findViewById(R.id.ll_Printer);
        ll_Printer.setOnClickListener(this);
        if (MainApplication.IS_POS_VERSION) {
            ll_Printer.setVisibility(View.GONE);
        }else{
            ll_Printer.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_Speaker:
                showPage(MySpeakerListActivity.class);
                break;
            case R.id.ll_Printer:
                showPage(BluetoothSettingActivity.class);
                break;
        }
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(getStringById(R.string.fragment_tab_setting_terminal_management));
    }
}
