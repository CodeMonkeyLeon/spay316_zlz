package cn.swiftpass.enterprise.ui.activity.user;

import static cn.swiftpass.enterprise.intl.BuildConfig.IS_POS_VERSION;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.common.sentry.SentryUtils;
import com.example.common.sp.PreferenceUtil;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.enums.ClientEnum;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.common.Constant;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.RefundRecordOrderDetailsContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.RefundRecordOrderDetailsPresenter;
import cn.swiftpass.enterprise.print.PrintOrder;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.activity.RotateCanvasViewActivity;
import cn.swiftpass.enterprise.ui.activity.TicketPreviewActivity;
import cn.swiftpass.enterprise.ui.activity.print.BluePrintUtil;
import cn.swiftpass.enterprise.ui.activity.print.BluetoothSettingActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftplus.enterprise.printsdk.print.PrintClient;

/**
 * 订单详细 跟退款详细 User: hehui Date: 13-9-29 Time: 下午2:32
 */
public class RefundRecordOrderDetailsActivity extends BaseActivity<RefundRecordOrderDetailsContract.Presenter> implements RefundRecordOrderDetailsContract.View {

    private static final String TAG = RefundRecordOrderDetailsActivity.class.getSimpleName();
    private final static int QUERY_REFUND = 0x111;
    //private LinearLayout llBanktype, llNotifyTime;
    String refundNum = null; // 退款单号

    //private TextView tvAddTime, tvRefund, tvUser, tvRefundCode, tvBankType, tvTransactionId, tvTitleTime, tvBankTypeTitle;
    //private ImageView ivPayType;
    private TextView tvOrderCode, tvMoney, tvNotifyTime, tvState, wx_tvOrderCode, wx_title_info;

    //private LinearLayout llTransactionId,  lr_wx;
    private LinearLayout llRefund;
    //private Context context;
    private ImageView iv_pay_img;

    //private RefundModel refundModel;
    //private Bitmap bmp;
    private Order orderModel;

    //private ImageView signatureView; // 获取签名图片
    private Button btnRefund, blue_print;
    //private TextView tv_order_no;
    private LinearLayout cashierLay, logo_lay, ll_pay_top, lay_refund_time;

    //private RelativeLayout pay_top_re;
    //private ImageView iv_pay_image;
    private TextView cashierText, body_info, pay_mch, pay_method, refund_apply_time, pay_mchId, refund_money;
    private ImageView logo_title, iv_code;
    private TextView tv_code, refund_apply_peop, refund_status, refund_no, tv_transNo;
    private TextView spay_pay_client, refund_time, id_total;
    //private LinearLayout ly_rmb;
    private LinearLayout lay_refund_peo, id_lin_surcharge, id_lin_withholding, id_order_line;
    //private TextView tx_rmb;
    private LinearLayout ly_attach, ly_refund;
    //private View id_ly_rmb;
    private TextView tv_attach, id_parser_money, tx_surcharge, tx_withholding;
    private View id_line_lay_refund_peo, id_line_timecomplete;
    private int Tag = 0;

    public static void startActivity(Context context, Order orderModel) {
        Intent it = new Intent();
        it.setClass(context, RefundRecordOrderDetailsActivity.class);
        it.putExtra("order", orderModel);
        context.startActivity(it);
    }

    @Override
    protected RefundRecordOrderDetailsContract.Presenter createPresenter() {
        return new RefundRecordOrderDetailsPresenter();
    }


    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.refund_order_details);
        orderModel = (Order) getIntent().getSerializableExtra("order");
        Tag = getIntent().getIntExtra("Tag", 0);
        setupInitViews();
        setLister();
        //表示来源于OrderDetailActivity
        if (null != orderModel && QUERY_REFUND == Tag) {
            getRefundData(orderModel);
        } else {
            initData();
        }
    }

    void setLister() {
        blue_print.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                bluePrint();
                orderModel.setPrintInfo(getString(R.string.tv_refunding_info));
                orderModel.setPay(false);
                orderModel.setAddTimeNew(orderModel.getRefundTime());
                TicketPreviewActivity.Companion.startTicketPreviewActivity(RefundRecordOrderDetailsActivity.this,
                        orderModel);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印的绑定操作
        if (IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)) {
            PrintClient.getInstance().bindDeviceService();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印的解绑操作
        if (IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)) {
            PrintClient.getInstance().unbindDeviceService();
        }
    }

    @SuppressLint("NewApi")
    void bluePrint() {
        orderModel.setPrintInfo(getString(R.string.tv_refunding_info));
        orderModel.setPay(false);
        orderModel.setAddTimeNew(orderModel.getRefundTime());
        orderModel.setPartner(getString(R.string.tv_pay_user_stub));

        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印
        if (IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)) {
            if (BuildConfig.posTerminal.equalsIgnoreCase("A8") || BuildConfig.posTerminal.equalsIgnoreCase("BBPos")) {
                orderModel.setPrintInfo(getString(R.string.tv_refunding_info_A8));
            }
            PrintOrder.printOrderDetails(RefundRecordOrderDetailsActivity.this, false, orderModel);
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    try {
                        orderModel.setPay(false);
                        orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
                        PrintOrder.printOrderDetails(RefundRecordOrderDetailsActivity.this, false, orderModel);
                    } catch (Exception e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                    }
                }
            };
            Timer timer = new Timer();
            timer.schedule(task, PrintClient.getInstance().getPrintDelay());

        } else {
            bluePrintWithBlueToothPermission();
        }
    }

    /**
     * 获得蓝牙权限后进行打印
     */
    private void bluePrintWithBlueToothPermission() {
        if (!MainApplication.getInstance().getBluePrintSetting() && !BuildConfig.IS_POS_VERSION) {//关闭蓝牙打印
            showDialog();
            return;
        }
        if (MainApplication.getInstance().getBluetoothSocket() != null && MainApplication.getInstance().getBluetoothSocket().isConnected()) {
            RefundRecordOrderDetailsActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        BluePrintUtil.print(false, orderModel);
                        //打印第二联
                        try {
                            Thread thread = Thread.currentThread();
                            thread.sleep(3000);
                            orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
                            BluePrintUtil.print(false, orderModel);
                        } catch (InterruptedException e) {
                            Log.e(TAG, Log.getStackTraceString(e));
                        }
                    } catch (Exception e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                        Log.e(TAG, Log.getStackTraceString(e));
                    }
                }
            });
        } else {
            String bluetoothDeviceAddress = MainApplication.getInstance().getBlueDeviceAddress();
            if (!StringUtil.isEmptyOrNull(bluetoothDeviceAddress)) {
                boolean isSucc = BluePrintUtil.blueConnent(bluetoothDeviceAddress, RefundRecordOrderDetailsActivity.this);
                if (isSucc) {
                    try {
                        BluePrintUtil.print(false, orderModel);
                        Thread thread = Thread.currentThread();
                        thread.sleep(3000);
                        orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
                        BluePrintUtil.print(false, orderModel);
                    } catch (InterruptedException e) {
                        Log.e(TAG, Log.getStackTraceString(e));
                    }
                }
            } else {
                showDialog();
            }
        }
    }

    void showDialog() {
        dialog = new DialogInfo(RefundRecordOrderDetailsActivity.this, getString(R.string.public_cozy_prompt), getString(R.string.tx_blue_set), getString(R.string.title_setting), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                dialog.cancel();
                dialog.dismiss();
                showPage(BluetoothSettingActivity.class);
            }

            @Override
            public void handleCancelBtn() {
                dialog.cancel();
            }
        }, null);

        DialogHelper.resize(RefundRecordOrderDetailsActivity.this, dialog);
        dialog.show();
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tx_refund_detail);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {
                // TODO Auto-generated method stub

            }

            @Override
            public void onRightButLayClick() {
                // TODO Auto-generated method stub

            }
        });
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    private void setupInitViews() {
        //context = this;
        blue_print = findViewById(R.id.ticket_preview);
        id_parser_money = findViewById(R.id.id_parser_money);
        //tx_rmb = findViewById(R.id.tx_rmb);
        tx_surcharge = findViewById(R.id.tx_surcharge);
        tx_withholding = findViewById(R.id.tx_withholding);
        id_lin_withholding = findViewById(R.id.id_lin_withholding);
        id_lin_surcharge = findViewById(R.id.id_lin_surcharge);
        id_order_line = findViewById(R.id.id_order_line);
        id_total = findViewById(R.id.id_total);
        //id_ly_rmb = findViewById(R.id.id_ly_rmb);
        ly_refund = findViewById(R.id.ly_refund);
        ly_attach = findViewById(R.id.ly_attach);
        tv_attach = findViewById(R.id.tv_attach);
        lay_refund_peo = findViewById(R.id.lay_refund_peo);
        refund_money = findViewById(R.id.refund_money);
        lay_refund_time = findViewById(R.id.lay_refund_time);
        refund_time = findViewById(R.id.refund_time);
        spay_pay_client = findViewById(R.id.spay_pay_client);
        iv_code = findViewById(R.id.iv_code);
        tv_code = findViewById(R.id.tv_code);
        tv_transNo = findViewById(R.id.tv_transNo);
        logo_lay = findViewById(R.id.logo_lay);
        id_line_lay_refund_peo = findViewById(R.id.id_line_lay_refund_peo);
        ll_pay_top = findViewById(R.id.ll_pay_top);
        logo_title = findViewById(R.id.logo_title);
        refund_apply_time = findViewById(R.id.refund_apply_time);
        refund_apply_peop = findViewById(R.id.refund_apply_peop);
        refund_status = findViewById(R.id.refund_status);
        refund_no = findViewById(R.id.refund_no);
        body_info = findViewById(R.id.body_info);
        pay_mch = findViewById(R.id.pay_mch);
        pay_method = findViewById(R.id.pay_method);
        tvOrderCode = findViewById(R.id.tvOrderCode);
        //llBanktype = findViewById(R.id.ll_banktype);
        //tvBankTypeTitle = findViewById(R.id.tv_banktypeTitle);
        tvMoney = findViewById(R.id.tv_money);
        //tvAddTime = findViewById(R.id.tv_addtime);
        //ivPayType = findViewById(R.id.iv_payType);

        tvState = findViewById(R.id.tv_state);
        btnRefund = findViewById(R.id.btn_refund);
        iv_pay_img = findViewById(R.id.iv_pay_img);
        //iv_pay_image = findViewById(R.id.iv_pay_image);

        llRefund = findViewById(R.id.ll_refund_code);
        //tvRefundCode = findViewById(R.id.tv_refund_code);
        // tvUser = findViewById(R.id.tv_user);

        wx_tvOrderCode = findViewById(R.id.wx_tvOrderCode);
        // pay_top_re = findViewById(R.id.pay_top_re);

        //tvBankType = findViewById(R.id.tv_banktype);


        //refundModel = (RefundModel) getIntent().getSerializableExtra("refund");

        //llTransactionId = findViewById(R.id.ll_transactionId);
        //tvTransactionId = findViewById(R.id.tv_transactionId);

        //llNotifyTime = findViewById(R.id.ll_notifytime);
        tvNotifyTime = findViewById(R.id.tv_notifyTime);

        wx_title_info = findViewById(R.id.wx_title_info);

        //lr_wx = (LinearLayout) findViewById(R.id.lr_wx);

        //tvTitleTime = findViewById(R.id.tv_titletime);
        //signatureView = findViewById(R.id.signature_pic);
        pay_mchId = findViewById(R.id.pay_mchId);
        cashierText = findViewById(R.id.cashierText);
        cashierLay = findViewById(R.id.cashierLay);
        id_line_timecomplete = findViewById(R.id.line_time_comlete);
    }

    private void initData() {

        if (null != orderModel && !StringUtil.isEmptyOrNull(orderModel.getAttach())) {
            ly_attach.setVisibility(View.GONE);
            tv_attach.setText(orderModel.getAttach());
        }

        if (null != orderModel && !StringUtil.isEmptyOrNull(orderModel.getUserName()) && !StringUtil.isEmptyOrNull(orderModel.getUseId())) {
            cashierText.setText(orderModel.getUserName() + "(" + orderModel.getUseId() + ")");
        } else {
            cashierLay.setVisibility(View.GONE);
        }

        if (orderModel != null) {
            if (orderModel.getCashFeel() > 0) {
                id_parser_money.setVisibility(View.VISIBLE);
                id_parser_money.setText(getString(R.string.tx_about) + getString(R.string.tx_mark) + DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()));
            } else {
                id_parser_money.setVisibility(View.GONE);
            }

            if (!MainApplication.getInstance().isSurchargeOpen()) {
                id_lin_surcharge.setVisibility(View.GONE);
                id_order_line.setVisibility(View.GONE);
            } else {
                if (orderModel != null) {
                    if (orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") || orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")) {
                        id_lin_surcharge.setVisibility(View.GONE);
                        id_order_line.setVisibility(View.GONE);
                    } else {
                        tx_surcharge.setText(MainApplication.getInstance().getUserInfo().feeFh
                                + DateUtil.formatMoneyUtils(orderModel.getSurcharge()));
                    }
                }

            }

            /*if (!MainApplication.isTaxRateOpen()) {
                id_lin_withholding.setVisibility(View.GONE);
            } else {
                tx_withholding.setText(MainApplication.feeFh + DateUtil.formatMoneyUtils(orderModel.getWithholdingTax()));
            }*/
            id_lin_withholding.setVisibility(View.GONE);

            id_total.setText(MainApplication.getInstance().getUserInfo().feeFh
                    + DateUtil.formatMoneyUtils(orderModel.getOrderFee()));
            pay_mchId.setText(MainApplication.getInstance().getMchId());

            if (!isAbsoluteNullStr(orderModel.getClient())) {
                spay_pay_client.setText(ClientEnum.getClienName(orderModel.getClient()));
            } else {
                spay_pay_client.setText(ClientEnum.UNKNOW.clientType);
            }

//            // 生成一维码
//            if (!isAbsoluteNullStr(orderModel.getOrderNoMch())) {
//
//                WindowManager wm = this.getWindowManager();
//                int width = wm.getDefaultDisplay().getWidth();
//                int w = (int) (width * 0.85);
//                iv_code.setImageBitmap(CreateOneDiCodeUtil.createCode(orderModel.getOrderNoMch(), w, 180));
//                tv_code.setText(orderModel.getOrderNoMch());
//            }
//
//            if (!MainApplication.mchLogo.equals("") && !MainApplication.mchLogo.equals("null") && MainApplication.mchLogo != null) {
//                logo_lay.setVisibility(View.VISIBLE);
//                ll_pay_top.setVisibility(View.GONE);
//            }

            if (!"".equals(orderModel.getBody()) && !"null".equals(orderModel.getBody()) && null != orderModel.getBody()) {
                body_info.setText(orderModel.getBody());
            }
            tv_transNo.setText(orderModel.getOrderNoMch());
            //            pay_method.setText(orderModel.tradeName);
            pay_mch.setText(MainApplication.getInstance().getUserInfo().mchName);
            llRefund.setVisibility(View.GONE);
            //            btnRefund.setVisibility(View.VISIBLE);
            //根据当前的小数点的位数来判断应该除以多少
            double min = 1;
            for (int i = 0; i < MainApplication.getInstance().getNumFixed(); i++) {
                min = min * 10;
            }
            double money = orderModel.getTotalFee() / min;
            tvOrderCode.setText(orderModel.getOrderNoMch());
            wx_tvOrderCode.setText(orderModel.transactionId);
            refund_apply_time.setText(orderModel.getAddTime());
            tvMoney.setText(MainApplication.getInstance().getUserInfo().feeFh + DateUtil.formatMoneyUtil(money));
            refund_time.setText(orderModel.getRefundTime());
            try {

                if (!TextUtils.isEmpty(orderModel.getTradeTime())) {
                    tvNotifyTime.setText(DateUtil.formatTime(Long.parseLong(orderModel.getTradeTime())));
                }
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                tvNotifyTime.setText(orderModel.getRefundTime());
            }

            if (!"".equals(orderModel.getUserName()) && null != orderModel.getUserName() && !orderModel.getUserName().equals("null")) {
                refund_apply_peop.setText(orderModel.getUserName() + "(" + orderModel.getUseId() + ")");

            } else {
                if (MainApplication.getInstance().isAdmin(1) || MainApplication.getInstance().isOrderAuth("1")) {
                    if (!TextUtils.isEmpty(orderModel.getMchName()) && !TextUtils.isEmpty(orderModel.getUseId()) && !orderModel.getUseId().equals("null")) {
                        refund_apply_peop.setText(orderModel.getMchName() + "(" + orderModel.getUseId() + ")");
                    }
                }
            }

            if (StringUtil.isEmptyOrNull(refund_apply_peop.getText().toString())) {
                lay_refund_peo.setVisibility(View.GONE);
                id_line_lay_refund_peo.setVisibility(View.GONE);
            }

            if (orderModel.getRefundMoney() > 0) {
                refund_money.setText(MainApplication.getInstance().getUserInfo().feeFh
                        + DateUtil.formatMoneyUtils(orderModel.getRefundMoney()));
            } else {
                findViewById(R.id.refund_money_layout).setVisibility(View.GONE);
            }

            refund_no.setText(orderModel.getRefundNo());

            tvState.setText(R.string.tv_payment);
            if (orderModel.getTradeType().equalsIgnoreCase(Constant.PAY_QQ_NATIVE)
                    || orderModel.getTradeType().equalsIgnoreCase(Constant.PAY_QQ_NATIVE1)
                    || orderModel.getTradeType().equalsIgnoreCase(Constant.PAY_QQ_WAP)
                    || orderModel.getTradeType().contains(Constant.PAY_QQ_TAG)) {
                wx_title_info.setText(R.string.qq_odd_numbers);
                pay_method.setText(R.string.qq_scan_pay);
            } else if (orderModel.getTradeType().startsWith(Constant.PAY_ZFB_NATIVE)
                    || orderModel.getTradeType().startsWith(Constant.PAY_ALIPAY_WAP)
                    || orderModel.getTradeType().contains(Constant.PAY_ALIPAY_TAG)) {
                wx_title_info.setText(R.string.zfb_odd_numbers);
                //                pay_method.setText("支付宝扫码支付");
            } else if (orderModel.getTradeType().equals(Constant.PAY_ZFB_MICROPAY)) {
                wx_title_info.setText(R.string.zfb_odd_numbers);
                //                pay_method.setText("支付宝付款码支付");
            } else if (orderModel.getTradeType().equalsIgnoreCase(Constant.PAY_JINGDONG)
                    || orderModel.getTradeType().equalsIgnoreCase(Constant.PAY_JINGDONG_NATIVE)
                    || orderModel.getTradeType().contains(Constant.PAY_JD_TAG)) {
                wx_title_info.setText(R.string.tx_pay_jd_order);
                iv_pay_img.setImageResource(R.drawable.icon_list_jd);
            } else if (orderModel.getTradeType().equals(Constant.PAY_QQ_MICROPAY)
                    || orderModel.getTradeType().equals(Constant.PAY_QQ_PROXY_MICROPAY)
                    || orderModel.getTradeType().equalsIgnoreCase(Constant.PAY_QQ_WAP)) {
                wx_title_info.setText(R.string.qq_odd_numbers);
                pay_method.setText(R.string.qq_code_pay);
                iv_pay_img.setImageResource(R.drawable.icon_qq_color);
            }

            if (!TextUtils.isEmpty(orderModel.getTradeName())) {
                pay_method.setText(orderModel.getTradeName());
            } else {
                pay_method.setText(Order.TradeTypetoStr.getTradeNameForType(orderModel.getTradeType()));
            }
            if (orderModel.getTradeType().startsWith(Constant.PAY_WX_MICROPAY)
                    || orderModel.getTradeType().equals(Constant.PAY_WX_SJPAY)) {
                // ivPayType.setImageResource(R.drawable.picture_pay_barcode);
                iv_pay_img.setImageResource(R.drawable.icon_cancel_wechat);
            } else if (orderModel.getTradeType().equalsIgnoreCase(Constant.PAY_QQ_NATIVE)
                    || orderModel.getTradeType().equals(Constant.PAY_QQ_MICROPAY)
                    || orderModel.getTradeType().equals(Constant.PAY_QQ_PROXY_MICROPAY)
                    || orderModel.getTradeType().equalsIgnoreCase(Constant.PAY_QQ_NATIVE1)
                    || orderModel.getTradeType().equals(Constant.PAY_QQ_WAP)
                    || orderModel.getTradeType().contains(Constant.PAY_QQ_TAG)) {
                // ivPayType.setImageResource(R.drawable.qq_logo);
                iv_pay_img.setImageResource(R.drawable.icon_qq_color);
            } else if (orderModel.getTradeType().startsWith(Constant.PAY_ZFB_NATIVE)
                    || orderModel.getTradeType().equals(Constant.PAY_ZFB_MICROPAY)
                    || orderModel.getTradeType().equals(Constant.PAY_ZFB_WAP)
                    || orderModel.getTradeType().contains(Constant.PAY_ALIPAY_TAG)) {
                // ivPayType.setImageResource(R.drawable.zfb_logo);
                iv_pay_img.setImageResource(R.drawable.icon_alipay_color);
            } else if (orderModel.getTradeType().equalsIgnoreCase(Constant.PAY_JINGDONG)
                    || orderModel.getTradeType().equalsIgnoreCase(Constant.PAY_JINGDONG_NATIVE)
                    || orderModel.getTradeType().contains(Constant.PAY_JD_TAG)) {
                iv_pay_img.setImageResource(R.drawable.icon_list_jd);
            } else {
                // ivPayType.setImageResource(R.drawable.n_pay_weixin);
                iv_pay_img.setImageResource(R.drawable.icon_cancel_wechat);
            }

            switch (orderModel.getRefundState()) {
                case 1:
                    blue_print.setVisibility(View.VISIBLE);
                    lay_refund_time.setVisibility(View.VISIBLE);
                    id_line_timecomplete.setVisibility(View.VISIBLE);
                    refund_status.setText(R.string.refund_accepted);
                    ly_refund.setVisibility(View.VISIBLE);
                    refund_status.setTextColor(getResources().getColor(R.color.pay_fail));
                    break;
                case 2:
                    blue_print.setVisibility(View.GONE);
                    refund_status.setText(R.string.refund_failure);
                    refund_status.setTextColor(getResources().getColor(R.color.pay_fail));
                    lay_refund_time.setVisibility(View.GONE);
                    id_line_timecomplete.setVisibility(View.GONE);
                    break;
                case 0:
                    blue_print.setVisibility(View.GONE);
                    refund_status.setText(R.string.tx_verify_loading);
                    refund_status.setTextColor(getResources().getColor(R.color.paytype_title_reset));
                    lay_refund_time.setVisibility(View.GONE);
                    id_line_timecomplete.setVisibility(View.GONE);

                    break;
                case 3:
                    blue_print.setVisibility(View.VISIBLE);
                    ly_refund.setVisibility(View.VISIBLE);
                    lay_refund_time.setVisibility(View.VISIBLE);
                    id_line_timecomplete.setVisibility(View.VISIBLE);
                    refund_status.setText(R.string.refund_accepted);
                    refund_status.setTextColor(getResources().getColor(R.color.pay_fail));
                    break;
            }
            if (MainApplication.getInstance().getRefundStateMap() != null && !TextUtils.isEmpty(orderModel.getRefundState() + "")) {
                refund_status.setText(MainApplication.getInstance().getRefundStateMap().get(orderModel.getRefundState() + ""));
            } else {
                refund_status.setText("");
            }

            if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getPayTypeMap().get(orderModel.getApiCode()))) {
                String language = PreferenceUtil.getString("language", "");
                Locale locale = MainApplication.getInstance().getResources().getConfiguration().locale; //zh-rHK
                String lan = locale.getCountry();
                if (!TextUtils.isEmpty(language)) {
                    if (language.equals(Constant.LANG_CODE_EN_US)) {
                        wx_title_info.setText(MainApplication.getInstance().getPayTypeMap().get(orderModel.getApiCode()) + " " + getString(R.string.tx_orderno));
                    } else {
                        wx_title_info.setText(MainApplication.getInstance().getPayTypeMap().get(orderModel.getApiCode()) + getString(R.string.tx_orderno));
                    }
                } else {
                    if (lan.equalsIgnoreCase("en")) {
                        wx_title_info.setText(MainApplication.getInstance().getPayTypeMap().get(orderModel.getApiCode()) + " " + getString(R.string.tx_orderno));
                    } else {
                        wx_title_info.setText(MainApplication.getInstance().getPayTypeMap().get(orderModel.getApiCode()) + getString(R.string.tx_orderno));
                    }

                }
            }

        }
    }


    @Override
    public void queryRefundDetailSuccess(@Nullable Order response) {
        if (response != null) {
            orderModel = response;
            RefundRecordOrderDetailsActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    initData();
                }
            });

        }
    }

    @Override
    public void queryRefundDetailFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }
        if (error != null) {
            toastDialog(RefundRecordOrderDetailsActivity.this, error.toString(), null);
        }
    }

    /**
     * 获取订单详情接口
     *
     * @param order
     */
    private void getRefundData(Order order) {
        if (mPresenter != null) {
            mPresenter.queryRefundDetail(order.getOutRefundNo(), MainApplication.getInstance().getMchId());
        }
    }

    /**
     * 暂时不支持退款
     */
    //
    public void onRefund(View v) {
        /*
         * if (ApiConstant.ISOVERSEASY) { showToastInfo("暂不支持境外退款，谢谢合作！"); }
         * else {
         */
        //
        // if (orderModel.payType == PayType.PAY_POS.getValue())
        // {
        // showToastInfo("暂不支持POS退款，谢谢合作！");
        // return;
        // }
        if (orderModel != null) {
            // 目前只能退当天
            // String notifyTime =
            // DateUtil.formatYYMD(Long.parseLong(orderModel.notifyTime));
            // String nowTime = DateUtil.formatYYMD(System.currentTimeMillis());
            // if (!notifyTime.equalsIgnoreCase(nowTime))
            // {
            // showConfirm("退款只能退当天的!", new OnConfirmListener()
            // {
            // @Override
            // public void onOK()
            // {
            // OrderDetailsActivity.this.finish();
            // }
            //
            // @Override
            // public void onCancel()
            // {
            // }
            // });
            // return;
            // }

            // 退款需要输入密码
            //            RefundCheckDialog dialog1 =
            //                new RefundCheckDialog(this, RefundCheckDialog.REFUND, "请输入登录密码", orderModel.outTradeNo,
            //                    String.valueOf(orderModel.money), new RefundCheckDialog.ConfirmListener()
            //                    {
            //                        @Override
            //                        public void ok(String code)
            //                        {
            //                            RefundConditonConfirmDialog dialog;
            //                            dialog = new RefundConditonConfirmDialog(context, "温馨提示,是否确定退款？", orderModel.money, btnListener);
            //                            dialog.show();
            //                        }
            //                        
            //                        @Override
            //                        public void cancel()
            //                        {
            //                            
            //                        }
            //                    });
            //            dialog1.show();

        }
        // else if (refundModel != null)
        // {
        // //审核
        // RefundManager.getInstant().checkRefund(refundModel.id, new
        // UINotifyListener<Boolean>()
        // {
        // @Override
        // public void onError(Object object)
        // {
        // super.onError(object);
        // if (object != null)
        // {
        // showToastInfo(object.toString());
        // }
        // }
        //
        // @Override
        // public void onPreExecute()
        // {
        // super.onPreExecute();
        // showLoading(true, "请稍候...");
        // }
        //
        // @Override
        // public void onPostExecute()
        // {
        // super.onPostExecute();
        // dismissLoading();
        // }
        //
        // @Override
        // public void onSucceed(Boolean result)
        // {
        // super.onSucceed(result);
        // if (result)
        // {
        // showToastInfo("操作成功！");
        // }
        // }
        // });
        // }
        // }
    }

//    private RefundDialog.ConfirmListener btnListener = new RefundDialog.ConfirmListener() {
//        @Override
//        public void ok(long money) {
//            // if (orderModel.transactionType != PayType.PAY_WX.getValue()) //
//            // 微信反扫
//            // {
//            // refundNum = orderModel.outTradeNo;
//            // }
//            // else
//            // {
//            refundNum = orderModel.outTradeNo;
//            // }
//            regisRefund(money);
//        }
//
//        @Override
//        public void cancel() {
//
//        }
//    };

    private void regisRefund(long money) {
//        String transaction = null;
//        RefundManager.getInstant().regisRefund(refundNum, transaction, orderModel.money, money, new UINotifyListener<Boolean>() {
//            @Override
//            public void onError(final Object object) {
//                dismissLoading();
//                super.onError(object);
//                if (object != null) {
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            showToastInfo(object.toString());
//                        }
//                    });
//
//                }
//            }
//
//            @Override
//            public void onPreExecute() {
//                super.onPreExecute();
//
//                showLoading("退款中，请稍候..");
//            }
//
//            @Override
//            public void onPostExecute() {
//                super.onPostExecute();
//                dismissLoading();
//            }
//
//            @Override
//            public void onSucceed(Boolean result) {
//                dismissLoading();
//                super.onSucceed(result);
//                if (result) {
//                    showToastInfo("申请退款成功,一般发起退款后1-3工作日将原来返回给顾客,请注意查收");
//                    btnRefund.setEnabled(false);
//
//                    // OrderStreamActivity.startActivity(OrderDetailsActivity.this,
//                    // true);
//                    //                    MainActivity.startActivity(RefundOrderDetailsActivity.this, "OrderDetailsActivity"); // 退款成功跳转
//                    //                    MainApplication.isRefund = true;
//                    finish();
//                }
//            }
//        });
    }

    public void showBigOneCode(View view) {
        if (orderModel != null) {
//            OneDiCodeActivity.startActivity(RefundRecordOrderDetailsActivity.this, orderModel.getOrderNoMch());
            RotateCanvasViewActivity.startActivity(RefundRecordOrderDetailsActivity.this, orderModel.getOrderNoMch());
        }
    }
}
