package cn.swiftpass.enterprise.ui.activity.setting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.example.common.utils.PngSetting;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.common.Constant;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.SettingSwitchLanContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.SettingSwitchLanPresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.activity.spayMainTabActivity;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.LocaleUtils;
import cn.swiftpass.enterprise.utils.PngUtils;

/**
 * 设置 会自动检查版本 User: ALAN Date: 13-10-28 Time: 下午5:11 To change this template use
 * File | Settings | File Templates.
 */
public class SettingSwitchLanActivity extends BaseActivity<SettingSwitchLanContract.Presenter> implements SettingSwitchLanContract.View {

    private LinearLayout ly_close, ly_open, ly_china;
    private ImageView iv_open, iv_close, iv_china;

    @Override
    protected SettingSwitchLanContract.Presenter createPresenter() {
        return new SettingSwitchLanPresenter();
    }


    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    public void getBaseSuccess(boolean response) {
        for (Activity a : MainApplication.getInstance().getAllActivities()) {
            a.finish();
        }

        Intent it = new Intent();
        it.setClass(SettingSwitchLanActivity.this, spayMainTabActivity.class);
        startActivity(it);
    }

    @Override
    public void getBaseFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }
        finish();

        Intent it = new Intent();
        it.setClass(SettingSwitchLanActivity.this, spayMainTabActivity.class);
        startActivity(it);
    }

    void getBase() {

        if (mPresenter != null) {
            mPresenter.getBase();
        }

    }

    private void initViews() {
        setContentView(R.layout.activity_setting_switch_lan);

        iv_china = getViewById(R.id.iv_china);
        ly_china = getViewById(R.id.ly_china);
        iv_open = getViewById(R.id.iv_open);
        iv_close = getViewById(R.id.iv_close);
        ly_close = getViewById(R.id.ly_close);
        ly_open = getViewById(R.id.ly_open);

        String language = LocaleUtils.getLocaleLanguage();

        iv_open.setVisibility(language.equals(Constant.LANG_CODE_EN_US)
                ? View.VISIBLE : View.GONE);
        iv_china.setVisibility(language.equals(Constant.LANG_CODE_ZH_CN)
                ? View.VISIBLE : View.GONE);
        iv_close.setVisibility(language.equals(Constant.LANG_CODE_ZH_TW)
                ? View.VISIBLE : View.GONE);

        ly_china.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                iv_china.setVisibility(View.VISIBLE);
                iv_open.setVisibility(View.GONE);
                iv_close.setVisibility(View.GONE);

                LocaleUtils.switchLanguage(SettingSwitchLanActivity.this,
                        Constant.LANG_CODE_ZH_CN);

                getBase();
            }
        });

        ly_open.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                iv_open.setVisibility(View.VISIBLE);
                iv_close.setVisibility(View.GONE);
                iv_china.setVisibility(View.GONE);

                LocaleUtils.switchLanguage(SettingSwitchLanActivity.this,
                        Constant.LANG_CODE_EN_US);

                getBase();


            }
        });

        ly_close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                iv_open.setVisibility(View.GONE);
                iv_close.setVisibility(View.VISIBLE);
                iv_china.setVisibility(View.GONE);

                LocaleUtils.switchLanguage(SettingSwitchLanActivity.this,
                        Constant.LANG_CODE_ZH_TW);

                getBase();
            }
        });


        if (PngSetting.INSTANCE.isChangePng1()) {
            initPng();
        }
    }

    private void initPng() {
        setChooseView(iv_open);
        setChooseView(iv_close);
        setChooseView(iv_china);
    }

    private void setChooseView(ImageView imageView) {
        PngUtils.INSTANCE.setPng1(this, imageView, R.drawable.icon_list_chosen);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initViews();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected boolean isLoginRequired() {
        return false;
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_common_yuyan);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButLayClick() {

            }
        });
    }


}