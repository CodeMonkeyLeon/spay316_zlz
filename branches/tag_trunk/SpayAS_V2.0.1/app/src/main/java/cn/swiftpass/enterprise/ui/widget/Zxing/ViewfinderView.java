/*
 * Copyright (C) 2008 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.swiftpass.enterprise.ui.widget.Zxing;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.google.zxing.ResultPoint;

import java.util.Collection;
import java.util.HashSet;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DisplayUtil;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.ScreenUtils;

/**
 * This view is overlaid on top of the camera preview. It adds the viewfinder
 * rectangle and partial transparency outside it, as well as the laser scanner
 * animation and result points.
 *
 * @author dswitkin@google.com (Daniel Switkin)
 */
public final class ViewfinderView extends View {

    private static final int[] SCANNER_ALPHA = {0, 64, 128, 192, 255, 192, 128, 64};

    private static final long ANIMATION_DELAY = 100L;

    private static final int OPAQUE = 0xFF;

    private final Paint paint;

    private Bitmap resultBitmap;

    private final int maskColor;

    private final int resultColor;

    private final int frameColor;

    private final int resultPointColor;

    private int scannerAlpha;

    /**
     * 四个绿色边角对应的长度
     */
    private int ScreenRate;

    private Collection<ResultPoint> possibleResultPoints;

    private Collection<ResultPoint> lastPossibleResultPoints;

    //private GradientDrawable mDrawable;// 采用渐变图作为扫描线

    private int i = 0;// 添加的

    private Rect mRect;// 扫描线填充边界

    private Drawable lineDrawable;// 采用图片作为扫描线

    /**
     * 四个绿色边角对应的宽度
     */
    private static final int CORNER_WIDTH = 10;

    private Context context;

    /**
     * 手机的屏幕密度
     */
    private static float density;

    private boolean isScan = true; // 默认开启扫描

    public Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case 0: // 开始扫描
                    isScan = true;
                    invalidate();
                    break;
                case 1: // 停止扫描
                    isScan = false;
                    break;

            }
        }

    };

    // This constructor is used when the class is built from an XML resource.
    public ViewfinderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        paint = new Paint();
        Resources resources = getResources();
        maskColor = resources.getColor(R.color.viewfinder_mask);
        resultColor = resources.getColor(R.color.result_view);
        frameColor = resources.getColor(R.color.title_bg_new);
        //  laserColor = resources.getColor(R.color.viewfinder_laser);
        resultPointColor = resources.getColor(R.color.possible_result_points);
        scannerAlpha = 0;
        possibleResultPoints = new HashSet<ResultPoint>(5);
        mRect = new Rect();
        lineDrawable = getResources().getDrawable(R.drawable.qrcode_scan_line);

        density = context.getResources().getDisplayMetrics().density;
        //将像素转换成dp
        ScreenRate = (int) (20 * density);

        HandlerManager.registerHandler(HandlerManager.SCANCODE, handler);

    }

    @Override
    public void onDraw(Canvas canvas) {
        //        Rect frame = CameraManager.get().getFramingRect();
        Rect frame = new Rect();
        setWillNotDraw(false);
        LayoutInflater inflater = LayoutInflater.from(context);
        RelativeLayout relativeLayout = (RelativeLayout) inflater.inflate(R.layout.layout_qr_code_scanner, null);
        FrameLayout frameLayout = (FrameLayout) relativeLayout.findViewById(R.id.qr_code_fl_scanner);
        RelativeLayout.LayoutParams layoutParams = (LayoutParams) frameLayout.getLayoutParams();
        if (null != frame && null != layoutParams) {
            frame.left = (ScreenUtils.getScreenWidth(context) - layoutParams.width) / 2;
            frame.top = layoutParams.topMargin + DisplayUtil.dip2Px(context, 25);
            frame.right = frame.left + layoutParams.width;
            frame.bottom = frame.top + layoutParams.height;
        }else{
            return;
        }
        int width = canvas.getWidth();
        int height = canvas.getHeight();
        paint.setColor(resultBitmap != null ? resultColor : maskColor);
        //public voiddrawRect(float left, float top, float right, float bottom,Paint paint)
        // 头部
        canvas.drawRect(0, 0, width, frame.top, paint);
        // 左边
        canvas.drawRect(0, frame.top, frame.left, frame.bottom, paint);
        //        // 右边
        canvas.drawRect(frame.right, frame.top, width, frame.bottom, paint);
        //        // 底部
        canvas.drawRect(0, frame.bottom, width, height, paint);
        //        
        if (resultBitmap != null) {
            // 在扫描框中画出预览图
            paint.setAlpha(OPAQUE);
            canvas.drawBitmap(resultBitmap, null, frame, paint);
        } else {
            paint.setColor(frameColor);
            // 左上角
            //            canvas.drawRect(frame.left, frame.top, frame.left + 15, frame.top + 5, paint);
            //            canvas.drawRect(frame.left, frame.top, frame.left + 5, frame.top + 15, paint);
            //            // 右上角
            //            canvas.drawRect(frame.right - 15, frame.top, frame.right, frame.top + 5, paint);
            //            canvas.drawRect(frame.right - 5, frame.top, frame.right, frame.top + 15, paint);
            //            // 左下角
            //            canvas.drawRect(frame.left, frame.bottom - 5, frame.left + 15, frame.bottom, paint);
            //            canvas.drawRect(frame.left, frame.bottom - 15, frame.left + 5, frame.bottom, paint);
            //            // 右下角
            //            canvas.drawRect(frame.right - 15, frame.bottom - 5, frame.right, frame.bottom, paint);
            //            canvas.drawRect(frame.right - 5, frame.bottom - 15, frame.right, frame.bottom, paint);

            canvas.drawRect(frame.left, frame.top, frame.left + ScreenRate, frame.top + CORNER_WIDTH, paint);
            canvas.drawRect(frame.left, frame.top, frame.left + CORNER_WIDTH, frame.top + ScreenRate, paint);

            canvas.drawRect(frame.right - ScreenRate, frame.top, frame.right, frame.top + CORNER_WIDTH, paint);
            canvas.drawRect(frame.right - CORNER_WIDTH, frame.top, frame.right, frame.top + ScreenRate, paint);

            canvas.drawRect(frame.left, frame.bottom - CORNER_WIDTH, frame.left + ScreenRate, frame.bottom, paint);
            canvas.drawRect(frame.left, frame.bottom - ScreenRate, frame.left + CORNER_WIDTH, frame.bottom, paint);

            canvas.drawRect(frame.right - ScreenRate, frame.bottom - CORNER_WIDTH, frame.right, frame.bottom, paint);
            canvas.drawRect(frame.right - CORNER_WIDTH, frame.bottom - ScreenRate, frame.right, frame.bottom, paint);

            scannerAlpha = (scannerAlpha + 1) % SCANNER_ALPHA.length;

            //将扫描线修改为上下走的线
            if ((i += 3) < frame.bottom - frame.top) {
                /* 以下为图片作为扫描线 */
                mRect.set(frame.left + DisplayUtil.dip2Px(context, 1), frame.top + i - DisplayUtil.dip2Px(context, 1), frame.right - DisplayUtil.dip2Px(context, 1), frame.top + DisplayUtil.dip2Px(context, 1) + i);
                lineDrawable.setBounds(mRect);
                lineDrawable.draw(canvas);
                // 刷新
                invalidate();
            } else {
                i = 0;
            }

            Collection<ResultPoint> currentPossible = possibleResultPoints;
            Collection<ResultPoint> currentLast = lastPossibleResultPoints;
            if (currentPossible.isEmpty()) {
                lastPossibleResultPoints = null;
            } else {
                possibleResultPoints = new HashSet<ResultPoint>(5);
                lastPossibleResultPoints = currentPossible;
                paint.setAlpha(OPAQUE);
                paint.setColor(resultPointColor);
                for (ResultPoint point : currentPossible) {
                    canvas.drawCircle(frame.left + point.getX(), frame.top + point.getY(), 6.0f, paint);
                }
            }
            if (currentLast != null) {
                paint.setAlpha(OPAQUE / 2);
                paint.setColor(resultPointColor);
                for (ResultPoint point : currentLast) {
                    canvas.drawCircle(frame.left + point.getX(), frame.top + point.getY(), 3.0f, paint);
                }
            }
            if (isScan) {
                postInvalidateDelayed(ANIMATION_DELAY, frame.left, frame.top, frame.right, frame.bottom);
            }
        }
    }

    public void drawViewfinder() {
        resultBitmap = null;
        invalidate();
    }

    public void addPossibleResultPoint(ResultPoint point) {
        possibleResultPoints.add(point);
    }

    /**
     * Draw a bitmap with the result points highlighted instead of the live scanning display.
     *
     * @param barcode An image of the decoded barcode.
     */
    public void drawResultBitmap(Bitmap barcode) {
        resultBitmap = barcode;
        invalidate();
    }
}
