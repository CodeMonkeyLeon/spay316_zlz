/*
 * 文 件 名:  ShopBankDataTab.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2014-3-20
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.io.database.table;

import android.database.sqlite.SQLiteDatabase;

/**
 * 商户基本 银行信息
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2014-3-20]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class ShopBankDataTab extends TableBase
{
    
    // 商户银行基本数据
    public static final String TABLE_NAME = "t_shop_bank";
    
    public static final String COLUMN_BANK_ID = "bankId";
    
    public static final String COLUMN_BANK_NAME = "bankName";
    
    public static final String COLUMN_CODE = "code";
    
    public static final String COLUMN_BANK_CODE = "bankNumberCode";
    
    public static final String COLUMN_PARENT_BANK_ID = "parentBankId";
    
    @Override
    public void createTable(SQLiteDatabase db)
    {
        final String SQL_CREATE_TABLE =
            "create table " + TABLE_NAME + "(" + COLUMN_BANK_ID + " VARCHAR(50)," + COLUMN_BANK_NAME + " VARCHAR(50),"
                + COLUMN_CODE + " VARCHAR(50)," + COLUMN_BANK_CODE + " VARCHAR(50)," + COLUMN_PARENT_BANK_ID
                + " VARCHAR(50))";
        db.execSQL(SQL_CREATE_TABLE);
    }
    
}
