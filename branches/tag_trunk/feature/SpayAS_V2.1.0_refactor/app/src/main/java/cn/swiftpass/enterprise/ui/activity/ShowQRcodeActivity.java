package cn.swiftpass.enterprise.ui.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.zxing.WriterException;
import com.tencent.stat.StatService;

import net.tsz.afinal.FinalBitmap;

import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.MaxCardManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.PaySuccessInfo;
import cn.swiftpass.enterprise.bussiness.model.QRcodeInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.scan.CaptureActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.ProgressInfoDialog;
import cn.swiftpass.enterprise.ui.widget.SelectPicPopupWindow;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.DisplayUtil;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.Utils;

/**
 * 显示二维码
 * User: he_hui
 * Date: 15-6-25
 * Time: 下午5:43
 */
public class ShowQRcodeActivity extends TemplateActivity {
    private static final String TAG = ShowQRcodeActivity.class.getSimpleName();
    private ImageView img;

    //private TextView tvState;

    private TextView tvMoney, pay_tv_info, pay_wx_str, tv_switch;
    //private TextView pay_title, tv_pay_info, tv_pay_tel;

    private ImageView iv_payment_way_small_icon;

    //private ImageView ivImgstate;

    private Context mContext;

    //private RelativeLayout llState;

    private QRcodeInfo qrcodeInfo;

    Dialog dialog;

    //long startTime;

    //private long money;

    // 是否交易完成
    private boolean isbussFinsh;

    private Timer timer;

    private TimeCount time;

    //private int state; // 状态

    //private boolean isMove = false;

    private FinalBitmap finalBitmap;

    //private PopupWindow pop;

    //private PayMyPopupWindowUtils popUtils;

    //private LinearLayout preview_top_view;

    private DialogInfo dialogInfo;

    private Handler mHandler;

    private boolean isMark = true;

    private Button confirm;

    private LinearLayout money_lay;

    private LinearLayout vcard_lay;

    //double favourable_money = 0;

    //private TextView receivableMoney, favourableMoney, relMoney;
    private TextView id_tv_amount;

    //private DialogInfo dialogs;

    ProgressInfoDialog dialog1;

    private TextView tv_payMoney;

    private boolean isStop = true; // 强行中断交易

    private TextView tv_scan, tv_pase;

    //自定义的弹出框类
    cn.swiftpass.enterprise.ui.widget.SelectPicPopupWindow menuWindow;

    //List<View> viewList = new ArrayList<View>();

    //private AlertDialog dialogInfos;


    LinearLayout mHasFeelayout;
    private TextView mSurcharge;
    private String mark = "";
    double minus = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        timer = new Timer();

        finalBitmap = FinalBitmap.create(ShowQRcodeActivity.this);//
        try {
            finalBitmap.configDiskCachePath(AppHelper.getImgCacheDir());
        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }
        //根据当前的小数点的位数来判断应该除以多少
        for(int i = 0 ; i < MainApplication.numFixed ; i++ ){
            minus = minus * 10;
        }
        initViews();
        setLister();
        MainApplication.listActivities.add(this);
    }

    private void setLister() {
        tv_scan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent it = new Intent();
                it.setClass(ShowQRcodeActivity.this, CaptureActivity.class);
                it.putExtra("money", qrcodeInfo.totalMoney);
                it.putExtra("payType", MainApplication.PAY_WX_MICROPAY);
                startActivity(it);
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CaptureActivity.startActivity(ShowQRcodeActivity.this, qrcodeInfo.payType, CaptureActivity.FLAG, qrcodeInfo.totalMoney);

                MainApplication.listActivities.add(ShowQRcodeActivity.this);
            }

        });
    }

    private void initViews() {
        mContext = this;
        setContentView(R.layout.activity_show_qrcode_new);
        tv_pase = getViewById(R.id.tv_pase);
        tv_pase.setVisibility(View.GONE);
        tv_scan = getViewById(R.id.tv_scan);
        tv_payMoney = getViewById(R.id.tv_payMoney);
        tv_payMoney.setText(MainApplication.getFeeFh());
        iv_payment_way_small_icon = getViewById(R.id.iv_payment_way_small_icon);
        tv_switch = getViewById(R.id.tv_switch);
        //favourableMoney = getViewById(R.id.favourable_money);
        //receivableMoney = getViewById(R.id.receivable_money);
        //relMoney = getViewById(R.id.rel_money);
        id_tv_amount = getViewById(R.id.id_tv_amount);
        money_lay = getViewById(R.id.money_lay);
        vcard_lay = getViewById(R.id.vcard_lay);
        confirm = getViewById(R.id.confirm);
        setButBgAndFont(confirm);
        //pay_title = getViewById(R.id.pay_title);
        pay_tv_info = getViewById(R.id.pay_tv_info);
        //popUtils = new PayMyPopupWindowUtils(this, null);
        mHandler = new Handler();
        pay_wx_str = getViewById(R.id.pay_wx_str);
        //preview_top_view = getViewById(R.id.preview_top_view);
        //ivImgstate = getViewById(R.id.iv_imgstate);
        //llState = getViewById(R.id.ll_state);
        qrcodeInfo = (QRcodeInfo) getIntent().getSerializableExtra("qrcodeInfo");
        mark = getIntent().getStringExtra("mark");
        Logger.i("JAMY", "showQR_mark -->" + mark);

        //tv_pay_info = getViewById(R.id.tv_pay_info);
        //tv_pay_tel = getViewById(R.id.tv_pay_tel);
        tvMoney = getViewById(R.id.tv_money);

        //新增
        mHasFeelayout  = getViewById(R.id.has_fee_layout);
        mSurcharge = getViewById(R.id.surcharge);
        if (qrcodeInfo.isIswxCard()) {
            tv_switch.setVisibility(View.GONE);
        }

        money_lay.setVisibility(View.GONE);
        vcard_lay.setVisibility(View.GONE);
        BigDecimal bigDecimal = new BigDecimal(Long.parseLong(qrcodeInfo.totalMoney) / minus);
        String strSurchargelMoney = qrcodeInfo.getSurcharge();
        BigDecimal bigSurcharge = null;
        if (TextUtils.isEmpty(strSurchargelMoney)) {
            tvMoney.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(Long.parseLong(qrcodeInfo.totalMoney)));
            bigSurcharge = new BigDecimal(0);
            mHasFeelayout.setVisibility(View.GONE);
        } else {
            tvMoney.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(Long.parseLong(qrcodeInfo.totalMoney) + Long.parseLong(strSurchargelMoney)));
            bigSurcharge = new BigDecimal(Long.parseLong(strSurchargelMoney) / minus);
            mHasFeelayout.setVisibility(View.VISIBLE);
        }
        paseRMBWithSurcharge(bigDecimal, bigSurcharge);

        if (qrcodeInfo.getIsMark() == 1) {
            confirm.setVisibility(View.GONE);
        }


        img = getViewById(R.id.img);
        //tvState = getViewById(R.id.tv_state);

        connection = new HttpURLConnection[1];

        createMaxCard(qrcodeInfo.uuId);

        if (MainApplication.isSurchargeOpen()) {
            id_tv_amount.setVisibility(View.VISIBLE);
            mHasFeelayout.setVisibility(View.VISIBLE);
        } else {
            id_tv_amount.setVisibility(View.GONE);
            mHasFeelayout.setVisibility(View.GONE);
        }

        /**
         * 切换扫码
         */
        tv_switch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                menuWindow = new SelectPicPopupWindow(ShowQRcodeActivity.this, qrcodeInfo.payType, new SelectPicPopupWindow.HandleNative() {

                    @Override
                    public void toPay(String type) {
                        String msg = getString(R.string.tv_pay_prompt);
                        toNativePay(msg, qrcodeInfo.totalMoney, type,mark);
                    }

                });
                try {
                    StatService.trackCustomEvent(mContext, "kMTASPayPayQRCode", "切换支付方式");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
                //显示窗口
                menuWindow.showAtLocation(tv_switch, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0); //设置layout在PopupWindow中显示的位置
            }
        });
    }

    private void paseRMBWithSurcharge(BigDecimal bigDecimal, BigDecimal bigSurcharge) {
        try {
            if (!MainApplication.getFeeType().equalsIgnoreCase("CNY")) {
                //新增
                tv_pase.setVisibility(View.GONE);
                BigDecimal paseBigDecimal = null;
                if (bigDecimal != null) {
                    if (MainApplication.getSourceToUsdExchangeRate() > 0 && MainApplication.getUsdToRmbExchangeRate() > 0) {
                        BigDecimal sourceToUsd = new BigDecimal(MainApplication.getSourceToUsdExchangeRate());
                        BigDecimal usdRate = new BigDecimal(MainApplication.getUsdToRmbExchangeRate());
//                        paseBigDecimal = bigSurcharge.add(bigDecimal).multiply(usdRate).multiply(sourceToUsd).setScale(2, BigDecimal.ROUND_FLOOR);
                        paseBigDecimal = bigSurcharge.add(bigDecimal).multiply(sourceToUsd).setScale(MainApplication.numFixed, BigDecimal.ROUND_HALF_UP)
                                .multiply(usdRate).setScale(MainApplication.numFixed, BigDecimal.ROUND_DOWN);


                    } else {
                        BigDecimal rate = new BigDecimal(MainApplication.getExchangeRate());
                        paseBigDecimal = bigSurcharge.add(bigDecimal).multiply(rate).setScale(MainApplication.numFixed, BigDecimal.ROUND_FLOOR);
                    }
//                    String totalStr = "(" + getString(R.string.tv_charge_total) + ":" + DateUtil.formatPaseMoney(bigDecimal) + "," + getString(R.string.tx_surcharge) + ":" + DateUtil.formatPaseMoney(bigSurcharge) + ")";
                    String totalStr = MainApplication.getFeeFh()+ DateUtil.formatPaseMoney(bigDecimal);
                    String surchargeFee = MainApplication.getFeeFh()+ DateUtil.formatPaseMoney(bigSurcharge);
                    id_tv_amount.setText(totalStr);
                    mSurcharge.setText(surchargeFee);
                    tv_pase.setText(getString(R.string.tx_about) + getString(R.string.tx_mark) + DateUtil.formatPaseRMBMoney(paseBigDecimal));
                }
            } else {
                tv_pase.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Logger.e("hehui", "parserToRmb-->" + e);
        }
    }
//
//    private void showConfirm(String msg, final ProgressInfoDialog dialogs) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(ShowQRcodeActivity.this);
//        builder.setTitle(R.string.public_cozy_prompt);
//        builder.setMessage(msg);
//        builder.setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                isStop = false;
//                if (dialogs != null) {
//                    dialogs.dismiss();
//                }
//            }
//        });
//
//        builder.setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//            }
//        });
//        //dialogInfos = builder.show();
//        builder.show();
//    }

    private void toNativePay(final String msg, final String money, final String type,final String note) {

        Logger.i("JAMY", "showQR_mark2 -->" + note);

        OrderManager.getInstance().unifiedNativePay(money, type, 0, null, null, note,new UINotifyListener<QRcodeInfo>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                loadDialog(ShowQRcodeActivity.this, R.string.tv_pay_prompt);
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                dismissLoading();

                if (checkSession()) {
                    return;
                }
                if (!isStop) {
                    return;
                }
                if (object != null) {
                    ShowQRcodeActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toastDialog(ShowQRcodeActivity.this, object.toString(), null);
                        }
                    });
                }
            }

            @Override
            public void onSucceed(QRcodeInfo result) {
                super.onSucceed(result);
                dismissLoading();
                if (result != null && isStop) {
                    qrcodeInfo.orderNo = result.orderNo;
                    qrcodeInfo.payType = type;
                    createMaxCard(result.uuId);
                } else {
                    isStop = true;
                }
            }
        });
    }

    /**
     * 从服务器取图片
     * http://bbs.3gstdy.com
     *
     * @param url
     * @return
     */
    /*private Bitmap getHttpBitmap(String url) {
        URL myFileUrl = null;
        Bitmap bitmap = null;
        try {
            myFileUrl = new URL(url);
        } catch (MalformedURLException e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }
        try {
            HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
            conn.setConnectTimeout(0);
            conn.setDoInput(true);
            conn.connect();
            InputStream is = conn.getInputStream();
            bitmap = BitmapFactory.decodeStream(is);
            is.close();
        } catch (IOException e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }
        return bitmap;
    }*/

    String qrCodeURL = "https://login.weixin.qq.com/p/";

    long getQRTime;

    private void createMaxCard(final String uuId) {
        // 生成二维码
        try {

            //String wxPay = PreferenceUtil.getString(MainApplication.getMchId() + "wxPay", "");
            titleBar.setRightButLayVisible(true, null);
            titleBar.setTitle(getString(R.string.tv_code_wx));
            pay_tv_info.setVisibility(View.VISIBLE);
            pay_wx_str.setText(MainApplication.getPayTypeMap().get(String.valueOf(qrcodeInfo.payType)) + getString(R.string.collect_money) + ",");
            Object object = SharedPreUtile.readProduct("payTypeNameMap" + ApiConstant.bankCode + MainApplication.getMchId());
            if (object != null) {
                Map<String, DynModel> payTypeNameMap = (Map<String, DynModel>) object;

                DynModel dynModel = payTypeNameMap.get(qrcodeInfo.payType);

                if (null != dynModel && !StringUtil.isEmptyOrNull(dynModel.getProviderName())) {
                    try {
                        String language = PreferenceUtil.getString("language", "");
                        if (StringUtil.isEmptyOrNull(language)) {
                            language = Locale.getDefault().toString();
                        }
                        if (language.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN) || language.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_HANS)) {
                            titleBar.setTitle(MainApplication.getPayTypeMap().get(qrcodeInfo.payType) +  getString(R.string.title_pay));
                            pay_wx_str.setText(MainApplication.getPayTypeMap().get(qrcodeInfo.payType) + getString(R.string.title_pay) + ",");

                        } else if (language.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK) || language.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO) ||
                                language.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW) || language.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_HANT)) {
                            titleBar.setTitle(MainApplication.getPayTypeMap().get(qrcodeInfo.payType) + getString(R.string.title_pay));
                            pay_wx_str.setText(MainApplication.getPayTypeMap().get(qrcodeInfo.payType) +  getString(R.string.title_pay) + ",");
                        } else {
                            titleBar.setTitle(MainApplication.getPayTypeMap().get(qrcodeInfo.payType) + " " + getString(R.string.title_pay));
                            pay_wx_str.setText(MainApplication.getPayTypeMap().get(qrcodeInfo.payType) + " " + getString(R.string.title_pay) + ",");
                        }



                    } catch (Exception e) {
                        Log.e(TAG,Log.getStackTraceString(e));
                    }
                }
            }

            Object object_icon = SharedPreUtile.readProduct("payTypeIcon" + ApiConstant.bankCode + MainApplication.getMchId());
            if (object_icon != null) {
                try {
                    Map<String, String> typePicMap = (Map<String, String>) object_icon;
                    if (typePicMap != null && typePicMap.size() > 0) {
                        String picUrl = typePicMap.get(qrcodeInfo.payType);
                        if (!StringUtil.isEmptyOrNull(picUrl)) {
                            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.icon_general_wechat);
                            if (bitmap != null) {
                                MainApplication.finalBitmap.display(iv_payment_way_small_icon, picUrl);
                            } else {
                                MainApplication.finalBitmap.display(iv_payment_way_small_icon, picUrl);
                            }
                        } else {
                            iv_payment_way_small_icon.setImageResource(R.drawable.icon_general_receivables);
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
            }

            ShowQRcodeActivity.this.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    int w = DisplayUtil.dip2Px(ShowQRcodeActivity.this, 280);
                    int h = DisplayUtil.dip2Px(ShowQRcodeActivity.this, 280);
                    float scale = ShowQRcodeActivity.this.getResources().getDisplayMetrics().density;
                    if (scale == 1.0f) {
                        w = DisplayUtil.dip2Px(ShowQRcodeActivity.this, 380);
                        h = DisplayUtil.dip2Px(ShowQRcodeActivity.this, 380);
                    }
                    // TODO Auto-generated method stub
                    Bitmap bitmap;
                    try {
                        bitmap = MaxCardManager.getInstance().create2DCode(uuId, w, h);
                        img.setImageBitmap(bitmap);
                    } catch (WriterException e) {
                        Log.e(TAG,Log.getStackTraceString(e));
                    }

                }
            });

            Countdown();

            //            queryOrderGetStuts(qrcodeInfo.orderNo);
        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }
    }

    /**
     * 支付一分钟倒计时
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void Countdown() {
        // 总共2分钟 间隔2秒
        time = new TimeCount(1000 * 60 * 2, 2000);
        time.start();
    }

    private long timeCount = 5;

    private Runnable myRunnable = new Runnable() {

        @Override
        public void run() {
            if (timeCount > 0 && dialogInfo != null) {
                dialogInfo.setBtnOkText(getString(R.string.btnOk) + "(" + getString(R.string.show_close) + timeCount + getString(R.string.tv_second) + ")");
                timeCount -= 1;
                mHandler.postDelayed(this, 1000);
            } else {
                mHandler.removeCallbacks(this);
                if (dialogInfo != null && dialogInfo.isShowing()) {

                    dialogInfo.dismiss();
                }
                dialogInfo.setBtnOkText(getString(R.string.dialog_close));
                //Toast.makeText(QrcodeActivity.this, "关闭", 0).show();
                ShowQRcodeActivity.this.finish();
            }

        }
    };

    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }

        @Override
        public void onFinish() {//计时完毕时触发

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String mmsg = getString(R.string.show_request_outtime);
                    //                    dialogInfo =
                    //                        new DialogInfoSdk(ShowQRcodeActivity.this,
                    //                            getResources().getString(Resourcemap.getById_title_prompt()), "" + mmsg, "确定",
                    //                            DialogInfoSdk.SUBMIT_FINISH, null);
                    try {
                        dialogInfo = new DialogInfo(ShowQRcodeActivity.this, getStringById(R.string.public_cozy_prompt), mmsg, getStringById(R.string.btnOk), DialogInfo.REGISTFLAG, null, null);

                        DialogHelper.resize(ShowQRcodeActivity.this, dialogInfo);
                        dialogInfo.setOnKeyListener(new OnKeyListener() {

                            @Override
                            public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                                if (keycode == KeyEvent.KEYCODE_BACK) {
                                    return true;
                                }
                                return false;
                            }
                        });
                        if (dialogInfo != null) {

                            dialogInfo.show();
                        }
                        mHandler.post(myRunnable);
                    } catch (Exception e) {
                        Log.e(TAG,Log.getStackTraceString(e));
                    }
                }
            });
            //            time.cancel();
        }

        @Override
        public void onTick(long millisUntilFinished) {//计时过程显示
            //            if (!isMove)
            //            {
            if (null != qrcodeInfo.orderNo && !"".equals(qrcodeInfo.orderNo)) {
                queryOrderGetStuts(qrcodeInfo.orderNo);
            }
            //            }
        }
    }

    boolean flag = true;

    /**
     * 查询订单获取状态
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void queryOrderGetStuts(final String orderNo) {
        OrderManager.getInstance().queryOrderByOrderNo(qrcodeInfo.orderNo, MainApplication.PAY_ZFB_QUERY, new UINotifyListener<Order>() {
            @Override
            public void onError(Object object) {
                super.onError(object);
            }

            @Override
            public void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(Order result) {
                super.onSucceed(result);
                if (result != null) {
                    //                        result.notifyTime = result.notify_time + "";
                    if (result.state.equals("2") && isMark) {
                        if (timer != null) {
                            timer.cancel();
                            timer = null;
                        }
                        qrcodeInfo.orderNo = null;
                        isMark = false;
                        NoteMarkActivity.setNoteMark("");
                        PayResultActivity.startActivity(ShowQRcodeActivity.this, result);
                        finish();

                    }
                    //                    else if (result.state == 8) // 扫描成功
                    //                    {
                    //                        tvState.setText(QRCodeState.SCAN_SUCCESS.getDisplayName());
                    //                        ivImgstate.setImageResource(R.drawable.n_right_select);
                    //                        llState.setBackgroundResource(R.drawable.n_code_green_bg);
                    //                        //                        queryOrderGetStuts(qrcodeInfo.orderNo);
                    //                    }
                    //                    else
                    //                    {
                    //                        queryOrderGetStuts(qrcodeInfo.orderNo);
                    //                    }
                }
                //                else
                //                {
                //                    queryOrderGetStuts(qrcodeInfo.orderNo);
                //                }
            }
        });
    }

    /**
     * 获取订单状态
     */
    public TimerTask taskGetOrderStuts = new TimerTask() {
        @Override
        public void run() {
            //            ShowQRcodeActivity.this.runOnUiThread(new Runnable()
            //            {
            //                @Override
            //                public void run()
            //                {
            queryOrderGetStuts(qrcodeInfo.orderNo);

            //System.gc();

            //                }

            //            });
        }

    };

    private HttpURLConnection[] connection;

    /*private void checkQRCode(String uuId) {
        checkQRCode(uuId, QRCodeState.NO_PAY.getValue(), connection);
    }
*/
    /*private void checkQRCode(String uuId, int code, HttpURLConnection[] c) {
        final long t = System.currentTimeMillis();
        OrderManager.getInstance().checkQRCodeState(uuId, code, new UINotifyListener<QRCodeState>() {
            @Override
            public void onSucceed(QRCodeState result) {
                super.onSucceed(result);
                long ct = System.currentTimeMillis() - t;
                if (result == QRCodeState.PAY_SUCCESS) // 交易成功
                {
                    //订单明细  如果有优惠券则打开优惠券页面  查询交易用户姓名和银行信息

                    // 交易完成状态
                    isbussFinsh = true;

                    //                    searchPayInfo();
                    //                    Order order = new 

                    Order order = qrcodeInfo.order;
                    order.money = Utils.Integer.tryParse(qrcodeInfo.totalMoney, 0);
                    //                    order.notifyTime = new Date();
                    //                    if (result != null)
                    //                    {
                    //                        order.wxUserName = result.username;
                    //                        order.bankCardName = result.bank_card;
                    //                    }
                    PayResultActivity.startActivity(ShowQRcodeActivity.this, order);
                    finish();
                    return;
                } else if (result == QRCodeState.SCAN_SUCCESS) { //扫描成功
                    tvState.setText(QRCodeState.SCAN_SUCCESS.getDisplayName());
                    ivImgstate.setImageResource(R.drawable.n_right_select);
                    llState.setBackgroundResource(R.drawable.n_code_green_bg);
                    //PayResultActivity.startActivity(mContext,order);
                } else if (result == QRCodeState.NO_PAY) { //未交易
                    tvState.setText(QRCodeState.NO_PAY.getDisplayName());
                    ivImgstate.setImageResource(R.drawable.n_scanning);
                    llState.setBackgroundResource(R.drawable.n_code_blue_bg);
                } else if (result == QRCodeState.FAIL_UUID) { //uuid 过期
                    ivImgstate.setImageResource(R.drawable.n_scanning);
                    llState.setBackgroundResource(R.drawable.n_code_blue_bg);
                    repollQRCodeInfo();//如果失败了重新生成二维码
                    return;
                } else if (result == QRCodeState.SCAN_SUCCESS_CANCEL) //用户取消扫描
                {
                    tvState.setText(QRCodeState.NO_PAY.getDisplayName());
                    ivImgstate.setImageResource(R.drawable.n_scanning);
                    llState.setBackgroundResource(R.drawable.n_code_blue_bg);
                } else {
                    ivImgstate.setImageResource(R.drawable.n_scanning);
                    tvState.setText(QRCodeState.DEFAULT.getDisplayName());
                }

                if (OrderManager.getInstance().qRcodeInfo != null && result.getValue() != QRCodeState.FAIL_UUID.getValue() && result.getValue() != QRCodeState.PAY_FAIL.getValue()) {
                    checkQRCode(OrderManager.getInstance().qRcodeInfo.service_uuid, result.getValue(), connection);
                }

            }
        }, c);
    }*/

//    private void searchPayInfo() {
//        OrderManager.getInstance().searchCFTOrderPayInfo(new UINotifyListener<PaySuccessInfo>() {
//            @Override
//            public void onError(Object object) {
//                super.onError(object);
//            }
//
//            @Override
//            public void onPreExecute() {
//                super.onPreExecute();
//            }
//
//            @Override
//            public void onPostExecute() {
//                super.onPostExecute();
//            }
//
//            @Override
//            public void onSucceed(PaySuccessInfo result) {
//                super.onSucceed(result);
//                Order order = qrcodeInfo.order;
//                order.money = Utils.Integer.tryParse(qrcodeInfo.totalMoney, 0);
//                //                order.notifyTime = new Date();
//                if (result != null) {
//                    order.wxUserName = result.username;
//                    order.bankCardName = result.bank_card;
//                }
//                PayResultActivity.startActivity(mContext, order);
//                finish();
//            }
//        });
//    }
//
//    //重新获取二维码
//    private void repollQRCodeInfo() {
//        OrderManager.getInstance().getQRCodeInfo(new UINotifyListener<String>() {
//            @Override
//            public void onPreExecute() {
//                super.onPreExecute();
//            }
//
//            @Override
//            public void onPostExecute() {
//                super.onPostExecute();
//            }
//
//            @Override
//            public void onError(Object object) {
//                super.onError(object);
//            }
//
//            @Override
//            public void onSucceed(String uuId) {
//                super.onSucceed(uuId);
//                createMaxCard(uuId);
//            }
//        });
//
//    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(R.string.title_wx_pay);
        titleBar.setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
        titleBar.setRightButLayVisible(false, null);
        titleBar.setLeftButtonVisible(true);
        //        titleBar.setRightButLayBackground(R.drawable.icon_general_help);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                // 交易没完成，按“返回”退出提示
                if (!isbussFinsh) {
                    //                    dialogs =
                    //                        new DialogInfo(ShowQRcodeActivity.this, getString(R.string.public_cozy_prompt),
                    //                            getResources().getString(R.string.dialog_pay_unFinished), getString(R.string.btnOk),
                    //                            getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn()
                    //                            {
                    //                                
                    //                                @Override
                    //                                public void handleOkBtn()
                    //                                {
                    //                                    finish();
                    //                                }
                    //                                
                    //                                @Override
                    //                                public void handleCancleBtn()
                    //                                {
                    //                                    dialogs.cancel();
                    //                                }
                    //                            }, null);
                    //                    
                    //                    DialogHelper.resize(ShowQRcodeActivity.this, dialogs);
                    //                    dialogs.show();
                    finish();
                } else {
                    finish();
                }
            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onRightLayClick() {
                // TODO Auto-generated method stub

            }

            @Override
            public void onRightButLayClick() {
                //                showPage(ShowPicActivity.class);
                //                ShowPicActivity.startActivity(ShowQRcodeActivity.this, qrcodeInfo.payType);
                //                shouGuideDialog();
                //                WindowManager windowManager = getWindowManager();
                //                Display display = windowManager.getDefaultDisplay();
                //                WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
                //                lp.width = (int)(display.getWidth()); //设置宽度
                //                dialog.getWindow().setAttributes(lp);
                //                pop = popUtils.getPayHelpDialog(qrcodeInfo.payType, preview_top_view.getWidth());
                //                pop.showAsDropDown(preview_top_view);

            }
        });
    }

    void shouGuideDialog() {
        //        GuideDialog dialog = new GuideDialog(ShowQRcodeActivity.this, qrcodeInfo.payType);
        //        LayoutParams lay = dialog.getWindow().getAttributes();
        //        setParams(lay);
        //        dialog.show();
    }

//    private void setParams(LayoutParams lay) {
//        DisplayMetrics dm = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(dm);
//        Rect rect = new Rect();
//        View view = getWindow().getDecorView();
//        view.getWindowVisibleDisplayFrame(rect);
//        lay.height = dm.heightPixels - rect.top;
//        lay.width = dm.widthPixels;
//    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        OrderManager.getInstance().destory();
        if (null != connection && connection.length > 0) {
            try {
                if (null != connection[0])
                {
                    connection[0].disconnect();
                }
            } catch (Exception e) {
                Logger.i(e.getMessage());
            }
        }

        if (timer != null) {
            timer.cancel();
            taskGetOrderStuts.cancel();
            taskGetOrderStuts = null;
            timer = null;
        }
        if (time != null) {
            //isMove = true;
            time.cancel();
        }
    }



    public static void startActivity(QRcodeInfo qrcodeInfo, Context context) {
        Intent it = new Intent();
        it.putExtra("qrcodeInfo", qrcodeInfo);
        it.setClass(context, ShowQRcodeActivity.class);
        context.startActivity(it);
    }

    public static void startActivity(QRcodeInfo qrcodeInfo, Context context,String mark) {
        Intent it = new Intent();
        it.putExtra("qrcodeInfo", qrcodeInfo);
        it.putExtra("mark", mark);
        it.setClass(context, ShowQRcodeActivity.class);
        context.startActivity(it);
    }

    @Override
    public boolean onKeyDown(int keycode, KeyEvent event) {
        if (keycode == KeyEvent.KEYCODE_BACK) {
            if (!isbussFinsh) {
                //                dialogs =
                //                    new DialogInfo(ShowQRcodeActivity.this, getString(R.string.public_cozy_prompt),
                //                        getResources().getString(R.string.dialog_pay_unFinished), getString(R.string.btnOk),
                //                        getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn()
                //                        {
                //                            
                //                            @Override
                //                            public void handleOkBtn()
                //                            {
                //                                finish();
                //                            }
                //                            
                //                            @Override
                //                            public void handleCancleBtn()
                //                            {
                //                                dialogs.cancel();
                //                            }
                //                        }, null);
                //                
                //                DialogHelper.resize(ShowQRcodeActivity.this, dialogs);
                //                dialogs.show();
                finish();
            }
            return true;
        }
        return super.onKeyDown(keycode, event);
    }

}
