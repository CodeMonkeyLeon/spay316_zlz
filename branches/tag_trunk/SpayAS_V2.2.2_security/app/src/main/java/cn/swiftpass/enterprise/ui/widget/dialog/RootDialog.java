package cn.swiftpass.enterprise.ui.widget.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import cn.swiftpass.enterprise.intl.R;

/**
 * Created by aijingya on 2018/10/27.
 *
 * @Package cn.swiftpass.enterprise.ui.widget.dialog
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/10/27.14:57.
 */

public class RootDialog extends Dialog{
    private static final String TAG = RootDialog.class.getSimpleName();

    private TextView tv_message;
//    private TextView bt_confirm;
    private LinearLayout ll_confirm;
    private Context mContext;
    private View.OnClickListener confirmButtonListener;

    public RootDialog(Activity context , View.OnClickListener ButtonListener) {
        super(context, R.style.simpleDialog);
        confirmButtonListener = ButtonListener;
        mContext = context;
        init(context);
        setCancelable(true);

    }

    private void init(Context context) {
        View rootView = View.inflate(context, R.layout.dialog_root_layout, null);
        setContentView(rootView);

        tv_message = (TextView)rootView.findViewById(R.id.tv_message);
//        bt_confirm = (TextView) rootView.findViewById(R.id.bt_confirm);
        ll_confirm = rootView.findViewById(R.id.ll_confirm);
        ll_confirm.setOnClickListener(confirmButtonListener);
    }

    public void  setMessage(String message){
        if(!TextUtils.isEmpty(message)){
            tv_message.setText(message);
        }else{
            Toast.makeText(mContext,"with no message",Toast.LENGTH_SHORT).show();
        }
    }
}
