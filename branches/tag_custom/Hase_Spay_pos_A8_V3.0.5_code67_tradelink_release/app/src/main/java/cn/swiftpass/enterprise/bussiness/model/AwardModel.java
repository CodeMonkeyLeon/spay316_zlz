package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

public class AwardModel implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    private String userId;
    
    private String userName;
    
    private long activeOrder; //排名
    
    private String activeOrderName;//排名名称
    
    private long money; //收款金额
    
    private String activeId;//活动id
    
    private long activeOrderId;//当前排名
    
    private long balance; //相差额
    
    private long closingDate; //倒计时
    
    private String activeDetailUrl; //活动详情
    
    private String activeImageUrl_m;
    
    private String percent;//比例
    
    private String activeTitle; //活动标题
    
    private String activeName;//活动名称
    
    private boolean hadWin;
    
    private Integer activeState; // 活动状态
    
    private String startDate;
    
    private String endDate;
    
    private Integer noStartNum; // 未开始活动数量
    
    private Integer activeStatus; // 活动状态
    
    private long fullMoney;
    
    private long orderFullMoney;
    
    private long minMoney;
    
    private long maxMoney;
    
    private long maxNum;
    
    private long activeType = 1;
    
    private long smljTotalNum;
    
    private long smljTotalMoney;
    
    private String addDate;
    
    private boolean isAdd;
    
    private Integer totalPageCount;
    
    /**
     * @return 返回 totalPageCount
     */
    public Integer getTotalPageCount()
    {
        return totalPageCount;
    }
    
    /**
     * @param 对totalPageCount进行赋值
     */
    public void setTotalPageCount(Integer totalPageCount)
    {
        this.totalPageCount = totalPageCount;
    }
    
    /**
     * @return 返回 isAdd
     */
    public boolean isAdd()
    {
        return isAdd;
    }
    
    /**
     * @param 对isAdd进行赋值
     */
    public void setAdd(boolean isAdd)
    {
        this.isAdd = isAdd;
    }
    
    /**
     * @return 返回 addDate
     */
    public String getAddDate()
    {
        return addDate;
    }
    
    /**
     * @param 对addDate进行赋值
     */
    public void setAddDate(String addDate)
    {
        this.addDate = addDate;
    }
    
    /**
     * @return 返回 smljTotalMoney
     */
    public long getSmljTotalMoney()
    {
        return smljTotalMoney;
    }
    
    /**
     * @param 对smljTotalMoney进行赋值
     */
    public void setSmljTotalMoney(long smljTotalMoney)
    {
        this.smljTotalMoney = smljTotalMoney;
    }
    
    /**
     * @return 返回 smljTotalNum
     */
    public long getSmljTotalNum()
    {
        return smljTotalNum;
    }
    
    /**
     * @param 对smljTotalNum进行赋值
     */
    public void setSmljTotalNum(long smljTotalNum)
    {
        this.smljTotalNum = smljTotalNum;
    }
    
    /**
     * @return 返回 maxNum
     */
    public long getMaxNum()
    {
        return maxNum;
    }
    
    /**
     * @param 对maxNum进行赋值
     */
    public void setMaxNum(long maxNum)
    {
        this.maxNum = maxNum;
    }
    
    /**
     * @return 返回 activeType
     */
    public long getActiveType()
    {
        return activeType;
    }
    
    /**
     * @param 对activeType进行赋值
     */
    public void setActiveType(long activeType)
    {
        this.activeType = activeType;
    }
    
    /**
     * @return 返回 maxMoney
     */
    public long getMaxMoney()
    {
        return maxMoney;
    }
    
    /**
     * @param 对maxMoney进行赋值
     */
    public void setMaxMoney(long maxMoney)
    {
        this.maxMoney = maxMoney;
    }
    
    /**
     * @return 返回 minMoney
     */
    public long getMinMoney()
    {
        return minMoney;
    }
    
    /**
     * @param 对minMoney进行赋值
     */
    public void setMinMoney(long minMoney)
    {
        this.minMoney = minMoney;
    }
    
    /**
     * @return 返回 orderFullMoney
     */
    public long getOrderFullMoney()
    {
        return orderFullMoney;
    }
    
    /**
     * @param 对orderFullMoney进行赋值
     */
    public void setOrderFullMoney(long orderFullMoney)
    {
        this.orderFullMoney = orderFullMoney;
    }
    
    /**
     * @return 返回 fullMoney
     */
    public long getFullMoney()
    {
        return fullMoney;
    }
    
    /**
     * @param 对fullMoney进行赋值
     */
    public void setFullMoney(long fullMoney)
    {
        this.fullMoney = fullMoney;
    }
    
    /**
     * @return 返回 activeStatus
     */
    public Integer getActiveStatus()
    {
        return activeStatus;
    }
    
    /**
     * @param 对activeStatus进行赋值
     */
    public void setActiveStatus(Integer activeStatus)
    {
        this.activeStatus = activeStatus;
    }
    
    /**
     * @return 返回 noStartNum
     */
    public Integer getNoStartNum()
    {
        return noStartNum;
    }
    
    /**
     * @param 对noStartNum进行赋值
     */
    public void setNoStartNum(Integer noStartNum)
    {
        this.noStartNum = noStartNum;
    }
    
    /**
     * @return 返回 startDate
     */
    public String getStartDate()
    {
        return startDate;
    }
    
    /**
     * @param 对startDate进行赋值
     */
    public void setStartDate(String startDate)
    {
        this.startDate = startDate;
    }
    
    /**
     * @return 返回 endDate
     */
    public String getEndDate()
    {
        return endDate;
    }
    
    /**
     * @param 对endDate进行赋值
     */
    public void setEndDate(String endDate)
    {
        this.endDate = endDate;
    }
    
    /**
     * @return 返回 activeState
     */
    public Integer getActiveState()
    {
        return activeState;
    }
    
    /**
     * @param 对activeState进行赋值
     */
    public void setActiveState(Integer activeState)
    {
        this.activeState = activeState;
    }
    
    /**
     * @return 返回 hadWin
     */
    public boolean isHadWin()
    {
        return hadWin;
    }
    
    /**
     * @param 对hadWin进行赋值
     */
    public void setHadWin(boolean hadWin)
    {
        this.hadWin = hadWin;
    }
    
    /**
     * @return 返回 activeName
     */
    public String getActiveName()
    {
        return activeName;
    }
    
    /**
     * @param 对activeName进行赋值
     */
    public void setActiveName(String activeName)
    {
        this.activeName = activeName;
    }
    
    /**
     * @return 返回 activeTitle
     */
    public String getActiveTitle()
    {
        return activeTitle;
    }
    
    /**
     * @param 对activeTitle进行赋值
     */
    public void setActiveTitle(String activeTitle)
    {
        this.activeTitle = activeTitle;
    }
    
    /**
     * @return 返回 percent
     */
    public String getPercent()
    {
        return percent;
    }
    
    /**
     * @param 对percent进行赋值
     */
    public void setPercent(String percent)
    {
        this.percent = percent;
    }
    
    /**
     * @return 返回 activeId
     */
    public String getActiveId()
    {
        return activeId;
    }
    
    /**
     * @param 对activeId进行赋值
     */
    public void setActiveId(String activeId)
    {
        this.activeId = activeId;
    }
    
    /**
     * @return 返回 activeOrderId
     */
    public long getActiveOrderId()
    {
        return activeOrderId;
    }
    
    /**
     * @param 对activeOrderId进行赋值
     */
    public void setActiveOrderId(long activeOrderId)
    {
        this.activeOrderId = activeOrderId;
    }
    
    /**
     * @return 返回 balance
     */
    public long getBalance()
    {
        return balance;
    }
    
    /**
     * @param 对balance进行赋值
     */
    public void setBalance(long balance)
    {
        this.balance = balance;
    }
    
    /**
     * @return 返回 closingDate
     */
    public long getClosingDate()
    {
        return closingDate;
    }
    
    /**
     * @param 对closingDate进行赋值
     */
    public void setClosingDate(long closingDate)
    {
        this.closingDate = closingDate;
    }
    
    /**
     * @return 返回 activeDetailUrl
     */
    public String getActiveDetailUrl()
    {
        return activeDetailUrl;
    }
    
    /**
     * @param 对activeDetailUrl进行赋值
     */
    public void setActiveDetailUrl(String activeDetailUrl)
    {
        this.activeDetailUrl = activeDetailUrl;
    }
    
    /**
     * @return 返回 activeImageUrl_m
     */
    public String getActiveImageUrl_m()
    {
        return activeImageUrl_m;
    }
    
    /**
     * @param 对activeImageUrl_m进行赋值
     */
    public void setActiveImageUrl_m(String activeImageUrl_m)
    {
        this.activeImageUrl_m = activeImageUrl_m;
    }
    
    /**
     * @return 返回 userId
     */
    public String getUserId()
    {
        return userId;
    }
    
    /**
     * @param 对userId进行赋值
     */
    public void setUserId(String userId)
    {
        this.userId = userId;
    }
    
    /**
     * @return 返回 userName
     */
    public String getUserName()
    {
        return userName;
    }
    
    /**
     * @param 对userName进行赋值
     */
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    /**
     * @return 返回 activeOrder
     */
    public long getActiveOrder()
    {
        return activeOrder;
    }
    
    /**
     * @param 对activeOrder进行赋值
     */
    public void setActiveOrder(long activeOrder)
    {
        this.activeOrder = activeOrder;
    }
    
    /**
     * @return 返回 activeOrderName
     */
    public String getActiveOrderName()
    {
        return activeOrderName;
    }
    
    /**
     * @param 对activeOrderName进行赋值
     */
    public void setActiveOrderName(String activeOrderName)
    {
        this.activeOrderName = activeOrderName;
    }
    
    /**
     * @return 返回 money
     */
    public long getMoney()
    {
        return money;
    }
    
    /**
     * @param 对money进行赋值
     */
    public void setMoney(long money)
    {
        this.money = money;
    }
    
}
