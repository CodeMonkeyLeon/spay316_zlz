package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.QRcodeInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.ImageUtil;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * Created by aijingya on 2020/8/27.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description:
 * @date 2020/8/27.21:20.
 */
public class InstapayInfoActivity extends TemplateActivity{
    private TextView tv_merchant_name;
    private TextView tv_beneficiary_content,tv_amount_content,tv_account_no_content,tv_note_title,tv_note_content,tv_pay_before_time;
    private View view_line_note;
    private Button bt_save_image;

    private QRcodeInfo qrcodeInfo;

    private String PicSign = null;   //图片名称的唯一标识，根据图片的内容生成
    private String viewPicSavePath; //保存的图片的完整路径名称

    private TimeCount time;
    private DialogInfo dialogInfo;

    private boolean isMark = true;


    public static void startActivity(Context context,QRcodeInfo qrcodeInfo) {
        Intent it = new Intent();
        it.putExtra("qrcodeInfo", qrcodeInfo);
        it.setClass(context, InstapayInfoActivity.class);
        context.startActivity(it);
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth","sale");
        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(saleOrPreauth,"pre_auth")){
            titleBar.setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
        }else{
            titleBar.setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
        }
        titleBar.setRightButLayVisible(false, null);
        titleBar.setLeftButtonVisible(true);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
              finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {
            }

            @Override
            public void onRightButLayClick() {
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        qrcodeInfo = (QRcodeInfo) getIntent().getSerializableExtra("qrcodeInfo");

        initViews();
    }

    private void initViews(){
        setContentView(R.layout.activity_instapay_info);

        tv_merchant_name = getViewById(R.id.tv_merchant_name);
        tv_beneficiary_content = getViewById(R.id.tv_beneficiary_content);
        tv_amount_content = getViewById(R.id.tv_amount_content);
        tv_account_no_content = getViewById(R.id.tv_account_no_content);
        tv_note_title = getViewById(R.id.tv_note_title);
        tv_note_content = getViewById(R.id.tv_note_content);
        view_line_note = getViewById(R.id.view_line_note);
        tv_pay_before_time = getViewById(R.id.tv_pay_before_time);
        bt_save_image = getViewById(R.id.bt_save_image);
        bt_save_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //保存当前图片
                saveToSdcard();
            }
        });

        initUIDate();

        //开始扫码查单
        Countdown();

    }

    public void initUIDate(){
        if(qrcodeInfo != null){

            if(!TextUtils.isEmpty(qrcodeInfo.payType)){
                titleBar.setTitle(MainApplication.getPayTypeMap().get(qrcodeInfo.payType) );
            }

            if(!TextUtils.isEmpty(MainApplication.getMchName())){
                tv_merchant_name.setText(MainApplication.getMchName());
            }

            //如果有附加费，则展示总额（附加费+金额）
            if(MainApplication.isSurchargeOpen()){
                if(!TextUtils.isEmpty(qrcodeInfo.getSurcharge())){
                    if(!TextUtils.isEmpty(qrcodeInfo.totalMoney)){
                        tv_amount_content.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(Long.parseLong(qrcodeInfo.totalMoney)+ Long.parseLong(qrcodeInfo.getSurcharge())));
                    }

                }else{
                    if(!TextUtils.isEmpty(qrcodeInfo.totalMoney)){
                        tv_amount_content.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(Long.parseLong(qrcodeInfo.totalMoney)));
                    }
                }
            }else{
                if(!TextUtils.isEmpty(qrcodeInfo.totalMoney)){
                    tv_amount_content.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(Long.parseLong(qrcodeInfo.totalMoney)));
                }
            }


            if(!TextUtils.isEmpty(qrcodeInfo.getInvoiceId())){
                tv_account_no_content.setText(qrcodeInfo.getInvoiceId());
            }

            String note_mark = NoteMarkActivity.getNoteMark();
            if(!TextUtils.isEmpty(note_mark)){
                tv_note_title.setVisibility(View.VISIBLE);
                tv_note_content.setVisibility(View.VISIBLE);
                view_line_note.setVisibility(View.VISIBLE);

                tv_note_content.setText(note_mark);
            }else{
                tv_note_title.setVisibility(View.GONE);
                tv_note_content.setVisibility(View.GONE);
                view_line_note.setVisibility(View.GONE);
            }

            tv_pay_before_time.setText(getStringById(R.string.instapay_pay_before) + qrcodeInfo.getExpirationDate().trim());

            //根据当前的图片内容，生成图片的名称，这样就可以避免同样的内容不保存两张图
            Map<String, String> params = new HashMap<String, String>();
            params.put("mch_name", MainApplication.getMchName());
            params.put("totalMoney", qrcodeInfo.totalMoney);
            params.put("invoiceId", qrcodeInfo.getInvoiceId());
            params.put("noteMark", note_mark);
            params.put("endTime", qrcodeInfo.getExpirationDate().trim());
            PicSign = SignUtil.getInstance().createSign(params, MainApplication.getSignKey());
        }
    }


    /**
     *将图片保存到本地
     */
    public void saveToSdcard() {
        if (TextUtils.isEmpty(PicSign)) {
            return;
        }
        if (!TextUtils.isEmpty(viewPicSavePath)) {
            if (new File(viewPicSavePath).exists() && viewPicSavePath.contains(PicSign)) {
                showToastInfo(ToastHelper.toStr(R.string.instapay_save_success));
                return;
            }
        }

        ScrollView scrollView_instapay = getViewById(R.id.sv_aub_instapay_static_info);
        scrollView_instapay.scrollTo(0, 0);
        showLoading(false, ToastHelper.toStr(R.string.loading));

        final LinearLayout mSaveLayout = getViewById(R.id.ll_static_code_info);

        Executors.newSingleThreadExecutor().submit(new Runnable() {
            @Override
            public void run() {
                viewPicSavePath = ImageUtil.saveViewBitmapFile(PicSign, mSaveLayout);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dismissMyLoading();
                    }
                });
                if (!TextUtils.isEmpty(viewPicSavePath)) {
                    showToastInfo(ToastHelper.toStr(R.string.instapay_save_success) + ":" + viewPicSavePath);
                } else {
                    showToastInfo(ToastHelper.toStr(R.string.instapay_save_failed));
                }
            }
        });

    }

    //轮询查单的逻辑
    private void Countdown() {
        // 总共2分钟 间隔2秒
        time = new TimeCount(1000 * 60 * 2, 2000);
        time.start();
    }


    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }

        @Override
        public void onFinish() {//计时完毕时触发
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String mmsg = getString(R.string.instapay_cancel_query_to_bill);
                    try {
                        dialogInfo = new DialogInfo(InstapayInfoActivity.this, getStringById(R.string.public_cozy_prompt), mmsg, getStringById(R.string.btnOk), DialogInfo.REGISTFLAG, new DialogInfo.HandleBtn() {
                            @Override
                            public void handleOkBtn() {
                                if (dialogInfo != null && dialogInfo.isShowing()) {
                                    dialogInfo.dismiss();
                                }

                                if (time != null) {
                                    time.cancel();
                                }

                                for (Activity a : MainApplication.allActivities)
                                {
                                    a.finish();
                                }

                                //跳转到账单列表
                                Intent it = new Intent();
                                it.putExtra("tab_index","bill");
                                it.setClass(InstapayInfoActivity.this, spayMainTabActivity.class);
                                startActivity(it);

                            }

                            @Override
                            public void handleCancleBtn() {

                            }
                        }, null);

                        DialogHelper.resize(InstapayInfoActivity.this, dialogInfo);
                        dialogInfo.setOnKeyListener(new DialogInterface.OnKeyListener() {

                            @Override
                            public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {
                                if (keycode == KeyEvent.KEYCODE_BACK) {
                                    return true;
                                }
                                return false;
                            }
                        });
                        if (dialogInfo != null) {
                            dialogInfo.show();
                        }
                    } catch (Exception e) {

                    }
                }
            });
        }

        @Override
        public void onTick(long millisUntilFinished) {//计时过程每隔 间隔时间触发一次
            //每次2S 查询一次订单状态
            queryInvoidIDOrderStatus(qrcodeInfo.getInvoiceId());
        }
    }

    public void queryInvoidIDOrderStatus(String invoiceId){
        OrderManager.getInstance().queryOrderByInvoiceId(invoiceId, new UINotifyListener<Order>() {
            @Override
            public void onError(Object object) {
                super.onError(object);
            }

            @Override
            public void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(Order result) {
                super.onSucceed(result);

                if (result != null) {
                    if (result.state.equals("2") && isMark) {
                        if(time != null){
                            time.cancel();
                        }

                        isMark = false;

                        qrcodeInfo.invoiceId = null;

                        NoteMarkActivity.setNoteMark("");

                        PayResultActivity.startActivity(InstapayInfoActivity.this, result);

                        finish();

                    }

                }

            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        OrderManager.getInstance().destory();

        if (time != null) {
            time.cancel();
        }

        if(dialogInfo != null) {
            dialogInfo.dismiss();
        }
    }
}
