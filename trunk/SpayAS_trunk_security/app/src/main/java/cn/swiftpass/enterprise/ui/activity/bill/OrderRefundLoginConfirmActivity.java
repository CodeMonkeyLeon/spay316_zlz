/*
 * 文 件 名:  ReportActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-8
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.bill;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.common.sentry.SentryUtils;
import com.example.common.sp.PreferenceUtil;
import com.ziyeyouhu.library.KeyboardTouchListener;
import com.ziyeyouhu.library.KeyboardUtil;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants;
import cn.swiftpass.enterprise.mvp.contract.activity.OrderRefundLoginConfirmContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.OrderRefundLoginConfirmPresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.activity.MasterCardOrderRefundCompleteActivity;
import cn.swiftpass.enterprise.ui.activity.PreAutFinishActivity;
import cn.swiftpass.enterprise.ui.activity.spayMainTabActivity;
import cn.swiftpass.enterprise.ui.widget.CustomDialog;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.utils.ECDHUtils;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 退款登录确认验证
 *
 * @author he_hui
 * @version [版本号, 2016-11-8]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class OrderRefundLoginConfirmActivity extends BaseActivity<OrderRefundLoginConfirmContract.Presenter> implements OrderRefundLoginConfirmContract.View {

    private static final String TAG = OrderRefundLoginConfirmActivity.class.getCanonicalName();
    private Order orderModel;
    private EditText et_id;
    private Button btn_next_step;
    private int activityType;
    private int minus = 1;
    private boolean isCardPaymentRefund = false;
    private LinearLayout rootView;
    private KeyboardUtil keyboardUtil;
    private ScrollView scrollView;
    private CustomDialog errorDialog;

    public static void startActivity(Context context, Order orderModel) {
        Intent it = new Intent();
        it.setClass(context, OrderRefundLoginConfirmActivity.class);
        it.putExtra("order", orderModel);
        context.startActivity(it);
    }

    public static void startActivity(Context context, Order orderModel, boolean isCardPaymentRefund) {
        Intent it = new Intent();
        it.setClass(context, OrderRefundLoginConfirmActivity.class);
        it.putExtra("order", orderModel);
        it.putExtra("isCardPaymentRefund", isCardPaymentRefund);
        context.startActivity(it);
    }

    /**
     * 授权解冻跳转 参数2
     * 转支付跳转 参数3  不需要密码验证，不需要该界面
     *
     * @param context
     * @param orderModel
     */
    public static void startActivity(Context context, Order orderModel, int activityType) {
        Intent it = new Intent();
        it.setClass(context, OrderRefundLoginConfirmActivity.class);
        it.putExtra("order", orderModel);
        it.putExtra("activityType", activityType);
        context.startActivity(it);
    }

    @Override
    protected OrderRefundLoginConfirmContract.Presenter createPresenter() {
        return new OrderRefundLoginConfirmPresenter();
    }

    //安全键盘
    private void initMoveKeyBoard() {
        rootView = (LinearLayout) findViewById(R.id.rootview);
        scrollView = (ScrollView) findViewById(R.id.scrollview);
        keyboardUtil = new KeyboardUtil(this, rootView, scrollView);

        // monitor the KeyBarod state
        keyboardUtil.setKeyBoardStateChangeListener(new OrderRefundLoginConfirmActivity.KeyBoardStateListener());
        // monitor the finish or next Key
        keyboardUtil.setInputOverListener(new OrderRefundLoginConfirmActivity.inputOverListener());
        et_id.setOnTouchListener(new KeyboardTouchListener(keyboardUtil, KeyboardUtil.INPUT_TYPE_ABC, -1));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (keyboardUtil.isShow) {
                keyboardUtil.hideSystemKeyBoard();
                keyboardUtil.hideAllKeyBoard();
                keyboardUtil.hideKeyboardLayout();
                finish();
            } else {
                return super.onKeyDown(keyCode, event);
            }

            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }


    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_refund_login);
        orderModel = (Order) getIntent().getSerializableExtra("order");
        isCardPaymentRefund = getIntent().getBooleanExtra("isCardPaymentRefund", false);
        activityType = getIntent().getIntExtra("activityType", 0);
        if (activityType == 0) {
            titleBar.setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
        }
        initview();
        setLister();
        btn_next_step.getBackground().setAlpha(102);
        MainApplication.getInstance().getListActivities().add(this);
        initMoveKeyBoard();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                keyboardUtil.showKeyBoardLayout(et_id, KeyboardUtil.INPUT_TYPE_ABC, -1);
            }
        }, 200);

        //根据当前的小数点的位数来判断应该除以多少
        for (int i = 0; i < MainApplication.getInstance().getNumFixed(); i++) {
            minus = minus * 10;
        }

        if (activityType == 2) {
            btn_next_step.setBackgroundResource(R.drawable.btn_pre_finish);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        //onPause的时候把密码清空
        et_id.setText("");
    }

    private void setLister() {
        btn_next_step.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String pass = et_id.getText().toString();
                if (StringUtil.isEmptyOrNull(pass)) {
                    toastDialog(OrderRefundLoginConfirmActivity.this,
                            R.string.pay_refund_pwd, null);
                    et_id.setFocusable(true);
                    return;
                }
                checkUser();
            }
        });
    }


    @Override
    public void ecdhKeyExchangeSuccess(@Nullable ECDHInfo response) {
        if (response != null) {
            try {
                final String privateKey = SharedPreUtils.readProduct(ParamsConstants.APP_PRIVATE_KEY).toString();
                String secretKey = ECDHUtils.getInstance().ecdhGetShareKey(MainApplication.getInstance().getSerPubKey(), privateKey);
                SharedPreUtils.saveObject(secretKey, ParamsConstants.SECRET_KEY);
                //再去请求一次登录接口
                checkUser();
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                Log.e(TAG, Log.getStackTraceString(e));
            }
        }
    }

    @Override
    public void ecdhKeyExchangeFailed(@Nullable Object error) {

    }

    private void ECDHKeyExchange() {
        try {
            ECDHUtils.getInstance().getAppPubKey();
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
        }
        final String publicKey = SharedPreUtils.readProduct(ParamsConstants.APP_PUBLIC_KEY).toString();
        if (mPresenter != null) {
            mPresenter.ecdhKeyExchange(publicKey);
        }
    }


    @Override
    public void refundLoginSuccess(boolean response) {
        if (response) {
            if (orderModel != null) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        switch (activityType) {
                            case 0://0 默认消费
                                if (isCardPaymentRefund) {
                                    cardPaymentRefund();
                                } else {
                                    regisRefund();//消费
                                }
                                break;
                            case 2:
                                authUnfreeze();//预授权解冻 spay/authUnfreeze
                                break;
                            default:
                                break;
                        }
                    }
                });

            }
        }
    }

    @Override
    public void refundLoginFailed(@Nullable Object error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (error != null) {
                    if (error.toString().startsWith("405")) {//Require to renegotiate ECDH key
                        ECDHKeyExchange();
                    } else {
                        toastDialog(OrderRefundLoginConfirmActivity.this, error.toString(), null);
                    }
                }
            }
        });
    }

    /**
     * 验证用户
     */
    private void checkUser() {

        if (mPresenter != null) {
            mPresenter.refundLogin(
                    null,
                    MainApplication.getInstance().getUserInfo().username,
                    et_id.getText().toString()
            );
        }
    }

    @Override
    public void regisRefundsSuccess(@Nullable Order response) {
        if (response != null) {
            orderModel.setRefundNo(response.getRefundNo());
            orderModel.setAddTimeNew(response.getAddTimeNew());
            orderModel.setMoney(orderModel.getRefundFeel());

            orderModel.setCashFeel(response.getCashFeel());
            orderModel.setSurcharge(response.getSurcharge());
            orderModel.setRefundFee(response.getRefundFee());
            orderModel.setVat(response.getVat());
            orderModel.setOrderFee(response.getOrderFee());
            OrderRefundCompleteActivity.startActivity(OrderRefundLoginConfirmActivity.this, orderModel);

        }
    }

    @Override
    public void regisRefundsFailed(@Nullable Object error) {
        if (error != null) {
            toastDialog(OrderRefundLoginConfirmActivity.this, error.toString(),
                    getString(R.string.btnOk), new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            for (Activity a : MainApplication.getInstance().getListActivities()) {
                                a.finish();
                            }
                            OrderRefundLoginConfirmActivity.this.finish();
                        }
                    });
        }
    }

    private void regisRefund() {
        String transaction = null;
        if (mPresenter != null) {
            mPresenter.regisRefunds(
                    orderModel.getOutTradeNo(),
                    orderModel.getMoney(),
                    orderModel.getRefundFeel()
            );
        }
    }


    @Override
    public void cardPaymentRefundSuccess(@Nullable Order response) {
        if (response != null) {
            MasterCardOrderRefundCompleteActivity.startActivity(OrderRefundLoginConfirmActivity.this, response);
        }
    }

    @Override
    public void cardPaymentRefundFailed(@Nullable Object error) {
        if (error != null) {
            toastDialog(OrderRefundLoginConfirmActivity.this, error.toString(), getString(R.string.btnOk), new NewDialogInfo.HandleBtn() {
                @Override
                public void handleOkBtn() {
                    for (Activity a : MainApplication.getInstance().getListActivities()) {
                        if (!(a instanceof spayMainTabActivity)) {
                            a.finish();
                        }
                    }
                    OrderRefundLoginConfirmActivity.this.finish();
                }
            });
        }
    }

    private void cardPaymentRefund() {
        if (mPresenter != null) {
            mPresenter.cardPaymentRefund(
                    orderModel.getOrderNoMch(),
                    orderModel.getOutTradeNo(),
                    orderModel.getMoney(),
                    orderModel.getRefundFeel()
            );
        }
    }


    @Override
    public void authUnfreezeSuccess(@Nullable Order response) {
        if (response != null) {//最后跳转到完成界面

            if (MainApplication.getInstance().isAdmin(1)) {
                //此时要在打印小票的时候更新申请人
                if (orderModel != null) {
                    orderModel.setUserName(MainApplication.getInstance().getUserInfo().mchName);
                }
            } else {
                //此时要在打印小票的时候更新申请人
                if (orderModel != null) {
                    orderModel.setUserName(MainApplication.getInstance().getUserInfo().realname);
                }
            }
            orderModel.setUnFreezeTime(response.getUnFreezeTime());
            orderModel.setOutRequestNo(response.getOutRequestNo());
            orderModel.setAuthNo(response.getAuthNo());
            orderModel.setRequestNo(response.getRequestNo());
            orderModel.setTotalFee(response.getTotalFee());
            orderModel.setUnFreezeTime(response.getUnFreezeTime());

            PreAutFinishActivity.startActivity(OrderRefundLoginConfirmActivity.this, orderModel, 2);//传2
            for (Activity a : MainApplication.getInstance().getListActivities()) {
                a.finish();
            }
            finish();
        }
    }

    @Override
    public void authUnfreezeFailed(@Nullable Object error) {
        OrderRefundLoginConfirmActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showErrorDialog();
            }
        });
    }

    /**
     * cxy
     * 预授权解冻  需要密码
     */
    private void authUnfreeze() {
        if (orderModel != null) {

            if (mPresenter != null) {
                mPresenter.authUnfreeze(orderModel.getAuthNo(), orderModel.getMoney() + "");
            }

        }

    }

    private void showErrorDialog() {
        errorDialog = new CustomDialog.Builder(OrderRefundLoginConfirmActivity.this)
                .gravity(Gravity.CENTER).widthdp(250)
                .cancelTouchout(true)
                .view(R.layout.dialog_error)
                .addViewOnclick(R.id.tv_confirm, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        errorDialog.cancel();

                   /*     Intent it = new Intent();
                        it.putExtra("tab_index","bill");
                        it.setClass(OrderRefundLoginConfirmActivity.this, spayMainTabActivity.class);
                        startActivity(it);*/

                        for (Activity a : MainApplication.getInstance().getListActivities()) {
                            a.finish();
                        }
                        finish();
                    }
                })
                .build();
        errorDialog.show();
        TextView tv_title = (TextView) errorDialog.findViewById(R.id.tv_title);
        tv_title.setText(getString(R.string.Pre_auth_unfreezen_failed));
    }

    private void initview() {
        et_id = getViewById(R.id.et_id);
        btn_next_step = getViewById(R.id.btn_next_step);

        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (et_id.isFocused()) {
                    if (et_id.getText().toString().length() > 0) {
                        //                        btn_next_step.setEnabled(true);
                        //                        btn_next_step.setBackgroundResource(R.drawable.btn_register);

                        setButtonBg(btn_next_step, true, R.string.submit);
                    } else {
                        //                        btn_next_step.setEnabled(false);
                        //                        btn_next_step.setBackgroundResource(R.drawable.general_button_grey_default);

                        setButtonBg(btn_next_step, false, R.string.submit);
                    }
                }
            }
        });
        et_id.addTextChangedListener(editTextWatcher);
    }

    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b, enable, res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            if (activityType == 2) {
                b.setBackgroundResource(R.drawable.btn_nor_pre_shape);
            } else {
                b.setBackgroundResource(R.drawable.btn_nor_shape);
            }
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            if (activityType == 2) {
                b.setBackgroundResource(R.drawable.btn_press_pre_shape);
            } else {
                b.setBackgroundResource(R.drawable.btn_press_shape);
            }
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(R.string.tx_bill_stream_login_cirfom);
        titleBar.setTitleChoice(false);
        titleBar.setLeftButtonVisible(true);
        titleBar.setLeftButtonIsVisible(true);
        String saleOrPreauth = PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth", "sale");
        if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(saleOrPreauth, "pre_auth")) {
            titleBar.setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
        } else {
            titleBar.setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
        }
    }


    class KeyBoardStateListener implements KeyboardUtil.KeyBoardStateChangeListener {

        @Override
        public void KeyBoardStateChange(int state, EditText editText) {

            if (state == 1) {

                et_id.setFocusable(true);
                et_id.setFocusableInTouchMode(true);

                et_id.requestFocus();

                et_id.findFocus();
            }

        }
    }

    class inputOverListener implements KeyboardUtil.InputFinishListener {

        @Override
        public void inputHasOver(int onclickType, EditText editText) {

        }
    }

}
