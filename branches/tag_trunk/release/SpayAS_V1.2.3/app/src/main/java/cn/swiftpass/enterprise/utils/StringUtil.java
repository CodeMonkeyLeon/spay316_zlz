package cn.swiftpass.enterprise.utils;

public class StringUtil
{
    /**
     * <一句话功能简述>
     * <功能详细描述>
     * @param str
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static boolean isEmptyOrNull(String str)
    {
        if ("".equals(str) || null == str || "null".equals(str))
        {
            return true;
        }
        
        return false;
    }
    
    public static long paseStrToLong(String str)
    {
        if (str.equals("0.0"))
        {
            return 0;
        }
        else
        {
            return Long.parseLong(str);
        }
        
    }
    
}
