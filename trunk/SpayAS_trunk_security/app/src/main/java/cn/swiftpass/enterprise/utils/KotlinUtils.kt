package cn.swiftpass.enterprise.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.view.KeyEvent
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.common.Constant
import cn.swiftpass.enterprise.intl.BuildConfig
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.ui.activity.BaseActivity
import cn.swiftpass.enterprise.ui.activity.GuideActivity
import cn.swiftpass.enterprise.ui.activity.diagnosis.entity.IcmpSeqEntity
import cn.swiftpass.enterprise.ui.activity.diagnosis.entity.StatisticsAvgEntity
import cn.swiftpass.enterprise.ui.activity.diagnosis.entity.StatisticsEntity
import cn.swiftpass.enterprise.ui.activity.print.BluetoothSettingActivity
import cn.swiftpass.enterprise.ui.widget.DialogInfo
import cn.swiftpass.enterprise.utils.interfaces.OnOkClickListener
import com.example.common.sentry.SentryUtils.getClassNameAndMethodName
import com.example.common.sentry.SentryUtils.uploadTryCatchException
import com.google.gson.Gson
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.math.BigDecimal
import java.security.cert.Certificate
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.regex.Pattern


object KotlinUtils {

    const val TAG = "KotlinUtils"

    const val EXTERNAL_STORAGE_REQUEST_PERMISSION = 6666
    const val EXTERNAL_STORAGE_REQUEST_PERMISSION_PAY = 6667
    const val EXTERNAL_STORAGE_REQUEST_PERMISSION_SETTING = 6668

    const val APP_INSTALL_REQUEST_PERMISSION = 9999
    const val APP_INSTALL_REQUEST_PERMISSION_PAY = 9998
    const val APP_INSTALL_REQUEST_PERMISSION_SETTING = 9997


    /**
     * 获取证书
     */
    fun getCertificates(): ArrayList<Certificate> {
        val certificates = ArrayList<Certificate>()
        certificates.clear()
        //本地证书
        val localCerts = getCertificatesFromAssets()
        localCerts.forEach {
            certificates.add(it)
        }
        //服务器证书
        MainApplication.getInstance().certificateBean?.run {
            if (!TextUtils.isEmpty(this.publicKey)) {
                val serverCert =
                    getCertificateFromServer(this.publicKey)
                serverCert?.let {
                    certificates.add(it)
                }
            }
        }

        return certificates
    }


    /**
     * 获取本地证书
     */
    fun getCertificatesFromAssets(): ArrayList<Certificate> {
        val certificates = ArrayList<Certificate>()
        certificates.clear()
        try {
            val certificateFactory = CertificateFactory.getInstance("X.509")
            val certPath = "certs"
            val am = MainApplication.getInstance().applicationContext.assets
            val list = am.list(certPath)
            list?.run {
                forEachIndexed { _, v ->
                    certificates.add(certificateFactory.generateCertificate(am.open("$certPath/$v")))
                }
            }
            return certificates
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            Log.e("TAG_ZLZ", e.message!!)
        }
        return certificates
    }


    /**
     * 获取从服务器下载的证书
     */
    private fun getCertificateFromServer(base64CertificateString: String): X509Certificate? {
        try {
            // 将Base64编码的证书字符串转换为字节数组
            val certificateData: ByteArray = Base64.decode(base64CertificateString, Base64.DEFAULT)
            val certFactory = CertificateFactory.getInstance("X.509")
            val inputStream: InputStream = ByteArrayInputStream(certificateData)
            return certFactory.generateCertificate(inputStream) as X509Certificate
        } catch (e: Exception) {
            uploadTryCatchException(
                e,
                getClassNameAndMethodName()
            )
        }
        return null
    }


    fun getString(strId: Int): String = MainApplication.getInstance().getString(strId)


    @Throws
    fun analysisStatisticsAvg(str: String?): StatisticsAvgEntity? {
        str?.run {
            //rtt min/avg/max/mdev = 7.441/13.580/31.051/6.413 m
            val rttPattern =
                Pattern.compile("""rtt min/avg/max/mdev = ([\d.]+)/([\d.]+)/([\d.]+)/([\d.]+)""")
            val rttMatcher = rttPattern.matcher(this)

            rttMatcher.find()

            val rttMin = rttMatcher.group(1)
            val rttAvg = rttMatcher.group(2)
            val rttMax = rttMatcher.group(3)

            return StatisticsAvgEntity(
                rttMax,
                rttMin,
                rttAvg
            )
        }
        return null
    }


    @Throws
    fun analysisStatistics(str: String?): StatisticsEntity? {
        str?.run {
            //10 packets transmitted, 10 received, +27 duplicates, 0% packet loss, time 9013ms
            val packetsPattern = Pattern.compile("""(\d+)\s+packets""")
            val receivedPattern = Pattern.compile("""(\d+)\s+received""")

            val packetsMatcher = packetsPattern.matcher(this)
            val receivedMatcher = receivedPattern.matcher(this)

            packetsMatcher.find()
            receivedMatcher.find()

            val sent = packetsMatcher.group(1)
            val received = receivedMatcher.group(1)

            return StatisticsEntity(
                sent,
                received
            )
        }
        return null
    }

    @Throws
    fun analysisIcmp(str: String?): IcmpSeqEntity? {
        //64 bytes from 119.29.126.90: icmp_seq=1 ttl=52 time=7.96 ms
        str?.run {
            val bytesPattern = Pattern.compile("""(\d+)\s+bytes""")
            val icmpSeqPattern = Pattern.compile("""icmp_seq=(\d+)""")
            val timePattern = Pattern.compile("""time=([\d.]+)\s+ms""")

            val bytesMatcher = bytesPattern.matcher(this)
            val icmpSeqMatcher = icmpSeqPattern.matcher(this)
            val timeMatcher = timePattern.matcher(this)

            bytesMatcher.find()
            icmpSeqMatcher.find()
            timeMatcher.find()

            val bytes = bytesMatcher.group(1)
            val icmpSeq = icmpSeqMatcher.group(1)
            val time = timeMatcher.group(1)

            return IcmpSeqEntity(
                bytes,
                icmpSeq,
                time
            )
        }
        return null
    }


    fun closeBlueSetting() {
        MainApplication.getInstance().bluePrintSetting = false
        MainApplication.getInstance().blueDeviceName = ""
        MainApplication.getInstance().setBlueDeviceNameAddress("")
    }


    // 根据图片的URI自动校正图片的方向并显示到ImageView中
    fun displayImageWithOrientation(imageUri: Uri?, context: Context?): Bitmap {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            //android7
            try {
                context?.let { c ->
                    imageUri?.let { u ->
                        val inputStream = c.contentResolver.openInputStream(u)
                        val exifInterface = ExifInterface(inputStream!!)
                        val orientation = exifInterface.getAttributeInt(
                            ExifInterface.TAG_ORIENTATION,
                            ExifInterface.ORIENTATION_NORMAL
                        )
                        val bitmap = ImageUtil.getBitmapFromPath(c, u)
                        val matrix = Matrix()

                        when (orientation) {
                            ExifInterface.ORIENTATION_ROTATE_90 -> matrix.postRotate(90f)
                            ExifInterface.ORIENTATION_ROTATE_180 -> matrix.postRotate(180f)
                            ExifInterface.ORIENTATION_ROTATE_270 -> matrix.postRotate(270f)
                        }

                        return Bitmap.createBitmap(
                            bitmap,
                            0,
                            0,
                            bitmap.width,
                            bitmap.height,
                            matrix,
                            true
                        )
                    }
                }
            } catch (e: Exception) {
                uploadTryCatchException(
                    e,
                    getClassNameAndMethodName()
                )
            }
        }
        return ImageUtil.getBitmapFromPath(context, imageUri)
    }


    /**
     * 将金额格式分
     */
    fun getMoney(strMoney: String?): String? {
        var moneyText = strMoney
        val p = Pattern.compile("[^0-9.]") //去掉当前字符串中除去0-9以及小数点的其他字符
        val m = p.matcher(moneyText)
        moneyText = m.replaceAll("")
        val bigDecimal = BigDecimal(moneyText)
        var money = bigDecimal.toDouble()
        for (i in 0 until MainApplication.getInstance().getNumFixed()) {
            money *= 10
        }
        //        double t = money * 100;
        val df = DecimalFormat("0")
        return df.format(money)
    }


    fun parseToJson(
        listStr: List<String>?,
        tradeType: MutableList<Int?>,
        payType: MutableList<Int?>
    ) {
        if (listStr != null && listStr.isNotEmpty()) {
            for (s in listStr) {
                if (s == "wx") {
                    tradeType.add(1)
                } else if (s == "zfb") {
                    tradeType.add(2)
                } else if (s == "jd") {
                    tradeType.add(12)
                } else if (s == "qq") {
                    tradeType.add(4)
                } else if (s == "2") {
                    payType.add(2)
                } else if (s == "1") {
                    payType.add(1)
                } else if (s == "3") {
                    payType.add(3)
                } else if (s == "4") {
                    payType.add(4)
                } else if (s == "8") {
                    payType.add(8)
                }
            }
        }
    }


    /**
     * 是否已经登陆
     *
     * @return
     */
    @Synchronized
    fun isLoggedIn(): Boolean {
        return MainApplication.getInstance().userInfo != null && MainApplication.getInstance().userInfo.username != null
    }

    /**
     * 保存检测版本时间
     */
    fun saveCheckVersionTime() {
        val sp = MainApplication.getInstance().applicationPreferences
        val edit: SharedPreferences.Editor = sp.edit()
        edit.putLong(GlobalConstant.FILED_T_CHECK_VERSION, System.currentTimeMillis())
        edit.commit()
    }


    fun isNewVersion(): Boolean {
        return BuildConfig.VERSION_CODE > Constant.OLD_VERSION
    }

    /**
     * 必须升级标识
     */
    fun saveCheckVersionFlag(mustUpdate: Boolean) {
        val sp = MainApplication.getInstance().applicationPreferences
        val edit: SharedPreferences.Editor = sp.edit()
        edit.putBoolean(GlobalConstant.FILED_T_CHECK_VERSION_MUST_UPDATE, mustUpdate)
        edit.commit()
    }


    fun getAppVersionName(context: Context?): String {
        context?.let { c ->
            val info = getCurrentAppPackageInfo(c)
            info?.let {
                return it.versionName
            }
        }
        return ""
    }


    /**
     * 获取当前app包信息对象
     */
    fun getCurrentAppPackageInfo(context: Context?): PackageInfo? {
        context?.let { c ->
            try {
                val manager = c.packageManager
                val packageName = c.packageName
                return manager.getPackageInfo(packageName, 0)
            } catch (e: PackageManager.NameNotFoundException) {
                uploadTryCatchException(
                    e,
                    getClassNameAndMethodName()
                )
                Log.e(TAG, Log.getStackTraceString(e))
                throw RuntimeException(e)
            }
        }
        return null
    }


    /**
     * 检查权限
     */
    fun isPermissionsGranted(context: Context?, permissions: Array<String>): Boolean {
        context?.let { c ->
            permissions.forEach { p ->
                if (ContextCompat.checkSelfPermission(
                        c,
                        p
                    ) == PackageManager.PERMISSION_DENIED
                ) {
                    return false
                }
            }
            return true

        }
        return false
    }


    fun shouldShowRequestPermissionRationale(
        activity: Activity?,
        permissions: Array<String>
    ): Boolean {
        activity?.let { c ->
            permissions.forEach { p ->
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, p)) {
                    return true
                }
            }
        }
        return false
    }


    /**
     * 显示提示信息
     *
     * @since 2.5.0
     */
    fun showMissingPermissionDialogStartAppSetting(
        context: Context?,
        msg: String,
        title: String,
        cancel: String,
        permission: Array<String>
    ) {
        val permissionList = mutableListOf<String>()
        permission.forEach {
            permissionList.add(it)
        }


        val dialogInfo = DialogInfo(
            context,
            null,
            msg,
            title,
            cancel,
            DialogInfo.UNIFED_DIALOG,
            object : DialogInfo.HandleBtn {
                override fun handleOkBtn() {
                    (context as BaseActivity<*>).startPermissionActivity(permissionList)
                }

                override fun handleCancelBtn() {

                }
            },
            null
        )
        dialogInfo.setOnKeyListener { arg0, keycode, arg2 -> keycode == KeyEvent.KEYCODE_BACK }
        DialogHelper.resize(context, dialogInfo)
        dialogInfo.show()
    }


    /**
     * 显示提示信息
     *
     * @since 2.5.0
     */
    fun showMissingPermissionDialog(
        context: Context?,
        msg: String?,
        ok: String?,
        cancel: String?,
        listener: OnOkClickListener?
    ) {
        val dialogInfo = DialogInfo(
            context,
            null,
            msg,
            ok,
            cancel,
            DialogInfo.UNIFED_DIALOG,
            object : DialogInfo.HandleBtn {
                override fun handleOkBtn() {
                    listener?.let {
                        it.onOkClick()
                    }
//                    startAppSettings()
                }

                override fun handleCancelBtn() {

                }
            },
            null
        )
        dialogInfo.setOnKeyListener { arg0, keycode, arg2 -> keycode == KeyEvent.KEYCODE_BACK }
        DialogHelper.resize(context, dialogInfo)
        dialogInfo.show()
    }

    /**
     * 分享图片
     */
    fun shareImage(context: Context, imgPath: Uri?) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_STREAM, imgPath)
        //如果修改为 image/png P30 分享会报错 分享到备忘录 报图片太大 只能image/*
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_SUBJECT, "")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            intent.addFlags(
                Intent.FLAG_GRANT_READ_URI_PERMISSION
                        or Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_PREFIX_URI_PERMISSION
            )
        }
        context.startActivity(Intent.createChooser(intent, ""))
    }

    /**
     * 显示蓝牙设置
     */
    fun showBlueSettingDialog(activity: Activity?) {
        activity?.let { act ->
            var dialog: Dialog? = null
            dialog = DialogInfo(
                act,
                null,
                act.getString(R.string.tx_blue_set),
                act.getString(R.string.title_setting),
                act.getString(R.string.btnCancel),
                DialogInfo.UNIFED_DIALOG,
                object : DialogInfo.HandleBtn {
                    override fun handleOkBtn() {
                        act.startActivity(Intent(act, BluetoothSettingActivity::class.java))
                        dialog?.let { dia ->
                            dia.cancel()
                            dia.dismiss()
                        }
                    }

                    override fun handleCancelBtn() {
                        dialog?.let { dia ->
                            dia.dismiss()
                        }
                    }
                },
                null
            )
            if (dialog != null) {
                DialogHelper.resize(act, dialog)
                dialog.show()
            }
        }
    }


    /**
     * dp转换成px
     *
     * @param dp dp
     * @return px值
     */
    fun dp2px(dp: Float, context: Context): Int {
        val scale: Float = context.resources.displayMetrics.density
        return (dp * scale + 0.5f).toInt()
    }

    /**
     * json对象 -> String
     */
    fun <T> jsonToString(t: T): String {
        val gson = Gson()
        return gson.toJson(t)
    }

    /**
     * String -> json对象
     */
    fun <T> stringToJson(str: String, t: T): T? {
        try {
            val gson = Gson()
            t?.let {
                return gson.fromJson(str, it::class.java) as T
            }
            return null
        } catch (e: Exception) {
            uploadTryCatchException(
                e,
                getClassNameAndMethodName()
            )
        }
        return null
    }


    fun isReStartApp(savedInstanceState: Bundle?, fromActivity: Activity) {
        savedInstanceState?.let {
            reStartApp(fromActivity)
        }
    }


    fun reStartApp(fromActivity: Activity) {
        fromActivity.startActivity(Intent(fromActivity, GuideActivity::class.java))
        fromActivity.finish()
    }


//    /**
//     * 获取当前方法名
//     */
//    fun getCurrentMethodName(): String {
//        return Throwable().stackTrace[1].methodName
//    }
//
//    /**
//     * 获取当前类名
//     */
//    fun getCurrentClassName(): String {
//        return Throwable().stackTrace[1].className
//    }

//    /**
//     * 获取当前类名+方法名
//     */
//    fun getClassNameAndMethodName(): String {
//        return "类名: ${Throwable().stackTrace[1].className}   方法名: ${Throwable().stackTrace[1].methodName}"
//    }


    /**
     * 获取当前系统时间
     *
     * @return String
     */
    fun getSystemCurrentTime(): Date? {
        val sd = SimpleDateFormat("yyyy-MM-dd HH:mm")
        //long转Date
        return sd.parse(sd.format(Date(System.currentTimeMillis())))
    }

    /**
     * long类型转换成日期
     *
     * @param lo 毫秒数
     * @return String yyyy-MM-dd
     */
    fun longToDate(lo: Long): Date? {
        val sd = SimpleDateFormat("yyyy-MM-dd")
        //long转Date
        return sd.parse(sd.format(Date(lo)))
    }

    /**
     * 获取前90天前的年份
     *
     * @return
     */
    fun getStartYear(day: Int): Int {
        val beforeDate = Calendar.getInstance()
        beforeDate.add(Calendar.DATE, day)
        return beforeDate[Calendar.YEAR]
    }

    /**
     * 获取前后日期 i为正数 向后推迟i天，负数时向前提前i天
     *
     * @return
     */
    fun getStartDate(day: Int): Calendar? {
        val beforeDate = Calendar.getInstance()
        beforeDate.add(Calendar.DATE, day)
        return beforeDate
    }
}