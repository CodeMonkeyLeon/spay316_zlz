/*
 * 文 件 名:  MerchantTempDataModel.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2014-3-18
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 完善资料
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2014-3-18]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class MerchantTempDataModel implements Serializable {

    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;

    private Integer id;

    // 注册的手机号码
    private String phone;//电话

    private String addTime;//添加时间

    private String userName;//用户名

    private String password;//密码

    private Integer status; //激活状态：1：已经激活；2：未激活

    //spay新版本注册 
    public Integer merchantType; //商户小类型

    private String merchantTypeName; //商户类型名称

    private String merchantName; //商户名称

    private String regionNo; //区域

    private String regionName; //区域名称

    private String address; //详细地址  

    private String telephone;//联系电话 有格式规定 比如区号-电话号码 0755-26879999

    private String areaCode; //所在地区电话区号（4位数字）

    private Integer bankType; //商户银行类型

    private String bankTypeName; //银行类型

    private String branchBankName; //开户银行支行名称
    public String bankName; //开户银行支行名称

    private String bankUserName;//收款姓名 *
    public String accountName;//收款姓名 *

    private String bank; //商户银行账号
    public String bankNumberCode; //商户银行账号

    private String identityNo; //身份证号码
    public String accountIdEntity; //身份证号码

    private String idCardJustPic;//身份证正面照片名
    public String linencePhoto;//身份证正面照片名

    private String idCardTrunmPic;//身份证反面照片名

    private String identityPic; //运营者手持照片（路径）

    private String licensePic; // 营业执照 路径
    public String indentityPhoto; // 营业执照 路径

    private String idCode; // 机构代码证
    public String protocolPhoto; // 机构代码证

    private String principalPhone;

    private String principal;//负责人
    public ArrayList<ChannelRate> centerRate;

    private String email;

    private String bankNum;

    private String province; //省

    private String city; //市

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getPrincipalPhone() {
        return principalPhone;
    }

    public void setPrincipalPhone(String principalPhone) {
        this.principalPhone = principalPhone;
    }
    public String getIdCode() {
        return idCode;
    }

    /**
     * @param 对idCode进行赋值
     */
    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }



    /**
     * @return 返回 province
     */
    public String getProvince() {
        return province;
    }

    /**
     * @param 对province进行赋值
     */
    public void setProvince(String province) {
        this.province = province;
    }

    /**
     * @return 返回 city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param 对city进行赋值
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return 返回 bankNum
     */
    public String getBankNum() {
        return bankNum;
    }

    /**
     * @param 对bankNum进行赋值
     */
    public void setBankNum(String bankNum) {
        this.bankNum = bankNum;
    }

    /**
     * @return 返回 emil
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param 对emil进行赋值
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return 返回 licensePic
     */
    public String getLicensePic() {
        return licensePic;
    }

    /**
     * @param 对licensePic进行赋值
     */
    public void setLicensePic(String licensePic) {
        this.licensePic = licensePic;
    }

    //Bill Xu 2014-01-07 新增字段
    private String taxRegCertPic; //税务登记表扫描件

    private String orgaCodeCertPic; // 组织机构代码扫描件

    private String mercInfoFormPic; // 商户信息登记表扫描件 //暂无

    //临时字段
    private String merchantId;//商户表的商户号

    /**
     * @return 返回 phone
     */
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getMerchantType() {
        return merchantType;
    }

    public void setMerchantType(Integer merchantType) {
        this.merchantType = merchantType;
    }

    public String getMerchantTypeName() {
        return merchantTypeName;
    }

    public void setMerchantTypeName(String merchantTypeName) {
        this.merchantTypeName = merchantTypeName;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getRegionNo() {
        return regionNo;
    }

    public void setRegionNo(String regionNo) {
        this.regionNo = regionNo;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public Integer getBankType() {
        return bankType;
    }

    public void setBankType(Integer bankType) {
        this.bankType = bankType;
    }

    public String getBankTypeName() {
        return bankTypeName;
    }

    public void setBankTypeName(String bankTypeName) {
        this.bankTypeName = bankTypeName;
    }

    public String getBranchBankName() {
        return branchBankName;
    }

    public void setBranchBankName(String branchBankName) {
        this.branchBankName = branchBankName;
    }

    public String getBankUserName() {
        return bankUserName;
    }

    public void setBankUserName(String bankUserName) {
        this.bankUserName = bankUserName;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public String getIdCardJustPic() {
        return idCardJustPic;
    }

    public void setIdCardJustPic(String idCardJustPic) {
        this.idCardJustPic = idCardJustPic;
    }

    public String getIdCardTrunmPic() {
        return idCardTrunmPic;
    }

    public void setIdCardTrunmPic(String idCardTrunmPic) {
        this.idCardTrunmPic = idCardTrunmPic;
    }

    public String getIdentityPic() {
        return identityPic;
    }

    public void setIdentityPic(String identityPic) {
        this.identityPic = identityPic;
    }

    public String getTaxRegCertPic() {
        return taxRegCertPic;
    }

    public void setTaxRegCertPic(String taxRegCertPic) {
        this.taxRegCertPic = taxRegCertPic;
    }

    public String getOrgaCodeCertPic() {
        return orgaCodeCertPic;
    }

    public void setOrgaCodeCertPic(String orgaCodeCertPic) {
        this.orgaCodeCertPic = orgaCodeCertPic;
    }

    public String getMercInfoFormPic() {
        return mercInfoFormPic;
    }

    public void setMercInfoFormPic(String mercInfoFormPic) {
        this.mercInfoFormPic = mercInfoFormPic;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    @Override
    public String toString() {
        return "MerchantTempDataModel [id=" + id + ", phone=" + phone + ", addTime=" + addTime + ", userName="
                + userName + ", password=" + password + ", status=" + status + ", merchantType=" + merchantType
                + ", merchantTypeName=" + merchantTypeName + ", merchantName=" + merchantName + ", regionNo=" + regionNo
                + ", regionName=" + regionName + ", address=" + address + ", telephone=" + telephone + ", areaCode="
                + areaCode + ", bankType=" + bankType + ", bankTypeName=" + bankTypeName + ", branchBankName="
                + branchBankName + ", bankUserName=" + bankUserName + ", bank=" + bank + ", identityNo=" + identityNo
                + ", idCardJustPic=" + idCardJustPic + ", idCardTrunmPic=" + idCardTrunmPic + ", identityPic="
                + identityPic + ", licensePic=" + licensePic + ", taxRegCertPic=" + taxRegCertPic + ", orgaCodeCertPic="
                + orgaCodeCertPic + ", mercInfoFormPic=" + mercInfoFormPic + ", merchantId=" + merchantId + "]";
    }

}
