package cn.swiftpass.enterprise.bussiness.logica.Zxing;

import java.util.Hashtable;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import cn.swiftpass.enterprise.bussiness.logica.BaseManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.NotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

public class MaxCardManager extends BaseManager
{
    private static MaxCardManager instance;
    
    // 图片大小,注意图片不要设置太大,否则会影响二维码的识别
    private static final int IMAGE_WIDTH = 30;
    
    private MaxCardManager()
    {
    }
    
    public static MaxCardManager getInstance()
    {
        if (instance == null)
        {
            instance = new MaxCardManager();
        }
        return instance;
    }
    
    @Override
    public void init()
    {
    }
    
    @Override
    public void destory()
    {
    }
    
    public void create2DCode(final String str, final UINotifyListener<Bitmap> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<Bitmap>()
        {
            
            @Override
            public Bitmap execute()
                throws Exception
            {
                Bitmap bitmap = create2DCode(str, 400, 400);
                if (null != bitmap)
                {
                    
                    return bitmap;
                }
                else
                {
                    return null;
                }
                
            }
            
        }, listener);
    }
    
    public void uploadMaxCard(final Bitmap bitmap, final NotifyListener<String> listener)
    {
        //保存 并上传
        
    }
    
    public Bitmap create2DCode(Activity activity, String str, int w, int h, Bitmap mBitmap)
        throws WriterException
    {
        // 生成二维矩阵,编码时指定大小,不要生成了图片以后再进行缩放,这样会模糊导致识别失败
        //        Bitmap mBitmap = ((BitmapDrawable)activity.getResources().getDrawable(logo)).getBitmap();
        // 进行缩放图片
        Matrix m = new Matrix();
        float sx = (float)2 * IMAGE_WIDTH / mBitmap.getWidth();
        float sy = (float)2 * IMAGE_WIDTH / mBitmap.getHeight();
        m.setScale(sx, sy);
        // 重新构造位图对象
        mBitmap = Bitmap.createBitmap(mBitmap, 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), m, true);
        Hashtable hints = new Hashtable();
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        
        BitMatrix matrix = new MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, w, h, hints);//360   360
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        // 二维矩阵转为一维像素数组,也就是一直横着排了
        int halfW = width / 2;
        int halfH = height / 2;
        
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                // 如果mBitmap不为空，则添加中间小图片
                if (mBitmap != null && x > halfW - IMAGE_WIDTH && x < halfW + IMAGE_WIDTH && y > halfH - IMAGE_WIDTH
                    && y < halfH + IMAGE_WIDTH)
                {
                    pixels[y * width + x] = mBitmap.getPixel(x - halfW + IMAGE_WIDTH, y - halfH + IMAGE_WIDTH);
                }
                else if (matrix.get(x, y))
                {
                    pixels[y * width + x] = 0xff000000;
                }
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        // 通过像素数组生成bitmap,具体参考api
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }
    
    /**
     * 用字符串生成二维码
     */
    public Bitmap create2DCode(String str, int w, int h)
        throws WriterException
    {
        // 生成二维矩阵,编码时指定大小,不要生成了图片以后再进行缩放,这样会模糊导致识别失败
        BitMatrix matrix = new MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, w, h);//240 240
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        // 二维矩阵转为一维像素数组,也就是一直横着排了
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if (matrix.get(x, y))
                {
                    pixels[y * width + x] = 0xff000000;
                }
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        // 通过像素数组生成bitmap,具体参考api
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }
}
