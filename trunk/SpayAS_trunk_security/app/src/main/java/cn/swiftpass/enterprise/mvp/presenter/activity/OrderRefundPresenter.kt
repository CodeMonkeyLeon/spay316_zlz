package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.common.Constant
import cn.swiftpass.enterprise.intl.BuildConfig
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.OrderRefundContract
import cn.swiftpass.enterprise.utils.JsonUtil
import com.example.common.entity.EmptyData
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/2
 *
 * @高尚的情趣会支撑你一生
 * @使你在最严酷的冬天也不忘记玫瑰的芳香
 */
class OrderRefundPresenter : OrderRefundContract.Presenter {

    private var mView: OrderRefundContract.View? = null


    override fun authPayQuery(
        orderNo: String?,
        outRequestNo: String?
    ) {
        mView?.let { view ->

            AppClient.authPayQuery(
                MainApplication.getInstance().getMchId(),
                MainApplication.getInstance().getUserId().toString(),
                outRequestNo,
                orderNo,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        commonResponse?.let {
                            view.authPayQueryFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        r?.let { response ->
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                Order::class.java
                            ) as Order
                            view.authPayQuerySuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun unifiedAuthPay(
        money: String?,
        authNo: String?,
        body: String
    ) {

        mView?.let { view ->

            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)

            AppClient.unifiedAuthPay(
                MainApplication.getInstance().getMchId(),
                MainApplication.getInstance().getUserId().toString(),
                authNo,
                body,
                money,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {


                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.unifiedAuthPayFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                Order::class.java
                            ) as Order
                            view.unifiedAuthPaySuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun checkCanRefundOrNot(
        orderNo: String?,
        money: Long,
        refundMoney: Long
    ) {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
            val userName = if (MainApplication.getInstance().isAdmin(0)) {
                // 普通用户才需要查自己的数据，管理员可以查询全部
                MainApplication.getInstance().userInfo.realname // 收银员名称
            } else {
                MainApplication.getInstance().userInfo.mchName // 商户名称
            }
            val body =
                if (!TextUtils.isEmpty(MainApplication.getInstance().userInfo.tradeName)) MainApplication.getInstance().userInfo.tradeName else BuildConfig.body
            AppClient.checkCanRefundOrNot(
                orderNo,
                Constant.CLIENT,
                refundMoney.toString(),
                money.toString(),
                MainApplication.getInstance().getMchId(),
                MainApplication.getInstance().getUserId().toString(),
                userName,
                body,
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.checkCanRefundOrNotFailed(if (TextUtils.isEmpty(it.message)) it.message else it.result)
                        }
                    }

                    override fun onResponse(response: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        view.checkCanRefundOrNotSuccess(true)
                    }
                }
            )
        }
    }

    override fun attachView(view: OrderRefundContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}