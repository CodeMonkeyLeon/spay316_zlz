package cn.swiftpass.enterprise.bussiness.logica.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import cn.newpost.tech.blue.socket.PosBlueSocket;
import cn.newpost.tech.blue.socket.entity.Order;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.BlueDevice;
import cn.swiftpass.enterprise.bussiness.model.PosBluetoothResult;
import cn.swiftpass.enterprise.utils.Logger;

/**
 * pos
 * User: Alan
 * Date: 14-1-20
 * Time: 下午8:30
 */
public class BluetoothManager {

    private static final String TAG = BluetoothManager.class.getSimpleName();
    /*******************************单列******************************/
    public static BluetoothManager getInstance() {
        return Container.instance;
    }
    private static class Container {
        public static BluetoothManager instance = new BluetoothManager();
    }
    /*******************************成员******************************/
    public final static String ip = "210.22.14.210";
    public final static int port = 6608;
    private static final String tag = "pos";

    /*******************************公开业务方法******************************/
    /***
     * 连接服务器进行创建连接
     * @param myDevice
     * @param listener
     */
    public void connectionBluetoothPos(final BluetoothDevice myDevice,UINotifyListener<PosBluetoothResult> listener){
        ThreadHelper.executeWithCallback(new Executable<PosBluetoothResult>() {
            @Override
            public PosBluetoothResult execute() throws Exception {
                PosBluetoothResult result = new PosBluetoothResult();
                PosBlueSocket blueSocket = new PosBlueSocket(MainApplication.getContext(), ip, port);
                result.state = blueSocket.create(myDevice,Order.OFFINE);
                result.posBlueSocket = blueSocket;
                return  result;
            }
        }, listener);
    }
    /***
     * 查找已经蓝牙设备
     * @param bondedDevices
     * @param listener
     */
    public void searchBlueDevice(final Set<BluetoothDevice> bondedDevices,final UINotifyListener<List<BlueDevice>> listener)
    {
        ThreadHelper.executeWithCallback(new Executable() {
            @Override
            public  List<BlueDevice> execute() throws Exception {
                List<BlueDevice> devices = new ArrayList<BlueDevice>();
                if (bondedDevices.size() > 0) {
                    for (BluetoothDevice device : bondedDevices) {
                        if (device.getName().contains(tag)) {
                            BlueDevice bd = new BlueDevice();
                            bd.setDeviceName(device.getName());
                            bd.setAddress(device.getAddress());
                            bd.setBondState(device.getBondState());
                            devices.add(bd);
                        }
                    }
                }
                return devices;
            }
        },listener);
    }

    /***
     * pos刷卡交易
     * @param listener
     */
    public void posTransacte(final PosBlueSocket blueSocket ,final Order order ,final UINotifyListener<Order> listener)
    {
        ThreadHelper.executeWithCallback(new Executable() {
            @Override
            public Order execute() throws Exception {
                return  blueSocket.writeToPOS(order);//order).transacte
            }
        },listener);
    }

    /**
     * 左端填充0,补齐12位
     * @param num
     * @param length
     * @return
     */
    public static String fillLeftWith0(String num, int length) {
        StringBuffer s = new StringBuffer();
        if (num.contains(","))
            num = num.replace(",", ".");
        String stn = num.replace(".", "");
        if (stn.length() < length) {
            for (int i = 0; i < length - stn.length(); i++) {
                s.append("0");
            }
            s.append(stn);
        }
        Log.i(TAG,s.toString());

        return s.toString();
    }
    /**
     * 金额格式转换
     * @param amount
     * @return
     */
    public static String amountConvert(String amount) {
        int i = Integer.parseInt(amount);
        StringBuffer sb = new StringBuffer();
        String si = String.valueOf(i);
        if (si.length() > 2) {
            sb.append(si.substring(0, si.length() - 2)).append(".")
                    .append(si.subSequence(si.length() - 2, si.length()));
        } else if (si.length() == 2) {
            sb.append("0.").append(si);
        } else if (si.length() == 1) {
            sb.append("0.0").append(si);
        }
        return sb.toString();
    }
    /**
     * 将String型格式化,比如想要将2011-11-11格式化成2011年11月11日,就StringPattern("2011-11-11","yyyy-MM-dd","yyyy年MM月dd日").
     * @param date String 想要格式化的日期
     * @param oldPattern String 想要格式化的日期的现有格式
     * @param newPattern String 想要格式化成什么格式
     * @return String
     */
    public final static String StringPattern(String date, String oldPattern, String newPattern) {
        if (date == null || oldPattern == null || newPattern == null)
            return "";
        SimpleDateFormat sdf1 = new SimpleDateFormat(oldPattern); // 实例化模板对象
        SimpleDateFormat sdf2 = new SimpleDateFormat(newPattern); // 实例化模板对象
        Date d = null;
        try {
            d = sdf1.parse(date); // 将给定的字符串中的日期提取出来
        } catch (Exception e) { // 如果提供的字符串格式有错误，则进行异常处理
            Log.e(TAG, Log.getStackTraceString(e)); // 打印异常信息
        }
        return sdf2.format(d);
    }
}
