package cn.swiftpass.enterprise.ui.activity.live;

import android.annotation.TargetApi;
import android.app.Service;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;


import cn.swiftpass.enterprise.ui.activity.MainActivity;

/**
 * JobService，支持5.0以上forcestop依然有效
 * <p>
 * Created by jianddongguo on 2017/7/10.
 */
@TargetApi(21)
public class AliveJobService extends JobService {
    private final static String TAG = "KeepAliveService";
    // 告知编译器，这个变量不能被优化
    private volatile static Service mKeepAliveService = null;

    public static boolean isJobServiceAlive() {
        return mKeepAliveService != null;
    }

    private static final int MESSAGE_ID_TASK = 0x01;

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (SystemUtils.isAPPALive(getApplicationContext(), getPackageName())) {
                Log.d(TAG, "KeepAliveService----->APP live");
                Intent intent = new Intent(getApplicationContext(), PlayerMusicService.class);
                startService(intent);

                Intent foregroundService = new Intent(getApplicationContext(), ForegroundLiveService.class);
                startService(foregroundService);

                Intent liveService = new Intent(getApplicationContext(), PlayerMusicService.class);
                startService(liveService);


            } else {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Log.d(TAG, "KeepAliveService----->APP is kill  start again ");
            }
            jobFinished((JobParameters) msg.obj, false);
            return true;
        }
    });

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "KeepAliveService----->onStartCommand...");
        return START_STICKY;
    }

    @Override
    public boolean onStartJob(JobParameters params) {

        Log.d(TAG, "KeepAliveService----->onStartJob...");
        mKeepAliveService = this;
        // 返回false，系统假设这个方法返回时任务已经执行完毕；
        // 返回true，系统假定这个任务正要被执行
        Message msg = Message.obtain(mHandler, MESSAGE_ID_TASK, params);
        mHandler.sendMessage(msg);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        mHandler.removeMessages(MESSAGE_ID_TASK);
        Log.d(TAG, "KeepAliveService----->JobService onStopJob");
        return false;
    }
}
