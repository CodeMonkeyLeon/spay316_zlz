package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.bussiness.model.ReportTransactionDetailsBean
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.TransactionDetailsContract
import cn.swiftpass.enterprise.utils.JsonUtil
import okhttp3.Call


/**
 * @author lizheng.zhao
 * @date 2022/11/21
 *
 * @世界都湿了
 * @星星亮得怕人
 * @我收起伞
 * @天收起滴水的云
 * @时针转到零点
 * @㧟了上帝的脚跟
 * @你没有来
 * @我还在等
 */
class TransactionDetailsPresenter : TransactionDetailsContract.Presenter {


    private var mView: TransactionDetailsContract.View? = null


    override fun queryTransactionReportDetails(
        reportType: String?, startDate: String?, endDate: String?
    ) {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
            AppClient.queryTransactionReportDetails(
                reportType,
                startDate,
                endDate,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<ReportTransactionDetailsBean>(
                    ReportTransactionDetailsBean()
                ) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { error ->
                            view.queryTransactionReportDetailsFailed(error.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            response.message = response.message.replace(
                                "\"dayStatistics\":\"\"", "\"dayStatistics\":[]"
                            )
                            response.message = response.message.replace(
                                "\"payChannelStatistical\":\"\"", "\"payChannelStatistical\":[]"
                            )
                            response.message = response.message.replace(
                                "\"statisticsByAmount\":\"\"", "\"statisticsByAmount\":[]"
                            )
                            response.message = response.message.replace(
                                "\"statisticsByCount\":\"\"", "\"statisticsByCount\":[]"
                            )
                            val bean = JsonUtil.jsonToBean(
                                response.message, ReportTransactionDetailsBean::class.java
                            ) as ReportTransactionDetailsBean

                            view.queryTransactionReportDetailsSuccess(bean)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: TransactionDetailsContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}