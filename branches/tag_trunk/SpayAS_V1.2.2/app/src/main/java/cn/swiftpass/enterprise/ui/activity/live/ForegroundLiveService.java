package cn.swiftpass.enterprise.ui.activity.live;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import cn.swiftpass.enterprise.intl.R;


public class ForegroundLiveService extends Service {

    private static final String TAG = "ForegroundLiveService";
    public static final int NOTIFICATION_ID = 0x11;
    public static final String SETTINGS_ACTION =
            "android.settings.APPLICATION_DETAILS_SETTINGS";

    public ForegroundLiveService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "ForegroundLiveService ->onCreate");
        //API 18以下，直接发送Notification并将其置为前台
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
////            RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.layout_notification);
//            Notification builder = new Notification.Builder(this.getApplicationContext()).setSmallIcon(R.drawable.icon_app_logo).build();// 设置自定义的Notification内容
////            builder.setWhen(System.currentTimeMillis()).setSmallIcon(R.mipmap.ic_launcher);
////            Notification notification = builder.getNotification();
////            // 获取构建好的通知--.build()最低要求在// API16及以上版本上使用，低版本上可以使用.getNotification()。
////            Notificationnotification.defaults = Notification.DEFAULT_SOUND;//设置为默认的声音
//            startForeground(NOTIFICATION_ID, builder);
////            startForeground(NOTIFICATION_ID, new Notification());
//        } else {
        //API 18以上，发送Notification并将其置为前台后，启动InnerService
        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.drawable.icon_app_logo);
        builder.setContentTitle(getString(R.string.app_name)+ " " + getString(R.string.tv_notification_title));
        builder.setContentText(getString(R.string.tv_notification_content));

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(SETTINGS_ACTION);
        broadcastIntent.setData(Uri.fromParts("package", getPackageName(), null));
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, broadcastIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        startForeground(NOTIFICATION_ID, builder.build());
//            startService(new Intent(this, InnerService.class));
//        }
    }


    public static class InnerService extends Service {
        @Override
        public IBinder onBind(Intent intent) {
            return null;
        }

        @Override
        public void onCreate() {
            super.onCreate();
            Log.i(TAG, "Service ->onCreate");
            //发送与KeepLiveService中ID相同的Notification，然后将其取消并取消自己的前台显示
            Notification.Builder builder = new Notification.Builder(this);
            builder.setSmallIcon(R.drawable.ic_launcher);
            startForeground(NOTIFICATION_ID, builder.build());
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    stopForeground(true);
                    NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    manager.cancel(NOTIFICATION_ID);
                    stopSelf();
                }
            }, 100);
        }
    }
}
