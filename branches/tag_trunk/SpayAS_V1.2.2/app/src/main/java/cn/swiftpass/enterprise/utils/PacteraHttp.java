/*
 * 文 件 名:  PacteraHttp.java
 * 版    权:  copyright: 2013 Pactera. All rights reserved.
 * 描    述:  <描述>
 * 修 改 人:  jiaohongyun
 * 修改时间:  2013年11月4日
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.SecureRandom;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import org.apache.http.conn.ssl.AllowAllHostnameVerifier;

/**
 * Http操作处理类
 * 
 * @author jiaohongyun
 * @version [版本号, 2013年11月4日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class PacteraHttp
{
    /**
     * 日志用的TAG
     */
    private static final String LOG_TAG = PacteraHttp.class.getCanonicalName();
    
    /**
     * 授权,认证
     */
    @SuppressWarnings("unused")
    private String authorization = "";
    
    /**
     * 接收编码
     */
    @SuppressWarnings("unused")
    private static final String Accept_Encoding = "gzip";
    
    /**
     * 内容类型
     */
    @SuppressWarnings("unused")
    private static final String CONTENT_TYPE = "application/json;charset=UTF-8";
    
    @SuppressWarnings("unused")
    private static final String User_Agent = "Android/1.0";
    
    /**
     * 是否使用https连接
     */
    private boolean isHttps = false;
    
    /**
     * 以POST方法发出请求
     */
    private static final String POST = "POST";
    
    /**
     * 以GET方法发出请求
     */
    @SuppressWarnings("unused")
    private static final String GET = "GET";
    
    /**
     * 使用的字符集
     */
    private String charset = "UTF-8";
    
    /**
     * 超时时间，默认6秒
     */
    private static int TIME_OUT = 16 * 1000;
    
    private static PacteraHttp pacteraHttp = null;
    
    /**
     * 默认构造函数
     */
    public PacteraHttp()
    {
        super();
    }
    
    public static PacteraHttp getInstance()
    {
        
        if (pacteraHttp == null)
        {
            pacteraHttp = new PacteraHttp();
        }
        return pacteraHttp;
        
    }
    
    /**
     * 有参构造函数 可以设置是否启用https
     */
    public PacteraHttp(boolean isHttps)
    {
        super();
        this.isHttps = isHttps;
    }
    
    /**
     * @param 对authorization进行赋值
     */
    public void setAuthorization(String authorization)
    {
        this.authorization = authorization;
    }
    
    /**
     * Http Post请求方法
     * @param url 请求路径
     * @param data 请求参数（json字符串）
     * @return 响应的json字符串
     * @see [类、类#方法、类#成员]
     */
    public String post(String url, String data)
    {
        String jsonStr = "";
        if (isHttps)
        {
            jsonStr = HttpsPost(url, data);
        }
        else
        {
            jsonStr = HttpPost(url, data);
        }
        return jsonStr;
    }
    
    /**
     * Http Post请求方法
     * @param url
     * @return
     * @see [类、类#方法、类#成员]
     */
    private String HttpPost(String url, String data)
    {
        String jsonStr = "";
        HttpURLConnection connection = null;
        try
        {
            connection = (HttpURLConnection)new URL(url).openConnection();
            connection.setDoInput(true);// 设置可以读取返回的数据
            connection.setDoOutput(true); // 设置可以提交数据
            
            connection.setRequestProperty("Connection", "close");
            connection.setUseCaches(false);// 不用cache
            // 设置超时时间
            connection.setConnectTimeout(TIME_OUT);
            connection.setReadTimeout(TIME_OUT);
            
            connection.setRequestMethod(POST);
            connection.setRequestProperty("authentication", authorization);
            
            connection.setRequestProperty("User-Agent", "Android/1.0");
            connection.setRequestProperty("Accept-Encoding", "gzip");
            connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            
            connection.getOutputStream().write(data.getBytes());
            connection.getOutputStream().flush();
            connection.getOutputStream().close();
            
            int code = connection.getResponseCode();
            if (code == HttpURLConnection.HTTP_OK)
            {
                InputStream in = connection.getInputStream();
                BufferedReader rd = null;
                
                String contentEncoding = connection.getContentEncoding();
                if (contentEncoding != null && contentEncoding.equalsIgnoreCase("gzip"))
                {
                    rd = new BufferedReader(new InputStreamReader(new GZIPInputStream(in), charset), 8192);
                }
                else
                {
                    rd = new BufferedReader(new InputStreamReader(in, charset), 8192); // 对应的字符编码转换
                }
                String tempLine = rd.readLine();
                StringBuffer temp = new StringBuffer();
                while (tempLine != null)
                {
                    temp.append(tempLine);
                    tempLine = rd.readLine();
                }
                jsonStr = temp.toString();
                rd.close();
                in.close();
            }
            else
            {
                Logger.i(LOG_TAG, "Http Response Code:" + code);
            }
        }
        catch (MalformedURLException e)
        {
            Logger.e(LOG_TAG, e.getMessage());
        }
        catch (IOException e)
        {
            Logger.e(LOG_TAG, "connect service failed !");
        }
        finally
        {
            if (connection != null)
            {
                connection.disconnect();
            }
        }
        return jsonStr;
    }
    
    /**
     * Https Post请求方法
     * @param url
     * @return
     * @see [类、类#方法、类#成员]
     */
    private String HttpsPost(String url, String data)
    {
        // 未完成
        String jsonStr = "";
        HttpsURLConnection connection = null;
        try
        {
            connection = (HttpsURLConnection)new URL(url).openConnection();
            // 初始化X509TrustManager中的SSLContext
            // Trust all certificates
            final TrustManager[] TRUST_MANAGER = new TrustManager[] {new MyX509TrustManager()};
            final AllowAllHostnameVerifier hostnameVerifierHostNameVerifier = new AllowAllHostnameVerifier();
            
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(new KeyManager[0], TRUST_MANAGER, new SecureRandom());
            SSLSocketFactory socketFactory = context.getSocketFactory();
            ((HttpsURLConnection)connection).setSSLSocketFactory(socketFactory);
            
            // Allow all hostnames
            ((HttpsURLConnection)connection).setHostnameVerifier(hostnameVerifierHostNameVerifier);
            
            connection.setDoInput(true);// 设置可以读取返回的数据
            connection.setDoOutput(true); // 设置可以提交数据
            
            connection.setRequestProperty("Connection", "close");
            connection.setUseCaches(false);// 不用cache
            // 设置超时时间
            connection.setConnectTimeout(TIME_OUT);
            connection.setReadTimeout(TIME_OUT);
            
            connection.setRequestMethod(POST);
            
            connection.setRequestProperty("authentication", authorization);
            
            connection.setRequestProperty("User-Agent", "Android/1.0");
            connection.setRequestProperty("Accept-Encoding", "gzip");
            connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            
            connection.getOutputStream().write(data.getBytes());
            connection.getOutputStream().flush();
            connection.getOutputStream().close();
            
            int code = connection.getResponseCode();
            if (code == HttpURLConnection.HTTP_OK)
            {
                InputStream in = connection.getInputStream();
                BufferedReader rd = null;
                
                String contentEncoding = connection.getContentEncoding();
                if (contentEncoding != null && contentEncoding.equalsIgnoreCase("gzip"))
                {
                    
                    rd = new BufferedReader(new InputStreamReader(new GZIPInputStream(in), charset), 8192);
                }
                else
                {
                    rd = new BufferedReader(new InputStreamReader(in, charset), 8192); // 对应的字符编码转换
                }
                String tempLine = rd.readLine();
                StringBuffer temp = new StringBuffer();
                while (tempLine != null)
                {
                    temp.append(tempLine);
                    tempLine = rd.readLine();
                }
                jsonStr = temp.toString();
                rd.close();
                in.close();
            }
            else
            {
                Logger.i(LOG_TAG, "Http Response Code:" + code);
            }
        }
        catch (MalformedURLException e)
        {
            Logger.e(LOG_TAG, e.getMessage());
        }
        catch (IOException e)
        {
            Logger.e(LOG_TAG, e.getMessage());
        }
        catch (Exception e)
        {
            Logger.e(LOG_TAG, e.getMessage());
        }
        if (connection != null)
        {
            connection.disconnect();
        }
        return null;
    }
}
