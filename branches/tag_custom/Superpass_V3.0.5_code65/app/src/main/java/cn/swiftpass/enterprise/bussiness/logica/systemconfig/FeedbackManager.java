package cn.swiftpass.enterprise.bussiness.logica.systemconfig;

import org.json.JSONObject;

import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.NetHelper;
import cn.swiftpass.enterprise.utils.Utils;

/**
 * 问题意见反馈
 * User: Administrator
 * Date: 13-11-27
 * Time: 下午6:04
 * To change this template use File | Settings | File Templates.
 */
public class FeedbackManager
{
    
    private static FeedbackManager instance;
    
    public static FeedbackManager getInstance()
    {
        if (instance == null)
        {
            instance = new FeedbackManager();
        }
        return instance;
    }
    
    public void sendFeedBack(final String content, final boolean isSendLog, final UINotifyListener<Boolean> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<Boolean>()
        {
            @Override
            public Boolean execute()
                throws Exception
            {
                UserModel userModel = LocalAccountManager.getInstance().getLoggedUser();
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("uId", userModel.uId + "");
                jsonObject.put("content", content);
                jsonObject.put("clientType", ApiConstant.SPAY + "");
                RequestResult result =
                    NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.ORDER_GET_SUB_FEEDBACK, jsonObject);
                if (!result.hasError())
                {
                    int ret = Utils.Integer.tryParse(result.data.getString("result"), -1);
                    switch (ret)
                    {
                        case 0:
                            return true;
                        case -1:
                            listener.onError("请稍候在试，内部异常！");
                            break;
                    }
                }
                else
                {
                    return false;
                }
                return false;
            }
        },
            listener);
    }
}
