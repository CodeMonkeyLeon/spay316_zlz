package cn.swiftpass.enterprise.ui.activity.setting;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.activity.ContentTextActivity;
import cn.swiftpass.enterprise.ui.activity.diagnosis.view.NetworkDiagnosisActivity;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.AppHelper;

/**
 * File | Settings | File Templates.
 */
public class SettingAboutActivity extends BaseActivity {

    private TextView tv_ver;
    private LinearLayout ll_privacy_policy;

    private TextView mTvNetworkDiagnosis;

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_aboutus);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {
            }

            @Override
            public void onRightButLayClick() {
            }
        });
    }


    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void initViews() {
        setContentView(R.layout.activity_setting_about);
        ll_privacy_policy = findViewById(R.id.ll_privacy_policy);
        tv_ver = findViewById(R.id.tv_ver);
        tv_ver.setText(getString(R.string.app_name) + " " + "V" + AppHelper.getVerName(this));
        mTvNetworkDiagnosis = findViewById(R.id.id_btn_network_diagnosis);
        if (!BuildConfig.isShowNetworkDiagnosis) {
            mTvNetworkDiagnosis.setVisibility(View.GONE);
        }
        ll_privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {// 跳转到隐私页面
                ContentTextActivity.startActivity(SettingAboutActivity.this,
                        BuildConfig.privacyPolicy, null);
            }
        });


        mTvNetworkDiagnosis.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mTvNetworkDiagnosis.setTextColor(getResources().getColor(R.color.color_app_theme_half));
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    mTvNetworkDiagnosis.setTextColor(getResources().getColor(R.color.color_app_theme));
                    goToNetworkDiagnosisPage();
                }
                return true;
            }
        });
    }

    private void goToNetworkDiagnosisPage() {
        NetworkDiagnosisActivity.Companion.startNetworkDiagnosisActivity(this);
    }


    @Override
    protected boolean isLoginRequired() {
        return false;
    }
}