/*
 * 文 件 名:  SettleActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-5-31
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.settle;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.Date;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.SettleModel;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.SettleUserContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.SettleUserPresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.widget.DateTimePickDialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 结算汇总
 *
 * @author he_hui
 * @version [版本号, 2016-5-31]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class SettleUserActivity extends BaseActivity<SettleUserContract.Presenter> implements SettleUserContract.View {

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case HandlerManager.SETTLE_TYPE:
//                    try {
//                        userModel = (UserModel) msg.obj;
//                        if (null != userModel) {
//                            initValue(userModel);
//
//                            loadData();
//                        }
//
//                    } catch (Exception e) {
//
//                    }
                    break;
                default:
                    break;
            }
        }

    };
    private RelativeLayout lay_end_time, lay_start_time;
    private TextView tv_start_time, tv_end_time;
    private TextView et_order_max_money, tv_succ_count, tv_refund_total, tv_refund_count, tv_wx_card_money, tv_wx_card,
            tv_total_count, tv_net_fee;
    private LinearLayout lay_cashier;
    private TextView tv_num, tv_name, tv_tel, tv_department;
    private UserModel userModel;
    private ImageView iv_clear;

    public static void startActivity(Context context, UserModel userModel) {
        Intent it = new Intent();
        it.setClass(context, SettleUserActivity.class);
        it.putExtra("userModel", userModel);
        context.startActivity(it);
    }

    @Override
    protected SettleUserContract.Presenter createPresenter() {
        return new SettleUserPresenter();
    }

    private void initValue(UserModel userModel) {
        lay_cashier.setVisibility(View.VISIBLE);
        tv_num.setText(userModel.getId() + "");
        tv_name.setText(userModel.getRealname());
        tv_tel.setText(userModel.getPhone());
        tv_department.setText(userModel.getDeptname());

    }


    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settle);

        initView();
        userModel = (UserModel) getIntent().getSerializableExtra("userModel");
        if (userModel != null) {
            initValue(userModel);
            titleBar.setRightButLayVisibleForTotal(true, getString(R.string.btnCancel));
        }
        initValue();
        setLister();
        loadData();

        HandlerManager.registerHandler(HandlerManager.SETTLE_TYPE, handler);
    }


    @Override
    public void getOrderTotalSuccess(@Nullable SettleModel response) {
        if (response != null) {
            if (!StringUtil.isEmptyOrNull(response.getSuccess_total_fee())) {
                et_order_max_money.setText(MainApplication.getInstance().getFeeFh() + response.getSuccess_total_fee() + "");
                tv_succ_count.setText(response.getSuccess_total_count() + getString(R.string.stream_cases));
            } else {
                et_order_max_money.setText(MainApplication.getInstance().getFeeFh() + "0.00");
                tv_succ_count.setText("0 " + getString(R.string.stream_cases));
            }
            if (!StringUtil.isEmptyOrNull(response.getRefund_total_fee())) {
                tv_refund_total.setText(MainApplication.getInstance().getFeeFh() + response.getRefund_total_fee() + "");
                tv_refund_count.setText(response.getRefund_total_count() + getString(R.string.stream_cases));
            } else {
                tv_refund_total.setText(MainApplication.getInstance().getFeeFh() + "0.00");
                tv_refund_count.setText("0 " + getString(R.string.stream_cases));
            }
            if (!StringUtil.isEmptyOrNull(response.getCoupon_total_fee())) {

                tv_wx_card_money.setText(MainApplication.getInstance().getFeeFh() + response.getCoupon_total_fee() + "");
                tv_wx_card.setText(response.getCoupon_total_count() + getString(R.string.stream_cases));
            } else {
                tv_wx_card_money.setText(MainApplication.getInstance().getFeeFh() + "0.00");
                tv_wx_card.setText("0 " + getString(R.string.stream_cases));
            }

            if (!StringUtil.isEmptyOrNull(response.getTotal_net_fee())) {
                tv_net_fee.setText(MainApplication.getInstance().getFeeFh() + response.getTotal_net_fee() + "");
                tv_total_count.setText((StringUtil.paseStrToLong(response.getSuccess_total_count()) - StringUtil.paseStrToLong(response.getRefund_total_count()))
                        + getString(R.string.stream_cases));
            } else {
                tv_net_fee.setText(MainApplication.getInstance().getFeeFh() + "0.00");
                tv_total_count.setText("0 " + getString(R.string.stream_cases));
            }

        }
    }

    @Override
    public void getOrderTotalFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }
        if (error != null) {
            showToastInfo(error.toString());
        }
    }

    private void loadData() {
        String mobile = null;

        if (MainApplication.getInstance().isAdmin(0) && MainApplication.getInstance().isOrderAuth("0")) { //收银员且没有流水权限
            mobile = MainApplication.getInstance().getUserInfo().phone;
            titleBar.setRightButLayVisibleForTotal(false, getString(R.string.tx_user));
        } else {
            if (null != userModel) {
                mobile = userModel.getPhone();
            }
        }

        if (mPresenter != null) {
            mPresenter.getOrderTotal(
                    tv_start_time.getText().toString().replaceAll("/", "-"),
                    tv_end_time.getText().toString().replaceAll("/", "-"),
                    mobile
            );
        }
    }

    private void initValue() {
        tv_start_time.setText(DateUtil.formatsYYYYMD(new Date().getTime()));

        tv_end_time.setText(DateUtil.formatNowTime(new Date().getTime()));
    }

    private void setLister() {
        iv_clear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lay_cashier.setVisibility(View.GONE);
                if (null != userModel) {
                    userModel = null;

                    loadData();
                }
            }
        });

        lay_start_time.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DateTimePickDialogInfo dateTimeDialog =
                        new DateTimePickDialogInfo(SettleUserActivity.this, DateUtil.paseTime(tv_start_time.getText()
                                .toString()), null, R.string.tv_settle_choice_start_time,
                                new DateTimePickDialogInfo.HandleBtn() {

                                    @Override
                                    public void handleOkBtn(String date) {
                                        tv_start_time.setText(date);
                                    }

                                }, null, false);

                DialogHelper.resize(SettleUserActivity.this, dateTimeDialog);
                dateTimeDialog.show();
            }
        });

        lay_end_time.setOnClickListener(new View.OnClickListener()//tv_settle_choice_end_time
        {

            @Override
            public void onClick(View v) {
                DateTimePickDialogInfo dateTimeDialog =
                        new DateTimePickDialogInfo(SettleUserActivity.this, DateUtil.paseTime(tv_end_time.getText()
                                .toString()), tv_start_time.getText().toString(), R.string.tv_settle_choice_end_time,
                                new DateTimePickDialogInfo.HandleBtn() {

                                    @Override
                                    public void handleOkBtn(String date) {
                                        tv_end_time.setText(date);

                                        loadData();
                                    }

                                }, null, false);

                DialogHelper.resize(SettleUserActivity.this, dateTimeDialog);
                dateTimeDialog.show();
            }
        });
    }

    private void initView() {
        //tv_num,tv_name,tv_tel,tv_department
        iv_clear = findViewById(R.id.iv_clear);
        tv_num = findViewById(R.id.tv_num);
        tv_name = findViewById(R.id.tv_name);
        tv_tel = findViewById(R.id.tv_tel);
        tv_department = findViewById(R.id.tv_department);

        lay_cashier = findViewById(R.id.lay_cashier);

        lay_start_time = findViewById(R.id.lay_start_time);
        lay_end_time = findViewById(R.id.lay_end_time);
        tv_start_time = findViewById(R.id.tv_start_time);
        tv_end_time = findViewById(R.id.tv_end_time);

        et_order_max_money = findViewById(R.id.et_order_max_money);
        tv_succ_count = findViewById(R.id.tv_succ_count);
        tv_refund_total = findViewById(R.id.tv_refund_total);
        tv_refund_count = findViewById(R.id.tv_refund_count);
        tv_wx_card_money = findViewById(R.id.tv_wx_card_money);
        tv_wx_card = findViewById(R.id.tv_wx_card);
        tv_net_fee = findViewById(R.id.tv_net_fee);
        tv_total_count = findViewById(R.id.tv_total_count);
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tv_settle);
        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.btnCancel));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButLayClick() {
                finish();
                if (MainApplication.getInstance().getListActivities().size() > 0) {
                    for (Activity activity : MainApplication.getInstance().getListActivities()) {
                        activity.finish();
                    }
                }
            }
        });
    }


}
