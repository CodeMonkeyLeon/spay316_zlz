package cn.swiftpass.enterprise.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cn.swiftpass.enterprise.bussiness.model.DrawerContentModel;
import cn.swiftpass.enterprise.intl.R;

/**
 * Created by aijingya on 2018/4/28.
 *
 * @Package cn.swiftpass.enterprise.ui.adapter
 * @Description: ${TODO}(侧边栏内容填充的adapter)
 * @date 2018/4/28.11:47.
 */

public class DrawerContentAdapter extends BaseAdapter {

    private Context context;
    private List<DrawerContentModel> list;

    public DrawerContentAdapter(Context context, List<DrawerContentModel> list) {
        super();
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (list != null) {
            return list.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHold hold;
        if (convertView == null) {
            hold = new ViewHold();
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_drawer_content_item, null);
            convertView.setTag(hold);
        } else {
            hold = (ViewHold) convertView.getTag();
        }

        hold.imageView = (ImageView) convertView.findViewById(R.id.item_imageview);
        hold.textView = (TextView) convertView.findViewById(R.id.item_textview);

        hold.imageView.setImageResource(list.get(position).getImageView());
        hold.textView.setText(list.get(position).getText());
        return convertView;
    }

    class ViewHold {
        public ImageView imageView;
        public TextView textView;
    }
}
