package cn.swiftpass.enterprise.ui.paymentlink;

import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.CREATE_ORDER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.CREATE_ORDER_SUCCESS;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.DATA_TAG;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.DEFAULT_TAG;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_CREATE_ORDER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_CREATE_ORDER_SUCCESS;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.TYPE_TAG;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Lifecycle;

import com.bigkoo.pickerview.TimePickerView;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.OrderCreateContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.OrderCreatePresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.paymentlink.model.OrderConfig;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkOrder;
import cn.swiftpass.enterprise.ui.widget.SpayTitleView;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogUtils;
import cn.swiftpass.enterprise.utils.KeyBoardUtil;
import cn.swiftpass.enterprise.utils.KotlinUtils;

/**
 * Created by congwei.li on 2021/9/10.
 *
 * @Description: 创建payment link order
 */
public class OrderCreateActivity extends BaseActivity<OrderCreateContract.Presenter> implements OrderCreateContract.View {

    private static final String TAG_PRODUCT_MODE = "ProductMode";
    private static final String TAG_AMOUNT_MODE = "AmountMode";
    @BindView(R.id.tv_product_model)
    TextView tvChooseProduct;
    @BindView(R.id.tv_amount_model)
    TextView tvSetAmount;
    @BindView(R.id.fl_new_order)
    FrameLayout flContent;
    @BindView(R.id.sv_new_order_title)
    SpayTitleView title;
    Fragment fragmentProductMode;
    Fragment fragmentAmountMode;
    FragmentManager fm;
    PaymentLinkOrder order;
    boolean isCreate = true;
    private int requestCode;
    private String INIT_MODE = TAG_PRODUCT_MODE;
    private String currentPage;
    private String label_year, label_month, label_day, label_hours, label_mins, label_seconds;

    public static void startActivity(Context mContext) {
        Intent intent = new Intent(mContext, OrderCreateActivity.class);
        mContext.startActivity(intent);
    }

    public static void startActivity(Context mContext, PaymentLinkOrder order, boolean isCreate) {
        Intent intent = new Intent(mContext, OrderCreateActivity.class);
        intent.putExtra(DATA_TAG, order);
        intent.putExtra("isCreate", isCreate);
        mContext.startActivity(intent);
    }

    public static void startActivityForResult(Activity mContext, int type) {
        Intent intent = new Intent(mContext, OrderCreateActivity.class);
        intent.putExtra(TYPE_TAG, type);
        mContext.startActivityForResult(intent, type);
    }

    @Override
    protected OrderCreateContract.Presenter createPresenter() {
        return new OrderCreatePresenter();
    }

    @Override
    protected boolean isWindowFeatureNoTitle() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        KotlinUtils.INSTANCE.isReStartApp(savedInstanceState, this);
        setContentView(R.layout.activity_order_create);
        ButterKnife.bind(this);

        requestCode = getIntent().getIntExtra(TYPE_TAG, DEFAULT_TAG);
        order = (PaymentLinkOrder) getIntent().getSerializableExtra(DATA_TAG);
        isCreate = getIntent().getBooleanExtra("isCreate", true);
        initLabel();
        initView(savedInstanceState);
    }

    private void initView(Bundle savedInstanceState) {
        initTitle();
        fm = getSupportFragmentManager();

        if (savedInstanceState != null) {
            fragmentProductMode = (FragmentProductMode) fm.getFragment(savedInstanceState, TAG_PRODUCT_MODE);
            fragmentAmountMode = (FragmentAmountMode) fm.getFragment(savedInstanceState, TAG_AMOUNT_MODE);
        } else {
            if (isCreate) {
                fragmentProductMode = new FragmentProductMode(true);
                fragmentAmountMode = new FragmentAmountMode(true);
            } else {
                fragmentProductMode = new FragmentProductMode(order.deepClone(), false);
                fragmentAmountMode = new FragmentAmountMode(order.deepClone(), false);
            }

            if (null != order && !TextUtils.isEmpty(order.totalFee) &&
                    (null == order.orderRelateGoodsList ||
                            order.orderRelateGoodsList.size() <= 0)) {
                INIT_MODE = TAG_AMOUNT_MODE;
            } else {
                INIT_MODE = TAG_PRODUCT_MODE;
            }
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            addFragment(fragmentTransaction, fragmentProductMode, TAG_PRODUCT_MODE);
            addFragment(fragmentTransaction, fragmentAmountMode, TAG_AMOUNT_MODE);
            fragmentTransaction.commitNow();
        }
        setProductMode(INIT_MODE.equals(TAG_PRODUCT_MODE));
        requestOrderConfig();
    }

    private void initTitle() {
        title.addMiddleTextView(requestCode == CREATE_ORDER ? getString(R.string.payment_link_add_order)
                : getString(R.string.pl_edit_order_title));
        title.setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
        title.addLeftImageView(R.drawable.icon_general_top_back_default, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            if (TextUtils.isEmpty(currentPage)) {
                return getWindow().superDispatchTouchEvent(ev) || onTouchEvent(ev);
            }
            View v = OrderCreateActivity.this.getCurrentFocus();
            if (KeyBoardUtil.isShouldHideInput(v, ev)) {//点击editText控件外部
                InputMethodManager imm = (InputMethodManager) OrderCreateActivity.this
                        .getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    KeyBoardUtil.hideKeyboard(v);//软键盘工具类
                    if (flContent != null) {
                        flContent.clearFocus();
                    }
                }
            }
        }
        return getWindow().superDispatchTouchEvent(ev) || onTouchEvent(ev);
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        //当前的界面的保存状态，只是从新让新的Fragment指向了原本未被销毁的fragment，
        // 它就是onAttach方法对应的Fragment对象
        if (fragmentProductMode == null && fragment instanceof FragmentProductMode) {
            fragmentProductMode = fragment;
        } else if (fragmentAmountMode == null && fragment instanceof FragmentAmountMode) {
            fragmentAmountMode = fragment;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideAllFragment();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (TAG_PRODUCT_MODE.equals(currentPage)) {
            showFragment(fm.beginTransaction(), fragmentProductMode);
            setProductMode(true);
        } else {
            showFragment(fm.beginTransaction(), fragmentAmountMode);
            setProductMode(false);
        }
    }

    private void hideAllFragment() {
        FragmentTransaction transaction = fm.beginTransaction();
        List<Fragment> fragments = fm.getFragments();
        for (Fragment fragment1 : fragments) {
            transaction.hide(fragment1);
            transaction.setMaxLifecycle(fragment1, Lifecycle.State.STARTED);
        }
        transaction.commitNowAllowingStateLoss();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearFragment();
    }

    public void clearFragment() {
        fragmentProductMode = null;
        fragmentAmountMode = null;
    }

    @OnClick({R.id.tv_product_model, R.id.tv_amount_model})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_product_model:
                setProductMode(true);
                showFragment(fm.beginTransaction(), fragmentProductMode);
                break;
            case R.id.tv_amount_model:
                setProductMode(false);
                showFragment(fm.beginTransaction(), fragmentAmountMode);
                break;
        }
    }

    private void addFragment(FragmentTransaction transaction, Fragment fragment, String tag) {
        Fragment old = fm.findFragmentByTag(tag);
        if (old == null) {
            old = fragment;
            transaction.add(R.id.fl_new_order, fragment, tag);
        }
        if (!INIT_MODE.equals(tag)) {
            transaction.hide(old);
            transaction.setMaxLifecycle(fragment, Lifecycle.State.STARTED);
        } else {
            currentPage = tag;
            transaction.setMaxLifecycle(fragment, Lifecycle.State.RESUMED);
        }
    }

    private void showFragment(FragmentTransaction transaction, Fragment fragment) {
        transaction.show(fragment);
        transaction.setMaxLifecycle(fragment, Lifecycle.State.RESUMED);
        currentPage = fragment.getTag();
        List<Fragment> fragments = fm.getFragments();
        for (Fragment fragment1 : fragments) {
            if (fragment != fragment1) {
                transaction.hide(fragment1);
                transaction.setMaxLifecycle(fragment1, Lifecycle.State.STARTED);
            }
        }
        transaction.commitNowAllowingStateLoss();
    }

    private void setProductMode(boolean isChooseP) {
        tvChooseProduct.setSelected(isChooseP);
        tvSetAmount.setSelected(!isChooseP);
    }

    public void showDateSelect(String effectiveDate, TimePickerView.OnTimeSelectListener listener) {
        Calendar calendar = Calendar.getInstance();
        Date selectDate = DateUtil.getStrToDate(effectiveDate);
        if (null != selectDate) {
            calendar.setTime(selectDate);
        }
        TimePickerView pvTime = new TimePickerView.Builder(this, listener)
                .setCancelText(getString(R.string.data_select_cancel))//取消按钮文字
                .setSubmitText(getString(R.string.data_select_done))//确认按钮文字
                .setTitleText(getString(R.string.data_title))//标题文字
                .setRange(2000, 2099)
                .setLabel(label_year, label_month, label_day, label_hours, label_mins, label_seconds)
                .isCenterLabel(false)
                .setLanguage(DateUtil.isEnglish())
                .setDate(calendar)
                .build();
        pvTime.show();
    }

    private void initLabel() {
        label_year = getString(R.string.pickerview_year);
        label_month = getString(R.string.pickerview_month);
        label_day = getString(R.string.pickerview_day);
        label_hours = getString(R.string.pickerview_hours);
        label_mins = getString(R.string.pickerview_minutes);
        label_seconds = getString(R.string.pickerview_seconds);
    }


    @Override
    public void getOrderConfigSuccess(@NonNull OrderConfig response) {
        DialogUtils.dismissDialog(OrderCreateActivity.this);
        if (null != fragmentProductMode && null == order) {
            ((FragmentProductMode) fragmentProductMode).setOrderConfig(response);
        }
        if (null != fragmentAmountMode && null == order) {
            ((FragmentAmountMode) fragmentAmountMode).setOrderConfig(response);
        }
    }

    @Override
    public void getOrderConfigFailed(@Nullable Object error) {
        DialogUtils.dismissDialog(OrderCreateActivity.this);
        if (null != fragmentProductMode && null == order) {
            ((FragmentProductMode) fragmentProductMode).setOrderConfig(null);
        }
        if (null != fragmentAmountMode && null == order) {
            ((FragmentAmountMode) fragmentAmountMode).setOrderConfig(null);
        }
    }

    private void requestOrderConfig() {
        if (mPresenter != null) {
            mPresenter.getOrderConfig();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CREATE_ORDER_SUCCESS && resultCode == RESULT_CODE_CREATE_ORDER_SUCCESS) {
            Intent i = new Intent();
            OrderCreateActivity.this.setResult(RESULT_CODE_CREATE_ORDER, i);
            if (!OrderCreateActivity.this.isFinishing()) {
                OrderCreateActivity.this.finish();
            }
        }
    }
}
