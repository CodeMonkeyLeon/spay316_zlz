package cn.swiftpass.enterprise.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnLoadListener;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnMenuItemClickListener;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnRefreshListener;
import com.tencent.stat.StatService;
import java.util.ArrayList;
import java.util.List;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.user.CashierAddActivity;
import cn.swiftpass.enterprise.ui.activity.user.RefundManagerActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DataReportUtils;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.DisplayUtil;

public class CashierManager extends TemplateActivity implements OnRefreshListener, OnLoadListener {
    private static final String TAG = CashierManager.class.getSimpleName();
    
    private ViewHolder holder;
    
    private SwipeMenuListView listView;

    private List<UserModel> listUser;
    
    private Handler mHandler = new Handler();
    
    private CashierAdapter cashierAdapter;
    
    private int pageFulfil = 0;
    
    private int pageCount = 0;
    
    private EditText et_input;
    
    private DialogInfo dialogInfo;
    
    private TextView ly_cashier_no;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cashier_manager);

        listUser = new ArrayList<UserModel>();
        cashierAdapter = new CashierAdapter(listUser);

        listView = getViewById(R.id.listView);
        listView.setAdapter(cashierAdapter);
        
        initView();
        
        et_input = getViewById(R.id.et_input);
        et_input.setFocusable(true);
        et_input.setFocusableInTouchMode(true);
        et_input.requestFocus();
        et_input.setOnTouchListener(new OnTouchListener()
        {
            
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    try {
                        StatService.trackCustomEvent(CashierManager.this, "kMTASPayMeCashierSearch", "收银员管理搜索入口");
                    } catch (Exception e) {
                        Log.e(TAG,Log.getStackTraceString(e));
                    }

                    // 1.携带参数的打点
                    Bundle value1 = new Bundle();
                    value1.putString("kGFASPayMeCashierSearch","收银员管理搜索入口");
                    DataReportUtils.getInstance().report("kGFASPayMeCashierSearch",value1);

                    Bundle b = new Bundle();
                    b.putBoolean("isFromOrderStreamSearch", true);
                    b.putInt("serchType", 2); //从收银员列表查询
                    showPage(RefundManagerActivity.class, b);
                }
                return true;
            }
        });


        listView.setOnItemClickListener(new OnItemClickListener()
        {
            
            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3)
            {
                try
                {
                    UserModel userModel = listUser.get(position - 1);
                    if (userModel != null)
                    {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("userModel", userModel);
                        showPage(CashierAddActivity.class, bundle);
                    }
                    
                }
                catch (Exception e)
                {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
        });

        pageFulfil = 0;
        loadData(pageFulfil, false, SwipeMenuListView.REFRESH);
    }
    
    private void initView()
    {
        ly_cashier_no = getViewById(R.id.ly_cashier_no);
        
        SwipeMenuCreator creator = new SwipeMenuCreator() {
            
            @Override
            public void create(SwipeMenu menu)
            {
                SwipeMenuItem openItem = new SwipeMenuItem(getApplicationContext());
                openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9, 0xCE)));
                openItem.setWidth(DisplayUtil.dip2Px(CashierManager.this, 90));
                openItem.setTitle(R.string.bt_delete);
                openItem.setTitleSize(18);
                openItem.setTitleColor(Color.WHITE);
                openItem.setBackground(new ColorDrawable(Color.RED));
                menu.addMenuItem(openItem);
            }
        };
        
        listView.setMenuCreator(creator);
        listView.setOnRefreshListener(this);
        listView.setOnLoadListener(this);
        listView.setOnMenuItemClickListener(new OnMenuItemClickListener()
        {
            @Override
            public void onMenuItemClick(int position, SwipeMenu menu, int index)
            {
                
                switch (index)
                {
                    case 0:
                        UserModel userModel = listUser.get(position);
                        if (userModel != null)
                        {
                            delete(userModel.getId(), position);
                        }
                        break;
                }
            }
        });
    }
    
    void delete(final long userId, final int position)
    {
        
        dialogInfo = new DialogInfo(CashierManager.this, getString(R.string.public_cozy_prompt),
                getString(R.string.dialog_delete), getString(R.string.btnOk), getString(R.string.btnCancel),
                DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn()
                {
                    @Override
                    public void handleOkBtn()
                    {
                        UserManager.cashierDelete(userId, new UINotifyListener<Boolean>()
                        {
                            @Override
                            public void onPreExecute()
                            {
                                super.onPreExecute();
                                
                                showLoading(false, getString(R.string.show_delete_loading));
                            }
                            
                            @Override
                            public void onError(final Object object)
                            {
                                super.onError(object);
                                dismissLoading();
                                if (object != null)
                                {
                                    toastDialog(CashierManager.this, object.toString(), null);
                                }
                            }
                            
                            @Override
                            public void onSucceed(Boolean result)
                            {
                                super.onSucceed(result);
                                dismissLoading();
                                
                                if (result)
                                {
                                    toastDialog(CashierManager.this,
                                        R.string.show_delete_succ,
                                        new NewDialogInfo.HandleBtn()
                                        {
                                            
                                            @Override
                                            public void handleOkBtn()
                                            {
                                                listUser.remove(position);
                                                cashierAdapter.notifyDataSetChanged();
                                            }
                                        });
                                }
                                
                            }
                        });
                    }
                    
                    @Override
                    public void handleCancleBtn()
                    {
                        dialogInfo.cancel();
                    }
                }, null);
        
        DialogHelper.resize(CashierManager.this, dialogInfo);
        dialogInfo.show();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        //重新刷新界面
        pageFulfil = 0;
        loadData(pageFulfil, false, SwipeMenuListView.REFRESH);
    }

    @Override
    protected void onResume()
    {
        // TODO Auto-generated method stub
        super.onResume();

    }

    
    /** <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void loadData(final int page, final boolean isLoadMore, final int Type)
    {
        
        UserManager.queryCashier(page, 10, null, new UINotifyListener<List<UserModel>>()
        {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                showLoading(false, R.string.public_data_loading);
            }
            
            @Override
            public void onError(final Object object) {
                super.onError(object);
                dismissLoading();
                if (checkSession()) {
                    return;
                }
                if (object != null) {
                    toastDialog(CashierManager.this, object.toString(), new NewDialogInfo.HandleBtn()
                    {
                        
                        @Override
                        public void handleOkBtn()
                        {
                            if (listUser.size() == 0)
                            {
                                finish();
                            }
                        }
                        
                    });
                }
            }
            
            @Override
            public void onSucceed(List<UserModel> result)
            {
                super.onSucceed(result);
                dismissLoading();
                
                if (result != null && result.size() > 0) {
                    listView.setVisibility(View.VISIBLE);
                    pageCount = result.get(0).getPageCount();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run()
                        {
                            ly_cashier_no.setVisibility(View.GONE);
                            listView.setVisibility(View.VISIBLE);
                        }
                    });

                    if (Type == SwipeMenuListView.REFRESH)
                    {
                        listUser.clear();
                        listUser.addAll(result);
                        cashierAdapter.notifyDataSetChanged();
                        listView.onRefreshComplete();
                        listView.onLoadComplete();
                    } else if(Type == SwipeMenuListView.LOAD){
                        listUser.addAll(result);
                        cashierAdapter.notifyDataSetChanged();
                        listView.onRefreshComplete();
                        listView.onLoadComplete();
                    }
                    if(page == 0){
                        listView.setSelection(0);
                    }
                } else {
                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            if (listUser.size() == 0) {
                                ly_cashier_no.setVisibility(View.VISIBLE);
                                ly_cashier_no.setText(R.string.tv_user_list);
                                listView.setVisibility(View.GONE);
                                listView.setResultSize(listUser.size());
                            }
                        }
                    });
                }
            }
        });
        
    }

    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setTitle(R.string.tx_user_manager);
        titleBar.setLeftButtonVisible(true);
        titleBar.setRightButtonVisible(false);
        titleBar.setRightButLayVisible(true, 0);
        titleBar.setRightButLayBackground(R.drawable.icon_add);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                try {
                    StatService.trackCustomEvent(CashierManager.this, "kMTASPayMeCashierAdd", "添加收银员按钮");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                // 1.携带参数的打点
                Bundle value1 = new Bundle();
                value1.putString("kGFASPayMeCashierAdd","添加收银员按钮");
                DataReportUtils.getInstance().report("kGFASPayMeCashierAdd",value1);

                showPage(CashierAddActivity.class);
            }
            
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
        });
    }
    
    private class CashierAdapter extends BaseAdapter
    {
        
        private List<UserModel> userModels;
        
        public CashierAdapter()
        {
        }
        
        public CashierAdapter(List<UserModel> userModels)
        {
            this.userModels = userModels;
        }
        
        @Override
        public int getCount()
        {
            return userModels.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            return userModels.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(CashierManager.this, R.layout.cashier_list, null);
                holder = new ViewHolder();
                holder.cashier_state = (TextView)convertView.findViewById(R.id.cashier_state);
//                holder.cashier_store = (TextView)convertView.findViewById(R.id.cashier_store);
                holder.user_account = (TextView)convertView.findViewById(R.id.user_account);
                holder.userId = (TextView)convertView.findViewById(R.id.userId);
                holder.userName = (TextView)convertView.findViewById(R.id.userName);
                holder.usertel = (TextView)convertView.findViewById(R.id.usertel);
//                holder.v_line = convertView.findViewById(R.id.v_line);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            
            UserModel userModel = userModels.get(position);

            holder.userId.setText(getResources().getString(R.string.cash_number)+": "+String.valueOf(userModel.getId()));
            if (userModel.getPhone() != null)
            {
                holder.usertel.setText(userModel.getPhone());
            }
            
            if (userModel.getRealname() != null)
            {
                holder.userName.setText(getResources().getString(R.string.cashier)+": "+userModel.getRealname());
            }
            if(userModel.getUsername() != null){
                holder.user_account.setText(getResources().getString(R.string.cashier_account)+": "+userModel.getUsername());
            }
            
            return convertView;
        }
    }
    
    private class ViewHolder
    {
        private TextView userId, cashier_state, userName, usertel ,user_account;
        
    }
    
    @Override
    public void onLoad()
    {
        pageFulfil = pageFulfil + 1;
        if (pageFulfil < pageCount)
        {
            loadData(pageFulfil, true, SwipeMenuListView.LOAD);
        }
    }
    
    @Override
    public void onRefresh()
    {
        pageFulfil = 0;
        loadData(pageFulfil, false, SwipeMenuListView.REFRESH);
    }
    
}
