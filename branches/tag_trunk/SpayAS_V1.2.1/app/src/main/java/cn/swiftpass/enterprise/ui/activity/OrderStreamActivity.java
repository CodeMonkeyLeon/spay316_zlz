package cn.swiftpass.enterprise.ui.activity;

import java.util.Calendar;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.enums.OrderStatusEnum;
import cn.swiftpass.enterprise.bussiness.enums.PayType;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.OrderSearchResult;
import cn.swiftpass.enterprise.ui.activity.OrderStreamPayModeDialog.SelectListener;
import cn.swiftpass.enterprise.ui.adapter.OrderAdapter;
import cn.swiftpass.enterprise.ui.widget.PullDownListView;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * User: 流水 Date: 13-9-27 Time: 下午7:33
 */
public class OrderStreamActivity extends TemplateActivity implements View.OnClickListener,
    AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, PullDownListView.OnRefreshListioner
{
    
    private OrderAdapter adapter;
    
    private Context mContext;
    
    private boolean isDown = true;
    
    private int currentPage = 0;
    
    private long searchTime;
    
    private List<Order> data;// 已收款
    
    private List<Order> list;// 待收款
    
    private TextView tvDateTime;
    
    private Button btnLeft, btnRight;
    
    private EditText etOrderNo;
    
    private OrderStatusEnum orderState;
    
    private TextView tvSearch;
    
    private RelativeLayout llDateTime, ll_datetime;
    
    private TextView tvTotal, tvTotalTitle;
    
    private ImageView ivTotalLine, ivLeftBlueLine, ivRightBlueLine;;
    
    private boolean isSearchButton = false;
    
    private OrderStreamPayModeDialog modeDialog;
    
    private Integer couponId;
    
    private int transactionType = 0;
    
    private PullDownListView mPullDownView;
    
    private ListView mListView;
    
    private TextView tvInfo;
    
    private int maxAount = 100;// 设置了最大数据值
    
    private Handler mHandler = new Handler();
    
    private LayoutInflater inflater;
    
    private View view;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_order_stream);
        initViews(); // 暂时屏蔽掉
    }
    
    private void initViews()
    {
        tvDateTime = getViewById(R.id.tv_datetime_value);
        ivTotalLine = getViewById(R.id.iv_total_line);
        ivLeftBlueLine = (ImageView)getViewById(R.id.iv_left_blue_line);
        ivRightBlueLine = (ImageView)getViewById(R.id.iv_right_blue_line);
        
        tvSearch = getViewById(R.id.tv_search);
        tvSearch.setOnClickListener(this);
        btnLeft = getViewById(R.id.btn_left);
        btnLeft.setOnClickListener(this);
        btnRight = getViewById(R.id.btn_right);
        btnRight.setOnClickListener(this);
        etOrderNo = getViewById(R.id.et_orderNo);
        llDateTime = getViewById(R.id.ll_datetime);
        llDateTime.setOnClickListener(this);
        mPullDownView = (PullDownListView)findViewById(R.id.list);
        mPullDownView.setRefreshListioner(this);
        mListView = mPullDownView.mListView;
        
        adapter = new OrderAdapter(this);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(this);
        mListView.setOnItemLongClickListener(this);
        
        adapter.setListView(mListView);
        tvInfo = getViewById(R.id.tv_info);
        ll_datetime = getViewById(R.id.ll_datetime);
        ll_datetime.setOnClickListener(this);
        searchTime = System.currentTimeMillis();
        // titleBar.setTitle(R.string.title_order_pay_list); //有pos打开
        orderState = OrderStatusEnum.PAY_SUCCESS;
        
        tvTotal = getViewById(R.id.tv_total);
        tvTotalTitle = getViewById(R.id.tv_total_title);
        
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        {
            
            @Override
            public void onExecute(CharSequence s, int start, int before, int count)
            {
            }
            
            @Override
            public void onAfterTextChanged(Editable s)
            {
                if (s != null && s.length() >= 1)
                {
                    tvSearch.setVisibility(View.VISIBLE);
                }
                else
                {
                    tvSearch.setVisibility(View.GONE);
                }
                
            }
        });
        etOrderNo.addTextChangedListener(editTextWatcher);
        loadMoreDate(true);
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(false);
        titleBar.setTitle(R.string.title_order_pay_list);
        /*
         * titleBar.setOnCentreTitleBarClickListener(new
         * OnCentreTitleBarClickListener() {
         * 
         * @Override public void onCentreButtonClick(View v) { showPayDialog(v);
         * } });
         */
    }
    
    /***
     * 查询服务器数据
     */
    private void loadMoreDate(boolean isFirst)
    {
        @SuppressWarnings("unused")
        final boolean isFirstRef = isFirst;
        // if (isFirst)
        // {
        // // isFirst = false;
        // // if (orderState == OrderStatusEnum.PAY_SUCCESS)
        // // {
        // // if (data != null)
        // // data.clear();
        // // }
        // // else
        // // {
        // // if (list != null)
        // // list.clear();
        // // }
        // }
        // currentPage=currentPage-rowCount%ApiConstant.PAGE_SIZE; //每次获取5条
        if (orderState == OrderStatusEnum.PAY_SUCCESS)
        {
            if (data != null)
            {
                currentPage = data.size();
            }
        }
        else
        {
            if (list != null)
            {
                currentPage = list.size();
            }
        }
        // runOnUiThread(new Runnable()
        // {
        // @Override
        // public void run()
        // {
        //
        // }
        // });
        
        search(orderState, transactionType, isFirst);
    }
    
    public View getView()
    {
        this.inflater = LayoutInflater.from(mContext);
        view = inflater.inflate(R.layout.activity_order_stream, null);
        return view;
    }
    
    private void search(final OrderStatusEnum orderState, int transactionType, final Boolean isLoding)
    {
        // UserModel userModel =
        // LocalAccountManager.getInstance().getLoggedUser();
        
        if (!TextUtils.isEmpty(MainApplication.getMchId()))
        {
            String orderNo = "";
            // Integer state = OrderStatusEnum.PAY_SUCCESS.getValue() ;
            if (isSearchButton == true)
            {
                isSearchButton = false;
                orderNo = etOrderNo.getText().toString();
            }
            
            OrderManager.getInstance().queryOrderData(orderNo,
                currentPage,
                orderState.getValue(),
                DateUtil.formatYYMD(searchTime),
                transactionType,
                null,
                new UINotifyListener<OrderSearchResult>()
                {
                    @Override
                    public void onPostExecute()
                    {
                        super.onPostExecute();
                        // dismissLoading();
                    }
                    
                    @Override
                    public void onSucceed(OrderSearchResult result)
                    {
                        super.onSucceed(result);
                        titleBar.setRightLodingVisible(false, false);
                        dismissLoading();
                        if (result != null && result.orders != null && result.orders.size() > 0)
                        {
                            mPullDownView.setVisibility(View.VISIBLE);
                            
                            tvTotal.setText("¥ " + DateUtil.formatMoneyUtil(result.total / 100d));
                            tvInfo.setVisibility(View.GONE);
                            
                            if (orderState == OrderStatusEnum.PAY_SUCCESS)
                            {
                                if (data == null)
                                {
                                    if (result != null)
                                        data = result.orders;
                                }
                                else
                                {
                                    if (result != null)
                                    {
                                        // 首先清楚缓存，然后再添加
                                        data.clear();
                                        data.addAll(result.orders);
                                    }
                                    
                                }
                                adapter.setList(data);
                            }
                            else
                            {
                                if (list == null)
                                {
                                    if (result != null)
                                        list = result.orders;
                                }
                                else
                                {
                                    if (result != null)
                                    {
                                        list.clear();
                                        list.addAll(result.orders);
                                    }
                                }
                                adapter.setList(list);
                            }
                            mPullDownView.onfinish("刷新成功");// 刷新完成
                            mHandler.postDelayed(new Runnable()
                            {
                                
                                public void run()
                                {
                                    // 这里表示刷新处理完成后把上面的加载刷新界面隐藏
                                    mPullDownView.onRefreshComplete();
                                }
                            }, 500);
                        }
                        else
                        {
                            mPullDownView.onfinish("刷新失败");// 刷新完成
                            mHandler.postDelayed(new Runnable()
                            {
                                
                                public void run()
                                {
                                    // 这里表示刷新处理完成后把上面的加载刷新界面隐藏
                                    mPullDownView.onRefreshComplete();
                                }
                            }, 500);
                            mPullDownView.setVisibility(View.GONE);
                            //                            tvInfo.setText(R.string.msg_no_order_info);
                            tvInfo.setVisibility(View.VISIBLE);
                            tvTotal.setText("¥ 0.00");
                        }
                        
                        // mListView.post(new Runnable()
                        // {
                        // @Override
                        // public void run()
                        // { // 自动滚动第一个
                        if (mListView != null)
                        {
                            mListView.setSelection(0);
                        }
                        
                        // // mPullDownView.setMore(false);
                        //
                        // }
                        // });
                    }
                    
                    @Override
                    public void onPreExecute()
                    {
                        super.onPreExecute();
                        // showLoading(true, "请稍候！ ");
                        if (isLoding)
                        {
                            titleBar.setRightLodingVisible(true);
                        }
                        // showLoading("正在查询订单,请稍等...");
                    }
                    
                    @Override
                    public void onError(Object object)
                    {
                        super.onError(object);
                        dismissLoading();
                        if (object != null)
                        {
                            showToastInfo(object.toString());
                        }
                        
                    }
                });
            
        }
        else
        {
            //            tvInfo.setText(R.string.msg_no_order_null);
            tvInfo.setVisibility(View.VISIBLE);
        }
    }
    
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
    {
        if (orderState == OrderStatusEnum.PAY_SUCCESS)
        {
            if (data.size() > 0)
            {
                Order o = data.get(i - 1);
                OrderDetailsActivity.startActivity(mContext, o);
            }
        }
        else
        {
            if (list.size() > 0)
            {
                Order o = list.get(i - 1);
                OrderDetailsActivity.startActivity(mContext, o);
            }
        }
        
    }
    
    public static void startActivity(Context context, boolean isAll)
    {
        Intent it = new Intent();
        it.setClass(context, OrderStreamActivity.class);
        it.putExtra("isAll", isAll);
        context.startActivity(it);
        
    }
    
    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l)
    {
        if (orderState == OrderStatusEnum.PAY_SUCCESS)
        {
            Order o = data.get(i - 1);
            AppHelper.copy(o.orderNo, mContext);
            ToastHelper.showInfo("内容已复制！");
            return false;
        }
        else
        {
            Order o = list.get(i - 1);
            AppHelper.copy(o.orderNo, mContext);
            ToastHelper.showInfo("内容已复制！");
            return false;
        }
    }
    
    @SuppressLint("ResourceAsColor")
    @Override
    public void onClick(View view)
    {
        int color = Color.parseColor("#676767");
        int color1 = Color.parseColor("#4197D2");
        /*
         * ivRightBlueLine.setVisibility(View.GONE);
         * btnLeft.setBackgroundResource(R.drawable.n_button_bg03);
         */
        switch (view.getId())
        {
            case R.id.ll_datetime:
            {
                
                showDatetimeDialog();
                
                break;
            }
            case R.id.btn_left:
            {
                // 已经收款
                btnLeft.setClickable(false);
                btnRight.setClickable(true);
                tvTotalTitle.setTextColor(Color.parseColor("#333333"));
                tvTotal.setTextColor(Color.parseColor("#EE1111"));
                orderState = OrderStatusEnum.PAY_SUCCESS;
                btnRight.setBackgroundResource(R.drawable.n_button_bg_04);
                btnRight.setTextColor(color);
                btnLeft.setBackgroundResource(R.drawable.n_button_bg03);
                btnLeft.setTextColor(color1);
                ivLeftBlueLine.setVisibility(View.VISIBLE);
                ivRightBlueLine.setVisibility(View.GONE);
                currentPage = 0;
                loadMoreDate(true);
                break;
            }
            case R.id.btn_right:
            {
                btnLeft.setClickable(true);
                btnRight.setClickable(false);
                // 待收款
                tvTotal.setTextColor(Color.parseColor("#dddddd"));
                tvTotalTitle.setTextColor(Color.parseColor("#dddddd"));
                tvTotal.setText("");
                orderState = OrderStatusEnum.PAY_FAILL;
                btnRight.setBackgroundResource(R.drawable.n_button_bg03);
                btnRight.setTextColor(color1);
                btnLeft.setBackgroundResource(R.drawable.n_button_bg_04);
                btnLeft.setTextColor(color);
                ivRightBlueLine.setVisibility(View.VISIBLE);
                ivLeftBlueLine.setVisibility(View.GONE);
                currentPage = 0;
                loadMoreDate(true);
                break;
            }
            case R.id.tv_search:
                isSearchButton = true;
                currentPage = 0;
                loadMoreDate(true);
                break;
        
        }
        
    }
    
    private Calendar c = null;
    
    public void showDatetimeDialog()
    {
        c = Calendar.getInstance();
        c.setTimeInMillis(searchTime);
        dialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener()
        {
            public void onDateSet(DatePicker dp, int year, int month, int dayOfMonth)
            {
                // String d = DateUtil.formatTime(datetime, "yyyy-MM-dd") ;
                // c.set(Calendar.DAY_OF_YEAR,);
                c.set(year, month, dayOfMonth);
                searchTime = c.getTimeInMillis();
                System.out.println(DateUtil.formatYMD(c.getTime()));
                if (DateUtil.isSameDay(System.currentTimeMillis(), searchTime))
                {
                    tvDateTime.setText("今天");
                }
                else
                {
                    tvDateTime.setText(year + "年" + (month + 1) + "月" + dayOfMonth + "日");
                }
                // tvItemTime.setText(d);
                currentPage = 0;
                loadMoreDate(true);
                
            }
        }, c.get(Calendar.YEAR), // 传入年份
            c.get(Calendar.MONTH), // 传入月份
            c.get(Calendar.DAY_OF_MONTH) // 传入天数*/
            );
        dialog.show();
    }
    
    public List<Order> removeDuplicate(List<Order> list)
    {
        for (int i = 0; i < list.size() - 1; i++)
        {
            for (int j = list.size() - 1; j > i; j--)
            {
                if (list.get(j).id == (list.get(i)).id)
                {
                    list.remove(j);
                }
            }
        }
        return list;
    }
    
    @Override
    protected void onResume()
    {
        super.onResume();
        loadMoreDate(true);
    }
    
    private void showPayDialog(View v)
    {
        if (modeDialog != null && modeDialog.isShowing())
        {
            v.setBackgroundResource(R.drawable.icon_arrow_04);
            modeDialog.dismiss();
        }
        else
        {
            v.setBackgroundResource(R.drawable.icon_arrow_03);
            modeDialog = new OrderStreamPayModeDialog(mContext, R.style.MyDialogStyleTop);
            modeDialog.show();
            modeDialog.setmListener(new SelectListener()
            {
                
                @Override
                public void okSelect(View v)
                {
                    switch (v.getId())
                    {
                        case R.id.ll_dialog_pay:
                            titleBar.setTitle("微信支付");
                            transactionType = PayType.PAY_WX.getValue();
                            break;
                        case R.id.ll_dialog_pos:
                            titleBar.setTitle("pos支付");
                            transactionType = PayType.PAY_POS.getValue();
                            break;
                        case R.id.ll_dialog_all:
                            titleBar.setTitle("全部支付");
                            transactionType = 3;
                            break;
                    }
                    search(orderState, transactionType, true);
                    modeDialog.dismiss();
                }
            });
        }
    }
    
    @Override
    public void onRefresh()
    {
        // mHandler.postDelayed(new Runnable()
        // {
        //
        // public void run()
        // {
        currentPage = 0;
        loadMoreDate(false);
        // 这里表示刷新处理完成后把上面的加载刷新界面隐藏
        // mPullDownView.setMore(true);//这里设置true表示还有更多加载，设置为false底部将不显示更多
        // }
        // }, 1500);
        
    }
    
    @Override
    public void onLoadMore()
    {
        mHandler.postDelayed(new Runnable()
        {
            public void run()
            {
                // loadMoreDate(false);
                // mPullDownView.onLoadMoreComplete();//这里表示加载更多处理完成后把下面的加载更多界面（隐藏或者设置字样更多）
                /*
                 * if (data.size() <
                 * maxAount)//判断当前list中已添加的数据是否小于最大值maxAount，是那么久显示更多否则不显示
                 * mPullDownView
                 * .setMore(true);//这里设置true表示还有更多加载，设置为false底部将不显示更多 else
                 * mPullDownView.setMore(false);
                 */
            }
        }, 1500);
        
    }
    
}
