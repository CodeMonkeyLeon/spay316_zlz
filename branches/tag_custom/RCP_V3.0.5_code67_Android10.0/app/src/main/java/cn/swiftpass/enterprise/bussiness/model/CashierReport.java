package cn.swiftpass.enterprise.bussiness.model;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-11-18
 * Time: 下午3:40
 * To change this template use File | Settings | File Templates.
 */
public class CashierReport
{
    public String addTime;
    
    public Integer tradeNum;
    
    public Long turnover;
    
    public Integer refundNum;
    
    /**
     * @return 返回 addTime
     */
    public String getAddTime()
    {
        return addTime;
    }
    
    /**
     * @param 对addTime进行赋值
     */
    public void setAddTime(String addTime)
    {
        this.addTime = addTime;
    }
    
    /**
     * @return 返回 tradeNum
     */
    public Integer getTradeNum()
    {
        return tradeNum;
    }
    
    /**
     * @param 对tradeNum进行赋值
     */
    public void setTradeNum(Integer tradeNum)
    {
        this.tradeNum = tradeNum;
    }
    
    /**
     * @return 返回 turnover
     */
    public Long getTurnover()
    {
        return turnover;
    }
    
    /**
     * @param 对turnover进行赋值
     */
    public void setTurnover(Long turnover)
    {
        this.turnover = turnover;
    }
    
    /**
     * @return 返回 refundNum
     */
    public Integer getRefundNum()
    {
        return refundNum;
    }
    
    /**
     * @param 对refundNum进行赋值
     */
    public void setRefundNum(Integer refundNum)
    {
        this.refundNum = refundNum;
    }
    
    /**
     * @return 返回 refundFee
     */
    public Long getRefundFee()
    {
        return refundFee;
    }
    
    /**
     * @param 对refundFee进行赋值
     */
    public void setRefundFee(Long refundFee)
    {
        this.refundFee = refundFee;
    }
    
    /**
     * @return 返回 streamTotabeans
     */
    public List<StreamTotabean> getStreamTotabeans()
    {
        return streamTotabeans;
    }
    
    /**
     * @param 对streamTotabeans进行赋值
     */
    public void setStreamTotabeans(List<StreamTotabean> streamTotabeans)
    {
        this.streamTotabeans = streamTotabeans;
    }
    
    public Long refundFee;
    
    private List<StreamTotabean> streamTotabeans;
    
    //    public String userId;
}
