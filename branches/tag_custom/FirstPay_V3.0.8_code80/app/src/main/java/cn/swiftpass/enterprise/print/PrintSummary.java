package cn.swiftpass.enterprise.print;

import android.content.Context;
import android.text.TextUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalItemInfo;
import cn.swiftplus.enterprise.printsdk.print.PrintClient;

/**
 * Created by aijingya on 2020/8/19.
 *
 * @Package cn.swiftpass.enterprise.print
 * @Description:
 * @date 2020/8/19.17:33.
 */
public class PrintSummary {

    public static void PrintSummaryText(Context context,OrderTotalInfo summaryOrderInfo){

        //打印开始前，先bindService
        PrintClient.getInstance().bindDeviceService();

        PrintClient.getInstance().printTitle(context.getString(R.string.tx_blue_print_data_sum));
        PrintClient.getInstance().printTextLeft(context.getString(R.string.shop_name) + "：");
        if (!StringUtil.isEmptyOrNull(MainApplication.getMchName())) {
            PrintClient.getInstance().printTextLeft(MainApplication.getMchName());
        }
        PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "："+ MainApplication.getMchId());
        if (!StringUtil.isEmptyOrNull(summaryOrderInfo.getUserName())) {
            PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_user) + "：");
            PrintClient.getInstance().printTextLeft(summaryOrderInfo.getUserName());
        }
        PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_start_time) + "：" + summaryOrderInfo.getStartTime());
        PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_end_time) + "：" + summaryOrderInfo.getEndTime());
        PrintClient.getInstance().printDoubleLine();

        if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
            PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_bill_stream_pay_money) + "：" + DateUtil.formatMoneyUtils(summaryOrderInfo.getCountTotalFee()) + ToastHelper.toStr(R.string.pay_yuan));
        } else {
            PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_bill_stream_pay_money) + "：" + MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(summaryOrderInfo.getCountTotalFee()));
        }
        PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_bill_stream_pay_money_num) + "：" + summaryOrderInfo.getCountTotalCount());

        if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
            PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tv_refund_dialog_refund_money) + DateUtil.formatMoneyUtils(summaryOrderInfo.getCountTotalRefundFee()) + ToastHelper.toStr(R.string.pay_yuan));
        } else {
            PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tv_refund_dialog_refund_money) + MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(summaryOrderInfo.getCountTotalRefundFee()));
        }
        PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_bill_reufun_total_num) + "：" + summaryOrderInfo.getCountTotalRefundCount());

        if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
            PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_pay_total) + "：" + DateUtil.formatMoneyUtils((summaryOrderInfo.getCountTotalFee() - summaryOrderInfo.getCountTotalRefundFee())) + ToastHelper.toStr(R.string.pay_yuan));
        } else {
            PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_pay_total) + "：" + MainApplication.getFeeFh() + DateUtil.formatMoneyUtils((summaryOrderInfo.getCountTotalFee() - summaryOrderInfo.getCountTotalRefundFee())));
        }
        PrintClient.getInstance().printDoubleLine();

        /*是否开通小费功能，打印小费金额、总计*/
        if (MainApplication.isTipOpenFlag()){
            // 小费金额
            PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tip_amount) + "：" + MainApplication.getFeeType() + DateUtil.formatMoneyUtils(summaryOrderInfo.getCountTotalTipFee()));
            //小费笔数
            PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tip_count) + "：" + summaryOrderInfo.getCountTotalTipFeeCount());
            PrintClient.getInstance().printDoubleLine();
        }
        List<OrderTotalItemInfo> list = summaryOrderInfo.getOrderTotalItemInfo();

        Map<Integer, Object> map = new HashMap<Integer, Object>();

        for (OrderTotalItemInfo itemInfo : list) {

            switch (itemInfo.getPayTypeId()) {

                case 1:
                    map.put(0, itemInfo);
                    break;
                case 2:
                    map.put(1, itemInfo);
                    break;

                case 4:
                    map.put(2, itemInfo);
                    break;
                case 12:
                    map.put(3, itemInfo);
                    break;
                default:
                    map.put(itemInfo.getPayTypeId(), itemInfo);
                    break;
            }

        }
        for (Integer key : map.keySet()) {
            OrderTotalItemInfo itemInfo = (OrderTotalItemInfo) map.get(key);
            if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
                PrintClient.getInstance().printTextLeft(MainApplication.getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId())));
                PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_money)+ DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + ToastHelper.toStr(R.string.pay_yuan));
            } else {
                PrintClient.getInstance().printTextLeft(MainApplication.getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId())));
                PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_money)+ MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()));
            }
            PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tv_settle_count)  + "："+ itemInfo.getSuccessCount());
        }
        PrintClient.getInstance().pintSingleLine();

        PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()));
        PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_sign));

        PrintClient.getInstance().printEmptyLine();
        PrintClient.getInstance().printEmptyLine();
        PrintClient.getInstance().printEmptyLine();

        PrintClient.getInstance().startPrint();

    }

}
