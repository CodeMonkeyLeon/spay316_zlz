package cn.swiftpass.enterprise.ui.activity;

import java.util.List;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.shop.PersonalManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.RegisterSuccActivity;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.bussiness.model.ShopBankBaseInfo;
import cn.swiftpass.enterprise.bussiness.model.ShopBaseDataInfo;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.database.access.ShopBankDataDB;
import cn.swiftpass.enterprise.io.database.access.ShopBaseDataDB;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.NetworkUtils;
import cn.swiftpass.enterprise.utils.VerifyUtil;

public class RegisterActivity extends TemplateActivity
{
    private static final String LOG_TAG = RegisterActivity.class.getCanonicalName();
    
    private EditText user_phone;
    
    private EditText shop_name;
    
    private EditText code, password_id, commit_password_id, requestcode_id;
    
    //    private ImageView iv_clearPhone, iv_tel_promt;
    
    private ImageView iv_clearName;
    
    private Button confirm;
    
    private Button getCode;
    
    private TimeCount time;
    
    // 服务条款
    private TextView service;
    
    // 用户保护隐私
    //    private TextView protection;
    
    // 规则
    //    private TextView rule;
    
    private CheckBox remember;
    
    public static String REGISTERFLAG = "prompt";
    
    private SharedPreferences sp;
    
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle("注册");
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.register);
        
        // HandlerManager.registerHandler(HandlerManager.REGISTERPROMT, PayActivity.handler);
        sp = this.getSharedPreferences("login", 0);
        initView();
        
        setLinister();
        
        List<ShopBaseDataInfo> list = ShopBaseDataDB.getInstance().query();
        List<ShopBankBaseInfo> bankList = ShopBankDataDB.getInstance().query();
        if (list == null || list.size() == 0)
        {
            loadShoBaseData("1");
        }
        // 银行基本数据
        if (bankList == null || bankList.size() == 0)
        {
            loadShoBaseData("2");
            
        }
    }
    
    /**
     * 加载 行业数据
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void loadShoBaseData(String type)
    {
        PersonalManager.getShopAction_baseData(type, new UINotifyListener<Boolean>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                //                titleBar.setRightLodingVisible(true);
                
                //                showLoading(false, "注册中...");
            }
            
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                if (object != null)
                {
                    showToastInfo(object.toString());
                }
            }
            
            @Override
            public void onSucceed(Boolean result)
            {
                super.onSucceed(result);
                //                dismissLoading();
                
            }
        });
    }
    
    /**
     * 注册
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void register()
    {
        final String telNum = user_phone.getText().toString();
        String name = shop_name.getText().toString();
        String code_num = code.getText().toString();
        String password = password_id.getText().toString();
        String commit_password = commit_password_id.getText().toString();
        String requestcode = requestcode_id.getText().toString();
        
        UserManager.register(telNum,
            password,
            commit_password,
            requestcode,
            code_num,
            new UINotifyListener<UserModel>()
            {
                @Override
                public void onPreExecute()
                {
                    super.onPreExecute();
                    //                titleBar.setRightLodingVisible(true);
                    
                    showLoading(false, "注册中...");
                }
                
                @Override
                public void onError(final Object object)
                {
                    super.onError(object);
                    dismissLoading();
                    if (object != null)
                    {
                        RegisterActivity.this.runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                            }
                        });
                    }
                }
                
                @Override
                public void onSucceed(UserModel result)
                {
                    super.onSucceed(result);
                    dismissLoading();
                    
                    if (result != null)
                    {
                        //                    showToastInfo("注册成功!");
                        // 为激活授权的用户 注册 首先保持临时表中
                        //                        try
                        //                        {
                        //                            //                            UserInfoDB.getInstance().save(result);
                        //                        showPage(MainActivity.class, REGISTERFLAG);
                        //                        finish();
                        sp.edit().putString("user_name", telNum).commit();
                        ApiConstant.TEMP_USER = true;
                        
                        /* HandlerManager.notifyMessage(HandlerManager.REGISTERPROMT,
                             HandlerManager.REGISTERPROMT,
                             REGISTERFLAG);*/
                        //                        MainApplication.isRegisterUser = false;
                        if (MainApplication.mchName != null && !MainApplication.mchName.equals(""))
                        {
                            MainApplication.mchName = "";
                        }
                        todo();
                        
                        //                        }
                        //                        catch (SQLException e)
                        //                        {
                        //                            //                        showToastInfo("注册失败,请重新操作!");
                        //                            Log.e(LOG_TAG, "register() insert db failed " + e.getMessage());
                        //                        }
                        
                    }
                    
                }
            });
        
    }
    
    private void todo()
    {
        //启动心跳
        //HearbateService.startService(context);
        //      
        //        MainActivity.startActivity(RegisterActivity.this, "RegisterActivity");
        showPage(RegisterSuccActivity.class);
        
        //清空里面的缓存数据
        // 清除sharedPreferences缓存信息
        //        CleanManager.cleanSharedPreference(ShopKeeperDetialActivity.this, "dataOnes");
        //        CleanManager.cleanSharedPreference(ShopKeeperDetialActivity.this, "dataTwo");
        //        SharedPreferences sharedPreferences = this.getSharedPreferences("dataTwo", 0);
        //        SharedPreferences sharedPreferencesOne = this.getSharedPreferences("dataOnes", 0);
        //        
        //        Editor editor = sharedPreferences.edit();
        //        editor.clear();
        //        editor.commit();
        //        
        //        Editor editorOne = sharedPreferencesOne.edit();
        //        editorOne.clear();
        //        editorOne.commit();
        //不是操作员就是管理员
        /* if (LocalAccountManager.getInstance().getLoggedUser().role == UserRole.approve) {
             RefundActivity.startActivity(context);  //审核员
         } else  if( LocalAccountManager.getInstance().getLoggedUser().role == UserRole.general){
             PayActivity.startActivity(context); //普通用户
           }else if( LocalAccountManager.getInstance().getLoggedUser().role == UserRole.manager){
             SearchActivity.startActivity(context);  //管理员
         }*/
        finish();
    }
    
    /**
     * 设置监听
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void setLinister()
    {
        confirm.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (!isAbsoluteNullStr(user_phone.getText().toString()))
                {
                    if (!VerifyUtil.verifyValid(user_phone.getText().toString(), GlobalConstant.MOBILEPHOE))
                    {
                        showToastInfo("输入手机号码格式不正确");
                        user_phone.setText("");
                        user_phone.setFocusable(true);
                        return;
                    }
                }
                else
                {
                    showToastInfo("手机号码不能为空");
                    user_phone.setFocusable(true);
                    return;
                }
                
                //                if(isAbsoluteNullStr(shop_name.getText().toString()))
                //                {
                //                    ToastHelper.showInfo("手机号码 不能为空!!");
                //                    shop_name.setFocusable(true);
                //                    return;
                //                }
                
                if (isAbsoluteNullStr(code.getText().toString()))
                {
                    showToastInfo("验证码不能为空");
                    code.setFocusable(true);
                    return;
                }
                
                if (isAbsoluteNullStr(password_id.getText().toString()))
                {
                    showToastInfo("密码不能为空");
                    password_id.setFocusable(true);
                    return;
                    
                }
                
                if (isAbsoluteNullStr(commit_password_id.getText().toString()))
                {
                    showToastInfo("确认密码不能为空");
                    commit_password_id.setFocusable(true);
                    return;
                    
                }
                
                if (!commit_password_id.getText().toString().equals(password_id.getText().toString()))
                {
                    showToastInfo("两次密码不一致");
                    commit_password_id.setFocusable(true);
                    return;
                    
                }
                
                if (password_id.getText().toString().length() < 8)
                {
                    showToastInfo("密码不能小于8位数");
                    password_id.setFocusable(true);
                    return;
                }
                
                if (!NetworkUtils.isNetworkAvailable(getApplicationContext()))
                {
                    showToastInfo("sorry,网络连接异常，请检查网络后再试!");
                    return;
                }
                // 已阅读服务条款
                if (!remember.isChecked())
                {
                    showToastInfo("请选择同意阅读并接受服务条款!");
                    return;
                }
                register();
            }
            
        });
        
        // 获取验证码
        getCode.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                String telNumber = user_phone.getText().toString();
                
                if ("".equals(telNumber) || telNumber.length() == 0)
                {
                    showToastInfo("手机号码不能为空");
                    return;
                }
                
                if (!VerifyUtil.verifyValid(user_phone.getText().toString(), GlobalConstant.MOBILEPHOE))
                {
                    showToastInfo("输入手机号码格式不正确");
                    user_phone.setFocusable(true);
                    return;
                }
                
                if (!NetworkUtils.isNetworkAvailable(getApplicationContext()))
                {
                    showToastInfo("sorry,网络连接异常，请检查网络后再试");
                    return;
                }
                
                // 60秒 验证码倒计时
                
                getCode.setEnabled(false);
                Countdown();
                UserManager.getRegisterCode(telNumber, "roundCode", new UINotifyListener<Integer>()
                {
                    @Override
                    public void onPreExecute()
                    {
                        super.onPreExecute();
                        titleBar.setRightLodingVisible(true);
                    }
                    
                    @Override
                    public void onError(Object object)
                    {
                        super.onError(object);
                        if (object != null)
                        {
                            showToastInfo(object.toString());
                        }
                        titleBar.setRightLodingVisible(false, false);
                    }
                    
                    @Override
                    public void onSucceed(Integer result)
                    {
                        super.onSucceed(result);
                        titleBar.setRightLodingVisible(false, false);
                        getCode.setEnabled(true);
                        getCode.setClickable(true);
                        switch (result)
                        {
                        // 成功
                            case 0:
                                showToastInfo("获取验证码成功,请注意查收短信");
                                
                                return;
                                // 手机号码以注册
                            case 1:
                                showToastInfo("该手机号码已注册，请重新输入");
                                return;
                            case 2:
                                showToastInfo("获取验证失败，请稍后再试");
                                getCode.setEnabled(true);
                                getCode.setClickable(true);
                                getCode.setText("重新获取验证码");
                                if (time != null)
                                {
                                    time.onFinish();
                                    time.cancel();
                                    
                                }
                                return;
                            case RequestResult.RESULT_TIMEOUT_ERROR: // 请求超时
                                showToastInfo("连接超时，请稍微再试!");
                                return;
                                
                        }
                        //                        if (result)
                        //                        {
                        //                            
                        //                            getCode.setEnabled(true);
                        //                            getCode.setClickable(true);
                        //                            showToastInfo("获取验证码成功,请注意查收短信!");
                        //                        }
                        //                        else
                        //                        {
                        //                            getCode.setEnabled(true);
                        //                            getCode.setClickable(true);
                        //                            showToastInfo("该手机号码已注册，请重新输入!");
                        //                        }
                        
                    }
                });
            }
            
        });
        
        //设置几个文字的监听器
        service.setOnClickListener(listener);
        //        protection.setOnClickListener(listener);
        //        rule.setOnClickListener(listener);
    }
    
    //各种协议监听   
    OnClickListener listener = new OnClickListener()
    {
        
        @Override
        public void onClick(View v)
        {
            String flag = ""; //传一个标识  识别是service or protection or rule
            switch (v.getId())
            {
                case R.id.service:
                    flag = "service";
                    break;
            //                case R.id.protection:
            //                    flag = "protection";
            //                    break;
            //                case R.id.rule:
            //                    flag = "rule";
            //                    break;
            
            }
            //            Intent intent = new Intent(RegisterActivity.this, ProtocolActivity.class);
            //            intent.putExtra("flag", flag);
            //            startActivity(intent);
            ContentTextActivity.startActivity(RegisterActivity.this,
                ApiConstant.URL_REGIST_PROVISIONS,
                R.string.title_common_question2);
        }
    };
    
    //    
    /** {@inheritDoc} */
    
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (time != null)
        {
            time.cancel();
        }
    }
    
    /**
     *验证码倒计时60秒
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void Countdown()
    {
        // 总共60 没间隔1秒
        time = new TimeCount(60000, 1000);
        time.start();
    }
    
    class TimeCount extends CountDownTimer
    {
        public TimeCount(long millisInFuture, long countDownInterval)
        {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }
        
        @Override
        public void onFinish()
        {//计时完毕时触发
            getCode.setEnabled(true);
            getCode.setClickable(true);
            getCode.setText("重新获取验证码");
        }
        
        @Override
        public void onTick(long millisUntilFinished)
        {//计时过程显示
            getCode.setEnabled(false);
            getCode.setClickable(false);
            getCode.setText("重新获取验证码" + millisUntilFinished / 1000 + "秒");
        }
    }
    
    private OnClickListener clearImage = new OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            switch (v.getId())
            {
                case R.id.iv_clearPhone:
                    user_phone.setText("");
                    break;
                case R.id.iv_clearName:
                    shop_name.setText("");
                    break;
            }
        }
    };
    
    /***
     * 检查手机号码格式
     * <功能详细描述>
     * @param mobile
     * @return
     * @see [类、类#方法、类#成员]
     */
    //    private boolean checkMobile(String mobile)
    //    {
    //        Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0-9])|(147))\\d{8}$");
    //        Matcher m = p.matcher(mobile);
    //        boolean flag = m.matches();
    //        if (!flag)
    //        {
    //            return false;
    //        }
    //        
    //        return true;
    //    }
    
    /**
     * <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView()
    {
        user_phone = getViewById(R.id.userPhone);
        shop_name = getViewById(R.id.shop_name);
        
        code = getViewById(R.id.code);
        password_id = getViewById(R.id.password_id);
        commit_password_id = getViewById(R.id.commit_password_id);
        requestcode_id = getViewById(R.id.requestcode_id);
        
        //        iv_clearPhone = getViewById(R.id.iv_clearPhone);
        iv_clearName = getViewById(R.id.iv_clearName);
        
        //        iv_clearPhone.setOnClickListener(clearImage);
        iv_clearName.setOnClickListener(clearImage);
        
        //        iv_tel_promt = getViewById(R.id.iv_tel_promt);
        
        confirm = getViewById(R.id.confirm);
        
        getCode = getViewById(R.id.getCode);
        service = getViewById(R.id.service);
        //        protection = getViewById(R.id.protection);
        //        rule = getViewById(R.id.rule);
        remember = getViewById(R.id.remember);
        
        // 设置下划线
        //  service.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        // protection.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        //  rule.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        {
            
            @Override
            public void onExecute(CharSequence s, int start, int before, int count)
            {
            }
            
            @Override
            public void onAfterTextChanged(Editable s)
            {
                if (s != null && s.length() >= 1)
                {
                    if (user_phone.isFocused())
                    {
                        //                        iv_clearPhone.setVisibility(View.VISIBLE);
                        iv_clearName.setVisibility(View.GONE);
                        //                        iv_tel_promt.setVisibility(View.GONE);
                    }
                    else if (shop_name.isFocused())
                    {
                        iv_clearName.setVisibility(View.VISIBLE);
                        //                        iv_clearPhone.setVisibility(View.GONE);
                    }
                }
                else
                {
                    if (user_phone.isFocused())
                    {
                        iv_clearName.setVisibility(View.GONE);
                        //                        iv_clearPhone.setVisibility(View.GONE);
                        //                        iv_tel_promt.setVisibility(View.VISIBLE);
                    }
                    else if (shop_name.isFocused())
                    {
                        iv_clearName.setVisibility(View.GONE);
                        //                        iv_clearPhone.setVisibility(View.GONE);
                        
                    }
                }
            }
            
        });
        
        user_phone.addTextChangedListener(editTextWatcher);
        shop_name.addTextChangedListener(editTextWatcher);
        
        user_phone.setOnFocusChangeListener(listenerFocus);
        shop_name.setOnFocusChangeListener(listenerFocus);
    }
    
    private final OnFocusChangeListener listenerFocus = new OnFocusChangeListener()
    {
        @Override
        public void onFocusChange(View v, boolean hasFocus)
        {
            switch (v.getId())
            {
                case R.id.userPhone:
                    if (hasFocus)
                    {
                        //                        if (user_phone.getText().length() >= 1)
                        //                        {
                        //                            iv_clearPhone.setVisibility(View.VISIBLE);
                        //                        }
                        //                        else
                        //                        {
                        //                            iv_clearPhone.setVisibility(View.GONE);
                        //                        }
                    }
                    else
                    {
                        //                        iv_clearPhone.setVisibility(View.GONE);
                    }
                    break;
                case R.id.shop_name:
                    //                    sp.edit().putString("userPwd", userPwd.getText().toString()).commit();
                    if (hasFocus)
                    {
                        if (shop_name.getText().length() >= 1)
                        {
                            iv_clearName.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            iv_clearName.setVisibility(View.GONE);
                        }
                    }
                    else
                    {
                        iv_clearName.setVisibility(View.GONE);
                    }
                    break;
            }
            
        }
    };
}
