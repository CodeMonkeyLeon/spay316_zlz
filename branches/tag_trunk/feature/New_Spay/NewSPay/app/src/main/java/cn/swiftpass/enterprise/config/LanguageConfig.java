package cn.swiftpass.enterprise.config;

public interface LanguageConfig {

    String LANG_CODE_ZH_CN_NEW = "zh-CN";
    String LANG_CODE_ZH_TW_NEW = "zh-tw";
    String LANG_CODE_ZH_MO_NEW = "zh-MO";
    String LANG_CODE_ZH_HK_NEW = "zh-HK";
    String LANG_CODE_JA_JP_NEW = "ja-JP";

    /**
     * 简体中文
     */
    String LANG_CODE_ZH_CN = "zh_cn";

    /**
     * 繁体中文
     */
    String LANG_CODE_ZH_TW = "zh_tw";

    String LANG_CODE_ZH_MO = "zh_MO";

    String LANG_CODE_ZH_HK = "zh_HK";

    String LANG_CODE_JA_JP = "ja_JP";

    /**
     * 英语
     */
    String LANG_CODE_EN_US = "en_us";

    String LANG_CODE_ZH_CN_HANS = "zh_CN_#Hans";
    String LANG_CODE_ZH_HK_HANT = "zh_HK_#Hant";

}
