package cn.swiftpass.enterprise.bussiness.logica.cache;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.model.CacheModel;
import cn.swiftpass.enterprise.bussiness.model.CashierReport;
import cn.swiftpass.enterprise.io.database.access.CacheInfoDB;
import cn.swiftpass.enterprise.utils.FileUtils;
import cn.swiftpass.enterprise.utils.Utils;

/**
 * 缓存管理
 * User: Administrator
 * Date: 13-11-18
 * Time: 下午8:14
 * To change this template use File | Settings | File Templates.
 */
public class CacheInfoManager
{
    
    private CacheInfoManager()
    {
    }
    
    private static CacheInfoManager instance;
    
    public static CacheInfoManager getInstence()
    {
        if (instance == null)
        {
            instance = new CacheInfoManager();
        }
        return instance;
    }
    
    public void saveCacheByType(CacheModel cacheModel)
        throws SQLException
    {
        CacheInfoDB.getInstance().save(cacheModel);
    }
    
    public CacheModel queryByType(int type)
        throws SQLException
    {
        int hour = 1;
        return CacheInfoDB.getInstance().queryWhere(type, LocalAccountManager.getInstance().getUID(), true, hour);
    }
    
    /**超过一个小时的数据需要更新，并且更新之前的时间
     * @param cacheModel
     * @return   true 从本地获取数据，false 网络下载数据
     */
    public boolean isUpdate(CacheModel cacheModel)
    {
        try
        {
            if (null == CacheInfoDB.getInstance().queryWhere(cacheModel.type,
                LocalAccountManager.getInstance().getUID()))
            {
                CacheInfoDB.getInstance().save(cacheModel);
                return false;
            }
            CacheModel c = queryByType(cacheModel.type);
            if (c == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
        
    }
    
    /**
     * 读取缓存数据
     * @param
     * @return
     * @throws
     */
    public List<CashierReport> readTextInputStream(String fileName)
        throws Exception
    {
        StringBuffer strbuffer = new StringBuffer();
        String line;
        BufferedReader reader = null;
        List<CashierReport> list = null;
        FileInputStream is;
        try
        {
            File file = new File(FileUtils.getAppFiled(fileName));
            if (!file.exists())
            {
                return null;
            }
            list = new ArrayList<CashierReport>();
            is = new FileInputStream(file);
            reader = new BufferedReader(new InputStreamReader(is));
            while ((line = reader.readLine()) != null)
            {
                strbuffer.append(line);//.append("\r\n");
                CashierReport t = new CashierReport();
                String[] strs = line.split("\\|");
                if (strs.length > 1)
                {
                    t.addTime = strs[0];
                    t.tradeNum = Utils.Integer.tryParse(strs[1], 0);
                    t.turnover = Utils.Long.tryParse(strs[2], 0);
                    t.refundNum = Utils.Integer.tryParse(strs[3], 0);
                    t.refundFee = Utils.Long.tryParse(strs[4], 0);
                    list.add(t);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
        finally
        {
            if (reader != null)
            {
                reader.close();
            }
        }
        return list;
    }
    
    /**
     * 将文本内容写入文件
     * @param file
     * @param str
     */
    public static void writeTextFile(File file, String str)
        throws IOException
    {
        DataOutputStream out = null;
        try
        {
            
            out = new DataOutputStream(new FileOutputStream(file));
            out.write(str.getBytes());
        }
        finally
        {
            if (out != null)
            {
                out.close();
            }
        }
    }
    
    /**交易统计数据缓存*/
    public void writeCashierReportFile(String fileName, List<CashierReport> data)
    {
        StringBuffer sb = new StringBuffer();
        for (CashierReport c : data)
        {
            sb.append(c.addTime);
            sb.append("|");
            sb.append(c.tradeNum);
            sb.append("|");
            sb.append(c.turnover);
            sb.append("|");
            sb.append(c.refundNum);
            sb.append("|");
            sb.append(c.refundFee);
            sb.append("\n\r");
        }
        try
        {
            writeTextFile(new File(FileUtils.getAppFiled(fileName)), sb.toString());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            ;
        }
    }
}
