package cn.swiftpass.enterprise.utils;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import androidx.core.content.FileProvider;

import com.example.common.sentry.SentryUtils;
import com.example.common.sp.PreferenceUtil;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.BuildConfig;

/***
 * 图片工具管理
 */
public class ImageUtil {
    /**
     * 打开系统相机
     * 兼容Android 10.0 和以下
     * 用来解决拍照之后图片模糊的问题
     */
    public final static int REQUEST_CODE_TAKE_PICTURE = 1001;
    private static final String TAG = ImageUtil.class.getSimpleName();
    private static final String LAST_SHARE_URI = "last_share_uri";

    public static int getConstellationImgId(Context mContext, String pic) {
        return mContext.getResources().getIdentifier(pic, "drawable", mContext.getPackageName());
    }

    // 生成圆角图片
    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, float radius) {
        try {
            Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
            Canvas canvas = new Canvas(output);
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            final RectF rectF = new RectF(new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()));
            final float roundPx = radius;
            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(Color.WHITE);
            canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
            paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
            final Rect src = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            canvas.drawBitmap(bitmap, src, rect, paint);
            return output;
        } catch (Exception e) {
            return bitmap;
        }
    }

    /**
     * 将图片变为灰度图
     *
     * @param old
     * @return
     */
    public static Bitmap getGreyImage(Bitmap old) {
        int width, height;
        height = old.getHeight();
        width = old.getWidth();
        Bitmap grayImg = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        Canvas c = new Canvas(grayImg);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(old, 0, 0, paint);
        return grayImg;
    }

    /**
     * @param bitmap 需要处理的图像
     * @param outImg 相框
     *               相框与图像叠加的偏移量。X轴方向。
     * @return 合成后的图像
     * @author huangdonghua
     */
    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, Bitmap outImg, int x, int y, float density) {

        if (bitmap == null || outImg == null) return null;

        int offsetX = (int) (x * density + 0.5f);
        int offsetY = (int) (y * density + 0.5f);

        int width = outImg.getWidth();
        int height = outImg.getHeight();
        //float roundPx = (width - offsetX * 2) / 2;

        // 创建画布。以相框大小为基准。
        Bitmap output = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        // 剪裁圆形区域
        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect src = new Rect(0, 0, bitmap.getWidth(), bitmap.getWidth()); // 来源图片尺寸
        final Rect dst = new Rect(offsetX, offsetY, width - offsetX, width - offsetX * 2 + offsetY); // 需要最终大小的尺寸
        final RectF rectF = new RectF(dst);
        paint.setAntiAlias(true);

        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        //canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawBitmap(bitmap, src, rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, src, dst, paint);

        // 添加相框
        final Rect outR = new Rect(0, 0, width, height);
        final Paint outP = new Paint();
        outP.setXfermode(new PorterDuffXfermode(Mode.SRC_OVER));
        canvas.drawBitmap(outImg, outR, outR, outP);

        return output;
    }

    
   /* public static Bitmap decodeFile2(String imageFile)
    {
        BitmapFactory.Options opts;
        try
        {
            if (!new File(imageFile).exists())
            {
                return null;
            }
            opts = new BitmapFactory.Options();
            opts.inJustDecodeBounds = true;
            Bitmap bitmap = BitmapFactory.decodeFile(imageFile);
            return bitmap;
        }
        catch (Exception e)
        {

            Log.e(TAG,Log.getStackTraceString(e));
            opts = new BitmapFactory.Options();
            opts.inSampleSize = 2;
            Bitmap bitmap = BitmapFactory.decodeFile(imageFile, opts);
            return bitmap;
        }
        
    }*/

//    private static int computeSampleSize(BitmapFactory.Options options, int minSideLength, int maxNumOfPixels) {
//        int initialSize = computeInitialSampleSize(options, minSideLength, maxNumOfPixels);
//        int roundedSize;
//        if (initialSize <= 8) {
//            roundedSize = 1;
//            while (roundedSize < initialSize) {
//                roundedSize <<= 1;
//            }
//        } else {
//            roundedSize = (initialSize + 7) / 8 * 8;
//        }
//        return roundedSize;
//    }

//    private static int computeInitialSampleSize(BitmapFactory.Options options, int minSideLength, int maxNumOfPixels) {
//        double w = options.outWidth;
//        double h = options.outHeight;
//        int lowerBound = (maxNumOfPixels == -1) ? 1 : (int) Math.ceil(Math.sqrt(w * h / maxNumOfPixels));
//        int upperBound = (minSideLength == -1) ? 128 : (int) Math.min(Math.floor(w / minSideLength), Math.floor(h / minSideLength));
//        if (upperBound < lowerBound) {
//            return lowerBound;
//        }
//        if ((maxNumOfPixels == -1) && (minSideLength == -1)) {
//            return 1;
//        } else if (minSideLength == -1) {
//            return lowerBound;
//        } else {
//            return upperBound;
//        }
//    }

    /**
     * 比例缩小图片
     *
     * @param f
     * @param isInSample
     * @param requiredSize
     * @return
     */
   /* public static Bitmap decodeFile(File f, boolean isInSample, int requiredSize)
    {
        if (!f.exists() || f.length() == 0)
            return null;
        try
        {
            int scale = 1;
            if (isInSample)
            {
                // decode image size
                BitmapFactory.Options o = new BitmapFactory.Options();
                o.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(new FileInputStream(f), null, o);
                
                // Find the correct scale value. It should be the power of 2.
                int REQUIRED_SIZE = requiredSize;
                int width_tmp = o.outWidth, height_tmp = o.outHeight;
                while (true)
                {
                    if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                        break;
                    width_tmp /= 2;
                    height_tmp /= 2;
                    scale *= 2;
                }
            }
            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        }
        catch (FileNotFoundException e)
        {
            Log.e(TAG,Log.getStackTraceString(e));
        }
        catch (Exception e)
        {
            Log.e(TAG,Log.getStackTraceString(e));
        }
        return null;
    }*/

    /**
     * 放大图
     *
     * @param bitmap
     * @param width
     * @param height
     * @return
     */
    public static Bitmap magnifyBitmap(Bitmap bitmap, int width, int height) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        int sw = width;
        int sh = height;
        if (w * h > width * height) {
            sw = width;
            sh = height;
        }
        Matrix matrix = new Matrix();
        matrix.postScale((float) sw / w, (float) sh / h);
        return Bitmap.createBitmap(bitmap, 0, 0, w, h, matrix, true);
    }

    /**
     * 适配缩放图片大小
     *
     * @param bitmap
     * @param width
     * @param height
     * @return
     */
    public static Bitmap zoomAdjustBitmap(Bitmap bitmap, int width, int height) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        int sw = w > width ? width : w;
        int sh = h > height ? height : h;
        if (w * h > width * height) {
            sw = width;
            sh = height;
        }
        Matrix matrix = new Matrix();
        matrix.postScale((float) sw / w, (float) sh / h);
        return Bitmap.createBitmap(bitmap, 0, 0, w, h, matrix, true);
    }

    /**
     * 下载图片
     */
    
   /* public static Bitmap getHttpLoadPicToBitmap(String url, String name)
    {
        URL myFileUrl = null;
        Bitmap bitmap = null;
        try
        {
            myFileUrl = new URL(url);
        }
        catch (MalformedURLException e)
        {
            Log.e(TAG,Log.getStackTraceString(e));
        }
        try
        {
            HttpURLConnection conn = (HttpURLConnection)myFileUrl.openConnection();
            conn.setConnectTimeout(0);
            conn.setDoInput(true);
            conn.connect();
            InputStream is = conn.getInputStream();
            bitmap = BitmapFactory.decodeStream(is);
            if (bitmap != null)
            {
                String path = AppHelper.getImgCacheDir() + name + ".jpg";
                try
                {
                    saveSimpleImag(path, bitmap, Bitmap.CompressFormat.JPEG);
                }
                catch (Exception e)
                {
                }
            }
            is.close();
        }
        catch (Exception e)
        {
            Log.e(TAG,Log.getStackTraceString(e));
        }
        return bitmap;
    }*/

    /**
     * 将二进制数组保存为bitmap
     */
    public static Bitmap BytesToBimap(byte[] b) {
        if (b.length != 0) {
            return BitmapFactory.decodeByteArray(b, 0, b.length);
        } else {
            return null;
        }
    }

    /**
     * 保存图片到指定文件
     */
    public static boolean saveSimpleImag(String imgFile, Bitmap bitmap, CompressFormat format) {
        File file = new File(imgFile);
        BufferedOutputStream dos = null;
        try {
            dos = new BufferedOutputStream(new FileOutputStream(file));

            return bitmap.compress(format, 100, dos);
        } catch (FileNotFoundException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
        } finally {
            if (dos != null) {
                try {
                    dos.close();
                } catch (IOException e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(e));
                }
                dos = null;
            }
        }
        return false;
    }

    /**
     * 保存图片到指定文件
     */
    public static boolean saveImag2(String imgFile, Bitmap bitmap, CompressFormat format) {
        File file = new File(imgFile);
        BufferedOutputStream dos = null;
        try {
            dos = new BufferedOutputStream(new FileOutputStream(file));

            int w = bitmap.getWidth();
            int h = bitmap.getHeight();

            Bitmap newb = Bitmap.createBitmap(w, h, Config.ARGB_8888);// 创建一个新的和SRC长度宽度一
            newb.setDensity(bitmap.getDensity());

            Canvas canvas = new Canvas(newb);
            canvas.drawColor(Color.WHITE);
            canvas.drawBitmap(bitmap, 0, 0, null); // 通过Canvas绘制Bitmap
            bitmap = newb;

            // 画背景
            return bitmap.compress(format, 100, dos);
        } catch (FileNotFoundException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
        } finally {
            if (dos != null) {
                try {
                    dos.close();
                } catch (IOException e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(e));
                }
                dos = null;
            }
        }
        return false;
    }

    /**
     * 检查合法后缀名
     *
     * @param filePath
     * @return
     */
    /*public static String getFileSuffix(String filePath)
    {
        String[] suffix =
            new String[] {".amr", ".rar", ".txt", ".mp3", ".mp4", ".3gp", ".jpg", ".ico", ".png", ".gif", ".zip",
                ".pdf", ".xml", ".apk", ".log"};
        String ret = filePath.trim().toLowerCase();
        for (int i = 0; i < suffix.length; i++)
        {
            if (ret.endsWith(suffix[i]))
            {
                return suffix[i];
            }
        }
        return null;
    }*/

    /**
     * 保存图片到指定文件
     */
    public static boolean saveImag(String imgFile, Bitmap bitmap, CompressFormat format) {
        File file = new File(imgFile);
        BufferedOutputStream dos = null;
        try {
            dos = new BufferedOutputStream(new FileOutputStream(file));

            int w = bitmap.getWidth();
            int h = bitmap.getHeight();

            Bitmap newb = Bitmap.createBitmap(w + 100, h + 100, Config.ARGB_8888);// 创建一个新的和SRC长度宽度一
            newb.setDensity(bitmap.getDensity());

            Canvas canvas = new Canvas(newb);
            canvas.drawColor(Color.WHITE);
            canvas.drawBitmap(bitmap, 0, 10, null); // 通过Canvas绘制Bitmap
            bitmap = newb;

            // 画背景
            return bitmap.compress(format, 100, dos);
        } catch (FileNotFoundException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
        } finally {
            if (dos != null) {
                try {
                    dos.close();
                } catch (IOException e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(e));
                }
                dos = null;
            }
        }
        return false;
    }

    /**
     * bitmap转换stream
     *
     * @param bitmap
     * @return
     */
    public static byte[] getBitmapStream(Bitmap bitmap) {

        return getBitmapStream(bitmap, CompressFormat.PNG, 100);
    }

    /**
     * bitmap转换stream
     *
     * @param bitmap
     * @return
     */
    public static byte[] getBitmapStream(Bitmap bitmap, CompressFormat paramCompressFormat, int quality) {

        if (bitmap != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(paramCompressFormat, quality, stream);
            byte[] byteArray = stream.toByteArray();
            return byteArray;
        } else {
            return null;
        }
    }

    /**
     * 转换bitmap
     */
    public static Bitmap drawableToBitmap(Drawable drawable) {
        // 取 drawable 的长宽
        int w = drawable.getIntrinsicWidth();
        int h = drawable.getIntrinsicHeight();
        // 取 drawable 的颜色格式
        Config config = drawable.getOpacity() != PixelFormat.OPAQUE ? Config.ARGB_8888 : Config.RGB_565;
        // 建立对应 bitmap
        Bitmap bitmap = Bitmap.createBitmap(w, h, config);
        // 建立对应 bitmap 的画布
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, w, h);
        // 把 drawable 内容画到画布中
        drawable.draw(canvas);
        return bitmap;
    }

    /**
     * 缩小图
     */
    public static Bitmap zoomBitmap(Bitmap src, int destWidth, int destHeigth) {
        //String tag = "lessenBitmap";
        if (src == null) {
            return null;
        }

        int w = src.getWidth();// 源文件的大小
        int h = src.getHeight();

        // calculate the scale - in this case = 0.4f
        float scaleWidth = ((float) destWidth) / w;// 宽度缩小比例
        float scaleHeight = ((float) destHeigth) / h;// 高度缩小比例

        Matrix m = new Matrix();// 矩阵
        m.postScale(scaleWidth, scaleHeight);// 设置矩阵比例
        Bitmap resizedBitmap = Bitmap.createBitmap(src, 0, 0, w, h, m, true);// 直接按照矩阵的比例把源文件画入进行
        return resizedBitmap;
    }

    /**
     * 压缩图
     */
    public static Bitmap transImage(String fromFile, int width, int height) {
        Bitmap bitmap = null;
        Bitmap resizeBitmap = null;
        try {

            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(fromFile, opts);
            bitmap = BitmapFactory.decodeFile(fromFile);
            if (opts.outWidth * opts.outHeight < width * height) {
                // 指定太大无法压缩，原图返回
                return bitmap;
            }

            int bitmapWidth = bitmap.getWidth();
            int bitmapHeight = bitmap.getHeight();
            // 缩放图片的尺寸
            float scaleWidth = (float) width / bitmapWidth;
            float scaleHeight = (float) height / bitmapHeight;
            Matrix matrix = new Matrix();
            matrix.postScale(scaleWidth, scaleHeight);
            // 产生缩放后的Bitmap对象
            resizeBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmapWidth, bitmapHeight, matrix, true);
            return resizeBitmap;
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
            if (bitmap != null && !bitmap.isRecycled()) {
                // 记得释放资源，否则会内存溢出
                bitmap.recycle();
                bitmap = null;
            }
            return null;
        }
    }

    /**
     * 根据intent得到文件路径
     *
     * @param data
     * @param context
     * @return
     */
    public static String getFilePath(Intent data, Context context) {
        ContentResolver cr = null;
        Uri uri = null;
        Cursor cursor = null;
        if (null != data) {
            uri = data.getData();
            String path = uri.getPath();
            if (path.lastIndexOf(".") == -1) {
                cr = context.getContentResolver();
                cursor = cr.query(uri, null, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();
                    return cursor.getString(1);// 得到多媒体路径
                }
            } else {
                return path;
            }
        }
        return null;
    }

    public static Bitmap getimage(String srcPath, int target) {
        Bitmap sourceBitmap = null;
        do {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(srcPath, options);
            if (options.mCancel || options.outWidth == -1 || options.outHeight == -1) {
                return null;
            }
            options.inSampleSize = computeSampleSize(options, target);
            options.inJustDecodeBounds = false;
            options.inDither = false;

            // options.inPreferredConfig = Bitmap.Config.ARGB_4444;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            InputStream is = null;
            try {
                is = new FileInputStream(srcPath);
                sourceBitmap = BitmapFactory.decodeStream(is, null, options);
                options = null;
                if (null == sourceBitmap) {
                    break;
                }
                int width = sourceBitmap.getWidth();
                int height = sourceBitmap.getHeight();
                int max = Math.max(width, height);
                if (max > target) {
                    Matrix matrix = new Matrix();
                    float scale = ((float) target) / max;
                    matrix.postScale(scale, scale);
                    sourceBitmap = Bitmap.createBitmap(sourceBitmap, 0, 0, width, height, matrix, true);
                }
            } catch (OutOfMemoryError e) {
                SentryUtils.INSTANCE.uploadTryCatchError(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                try {
                    if (is != null) {
                        is.close();
                        is = null;
                    }

                } catch (IOException ex) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            ex,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    // TODO Auto-generated catch block
                    Log.e(TAG, Log.getStackTraceString(ex));
                }
                if (null != sourceBitmap && !sourceBitmap.isRecycled()) {
                    sourceBitmap.recycle();
                    sourceBitmap = null;
                }
                Log.e(TAG, Log.getStackTraceString(e));
            } catch (FileNotFoundException e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                try {
                    if (is != null) {
                        is.close();
                        is = null;
                    }
                } catch (IOException ex) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            ex,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(ex));
                }
                if (null != sourceBitmap && !sourceBitmap.isRecycled()) {
                    sourceBitmap.recycle();
                    sourceBitmap = null;
                }
                Log.e(TAG, Log.getStackTraceString(e));
            } finally {
                try {
                    if (is != null) {
                        is.close();
                        is = null;
                    }
                } catch (IOException e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(e));
                }

            }
        } while (false);
        return sourceBitmap;
    }

    /**
     * 计算缩放比
     *
     * @param options
     * @param target
     * @return
     */
    public static int computeSampleSize(BitmapFactory.Options options, int target) {
        int w = options.outWidth;
        int h = options.outHeight;
        int candidateW = w / target;
        int candidateH = h / target;

        int candidate = Math.max(candidateW, candidateH);
        if (candidate == 0) return 1;
        return candidate;
    }

    public static Bitmap compressImage(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);//质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        int options = 100;
        while (baos.toByteArray().length / 1024 > 100) { //循环判断如果压缩后图片是否大于100kb,大于继续压缩
            baos.reset();//重置baos即清空baos
            options -= 10;//每次都减少10
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);//这里压缩options%，把压缩后的数据存放到baos中

        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());//把压缩后的数据baos存放到ByteArrayInputStream中
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);//把ByteArrayInputStream数据生成图片
        return bitmap;
    }

    /**
     * 从文件中读取图片,引导页
     *
     * @param picPath 文件存放路径
     */
    public static Bitmap readBitmapFromStream(String picPath) {
        Bitmap sourceBitmap = null;
        if (TextUtils.isEmpty(picPath)) {
            sourceBitmap = null;
        }
        do {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(picPath, options);
            if (options.mCancel || options.outWidth == -1 || options.outHeight == -1) {
                return null;
            }
            options.inJustDecodeBounds = false;
            options.inDither = false;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            InputStream is = null;
            try {
                is = new FileInputStream(picPath);
                sourceBitmap = BitmapFactory.decodeStream(is, null, options);
                options = null;
                if (null == sourceBitmap) {
                    break;
                }
            } catch (OutOfMemoryError e) {
                SentryUtils.INSTANCE.uploadTryCatchError(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                try {
                    if (is != null) {
                        is.close();
                        is = null;
                    }
                } catch (IOException ex) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            ex,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(ex));
                }
                if (null != sourceBitmap && !sourceBitmap.isRecycled()) {
                    sourceBitmap.recycle();
                    sourceBitmap = null;
                }
                Log.e(TAG, Log.getStackTraceString(e));
            } catch (FileNotFoundException e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                if (null != sourceBitmap && !sourceBitmap.isRecycled()) {
                    sourceBitmap.recycle();
                    sourceBitmap = null;
                }
                Log.e(TAG, Log.getStackTraceString(e));
            } finally {
                try {
                    if (is != null) {
                        is.close();
                        is = null;
                    }
                } catch (IOException e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
        } while (false);
        return sourceBitmap;
    }

    public static Bitmap adjustPhotoRotation(Bitmap bm, final int orientationDegree) {
        Matrix m = new Matrix();
        m.setRotate(orientationDegree, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);

        try {
            Bitmap bm1 = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m, true);
            return bm1;
        } catch (OutOfMemoryError ex) {
            SentryUtils.INSTANCE.uploadTryCatchError(
                    ex,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(ex));
        }
        return null;
    }

    /**
     * 通知 相册更新状态
     *
     * @param pictureUir
     */
    public static void notificationSystemUpdateAlbum(Uri pictureUir, Context context) {
        if (context != null && pictureUir != null) {
            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, pictureUir));
        }
    }

    /**
     * 保存小票预览图片
     * 删除旧图
     *
     * @param context
     * @param fileName
     * @param v
     * @return
     */
    public static Uri saveTicketPreViewBitmapFileDeleteOld(Context context, String fileName, View v) {
        Bitmap bitmap = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(Color.WHITE);
        v.draw(canvas);
        File file;
        FileOutputStream out = null;
        fileName += "_" + System.currentTimeMillis() + ".jpg";
        try {
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                // 适配android 10
                Uri photoUri = createImageUri(context, fileName);
                notificationSystemUpdateAlbum(photoUri, context);
                if (photoUri != null) {
                    //若生成了uri，则表示该文件添加成功
                    //使用流将内容写入该uri中即可
                    PreferenceUtil.commitString(LAST_SHARE_URI, fileName);
                    OutputStream outputStream = context.getContentResolver().openOutputStream(photoUri);
                    if (outputStream != null) {
                        bitmap.compress(CompressFormat.JPEG, 100, outputStream);
                        outputStream.flush();
                        outputStream.close();
                    }
                }
                return photoUri;
            } else {
                // 适配android 9.0以及以下
                String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getPath();
                File dir = new File(path + "/swiftpass_pay", Environment.DIRECTORY_DCIM);
                if (!dir.exists()) {
                    boolean isSuccess = dir.mkdirs();
                    //TODO FIX ZHANGXINCHAO 主要是解决扫描漏洞 不处理返回值的问题
                    if (isSuccess) {
                        //删除旧图
                        String lastFileName = PreferenceUtil.getString(LAST_SHARE_URI, "");
                        if (!TextUtils.isEmpty(lastFileName)) {
                            File deleteFile = new File(dir, lastFileName);
                            if (deleteFile != null && deleteFile.exists()) {
                                boolean deleteResult = deleteFile.delete();
                                Uri uri = Uri.fromFile(deleteFile);
                                notificationSystemUpdateAlbum(uri, context);
                                Logger.d(TAG, "fileName：" + fileName + " 删除结果：" + deleteResult);
                            }
                        }
                    } else {

                    }
                } else {
                    //删除旧图
                    String lastFileName = PreferenceUtil.getString(LAST_SHARE_URI, "");
                    if (!TextUtils.isEmpty(lastFileName)) {
                        File deleteFile = new File(dir, lastFileName);
                        if (deleteFile != null && deleteFile.exists()) {
                            boolean deleteResult = deleteFile.delete();
                            Uri uri = Uri.fromFile(deleteFile);
                            notificationSystemUpdateAlbum(uri, context);
                            Logger.d(TAG, "fileName：" + fileName + " 删除结果：" + deleteResult);
                        }
                    }
                }
                file = new File(dir, fileName);
                out = new FileOutputStream(file);
                bitmap.compress(CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
                MediaScannerConnection.scanFile(MainApplication.getInstance(), new String[]{file.getPath()}, null, null);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Uri uri = FileProvider.getUriForFile(
                            context,
                            BuildConfig.APPLICATION_ID + ".fileprovider",
                            file
                    );
                    notificationSystemUpdateAlbum(uri, context);
                    PreferenceUtil.commitString(LAST_SHARE_URI, fileName);
                    return uri;
                } else {
                    Uri uri = Uri.fromFile(file);
                    notificationSystemUpdateAlbum(uri, context);
                    PreferenceUtil.commitString(LAST_SHARE_URI, fileName);
                    return uri;
                }
            }
        } catch (FileNotFoundException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        } catch (IOException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        } finally {
            if (!bitmap.isRecycled()) {
                bitmap.recycle();
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ioe) {
                    Log.e(TAG, Log.getStackTraceString(ioe));
                }
            }
        }
        return null;
    }

    /**
     * 保存小票预览图片
     *
     * @param context
     * @param fileName
     * @param v
     * @return
     */
    public static boolean saveTicketPreViewBitmapFile(Context context, String fileName, View v) {
        Bitmap bitmap = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(Color.WHITE);
        v.draw(canvas);
        File file;
        FileOutputStream out = null;
        fileName += "_" + System.currentTimeMillis() + ".jpg";
        try {
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                // 适配android 10
                Uri photoUri = createImageUri(context, fileName);
                notificationSystemUpdateAlbum(photoUri, context);
                if (photoUri != null) {
                    //若生成了uri，则表示该文件添加成功
                    //使用流将内容写入该uri中即可
                    OutputStream outputStream = context.getContentResolver().openOutputStream(photoUri);
                    if (outputStream != null) {
                        bitmap.compress(CompressFormat.JPEG, 100, outputStream);
                        outputStream.flush();
                        outputStream.close();
                        return true;
                    }
                }
            } else {
                // 适配android 9.0以及以下
                String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getPath();
                File dir = new File(path + "/db_spay", Environment.DIRECTORY_DCIM);
                if (!dir.exists()) {
                    boolean isSuccess = dir.mkdirs();
                    //TODO FIX ZHANGXINCHAO 主要是解决扫描漏洞 不处理返回值的问题
                    if (isSuccess) {
                    } else {
                    }
                }
                file = new File(dir, fileName);
                out = new FileOutputStream(file);
                bitmap.compress(CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
                MediaScannerConnection.scanFile(MainApplication.getInstance(), new String[]{file.getPath()}, null, null);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Uri uri = FileProvider.getUriForFile(
                            context,
                            BuildConfig.APPLICATION_ID + ".fileprovider",
                            file
                    );
                    notificationSystemUpdateAlbum(uri, context);
                } else {
                    Uri uri = Uri.fromFile(file);
                    notificationSystemUpdateAlbum(uri, context);
                }
                return true;
            }
        } catch (FileNotFoundException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        } catch (IOException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        } finally {
            if (!bitmap.isRecycled()) {
                bitmap.recycle();
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ioe) {
                    Log.e(TAG, Log.getStackTraceString(ioe));
                }
            }
        }
        return false;
    }

    public static String saveViewBitmapFile(String fileName, View v, Context context) {
        Bitmap bitmap = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        v.draw(canvas);
        File file;
        FileOutputStream out = null;
        try {
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                // 适配android 10
                Uri photoUri = createImageUri(context, fileName);
                if (photoUri != null) {
                    //若生成了uri，则表示该文件添加成功
                    //使用流将内容写入该uri中即可
                    OutputStream outputStream = context.getContentResolver().openOutputStream(photoUri);
                    if (outputStream != null) {
                        bitmap.compress(Bitmap.CompressFormat.PNG, 90, outputStream);
                        outputStream.flush();
                        outputStream.close();
                    }
                }
                return photoUri.getPath();
            } else {
                // 适配android 9.0以及以下
                String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getPath();
                File dir = new File(path + "/swiftpass_pay", Environment.DIRECTORY_DCIM);
                if (!dir.exists()) {
                    boolean isSuccess = dir.mkdirs();
                    //TODO FIX ZHANGXINCHAO 主要是解决扫描漏洞 不处理返回值的问题
                    if (isSuccess) {

                    } else {

                    }
                }
                file = new File(dir, fileName + System.currentTimeMillis() + ".jpg");
                out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
                out.flush();
                out.close();
                MediaScannerConnection.scanFile(MainApplication.getInstance(), new String[]{file.getPath()}, null, null);

                return file.getPath();

            }

        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        } finally {
            if (!bitmap.isRecycled()) {
                bitmap.recycle();
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ioe) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            ioe,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(ioe));
                }
            }
        }
        return null;
    }

    public static String saveWebViewBitmapFile(String fileName, View webView, Context context) {
        Bitmap bitmap = viewShot(webView);
        File file;
        FileOutputStream out = null;
        try {
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                // 适配android 10
                Uri photoUri = createImageUri(context, fileName);
                if (photoUri != null) {
                    //若生成了uri，则表示该文件添加成功
                    //使用流将内容写入该uri中即可
                    OutputStream outputStream = context.getContentResolver().openOutputStream(photoUri);
                    if (outputStream != null) {
                        bitmap.compress(Bitmap.CompressFormat.PNG, 90, outputStream);
                        outputStream.flush();
                        outputStream.close();
                    }
                }
                return photoUri.getPath();
            } else {
                // 适配android 9.0以及以下
                String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getPath();
                File dir = new File(path + "/swiftpass_pay", Environment.DIRECTORY_DCIM);
                if (!dir.exists()) {
                    boolean isSuccess = dir.mkdirs();
                    //TODO FIX ZHANGXINCHAO 主要是解决扫描漏洞 不处理返回值的问题
                    if (isSuccess) {

                    } else {

                    }
                }
                file = new File(dir, fileName + System.currentTimeMillis() + ".jpg");
                out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
                out.flush();
                out.close();
                MediaScannerConnection.scanFile(MainApplication.getInstance(), new String[]{file.getPath()}, null, null);

                return file.getPath();

            }

        } catch (FileNotFoundException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
        } catch (IOException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
        } finally {
            if (!bitmap.isRecycled()) {
                bitmap.recycle();
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ioe) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            ioe,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(ioe));
                }
            }
        }
        return null;
    }
    
   /* public static String saveViewBitmapFile(View v)
    {
        String fileName = "QCODE_" + Utils.formatYM(new Date().getTime()) + ".jpg";
        return saveViewBitmapFile(fileName, v);
    }*/

    public static Bitmap viewShot(final View view) {
        if (view == null)
            return null;
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(measureSpec, measureSpec);
        if (view.getMeasuredWidth() <= 0 || view.getMeasuredHeight() <= 0) {
            return null;
        }
        Bitmap bm;
        try {
            bm = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError e) {
            SentryUtils.INSTANCE.uploadTryCatchError(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
//            System.gc();
            try {
                bm = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            } catch (OutOfMemoryError ee) {
                SentryUtils.INSTANCE.uploadTryCatchError(
                        ee,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                return null;
            }
        }
        Canvas bigCanvas = new Canvas(bm);
        Paint paint = new Paint();
        int iHeight = bm.getHeight();
        bigCanvas.drawBitmap(bm, 0, iHeight, paint);
        view.draw(bigCanvas);
        return bm;
    }

    public static String getSDcardPath() {
        String state = Environment.getExternalStorageState();
        if (TextUtils.equals(Environment.MEDIA_MOUNTED, state)) {
            File sdcardDir = Environment.getExternalStorageDirectory();
            return sdcardDir.getPath();
        }
        return null;
    }

    /**
     * 创建图片地址uri,用于保存拍照后的照片 Android 10以后使用这种方法
     */
    //将文件保存到公共的媒体文件夹
    //这里的filename单纯的指文件名，不包含路径
    public static Uri createImageUri(Context mContext, String fileName) {
        if (!fileName.endsWith(".jpg") || !fileName.endsWith(".png")) {
            fileName = fileName + ".jpg";
        }
        //设置保存参数到ContentValues中
        ContentValues contentValues = new ContentValues();
        //设置文件名
        contentValues.put(MediaStore.Images.Media.DISPLAY_NAME, fileName);
        //android Q中不再使用DATA字段，而用RELATIVE_PATH代替
        //RELATIVE_PATH是相对路径不是绝对路径
        //DCIM是系统文件夹，关于系统文件夹可以到系统自带的文件管理器中查看，不可以写没存在的名字
        contentValues.put(MediaStore.Images.Media.RELATIVE_PATH, Environment.DIRECTORY_DCIM);

        String status = Environment.getExternalStorageState();
        // 判断是否有SD卡,优先使用SD卡存储,当没有SD卡时使用手机存储
        if (status.equals(Environment.MEDIA_MOUNTED)) {
            return mContext.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
        } else {
            return mContext.getContentResolver().insert(MediaStore.Images.Media.INTERNAL_CONTENT_URI, contentValues);
        }

    }

    public static boolean isContainsCurrentImageFile(Context mContext, String fileName) {
        ;
        Uri uri = null;
        String status = Environment.getExternalStorageState();
        // 判断是否有SD卡,优先使用SD卡存储,当没有SD卡时使用手机存储
        if (status.equals(Environment.MEDIA_MOUNTED)) {
            uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        } else {
            uri = MediaStore.Images.Media.INTERNAL_CONTENT_URI;
        }
        Cursor query = mContext.getContentResolver().query(uri,
                new String[]{MediaStore.Images.Media.DISPLAY_NAME},
                MediaStore.Images.Media.DISPLAY_NAME + "=?",
                new String[]{fileName + ".jpg"},
                null);

        if (query != null && query.getCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 将字符互传转换成Bitmap
     *
     * @param opts
     * @return Bitmap
     */
    public static Bitmap getPicFromBytes(String strPic, BitmapFactory.Options opts) {
        if (TextUtils.isEmpty(strPic)) {
            return null;
        }
        byte[] pic = strPic.getBytes();
        if (pic != null) {
            if (opts != null) {
                return BitmapFactory.decodeByteArray(pic, 0, pic.length,
                        opts);
            } else {
                return BitmapFactory.decodeByteArray(pic, 0, pic.length);
            }
        }
        return null;
    }

    /**
     * base64转为bitmap
     *
     * @param base64Data
     * @return
     */
    public static Bitmap base64ToBitmap(String base64Data) {
        byte[] bytes = Base64.decode(base64Data, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }


//    apk安装路径为 /data/data/，沙盒路径 /sdcard/Android/data/xxx 不做操作安装的同时不会立即生成

    /*
     * bitmap转base64
     * */
    public static String bitmapToBase64(Bitmap bitmap) {
        String result = null;
        ByteArrayOutputStream baos = null;
        try {
            if (bitmap != null) {
                baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                baos.flush();
                baos.close();

                byte[] bitmapBytes = baos.toByteArray();
                result = android.util.Base64.encodeToString(bitmapBytes, android.util.Base64.DEFAULT);
            }
        } catch (IOException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        } finally {
            try {
                if (baos != null) {
                    baos.flush();
                    baos.close();
                }
            } catch (IOException e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
            }
        }
        return result;
    }

    /**
     * storage location: sdcard/Android/data/packagename
     * 存储图像至沙盒
     *
     * @param context
     * @param fileName
     * @param image
     * @param environmentType
     * @param dirName
     */
    public static void saveImage2SandBox(Context context, String fileName, byte[] image, String environmentType, String dirName) {
        File standardDirectory;
        String dirPath;

        if (TextUtils.isEmpty(fileName) || 0 == image.length) {
            Log.e(TAG, "saveImage2SandBox: fileName is null or image is null!");
            return;
        }

        if (!TextUtils.isEmpty(environmentType)) {
            standardDirectory = context.getExternalFilesDir(environmentType);
        } else {
            standardDirectory = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        }

        if (!TextUtils.isEmpty(dirName)) {
            dirPath = standardDirectory + "/" + dirName;
        } else {
            dirPath = String.valueOf(standardDirectory);
        }

        File imageFileDirctory = new File(dirPath);
        if (!imageFileDirctory.exists()) {
            if (!imageFileDirctory.mkdir()) {
                Log.e(TAG, "saveImage2SandBox: mkdir failed! Directory: " + dirPath);
                return;
            }
        }

//        if (queryImageFromSandBox(context, fileName, environmentType, dirName)) {
//            Log.e(TAG, "saveImage2SandBox: The file with the same name already exists！");
//            return;
//        }

        try {
            File imageFile = new File(dirPath + "/" + fileName);
            FileOutputStream fileOutputStream = new FileOutputStream(imageFile);
            fileOutputStream.write(image);
            fileOutputStream.flush();
            FileUtils.closeIO(fileOutputStream);
        } catch (IOException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
    }

    /**
     * storage location: sdcard/Android/data/packagename
     * 沙盒中加载图像
     *
     * @param context
     * @param fileName
     * @param environmentType
     * @param dirName
     * @return
     */
    public static byte[] loadImageFromSandBox(Context context, String fileName, String environmentType, String dirName) {
        String type;
        String dirPath;

        if (TextUtils.isEmpty(fileName)) {
            Log.e(TAG, "loadImageFromSandBox: fileName is null");
            return null;
        }

        if (!TextUtils.isEmpty(environmentType)) {
            type = environmentType;
        } else {
            type = Environment.DIRECTORY_PICTURES;
        }

        File standardDirectory = context.getExternalFilesDir(type);
        if (null == standardDirectory) {
            return null;
        }

        if (!TextUtils.isEmpty(dirName)) {
            dirPath = standardDirectory + "/" + dirName;
        } else {
            dirPath = String.valueOf(standardDirectory);
        }

        File direction = new File(dirPath);
        File[] files = direction.listFiles();
        if (files != null) {
            for (File file : files) {
                String name = file.getName();   // 此处的文件名不带路径
                if (file.isFile() && fileName.equals(name)) {
                    try {
                        InputStream inputStream = new FileInputStream(file);
                        byte[] image = new byte[inputStream.available()];
                        inputStream.read(image);
                        FileUtils.closeIO(inputStream);
                        return image;
                    } catch (FileNotFoundException e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                    } catch (IOException e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                    } catch (Exception e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                    }
                }
            }
        }
        return null;
    }

    /**
     * 存储图像至公共目录
     *
     * @param context
     * @param fileName just file name, not include path
     * @param image
     * @param subDir   sub direction name, not absolute path
     */
    public static void saveImage2Public(Context context, String fileName, byte[] image, String subDir) {
        String subDirection;
        if (!TextUtils.isEmpty(subDir)) {
            if (subDir.endsWith("/")) {
                subDirection = subDir.substring(0, subDir.length() - 1);
            } else {
                subDirection = subDir;
            }
        } else {
            subDirection = Environment.DIRECTORY_DCIM;
        }

        Cursor cursor = searchImageFromPublic(context, subDir, fileName);
        if (cursor != null && cursor.moveToFirst()) {
            try {
                int index = cursor.getColumnIndex(MediaStore.Images.Media._ID);
                int id = cursor.getInt(Math.max(index, 0));                     // uri的id，用于获取图片
                Uri uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + id);
                Uri contentUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);
                if (uri != null) {
                    OutputStream outputStream = context.getContentResolver().openOutputStream(uri);
                    if (outputStream != null) {
                        outputStream.write(image);
                        outputStream.flush();
                        outputStream.close();
                    }
                }
                return;
            } catch (IOException e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
            }
        }

        try {
            //设置保存参数到ContentValues中
            ContentValues contentValues = new ContentValues();
            //设置文件名
            contentValues.put(MediaStore.Images.Media.DISPLAY_NAME, fileName);
            //兼容Android Q和以下版本
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                //android Q中不再使用DATA字段，而用RELATIVE_PATH代替
                //RELATIVE_PATH是相对路径不是绝对路径
                //关于系统文件夹可以到系统自带的文件管理器中查看，不可以写没存在的名字
                contentValues.put(MediaStore.Images.Media.RELATIVE_PATH, subDirection);
                //contentValues.put(MediaStore.Images.Media.RELATIVE_PATH, "Music/sample");
            } else {
                contentValues.put(MediaStore.Images.Media.DATA, Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath());
            }
            //设置文件类型
            contentValues.put(MediaStore.Images.Media.MIME_TYPE, "image/JPEG");
            //执行insert操作，向系统文件夹中添加文件
            //EXTERNAL_CONTENT_URI代表外部存储器，该值不变
            Uri uri = context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
            if (uri != null) {
                //若生成了uri，则表示该文件添加成功
                //使用流将内容写入该uri中即可
                OutputStream outputStream = context.getContentResolver().openOutputStream(uri);
                if (outputStream != null) {
                    outputStream.write(image);
                    outputStream.flush();
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
    }

    /**
     * 公共目录加载图像
     *
     * @param context
     * @param filePath relative path in Q, such as: "DCIM/" or "DCIM/dir_name/"
     *                 absolute path before Q
     * @return
     */
    public static byte[] loadImageFromPublic(Context context, String filePath, String fileName) {
        Cursor cursor = searchImageFromPublic(context, filePath, fileName);

        try {
            if (cursor != null && cursor.moveToFirst()) {
                //循环取出所有查询到的数据
                do {
                    //一张图片的基本信息
                    int indexId = cursor.getColumnIndex(MediaStore.Images.Media._ID);
                    int id = cursor.getInt(Math.max(indexId, 0));                     // uri的id，用于获取图片
//                    String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.RELATIVE_PATH));   // 图片的相对路径
//                    String type = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.MIME_TYPE));       // 图片类型
                    int indexName = cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME);
                    String name = cursor.getString(Math.max(indexName, 0));    // 图片名字
                    Log.d(TAG, "loadImageFromPublic: id = " + id);
                    Log.d(TAG, "loadImageFromPublic: name = " + name);
                    //根据图片id获取uri，这里的操作是拼接uri
                    Uri uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + id);
                    //官方代码：
                    Uri contentUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);
                    if (uri != null) {
                        byte[] image;
                        InputStream inputStream = context.getContentResolver().openInputStream(uri);
                        if (null == inputStream || 0 == inputStream.available()) {
                            return null;
                        }
                        image = new byte[inputStream.available()];
                        inputStream.read(image);
                        inputStream.close();
                        return image;
                    }
                } while (cursor.moveToNext());
            }

            if (cursor != null) {
                cursor.close();
            }
        } catch (IOException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
        return null;
    }

    /**
     * 公共目录删除图像
     *
     * @param context
     * @param filePath relative path in Q, such as: "DCIM/" or "DCIM/dir_name/"
     *                 absolute path before Q
     * @return
     */
    public static void deleteImageFromPublic(Context context, String filePath, String fileName) {
        Cursor cursor = searchImageFromPublic(context, filePath, fileName);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                int index = cursor.getColumnIndex(MediaStore.Images.Media._ID);
                int id = cursor.getInt(Math.max(index, 0));                     // uri的id，用于获取图片
//                String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.RELATIVE_PATH));   // 图片的相对路径
//                String type = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.MIME_TYPE));       // 图片类型
//                String name = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME));    // 图片名字
                context.getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, MediaStore.Images.Media._ID + " LIKE ?", new String[]{String.valueOf(id)});
            } while (cursor.moveToNext());
        }

        if (cursor != null) {
            cursor.close();
        }
    }

    /**
     * 公共目录查询图像
     *
     * @param context
     * @param filePath relative path in Q, such as: "DCIM/" or "DCIM/dir_name/"
     *                 absolute path before Q
     * @return
     */
    private static Cursor searchImageFromPublic(Context context, String filePath, String fileName) {
        if (TextUtils.isEmpty(fileName)) {
            Log.e(TAG, "searchImageFromPublic: fileName is null");
            return null;
        }
        if (TextUtils.isEmpty(filePath)) {
            filePath = Environment.DIRECTORY_DCIM + "/";
        } else {
            if (!filePath.endsWith("/")) {
                filePath = filePath + "/";
            }
        }

        //兼容androidQ和以下版本
        String queryPathKey = android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q ? MediaStore.Images.Media.RELATIVE_PATH : MediaStore.Images.Media.DATA;
        String selection = queryPathKey + "=? and " + MediaStore.Images.Media.DISPLAY_NAME + "=?";
        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Images.Media._ID, queryPathKey, MediaStore.Images.Media.MIME_TYPE, MediaStore.Images.Media.DISPLAY_NAME},
                selection,
                new String[]{filePath, fileName},
                null);

        return cursor;
    }

    /**
     * 读写普通文件，例如txt
     * 存储普通文件至公共目录
     *
     * @param context
     * @param fileName just file name, not include path
     * @param subDir   sub direction name, not absolute path
     */
    public static void saveTxt2Public(Context context, String fileName, String content, String subDir) {
        String subDirection;
        if (!TextUtils.isEmpty(subDir)) {
            if (subDir.endsWith("/")) {
                subDirection = subDir.substring(0, subDir.length() - 1);
            } else {
                subDirection = subDir;
            }
        } else {
            subDirection = "Documents";
        }

        Cursor cursor = searchTxtFromPublic(context, subDir, fileName);
        if (cursor != null && cursor.moveToFirst()) {
            try {
                int index = cursor.getColumnIndex(MediaStore.Files.FileColumns._ID);
                int id = cursor.getInt(Math.max(index, 0));
                Uri uri = Uri.withAppendedPath(MediaStore.Files.getContentUri(MediaStore.VOLUME_EXTERNAL), "" + id);
                Uri contentUri = ContentUris.withAppendedId(MediaStore.Files.getContentUri(MediaStore.VOLUME_EXTERNAL), id);
                if (uri != null) {
                    OutputStream outputStream = context.getContentResolver().openOutputStream(uri);
                    if (outputStream != null) {
                        outputStream.write(content.getBytes());
                        outputStream.flush();
                        outputStream.close();
                    }
                }
                return;
            } catch (IOException e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
            }
        }

        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(MediaStore.Files.FileColumns.DISPLAY_NAME, fileName);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                contentValues.put(MediaStore.Files.FileColumns.RELATIVE_PATH, subDirection);
            } else {

            }
            //设置文件类型
            contentValues.put(MediaStore.Files.FileColumns.MEDIA_TYPE, MediaStore.Files.FileColumns.MEDIA_TYPE_NONE);
            Uri uri = context.getContentResolver().insert(MediaStore.Files.getContentUri(MediaStore.VOLUME_EXTERNAL), contentValues);
            if (uri != null) {
                OutputStream outputStream = context.getContentResolver().openOutputStream(uri);
                if (outputStream != null) {
                    outputStream.write(content.getBytes());
                    outputStream.flush();
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
    }

    /**
     * 公共目录查询普通文件
     *
     * @param context
     * @param filePath relative path in Q, such as: "DCIM/" or "DCIM/dir_name/"
     *                 absolute path before Q
     * @return
     */
    private static Cursor searchTxtFromPublic(Context context, String filePath, String fileName) {
        if (TextUtils.isEmpty(fileName)) {
            Log.e(TAG, "searchTxtFromPublic: fileName is null");
            return null;
        }
        if (!filePath.endsWith("/")) {
            filePath = filePath + "/";
        }

        String queryPathKey = MediaStore.Files.FileColumns.RELATIVE_PATH;
        String selection = queryPathKey + "=? and " + MediaStore.Files.FileColumns.DISPLAY_NAME + "=?";
        Cursor cursor = context.getContentResolver().query(MediaStore.Files.getContentUri(MediaStore.VOLUME_EXTERNAL),
                new String[]{MediaStore.Files.FileColumns._ID, queryPathKey, MediaStore.Files.FileColumns.DISPLAY_NAME},
                selection,
                new String[]{filePath, fileName},
                null);

        return cursor;
    }

    /**
     * storage location: /data/data/packagename
     * 安装路径加载图像
     *
     * @param filePath
     * @return
     */
    public static byte[] loadImageFromSandBox2(String filePath) {
        byte[] image = null;
        try {
            InputStream inputStream = new FileInputStream(filePath);
            image = new byte[inputStream.available()];
            inputStream.read(image);
            FileUtils.closeIO(inputStream);
            return image;
        } catch (FileNotFoundException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        } catch (IOException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
        return image;
    }

    /**
     * storage location: /data/data/packagename
     * 存储图像至安装路径
     *
     * @param filePath
     * @param image
     */
    public static void saveImage2SandBox2(String filePath, byte[] image) {
        try {
            File imageFile = new File(filePath);
            FileOutputStream fileOutputStream = new FileOutputStream(imageFile);
            fileOutputStream.write(image);
            fileOutputStream.flush();
            FileUtils.closeIO(fileOutputStream);
        } catch (IOException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
    }

//    public static Uri openSystemCamera(Activity activity, String fileName) {
//        //兼容Android Q和以下版本
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//            Intent intent_camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            Uri photoUri = createImageUri(activity, fileName);//拍照文件保存路径
//            intent_camera.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
//            activity.startActivityForResult(intent_camera, REQUESTCODE_TAKE_PICTURE);
//
//            return photoUri;
//
//        } else {
//            Intent intent_camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getPath();
//            File dir = new File(path + "/swiftpass_pay", Environment.DIRECTORY_DCIM);
//
//            if (!dir.exists()) {
//                dir.mkdirs();
//            }
//            File file = new File(dir, fileName + ".jpg");
//            Uri photoUri = getUriForFile(activity, file);
//            intent_camera.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
//            activity.startActivityForResult(intent_camera, REQUESTCODE_TAKE_PICTURE);
//
//            return photoUri;
//        }
//
//    }


    public static Uri getUriForFile(Context context, File file) {
        if (context == null || file == null) {
            throw new NullPointerException();
        }
        Uri uri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            uri = FileProvider.getUriForFile(context.getApplicationContext(), BuildConfig.APPLICATION_ID + ".fileprovider", file);
        } else {
            uri = Uri.fromFile(file);
        }
        return uri;
    }

    public static Uri saveImageToGalleryUriByScopedStorage(Context context, Bitmap bmp, String name) {
        // 首先保存图片 必须带文件后缀
        String fileName = name + ".jpg";
        OutputStream outputStream = null;
        FileOutputStream fos = null;
        try {
            //设置保存参数到ContentValues中
            ContentValues contentValues = new ContentValues();
            //设置文件名
            contentValues.put(MediaStore.Images.Media.DISPLAY_NAME, fileName);
            //设置文件类型
            contentValues.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
            //兼容Android Q和以下版本
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                //android Q中不再使用DATA字段，而用RELATIVE_PATH代替
                //RELATIVE_PATH是相对路径不是绝对路径
                //DCIM是系统文件夹，关于系统文件夹可以到系统自带的文件管理器中查看，不可以写没存在的名字
                contentValues.put(MediaStore.Images.Media.RELATIVE_PATH, Environment.DIRECTORY_DCIM);
                //因为insert 如果uri存在的话，会返回null 所以需要先delete
                Cursor query = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        new String[]{MediaStore.Images.Media.DISPLAY_NAME},
                        MediaStore.Images.Media.DISPLAY_NAME + "=?",
                        new String[]{fileName},
                        null
                );
                if (query != null && query.getCount() > 0) {
                    context.getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            MediaStore.Images.Media.DISPLAY_NAME + "=?",
                            new String[]{fileName});

                }
                //执行insert操作，向系统文件夹中添加文件
                //EXTERNAL_CONTENT_URI代表外部存储器，该值不变
                Uri uri = context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
                if (uri != null) {
                    //若生成了uri，则表示该文件添加成功
                    //使用流将内容写入该uri中即可
                    outputStream = context.getContentResolver().openOutputStream(uri);
                    if (outputStream != null) {
                        boolean isSuccess = bmp.compress(Bitmap.CompressFormat.JPEG, 60, outputStream);
                        outputStream.flush();
                        outputStream.close();
                        return uri;
                    }
                }
            } else {
                String storePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "/swiftpass_pay";
                File appDir = new File(storePath);
                if (!appDir.exists()) {
                    appDir.mkdir();
                }
                File file = new File(appDir, fileName);

                fos = new FileOutputStream(file);
                //通过io流的方式来压缩保存图片
                boolean isSuccess = bmp.compress(Bitmap.CompressFormat.JPEG, 60, fos);
                fos.flush();
                //保存图片后发送广播通知更新数据库
                Uri uri = Uri.fromFile(file);
                context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
                if (isSuccess) {
                    return uri;
                } else {
                    return null;
                }
            }

        } catch (IOException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        } finally {
            bmp.recycle();// 对原图进行回收
            FileUtils.closeIO(outputStream);
            FileUtils.closeIO(fos);
        }
        return null;
    }

    public static Bitmap getBitmapFromPath(Context context, Uri uri) {
        return getBitmapFromPath(context, getRealPathFromURI(context, uri));
    }

    public static Bitmap getBitmapFromPath(Context context, String path) {
        if (TextUtils.isEmpty(path) || context == null) {
            return null;
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap original = BitmapFactory.decodeFile(path, options);
        Bitmap bitmap = null;

        try {
            ExifInterface exif = new ExifInterface(path);
            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            Matrix matrix = new Matrix();
            int rotationInDegrees = ImageUtil.exifToDegrees(rotation);
            if (rotation != 0f) {
                matrix.preRotate(rotationInDegrees);
            }

            // 图片太大会导致内存泄露，所以在显示前对图片进行裁剪。
            int maxPreviewImageSize = 2560;

            int min = Math.min(options.outWidth, options.outHeight);
            min = Math.min(min, maxPreviewImageSize);

            WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            Point screenSize = new Point();
            windowManager.getDefaultDisplay().getSize(screenSize);
            min = Math.min(min, screenSize.x * 2 / 3);

            options.inSampleSize = ImageUtil.calculateInSampleSize(options, min, min);
            options.inScaled = true;
            options.inDensity = options.outWidth;
            options.inTargetDensity = min * options.inSampleSize;

            options.inJustDecodeBounds = false;
            bitmap = BitmapFactory.decodeFile(path, options);
        } catch (IOException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            bitmap = original;
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
        return bitmap;
    }

    public static String getRealPathFromURI(Context context, Uri uri) {
        if (context == null || uri == null) {
            return "";
        }
        String result;
        Cursor cursor = context.getContentResolver().query(uri, null,
                null, null, null);
        if (cursor == null) {
            result = uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    // Returns the degrees in clockwise. Values are 0, 90, 180, or 270.
    public static int getOrientation(byte[] jpeg) {
        if (jpeg == null) {
            return 0;
        }

        int offset = 0;
        int length = 0;

        // ISO/IEC 10918-1:1993(E)
        while (offset + 3 < jpeg.length && (jpeg[offset++] & 0xFF) == 0xFF) {
            int marker = jpeg[offset] & 0xFF;

            // Check if the marker is a padding.
            if (marker == 0xFF) {
                continue;
            }
            offset++;

            // Check if the marker is SOI or TEM.
            if (marker == 0xD8 || marker == 0x01) {
                continue;
            }
            // Check if the marker is EOI or SOS.
            if (marker == 0xD9 || marker == 0xDA) {
                break;
            }

            // Get the length and check if it is reasonable.
            length = pack(jpeg, offset, 2, false);
            if (length < 2 || offset + length > jpeg.length) {
                Log.e(TAG, "Invalid length");
                return 0;
            }

            // Break if the marker is EXIF in APP1.
            if (marker == 0xE1 && length >= 8
                    && pack(jpeg, offset + 2, 4, false) == 0x45786966
                    && pack(jpeg, offset + 6, 2, false) == 0) {
                offset += 8;
                length -= 8;
                break;
            }

            // Skip other markers.
            offset += length;
            length = 0;
        }

        // JEITA CP-3451 Exif Version 2.2
        if (length > 8) {
            // Identify the byte order.
            int tag = pack(jpeg, offset, 4, false);
            if (tag != 0x49492A00 && tag != 0x4D4D002A) {
                Log.e(TAG, "Invalid byte order");
                return 0;
            }
            boolean littleEndian = (tag == 0x49492A00);

            // Get the offset and check if it is reasonable.
            int count = pack(jpeg, offset + 4, 4, littleEndian) + 2;
            if (count < 10 || count > length) {
                Log.e(TAG, "Invalid offset");
                return 0;
            }
            offset += count;
            length -= count;

            // Get the count and go through all the elements.
            count = pack(jpeg, offset - 2, 2, littleEndian);
            while (count-- > 0 && length >= 12) {
                // Get the tag and check if it is orientation.
                tag = pack(jpeg, offset, 2, littleEndian);
                if (tag == 0x0112) {
                    // We do not really care about type and count, do we?
                    int orientation = pack(jpeg, offset + 8, 2, littleEndian);
                    switch (orientation) {
                        case 1:
                            return 0;
                        case 3:
                            return 180;
                        case 6:
                            return 90;
                        case 8:
                            return 270;
                        default:
                            return 0;
                    }
                }
                offset += 12;
                length -= 12;
            }
        }

        Log.i(TAG, "Orientation not found");
        return 0;
    }

    private static int pack(byte[] bytes, int offset, int length,
                            boolean littleEndian) {
        int step = 1;
        if (littleEndian) {
            offset += length - 1;
            step = -1;
        }

        int value = 0;
        while (length-- > 0) {
            value = (value << 8) | (bytes[offset] & 0xFF);
            offset += step;
        }
        return value;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

}
