package com.cxy.networklibrary.other;

import java.io.File;

/**
 * 创建人：caoxiaoya
 * 时间：2018/6/13
 * 描述：其他模块的配置
 * 备注：
 */

public class OtherConfig {

    private File mCrashDiaryFolder;
    private boolean mIsUseCrashDiary;
    private boolean mIsShowLog;

    public File getCrashDiaryFolder() {
        return mCrashDiaryFolder;
    }

    //设置崩溃日志的地址，传入的file需为文件夹，默认保存在/storage/emulated/0/Android/data/com.xxx.xxx/cache/crash_log下
    public OtherConfig setCrashDiaryFolder(File fileDirectory) {
        this.mCrashDiaryFolder = fileDirectory;
        return this;
    }

    public boolean isUseCrashDiary() {
        return mIsUseCrashDiary;
    }

    //设置是否开启崩溃日志功能，默认不开启
    public OtherConfig setIsUseCrashDiary(boolean isUseCrashDiary) {
        mIsUseCrashDiary = isUseCrashDiary;
        return this;
    }

    public boolean isShowLog() {
        return mIsShowLog;
    }

    //设置是否显示Ringlog打印的内容，默认true
    public OtherConfig setIsShowLog(boolean isShowLog) {
        mIsShowLog = isShowLog;
        return this;
    }
}
