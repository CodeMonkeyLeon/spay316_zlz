package cn.swiftpass.enterprise.ui.test.entity

import com.chad.library.adapter.base.entity.MultiItemEntity

data class DomainEntity(
    var domain: String = ""
) : java.io.Serializable, MultiItemEntity {

    companion object {
        const val LAYOUT_TYPE_DOMAIN = 0
    }

    override fun getItemType() = LAYOUT_TYPE_DOMAIN
}
