/*
 * 文 件 名:  ReportActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-8
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.bill;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.bigkoo.pickerview.utils.ShapreUtils;
import com.bigkoo.pickerview.view.TimePickerViews;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.tencent.stat.StatService;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.bill.BillOrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.print.BluePrintUtil;
import cn.swiftpass.enterprise.ui.activity.print.BluetoothSettingActivity;
import cn.swiftpass.enterprise.ui.view.MyTextView;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateTimeUtil;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.GlideApp;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalInfo;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalItemInfo;


import static cn.swiftpass.enterprise.MainApplication.getContext;
import static cn.swiftpass.enterprise.utils.DateUtil.getTime;

/**
 * 报表
 *
 * @author he_hui
 * @version [版本号, 2016-11-8]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class ReportActivity extends TemplateActivity {

    private static final String TAG = ReportActivity.class.getSimpleName();

    private LinearLayout ly_date, ly_user;

    private TextView tv_date, tv_total_pay, tv_order_pay_num, tv_refund_total, tv_refund_total_num;
    private MyTextView tv_total;
    private LinearLayout  ly_pie1;

    private OrderTotalInfo info;

    private LinearLayout ly_order_summary;

    List<String> dateList, thirtyDays;

    private List<OrderTotalItemInfo> barList = new ArrayList<OrderTotalItemInfo>();

    private List<OrderTotalItemInfo> linList = new ArrayList<OrderTotalItemInfo>();

    private ImageView iv_switch;

    private TextView tv_all_user;

    private UserModel userModel;

    private String startTime, endTime, startPrint, endPrint;

    private ListView lv_one_list;

    private ViewHolder holder;

    private ReportAdapter reportAdapter;

    private Map<String, String> colorMap = new HashMap<String, String>();

    private boolean loadNext = false;

    private List<OrderTotalItemInfo> orderTotalItemInfo = new ArrayList<OrderTotalItemInfo>();

    private List<OrderTotalItemInfo> orderTotalItemInfoTwo = new ArrayList<OrderTotalItemInfo>();

//    private Button btn_next_step;
    private LinearLayout ll_tip;
    private TextView tv_tip_amount;
    private TextView tv_tip_count;

    private ShapreUtils mSharePreUtils;

    class ReportAdapter extends BaseAdapter {
        private List<OrderTotalItemInfo> list;

        private Context context;

        private ReportAdapter(Context context, List<OrderTotalItemInfo> list, boolean isNum) {
            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.activity_report_circle_list_item, null);
                holder = new ViewHolder();
                holder.v_line = (View) convertView.findViewById(R.id.v_line);
                holder.tv_pay_type = (TextView) convertView.findViewById(R.id.tv_pay_type);
                holder.tv_money = (TextView) convertView.findViewById(R.id.tv_money);
                holder.tv_num = (TextView) convertView.findViewById(R.id.tv_num);
                holder.iv_type = (ImageView) convertView.findViewById(R.id.iv_type);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            if ((position + 1) == list.size()) {
                holder.v_line.setVisibility(View.GONE);
            } else {
                holder.v_line.setVisibility(View.VISIBLE);
            }

            OrderTotalItemInfo orderTotalItemInfo = list.get(position);
            if (orderTotalItemInfo != null) {

                if (MainApplication.getPayTypeMap() != null && MainApplication.getPayTypeMap().size() > 0) {

                    String typeName = MainApplication.getPayTypeMap().get(String.valueOf(orderTotalItemInfo.getPayTypeId()));

                    holder.tv_pay_type.setText(typeName);
                }

                @SuppressWarnings("unchecked") Map<String, String> typePicMap = (Map<String, String>) SharedPreUtile.readProduct("payTypeIcon" + ApiConstant.bankCode + MainApplication.getMchId());
                if (typePicMap != null && typePicMap.size() > 0) {
                    String url = typePicMap.get(String.valueOf(orderTotalItemInfo.getPayTypeId()));
                    if (!StringUtil.isEmptyOrNull(url)) {
                        GlideApp.with(getContext())
                                .load(url)
                                .placeholder(R.drawable.icon_general_receivables)
                                .error(R.drawable.icon_general_receivables)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .into(holder.iv_type);

                    }
                }
                holder.tv_money.setText(getString(R.string.tx_money) + DateUtil.formatMoneyUtils(orderTotalItemInfo.getSuccessFee()));
                holder.tv_num.setText(getString(R.string.tx_bill_stream_pay_type_num) + "：" + orderTotalItemInfo.getSuccessCount() );
            }

            return convertView;
        }
    }

    class ViewHolder {
        private View v_line;

        private ImageView iv_type;

        TextView tv_pay_type, tv_money, tv_num;
    }

    /**
     * 暂停 后 才去请求
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void sleep(long time) {
        try {
            //            Thread.sleep(time * 1000);

            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.i("hehui", "sleep-->");
                            loadNext = false;
                        }
                    });

                }

            };
            Timer timer = new Timer();
            timer.schedule(task, time * 1000);

        } catch (Exception e) {
            return;
        }
    }

    boolean isOpen = true;

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == HandlerManager.BILL_CHOICE_USER) { //收银员
                try {
                    if (loadNext) { //
                        toastDialog(ReportActivity.this, R.string.tx_request_more, null);
                        return;
                    }

                    userModel = (UserModel) msg.obj;
                    if (userModel != null) {
                        iv_switch.setVisibility(View.VISIBLE);
                        tv_all_user.setVisibility(View.VISIBLE);
                        tv_all_user.setText(userModel.getRealname());
                        if (!StringUtil.isEmptyOrNull(startTime) && !StringUtil.isEmptyOrNull(endTime)) {

                            loadDate(startTime, endTime, 2, userModel.getId() + "", true, null);
                        } else {

                            loadDate(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00", DateUtil.formatTime(System.currentTimeMillis()), 2, userModel.getId() + "", true, null);
                        }
                    } else {
                        iv_switch.setVisibility(View.VISIBLE);
                        tv_all_user.setVisibility(View.VISIBLE);
                        tv_all_user.setText(R.string.tv_all_user);
                        if (!StringUtil.isEmptyOrNull(startTime) && !StringUtil.isEmptyOrNull(endTime)) {
                            loadDate(startTime, endTime, 2, null, true, null);
                        } else {
                            loadDate(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00", DateUtil.formatTime(System.currentTimeMillis()), 2, null, true, null);
                        }
                    }
                } catch (Exception e) {
                    iv_switch.setVisibility(View.VISIBLE);
                    tv_all_user.setVisibility(View.VISIBLE);
                    tv_all_user.setText(R.string.tv_all_user);
                    if (!StringUtil.isEmptyOrNull(startTime) && !StringUtil.isEmptyOrNull(endTime)) {
                        loadDate(startTime, endTime, 2, null, true, null);
                    } else {
                        loadDate(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00", DateUtil.formatTime(System.currentTimeMillis()), 2, null, true, null);
                    }
                }
            } else if (msg.what == HandlerManager.BILL_CHOICE_TIME) {//选择时间
                if (loadNext) { //
                    toastDialog(ReportActivity.this, R.string.tx_request_more, null);
                    return;
                }
                String time = (String) msg.obj;
                if (!StringUtil.isEmptyOrNull(time)) {
                    String t[] = time.split("\\|");
                      /*  tv_date.setText(DateUtil.formartDateToYYMMDD(t[0]) + " " + getString(R.string.tv_least) + " " + DateUtil.formartDateToMMDD(t[1]));
                        startPrint = DateUtil.formartDateToYYMMDD(t[0]);
                        endPrint = DateUtil.formartDateToYYMMDD(t[1]);*/
                    startPrint = startTime;
                    endPrint = endTime;
                    if (userModel != null) {
                        startTime = t[0];
                        endTime = t[1];
                        loadDate(startTime, endTime, 2, userModel.getId() + "", true, null);
                    } else {
                        startTime = t[0];
                        endTime = t[1];
                        loadDate(startTime, endTime, 2, null, true, null);
                    }
                }
            } else if (msg.what == HandlerManager.BLUE_CONNET_STUTS) {
//                btn_next_step.setEnabled(false);
                if (isOpen) {
                    isOpen = false;
                    //                    bluePrint();
                }
//                btn_next_step.setEnabled(true);
            }
        }

        ;
    };

    /**
     * 动态设置ListView的高度
     *
     * @param listView
     */
    public void setListViewHeightBasedOnChildren(ListView listView) {
        if (listView == null) return;
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_main_view);
        //        page = getViewById(R.id.pager);
        //        page.setScrollble(false);
        initview();

        setLister();
        initLabel();
               //从本地缓存里面取服务器地址
        PreferenceUtil.init(this);
        //新增时间控制缓存
        mSharePreUtils = new ShapreUtils(this);
        setDefaultTimeSelct();
        HandlerManager.registerHandler(HandlerManager.BILL_CHOICE_USER, handler);

        dateList = DateTimeUtil.getAllThirtyDays();
        thirtyDays = DateTimeUtil.getThirtyDays();

        if (reportAdapter == null) {
            reportAdapter = new ReportAdapter(ReportActivity.this, orderTotalItemInfo, false);
            lv_one_list.setAdapter(reportAdapter);
        }

        //注册handler 检查蓝牙打开和关闭的操作
        HandlerManager.registerHandler(HandlerManager.BLUE_CONNET_STUTS, handler);
    }

    /**
     * 设置默认的选择时间
     */
    private void setDefaultTimeSelct(){
        if (null != mSharePreUtils){
            mSharePreUtils.setTotime(0L);
            mSharePreUtils.setFromtime(0L);
            mSharePreUtils.setSelect(false);
        }
    }


    /**
     * 蓝牙打印
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    @SuppressLint("NewApi")
    void bluePrint() {
        if (info != null) {
            if (!StringUtil.isEmptyOrNull(startTime) && !StringUtil.isEmptyOrNull(endTime)) {
                try {
                    info.setStartTime(startPrint);
                    info.setEndTime(endPrint);
                } catch (Exception e) {
                    Log.e("hehui", "" + e);
                }
            } else {
                info.setStartTime(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00");
                info.setEndTime(DateUtil.formatTime(System.currentTimeMillis()));
            }

            if (userModel != null) {
                info.setUserName(userModel.getRealname());
            } else {
//                if (MainApplication.isAdmin.equals("0")) {
//                    info.setUserName(MainApplication.realName);
//                }
            }
            if (!MainApplication.getBluePrintSetting()&&!MainApplication.IS_POS_VERSION) {//关闭蓝牙打印
                showDialog();
                return;
            }
            if (MainApplication.bluetoothSocket != null && MainApplication.bluetoothSocket.isConnected()) {
                BluePrintUtil.printDateSum(info);
            } else {
                String bluetoothDeviceAddress = MainApplication.getBlueDeviceAddress();
                if (!StringUtil.isEmptyOrNull(bluetoothDeviceAddress)) {
                    boolean isSucc = BluePrintUtil.blueConnent(bluetoothDeviceAddress, ReportActivity.this);
                    if (isSucc) {
                        BluePrintUtil.printDateSum(info);
                    }
                } else {
                    Log.i("hehui", "showDialog()");
                    showDialog();
                }
            }

        }
    }

    void showDialog() {
        dialog = new DialogInfo(ReportActivity.this, null, getString(R.string.tx_blue_set), getString(R.string.title_setting), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                dialog.cancel();
                dialog.dismiss();
                showPage(BluetoothSettingActivity.class);
            }

            @Override
            public void handleCancleBtn() {
                dialog.cancel();
            }
        }, null);

        DialogHelper.resize(ReportActivity.this, dialog);
        dialog.show();
    }

    private TimePickerViews pvTime;
    //private TimePickerView pvTime2;
    //TimePickerView.Builder builder;
    //Calendar selectedDate = null;
    //Date selectDate = null;

    /*private void initTimePicker() {
        if (selectedDate != null) {
            if (selectDate != null) {
                selectedDate.setTimeInMillis(selectDate.getTime());
            }
        } else {
            selectedDate = Calendar.getInstance();//系统当前时间
        }

        Calendar startDate = Calendar.getInstance();
        //startDate.add(startDate.MONTH, -3);  //设置为前3月
        //startDate.add(startDate.YEAR,-2);
        startDate.add(startDate.DAY_OF_MONTH,-89);
        startDate.set(startDate.get(Calendar.YEAR), startDate.get(Calendar.MONTH), startDate.get(Calendar.DAY_OF_MONTH));
        Calendar endDate = Calendar.getInstance();
        endDate.set(endDate.get(Calendar.YEAR), endDate.get(Calendar.MONTH), endDate.get(Calendar.DAY_OF_MONTH));
        //时间选择器
        builder = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                //  setTime(time, tvDate);
               *//* String picktime = DateUtil.getYMD(date);
                selectDate = date;
                if(!StringUtil.isEmptyOrNull(picktime)){

                    time=picktime;
                    if (DateUtil.isToday(picktime)) {
                        tvDate.setText(DateUtil.formatMD(System.currentTimeMillis())+"("+getStringById(R.string.public_today)+")");
                    } else {
                        tvDate.setText(picktime);
                    }
                    ReportActivity.this.time =picktime;

                }*//*
            }
        })
                .setDate(selectedDate)
                .setRangDate(startDate, endDate)
                .setLayoutRes(R.layout.pickerview_custom_time, new CustomListener() {

                    @Override
                    public void customLayout(View v) {
                        final TextView tvSubmit = (TextView) v.findViewById(R.id.tv_finish);
                        TextView ivCancel = (TextView) v.findViewById(R.id.iv_cancel);
                        tvSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvCustomTime.returnData();
                                pvCustomTime.dismiss();
                            }
                        });
                        ivCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvCustomTime.dismiss();
                            }
                        });
                    }
                })
                .setType(new boolean[]{true, true, true, false, false, false})
                .setLabel("年", "月", "日", "时", "分", "秒")
                .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                .setDividerColor(getResources().getColor(R.color.home_line))
                // .setDividerColor(0xFF24AD9D)
                .setLineSpacingMultiplier(3.0f)
                .setContentSize(15)
                .setSubCalSize(15)
        ;
        pvCustomTime = builder.build();


    }*/

    //设置开始年与结束年
    //private int StartYear = 0;
    Date tempSDate,tempEDate;
    private int EndYear = Calendar.getInstance().get(Calendar.YEAR);
    private void setLister() {
        //打印日结
       /* btn_next_step.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                bluePrint();
            }
        });
*/
        ly_user.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (userModel != null) {
                    UserListActivity.startActivity(ReportActivity.this, userModel,false,false);
                } else {
                    Intent intent = new Intent(ReportActivity.this, UserListActivity.class);
                    intent.putExtra("isFromBillEnter",false);
                    intent.putExtra("isFromCodeListEnter",false);
                    startActivity(intent);
                }
            }
        });

        ly_date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                showPage(DateChoiceActivity.class);
                //时间选择器
                pvTime = new TimePickerViews.Builder(ReportActivity.this, new TimePickerViews.OnTimeSelectListener() {
                    @Override
                    public void onTimeSelect(Date startdate, Date enddate,View v) {//选中事件回调
                        long starttime = startdate.getTime();
                        long endtime = enddate.getTime();
                        long tempstime = 0l;
                        long tempetime = 0l;
                        Log.e("JAMY","starttime: "+starttime+" endtime:"+endtime);
                        if (startdate.getTime() >= enddate.getTime()) {
                            mSharePreUtils.setSelect(false);
                            tempetime = mSharePreUtils.getTotime();
                            tempstime = mSharePreUtils.getFromtime();
                            Log.e("JAMY","tempetime: "+tempetime+" tempstime:"+tempstime);
                            if (0l != tempstime && 0l != tempetime){
                                Log.e("JAMY","etime: "+DateUtil.getAllNormalTime(tempetime)
                                        +" stime:"+DateUtil.getAllNormalTime(tempstime));
                                tempEDate = DateUtil.getStrToDate(DateUtil.getAllNormalTime(tempetime));
                                tempSDate = DateUtil.getStrToDate(DateUtil.getAllNormalTime(tempstime));
                                tv_date.setText("     "+DateUtil.getNormalTime(getTime(tempSDate))+"\n"
                                        +getResources().getString(R.string.data_to)+" "+DateUtil.getNormalTime(getTime(tempEDate)));
                            }else{
                                tv_date.setText(DateUtil.formatMD(System.currentTimeMillis()) + "(" + getString(R.string.public_today) + ")");
                            }
                            toastDialog(ReportActivity.this, R.string.show_endtime_than_starttime, null);
                            return;
                        }
                        startTime = DateUtil.getAllNormalTime(getTime(startdate));
                        endTime = DateUtil.getAllNormalTime(getTime(enddate));
                        mSharePreUtils.setSelect(true);
                        mSharePreUtils.setFromtime(getTime(startdate));
                        mSharePreUtils.setTotime(getTime(enddate));
                        String time = startTime + "|" + endTime;
                        tv_date.setText("     "+DateUtil.getNormalTime(getTime(startdate))+"\n"
                                +getResources().getString(R.string.data_to)+" "+DateUtil.getNormalTime(getTime(enddate)));
                        HandlerManager.notifyMessage(HandlerManager.BILL_CHOICE_USER, HandlerManager.BILL_CHOICE_TIME, time);
                    }
                })
                        .setCancelText(getStringById(R.string.data_select_cancel))//取消按钮文字
                        .setSubmitText(getStringById(R.string.data_select_done))//确认按钮文字
                        .setTitleText(getStringById(R.string.data_title))//标题文字
                        .setLabel(label_year,label_month,label_day,label_hours,label_mins,label_seconds)
                        .isCenterLabel(false)
                        .setLanguage(isEnglish())
                        .setRange(getStartYear(-89),EndYear)
                        .setRangDate(getStartDate(-89),Calendar.getInstance())
                        .setDate(Calendar.getInstance())
                        .build();
                pvTime.show();
            }
        });
    }



    void setValue(OrderTotalInfo info) {
        long moeny = info.getCountTotalFee() - info.getCountTotalRefundFee();

        String totalvalues = MainApplication.feeFh+DateUtil.formatMoneyUtils(moeny);
        if (totalvalues.length() > 10){
            tv_total.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
        }else{
            tv_total.setTextSize(TypedValue.COMPLEX_UNIT_SP, 48);
        }
        tv_total.setText(MainApplication.feeFh+DateUtil.formatMoneyUtils(moeny));

        tv_total_pay.setText(DateUtil.formatMoneyUtils(info.getCountTotalFee()));
        tv_order_pay_num.setText(info.getCountTotalCount() + "");
        tv_refund_total.setText(DateUtil.formatMoneyUtils(info.getCountTotalRefundFee()));
        tv_refund_total_num.setText(info.getCountTotalRefundCount() + "");

    }


    private void initview() {

        ll_tip=(LinearLayout)findViewById(R.id.ll_tip);
        tv_tip_amount=(TextView)findViewById(R.id.tv_tip_amount);
        tv_tip_count=(TextView)findViewById(R.id.tv_tip_count);
        tv_all_user = getViewById(R.id.tv_choice_cashier);
        iv_switch = getViewById(R.id.iv_select_more_cashier);
        tv_total = (MyTextView) findViewById(R.id.tv_total_money);

        ly_order_summary = getViewById(R.id.ly_order_summary);


        ly_pie1 = (LinearLayout) findViewById(R.id.ll_summary_pay_type);

        tv_total_pay = (TextView) findViewById(R.id.tv_total_pay_money);
        tv_order_pay_num = (TextView) findViewById(R.id.tv_order_pay_num);
        tv_refund_total = (TextView) findViewById(R.id.tv_refund_total_money);
        tv_refund_total_num = (TextView) findViewById(R.id.tv_refund_total_num);

        ly_date = (LinearLayout) findViewById(R.id.ll_select_date);
        tv_date = (TextView) findViewById(R.id.tv_choice_date);
        ly_user = (LinearLayout) findViewById(R.id.ll_select_cashier);
//        if (MainApplication.isAdmin.equals("0")) {//收银员
        if ( MainApplication.isOrderAuth.equals("0")) {
            iv_switch.setVisibility(View.GONE);
            tv_all_user.setText(MainApplication.realName);
            ly_user.setEnabled(false);
        }else{
               /* if (MainApplication.isAdmin.equals("0")){
                    tv_all_user.setText(MainApplication.realName);
                }*/
            tv_all_user.setText(R.string.tv_all_user);
            iv_switch.setVisibility(View.VISIBLE);
            ly_user.setEnabled(true);
        }
//        }
        tv_date.setText(DateUtil.formatMD(System.currentTimeMillis()) + "(" + getString(R.string.public_today) + ")");

        loadDate(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00", DateUtil.formatTime(System.currentTimeMillis()), 2, null, true, null);

        lv_one_list = getViewById(R.id.listView_payType);


    }


    private void loadDate(String startTime, String endTime, final int countMethod, String userId, final boolean isTrade, final String countDayKind) {
        BillOrderManager.getInstance().orderCount(startTime, endTime, countMethod, userId, countDayKind, new UINotifyListener<OrderTotalInfo>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                if (isTrade) {
                    loadDialog(ReportActivity.this, R.string.public_data_loading);
                }
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(Object object) {
                super.onError(object);
                dissDialog();
                if (checkSession()) {
                    return;
                }
                if (null != object) {

                    if (!StringUtil.isEmptyOrNull(object.toString())) {

                        toastDialog(ReportActivity.this, object.toString(), new NewDialogInfo.HandleBtn() {

                            @Override
                            public void handleOkBtn() {
                                ReportActivity.this.finish();
                            }

                        });
                    } else {
                        toastDialog(ReportActivity.this, R.string.tx_load_fail, null);
                    }
                }
            }

            @Override
            public void onSucceed(OrderTotalInfo model) {
                dissDialog();
                if (null != model) {
                    //暂停
                    if (model.getReqFeqTime() > 0) {
                        loadNext = true;
                        sleep(model.getReqFeqTime());
                    }

                    if (MainApplication.isTipOpen()){
                        ll_tip.setVisibility(View.VISIBLE);
                        tv_tip_amount.setText(DateUtil.formatMoneyUtils(model.getCountTotalTipFee()));
                        tv_tip_count.setText(String.valueOf(model.getCountTotalTipFeeCount()));
                    }else {
                        ll_tip.setVisibility(View.GONE);
                    }


                    if (countMethod == 2) {
                        orderTotalItemInfo.clear();
                        orderTotalItemInfo.addAll(model.getOrderTotalItemInfo());

                        orderTotalItemInfoTwo.clear();
                        orderTotalItemInfoTwo.addAll(model.getOrderTotalItemInfo());
                        if (reportAdapter != null) {
                            reportAdapter.notifyDataSetChanged();
                            //                                reportAdapterTwo.notifyDataSetChanged();
                        }
                        info = model;

                        setValue(model);
                        if (model.getOrderTotalItemInfo().size() > 0) {
                            ly_pie1.setVisibility(View.VISIBLE);
                            setListViewHeightBasedOnChildren(lv_one_list);
                            lv_one_list.setVisibility(View.VISIBLE);
//                            btn_next_step.setVisibility(View.VISIBLE);
                        } else {
                            ly_pie1.setVisibility(View.GONE);
                            lv_one_list.setVisibility(View.GONE);
                        }
                        //                            createPie(model);
                    } else {
                        //走势图
                      /*  Collections.reverse(model.getOrderTotalItemInfo());

                        if (countDayKind.equals("2")) {//走势图-柱状图
                            createBarOne(model.getOrderTotalItemInfo());
                            createBarTwo(model.getOrderTotalItemInfo());

                            List<String> dateList = DateTimeUtil.getThirtyDaysForYYYYDDMM();
                            loadDate(dateList.get(0) + " 00:00:00", DateUtil.formatYYMD(DateUtil.getBeforeDate().getTime()) + " 23:59:59", 1, null, true, "1");
                        } else {
                            if (model != null && model.getOrderTotalItemInfo().size() > 0) {

                                createLineChartOne(model.getOrderTotalItemInfo());
                                createLineChartTwo(model.getOrderTotalItemInfo());
                            } else {
                                List<String> dateList = DateTimeUtil.getThirtyDaysForYYYYDDMM();
                                loadDate(dateList.get(0) + " 00:00:00", DateUtil.formatYYMD(DateUtil.getBeforeDate().getTime()) + " 23:59:59", 1, null, true, "1");
                            }
                        }*/
                    }
                }
            }
        });
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();

        titleBar.setTitleChoice(false);
        titleBar.setLeftButtonVisible(true);
        titleBar.setLeftButtonIsVisible(true);
        titleBar.setTitle(R.string.tab_sum_title);
        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.print));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButLayClick() {
                try {
                    StatService.trackCustomEvent(ReportActivity.this, "kMTASPayBillSummaryPrint", "汇总/打印");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                bluePrint();
            }
        });
    }

    @Override
    protected void setOnCentreTitleBarClickListener() {
        super.setOnCentreTitleBarClickListener();
        titleBar.setOnCentreTitleBarClickListener(new TitleBar.OnCentreTitleBarClickListener() {
            @Override
            public void onCentreButtonClick(View v) {

            }

            @Override
            public void onTitleRepotLeftClick() {
                //                page.setCurrentItem(0);
                //                ly_order_summary, ly_order_trend
                ly_order_summary.setVisibility(View.VISIBLE);
//                ly_order_trend.setVisibility(View.GONE);

                try {
                    StatService.trackCustomEvent(ReportActivity.this, "SPConstTapReportTotalButton", "汇总");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

            }

            @Override
            public void onTitleRepotRigthClick() {
                try {
                    StatService.trackCustomEvent(ReportActivity.this, "SPConstTapReportTrendButton", "走势");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
                //                page.setCurrentItem(1);
                ly_order_summary.setVisibility(View.GONE);
//                ly_order_trend.setVisibility(View.VISIBLE);
                try {
                    if (barList.size() == 0 || linList.size() == 0) {
                        //                        if (userModel != null)
                        //                        {
                        //                            loadDate(DateUtil.getSixMonthodDate() + " 00:00:00",
                        //                                DateUtil.formatYYMD(DateUtil.getBeforeDate().getTime()) + " 23:59:59",
                        //                                1,
                        //                                String.valueOf(userModel.getId()),
                        //                                true,
                        //                                "2");//6个月countDayKind    1最近30天，2最近6个月  (int)   只当方法类型为1时有效
                        //
                        //                        }
                        //                        else
                        //                        {

                        loadDate(DateUtil.getSixMonthodDate() + " 00:00:00", DateUtil.formatYYMD(DateUtil.getBeforeDate().getTime()) + " 23:59:59", 1, null, true, "2");//6个月countDayKind    1最近30天，2最近6个月  (int)   只当方法类型为1时有效
                        //                        }
                    }
                } catch (ParseException e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                } //走势 首先加载6个月的
            }
        });
    }

   /* class OnChartValueSelectedListenerImp implements OnChartValueSelectedListener {
        @Override
        public void onNothingSelected() {
            // TODO Auto-generated method stub

        }

        @Override
        public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
            float vale = e.getVal();
            //        tv_one_line_moeny, tv_line_one_months
            tv_one_line_moeny.setText(DateUtil.formatMoneyUtils(vale) + getString(R.string.pay_yuan));
            tv_line_one_months.setText(dateList.get(e.getXIndex()));
        }
    }

    class OnChartValueSelectedListenerTwoImp implements OnChartValueSelectedListener {
        @Override
        public void onNothingSelected() {
            // TODO Auto-generated method stub

        }

        @Override
        public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
            float vale = e.getVal();
            //        tv_one_line_moeny, tv_line_one_months tv_line_two_months, tv_two_line_moeny
            long v = (long) vale;
            tv_two_line_moeny.setText(v + getString(R.string.stream_cases));
            tv_line_two_months.setText(dateList.get(e.getXIndex()));
        }
    }*/

    /**
     * 初始化标签
     */

    private String label_year, label_month, label_day, label_hours, label_mins, label_seconds;
    private void initLabel(){
        label_year=getStringById(R.string.pickerview_year);
        label_month=getStringById(R.string.pickerview_month);
        label_day=getStringById(R.string.pickerview_day);
        label_hours=getStringById(R.string.pickerview_hours);
        label_mins=getStringById(R.string.pickerview_minutes);
        label_seconds=getStringById(R.string.pickerview_seconds);
        Log.e("JAMY","mins initlabel: "+label_mins);
    }

    /**
     *  获取前后日期 i为正数 向后推迟i天，负数时向前提前i天
     * @return
     */
    public Calendar getStartDate(int day)
    {
        Calendar beforeDate = Calendar.getInstance();
        beforeDate.add(Calendar.DATE, day);
        return beforeDate;
    }

    /**
     *  获取默认显示的日历
     * @return
     */
    public Calendar getDefaultFromDate()
    {
        Calendar beforeDate = Calendar.getInstance();
        beforeDate.set(Calendar.HOUR,00);
        beforeDate.set(Calendar.MINUTE,00);
        return beforeDate;
    }

    /**
     *  获取前90天前的年份
     * @return
     */
    public int getStartYear(int day)
    {
        Calendar beforeDate = Calendar.getInstance();
        beforeDate.add(Calendar.DATE, day);
        return beforeDate.get(Calendar.YEAR);
    }

}
