package cn.swiftpass.enterprise.bussiness.logica.order;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.util.Log;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.BaseManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.AwardModel;
import cn.swiftpass.enterprise.bussiness.model.AwardModelList;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.NetHelper;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 历史活动，颁奖
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2015-12-17]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class HistoryRankingManager extends BaseManager {

    public static final String ACTIVTE = "ACTIVTE";

    public static HistoryRankingManager getInstance() {
        return Container.instance;
    }

    private static class Container {
        public static HistoryRankingManager instance = new HistoryRankingManager();
    }

    @Override
    public void init() {

    }

    @Override
    public void destory() {

    }

    /**
     * 活动统计详情
     * <功能详细描述>
     *
     * @param cardId
     * @param cardCode
     * @param uiNotifyListener
     * @see [类、类#方法、类#成员]
     */

    public void queryActiveTotal(final String activeId, final UINotifyListener<List<AwardModel>> uiNotifyListener) {
        ThreadHelper.executeWithCallback(new Executable<List<AwardModel>>() {

            @Override
            public List<AwardModel> execute() throws Exception {
                JSONObject json = new JSONObject();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);// todo
                } else {
                    json.put("mchId", MainApplication.getMchId());// todo
                }
                json.put("activeId", activeId);
                //                json.put("page", cardId);
                //                json.put("pageSize", cardCode);
                try {
                    long spayRs = System.currentTimeMillis();
                    //                    //加签名
                    if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                        json.put("spayRs", spayRs);
                        json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                    }

                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/active/getActiveTotal", json, null, null);

                    result.setNotifyListener(uiNotifyListener);
                    if (!result.hasError()) {
                        try {
                            int code = result.data.getInt("result");
                            if (code == 200) {

                                String messStr = result.data.getString("message");
                                if ("".equals(messStr)) {
                                    uiNotifyListener.onError(ToastHelper.toStr(R.string.tx_no_active_date));
                                    return null;
                                }
                                JSONObject jsonObject = new JSONObject(result.data.getString("message"));
                                Log.i("hehui", "queryActiveTotal result.data-->" + jsonObject);
                                Gson gson2 = new Gson();
                                List<AwardModel> orders = gson2.fromJson(jsonObject.getString("data"), new TypeToken<List<AwardModel>>() {
                                }.getType());
                                return orders;
                            } else {
                                uiNotifyListener.onError(result.data.getString("message"));
                            }
                        } catch (Exception e) {
                            uiNotifyListener.onError(ToastHelper.toStr(R.string.show_get_date_fail));
                            return null;
                        }
                    }
                } catch (Exception e) {
                    uiNotifyListener.onError(ToastHelper.toStr(R.string.show_get_date_fail));
                }
                return null;
            }
        }, uiNotifyListener);
    }

    /**
     * 活动详情
     * <功能详细描述>
     *
     * @param cardId
     * @param cardCode
     * @param uiNotifyListener
     * @see [类、类#方法、类#成员]
     */

    public void queryActiveDetais(final String activeId, final UINotifyListener<AwardModel> uiNotifyListener) {
        ThreadHelper.executeWithCallback(new Executable<AwardModel>() {

            @Override
            public AwardModel execute() throws Exception {
                JSONObject json = new JSONObject();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);// todo
                } else {
                    json.put("mchId", MainApplication.getMchId());// todo
                }
                json.put("activeId", activeId);
                //                json.put("page", cardId);
                //                json.put("pageSize", cardCode);
                try {
                    long spayRs = System.currentTimeMillis();
                    //                    //加签名
                    if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                        json.put("spayRs", spayRs);
                        json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                    }

                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/active/getActive", json, null, null);

                    result.setNotifyListener(uiNotifyListener);
                    if (!result.hasError()) {
                        try {
                            int code = result.data.getInt("result");
                            if (code == 200) {

                                return (AwardModel) JsonUtil.jsonToBean(result.data.getString("message"), AwardModel.class);
                            } else {
                                uiNotifyListener.onError(result.data.getString("message"));
                            }
                        } catch (Exception e) {
                            uiNotifyListener.onError(ToastHelper.toStr(R.string.show_get_date_fail));
                            return null;
                        }
                    }
                } catch (Exception e) {
                    uiNotifyListener.onError(ToastHelper.toStr(R.string.show_get_date_fail));
                }
                return null;
            }
        }, uiNotifyListener);
    }

    /**
     * 活动是否开启
     * <功能详细描述>
     *
     * @param cardId
     * @param cardCode
     * @param uiNotifyListener
     * @see [类、类#方法、类#成员]
     */

    public void queryActiveState(final UINotifyListener<AwardModel> uiNotifyListener) {
        ThreadHelper.executeWithCallback(new Executable<AwardModel>() {

            @Override
            public AwardModel execute() throws Exception {
                JSONObject json = new JSONObject();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);// todo
                } else {
                    json.put("mchId", MainApplication.getMchId());// todo
                }
                json.put("userId", MainApplication.userId + "");
                //                json.put("page", cardId);
                //                json.put("pageSize", cardCode);
                try {
                    long spayRs = System.currentTimeMillis();
                    //                    //加签名
                    if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                        json.put("spayRs", spayRs);
                        json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                    }

                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/active/activeState", json, null, null);

                    result.setNotifyListener(uiNotifyListener);
                    if (!result.hasError()) {
                        try {
                            int code = result.data.getInt("result");
                            if (code == 200) {

                                return (AwardModel) JsonUtil.jsonToBean(result.data.getString("message"), AwardModel.class);
                            } else {
                                uiNotifyListener.onError(result.data.getString("message"));
                            }
                        } catch (Exception e) {
                            uiNotifyListener.onError(ToastHelper.toStr(R.string.show_get_date_fail));
                            return null;
                        }
                    }
                } catch (Exception e) {
                    uiNotifyListener.onError(ToastHelper.toStr(R.string.show_get_date_fail));
                }
                return null;
            }
        }, uiNotifyListener);
    }

    /**
     * 活动激活，停止
     * <功能详细描述>
     *
     * @param type
     * @param uiNotifyListener
     * @see [类、类#方法、类#成员]
     */
    public void updateActiveStatus(final String activeId, final Integer activeStatus, final UINotifyListener<Boolean> uiNotifyListener) {
        ThreadHelper.executeWithCallback(new Executable<Boolean>() {

            @Override
            public Boolean execute() throws Exception {
                JSONObject json = new JSONObject();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);// todo
                } else {
                    json.put("mchId", MainApplication.getMchId());// todo
                }
                json.put("activeId", activeId);
                if (activeStatus == 0) {

                    json.put("activeStatus", "1");
                } else if (activeStatus == 1) {
                    json.put("activeStatus", "2");
                } else {
                    json.put("activeStatus", "2");
                }
                try {

                    long spayRs = System.currentTimeMillis();
                    //                    //加签名
                    if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                        json.put("spayRs", spayRs);
                        json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                    }

                    Log.i("hehui", "updateActiveStatus param-->" + json);
                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/active/updateActiveStatus", json, null, null);

                    result.setNotifyListener(uiNotifyListener);
                    if (!result.hasError()) {
                        try {
                            int code = result.data.getInt("result");
                            if (code == 200) {
                                return true;
                                //                                String messStr = result.data.getString("message");
                                //                                if ("".equals(messStr))
                                //                                {
                                //                                    uiNotifyListener.onError("暂无活动数据");
                                //                                    return null;
                                //                                }
                                //                                JSONObject jsonObject = new JSONObject(result.data.getString("message"));
                                //                                Log.i("hehui", "getActiveList result.data-->" + jsonObject);
                                //                                int status = jsonObject.optInt("status", 0);
                                //                                if (status == 0)
                                //                                {
                                //                                    
                                //                                }
                                //                                else
                                //                                {
                                //                                    uiNotifyListener.onError(result.data.getString("message"));
                                //                                    return false;
                                //                                }
                            } else {
                                uiNotifyListener.onError(result.data.getString("message"));
                            }
                        } catch (Exception e) {
                            uiNotifyListener.onError(ToastHelper.toStr(R.string.show_get_date_fail));
                            return false;
                        }
                    }
                } catch (Exception e) {
                    uiNotifyListener.onError(ToastHelper.toStr(R.string.show_get_date_fail));
                }
                return false;
            }
        }, uiNotifyListener);
    }

    /**
     * 获取所有活动
     * <功能详细描述>
     *
     * @param type
     * @param uiNotifyListener
     * @see [类、类#方法、类#成员]
     */
    public void addActive(final AwardModel awardModel, final UINotifyListener<AwardModel> uiNotifyListener) {
        ThreadHelper.executeWithCallback(new Executable<AwardModel>() {

            @Override
            public AwardModel execute() throws Exception {
                JSONObject json = new JSONObject();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);// todo
                } else {
                    json.put("mchId", MainApplication.getMchId());// todo
                }
                json.put("activeName", awardModel.getActiveName());
                json.put("fullMoney", awardModel.getFullMoney());
                json.put("orderFullMoney", awardModel.getOrderFullMoney());
                json.put("minMoney", awardModel.getMinMoney());
                json.put("maxMoney", awardModel.getMaxMoney());
                json.put("maxNum", awardModel.getMaxNum());
                json.put("startDate", awardModel.getStartDate());
                json.put("endDate", awardModel.getEndDate());
                json.put("activeType", "1");
                long spayRs = System.currentTimeMillis();
                //                    //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                    json.put("spayRs", spayRs);
                    json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                }
                try {
                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/active/saveOrUpdateActive", json, null, null);

                    result.setNotifyListener(uiNotifyListener);
                    if (!result.hasError()) {
                        try {
                            int code = result.data.getInt("result");
                            if (code == 200) {
                                return (AwardModel) JsonUtil.jsonToBean(result.data.optString("message"), AwardModel.class);
                                //                                String messStr = result.data.getString("message");
                                //                                if ("".equals(messStr))
                                //                                {
                                //                                    uiNotifyListener.onError("暂无活动数据");
                                //                                    return null;
                                //                                }
                                //                                JSONObject jsonObject = new JSONObject(result.data.getString("message"));
                                //                                Log.i("hehui", "getActiveList result.data-->" + jsonObject);
                                //                                int status = jsonObject.optInt("status", 0);
                                //                                if (status == 0)
                                //                                {
                                //                                }
                                //                                else
                                //                                {
                                //                                    uiNotifyListener.onError(result.data.getString("message"));
                                //                                    return false;
                                //                                }
                            } else {
                                uiNotifyListener.onError(result.data.getString("message"));
                            }
                        } catch (Exception e) {
                            uiNotifyListener.onError(ToastHelper.toStr(R.string.show_get_date_fail));
                            return null;
                        }
                    }
                } catch (Exception e) {
                    uiNotifyListener.onError(ToastHelper.toStr(R.string.show_get_date_fail));
                }
                return null;
            }
        }, uiNotifyListener);
    }

    /**
     * 获取所有活动
     * <功能详细描述>
     *
     * @param type
     * @param uiNotifyListener
     * @see [类、类#方法、类#成员]
     */
    public void getActiveList(final String type, final String activeStatus, final Integer page, final UINotifyListener<List<AwardModel>> uiNotifyListener) {
        ThreadHelper.executeWithCallback(new Executable<List<AwardModel>>() {

            @Override
            public List<AwardModel> execute() throws Exception {
                JSONObject json = new JSONObject();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);// todo
                } else {
                    json.put("mchId", MainApplication.getMchId());// todo
                }
                if (!StringUtil.isEmptyOrNull(type)) {
                    json.put("activeType", Integer.parseInt(type));
                }
                if (activeStatus != null) {
                    json.put("activeStatus", activeStatus);

                }
                //                else
                //                {
                //                    json.put("notNeedPage", true);
                //                }

                if (page > 0) {
                    json.put("page", page);
                    json.put("pageSize", 20);
                }

                long spayRs = System.currentTimeMillis();
                //                    //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                    json.put("spayRs", spayRs);
                    json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                }

                try {
                    Log.i("hehui", "getActiveList param-->" + json);
                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/active/getActiveList", json, null, null);

                    result.setNotifyListener(uiNotifyListener);
                    if (!result.hasError()) {
                        try {
                            int code = result.data.getInt("result");
                            List<AwardModel> orders = new ArrayList<AwardModel>();
                            if (code == 200) {
                                String messStr = result.data.getString("message");
                                if ("".equals(messStr)) {
                                    //                                    uiNotifyListener.onError(ACTIVTE);
                                    return orders;
                                }
                                JSONObject jsonObject = new JSONObject(result.data.getString("message"));
                                Log.i("hehui", "getActiveList result.data-->" + jsonObject);
                                Gson gson2 = new Gson();
                                orders = gson2.fromJson(jsonObject.getString("data"), new TypeToken<List<AwardModel>>() {
                                }.getType());
                                if (orders.size() > 0) {
                                    orders.get(0).setTotalPageCount(result.data.optInt("totalPageCount", 0));
                                }
                                return orders;
                            } else {
                                uiNotifyListener.onError(result.data.getString("message"));
                            }
                        } catch (Exception e) {
                            uiNotifyListener.onError(ToastHelper.toStr(R.string.show_get_date_fail));
                            return null;
                        }
                    }
                } catch (Exception e) {
                    uiNotifyListener.onError(ToastHelper.toStr(R.string.show_get_date_fail));
                }
                return null;
            }
        }, uiNotifyListener);
    }

    /**
     * 历史活动
     * <功能详细描述>
     *
     * @param cardId
     * @param cardCode
     * @param uiNotifyListener
     * @see [类、类#方法、类#成员]
     */

    public void queryHistoryActive(final String page, final UINotifyListener<List<AwardModel>> uiNotifyListener) {
        ThreadHelper.executeWithCallback(new Executable<List<AwardModel>>() {

            @Override
            public List<AwardModel> execute() throws Exception {
                JSONObject json = new JSONObject();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);// todo
                } else {
                    json.put("mchId", MainApplication.getMchId());// todo
                }
                if (MainApplication.userId > 0) {
                    json.put("userId", MainApplication.userId + "");
                } else {
                    json.put("userId", MainApplication.getUserId() + "");
                }
                //                json.put("page", cardId);
                //                json.put("pageSize", cardCode);
                try {
                    long spayRs = System.currentTimeMillis();
                    //                    //加签名
                    if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                        json.put("spayRs", spayRs);
                        json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                    }

                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/active/activeHistory", json, null, null);

                    result.setNotifyListener(uiNotifyListener);
                    if (!result.hasError()) {
                        try {
                            int code = result.data.getInt("result");
                            if (code == 200) {
                                String messStr = result.data.getString("message");
                                if ("".equals(messStr)) {
                                    uiNotifyListener.onError(ToastHelper.toStr(R.string.show_no_history_date));
                                    return null;
                                }
                                JSONObject jsonObject = new JSONObject(result.data.getString("message"));
                                Log.i("hehui", "queryHistoryActive result.data-->" + jsonObject);
                                Gson gson2 = new Gson();
                                List<AwardModel> orders = gson2.fromJson(jsonObject.getString("data"), new TypeToken<List<AwardModel>>() {
                                }.getType());
                                return orders;
                            } else {
                                uiNotifyListener.onError(result.data.getString("message"));
                            }
                        } catch (Exception e) {
                            uiNotifyListener.onError(ToastHelper.toStr(R.string.show_get_date_fail));
                            return null;
                        }
                    }
                } catch (Exception e) {
                    uiNotifyListener.onError(ToastHelper.toStr(R.string.show_get_date_fail));
                }
                return null;
            }
        }, uiNotifyListener);
    }

    /**
     * 活动排名
     * <功能详细描述>
     *
     * @param cardId
     * @param cardCode
     * @param uiNotifyListener
     * @see [类、类#方法、类#成员]
     */

    public void queryActiveOrder(final Integer page, final String activeId, final UINotifyListener<AwardModelList> uiNotifyListener) {
        ThreadHelper.executeWithCallback(new Executable<AwardModelList>() {

            @Override
            public AwardModelList execute() throws Exception {
                JSONObject json = new JSONObject();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);
                    json.put("userId", MainApplication.userId + "");
                } else {
                    json.put("mchId", MainApplication.getMchId());
                    json.put("userId", MainApplication.getUserId() + "");
                }
                if (null != activeId && !"".equals(activeId)) {
                    json.put("activeId", activeId);

                }
                json.put("page", page);
                json.put("pageSize", "10");
                try {
                    long spayRs = System.currentTimeMillis();
                    //                    //加签名
                    if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                        json.put("spayRs", spayRs);
                        json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                    }

                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/active/winningOrder", json, null, null);

                    result.setNotifyListener(uiNotifyListener);
                    AwardModelList awardModelList = new AwardModelList();
                    if (!result.hasError()) {
                        try {
                            int code = result.data.getInt("result");
                            if (code == 200) {
                                String jstr = result.data.getString("message");
                                if ("".equals(jstr)) {
                                    return awardModelList;
                                }
                                JSONObject jsonObject = new JSONObject(jstr);
                                Log.i("hehui", "queryActiveOrder result.data-->" + jsonObject);
                                Gson gson2 = new Gson();
                                List<AwardModel> orders = gson2.fromJson(jsonObject.getString("data"), new TypeToken<List<AwardModel>>() {
                                }.getType());
                                AwardModel awardModel = (AwardModel) JsonUtil.jsonToBean(jsonObject.getString("myWinning"), AwardModel.class);
                                if (awardModel != null) {
                                    awardModel.setActiveState(jsonObject.optInt("activeState", 0));
                                }
                                awardModelList.setAwardModel(awardModel);
                                awardModelList.setAwardModelList(orders);

                                return awardModelList;
                            } else {
                                uiNotifyListener.onError(result.data.getString("message"));
                            }
                        } catch (Exception e) {
                            uiNotifyListener.onError(ToastHelper.toStr(R.string.show_get_date_fail));
                            return null;
                        }
                    }
                } catch (Exception e) {
                    uiNotifyListener.onError(ToastHelper.toStr(R.string.show_get_date_fail));
                }
                return null;
            }
        }, uiNotifyListener);
    }
}
