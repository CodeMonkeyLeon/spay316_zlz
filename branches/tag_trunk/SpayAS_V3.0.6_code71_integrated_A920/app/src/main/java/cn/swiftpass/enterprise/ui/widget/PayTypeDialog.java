/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.widget;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.WalletListBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 弹出提示框
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-11]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class PayTypeDialog extends Dialog
{
    private Context context;
    private ViewGroup mRootView;

    private PayTypeDialog.HandleItemBtn handleItemBtn;

    private ImageView iv_back_select,iv_close_dialog;

    private String money;
    
    private ListView lv_pay ,lv_second_pay;
    
    private ViewHolder holder;
    
    private PayTypeAdape payTypeAdape;

    private WalletPayTypeAdapter walletPayTypeAdapter;
    
    private List<DynModel> list;

    private String LiquidApiCode;
    /**
     * title 
     * content 提示内容
     * <默认构造函数>
     */
    public PayTypeDialog(Context context, String money,PayTypeDialog.HandleItemBtn handleItemBtn)
    {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        
        mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.pay_type_dialog_info, null);
        
        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);
        this.handleItemBtn = handleItemBtn;
        this.money = money;
        this.context = context;

        initView();
        
        initValue();
        setLinster();
        
    }
    
    private void initValue()
    {
        
//        Object object = SharedPreUtile.readProduct("dynPayType" + ApiConstant.bankCode + MainApplication.getMchId());

        Object object;
        //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth","sale");
        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(saleOrPreauth,"pre_auth")){
            object = SharedPreUtile.readProduct("dynPayType_pre_auth" + ApiConstant.bankCode + MainApplication.getMchId());
        }else{
//            object = SharedPreUtile.readProduct("dynPayType" + ApiConstant.bankCode + MainApplication.getMchId());
            object = SharedPreUtile.readProduct("ActiveDynPayType" + ApiConstant.bankCode + MainApplication.getMchId());
        }

        if (object != null)
        {
            list = (List<DynModel>)object;
            if (null != list && list.size() > 0)
            {
                payTypeAdape = new PayTypeAdape(context, list);
                lv_pay.setAdapter(payTypeAdape);
            }
        }

        //如果获取过liquid pay小钱包数据信息
        if(MainApplication.walletListBeans != null && MainApplication.walletListBeans.size() > 0){
            walletPayTypeAdapter = new WalletPayTypeAdapter(context,MainApplication.walletListBeans);
            lv_second_pay.setAdapter(walletPayTypeAdapter);
        }

        //进来一级列表的时候，默认是不展示的
        iv_back_select.setVisibility(View.GONE);

        //进入弹框界面，默认先展示一级列表
        lv_pay.setVisibility(View.VISIBLE);
        lv_second_pay.setVisibility(View.GONE);

    }


    /**
     * 设置监听
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void setLinster()
    {
        lv_pay.setOnItemClickListener(new OnItemClickListener()
        {
            
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
            {
                if(list.size() > 0 ){
                    DynModel dynModel = list.get(position);
                    if(dynModel != null && !StringUtil.isEmptyOrNull(money)){
                        //如果当前点击的是小钱包liquid pay 的通道
                        if(getNativePayType(dynModel.getApiCode()).equalsIgnoreCase(MainApplication.liquidServiceType)){
                            //如果二级列表有数据，则展示二级列表
                            if(MainApplication.walletListBeans != null && MainApplication.walletListBeans.size() > 0){
                                iv_back_select.setVisibility(View.VISIBLE);

                                lv_pay.setVisibility(View.GONE);
                                lv_second_pay.setVisibility(View.VISIBLE);

                                LiquidApiCode = dynModel.getApiCode();
                            }

                        }else{
                            dismiss();
                            handleItemBtn.toPay(money, dynModel.getApiCode(),null,null);
                        }
                    }
                }
            }
        });

        lv_second_pay.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dismiss();

                if (MainApplication.walletListBeans != null && MainApplication.walletListBeans.size() > 0)
                {
                    WalletListBean walletModel = MainApplication.walletListBeans.get(position);
                    if (walletModel != null && !StringUtil.isEmptyOrNull(money) && !StringUtil.isEmptyOrNull(LiquidApiCode))
                    {
                        handleItemBtn.toPay(money, LiquidApiCode,walletModel.getPayloadCode(),walletModel);
                    }
                }
            }
        });
    }
    
    /**
     * 初始化
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView()
    {
        iv_back_select = findViewById(R.id.iv_back_select);
        iv_close_dialog = findViewById(R.id.iv_close_dialog);

        lv_pay = findViewById(R.id.lv_pay);
        lv_second_pay = findViewById(R.id.lv_second_pay);

        iv_close_dialog.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });

        iv_back_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //如果按回退按钮，则回到一级列表，回退按钮不展示
                iv_back_select.setVisibility(View.GONE);

                lv_pay.setVisibility(View.VISIBLE);
                lv_second_pay.setVisibility(View.GONE);
            }
        });

    }
    
    class PayTypeAdape extends BaseAdapter
    {
        
        private List<DynModel> list;
        
        private Context context;
        
        private PayTypeAdape(Context context, List<DynModel> list)
        {
            this.context = context;
            this.list = list;
        }
        
        @Override
        public int getCount()
        {
            return list.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            return list.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(context, R.layout.activity_paytype_list_item, null);
                holder = new ViewHolder();
                holder.v_line = convertView.findViewById(R.id.v_line);
                holder.iv_image = convertView.findViewById(R.id.iv_image);
                holder.tv_pay_name = convertView.findViewById(R.id.tv_pay_name);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            if ((position + 1) == list.size())
            {
                holder.v_line.setVisibility(View.GONE);
            }
            else
            {
                holder.v_line.setVisibility(View.VISIBLE);
            }
            
            DynModel dynModel = list.get(position);
            if (null != dynModel)
            {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType()))
                {
                    holder.tv_pay_name.setText(MainApplication.getPayTypeMap().get(dynModel.getApiCode()));
                    Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_general_receivables);
                    if (bitmap != null)
                    {
                        
                        MainApplication.finalBitmap.display(holder.iv_image, dynModel.getSmallIconUrl(), bitmap,bitmap);
                    }
                    else
                    {
                        MainApplication.finalBitmap.display(holder.iv_image, dynModel.getSmallIconUrl());
                    }
                }
            }
            
            return convertView;
        }
        
    }
    
    class ViewHolder
    {
        private View v_line;
        
        private ImageView iv_image;
        
        TextView tv_pay_name;
    }
    
    public void showPage(Class clazz)
    {
        Intent intent = new Intent(context, clazz);
        context.startActivity(intent);
    }
    

    public interface HandleItemBtn
    {
        void toPay(String money, String payType,String liquidPayType,WalletListBean walletBean);
    }


    class WalletPayTypeAdapter extends BaseAdapter
    {

        private List<WalletListBean> list;

        private Context context;

        private WalletPayTypeAdapter(Context context, List<WalletListBean> list)
        {
            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount()
        {
            return list.size();
        }

        @Override
        public Object getItem(int position)
        {
            return list.get(position);
        }

        @Override
        public long getItemId(int position)
        {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(context, R.layout.activity_paytype_list_item, null);
                holder = new ViewHolder();
                holder.v_line = convertView.findViewById(R.id.v_line);
                holder.iv_image = convertView.findViewById(R.id.iv_image);
                holder.tv_pay_name = convertView.findViewById(R.id.tv_pay_name);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            if ((position + 1) == list.size())
            {
                holder.v_line.setVisibility(View.GONE);
            }
            else
            {
                holder.v_line.setVisibility(View.VISIBLE);
            }

            WalletListBean walletBean = list.get(position);
            if (null != walletBean)
            {
                if (!StringUtil.isEmptyOrNull(walletBean.getName()))
                {
                    holder.tv_pay_name.setText(walletBean.getName());
                    Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_general_receivables);
                    if (bitmap != null)
                    {
                        MainApplication.finalBitmap.display(holder.iv_image, walletBean.getImageUrl(), bitmap,bitmap);
                    }
                    else
                    {
                        MainApplication.finalBitmap.display(holder.iv_image,  walletBean.getImageUrl());
                    }
                }
            }

            return convertView;
        }

    }


    //通过ApiCode得到当前支付类型的NativePayType
    public String getNativePayType(String ApiCode){
        //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth","sale");
        Object object;
        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(saleOrPreauth,"pre_auth")){
            object = SharedPreUtile.readProduct("dynPayType_pre_auth" + ApiConstant.bankCode + MainApplication.getMchId());
        }else{
            object = SharedPreUtile.readProduct("ActiveDynPayType" + ApiConstant.bankCode + MainApplication.getMchId());
        }

        if (object != null) {
            List<DynModel> list = (List<DynModel>) object;

            if (null != list && list.size() > 0) {
                for(int i = 0 ; i < list.size();i++){
                    if(list.get(i).getApiCode().equalsIgnoreCase(ApiCode)){
                        return list.get(i).getNativeTradeType();
                    }
                }
            } else {
                return null;
            }
        }else {
            return null;
        }
        return null;
    }


}
