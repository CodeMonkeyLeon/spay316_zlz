package cn.swiftpass.enterprise.bussiness.enums;

/**
 * 交易类型
 * User: Alan
 * Date: 14-1-22
 * Time: 下午7:47
 * To change this template use File | Settings | File Templates.
 */
public enum TransactionTypeEnum {
    PAY_WX_WEB(0,"WEB扫描支付"),
    PAY_WX_PUBLIC_ACCOUNT(1,"公共账号内支付"),
    PAY_WX_NATIVE(2,"公共账号内支付"),
    PAY_POS(3,"POS刷卡");

    private final Integer value;
    private String displayName = "";
    private TransactionTypeEnum(Integer v ,String displayName)
    {
        this.value = v;
        this.displayName = displayName;

    }
    public String getDisplayName() {
        return displayName;
    }

    public Integer getValue() {
        return value;
    }


}
