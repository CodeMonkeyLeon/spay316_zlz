package cn.swiftpass.enterprise.ui.widget;

import java.util.HashSet;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.SelectListModel;

/**选择框 通用*/
public class SelectItemCheckView extends LinearLayout
{
    
    public TextView tvText;
    
    private CheckBox cbSelected;
    
    private boolean checked;
    
    private HashSet<SelectListModel> checkData;
    
    public SelectItemCheckView(Context context, AttributeSet attr)
    {
        super(context, attr);
    }
    
    public void bindData(SelectListModel m, HashSet<SelectListModel> checkData)
    {
        tvText.setText(m.title);
        cbSelected.setChecked(m.isCheck);
        cbSelected.setTag(m);
        if (checkData != null)
        { //W.l 判断一下比较好
            this.checkData = checkData;
        }
        else
        {
            this.checkData = new HashSet<SelectListModel>();
        }
        //cbSelected.setOnClickListener(this);
    }
    
    @Override
    protected void onFinishInflate()
    {
        super.onFinishInflate();
        tvText = (TextView)findViewById(R.id.title);
        cbSelected = (CheckBox)findViewById(R.id.checkbox);
    }
    /*@Override
    public void setChecked(boolean checked)
    {
    	this.checked = checked;
    	cbSelected.setChecked(checked);
    }
    @Override
    public boolean isChecked()
    {
    	return checked;
    }

    @Override
    public void toggle()
    {
    	setChecked(!isChecked());
    }
    @Override
    public void onClick(View v) {
    	SelectListModel sm = (SelectListModel)v.getTag();
    	if(cbSelected.isChecked()){
    		sm.isCheck=true;  //点击后就让实体改变
    		checkData.add(sm);
    		if(sm.id==10||sm.id==4){
    			for (SelectListModel sss : checkData) {
    				sss.isCheck = false;
    			}
    			isCheckboxState=true;
    			sm.isCheck=true;
    			checkData.clear();
    			checkData.add(sm);
    		}else{
    			if(isCheckboxState){
    				cbSelected.setClickable(true);
    			}else{
    				cbSelected.setClickable(false);
    				sm.isCheck=true;  //点击后就让实体改变
    				checkData.add(sm);
    			}
    			
    		}
    	}else{
    		sm.isCheck=false;
    		checkData.remove(sm);
    	}
    }
    */
}
