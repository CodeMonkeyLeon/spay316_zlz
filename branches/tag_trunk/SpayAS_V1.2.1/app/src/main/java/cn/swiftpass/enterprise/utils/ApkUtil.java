/**
 * @description:
 * @author chenshiqiang E-mail:csqwyyx@163.com
 * @date 2012-12-24 上午09:00:56   
 * @version 1.0
 * copyrights reserved by Petfone 2007-2011   
 */
package cn.swiftpass.enterprise.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;

public class ApkUtil
{
    
    public static boolean launchApk(Context context, String pkg)
    {
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent();
        intent = packageManager.getLaunchIntentForPackage(pkg);
        if (intent != null)
        {
            context.startActivity(intent);
            return true;
        }
        else
        {
            ToastHelper.showInfo("没安装不能启动！");
            return false;
        }
    }
    
    /**安装apk*/
    public static void onInstallApk(Context context, String apkPath)
    {
        context.startActivity(getApkInstallIntent(context, apkPath));
    }
    
    public static Intent getApkInstallIntent(Context context, String strApkPath)
    {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(new File(strApkPath)), "application/vnd.android.package-archive");
        //去掉Intent.FLAG_ACTIVITY_NEW_TASK，解决按Home下次进入会弹出以前安装任务的bug
        //        if (!(context instanceof Activity))
        //        {
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //        }
        return intent;
    }
    
    /**获取已安装apk*/
    public static List<PackageInfo> getAllApps(Context context)
    {
        List<PackageInfo> apps = new ArrayList<PackageInfo>();
        PackageManager pManager = context.getPackageManager();
        //获取手机内所有应用   
        List<PackageInfo> paklist = pManager.getInstalledPackages(0);
        for (int i = 0; i < paklist.size(); i++)
        {
            PackageInfo pak = (PackageInfo)paklist.get(i);
            //判断是否为非系统预装的应用程序   
            if ((pak.applicationInfo.flags & pak.applicationInfo.FLAG_SYSTEM) <= 0)
            {
                // customs applications
                //pak.applicationInfo.packageName
                
                apps.add(pak);
            }
        }
        return apps;
    }
    
    /**获取已安装apk*/
    public static boolean isInstallMyApp(Context context, String packageName)
    {
        boolean result = false;
        PackageManager pManager = context.getPackageManager();
        //获取手机内所有应用   
        List<PackageInfo> paklist = pManager.getInstalledPackages(0);
        for (int i = 0; i < paklist.size(); i++)
        {
            PackageInfo pak = (PackageInfo)paklist.get(i);
            //判断是否为非系统预装的应用程序
            if ((pak.applicationInfo.flags & pak.applicationInfo.FLAG_SYSTEM) <= 0)
            {
                if (pak.packageName.equals(packageName))
                {
                    result = true;
                }
            }
        }
        return result;
    }
    
    public static List<String> getInstallApp(Context context)
    {
        List<PackageInfo> list = getAllApps(context);
        List<String> data = new ArrayList<String>();
        for (PackageInfo p : list)
        {
            data.add(p.applicationInfo.packageName);
        }
        return data;
        
    }
    
    /**自己程序是否已经启用*/
    public static boolean isRunApp(Context context)
    {
        ActivityManager am = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(3);
        PackageManager pManager = context.getPackageManager();
        
        for (ActivityManager.RunningTaskInfo info : list)
        {
            if (info.topActivity.getPackageName().equals(context.getPackageName()))
            {
                return true;
            }
        }
        return false;
        
    }
    
    public static void installEpaylinks(Context context, String apkName)
    {
        if (isInstallMyApp(context, "revenco.epaycharge.plugin"))
        {//"epaycharge.third.party.pay"  .Ui.LoginActivity
            return;
        }
        //Intent intentInstall = new Intent();
        String path = FileUtils.getAppPath();
        File f = new File(path);
        if (!f.exists())
            f.mkdirs();
        String pathfile = f.getAbsolutePath() + "/" + apkName;
        File file = new File(pathfile);
        try
        {
            InputStream is = context.getAssets().open(apkName);
            if (!file.exists())
            {
                file.createNewFile();
                FileOutputStream os = new FileOutputStream(file, true);
                byte[] bytes = new byte[512];
                int i = -1;
                while ((i = is.read(bytes)) > 0)
                {
                    os.write(bytes);
                }
                os.close();
                is.close();
                Logger.i("", "----------- has been copy to ");
            }
            else
            {
                // file.delete();
                Logger.i("", "-----------has is hava a e ");
                
            }
            String permission = "666";
            String command = "chmod " + permission + " " + f.getAbsolutePath() + "/" + apkName;
            Runtime runtime = Runtime.getRuntime();
            runtime.exec(command);
            /*intentInstall.setAction(android.content.Intent.ACTION_VIEW); 
            intentInstall.setDataAndType(Uri.fromFile(file), 
            "application/vnd.android.package-archive"); 
            intentInstall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
            context.startActivity(intentInstall); */
            
            onInstallApk(context, file.getAbsolutePath());
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
    }

    public static String getProcessName(Context cxt, int pid) {
        ActivityManager am = (ActivityManager) cxt.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningApps = am.getRunningAppProcesses();
        if (runningApps == null) {
            return null;
        }
        for (ActivityManager.RunningAppProcessInfo procInfo : runningApps) {
            if (procInfo.pid == pid) {
                return procInfo.processName;
            }
        }
        return null;
    }
    
}
