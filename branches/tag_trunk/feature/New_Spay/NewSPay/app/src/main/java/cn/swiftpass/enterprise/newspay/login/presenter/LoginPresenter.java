package cn.swiftpass.enterprise.newspay.login.presenter;


import com.trello.rxlifecycle2.LifecycleProvider;
import com.trello.rxlifecycle2.android.ActivityEvent;


import cn.swiftpass.enterprise.base.BaseBean;
import cn.swiftpass.enterprise.base.mvp.BasePresenter;
import cn.swiftpass.enterprise.base.mvp.Call;
import cn.swiftpass.enterprise.base.mvp.ResultCallback;
import cn.swiftpass.enterprise.newspay.login.contract.LoginContract;
import cn.swiftpass.enterprise.newspay.login.model.LoginModel;
import cn.swiftpass.enterprise.newspay.login.model.bean.ExchangeKeyBean;
import cn.swiftpass.enterprise.newspay.login.model.bean.LoginBean;


public class LoginPresenter extends BasePresenter<LoginContract.View> implements LoginContract.Presenter {

    private LoginModel mLoginModel;
    /**
     *
     * @param provider V层生命周期，方便Rxlife注销
     * @param view
     */
    public LoginPresenter(LifecycleProvider<ActivityEvent> provider, LoginContract.View view) {
        super(provider);
        this.mLoginModel = new LoginModel();
        //UI层去创建LoginPresenter时，关联view
        this.attachView(view);
    }

    @Override
    public void exchangeServiceKey() {
        if (!isViewAttached()) {
            return;
        }
        mLoginModel.getExchangeServiceKey(new ResultCallback<>(getView(), new Call<ExchangeKeyBean>() {
            @Override
            public void onSuccess(ExchangeKeyBean data) {
                getView().onExchangeKeySuccess(data);
            }

            @Override
            public void onError(int fromRequest, String code, String errorMsg) {

            }
        }),getProvider().<BaseBean<ExchangeKeyBean>>bindUntilEvent(ActivityEvent.DESTROY));
    }

    @Override
    public void login(String userName, String userPwd) {
        if (!isViewAttached()) {
            return;
        }

        mLoginModel.getLogin(userName,userPwd,new ResultCallback<>(getView(), new Call<LoginBean>() {
            @Override
            public void onSuccess(LoginBean data) {
                getView().onLoginSuccess(data);
            }

            @Override
            public void onError(int fromRequest, String code, String errorMsg) {
                getView().onError(fromRequest,code,errorMsg);
            }
        }), getProvider().<BaseBean<LoginBean>>bindUntilEvent(ActivityEvent.DESTROY));

    }


}
