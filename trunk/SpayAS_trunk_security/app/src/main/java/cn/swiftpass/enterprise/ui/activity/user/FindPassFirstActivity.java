/*
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-14
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.user;

import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.ForgetPSWBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.FindPassFirstContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.FindPassFirstPresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 忘记密码
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-14]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class FindPassFirstActivity extends BaseActivity<FindPassFirstContract.Presenter> implements FindPassFirstContract.View {

    private EditText et_id;
    private ImageView iv_clean_input;
    private Button btn_next_step;

    @Override
    protected FindPassFirstContract.Presenter createPresenter() {
        return new FindPassFirstPresenter();
    }

    @Override
    protected boolean isLoginRequired() {
        return false;
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_pass_first);

        initView();
        btn_next_step.getBackground().setAlpha(102);
        //        btn_next_step.sett
        setLister();

    }

    private void initView() {
        et_id = findViewById(R.id.et_id);
        iv_clean_input = findViewById(R.id.iv_clean_input);
        btn_next_step = findViewById(R.id.btn_next_step);
        et_id.requestFocus();
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (et_id.isFocused()) {
                    if (et_id.getText().toString().length() > 0) {
                        iv_clean_input.setVisibility(View.VISIBLE);
                        setButtonBg(btn_next_step, true, R.string.reg_next_step);
                    } else {
                        iv_clean_input.setVisibility(View.GONE);
                        setButtonBg(btn_next_step, false, R.string.reg_next_step);
                    }
                }
            }
        });
        et_id.addTextChangedListener(editTextWatcher);

        showSoftInputFromWindow(FindPassFirstActivity.this, et_id);
    }

    @Override
    public void checkForgetPwdInputSuccess(@Nullable ForgetPSWBean response) {
        setButtonBg(btn_next_step, true, R.string.reg_next_step);
        if (response != null) {
            response.setInput(et_id.getText().toString().trim());
            MainApplication.getInstance().getListActivities().add(FindPassFirstActivity.this);

            FindPassSecondActivity.startActivity(FindPassFirstActivity.this, response);
        }
    }

    @Override
    public void checkForgetPwdInputFailed(@Nullable Object error) {
        FindPassFirstActivity.this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                setButtonBg(btn_next_step, true, R.string.reg_next_step);
                if (null != error) {
                    toastDialog(FindPassFirstActivity.this, error.toString(), null);
                }
            }
        });
    }

    private void setLister() {
        btn_next_step.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (StringUtil.isEmptyOrNull(et_id.getText().toString())) {
                    toastDialog(FindPassFirstActivity.this, R.string.show_user_name, null);
                    et_id.setFocusable(true);
                    return;
                }

                if (mPresenter != null) {
                    FindPassFirstActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            setButtonBg(btn_next_step, false, R.string.reg_next_step_loading);
                        }
                    });
                    mPresenter.checkForgetPwdInput(et_id.getText().toString().trim());
                }
            }
        });

        iv_clean_input.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                et_id.setText("");
                iv_clean_input.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tv_find_pass_title);
    }


    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b, enable, res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_press_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }
}
