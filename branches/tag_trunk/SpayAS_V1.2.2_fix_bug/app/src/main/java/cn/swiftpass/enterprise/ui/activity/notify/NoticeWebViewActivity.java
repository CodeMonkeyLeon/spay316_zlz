package cn.swiftpass.enterprise.ui.activity.notify;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.PreferenceUtil;

/**
 * 内容显示
 * Date: 13-10-29
 * Time: 下午6:22
 *
 */
@SuppressLint({"SetJavaScriptEnabled", "NewApi"})
public class NoticeWebViewActivity extends TemplateActivity
{
    private WebView wb;
    
    private boolean isDisplayShared;
    
    private Context mContext;
    
    // private SharedDialog sharedDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contenttext);
        initViews();
    }
    
    @SuppressLint("JavascriptInterface")
    private void initViews()
    {
        mContext = this;
        wb = (WebView)findViewById(R.id.webview);
//        wb.addJavascriptInterface(new InJavaScriptLocalObj(), "local_obj");
        wb.getSettings().setJavaScriptEnabled(true);
        wb.getSettings().setAllowFileAccessFromFileURLs(false);
        wb.getSettings().setAllowUniversalAccessFromFileURLs(false);
        
        WebSettings settings = wb.getSettings();
        if (AppHelper.getAndroidSDKVersion() == 17)
        {
            settings.setDisplayZoomControls(false);
        }
        settings.setLoadWithOverviewMode(true);
        //settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        //各种分辨率适应
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int mDensity = metrics.densityDpi;
        if (mDensity == 120)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        }
        else if (mDensity == 160)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        }
        else if (mDensity == 240)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        }
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        
        wb.setWebChromeClient(new myWebChromeClien());
        wb.setWebViewClient(new MyWebViewClient());
        isDisplayShared = getIntent().getBooleanExtra("isShared", false);
        String title = getIntent().getStringExtra("title");
        
        titleBar.setTitle(title);
        String url = getIntent().getStringExtra("url");
        wb.setWebViewClient(new WebViewClient()
        {
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                //调用拨号程序  
                if (url.startsWith("mailto:") || url.startsWith("geo:") || url.startsWith("tel:"))
                {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                }
                return true;
            }
        });
        
        String language = PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN);
        Map<String, String> heards = new HashMap<String, String>();
        Locale locale = getResources().getConfiguration().locale; //zh-rHK
        String lan = locale.getCountry();
        
        if (!TextUtils.isEmpty(language))
        {
            heards.put("fp-lang", PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN));
        }
        else
        {
            if (lan.equalsIgnoreCase("CN"))
            {
                heards.put("fp-lang", MainApplication.LANG_CODE_ZH_CN);
            }
            else
            {
                heards.put("fp-lang", MainApplication.LANG_CODE_ZH_TW);
            }
            
        }
        wb.loadUrl(url, heards);
        
        // bottomBar.setVisibility(View.GONE);
    }
    
    final class InJavaScriptLocalObj
    {
        public void showSource(String html)
        {
            Log.d("HTML", html);
        }
    }
    
    final class MyWebViewClient extends WebViewClient
    {
        public MyWebViewClient()
        {
            super();
        }
        
        /** {@inheritDoc} */
        
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            //            showNewLoading(true, getString(R.string.public_loading));
            super.onPageStarted(view, url, favicon);
        }
        
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            Log.i("hehui", "shouldOverrideUrlLoading");
            showNewLoading(true, getString(R.string.public_loading));
            return super.shouldOverrideUrlLoading(view, url);
            
        }
        
        @Override
        public void onPageFinished(WebView view, String url)
        {
            dismissLoading();
            super.onPageFinished(view, url);
        }
        
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
        {
            super.onReceivedError(view, errorCode, description, failingUrl);
            
            dismissLoading();
            wb.loadUrl("file:///android_asset/error.html");
            
        }
    }
    
    class myWebChromeClien extends WebChromeClient
    {
        @Override
        public void onProgressChanged(WebView view, int newProgress)
        {
            super.onProgressChanged(view, newProgress);
            
        }
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setRightLodingVisible(false);
        titleBar.setLeftButtonVisible(true);
        titleBar.setRightButtonVisible(false);
        if (isDisplayShared)
            titleBar.setRightButtonVisible(true, "分享");
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
                //                if (isDisplayShared)
                //                {
                //                    // showSharedDialog();
                //                }
            }
            
            @Override
            public void onRightLayClick()
            {
                // TODO Auto-generated method stub
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                // TODO Auto-generated method stub
                
            }
        });
    }
    
    /*
        private void showSharedDialog() {
            if (sharedDialog != null && sharedDialog.isShowing()) {
                sharedDialog.dismiss();
            } else {
                sharedDialog = new SharedDialog(mContext, R.style.MyDialogStyleTop);
                sharedDialog.show();
            }
        }*/
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
    
    public static void startActivity(Context context, String url, int title)
    {
        Intent it = new Intent();
        it.putExtra("url", url);
        it.putExtra("title", title);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        it.setClass(context, NoticeWebViewActivity.class);
        context.startActivity(it);
    }
    
    public static void startActivity(Context context, String url, String title)
    {
        Intent it = new Intent();
        it.putExtra("url", url);
        it.putExtra("title", title);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        it.setClass(context, NoticeWebViewActivity.class);
        context.startActivity(it);
    }
    
    public static void startActivity(Context context, String url, int title, boolean isShared)
    {
        Intent it = new Intent();
        it.putExtra("url", url);
        it.putExtra("title", title);
        it.putExtra("isShared", isShared);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        it.setClass(context, NoticeWebViewActivity.class);
        context.startActivity(it);
    }
}
