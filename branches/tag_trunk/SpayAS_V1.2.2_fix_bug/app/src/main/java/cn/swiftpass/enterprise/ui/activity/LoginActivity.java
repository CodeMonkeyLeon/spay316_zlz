package cn.swiftpass.enterprise.ui.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

//import com.ziyeyouhu.library.KeyboardUtil;


import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.common.DataObserver;
import cn.swiftpass.enterprise.bussiness.logica.common.DataObserverManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.NetworkUtils;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * Created with IntelliJ IDEA.
 * User: Alan
 * Date: 13-8-20
 * Time: 下午3:32
 * 登录
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private Context context;

    private EditText etName, etPwd;

    private SharedPreferences sp;

    private String imei = "no";

    private String imsi = "no";

    private TextView tvForget;

    private ImageView ivDown;

    private Button btnLogin;

    private EditText etMerchant;

    private RelativeLayout llMerchant;

    private ImageView lineMerchant;

    private boolean actionDrviceFlag;

    private ImageView ivClearUser;

    private ImageView ivClearPwd;

    private ImageView ivClearMId;
    public static int keyboardHeight = 0;
    boolean isVisiableForLast = false;

//    private LinearLayout rootView;
//    private ScrollView scrollView;
//    private KeyboardUtil keyboardUtil;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        context = this;
        try {

            imei = AppHelper.getIMEI();
            imsi = AppHelper.getIMSI();
            System.out.println("imei：" + imei + " imsi：" + imsi);
            if (imei == null) {
                //                showToastInfo(R.string.msg_login_no_imei);
                finish();
                return;
            }
        } catch (Exception e) {
            Logger.i(e.getMessage());
        }
        setContentView(R.layout.activity_login);
        sp = MainApplication.getContext().getApplicationPreferences();
        init();
    }

    @Override
    protected boolean isLoginRequired() {
        return false;
    }

    private void init() {

        actionDrviceFlag = getIntent().getBooleanExtra("isAction", false);
        ivDown = getViewById(R.id.iv_down);
        ivDown.setOnClickListener(this);
        etName = getViewById(R.id.et_username);
        etPwd = getViewById(R.id.et_pwd);

        llMerchant = getViewById(R.id.ll_merachant);
        lineMerchant = getViewById(R.id.lv_line);
        etMerchant = getViewById(R.id.et_merchantId);

        tvForget = getViewById(R.id.tv_forget);
        tvForget.setOnClickListener(this);
        btnLogin = getViewById(R.id.btn_login);
        String merchantNo = LocalAccountManager.getInstance().getMerchantNo();
        if (actionDrviceFlag || "".equals(merchantNo)) {
            //.setText(R.string.msg_login_action_d_title);
            llMerchant.setVisibility(View.VISIBLE);
            lineMerchant.setVisibility(View.VISIBLE);
        }
        ivClearMId = getViewById(R.id.iv_clearMId);
        ivClearMId.setOnClickListener(this);
        ivClearUser = getViewById(R.id.iv_clearUser);
        ivClearUser.setOnClickListener(this);
        ivClearPwd = getViewById(R.id.iv_clearPwd);
        ivClearPwd.setOnClickListener(this);
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (s != null && s.length() >= 1) {
                    if (etName.isFocused()) {
                        ivClearUser.setVisibility(View.VISIBLE);
                        ivClearPwd.setVisibility(View.GONE);
                        ivClearMId.setVisibility(View.GONE);
                    } else if (etPwd.isFocused()) {
                        ivClearPwd.setVisibility(View.VISIBLE);
                        ivClearMId.setVisibility(View.GONE);
                        ivClearUser.setVisibility(View.GONE);
                    } else if (etMerchant.isFocused()) {
                        ivClearMId.setVisibility(View.VISIBLE);
                        ivClearUser.setVisibility(View.GONE);
                        ivClearPwd.setVisibility(View.GONE);
                    }
                } else {
                    if (etName.isFocused()) {
                        ivClearUser.setVisibility(View.GONE);
                        ivClearPwd.setVisibility(View.GONE);
                        ivClearMId.setVisibility(View.GONE);
                    } else if (etPwd.isFocused()) {
                        ivClearUser.setVisibility(View.GONE);
                        ivClearPwd.setVisibility(View.GONE);
                        ivClearMId.setVisibility(View.GONE);
                    } else if (etMerchant.isFocused()) {
                        ivClearUser.setVisibility(View.GONE);
                        ivClearPwd.setVisibility(View.GONE);
                        ivClearMId.setVisibility(View.GONE);
                    }
                }
            }
        });
        etName.addTextChangedListener(editTextWatcher);
        etMerchant.addTextChangedListener(editTextWatcher);
        etPwd.addTextChangedListener(editTextWatcher);

        etMerchant.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    if (etMerchant.getText().length() >= 1) {
                        ivClearMId.setVisibility(View.VISIBLE);
                    } else {
                        ivClearMId.setVisibility(View.GONE);
                    }
                } else {
                    ivClearMId.setVisibility(View.GONE);
                }
            }
        });

        etName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    if (etName.getText().length() >= 1) {
                        ivClearUser.setVisibility(View.VISIBLE);
                    } else {
                        ivClearUser.setVisibility(View.GONE);
                    }
                } else {
                    ivClearUser.setVisibility(View.GONE);
                }
            }
        });
        etPwd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    if (etPwd.getText().length() >= 1) {
                        ivClearPwd.setVisibility(View.VISIBLE);
                    } else {
                        ivClearPwd.setVisibility(View.GONE);
                    }
                } else {
                    ivClearPwd.setVisibility(View.GONE);
                }
            }
        });

        //adsInfo初始化
        // AdsManager.getInstance().loadAdsInfo(new UINotifyListener<String>());
        //initData();
//        initMoveKeyBoard();
    }

//    private void initMoveKeyBoard() {
//        rootView = (LinearLayout) findViewById(R.id.id_rootView);
//        scrollView = (ScrollView) findViewById(R.id.sv_main);
//        keyboardUtil = new KeyboardUtil(this, rootView, scrollView);
//        keyboardUtil.setOtherEdittext(etName);
//        // monitor the KeyBarod state
//        keyboardUtil.setKeyBoardStateChangeListener(new KeyBoardStateListener());
//        // monitor the finish or next Key
//        keyboardUtil.setInputOverListener(new inputOverListener());
//        etPwd.setOnTouchListener(new KeyboardTouchListener(keyboardUtil, KeyboardUtil.INPUTTYPE_ABC, -1));
//    }





    private void initData() {
        //提示是否离线支付
        DataObserverManager.getInstance().registerObserver(new DataObserver(LoginActivity.class.getName(),
                DataObserver.EVENT_LOGIN_FAIL_OFFLINE_PAY) {
            @Override
            public void onChange() {
                showOfflineDialog();
            }
        });
    }

    private String mId;

    private boolean check() {
        // Animation anim = AnimationUtils.loadAnimation(this, R.anim.shake);
        String localMID = LocalAccountManager.getInstance().getMerchantNo();
        if ((actionDrviceFlag || "".equals(localMID))) {
            if (TextUtils.isEmpty(etMerchant.getText())) {
                showToastInfo(getStringById(R.string.business_number));
                etMerchant.setFocusable(true);
                return false;
            } else {
                mId = etMerchant.getText().toString();
            }
        } else {
            mId = localMID;
        }

        if (TextUtils.isEmpty(etName.getText())) {
            ToastHelper.showInfo(getStringById(R.string.please_Input_id));
            // etName.setBackgroundResource(R.drawable.bg_edit_text_wrong);
            etName.setFocusable(true);
            // etName.setAnimation(anim);
            return false;
        }
        if (TextUtils.isEmpty(etPwd.getText().toString())) {
            etPwd.setFocusable(true);
            ToastHelper.showInfo(getStringById(R.string.please_Input_password));
            return false;
        }

        return true;
    }

    public static void startActivity(Activity context) {
        Intent it = new Intent();
        it.setClass(context, WelcomeActivity.class);
        context.startActivity(it);
        context.overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);

    }

    public static void startActivity(Context context) {
        Intent it = new Intent();
        it.setClass(context, WelcomeActivity.class);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(it);

    }

    public static void startActivity(Context context, boolean isAction) {
        Intent it = new Intent();
        it.setClass(context, LoginActivity.class);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        it.putExtra("isAction", isAction);
        context.startActivity(it);

    }

    private PopupWindow popView;

    private MyAdapter dropDownAdapter;

    private void initPopView(List<UserModel> users) {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        for (int i = 0; i < users.size(); i++) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("name", users.get(i).name);
            //            map.put("drawable", R.drawable.btn_close);
            list.add(map);
        }
        dropDownAdapter =
                new MyAdapter(this, list, R.layout.down_item_layout, new String[]{"name", "drawable"}, new int[]{
                        R.id.textview, R.id.delete});
        ListView listView = new ListView(this);
        listView.setAdapter(dropDownAdapter);

        popView = new PopupWindow(listView, etName.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT, true);
        popView.setFocusable(true);
        popView.setOutsideTouchable(true);
        //popView.setBackgroundDrawable(getResources().getDrawable(R.drawable.white));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                checkData();
                break;
            case R.id.iv_clearMId:
                etMerchant.setText("");
                //LocalAccountManager.getInstance().clearMId();
                break;
            case R.id.iv_clearUser:
                etName.setText("");
                etPwd.setText("");
                LocalAccountManager.getInstance().clearUserName();
                LocalAccountManager.getInstance().clearPwd();
                break;
            case R.id.iv_clearPwd:
                etPwd.setText("");
                LocalAccountManager.getInstance().clearPwd();
                break;
            case R.id.tv_forget:
                break;
            case R.id.iv_down:
                if (popView != null) {
                    if (!popView.isShowing()) {
                        popView.showAsDropDown(etName);
                    } else {
                        popView.dismiss();
                    }
                } else {
                    // 如果有已经登录过账号
                    getLoginUsers();
                }
                break;

        }
    }

    private void getLoginUsers() {
        LocalAccountManager.getInstance().query(new UINotifyListener<List<UserModel>>() {
            @Override
            public void onSucceed(List<UserModel> result) {
                super.onSucceed(result);
                if (result.size() > 0) {
                    initPopView(result);
                    if (!popView.isShowing()) {
                        popView.showAsDropDown(etName);
                    } else {
                        popView.dismiss();
                    }

                }
            }
        });
    }

    private void checkData() {
        if (check()) {
            checkNet();
            final String name = etName.getText().toString();
            final String pwd = etPwd.getText().toString();

            if (actionDrviceFlag) {
                derviceAction(name, pwd);
            } else {
                login(name, pwd);
            }
        }
    }

    private void checkNet() {
        if (!NetworkUtils.isNetworkAvailable(this)) {
            ToastHelper.showError(getStringById(R.string.network_exception));
        } else {
            DataObserverManager.getInstance().registerObserver(new DataObserver(LoginActivity.class.getName(),
                    DataObserver.EVENT_LOGIN_FAIL_MAKE_DEVICE) {
                @Override
                public void onChange() {
                    showMakeDeviceDialog();
                }
            });
        }
    }

    private int loginFailCount = 0;

    private void login(String username, String password) {

        //        LocalAccountManager.getInstance().loginAsync(mId,
        //            username,
        //            password,
        //            imei,
        //            imsi,
        //            !actionDrviceFlag,
        //            new UINotifyListener<Boolean>()
        //            {
        //                @Override
        //                public void onPreExecute()
        //                {
        //                    super.onPreExecute();
        //                    showLoading(false, "登录中...");
        //                }
        //                
        //                @Override
        //                public void onPostExecute()
        //                {
        //                    super.onPostExecute();
        //                    dismissLoading();
        //                }
        //                
        //                @Override
        //                public void onError(Object object)
        //                {
        //                    super.onError(object);
        //                    dismissLoading();
        //                    if (object != null)
        //                        showToastInfo(object.toString());
        //                }
        //                
        //                @Override
        //                public void onSucceed(Boolean object)
        //                {
        //                    super.onSucceed(object);
        //                    dismissLoading();
        //                    
        //                    if (object)
        //                    {
        //                        ApiConstant.OFFLINE = false;
        //                        todo();
        //                    }
        //                }
        //            });

    }

    private void derviceAction(String username, String password) {
        LocalAccountManager.getInstance().activateDevice(username,
                password,
                imei,
                imsi,
                new UINotifyListener<Boolean>() {
                    @Override
                    public void onError(final Object object) {
                        super.onError(object);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (object != null) {
                                    // ToastHelper.showInfo(object.toString());
                                    DialogHelper.showDialog(null, object.toString(), R.string.btnOk, context).show();
                                }
                            }
                        });
                    }

                    @Override
                    public void onPreExecute() {
                        super.onPreExecute();
                        showLoading(true, getStringById(R.string.wait_a_moment));
                    }

                    @Override
                    public void onPostExecute() {
                        super.onPostExecute();
                        dismissLoading();
                    }

                    @Override
                    public void onSucceed(Boolean result) {
                        super.onSucceed(result);
                        if (result) {
                            ToastHelper.showInfo(getStringById(R.string.device_has_activated));
                            btnLogin.setText(getStringById(R.string.login));
                            if (actionDrviceFlag) {

                                Intent it = new Intent();
                                it.setClass(LoginActivity.this, WelcomeActivity.class);
                                LoginActivity.this.startActivity(it);
                                finish();
                            }
                        }
                    }
                });

    }

    private void todo() {
        //启动心跳
        //HearbateService.startService(context);
        MainActivity.startActivity(context, "LoginActivity");
        //不是操作员就是管理员
        /* if (LocalAccountManager.getInstance().getLoggedUser().role == UserRole.approve) {
             RefundActivity.startActivity(context);  //审核员
         } else  if( LocalAccountManager.getInstance().getLoggedUser().role == UserRole.general){
             PayActivity.startActivity(context); //普通用户
           }else if( LocalAccountManager.getInstance().getLoggedUser().role == UserRole.manager){
             SearchActivity.startActivity(context);  //管理员
         }*/
        finish();
    }

    private void showMakeDeviceDialog() {
        final String name = etName.getText().toString();
        final String pwd = etPwd.getText().toString();
        DialogHelper.showDialog(getStringById(R.string.public_prompt),
                getStringById(R.string.activate_immediately),
                R.string.btnCancel,
                R.string.btnOk,
                context,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        derviceAction(name, pwd);
                    }
                }).show();
    }

    private void showOfflineDialog() {
        //        DialogHelper.showDialog(getString(R.string.title_dialog),
        //            getString(R.string.msg_login_fail_info),
        //            R.string.btnCancel,
        //            R.string.btnOk,
        //            context,
        //            new DialogInterface.OnClickListener()
        //            {
        //                @Override
        //                public void onClick(DialogInterface dialogInterface, int i)
        //                {
        //                    //需要设备ID 用户 密码  先比较用户密码 如果发现有两个账号一样 商户不一样需要提示
        //                    UserModel userModel =
        //                        LocalAccountManager.getInstance().checkInUser(etName.getText().toString(),
        //                            etPwd.getText().toString());
        //                    if (userModel != null)
        //                    {
        //                        ApiConstant.OFFLINE = true;
        //                        //todo();
        //                    }
        //                    else
        //                    {
        //                        showToastInfo(R.string.msg_login_fial_userAndPwd);
        //                    }
        //                    
        //                }
        //            }).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //        menu.add(1, 1, 1, getString(R.string.menu_setting));
        //        menu.add(1, 1, 2, getString(R.string.menu_aboutus));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                //设置
                showDialog(SettingNetWork.getInstance().showNetWorkSet(LoginActivity.this));
                break;
            case 2:
                //使用手册
                Intent aboutUs = new Intent(context, AboutUsActivity.class);
                startActivity(aboutUs);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sp != null) {
            String name = sp.getString(GlobalConstant.FIELD_USERNAME, "");
            etName.setText(name);
            if (!"".equals(name)) {
                try {
                    etName.setSelection(name.length());
                } catch (Exception e) {
                    Logger.i(e);
                }
            }
            etPwd.setText(sp.getString(GlobalConstant.FIELD_PAWESSORD, ""));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DataObserverManager.getInstance().unregisterObserver(DataObserver.EVENT_LOGIN_FAIL_MAKE_DEVICE,
                LoginActivity.class.getName());
        DataObserverManager.getInstance().unregisterObserver(DataObserver.EVENT_LOGIN_FAIL_OFFLINE_PAY,
                LoginActivity.class.getName());
    }

    class MyAdapter extends SimpleAdapter {

        private List<HashMap<String, Object>> data;

        public MyAdapter(Context context, List<HashMap<String, Object>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(LoginActivity.this).inflate(R.layout.down_item_layout, null);
                holder.btn = (ImageButton) convertView.findViewById(R.id.delete);
                holder.tv = (TextView) convertView.findViewById(R.id.textview);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.tv.setText(data.get(position).get("name").toString());
            holder.tv.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    popView.dismiss();
                }
            });
            holder.btn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                }
            });
            return convertView;
        }
    }

    class ViewHolder {
        private TextView tv;

        private ImageButton btn;
    }

}
