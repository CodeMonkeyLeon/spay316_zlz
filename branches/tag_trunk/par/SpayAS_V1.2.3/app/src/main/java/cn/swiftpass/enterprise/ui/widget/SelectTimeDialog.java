package cn.swiftpass.enterprise.ui.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.widget.timewheel.WheelMain;
import cn.swiftpass.enterprise.utils.DateUtil;

/**
 * Created with IntelliJ IDEA.
 * User: Alan
 * Date: 13-10-16
 * Time: 上午10:49
 * To change this template use File | Settings | File Templates.
 */
public class SelectTimeDialog
{
    private static final String TAG = SelectTimeDialog.class.getSimpleName();
    private static WheelMain wheelMain;
    
    private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    
    public static void createDatadialog(String defaultTime, Activity mContext, final OnMyClickListener listener)
    {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final View timepickerview = inflater.inflate(R.layout.timepicker, null);
        ScreenInfo screenInfo = new ScreenInfo(mContext);
        wheelMain = new WheelMain(timepickerview);
        wheelMain.screenheight = screenInfo.getHeight();
        Calendar calendar = Calendar.getInstance();
        if (DateUtil.isDate(defaultTime, "yyyy-MM-dd"))
        {
            try
            {
                calendar.setTime(dateFormat.parse(defaultTime));
            }
            catch (ParseException e)
            {
                Log.e(TAG,Log.getStackTraceString(e));
            }
        }
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        wheelMain.initDateTimePicker(year, month, day);
        new AlertDialog.Builder(mContext).setTitle("选择时间")
            .setView(timepickerview)
            .setPositiveButton("确定", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    // etDatetime.setText(wheelMain.getTime());
                    Date selectDate = wheelMain.getDate();
                    listener.onOk(selectDate.getTime());
                }
            })
            .setNegativeButton("取消", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    listener.onCancel();
                }
                
            })
            .show();
        
    }
    
    public interface OnMyClickListener
    {
        void onOk(long datetime);
        
        void onCancel();
    }
}
