package cn.swiftpass.enterprise.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.widget.PageScrollGroup;
import cn.swiftpass.enterprise.ui.widget.PicPopupWindow;
import cn.swiftpass.enterprise.utils.GlobalConstant;

/**
 * 新手指导UI
 * User: hehui
 * Date: 13-11-16
 * Time: 下午2:33
 * To change this template use File | Settings | File Templates.
 */
public class WelcomeGuideActivity extends BaseActivity implements View.OnClickListener
{
    private ImageView iv_select1, iv_select2, iv_select3, iv_select4;
    
    private PageScrollGroup pageScrollGroup;
    
    private int currentTab = 0;
    
    private boolean isFirstView = false;
    
    private Button btnLogin;
    
    private Button btnQuickLogin;
    
    private Context mContext;
    
    private View v1, v2;
    
    private Button btnDerviceAction;
    
    private PicPopupWindow popupWindow;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mContext = this;
        initViews();
    }
    
    private void initViews()
    {
        
        setContentView(R.layout.activity_welcome_guide);
        pageScrollGroup = (PageScrollGroup)findViewById(R.id.pagescroll);
        btnQuickLogin = getViewById(R.id.btn_quick_login);
        btnQuickLogin.setOnClickListener(this);
        //        btnLogin = getViewById(R.id.btn_vwt_login);
        //        btnLogin.setOnClickListener(this);
        //        btnDerviceAction = getViewById(R.id.btn_device_action);
        //        btnDerviceAction.setOnClickListener(this);
        
        v1 = getViewById(R.id.ll_bottom);
        //        v2 = getViewById(R.id.ll_bottom2);
        
        iv_select1 = getViewById(R.id.iv_select1);
        //        iv_select2 = getViewById(R.id.iv_select2);
        iv_select3 = getViewById(R.id.iv_select3);
        //        iv_select4 = getViewById(R.id.iv_select4);
        pageScrollGroup.setOnViewChangeListener(new PageScrollGroup.OnViewChangeListener()
        {
            
            @Override
            public void OnViewChange(int index)
            {
                currentTab = index;
                isFirstView = false;
                updateViews();
                
            }
        });
        
    }
    
    private void updateViews()
    {
        switch (currentTab)
        {
        
            case 0:
                iv_select1.setVisibility(View.VISIBLE);
                //                iv_select2.setVisibility(View.VISIBLE);
                iv_select3.setVisibility(View.VISIBLE);
                //                iv_select4.setVisibility(View.VISIBLE);
                isFirstView = true;
                iv_select1.setImageResource(R.drawable.guide_round_selected);
                //                iv_select2.setImageResource(R.drawable.guide_round_normal);
                iv_select3.setImageResource(R.drawable.guide_round_normal);
                //                iv_select4.setImageResource(R.drawable.icon_dot_default);
                if (!getFlag())
                {
                    //                    showPop();
                    saveFlag();
                }
                break;
            case 1:
                //                iv_select1.setVisibility(View.VISIBLE);
                //                iv_select2.setVisibility(View.VISIBLE);
                //                iv_select3.setVisibility(View.VISIBLE);
                //                //                iv_select4.setVisibility(View.VISIBLE);
                //                iv_select1.setImageResource(R.drawable.guide_round_normal);
                //                iv_select2.setImageResource(R.drawable.guide_round_selected);
                //                iv_select3.setImageResource(R.drawable.guide_round_normal);
                
                v1.setVisibility(View.VISIBLE);
                iv_select1.setVisibility(View.GONE);
                //                iv_select2.setVisibility(View.GONE);
                iv_select3.setVisibility(View.GONE);
                //                iv_select4.setImageResource(R.drawable.icon_dot_default);
                break;
            //            case 2:
            //                //                iv_select1.setVisibility(View.VISIBLE);
            //                //                iv_select2.setVisibility(View.VISIBLE);
            //                //                iv_select3.setVisibility(View.VISIBLE);
            //                //                iv_select4.setVisibility(View.VISIBLE);
            //                //                iv_select1.setImageResource(R.drawable.guide_round_normal);
            //                //                iv_select2.setImageResource(R.drawable.guide_round_normal);
            //                //                iv_select3.setImageResource(R.drawable.guide_round_selected);
            //                
            //                v1.setVisibility(View.VISIBLE);
            //                iv_select1.setVisibility(View.GONE);
            //                iv_select2.setVisibility(View.GONE);
            //                iv_select3.setVisibility(View.GONE);
            //                //                iv_select4.setImageResource(R.drawable.icon_dot_default);
            //                break;
            case 2:
                //                iv_select1.setImageResource(R.drawable.icon_dot_default);
                //                iv_select2.setImageResource(R.drawable.icon_dot_default);
                //                iv_select3.setImageResource(R.drawable.icon_dot_default);
                //                iv_select4.setImageResource(R.drawable.icon_dot_choose);
                // String partner = LocalAccountManager.getInstance().getMerchantNo();
                //                v1.setVisibility(View.VISIBLE);
                //                iv_select1.setVisibility(View.GONE);
                //                iv_select2.setVisibility(View.GONE);
                //                iv_select3.setVisibility(View.GONE);
                //                iv_select4.setVisibility(View.GONE);
                
                //v2.setVisibility(View.GONE);
                
                break;
        }
    }
    
    public static Boolean getFlag()
    {
        MainApplication.getContext().getApplicationPreferences();
        SharedPreferences sp = MainApplication.getContext().getApplicationPreferences();
        return sp.getBoolean(GlobalConstant.FIELD_IS_FRIST_WELCOME, false);
    }
    
    public void saveFlag()
    {
        MainApplication.getContext().getApplicationPreferences();
        SharedPreferences sp = MainApplication.getContext().getApplicationPreferences();
        sp.edit().putBoolean(GlobalConstant.FIELD_IS_FRIST_WELCOME, true).commit();
    }
    
    //FIELD_IS_FRIST_WELCOME
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    public static void startActivity(Context context)
    {
        Intent it = new Intent();
        it.setClass(context, WelcomeGuideActivity.class);
        context.startActivity(it);
        
    }
    
    @Override
    public void onClick(View view)
    {
        if (view.getId() == R.id.btn_quick_login)
        {
            
            saveFlag();
            WelcomeGuideActivity.this.finish();
        }
    }
    
    @Override
    public void onWindowFocusChanged(boolean hasFocus)
    {
        // TODO Auto-generated method stub
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus)
        {
            if (!getFlag())
            {
                //                showPop();
                saveFlag();
            }
        }
    }
    
    private void showPop()
    {
        if (popupWindow != null && popupWindow.isShowing())
        {
            popupWindow.dismiss();
            popupWindow = null;
        }
        else
        {
            if (!isFirstView && currentTab == 0)
            {
                isFirstView = true;
                popupWindow = new PicPopupWindow(this, 0);
                //                View view = LayoutInflater.from(mContext).inflate(R.layout.view_welcome_first, null);
                //                popupWindow.showAtLocation(view.findViewById(R.id.view1), Gravity.CENTER, 0, 0);
            }
        }
    }
}
