package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.MySpeakerBindContract
import com.example.common.entity.EmptyData
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @如果你是条船
 * @漂泊就是你的命运
 * @可别靠岸
 */
class MySpeakerBindPresenter : MySpeakerBindContract.Presenter {

    private var mView: MySpeakerBindContract.View? = null


    override fun unbindSpeaker(userId: String?, deviceName: String?) {
        mView?.let { view ->
            view.showLoading(R.string.public_loading, ParamsConstants.COMMON_LOADING)
            AppClient.unbindSpeaker(
                userId,
                deviceName,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.unbindSpeakerFailed(it.message)
                        }
                    }


                    override fun onResponse(response: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        view.unbindSpeakerSuccess(true)
                    }
                }
            )
        }
    }

    override fun bindSpeaker(deviceName: String?) {
        mView?.let { view ->

            view.showLoading(R.string.public_loading, ParamsConstants.COMMON_LOADING)

            AppClient.bindSpeaker(
                deviceName,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.bindSpeakerFailed(it.message)
                        }
                    }


                    override fun onResponse(response: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        view.bindSpeakerSuccess(true)
                    }
                }
            )
        }
    }

    override fun attachView(view: MySpeakerBindContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}