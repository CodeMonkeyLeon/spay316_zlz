package cn.swiftpass.enterprise.ui.widget;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.SelectListModel;
import cn.swiftpass.enterprise.ui.adapter.ListSelectAdapter;

/** 带checkbox对话框 */
public class SelectItemDialog extends Dialog implements View.OnClickListener, OnItemClickListener
{
    
    private ListView listView;
    
    private TextView tvTitle;
    
    private ListSelectAdapter adapter;
    
    private ViewGroup mRootView;
    
    private TextView btnOk;
    
    private TextView btnCancel;
    
    private OnButtonListener listener;
    
    private String title;
    
    private LinearLayout llAddItemInput, llAddItem;
    
    private TextView tvOther;// W.l添加其它
    
    private EditText etInputContent;
    
    private ImageView ivAddIco, ivAddItemInputLine;
    
    private boolean isOpen = true;
    
    private String definedtxt;
    
    private Handler mHandler;
    
    protected SelectItemDialog(Context context, ListSelectAdapter adapter, String title, OnButtonListener listener,
        HashSet<SelectListModel> postModels, String useDaysDefinedtxt, Handler mHandler)
    {
        super(context);// , R.style.baseDialog
        this.listener = listener;
        this.adapter = adapter;
        this.title = title;
        if (useDaysDefinedtxt != null && !useDaysDefinedtxt.equals(""))
        {
            if (useDaysDefinedtxt.substring(0, 1).equals(","))
            {
                this.definedtxt = useDaysDefinedtxt.substring(1, useDaysDefinedtxt.length());
            }
            else
            {
                this.definedtxt = useDaysDefinedtxt;
            }
        }
        else
        {
            this.definedtxt = useDaysDefinedtxt;
        }
        if (postModels != null)
        {
            this.data = postModels;
        }
        else
        {
            this.data = new HashSet<SelectListModel>();
        }
        this.mHandler = mHandler;
        initViews();
    }
    
    private void initViews()
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.dialog_select, null);
        setContentView(mRootView);
        listView = (ListView)findViewById(R.id.listView);
        listView.setOnItemClickListener(this);
        
        listView.addFooterView(getLayoutInflater().inflate(R.layout.view_footview_selectitem_dialog, null));
        
        listView.setAdapter(adapter);
        tvTitle = (TextView)findViewById(R.id.title);
        tvTitle.setText(title);
        
        llAddItem = (LinearLayout)findViewById(R.id.ll_add_item);
        llAddItem.setOnClickListener(this);
        tvOther = (TextView)findViewById(R.id.tv_other);
        
        ivAddIco = (ImageView)findViewById(R.id.iv_add_ico);
        llAddItemInput = (LinearLayout)findViewById(R.id.ll_add_item_input);
        ivAddItemInputLine = (ImageView)findViewById(R.id.iv_add_item_input_line);
        etInputContent = (EditText)findViewById(R.id.et_input_content);
        if (title.endsWith("选择使用日期"))
        { // W.l识别标题显示添加提示
            tvOther.setText("添加其它使用日期");
            if (definedtxt != null)
            {
                etInputContent.setText(definedtxt);
                llAddItemInput.setVisibility(View.VISIBLE);
                ivAddItemInputLine.setVisibility(View.VISIBLE);
            }
            else
            {
                etInputContent.setHint("添加其它使用日期");
            }
            for (SelectListModel selectListModel : data)
            {
                if (selectListModel.isCheck == true && selectListModel.id == 1)
                {
                    isCheckboxState = true;
                }
            }
        }
        else if (title.endsWith("选择使用时段"))
        {
            tvOther.setText("添加其它使用时段");
            if (definedtxt != null)
            {
                etInputContent.setText(definedtxt);
                llAddItemInput.setVisibility(View.VISIBLE);
                ivAddItemInputLine.setVisibility(View.VISIBLE);
            }
            else
            {
                etInputContent.setHint("添加其它使用时段");
            }
            for (SelectListModel selectListModel : data)
            {
                if (selectListModel.isCheck == true && selectListModel.id == 1)
                {
                    isCheckboxState = true;
                }
            }
        }
        else if (title.endsWith("选择使用说明"))
        {
            tvOther.setText("添加其它使用说明");
            if (definedtxt != null)
            {
                etInputContent.setText(definedtxt);
                llAddItemInput.setVisibility(View.VISIBLE);
                ivAddItemInputLine.setVisibility(View.VISIBLE);
            }
            else
            {
                etInputContent.setHint("添加其它使用说明");
            }
        }
        btnOk = (TextView)findViewById(R.id.btnOk);
        btnOk.setOnClickListener(this);
        btnCancel = (TextView)findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(this);
    }
    
    private boolean isCheckboxState;
    
    public static void show(Context context, String title, ListSelectAdapter adapter, OnButtonListener listener,
        HashSet<SelectListModel> requestData, String useDaysDefinedtxt, Handler mHandler)
    {
        SelectItemDialog dialog =
            new SelectItemDialog(context, adapter, title, listener, requestData, useDaysDefinedtxt, mHandler);
        
        dialog.show();
    }
    
    @Override
    public void onClick(View v)
    {
        String addContent = "";
        switch (v.getId())
        {
            case R.id.btnOk:
                if (adapter.checkData.size() >= 0)
                {
                    data.addAll(adapter.checkData);
                }
                if (!TextUtils.isEmpty(etInputContent.getText().toString()))
                {
                    addContent = etInputContent.getText().toString();
                }
                listener.onOK(data, addContent);
                dismiss();
                break;
            case R.id.btnCancel:
                listener.onCancel();
                dismiss();
                break;
            case R.id.ll_add_item:
                if (isOpen)
                {
                    etInputContent.requestFocus();
                    llAddItemInput.setVisibility(View.VISIBLE);
                    ivAddItemInputLine.setVisibility(View.VISIBLE);
                    ivAddIco.startAnimation(getRotateAnimation(-270, 0, 300));
                    isOpen = false;
                    listView.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            listView.setSelection(adapter.getCount() - 1);
                        }
                    });
                    ivAddIco.setBackgroundResource(R.drawable.n_icon_subtraction);
                }
                else
                {
                    etInputContent.clearFocus();
                    llAddItemInput.setVisibility(View.GONE);
                    ivAddItemInputLine.setVisibility(View.GONE);
                    ivAddIco.startAnimation(getRotateAnimation(0, -270, 300));
                    isOpen = true;
                    ivAddIco.setBackgroundResource(R.drawable.n_icon_add);
                    /*
                     * listView.post(new Runnable() {
                     * 
                     * @Override public void run() {
                     * listView.setSelection(adapter.getCount()-1); } });
                     */
                    
                }
                break;
        }
        
    }
    
    public interface OnButtonListener
    {
        public void onOK(HashSet<SelectListModel> data, String addContent);
        
        public void onCancel();
    }
    
    HashSet<SelectListModel> data;
    
    @Override
    public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3)
    {
        SelectListModel s = (SelectListModel)adapter.getItem(position);
        if (view != null)
        {
            CheckBox cb = (CheckBox)view.findViewById(R.id.checkbox);
            if (cb != null)
            {
                if (cb.isChecked())
                {
                    s.isCheck = false;
                    data.remove(s);
                    cb.setChecked(false);
                    if (s.id == 1 && title.endsWith("选择使用日期") || s.id == 1 && title.endsWith("选择使用时段"))
                    {
                        isCheckboxState = false;
                    }
                }
                else
                {
                    if (s.id == 1 && title.endsWith("选择使用日期") || s.id == 1 && title.endsWith("选择使用时段"))
                    {
                        for (SelectListModel sss : data)
                        {
                            sss.isCheck = false;
                        }
                        isCheckboxState = true;
                        s.isCheck = true;
                        if (s.id == 10 && title.endsWith("选择使用日期"))
                        {
                            mHandler.sendEmptyMessage(1);
                        }
                        else if (s.id == 4 && title.endsWith("选择使用时段"))
                        {
                            mHandler.sendEmptyMessage(2);
                        }
                        data.clear();
                        data.add(s);
                        adapter.notifyDataSetChanged();
                    }
                    else
                    {
                        if (isCheckboxState)
                        {
                            cb.setClickable(true);
                        }
                        else
                        {
                            cb.setClickable(false);
                            s.isCheck = true;
                            data.add(s);
                            cb.setChecked(true);
                        }
                        
                    }
                    
                }
            }
        }
    }
    
    /***
     * 处理对话框选择的值
     * 
     * @param temData
     * @param addContent
     * @param ids
     * @param tvDisplay
     * @return
     */
    @SuppressWarnings("unchecked")
    public static String executeDialogRest(HashSet<SelectListModel> temData, String addContent, Set<Integer> ids,
        TextView tvDisplay)
    {
        ArrayList<SelectListModel> data = new ArrayList<SelectListModel>();
        String result = "";
        String retVal = "";
        if (ids.size() > 0)
        { // W.l修改 // 当temData这个==0的时候，要所有的item都没有的时候才为0，会导致在点击会选中状态
            ids.clear();
        }
        if (temData.size() > 0)
        {
            for (SelectListModel s : temData)
            {
                data.add(s);
                ids.add(s.id);
            }
            Collections.sort(data, new Comparator()
            { // W.l 做一下排序
                
                    public int compare(Object lhs, Object rhs)
                    {
                        SelectListModel c1 = (SelectListModel)lhs;
                        SelectListModel c2 = (SelectListModel)rhs;
                        if (c2.id > c1.id)
                        {
                            return 1;
                        }
                        return -1;
                    }
                    
                });
            // Collections.sort(data, Comparators.getComparator());
            for (int i = data.size() - 1; i >= 0; i--)
            {
                SelectListModel s = data.get(i);

                retVal = new StringBuffer(retVal).append(s.id + ",").toString();

                result = new StringBuffer(result).append(s.id + ",").toString();

              /*  retVal += s.id + ",";
                result += s.title + ",";*/
            }
            if (result != null)
            {
                result += addContent + ",";
            }
            int idx;
            if (!"".equals(addContent))
            {
                idx = result.length() - 1;
            }
            else
            {
                idx = result.length() - 2; // W.l lastIndexOf(",") 去掉最后一个逗号
            }
            if (idx != -1)
            {
                String ret = result.substring(0, idx);
                /*
                 * if(ret.length() >= 15){ tvDisplay.setText(result.substring(0,
                 * 15)+"..."); }else{ }
                 */
                if (result.contains("周一可"))
                {
                    ret = ret.replaceAll("可使用", "").replaceAll("周", ""); // W.L
                                                                         // 去掉多余可使用
                                                                         // 内容
                    tvDisplay.setText("周" + ret + "可使用");
                }
                else if (result.contains("只限") && !result.contains("只限堂食"))
                {
                    ret = ret.replaceAll("使用", "").replaceAll("只限", "");
                    tvDisplay.setText("只限" + ret + "使用");
                }
                else
                {
                    tvDisplay.setText(ret); // W.L 显示全部内容
                }
                
            }
            else
            {
                tvDisplay.setText(result);
            }
            String str = retVal.substring(0, retVal.lastIndexOf(","));
            return str;
        }
        else
        {
            if (addContent.length() > 0)
            {
                tvDisplay.setText(addContent + "");
                return addContent;
            }
            else
            {
                tvDisplay.setText("");
                ids.clear();
            }
            return "";
        }
        
    }
    
    public Animation getRotateAnimation(float fromDegrees, float toDegrees, int durationMillis)
    {
        RotateAnimation rotate =
            new RotateAnimation(fromDegrees, toDegrees, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        rotate.setDuration(durationMillis);
        rotate.setFillAfter(true);
        return rotate;
    }
}
