package cn.swiftpass.enterprise.ui.paymentlink.adapter;

/**
 * 加载更多基类  主要处理加载更多布局显示隐藏状态
 *
 * @author shican
 */
public abstract class BaseLoadMoreView {
    public static final int STATUS_DEFAULT = 1;
    public static final int STATUS_LOADING = 2;
    public static final int STATUS_FAIL = 3;
    public static final int STATUS_END = 4;
    public static final int STATUS_CLICK = 5;

    private int mLoadMoreStatus = STATUS_DEFAULT;
    //加载更多后是否隐藏加载更多view
    private boolean mLoadMoreEndGone = false;

    public void setLoadMoreStatus(int loadMoreStatus) {
        this.mLoadMoreStatus = loadMoreStatus;
    }

    public int getLoadMoreStatus() {
        return mLoadMoreStatus;
    }

    public void setLoadStatus(BaseViewHolder holder) {
        switch (mLoadMoreStatus) {
            case STATUS_LOADING:
                goneLoading(holder, false);
                goneLoadFail(holder, true);
                goneLoadEnd(holder, true);
                goneLoadClick(holder, true);
                break;
            case STATUS_FAIL:
                goneLoading(holder, true);
                goneLoadFail(holder, false);
                goneLoadEnd(holder, true);
                goneLoadClick(holder, true);
                break;
            case STATUS_END:
                goneLoading(holder, true);
                goneLoadFail(holder, true);
                goneLoadEnd(holder, false);
                goneLoadClick(holder, true);
                break;
            case STATUS_DEFAULT:
                goneLoading(holder, true);
                goneLoadFail(holder, true);
                goneLoadEnd(holder, true);
                goneLoadClick(holder, true);
                break;
            case STATUS_CLICK:
                goneLoading(holder, true);
                goneLoadFail(holder, true);
                goneLoadEnd(holder, true);
                goneLoadClick(holder, false);
                break;
            default:
                break;
        }
    }

    private void goneLoading(BaseViewHolder holder, boolean gone) {
        holder.setGone(getLoadingViewId(), gone);
    }

    private void goneLoadFail(BaseViewHolder holder, boolean gone) {
        holder.setGone(getLoadFailViewId(), gone);
    }

    private void goneLoadClick(BaseViewHolder holder, boolean gone) {
        holder.setGone(getLoadClickViewId(), gone);
    }

    private void goneLoadEnd(BaseViewHolder holder, boolean gone) {
        final int loadEndViewId = getLoadEndViewId();
        if (loadEndViewId != 0) {
            holder.setGone(loadEndViewId, gone);
        }
    }

    public final void setLoadMoreEndGone(boolean loadMoreEndGone) {
        this.mLoadMoreEndGone = loadMoreEndGone;
    }

    public final boolean isLoadEndMoreGone() {
        if (getLoadEndViewId() == 0) {
            return true;
        }
        return mLoadMoreEndGone;
    }

    public abstract int getLayoutId();

    protected abstract int getLoadingViewId();

    protected abstract int getLoadFailViewId();

    protected abstract int getLoadEndViewId();

    protected abstract int getLoadClickViewId();
}
