package cn.swiftpass.enterprise.partyking.view.refresh

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import cn.swiftpass.enterprise.intl.R
import com.scwang.smartrefresh.layout.api.RefreshFooter
import com.scwang.smartrefresh.layout.api.RefreshKernel
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.constant.RefreshState
import com.scwang.smartrefresh.layout.constant.SpinnerStyle

class PtkRefreshFooter(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyle: Int = 0
) : LinearLayout(context, attributeSet, defStyle), RefreshFooter {


    companion object {
        const val TAG = "PtkRefreshFooter"
    }

    private lateinit var mImgRefresh: ImageView
    private lateinit var mProgressBarRefresh: ProgressBar
    private lateinit var mTvContent: TextView
    private lateinit var mTvSubContent: TextView


    private var mPullUpToLoadText = "PullUpToLoad"
    private var mReleaseToLoadText = "ReleaseToLoad"
    private var mLoadingText = "Loading"


    fun setLoadText(pullUpText: String, releaseText: String, loadingText: String) {
        mPullUpToLoadText = pullUpText
        mReleaseToLoadText = releaseText
        mLoadingText = loadingText
    }


    constructor(context: Context) : this(context, null, 0) {

    }

    constructor(context: Context, attributeSet: AttributeSet?) : this(context, attributeSet, 0) {

    }


    init {
        initView()
    }


    private fun initView() {
        LayoutInflater.from(context).inflate(R.layout.pull_to_refresh_header, this)
        mImgRefresh = findViewById(R.id.pull_to_refresh_image)
        mProgressBarRefresh = findViewById(R.id.pull_to_refresh_progress)
        mTvContent = findViewById(R.id.pull_to_refresh_text)
        mTvSubContent = findViewById(R.id.pull_to_refresh_sub_text)
        mTvSubContent.visibility = View.GONE
    }


    override fun onStateChanged(
        refreshLayout: RefreshLayout,
        oldState: RefreshState,
        newState: RefreshState
    ) {
        when (newState) {
            RefreshState.PullUpToLoad -> {
                //下拉加载
                mTvContent.text = mPullUpToLoadText
                mImgRefresh.visibility = View.VISIBLE
                mProgressBarRefresh.visibility = View.GONE
                mImgRefresh.setImageResource(R.drawable.arrow_up)
            }
            RefreshState.ReleaseToLoad -> {
                //释放加载
                mTvContent.text = mReleaseToLoadText
                mImgRefresh.visibility = View.VISIBLE
                mProgressBarRefresh.visibility = View.GONE
                mImgRefresh.setImageResource(R.drawable.arrow_down)
            }
            RefreshState.Loading -> {
                //加载中
                mTvContent.text = mLoadingText
                mProgressBarRefresh.visibility = View.VISIBLE
                mImgRefresh.visibility = View.GONE
            }
        }
    }

    override fun getView() = this

    override fun getSpinnerStyle() = SpinnerStyle.Translate

    override fun onFinish(refreshLayout: RefreshLayout, success: Boolean) = 0

    override fun isSupportHorizontalDrag() = false

    override fun setNoMoreData(noMoreData: Boolean) = false

    override fun setPrimaryColors(vararg colors: Int) {

    }

    override fun onInitialized(kernel: RefreshKernel, height: Int, maxDragHeight: Int) {
        //控制是否稍微上滑动就刷新
        kernel.refreshLayout.setEnableAutoLoadMore(false)
    }

    override fun onMoving(
        isDragging: Boolean,
        percent: Float,
        offset: Int,
        height: Int,
        maxDragHeight: Int
    ) {

    }

    override fun onReleased(refreshLayout: RefreshLayout, height: Int, maxDragHeight: Int) {

    }

    override fun onStartAnimator(refreshLayout: RefreshLayout, height: Int, maxDragHeight: Int) {

    }

    override fun onHorizontalDrag(percentX: Float, offsetX: Int, offsetMax: Int) {

    }
}