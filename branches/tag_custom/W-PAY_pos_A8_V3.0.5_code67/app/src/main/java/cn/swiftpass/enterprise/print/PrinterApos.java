package cn.swiftpass.enterprise.print;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;

import com.landicorp.android.eptapi.device.Printer;
import com.landicorp.android.eptapi.device.Printer.Alignment;
import com.landicorp.android.eptapi.device.Printer.Format;
import com.landicorp.android.eptapi.exception.RequestException;
import com.landicorp.android.eptapi.utils.QrCode;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Locale;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 新大陆 N900打印 <一句话功能简述> <功能详细描述>
 * 
 * @author he_hui
 * @version [版本号, 2016-2-4]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public abstract class PrinterApos {

	private Context context;

	private StaticLayout mLayout = null;

	private TextPaint mPaint;

	private Canvas mCanvas;

	private String content = null;

	private Printer.Progress progress = new Printer.Progress() {

		@Override
		public void doPrint(Printer arg0) throws Exception {

		}

		@Override
		public void onFinish(int code) {
			if (code == Printer.ERROR_NONE) {
				displayPrinterInfo("PRINT SUCCESS END ");
			}
			/**
			 * Has some error. Here is display it, but you may want to hanle the
			 * error such as ERROR_OVERHEAT、ERROR_BUSY、ERROR_PAPERENDED to start
			 * again in the right time later.
			 */
			else {
			    
				displayPrinterInfo("PRINT ERR - " + (code));
			}
		}

		@Override
		public void onCrash() {
			onDeviceServiceCrash();
		}

	};

	/**
	 * Search card and show all track info
	 */
	public void startPrint() {
		try {
			progress.start();
			// DeviceService.logout();
		} catch (RequestException e) {
			onDeviceServiceCrash();
		}
	}

	protected abstract void displayPrinterInfo(String info);

	protected abstract void onDeviceServiceCrash();

	public PrinterApos(Context context, final boolean isCashierShow, final Bitmap bitmapLogo, final Order orderModel)
    {
        this.context = context;

        progress.addStep(new Printer.Step() {

            @Override
            public void doPrint(Printer printer)
                    throws Exception {
              /*  printer.setAutoTrunc(true);

                // Default mode is real mode, now set it to virtual mode.
                printer.setMode(Printer.MODE_REAL);*/
                printer.setAutoTrunc(false);
                printer.setPageSpace(1500);

                Format format = new Format();
                format.setAscSize(Format.ASC_DOT24x12);
                format.setAscScale(Format.ASC_SC1x2);
                format.setHzSize(Format.HZ_DOT24x24);
                format.setHzScale(Format.HZ_SC1x2);
                printer.setFormat(format);


//                Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.luso_logo);
                /**
                 *  打印图片
                 */
 /*               if (null != bitmap) {
                    byte[] bytes = Bitmap2Bytes(bitmap);
                    InputStream bitmapStream = BytesToInStream(bytes);
//                    printer.printImage(0, 300, 40, bytes);
                    printer.printImage(Alignment.CENTER,bitmapStream);
                }*/

                format.setAscSize(Printer.Format.ASC_DOT24x12);
                format.setAscScale(Printer.Format.ASC_SC1x1);
                format.setHzSize(Printer.Format.HZ_DOT24x24);
                format.setHzScale(Printer.Format.HZ_SC1x1);
                printer.setFormat(format);

                printer.printText("-------------------------------\r\n");
                printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_Merchant_name) + "  " + MainApplication.getMchName() + "\n");

                if (!TextUtils.isEmpty(orderModel.getOutMchId())) {
                    printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_MID) + orderModel.getOutMchId() + "\n");
                } else {
                    if (!StringUtil.isEmptyOrNull(MainApplication.getMchId())) {
                        printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_MID) + MainApplication.getMchId() + "\n");
                    }
                }
                if (!TextUtils.isEmpty(orderModel.getUpiDeviceId())) {
                    printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_TID) + orderModel.getUpiDeviceId() + "\n");
                } else {
                    printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_TID) + "**" + "\n");
                }
                printer.printText("-------------------------------\r\n");
                printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_card) + "**" + "\n");
                printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_exp_date) + "**/**" + "\n");
                printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_card_type) + orderModel.getTradeName() + "\n");

                if (orderModel.isPay()) {
                    if (MainApplication.getPayTypeMap() != null && MainApplication.getPayTypeMap().size() > 0) {
                        printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_trans_type) + MainApplication.getTradeTypeMap().get(orderModel.getTradeState() + "") + "\n");
                    }
                } else {
                    if (MainApplication.getRefundStateMap() != null && MainApplication.getRefundStateMap().size() > 0) {
                        printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_trans_type) + MainApplication.getRefundStateMap().get(orderModel.getRefundState() + "") + "\n");
                    }
                }
                String date = "";
                String time = "";
                try {
                    date = DateUtil.StringToDateDMY(orderModel.getAddTimeNew());
                    time = DateUtil.StringToDateHMS(orderModel.getAddTimeNew());
                } catch (Exception e) {
                }
                printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_date) + date + "\n");
                printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_time) + time + "\n");
                printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_batch) + "**" + "\n");
                printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_trace) + orderModel.getOrderNoMch() + "\n");
                printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_acode) + "**" + "\n");


                //此處打印Voucher No. 若是其他第三方交易，则打印第三方订单号,server端做了判断故直接取值就好
                //如果没有第三方订单号,给个空值,空字符串
                printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_RRN) + orderModel.getVoucherNo() + "\n");
                printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_TVR) + "**" + "\n");
                printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_TSI) + "**" + "\n");
                printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_AID) + "**" + "\n");
                printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_APP1_Label) + "**" + "\n");
                printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_TC) + "\n");
                printer.printText(Alignment.LEFT, " " + "\n");
                printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_trans_total) + "\n");
                printer.printText(Alignment.RIGHT, MainApplication.feeType + " " + DateUtil.formatMoneyUtils(orderModel.getTotalFee()) + "\n");

                //Uplan ---V3.0.5迭代新增

                //如果为预授权的支付类型，则不展示任何优惠相关的信息
                    /*因目前SPAY與網關支持的預授權接口僅為‘支付寶’預授權，
                    遂與銀聯Uplan不衝突，遂預授權頁面內，無需處理該優惠信息。*/
                if (!TextUtils.isEmpty(orderModel.getTradeType()) && !orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") && !orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")) {
                    // 优惠详情 --- 如果列表有数据，则展示，有多少条，展示多少条
                    if (orderModel.getUplanDetailsBeans() != null && orderModel.getUplanDetailsBeans().size() > 0) {
                        for (int i = 0; i < orderModel.getUplanDetailsBeans().size(); i++) {
                            String string_Uplan_details;
                            //如果是Uplan Discount或者Instant Discount，则采用本地词条，带不同语言的翻译
                            if (orderModel.getUplanDetailsBeans().get(i).getDiscountNote().equalsIgnoreCase("Uplan discount")) {
                                string_Uplan_details = ToastHelper.toStr(R.string.wpay_receipt_uplan_discount) + "：";
                            } else if (orderModel.getUplanDetailsBeans().get(i).getDiscountNote().equalsIgnoreCase("Instant Discount")) {
                                string_Uplan_details = ToastHelper.toStr(R.string.wpay_receipt_instant_discount) + "：";
                            } else {//否则，后台传什么展示什么
                                string_Uplan_details = orderModel.getUplanDetailsBeans().get(i).getDiscountNote() + "：";
                            }
                            printer.printText(Alignment.LEFT, string_Uplan_details + "\n");
                            printer.printText(Alignment.RIGHT, MainApplication.feeType + " " + "-" + orderModel.getUplanDetailsBeans().get(i).getDiscountAmt() + "\n");
                        }
                    }

                    // 消费者实付金额 --- 如果不为0 ，就展示
                    if (orderModel.getCostFee() != 0) {
                        printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_uplan_actual_paid_amount) + "\n");
                        printer.printText(Alignment.RIGHT, MainApplication.feeType + " " + DateUtil.formatMoneyUtils(orderModel.getCostFee()) + "\n");
                    }
                }

                printer.printText(" " + "\n");
                printer.printText("-------------------------------\r\n");
                printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_no_sign) + "\n");
                printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_no_sign_english) + "\n");
                printer.printText("-------------------------------\r\n");
                printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_confirm_chinese) + "\n");
                printer.printText(Alignment.LEFT, ToastHelper.toStr(R.string.wpay_receipt_confirm_english) + "\n");
                printer.printText(" " + "\n");
                printer.printText(Alignment.CENTER,"---------" + orderModel.getPartner() + "---------\n");
                printer.printText(" " + "\n");
                printer.printText(" " + "\n");
                printer.printText(" " + "\n");

                printer.printText("\r\n\n");
                printer.printText("\r\n\n");
            }
        });
    }


    /************************新增获取图片方法*******************************/
    public  byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos =new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);

        return baos.toByteArray();
    }

    public InputStream BytesToInStream(byte[] bytes)
    {
        InputStream is = new ByteArrayInputStream(bytes);
        return is;
    }
}
