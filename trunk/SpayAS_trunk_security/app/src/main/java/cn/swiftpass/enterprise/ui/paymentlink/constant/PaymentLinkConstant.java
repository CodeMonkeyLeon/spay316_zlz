package cn.swiftpass.enterprise.ui.paymentlink.constant;

/**
 * Created by congwei.li on 2021/9/17.
 *
 * @Description:
 */
public class PaymentLinkConstant {
    public static final int DEFAULT_TAG = -1;
    public static final int TAB_LIST_TYPE_ORDER = 0;
    public static final int TAB_LIST_TYPE_CUSTOMER = 1;
    public static final int TAB_LIST_TYPE_PRODUCT = 2;
    public static final int MANAGE_CREATE_ORDER = 10;
    public static final int MANAGE_CREATE_CUSTOMER = 11;
    public static final int MANAGE_CREATE_PRODUCT = 12;
    public static final int MANAGE_EDIT_ORDER = 20;
    public static final int MANAGE_EDIT_CUSTOMER = 21;
    public static final int MANAGE_EDIT_PRODUCT = 22;
    public static final int CREATE_ORDER_SUCCESS = 23;
    public static final int RESULT_CODE_CREATE_ORDER_SUCCESS = 23;
    public static final int CREATE_ORDER = 24;
    public static final int RESULT_CODE_CREATE_ORDER = 25;
    public static final int ADD_ORDER_CREATE_CUSTOMER = 31;
    public static final int ADD_ORDER_CREATE_PRODUCT = 32;
    public static final int ADD_ORDER_SHARE_EMAIL = 41;
    public static final int ADD_ORDER_SHARE_SMS = 42;

    public static final int REQUEST_CODE_SELECT_CUSTOMER = 60;
    public static final int RESULT_CODE_SELECT_CUSTOMER = 61;
    public static final int REQUEST_CODE_SELECT_PRODUCT = 62;
    public static final int RESULT_CODE_SELECT_PRODUCT = 63;
    public static final int RESULT_CODE_CREATE_CUSTOMER = 71;
    public static final int RESULT_CODE_CREATE_PRODUCT = 72;
    public static final int RESULT_CODE_SEARCH_CHANGE= 73;

    public static final int SEARCH_TYPE_MANAGE_ORDER = 80;
    public static final int SEARCH_TYPE_MANAGE_PRODUCT = 81;
    public static final int SEARCH_TYPE_MANAGE_CUSTOMER = 82;
    public static final int SEARCH_TYPE_ADD_ORDER_PRODUCT = 83;
    public static final int SEARCH_TYPE_ADD_ORDER_CUSTOMER = 84;

    public static final int REQUEST_CODE_PICK_IMAGE = 100;
    public static final int PERMISSIONS_EXTERNAL_STORAGE = 801;

    public static final int DEFAULT_PAGE_SIZE = 20;

    public static final String DATA_TAG = "dataInfo";
    public static final String TYPE_TAG = "type";
    public static final String RESULT_TAG = "result";
    public static final String EMAIL_NAME = "[name]";
    public static final String SUCCESS_TAG = "1";
    public static final String CREATE_ORDER_SUCCESS_TAG = "CreateOrderSuccess";
}
