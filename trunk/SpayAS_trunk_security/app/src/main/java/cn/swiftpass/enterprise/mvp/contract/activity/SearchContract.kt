package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView
import cn.swiftpass.enterprise.ui.paymentlink.model.*

/**
 * @author lizheng.zhao
 * @date 2022/11/23
 *
 * @有人像家雀儿
 * @不愿意挪窝
 * @有人像候鸟
 * @永远在路上
 */
class SearchContract {

    interface View : BaseView {

        fun deleteProductSuccess(
            response: String?,
            position: Int
        )

        fun deleteProductFailed(error: Any?)

        fun deleteCustomerSuccess(
            response: String?,
            position: Int
        )

        fun deleteCustomerFailed(error: Any?)


        fun getOrderListSuccess(response: PaymentLinkOrderModel?)

        fun getOrderListFailed(error: Any?)

        fun getProductListSuccess(response: PaymentLinkProductModel?)

        fun getProductListFailed(error: Any?)


        fun getCustomerListSuccess(response: PaymentLinkCustomerModel?)

        fun getCustomerListFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {

        fun getCustomerList(
            pageNumber: String?,
            custName: String?,
            custMobile: String?,
            custEmail: String?
        )


        fun getProductList(
            pageNumber: String?,
            goodsId: String?,
            goodsName: String?,
            goodsCode: String?
        )

        fun getOrderList(
            orderNo: String?,
            realPlatOrderNo: String?,
            custName: String?,
            orderRemark: String?,
            pageNumber: String?
        )


        fun deleteCustomer(
            customer: PaymentLinkCustomer?,
            position: Int
        )

        fun deleteProduct(
            product: PaymentLinkProduct?,
            position: Int
        )

    }
}