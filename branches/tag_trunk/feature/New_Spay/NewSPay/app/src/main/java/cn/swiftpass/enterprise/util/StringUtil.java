package cn.swiftpass.enterprise.util;

import android.text.TextUtils;

public class StringUtil {
    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @param str
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static boolean isEmptyOrNull(String str) {
        if (TextUtils.isEmpty(str)){
            return true;
        }
        if (null == str || "".equals(str) || "null".equals(str)) {
            return true;
        }

        return false;
    }

    public static long paseStrToLong(String str) {
        if (str.equals("0.0")) {
            return 0;
        } else {
            return Long.parseLong(str);
        }

    }

    public static String paseStrToMarkStr(String str) {
        String frontStr = "";//前面四位
        String behindStr = "";//后面四位
        String orderNoMch = "";
        if (!StringUtil.isEmptyOrNull(str)) {
            frontStr = str.substring(0, 4);
            behindStr = str.substring(str.length() - 4,str.length());
            orderNoMch = frontStr + "****" + behindStr;
        }
        return  orderNoMch;
    }

}
