package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.AccountCancelConfirmContract
import cn.swiftpass.enterprise.utils.AESHelper
import cn.swiftpass.enterprise.utils.MD5
import cn.swiftpass.enterprise.utils.SharedPreUtils
import com.example.common.entity.EmptyData
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/11/23
 *
 * @生活是一次机会
 * @仅仅一次
 * @谁校对时间
 * @谁就会突然老去
 */
class AccountCancelConfirmPresenter : AccountCancelConfirmContract.Presenter {

    private var mView: AccountCancelConfirmContract.View? = null


    override fun checkVerifyCodeAndPassword(emailVerifyCode: String?, password: String?) {
        mView?.let { view ->

            view.showLoading(R.string.public_loading, ParamsConstants.COMMON_LOADING)
            //password要加密
            val priKey = SharedPreUtils.readProduct(ParamsConstants.SECRET_KEY) as String?
            val md5 = MD5.md5s(priKey).toUpperCase()
            AppClient.checkVerifyCodeAndPassword(
                emailVerifyCode,
                AESHelper.aesEncrypt(password, md5.substring(8, 24)),
                MainApplication.getInstance().getSKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.checkVerifyCodeAndPasswordFailed(it.message)
                        }
                    }

                    override fun onResponse(response: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        view.checkVerifyCodeAndPasswordSuccess(true)
                    }
                }
            )
        }
    }

    override fun attachView(view: AccountCancelConfirmContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}