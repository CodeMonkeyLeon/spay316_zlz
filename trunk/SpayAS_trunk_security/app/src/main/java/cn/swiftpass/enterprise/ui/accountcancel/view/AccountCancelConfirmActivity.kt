package cn.swiftpass.enterprise.ui.accountcancel.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.mvp.contract.activity.AccountCancelConfirmContract
import cn.swiftpass.enterprise.mvp.presenter.activity.AccountCancelConfirmPresenter
import cn.swiftpass.enterprise.ui.activity.BaseActivity
import cn.swiftpass.enterprise.ui.widget.DialogInfo
import cn.swiftpass.enterprise.ui.widget.TitleBar
import cn.swiftpass.enterprise.utils.DialogHelper
import cn.swiftpass.enterprise.utils.EditTextWatcher
import cn.swiftpass.enterprise.utils.NetworkUtils


/**
 * @author lizheng.zhao
 * @date 2022/06/16
 */
class AccountCancelConfirmActivity : BaseActivity<AccountCancelConfirmContract.Presenter>(),
    AccountCancelConfirmContract.View {


    override fun createPresenter() = AccountCancelConfirmPresenter()

    private fun getLayoutId() = R.layout.act_account_cancel_confirm


    companion object {
        const val TAG = "AccountCancel"

        fun startAccountCancelConfirmActivity(fromActivity: Activity) {
            fromActivity.startActivity(
                Intent(
                    fromActivity,
                    AccountCancelConfirmActivity::class.java
                )
            )
        }
    }


    private lateinit var mEditVerifyCode: EditText
    private lateinit var mEditPassword: EditText
    private lateinit var mBtnConfirm: Button


    override fun useToolBar() = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        initView()
    }


    private fun initView() {
        mEditVerifyCode = findViewById(R.id.id_edit_verify_code)
        mEditPassword = findViewById(R.id.id_edit_password)
        mBtnConfirm = findViewById(R.id.btn_confirm)


        setButtonBg(mBtnConfirm, false, R.string.string_cancel_account)

        addEditTextListener()

        mBtnConfirm.setOnClickListener {
            if (isVerifyCodeAndPasswordFormatCorrect()) {
                cancelAccount()
            }
        }
    }


    /**
     * 监听验证码和密码是否输入为空
     */
    private fun addEditTextListener() {

        val textWatcher = EditTextWatcher()
        textWatcher.setOnTextChanaged(object : EditTextWatcher.OnTextChanged {
            override fun onExecute(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun onAfterTextChanged(s: Editable?) {
                val verifyCode = mEditVerifyCode.text.toString().trim()
                val password = mEditPassword.text.toString().trim()

                if (TextUtils.isEmpty(verifyCode) || TextUtils.isEmpty(password)) {
                    setButtonBg(mBtnConfirm, false, R.string.string_cancel_account)
                } else {
                    setButtonBg(mBtnConfirm, true, R.string.string_cancel_account)
                }
            }
        })


        mEditVerifyCode.addTextChangedListener(textWatcher)
        mEditPassword.addTextChangedListener(textWatcher)
    }


    /**
     * 检查验证码/密码格式
     */
    private fun isVerifyCodeAndPasswordFormatCorrect(): Boolean {
        val verifyCode = mEditVerifyCode.text.trim().toString()
        val password = mEditPassword.text.trim().toString()


        if (TextUtils.isEmpty(verifyCode)) {
            toastDialog(activity, R.string.tx_ver_code, null)
            mEditVerifyCode.isFocusable = true
            return false
        }


        if (TextUtils.isEmpty(password)) {
            toastDialog(activity, R.string.pay_login_pwd, null)
            mEditPassword.isFocusable = true
            return false
        }


        if (password.length < 8) {
            toastDialog(activity, R.string.psw_instruction, null)
            mEditPassword.isFocusable = true
            return false
        }


        return true
    }


    /**
     * 注销账户
     */
    private fun cancelAccount() {
        val dialog = DialogInfo(
            activity,
            getString(R.string.string_cancel_account),
            getString(R.string.string_account_cancel_confirm_dialog),
            getString(R.string.bt_confirm),
            getString(R.string.btnCancel),
            DialogInfo.UNIFED_DIALOG,
            object : DialogInfo.HandleBtn {
                override fun handleOkBtn() {
                    val verifyCode = mEditVerifyCode.text.trim().toString()
                    val password = mEditPassword.text.trim().toString()
                    if (NetworkUtils.isNetWorkValid(activity)) {
                        checkVerifyCodeAndPassword(verifyCode, password)
                    } else {
                        showToastInfo(getString(R.string.show_no_network))
                    }
                }

                override fun handleCancelBtn() {

                }
            },
            null
        )
        DialogHelper.resize(activity, dialog)
        dialog.show()
    }


    override fun checkVerifyCodeAndPasswordSuccess(response: Boolean) {
        //本地退出
        clearAPPState()
    }

    override fun checkVerifyCodeAndPasswordFailed(error: Any?) {
        toastDialog(activity, error.toString()) {}
    }

    /**
     * 检查验证码和密码
     */
    fun checkVerifyCodeAndPassword(verifyCode: String, password: String) {
        mPresenter?.let { p ->
            p.checkVerifyCodeAndPassword(verifyCode, password)
        }
    }


    override fun setupTitleBar() {
        super.setupTitleBar()
        titleBar.setTitle(getString(R.string.string_cancel_account))
        titleBar.setLeftButtonVisible(true)
        titleBar.setOnTitleBarClickListener(object : TitleBar.OnTitleBarClickListener {
            override fun onLeftButtonClick() {
                finish()
            }

            override fun onRightButtonClick() {

            }

            override fun onRightLayClick() {

            }

            override fun onRightButLayClick() {

            }
        })
    }


    override fun setButtonBg(b: Button?, enable: Boolean, res: Int) {
        if (enable) {
            b!!.isEnabled = true
            b.setTextColor(resources.getColor(R.color.bg_text_new))
            if (res > 0) {
                b.setText(res)
            }
            b.setBackgroundResource(R.drawable.btn_save)
        } else {
            b!!.isEnabled = false
            if (res > 0) {
                b!!.setText(res)
            }
            b!!.setBackgroundResource(R.drawable.btn_press_shape)
            b!!.setTextColor(resources.getColor(R.color.bt_enable))
            b!!.background.alpha = 102
        }
    }

}