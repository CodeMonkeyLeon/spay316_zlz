package cn.swiftpass.enterprise.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.systemconfig.FeedbackManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.NetworkUtils;

/**
 * 上传日志
 * User: Alan
 * Date: 13-11-27
 * Time: 下午5:47
 */
public class FeedbackActivity extends TemplateActivity implements View.OnClickListener
{
    
    private EditText etFeedback;
    
    private Button btnSend;
    
    private CheckBox cbLog;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        initView();
        
    }
    
    private void initView()
    {
        setContentView(R.layout.activity_feedback);
        btnSend = getViewById(R.id.btn_send_feedback);
        btnSend.setOnClickListener(this);
        etFeedback = getViewById(R.id.et_feedback);
        cbLog = getViewById(R.id.cb_send_log_box);
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_feedback);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
            }
            
            @Override
            public void onRightLayClick()
            {
                // TODO Auto-generated method stub
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                // TODO Auto-generated method stub
                
            }
        });
    }
    
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    long time;
    
    @Override
    public void onClick(View view)
    {
        if (view.getId() == R.id.btn_send_feedback)
        {
            //进行网络判断
            if (!NetworkUtils.isNetworkAvailable(this))
            {
                showToastInfo("网络不可用，请打开网络连接！");
                return;
            }
            
            String content = etFeedback.getText().toString();
            if (content.length() == 0)
            {
                showToastInfo(R.string.mgs_feedback_tip);
                return;
            }
            if (System.currentTimeMillis() - time < 1000 * 30)
            {
                showToastInfo(R.string.mgs_common_error_tip);
                return;
            }
            FeedbackManager.getInstance().sendFeedBack(content, cbLog.isChecked(), new UINotifyListener<Boolean>()
            {
                @Override
                public void onSucceed(Boolean result)
                {
                    super.onSucceed(result);
                    showToastInfo(R.string.mgs_feedback_send_tip);
                    
                }
                
                @Override
                public void onPreExecute()
                {
                    super.onPreExecute();
                    showLoading(true, R.string.loading);
                }
                
                @Override
                public void onPostExecute()
                {
                    super.onPostExecute();
                    etFeedback.setText("");
                    dismissLoading();
                    time = System.currentTimeMillis();
                }
                
                @Override
                public void onError(Object object)
                {
                    super.onError(object);
                    if (object != null)
                    {
                        showToastInfo(object.toString());
                    }
                }
            });
        }
    }
    
    public static void startActivity(Context mContext)
    {
        Intent it = new Intent();
        it.setClass(mContext, FeedbackActivity.class);
        mContext.startActivity(it);
    }
    
}
