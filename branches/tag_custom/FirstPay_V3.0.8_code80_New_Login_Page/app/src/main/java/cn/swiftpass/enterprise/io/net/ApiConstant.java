package cn.swiftpass.enterprise.io.net;

/**
 * 配置
 *
 * @author Alan
 */
public class ApiConstant {
    /////////////////////////////////////////////// 常用设置///////////////////////////////////////////////
    /**
     * 0.spay 1.spay
     */
    public static int pad = 0;

    /**
     * 手机端
     */
    public static int SPAY = 1;

    /**
     * 下载包名
     */
    public static String APK_NAME = "";

    public static String IP = "";

    public static String IP_WITHOUT_CDN = "";


    ////////////////////////////////////////////////net configer ///////////////////////////////////////////////

    public static String BASE_URL_PORT;

    public static String PAYGATEWAY = "";

    // 下载路径
    public static String DOWNLOAD_APP_URL;

    public static String DO_CFT_GET_UUID_URL;


    public static String NOTIFY_URL;

    public static String RETURN_URL;

    /**
     * 临时用户
     */
    public static boolean TEMP_USER = false;

    //////////////////////////////通讯接口组织前部分//////////////////////////////////////////////////////////
    /**
     * 订单接口
     */
    public static final String ORDER_GET_ORDER = "spay/order/querySpayOrder";

    public static final String ORDER_GETLAST_TTIME = "spay/payQueryOrder";

    public static final String PRE_AUTH_ORDER_GETLAST_TTIME = "spay/unifiedQueryAuth";

    public static final String ORDER_CREATE_ORDER = "order/createOrder";

    public static final String ORDER_GET_SUB_FEEDBACK = "feedback/subFeedback";

    public static final String GET_ORDER_BY_INVOICEID = "spay/invoiceIdQuery";

    /**
     * 下载图片
     */
    public static final String DONLOAD_IMAGE = "spay/mch/download?url=";

    //财付通测试
    public static final String TEST_CFT_ORDER = "order/doCreateOrderThree";

    /**
     * 查询完善资料
     */
    public static final String SHOPDATAQUERY = "spay/mch/get";

    public static final String CASHIERADDNEW = "spay/user/saveOrUpdateSpayUserV2";

    public static final String CASHIERCREATE = "spay/user/saveCashier";

    /**
     * 用户接口
     */
    public static final String USER_BAST = "spay/user";


    /**
     * 退单
     */
    public static final String REFUND_REGIS = "spay/spayRefund";

    /**
     * 退单
     */
    public static final String REFUND_REGIS_NEW = "spay/spayRefundV2";

    /**
     * 查询是否可以退款
     */
    public static final String REFUND_CANORNOT = "spay/calcRefund";


    /**
     * 升级
     */
    public static String UPGRADE_CHECKIN = "spay/upgrade/checkinVersion";

    public static boolean isLocaltionQRcode;


    /***添加境外支付*/
    public static boolean ISOVERSEASY;

    public static String body = "";

    public static Integer bankType = 0;

    public static String bankCode = "";

    public static String bankName = "";

    public static String wxCardUrl = "";

    public static String redPack = "";

    public static String Channel;

    public static String pushMoneyUrl = "";

    public static String serverAddrTest123 = "";

    public static String serverAddrTest61 = "";

    public static String serverAddrTest63 = "";

    public static String serverAddrTestUAT = "";

    public static String serverAddrPrd = "";

    public static String pushMoneyUrlTest = "";

    public static String pushMoneyUrlPrd = "";

    public static String serverAddrDev = "";

    public static String serverAddrDev_JH = "";

    public static String imie = "";

}
