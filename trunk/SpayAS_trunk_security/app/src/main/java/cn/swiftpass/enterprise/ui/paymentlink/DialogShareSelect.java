package cn.swiftpass.enterprise.ui.paymentlink;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.fmt.BaseDialogFragment;

/**
 * Created by congwei.li on 2021/9/14.
 *
 * @Description: payment link列表 order details选择分享途径
 */
public class DialogShareSelect extends BaseDialogFragment {

    private static final String TAG = "DialogShareSelect";
    private OnItemClickListener clickListener;

    public void setClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public static DialogShareSelect newInstance() {
        DialogShareSelect fragment = new DialogShareSelect();
        return fragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_select_share_type;
    }

    @Override
    protected void init(View view, Bundle savedInstanceState) {
        TextView shareEmail = view.findViewById(R.id.tv_share_email);
        View lineEmail = view.findViewById(R.id.line_share_email);
        TextView shareMsg = view.findViewById(R.id.tv_share_message);
        View lineMsg = view.findViewById(R.id.line_share_message);
        TextView shareLink = view.findViewById(R.id.tv_share_link);
        TextView shareToApp = view.findViewById(R.id.tv_share_to_app);
        TextView cancel = view.findViewById(R.id.tv_select_cancel);

        if (null != MainApplication.getInstance().getPaymentLinkAuthority().orderFlag &&
                MainApplication.getInstance().getPaymentLinkAuthority().msgFlag.equals("1")) {
            shareMsg.setVisibility(View.VISIBLE);
            lineMsg.setVisibility(View.VISIBLE);
        } else {
            shareMsg.setVisibility(View.GONE);
            lineMsg.setVisibility(View.GONE);
        }
        if (null != MainApplication.getInstance().getPaymentLinkAuthority().orderFlag &&
                MainApplication.getInstance().getPaymentLinkAuthority().emailFlag.equals("1")) {
            shareEmail.setVisibility(View.VISIBLE);
            lineEmail.setVisibility(View.VISIBLE);
        } else {
            shareEmail.setVisibility(View.GONE);
            lineEmail.setVisibility(View.GONE);
        }
        shareEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                if (clickListener != null) {
                    clickListener.onShareEmail();
                }
            }
        });

        shareMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                if (clickListener != null) {
                    clickListener.onShareMsg();
                }
            }
        });

        shareLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                if (clickListener != null) {
                    clickListener.onShareLink();
                }
            }
        });

        shareToApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                if (clickListener != null) {
                    clickListener.onShareToApp();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    public interface OnItemClickListener {
        void onShareEmail();

        void onShareMsg();

        void onShareLink();

        void onShareToApp();
    }

    public void show(FragmentManager manager) {
        show(manager, TAG);
    }
}
