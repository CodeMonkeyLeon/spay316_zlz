package org.xclcharts.renderer.axis;


/**
 * @ClassName CategoryAxisRender
 * @Description 分类轴(Category Axis)类，设置步长
 * @author XiongChuanLiang<br/>(xcl_168@aliyun.com)
 *  
 */
public class CategoryAxis extends XYAxis {
	
	//分类轴分隔值	
	private double  mAxisSteps = 0.0d;
	
	private boolean mAxisBindStd = false;
	
	public CategoryAxis()
	{
		
	}
	
	/**
	 * 设置分类轴步长
	 * @param steps 步长
	 */
	public void setAxisSteps(double steps)
	{
		 mAxisSteps = steps;
	}
	
	/**
	 * 返回分类轴步长
	 * @return 步长
	 */
	public double getAxisSteps()
	{
		return mAxisSteps;
	}
	
	/**
	 * 是否将分类轴与数据轴的正负标准值绑定, 
	 * 如绑定，则轴会显示在标准值所在位置
	 * @param status 设置状态
	 */
	public void setAxisBuildStd(boolean status)
	{
		mAxisBindStd = status;
	}
	
	/**
	 * 分类轴与数据轴的正负标准值绑定状态 
	 * @return 状态
	 */
	public boolean getAxisBuildStdStatus()
	{
		return mAxisBindStd;
	}
	

}
