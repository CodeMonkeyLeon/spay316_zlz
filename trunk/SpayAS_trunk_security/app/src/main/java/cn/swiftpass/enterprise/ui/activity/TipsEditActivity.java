package cn.swiftpass.enterprise.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.common.OnSwitchButtonChangeListener;
import com.example.common.view.SwitchButton;

import java.util.ArrayList;

import cn.swiftpass.enterprise.bussiness.model.TipsBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.TipsEditContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.TipsEditPresenter;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;

/**
 * Created by aijingya on 2019/7/2.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(小费编辑的界面)
 * @date 2019/7/2.17:22.
 */
public class TipsEditActivity extends BaseActivity<TipsEditContract.Presenter> implements View.OnClickListener,
        TipsEditContract.View {

    private TextView tv_tips_label_1, tv_tips_label_2, tv_tips_label_3;
    private EditText et_input_proprotion_1, et_input_proprotion_2, et_input_proprotion_3;

    private SwitchButton mSwitchSel1;
    private SwitchButton mSwitchSel2;
    private SwitchButton mSwitchSel3;

    private Button btn_tips_edit_confrim;
    private TipsBean bean1, bean2, bean3;
    private ArrayList<TipsBean> beanArrayList;

    @Override
    protected TipsEditContract.Presenter createPresenter() {
        return new TipsEditPresenter();
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips_edit_layout);
        bean1 = (TipsBean) getIntent().getSerializableExtra("TipsBean1");
        bean2 = (TipsBean) getIntent().getSerializableExtra("TipsBean2");
        bean3 = (TipsBean) getIntent().getSerializableExtra("TipsBean3");

        initView();
        initData();
        initListener();

    }


    public void initView() {
        tv_tips_label_1 = findViewById(R.id.tv_tips_label_1);
        tv_tips_label_2 = findViewById(R.id.tv_tips_label_2);
        tv_tips_label_3 = findViewById(R.id.tv_tips_label_3);

        et_input_proprotion_1 = findViewById(R.id.et_input_proprotion_1);
        et_input_proprotion_2 = findViewById(R.id.et_input_proprotion_2);
        et_input_proprotion_3 = findViewById(R.id.et_input_proprotion_3);

        mSwitchSel1 = findViewById(R.id.id_switch_sel1);
        mSwitchSel2 = findViewById(R.id.id_switch_sel2);
        mSwitchSel3 = findViewById(R.id.id_switch_sel3);

        btn_tips_edit_confrim = findViewById(R.id.btn_tips_edit_confrim);
    }

    public void initData() {
        beanArrayList = new ArrayList<>();
        if (bean1 != null) {
            et_input_proprotion_1.setText(bean1.getTipsRate());
            if (bean1.isDefaultTip()) {
                mSwitchSel1.openWithoutAnim(true);
                mSwitchSel2.openWithoutAnim(false);
                mSwitchSel3.openWithoutAnim(false);
            } else {
                mSwitchSel1.openWithoutAnim(false);
            }
        }

        if (bean2 != null) {
            et_input_proprotion_2.setText(bean2.getTipsRate());

            if (bean2.isDefaultTip()) {
                mSwitchSel2.openWithoutAnim(true);
                mSwitchSel1.openWithoutAnim(false);
                mSwitchSel3.openWithoutAnim(false);
            } else {
                mSwitchSel2.openWithoutAnim(false);
            }
        }

        if (bean3 != null) {
            et_input_proprotion_3.setText(bean3.getTipsRate());

            if (bean3.isDefaultTip()) {
                mSwitchSel3.openWithoutAnim(true);
                mSwitchSel1.openWithoutAnim(false);
                mSwitchSel2.openWithoutAnim(false);
            } else {
                mSwitchSel3.openWithoutAnim(false);
            }
        }

        //设置confirm的按钮的点击状态
        updateButtonState();

        et_input_proprotion_1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s.toString().trim())) {
                    int tempNum = Integer.parseInt(s.toString());
                    if (tempNum == 0) {
                        s.replace(0, s.length(), "");
                        updateButtonState();
                    } else if (tempNum > 100) {
                        s.replace(s.length() - 1, s.length(), "");
                        updateButtonState();
                    } else {
                        updateButtonState();
                        return;
                    }

                } else {
                    updateButtonState();
                }

            }
        });

        et_input_proprotion_2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s.toString().trim())) {
                    int tempNum = Integer.parseInt(s.toString());
                    if (tempNum == 0) {
                        s.replace(0, s.length(), "");
                        updateButtonState();
                    } else if (tempNum > 100) {
                        s.replace(s.length() - 1, s.length(), "");
                        updateButtonState();
                    } else {
                        updateButtonState();
                        return;
                    }

                } else {
                    updateButtonState();
                }
            }
        });
        et_input_proprotion_3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s.toString().trim())) {
                    int tempNum = Integer.parseInt(s.toString());
                    if (tempNum == 0) {
                        s.replace(0, s.length(), "");
                        updateButtonState();
                    } else if (tempNum > 100) {
                        s.replace(s.length() - 1, s.length(), "");
                        updateButtonState();
                    } else {
                        updateButtonState();
                        return;
                    }

                } else {
                    updateButtonState();
                }
            }
        });
    }


    public void updateButtonState() {
        boolean isEditTextNotNull = !TextUtils.isEmpty(et_input_proprotion_1.getText().toString())
                && !TextUtils.isEmpty(et_input_proprotion_2.getText().toString())
                && !TextUtils.isEmpty(et_input_proprotion_3.getText().toString());

        boolean isSelectionNotNull = mSwitchSel1.isOpen() || mSwitchSel2.isOpen() || mSwitchSel3.isOpen();
        //同时非空，button才能点击
        if (isEditTextNotNull && isSelectionNotNull) {
            setButtonBg(btn_tips_edit_confrim, true, 0);
        } else {
            setButtonBg(btn_tips_edit_confrim, false, 0);
        }
    }


    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b, enable, res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_press_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }

    public void initListener() {
        btn_tips_edit_confrim.setOnClickListener(this);

        et_input_proprotion_1.setLongClickable(false);
        et_input_proprotion_2.setLongClickable(false);
        et_input_proprotion_3.setLongClickable(false);


        mSwitchSel1.setOnStatusChangeListener(new OnSwitchButtonChangeListener() {
            @Override
            public void onChange(boolean isOpen) {
                if (isOpen) {
                    mSwitchSel2.open(false);
                    mSwitchSel3.open(false);
                }
                //设置confirm的按钮的点击状态
                updateButtonState();
            }
        });


        mSwitchSel2.setOnStatusChangeListener(new OnSwitchButtonChangeListener() {
            @Override
            public void onChange(boolean isOpen) {
                if (isOpen) {
                    mSwitchSel1.open(false);
                    mSwitchSel3.open(false);
                }
                //设置confirm的按钮的点击状态
                updateButtonState();
            }
        });


        mSwitchSel3.setOnStatusChangeListener(new OnSwitchButtonChangeListener() {
            @Override
            public void onChange(boolean isOpen) {
                if (isOpen) {
                    mSwitchSel1.open(false);
                    mSwitchSel2.open(false);
                }
                //设置confirm的按钮的点击状态
                updateButtonState();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_tips_edit_confrim:
                clickConfirmBtn();
                break;
        }
    }

    public void clickConfirmBtn() {
        if (!TextUtils.isEmpty(et_input_proprotion_1.getText().toString().trim()) &&
                !TextUtils.isEmpty(et_input_proprotion_2.getText().toString().trim()) &&
                !TextUtils.isEmpty(et_input_proprotion_3.getText().toString().trim())) {

            if (et_input_proprotion_1.getText().toString().trim().equalsIgnoreCase(et_input_proprotion_2.getText().toString().trim())
                    || et_input_proprotion_1.getText().toString().trim().equalsIgnoreCase(et_input_proprotion_3.getText().toString().trim())
                    || et_input_proprotion_2.getText().toString().trim().equalsIgnoreCase(et_input_proprotion_3.getText().toString().trim())) {

                //如果有输入相同小费比例，则提示请输入不相同的小费比例
                toastDialog(TipsEditActivity.this, R.string.tips_can_not_same, null);
                return;
            }

        }

        //理论上不会进下面的判断
        if (!mSwitchSel1.isOpen() && !mSwitchSel2.isOpen() && !mSwitchSel3.isOpen()) {
            //如果没有选择默认选项，则提示请选择其中一个选项设为默认选项
            toastDialog(TipsEditActivity.this, R.string.tips_select_option, null);
            return;
        }

        bean1.setTipsRate(et_input_proprotion_1.getText().toString().trim());
        bean1.setDefaultTip(mSwitchSel1.isOpen());

        bean2.setTipsRate(et_input_proprotion_2.getText().toString().trim());
        bean2.setDefaultTip(mSwitchSel2.isOpen());

        bean3.setTipsRate(et_input_proprotion_3.getText().toString().trim());
        bean3.setDefaultTip(mSwitchSel3.isOpen());

        //请求网络，提交小费比例数据
        submitTipsSetting();
    }


    @Override
    public void submitTipsSettingSuccess(boolean response) {
        if (response) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    toastDialog(TipsEditActivity.this, getStringById(R.string.tips_edit_success), new NewDialogInfo.HandleBtn() {
                        @Override
                        public void handleOkBtn() {
                            //数据提交成功，则finish当前页面，跳转到设置页面，同时更新数据
                            finish();

                            Intent it = new Intent();
                            it.setClass(TipsEditActivity.this, TipsSettingActivity.class);
                            startActivity(it);
                        }
                    });
                }
            });
        }
    }

    @Override
    public void submitTipsSettingFailed(@Nullable Object error) {
        if (error != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    toastDialog(TipsEditActivity.this, error.toString(), null);
                }
            });
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    toastDialog(TipsEditActivity.this, getStringById(R.string.tips_edit_failed), null);
                }
            });
        }
    }

    public void submitTipsSetting() {
        beanArrayList.clear();
        beanArrayList.add(bean1);
        beanArrayList.add(bean2);
        beanArrayList.add(bean3);

        if (mPresenter != null) {
            mPresenter.submitTipsSetting(beanArrayList);
        }
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(getStringById(R.string.tips_setting));
        titleBar.setLeftButtonVisible(true);

        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {
            }

            @Override
            public void onRightButLayClick() {
            }
        });
    }


}
