package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.MerchantTempDataModel
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.AccountCancelWarningContract
import cn.swiftpass.enterprise.utils.JsonUtil
import com.example.common.entity.EmptyData
import okhttp3.Call


/**
 * @author lizheng.zhao
 * @date 2022/11/23
 *
 * @天空吸收着水分
 * @越来越蓝
 * @蓝得醉人
 * @那是画家调不出来的颜色。
 */
class AccountCancelWarningPresenter : AccountCancelWarningContract.Presenter {

    private var mView: AccountCancelWarningContract.View? = null


    override fun sendEmailForAccountCancel(email: String?) {
        mView?.let { view ->
            view.showLoading(R.string.public_loading, ParamsConstants.COMMON_LOADING)
            AppClient.sendEmailForAccountCancel(
                email,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.sendEmailForAccountCancelFailed(it.message)
                        }

                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        view.sendEmailForAccountCancelSuccess(true)
                    }
                }
            )
        }
    }

    override fun queryMerchantDataByTel() {
        mView?.let { view ->
            view.showLoading(R.string.public_loading, ParamsConstants.COMMON_LOADING)
            AppClient.queryMerchantDataByTel(
                MainApplication.getInstance().getMchId(),
                MainApplication.getInstance().userInfo.phone,
                System.currentTimeMillis().toString(),
                object :
                    CallBackUtil.CallBackCommonResponse<MerchantTempDataModel>(MerchantTempDataModel()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.queryMerchantDataByTelFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果centerRate值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"centerRate\":\"\"",
                                "\"centerRate\":[]"
                            )
                            val model = JsonUtil.jsonToBean(
                                response.message,
                                MerchantTempDataModel::class.java
                            ) as MerchantTempDataModel

                            model.merchantTypeName = model.merchantType.toString()
                            model.bankUserName = model.accountName
                            model.identityNo = model.accountIdEntity
                            model.bank = model.bankNumberCode
                            model.branchBankName = model.bankName
                            model.idCardJustPic = model.linencePhoto
                            model.licensePic = model.indentityPhoto
                            model.idCode = model.protocolPhoto
                            view.queryMerchantDataByTelSuccess(model)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: AccountCancelWarningContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}