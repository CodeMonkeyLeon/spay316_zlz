/*
 * 文 件 名:  ReportActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-8
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.bill;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.refund.RefundManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 退款登录确认验证
 *
 * @author he_hui
 * @version [版本号, 2016-11-8]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class OrderRefundLoginConfirmActivity extends TemplateActivity {

    private Order orderModel;

    private EditText et_id;

    private Button btn_next_step;

    public static void startActivity(Context context, Order orderModel) {
        Intent it = new Intent();
        it.setClass(context, OrderRefundLoginConfirmActivity.class);
        it.putExtra("order", orderModel);
        context.startActivity(it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_refund_login);

        initview();

        setLister();

        btn_next_step.getBackground().setAlpha(102);
//        showSoftInputFromWindow(OrderRefundLoginConfirmActivity.this, et_id);
        orderModel = (Order) getIntent().getSerializableExtra("order");
        MainApplication.listActivities.add(this);
//        initMoveKeyBoard();
//
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                keyboardUtil.showKeyBoardLayout(et_id, KeyboardUtil.INPUTTYPE_ABC,-1);
//            }
//        }, 200);
    }


//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
//            if (keyboardUtil.isShow) {
//                keyboardUtil.hideSystemKeyBoard();
//                keyboardUtil.hideAllKeyBoard();
//                keyboardUtil.hideKeyboardLayout();
//            } else {
//                return super.onKeyDown(keyCode, event);
//            }
//
//            return false;
//        } else
//            return super.onKeyDown(keyCode, event);
//    }


    /**
     * {@inheritDoc}
     */

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            if (keyboardUtil.isShow) {
//                keyboardUtil.hideSystemKeyBoard();
//                keyboardUtil.hideAllKeyBoard();
//                keyboardUtil.hideKeyboardLayout();
//            }
            //showPage(CashierManager.class);
            finish();
        }

        return super.onKeyDown(keyCode, event);

    }

    private void setLister() {
        btn_next_step.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String pass = et_id.getText().toString();
                if (StringUtil.isEmptyOrNull(pass)) {
                    toastDialog(OrderRefundLoginConfirmActivity.this, R.string.pay_refund_pwd, null);
                    et_id.setFocusable(true);
                    return;
                }

                checkUser();
            }
        });
    }

    /**
     * 验证用户
     */
    private void checkUser() {
        LocalAccountManager.getInstance().refundLogin(null, MainApplication.getMchId(), MainApplication.userName, et_id.getText().toString(), AppHelper.getIMEI(), AppHelper.getIMSI(), false, new UINotifyListener<Boolean>() {
            @Override
            public void onError(final Object object) {
                super.onError(object);
                dismissMyLoading();
                if (object != null) {
                    toastDialog(OrderRefundLoginConfirmActivity.this, object.toString(), null);
                }

            }

            @Override
            public void onSucceed(Boolean result) {
                super.onSucceed(result);
                dismissMyLoading();
                if (result) {
                    if (orderModel != null) {
                        regisRefund();
                    }
                }
            }

            @Override
            public void onPreExecute() {
                super.onPreExecute();
                loadDialog(OrderRefundLoginConfirmActivity.this, R.string.tx_bill_stream_refund_login_loading);
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
                dismissMyLoading();
            }
        });
    }

    private void regisRefund() {
        String transaction = null;
        RefundManager.getInstant().regisRefunds(orderModel.getOutTradeNo(), transaction, orderModel.getMoney(), orderModel.getRefundFeel(), new UINotifyListener<Order>() {
            @Override
            public void onError(final Object object) {
                dismissLoading();
                super.onError(object);
                if (object != null) {
                    toastDialog(OrderRefundLoginConfirmActivity.this, object.toString(), getString(R.string.submit), new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            for (Activity a : MainApplication.listActivities) {
                                a.finish();
                            }
                            OrderRefundLoginConfirmActivity.this.finish();
                        }
                    });
                }
            }

            @Override
            public void onPreExecute() {
                super.onPreExecute();

                loadDialog(OrderRefundLoginConfirmActivity.this, R.string.refunding_wait);
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
                dismissLoading();
            }

            @Override
            public void onSucceed(Order result) {
                dismissLoading();
                super.onSucceed(result);
                if (result != null) {
                    orderModel.setRefundNo(result.getRefundNo());
                    orderModel.setAddTimeNew(result.getAddTimeNew());
                    orderModel.setMoney(orderModel.getRefundFeel());

                    orderModel.setCashFeel(result.getCashFeel());
                    orderModel.setSurcharge(result.getSurcharge());
                    orderModel.setRefundFee(result.getRefundFee());
                    orderModel.setVat(result.getVat());
                    orderModel.setOrderFee(result.getOrderFee());
                    OrderRefundCompleteActivity.startActivity(OrderRefundLoginConfirmActivity.this, orderModel);

                }
            }
        });
    }

    private void initview() {
        et_id = getViewById(R.id.et_id);
        btn_next_step = getViewById(R.id.btn_next_step);
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (et_id.isFocused()) {
                    if (et_id.getText().toString().length() > 0) {
                        //                        btn_next_step.setEnabled(true);
                        //                        btn_next_step.setBackgroundResource(R.drawable.btn_register);

                        setButtonBg(btn_next_step, true, R.string.submit);
                    } else {
                        //                        btn_next_step.setEnabled(false);
                        //                        btn_next_step.setBackgroundResource(R.drawable.general_button_grey_default);

                        setButtonBg(btn_next_step, false, R.string.submit);
                    }
                }
            }
        });
        et_id.addTextChangedListener(editTextWatcher);
    }

    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b,enable,res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_press_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }
    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(R.string.tx_bill_stream_login_cirfom);
        titleBar.setTitleChoice(false);
        titleBar.setLeftButtonVisible(true);
        titleBar.setLeftButtonIsVisible(true);
    }

}
