package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Alan
 * Date: 13-11-7
 * Time: 上午11:33
 * To change this template use File | Settings | File Templates.
 */
public class QRcodeInfo implements Serializable
{
    
    private long discountAmount;
    
    private boolean isWitch = true; // 是否切换
    
    private String apiCode;
    
    /**
     * @return 返回 apiCode
     */
    public String getApiCode()
    {
        return apiCode;
    }
    
    /**
     * @param 对apiCode进行赋值
     */
    public void setApiCode(String apiCode)
    {
        this.apiCode = apiCode;
    }
    
    /**
     * @return 返回 isWitch
     */
    public boolean isWitch()
    {
        return isWitch;
    }
    
    /**
     * @param 对isWitch进行赋值
     */
    public void setWitch(boolean isWitch)
    {
        this.isWitch = isWitch;
    }
    
    /**
     * @return 返回 discountAmount
     */
    public long getDiscountAmount()
    {
        return discountAmount;
    }
    
    /**
     * @param 对discountAmount进行赋值
     */
    public void setDiscountAmount(long discountAmount)
    {
        this.discountAmount = discountAmount;
    }
    
    /**
     * @return 返回 payType
     */
    public String getPayType()
    {
        return payType;
    }
    
    /**
     * @param 对payType进行赋值
     */
    public void setPayType(String payType)
    {
        this.payType = payType;
    }
    
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    public String uuId;
    
    public String appId;
    
    public String reqKey;
    
    public String orderNo;
    
    public String transactionNo;
    
    private String city;
    
    public String service_uuid; // 服务器环境 
    
    public boolean isQpay;
    
    public String payType;
    
    private Integer isMark = 0;
    
    private boolean iswxCard; //是否带微信卡券

    public String getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(String surcharge) {
        this.surcharge = surcharge;
    }

    private String surcharge;
    
    //    public Bitmap bitmap;
    //    
    //    public InputStream inputStream;
    
    /**
     * @return 返回 iswxCard
     */
    public boolean isIswxCard()
    {
        return iswxCard;
    }
    
    /**
     * @param 对iswxCard进行赋值
     */
    public void setIswxCard(boolean iswxCard)
    {
        this.iswxCard = iswxCard;
    }
    
    /**
     * @return 返回 isMark
     */
    public Integer getIsMark()
    {
        return isMark;
    }
    
    /**
     * @param 对isMark进行赋值
     */
    public void setIsMark(Integer isMark)
    {
        this.isMark = isMark;
    }
    
    /**
     * @return 返回 city
     */
    public String getCity()
    {
        return city;
    }
    
    /**
    //     * @return 返回 inputStream
    //     */
    //    public InputStream getInputStream()
    //    {
    //        return inputStream;
    //    }
    //    
    //    /**
    //     * @param 对inputStream进行赋值
    //     */
    //    public void setInputStream(InputStream inputStream)
    //    {
    //        this.inputStream = inputStream;
    //    }
    
    /**
     * @param 对city进行赋值
     */
    public void setCity(String city)
    {
        this.city = city;
    }
    
    //交易金额
    public String totalMoney;
    
    //有优惠券 消费后直接打开优惠券
    //public String couponUrl;
    //次此消费是否有优惠券
    public boolean isCoupon;
    
    public Order order;
}
