package cn.swiftpass.enterprise.ui.paymentlink.model;

import android.content.Context;
import android.text.TextUtils;

import com.example.common.sentry.SentryUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.KotlinUtils;

public class PaymentLinkOrder implements Serializable {
    //0:待付款；1:已过期；2:付款中;3:已完成;4:转入退款；5:已取消
    public String orderNo;
    public String platformOrderNo;
    public String realPlatOrderNo;
    public String orderStatus;
    public String totalFee;
    public String feeType;
    public String orderLanguage;
    public String custId;
    public String custName;
    public String custMobile;
    public String custEmail;
    public String merchantId;
    public String createTime;
    public String updateTime;
    public String sumGoodsFee;

    public ArrayList<PaymentLinkProduct> orderRelateGoodsList;

    public String payUrl;
    public String payUrlQr64;
    public String effectiveDate;
    public String tradeTime;
    public String currentDate;
    public String orderRemark;
    public String orderVersion;

    public String merchantName;
    public String parentMerchant;
    public String acceptOrgId;
    public String channelId;
    public String orgId;
    public String orgType;
    public String mchType;
    public String merchantIdList;
    public String merchantLogo;
    public String picUrl;
    public String mainColors;
    public String token;
    public String sumExtraFee;

    public String goodsPic;
    public ArrayList<OtherFee> orderCostList;

    public PaymentLinkCustomer customer;

    public String getTotalProductNum() {
        if (null == orderRelateGoodsList) {
            return "";
        }
        int total = 0;
        for (int i = 0; i < orderRelateGoodsList.size(); i++) {
            total += Integer.parseInt(orderRelateGoodsList.get(i).goodsNum);
        }
        return String.valueOf(total);
    }

    public String getOrderStatusText(Context context) {
        String status = "";
        switch (orderStatus) {
            case "0":
                status = context.getResources().getString(R.string.pl_order_status_unpaid);
                break;
            case "1":
                status = context.getResources().getString(R.string.pl_order_status_expired);
                break;
            case "2":
                status = context.getResources().getString(R.string.pl_order_status_paying);
                break;
            case "3":
                status = context.getResources().getString(R.string.pl_order_status_successful);
                break;
            case "4":
                status = context.getResources().getString(R.string.pl_order_status_refunded);
                break;
            case "5":
                status = context.getResources().getString(R.string.pl_order_status_cancelled);
                break;
        }
        return status;
    }

    public int getOrderStatusTextColor(Context context) {
        int status;
        switch (orderStatus) {
            case "0":
            case "1":
            case "2":
            case "4":
            case "5":
                status = context.getResources().getColor(R.color.color_A3A3A3);
                break;
            case "3":
            default:
                status = context.getResources().getColor(R.color.oreder_success);
                break;
        }
        return status;
    }

    public boolean isEditAble() {
        return "0".equals(orderStatus) || "1".equals(orderStatus);
    }

    public boolean isUpdateAble() {
        return !"3".equals(orderStatus);
    }

    public PaymentLinkOrder deepClone() {
        PaymentLinkOrder outer = null;
        try { // 将该对象序列化成流,因为写在流里的是对象的一个拷贝，而原对象仍然存在于JVM里面。所以利用这个特性可以实现对象的深拷贝
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(this);
            // 将流序列化成对象
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            outer = (PaymentLinkOrder) ois.readObject();
        } catch (IOException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        } catch (ClassNotFoundException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
        return outer;
    }

    @Override
    public String toString() {
        return "PaymentLinkOrder{" +
                "orderNo='" + orderNo + '\'' +
                ", platformOrderNo='" + platformOrderNo + '\'' +
                ", realPlatOrderNo='" + realPlatOrderNo + '\'' +
                ", orderStatus='" + orderStatus + '\'' +
                ", totalFee='" + totalFee + '\'' +
                ", feeType='" + feeType + '\'' +
                ", orderLanguage='" + orderLanguage + '\'' +
                ", custId='" + custId + '\'' +
                ", custName='" + custName + '\'' +
                ", custMobile='" + custMobile + '\'' +
                ", custEmail='" + custEmail + '\'' +
                ", merchantId='" + merchantId + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", sumGoodsFee='" + sumGoodsFee + '\'' +
                ", orderRelateGoodsList=" + orderRelateGoodsList +
                ", payUrl='" + payUrl + '\'' +
                ", payUrlQr64='" + payUrlQr64 + '\'' +
                ", effectiveDate='" + effectiveDate + '\'' +
                ", tradeTime='" + tradeTime + '\'' +
                ", currentDate='" + currentDate + '\'' +
                ", orderRemark='" + orderRemark + '\'' +
                ", orderVersion='" + orderVersion + '\'' +
                ", merchantName='" + merchantName + '\'' +
                ", parentMerchant='" + parentMerchant + '\'' +
                ", acceptOrgId='" + acceptOrgId + '\'' +
                ", channelId='" + channelId + '\'' +
                ", orgId='" + orgId + '\'' +
                ", orgType='" + orgType + '\'' +
                ", mchType='" + mchType + '\'' +
                ", merchantIdList='" + merchantIdList + '\'' +
                ", merchantLogo='" + merchantLogo + '\'' +
                ", picUrl='" + picUrl + '\'' +
                ", mainColors='" + mainColors + '\'' +
                ", token='" + token + '\'' +
                ", sumExtraFee='" + sumExtraFee + '\'' +
                ", orderCostList=" + orderCostList +
                ", customer=" + customer +
                '}';
    }

    public boolean isHaveData() {
        return (null != orderRelateGoodsList && !orderRelateGoodsList.isEmpty()) ?
                // 商品模式
                !TextUtils.isEmpty(custId) || (null != orderCostList && !orderCostList.isEmpty())
                        || !TextUtils.isEmpty(picUrl) || !TextUtils.isEmpty(orderRemark)
                // 金额模式
                : !TextUtils.isEmpty(custId) || (null != orderCostList && !orderCostList.isEmpty())
                || !TextUtils.isEmpty(picUrl) || !TextUtils.isEmpty(orderRemark);
    }
}
