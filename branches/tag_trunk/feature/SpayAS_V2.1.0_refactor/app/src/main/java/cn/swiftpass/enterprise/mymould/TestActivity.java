package cn.swiftpass.enterprise.mymould;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.cxy.networklibrary.other.LogUtil;
import com.cxy.networklibrary.util.GsonUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mymould.base.BaseActivity;
import cn.swiftpass.enterprise.mymould.bean.BaseBean;
import cn.swiftpass.enterprise.mymould.bean.QueryMsgBean;
import cn.swiftpass.enterprise.mymould.contract.QueryContract;
import cn.swiftpass.enterprise.mymould.presenter.QueryPresenter;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

public class TestActivity extends BaseActivity<QueryPresenter> implements QueryContract.View {

    //@BindView(R.id.btn)
    Button btn;
    TextView tv;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test;
    }

    @Override
    public void initView() {
        mPresenter = new QueryPresenter(this);//创建P
       // mPresenter.attachView(this);//关联V
        btn = (Button) findViewById(R.id.btn);
        tv = (TextView) findViewById(R.id.tv);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                query(0, 10, "OliviaJiang06");
            }
        });

     //   String json = "{\"result\":\"200\",\"message\":\"{\\\"data\\\":[],\\\"currentPage\\\":0,\\\"perPage\\\":10,\\\"pageCount\\\":0,\\\"totalRows\\\":0,\\\"reqFeqTime\\\":\\\"\\\"}\",\"code\":\"\"}\n";
//        BaseBean<QueryMsgBean> bean = GsonUtil.getInstance().fromJson(json, new TypeToken<BaseBean<QueryMsgBean>>() {
//        }.getType());



        Type listType = new TypeToken<BaseBean<QueryMsgBean>>(){}.getType();
//        Gson gson=new Gson();
//        BaseBean<QueryMsgBean> bean=gson.fromJson(json, listType);
//        String s = bean.toString();

    }


    @Override
    public void onError(Throwable throwable) {
        //  ToastUtil.show("异常 "+throwable.getMessage());
        ToastHelper.showInfo(throwable.getMessage());
        LogUtil.e("cxy异常------------" + throwable.getMessage());
    }

    @Override
    public void onSuccess(BaseBean<QueryMsgBean> bean) {
        // ToastUtil.show("成功 "+bean.getCode());
        Gson gson = new Gson();
        String json = gson.toJson(bean);//把对象转为JSON格式的字符串
        LogUtil.e("cxy成功------------" + json);
        tv.setText(json);
        QueryMsgBean queryMsgBean=bean.getMessage();
        LogUtil.e("cxy成功------------" + queryMsgBean.getPerPage());
    }

    /**
     * cxy  接口测试
     */
    private void query(int page, int pageSize, String input) {
        try {
            JSONObject json = new JSONObject();
            Map<String, String> params = new HashMap<>();

            if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                json.put("mchId", MainApplication.merchantId);
                params.put("mchId", MainApplication.merchantId);
            } else {
                json.put("mchId", MainApplication.getMchId());
                params.put("mchId", MainApplication.getMchId());

            }
            json.put("page", page + "");
            params.put("page", page + "");
            if (null != input) {
                json.put("searchContent", input);
                params.put("searchContent", input);
            }
            if (pageSize > 0) {

                params.put("pageSize", pageSize + "");
                json.put("pageSize", pageSize);
            }
            if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey())) {
                json.put("sign", SignUtil.getInstance().createSign(params, MainApplication.getSignKey()));
            }

            long spayRs = System.currentTimeMillis();
            //                    //加签名
            if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                json.put("spayRs", spayRs);
                json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
            }
            mPresenter.queryData(json.toString(), spayRs + "");
        } catch (JSONException e) {

        }


    }
}
