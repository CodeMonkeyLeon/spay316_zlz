package com.example.printsdk.A920.printUtils

import android.graphics.Bitmap
import com.pax.dal.IPrinter
import com.pax.dal.entity.EFontTypeAscii
import com.pax.dal.entity.EFontTypeExtCode
import com.pax.dal.exceptions.PrinterDevException

class PrinterTester : TestLog() {
    private var printer: IPrinter? = null


    init {
        GetObj.getDal()?.let {
            printer = it.printer
        }
    }


    companion object {
        private var printerTester: PrinterTester? = null

        fun getInstance(): PrinterTester? {
            if (printerTester == null) {
                printerTester = PrinterTester()
            }
            return printerTester
        }
    }

    fun init() {
        try {
            printer?.let {
                it.init()
            }
            logTrue("init")
        } catch (e: PrinterDevException) {
            e.printStackTrace()
            logErr("init", e.toString())
        }
    }

    fun getStatus(): String? {
        return try {
            printer?.let {
                val status = it.status
                logTrue("getStatus")
                statusCode2Str(status)
            }
        } catch (e: PrinterDevException) {
            e.printStackTrace()
            logErr("getStatus", e.toString())
            ""
        }
    }

    fun fontSet(asciiFontType: EFontTypeAscii?, cFontType: EFontTypeExtCode?) {
        try {
            printer?.let {
                it.fontSet(asciiFontType, cFontType)
            }
            logTrue("fontSet")
        } catch (e: PrinterDevException) {
            e.printStackTrace()
            logErr("fontSet", e.toString())
        }
    }

    fun spaceSet(wordSpace: Byte, lineSpace: Byte) {
        try {
            printer?.let {
                it.spaceSet(wordSpace, lineSpace)
            }
            logTrue("spaceSet")
        } catch (e: PrinterDevException) {
            e.printStackTrace()
            logErr("spaceSet", e.toString())
        }
    }

    fun printStr(addUnit: String?, charset: String?) {
        try {
            printer?.let {
                it.printStr(addUnit, charset)
            }
            logTrue("printStr")
        } catch (e: PrinterDevException) {
            e.printStackTrace()
            logErr("printStr", e.toString())
        }
    }

    fun step(b: Int) {
        try {
            printer?.let {
                it.step(b)
            }
            logTrue("setStep")
        } catch (e: PrinterDevException) {
            e.printStackTrace()
            logErr("setStep", e.toString())
        }
    }

    fun printBitmap(bitmap: Bitmap?) {
        try {
            printer?.let {
                it.printBitmap(bitmap)
            }
            logTrue("printBitmap")
        } catch (e: PrinterDevException) {
            e.printStackTrace()
            logErr("printBitmap", e.toString())
        }
    }

    fun start(): String? {
        return try {
            printer?.let {
                val res = it.start()
                logTrue("start")
                statusCode2Str(res)
            }
        } catch (e: PrinterDevException) {
            e.printStackTrace()
            logErr("start", e.toString())
            ""
        }
    }

    fun leftIndents(indent: Short) {
        try {
            printer?.let {
                it.leftIndent(indent.toInt())
            }
            logTrue("leftIndent")
        } catch (e: PrinterDevException) {
            e.printStackTrace()
            logErr("leftIndent", e.toString())
        }
    }

    fun getDotLine(): Int {
        return try {
            printer?.let {
                val dotLine = it.dotLine
                logTrue("getDotLine")
                return dotLine
            }
            return -2
        } catch (e: PrinterDevException) {
            e.printStackTrace()
            logErr("getDotLine", e.toString())
            -2
        }
    }

    fun setGray(level: Int) {
        try {
            printer?.let {
                it.setGray(level)
            }
            logTrue("setGray")
        } catch (e: PrinterDevException) {
            e.printStackTrace()
            logErr("setGray", e.toString())
        }
    }

    fun setDoubleWidth(isAscDouble: Boolean, isLocalDouble: Boolean) {
        try {
            printer?.let {
                it.doubleWidth(isAscDouble, isLocalDouble)
            }
            logTrue("doubleWidth")
        } catch (e: PrinterDevException) {
            e.printStackTrace()
            logErr("doubleWidth", e.toString())
        }
    }

    fun setDoubleHeight(isAscDouble: Boolean, isLocalDouble: Boolean) {
        try {
            printer?.let {
                it.doubleHeight(isAscDouble, isLocalDouble)
            }
            logTrue("doubleHeight")
        } catch (e: PrinterDevException) {
            e.printStackTrace()
            logErr("doubleHeight", e.toString())
        }
    }

    fun setInvert(isInvert: Boolean) {
        try {
            printer?.let {
                it.invert(isInvert)
            }
            logTrue("setInvert")
        } catch (e: PrinterDevException) {
            e.printStackTrace()
            logErr("setInvert", e.toString())
        }
    }

    fun statusCode2Str(status: Int): String? {
        var res = ""
        when (status) {
            0 -> res = "Success "
            1 -> res = "Printer is busy "
            2 -> res = "Out of paper "
            3 -> res = "The format of print data packet error "
            4 -> res = "Printer malfunctions "
            8 -> res = "Printer over heats "
            9 -> res = "Printer voltage is too low"
            240 -> res = "Printing is unfinished "
            252 -> res = " The printer has not installed font library "
            254 -> res = "Data package is too long "
            else -> {}
        }
        return res
    }


}