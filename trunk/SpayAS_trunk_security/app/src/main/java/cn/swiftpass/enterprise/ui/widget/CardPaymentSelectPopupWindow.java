package cn.swiftpass.enterprise.ui.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * Created by aijingya on 2021/3/18.
 *
 * @Package cn.swiftpass.enterprise.ui.widget
 * @Description: 卡交易列表选择的弹框
 * @date 2021/3/18.17:03.
 */
public class CardPaymentSelectPopupWindow extends PopupWindow {
    //选择的----接口提供方列表(58,1,2等等)
    List<Integer> mApiProviderList = new ArrayList<>();
    //当前界面已选择的状态---交易状态：(筛选交易类型（集合）
    //1、消费 2、预授权 3、预授权完成 4、退款 5、撤销 6、冲正)
    private List<Integer> mTradeTypeList = new ArrayList<Integer>();
    //交易状态：2：成功，3：失败
    private List<Integer> mTradeStateList = new ArrayList<Integer>();
    private List<Button> listTypeButton = new ArrayList<Button>();
    private List<Button> listStatusButton = new ArrayList<Button>();
    private HandleBtn handleBtn;
    private Button tv_type_sale, tv_type_pre_auth, tv_type_pa_complete, tv_type_refund, tv_type_cancellation, tv_type_reversal;
    private Button tv_status_success, tv_status_failed;
    private TextView tv_reset, tv_confirm;
    private GridViewForScrollView gv_card_payment_type;
    private CardPaymentTypeAdapter CardPaymentAdaper;
    private CardPaymentTypeHolder CardPaymentHolder;
    private List<DynModel> list;
    private Activity activity;
    private View mMenuView;
    private Context mContext;
    private boolean isReset = false; //是否是重置的请求

    public interface HandleBtn {
        void handleOkBtn(List<Integer> apiProviderList, List<Integer> tradeTypeList, List<Integer> tradeStatusList);
    }

    public CardPaymentSelectPopupWindow(Activity context, final List<Integer> apiProviderList, final List<Integer> tradeTypeList, List<Integer> tradeStateList, HandleBtn handleBtn) {
        super(context);
        mContext = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.card_payment_pay_type_choice_dialog, null);
        this.handleBtn = handleBtn;
        this.activity = context;
        this.mApiProviderList = apiProviderList;
        initview(mMenuView);
        setDate(tradeTypeList, tradeStateList);
        setListener();
        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(ViewGroup.LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int height = mMenuView.findViewById(R.id.pop_layout_card_payment).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        //相当于dismiss弹框，直接所有数据清空
                        for (Button buttonType : listTypeButton) {
                            resetTradeTypeButton(buttonType);
                        }
                        for (Button buttonStatus : listStatusButton) {
                            resetTradeStatusButton(buttonStatus);
                        }

                        mTradeStateList.clear();
                        mTradeTypeList.clear();

                        if (list != null && list.size() > 0) {
                            for (DynModel d : list) {
                                d.setEnaled(0);
                            }
                            CardPaymentAdaper.notifyDataSetChanged();
                        }
                        mApiProviderList.clear();

                        dismiss();
                    }
                }
                return true;
            }
        });
    }

    public void resetTradeTypeButton(Button b) {
        //设置成未选中的状态,并在列表中删除
        b.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
        b.setTextColor(Color.parseColor("#535353"));
        b.setEnabled(true);
        mTradeTypeList.remove(Integer.valueOf(b.getTag().toString()));
    }

    public void setTradeTypeButtonGrey(Button b) {
        //设置成灰色
        b.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
        b.setTextColor(Color.parseColor("#E3E3E3"));
        b.setEnabled(false);
        mTradeTypeList.remove(Integer.valueOf(b.getTag().toString()));
    }

    public void resetTradeStatusButton(Button b) {
        //设置成未选中的状态
        b.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
        b.setTextColor(Color.parseColor("#535353"));
        b.setEnabled(true);
        mTradeStateList.remove(Integer.valueOf(b.getTag().toString()));
    }

    public void setTradeStatusButtonGrey(Button b) {
        //设置成灰色
        b.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
        b.setTextColor(Color.parseColor("#E3E3E3"));
        b.setEnabled(false);
        mTradeStateList.remove(Integer.valueOf(b.getTag().toString()));
    }

    private void initview(View v) {
        gv_card_payment_type = (GridViewForScrollView) v.findViewById(R.id.gv_card_payment_type);
        gv_card_payment_type.setSelector(new ColorDrawable(Color.TRANSPARENT));
        gv_card_payment_type.setFocusable(true);
        gv_card_payment_type.setFocusableInTouchMode(true);
        gv_card_payment_type.requestFocus();

        tv_type_sale = (Button) v.findViewById(R.id.tv_type_sale);
        tv_type_pre_auth = (Button) v.findViewById(R.id.tv_type_pre_auth);
        tv_type_pa_complete = (Button) v.findViewById(R.id.tv_type_pa_complete);
        tv_type_refund = (Button) v.findViewById(R.id.tv_type_refund);
        tv_type_cancellation = (Button) v.findViewById(R.id.tv_type_cancellation);
        tv_type_reversal = (Button) v.findViewById(R.id.tv_type_reversal);

        tv_status_success = (Button) v.findViewById(R.id.tv_status_success);
        tv_status_failed = (Button) v.findViewById(R.id.tv_status_failed);

        tv_confirm = (TextView) v.findViewById(R.id.tv_confirm);
        tv_confirm.setBackgroundResource(R.drawable.btn_finish);

        tv_reset = (TextView) v.findViewById(R.id.tv_reset);
        tv_reset.setTextColor(mContext.getResources().getColor(R.color.bg_text_new));

        listTypeButton.add(tv_type_sale);
        listTypeButton.add(tv_type_pre_auth);
        listTypeButton.add(tv_type_pa_complete);
        listTypeButton.add(tv_type_refund);
        listTypeButton.add(tv_type_cancellation);
        listTypeButton.add(tv_type_reversal);

        listStatusButton.add(tv_status_success);
        listStatusButton.add(tv_status_failed);

        //先把状态和类型的button都设置成未选中的状态
        ResetAllTradeTypeButton();
        ResetAllTradeStatusButton();

        list = new ArrayList<DynModel>();
        List<DynModel> listTemp = new ArrayList<DynModel>();
        //获取卡列表通道，全部的卡列表通道
        Object object = SharedPreUtils.readProduct("cardPayment_dynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
        if (object != null) {
            listTemp = (List<DynModel>) object;
            list.clear();
            list.addAll(listTemp);

            //根据初始化数据，更新已选品牌列表数据
            if (null != list && list.size() > 0) {
                if (mApiProviderList.size() > 0) {
                    for (int s : mApiProviderList) {
                        for (int i = 0; i < list.size(); i++) {
                            DynModel dynModel = list.get(i);
                            //把int转成string去匹配
                            String channel = s + "";
                            if (channel.equals(dynModel.getApiCode())) {
                                //0，默认状态 ，1，已经选中，2，置灰状态
                                dynModel.setEnaled(1);
                                list.set(i, dynModel);
                            }
                        }
                    }
                }
                gv_card_payment_type.setVisibility(View.VISIBLE);
                CardPaymentAdaper = new CardPaymentTypeAdapter(activity, list);
                gv_card_payment_type.setAdapter(CardPaymentAdaper);
            } else {
                gv_card_payment_type.setVisibility(View.GONE);
            }
        } else {
            gv_card_payment_type.setVisibility(View.GONE);
        }
    }

    private void setDate(List<Integer> payTypeList, List<Integer> payStatusList) {
        //更新交易类型---筛选交易类型（集合）
        //1、消费 2、预授权 3、预授权完成 4、退款 5、撤销 6、冲正
        //此处的代码注意
        //每次新进入到筛选界面，mTradeTypeList，都是null,初始化的时候并没有赋值
        //走完setDate之后才赋值
        if (null != payTypeList && payTypeList.size() > 0) {
            for (int s : payTypeList) {
                String payType = s + "";
                if (payType.equals(tv_type_sale.getTag().toString())) {
                    setCardPaymentTypeBtnSelected(tv_type_sale);
                } else if (payType.equals(tv_type_pre_auth.getTag().toString())) {
                    setCardPaymentTypeBtnSelected(tv_type_pre_auth);
                } else if (payType.equals(tv_type_pa_complete.getTag().toString())) {
                    setCardPaymentTypeBtnSelected(tv_type_pa_complete);
                } else if (payType.equals(tv_type_refund.getTag().toString())) {
                    setCardPaymentTypeBtnSelected(tv_type_refund);
                } else if (payType.equals(tv_type_cancellation.getTag().toString())) {
                    setCardPaymentTypeBtnSelected(tv_type_cancellation);
                } else if (payType.equals(tv_type_reversal.getTag().toString())) {
                    setCardPaymentTypeBtnSelected(tv_type_reversal);
                }
            }
        }

        //更新交易状态的类型---交易状态：2：成功，3：失败
        //此处的代码注意
        //每次新进入到筛选界面，mTradeStateList，都是null,初始化的时候并没有赋值
        //走完setDate之后才赋值
        if (null != payStatusList && payStatusList.size() > 0) {
            for (int s : payStatusList) {
                String payStatus = s + "";
                if (payStatus.equals(tv_status_success.getTag().toString())) {
                    setTradeStatusButtonSelected(tv_status_success);
                } else if (payStatus.equals(tv_status_failed.getTag().toString())) {
                    setTradeStatusButtonSelected(tv_status_failed);
                }
            }
        }
    }

    //重置所有类型button到未选中的状态
    public void ResetAllTradeTypeButton() {
        tv_type_sale.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
        tv_type_sale.setTextColor(Color.parseColor("#535353"));

        tv_type_pre_auth.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
        tv_type_pre_auth.setTextColor(Color.parseColor("#535353"));

        tv_type_pa_complete.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
        tv_type_pa_complete.setTextColor(Color.parseColor("#535353"));

        tv_type_refund.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
        tv_type_refund.setTextColor(Color.parseColor("#535353"));

        tv_type_cancellation.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
        tv_type_cancellation.setTextColor(Color.parseColor("#535353"));

        tv_type_reversal.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
        tv_type_reversal.setTextColor(Color.parseColor("#535353"));
    }

    //重置所有状态button到未选中的状态
    public void ResetAllTradeStatusButton() {
        tv_status_success.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
        tv_status_success.setTextColor(Color.parseColor("#535353"));

        tv_status_failed.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
        tv_status_failed.setTextColor(Color.parseColor("#535353"));
    }


    public void setCardPaymentTypeBtnSelected(Button b) {
        isReset = false;
        //选中的状态
        b.setBackgroundResource(R.drawable.button_shape);
        b.setTextColor(Color.WHITE);
        mTradeTypeList.add(Integer.parseInt(b.getTag().toString()));
    }

    public void ClickUpdateCardPaymentTypeBg(Button b) {
        isReset = false;
        if (mTradeTypeList.contains(Integer.valueOf(b.getTag().toString()))) {
            //未选中的状态
            b.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            mTradeTypeList.remove(Integer.valueOf(b.getTag().toString()));
            b.setTextColor(Color.parseColor("#535353"));
        } else {
            //选中的状态
            b.setBackgroundResource(R.drawable.button_shape);
            mTradeTypeList.add(Integer.parseInt(b.getTag().toString()));
            b.setTextColor(Color.WHITE);
            b.setEnabled(true);
        }
    }

    public void setTradeStatusButtonSelected(Button b) {
        isReset = false;
        //设置成选中的状态
        b.setBackgroundResource(R.drawable.button_shape);
        b.setTextColor(Color.WHITE);
        mTradeStateList.add(Integer.parseInt(b.getTag().toString()));
    }

    public void ClickUpdateCardPaymentStatusBg(Button b) {
        isReset = false;
        if (mTradeStateList.contains(Integer.valueOf(b.getTag().toString()))) {
            //未选中的状态
            b.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            mTradeStateList.remove(Integer.valueOf(b.getTag().toString()));
            b.setTextColor(Color.parseColor("#535353"));
        } else {
            //选中的状态
            b.setBackgroundResource(R.drawable.button_shape);
            mTradeStateList.add(Integer.parseInt(b.getTag().toString()));
            b.setTextColor(Color.WHITE);
            b.setEnabled(true);
        }
    }


    private void setListener() {
        gv_card_payment_type.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int postion, long arg3) {
                DynModel dynModel = list.get(postion);
                if (dynModel != null) {
                    TextView tv = (TextView) v.findViewById(R.id.tv_content);
                    if (mApiProviderList.contains(Integer.valueOf(dynModel.getApiCode()))) {
                        mApiProviderList.remove(Integer.valueOf(dynModel.getApiCode()));

                        //设置成未选中的状态
                        tv.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
                        tv.setTextColor(Color.parseColor("#535353"));
                        tv.setEnabled(true);

                    } else {
                        mApiProviderList.add(Integer.parseInt(dynModel.getApiCode()));
                        //变成可点击
                        tv.setBackgroundResource(R.drawable.button_shape);
                        tv.setTextColor(Color.WHITE);
                        tv.setEnabled(false);
                    }
                }
            }
        });

        tv_type_sale.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ClickUpdateCardPaymentTypeBg(tv_type_sale);
            }
        });

        tv_type_pre_auth.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ClickUpdateCardPaymentTypeBg(tv_type_pre_auth);
            }
        });
        tv_type_pa_complete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ClickUpdateCardPaymentTypeBg(tv_type_pa_complete);
            }
        });
        tv_type_refund.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ClickUpdateCardPaymentTypeBg(tv_type_refund);
            }
        });
        tv_type_cancellation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ClickUpdateCardPaymentTypeBg(tv_type_cancellation);
            }
        });
        tv_type_reversal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ClickUpdateCardPaymentTypeBg(tv_type_reversal);
            }
        });


        tv_status_success.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClickUpdateCardPaymentStatusBg(tv_status_success);
            }
        });

        tv_status_failed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClickUpdateCardPaymentStatusBg(tv_status_failed);
            }
        });


        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* if (mApiProviderList.size() == 0 && mTradeTypeList.size() == 0 && mTradeStateList.size() == 0 && !isReset) {
                    ToastHelper.showInfo(activity, ToastHelper.toStr(R.string.tx_bill_stream_chioce_not_null));
                    return;
                } else {
                    handleBtn.handleOkBtn(mApiProviderList, mTradeTypeList,mTradeStateList);
                    dismiss();
                }*/

                handleBtn.handleOkBtn(mApiProviderList, mTradeTypeList, mTradeStateList);
                dismiss();
            }
        });

        tv_reset.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               /* if(mApiProviderList.size() > 0 || mTradeStateList.size() > 0 || mTradeTypeList.size() > 0){
                    isReset = true;
                }*/

                for (Button buttonType : listTypeButton) {
                    resetTradeTypeButton(buttonType);
                }
                for (Button buttonStatus : listStatusButton) {
                    resetTradeStatusButton(buttonStatus);
                }

                mTradeStateList.clear();
                mTradeTypeList.clear();

                if (list != null && list.size() > 0) {
                    for (DynModel d : list) {
                        d.setEnaled(0);
                    }
                    CardPaymentAdaper.notifyDataSetChanged();
                }
                mApiProviderList.clear();
            }
        });
    }


    class CardPaymentTypeAdapter extends BaseAdapter {
        private List<DynModel> list;
        private Context context;

        private CardPaymentTypeAdapter(Context context, List<DynModel> list) {
            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.activity_bill_paytype_list_item, null);
                CardPaymentHolder = new CardPaymentTypeHolder();
                CardPaymentHolder.tv_content = (TextView) convertView.findViewById(R.id.tv_content);
                convertView.setTag(CardPaymentHolder);
            } else {
                CardPaymentHolder = (CardPaymentTypeHolder) convertView.getTag();
            }

            DynModel dynModel = list.get(position);
            if (dynModel != null && !StringUtil.isEmptyOrNull(dynModel.getProviderName())) {
                CardPaymentHolder.tv_content.setText(MainApplication.getInstance().getPayTypeMap().get(dynModel.getApiCode()));
                switch (dynModel.getEnabled()) {
                    case 0://默认状态
                        CardPaymentHolder.tv_content.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
                        CardPaymentHolder.tv_content.setTextColor(Color.parseColor("#535353"));
                        CardPaymentHolder.tv_content.setEnabled(true);
                        break;
                    case 1://选中
                        CardPaymentHolder.tv_content.setBackgroundResource(R.drawable.button_shape);
                        CardPaymentHolder.tv_content.setTextColor(Color.WHITE);
                        CardPaymentHolder.tv_content.setEnabled(true);
                        break;
                    case 2://置灰
                        CardPaymentHolder.tv_content.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
                        CardPaymentHolder.tv_content.setTextColor(Color.parseColor("#E3E3E3"));
                        CardPaymentHolder.tv_content.setEnabled(false);
                        break;
                    default:
                        break;
                }

            }
            return convertView;
        }
    }

    class CardPaymentTypeHolder {
        TextView tv_content;
    }
}
