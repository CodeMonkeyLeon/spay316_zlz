package cn.swiftpass.enterprise.bussiness.model;

import java.util.List;

/**
 * 流水查询结果
 * User: Alan
 * Date: 14-1-7
 * Time: 下午7:00
 * To change this template use File | Settings | File Templates.
 */
public class OrderSearchResult
{
    
    public List<Order> data;
    
    public Integer total;
}
