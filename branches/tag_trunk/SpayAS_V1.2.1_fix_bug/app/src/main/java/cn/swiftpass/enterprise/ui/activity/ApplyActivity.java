/*
 * 文 件 名:  ApplyActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2014-8-6
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.ApplyInfo;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.ToastHelper;
import cn.swiftpass.enterprise.utils.VerifyUtil;

/**
 * 我要申请
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2014-8-6]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class ApplyActivity extends TemplateActivity
{
    
    private TextView userName;
    
    private TextView phone;
    
    private TextView city;
    
    private TextView ac;
    
    private ImageView iv_ac_promt, iv_clearAc, iv_city_promt, iv_clearCity, iv_clearPhone, iv_phone_promt,
        iv_clearUser, iv_user_promt;
    
    private Button apple_but;
    
    private TextView text;
    
    private TextView agent_tel;
    
    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.apply);
        
        initVew();
        
        setLister(); // 暂时注释
        
        if (null != MainApplication.cityStr && !MainApplication.cityStr.equals(""))
        {
            //            loadAglentTel(MainApplication.cityStr);  暂时屏蔽
        }
        else
        {
            //            agent_tel.setText("，由工作人员与你签署协议开通授权");
            //            agent_tel.setTextSize(18.0f);
        }
    }
    
    /**
     * 加载代理商电话号码
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    @SuppressLint("ResourceAsColor")
    private void loadAglentTel(String city)
    {
        /**
         * 加载 行业数据
         * <功能详细描述>
         * @see [类、类#方法、类#成员]
         */
        
        UserManager.getAgentPhone(city, new UINotifyListener<String>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
            }
            
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                if (object != null)
                {
                    showToastInfo(object.toString());
                }
            }
            
            @Override
            public void onSucceed(String result)
            {
                super.onSucceed(result);
                if (result != null)
                {
                    agent_tel.setText(result);
                }
                else
                {
                    //                    agent_tel.setText("，由工作人员与你签署协议开通授权");
                    //                    agent_tel.setTextSize(18.0f);
                }
            }
        });
        
    }
    
    private void setLister()
    {
        iv_clearAc.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                ac.setText("");
            }
        });
        
        iv_clearCity.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                city.setText("");
            }
        });
        
        iv_clearUser.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                userName.setText("");
            }
        });
        
        iv_clearPhone.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                phone.setText("");
            }
        });
        
        // 提交
        apple_but.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                String userVal = userName.getText().toString();
                String phoneVal = phone.getText().toString();
                String cityVal = city.getText().toString();
                String acVal = ac.getText().toString();
                
                if (isAbsoluteNullStr(userVal))
                {
                    ToastHelper.showInfo("称呼不能为空!");
                    userName.setFocusable(true);
                    return;
                }
                
                if (isAbsoluteNullStr(phoneVal))
                {
                    ToastHelper.showInfo("电话不能为空!");
                    phone.setFocusable(true);
                    return;
                }
                
                if (isAbsoluteNullStr(cityVal))
                {
                    ToastHelper.showInfo("城市不能为空!");
                    city.setFocusable(true);
                    return;
                }
                
                if (VerifyUtil.verifyValid(phoneVal.trim(), GlobalConstant.MOBILEPHOE)
                    || VerifyUtil.verifyValid(phoneVal.trim(), GlobalConstant.TELPHONE))
                {
                    
                }
                else
                {
                    ToastHelper.showInfo("输入手机号码格式不正确!");
                    phone.setFocusable(true);
                    return;
                }
                
                if (isAbsoluteNullStr(acVal))
                {
                    ToastHelper.showInfo("企业名称不能为空!");
                    ac.setFocusable(true);
                    return;
                }
                ApplyInfo applyInfo = new ApplyInfo();
                applyInfo.setCity(cityVal);
                applyInfo.setName(userVal);
                applyInfo.setCompany(acVal);
                applyInfo.setPhone(phoneVal);
                
                UserManager.addApply(applyInfo, new UINotifyListener<Boolean>()
                {
                    @Override
                    public void onPreExecute()
                    {
                        showLoading(false, "提交中...");
                        super.onPreExecute();
                    }
                    
                    @Override
                    public void onError(Object object)
                    {
                        super.onError(object);
                        dismissLoading();
                        if (object != null)
                        {
                            showToastInfo(object.toString());
                        }
                    }
                    
                    @Override
                    public void onSucceed(Boolean result)
                    {
                        super.onSucceed(result);
                        dismissLoading();
                        if (result)
                        {
                            Toast.makeText(getApplicationContext(), "您提交申请已成功，稍候威富通工作人员会主动与你联系!", Toast.LENGTH_LONG)
                                .show();
                            finish();
                        }
                        else
                        {
                            ToastHelper.showInfo("提交申请失败，请稍候再试!");
                        }
                    }
                });
            }
        });
        
        text.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                //                showPage(AgentActivity.class);
                //                AgentManager.queryAllAgent(new UINotifyListener<ArrayList<AgentInfo>>()
                //                {
                //                    @Override
                //                    public void onPreExecute()
                //                    {
                //                        showLoading(false, "获取中...");
                //                        super.onPreExecute();
                //                    }
                //                    
                //                    @Override
                //                    public void onError(Object object)
                //                    {
                //                        super.onError(object);
                //                        dismissLoading();
                //                        if (object != null)
                //                        {
                //                            showToastInfo(object.toString());
                //                        }
                //                    }
                //                    
                //                    @Override
                //                    public void onSucceed(ArrayList<AgentInfo> result)
                //                    {
                //                        super.onSucceed(result);
                //                        dismissLoading();
                //                        if (result != null)
                //                        {
                //                            Bundle bundle = new Bundle();
                //                            bundle.putParcelableArrayList("agentList", (ArrayList<? extends Parcelable>)result);
                //                            showPage(AgentActivity.class, bundle);
                //                        }
                //                        else
                //                        {
                //                            ToastHelper.showError("获取失败，请稍候再试!");
                //                        }
                //                        
                //                    }
                //                });
            }
        });
    }
    
    private void initVew()
    {
        userName = getViewById(R.id.userName);
        
        phone = getViewById(R.id.phone);
        
        city = getViewById(R.id.city);
        
        text = getViewById(R.id.text);
        
        ac = getViewById(R.id.ac);
        iv_ac_promt = getViewById(R.id.iv_ac_promt);
        iv_clearAc = getViewById(R.id.iv_clearAc);
        iv_city_promt = getViewById(R.id.iv_city_promt);
        iv_clearCity = getViewById(R.id.iv_clearCity);
        iv_clearPhone = getViewById(R.id.iv_clearPhone);
        iv_phone_promt = getViewById(R.id.iv_phone_promt);
        iv_clearUser = getViewById(R.id.iv_clearUser);
        iv_user_promt = getViewById(R.id.iv_user_promt);
        
        agent_tel = getViewById(R.id.agent_tel);
        apple_but = getViewById(R.id.apple_but);
        
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        {
            
            @Override
            public void onExecute(CharSequence s, int start, int before, int count)
            {
            }
            
            @Override
            public void onAfterTextChanged(Editable s)
            {
                
                if (s != null && s.length() >= 1)
                {
                    if (userName.hasFocus())
                    {
                        iv_user_promt.setVisibility(View.GONE);
                        iv_clearUser.setVisibility(View.VISIBLE);
                    }
                    else if (phone.hasFocus())
                    {
                        iv_phone_promt.setVisibility(View.GONE);
                        iv_clearPhone.setVisibility(View.VISIBLE);
                    }
                    else if (city.hasFocus())
                    {
                        iv_city_promt.setVisibility(View.GONE);
                        iv_clearCity.setVisibility(View.VISIBLE);
                    }
                    else if (ac.hasFocus())
                    {
                        iv_ac_promt.setVisibility(View.GONE);
                        iv_clearAc.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    if (userName.hasFocus())
                    {
                        iv_user_promt.setVisibility(View.VISIBLE);
                        iv_clearUser.setVisibility(View.GONE);
                    }
                    else if (phone.hasFocus())
                    {
                        iv_phone_promt.setVisibility(View.VISIBLE);
                        iv_clearPhone.setVisibility(View.GONE);
                    }
                    else if (city.hasFocus())
                    {
                        iv_city_promt.setVisibility(View.VISIBLE);
                        iv_clearCity.setVisibility(View.GONE);
                    }
                    else if (ac.hasFocus())
                    {
                        iv_ac_promt.setVisibility(View.VISIBLE);
                        iv_clearAc.setVisibility(View.GONE);
                    }
                    
                }
                
            }
            
        });
        
        userName.addTextChangedListener(editTextWatcher);
        phone.addTextChangedListener(editTextWatcher);
        city.addTextChangedListener(editTextWatcher);
        ac.addTextChangedListener(editTextWatcher);
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_apply);
    }
    
}
