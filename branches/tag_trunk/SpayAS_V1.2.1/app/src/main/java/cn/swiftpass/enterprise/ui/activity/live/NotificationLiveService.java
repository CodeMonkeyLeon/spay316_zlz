package cn.swiftpass.enterprise.ui.activity.live;

import android.annotation.TargetApi;
import android.os.Build;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;


@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class NotificationLiveService extends NotificationListenerService {

    private static final String TAG = "NotificationLiveService";

    public NotificationLiveService() {

    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        Log.i(TAG, "open" + "-----" + sbn.getPackageName());
        Log.i(TAG, "open" + "------" + sbn.getNotification().tickerText);
        Log.i(TAG, "open" + "-----" + sbn.getNotification().extras.get("android.title"));
        Log.i(TAG, "open" + "-----" + sbn.getNotification().extras.get("android.text"));

        Log.i(TAG, "onNotificationPosted:" + sbn.toString());

    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        Log.i(TAG, "open" + "-----" + sbn.getPackageName());
    }
}
