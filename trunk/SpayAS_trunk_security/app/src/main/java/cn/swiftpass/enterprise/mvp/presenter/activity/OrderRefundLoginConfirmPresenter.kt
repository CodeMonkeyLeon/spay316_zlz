package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo
import cn.swiftpass.enterprise.bussiness.model.ErrorMsg
import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.bussiness.model.UserInfo
import cn.swiftpass.enterprise.common.Constant
import cn.swiftpass.enterprise.intl.BuildConfig
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.OrderRefundLoginConfirmContract
import cn.swiftpass.enterprise.utils.AESHelper
import cn.swiftpass.enterprise.utils.JsonUtil
import cn.swiftpass.enterprise.utils.MD5
import cn.swiftpass.enterprise.utils.SharedPreUtils
import cn.swiftpass.enterprise.utils.SignUtil
import cn.swiftpass.enterprise.utils.StringUtil
import com.igexin.sdk.PushManager
import okhttp3.Call
import java.util.Locale

/**
 * @author lizheng.zhao
 * @date 2022/12/2
 *
 * @那就折一张阔些的荷叶
 * @包一片月光回去
 * @回去夹在唐诗里
 * @扁扁的
 * @象压过的相思
 */
class OrderRefundLoginConfirmPresenter : OrderRefundLoginConfirmContract.Presenter {

    private var mView: OrderRefundLoginConfirmContract.View? = null


    override fun authUnfreeze(authNo: String?, money: String?) {
        mView?.let { view ->
            view.showLoading(R.string.loading, ParamsConstants.COMMON_LOADING)
            val remark =
                if (!TextUtils.isEmpty(MainApplication.getInstance().userInfo.tradeName)) MainApplication.getInstance().userInfo.tradeName else BuildConfig.body
            AppClient.authUnfreeze(
                MainApplication.getInstance().getMchId(),
                MainApplication.getInstance().getUserId().toString(),
                authNo,
                MainApplication.getInstance().userInfo.token,
                money,
                remark,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.authUnfreezeFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                Order::class.java
                            ) as Order
                            view.authUnfreezeSuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun cardPaymentRefund(
        orderNoMch: String?,
        outTradeNo: String?,
        totalFee: Long,
        refundMoney: Long
    ) {
        mView?.let { view ->
            view.showLoading(R.string.refunding_wait, ParamsConstants.COMMON_LOADING)
            val body =
                if (!TextUtils.isEmpty(MainApplication.getInstance().userInfo.tradeName)) MainApplication.getInstance().userInfo.tradeName else BuildConfig.body
            val userName =
                if (MainApplication.getInstance()
                        .isAdmin(0)
                ) MainApplication.getInstance().userInfo.realname else MainApplication.getInstance().userInfo.mchName
            AppClient.cardPaymentRefund(
                body,
                MainApplication.getInstance().getMchId(),
                userName,
                refundMoney.toString(),
                totalFee.toString(),
                Constant.CLIENT,
                outTradeNo,
                orderNoMch,
                MainApplication.getInstance().userInfo.token,
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.cardPaymentRefundFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"",
                                "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                Order::class.java
                            ) as Order
                            view.cardPaymentRefundSuccess(order)
                        }
                    }
                }
            )

        }
    }

    override fun regisRefunds(
        orderNo: String?,
        money: Long,
        refundMoney: Long
    ) {
        mView?.let { view ->
            view.showLoading(R.string.refunding_wait, ParamsConstants.COMMON_LOADING)

            val param: MutableMap<String, Any> = HashMap()
            orderNo?.let {
                param[ParamsConstants.OUT_TRADE_NO] = it
            }
            param[ParamsConstants.CLIENT] = Constant.CLIENT
            param[ParamsConstants.REFUND_MONEY] = refundMoney.toString() // 退款金额

            param[ParamsConstants.TOTAL_FEE] = money.toString() // 总金额


            param[ParamsConstants.MCH_ID] = MainApplication.getInstance().getMchId()

            if (MainApplication.getInstance().isAdmin(0)) {
                // 普通用户才需要查自己的数据，管理员可以查询全部
                param[ParamsConstants.USER_ID] =
                    MainApplication.getInstance().getUserId().toString()
                param[ParamsConstants.USER_NAME_N] =
                    MainApplication.getInstance().userInfo.realname // 收银员名称
            } else {
                param[ParamsConstants.USER_NAME_N] =
                    MainApplication.getInstance().userInfo.mchName // 商户名称
            }

            if (!TextUtils.isEmpty(MainApplication.getInstance().userInfo.tradeName)) {
                param[ParamsConstants.BODY] = MainApplication.getInstance().userInfo.tradeName
            } else {
                param[ParamsConstants.BODY] = BuildConfig.body
            }

            if (!TextUtils.isEmpty(MainApplication.getInstance().getSignKey())) {
                param[ParamsConstants.SIGN] = SignUtil.getInstance().createSign(
                    JsonUtil.jsonToMap(JsonUtil.mapToJsons(param)),
                    MainApplication.getInstance().getSignKey()
                )
            }
            //验证身份返回的token
            param[ParamsConstants.TOKEN] = MainApplication.getInstance().userInfo.token

            val spayRs = System.currentTimeMillis().toString()
            //加签名
            if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getNewSignKey())) {
                param[ParamsConstants.SPAY_RS] = spayRs
                param[ParamsConstants.NNS] = SignUtil.getInstance().createSign(
                    JsonUtil.jsonToMap(JsonUtil.mapToJsons(param)),
                    MainApplication.getInstance().getNewSignKey()
                )
            }

            AppClient.regisRefunds(
                spayRs,
                JsonUtil.mapToJsons(param),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.regisRefundsFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"",
                                "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                Order::class.java
                            ) as Order
                            order.refundFee = order.refundMoney
                            order.cashFeel =
                                if (TextUtils.isEmpty(order.cashFee)) 0 else order.cashFee.toLong()

                            view.regisRefundsSuccess(order)

                        }
                    }
                }
            )
        }
    }

    override fun refundLogin(
        code: String?,
        username: String?,
        password: String?
    ) {

        mView?.let { view ->

            view.showLoading(
                R.string.tx_bill_stream_refund_login_loading,
                ParamsConstants.COMMON_LOADING
            )

            val param: MutableMap<String, Any> = java.util.HashMap()
            username?.let {
                param[ParamsConstants.USER_NAME] = it
            }
            val spayRs = System.currentTimeMillis().toString()
            val priKey = SharedPreUtils.readProduct(ParamsConstants.SECRET_KEY) as String?
            val paseStr = MD5.md5s(priKey).uppercase(Locale.getDefault())
            param[ParamsConstants.PASSWORD] =
                AESHelper.aesEncrypt(password, paseStr.substring(8, 24))
            param[ParamsConstants.S_KEY] = MainApplication.getInstance().getSKey()

            param[ParamsConstants.CLIENT] = Constant.CLIENT
            param[ParamsConstants.BANK_CODE] = BuildConfig.bankCode
            param[ParamsConstants.ANDROID_TRANS_PUSH] = "1"

            code?.let {
                param[ParamsConstants.CODE] = it
            }
            val cid = PushManager.getInstance().getClientid(MainApplication.getInstance())
            cid?.let {
                param[ParamsConstants.PUSH_CID] = it
            }

            param[ParamsConstants.SPAY_RS] = spayRs
            param.put(
                ParamsConstants.NNS, SignUtil.getInstance().createSign(
                    JsonUtil.jsonToMap(JsonUtil.mapToJsons(param)),
                    MD5.md5s(priKey).substring(0, 16)
                )
            )


            AppClient.refundLogin(
                spayRs,
                JsonUtil.mapToJsons(param),
                object : CallBackUtil.CallBackCommonResponse<UserInfo>(false, UserInfo()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            when (it.result) {
                                "403" -> {
                                    //需要输入验证
                                    if (!StringUtil.isEmptyOrNull(code)) {
                                        val msg = ErrorMsg()
                                        msg.message = it.message
                                        msg.content = it.code
                                        view.refundLoginFailed(msg)
                                    } else {
                                        view.refundLoginFailed("403${it.code}")
                                    }
                                }

                                "403" -> {

                                }

                                "405" -> {
                                    view.refundLoginFailed("405${it.message}")
                                }

                                else -> {
                                    view.refundLoginFailed(it.message)
                                }

                            }
                        }

                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val user = JsonUtil.jsonToBean(
                                response.message,
                                UserInfo::class.java
                            ) as UserInfo
                            MainApplication.getInstance().userInfo.token = user.token
                            view.refundLoginSuccess(true)
                        }

                    }
                }
            )

        }
    }

    override fun ecdhKeyExchange(publicKey: String?) {
        mView?.let { view ->
            AppClient.ecdhKeyExchange(
                publicKey,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<ECDHInfo>(ECDHInfo()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        commonResponse?.let {
                            view.ecdhKeyExchangeFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        r?.let { response ->
                            val info = JsonUtil.jsonToBean(
                                response.message,
                                ECDHInfo::class.java
                            ) as ECDHInfo
                            MainApplication.getInstance().setSerPubKey(info.serPubKey)
                            MainApplication.getInstance().setSKey(info.skey)
                            view.ecdhKeyExchangeSuccess(info)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: OrderRefundLoginConfirmContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}