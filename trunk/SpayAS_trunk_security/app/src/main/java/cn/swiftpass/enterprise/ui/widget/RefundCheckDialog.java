package cn.swiftpass.enterprise.ui.widget;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.common.sentry.SentryUtils;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.dialog.RefundCheckContract;
import cn.swiftpass.enterprise.mvp.presenter.dialog.RefundCheckPresenter;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.KotlinUtils;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/***
 * 退款检查对话框
 */
public class RefundCheckDialog extends BaseDialog<RefundCheckContract.View, RefundCheckContract.Presenter>
        implements View.OnClickListener, RefundCheckContract.View {


    public static final int REFUND = 0;
    //private ViewGroup mRootView;
    public static final int PAY = 1;
    public static final int REVERS = 2; // 冲正
    public static final int VARD = 3; // 冲正
    private static final String TAG = RefundCheckDialog.class.getSimpleName();
    String money;
    //private UserModel userModel;
//    ProgressDialog loadingDialog;
    private Button btnOk, btnCancel;
    private ConfirmListener btnListener;
    private EditText etPwd, edit_input_money;
    private TextView tvUserName, tv_order_no, tv_order_tx, tv_money_tx, tv_refund_money, tv_retreat;
    private Activity mContext;
    private LinearLayout ly_order, ly_money, lay_pass, ly_refund_money, ly_retreat;
    private LinearLayout ll_bg_reverse;
    private int flag;
    private Order orderModel;
    private long lastRefundMoney = 0;
    private TextView tv_promt;
    private double minus = 1;
    private boolean isPreAuth = false;

    public RefundCheckDialog(Activity context, boolean isPreAuth, int flag, String title, String tv_moneyStr, String order_no,
                             String money, Order orderModel, ConfirmListener btnListener) {
        super(context);
        mContext = context;
        this.isPreAuth = isPreAuth;
        this.orderModel = orderModel;
        this.btnListener = btnListener;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_refund_pwd);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        this.setCanceledOnTouchOutside(false);
        this.flag = flag;
        TextView tvTitle = (TextView) findViewById(R.id.title);
        if (title != null) {
            tvTitle.setText(title);
        }
        //根据当前的小数点的位数来判断应该除以多少
        for (int i = 0; i < MainApplication.getInstance().getNumFixed(); i++) {
            minus = minus * 10;
        }
        ll_bg_reverse = (LinearLayout) findViewById(R.id.id_ll_bg_reverse);
        ly_refund_money = (LinearLayout) findViewById(R.id.ly_refund_money);
        tv_refund_money = (TextView) findViewById(R.id.tv_refund_money);
        ly_retreat = (LinearLayout) findViewById(R.id.ly_retreat);
        tv_retreat = (TextView) findViewById(R.id.tv_retreat);
        etPwd = (EditText) findViewById(R.id.edit_input);
        etPwd.setVisibility(View.GONE);
        tvUserName = (TextView) findViewById(R.id.tv_userName);
        //userModel = LocalAccountManager.getInstance().getLoggedUser();
        btnOk = (Button) findViewById(R.id.ok);
        btnCancel = (Button) findViewById(R.id.cancel);
        tv_order_no = (TextView) findViewById(R.id.tv_order_no);
        tv_money_tx = (TextView) findViewById(R.id.tv_money_tx);
        lay_pass = (LinearLayout) findViewById(R.id.lay_pass);
        lay_pass.setVisibility(View.GONE);
        edit_input_money = (EditText) findViewById(R.id.edit_input_money);
        setPricePoint(edit_input_money);


        RelativeLayout rlButton = findViewById(R.id.rl_button);
        RelativeLayout rlReverseButton = findViewById(R.id.id_rl_reverse);
        if (flag == REVERS) {
            rlButton.setVisibility(View.GONE);
            rlReverseButton.setVisibility(View.VISIBLE);
            btnOk = (Button) findViewById(R.id.id_btn_reverse_ok);
            btnCancel = (Button) findViewById(R.id.id_btn_reverse_cancel);
        } else {
            rlButton.setVisibility(View.VISIBLE);
            rlReverseButton.setVisibility(View.GONE);
            btnOk = (Button) findViewById(R.id.ok);
            btnCancel = (Button) findViewById(R.id.cancel);
        }


        if (orderModel != null) {
            if (orderModel.getRfMoneyIng() > 0 || orderModel.getRefundMoney() > 0) {
                ly_refund_money.setVisibility(View.VISIBLE);
                ly_retreat.setVisibility(View.VISIBLE);
                long refundM = orderModel.getRefundMoney() + orderModel.getRfMoneyIng();
                tv_refund_money.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(refundM));

                lastRefundMoney = Long.parseLong(money) - refundM;

                tv_retreat.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtil(lastRefundMoney / minus));
            }
        }

        if (null == order_no) {
            tv_order_no.setVisibility(View.GONE);
            tv_order_tx = (TextView) findViewById(R.id.tv_order_tx);
            tv_order_tx.setVisibility(View.GONE);
            ly_order = (LinearLayout) findViewById(R.id.ly_order);
            ly_order.setVisibility(View.GONE);
        } else {
            tv_order_no.setText(order_no);
            tv_order_no.setVisibility(View.VISIBLE);
        }

        if (money == null) {
            ly_money = (LinearLayout) findViewById(R.id.ly_money);
            ly_money.setVisibility(View.GONE);
            tvUserName.setVisibility(View.GONE);
            tv_money_tx.setVisibility(View.GONE);
        } else {
            tvUserName.setVisibility(View.VISIBLE);
            tvUserName.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtil(Double.parseDouble(money) / minus));
            tv_money_tx.setText(tv_moneyStr);
            this.money = money;
        }
        if (flag == REVERS) {

            //设置背景白色
            ll_bg_reverse.setBackgroundResource(R.drawable.bg_reverse);
            //隐藏title
            tvTitle.setVisibility(View.GONE);
            LinearLayout llTitle = findViewById(R.id.id_ll_title);
            llTitle.setVisibility(View.GONE);
            edit_input_money.setVisibility(View.GONE);
            btnOk.setText(context.getString(R.string.revers_tx));
//            if (isPreAuth) {//如果是预授权
//                btnOk.setBackgroundResource(R.color.bg_btn_enable_pre_auth);
//            } else {
//                btnOk.setBackgroundResource(R.color.bg_btn_enable_new);
//            }
            btnCancel.setText(context.getString(R.string.query_tx));
            ly_money = (LinearLayout) findViewById(R.id.ly_money);
            ly_money.setVisibility(View.GONE);
            ly_order = (LinearLayout) findViewById(R.id.ly_order);
            ly_order.setVisibility(View.GONE);
            tv_promt = (TextView) findViewById(R.id.tv_promt);
            tv_promt.setVisibility(View.VISIBLE);
            tv_promt.setText(R.string.tv_revers_prompt);

            //内容布局
            tv_promt.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
            tv_promt.setTextColor(context.getResources().getColor(R.color.black));
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tv_promt.getLayoutParams();
            layoutParams.topMargin = KotlinUtils.INSTANCE.dp2px(41, context);
            layoutParams.bottomMargin = KotlinUtils.INSTANCE.dp2px(33, context);
            layoutParams.leftMargin = KotlinUtils.INSTANCE.dp2px(41, context);
            layoutParams.rightMargin = KotlinUtils.INSTANCE.dp2px(41, context);
            tv_promt.setLayoutParams(layoutParams);
            //内容下划线
            View tvLine = findViewById(R.id.id_view_reverse_line);
            tvLine.setVisibility(View.VISIBLE);

        } else if (flag == VARD) {
            btnOk.setText(R.string.bt_goon_affirm);
            btnCancel.setText(R.string.tx_to_receivables);
        }
        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    public RefundCheckDialog(Activity context, int flag, String title, String order_no, String money, Order orderModel,
                             ConfirmListener btnListener) {
        super(context);
        this.orderModel = orderModel;
        mContext = context;
        this.btnListener = btnListener;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_refund_pwd);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        this.setCanceledOnTouchOutside(false);
        this.flag = flag;
        TextView tvTitle = (TextView) findViewById(R.id.title);
        if (title != null) {
            tvTitle.setText(title);
        }
        ly_refund_money = (LinearLayout) findViewById(R.id.ly_refund_money);
        tv_refund_money = (TextView) findViewById(R.id.tv_refund_money);
        ly_retreat = (LinearLayout) findViewById(R.id.ly_retreat);
        tv_retreat = (TextView) findViewById(R.id.tv_retreat);
        etPwd = (EditText) findViewById(R.id.edit_input);
        tvUserName = (TextView) findViewById(R.id.tv_userName);
        //userModel = LocalAccountManager.getInstance().getLoggedUser();
        btnOk = (Button) findViewById(R.id.ok);
        btnCancel = (Button) findViewById(R.id.cancel);
        tv_order_no = (TextView) findViewById(R.id.tv_order_no);
        edit_input_money = (EditText) findViewById(R.id.edit_input_money);
        setPricePoint(edit_input_money);
        if (orderModel != null) {
            if (orderModel.getRfMoneyIng() > 0 || orderModel.getRefundMoney() > 0) {
                ly_refund_money.setVisibility(View.VISIBLE);
                ly_retreat.setVisibility(View.VISIBLE);
                long refundM = orderModel.getRefundMoney() + orderModel.getRfMoneyIng();
                tv_refund_money.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(refundM));

                lastRefundMoney = Long.parseLong(money) - refundM;

                tv_retreat.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtil(lastRefundMoney / minus));
            }
        }

        if (null == order_no) {
            tv_order_no.setVisibility(View.GONE);
            tv_order_tx = (TextView) findViewById(R.id.tv_order_tx);
            tv_order_tx.setVisibility(View.GONE);
            ly_order = (LinearLayout) findViewById(R.id.ly_order);
            ly_order.setVisibility(View.GONE);
        } else {
            tv_order_no.setText(order_no);
            tv_order_no.setVisibility(View.VISIBLE);
        }

        if (money == null) {
            ly_money = (LinearLayout) findViewById(R.id.ly_money);
            ly_money.setVisibility(View.GONE);
            tvUserName.setVisibility(View.GONE);
            tv_money_tx = (TextView) findViewById(R.id.tv_money_tx);
            tv_money_tx.setVisibility(View.GONE);
        } else {
            tvUserName.setVisibility(View.VISIBLE);
            tvUserName.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtil(Double.parseDouble(money) / minus));
            this.money = money;
        }

        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

        /**
         * 可配置设置
         */
        DynModel dynModel = (DynModel) SharedPreUtils.readProduct("dynModel" + BuildConfig.bankCode);
        if (null != dynModel) {
            try {
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonColor())) {//btCancel
                    btnOk.setBackgroundColor(Color.parseColor(dynModel.getButtonColor()));
                    btnCancel.setBackgroundColor(Color.parseColor(dynModel.getButtonColor()));
                }

                if (!StringUtil.isEmptyOrNull(dynModel.getButtonFontColor())) {
                    btnOk.setTextColor(Color.parseColor(dynModel.getButtonFontColor()));
                    btnCancel.setTextColor(Color.parseColor(dynModel.getButtonFontColor()));
                }
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                Log.e(TAG, Log.getStackTraceString(e));
            }
        }
    }

    @NonNull
    @Override
    protected RefundCheckContract.Presenter createPresenter() {
        return new RefundCheckPresenter();
    }

    @Override
    protected void bindView(@NonNull RefundCheckContract.Presenter presenter) {
        presenter.attachView(this);
    }

    private void setPricePoint(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().contains(".")) {
                    if (s.length() - 1 - s.toString().indexOf(".") > 2) {
                        s = s.toString().subSequence(0, s.toString().indexOf(".") + 3);
                        editText.setText(s);
                        editText.setSelection(s.length());
                    }
                }
                if (s.toString().trim().substring(0).equals(".")) {
                    s = "0" + s;
                    editText.setText(s);
                    editText.setSelection(2);
                }

                if (s.toString().startsWith("0") && s.toString().trim().length() > 1) {
                    if (!s.toString().substring(1, 2).equals(".")) {
                        editText.setText(s.subSequence(0, 1));
                        editText.setSelection(1);
                        return;
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

        });

    }

    //    public static void show(Activity context, String title, String order_no, String money, ConfirmListener btnListener)
    //    {
    //        RefundCheckDialog dia = new RefundCheckDialog(context, title, order_no, money, btnListener);
    //        dia.show();
    //    }

    public void setInputType(int type) {
        if (etPwd != null) {
            etPwd.setInputType(type);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ok:
            case R.id.id_btn_reverse_ok:
                switch (flag) {
                    case RefundCheckDialog.REFUND:
                        MyToast toast = new MyToast();
                        if (TextUtils.isEmpty(etPwd.getText().toString())) {
                            toast.showToast(mContext, ToastHelper.toStr(R.string.et_refund_pass));
                            return;
                        }
                        if (etPwd.getText().toString().length() < 8) {
                            toast.showToast(mContext, ToastHelper.toStr(R.string.et_refund_pass_less));
                            return;
                        }
                        String input_money = edit_input_money.getText().toString();
                        if (TextUtils.isEmpty(input_money)) {

                            toast.showToast(mContext, ToastHelper.toStr(R.string.et_refund_moeny));
                            return;
                        }
                        if (input_money.contains(".")
                                && input_money.substring(input_money.lastIndexOf('.')).equals(".")) {
                            toast.showToast(mContext, ToastHelper.toStr(R.string.et_refund_format));
                            edit_input_money.setFocusable(true);
                            return;
                        }

                        double editMoney = Double.parseDouble(input_money) * minus;
                        int lastMoney = (int) editMoney;
                        if (Integer.parseInt(money) < lastMoney) {
                            toast.showToast(mContext, ToastHelper.toStr(R.string.et_refund_less_money));
                            return;
                        }
                        if (orderModel != null && orderModel.getRfMoneyIng() > 0) {
                            if (lastMoney > lastRefundMoney) {
                                toast.showToast(mContext, ToastHelper.toStr(R.string.et_refund_less_remoney));
                                return;
                            }
                        }
                        checkUser();
                        break;
                    case RefundCheckDialog.PAY:
                        btnListener.ok(etPwd.getText().toString());
                        break;
                    case REVERS:

                        btnListener.ok("REVERS");
                        //                        dismiss();
                        break;

                }

                break;
            case R.id.cancel:
            case R.id.id_btn_reverse_cancel:
                if (btnListener != null) {
                    btnListener.cancel();
                    dismiss();
                }
                break;
            default:
                break;
        }

    }


    @Override
    public void loginAsyncSuccess(boolean response) {
        if (response) {
            if (btnListener != null) {
                String money = edit_input_money.getText().toString();
                if (!TextUtils.isEmpty(money)) {
                    btnListener.ok(KotlinUtils.INSTANCE.getMoney(money));
                }
                dismiss();
            }
        }
    }

    @Override
    public void loginAsyncFailed(@Nullable Object error) {
        if (error != null) {
            // ToastHelper.showInfo(object.toString());
            mContext.runOnUiThread(new Runnable() {
                public void run() {
                    MyToast myToast = new MyToast();
                    myToast.showToast(mContext, error.toString());
                }
            });
        }
    }

    /**
     * 验证用户
     */
    private void checkUser() {

        if (getMPresenter() != null) {
            getMPresenter().loginAsync(
                    null,
                    MainApplication.getInstance().getMchName(),
                    etPwd.getText().toString()
            );
        }
    }

//    public void showLoading(String str) {
//        if (loadingDialog == null) {
//            loadingDialog = new ProgressDialog(mContext);
//            loadingDialog.setCancelable(true);
//
//        }
//        loadingDialog.show();
//        loadingDialog.setMessage(str);
//    }
//
//    public void dismissMyLoading() {
//        if (loadingDialog != null) {
//            loadingDialog.dismiss();
//            loadingDialog = null;
//        }
//    }

    public interface ConfirmListener {
        public void ok(String code);

        public void cancel();
    }
}
