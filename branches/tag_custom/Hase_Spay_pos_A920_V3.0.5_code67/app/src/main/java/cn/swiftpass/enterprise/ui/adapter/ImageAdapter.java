package cn.swiftpass.enterprise.ui.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import cn.swiftpass.enterprise.intl.R;

public class ImageAdapter extends BaseAdapter
{
    
    //private Context mContext;
    
    private LayoutInflater mInflater;
    
    //private static final int[] ids = {R.drawable.img_ads_3,R.drawable.img_ads_1, R.drawable.img_ads_2};
    private List<Bitmap> bitmaps;
    
    private boolean isInify = false;
    
    public void setData(List<Bitmap> bitmaps)
    {
        this.bitmaps = bitmaps;
        isInify = true;
    }
    
    public void init()
    {
        isInify = false;
    }
    
    public ImageAdapter(Context context)
    {
        //mContext = context;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    
    @Override
    public int getCount()
    {
        return Integer.MAX_VALUE; //返回很大的值使得getView中的position不断增大来实现循环
    }
    
    @Override
    public Object getItem(int position)
    {
        return position;
    }
    
    @Override
    public long getItemId(int position)
    {
        return position;
    }
    
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            convertView = mInflater.inflate(R.layout.image_item, null);
        }
        ImageView iv = ((ImageView)convertView.findViewById(R.id.imgView));
        if (isInify)
        {
            iv.setImageBitmap(bitmaps.get(position % bitmaps.size()));
        }
        else
        {
            // iv.setImageResource(ids[position%ids.length]);
        }
        convertView.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                
            }
        });
        return convertView;
    }
    
}
