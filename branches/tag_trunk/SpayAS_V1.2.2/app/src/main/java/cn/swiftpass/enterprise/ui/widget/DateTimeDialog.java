package cn.swiftpass.enterprise.ui.widget;

import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-26
 * Time: 上午11:25
 * To change this template use File | Settings | File Templates.
 */
public class DateTimeDialog {

    public static void showDatetimeDialog(Context mContext,final OnSelectDateTimeListener listener){
        final Calendar c = Calendar.getInstance();
        DatePickerDialog  dialog = new DatePickerDialog(
                mContext,
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker dp, int year,int month, int dayOfMonth) {

                        c.set(year,month,dayOfMonth);

                        listener.onCheck(c.getTimeInMillis());
                    }
                },
                c.get(Calendar.YEAR), // 传入年份
                c.get(Calendar.MONTH), // 传入月份
                c.get(Calendar.DAY_OF_MONTH) // 传入天数*/
        );
        dialog.show();
    }

    public interface OnSelectDateTimeListener
    {
        public void onCheck(long time);
    }
}
