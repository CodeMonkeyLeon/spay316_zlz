package cn.swiftplus.enterprise.printsdk.print;
import android.content.Context;
import com.pax.gl.imgprocessing.IImgProcessing;

import cn.swiftplus.enterprise.printsdk.print.A8.POSA8Client;
import cn.swiftplus.enterprise.printsdk.print.A8.printUtils.PrintA8Bean;
import cn.swiftplus.enterprise.printsdk.print.A8.printUtils.PrintA8Text;
import cn.swiftplus.enterprise.printsdk.print.A920.POSA920Client;
import cn.swiftplus.enterprise.printsdk.print.A920.printUtils.PrintA920Text;


/**
 * Created by aijingya on 2020/8/7.
 *
 * @Package cn.swiftplus.enterprise.printsdk.print
 * @Description:
 * @date 2020/8/7.10:50.
 */
public class PrintClient {
    private static PrintClient sPrintClient;
    private static String sPosTerminal;
    private Context mAppContext;

    public static void initClient(Context context){
        if (sPrintClient != null) {
            throw new RuntimeException("PrintClient Already initialized");
        }
        sPrintClient = new PrintClient(context.getApplicationContext());
    }

    public static PrintClient getInstance() {
        if (sPrintClient == null) {
            throw new RuntimeException("PrintClient is not initialized");
        }
        return sPrintClient;
    }

    private PrintClient(Context context ){
        mAppContext = context;
    }

    public void initPrintSDK(String posTerminal){
        sPosTerminal = posTerminal;
        //根据不同的POS机型号，初始化不同的打印机SDK
        if(sPosTerminal.equalsIgnoreCase("A920")){
            POSA920Client.initA920(mAppContext);
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            POSA8Client.initA8(mAppContext);
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

    public void printTitle(String text){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().init(mAppContext);
            PrintA920Text.getInstance().printTitle(text);
            PrintA920Text.getInstance().printEmptyLine();
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.TITLE,text);
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

    public void printTextLeft(String text){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().printTextLine(text,24, IImgProcessing.IPage.EAlign.LEFT);
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.TEXT_LINE_LEFT,text);
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

    public void  printTextRight(String text){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().printTextLine(text,24, IImgProcessing.IPage.EAlign.RIGHT);
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.TEXT_LINE_RIGHT,text);
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

    public void  printTextCenter(String text){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().printTextLine(text,24, IImgProcessing.IPage.EAlign.CENTER);
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.TEXT_LINE_CENTER,text);
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

    public void printMultiLines(String text){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().printTextLine(text,24, IImgProcessing.IPage.EAlign.LEFT);
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.TEXT_MULTI_LINES,text);
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

    public void  printQRCode(String orderNoMch){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().printQRCode(orderNoMch);
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.QR_CODE,orderNoMch);
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

    public void printDoubleLine(){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().printDoubleLine();
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.DOUBLE_LINE,"");
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

    public void pintSingleLine(){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().pintSingleLine();
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.SINGLE_LINE,"");
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

    public void printEmptyLine(){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().printEmptyLine();
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.EMPTY_LINE,"");
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

    public void startPrint(){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().startPrint();
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().init(mAppContext);
            PrintA8Text.getInstance().startPrint();
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

    public void bindDeviceService(){
        if(sPosTerminal.equalsIgnoreCase("A920")){
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().bindDeviceService();
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }


    public void unbindDeviceService(){
        if(sPosTerminal.equalsIgnoreCase("A920")){
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().unbindDeviceService();
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

}