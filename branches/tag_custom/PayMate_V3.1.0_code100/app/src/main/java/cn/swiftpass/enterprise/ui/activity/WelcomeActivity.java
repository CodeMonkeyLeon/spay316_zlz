package cn.swiftpass.enterprise.ui.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.text.method.LinkMovementMethod;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.URLSpan;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.igexin.sdk.PushManager;
import com.tencent.stat.MtaSDkException;
import com.tencent.stat.StatService;
import com.ziyeyouhu.library.KeyboardTouchListener;
import com.ziyeyouhu.library.KeyboardUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.security.cert.Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.upgrade.UpgradeManager;
import cn.swiftpass.enterprise.bussiness.model.CertificateBean;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo;
import cn.swiftpass.enterprise.bussiness.model.ErrorMsg;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo;
import cn.swiftpass.enterprise.bussiness.model.WalletListBean;
import cn.swiftpass.enterprise.camera.CameraActivity;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.live.ForegroundLiveService;
import cn.swiftpass.enterprise.ui.activity.live.JobSchedulerManager;
import cn.swiftpass.enterprise.ui.activity.live.PlayerMusicService;
import cn.swiftpass.enterprise.ui.activity.setting.SettingCDNActivity;
import cn.swiftpass.enterprise.ui.activity.user.FindPassFirstActivity;
import cn.swiftpass.enterprise.ui.widget.CommonConfirmDialog;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.DialogInfo.HandleBtn;
import cn.swiftpass.enterprise.ui.widget.DialogShowInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.dialog.CashierNoticeDialog;
import cn.swiftpass.enterprise.ui.widget.dialog.RootDialog;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.CertificateUtils;
import cn.swiftpass.enterprise.utils.DataReportUtils;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.ECDHUtils;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.FileUtils;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.ImageUtil;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.NetworkUtils;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

import static cn.swiftpass.enterprise.MainApplication.IS_POS_VERSION;
import static cn.swiftpass.enterprise.camera.CameraActivity.KEY_IMAGE_PATH;
import static cn.swiftpass.enterprise.ui.activity.MyWebViewActicity.REQUESTCODE_CAMERA_TAKE_PICTURE;
import static cn.swiftpass.enterprise.utils.RootUtils.isDeviceRootedAndroid;
import static cn.swiftpass.enterprise.utils.RootUtils.isDeviceRootedPos;

/**
 * 欢迎界面
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-8-20
 * Time: 下午3:30
 */
public class WelcomeActivity extends BaseActivity {
    private static final String TAG = WelcomeActivity.class.getCanonicalName();

    private Context mContext;

    private Button btn_login;

    private TextView tvVersion;

    private TextView tvchangeCDN;

    private TextView tv_privacy_policy;

    private int[] imageResId; // 图片ID

    private ViewPager viewPager;

    private int currentItem = 0; // 当前图片的索引号

    private List<ImageView> imageViews; // 滑动的图片集合


    private SharedPreferences sp;

    private EditText userPwd;

    private EditText user_name;

    private ImageView iv_clearUser;

    private ImageView iv_clearPwd;

    private ImageView iv_code, v_code;

    private TextView tv_load;

    private LinearLayout ly_code;

    private EditText ed_code;

    private boolean isInputCode = false;

    private LinearLayout ly_title;

    private RelativeLayout ly_first;

    String login_skey, login_sauthid;

    private ImageView iv_login;

    private TextView tv_server_name;

    private Button btn_sign_up;
    private TextView tv_check_ekyc_state;

//    private EditText et_input_url;

    private boolean isNeedRequestECDHKey = false;

    private boolean isNeedDeviceLogin = false;

    private RootDialog Dialog_root;

    private CashierNoticeDialog ChangePswNoticeDialog;

    private boolean cerFailedRequestAgain = false;

    public LinearLayout ll_bdo_ekyc;

  /*  private String photoName;
    private Uri photoUri;*/

    //存储在本地的在服务端证书列表
    private ArrayList<Certificate> cerList = new ArrayList<Certificate>();

    //    // 切换当前显示的图片
    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            //            if (msg.what == 1)
            //            {
            //                initPromt();
            //            }
            //            viewPager.setCurrentItem(currentItem);// 切换当前显示的图片
            if (msg.what == 2) {
                Intent it = new Intent();
                it.setClass(WelcomeActivity.this, spayMainTabActivity.class);
                startActivity(it);
//                MainActivity.startActivity(WelcomeActivity.this, "OrderDetailsActivity");
                WelcomeActivity.this.finish();
                //                initPromt();
            }
        }

    };


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }


    private LinearLayout rootView;
    private KeyboardUtil keyboardUtil;
    private ScrollView scrollView;

    //安全键盘
    private void initMoveKeyBoard() {
        keyboardUtil = new KeyboardUtil(this, rootView, scrollView);
        keyboardUtil.setOtherEdittext(user_name);
        keyboardUtil.setOtherEdittext(ed_code);

        //测试代码
//        keyboardUtil.setOtherEdittext(et_input_url);

        // monitor the KeyBarod state
        keyboardUtil.setKeyBoardStateChangeListener(new WelcomeActivity.KeyBoardStateListener());
        // monitor the finish or next Key
        keyboardUtil.setInputOverListener(new WelcomeActivity.inputOverListener());
        userPwd.setOnTouchListener(new KeyboardTouchListener(keyboardUtil, KeyboardUtil.INPUTTYPE_ABC, -1));
    }

    class KeyBoardStateListener implements KeyboardUtil.KeyBoardStateChangeListener {

        @Override
        public void KeyBoardStateChange(int state, EditText editText) {

        }
    }

    class inputOverListener implements KeyboardUtil.InputFinishListener {

        @Override
        public void inputHasOver(int onclickType, EditText editText) {

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (keyboardUtil.isShow) {
                keyboardUtil.hideSystemKeyBoard();
                keyboardUtil.hideAllKeyBoard();
                keyboardUtil.hideKeyboardLayout();

                finish();
            } else {
                return super.onKeyDown(keyCode, event);
            }
            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Android 6.0  ---> Android 9.0
        if (isNeedCheck && Build.VERSION.SDK_INT >= 23 && Build.VERSION.SDK_INT < 29) {
            checkPermissions(PERMISSIONS);
        }else if(isNeedCheck && Build.VERSION.SDK_INT >= 29 ){//Android 10.0 以及以上
            checkPermissions(PERMISSIONS_ANDROID10);
        }

        if(Build.VERSION.SDK_INT < 29){
            //如果用户拒绝了读取手机设备权限 弹出框提示设置 重新进来 检测弹框是否依然存在
            if (isGranted(Manifest.permission.READ_PHONE_STATE)) {
                if (dialogInfo != null && dialogInfo.isShowing()) {
                    dialogInfo.dismiss();
                    showLoginPage();
                }
            }
        }

        //重新把登录button恢复成可点击的状态
        btn_login.setEnabled(true);
        btn_login.setText(R.string.login);

        user_name.requestFocus();
    }


    @Override
    protected void onPause() {
        super.onPause();

        //回到后台的时候，清除界面的密码
        iv_clearPwd.setVisibility(View.GONE);
        userPwd.setText("");

        //同时隐藏键盘
        if (keyboardUtil.isShow) {
            keyboardUtil.hideSystemKeyBoard();
            keyboardUtil.hideAllKeyBoard();
            keyboardUtil.hideKeyboardLayout();
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
        setContentView(R.layout.activity_guide);
        HandlerManager.registerHandler(HandlerManager.SHOWMERCHANT, handler);

        sp = this.getSharedPreferences("login", 0);

        btn_login = getViewById(R.id.btn_login);

        tvVersion = getViewById(R.id.tv_versionName);
        tvchangeCDN = getViewById(R.id.tv_network_setting);
        // 设置下划线
        //        tvVersion.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);

        //每次启动APP都会调用公钥交换接口
      /*  if(!TextUtils.isEmpty(AppHelper.getImei(MainApplication.getContext()))){
            ECDHKeyExchange();
        }*/
        tv_privacy_policy = getViewById(R.id.tv_privacy_policy);
        tv_privacy_policy.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); //下划线
        tv_privacy_policy.getPaint().setAntiAlias(true);//抗锯齿


        initView();

        boolean isDeviceRooted = false;
        if (IS_POS_VERSION) {
            isDeviceRooted = isDeviceRootedPos();
        } else {
            isDeviceRooted = isDeviceRootedAndroid();
        }

        if (isDeviceRooted) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Dialog_root = new RootDialog(WelcomeActivity.this, new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Dialog_root.dismiss();
                            for (Activity a : MainApplication.allActivities) {
                                a.finish();
                            }
                        }
                    });

                    if (Dialog_root != null && !Dialog_root.isShowing()) {
                        String message = getStringById(R.string.system_root_instruction);
                        Dialog_root.setMessage(message);
                        Dialog_root.setCanceledOnTouchOutside(false);
                        Dialog_root.show();
                    }
                }
            });
        }


        boolean hasAlerted = (boolean) getPrivacyFlag();
        if (!hasAlerted) {
            showPrivacyDialog();
        }


        login_skey = PreferenceUtil.getString("login_skey", "");
        login_sauthid = PreferenceUtil.getString("login_sauthid", "");

        //V3.0.5 CA 证书版本加入 --- 2020/04/02 Mia
        //加入CA证书校验的判断，是否过期
        //1：先读取本地证书
        CertificateUtils.getCertificateList(this);

        if (!StringUtil.isEmptyOrNull(login_skey) && !StringUtil.isEmptyOrNull(login_sauthid)
                && !StringUtil.isEmptyOrNull(MainApplication.isDefault)
                && !MainApplication.isDefault.equalsIgnoreCase("1")) {

            //先去调用公钥交换接口然后再调用设备登录接口,每次设备登录前都主动去调用ECDH去交换
            if(Build.VERSION.SDK_INT >= 29){//Android 10.0版本以及以上
                if (!TextUtils.isEmpty(AppHelper.getUUID())) {
                    //2：判断本地证书是否过期
                    if(CertificateUtils.checkCercateListValide()){//本地证书中只要有一个没有过期
                        //则先走正常的验证网络请求逻辑，检测证书是否有效
                        isNeedDeviceLogin = true;
                        ECDHKeyExchange();
                    }else {
                        //证书不在有效期内，则先请求CA证书公钥获取接口
                        requestCertificate(true);
                    }
                }
            }else {
                if (isGranted(Manifest.permission.READ_PHONE_STATE)) {
                    if (!TextUtils.isEmpty(AppHelper.getImei(MainApplication.getContext()))) {
                        //2：判断本地证书是否过期
                        if(CertificateUtils.checkCercateListValide()){//本地证书中只要有一个没有过期
                            //则先走正常的验证网络请求逻辑，检测证书是否有效
                            isNeedDeviceLogin = true;
                            ECDHKeyExchange();
                        }else {
                            //证书不在有效期内，则先请求CA证书公钥获取接口
                            requestCertificate(true);
                        }
                    }
                } else {
                    showMissingPermissionDialog(WelcomeActivity.this);
                }
            }
        } else {
            MainApplication.isUpdateShow = false;
            checkVersionisUpdate();
            //回话过期直接跳转到登录界面
            showLoginPage();
        }

        initVale();
        clearInstalledAPK();
        //设置监听
        setLister();
        keepAlive();

        // androidManifest.xml指定本activity最先启动
        // 因此，MTA的初始化工作需要在本onCreate中进行
        // 在startStatService之前调用StatConfig配置类接口，使得MTA配置及时生效
        // 2.3.0版本新增，参数为Application
        StatService.setContext(getApplicationContext());
        // 初始化并启动MTA
        // 第三方SDK必须按以下代码初始化MTA，其中appkey为规定的格式或MTA分配的代码。
        // 其它普通的app可自行选择是否调用
        try {
            // 第三个参数必须为：com.tencent.stat.common.StatConstants.VERSION
            StatService.startStatService(getApplicationContext(), BuildConfig.appKey,
                    com.tencent.stat.common.StatConstants.VERSION);
        } catch (MtaSDkException e) {
            // MTA初始化失败
        }
    }

    private void showLoginPage() {
        //回话过期直接跳转到登录界面
        ly_first.setVisibility(View.GONE);
        ly_title.setVisibility(View.VISIBLE);
        scrollView.setVisibility(View.VISIBLE);
    }

    private void ECDHKeyExchange() {
        try {
            ECDHUtils.getInstance().getAppPubKey();
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        final String publicKey = SharedPreUtile.readProduct("pubKey").toString();
        final String privateKey = SharedPreUtile.readProduct("priKey").toString();

        LocalAccountManager.getInstance().ECDHKeyExchange(publicKey, new UINotifyListener<ECDHInfo>() {
            @Override
            public void onError(Object object) {
                super.onError(object);
                if(!object.toString().contains("401")) {
                    //如果是设备登录请求的ECDH交换
                    if(isNeedDeviceLogin){
                        //修改此处逻辑---2020/04/09
                        //如果是设备登录证书校验失败,不管本地有没有公钥证书，则直接调招到一键重登界面
                        //只要是设备登录ECDH交换失败，不管是什么原因，证书校验失败也好，其他情况失败也好，都跳到一键登录
                        if(object.toString().contains(RequestResult.HANDSHAKE_CER_ERROR)){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //则跳转到一键重登页面
                                    dismissLoading();
                                    Intent it = new Intent();
                                    it.putExtra("isCerFailed",true);
                                    it.setClass(WelcomeActivity.this, ReLoginActivity.class);
                                    startActivity(it);
                                }
                            });
                        }else{
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //则跳转到一键重登页面
                                    dismissLoading();
                                    Intent it = new Intent();
                                    it.setClass(WelcomeActivity.this, ReLoginActivity.class);
                                    startActivity(it);
                                }
                            });
                        }

                    }else{ //如果是登录前请求的ECDH交换失败
                        //此处修改新逻辑----2020/04/09
                        //如果是证书校验失败,且不管本地没有公钥证书，则弹框提示,文案：请求失败，请重试
                        if(object.toString().contains(RequestResult.HANDSHAKE_CER_ERROR)){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //如果是登录前请求的ECDH，然后校验证书失败了，则弹框提示
                                    // 则下次点击，再登录前请求ECDH前，先请求一下公钥接口
                                    cerFailedRequestAgain = true;

                                    dismissLoading();
                                    showLoginPage();
                                    MainApplication.setNeedLogin(false);
                                    btn_login.setEnabled(true);
                                    btn_login.setText(R.string.login);

                                    toastDialog(WelcomeActivity.this, R.string.certificate_request_failed, null);
                                }
                            });
                        }else{
                            //如果错误不是证书校验失败，是ECDH交换失败的其他错误，则直接跳转到登录页，需要重新走登录流程
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    dismissLoading();
                                    showLoginPage();
                                    MainApplication.setNeedLogin(false);
                                    btn_login.setEnabled(true);
                                    btn_login.setText(R.string.login);
                                }
                            });
                        }
                    }
                } else{
                    //如果是会话已过期，则必须重新登录
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dismissLoading();
                            showLoginPage();
                            MainApplication.setNeedLogin(false);
                        }
                    });
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (isNeedRequestECDHKey) {
                    showLoading(false, getString(R.string.show_login_loading));
                }
            }

            @Override
            protected void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(ECDHInfo result) {
                super.onSucceed(result);
                if (result != null) {
                    try {
                        String secretKey = ECDHUtils.getInstance().ecdhGetShareKey(MainApplication.serPubKey, privateKey);
                        SharedPreUtile.saveObject(secretKey, "secretKey");

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //如果是登录失败报错之后再请求公钥交换接口，则此时直接调用登录
                                if (isNeedRequestECDHKey) {
                                    onLogin();
                                }

                                //如果是设备登录的时候发现没有skey调用公钥交换接口，则此时直接调用设备登录接口
                                if (isNeedDeviceLogin && !StringUtil.isEmptyOrNull(MainApplication.isDefault)
                                        && !MainApplication.isDefault.equalsIgnoreCase("1")) {
                                    deviceLogin();
                                }
                            }
                        });

                    } catch (Exception e) {
                        Log.e(TAG, Log.getStackTraceString(e));
                    }
                }
            }
        });
    }


    private void deviceLogin() {
        MainApplication.isUpdateShow = true;
        LocalAccountManager.getInstance().deviceLogin(new UINotifyListener<Boolean>() {
            @Override
            public void onError(final Object object) {
                // TODO Auto-generated method stub
                super.onError(object);
                dismissLoading();
                isNeedDeviceLogin = false;
                if (MainApplication.getContext().isNeedLogin()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //如果是会话已过期，则必须重新登录
                            showLoginPage();
                            MainApplication.setNeedLogin(false);
                            MainApplication.isUpdateShow = false;
                            checkVersionisUpdate();
                        }
                    });
                } else {
                    if (object != null) {
                        if(object.toString().contains("401")){
                            //如果是会话已过期，则必须重新登录
                            showLoginPage();
                            MainApplication.setNeedLogin(false);
                            MainApplication.isUpdateShow = false;
                            checkVersionisUpdate();
                        }else if(object.toString().contains(RequestResult.HANDSHAKE_CER_ERROR)){//证书校验失败
                            //理论上不会进入这个逻辑，以防万一
                            //因为只有ECDH交换成功才会进入设备登录，ECDH需要校验证书的合法，故进入此接口时，
                            //证书理论上不会校验失败，如果万一失败了，直接进入一键登录的页面
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //则跳转到一键重登页面
                                    Intent it = new Intent();
                                    it.putExtra("isCerFailed",true);
                                    it.setClass(WelcomeActivity.this, ReLoginActivity.class);
                                    startActivity(it);
                                }
                            });
                        } else{
                            toastDialog(WelcomeActivity.this, object.toString(), new NewDialogInfo.HandleBtn() {

                                @Override
                                public void handleOkBtn() {
                                    WelcomeActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            //则跳转到一键重登页面
                                            if(!object.toString().contains("401")){
                                                Intent it = new Intent();
                                                it.setClass(WelcomeActivity.this, ReLoginActivity.class);
                                                startActivity(it);
                                            }
                                        }
                                    });
                                }

                            });
                        }

                    }
                }
            }

            @Override
            public void onSucceed(Boolean result) {
                super.onSucceed(result);
                if (result) {
                    loginSuccToLoad(false);
                    isNeedDeviceLogin = false;
                }
            }
        });
    }

    private void keepAlive() {
        //JobScheduler 大于5.0才可以使用
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            JobSchedulerManager mJobManager = JobSchedulerManager.getJobSchedulerInstance(this);
            mJobManager.startJobScheduler();
        }
        Intent intent = new Intent(this, ForegroundLiveService.class);
        startService(intent);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            mContext.startForegroundService(intent);
        } else {
            mContext.startService(intent);
        }

        Intent liveService = new Intent(this, PlayerMusicService.class);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            mContext.startForegroundService(liveService);
        } else {
            mContext.startService(liveService);
        }

    }

    /**
     * 检查版本更新
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void checkVersionisUpdate() {
        if (NetworkUtils.isNetWorkValid(WelcomeActivity.this)) {
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            checkVersion();
                        }
                    });

                }

            };
            Timer timer = new Timer();
            timer.schedule(task, 200);
        } else {

            DialogInfo dialogInfo = new DialogInfo(mContext, getString(R.string.no_network), getString(R.string.to_open_network), getString(R.string.to_open), DialogInfo.NETWORKSTATUE, new HandleBtn() {

                @Override
                public void handleOkBtn() {
                    Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                    mContext.startActivity(intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                }

                @Override
                public void handleCancleBtn() {
                }
            }, null);
            DialogHelper.resize(this, dialogInfo);
            dialogInfo.show();
        }
    }



    public static void startActivity(Context context, String activeId, String activeName, String content) {

        Intent it = new Intent();
        //if (newTask)
        // {
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //}
        it.setClass(context, WelcomeActivity.class);
        it.putExtra("activeId", activeId);
        it.putExtra("activeName", activeName);
        it.putExtra("content", content);
        context.startActivity(it);

    }

    public static void startActivity(Context context) {

        Intent it = new Intent();
        //if (newTask)
        // {
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //}
        it.setClass(context, WelcomeActivity.class);
        //        it.putExtra("content", "OrderDetailsActivity");
        //isOrder = true;
        context.startActivity(it);

    }


    /**
     * base64转为bitmap
     *
     * @param base64Data
     * @return
     */
    public Bitmap base64ToBitmap(String base64Data) {
        byte[] bytes = Base64.decode(base64Data, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    public void refreshServerAddress(String serverString) { //点击登录按钮的时候，要重新根据当前选择的状态轮询一下取哪个网络地址
        if (serverString == null || TextUtils.isEmpty(serverString) || serverString.equals("")) {
            //要进行是否开启CDN的判断
            String CDN_status = PreferenceUtil.getString("CDN", "open");
            if (CDN_status.equals("open")) {
                ApiConstant.BASE_URL_PORT = MainApplication.config.getServerAddr();
            } else {
                ApiConstant.BASE_URL_PORT = MainApplication.config.getServerAddrBack();
            }
//            ApiConstant.BASE_URL_PORT = MainApplication.config.getServerAddr();
            ApiConstant.pushMoneyUrl = MainApplication.config.getPushMoneyUrl();
            PreferenceUtil.removeKey("serverCifg");
        } else {
            if (serverString.equals("test123")) {
                ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrTest123;
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest;
                PreferenceUtil.commitString("serverCifg", "test123");
            } else if (serverString.equals("test61")) {
                ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrTest61;
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest;
                PreferenceUtil.commitString("serverCifg", "test61");
            } else if (serverString.equals("test63")) {
                ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrTest63;
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest;
                PreferenceUtil.commitString("serverCifg", "test63");
            } else if (serverString.equals("testUAT")) {
                ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrTestUAT;
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest;
                PreferenceUtil.commitString("serverCifg", "testUAT");
            } else if (serverString.equals("prd")) {
                ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrPrd;
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlPrd;
                PreferenceUtil.commitString("serverCifg", "prd");
            } else if (serverString.equals("dev")) {
                ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrDev;
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest;
                PreferenceUtil.commitString("serverCifg", "dev");
            } else if (serverString.equals("dev-jh")) {
                ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrDev_JH;
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest;
                PreferenceUtil.commitString("serverCifg", "dev-jh");
            }
        }
    }

    int count = 0;

    void setServerAddress() {
        count = count + 1;
        if (count == 4) { //123测试环境
            ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrTest123;
            ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest;
            tv_server_name.setText("test123");
            tv_server_name.setVisibility(View.VISIBLE);
            PreferenceUtil.commitString("serverCifg", "test123");
        } else if (count == 7) { //61测试环境
            ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrTest61;
            ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest;
            tv_server_name.setText("test61");
            tv_server_name.setVisibility(View.VISIBLE);
            PreferenceUtil.commitString("serverCifg", "test61");
        } else if (count == 8) { //63测试环境
            ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrTest63;
            ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest;
            tv_server_name.setText("test63");
            tv_server_name.setVisibility(View.VISIBLE);
            PreferenceUtil.commitString("serverCifg", "test63");
        }else if (count == 9) { //UAT环境
            count = 0;
            ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrTestUAT;
            ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest;
            tv_server_name.setText("testUAT");
            tv_server_name.setVisibility(View.VISIBLE);
            PreferenceUtil.commitString("serverCifg", "testUAT");
        } else if (count == 5) {
            //灰度线环境
            ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrPrd;
            ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlPrd;
            tv_server_name.setText("prd");
            tv_server_name.setVisibility(View.VISIBLE);
            PreferenceUtil.commitString("serverCifg", "prd");
        } else if (count == 6) {

            ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrDev_JH;
            ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest;
            tv_server_name.setText("dev-jh");
            tv_server_name.setVisibility(View.VISIBLE);
            PreferenceUtil.commitString("serverCifg", "dev-jh");

        } else if (count == 3) {
            ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrDev;
            ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest;
            tv_server_name.setText("dev");
            tv_server_name.setVisibility(View.VISIBLE);
            PreferenceUtil.commitString("serverCifg", "dev");
        } else {
            tv_server_name.setText("");
            tv_server_name.setVisibility(View.GONE);

            //要进行是否开启CDN的判断
            String CDN_status = PreferenceUtil.getString("CDN", "open");
            if (CDN_status.equals("open")) {
                ApiConstant.BASE_URL_PORT = MainApplication.config.getServerAddr();
            } else {
                ApiConstant.BASE_URL_PORT = MainApplication.config.getServerAddrBack();
            }
//            ApiConstant.BASE_URL_PORT = MainApplication.config.getServerAddr();
            ApiConstant.pushMoneyUrl = MainApplication.config.getPushMoneyUrl();
            PreferenceUtil.removeKey("serverCifg");
        }
    }

    private void setLister() {
        iv_login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT >= 29){//Android 10.0版本以及以上
                    if(BuildConfig.FLAVOR.equalsIgnoreCase("u_test")){
                        setServerAddress();
                    }
                }else{
                    if (!StringUtil.isEmptyOrNull(ApiConstant.imie)) {

                        String arr[] = ApiConstant.imie.split("\\,");

                        String imie = AppHelper.getImei(WelcomeActivity.this);
                        for (int i = 0; i < arr.length; i++) {
                            if (arr[i].equalsIgnoreCase(imie)) {
                                setServerAddress();
                            }
                        }

                    }

                }
            }
        });


        iv_code.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                LocalAccountManager.getInstance().getCode(new UINotifyListener<String>() {
                    @Override
                    public void onError(Object object) {
                        // TODO Auto-generated method stub
                        super.onError(object);
                        WelcomeActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                tv_load.setText(R.string.tv_code_refresh);
                                tv_load.setVisibility(View.GONE);
                            }
                        });
                    }

                    @Override
                    public void onPreExecute() {
                        super.onPostExecute();
                        WelcomeActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                tv_load.setText(R.string.public_loading);
                                tv_load.setVisibility(View.VISIBLE);
                            }
                        });
                    }

                    @Override
                    public void onSucceed(String result) {
                        super.onSucceed(result);
                        if (null != result) {
                            tv_load.setVisibility(View.GONE);
                            iv_code.setVisibility(View.VISIBLE);
                            Bitmap bitmap = base64ToBitmap(result);
                            if (null != bitmap) {
                                iv_code.setImageBitmap(bitmap);
                            }
                        }
                    }
                });
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {//osMSkt5tgFUcWsbxFuPhUhLOQBPA
                // oCnu4uGVvVFaGrCEAY2eWVZj5ibE
                //                sendWxRedpack("osMSkt5tgFUcWsbxFuPhUhLOQBPA");

               /* if (!StringUtil.isEmptyOrNull(ApiConstant.imie)) {

                    String arr[] = ApiConstant.imie.split("\\,");

                    String imie = AppHelper.getImei(WelcomeActivity.this);
                    Log.i("hehui", "imie-->" + imie);
                    for (int i = 0; i < arr.length; i++) {
                        if (arr[i].equalsIgnoreCase(imie)) {
                            setServerAddress();
                        }
                    }

                }
                Log.i("hehui", "serverAdd-->" + ApiConstant.BASE_URL_PORT );*/
                try {
                    StatService.trackCustomEvent(WelcomeActivity.this, "kMTASPayLogin", "登录界面 登录按钮");
                } catch (Exception e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }

                // 1.携带参数的打点
                Bundle values = new Bundle();
                values.putString("kGFASPayLogin","登录界面 登录按钮");
                DataReportUtils.getInstance().report("kGFASPayLogin",values);

                //先收起键盘
                if (keyboardUtil.isShow) {
                    keyboardUtil.hideSystemKeyBoard();
                    keyboardUtil.hideAllKeyBoard();
                    keyboardUtil.hideKeyboardLayout();
                }

                refreshServerAddress(tv_server_name.getText().toString());
                //如果点击登录按钮的时候缓存中还没有skey和serPubKey,则先调用公钥接口
                if (MainApplication.skey != null && !TextUtils.isEmpty(MainApplication.skey)
                        && MainApplication.serPubKey != null && !TextUtils.isEmpty(MainApplication.serPubKey)) {
                    //如果是之前登录过，重登陆的时候，发现证书失效，则先请求公钥接口，再走后续流程
                    if(cerFailedRequestAgain){
                        requestCertificate(false);
                    }else {
                        onLogin();
                    }
                } else {

                    String name = user_name.getText().toString().trim();
                    String pwd = userPwd.getText().toString().trim();

                    if ("".equals(name)) {
                        //            showToastInfo(getString(R.string.show_user_name));
                        toastDialog(WelcomeActivity.this, R.string.show_user_name, null);
                        user_name.setFocusable(true);
                        return;
                    }

                    if ("".equals(pwd)) {
                        toastDialog(WelcomeActivity.this, R.string.pay_login_pwd, null);//(getString(R.string.pay_refund_pwd));
                        userPwd.setFocusable(true);
                        return;
                    }

                    if (isInputCode && StringUtil.isEmptyOrNull(ed_code.getText().toString())) {
                        toastDialog(WelcomeActivity.this, R.string.tx_ver_code, null);
                        ed_code.setFocusable(true);
                        return;
                    }

                    //1：先读取本地证书
                    CertificateUtils.getCertificateList(WelcomeActivity.this);
                    //2：判断本地证书是否过期
                    if(CertificateUtils.checkCercateListValide()){//本地证书中只要有一个没有过期
                        //则先走正常的验证网络请求逻辑，检测证书是否有效
                        if(cerFailedRequestAgain){
                            requestCertificate(false);
                        }else{
                            isNeedRequestECDHKey = true;
                            ECDHKeyExchange();
                        }
                    }else {
                        //证书不在有效期内，则先请求CA证书公钥获取接口
                        requestCertificate(false);
                    }
                }
            }
        });

        tvVersion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                refreshServerAddress(tv_server_name.getText().toString());
                showPage(FindPassFirstActivity.class);
                //                MainApplication.updatePwdActivities.add(WelcomeActivity.this);
            }
        });

        tvchangeCDN.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showPage(SettingCDNActivity.class);
            }
        });

        tv_privacy_policy.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // 跳转到隐私页面
                ContentTextActivity.startActivity(WelcomeActivity.this, "https://www.aub.com.ph/privacyPolicy/", null);
            }
        });
    }

    /***
     * 初始化
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initVale() {
        String userNmae = sp.getString("user_name", "");

        user_name.setText(userNmae);
    }

    public boolean flag = true;
    /**
     * 判断是否需要检测，防止不停的弹框
     */
    private boolean isNeedCheck = true;

    private static final int PERMISSON_REQUESTCODE = 0;
    // 所需的全部权限
    static final String[] PERMISSIONS = new String[]{Manifest.permission.MODIFY_AUDIO_SETTINGS, Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CAMERA};

    static final String[] PERMISSIONS_ANDROID10 = new String[]{Manifest.permission.MODIFY_AUDIO_SETTINGS, Manifest.permission.CAMERA};

    static final String[] PERMISSIONS_CAMERA = new String[]{Manifest.permission.CAMERA};

    /**
     * 检查是否 有权限
     *
     * @param permission
     * @return
     */
    private boolean lacksPermission(String permission) {
        return ContextCompat.checkSelfPermission(WelcomeActivity.this, permission) == PackageManager.PERMISSION_DENIED;
    }

    public boolean lacksPermissions(String... permissions) {
        for (String permission : permissions) {
            if (lacksPermission(permission)) {
                return true;
            }
        }
        return false;
    }


    /**
     * 申请权限结果的回调方法
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] paramArrayOfInt) {
        if (requestCode == PERMISSON_REQUESTCODE) {
            if (!verifyPermissions(paramArrayOfInt)) {
                //showMissingPermissionDialog(WelcomeActivity.this);
                isNeedCheck = false;
            }
        }
    }

    private DialogShowInfo dialogInfo;

    /**
     * 显示提示信息
     *
     * @since 2.5.0
     */
    protected void showMissingPermissionDialog(Context context) {
        dialogInfo = new DialogShowInfo(context, null, getString(R.string.setting_permisson_phone), getStringById(R.string.title_setting), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogShowInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                startAppSettings();
            }

            @Override
            public void handleCancleBtn() {
                dialogInfo.dismiss();
                showLoginPage();
            }
        }, null);
        dialogInfo.setOnKeyListener(new DialogInterface.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                if (keycode == KeyEvent.KEYCODE_BACK) {
                    return true;
                }
                startAppSettings();
                return false;
            }
        });
        DialogHelper.resize(context, dialogInfo);
        dialogInfo.show();
    }


    /**
     * 初始化
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initView() {
        iv_login = getViewById(R.id.iv_login);
        tv_server_name = getViewById(R.id.tv_server_name);
        rootView = findViewById(R.id.rootView);
        scrollView = findViewById(R.id.sv_main);

        String serverCifg = PreferenceUtil.getString("serverCifg", "");
        if (!StringUtil.isEmptyOrNull(serverCifg)) {
            if (serverCifg.equals("test123")) {
                tv_server_name.setText("test123");
                tv_server_name.setVisibility(View.VISIBLE);
            } else if (serverCifg.equals("test61")) {
                tv_server_name.setText("test61");
                tv_server_name.setVisibility(View.VISIBLE);
            } else if (serverCifg.equals("test63")) {
                tv_server_name.setText("test63");
                tv_server_name.setVisibility(View.VISIBLE);
            } else if (serverCifg.equals("testUAT")) {
                tv_server_name.setText("testUAT");
                tv_server_name.setVisibility(View.VISIBLE);
            } else if (serverCifg.equals("prd")) {
                tv_server_name.setText("prd");
                tv_server_name.setVisibility(View.VISIBLE);
            } else if (serverCifg.equals("dev")) {
                tv_server_name.setText("dev");
                tv_server_name.setVisibility(View.VISIBLE);
            } else if (serverCifg.equals("dev-jh")) {
                tv_server_name.setText("dev-jh");
                tv_server_name.setVisibility(View.VISIBLE);
            } else {
                tv_server_name.setText("");
                tv_server_name.setVisibility(View.GONE);
            }
        }

        ly_first = getViewById(R.id.ly_first);
        ly_title = getViewById(R.id.ly_title);
        ed_code = getViewById(R.id.ed_code);
        //定义hint的值
        SpannableString ss = new SpannableString(getResources().getString(R.string.bt_code_not_null));
        AbsoluteSizeSpan ass = new AbsoluteSizeSpan(14, true);
        ss.setSpan(ass, 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ed_code.setHint(new SpannedString(ss));
        ly_code = getViewById(R.id.ly_code);
        v_code = getViewById(R.id.v_code);
        tv_load = getViewById(R.id.tv_load);
        //        viewPager = (ViewPager)findViewById(R.id.vp);
        iv_code = getViewById(R.id.iv_code);

        //login_remember = getViewById(R.id.login_remember);

        imageResId = new int[]{R.drawable.n_icon_login_01, R.drawable.n_icon_login_02};

        imageViews = new ArrayList<ImageView>();


        user_name = getViewById(R.id.user_name);

        userPwd = getViewById(R.id.userPwd);
        userPwd.setTypeface(user_name.getTypeface());

        iv_clearUser = getViewById(R.id.iv_clearUser);

        iv_clearPwd = getViewById(R.id.iv_clearPwd);

        iv_clearUser.setOnClickListener(clearImage);
        iv_clearPwd.setOnClickListener(clearImage);

//        et_input_url = getViewById(R.id.et_input_url);

        ll_bdo_ekyc = getViewById(R.id.ll_bdo_ekyc);
        btn_sign_up = getViewById(R.id.btn_sign_up);
        btn_sign_up.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // 跳转到sign up 页面
//                 String url = "file:///android_asset/index.html";
//                String url = "http://192.168.1.65:9226/#/";
                String url = "http://192.168.1.65:9226/#/";
             /*   if( !TextUtils.isEmpty(et_input_url.getText().toString().trim())){
                    url = et_input_url.getText().toString().trim();
                }*/
                Intent intent = new Intent();
                intent.putExtra("loadUrl",url);
                intent.setClass(WelcomeActivity.this, MyWebViewActicity.class);
                startActivity(intent);

               //调用自定义相机的代码----测试用，可以移植到任何地方
//                gotoCamera(2,1);

                //调用相机的代码----测试用，可以移植到任何地方
               /* String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                photoName = "camera" + timeStamp;
                photoUri = ImageUtil.openSystemCamera(WelcomeActivity.this ,photoName);*/
            }
        });

        tv_check_ekyc_state = getViewById(R.id.tv_check_ekyc_state);
        tv_check_ekyc_state.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // 跳转到check_ekyc_state页面
//                String url = "http://192.168.1.65:9226/#/check-mid-process";
//                String url = "http://192.168.154.227:8080/#/check-mid-process";
                String url = "http://192.168.1.65:9226/#/check-mid-process";
            /*    if( !TextUtils.isEmpty(et_input_url.getText().toString().trim())){
                    url = et_input_url.getText().toString().trim();
                }*/
                Intent intent = new Intent();
                intent.putExtra("loadUrl",url);
                intent.setClass(WelcomeActivity.this, MyWebViewActicity.class);
                startActivity(intent);
            }
        });

        //如果是BDO下来的按钮才展示，否则不展示
        if(ApiConstant.bankCode.contains("bdo")){
            ll_bdo_ekyc.setVisibility(View.VISIBLE);
            btn_sign_up.setVisibility(View.VISIBLE);
            tv_check_ekyc_state.setVisibility(View.VISIBLE);
        }else{
            ll_bdo_ekyc.setVisibility(View.GONE);
            btn_sign_up.setVisibility(View.GONE);
            tv_check_ekyc_state.setVisibility(View.GONE);
        }

        user_name.setOnFocusChangeListener(listener);

        userPwd.setOnFocusChangeListener(listener);

        ed_code.setOnFocusChangeListener(listener);

        EditTextWatcher editTextWatcher = new EditTextWatcher();

        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {

                if (user_name.isFocused()) {
                    //                    userPwd.setCursorVisible(false);
                    user_name.setCursorVisible(true);
                    if (user_name.getText().toString().length() > 0) {
                        iv_clearUser.setVisibility(View.VISIBLE);
                    } else {
                        iv_clearPwd.setVisibility(View.GONE);

                    }
                } else if (userPwd.isFocused()) {
                    userPwd.setCursorVisible(true);
                    if (userPwd.getText().toString().length() > 0) {
                        iv_clearPwd.setVisibility(View.VISIBLE);
                    } else {

                        iv_clearUser.setVisibility(View.GONE);

                    }
                } /*else if (shop_id.isFocused()) {

                    iv_clearUser.setVisibility(View.GONE);
                    iv_clearPwd.setVisibility(View.GONE);

                }*/
                if (isInputCode) {
                    if (!StringUtil.isEmptyOrNull(user_name.getText().toString()) && !StringUtil.isEmptyOrNull(userPwd.getText().toString()) && !StringUtil.isEmptyOrNull(ed_code.getText().toString())) {
                        setButtonBg(btn_login, true, R.string.login);
                    }

                } else {

                    if (!StringUtil.isEmptyOrNull(user_name.getText().toString()) && !StringUtil.isEmptyOrNull(userPwd.getText().toString())) {
                        setButtonBg(btn_login, true, R.string.login);
                    }
                }

            }

        });

        user_name.addTextChangedListener(editTextWatcher);
        userPwd.addTextChangedListener(editTextWatcher);
        initMoveKeyBoard();
    }

    //调用自定义的camera，可以移植到任何地方
    public void gotoCamera(int apiCode,int camereType){
        //点击前先检查camera权限
        if (Build.VERSION.SDK_INT >= 23 ) {
            checkPermissions(PERMISSIONS_CAMERA);
        }

        //授权了camera权限才能跳转
        if(isGranted(Manifest.permission.CAMERA)){
            CameraActivity.startCamera(WelcomeActivity.this, REQUESTCODE_CAMERA_TAKE_PICTURE,apiCode,camereType);
        }
    }

    //调用自定义相机的回调代码----测试用，可以移植到任何地方
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUESTCODE_CAMERA_TAKE_PICTURE){
            if (data != null) {
                String path = data.getStringExtra(KEY_IMAGE_PATH);
                Uri photoUri =  Uri.parse(path);
                if(photoUri != null){
                    //若生成了uri，则表示该文件添加成功
                    //使用流将该uri中的内容写入字节数组即可
                    byte[] image;
                    try{
                        InputStream inputStream = getContentResolver().openInputStream(photoUri);
                        if (null == inputStream || 0 == inputStream.available()) {
                            return;
                        }
                        image = new byte[inputStream.available()];
                        inputStream.read(image);
                        inputStream.close();

                    }catch (IOException e){

                    }
                }
            }
        }else if(resultCode == Activity.RESULT_FIRST_USER && requestCode == REQUESTCODE_CAMERA_TAKE_PICTURE){
            showToastInfo(getStringById(R.string.instapay_save_failed));
        }

    }



    //调用相机的回调代码----测试用，可以移植到任何地方
   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        //如果是拍照返回的结果
        //如果调用相机用的这种方法指定Uri intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        //onActivityResult 方法中的data 返回为空（数据表明，93%的机型的data
        //将会是Null，所以如果我们指定了路径(Uri)，就不要使用data 来获取照片，起码在使用前要做空判断）
        //如果不指定图片的uri，则可能会返回data(照相机有自己默认的存储路径，拍摄的照片将返回一个缩略图。
        //可以通过data.getParcelableExtra("data")获得图片)
        //使用 onActivityResult 中的 intent(data)前要做空判断。！！！很重要
        if (resultCode == Activity.RESULT_OK && requestCode == ImageUtil.REQUESTCODE_TAKE_PICTURE) {

             if (photoUri != null) {
                //若生成了uri，则表示该文件添加成功
                //使用流将该uri中的内容写入字节数组即可
               byte[] image;
               try{
                   InputStream inputStream = getContentResolver().openInputStream(photoUri);
                   if (null == inputStream || 0 == inputStream.available()) {
                       return;
                   }
                   image = new byte[inputStream.available()];
                   inputStream.read(image);
                   inputStream.close();

               }catch (IOException e){

               }

                sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, photoUri));

            }

        }else {
            if(photoUri != null){
                //如果是Android 10.0 点击确定之后，又重试不保存之前确定的图，故应该先查询，如果存在就删除
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
                    //因为insert 如果uri存在的话，会返回null 所以需要先delete
                    String status = Environment.getExternalStorageState();
                    // 判断是否有SD卡,优先使用SD卡存储,当没有SD卡时使用手机存储
                    Uri uri ;
                    if (status.equals(Environment.MEDIA_MOUNTED)) {
                        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else {
                        uri =MediaStore.Images.Media.INTERNAL_CONTENT_URI;
                    }
                    Cursor query = getContentResolver().query(uri,
                            new String[]{MediaStore.Images.Media.DISPLAY_NAME},
                            MediaStore.Images.Media.DISPLAY_NAME + "=?",
                            new String[]{photoName+".jpg"},
                            null
                    );
                    if (query != null && query.getCount() > 0) {
                        getContentResolver().delete(uri,
                                MediaStore.Images.Media.DISPLAY_NAME + "=?",
                                new String[]{photoName+".jpg"});

                    }
                }

            };
        }
    }
*/
    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b, enable, res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_press_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }

    private OnClickListener clearImage = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.iv_clearUser:
                    iv_clearUser.setVisibility(View.GONE);
                    user_name.setText("");
                    break;
                case R.id.iv_clearPwd:
                    iv_clearPwd.setVisibility(View.GONE);
                    userPwd.setText("");
                    break;
                default:
                    break;
            }
        }
    };

    private final OnFocusChangeListener listener = new OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            switch (v.getId()) {
                case R.id.user_name:
                    if (hasFocus) {
                        //                        user_name.setCursorVisible(true);
                        if (user_name.getText().length() > 0) {
                            iv_clearUser.setVisibility(View.VISIBLE);
                        } else {
                            iv_clearUser.setVisibility(View.GONE);
                        }
                    } else {
                        iv_clearUser.setVisibility(View.GONE);
                    }
                    break;
                case R.id.userPwd:
                    //                    sp.edit().putString("userPwd", userPwd.getText().toString()).commit();

                    if (hasFocus) {
                        userPwd.setCursorVisible(true);
                        if (userPwd.getText().length() > 0) {
                            iv_clearPwd.setVisibility(View.VISIBLE);
                        } else {
                            iv_clearPwd.setVisibility(View.GONE);
                        }
                    } else {
                        iv_clearPwd.setVisibility(View.GONE);
                    }

                    break;
            }

        }
    };

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    protected boolean isLoginRequired() {
        return false;
    }

    private void startLogin() {
        if (LocalAccountManager.getInstance().isLoggedIn()) {
            Intent it = new Intent();
            it.setClass(WelcomeActivity.this, spayMainTabActivity.class);
            startActivity(it);

            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            finish();
        }
    }


    /***
     * 登录操作
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    public void onLogin() {

        //        Intent it = new Intent();
        //        it.setClass(WelcomeActivity.this, LoginActivity.class);
        //        startActivity(it);
        //        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        String name = user_name.getText().toString().trim();
        String pwd = userPwd.getText().toString().trim();

        if ("".equals(name)) {
            //            showToastInfo(getString(R.string.show_user_name));
            toastDialog(WelcomeActivity.this, R.string.show_user_name, null);
            user_name.setFocusable(true);
            return;
        }

        if ("".equals(pwd)) {
            toastDialog(WelcomeActivity.this, R.string.pay_login_pwd, null);//(getString(R.string.pay_refund_pwd));
            userPwd.setFocusable(true);
            return;
        }

        if (isInputCode && StringUtil.isEmptyOrNull(ed_code.getText().toString())) {
            toastDialog(WelcomeActivity.this, R.string.tx_ver_code, null);
            ed_code.setFocusable(true);
            return;
        }
        if (NetworkUtils.isNetWorkValid(WelcomeActivity.this)) {
            StatService.trackCustomEvent(this, "SPConstTapLoginButton", "登录");
            login(name, pwd);
        } else {
            showToastInfo(getString(R.string.show_no_network));
        }
    }

    //APP清掉本地缓存的预授权和消费的切换
    public void ClearAPPState() {
        PreferenceUtil.removeKey("choose_pay_or_pre_auth");
        PreferenceUtil.removeKey("bill_list_choose_pay_or_pre_auth");

    }

    /**
     * 登录
     * <功能详细描述>
     *
     * @param username
     * @param password
     * @see [类、类#方法、类#成员]
     */
    private void login(String username, final String password) {
        LocalAccountManager.getInstance().loginAsync(ed_code.getText().toString(),null ,username, password,  false, new UINotifyListener<Boolean>() {
            @Override 
            public void onPreExecute() {
                super.onPreExecute();
                btn_login.setEnabled(false);
                btn_login.setText(R.string.bt_login_loading);
                showLoading(false, getString(R.string.show_login_loading));
                ClearAPPState();
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (object != null) {
                            if (object.toString().startsWith("405")) {//Require to renegotiate ECDH key
                                isNeedRequestECDHKey = true;
                                ECDHKeyExchange();
                            }else if(object.toString().contains(RequestResult.HANDSHAKE_CER_ERROR)){//证书校验失败
                               cerFailedRequestAgain = true;
                              //登录如果证书校验失败，则直接弹框提示
                              dismissLoading();
                              toastDialog(WelcomeActivity.this,getStringById(R.string.certificate_request_failed),null);

                              btn_login.setEnabled(true);
                              btn_login.setText(R.string.login);

                            } else {
                                try {
                                    dismissLoading();
                                    btn_login.setEnabled(true);
                                    btn_login.setText(R.string.login);

                                    final ErrorMsg msg = (ErrorMsg) object;
                                    if (!StringUtil.isEmptyOrNull(msg.getMessage())) {
                                        toastDialog(WelcomeActivity.this, msg.getMessage(), new NewDialogInfo.HandleBtn() {

                                            @Override
                                            public void handleOkBtn() {
                                                if (!StringUtil.isEmptyOrNull(msg.getContent())) {
                                                    Bitmap bitmap = base64ToBitmap(msg.getContent());
                                                    if (null != bitmap) {
                                                        iv_code.setVisibility(View.VISIBLE);
                                                        tv_load.setVisibility(View.GONE);
                                                        iv_code.setImageBitmap(bitmap);
                                                    }
                                                }
                                            }

                                        });
                                    }
                                } catch (Exception e) {
                                    dismissLoading();
                                    btn_login.setEnabled(true);
                                    btn_login.setText(R.string.login);

                                    if (object.toString().startsWith("403")) {//有验证码
                                        isInputCode = true;
                                        ly_code.setVisibility(View.VISIBLE);
                                        v_code.setVisibility(View.VISIBLE);
                                        String code = object.toString().substring(3);
                                        if (!StringUtil.isEmptyOrNull(code)) {

                                            Bitmap bitmap = base64ToBitmap(code);
                                            if (null != bitmap) {
                                                iv_code.setVisibility(View.VISIBLE);
                                                tv_load.setVisibility(View.GONE);
                                                iv_code.setImageBitmap(bitmap);
                                            }
                                        }
                                    } else {
                                        toastDialog(WelcomeActivity.this, object.toString(), null);
                                    }
                                }
                            }
                        }
                    }
                });
            }

            @Override
            public void onSucceed(Boolean object) {
                super.onSucceed(object);
                if (object) {
                    dismissLoading();
                    sp.edit().putString("user_name", user_name.getText().toString()).commit();
                    //如果是默认密码登录的，则弹框提示，强制要求修改密码
                    final String oldpsw = password;
                    if (!StringUtil.isEmptyOrNull(MainApplication.isDefault)
                            && MainApplication.isDefault.equalsIgnoreCase("1")) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (ChangePswNoticeDialog == null) {
                                    ChangePswNoticeDialog = new CashierNoticeDialog(WelcomeActivity.this);
                                    ChangePswNoticeDialog.setContentText(R.string.change_psw_instruction);
                                    ChangePswNoticeDialog.setConfirmText(R.string.bt_dialog_ok);
                                    ChangePswNoticeDialog.setCanceledOnTouchOutside(false);
                                    //设置点击返回键不消失
                                    ChangePswNoticeDialog.setCancelable(false);
                                    ChangePswNoticeDialog.setCurrentListener(new CashierNoticeDialog.ButtonConfirmCallBack() {
                                        @Override
                                        public void onClickConfirmCallBack() {
                                            if (ChangePswNoticeDialog != null && ChangePswNoticeDialog.isShowing()) {
                                                ChangePswNoticeDialog.dismiss();
                                                //跳转到修改密码的页面
                                                ChangePswLoginActivity.startActivity(mContext, oldpsw);
                                            }
                                        }
                                    });
                                }
                                if (!ChangePswNoticeDialog.isShowing()) {
                                    ChangePswNoticeDialog.show();
                                }
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                MainApplication.isSessionOutTime = false;
                                isNeedRequestECDHKey = false;
                                loginSuccToLoad(true);
                            }
                        });
                    }
                }
            }

        });

    }

    void loginSuccToLoad(boolean isLoad) {
        //        //加载保存登录用户信息
        sp.edit().putString("user_name", user_name.getText().toString()).commit();
        ////        sp.edit().putString("userPwd", "").commit();
        //        if (login_remember.isChecked())
        //        {
        //            String pwd;
        //            try
        //            {
        //                pwd = AESHelper.encrypt("SPAY", userPwd.getText().toString());
        //                sp.edit().putString("userPwd", pwd).commit();
        //            }
        //            catch (Exception e)
        //            {
        //                // TODO Auto-generated catch block
        //                sp.edit().putString("userPwd", userPwd.getText().toString()).commit();
        //                Log.e(TAG,Log.getStackTraceString(e));
        //            }
        //        }

        //sp.edit().putString("shop_id", shop_id.getText().toString()).commit();
        MainApplication.userName = user_name.getText().toString();

        //        String dynModelMd5 = PreferenceUtil.getString("dynModelMd5", "");
        //        if (!StringUtil.isEmptyOrNull(dynModelMd5))
        //        {
        //            if (!StringUtil.isEmptyOrNull(MainApplication.themeMd5)
        //                && MainApplication.themeMd5.equalsIgnoreCase(dynModelMd5))
        //            {
        //                todo();
        //            }
        //            else
        //            {
        //                getDyn(isLoad);
        //            }
        //        }
        //        else
        //        {
        //            //加载样式
        //            getDyn(isLoad);
        //        }

        //                        getDyn();


        //主动做一次蓝牙连接
        try {
            connentBlue();
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }

        loadPayType();
        //getExchangeRate();
        todo();
    }


    /**
     * 过滤支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */

    private void filterListStream(List<DynModel> list, List<DynModel> filterLst) {

        for (DynModel dynModel : list) {
            //if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                for (String key : MainApplication.apiProviderMap.keySet()) {
                    if (key.equals(dynModel.getApiCode())) {
                        filterLst.add(dynModel);
                        break;
                    }

                }
           // }
        }
    }

    /**
     * 过滤支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void filterList(List<DynModel> list, List<DynModel> filterLst, Map<String, String> payTypeMap) {

        if (!StringUtil.isEmptyOrNull(MainApplication.serviceType)) {
            String[] arrPays = MainApplication.serviceType.split("\\|");

            for (DynModel dynModel : list) {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                    for (String key : MainApplication.apiProviderMap.keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if (dynModel.getNativeTradeType().contains(",")) {
                                    String[] arrNativeTradeType = dynModel.getNativeTradeType().split(",");
                                    for (int j = 0; j < arrNativeTradeType.length; j++) {
                                        if (arrNativeTradeType[j].equals(arrPays[i])) {
                                         /*   filterLst.add(dynModel);
                                            payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);*/
                                            //如果登录接口返回的dynModel里面的nativeTradeType里有多种支付类型，但是如果是已开通的支付方式里
                                            //有两条一模一样的数据就会有问题，就会匹配到两条数据，看是app做去重还是server做去重。
                                            //如果后台没办法做到去重的逻辑，需要前端APP做的话，就用下面的一段代码
                                            if(payTypeMap.get(dynModel.getApiCode()) != null &&
                                                    payTypeMap.get(dynModel.getApiCode()).equals(arrPays[i])){
                                            }else{
                                                filterLst.add(dynModel);
                                                payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);
                                            }
                                            break;
                                        }
                                    }
                                } else if (arrPays[i].equals(dynModel.getNativeTradeType())) {

                                    //这个地方的逻辑是有点问题，以后有空再改
                                    //如果登录接口返回的dynModel里面的nativeTradeType里只有一种支付类型，但是如果是已开通的支付方式里
                                    //有两条一模一样的数据就会有问题，只匹配到一条数据，然后就直接break跳出循环了，这个时候就做了去重处理，只展示一条
                                    filterLst.add(dynModel);
                                    payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    break;
                                }
                                /*if (arrPays[i].startsWith(dynModel.getNativeTradeType()) || dynModel.getNativeTradeType().contains(arrPays[i]) || arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    filterLst.add(dynModel);
                                    if (dynModel.getNativeTradeType().contains(",")) {
                                        payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType().substring(0, dynModel.getNativeTradeType().lastIndexOf(",")));
                                    } else {
                                        payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    }
                                    break;
                                }*/
                            }

                        }
                    }

                } else {
                    continue;
                }
            }
        }

    }


    /**
     * 过滤支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void filterList_active(List<DynModel> list, List<DynModel> filterLst, Map<String, String> payTypeMap) {

        if (!StringUtil.isEmptyOrNull(MainApplication.activateServiceType)) {
            String[] arrPays = MainApplication.activateServiceType.split("\\|");

            for (DynModel dynModel : list) {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                    for (String key : MainApplication.apiProviderMap.keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if (dynModel.getNativeTradeType().contains(",")) {
                                    String[] arrNativeTradeType = dynModel.getNativeTradeType().split(",");
                                    for (int j = 0; j < arrNativeTradeType.length; j++) {
                                        if (arrNativeTradeType[j].equals(arrPays[i])) {
                                         /*   filterLst.add(dynModel);
                                            payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);*/

                                            //如果登录接口返回的dynModel里面的nativeTradeType里有多种支付类型，但是如果是已开通的支付方式里
                                            //有两条一模一样的数据就会有问题，就会匹配到两条数据，看是app做去重还是server做去重。
                                            //如果后台没办法做到去重的逻辑，需要前端APP做的话，就用下面的一段代码
                                            if(payTypeMap.get(dynModel.getApiCode()) != null &&
                                                    payTypeMap.get(dynModel.getApiCode()).equals(arrPays[i])){
                                            }else{
                                                filterLst.add(dynModel);
                                                payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);
                                            }
                                            break;
                                        }
                                    }
                                } else if (arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    //这个地方的逻辑是有点问题，以后有空再改
                                    //如果登录接口返回的dynModel里面的nativeTradeType里只有一种支付类型，但是如果是已开通的支付方式里
                                    //有两条一模一样的数据就会有问题，只匹配到一条数据，然后就直接break跳出循环了，这个时候就做了去重处理，只展示一条
                                    filterLst.add(dynModel);
                                    payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    break;
                                }
                                /*if (arrPays[i].startsWith(dynModel.getNativeTradeType()) || dynModel.getNativeTradeType().contains(arrPays[i]) || arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    filterLst.add(dynModel);
                                    if (dynModel.getNativeTradeType().contains(",")) {
                                        payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType().substring(0, dynModel.getNativeTradeType().lastIndexOf(",")));
                                    } else {
                                        payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    }
                                    break;
                                }*/
                            }

                        }
                    }

                } else {
                    continue;
                }
            }
        }

    }

    //过滤卡通道类型
    public void filterCardPaymentList(List<DynModel> modelList, List<DynModel> mFilterCardPaymentList, Map<String, String> mCardPaymentPayTypeMap){
        //全部的卡交易通道
        if (!StringUtil.isEmptyOrNull(MainApplication.cardServiceType)) {
            String[] arrPays = MainApplication.cardServiceType.split("\\|");
            for (DynModel dynModel : modelList) {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                    for (String key : MainApplication.apiProviderMap.keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if (dynModel.getNativeTradeType().contains(",")) {
                                    String[] arrNativeTradeType = dynModel.getNativeTradeType().split(",");
                                    for (int j = 0; j < arrNativeTradeType.length; j++) {
                                        if (arrNativeTradeType[j].equals(arrPays[i])) {
                                         /*   filterLst.add(dynModel);
                                            payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);*/

                                            //如果登录接口返回的dynModel里面的nativeTradeType里有多种支付类型，但是如果是已开通的支付方式里
                                            //有两条一模一样的数据就会有问题，就会匹配到两条数据，看是app做去重还是server做去重。
                                            //如果后台没办法做到去重的逻辑，需要前端APP做的话，就用下面的一段代码
                                            if(mCardPaymentPayTypeMap.get(dynModel.getApiCode()) != null &&
                                                    mCardPaymentPayTypeMap.get(dynModel.getApiCode()).equals(arrPays[i])){
                                            }else{
                                                mFilterCardPaymentList.add(dynModel);
                                                mCardPaymentPayTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);
                                            }
                                            break;
                                        }
                                    }
                                } else if (arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    //这个地方的逻辑是有点问题，以后有空再改
                                    //如果登录接口返回的dynModel里面的nativeTradeType里只有一种支付类型，但是如果是已开通的支付方式里
                                    //有两条一模一样的数据就会有问题，只匹配到一条数据，然后就直接break跳出循环了，这个时候就做了去重处理，只展示一条
                                    mFilterCardPaymentList.add(dynModel);
                                    mCardPaymentPayTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    break;
                                }
                            }
                        }
                    }

                } else {
                    continue;
                }
            }
        }
    }


    //过滤已开通的卡通道类型
    public void filterActiveCardPaymentList(List<DynModel> modelList, List<DynModel> mFilterCardPaymentList_active, Map<String, String> mCardPaymentPayTypeMap_active){
        //全部已开通的卡交易通道
        if (!StringUtil.isEmptyOrNull(MainApplication.activateCardServiceType)) {
            String[] arrPays = MainApplication.activateCardServiceType.split("\\|");
            for (DynModel dynModel : modelList) {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                    for (String key : MainApplication.apiProviderMap.keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if (dynModel.getNativeTradeType().contains(",")) {
                                    String[] arrNativeTradeType = dynModel.getNativeTradeType().split(",");
                                    for (int j = 0; j < arrNativeTradeType.length; j++) {
                                        if (arrNativeTradeType[j].equals(arrPays[i])) {
                                         /*   filterLst.add(dynModel);
                                            payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);*/

                                            //如果登录接口返回的dynModel里面的nativeTradeType里有多种支付类型，但是如果是已开通的支付方式里
                                            //有两条一模一样的数据就会有问题，就会匹配到两条数据，看是app做去重还是server做去重。
                                            //如果后台没办法做到去重的逻辑，需要前端APP做的话，就用下面的一段代码
                                            if(mCardPaymentPayTypeMap_active.get(dynModel.getApiCode()) != null &&
                                                    mCardPaymentPayTypeMap_active.get(dynModel.getApiCode()).equals(arrPays[i])){
                                            }else{
                                                mFilterCardPaymentList_active.add(dynModel);
                                                mCardPaymentPayTypeMap_active.put(dynModel.getApiCode(), arrNativeTradeType[j]);
                                            }
                                            break;
                                        }
                                    }
                                } else if (arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    //这个地方的逻辑是有点问题，以后有空再改
                                    //如果登录接口返回的dynModel里面的nativeTradeType里只有一种支付类型，但是如果是已开通的支付方式里
                                    //有两条一模一样的数据就会有问题，只匹配到一条数据，然后就直接break跳出循环了，这个时候就做了去重处理，只展示一条
                                    mFilterCardPaymentList_active.add(dynModel);
                                    mCardPaymentPayTypeMap_active.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    break;
                                }
                            }
                        }
                    }

                } else {
                    continue;
                }
            }
        }
    }


    /**
     * 过滤预授权的支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void filterList_pre_auth(List<DynModel> list, List<DynModel> filterLst_pre_auth, Map<String, String> payTypeMap_pre_auth) {

        if (!StringUtil.isEmptyOrNull(MainApplication.authServiceType)) {
            String[] arrPays = MainApplication.authServiceType.split("\\|");

            for (DynModel dynModel : list) {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                    for (String key : MainApplication.apiProviderMap.keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if (dynModel.getNativeTradeType().contains(",")) {
                                    String[] arrNativeTradeType = dynModel.getNativeTradeType().split(",");
                                    for (int j = 0; j < arrNativeTradeType.length; j++) {
                                        if (arrNativeTradeType[j].equals(arrPays[i])) {
                                            filterLst_pre_auth.add(dynModel);
                                            payTypeMap_pre_auth.put(dynModel.getApiCode(), arrNativeTradeType[j]);
                                            break;
                                        }
                                    }
                                } else if (arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    filterLst_pre_auth.add(dynModel);
                                    payTypeMap_pre_auth.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    break;
                                }
                            }
                        }
                    }

                } else {
                    continue;
                }
            }
        }

    }

    /**
     * 获取所有的支付方式
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void saveAllpayList(List<DynModel> list, List<DynModel> filterLst) {
        for (DynModel dynModel : list) {
            filterLst.add(dynModel);
        }
    }

    /**
     * 过滤固定二维码支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void filterStacticList(List<DynModel> list, List<DynModel> filterLst) {

        if (!StringUtil.isEmptyOrNull(MainApplication.serviceType)) {
            String[] arrPays = MainApplication.serviceType.split("\\|");

            for (DynModel dynModel : list) {
                if (!StringUtil.isEmptyOrNull(dynModel.getFixedCodeTradeType())) {
                    for (String key : MainApplication.apiProviderMap.keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if (dynModel.getFixedCodeTradeType().contains(",")) {
                                    String[] arrFixedCodeTradeType = dynModel.getFixedCodeTradeType().split(",");
                                    for (int j = 0; j < arrFixedCodeTradeType.length; j++) {
                                        if (arrFixedCodeTradeType[j].equals(arrPays[i])) {
                                            filterLst.add(dynModel);
                                            break;
                                        }
                                    }
                                } else if (arrPays[i].equals(dynModel.getFixedCodeTradeType())) {
                                    filterLst.add(dynModel);
                                    break;
                                }

                               /* if (arrPays[i].startsWith(dynModel.getFixedCodeTradeType()) || dynModel.getFixedCodeTradeType().contains(arrPays[i]) || arrPays[i].equals(dynModel.getFixedCodeTradeType())) {
                                    filterLst.add(dynModel);
                                    break;
                                }*/
                            }
                        }
                    }

                } else {
                    continue;
                }
            }
        }

    }

    /**
     * 动态加载支付类型
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void loadPayType() {
        LocalAccountManager.getInstance().apiShowList(new UINotifyListener<List<DynModel>>() {
            @Override
            public void onError(Object object) {
                super.onError(object);
                if (checkSession()) {
                    return;
                }
            }

            @Override
            public void onSucceed(List<DynModel> model) {
                if (null != model && model.size() > 0) {

                    PreferenceUtil.removeKey("payTypeMd5" + ApiConstant.bankCode + MainApplication.getMchId());

                    PreferenceUtil.commitString("payTypeMd5" + ApiConstant.bankCode + MainApplication.getMchId(), model.get(0).getMd5());
                    //显示所有的支付方式
                    List<DynModel> allLst = new ArrayList<DynModel>();
                    saveAllpayList(model, allLst);
                    SharedPreUtile.saveObject(allLst, "dynallPayType" + ApiConstant.bankCode + MainApplication.getMchId());

                    //过滤支持类型
                    List<DynModel> filterLst = new ArrayList<DynModel>();
                    Map<String, String> payTypeMap = new HashMap<String, String>();
                    filterList(model, filterLst, payTypeMap);
                    SharedPreUtile.saveObject(filterLst, "dynPayType" + ApiConstant.bankCode + MainApplication.getMchId());


                    //过滤已开通的支持类型
                    List<DynModel> filterList_active = new ArrayList<DynModel>();
                    Map<String, String> payTypeMap_active = new HashMap<String, String>();
                    filterList_active(model, filterList_active, payTypeMap_active);
                    SharedPreUtile.saveObject(filterList_active, "ActiveDynPayType" + ApiConstant.bankCode + MainApplication.getMchId());

                    //读取上面过滤出来的，已开通的支付类型，如果已开通的支付类型里，有QR交易通道，则把参数置成true-----V3.1.0版本新增
                    if(filterList_active != null && filterList_active.size() > 0){
                        MainApplication.hasQRPayment = true;
                    }else{
                        MainApplication.hasQRPayment = false;
                    }

                    //过滤卡通道的类型---V3.1.0版本新增
                    List<DynModel> mFilterCardPaymentList = new ArrayList<DynModel>();
                    Map<String, String> mCardPaymentPayTypeMap = new HashMap<String, String>();
                    filterCardPaymentList(model, mFilterCardPaymentList, mCardPaymentPayTypeMap);
                    SharedPreUtile.saveObject(mFilterCardPaymentList, "cardPayment_dynPayType" + ApiConstant.bankCode + MainApplication.getMchId());

                    //过滤已开通的卡通道类型---V3.1.0版本新增
                    List<DynModel> mFilterCardPaymentList_active = new ArrayList<DynModel>();
                    Map<String, String> mCardPaymentPayTypeMap_active = new HashMap<String, String>();
                    filterActiveCardPaymentList(model, mFilterCardPaymentList_active, mCardPaymentPayTypeMap_active);
                    SharedPreUtile.saveObject(mFilterCardPaymentList_active, "cardPayment_ActiveDynPayType" + ApiConstant.bankCode + MainApplication.getMchId());

                    //读取上面过滤出来的，已开通的支付类型，如果已开通的支付类型里，有卡交易通道，则把参数置成true-----V3.1.0版本新增
                    if(mFilterCardPaymentList_active != null && mFilterCardPaymentList_active.size() > 0){
                        MainApplication.hasCardPayment = true;
                    }else{
                        MainApplication.hasCardPayment = false;
                    }

                    //过滤预授权支持类型
                   /* List<DynModel> filterLst_pre_auth = new ArrayList<DynModel>();
                    Map<String, String> payTypeMap_auth = new HashMap<String, String>();
                    filterList_pre_auth(model, filterLst_pre_auth, payTypeMap_auth);
                    SharedPreUtile.saveObject(filterLst_pre_auth, "dynPayType_pre_auth" + ApiConstant.bankCode + MainApplication.getMchId());*/

                    //过滤预授权支持类型---写死在前端，写死成支付宝
                    List<DynModel> filterLst_pre_auth = new ArrayList<DynModel>();
                    Map<String, String> payTypeMap_auth = new HashMap<String, String>();
                    DynModel dynModel = new DynModel();
                    dynModel.setNativeTradeType(MainApplication.authServiceType);
                    dynModel.setApiCode("2");
                    filterLst_pre_auth.add(dynModel);
                    payTypeMap_auth.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                    SharedPreUtile.saveObject(filterLst_pre_auth, "dynPayType_pre_auth" + ApiConstant.bankCode + MainApplication.getMchId());


                    //流水过滤条件筛选----用来用来筛选流水的字段，V3.1.0改了，废弃此字段，不用了
                    List<DynModel> filterLstStream = new ArrayList<DynModel>();
                    filterListStream(model, filterLstStream);
                    SharedPreUtile.saveObject(filterLstStream, "filterLstStream" + ApiConstant.bankCode + MainApplication.getMchId());

                    //过滤固定二维码支付类型
                    List<DynModel> filterStaticLst = new ArrayList<DynModel>();
                    filterStacticList(model, filterStaticLst);
                    SharedPreUtile.saveObject(filterStaticLst, "dynPayTypeStatic" + ApiConstant.bankCode + MainApplication.getMchId());

                    Map<String, String> typePicMap = new HashMap<String, String>();
                    // 颜色map集合
                    Map<String, String> colorMap = new HashMap<String, String>();

                    Map<String, DynModel> payTypeNameMap = new HashMap<String, DynModel>();

                    for (DynModel d : model) {
                        if (!StringUtil.isEmptyOrNull(d.getSmallIconUrl())) {
                            typePicMap.put(d.getApiCode(), d.getSmallIconUrl());
                        }

                        if (!StringUtil.isEmptyOrNull(d.getColor())) {
                            colorMap.put(d.getApiCode(), d.getColor());
                        }

                        if (!StringUtil.isEmptyOrNull(d.getProviderName())) {
                            payTypeNameMap.put(d.getApiCode(), d);
                        }

                    }
                    SharedPreUtile.saveObject(payTypeNameMap, "payTypeNameMap" + ApiConstant.bankCode + MainApplication.getMchId());
                    SharedPreUtile.saveObject(payTypeMap, "payTypeMap" + ApiConstant.bankCode + MainApplication.getMchId());
                    //V3.1.0新增，卡通道
                    SharedPreUtile.saveObject(mCardPaymentPayTypeMap, "mCardPaymentPayTypeMap" + ApiConstant.bankCode + MainApplication.getMchId());
                    SharedPreUtile.saveObject(payTypeMap_auth, "payTypeMap_pre_auth" + ApiConstant.bankCode + MainApplication.getMchId());
                    //图片
                    SharedPreUtile.saveObject(typePicMap, "payTypeIcon" + ApiConstant.bankCode + MainApplication.getMchId());
                    //颜色值
                    SharedPreUtile.saveObject(colorMap, "payTypeColor" + ApiConstant.bankCode + MainApplication.getMchId());


                    //读取上面过滤出来的，已开通的支付类型，如果已开通的支付类型里有小钱包，则去请求小钱包列表接口，否则不用请求
                    if(filterList_active != null ){
                        for(int i = 0 ; i  < filterList_active.size() ; i++ ){
                            if(filterList_active.get(i).getNativeTradeType().equalsIgnoreCase(MainApplication.liquidServiceType)){
                                //只要有一个liquid pay的支付类型，则去请求小钱包接口，跳出循环
                                getWalletList(MainApplication.liquidServiceType);
                                break;
                            }
                        }
                    }
                }
           }
        });
    }


    public void getWalletList(String serviceType){
        LocalAccountManager.getInstance().queryWalletList(serviceType,new UINotifyListener<List<WalletListBean>>(){
            @Override
            public void onError(Object object) {
                super.onError(object);
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(List<WalletListBean> result) {
                super.onSucceed(result);
            }
        });
    }

   private void todo() {
        //启动心跳
        Intent it = new Intent();
        it.setClass(WelcomeActivity.this, spayMainTabActivity.class);
        startActivity(it);

        finish();
    }



    public void checkVersion() {
        //不用太平凡的检测升级 所以使用时间间隔区分
        LocalAccountManager.getInstance().saveCheckVersionTime();
        UpgradeManager.getInstance().getVersonCode(new UINotifyListener<UpgradeInfo>() {
            @Override
            public void onError(Object object) {
                super.onError(object);
                startLogin();
            }

            @Override
            public void onSucceed(UpgradeInfo result) {
                super.onSucceed(result);
                if (null != result) {

                    //                    PreferenceUtil.getInt("update", 0);

                    //                    if (result.version > PreferenceUtil.getInt("update", 0))
                    //                    {
                    // "发现新版本"
                    LocalAccountManager.getInstance().saveCheckVersionFlag(result.mustUpgrade);
                    showUpgradeInfoDialog(result, new ComDialogListener(result));
                    //                    }
                } else {
                    startLogin();
                }
            }
        });
    }

    class ComDialogListener implements CommonConfirmDialog.ConfirmListener {
        private UpgradeInfo result;

        public ComDialogListener(UpgradeInfo result) {
            this.result = result;
        }

        @Override
        public void ok() {
            new UpgradeDailog(WelcomeActivity.this, result, new UpgradeDailog.UpdateListener() {
                @Override
                public void cancel() {

                }
            }).show();
        }

        @Override
        public void cancel() {
            //LocalAccountManager.getInstance().saveCheckVersionTime();
            //            if (result.mustUpgrade)
            //            {
            //                MainApplication.getContext().exit();
            //                WelcomeActivity.this.finish();
            //            }
            //            else
            //            {
            //                startLogin();
            //            }

            //            PreferenceUtil.commitInt("update", result.version); // 保存更新，下次进来不更新

        }

    }

    /*
     **如果证书已过期或者证书校验失败
     * 请求CA证书公钥获取接口得到最新CA证书公钥
     * */
    public void requestCertificate(boolean isDeviceLogin){
        LocalAccountManager.getInstance().getCertificateString(new UINotifyListener<CertificateBean>(){
            @Override
            public void onError(Object object) {
                super.onError(object);
                //如果返回失败或者网络异常时，则直接跳转到一键重登界面
                if(isDeviceLogin){//如果是设备登录请求的时候，先请求证书公钥
                   //获取公钥证书失败，则直接跳转到一键重登界面
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dismissLoading();
                            //则跳转到一键重登页面
                            Intent it = new Intent();
                            it.putExtra("isCerFailed",true);
                            it.setClass(WelcomeActivity.this, ReLoginActivity.class);
                            startActivity(it);
                        }
                    });

                }else{//如果是点登陆按钮去请求公钥证书
                    //获取公钥证书失败，则弹框提示
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dismissLoading();
                            showLoginPage();
                            MainApplication.setNeedLogin(false);
                            btn_login.setEnabled(true);
                            btn_login.setText(R.string.login);

                            toastDialog(WelcomeActivity.this, R.string.certificate_request_failed, null);
                        }
                    });

                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if(!isDeviceLogin){
                    showLoading(false, getString(R.string.show_login_loading));
                }

            }

            @Override
            protected void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(CertificateBean result) {
                super.onSucceed(result);
                //最新配置的公钥信息
                cerFailedRequestAgain = false;

                MainApplication.certificateBean = result;
                if(isDeviceLogin){//如果是设备登录请求的时候，先请求证书公钥,成功之后再走正常逻辑
                    //先去调用公钥交换接口然后再调用设备登录接口,每次设备登录前都主动去调用ECDH去交换
                    if(result != null){
                        isNeedDeviceLogin = true;
                        ECDHKeyExchange();
                    }
                }else{//如果是点登陆按钮去请求公钥证书
                    //如果请求公钥证书成功，则重新调用登录接口
                    if(result != null){
                        //则重新走一遍登录前的ECDH然后再走登录
                        isNeedRequestECDHKey = true;
                        ECDHKeyExchange();
                    }
                }
            }
        });
    }


    /**
     * 释放已经安装的apk
     */
    public static void clearInstalledAPK() {

        ThreadHelper.executeWithCallback(new Executable() {
            @Override
            public Void execute() {
                try {
                    String appPath = FileUtils.defaultDownloadPath + ApiConstant.APK_NAME;
                    Logger.i("DownloadManager", "delete apk");
                    FileUtils.deleteFile(appPath);
                } catch (Exception e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
                return null;
            }

        }, null);

    }

    public void showPrivacyDialog(){
        View layout = initDialogView();
        final AlertDialog dialog = new AlertDialog.Builder(this).setCancelable(false)
                .setTitle(getString(R.string.privacy_terms))
                .setView(layout)
                .setPositiveButton(getString(R.string.privacy_quit), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //用户拒绝当前隐私策略
                        PushManager.getInstance().setPrivacyPolicyStrategy(WelcomeActivity.this, false);
                        savePrivacyFlag(false);
                        finish();

                    }
                }).setNegativeButton(getString(R.string.privacy_agree), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //用户同意当前隐私策略
                        PushManager.getInstance().setPrivacyPolicyStrategy(WelcomeActivity.this, true);
                        savePrivacyFlag(true);
                    }
                }).create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.GRAY);
            }
        });
        dialog.show();
    }

    private View initDialogView() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int height = dm.heightPixels;

        ViewGroup viewGroup = (ViewGroup) LayoutInflater.from(this).inflate(R.layout.privacy_dialog, null);
        View scrollView = viewGroup.findViewById(R.id.scrollView);
        scrollView.getLayoutParams().height = (int) (height*0.5f);
        scrollView.requestLayout();
        String privacyUser = getString(R.string.privacy_agree_instruction1);
        String privacyName = "\""+"Privacy Policy"+"\"";
        SpannableString hintString = new SpannableString(privacyUser + privacyName);
        URLSpan urlSpan = new URLSpan("") {
            @Override
            public void onClick(View widget) {
                // 跳转到隐私页面
                ContentTextActivity.startActivity(WelcomeActivity.this, "https://www.aub.com.ph/privacyPolicy/", null);
            }
        };
        hintString.setSpan(urlSpan, privacyUser.length(), privacyUser.length() + privacyName.length(), Spanned.SPAN_MARK_MARK);

        TextView privacyView = viewGroup.findViewById(R.id.agreeTv);
        privacyView.setText(hintString);
        privacyView.setPadding(25, 15, 25, 10);
        privacyView.setMovementMethod(LinkMovementMethod.getInstance());

        return viewGroup;
    }

    public static Boolean getPrivacyFlag()
    {
        MainApplication.getContext().getApplicationPreferences();
        SharedPreferences sp = MainApplication.getContext().getApplicationPreferences();
        return sp.getBoolean(GlobalConstant.FIELD_IS_ACCESS_PRIVACE_POLICY, false);
    }

    public void savePrivacyFlag(boolean isAccessPrivacy)
    {
        MainApplication.getContext().getApplicationPreferences();
        SharedPreferences sp = MainApplication.getContext().getApplicationPreferences();
        sp.edit().putBoolean(GlobalConstant.FIELD_IS_ACCESS_PRIVACE_POLICY, isAccessPrivacy).commit();
    }
}
