package cn.swiftpass.enterprise.ui.activity.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.WxCard;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.activity.DescriptionActivity;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;

public class VerificationDetails extends BaseActivity implements OnClickListener {

    private TextView verification_body_info, verification_pay_mch, verification_tvOrderCode;
    private TextView verification_wx_tvOrderCode, verification_order, verification_user;

    //private TextView verification_pay_method;
    private TextView verification_tv_notifyTime, verification_tv_state, verification_detail_info;

    //private TextView  verification_client;
    private WxCard orderMode;
    private Button verification_btn_refund;
    private boolean isFromStream = true;
    private LinearLayout cashierLay, order_lay;
    private TextView verification_tv_desc, verification_detail_type, card_name;
    private RelativeLayout lay_desc;

    public static void startActivity(Context context, WxCard orderModel) {
        Intent it = new Intent();
        it.setClass(context, VerificationDetails.class);
        it.putExtra("order", orderModel);
        context.startActivity(it);
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_details);

        getIntentValue();
        initView();
        if (isFromStream) {
            verification_btn_refund.setVisibility(View.INVISIBLE);
        } else {
            verification_btn_refund.setVisibility(View.VISIBLE);
        }
        initListener();
        initData();
    }

    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initListener() {
        verification_btn_refund.setOnClickListener(this);
        lay_desc.setOnClickListener(this);

    }

    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initData() {
        if (orderMode != null) {
            if (isAbsoluteNullStr(orderMode.getOutTradeNo())) {
                order_lay.setVisibility(View.GONE);
            } else {
                verification_order.setText(orderMode.getOutTradeNo());
            }
            if (null != orderMode.getDealDetail() && !orderMode.getDealDetail().equals("null")) {
                verification_tv_desc.setText(orderMode.getDealDetail());
            }
            verification_body_info.setText(orderMode.getTitle());
            verification_pay_mch.setText(MainApplication.getInstance().getMchName());
            verification_tvOrderCode.setText(MainApplication.getInstance().getMchId());
            verification_wx_tvOrderCode.setText(orderMode.getCardCode());
            //            verification_pay_method.setText(text);
            verification_detail_type.setText(Order.WxCardType.getCardNameForType(orderMode.getCardType()));
            if (orderMode.getCardType().equalsIgnoreCase(Order.WxCardType.CARSH_CARD.cardType)) // 代金券
            {
                card_name.setText(R.string.tx_reduce_cost);
                verification_detail_info.setText(MainApplication.getInstance().getFeeFh()
                        + DateUtil.formatMoneyUtils(orderMode.getReduceCost()));
            } else if (orderMode.getCardType().equalsIgnoreCase(Order.WxCardType.DISCOUNT_CARD.cardType)) {
                card_name.setText(R.string.et_discount);
                verification_detail_info.setText(orderMode.getDiscount() + "%");
            } else if (orderMode.getCardType().equalsIgnoreCase(Order.WxCardType.GIFT_CARD.cardType)) {
                card_name.setText(R.string.tx_gift);
                if (null != orderMode.getGift() && !orderMode.getGift().equals("null")) {
                    verification_detail_info.setText(orderMode.getGift());
                }
            } else if (orderMode.getCardType().equalsIgnoreCase(Order.WxCardType.GROUPON_CARD.cardType)) {
                card_name.setText(R.string.tx_group_card);
                if (null != orderMode.getDealDetail() && !orderMode.getDealDetail().equals("null")) {
                    verification_tv_desc.setText(orderMode.getDealDetail());
                }
            }

            if (null != orderMode.getDealDetail() && !orderMode.getDealDetail().equals("null")) {
                verification_detail_info.setText(orderMode.getDealDetail());
            }
            verification_tv_notifyTime.setText(orderMode.getUseTimeNew());
            verification_tv_state.setText(R.string.tx_affirm_succ);

            //            if (!isAbsoluteNullStr(orderMode.getClient()))
            //            {
            //                verification_client.setText(ClientEnum.getClienName(orderMode.getClient()));
            //            }
            //            else
            //            {
            //                verification_client.setText(ClientEnum.UNKNOW.name());
            //            }

            if (MainApplication.getInstance().isAdmin(0)) {
                cashierLay.setVisibility(View.VISIBLE);
                verification_user.setText(MainApplication.getInstance().getUserInfo().realname + "("
                        + MainApplication.getInstance().getUserId() + ")");
            } else {
                if (null != orderMode.getUserName() && !"".equals(orderMode.getUserName())
                        && !"null".equals(orderMode.getUserName())) {
                    cashierLay.setVisibility(View.VISIBLE);
                    if (orderMode.getuId() != 0) {

                        verification_user.setText(orderMode.getUserName() + "(" +
                                orderMode.getuId() + ")");
                    } else {
                        verification_user.setText(orderMode.getUserName() + "(" +
                                orderMode.getuId() + ")");
                    }
                } else {
                    cashierLay.setVisibility(View.GONE);
                }
            }
        }
    }

    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void getIntentValue() {
        Intent intent = getIntent();
        orderMode = (WxCard) intent.getSerializableExtra("order");
    }

    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initView() {
        lay_desc = findViewById(R.id.lay_desc);
        card_name = findViewById(R.id.card_name);
        verification_detail_type = findViewById(R.id.verification_detail_type);
        order_lay = findViewById(R.id.order_lay);
        cashierLay = findViewById(R.id.cashierLay);
        verification_user = findViewById(R.id.verification_user);
        verification_order = findViewById(R.id.verification_order);
        //verification_client = findViewById(R.id.verification_client);
        verification_detail_info = findViewById(R.id.verification_detail_info);
        verification_body_info = findViewById(R.id.verification_body_info);
        verification_pay_mch = findViewById(R.id.verification_pay_mch);
        verification_tvOrderCode = findViewById(R.id.verification_tvOrderCode);
        verification_wx_tvOrderCode = findViewById(R.id.verification_wx_tvOrderCode);
        //verification_pay_method = findViewById(R.id.verification_pay_method);
        verification_tv_notifyTime = findViewById(R.id.verification_tv_notifyTime);
        verification_tv_state = findViewById(R.id.verification_tv_state);
        verification_btn_refund = findViewById(R.id.verification_btn_refund);

        verification_tv_desc = findViewById(R.id.verification_tv_desc);
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tx_affirm_detail);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButLayClick() {

            }
        });
    }


    /**
     * {@inheritDoc}
     */

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.verification_btn_refund:
                this.finish();
                break;
            case R.id.lay_desc:
                if (orderMode.getDealDetail() != null && orderMode.getDealDetail().length() > 15) {
                    DescriptionActivity.startActivity(VerificationDetails.this, orderMode);
                }

                break;
            default:
                break;
        }
    }

}
