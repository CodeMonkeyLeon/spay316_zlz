package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.ECDHInfo
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @在醒来时
 * @世界都远了
 * @我需要
 * @最狂的风
 * @和最静的海。
 */
class FindPassSubmitContract {

    interface View : BaseView {

        fun checkDataSuccess(response: Boolean)

        fun checkDataFailed(error: Any?)

        fun ecdhKeyExchangeSuccess(response: ECDHInfo?)

        fun ecdhKeyExchangeFailed(error: Any?)


    }

    interface Presenter : BasePresenter<View> {


        fun ecdhKeyExchange(publicKey: String?)

        fun checkData(
            token: String?,
            emailCode: String?,
            password: String?
        )

    }


}