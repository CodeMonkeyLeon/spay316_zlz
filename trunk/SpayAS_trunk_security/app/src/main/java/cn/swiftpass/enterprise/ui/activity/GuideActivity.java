package cn.swiftpass.enterprise.ui.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.common.sentry.SentryUtils;
import com.example.common.sp.PreferenceUtil;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.KotlinUtils;

/**
 * Created by aijingya on 2018/6/12.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(引导页)
 * @date 2018/6/12.14:29.
 */

public class GuideActivity extends BaseActivity implements ViewPager.OnPageChangeListener {

    private static final String TAG = GuideActivity.class.getSimpleName();
    View[] ViewLists;
    private ViewPager mViewPager;
    private LinearLayout ll_dots;
    private TextView tv_skip, tv_next, tv_privacy_quit, tv_privacy_agree, tv_privacy_url;
    private ConstraintLayout cl_privacy;
    private int[] ViewIdLists = {R.layout.item_guide_view, R.layout.item_guide_view2, R.layout.item_guide_view3};
    private int currentItem = 0;
    private int pageCount;
    /**
     * 装点点的ImageView数组
     */
    private ImageView[] dots;

    public static Boolean getPrivacyFlag() {
        MainApplication.getInstance().getApplicationPreferences();
        SharedPreferences sp = MainApplication.getInstance().getApplicationPreferences();
        return sp.getBoolean(GlobalConstant.FIELD_IS_ACCESS_PRIVACE_POLICY, false);
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        KotlinUtils.INSTANCE.isReStartApp(savedInstanceState, this);
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                | WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

        if (!getIsFirstLaunch()) {//如果是第一次启动APP
            startWelcomeActivity();
            return;
        }
        setContentView(R.layout.activity_guide_layout);
        mViewPager = (ViewPager) findViewById(R.id.vp_banner);
        ll_dots = (LinearLayout) findViewById(R.id.dots);
        tv_skip = (TextView) findViewById(R.id.tv_skip);
        tv_next = (TextView) findViewById(R.id.tv_next);
        cl_privacy = (ConstraintLayout) findViewById(R.id.cl_privacy);

        tv_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startWelcomeActivity();
                setIsFirstLaunch(false);
            }
        });

        tv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentItem == pageCount - 1) {
                    startWelcomeActivity();
                    setIsFirstLaunch(false);
                } else {
                    currentItem++;
                    mViewPager.setCurrentItem(currentItem);
                }
            }
        });

        //Load all the View to the array
        pageCount = ViewIdLists.length;
        ViewLists = new View[pageCount];
        for (int i = 0; i < ViewIdLists.length; i++) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View ItemView = inflater.inflate(ViewIdLists[i], null);
            ViewLists[i] = ItemView;
        }

        //将点点加入到ViewGroup中
        dots = new ImageView[pageCount];
        for (int i = 0; i < dots.length; i++) {
            ImageView imageView = new ImageView(this);
            imageView.setLayoutParams(new ViewGroup.LayoutParams(20, 20));
            dots[i] = imageView;
            if (i == 0) {
                dots[i].setBackgroundResource(R.drawable.icon_circle1);
            } else {
                dots[i].setBackgroundResource(R.drawable.icon_circle);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            layoutParams.leftMargin = 15;
            layoutParams.rightMargin = 15;
            ll_dots.addView(imageView, layoutParams);
        }

        mViewPager.setAdapter(new ImagePagerAdapter());
        //设置监听，主要是设置点点的背景
        mViewPager.addOnPageChangeListener(this);

        currentItem = 0;
        mViewPager.setCurrentItem(currentItem);
        boolean hasAlerted = (boolean) getPrivacyFlag();
        cl_privacy.setVisibility(hasAlerted ? View.GONE : View.VISIBLE);
        if (!hasAlerted) {
            tv_privacy_quit = findViewById(R.id.tv_privacy_quit);
            tv_privacy_agree = findViewById(R.id.tv_privacy_agree);
            tv_privacy_url = findViewById(R.id.tv_privacy_url);
            tv_privacy_quit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //用户拒绝当前隐私策略
                    savePrivacyFlag(false);
                    finish();
                }
            });
            tv_privacy_agree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //用户同意当前隐私策略
                    savePrivacyFlag(true);
                    if (BuildConfig.isShowGuidePage) {
                        //展示引导页
                        cl_privacy.setVisibility(View.GONE);
                    } else {
                        //隐藏引导页
                        setIsFirstLaunch(false);
                        startWelcomeActivity();
                    }
                }
            });
            String privacyUser = getString(R.string.privacy_agree_instruction1);
            String privacyName = "\"" + getString(R.string.app_name) + " " + getString(R.string.privacy_agree_instruction2) + "\"";
            SpannableString hintString = new SpannableString(privacyUser + privacyName);
            URLSpan urlSpan = new URLSpan("") {
                @Override
                public void onClick(View widget) {
                    // 跳转到隐私页面
                    ContentTextActivity.startActivity(GuideActivity.this,
                            BuildConfig.privacyPolicy, null);
                }
            };
            hintString.setSpan(urlSpan, privacyUser.length(), privacyUser.length() +
                    privacyName.length(), Spanned.SPAN_MARK_MARK);
            hintString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.bg_text_new)),
                    privacyUser.length(), privacyUser.length() + privacyName.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            tv_privacy_url.setText(hintString);
            tv_privacy_url.setPadding(35, 15, 35, 10);
            tv_privacy_url.setMovementMethod(LinkMovementMethod.getInstance());
        }

    }

    public void startWelcomeActivity() {
        Intent intent = new Intent(GuideActivity.this, WelcomeActivity.class);
        startActivity(intent);
        finish();
    }

    public void savePrivacyFlag(boolean isAccessPrivacy) {
        MainApplication.getInstance().getApplicationPreferences();
        SharedPreferences sp = MainApplication.getInstance().getApplicationPreferences();
        sp.edit().putBoolean(GlobalConstant.FIELD_IS_ACCESS_PRIVACE_POLICY, isAccessPrivacy).commit();
    }

    /**
     * @return
     * @throws
     * @Title: getIsFirstLaunch
     * @Description: 是否是当前版本的第一次打开
     */
    public boolean getIsFirstLaunch() {
        String version = KotlinUtils.INSTANCE.getAppVersionName(GuideActivity.this);
        return PreferenceUtil.getBoolean("is_first_launch_" + version, true);
    }

    /**
     * @param isFirstLaunch
     * @throws
     * @Title: setIsFirstLaunch
     * @Description: TODO
     */
    public void setIsFirstLaunch(boolean isFirstLaunch) {
        String version = KotlinUtils.INSTANCE.getAppVersionName(GuideActivity.this);
        PreferenceUtil.commitBoolean("is_first_launch_" + version, isFirstLaunch);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        currentItem = position;
        if (currentItem == pageCount - 1) {
            tv_skip.setVisibility(View.INVISIBLE);
            tv_next.setText(getString(R.string.guide_login));
        } else {
            tv_skip.setVisibility(View.VISIBLE);
            tv_next.setText(getString(R.string.guide_next));
        }
        setImageBackground(position % pageCount);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * 设置选中的dot的背景
     *
     * @param selectItems
     */
    private void setImageBackground(int selectItems) {
        for (int i = 0; i < dots.length; i++) {
            if (i == selectItems) {
                dots[i].setBackgroundResource(R.drawable.icon_circle1);
            } else {
                dots[i].setBackgroundResource(R.drawable.icon_circle);
            }
        }
    }

    public class ImagePagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return pageCount;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {

            View mView = ViewLists[position % pageCount];
            try {
                ((ViewPager) container).addView(mView);
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                Log.e(TAG, Log.getStackTraceString(e));
            }

            return mView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
        }
    }
}
