package cn.swiftpass.enterprise.io.database.migrations;

import android.database.sqlite.SQLiteDatabase;
import cn.swiftpass.enterprise.io.database.table.ActiveDataTab;
import cn.swiftpass.enterprise.io.database.table.AdsTable;
import cn.swiftpass.enterprise.io.database.table.CacheInfoTable;
import cn.swiftpass.enterprise.io.database.table.CashierReportSumTable;
import cn.swiftpass.enterprise.io.database.table.CodeTable;
import cn.swiftpass.enterprise.io.database.table.CountryAndUnitTable;
import cn.swiftpass.enterprise.io.database.table.DownloadTable;
import cn.swiftpass.enterprise.io.database.table.OrderTable;
import cn.swiftpass.enterprise.io.database.table.ShopBankDataTab;
import cn.swiftpass.enterprise.io.database.table.ShopBaseDataTable;
import cn.swiftpass.enterprise.io.database.table.ShopTable;
import cn.swiftpass.enterprise.io.database.table.UserInfoTable;

public class Migration_Init extends MigrationBase
{
    
    @Override
    public void onUpgrade(SQLiteDatabase db)
    {
        dropTables(db);
        createTables(db);
    }
    
    @Override
    public void onDowngrade(SQLiteDatabase db)
    {
        dropTables(db);
        createTables(db);
    }
    
    public static void createTables(SQLiteDatabase database)
    {
        new OrderTable().createTable(database);
        new DownloadTable().createTable(database);
        new AdsTable().createTable(database);
        new UserInfoTable().createTable(database);
        new CodeTable().createTable(database);
        new CacheInfoTable().createTable(database);
        new CashierReportSumTable().createTable(database);
        //shop
        new ShopTable().createTable(database);
        
        // 商户数据基本
        new ShopBaseDataTable().createTable(database);
        
        new ShopBankDataTab().createTable(database);
        
        new CountryAndUnitTable().createTable(database);
        
        new ActiveDataTab().createTable(database);
    }
    
    public static void dropTables(SQLiteDatabase database)
    {
        database.execSQL(DROP_BASE + OrderTable.TABLE_NAME);
        database.execSQL(DROP_BASE + DownloadTable.TABLE_DOWNLOAD);
        database.execSQL(DROP_BASE + AdsTable.TABLE_NAME);
        database.execSQL(DROP_BASE + UserInfoTable.TABLE_NAME);
        database.execSQL(DROP_BASE + CodeTable.TABLE_NAME);
        database.execSQL(DROP_BASE + CacheInfoTable.TABLE_NAME);
        database.execSQL(DROP_BASE + CashierReportSumTable.TABLE_NAME);
        database.execSQL(DROP_BASE + ShopTable.TABLE_NAME);
        
        database.execSQL(DROP_BASE + ShopBaseDataTable.TABLE_NAME);
        
        database.execSQL(DROP_BASE + ShopBankDataTab.TABLE_NAME);
        
        database.execSQL(DROP_BASE + CountryAndUnitTable.TABLE_NAME);
        
        database.execSQL(DROP_BASE + ActiveDataTab.TABLE_NAME_ACTIVE);
        
    }
}
