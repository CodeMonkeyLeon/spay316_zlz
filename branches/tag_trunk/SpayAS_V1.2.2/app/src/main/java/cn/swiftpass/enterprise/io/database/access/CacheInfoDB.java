package cn.swiftpass.enterprise.io.database.access;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.enums.CacheEnum;
import cn.swiftpass.enterprise.bussiness.model.CacheModel;
import cn.swiftpass.enterprise.io.database.table.CacheInfoTable;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.util.List;

/**
 * 缓存
 * User: Administrator
 * Date: 13-11-18
 * Time: 下午6:19
 * To change this template use File | Settings | File Templates.
 */
public class CacheInfoDB {

    private static Dao<CacheModel, Integer>  cacheDao;
    private CacheInfoDB() {
        try {
            cacheDao = MainApplication.getContext().getHelper()
                    .getDao(CacheModel.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    private static CacheInfoDB instance;
    public static CacheInfoDB getInstance() {
        if (instance == null) {
            instance = new CacheInfoDB();
        }
        return instance;
    }

    public void save(CacheModel c) throws SQLException
    {
        cacheDao.create(c);
    }
    public CacheModel queryWhere(int type,long uId)throws SQLException
    {
           return queryWhere(type,uId,false,0);
    }
     // System.currentTimeMillis() - 2 * 24 * 60 * 60 * 1000; 2天前
    //long i = System.currentTimeMillis() - 1 * 1 * 60 * 60 * 1000;
    /**
     * 查询 根据小时查询
     * */
    public CacheModel queryWhere(int type,long uId,boolean isTime,int hour)throws SQLException
    {
        QueryBuilder<CacheModel,Integer>  builder = cacheDao.queryBuilder();
        Where<CacheModel,Integer> where = builder.where();
        where.eq(CacheInfoTable.COLUMN_TYPE,type);
        where.and();
        where.eq(CacheInfoTable.COLUMN_UID,uId);
        if(isTime){
            where.and();
            long time = System.currentTimeMillis() - 1 * hour * 60 * 60 * 1000;
            where.le(CacheInfoTable.COLUMN_ADDTIME,time);
        }
        builder.orderBy(CacheInfoTable.COLUMN_ADDTIME,false);
        List<CacheModel> list = cacheDao.query(builder.prepare());
        if(list != null && list.size() > 0 )
        {
            return  list.get(0);
        }
        return null;
    }

    public void delete(CacheEnum type,long uId) throws SQLException
    {
        DeleteBuilder<CacheModel,Integer> builder =  cacheDao.deleteBuilder();
        Where<CacheModel,Integer> where = builder.where();
        where.eq(CacheInfoTable.COLUMN_TYPE,type.getValue());
        where.and();
        where.eq(CacheInfoTable.COLUMN_ID,uId);
        cacheDao.delete(builder.prepare());
    }

}
