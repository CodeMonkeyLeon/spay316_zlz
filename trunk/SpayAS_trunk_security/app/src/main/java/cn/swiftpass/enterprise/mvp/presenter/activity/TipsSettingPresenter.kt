package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.TipsList
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.TipsSettingContract
import cn.swiftpass.enterprise.utils.JsonUtil
import okhttp3.Call


/**
 * @author lizheng.zhao
 * @date 2022/12/2
 *
 * @这次我离开你
 * @是风
 * @是雨
 * @是夜晚
 * @你笑了笑
 * @我摆一摆手
 * @一条寂寞的路便展向两头了
 */
class TipsSettingPresenter : TipsSettingContract.Presenter {


    private var mView: TipsSettingContract.View? = null


    override fun getTipsSetting() {
        mView?.let { view ->

            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)

            AppClient.getTipsSetting(
                MainApplication.getInstance().getMchId(),
                MainApplication.getInstance().getUserId().toString(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<TipsList>(TipsList()) {


                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.getTipsSettingFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val list = JsonUtil.jsonToBean(
                                response.message,
                                TipsList::class.java
                            ) as TipsList
                            view.getTipsSettingSuccess(list.data)
                        }
                    }
                }
            )
        }
    }


    override fun attachView(view: TipsSettingContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}