package cn.swiftplus.enterprise.printsdk.print.TradelinkA8;

import android.content.Context;

/**
 * Created by aijingya on 2020/10/28.
 *
 * @Package cn.swiftplus.enterprise.printsdk.print.TradelinkA8
 * @Description:
 * @date 2020/10/28.16:51.
 */
public class POSTradelinkA8Client {
    private static POSTradelinkA8Client sTradelinkA8Client;
    private Context mContext;

    public static void initTradelinkA8(Context context){
        if (sTradelinkA8Client != null) {
            throw new RuntimeException("TradelinkA8Client Already initialized");
        }
        sTradelinkA8Client = new POSTradelinkA8Client(context);
    }

    public static POSTradelinkA8Client getInstance(){
        if (sTradelinkA8Client == null) {
            throw new RuntimeException("TradelinkA8Client is not initialized");
        }
        return sTradelinkA8Client;
    }

    private POSTradelinkA8Client(Context context){
        mContext = context;
        //初始化 tradelink A8 打印机

    }

}
