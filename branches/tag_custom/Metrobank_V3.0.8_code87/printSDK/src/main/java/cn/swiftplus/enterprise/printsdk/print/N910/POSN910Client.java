package cn.swiftplus.enterprise.printsdk.print.N910;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.newland.me.ConnUtils;
import com.newland.me.DeviceManager;
import com.newland.mtype.ConnectionCloseEvent;
import com.newland.mtype.event.DeviceEventListener;
import com.newland.mtypex.nseries.NSConnV100ConnParams;

/**
 * Created by aijingya on 2020/11/10.
 *
 * @Package cn.swiftplus.enterprise.printsdk.print.N910
 * @Description: N910以及N900的打印机调用
 * @date 2020/11/10.15:06.
 */
public class POSN910Client {

    private static  POSN910Client sPOSN910Client;
    private Context mContext;

    public static void initN910(Context context){
        if(sPOSN910Client != null){
            throw new RuntimeException("POSN910Client Already initialized");
        }
        sPOSN910Client = new POSN910Client(context);
    }

    public static POSN910Client getInstance(){
        if (sPOSN910Client == null) {
            throw new RuntimeException("POSN910Client is not initialized");
        }
        return sPOSN910Client;
    }

    private POSN910Client(Context context){
        mContext = context;
        //N910 pos 打印机初始化
        try {
            DeviceManager deviceManager = ConnUtils.getDeviceManager();
            deviceManager.init(mContext, "com.newland.me.K21Driver", new NSConnV100ConnParams(), new DeviceEventListener<ConnectionCloseEvent>() {
                @Override
                public void onEvent(ConnectionCloseEvent event, Handler arg1) {
                    if(event.isSuccess()) {
                        Log.i("PRINT_SDK", "设备已连接");
                    }

                    if(event.isFailed()) {
                        Log.i("PRINT_SDK", "设备断开连接");
                    }
                }

                @Override
                public Handler getUIHandler() {
                    return null;
                }
            });
            deviceManager.connect();
            deviceManager.getDevice().setBundle(new NSConnV100ConnParams());
        } catch (Exception var4) {

        }
    }

}
