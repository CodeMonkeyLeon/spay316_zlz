package cn.swiftpass.enterprise.print

import android.content.Context
import android.os.Build
import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.bussiness.model.OrderTotalInfo
import cn.swiftpass.enterprise.bussiness.model.OrderTotalItemInfo
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.utils.*
import com.example.printsdk.A920.printUtils.PrintA920Text
import com.example.printsdk.Constants
import com.example.printsdk.PrintClient
import com.pax.dal.exceptions.PrinterDevException

object PrintTextToPosMachine {

    fun printStr(isCashierShow: Boolean, orderModel: Order, context: Context? = null) {
        when (Build.MODEL) {
            Constants.TYPE_A8 -> PrintClient.getInstance()?.bindDeviceService()
            Constants.TYPE_A920 -> PrintA920Text.getInstance()?.init(context)
        }
        if (orderModel.isPay) {
            //收款
            PrintClient.getInstance()
                ?.printTextCenter(ToastHelper.toStr(R.string.tx_blue_print_pay_note))
        } else {
            //退款
            PrintClient.getInstance()
                ?.printTextCenter(ToastHelper.toStr(R.string.tx_blue_print_refund_note))
        }
        PrintClient.getInstance()?.printEmptyLine()

        if (orderModel.isPay) {
            //收款
            if (MainApplication.isAdmin == "0" && !isCashierShow) { //收银员
                PrintClient.getInstance()
                    ?.printTextLeft(ToastHelper.toStr(R.string.tx_user) + "：" + MainApplication.realName)
            }
            if (isCashierShow) {
                PrintClient.getInstance()
                    ?.printTextLeft(ToastHelper.toStr(R.string.tx_user) + "：" + orderModel.getUserName())
            }
        }

        PrintClient.getInstance()?.printTextLeft(orderModel.getPartner())
        PrintClient.getInstance()?.printTextLeft(ToastHelper.toStr(R.string.tv_pay_client_save))
        PrintClient.getInstance()?.printTextCenter("==========================")

        if (!StringUtil.isEmptyOrNull(MainApplication.getMchName())) {
            PrintClient.getInstance()?.printTextLeft(ToastHelper.toStr(R.string.shop_name))
            PrintClient.getInstance()?.printTextLeft(MainApplication.getMchName())
        }

        if (!StringUtil.isEmptyOrNull(MainApplication.getMchId())) {
            PrintClient.getInstance()
                ?.printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "：" + MainApplication.getMchId())
        }

        if (orderModel.isPay) {
            //收款
            PrintClient.getInstance()
                ?.printTextLeft(ToastHelper.toStr(R.string.tx_bill_stream_time) + "：")
            PrintClient.getInstance()?.printTextLeft(orderModel.addTimeNew)
        } else {
            //退款
            PrintClient.getInstance()
                ?.printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_refund_time) + "：")
            PrintClient.getInstance()?.printTextLeft(orderModel.addTimeNew)
            PrintClient.getInstance()
                ?.printTextLeft(ToastHelper.toStr(R.string.refund_odd_numbers) + "：")
            PrintClient.getInstance()?.printTextLeft(orderModel.refundNo)
        }


        PrintClient.getInstance()?.printTextLeft(ToastHelper.toStr(R.string.tx_order_no) + ": ")
        PrintClient.getInstance()?.printTextLeft(orderModel.orderNoMch)


        if (orderModel.isPay) {
            //收款
            if (MainApplication.getPayTypeMap() != null && MainApplication.getPayTypeMap()
                    .isNotEmpty()
            ) {
                PrintClient.getInstance()?.printTextLeft(
                    MainApplication.getPayTypeMap()[orderModel.apiCode].toString() + " " + ToastHelper.toStr(
                        R.string.tx_orderno
                    )
                )
            }

            PrintClient.getInstance()?.printTextLeft(orderModel.getTransactionId())
            PrintClient.getInstance()
                ?.printTextLeft(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：")
            PrintClient.getInstance()?.printTextLeft(orderModel.getTradeName())
            PrintClient.getInstance()
                ?.printTextLeft(ToastHelper.toStr(R.string.tx_bill_stream_statr) + "：")

            if (MainApplication.getPayTypeMap() != null && MainApplication.getPayTypeMap()
                    .isNotEmpty()
            ) {
                PrintClient.getInstance()?.printTextLeft(
                    MainApplication.getTradeTypeMap()[orderModel.getTradeState()
                        .toString() + ""]
                )
            }


            if (!StringUtil.isEmptyOrNull(orderModel.attach)) {
                PrintClient.getInstance()
                    ?.printTextLeft(ToastHelper.toStr(R.string.tx_bill_stream_attach) + "：" + " " + orderModel.attach)
            }

            if (MainApplication.feeFh.equals(
                    "¥",
                    ignoreCase = true
                ) && MainApplication.getFeeType()
                    .equals(ToastHelper.toStr(R.string.pay_yuan), ignoreCase = true)
            ) {
                if (orderModel.daMoney > 0) {
                    val actualMoney = orderModel.daMoney + orderModel.orderFee

                    PrintClient.getInstance()
                        ?.printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_money) + "：")
                    PrintClient.getInstance()?.printTextRight(
                        DateUtil.formatRMBMoneyUtils(actualMoney.toDouble()) + ToastHelper.toStr(
                            R.string.pay_yuan
                        )
                    )
                } else {
                    PrintClient.getInstance()
                        ?.printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_money) + "：")
                    PrintClient.getInstance()?.printTextRight(
                        DateUtil.formatRMBMoneyUtils(orderModel.orderFee.toDouble()) + ToastHelper.toStr(
                            R.string.pay_yuan
                        )
                    )
                }
            } else {
                if (MainApplication.isSurchargeOpen()) {
                    if (orderModel.tradeType != "pay.alipay.auth.micropay.freeze" && orderModel.tradeType != "pay.alipay.auth.native.freeze") {

                        PrintClient.getInstance()
                            ?.printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_money) + "：")
                        PrintClient.getInstance()?.printTextRight(
                            MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.orderFee.toDouble())
                        )
                        PrintClient.getInstance()
                            ?.printTextLeft(ToastHelper.toStr(R.string.tx_surcharge) + "：")
                        PrintClient.getInstance()?.printTextRight(
                            MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.surcharge.toDouble())
                        )
                    }
                }

                //Uplan ---V3.0.5迭代新增

                //如果为预授权的支付类型，则不展示任何优惠相关的信息
                /*因目前SPAY與網關支持的預授權接口僅為‘支付寶’預授權，
                遂與銀聯Uplan不衝突，遂預授權頁面內，無需處理該優惠信息。*/if (!TextUtils.isEmpty(orderModel.tradeType) && orderModel.tradeType != "pay.alipay.auth.micropay.freeze" && orderModel.tradeType != "pay.alipay.auth.native.freeze") {
                    // 优惠详情 --- 如果列表有数据，则展示，有多少条，展示多少条
                    if (orderModel.getUplanDetailsBeans() != null && orderModel.getUplanDetailsBeans().size > 0) {
                        for (i in orderModel.getUplanDetailsBeans().indices) {
                            var string_Uplan_details: String
                            //如果是Uplan Discount或者Instant Discount，则采用本地词条，带不同语言的翻译
                            string_Uplan_details =
                                if (orderModel.getUplanDetailsBeans()[i].getDiscountNote()
                                        .equals("Uplan discount", ignoreCase = true)
                                ) {
                                    ToastHelper.toStr(R.string.uplan_Uplan_discount) + "："
                                } else if (orderModel.getUplanDetailsBeans()[i].getDiscountNote()
                                        .equals("Instant Discount", ignoreCase = true)
                                ) {
                                    ToastHelper.toStr(R.string.uplan_Uplan_instant_discount) + "："
                                } else { //否则，后台传什么展示什么
                                    orderModel.getUplanDetailsBeans()[i].getDiscountNote() + "："
                                }

                            PrintClient.getInstance()?.printTextLeft(string_Uplan_details)
                            PrintClient.getInstance()
                                ?.printTextRight(MainApplication.feeType + " " + "-" + orderModel.getUplanDetailsBeans()[i].getDiscountAmt())
                        }
                    }

                    // 消费者实付金额 --- 如果不为0 ，就展示
                    if (orderModel.costFee != 0L) {
                        PrintClient.getInstance()
                            ?.printTextLeft(ToastHelper.toStr(R.string.uplan_Uplan_actual_paid_amount) + "：")
                        PrintClient.getInstance()?.printTextRight(
                            MainApplication.feeType + " " + DateUtil.formatMoneyUtils(orderModel.costFee.toDouble())
                        )
                    }
                }

                PrintClient.getInstance()
                    ?.printTextLeft(ToastHelper.toStr(R.string.tv_charge_total) + "：")
                PrintClient.getInstance()?.printTextRight(
                    MainApplication.feeType + DateUtil.formatMoneyUtils(
                        orderModel.getTotalFee().toDouble()
                    )
                )
                if (orderModel.cashFeel > 0) {
                    PrintClient.getInstance()?.printTextRight(
                        ToastHelper.toStr(R.string.pay_yuan) + DateUtil.formatRMBMoneyUtils(
                            orderModel.cashFeel.toDouble()
                        )
                    )
                }
            }
        } else {
            //退款
            PrintClient.getInstance()
                ?.printTextLeft(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：")
            PrintClient.getInstance()?.printTextLeft(orderModel.getTradeName())

            if (!StringUtil.isEmptyOrNull(orderModel.getUserName())) {

                PrintClient.getInstance()
                    ?.printTextLeft(ToastHelper.toStr(R.string.tv_refund_peop) + "：")
                PrintClient.getInstance()?.printTextLeft(orderModel.getUserName())
            }
            if (MainApplication.getRefundStateMap() != null && MainApplication.getRefundStateMap()
                    .isNotEmpty()
            ) {
                PrintClient.getInstance()
                    ?.printTextLeft(ToastHelper.toStr(R.string.tv_refund_state) + "：")
                PrintClient.getInstance()
                    ?.printTextLeft(MainApplication.getRefundStateMap()[orderModel.refundState.toString() + ""])
            }
            if (MainApplication.feeFh.equals(
                    "¥",
                    ignoreCase = true
                ) && MainApplication.getFeeType()
                    .equals(ToastHelper.toStr(R.string.pay_yuan), ignoreCase = true)
            ) {
                PrintClient.getInstance()
                    ?.printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_money) + "：")
                PrintClient.getInstance()?.printTextRight(
                    DateUtil.formatRMBMoneyUtils(
                        orderModel.getTotalFee().toDouble()
                    ) + ToastHelper.toStr(R.string.pay_yuan)
                )
                PrintClient.getInstance()
                    ?.printTextLeft(ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "：")
                PrintClient.getInstance()?.printTextRight(
                    DateUtil.formatRMBMoneyUtils(orderModel.refundMoney.toDouble()) + ToastHelper.toStr(
                        R.string.pay_yuan
                    )
                )
            } else {

                PrintClient.getInstance()
                    ?.printTextRight(ToastHelper.toStr(R.string.tv_charge_total) + "：")
                PrintClient.getInstance()?.printTextLeft(
                    MainApplication.feeType + DateUtil.formatMoneyUtils(
                        orderModel.getTotalFee().toDouble()
                    )
                )
                PrintClient.getInstance()
                    ?.printTextRight(ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "：")
                PrintClient.getInstance()?.printTextRight(
                    MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.refundMoney.toDouble())
                )

                if (orderModel.cashFeel > 0) {
                    PrintClient.getInstance()?.printTextRight(
                        ToastHelper.toStr(R.string.pay_yuan) + DateUtil.formatMoneyUtils(
                            orderModel.cashFeel.toDouble()
                        )
                    )
                }
            }
            if (!StringUtil.isEmptyOrNull(orderModel.printInfo)) {
                PrintClient.getInstance()?.printTextLeft(orderModel.printInfo)
            }
        }



        if (!StringUtil.isEmptyOrNull(orderModel.orderNoMch) && orderModel.isPay) {
            //打印小票的代码屏蔽
            PrintClient.getInstance()?.printQRCode(orderModel.orderNoMch)
            PrintClient.getInstance()
                ?.printTextCenter(ToastHelper.toStr(R.string.refound_QR_code))
//                page.addLine().addUnit(c, EAlign.CENTER)
        }

        PrintClient.getInstance()
            ?.printMultiLines("-------------------------------------------------------------")

        PrintClient.getInstance()?.printTextLeft(
            ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(
                System.currentTimeMillis()
            )
        )

        if (orderModel.getPartner()
                .equals(ToastHelper.toStr(R.string.tv_pay_mch_stub), ignoreCase = true)
            || orderModel.getPartner()
                .equals(ToastHelper.toStr(R.string.tv_pay_user_stub), ignoreCase = true)
        ) {
            PrintClient.getInstance()?.printEmptyLine()
            PrintClient.getInstance()?.printEmptyLine()
            PrintClient.getInstance()?.printEmptyLine()
        } else {
            PrintClient.getInstance()?.printEmptyLine()
            PrintClient.getInstance()?.printEmptyLine()
            PrintClient.getInstance()?.printEmptyLine()
        }

        PrintClient.getInstance()?.startPrint()
    }

    fun printPreAuthStr(printType: Int, orderModel: Order, context: Context? = null) {
        when (Build.MODEL) {
            Constants.TYPE_A8 -> PrintClient.getInstance()?.bindDeviceService()
            Constants.TYPE_A920 -> PrintA920Text.getInstance()?.init(context)
        }
        var minus = 1
        //根据当前的小数点的位数来判断应该除以或者乘以多少
        for (i in 0 until MainApplication.numFixed) {
            minus *= 10
        }
        try {
            when (printType) {
                1, 4, 5 ->
                    PrintClient.getInstance()
                        ?.printTextCenter(ToastHelper.toStr(R.string.pre_auth_receipt))
                2, 3 ->
                    PrintClient.getInstance()
                        ?.printTextCenter(ToastHelper.toStr(R.string.pre_auth_unfreezed_receipt))
            }
            PrintClient.getInstance()?.printEmptyLine()
            PrintClient.getInstance()?.printTextLeft(orderModel.getPartner())
            PrintClient.getInstance()
                ?.printTextLeft(ToastHelper.toStr(R.string.tv_pay_client_save))
            PrintClient.getInstance()?.printTextCenter("==========================")

            if (!StringUtil.isEmptyOrNull(MainApplication.getMchName())) {
                PrintClient.getInstance()?.printTextLeft(ToastHelper.toStr(R.string.shop_name))
                PrintClient.getInstance()?.printTextLeft(MainApplication.getMchName())
                PrintClient.getInstance()?.printTextLeft(MainApplication.getMchName())
            }
            if (!StringUtil.isEmptyOrNull(MainApplication.getMchId())) {
                PrintClient.getInstance()
                    ?.printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "：" + MainApplication.getMchId())
            }
            if (printType == 1) {
                PrintClient.getInstance()
                    ?.printTextLeft(ToastHelper.toStr(R.string.pre_auth_time) + "：")
                PrintClient.getInstance()?.printTextLeft(orderModel.tradeTime)
            } else if (printType == 4) {
                PrintClient.getInstance()
                    ?.printTextLeft(ToastHelper.toStr(R.string.pre_auth_time) + "：")
                PrintClient.getInstance()?.printTextLeft(orderModel.getTradeTimeNew())
            } else if (printType == 5) {
                PrintClient.getInstance()
                    ?.printTextLeft(ToastHelper.toStr(R.string.pre_auth_time) + "：")
                PrintClient.getInstance()?.printTextLeft(orderModel.getTimeEnd())
            } else if (printType == 2) {
                PrintClient.getInstance()
                    ?.printTextLeft(ToastHelper.toStr(R.string.unfreezed_time) + "：")
                PrintClient.getInstance()?.printTextLeft(orderModel.operateTime)
            } else if (printType == 3) {
                PrintClient.getInstance()
                    ?.printTextLeft(ToastHelper.toStr(R.string.unfreezed_time) + "：")
                PrintClient.getInstance()?.printTextLeft(orderModel.unFreezeTime)
            }
            if (printType == 1 || printType == 4 || printType == 5) {
                PrintClient.getInstance()
                    ?.printTextLeft(ToastHelper.toStr(R.string.platform_pre_auth_order_id) + "：")
                PrintClient.getInstance()?.printTextLeft(orderModel.getAuthNo())
                val language = PreferenceUtil.getString("language", "")
                val locale =
                    MainApplication.getContext().resources.configuration.locale //zh-rHK
                val lan = locale.country
                if (!TextUtils.isEmpty(language)) {
                    if (language == MainApplication.LANG_CODE_EN_US) {
                        PrintClient.getInstance()?.printTextLeft(
                            MainApplication.getPayTypeMap()["2"].toString() + " " + ToastHelper.toStr(
                                R.string.tx_orderno
                            ) + "："
                        )
                    } else {
                        PrintClient.getInstance()?.printTextLeft(
                            MainApplication.getPayTypeMap()["2"].toString() + ToastHelper.toStr(
                                R.string.tx_orderno
                            ) + "："
                        )
                    }
                } else {
                    if (lan.equals("en", ignoreCase = true)) {
                        PrintClient.getInstance()?.printTextLeft(
                            MainApplication.getPayTypeMap()["2"].toString() + " " + ToastHelper.toStr(
                                R.string.tx_orderno
                            ) + "："
                        )
                    } else {
                        PrintClient.getInstance()?.printTextLeft(
                            MainApplication.getPayTypeMap()["2"].toString() + ToastHelper.toStr(
                                R.string.tx_orderno
                            ) + "："
                        )
                    }
                }
                if (printType == 1 || printType == 5) {
                    PrintClient.getInstance()?.printTextLeft(orderModel.getOutTransactionId())
                }
                if (printType == 4) {
                    PrintClient.getInstance()?.printTextLeft(orderModel.getTransactionId())
                }
            } else if (printType == 2 || printType == 3) {
                PrintClient.getInstance()
                    ?.printTextLeft(ToastHelper.toStr(R.string.unfreezed_order_id) + "：")
                PrintClient.getInstance()?.printTextLeft(orderModel.outRequestNo)
                PrintClient.getInstance()
                    ?.printTextLeft(ToastHelper.toStr(R.string.platform_pre_auth_order_id) + "：")
                PrintClient.getInstance()?.printTextLeft(orderModel.getAuthNo())
            }
            if (printType == 5) {
                val tradename: String
                tradename =
                    if (!KotlinUtils.isAbsoluteNullStr(orderModel.getTradeName())) { //先判断大写的Name，再判断小写name
                        orderModel.getTradeName()
                    } else {
                        orderModel.tradename
                    }
                PrintClient.getInstance()
                    ?.printTextLeft(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：")
                PrintClient.getInstance()?.printTextLeft(tradename)
            } else {
                PrintClient.getInstance()
                    ?.printTextLeft(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：")
                PrintClient.getInstance()?.printTextLeft(orderModel.getTradeName())
            }
            if (printType == 3) {
                PrintClient.getInstance()
                    ?.printTextLeft(ToastHelper.toStr(R.string.tv_unfreezen_applicant) + "：")
                PrintClient.getInstance()?.printTextLeft(orderModel.getUserName())
            }
            PrintClient.getInstance()
                ?.printTextLeft(ToastHelper.toStr(R.string.tx_bill_stream_statr) + "：")
            var operationType: String? = ""
            if (printType == 1 || printType == 4) {
                operationType =
                    MainApplication.getPreAuthtradeStateMap()[orderModel.getTradeState()
                        .toString() + ""]
            } else if (printType == 2) {
                when (orderModel.operationType) {
                    2 -> operationType = ToastHelper.toStr(R.string.freezen_success)
                }
            } else if (printType == 3) {
                operationType = ToastHelper.toStr(R.string.unfreezing)
            } else if (printType == 5) {
                operationType = ToastHelper.toStr(R.string.pre_auth_authorized)
            }
            PrintClient.getInstance()?.printTextLeft(operationType)
            when (printType) {
                1, 4, 5 -> {
                    if (MainApplication.feeFh.equals(
                            "¥",
                            ignoreCase = true
                        ) && MainApplication.getFeeType()
                            .equals(ToastHelper.toStr(R.string.pay_yuan), ignoreCase = true)
                    ) {
                        PrintClient.getInstance()
                            ?.printTextLeft(ToastHelper.toStr(R.string.pre_auth_amount) + ":")
                        PrintClient.getInstance()?.printTextLeft(
                            DateUtil.formatRMBMoneyUtils(orderModel.cashFeel.toDouble()) + ToastHelper.toStr(
                                R.string.pay_yuan
                            )
                        )
                    } else {
                        var preAuthorizationMoney: Long = 0 //用来接收printType判断授权金额来自反扫、正扫
                        if (printType == 1) {
                            preAuthorizationMoney = orderModel.restAmount //反扫
                        } else if (printType == 5) {
                            preAuthorizationMoney = orderModel.getTotalFee() //正扫
                        } else if (printType == 4) {
                            preAuthorizationMoney = orderModel.totalFreezeAmount //预授权详情
                        }
                        PrintClient.getInstance()
                            ?.printTextLeft(ToastHelper.toStr(R.string.pre_auth_amount) + ":")
                        PrintClient.getInstance()?.printTextLeft(
                            MainApplication.feeType + DateUtil.formatMoneyUtils(
                                preAuthorizationMoney.toDouble()
                            )
                        )
                    }
                    PrintClient.getInstance()?.printEmptyLine()
                    PrintClient.getInstance()?.printEmptyLine()

                    //二维码
                    PrintClient.getInstance()?.printQRCode(orderModel.getAuthNo())
                    PrintClient.getInstance()?.printTextLeft(orderModel.printInfo)
                    PrintClient.getInstance()
                        ?.printTextCenter(ToastHelper.toStr(R.string.scan_to_check))
                }
                2 -> {
                    if (MainApplication.feeFh.equals(
                            "¥",
                            ignoreCase = true
                        ) && MainApplication.getFeeType()
                            .equals(ToastHelper.toStr(R.string.pay_yuan), ignoreCase = true)
                    ) {
                        PrintClient.getInstance()
                            ?.printTextLeft(ToastHelper.toStr(R.string.unfreezing_amount) + ":")
                        PrintClient.getInstance()?.printTextRight(
                            DateUtil.formatRMBMoneyUtils(orderModel.cashFeel.toDouble()) + ToastHelper.toStr(
                                R.string.pay_yuan
                            )
                        )
                    } else {
                        PrintClient.getInstance()
                            ?.printTextLeft(ToastHelper.toStr(R.string.unfreezing_amount) + ":")
                        PrintClient.getInstance()?.printTextRight(
                            MainApplication.feeType + DateUtil.formatMoneyUtils(
                                orderModel.getMoney().toDouble()
                            )
                        )
                    }
                    PrintClient.getInstance()
                        ?.printTextLeft(ToastHelper.toStr(R.string.unfreeze)) //解冻将原路返回支付账户或银行卡，到账时间已第三方为准
                }
                3 -> {
                    //授权金额
                    if (MainApplication.feeFh.equals(
                            "¥",
                            ignoreCase = true
                        ) && MainApplication.getFeeType()
                            .equals(ToastHelper.toStr(R.string.pay_yuan), ignoreCase = true)
                    ) {
                        PrintClient.getInstance()
                            ?.printTextLeft(ToastHelper.toStr(R.string.pre_auth_amount) + ":")
                        PrintClient.getInstance()?.printTextRight(
                            DateUtil.formatRMBMoneyUtils(orderModel.cashFeel.toDouble()) + ToastHelper.toStr(
                                R.string.pay_yuan
                            )
                        )
                    } else {
                        PrintClient.getInstance()
                            ?.printTextLeft(ToastHelper.toStr(R.string.pre_auth_amount) + ":")
                        PrintClient.getInstance()?.printTextRight(
                            MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.totalFreezeAmount.toDouble())
                        )
                    }
                    //解冻金额
                    if (MainApplication.feeFh.equals(
                            "¥",
                            ignoreCase = true
                        ) && MainApplication.getFeeType()
                            .equals(ToastHelper.toStr(R.string.pay_yuan), ignoreCase = true)
                    ) {
                        PrintClient.getInstance()
                            ?.printTextLeft(ToastHelper.toStr(R.string.unfreezing_amount) + ":")
                        PrintClient.getInstance()?.printTextRight(
                            DateUtil.formatRMBMoneyUtils(orderModel.cashFeel.toDouble()) + ToastHelper.toStr(
                                R.string.pay_yuan
                            )
                        )
                    } else {
                        PrintClient.getInstance()
                            ?.printTextLeft(ToastHelper.toStr(R.string.unfreezing_amount) + ":")
                        PrintClient.getInstance()?.printTextRight(
                            MainApplication.feeType + DateUtil.formatMoneyUtils(
                                orderModel.getTotalFee().toDouble()
                            )
                        )
                    }
                    PrintClient.getInstance()
                        ?.printTextLeft(ToastHelper.toStr(R.string.unfreeze)) //解冻将原路返回支付账户或银行卡，到账时间已第三方为准
                }
                else -> {}
            }

            PrintClient.getInstance()
                ?.printTextCenter("-------------------------------------------------------------")
            PrintClient.getInstance()?.printTextLeft(
                ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(
                    System.currentTimeMillis()
                )
            )
            if (orderModel.getPartner()
                    .equals(ToastHelper.toStr(R.string.tv_pay_mch_stub), ignoreCase = true)
                || orderModel.getPartner()
                    .equals(ToastHelper.toStr(R.string.tv_pay_user_stub), ignoreCase = true)
            ) {
                PrintClient.getInstance()?.printEmptyLine()
                PrintClient.getInstance()?.printEmptyLine()
                PrintClient.getInstance()?.printEmptyLine()
            } else {
                PrintClient.getInstance()?.printEmptyLine()
                PrintClient.getInstance()?.printEmptyLine()
                PrintClient.getInstance()?.printEmptyLine()
            }
            PrintClient.getInstance()?.startPrint()
        } catch (e: PrinterDevException) {
            e.printStackTrace()
        }
    }

    fun printSummary(info: OrderTotalInfo, context: Context? = null) {
        when (Build.MODEL) {
            Constants.TYPE_A8 -> PrintClient.getInstance()?.bindDeviceService()
            Constants.TYPE_A920 -> PrintA920Text.getInstance()?.init(context)
        }
        PrintClient.getInstance()
            ?.printTextCenter(ToastHelper.toStr(R.string.tx_blue_print_data_sum))
        PrintClient.getInstance()?.printEmptyLine()
        PrintClient.getInstance()?.printTextLeft(ToastHelper.toStr(R.string.shop_name) + "：")

        if (!StringUtil.isEmptyOrNull(MainApplication.getMchName())) {
            PrintClient.getInstance()?.printTextLeft(MainApplication.getMchName())
        }

        PrintClient.getInstance()
            ?.printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "：" + MainApplication.getMchId())

        /*if (!StringUtil.isEmptyOrNull(MainApplication.getMchId())) {
        page.addLine().addUnit(, 24, EAlign.LEFT);
    }*/

        /*if (!StringUtil.isEmptyOrNull(MainApplication.getMchId())) {
        page.addLine().addUnit(, 24, EAlign.LEFT);
    }*/if (StringUtil.isEmptyOrNull(info.userName)) {
            //            printBuffer.append("收银员：全部收银员\n");
        } else {
            PrintClient.getInstance()?.printTextLeft(ToastHelper.toStr(R.string.tx_user) + "：")
            PrintClient.getInstance()?.printTextLeft(info.userName)
        }

        PrintClient.getInstance()
            ?.printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_start_time) + "：" + info.startTime)
        PrintClient.getInstance()
            ?.printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_end_time) + "：" + info.endTime)
        PrintClient.getInstance()?.printTextCenter("==========================")

        if (MainApplication.feeFh.equals("¥", ignoreCase = true) && MainApplication.getFeeType()
                .equals(ToastHelper.toStr(R.string.pay_yuan), ignoreCase = true)
        ) {
            PrintClient.getInstance()?.printTextLeft(
                ToastHelper.toStr(R.string.tx_bill_stream_pay_money) + "：" + DateUtil.formatMoneyUtils(
                    info.countTotalFee.toDouble()
                ) + ToastHelper.toStr(R.string.pay_yuan)
            )
        } else {
            PrintClient.getInstance()?.printTextLeft(
                ToastHelper.toStr(R.string.tx_bill_stream_pay_money) + "：" + MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(
                    info.countTotalFee.toDouble()
                )
            )
        }
        PrintClient.getInstance()
            ?.printTextLeft(ToastHelper.toStr(R.string.tx_bill_stream_pay_money_num) + "：" + info.countTotalCount)

        if (MainApplication.feeFh.equals("¥", ignoreCase = true) && MainApplication.getFeeType()
                .equals(ToastHelper.toStr(R.string.pay_yuan), ignoreCase = true)
        ) {
            PrintClient.getInstance()?.printTextLeft(
                ToastHelper.toStr(R.string.tv_refund_dialog_refund_money) + DateUtil.formatMoneyUtils(
                    info.countTotalRefundFee.toDouble()
                ) + ToastHelper.toStr(R.string.pay_yuan)
            )
        } else {
            PrintClient.getInstance()?.printTextLeft(
                ToastHelper.toStr(R.string.tv_refund_dialog_refund_money) + MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(
                    info.countTotalRefundFee.toDouble()
                )
            )
        }
        PrintClient.getInstance()
            ?.printTextLeft(ToastHelper.toStr(R.string.tx_bill_reufun_total_num) + "：" + info.countTotalRefundCount)

        if (MainApplication.feeFh.equals("¥", ignoreCase = true) && MainApplication.getFeeType()
                .equals(ToastHelper.toStr(R.string.pay_yuan), ignoreCase = true)
        ) {
            PrintClient.getInstance()?.printTextLeft(
                ToastHelper.toStr(R.string.tx_blue_print_pay_total) + "：" + DateUtil.formatMoneyUtils(
                    (info.countTotalFee - info.countTotalRefundFee).toDouble()
                ) + ToastHelper.toStr(R.string.pay_yuan)
            )
        } else {
            PrintClient.getInstance()?.printTextLeft(
                ToastHelper.toStr(R.string.tx_blue_print_pay_total) + "：" + MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(
                    (info.countTotalFee - info.countTotalRefundFee).toDouble()
                )
            )
        }
        PrintClient.getInstance()?.printTextCenter("==========================")

        /**
         * 是否开通小费功能，打印小费金额、总计
         */
        /**
         * 是否开通小费功能，打印小费金额、总计
         */
        if (MainApplication.isTipOpenFlag()) {
            // 小费金额
            PrintClient.getInstance()?.printTextLeft(
                ToastHelper.toStr(R.string.tip_amount) + "：" + MainApplication.getFeeType() + DateUtil.formatMoneyUtils(
                    info.countTotalTipFee.toDouble()
                )
            )
            PrintClient.getInstance()
                ?.printTextLeft(ToastHelper.toStr(R.string.tip_count) + "：" + info.countTotalTipFeeCount)
            PrintClient.getInstance()?.printTextCenter("==========================")
        }

        val list = info.orderTotalItemInfo

        val map: MutableMap<Int, Any> = HashMap()

        for (itemInfo in list) {
            when (itemInfo.payTypeId) {
                1 -> map[0] = itemInfo
                2 -> map[1] = itemInfo
                4 -> map[2] = itemInfo
                12 -> map[3] = itemInfo
                else -> map[itemInfo.payTypeId] = itemInfo
            }
        }

        for (key in map.keys) {
            val itemInfo = map[key] as OrderTotalItemInfo?
            if (MainApplication.feeFh.equals(
                    "¥",
                    ignoreCase = true
                ) && MainApplication.getFeeType()
                    .equals(ToastHelper.toStr(R.string.pay_yuan), ignoreCase = true)
            ) {
                PrintClient.getInstance()
                    ?.printTextLeft(MainApplication.getPayTypeMap()[itemInfo!!.payTypeId.toString()])
                PrintClient.getInstance()?.printTextLeft(
                    ToastHelper.toStr(R.string.tx_money) + DateUtil.formatMoneyUtils(
                        itemInfo!!.successFee.toDouble()
                    ) + ToastHelper.toStr(R.string.pay_yuan)
                )
            } else {
                PrintClient.getInstance()
                    ?.printTextLeft(MainApplication.getPayTypeMap()[itemInfo!!.payTypeId.toString()])
                PrintClient.getInstance()?.printTextLeft(
                    ToastHelper.toStr(R.string.tx_money) + MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(
                        itemInfo!!.successFee.toDouble()
                    )
                )
            }
            PrintClient.getInstance()
                ?.printTextLeft(ToastHelper.toStr(R.string.tv_settle_count) + "：" + itemInfo!!.successCount)
        }

        PrintClient.getInstance()
            ?.printTextCenter("-------------------------------------------------------------")

        PrintClient.getInstance()?.printTextLeft(
            ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(
                System.currentTimeMillis()
            )
        )

        try {
            PrintClient.getInstance()?.printEmptyLine()
            PrintClient.getInstance()?.printEmptyLine()
            PrintClient.getInstance()?.printEmptyLine()
            PrintClient.getInstance()?.startPrint()
        } catch (e: PrinterDevException) {
            e.printStackTrace()
        }
    }

}