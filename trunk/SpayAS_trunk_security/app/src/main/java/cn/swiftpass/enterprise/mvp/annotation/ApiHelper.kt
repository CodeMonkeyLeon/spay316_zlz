package cn.swiftpass.enterprise.mvp.annotation

import cn.swiftpass.enterprise.io.okhttp.RequestUtil
import java.lang.reflect.InvocationHandler
import java.lang.reflect.Method
import java.lang.reflect.Proxy

/**
 * @author lizheng.zhao
 * @date 2022/12/14
 *
 * @description 注解解析工具类类
 */
object ApiHelper {

    fun <T> getApi(c: Class<T>): T {
        return Proxy.newProxyInstance(c.classLoader, arrayOf<Class<*>>(c), ApiProxyHandler()) as T
    }


    class ApiProxyHandler : InvocationHandler {
        override fun invoke(proxy: Any, method: Method, args: Array<Any>): Any {
            val returnType = method.returnType
            //判断返回值类型
            return if (returnType == RequestUtil::class.java) {
                RequestUtil(
                    AnnotationParser.parse(
                        method,
                        args
                    )
                )
            } else {
                throw RuntimeException("The return value type of the \"" + method.name + "\" method must be " + RequestUtil::class.java)
            }
        }
    }
}