package cn.swiftpass.enterprise.ui.activity.shop;

import java.io.File;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;

public class ImagePreviewActivity extends BaseActivity implements OnClickListener
{
    private final String TAG = "ImagePreviewActivity";
    
    /** 异步加载设置图片 */
    private static final int SET_IMG_ASYNC = 1001;
    
    private Uri imgUri;
    
    private ImageView image_preview;
    
    private Button image_preview_commit;
    
    private Button image_preview_cancel;
    
    private Handler imgHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            super.handleMessage(msg);
            switch (msg.what)
            {
                case SET_IMG_ASYNC:
                {
                    if (null == msg.obj)
                    {
                        Toast.makeText(ImagePreviewActivity.this,
                            ImagePreviewActivity.this.getResources().getString(R.string.upload_file_format_error),
                            Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        image_preview.setImageBitmap((Bitmap)msg.obj);
                        msg.obj = null;
                    }
                    break;
                }
                default:
                    break;
            }
        }
    };
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_preview_commit);
        initView();
        initBundle();
        new Thread()
        {
            @Override
            public void run()
            {
                Bitmap bitmap = getPreviewImage(imgUri);
                if (bitmap != null)
                {
                    imgHandler.sendMessage(imgHandler.obtainMessage(SET_IMG_ASYNC, bitmap));
                }
            }
            
        }.start();
        //屏幕长亮
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }
    
    /**
     * 取得传递参数值
     */
    private void initBundle()
    {
        Intent intent = getIntent();
        imgUri = intent.getData();
    }
    
    /**
     * 初始化UI控件
     */
    private void initView()
    {
        image_preview = (ImageView)findViewById(R.id.image_preview);
        image_preview_commit = (Button)findViewById(R.id.image_preview_commit);
        image_preview_cancel = (Button)findViewById(R.id.image_preview_cancel);
        
        image_preview_commit.setOnClickListener(this);
        image_preview_cancel.setOnClickListener(this);
    }
    
    @Override
    public void onClick(View v)
    {
        // TODO Auto-generated method stub
        int id = v.getId();
        switch (id)
        {
            case R.id.image_preview_commit:
                ShopkeeperNextActivity.IMAGE_PREVIEW = false;
                Intent cancelIntent = new Intent();
                setResult(RESULT_OK, cancelIntent);
                ImagePreviewActivity.this.finish();
                break;
            case R.id.image_preview_cancel:
                ShopkeeperNextActivity.cancel_perview = false;
                finish();
                break;
            default:
                break;
        }
    }
    
    @Override
    protected void onResume()
    {
        super.onResume();
    }
    
    @Override
    protected void onStop()
    {
        super.onStop();
    }
    
    @Override
    protected void onDestroy()
    {
        // TODO Auto-generated method stub
        super.onDestroy();
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
        }
        return super.onKeyDown(keyCode, event);
    }
    
    @Override
    protected void onPause()
    {
        super.onPause();
    }
    
    /**
     * 方法表述 通过uri获取图片
     * @param context
     * @param uri
     * @return Bitmap
     * Bitmap
     */
    @SuppressLint("NewApi")
    private Bitmap getPreviewImage(Uri uri)
    {
        Bitmap bitmap = null;
        if (uri == null)
        {
            return bitmap;
        }
        String path = "";
        // 获取选中图片地址
        if ((uri + "").contains("/sdcard"))
        {
            path = uri.toString().substring(uri.toString().indexOf("/sdcard"));
        }
        else if ((uri + "").contains("/data"))
        {
            path = uri.toString().substring(uri.toString().indexOf("/data"));
        }
        else
        {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT
                && uri.toString().contains("documents"))
            {
                String wholeID = DocumentsContract.getDocumentId(uri);
                String id = wholeID.split(":")[1];
                String[] column = {MediaStore.Images.Media.DATA};
                String sel = MediaStore.Images.Media._ID + " = ?";
                Cursor cursor =
                    ImagePreviewActivity.this.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column,
                        sel,
                        new String[] {id},
                        null);
                int columnIndex = cursor.getColumnIndex(column[0]);
                if (cursor.moveToFirst())
                {
                    path = cursor.getString(columnIndex);
                }
                cursor.close();
            }
            else
            {
                Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                //            String[] proj = {MediaStore.Images.Media.DATA, MediaColumns.DATA};
                //            Cursor cursor = managedQuery(uri, proj, null, null, null);
                
                if (cursor != null)
                {
                    int index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    
                    //                int index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    
                    cursor.moveToFirst();
                    /** 返回图片地址 */
                    path = cursor.getString(index);
                    cursor.close();
                    cursor = null;
                }
            }
        }
        
        // 通过path获取bitmap
        File file = new File(path);
        if (file.exists() && file.length() / 1024 > 100)
        {
            bitmap = ImagePase.createBitmap(path, ImagePase.bitmapSize_Image);
        }
        else
        {
            //            bitmap = BitmapFactory.decodeFile(path);
            bitmap = ImagePase.readBitmapFromStream(path);
        }
        return bitmap;
    }
    
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
}
