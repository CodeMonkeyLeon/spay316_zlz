package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.FindPassSubmitContract
import cn.swiftpass.enterprise.utils.AESHelper
import cn.swiftpass.enterprise.utils.JsonUtil
import cn.swiftpass.enterprise.utils.MD5
import cn.swiftpass.enterprise.utils.SharedPreUtils
import com.example.common.entity.EmptyData
import okhttp3.Call
import java.util.*

/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @在醒来时
 * @世界都远了
 * @我需要
 * @最狂的风
 * @和最静的海。
 */
class FindPassSubmitPresenter : FindPassSubmitContract.Presenter {

    private var mView: FindPassSubmitContract.View? = null


    override fun ecdhKeyExchange(publicKey: String?) {
        mView?.let { view ->
            AppClient.ecdhKeyExchange(
                publicKey,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<ECDHInfo>(ECDHInfo()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        commonResponse?.let {
                            view.ecdhKeyExchangeFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        r?.let { response ->
                            val info = JsonUtil.jsonToBean(
                                response.message,
                                ECDHInfo::class.java
                            ) as ECDHInfo
                            MainApplication.getInstance().setSerPubKey(info.serPubKey)
                            MainApplication.getInstance().setSKey(info.skey)
                            view.ecdhKeyExchangeFailed(info)
                        }
                    }
                }
            )
        }
    }

    override fun checkData(
        token: String?,
        emailCode: String?,
        password: String?
    ) {
        mView?.let { view ->

            view.showLoading(R.string.tx_confirm_loading, ParamsConstants.COMMON_LOADING)
            val priKey = SharedPreUtils.readProduct(ParamsConstants.SECRET_KEY) as String?
            val paseStr = MD5.md5s(priKey).uppercase(Locale.getDefault())

            AppClient.checkData(
                token,
                emailCode,
                MainApplication.getInstance().getSKey(),
                AESHelper.aesEncrypt(password, paseStr.substring(8, 24)),
                AESHelper.aesEncrypt(password, paseStr.substring(8, 24)),
                "true",
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            if (TextUtils.equals("405", it.result)) {
                                view.checkDataFailed("405${it.message}")
                            } else {
                                view.checkDataFailed(it.message)
                            }
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        view.checkDataSuccess(true)
                    }
                }
            )
        }
    }

    override fun attachView(view: FindPassSubmitContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}