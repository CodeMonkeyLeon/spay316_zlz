package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView


/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @路也许很窄
 * @但总是会有路
 * @也许很长
 * @但总会到头
 * @马不是好马
 * @那就能走就走
 * @马不听指挥
 * @那就随处停留
 */
class SettingSwitchLanContract {


    interface View : BaseView {

        fun getBaseSuccess(response: Boolean)

        fun getBaseFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {

        fun getBase()

    }


}