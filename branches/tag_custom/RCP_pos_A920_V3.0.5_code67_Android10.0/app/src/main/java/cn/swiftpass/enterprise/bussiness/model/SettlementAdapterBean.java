package cn.swiftpass.enterprise.bussiness.model;

/**
 * Created by aijingya on 2019/5/21.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2019/5/21.14:36.
 */
public class SettlementAdapterBean {
    //第一种布局的数据
    private String settlementDate;//结算日期（北京时间）
    private Long settlementAmount;//结算金额
    private String organizationBeginTime;//机构开始时间
    private String organizationEndTime;//机构结束时间

    //第二种布局的数据
    private String Title;//标题
    private Long Amount;//交易金额

    public String getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(String settlementDate) {
        this.settlementDate = settlementDate;
    }

    public Long getSettlementAmount() {
        return settlementAmount;
    }

    public void setSettlementAmount(Long settlementAmount) {
        this.settlementAmount = settlementAmount;
    }

    public String getOrganizationBeginTime() {
        return organizationBeginTime;
    }

    public void setOrganizationBeginTime(String organizationBeginTime) {
        this.organizationBeginTime = organizationBeginTime;
    }

    public String getOrganizationEndTime() {
        return organizationEndTime;
    }

    public void setOrganizationEndTime(String organizationEndTime) {
        this.organizationEndTime = organizationEndTime;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Long getAmount() {
        return Amount;
    }

    public void setAmount(Long amount) {
        Amount = amount;
    }



}
