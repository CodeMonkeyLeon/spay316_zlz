/*
 * 文 件 名:  FindPassWordActivity.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-14
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.card;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.WxCard;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;

/**
 * 微信卡券
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-14]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class WxCardListActivity extends TemplateActivity
{
    private List<WxCard> vardList = new ArrayList<WxCard>();
    
    public static void startActivity(Context context, List<WxCard> vardOrders)
    {
        Intent it = new Intent();
        it.setClass(context, WxCardListActivity.class);
        it.putExtra("vardOrders", (Serializable)vardOrders);
        context.startActivity(it);
    }
    
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wx_card);
        
        initView();
        vardList = ((List<WxCard>)getIntent().getSerializableExtra("vardOrders"));
    }
    
    private void initView()
    {
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_forget_pwd);
    }
    
}
