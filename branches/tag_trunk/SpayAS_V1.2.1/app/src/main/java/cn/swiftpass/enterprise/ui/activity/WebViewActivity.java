/*
 * 文 件 名:  PushMoneySuccActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-8-12
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.Blance;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * webView展示
 * 
 * @author  he_hui
 * @version  [版本号, 2016-8-12]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class WebViewActivity extends TemplateActivity
{
    private TextView tv_bank_name, tv_money;
    
    private WebView wb;
    
    private boolean isTag = true;
    
    final class InJavaScriptLocalObj
    {
        public void showSource(String html)
        {
            Log.i("hehui", "showSource");
        }
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.register);
        titleBar.setRightButLayVisibleForTotal(false, getString(R.string.tv_push_record));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
            }
            
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
        });
    }
    
    @SuppressLint({"NewApi", "JavascriptInterface"})
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contenttext);
        
        wb = getViewById(R.id.webview);
//        wb.addJavascriptInterface(new InJavaScriptLocalObj(), "local_obj");
        wb.getSettings().setJavaScriptEnabled(true);
        wb.getSettings().setAllowFileAccessFromFileURLs(false);
        wb.getSettings().setAllowUniversalAccessFromFileURLs(false);
        
        String url = getIntent().getStringExtra("url");
        WebSettings settings = wb.getSettings();
        if (AppHelper.getAndroidSDKVersion() == 17)
        {
            settings.setDisplayZoomControls(false);
        }
        settings.setLoadWithOverviewMode(true);
        settings.setDomStorageEnabled(true);
        //settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        //各种分辨率适应
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int mDensity = metrics.densityDpi;
        if (mDensity == 120)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        }
        else if (mDensity == 160)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        }
        else if (mDensity == 240)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        }
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        
        settings.setDefaultTextEncodingName("utf-8");
        wb.getSettings().setDomStorageEnabled(true);
        wb.getSettings().setAppCacheMaxSize(1024 * 1024 * 8);
        String appCachePath = getApplicationContext().getCacheDir().getAbsolutePath();
        wb.getSettings().setAppCachePath(appCachePath);
        wb.getSettings().setAllowFileAccess(true);
        wb.getSettings().setAppCacheEnabled(true);
        
        wb.setWebChromeClient(new myWebChromeClien());
        wb.setWebViewClient(new MyWebViewClient());
        
        String language = PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN);
        Map<String, String> heards = new HashMap<String, String>();
        Locale locale = getResources().getConfiguration().locale; //zh-rHK
        String lan = locale.getCountry();
        String langu = "zh_cn";
        if (!TextUtils.isEmpty(language))
        {
            heards.put("fp-lang", PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN));
            langu = PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN);
        }
        else
        {
            if (lan.equalsIgnoreCase("CN"))
            {
                heards.put("fp-lang", MainApplication.LANG_CODE_ZH_CN);
                langu = MainApplication.LANG_CODE_ZH_CN;
            }
            else
            {
                heards.put("fp-lang", MainApplication.LANG_CODE_ZH_TW);
                langu = MainApplication.LANG_CODE_ZH_TW;
            }
            
        }
        
        String key = "sp-" + "Android" + "-" + AppHelper.getImei(MainApplication.getContext());
        
        String value =
            AppHelper.getVerCode(MainApplication.getContext()) + "," + ApiConstant.bankCode + "," + langu + ","
                + AppHelper.getAndroidSDKVersionName() + "," + DateUtil.formatTime(System.currentTimeMillis()) + ","
                + "Android";
        
        heards.put(key, value);
        
        wb.loadUrl(url);
        
    }
    
    class myWebChromeClien extends WebChromeClient
    {
        @Override
        public void onProgressChanged(WebView view, int newProgress)
        {
            super.onProgressChanged(view, newProgress);
            
        }
    }
    
    final class MyWebViewClient extends WebViewClient
    {
        public MyWebViewClient()
        {
            super();
        }
        
        /** {@inheritDoc} */
        
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            Log.i("hehui", "onPageStarted");
            if (isTag)
            {
                isTag = false;
                showLoading(false, R.string.public_loading);
            }
            super.onPageStarted(view, url, favicon);
        }
        
        @SuppressLint("NewApi")
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url)
        {
            String ajaxUrl = url;
            Log.i("hehui", "shouldInterceptRequest--" + url);
            return super.shouldInterceptRequest(view, ajaxUrl);
            
        }
        
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            Log.i("hehui", "shouldOverrideUrlLoading-->" + url);
            if (isTag)
            {
                isTag = false;
                showLoading(false, R.string.public_loading);
            }
            
            if (!StringUtil.isEmptyOrNull(url))
            {
                if (url.contains("backApp"))
                {
                    WebViewActivity.this.finish();
                }
            }
            return super.shouldOverrideUrlLoading(view, url);
            
        }
        
        @Override
        public void doUpdateVisitedHistory(WebView view, String url, boolean isReload)
        {
            
            Log.i("hehui", "doUpdateVisitedHistory");
            super.doUpdateVisitedHistory(view, url, isReload);
        }
        
        @Override
        public void onLoadResource(WebView view, String url)
        {
            // TODO Auto-generated method stub
            Log.i("hehui", "onLoadResource");
            super.onLoadResource(view, url);
        }
        
        @Override
        public void onScaleChanged(WebView view, float oldScale, float newScale)
        {
            
            Log.i("hehui", "onScaleChanged");
            super.onScaleChanged(view, oldScale, newScale);
        }
        
        @Override
        public void onPageFinished(WebView view, String url)
        {
            dismissLoading();
            super.onPageFinished(view, url);
        }
        
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
        {
            super.onReceivedError(view, errorCode, description, failingUrl);
            //            dismissLoading();
            wb.loadUrl("file:///android_asset/error.html");
            
        }
        
    }
    
    //    private void initView()
    //    {
    //        tv_bank_name = getViewById(R.id.tv_bank_name);
    //        tv_money = getViewById(R.id.tv_money);
    //    }
    
    //    @Override
    //    protected void setupTitleBar()
    //    {
    //        super.setupTitleBar();
    //        titleBar.setLeftButtonVisible(true);
    //        titleBar.setTitle(R.string.tv_apply_push);
    //    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK && wb.canGoBack())
        {
            wb.goBack();// 返回前一个页面
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    public static void startActivity(Context context, String url)
    {
        Intent it = new Intent();
        it.setClass(context, WebViewActivity.class);
        it.putExtra("url", url);
        context.startActivity(it);
    }
    
    public static void startActivity(Context context, Blance blance)
    {
        Intent it = new Intent();
        it.setClass(context, WebViewActivity.class);
        it.putExtra("blance", blance);
        context.startActivity(it);
    }
}
