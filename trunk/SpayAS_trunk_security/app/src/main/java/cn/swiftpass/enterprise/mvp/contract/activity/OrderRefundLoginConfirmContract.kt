package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.ECDHInfo
import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView


/**
 * @author lizheng.zhao
 * @date 2022/12/2
 *
 * @那就折一张阔些的荷叶
 * @包一片月光回去
 * @回去夹在唐诗里
 * @扁扁的
 * @象压过的相思
 */
class OrderRefundLoginConfirmContract {


    interface View : BaseView {

        fun ecdhKeyExchangeSuccess(response: ECDHInfo?)

        fun ecdhKeyExchangeFailed(error: Any?)

        fun refundLoginSuccess(response: Boolean)

        fun refundLoginFailed(error: Any?)


        fun regisRefundsSuccess(response: Order?)

        fun regisRefundsFailed(error: Any?)


        fun cardPaymentRefundSuccess(response: Order?)

        fun cardPaymentRefundFailed(error: Any?)


        fun authUnfreezeSuccess(response: Order?)

        fun authUnfreezeFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {


        fun authUnfreeze(
            authNo: String?,
            money: String?
        )


        fun cardPaymentRefund(
            orderNoMch: String?,
            outTradeNo: String?,
            totalFee: Long,
            refundMoney: Long
        )


        fun regisRefunds(
            orderNo: String?,
            money: Long,
            refundMoney: Long
        )


        fun refundLogin(
            code: String?,
            username: String?,
            password: String?
        )


        fun ecdhKeyExchange(publicKey: String?)
    }


}