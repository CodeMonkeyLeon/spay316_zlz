/*
 * 文 件 名:  ShopBankDataTab.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2014-3-20
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.io.database.table;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2014-3-20]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class NoticeActiveDataTab extends TableBase
{
    
    public static final String TABLE_NAME_ACTIVE = "t_notice";
    
    public static final String title = "title";
    
    public static final String START_TIME = "startTime";
    
    public static final String END_TIME = "endTime";
    
    public static final String noticeContentUrl = "noticeContentUrl";
    
    public static final String noticeId = "noticeId";
    
    public static final String state = "state"; //状态
    
    @Override
    public void createTable(SQLiteDatabase db)
    {
        final String SQL_CREATE_TABLE =
            "create table " + TABLE_NAME_ACTIVE + "(" + noticeId + " LONG," + title + " VARCHAR(50)," + START_TIME
                + " VARCHAR(50), " + END_TIME + " VARCHAR(50)," + " state INT default 0 , " + noticeContentUrl
                + " VARCHAR(1250))";
        Log.i("hehui", "NoticeActiveDataTab SQL_CREATE_TABLE-->" + SQL_CREATE_TABLE);
        db.execSQL(SQL_CREATE_TABLE);
    }
    
    /**
     * @return 返回 title
     */
    public static String getTitle()
    {
        return title;
    }
    
    /**
     * @return 返回 startTime
     */
    public static String getStartTime()
    {
        return START_TIME;
    }
    
    /**
     * @return 返回 endTime
     */
    public static String getEndTime()
    {
        return END_TIME;
    }
    
    /**
     * @return 返回 noticecontenturl
     */
    public static String getNoticecontenturl()
    {
        return noticeContentUrl;
    }
    
    /**
     * @return 返回 noticeid
     */
    public static String getNoticeid()
    {
        return noticeId;
    }
    
}
