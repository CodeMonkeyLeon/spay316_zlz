/*

 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-14
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import androidx.annotation.Nullable;

import com.example.common.sentry.SentryUtils;
import com.ziyeyouhu.library.KeyboardTouchListener;
import com.ziyeyouhu.library.KeyboardUtil;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo;
import cn.swiftpass.enterprise.bussiness.model.ForgetPSWBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants;
import cn.swiftpass.enterprise.mvp.contract.activity.FindPassSubmitContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.FindPassSubmitPresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.utils.ECDHUtils;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.KotlinUtils;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 忘记密码
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-14]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class FindPassSubmitActivity extends BaseActivity<FindPassSubmitContract.Presenter> implements FindPassSubmitContract.View {

    private static final String TAG = FindPassSubmitActivity.class.getCanonicalName();
    private EditText et_id, et_pass;
    private ImageView iv_clean_input, iv_clean_input_pass;
    private Button btn_next_step;
    private ForgetPSWBean forgetPSWBean;

    //    private UserModel model;
    private LinearLayout rootView;
    private KeyboardUtil keyboardUtil;
    private ScrollView scrollView;

    public static void startActivity(Context context, ForgetPSWBean forgetPSWBean) {
        Intent it = new Intent();
        it.setClass(context, FindPassSubmitActivity.class);
        it.putExtra("ForgetPSWBean", forgetPSWBean);
//        it.putExtra("userModel", model);
        context.startActivity(it);
    }

    @Override
    protected FindPassSubmitContract.Presenter createPresenter() {
        return new FindPassSubmitPresenter();
    }

    @Override
    protected boolean isLoginRequired() {
        return false;
    }

    //安全键盘
    private void initMoveKeyBoard() {
        rootView = findViewById(R.id.rootview);
        scrollView = findViewById(R.id.scrollview);
        keyboardUtil = new KeyboardUtil(this, rootView, scrollView);
        // monitor the KeyBarod state
        keyboardUtil.setKeyBoardStateChangeListener(new FindPassSubmitActivity.KeyBoardStateListener());
        // monitor the finish or next Key
        keyboardUtil.setInputOverListener(new FindPassSubmitActivity.inputOverListener());
        et_id.setOnTouchListener(new KeyboardTouchListener(keyboardUtil, KeyboardUtil.INPUT_TYPE_ABC, -1));
        et_pass.setOnTouchListener(new KeyboardTouchListener(keyboardUtil, KeyboardUtil.INPUT_TYPE_ABC, -1));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (keyboardUtil.isShow) {
                keyboardUtil.hideSystemKeyBoard();
                keyboardUtil.hideAllKeyBoard();
                keyboardUtil.hideKeyboardLayout();

                finish();
            } else {
                return super.onKeyDown(keyCode, event);
            }

            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_pass_submit);

        initView();

        setLister();
        btn_next_step.getBackground().setAlpha(102);
        btn_next_step.setEnabled(false);
//        model = (UserModel) getIntent().getSerializableExtra("userModel");
        forgetPSWBean = (ForgetPSWBean) getIntent().getSerializableExtra("ForgetPSWBean");
        MainApplication.getInstance().getListActivities().add(this);
        initMoveKeyBoard();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                keyboardUtil.showKeyBoardLayout(et_id, KeyboardUtil.INPUT_TYPE_ABC, -1);
            }
        }, 200);
    }

    @Override
    protected void onPause() {
        super.onPause();
        et_id.setText("");
        et_pass.setText("");
        et_id.requestFocus();
    }

    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b, enable, res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_press_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }

    /**
     * {@inheritDoc}
     */

/*
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            if (keyboardUtil.isShow) {
//                keyboardUtil.hideSystemKeyBoard();
//                keyboardUtil.hideAllKeyBoard();
//                keyboardUtil.hideKeyboardLayout();
//            }
            //showPage(CashierManager.class);
            finish();
        }

        return super.onKeyDown(keyCode, event);

    }
*/
    private void initView() {
        et_pass = getViewById(R.id.et_pass);
        et_id = getViewById(R.id.et_id);
        iv_clean_input = getViewById(R.id.iv_clean_input);
        btn_next_step = getViewById(R.id.btn_next_step);
        iv_clean_input_pass = getViewById(R.id.iv_clean_input_pass);
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (et_id.isFocused()) {
                    if (et_id.getText().toString().length() > 0) {
                        iv_clean_input.setVisibility(View.VISIBLE);

                    } else {
                        iv_clean_input.setVisibility(View.GONE);
                    }
                }

                if (et_pass.isFocused()) {
                    if (et_pass.getText().toString().length() > 0) {
                        iv_clean_input_pass.setVisibility(View.VISIBLE);
                    } else {
                        iv_clean_input_pass.setVisibility(View.GONE);
                    }
                }
                if (!StringUtil.isEmptyOrNull(et_id.getText().toString()) && !StringUtil.isEmptyOrNull(et_pass.getText().toString())) {
                    setButtonBg(btn_next_step, true, R.string.bt_confirm);
                } else {
                    setButtonBg(btn_next_step, false, R.string.bt_confirm);
                }
            }
        });
        et_pass.addTextChangedListener(editTextWatcher);
        et_id.addTextChangedListener(editTextWatcher);
    }

    private void setLister() {
        iv_clean_input.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                et_id.setText("");
                iv_clean_input.setVisibility(View.GONE);
            }
        });

        iv_clean_input_pass.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                et_pass.setText("");
                iv_clean_input_pass.setVisibility(View.GONE);
            }
        });

        btn_next_step.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkPasswordDate();
            }
        });
    }

    //    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
//            if (keyboardUtil.isShow) {
//                keyboardUtil.hideSystemKeyBoard();
//                keyboardUtil.hideAllKeyBoard();
//                keyboardUtil.hideKeyboardLayout();
//            } else {
//                return super.onKeyDown(keyCode, event);
//            }
//
//            return false;
//        } else
//            return super.onKeyDown(keyCode, event);
//    }

    public void checkPasswordDate() {
        if (StringUtil.isEmptyOrNull(et_id.getText().toString())) {
            toastDialog(FindPassSubmitActivity.this, R.string.tx_newpass_notnull, null);
            et_id.setFocusable(true);
            return;
        }
        if (StringUtil.isEmptyOrNull(et_pass.getText().toString())) {
            toastDialog(FindPassSubmitActivity.this, R.string.tx_repeatpass_notnull, null);
            et_pass.setFocusable(true);
            return;
        }

        if (et_id.getText().toString().length() < 8) {
            et_id.setFocusable(true);
            toastDialog(FindPassSubmitActivity.this, R.string.show_pass_prompt, null);
            return;
        }

        //校验新密码是否符合8-16位字符
        if (!KotlinUtils.INSTANCE.isNewVersion() && !isContainAll(et_id.getText().toString())) {
            et_id.setFocusable(true);
            toastDialog(FindPassSubmitActivity.this, R.string.et_new_pass, null);
            return;
        }


        if (!et_id.getText().toString().equals(et_pass.getText().toString())) {
            toastDialog(FindPassSubmitActivity.this, R.string.tx_pass_notdiff, null);
            et_pass.setFocusable(true);
            return;
        }

        //如果本地有skey,则直接进行确认重输入密码的操作
        if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getSKey())) {
            if (mPresenter != null) {
                mPresenter.checkData(forgetPSWBean.getToken(), forgetPSWBean.getEmailCode(), et_id.getText().toString().trim());
            }
        } else {//如果本地没有skey,则直接进行确认重输入密码的操作
            ECDHKeyExchange();
        }
    }


    @Override
    public void checkDataSuccess(boolean response) {
        if (response) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //toastDialog(FindPassSubmitActivity.this, R.string.tx_modify_succ, null);
                    showPage(SetNewPassSuccActivity.class);
                }
            });

        }
    }

    @Override
    public void checkDataFailed(@Nullable Object error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (error != null) {
                    if (error.toString().startsWith("405")) {//Require to renegotiate ECDH key
                        ECDHKeyExchange();
                    } else {
                        toastDialog(FindPassSubmitActivity.this, error.toString(), null);
                    }

                }
            }
        });
    }


    @Override
    public void ecdhKeyExchangeSuccess(@Nullable ECDHInfo response) {
        if (response != null) {
            try {
                final String privateKey = SharedPreUtils.readProduct(ParamsConstants.APP_PRIVATE_KEY).toString();
                String secretKey = ECDHUtils.getInstance().ecdhGetShareKey(MainApplication.getInstance().getSerPubKey(), privateKey);
                SharedPreUtils.saveObject(secretKey, ParamsConstants.SECRET_KEY);
                //再去请求一次登录接口
                checkPasswordDate();
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(e, SentryUtils.INSTANCE.getClassNameAndMethodName());
                Log.e(TAG, Log.getStackTraceString(e));
            }
        }
    }

    @Override
    public void ecdhKeyExchangeFailed(@Nullable Object error) {

    }

    private void ECDHKeyExchange() {
        try {
            ECDHUtils.getInstance().getAppPubKey();
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(e, SentryUtils.INSTANCE.getClassNameAndMethodName());
            Log.e(TAG, Log.getStackTraceString(e));
        }
        final String publicKey = SharedPreUtils.readProduct(ParamsConstants.APP_PUBLIC_KEY).toString();


        if (mPresenter != null) {
            mPresenter.ecdhKeyExchange(publicKey);
        }
    }

    //用来校验密码，是否包含数字和字母
    public boolean isContainAll(String str) {
        boolean isDigit = false;
                /*boolean isLowerCase = false;
                boolean isUpperCase = false;*/
        boolean isLetters = false;

        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))) {
                isDigit = true;
            } else if (Character.isLowerCase(str.charAt(i))) {
//                        isLowerCase = true;
                isLetters = true;
            } else if (Character.isUpperCase(str.charAt(i))) {
//                        isUpperCase = true;
                isLetters = true;
            }
        }
        String regex = "^[a-zA-Z0-9]+$";
//                boolean isCorrect = isDigit&&isLowerCase&&isUpperCase&&str.matches(regex);
        boolean isCorrect = isDigit && isLetters && str.matches(regex);
        return isCorrect;
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tv_find_pass_title_info1);
    }


    class KeyBoardStateListener implements KeyboardUtil.KeyBoardStateChangeListener {

        @Override
        public void KeyBoardStateChange(int state, EditText editText) {

        }
    }

    class inputOverListener implements KeyboardUtil.InputFinishListener {

        @Override
        public void inputHasOver(int onclickType, EditText editText) {

        }
    }

}
