package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created by aijingya on 2019/7/2.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: ${TODO}(小费设置界面的数据实体)
 * @date 2019/7/2.15:36.
 */
public class TipsBean implements Serializable {
    private int id;
    private String tipsRate;//小费比例
    private boolean defaultTip;//是否是默认的小费比例选项
    private String label; //小费标签，用来展示用

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipsRate() {
        return tipsRate;
    }

    public void setTipsRate(String tipsRate) {
        this.tipsRate = tipsRate;
    }

    public boolean isDefaultTip() {
        return defaultTip;
    }

    public void setDefaultTip(boolean defaultTip) {
        this.defaultTip = defaultTip;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
