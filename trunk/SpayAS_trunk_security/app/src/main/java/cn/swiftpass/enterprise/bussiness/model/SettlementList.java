package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by congwei.li on 2022/5/9.
 *
 * @Description:
 */
public class SettlementList implements Serializable {
    public ArrayList<SettlementBean> data;
}
