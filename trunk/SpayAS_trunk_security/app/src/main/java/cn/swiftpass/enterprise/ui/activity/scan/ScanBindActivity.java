package cn.swiftpass.enterprise.ui.activity.scan;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.common.sentry.SentryUtils;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bean.QRCodeBean;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.Camera.CameraManager;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.Decoding.CaptureActivityHandler;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.Decoding.InactivityTimer;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.ScanBindContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.ScanBindPresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.Zxing.ViewfinderView;
import cn.swiftpass.enterprise.ui.widget.dialog.CashierNoticeDialog;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.DisplayUtil;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.KotlinUtils;
import cn.swiftpass.enterprise.utils.permissionutils.OnPermissionCallback;
import cn.swiftpass.enterprise.utils.permissionutils.Permission;
import cn.swiftpass.enterprise.utils.permissionutils.PermissionDialogUtils;
import cn.swiftpass.enterprise.utils.permissionutils.PermissionsManage;

/**
 * 扫码绑定
 *
 * @author Jamy
 */
public class ScanBindActivity extends BaseActivity<ScanBindContract.Presenter> implements Callback, ScanBindContract.View {

    private static final String TAG = ScanBindActivity.class.getSimpleName();
    private static final float BEEP_VOLUME = 0.10f;
    private static final long VIBRATE_DURATION = 200L;
    /**
     * When the beep has finished playing, rewind to queue up another one.
     */
    private final OnCompletionListener beepListener = new OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            mediaPlayer.seekTo(0);
        }
    };
    SurfaceView surfaceView;
    private CaptureActivityHandler handler;
    private ViewfinderView viewfinderView;
    private boolean hasSurface;
    private Vector<BarcodeFormat> decodeFormats;
    private String characterSet;
    private InactivityTimer inactivityTimer;
    private MediaPlayer mediaPlayer;
    private boolean playBeep;
    //    private ScanCodeCheckDialog dialog;
    private boolean vibrate;
    private Context mContext;
    private LinearLayout ly_back;
    private DialogInfo dialogs;
    private TextView tv_code_info;
    private SurfaceHolder surfaceHolder;
    //    /**
//     * 判断是否需要检测，防止不停的弹框
//     */
    private boolean isNeedCheck = true;
    /**
     * 扫描识别的qrcodeId
     */
    private String qrcodeId = "";
    private String cashierdeskName = "";
    private DialogInfo dialogInfo;
    /**
     * 绑定二维码
     */
    private CashierNoticeDialog NoticeDialog;

    @Override
    protected ScanBindContract.Presenter createPresenter() {
        return new ScanBindPresenter();
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_scan_bind);
        ly_back = getViewById(R.id.ly_back);
        MainApplication.getInstance().getListActivities().add(this);
        CameraManager.init(this);
        viewfinderView = findViewById(R.id.viewfinder_view);
        hasSurface = false;
        inactivityTimer = new InactivityTimer(this);
        tv_code_info = getViewById(R.id.tv_code_info);
        tv_code_info.setPadding(0, 0, 0, DisplayUtil.dip2Px(ScanBindActivity.this, 20));
        setLister();
        if (null != getIntent()) {
            cashierdeskName = getIntent().getStringExtra("cashierDeskName");
        }

        if (!PermissionsManage.isGranted(this, Permission.CAMERA)) {
            PermissionDialogUtils.requestPermission(ScanBindActivity.this,
                    Permission.Group.CAMERA, true,
                    new OnPermissionCallback() {
                        @Override
                        public void onGranted(List<String> permissions, boolean all) {
                            if (all) {
                                restartCamera();
                            }
                        }
                    });
        }
    }

    private void setLister() {
        ly_back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 从系统设置权限完成后返回app的处理
        if (requestCode == PermissionsManage.REQUEST_CODE) {
            if (PermissionsManage.isGranted(ScanBindActivity.this, Permission.CAMERA)) {
//                restartCamera();
                KotlinUtils.INSTANCE.reStartApp(ScanBindActivity.this);
            } else {
                PermissionDialogUtils.finishWithoutPermission(ScanBindActivity.this,
                        Permission.Group.CAMERA, true);
            }
        }
    }

    /**
     * 显示提示信息
     *
     * @since 2.5.0
     */
    protected void showMissingPermissionDialog(Context context) {
        DialogInfo dialogInfo = new DialogInfo(context, null, getString(R.string.setting_permisson_camera), getStringById(R.string.title_setting), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                startAppSettings();
            }

            @Override
            public void handleCancelBtn() {
                finish();
            }
        }, null);
        dialogInfo.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                return keycode == KeyEvent.KEYCODE_BACK;
            }
        });
        DialogHelper.resize(context, dialogInfo);
        dialogInfo.show();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onResume() {
        super.onResume();

        surfaceView = findViewById(R.id.preview_view);
        surfaceHolder = surfaceView.getHolder();

        if (hasSurface) {
            initCamera(surfaceHolder, false);
        } else {
            surfaceHolder.addCallback(this);
            surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }
        decodeFormats = null;
        characterSet = null;

        playBeep = true;
        AudioManager audioService = (AudioManager) getSystemService(AUDIO_SERVICE);
        if (audioService.getRingerMode() != AudioManager.RINGER_MODE_NORMAL) {
            playBeep = false;
        }
        initBeepSound();
        vibrate = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
        CameraManager.get().closeDriver();
        if (!hasSurface) {
            SurfaceView surfaceView = findViewById(R.id.preview_view);
            SurfaceHolder surfaceHolder = surfaceView.getHolder();
            surfaceHolder.removeCallback(this);
        }
    }

    void closeCamera() {
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
    }

    @Override
    protected void onDestroy() {
        inactivityTimer.shutdown();
        super.onDestroy();
    }

    void restartCamera() {
        closeCamera();
        viewfinderView.setVisibility(View.VISIBLE);
        SurfaceView surfaceView = findViewById(R.id.preview_view);
        SurfaceHolder surfaceHolder = surfaceView.getHolder();
        initCamera(surfaceHolder, false);
    }

    private void initCamera(SurfaceHolder surfaceHolder, boolean isFirst) {
        try {
            CameraManager.get().openDriver(surfaceHolder, surfaceView);
        } catch (IOException ioe) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    ioe,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            return;
        } catch (RuntimeException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            return;
        }
        if (handler == null) {
            handler = new CaptureActivityHandler(this, decodeFormats, characterSet);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (!hasSurface) {
            hasSurface = true;
            initCamera(holder, false);
        }

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        hasSurface = false;

    }

    public ViewfinderView getViewfinderView() {
        return viewfinderView;
    }

    public Handler getHandler() {
        return handler;
    }

    public void drawViewfinder() {
        viewfinderView.drawViewfinder();
    }

    public void handleDecode(Result obj, Bitmap barcode) {
        inactivityTimer.onActivity();
        playBeepSoundAndVibrate();

        AlertDialog.Builder builder = new AlertDialog.Builder(ScanBindActivity.this);
        builder.setTitle(getString(R.string.public_cozy_prompt));
        builder.setPositiveButton(getString(R.string.btnOk), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                restartCamera();
            }
        });

        builder.setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.show();
    }

    private void initBeepSound() {
        if (playBeep && mediaPlayer == null) {
            // The volume on STREAM_SYSTEM is not adjustable, and users found it
            // too loud,
            // so we now play on the music stream.
            setVolumeControlStream(AudioManager.STREAM_MUSIC);
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setOnCompletionListener(beepListener);

            AssetFileDescriptor file = getResources().openRawResourceFd(R.raw.beep);
            try {
                mediaPlayer.setDataSource(file.getFileDescriptor(), file.getStartOffset(), file.getLength());
                file.close();
                mediaPlayer.setVolume(BEEP_VOLUME, BEEP_VOLUME);
                mediaPlayer.prepare();
            } catch (IOException e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                mediaPlayer = null;
            }
        }
    }

    private void playBeepSoundAndVibrate() {
        if (playBeep && mediaPlayer != null) {
            mediaPlayer.start();
        }
        if (vibrate) {
            Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
            vibrator.vibrate(VIBRATE_DURATION);
        }
    }

    @Override
    protected boolean isLoginRequired() {
        // TODO Auto-generated method stub
        return true;
    }

    public void submitData(final String code, boolean vibration) {
        if (vibration) {
            playBeepSoundAndVibrate();
        }
        if (TextUtils.isEmpty(code)) {
            return;
        }
        //扫码激活
        if (code.startsWith("http") && code.contains("qrcodeId")) {
            String qrId = getQrcodeId(code);
            if (!TextUtils.isEmpty(qrId)) {
                qrcodeId = qrId;
                getQRDetails();
            }
        }
    }

    @Override
    public void bindCodeDetailsSuccess(@NonNull QRCodeBean response) {
        String MerName = getString(R.string.pay_merchant_name) + ": "
                + MainApplication.getInstance().getUserInfo().mchName;
        String MerId = getString(R.string.merchant_id) + ": " + MainApplication.getInstance().getUserInfo().mchId;
        String cashierdesk = getString(R.string.cashier_desk) + ": " + cashierdeskName;
        String Id = getString(R.string.qrcode_Id) + ": " + response.id;
        //支付宝专属码
        if (1 == response.qrType) {
            showCodeDetails(true, ScanBindActivity.this, MerName, MerId,
                    cashierdesk, Id);
        } else {
            showCodeDetails(false, ScanBindActivity.this, MerName, MerId,
                    cashierdesk, Id);
        }
    }

    @Override
    public void bindCodeDetailsFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }
        if (error != null) {
            ScanBindActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    toastDialog(ScanBindActivity.this, error.toString(), new NewDialogInfo.HandleBtn() {
                        @Override
                        public void handleOkBtn() {
                            restartCamera();
                        }
                    });
                }
            });
        }
    }

    /**
     * 获取识别的二维码详细信息
     */
    private void getQRDetails() {
        if (mPresenter != null) {
            mPresenter.bindCodeDetails(qrcodeId, cashierdeskName);
        }
    }


    @Override
    public void bindCodeSuccess(@NonNull String response) {
        if (NoticeDialog == null) {
            NoticeDialog = new CashierNoticeDialog(ScanBindActivity.this);
            NoticeDialog.setContentTextSize(16);
            NoticeDialog.setContentText(R.string.bind_third_step_msg);
            NoticeDialog.setConfirmText(R.string.bt_know);
            NoticeDialog.setCanceledOnTouchOutside(false);
            NoticeDialog.setCurrentListener(new CashierNoticeDialog.ButtonConfirmCallBack() {
                @Override
                public void onClickConfirmCallBack() {
                    if (NoticeDialog != null && NoticeDialog.isShowing()) {
                        NoticeDialog.dismiss();
                        if (MainApplication.getInstance().getListActivities().size() > 0) {
                            if (MainApplication.getInstance().getListActivities().contains(CodeListActivity.class.getSimpleName())) {
                                HandlerManager.notifyMessage(HandlerManager.BIND_CODE_FINISH, HandlerManager.BIND_CODE_FINISH, null);
                                for (Activity a : MainApplication.getInstance().getListActivities()) {
                                    if (!(a instanceof CodeListActivity)) {
                                        a.finish();
                                    }
                                }
                            } else {
                                for (Activity a : MainApplication.getInstance().getListActivities()) {
                                    a.finish();
                                }
                                showPage(CodeListActivity.class);
                            }
                        }

                    }
                }
            });
        }
        if (!NoticeDialog.isShowing()) {
            NoticeDialog.show();
        }
    }

    @Override
    public void bindCodeFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }
        if (error != null) {
            ScanBindActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    toastDialog(ScanBindActivity.this, error.toString(), new NewDialogInfo.HandleBtn() {
                        @Override
                        public void handleOkBtn() {
                            restartCamera();
                        }
                    });
                }
            });
        }
    }

    private void BindCode() {
        if (mPresenter != null) {
            mPresenter.bindCode(qrcodeId, cashierdeskName);
        }
    }


    /**
     * 获取QrcodeId
     *
     * @param code
     * @return
     */
    private String getQrcodeId(String code) {
        String[] strs = code.split("qrcodeId=");
        int len = strs.length;
        if (!TextUtils.isEmpty(strs[len - 1])) {
            return strs[len - 1];
        } else {
            return null;
        }
    }


    public void showCodeDetails(boolean isAlipay, final Activity context, String MerName, String MerId,
                                String deskName, String QrId) {
        dialogInfo = new DialogInfo(isAlipay, context, getString(R.string.public_cozy_prompt), MerName, MerId,
                deskName, QrId, getString(R.string.cashier_add_pwd_confirm), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                BindCode();
            }

            @Override
            public void handleCancelBtn() {
                dialogInfo.cancel();
                restartCamera();

            }
        }, null);
        dialogInfo.setBtnOkTextColor(this.getResources().getColor(R.color.cashier_add));
        DialogHelper.resize(context, dialogInfo);
        dialogInfo.show();
    }

}
