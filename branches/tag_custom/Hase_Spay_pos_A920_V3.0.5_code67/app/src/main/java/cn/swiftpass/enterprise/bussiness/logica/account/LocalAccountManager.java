package cn.swiftpass.enterprise.bussiness.logica.account;

import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.enums.UserRole;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.CertificateBean;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo;
import cn.swiftpass.enterprise.bussiness.model.ErrorMsg;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.NetHelper;
import cn.swiftpass.enterprise.utils.AESHelper;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.MD5;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;
import cn.swiftpass.enterprise.utils.Utils;

/**
 * User: Alan
 * Date: 13-9-21
 * Time: 上午11:35
 */
public class LocalAccountManager {
    private SharedPreferences sp;

    public static final String TAG = "LocalAccountManager";

    private LocalAccountManager() {
        sp = MainApplication.getContext().getApplicationPreferences();
    }

    public static LocalAccountManager getInstance() {
        return Container.instance;
    }

    private static class Container {
        public static LocalAccountManager instance = new LocalAccountManager();
    }

    private UserModel user;

    public UserModel getLoggedUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public long getUID() {
        long id = user == null ? 0 : user.uId;
        if (id == 0) {
            id = getLoggedUser() != null ? getLoggedUser().uId : 0;
        }
        return id;
    }

    public String getUserName() {
        if (user != null) {
            //            String name = user.realName;
            //            return name == null ? user.name : name + "";
        }
        return "";
    }

    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    public void getCode(final UINotifyListener<String> listener) {

        ThreadHelper.executeWithCallback(new Executable<String>() {
            @Override
            public String execute() throws Exception {
                JSONObject param = new JSONObject();

                long spayRs = System.currentTimeMillis();
                //加签名
                String priKey = (String) SharedPreUtile.readProduct("secretKey");
                param.put("spayRs", String.valueOf(spayRs));

                if(!StringUtil.isEmptyOrNull(MainApplication.skey)){
                    param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MD5.md5s(priKey).substring(0,16)));

                }else{
                    param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MD5.md5s(String.valueOf(spayRs))));
                }


                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/refreshLoginCode", param, String.valueOf(spayRs), null);
                try {
                    if (!result.hasError()) {

                        if (result.data == null) {
                            listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                            return null;
                        }
                        String res = result.data.getString("result");
                        int ret = Utils.Integer.tryParse(res, -1);
                        if (ret == 200) {

                            return result.data.getString("message");
                        } else {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                        }
                        return null;
                    }
                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                    return null;
                }

            }
        }, listener);
    }

    /**
     * <一句话功能简述>
     * <功能详细描述>
     *客户端和服务器公钥交换的接口
     * @see [类、类#方法、类#成员]
     */
    public void ECDHKeyExchange(final String publicKey, final UINotifyListener<ECDHInfo> listener){

        ThreadHelper.executeWithCallback(new Executable<ECDHInfo>() {
            @Override
            public ECDHInfo execute() throws Exception {
                JSONObject param = new JSONObject();
                param.put("publicKey", publicKey);
                long spayRs = System.currentTimeMillis();
                //加签名
                param.put("spayRs", String.valueOf(spayRs));
                param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MD5.md5s(String.valueOf(spayRs))));

                RequestResult result = NetHelper.httpsPostECDHKeyExchange(ApiConstant.BASE_URL_PORT + "spay/key/pKeyExchange", param, String.valueOf(spayRs), null,false);
                try {
                    if (!result.hasError()) {

                        if (result.data == null) {
                           return null;
                        }
                        String res = result.data.getString("result");
                        int ret = Utils.Integer.tryParse(res, -1);
                        if (ret == 200) {
                            ECDHInfo ecdhInfo = new ECDHInfo();
                            try {
                                JSONObject jsonObject = new JSONObject(result.data.getString("message"));
                                MainApplication.serPubKey =jsonObject.optString("serPubKey", "");
                                MainApplication.skey = jsonObject.optString("skey", "");
                                ecdhInfo.serPubKey = MainApplication.serPubKey;
                                ecdhInfo.skey = MainApplication.skey;
                            } catch (Exception e) {
                                Log.e(TAG, Log.getStackTraceString(e));
                            }
                            return ecdhInfo;
                        } else {
                            listener.onError(result.data.getString("message"));
                        }

                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                break;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                break;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                break;
                            case RequestResult.RESULT_SSL_HANDSHAKE_ERROR://证书校验失败
                                listener.onError(RequestResult.HANDSHAKE_CER_ERROR);
                                break;
                            case RequestResult.RESULT_NEED_LOGIN:
                                listener.onError("401");
                                break;

                        }

                    }
                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }
                return null;
            }

        }, listener);
    }

    /**
     * <一句话功能简述>
     * 切换语言 得到不同语言配置信息
     *
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void getBase(final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable<Boolean>() {
            @Override
            public Boolean execute() throws Exception {
                JSONObject param = new JSONObject();

                long spayRs = System.currentTimeMillis();
                //加签名
                String priKey = (String) SharedPreUtile.readProduct("secretKey");
                param.put("spayRs", String.valueOf(spayRs));

                if(!StringUtil.isEmptyOrNull(MainApplication.skey)){
                    param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MD5.md5s(priKey).substring(0,16)));

                }else{
                    param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MD5.md5s(String.valueOf(spayRs))));
                }

                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/config/getBase", param, String.valueOf(spayRs), null);
                result.setNotifyListener(listener);
                try {
                    if (!result.hasError()) {

                        if (result.data == null) {
                            return false;
                        }

                        String res = result.data.getString("result");
                        Logger.i("hehui", "getBase-->" + result.data);
                        int ret = Utils.Integer.tryParse(res, -1);
                        if (ret == 200) {
                            //                            parseJson(result, logUser);
                            try {
                                JSONObject jsonObject = new JSONObject(result.data.getString("message"));
                                MainApplication.payTypeMap = JsonUtil.jsonToMap(jsonObject.optString("pay_type", ""));
                                MainApplication.tradeStateMap = JsonUtil.jsonToMap(jsonObject.optString("trade_state", ""));
                                MainApplication.refundStateMap = JsonUtil.jsonToMap(jsonObject.optString("refund_state", ""));
                                MainApplication.setRefundStateMap(MainApplication.refundStateMap);
                                MainApplication.setPayTypeMap(MainApplication.payTypeMap);
                                MainApplication.setTradeStateMap(MainApplication.tradeStateMap);

                                //后台没有返回，前端写死
                                //授权状态(1未授权，2已授权，3已撤销，4解冻)
                                MainApplication.preAuthtradeStateMap.put("1",ToastHelper.toStr(R.string.unauthorized));
                                MainApplication.preAuthtradeStateMap.put("2",ToastHelper.toStr(R.string.pre_auth_authorized));
                                MainApplication.preAuthtradeStateMap.put("3",ToastHelper.toStr(R.string.pre_auth_closed));
                                MainApplication.preAuthtradeStateMap.put("4",ToastHelper.toStr(R.string.pre_auth_unfreeze));
                                MainApplication.setPreAuthtradeStateMap(MainApplication.preAuthtradeStateMap);
                            } catch (Exception e) {
                                Log.e(TAG, Log.getStackTraceString(e));
                            }
                            return true;
                        } else {
                            listener.onError(result.data.getString("message"));
                            return false;
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return false;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return false;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return false;
                        }
                        return false;
                    }
                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                    return false;
                }

            }
        }, listener);
    }

    /**
     * <一句话功能简述>
     * 自动登录
     *
     * @see [类、类#方法、类#成员]
     */
    public void deviceLogin(final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable<Boolean>() {
            @Override
            public Boolean execute() throws Exception {
                JSONObject param = new JSONObject();

                long spayRs = System.currentTimeMillis();
                //加签名
                String priKey = (String) SharedPreUtile.readProduct("secretKey");
                param.put("spayRs", String.valueOf(spayRs));

                param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MD5.md5s(priKey).substring(0,16)));

                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.USER_BAST + "/deviceLogin", param, String.valueOf(spayRs), null);
                result.setNotifyListener(listener);
                if (result.data == null) {
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                    return false;
                }
                String res = result.data.getString("result");
                try {
                    if (!result.hasError()) {
                        int ret = Utils.Integer.tryParse(res, -1);
                        UserModel logUser = new UserModel();
                        if (ret == 200) {
                            parseJson(result, logUser);
                            return true;
                        } else {
                            listener.onError(result.data.getString("message"));
                            return false;
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return false;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return false;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return false;
                            case RequestResult.RESULT_SSL_HANDSHAKE_ERROR://证书校验失败
                                listener.onError(RequestResult.HANDSHAKE_CER_ERROR);
                                return false;
                            case RequestResult.RESULT_NEED_LOGIN:
                                listener.onError("401");
                                return false;
                        }
                        return false;
                    }
                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                    return false;
                }

            }
        }, listener);
    }

    public void Logout(final UINotifyListener<Boolean> listener){
        ThreadHelper.executeWithCallback(new Executable<Boolean>() {
            @Override
            public Boolean execute() throws Exception {

                JSONObject param = new JSONObject();

                long spayRs = System.currentTimeMillis();
                //加签名
                String priKey = (String) SharedPreUtile.readProduct("secretKey");
                param.put("spayRs", String.valueOf(spayRs));
                param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MD5.md5s(priKey).substring(0,16)));
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.USER_BAST + "/logout", param, null, null);
                result.setNotifyListener(listener);
                try {
                    if (!result.hasError()) {

                        String res = result.data.getString("result");
                        int ret = Utils.Integer.tryParse(res, -1);
                        if (ret == 200) {
                            return true;
                        } else {
                            listener.onError(result.data.getString("message"));
                            return false;
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return false;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return false;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return false;
                            case RequestResult.RESULT_SSL_HANDSHAKE_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.certificate_request_failed));
                                return false;
                        }

                        return false;
                    }
                } catch (Exception e) {
                    return false;
                }

            }
        }, listener);
    }

    public void refundLogin(final String code, final String mId, final String username, final String password,  final boolean isNormalUser, final UINotifyListener<Boolean> listener) {

        ThreadHelper.executeWithCallback(new Executable<Boolean>() {
            @Override
            public Boolean execute() throws Exception {

                JSONObject param = new JSONObject();
                param.put("username", username);
                long spayRs = System.currentTimeMillis();
//                String paseStr = MD5.md5s(spayRs + "2017SWIFTpassSPAYCCC").toUpperCase();
                String priKey = (String) SharedPreUtile.readProduct("secretKey");
                String paseStr = MD5.md5s(priKey).toUpperCase();
                param.put("password", AESHelper.aesEncrypt(password, paseStr.substring(8, 24)));
                param.put("skey",MainApplication.skey);

//                param.put("password", AESHelper.aesEncrypt(password, paseStr.substring(8, 24)));
                param.put("client", MainApplication.CLIENT);
                param.put("bankCode", ApiConstant.bankCode);
                param.put("androidTransPush", "1");
                if (!StringUtil.isEmptyOrNull(code)) {
                    param.put("code", code);
                }
//                String cid = PushManager.getInstance().getClientid(MainApplication.getContext());
//                param.put("pushCid", cid);

                param.put("spayRs", String.valueOf(spayRs));
//                param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MD5.md5s(String.valueOf(spayRs))));
                param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MD5.md5s(priKey).substring(0,16)));
                Logger.i("hehui", "loginAsync param->>" + param.toString());
//                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.USER_BAST + "/spayLogin", param, String.valueOf(spayRs), null);
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.USER_BAST + "/spayLoginV2", param, String.valueOf(spayRs), null);

                try {
                    if (!result.hasError()) {

                        if (result.data == null) {
                            listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                            return false;
                        }

                        String res = result.data.getString("result");
                        Logger.i(TAG, "res-->" + result.data);
                        int ret = Utils.Integer.tryParse(res, -1);
                        UserModel logUser = new UserModel();
                        if (ret == 200) {
                           /* MainApplication.userName = username;
                            parseJson(result, logUser);*/
                            String o = result.data.getString("message");
                            JSONObject jsonObject = new JSONObject(o);
                            //退款验证身份的token
                            MainApplication.refundToken = jsonObject.optString("token", "");

                            return true;
                        } else if (ret == 403) {//需要输入验证
                            String c = result.data.getString("code");
                            if (!StringUtil.isEmptyOrNull(code)) {
                                ErrorMsg msg = new ErrorMsg();
                                msg.setMessage(result.data.getString("message"));
                                msg.setContent(result.data.getString("code"));
                                listener.onError(msg);
                            } else {
                                listener.onError("403" + c);
                            }
                            return false;
                        }else if(ret == 405){
                            listener.onError("405" + result.data.getString("message"));
                            return false;
                        } else {
                            listener.onError(result.data.getString("message"));
                            return false;
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return false;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return false;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return false;
                        }
                        //登录失败，是否离线交易
                        if (result.resultCode >= 500 && result.resultCode <= 599) {
                            listener.onError(ToastHelper.toStr(R.string.failed_to_connect_server));
                        } else if (result.getMessage() == null) {
                            listener.onError(ToastHelper.toStr(R.string.failed_to_login_try_later));
                        }
                        return false;
                    }
                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                    return false;
                }

            }
        }, listener);
    }

    /**
     * 员工登录
     *
     * @param username
     * @param listener
     *                 0.登录成功
     *                 1.商户待审核
     *                 2.商户初审通过
     *                 3.商户初审不通过
     *                 5.商户终审不通过
     *                 6.商户失效或过期
     *                 7.商户不存在
     *                 8.密码错误
     */
    public void loginAsync(final String code, final String mId, final String username, final String password,  final boolean isNormalUser, final UINotifyListener<Boolean> listener) {

        ThreadHelper.executeWithCallback(new Executable<Boolean>() {
            @Override
            public Boolean execute() throws Exception {
                JSONObject param = new JSONObject();
                param.put("username", username);
                long spayRs = System.currentTimeMillis();
//                String paseStr = MD5.md5s(spayRs + "2017SWIFTpassSPAYCCC").toUpperCase();
                String priKey = (String) SharedPreUtile.readProduct("secretKey");
                String paseStr = MD5.md5s(priKey).toUpperCase();
                param.put("password", AESHelper.aesEncrypt(password, paseStr.substring(8, 24)));
                param.put("client", MainApplication.CLIENT);
                param.put("skey",MainApplication.skey);

                param.put("bankCode", ApiConstant.bankCode);
                param.put("androidTransPush", "1");
                if (!StringUtil.isEmptyOrNull(code)) {
                    param.put("code", code);
                }
//                String cid = PushManager.getInstance().getClientid(MainApplication.getContext());
//                param.put("pushCid", cid);
                param.put("spayRs", String.valueOf(spayRs));
//                param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MD5.md5s(String.valueOf(spayRs))));
                param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MD5.md5s(priKey).substring(0,16)));
                Logger.i("hehui", "loginAsync param->>" + param.toString());
//                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.USER_BAST + "/spayLoginWithCode", param, String.valueOf(spayRs), null);
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.USER_BAST + "/spayLoginWithCodeV2", param, String.valueOf(spayRs), null);

                try {
                    if (!result.hasError()) {

                        if (result.data == null) {
                            listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                            return false;
                        }

                        String res = result.data.getString("result");
                        Logger.i("hehui", "res-->" + result.data);
                        int ret = Utils.Integer.tryParse(res, -1);
                        UserModel logUser = new UserModel();
                        if (ret == 200) {
                            MainApplication.userName = username;
                            parseJson(result, logUser);
                            return true;
                        } else if(ret == 405){//Require to renegotiate ECDH key
                            listener.onError("405"+result.data.getString("message"));
                            return false;
                        } else if (ret == 403) {//需要输入验证
                            String c = result.data.getString("code");
                            if (!StringUtil.isEmptyOrNull(code)) {
                                ErrorMsg msg = new ErrorMsg();
                                msg.setMessage(result.data.getString("message"));
                                msg.setContent(result.data.getString("code"));
                                listener.onError(msg);
                            } else {
                                listener.onError("403" + c);
                            }
                            return false;
                        }else {
                            listener.onError(result.data.getString("message"));
                            return false;
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return false;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return false;
                            case RequestResult.RESULT_SSL_HANDSHAKE_ERROR: //网络证书校验失败
                                listener.onError(RequestResult.HANDSHAKE_CER_ERROR);
                                return false;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return false;
                        }
                        //登录失败，是否离线交易
                        if (result.resultCode >= 500 && result.resultCode <= 599) {
                            listener.onError(ToastHelper.toStr(R.string.failed_to_connect_server));
                        } else if (result.getMessage() == null) {
                            listener.onError(ToastHelper.toStr(R.string.failed_to_login_try_later));
                        }
                        return false;
                    }
                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                    return false;
                }

            }
        }, listener);
    }



    @SuppressWarnings("unused")
    private void parseJson(RequestResult result, UserModel logUser) throws JSONException {
        //        try
        //        {
        String o = result.data.getString("message");

        JSONObject jsonObject = new JSONObject(o);
        logUser.uId = Long.parseLong(jsonObject.getString("id"));
        MainApplication.userId = Long.parseLong(jsonObject.getString("id"));
        //        logUser.name = jsonObject.getString("userName");
        MainApplication.userName = jsonObject.optString("username", "");
        //        logUser.pwd = jsonObject.getString("pwd");
        //        logUser.realName = jsonObject.getString("realName");
        //        String name = jsonObject.optString("username", "");
        //        String str = name.substring(name.lastIndexOf("@") + 1);

        MainApplication.isActivityAuth = jsonObject.optInt("isActivityAuth", 0);
        MainApplication.themeMd5 = jsonObject.optString("themeMd5", "");
        MainApplication.Md5 = jsonObject.optString("apiShowListMd5", "");
        MainApplication.merchantId = jsonObject.optString("mchId", "");
        MainApplication.remark = jsonObject.optString("remark", "");
        MainApplication.phone = jsonObject.optString("phone", "");
        MainApplication.isAdmin = jsonObject.optString("isAdmin", "0");
        MainApplication.isOrderAuth = jsonObject.optString("isOrderAuth", "0");
        MainApplication.realName = jsonObject.optString("realname", "");
        MainApplication.mchName = jsonObject.optString("mchName", "");
        MainApplication.channelId = jsonObject.optString("channelId", "");
        MainApplication.isRefundAuth = jsonObject.optString("isRefundAuth", "0");
        String url = ApiConstant.BASE_URL_PORT + ApiConstant.DONLOAD_IMAGE;
        MainApplication.mchLogo = jsonObject.optString("mchLogo", "");
        MainApplication.body = jsonObject.optString("tradeName", "");
        MainApplication.serviceType = jsonObject.optString("serviceType", "");
        MainApplication.activateServiceType = jsonObject.optString("activateServiceType", "");

        MainApplication.signKey = jsonObject.optString("signKey", "");
        MainApplication.isTotalAuth = jsonObject.optInt("isTotalAuth", 0);
        MainApplication.realName = jsonObject.optString("realname", "");
        MainApplication.feeType = jsonObject.optString("feeType", "CNY");
        MainApplication.feeFh = jsonObject.optString("feeFh", ToastHelper.toStr(R.string.tx_mark));
        MainApplication.showEwallet = jsonObject.optInt("showEwallet", 0);
        //固定二维码的聚合和非聚合的判断
        MainApplication.isFixCode = jsonObject.optInt("isFixCode", 0);
        //是否开启电子卡的判断
        MainApplication.isCardOpen = jsonObject.optInt("isCardOpen", 0);
        //获取当前币种的最小单位，即小数点的位数，获取不到默认是两位
        MainApplication.numFixed = jsonObject.optInt("numFixed", 2);

        //退款验证身份的token
        MainApplication.refundToken = jsonObject.optString("token", "");

        //当前商户或者收银员是否开通预授权
        MainApplication.isPre_authOpen = jsonObject.optInt("isAuthFreezeOpen", 0);
        //当前收银员是否有预授权权限
        MainApplication.isUnfreezeAuth = jsonObject.optString("isUnfreezeAuth", "0");
        //当前登录的密码是否默认密码：1是
        MainApplication.isDefault = jsonObject.optString("isDefault","0");
        if(TextUtils.isEmpty(MainApplication.isDefault)){
            MainApplication.isDefault = "0";
        }

        /**
         * 新增一些费率信息
         */
        if (jsonObject.has("rate")) {
            MainApplication.setExchangeRate(jsonObject.optString("rate", ""));
        }
        if (jsonObject.has("vatRate")) {
            MainApplication.setVatRate(jsonObject.getDouble("vatRate"));
        }
        if (jsonObject.has("surchargeRate")) {
            MainApplication.setSurchargeRate(jsonObject.getDouble("surchargeRate"));
        }
        if (jsonObject.has("taxRate ")) {
            MainApplication.setTaxRate(jsonObject.getDouble("taxRate"));
        }
        if (jsonObject.has("usdToRmbExchangeRate")) {
            MainApplication.setUsdToRmbExchangeRate(jsonObject.getDouble("usdToRmbExchangeRate"));
        }
        if (jsonObject.has("sourceToUsdExchangeRate")) {
            MainApplication.setSourceToUsdExchangeRate(jsonObject.getDouble("sourceToUsdExchangeRate"));
        }
        if (jsonObject.has("alipayPayRate")) {
            MainApplication.setAlipayPayRate(jsonObject.getDouble("alipayPayRate"));
        }

        MainApplication.setSurchargeOpen(jsonObject.optString("isSurchargeOpen").equals("1"));
        MainApplication.setTipOpenFlag(jsonObject.optString("tipOpenFlag").equals("1"));
        //屏蔽小费功能
//        MainApplication.setTipOpen(jsonObject.optString("isTipOpen").equals("1"));
        //屏蔽预扣税功能
        MainApplication.setTaxRateOpen(jsonObject.optString("isTaxRateOpen").equals("1"));

        MainApplication.setMchId(MainApplication.merchantId);
        MainApplication.setMchName(MainApplication.mchName);
        MainApplication.setUserId(MainApplication.userId);

        MainApplication.setSignKey(MainApplication.signKey);
        MainApplication.setFeeType(MainApplication.feeType);
        if (MainApplication.feeFh == null || MainApplication.feeFh.equals("null")) {
            MainApplication.feeFh = ToastHelper.toStr(R.string.tx_mark);
        }
        if (!MainApplication.mchLogo.equals("") && !MainApplication.mchLogo.equals("null") && MainApplication.mchLogo != null) {
            MainApplication.mchLogo = url + MainApplication.mchLogo;
        }
        try {
            MainApplication.setFeeFh(MainApplication.feeFh);
            MainApplication.payTypeMap = JsonUtil.jsonToMap(jsonObject.optString("payTypeMap", ""));
            MainApplication.tradeStateMap = JsonUtil.jsonToMap(jsonObject.optString("tradeStateMap", ""));
            MainApplication.refundStateMap = JsonUtil.jsonToMap(jsonObject.optString("refundStateMap", ""));
            MainApplication.apiProviderMap = JsonUtil.jsonToMap(jsonObject.optString("apiProviderMap", ""));
            MainApplication.setPayTypeMap(MainApplication.payTypeMap);
            MainApplication.setTradeStateMap(MainApplication.tradeStateMap);
            MainApplication.setRefundStateMap(MainApplication.refundStateMap);

            //后台没有返回，前端写死
            //授权状态(1未授权，2已授权，3已撤销，4解冻)
            MainApplication.preAuthtradeStateMap.put("1",ToastHelper.toStr(R.string.unauthorized));
            MainApplication.preAuthtradeStateMap.put("2",ToastHelper.toStr(R.string.pre_auth_authorized));
            MainApplication.preAuthtradeStateMap.put("3",ToastHelper.toStr(R.string.pre_auth_closed));
            MainApplication.preAuthtradeStateMap.put("4",ToastHelper.toStr(R.string.pre_auth_unfreeze));
            MainApplication.setPreAuthtradeStateMap(MainApplication.preAuthtradeStateMap);
//            MainApplication.preAuthtradeStateMap.put("1",ToastHelper.toStr(R.string.tx_mark));


        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }

        //取登录cookie 
        try {

            JSONObject cObject = new JSONObject(jsonObject.optString("cookieMap", ""));
            if (null != cObject) {
                //            MainApplication.cookie_key =
                //            MainApplication.SAUTHID = cObject.optString("SAUTHID", "");
                PreferenceUtil.commitString("login_skey", cObject.optString("SKEY", ""));
                PreferenceUtil.commitString("login_sauthid", cObject.optString("SAUTHID", ""));

            }
        } catch (Exception e) {
        }
//                MainApplication.newSignKey = MD5.md5s(MainApplication.signKey + cObject.optString("SKEY", ""));
        String secretKey = (String) SharedPreUtile.readProduct("secretKey");
        if(!TextUtils.isEmpty(secretKey)){
            MainApplication.newSignKey = MD5.md5s(secretKey).substring(0,16);
        }
        MainApplication.setNewSignKey(MainApplication.newSignKey);
    }

    /**
     * 修改密码
     */
    public void changePwd(final String oldPwd, final String phone, final String newPwd, final String repPassword, final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable<Boolean>() {
            @Override
            public Boolean execute() throws Exception {
                long spayRs = System.currentTimeMillis();
                JSONObject param = new JSONObject();
                Map<String, String> map = new HashMap<String, String>();
//                String paseStr = MD5.md5s(spayRs + "2017SWIFTpassSPAYCCC").toUpperCase();
                String priKey = (String) SharedPreUtile.readProduct("secretKey");
                String paseStr = MD5.md5s(priKey).toUpperCase();
                param.put("skey",MainApplication.skey);
                param.put("telephone", phone);
                param.put("oldPassword", AESHelper.aesEncrypt(oldPwd, paseStr.substring(8, 24)));
                param.put("password", AESHelper.aesEncrypt(newPwd, paseStr.substring(8, 24)));
                param.put("repPassword", AESHelper.aesEncrypt(repPassword, paseStr.substring(8, 24)));
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    param.put("mchId", MainApplication.merchantId);
                    map.put("mchId", MainApplication.merchantId);
                } else {
                    param.put("mchId", MainApplication.getMchId());
                    map.put("mchId", MainApplication.getMchId());
                }

                if (MainApplication.userId > 0) {

                    param.put("userId", MainApplication.userId);
                    map.put("userId", MainApplication.userId + "");
                } else {
                    param.put("userId", MainApplication.getUserId());
                    map.put("userId", MainApplication.getUserId() + "");
                }

                map.put("telephone", phone);
                map.put("oldPassword", oldPwd);
                map.put("password", newPwd);
                map.put("repPassword", repPassword);

                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey())) {
                    param.put("sign", SignUtil.getInstance().createSign(map, MainApplication.getSignKey()));

                    Logger.i("hehui", "sign-->" + SignUtil.getInstance().createSign(map, MainApplication.getSignKey()));
                }

                //                    //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                    param.put("spayRs", spayRs);
                    param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MainApplication.getNewSignKey()));
                }

//                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/resetPwd", param, String.valueOf(spayRs), null);
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/resetPwdV2", param, String.valueOf(spayRs), null);

                if (!result.hasError()) {
                    int res = Integer.parseInt(result.data.getString("result"));
                    if (res == 200) {
                        return true;
                    }else if(res == 405){
                        listener.onError("405" + result.data.getString("message"));
                        return false;
                    } else {
                        listener.onError(result.data.getString("message"));
                        return false;
                    }
                } else {
                    if (result.getMessage() == null) {
                        listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                    } else {
                        Logger.i(result.getMessage());
                        listener.onError(result.getMessage());
                    }
                    return false;
                }
            }

        }, listener);
    }

    /**
     * 得到实时费率
     * <功能详细描述>
     *
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void getExchangeRate(final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable<Boolean>() {
            @Override
            public Boolean execute() {
                JSONObject jsonObject = new JSONObject();
                try {
                    if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                        jsonObject.put("mchId", MainApplication.merchantId);// todo
                    } else {
                        jsonObject.put("mchId", MainApplication.getMchId());// todo
                    }
//                    jsonObject.put("exchangeCurrency","USD");
                    long spayRs = System.currentTimeMillis();
                    //加签名
                    if (!StringUtil.isEmptyOrNull(MainApplication.newSignKey)) {
                        jsonObject.put("spayRs", spayRs);
                        jsonObject.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(jsonObject.toString()), MainApplication.newSignKey));
                    }
                    Logger.i("hehui", "getExchangeRate  param-->" + jsonObject.toString());
                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/mch/getExchangeRate", jsonObject, String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    if (!result.hasError()) {
                        if (result.data != null) {
                            Logger.i("hehui", "getExchangeRate-->" + result.data);
                            int code = Integer.parseInt(result.data.getString("result"));
                            if (code == 200) {
                                JSONObject json = new JSONObject(result.data.getString("message"));
                                if (json.has("rate")) {
                                    MainApplication.setExchangeRate(json.optString("rate", ""));
                                }
                                if (json.has("vatRate")) {
                                    MainApplication.setVatRate(json.getDouble("vatRate"));
                                }
                                if (json.has("surchargeRate")) {
                                    MainApplication.setSurchargeRate(json.getDouble("surchargeRate"));
                                }
                                if (json.has("taxRate ")) {
                                    MainApplication.setTaxRate(json.getDouble("taxRate"));
                                }
                                if (json.has("usdToRmbExchangeRate")) {
                                    MainApplication.setUsdToRmbExchangeRate(json.getDouble("usdToRmbExchangeRate"));
                                }
                                if (json.has("sourceToUsdExchangeRate")) {
                                    MainApplication.setSourceToUsdExchangeRate(json.getDouble("sourceToUsdExchangeRate"));
                                }

                                if (json.has("alipayPayRate")) {
                                    MainApplication.setAlipayPayRate(json.getDouble("alipayPayRate"));
                                }
                                return true;
                            } else {
                                MainApplication.setExchangeRate("1");
                                listener.onError(result.data.getString("message"));
                            }
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                        }
                    }
                    return null;
                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                    return false;
                }
            }

        }, listener);

    }

    /**
     * 动态加载支付类型以及对于的图片
     * <功能详细描述>
     *
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void apiShowList(final UINotifyListener<List<DynModel>> listener) {
        ThreadHelper.executeWithCallback(new Executable<List<DynModel>>() {
            @Override
            public List<DynModel> execute() {

                JSONObject jsonObject = new JSONObject();
                try {
                    if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                        jsonObject.put("mchId", MainApplication.merchantId);// todo
                    } else {
                        jsonObject.put("mchId", MainApplication.getMchId());// todo
                    }
                    long spayRs = System.currentTimeMillis();
                    //加签名
                    if (!StringUtil.isEmptyOrNull(MainApplication.newSignKey)) {
                        jsonObject.put("spayRs", spayRs);
                        jsonObject.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(jsonObject.toString()), MainApplication.newSignKey));
                    }
                    Logger.i("hehui", "apiShowList  param-->" + jsonObject.toString());
                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/apiShowList", jsonObject, String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    if (!result.hasError()) {

                        if (result.data != null) {
                            Logger.i("hehui", "apiShowList-->" + result.data);
                            int code = Integer.parseInt(result.data.getString("result"));
                            if (code == 200) {
                                JSONObject json = new JSONObject(result.data.getString("message"));
                                List<DynModel> list = new ArrayList<DynModel>();
                                list.addAll(((List<DynModel>) JsonUtil.jsonToList(json.getString("data"), new com.google.gson.reflect.TypeToken<List<DynModel>>() {
                                }.getType())));

                                //过滤
                                //List<DynModel> filterLst = new ArrayList<DynModel>();
                                if (list.size() > 0) {
                                    list.get(0).setMd5(json.optString("md5", ""));
                                }

                                return list;

                            } else {
                                listener.onError(result.data.getString("message"));
                            }
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                        }
                    }

                    return null;
                } catch (Exception e) {

                    listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                    return null;
                }
            }

        }, listener);

    }

  /*
   **CA证书公钥获取接口
   * 请求CA证书公钥获取接口得到最新CA证书公钥
   * 获取证书，使用旧的签名方式（签名Key为时间戳spayRs）
   * */
    public void getCertificateString(final UINotifyListener<CertificateBean> listener) {
        ThreadHelper.executeWithCallback(new Executable<CertificateBean>() {
            @Override
            public CertificateBean execute() throws Exception {
                JSONObject param = new JSONObject();
                long spayRs = System.currentTimeMillis();
                //加签名
                param.put("spayRs", String.valueOf(spayRs));
                // 获取证书，使用旧的签名方式（签名Key为时间戳spayRs）
                param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MD5.md5s(String.valueOf(spayRs))));

                //备注：因为获取公钥证书的接口和ECDH秘钥交换接口一样，不用传Skey,即这个spayDi不用传值，故此处用httpsPostECDHKeyExchange方法调用
                RequestResult result = NetHelper.httpsPostECDHKeyExchange(ApiConstant.BASE_URL_PORT + "spay/key/getCertificate", param, String.valueOf(spayRs), null,true);
                try {
                    if (!result.hasError()) {
                        if (result.data == null) {
                            listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                            return null;
                        }
                        String res = result.data.getString("result");
                        int ret = Utils.Integer.tryParse(res, -1);
                        if (ret == 200) {
                            CertificateBean certificateBean = new CertificateBean();
                            try {
                                JSONObject jsonObject = new JSONObject(result.data.getString("message"));
                                certificateBean.setPublicKey(jsonObject.optString("publicKey", ""));
                                certificateBean.setBeginDate(jsonObject.optString("beginDate",""));
                                certificateBean.setEndDate(jsonObject.optString("endDate", ""));
                            }catch (Exception e){
                            }
                            return certificateBean;
                        } else {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                break;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                break;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                break;
                        }
                        return null;
                    }
                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                    return null;
                }

            }
        }, listener);
    }


    /**
     * 保存检测版本时间
     */
    public void saveCheckVersionTime() {
        SharedPreferences.Editor edit = sp.edit();
        edit.putLong(GlobalConstant.FILED_T_CHECK_VERSION, System.currentTimeMillis());
        edit.commit();
    }


    /**
     * 必须升级标识
     */
    public void saveCheckVersionFlag(boolean mustUpdate) {
        SharedPreferences.Editor edit = sp.edit();
        edit.putBoolean(GlobalConstant.FILED_T_CHECK_VERSION_MUST_UPDATE, mustUpdate);
        edit.commit();
    }

    /**
     * 是否已经登陆
     *
     * @return
     */
    public synchronized boolean isLoggedIn() {
        return user != null && user.name != null;
    }




    public void onDestory() {
        this.user = null;
    }


    /**
     * 是否管理员
     */
    public boolean isManager() {
        return this.user.role == UserRole.approve;
    }
}
