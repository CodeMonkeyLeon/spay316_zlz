package cn.swiftpass.enterprise.io.net;

/**
 * 配置
 * @author Alan
 */
public class ApiConstant
{
    /////////////////////////////////////////////// 常用设置///////////////////////////////////////////////
    /**心跳频率*/
    public static long HEARBATE_TIME = 1000 * 80;//* 60s;
    
    /** 0.spay 1.spay*/
    public static int pad = 0;
    
    /** 手机端*/
    public static int SPAY = 1;
    
    /** spay企业版本*/
    public static int SPAY_SUB = 2;
    
    /**下载包名*/
    public static String APK_NAME = "";
    
    public static String IP = "";

    public static String IP_WITHOUT_CDN = "";

    
    public static final boolean isContentNetwork = true;
    
    ////////////////////////////////////////////////net configer ///////////////////////////////////////////////
    public static String PORT = "";
    
    public static String BASE_URL_PORT;//BASE_URL +":"+ PORT+"/";
    
    public static String PARTNET = ""; //1900000109
    
    public static String KEY = "";//"8934e7d15453e97507ef794cf7b0519d";
    
    public static String PAYGATEWAY = "";
    
    // 下载路径
    public static String DOWNLOAD_APP_URL;
    
    public static String DOWNLOAD_IMG_URL;
    
    public static String DO_CFT_GET_UUID_URL;
    
    ////////////////////////////////////////////////查询订单接口///////////////////////////////////////////////
    /**查询订单接口*/
    public static final String CFT_QUERY_ORDER_URL = "https://gw.tenpay.com/gateway/normalorderquery.xml?";
    
    /**CFT支付接口*/
    public static final String CFT_BASE_PAY_URL = "https://gw.tenpay.com/gateway/pay.htm?";
    
    public static String NOTIFY_URL;
    
    public static String RETURN_URL;
    
    /**默认的财付通需要的客服段自己的IP */
    public static String MY_IP = "192.168.1.18";
    
    //////////////////////////////////////参数名/////////////////////////////////////////////
    public static final String P_PAY_INFO = "pay_info";
    
    /**商户号 */
    public static final String P_PARTNER = "partner";
    
    /**与财付通的通信编码 */
    public static final String INPUT_CHARTE = "UTF-8";
    
    public static final String P_KEY = "key";
    
    public static final String P_IMEI = "imei";
    
    public static final String CFT_RET_SUCCESS = "success";
    
    public static final String CFT_RET_FAIL = "faill";
    
    public static String KEY_ISOVERSEASYURL = "1a649a1c15eec9c3fdcba2ca96b809f1";//"1a649a1c15eec9c3fdcba2ca96b809f1"; 境外支付key
    
    public static String PARTNET_ISOVERSEASYURL = "1218160901"; //境外支付 商户号 1218160901
    
    /**CFT境外支付接口*/
    public static final String CFT_BASE_PAY_URL_ISOVERSEASYURL = "https://gw.tenpay.com/intl/gateway/pay.htm?";
    
    /**境外通讯码 CNY人民币 GBP英镑 HKD港币 USD    美元 JPY日元 CAD加拿大元  AUD澳大利亚元 EUR欧元*/
    public static final String FEE_TYPE = "HKD"; //参考附录币种编码默认先给人民币
    
    /**返回状态码 */
    public static final String P_RETCODE = "retcode";
    
    /**返回消息 */
    public static final String P_RETMSG = "retmsg";
    
    /**签名方式*/
    public static final String P_INPUT_SIGN_TYPE = "sign_type";
    
    /**接口版本*/
    public static final String P_SERVICE_VERSION = "service_version";
    
    /**字符*/
    public static final String P_INPUT_CHARSET = "input_charset";
    
    /**密钥序号 */
    // public static final String P_KEY_INDEX = "sign_key_index";
    /**密钥序号*/
    public static final String SIGN_KEY_INDEX = "sign_key_index";
    
    /**加密签名 */
    public static final String P_SIGN = "sign";
    
    /**商户订单号 */
    public static final String P_OUT_TRADE_NO = "out_trade_no";
    
    ///////////////////////////////////////////////
    /**下载地址*/
    public static String DOWNLOAD_URL = "http://jime-files.b0.upaiyun.com/v1/";
    
    ////////////////////////////////////////////// ////////////////////////////////////////////////////////////////
    /**服务器自定义字段名称*/
    public static String CONFING_FIELD_PAY_NAME = "";
    
    public static String SESSION_ID;
    
    /**临时用户*/
    public static boolean TEMP_USER = false;
    
    //////////////////////////////通讯接口组织前部分//////////////////////////////////////////////////////////
    /**订单接口*/
    // public static final String ORDER_BAST="order/";
    public static final String ORDER_GET_ORDER = "spay/order/querySpayOrder";
    
    public static final String ORDER_GET_ORDER_DAY_TRADE = "order/getDayTrade";
    
    public static final String ORDER_GET_ORDER_DAY_TRADE_By = "spay/order/queryOrderTotalByDate";
    
    public static final String ORDER_GETLAST_TTIME = "spay/payQueryOrder";

    public static final String PRE_AUTH_ORDER_GETLAST_TTIME = "spay/unifiedQueryAuth";
    
    public static final String ORDER_GET_STATE = "order/getOrderState";
    
    public static final String ORDER_CREATE_ORDER = "order/createOrder";
    
    //发送数据
    public static final String ORDER_CREATE_SEND = "order/requestPostDecode";
    
    public static final String ORDER_GET_ORDER_NUM = "order/getOrderNum";
    
    //获取poskey
    public static final String ORDER_GET_ORDER_POS_KEY = "order/getPosKey";
    
    public static final String ORDER_GET_SUB_FEEDBACK = "feedback/subFeedback";
    
    /**本地生成订单二维码接口**/
    public static final String ORDER_QR_CODE_GET_UUIDINFO = "order/doCreateOrder";
    
    /**查询店铺*/
    public static final String SHOP_SEARCH = "shop/ShopAction_queryShopList";
    
    /**添加店铺*/
    public static final String SHOP_ADD = "shop/ShopAction_addShop";
    
    /**修改店铺*/
    public static final String SHOP_UPDATE = "shop/ShopAction_updateShop";
    
    /**添加优惠券*/
    public static final String COUPONS_ADD = "coupons/CouponsAction_addCoupons";
    
    /**优惠券查询*/
    public static final String COUPON_SEARCH = "coupons/CouponsAction_searchCoupons";
    
    /**优惠券已领用人查询*/
    public static final String COUPON_SEARCH_USER = "coupons/CouponsAction_getCollerUserInfo";
    
    /**优惠券详细*/
    public static final String COUPON_DETAIL = "coupons/CouponsAction_goDetail";
    
    /**优惠券审核*/
    public static final String COUPONS_VERIFY = "coupons/CouponsAction_verifyCoupons";
    
    /**删除优惠券*/
    public static final String COUPONS_DELETE = "coupons/CouponsAction_delCoupons";
    
    /**优惠修改*/
    public static final String COUPON_EDIT = "coupons/CouponsAction_updateCoupons";
    
    /**优惠劵详细页面*/
    public static final String COUPON_DEATAIL = "coupons/CouponsAction_createQRCode.action?couponId=";
    
    /**优惠劵分享图片地址1*/
    public static final String COUPON_CONTENT_LINK =
        "http://swiftpass.weixin.swiftpass.cn/weixin/openidPass?redirectTo=";
    
    /**优惠劵分享图片地址*/
    public static final String COUPON_CONTENT_IMG = "file/goImageShow?imagePath=";
    
    /**优惠劵分享内容*/
    public static final String COUPON_LINK = "coupons/CouponsAction_goShopCouDetail?couponId=";
    
    /**优惠二维码有效性*/
    public static final String COUPON_CHECKCOUPONS = "coupons/CouponsAction_checkCoupons";
    
    /**交易信息*/
    public static final String ORDER_PAY_SUBMIT_INFO = "order/updateOrderPayInfo";
    
    /**下载图片*/
    public static final String DONLOAD_IMAGE = "spay/mch/download?url=";
    
    /**下载店铺Logo图片*/
    public static final String DONLOAD_SHOP_LOGO_IMAGE = "file/goImageShow?dataType=3&imagePath=";
    
    /**下载pos签名图片*/
    public static final String DONLOAD_SIGNATURE_IMAGE = "file/goImageShow?dataType=2&imagePath=";
    
    /***
     * 优惠劵下载图片
     */
    public static final String DONLOAD_COU_IMAGE = "file/goImageShow?imagePath=";
    
    //财付通测试
    public static final String TEST_CFT_ORDER = "order/doCreateOrderThree";
    
    /** 上传图片*/
    public static final String UPLOADPHOTO = "spay/mch/upload";
    
    /** 完善资料添加*/
    public static final String SHOPDATAADD = "spay/mch/addOrUpdate";
    
    /** 完善资料基本数据查询*/
    public static final String SHOPBASEDATA = "mch/type";
    
    /** 查询完善资料*/
    public static final String SHOPDATAQUERY = "spay/mch/get";
    

    /** 忘记密码检查*/
    public static final String CHACKDATA = "spay/user/setPasswordForPhone";
    
    /**忘记密码*/
    public static final String UPDATEPWD = "employee/updatePwd";

    
    public static final String CASHIERADD = "spay/user/saveOrUpdateSpayUser";

    public static final String CASHIERADDNEW = "spay/user/saveOrUpdateSpayUserV2";

    public static final String CASHIERCREATE = "spay/user/saveCashier";
    
    /**用户接口*/
    public static final String USER_BAST = "spay/user";
    
    /**心跳接口*/
    public static final String HEARBEAT_BAST = "hearbeat_action/hearbeatAction_build";
    
    /**退单*/
    public static final String REFUND_REGIS = "spay/spayRefund";

    /**退单*/
    public static final String REFUND_REGIS_NEW = "spay/spayRefundV2";

    /**查询是否可以退款*/
    public static final String REFUND_CANORNOT= "spay/calcRefund";

    
    public static final String REFUND_QUERY = "refund_action/refundAction_query";
    
    /**升级*/
    public static String UPGRADE_CHECKIN = "spay/upgrade/checkinVersion";
    
    public static String UPGRADE_DONWLOAD_FINSH = "spay/upgrade/downloadFinsh";
    
    /**注册获取国家与货市单位*/
    public static final String GET_COUNTRY_AND_UNIT = "spay/mch/getCountryAndUnit";
    
    /**广告*/
    public static String ADS_QUERY = "ad/action_query";
    
    /**广告*/
    public static String ADS_QUERY_GET_ADSIMG = "ad/action_show?img=";
    
    /**固定url 帮助 关于我们*/
    public final static String URL_ABOUT_US = "file:///android_asset/aboutus.html";
    
    /**固定url 帮助 关于我们*/
    public final static String URL_HELP = "file:///android_asset/question.html";
    
    //    public final static String URL_HELP = "file:///android_asset/gcdl_ad/gcdl_ad.html";
    
    public final static String URL_REGIST_PROVISIONS = "file:///android_asset/question2.html";
    
    /**离线*/
    public static boolean OFFLINE = false;
    
    public static boolean isLocaltionQRcode;
    
    /**快速登录*/
    public static String QUITE_USER_NAME = "";
    
    public static String QUITE_USER_PWD = "";
    
    public static String QUITE_MERCHANT_ID = "";
    
    /**每页显示数量*/
    public static int PAGE_SIZE = 10;
    
    /***添加境外支付*/
    public static boolean ISOVERSEASY;
    
    public static String body = "";
    
    public static Integer bankType = 0;
    
    public static String bankCode = "";

    public static String bankName = "";
    
    public static String wxCardUrl = "";
    
    public static String redPack = "";
    
    public static String appKey = "";
    
    public static String Channel;
    
    public static String pushMoneyUrl = "";
    
    public static String serverAddrTest = "";
    
    public static String serverAddrPrd = "";
    
    public static String pushMoneyUrlTest = "";
    
    public static String pushMoneyUrlPrd = "";
    
    public static String serverAddrDev = "";

    public static String serverAddrDev_JH = "";

    public static String imie = "";

}
