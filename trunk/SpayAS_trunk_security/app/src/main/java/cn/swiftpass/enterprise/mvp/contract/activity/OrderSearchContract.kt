package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/11/21
 *
 * @人的责任
 * @是照顾一块屋顶
 * @在活的时候
 * @让它有烟
 * @早上有门
 */
class OrderSearchContract {


    interface View : BaseView {

        fun queryCardPaymentOrderDetailsSuccess(response: Order?)

        fun queryCardPaymentOrderDetailsFailed(error: Any?)

        fun queryOrderDetailSuccess(response: Order?, isItemClick: Boolean)

        fun queryOrderDetailFailed(error: Any?)


        fun queryOrderByInvoiceIdSuccess(response: Order?)

        fun queryOrderByInvoiceIdFailed(error: Any?)


        fun querySpayOrderNewSuccess(
            response: ArrayList<Order>?,
            isLoadMore: Boolean,
            page: Int
        )

        fun querySpayOrderNewFailed(error: Any?)


        fun queryCardPaymentOrderListSuccess(
            response: ArrayList<Order>?,
            isLoadMore: Boolean,
            page: Int
        )


        fun queryCardPaymentOrderListFailed(error: Any?)

    }


    interface Presenter : BasePresenter<View> {


        fun queryCardPaymentOrderList(
            apiProviderList: ArrayList<Int>?,
            tradeTypeList: ArrayList<Int>?,
            tradeStateList: ArrayList<Int>?,
            startTime: String?,
            page: Int,
            isSearch: Boolean,
            orderNoMch: String?,
            isLoadMore: Boolean
        )


        fun querySpayOrderNew(
            listStr: ArrayList<String?>?,
            payTypeList: ArrayList<String?>?,
            isRefund: Int,
            page: Int,
            order: String?,
            isUnfrozen: Int,
            userId: String?,
            startDate: String?,
            isLoadMore: Boolean
        )


        fun queryOrderByInvoiceId(invoiceId: String?)


        fun queryOrderDetail(
            orderNo: String?,
            mchId: String?,
            isMark: Boolean,
            isItemClick: Boolean
        )

        fun queryCardPaymentOrderDetails(
            outTradeNo: String?,
            orderNoMch: String?
        )
    }


}