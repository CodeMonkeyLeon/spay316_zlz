package cn.swiftpass.enterprise.ui.activity.user;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.UserModel;

public class CashierAdapter extends BaseAdapter
{
    
    private List<UserModel> userModels;
    
    private ViewHolder holder;
    
    private Context context;
    
    public CashierAdapter()
    {
    }
    
    public CashierAdapter(List<UserModel> userModels, Context context)
    {
        this.userModels = userModels;
        this.context = context;
    }
    
    @Override
    public int getCount()
    {
        return userModels.size();
    }
    
    @Override
    public Object getItem(int position)
    {
        return userModels.get(position);
    }
    
    @Override
    public long getItemId(int position)
    {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            convertView = View.inflate(context, R.layout.cashier_list, null);
            holder = new ViewHolder();
            holder.cashier_state = (TextView)convertView.findViewById(R.id.cashier_state);
            holder.cashier_store = (TextView)convertView.findViewById(R.id.cashier_store);
            holder.userId = (TextView)convertView.findViewById(R.id.userId);
            holder.userName = (TextView)convertView.findViewById(R.id.userName);
            holder.usertel = (TextView)convertView.findViewById(R.id.usertel);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder)convertView.getTag();
        }
        
        UserModel userModel = userModels.get(position);
        if (userModel.getDeptname() != null)
        {
            holder.cashier_store.setText(userModel.getDeptname());
        }
        
        //        if (userModel.getEnabled() != null && userModel.getEnabled().equals("true"))
        //        {
        //            holder.cashier_state.setText("(" + ToastHelper.toStr(R.string.open) + ")");
        //        }
        //        else
        //        {
        //            holder.cashier_state.setText("(" + ToastHelper.toStr(R.string.tx_frozen) + ")");
        //        }
        holder.userId.setText(String.valueOf(userModel.getId()));
        if (userModel.getPhone() != null)
        {
            holder.usertel.setText(userModel.getPhone());
        }
        
        if (userModel.getRealname() != null)
        {
            holder.userName.setText(userModel.getRealname());
        }
        
        return convertView;
    }
    
    private class ViewHolder
    {
        private TextView userId, cashier_state, userName, usertel, cashier_store;
        
    }
    
}
