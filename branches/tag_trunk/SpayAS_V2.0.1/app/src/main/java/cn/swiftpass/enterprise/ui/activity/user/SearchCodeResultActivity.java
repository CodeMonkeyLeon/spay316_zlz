/*
 * 文 件 名:  RefundManagerActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2015-3-16
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.user;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnMenuItemClickListener;
import com.google.zxing.WriterException;

import org.xclcharts.test.OrderTotalModel;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bean.QRCodeBean;
import cn.swiftpass.enterprise.bussiness.enums.OrderStatusEnum;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.MaxCardManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.refund.RefundManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.OrderSearchResult;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.activity.OrderDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.RefundOrderDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.StaticCodeActivity;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.scan.CaptureActivity;
import cn.swiftpass.enterprise.ui.activity.scan.CodeListActivity;
import cn.swiftpass.enterprise.ui.activity.settle.SettleCashierAdapter;
import cn.swiftpass.enterprise.ui.activity.settle.SettleUserActivity;
import cn.swiftpass.enterprise.ui.activity.total.DistributionNewAdapter;
import cn.swiftpass.enterprise.ui.adapter.RefundHistoryAdapter;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.utils.DateTimeUtil;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.DisplayUtil;
import cn.swiftpass.enterprise.utils.EditTextUtil;
import cn.swiftpass.enterprise.utils.ImageUtil;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;
import handmark.pulltorefresh.library.PullToRefreshBase;
import handmark.pulltorefresh.library.PullToRefreshGridView;
import handmark.pulltorefresh.library.internal.LoadingLayout;

/**
 * 搜索二维码
 * 
 * @author he_hui
 * @version [版本号, 2015-3-16]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class SearchCodeResultActivity extends BaseActivity implements OnClickListener,OnItemClickListener{
    private static final String TAG = SearchCodeResultActivity.class.getSimpleName();
    private LinearLayout refund_top;
    private LinearLayout refund_back_btn;
    private EditText refund_search_input;
    private ImageView refund_search_clear_btn;
    private Button but_seach;
    private PullToRefreshGridView pull_gridview;
    private CodeAdapter adapter;
    private GridView gridView;
    private LinearLayout layout_pay_foot;
    private LinearLayout layout_refresh_foot;
    private LinearLayout layout_pay_head;
    private LinearLayout layout_refresh_head;
    LoadingLayout mFootView = null;
    LoadingLayout mHeadView = null;
    private Context context;
    private List<QRCodeBean> codes;
    private int page = 1;
    private boolean isLoadMore = false;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.find_cashier_code);
        initView();
        context = SearchCodeResultActivity.this;
        showSoftInputFromWindow(SearchCodeResultActivity.this, refund_search_input);
    }

    /**
     * EditText获取焦点并显示软键盘
     */
    @Override
    public void showSoftInputFromWindow(Activity activity, EditText editText) {
        super.showSoftInputFromWindow(activity,editText);
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    private void initView(){
        codes = new ArrayList<QRCodeBean>();
        this.but_seach = getViewById(R.id.but_seach);
        refund_top = getViewById(R.id.refund_top);
        this.refund_back_btn = getViewById(R.id.refund_back_btn);
        this.refund_search_input = getViewById(R.id.refund_search_input);
        this.refund_search_clear_btn = getViewById(R.id.refund_search_clear_btn);
        refund_back_btn.setOnClickListener(this);
        refund_search_clear_btn.setOnClickListener(this);
        but_seach.setOnClickListener(this);

        pull_gridview = getViewById(R.id.pull_gridView);
        gridView = pull_gridview.getRefreshableView();
        gridView.setOnItemClickListener(this);
        pull_gridview.setScrollingWhileRefreshingEnabled(true);
        pull_gridview.setTag(0x11);
        pull_gridview.setMode(PullToRefreshBase.Mode.BOTH);
        adapter = new CodeAdapter();
        gridView.setAdapter(adapter);
        pull_gridview.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<GridView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase refreshView) {
                pull_gridview.getLoadingLayoutProxy().setRefreshingLabel(context.getString(R.string.loading));
                pull_gridview.getLoadingLayoutProxy().setPullLabel(context.getString(R.string.drop_dowm));
                pull_gridview.getLoadingLayoutProxy().setReleaseLabel(context.getString(R.string.release_update));
                page = 1;
                isLoadMore = false;
                closeKeybord(refund_search_input,context);
                String BinduserId = refund_search_input.getText().toString().trim();
                if (!TextUtils.isEmpty(BinduserId)){
                    getData(isLoadMore,page,BinduserId);
                }
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase refreshView) {
                isLoadMore = true;
                closeKeybord(refund_search_input,context);
                String BinduserId = refund_search_input.getText().toString().trim();
                if (!TextUtils.isEmpty(BinduserId)){
                    getData(isLoadMore,page,BinduserId);
                }
            }
        });

        mFootView = pull_gridview.getFooterLayout();
        mHeadView = pull_gridview.getHeaderLayout();
        if (null != mFootView){
            layout_pay_foot = mFootView.findViewById(R.id.layout_pay);
            layout_refresh_foot = mFootView.findViewById(R.id.layout_refresh);
            layout_refresh_foot.setVisibility(View.VISIBLE);
        }
        if (null != mHeadView){
            layout_pay_foot = mHeadView.findViewById(R.id.layout_pay);
            layout_refresh_head = mHeadView.findViewById(R.id.layout_refresh);
            layout_refresh_head.setVisibility(View.VISIBLE);
        }
    }


    private void getData(final boolean isLoad,int pageNo,String bindUserId){
        Logger.e("JAMY","pageNo: "+pageNo);
        OrderManager.getInstance().GetCodeList(pageNo,20,bindUserId,new UINotifyListener<List<QRCodeBean>>(){
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                loadDialog(SearchCodeResultActivity.this, R.string.public_data_loading);
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                dismissLoading();
                if (checkSession()) {
                    return;
                }
                if (object != null) {
                    SearchCodeResultActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toastDialog(SearchCodeResultActivity.this, object.toString(), null);
                        }
                    });
                }
            }

            @Override
            public void onSucceed(List<QRCodeBean> result) {
                super.onSucceed(result);
                dismissLoading();
                if (result.size() > 0){
                    if (!isLoad){
                        codes.clear();
                    }
                    page++;
                    codes.addAll(result);
                }
                adapter.notifyDataSetChanged();
                pull_gridview.onRefreshComplete();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.refund_search_clear_btn:
                this.refund_search_input.setText("");
                break;
            case R.id.but_seach:
                String BinduserId = refund_search_input.getText().toString().trim();
                closeKeybord(refund_search_input,context);
                if (!TextUtils.isEmpty(BinduserId)){
                    getData(isLoadMore,page,BinduserId);
                }
                break;
            case R.id.refund_back_btn:
                this.finish();
                break;

        }

    }

    /**
     * 关闭软键盘
     *
     */
    public static void closeKeybord(EditText mEditText, Context mContext) {
        InputMethodManager imm = (InputMethodManager) mContext
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEditText.getWindowToken(), 0);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (codes.size() > 0){
            QRCodeBean qrCodeBean =codes.get(position);
            go2StaticCodeActivity(qrCodeBean);
        }
    }


    private void go2StaticCodeActivity(QRCodeBean qrCodeBean){
        Intent intent = new Intent(this, StaticCodeActivity.class);
        qrCodeBean.switch_type = 1;
        intent.putExtra("QRCodeBean",qrCodeBean);
        this.startActivity(intent);
    }

    private class CodeAdapter extends BaseAdapter
    {


        public CodeAdapter() {
        }

        @Override
        public int getCount() {
            return codes.size();
        }

        @Override
        public Object getItem(int position) {
            return codes.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.code_list_item, null);
                holder = new SearchCodeResultActivity.ViewHolder();
                holder.ali_pay_layout = convertView.findViewById(R.id.ali_pay_layout);
                holder.all_pay_layout = convertView.findViewById(R.id.all_pay_layout);
                holder.tv_code_number = convertView.findViewById(R.id.code_number);
                holder.tv_code_id = convertView.findViewById(R.id.code_id);
                holder.iv_code_image = convertView.findViewById(R.id.code_image);
                holder.iv_icon_ali = convertView.findViewById(R.id.icon_ali);
                holder.iv_ali_code_image = convertView.findViewById(R.id.ali_code_image);
                convertView.setTag(holder);
            } else {
                holder = (SearchCodeResultActivity.ViewHolder) convertView.getTag();
            }
            holder.tv_code_number.setText(codes.get(position).cashierDesk);
            holder.tv_code_id.setText("ID:"+codes.get(position).Id+"");
            String code_content = ApiConstant.BASE_URL_PORT+"spay/scanQr?qrcodeId="+codes.get(position).qrCodeId;
            Bitmap logo = ImageUtil.base64ToBitmap(codes.get(position).qrLogo);
            Bitmap bitmap = null;
            bitmap = createCode(SearchCodeResultActivity.this,code_content,122,122,logo);
            int qrtype = codes.get(position).qrType;
            if (1 == qrtype){
                holder.ali_pay_layout.setVisibility(View.VISIBLE);
                holder.all_pay_layout.setVisibility(View.GONE);
                if (null != bitmap){
                    holder.iv_ali_code_image.setImageBitmap(bitmap);
                }
            }else{
                holder.ali_pay_layout.setVisibility(View.GONE);
                holder.all_pay_layout.setVisibility(View.VISIBLE);
                if (null != bitmap){
                    holder.iv_code_image.setImageBitmap(bitmap);
                }
            }

            return convertView;
        }

        private Bitmap createCode(Activity activity, String uuId, int width, int height, Bitmap mBitmapfinal) {
            Bitmap bitmap = null;
            int w = DisplayUtil.dip2Px(SearchCodeResultActivity.this, 122);
            int h = DisplayUtil.dip2Px(SearchCodeResultActivity.this, 122);
            try {
                if (null != mBitmapfinal){
                    bitmap = MaxCardManager.getInstance().create2DCode(activity,uuId, w, h,mBitmapfinal);
                }else{
                    bitmap = MaxCardManager.getInstance().create2DCode(uuId, w, h);
                }
            } catch (WriterException e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }

            return bitmap;
        }

    }

    SearchCodeResultActivity.ViewHolder holder = null;
    static class ViewHolder {
        TextView tv_code_number, tv_ali,tv_code_id;
        ImageView iv_code_image,iv_icon_ali,iv_ali_code_image;
        RelativeLayout all_pay_layout;
        LinearLayout ali_pay_layout;
    }
}
