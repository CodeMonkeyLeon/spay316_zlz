package cn.swiftpass.enterprise.mymould.presenter;

import com.cxy.networklibrary.CoreComponent;
import com.cxy.networklibrary.http.support.observer.CommonObserver;
import com.cxy.networklibrary.http.support.throwable.HttpThrowable;
import com.cxy.networklibrary.util.RxLifecycleUtil;
import com.trello.rxlifecycle2.android.ActivityEvent;


import cn.swiftpass.enterprise.mymould.bean.BaseBean;
import cn.swiftpass.enterprise.mymould.base.mvp.BasePresenter;
import cn.swiftpass.enterprise.mymould.bean.QueryMsgBean;
import cn.swiftpass.enterprise.mymould.contract.QueryContract;
import cn.swiftpass.enterprise.mymould.model.QueryModel;



public class QueryPresenter extends BasePresenter<QueryContract.View> implements QueryContract.Presenter {

    private QueryModel mModel;

    public QueryPresenter(QueryContract.View view) {
        mModel=new QueryModel();
        this.attachView(view);
    }

    @Override
    public void queryData(String data, String spayRs) {
        if (!isViewAttached()) {
            return;
        }

        CoreComponent.httpManager().commonRequest(mModel.queryData(data,spayRs),
                new CommonObserver<BaseBean<QueryMsgBean>>() {

                    @Override
                    public void onResult(BaseBean<QueryMsgBean> result) {
                        getView().onSuccess(result);
                    }

                    @Override
                    public void onError(HttpThrowable throwable) {
                        getView().onError(throwable);
                    }
                }, RxLifecycleUtil.bindUntilEvent(getView(), ActivityEvent.DESTROY));
    }


}
