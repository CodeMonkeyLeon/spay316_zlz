package cn.swiftpass.enterprise.newmvp.loginmodule.contract;

import com.trello.rxlifecycle2.LifecycleTransformer;

import cn.swiftpass.enterprise.newmvp.base.mvp.IModel;
import cn.swiftpass.enterprise.newmvp.base.mvp.IPresenter;
import cn.swiftpass.enterprise.newmvp.base.mvp.IView;
import cn.swiftpass.enterprise.newmvp.base.mvp.ResultCallback;
import cn.swiftpass.enterprise.newmvp.loginmodule.model.bean.ExchangeKeyBean;
import cn.swiftpass.enterprise.newmvp.loginmodule.model.bean.LoginBean;

/**
 * 创建人：caoxiaoya
 * 时间：2019/4/28
 * 描述：模块控制器
 * 备注：
 */
public interface LoginContract {

    /**
     * 用于管理M层的方法
     */
    interface Model extends IModel {

        void getLogin(String userName, String pwd, ResultCallback<LoginBean> newCall, LifecycleTransformer transformer);

        void getExchangeServiceKey(final ResultCallback<ExchangeKeyBean> newCall, LifecycleTransformer transformer);
    }

    /**
     * 用于管理UI层的方法
     */
    interface View extends IView {
        void onExchangeKeySuccess(ExchangeKeyBean result);

        void onLoginSuccess(LoginBean bean);

    }

    /**
     * 用于管理P层的方法
     */
    interface Presenter extends IPresenter {

        void exchangeServiceKey();

        void login(String userName, String userPwd);

    }
}
