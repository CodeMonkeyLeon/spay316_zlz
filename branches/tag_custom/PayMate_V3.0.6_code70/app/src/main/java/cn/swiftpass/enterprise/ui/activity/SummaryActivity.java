package cn.swiftpass.enterprise.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabSummary;
import cn.swiftpass.enterprise.ui.widget.SpayTitleView;
import cn.swiftpass.enterprise.utils.HandlerManager;

/**
 * Created by aijingya on 2019/4/17.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(从报表title进去的汇总页面activity)
 * @date 2019/4/17.19:34.
 */
public class SummaryActivity extends FragmentActivity {
    SpayTitleView spay_title_view;
    Fragment FragmentSummary;
    private FrameLayout mContent_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_summary);
        mContent_view = findViewById(R.id.fl_content_summary);
        spay_title_view = findViewById(R.id.summary_spay_title_view);

        initSummaryTitle();

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction mfragmentTransaction = fm.beginTransaction();

        if (FragmentSummary == null) {
            FragmentSummary = new FragmentTabSummary();
            mfragmentTransaction.add(R.id.fl_content_summary, FragmentSummary);
        }else {
            mfragmentTransaction.show(FragmentSummary);
        }
        mfragmentTransaction.commit();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    public void onAttachFragment(Fragment fragment){
        //当前的界面的保存状态，只是从新让新的Fragment指向了原本未被销毁的fragment，它就是onAttach方法对应的Fragment对象
         if(FragmentSummary == null && fragment instanceof FragmentTabSummary){
            FragmentSummary = fragment;
        }
    }

    public SpayTitleView getTitleView() {
        return spay_title_view;
    }

    public void cleanTitleAllView() {
        if (spay_title_view != null){
            spay_title_view.cleanAllView();
        }
    }

    public void initSummaryTitle(){
        cleanTitleAllView();
        getTitleView().addMiddleTextView(getResources().getString(R.string.tab_sum_title));
        getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));

        getTitleView().addLeftImageView(R.drawable.icon_general_top_back_default, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getTitleView().addRightTextView(getResources().getString(R.string.print), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HandlerManager.notifyMessage(HandlerManager.CLICK_TITLE,HandlerManager.SUNMMARY_PRINT_DATA);
            }
        });
    }
}
