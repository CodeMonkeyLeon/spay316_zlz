/*
 * 文 件 名:  SetCodeMoneyActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-5-13
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;
import cn.swiftpass.enterprise.ui.widget.SetMarkDialog;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.HandlerManager;

/**
 * 设置二维码金额
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2016-5-13]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class SetCodeMoneyActivity extends BaseActivity {

    private EditText et_monet;
    private TextView setMark;
    private Button but_confirm;
    private ImageView iv_clearTel;
    private String reMark = "";

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }


    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_code_money);

        initView();
        but_confirm.getBackground().setAlpha(102);
        setLister();
        showSoftInputFromWindow(SetCodeMoneyActivity.this, et_monet);
        setButBgAndFont(but_confirm);
    }

    private void setPricePoint(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (MainApplication.getInstance().getNumFixed() == 0) {//即没有小数点的位数，最小币种单位为1
                    //如果币种的最小单位为1，则不让输入小数点
                    if (s.toString().contains(".")) {
                        s = s.toString().subSequence(0, s.toString().indexOf("."));
                        editText.setText(s);
                        editText.setSelection(s.length());
                    }
                } else {//如果有小数点位数，则根据位数判断输入的截取
                    if (s.toString().contains(".")) {
                        if (s.length() - 1 - s.toString().indexOf(".") > MainApplication.getInstance().getNumFixed()) {
                            s = s.toString().subSequence(0, s.toString().indexOf(".") + MainApplication.getInstance().getNumFixed() + 1);
                            editText.setText(s);
                            editText.setSelection(s.length());
                        }
                    }
                   /* if (s.toString().contains(".")) {
                        if (s.length() - 1 - s.toString().indexOf(".") > 2) {
                            s = s.toString().subSequence(0, s.toString().indexOf(".") + 3);
                            editText.setText(s);
                            editText.setSelection(s.length());
                        }
                    }*/
                    if (s.toString().trim().substring(0).equals(".")) {
                        s = "0" + s;
                        editText.setText(s);
                        editText.setSelection(2);
                    }

                    if (s.toString().startsWith("0") && s.toString().trim().length() > 1) {
                        if (!s.toString().substring(1, 2).equals(".")) {
                            editText.setText(s.subSequence(0, 1));
                            editText.setSelection(1);
                            return;
                        }
                    }
                }

                //判断输入的金额是否校验通过
                if (et_monet.isFocused()) {
                    if (et_monet.getText().toString().length() > 0) {

                        String maxMoneyStr = "999999999.";
                        Double maxMoney = Double.parseDouble(maxMoneyStr);

                        //如果币种的最小单位为1，则不让输入小数点
                        if (MainApplication.getInstance().getNumFixed() == 0) {
                            maxMoney = Double.parseDouble(maxMoneyStr);
                        } else { //如果输入的金额带小数点
                            for (int i = 0; i < MainApplication.getInstance().getNumFixed(); i++) {
                                maxMoneyStr = maxMoneyStr + "9";
                            }
                            maxMoney = Double.parseDouble(maxMoneyStr);
                        }
                        if (!et_monet.getText().toString().contains(".") && Long.parseLong(et_monet.getText().toString()) > maxMoney) {
//                            showToastInfo(R.string.not_exceed_5w);
                            editText.setText(s.subSequence(0, et_monet.getText().toString().length() - 1));
                            editText.setSelection(et_monet.getText().toString().length());
                            return;
                        } else if (et_monet.getText().toString().contains(".") && Double.parseDouble(et_monet.getText().toString()) > maxMoney) {
//                            showToastInfo(R.string.not_exceed_5w);
                            editText.setText(s.subSequence(0, et_monet.getText().toString().length() - 1));
                            editText.setSelection(et_monet.getText().toString().length());
                            return;
                        }

                      /*  if (!et_monet.getText().toString().contains(".") && Long.parseLong(et_monet.getText().toString()) > 999999999.99) {
//                            showToastInfo(R.string.not_exceed_5w);
                            editText.setText(s.subSequence(0, et_monet.getText().toString().length() - 1));
                            editText.setSelection(et_monet.getText().toString().length());
                            return;
                        } else if (et_monet.getText().toString().contains(".") && Double.parseDouble(et_monet.getText().toString()) > 999999999.99) {
//                            showToastInfo(R.string.not_exceed_5w);
                            editText.setText(s.subSequence(0, et_monet.getText().toString().length() - 1));
                            editText.setSelection(et_monet.getText().toString().length());
                            return;
                        }*/
                        iv_clearTel.setVisibility(View.GONE);

                        setButtonBg(but_confirm, true, 0);
                    } else {
                        setButtonBg(but_confirm, false, 0);
                        iv_clearTel.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

        });

    }

    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b, enable, res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_press_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }

    private void setLister() {
        but_confirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String money = et_monet.getText().toString();

                if (TextUtils.isEmpty(money)) {
                    showToastInfo(R.string.et_hit_code_moeny);
                    return;
                } else {
                    if (money.contains(".") && money.substring(money.lastIndexOf('.')).equals(".")) {
                        showToastInfo(R.string.tv_money_error);
                        et_monet.setFocusable(true);
                        return;
                    }
                }

                if (!money.contains(".")) {
                    if (Integer.parseInt(money) <= 0) {
                        showToastInfo(R.string.please_input_amount);
                        return;
                    }
                   /* if (Integer.parseInt(money) > 50000) {
                        showToastInfo(R.string.not_exceed_5w);
                        return;
                    }*/
                } else {
                    if (Double.parseDouble(money) <= 0) {
                        showToastInfo(R.string.please_input_amount);
                        return;
                    }
                }

                String content = "";
                if (!TextUtils.isEmpty(reMark)) {
                    content = money + "|" + reMark;
                } else {
                    content = money;
                }
                HandlerManager.notifyMessage(HandlerManager.SETCODEMONEY, HandlerManager.SETCODEMONEY, content);
                SetCodeMoneyActivity.this.finish();

            }
        });

        setMark.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                SetMarkDialog dialog = new SetMarkDialog(SetCodeMoneyActivity.this, reMark, new SetMarkDialog.HandleBtn() {

                    @Override
                    public void handleOkBtn(String lang) {
                        reMark = lang;
                        setMark.setText(lang);
                    }

                }, new SetMarkDialog.HandleBtnCancle() {

                    @Override
                    public void handleCancleBtn() {

                    }

                });

                DialogHelper.resize(SetCodeMoneyActivity.this, dialog);
                dialog.show();
            }
        });

    }

    private void initView() {
        et_monet = findViewById(R.id.et_monet);
        et_monet.setMaxEms(10 + MainApplication.getInstance().getNumFixed());
        setMark = findViewById(R.id.setMark);
        setPricePoint(et_monet);

        but_confirm = findViewById(R.id.but_confirm);

        iv_clearTel = findViewById(R.id.iv_clearTel);

        setButtonBg(but_confirm, false, 0);

        //        EditTextWatcher editTextWatcher = new EditTextWatcher();
        //        
        //        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        //        {
        //            
        //            @Override
        //            public void onExecute(CharSequence s, int start, int before, int count)
        //            {
        //            }
        //            
        //            @Override
        //            public void onAfterTextChanged(Editable s)
        //            {
        //                if (et_monet.isFocused())
        //                {
        //                    if (et_monet.getText().toString().length() > 0)
        //                    {
        //                        if (Integer.parseInt(et_monet.getText().toString()) > 50000)
        //                        {
        //                            showToastInfo(R.string.not_exceed_5w);
        //                        }
        //                        iv_clearTel.setVisibility(View.VISIBLE);
        //                    }
        //                    else
        //                    {
        //                        iv_clearTel.setVisibility(View.GONE);
        //                    }
        //                }
        //            }
        //        });
        //        
        //        et_monet.addTextChangedListener(editTextWatcher);

        iv_clearTel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(et_monet.getText().toString())) {
                    et_monet.setText("");
                }
            }
        });

    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tv_code_set_moeny);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onRightButLayClick() {
            }

            @Override
            public void onLeftButtonClick() {
                SetCodeMoneyActivity.this.finish();
            }
        });

    }
}
