package cn.swiftpass.enterprise.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.SlideViewModel;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.widget.ItemCoupon;
import cn.swiftpass.enterprise.ui.widget.ImageCache.ImageLoader;

/**
 * 优惠券列表适配
 * User: Alan
 * Date: 13-12-23
 * Time: 下午4:29
 */
public class CouponAdapter extends ArrayListAdapter<SlideViewModel>
{
    private ListView mListView;
    
    private Context mContext;
    
    private OnDelItemListener delItemListener;
    
    private ImageLoader mImageLoader;
    
    public CouponAdapter(Context context)
    {
        super(context);
    }
    
    public CouponAdapter(Context context, ListView mListView)
    {
        super(context);
        this.mContext = context;
        this.mListView = mListView;
        mImageLoader = new ImageLoader();
    }
    
    public void setDelItemListener(OnDelItemListener delItemListener)
    {
        this.delItemListener = delItemListener;
    }
    
    @SuppressWarnings("null")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        ItemCoupon itemCoupon = null;
        if (convertView != null && convertView instanceof ItemCoupon)
        {
            itemCoupon = (ItemCoupon)convertView.getTag();
        }
        if (convertView == null)
        {
            convertView = (ItemCoupon)LayoutInflater.from(mContext).inflate(R.layout.listitem_coupon, null);
            itemCoupon = new ItemCoupon(mContext, convertView);
            convertView.setTag(itemCoupon);
        }
        
        final SlideViewModel c = (SlideViewModel)getItem(position);
        itemCoupon.ivIco.setTag(ApiConstant.BASE_URL_PORT + ApiConstant.DONLOAD_COU_IMAGE + c.c.couponPic);
        itemCoupon.tvAddress.setOnClickListener(new OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (c.c.collectCount <= 0)
                {
                    Toast.makeText(mContext, "没有领用人", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    delItemListener.onSeeMessage(position);
                }
            }
        });
        itemCoupon.setData(c.c, mListView, mImageLoader);
        return convertView;
    }
    
    public interface OnDelItemListener
    {
        public void onSeeMessage(int position);
    }
}
