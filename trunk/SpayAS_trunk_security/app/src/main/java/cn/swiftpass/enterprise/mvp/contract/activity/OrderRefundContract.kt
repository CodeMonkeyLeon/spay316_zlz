package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView


/**
 * @author lizheng.zhao
 * @date 2022/12/2
 *
 * @高尚的情趣会支撑你一生
 * @使你在最严酷的冬天也不忘记玫瑰的芳香
 */
class OrderRefundContract {


    interface View : BaseView {

        fun checkCanRefundOrNotSuccess(response: Boolean)

        fun checkCanRefundOrNotFailed(error: Any?)


        fun unifiedAuthPaySuccess(response: Order?)

        fun unifiedAuthPayFailed(error: Any?)

        fun authPayQuerySuccess(response: Order?)

        fun authPayQueryFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {


        fun authPayQuery(
            orderNo: String?,
            outRequestNo: String?
        )


        fun unifiedAuthPay(
            money: String?,
            authNo: String?,
            body: String
        )


        fun checkCanRefundOrNot(
            orderNo: String?,
            money: Long,
            refundMoney: Long
        )
    }

}