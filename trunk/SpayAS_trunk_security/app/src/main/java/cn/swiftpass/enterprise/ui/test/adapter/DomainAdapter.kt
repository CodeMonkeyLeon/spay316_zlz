package cn.swiftpass.enterprise.ui.test.adapter

import android.content.Context
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.ui.test.entity.DomainEntity
import cn.swiftpass.enterprise.ui.test.interfaces.OnDomainItemClickListener
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder


class DomainAdapter(
    val mContext: Context,
    val mList: ArrayList<DomainEntity>
) : BaseMultiItemQuickAdapter<DomainEntity, BaseViewHolder>(mList) {

    init {
        addItemType(DomainEntity.LAYOUT_TYPE_DOMAIN, R.layout.item_test)
    }

    private var mOnDomainItemClickListener: OnDomainItemClickListener? = null

    fun setOnDomainItemClickListener(listener: OnDomainItemClickListener) {
        mOnDomainItemClickListener = listener
    }


    override fun convert(baseViewHolder: BaseViewHolder?, domainEntity: DomainEntity?) {
        baseViewHolder?.let { helper ->
            domainEntity?.let { item ->
                if (helper.itemViewType == DomainEntity.LAYOUT_TYPE_DOMAIN) {
                    helper.setText(R.id.id_tv_domain, item.domain)
                    helper.itemView.setOnClickListener { view ->
                        mOnDomainItemClickListener?.let { listener ->
                            listener.onItemClick(helper.position, item, view)
                        }
                    }
                }
            }
        }
    }
}