package cn.swiftpass.enterprise.ui.paymentlink.view

import android.content.Context
import android.net.Uri
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ImageView
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.ui.paymentlink.interfaces.OnImageDeleteViewClickListener
import cn.swiftpass.enterprise.utils.KotlinUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners

class ImageCanDeleteView : FrameLayout {

    private lateinit var mContext: Context

    private lateinit var mImg: ImageView
    private lateinit var mBtnDelete: ImageView


    private var mOnClickListener: OnImageDeleteViewClickListener? = null

    fun setOnClickListener(listener: OnImageDeleteViewClickListener) {
        mOnClickListener = listener
    }


    constructor(context: Context) : super(context) {
        mContext = context
        init()
    }

    constructor(
        context: Context,
        attributeSet: AttributeSet?
    ) : super(context, attributeSet) {
        mContext = context
        init()
    }

    constructor(context: Context, attributeSet: AttributeSet?, def: Int) : super(
        context,
        attributeSet,
        def
    ) {
        mContext = context
        init()
    }


    private fun init() {
        //加载布局
        LayoutInflater.from(mContext).inflate(R.layout.layout_image_can_delete, this)
        //绑定控件
        mImg = findViewById(R.id.id_img)
        mBtnDelete = findViewById(R.id.id_btn_delete)

        //圆角效果
        setImage()

        //点击事件
        mImg.setOnClickListener {
            mOnClickListener?.let {
                it.onImageClick(mImg)
            }
        }
        mBtnDelete.setOnClickListener {
            mOnClickListener?.let {
                it.onDeleteClick()
            }
        }
    }


    fun setImage(imgUri: Uri? = null) {
        Glide.with(this)
            .load(imgUri)
            .error(R.drawable.icon_payment_link_default)
            .transform(CenterCrop(), RoundedCorners(KotlinUtils.dp2px(4f, mContext)))
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .into(mImg)
    }


}