package cn.swiftpass.enterprise.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo.State;
import android.widget.Toast;

import cn.swiftpass.enterprise.bussiness.logica.common.DataObserver;
import cn.swiftpass.enterprise.bussiness.logica.common.DataObserverManager;
import cn.swiftpass.enterprise.intl.R;

/**
 * 监听网络状态
 */
public class NetworkStateReceiver extends BroadcastReceiver {
    //private static final String TAG = "NetworkStateReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        State wifiState = null, mobileState = null;
        ConnectivityManager cManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null) {
            mobileState = cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState();
        }
        if (cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null) {
            wifiState = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState();
        }

        if (State.CONNECTING == mobileState || State.CONNECTING == wifiState) {
            // 正在连接中，do nothing ...
        } else if (State.DISCONNECTED == mobileState && State.DISCONNECTED == wifiState) {
            // 网络无效 进行提示并提示是否打开网络设置
            //DialogShowActivity.startActivity(DialogShowActivity.DIALOG_NETWORK);
            Toast.makeText(context, context.getString(R.string.failed_to_connect_internet), Toast.LENGTH_LONG).show();
            //todo notify
            DataObserverManager.getInstance().notifyChange(DataObserver.EVENT_NETWORK_CHANGER_DESTORY);
        } else if (State.CONNECTED == mobileState) {
            // 网络连接成功		    
            //Toast.makeText(context, "网络连接成功！", 1).show();
            //todo notify start
            DataObserverManager.getInstance().notifyChange(DataObserver.EVENT_NETWORK_CHANGER_COMMEND);
        } else if (State.CONNECTED == wifiState) {
            // 网络连接成功
            //Toast.makeText(context, "网络连接成功！", 1).show();
            //DOTO  notify start
            DataObserverManager.getInstance().notifyChange(DataObserver.EVENT_NETWORK_CHANGER_COMMEND);
        }
    }
}
