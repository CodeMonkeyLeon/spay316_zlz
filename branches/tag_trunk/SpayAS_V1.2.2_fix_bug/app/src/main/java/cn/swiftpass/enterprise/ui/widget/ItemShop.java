package cn.swiftpass.enterprise.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.ShopModel;

/**
 *  适配
 * User: Alan
 * Date: 13-12-23
 * Time: 下午4:35
 * To change this template use File | Settings | File Templates.
 */
public class ItemShop extends LinearLayout
{
    private TextView tvName, tvAddress, tvPhone;
    
    private ImageView ivIco;
    
    public ItemShop(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
    
    @Override
    protected void onFinishInflate()
    {
        tvName = (TextView)findViewById(R.id.tv_shop_name);
        tvPhone = (TextView)findViewById(R.id.tv_phone);
        tvAddress = (TextView)findViewById(R.id.tv_address);
        ivIco = (ImageView)findViewById(R.id.iv_ico);
    }
    
    public void setData(ShopModel shopModel)
    {
        tvName.setText(shopModel.shopName);
        tvPhone.setText(shopModel.linkPhone);
        tvAddress.setText(shopModel.address);
    }
}
