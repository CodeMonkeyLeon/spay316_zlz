package cn.swiftpass.enterprise.bussiness.model;

import cn.swiftpass.enterprise.io.database.table.CashierReportSumTable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * 订单统计
 * User: Administrator
 * Date: 13-12-6
 * Time: 下午3:20
 * To change this template use File | Settings | File Templates.
 */
@DatabaseTable(tableName = CashierReportSumTable.TABLE_NAME)
public class CashierReportSum implements java.io.Serializable
{
    private static final long serialVersionUID = 1L;
    
    @DatabaseField(generatedId = true)
    public long id;
    
    @DatabaseField(columnName = CashierReportSumTable.COLUMN_TRADENUM)
    //交总易数
    public Integer tradeNum;
    
    @DatabaseField(columnName = CashierReportSumTable.COLUMN_TURNOVER)
    //交易总金额
    public Long turnover;
    
    @DatabaseField(columnName = CashierReportSumTable.COLUMN_REFUND_NUM)
    //退款总数
    public Integer refundNum;
    
    @DatabaseField(columnName = CashierReportSumTable.COLUMN_REFUND_FEE)
    //退款总金额
    public Long refundFee;
    
    @DatabaseField(columnName = CashierReportSumTable.COLUMN_UID)
    public Long uId;
    
    @DatabaseField(columnName = CashierReportSumTable.COLUMN_FLAG)
    public int flag;
    
    private String tradeTime;
    
    private String outTradeNo;
    
    private String mchName;
    
    private String transactionId;
    
    private String tradeName;
    
    private String tradeState;
    
    private String tradeStateText;
    
    private String money;
    
    private String refundMoney;
    
    private String orderNoMch;
    
    /**
     * @return 返回 tradeNum
     */
    public Integer getTradeNum()
    {
        return tradeNum;
    }
    
    /**
     * @param 对tradeNum进行赋值
     */
    public void setTradeNum(Integer tradeNum)
    {
        this.tradeNum = tradeNum;
    }
    
    /**
     * @return 返回 turnover
     */
    public Long getTurnover()
    {
        return turnover;
    }
    
    /**
     * @param 对turnover进行赋值
     */
    public void setTurnover(Long turnover)
    {
        this.turnover = turnover;
    }
    
    /**
     * @return 返回 refundNum
     */
    public Integer getRefundNum()
    {
        return refundNum;
    }
    
    /**
     * @param 对refundNum进行赋值
     */
    public void setRefundNum(Integer refundNum)
    {
        this.refundNum = refundNum;
    }
    
    /**
     * @return 返回 refundFee
     */
    public Long getRefundFee()
    {
        return refundFee;
    }
    
    /**
     * @param 对refundFee进行赋值
     */
    public void setRefundFee(Long refundFee)
    {
        this.refundFee = refundFee;
    }
    
    /**
     * @return 返回 uId
     */
    public Long getuId()
    {
        return uId;
    }
    
    /**
     * @param 对uId进行赋值
     */
    public void setuId(Long uId)
    {
        this.uId = uId;
    }
    
    /**
     * @return 返回 tradeTime
     */
    public String getTradeTime()
    {
        return tradeTime;
    }
    
    /**
     * @param 对tradeTime进行赋值
     */
    public void setTradeTime(String tradeTime)
    {
        this.tradeTime = tradeTime;
    }
    
    /**
     * @return 返回 outTradeNo
     */
    public String getOutTradeNo()
    {
        return outTradeNo;
    }
    
    /**
     * @param 对outTradeNo进行赋值
     */
    public void setOutTradeNo(String outTradeNo)
    {
        this.outTradeNo = outTradeNo;
    }
    
    /**
     * @return 返回 mchName
     */
    public String getMchName()
    {
        return mchName;
    }
    
    /**
     * @param 对mchName进行赋值
     */
    public void setMchName(String mchName)
    {
        this.mchName = mchName;
    }
    
    /**
     * @return 返回 transactionId
     */
    public String getTransactionId()
    {
        return transactionId;
    }
    
    /**
     * @param 对transactionId进行赋值
     */
    public void setTransactionId(String transactionId)
    {
        this.transactionId = transactionId;
    }
    
    /**
     * @return 返回 tradeName
     */
    public String getTradeName()
    {
        return tradeName;
    }
    
    /**
     * @param 对tradeName进行赋值
     */
    public void setTradeName(String tradeName)
    {
        this.tradeName = tradeName;
    }
    
    /**
     * @return 返回 tradeState
     */
    public String getTradeState()
    {
        return tradeState;
    }
    
    /**
     * @param 对tradeState进行赋值
     */
    public void setTradeState(String tradeState)
    {
        this.tradeState = tradeState;
    }
    
    /**
     * @return 返回 tradeStateText
     */
    public String getTradeStateText()
    {
        return tradeStateText;
    }
    
    /**
     * @param 对tradeStateText进行赋值
     */
    public void setTradeStateText(String tradeStateText)
    {
        this.tradeStateText = tradeStateText;
    }
    
    /**
     * @return 返回 money
     */
    public String getMoney()
    {
        return money;
    }
    
    /**
     * @param 对money进行赋值
     */
    public void setMoney(String money)
    {
        this.money = money;
    }
    
    /**
     * @return 返回 refundMoney
     */
    public String getRefundMoney()
    {
        return refundMoney;
    }
    
    /**
     * @param 对refundMoney进行赋值
     */
    public void setRefundMoney(String refundMoney)
    {
        this.refundMoney = refundMoney;
    }
    
    /**
     * @return 返回 orderNoMch
     */
    public String getOrderNoMch()
    {
        return orderNoMch;
    }
    
    /**
     * @param 对orderNoMch进行赋值
     */
    public void setOrderNoMch(String orderNoMch)
    {
        this.orderNoMch = orderNoMch;
    }
    
}
