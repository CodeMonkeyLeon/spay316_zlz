package cn.swiftpass.enterprise.camera;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.common.sentry.SentryUtils;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.utils.ImageUtil;


/**
 * Created by aijingya on 2020/9/22.
 *
 * @Package cn.swiftpass.enterprise.camera
 * @Description:自定义camera相机
 * @date 2020/9/22.14:50.
 */
public class CameraActivity extends BaseActivity implements View.OnClickListener {

    public static final String KEY_IMAGE_PATH = "imagePath";
    private static final int CAMERA_CHOOSE_ALBUM_PICTURE = 3005;
    /**
     * 相机预览
     */
    private FrameLayout mPreviewLayout;
    /**
     * 拍摄按钮视图
     */
    private RelativeLayout mPhotoLayout;
    /**
     * 确定按钮视图
     */
    private RelativeLayout mConfirmLayout;
    /**
     * 闪光灯开启关闭按钮
     */
    private ImageView mFlashLightButton;
    /**
     * 切换摄像头方向按钮
     */
    private ImageView mReverseCameraButton;
    /**
     * 拍照按钮
     */
    private ImageView mTakePhotoButton;
    /**
     * 取消按钮
     */
    private Button mCancleButton;
    /**
     * 相册按钮
     */
    private Button mAlbumButton;
    /**
     * 聚焦视图
     */
    private OverCameraView mOverCameraView;
    /**
     * 相机类
     */
    private Camera mCamera;
    /**
     * Handle
     */
    private Handler mHandler = new Handler();
    private Runnable mRunnable;
    /**
     * 重拍按钮 retake_button
     */
    private Button mRetakeButton;
    /**
     * 保存按钮 use_photo_button
     */
    private Button mUsePhotoButton;
    /**
     * 是否开启闪光灯
     */
    private boolean isFlashing = false;
    /**
     * 图片流暂存
     */
    private byte[] imageData;
    /**
     * 拍照标记
     */
    private boolean isTakePhoto;
    /**
     * 是否正在聚焦
     */
    private boolean isFoucing;
    /**
     * 提示文案容器
     */
    private RelativeLayout rlCameraTip;
    /**
     * 蒙版类型
     */
//    private MongolianLayerType mMongolianLayerType;
    /**
     * 蒙版图片
     */
//    private ImageView mMaskImage;
    /**
     * 护照出入境蒙版
     */
//    private ImageView mPassportEntryAndExitImage;
    /**
     * 提示文案内容
     */
    private TextView tv_camera_tips_info;
    //启动相机带的参数
    private int apiCode; //1：常规相机 2：特殊相机 3：注册成功，返回登录页 4：截图当前屏幕所有内容
    private int cameraType;//1：拍正面时：Take a photo of the front of your ID. 2：拍反面时：Take a photo of the back of your ID.
    private int cameraStatus = 1;//0代表前置摄像头，1代表后置摄像头,默认是后置
    private CameraPreview preview;
    /**
     * 注释：自动对焦回调
     */
    private Camera.AutoFocusCallback autoFocusCallback = new Camera.AutoFocusCallback() {
        @Override
        public void onAutoFocus(boolean success, Camera camera) {
            isFoucing = false;
            mOverCameraView.setFoucuing(false);
            mOverCameraView.disDrawTouchFocusRect();
            //停止聚焦超时回调
            mHandler.removeCallbacks(mRunnable);
        }
    };

    /**
     * 启动拍照界面
     *
     * @param activity
     * @param requestCode
     */
    public static void startCamera(Activity activity, int requestCode, int apiCode, int cameraType) {
        Intent intent = new Intent(activity, CameraActivity.class);
        intent.putExtra("apiCode", apiCode);
        intent.putExtra("cameraType", cameraType);
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_layout);
        apiCode = getIntent().getIntExtra("apiCode", 1);
        cameraType = getIntent().getIntExtra("cameraType", 1);
        initView();
        setOnclickListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
        preview = new CameraPreview(this, mCamera);
        mOverCameraView = new OverCameraView(this);
        mPreviewLayout.addView(preview);
        mPreviewLayout.addView(mOverCameraView);
    }

    private void initView() {
        mPreviewLayout = findViewById(R.id.camera_preview_layout);
        rlCameraTip = findViewById(R.id.camera_tips);
        tv_camera_tips_info = findViewById(R.id.tv_camera_tips_info);

        mFlashLightButton = findViewById(R.id.flash_button);
        mReverseCameraButton = findViewById(R.id.camera_reverse_button);

        mConfirmLayout = findViewById(R.id.ll_confirm_layout);
        mRetakeButton = findViewById(R.id.retake_button);
        mUsePhotoButton = findViewById(R.id.use_photo_button);

        mPhotoLayout = findViewById(R.id.ll_photo_layout);
        mCancleButton = findViewById(R.id.cancle_button);
        mTakePhotoButton = findViewById(R.id.iv_take_photo_button);
        mAlbumButton = findViewById(R.id.album_button);

        //apiCode 1：常规相机 2：特殊相机 3：注册成功，返回登录页 4：截图当前屏幕所有内容
        //cameraType 1：拍正面时：Take a photo of the front of your ID. 2：拍反面时：Take a photo of the back of your ID.
        //只有apiCode == 2 时候，才根据cameraType展示字段
        switch (apiCode) {
            case 1:
                rlCameraTip.setVisibility(View.GONE);
                break;
            case 2:
                rlCameraTip.setVisibility(View.VISIBLE);
                if (cameraType == 1) {
                    tv_camera_tips_info.setText(getStringById(R.string.bdo_camera_take_front));
                } else if (cameraType == 2) {
                    tv_camera_tips_info.setText(getStringById(R.string.bdo_camera_take_back));
                }
                break;

        }

    }

    /**
     * 注释：设置监听事件
     */
    private void setOnclickListener() {
        mRetakeButton.setOnClickListener(this);
        mUsePhotoButton.setOnClickListener(this);
        mFlashLightButton.setOnClickListener(this);
        mReverseCameraButton.setOnClickListener(this);
        mCancleButton.setOnClickListener(this);
        mTakePhotoButton.setOnClickListener(this);
        mAlbumButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.flash_button://开启关闭闪光灯按钮
                switchFlash();
                break;
            case R.id.camera_reverse_button://切换摄像头方向
                switchCameraStatus();
                break;
            case R.id.retake_button://重拍按钮
                cancleSavePhoto();
                break;
            case R.id.use_photo_button://use photo 保存图片按钮
                savePhotoToAlbum();
                break;
            case R.id.cancle_button: //取消拍照按钮
                finish();
                break;
            case R.id.iv_take_photo_button: //拍照按钮
                if (!isTakePhoto) {
                    takeCameraPhoto();
                }
                break;
            case R.id.album_button: //打开相册的按钮
                openAlbum();
                break;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (!isFoucing) {
                float x = event.getX();
                float y = event.getY();
                isFoucing = true;
                if (mCamera != null && !isTakePhoto) {
                    mOverCameraView.setTouchFoucusRect(mCamera, autoFocusCallback, x, y);
                }
                mRunnable = () -> {
                    Toast.makeText(CameraActivity.this, getStringById(R.string.bdo_camera_failed_auto_focus), Toast.LENGTH_SHORT);
                    isFoucing = false;
                    mOverCameraView.setFoucuing(false);
                    mOverCameraView.disDrawTouchFocusRect();
                };
                //设置聚焦超时
                mHandler.postDelayed(mRunnable, 3000);
            }
        }
        return super.onTouchEvent(event);
    }

    /**
     * 注释：拍照---点击拍照按钮
     */
    private void takeCameraPhoto() {
        isTakePhoto = true;
        //调用相机拍照
        mCamera.takePicture(null, null, null, (data, camera1) -> {
            //视图动画
            mPhotoLayout.setVisibility(View.GONE);
            mConfirmLayout.setVisibility(View.VISIBLE);
            imageData = data;
            //停止预览
            mCamera.stopPreview();
        });
    }

    /**
     * 注释：取消保存---点击retake的时候调用
     */
    private void cancleSavePhoto() {
        mPhotoLayout.setVisibility(View.VISIBLE);
        mConfirmLayout.setVisibility(View.GONE);
        //开始预览
        mCamera.startPreview();
        imageData = null;
        isTakePhoto = false;
    }

    /**
     * 注释：保存图片---点击use photo的时候调用
     */
    private void savePhotoToAlbum() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String photoName = "camera_" + "IMG_" + timeStamp;
        Bitmap retBitmap = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);

        int CameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
        if (cameraStatus == 1) {
            CameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
        } else if (cameraStatus == 0) {
            CameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
        }
        retBitmap = BitmapUtils.setTakePicktrueOrientation(CameraId, retBitmap);

        Uri photoUri = ImageUtil.saveImageToGalleryUriByScopedStorage(this, retBitmap, photoName);

        if (photoUri != null) {
            Intent intent = new Intent();
            intent.putExtra(KEY_IMAGE_PATH, photoUri.toString());
            setResult(RESULT_OK, intent);
        } else {
            setResult(RESULT_FIRST_USER);
        }
        finish();
    }

    //切换前后摄像头
    public void switchCameraStatus() {
        if (cameraStatus == 1) {  //现在是后置，变更为前置
            mCamera.stopPreview();//停掉原来摄像头的预览
            mCamera.release();//释放资源
            mCamera = null;//取消原来摄像头

            mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
            preview = new CameraPreview(this, mCamera);
            mOverCameraView = new OverCameraView(this);
            mPreviewLayout.addView(preview);
            mPreviewLayout.addView(mOverCameraView);

            //如果切换前置摄像头，则关掉闪光灯
            isFlashing = false;
            mFlashLightButton.setSelected(isFlashing);
            mFlashLightButton.setEnabled(false);

            cameraStatus = 0;

        } else if (cameraStatus == 0) {
            mCamera.stopPreview();//停掉原来摄像头的预览
            mCamera.release();//释放资源
            mCamera = null;//取消原来摄像头

            mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
            preview = new CameraPreview(this, mCamera);
            mOverCameraView = new OverCameraView(this);
            mPreviewLayout.addView(preview);
            mPreviewLayout.addView(mOverCameraView);

            mFlashLightButton.setSelected(isFlashing);
            mFlashLightButton.setEnabled(true);

            cameraStatus = 1;

        }
    }


    /**
     * 注释：切换闪光灯
     */
    private void switchFlash() {
        //只有后置摄像头才可以开启闪光灯
        if (cameraStatus == 1) {
            isFlashing = !isFlashing;
            mFlashLightButton.setEnabled(true);
            mFlashLightButton.setSelected(isFlashing);

            try {
                Camera.Parameters parameters = mCamera.getParameters();
                parameters.setFlashMode(isFlashing ? Camera.Parameters.FLASH_MODE_TORCH : Camera.Parameters.FLASH_MODE_OFF);
                mCamera.setParameters(parameters);
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                Toast.makeText(this, getStringById(R.string.bdo_camera_flashligh_open_failed), Toast.LENGTH_SHORT);
            }
        }
    }

    /**
     * 注释：打开相册，并选择图片上传
     */
    private void openAlbum() {
        Intent albumIntent = new Intent(Intent.ACTION_PICK, null);
        albumIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(albumIntent, CAMERA_CHOOSE_ALBUM_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == CAMERA_CHOOSE_ALBUM_PICTURE
                && null != data) {
            //得到相册选择的图片uri
            Uri uri = data.getData();

            //先把图片读到byte数组里，然后对图片进行大小尺寸是否超过的判断
            byte[] image = null;
            try {
                InputStream inputStream = getContentResolver().openInputStream(uri);
                if (null == inputStream || 0 == inputStream.available()) {
                    return;
                }
                image = new byte[inputStream.available()];
                inputStream.read(image);
                inputStream.close();

            } catch (IOException e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
            }

            if (image != null && image.length > 0) {

                //如果上传的拖大于5M，则弹框提示不让上传
                if (image.length > 5 * 1024 * 1024) {
                    showToastInfo(getStringById(R.string.bdo_check_image_size));
                } else {
                    if (uri != null) {
                        Intent intent = new Intent();
                        intent.putExtra(KEY_IMAGE_PATH, uri.toString());
                        setResult(RESULT_OK, intent);
                    } else {
                        setResult(RESULT_FIRST_USER);
                    }
                    finish();
                }

            }
        }

    }


}
