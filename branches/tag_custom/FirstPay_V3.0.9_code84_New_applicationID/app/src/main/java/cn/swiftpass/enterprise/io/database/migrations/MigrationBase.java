package cn.swiftpass.enterprise.io.database.migrations;

import android.database.sqlite.SQLiteDatabase;

public abstract class MigrationBase
{
    public static final String DROP_BASE = "DROP TABLE if exists ";
    
    /**
     * 执行合并操作
     * @param db
     * @param newVersion
     * @param oldVersion
     */
    public void excute(SQLiteDatabase db, int newVersion, int oldVersion)
    {
        if(newVersion > oldVersion)
        {
            onUpgrade(db);
        }else
        if(newVersion < oldVersion)
        {
            onDowngrade(db);
        }
    }
    
	/**
	 * 向上兼容
	 * @param db
	 */
	public abstract void onUpgrade(SQLiteDatabase db);
	
	/**
     * 向下兼容
     * @param db
     */
	public abstract void onDowngrade(SQLiteDatabase db);
}
