package cn.swiftpass.enterprise.ui.activity.total;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.user.DealScatterFragmentActivity;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.PreferenceUtil;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint({"JavascriptInterface", "SetJavaScriptEnabled"})
public class OrderTotalNewActivity extends TemplateActivity
{
    
    private WebView wb;
    
    private Context mContext;
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(false);
        titleBar.setTitle(R.string.title_search);
        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.tx_distribution));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
            }
            
            @Override
            public void onRightButtonClick()
            {
            }
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                showPage(DealScatterFragmentActivity.class);
            }
        });
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_contenttext);
        initViews();
    }
    
    final class InJavaScriptLocalObj
    {
        public void showSource(String html)
        {
            Log.d("HTML", html);
        }
    }
    
    private void initViews()
    {
        mContext = this;
        wb = getViewById(R.id.webview);
        
        //        wb.addJavascriptInterface(new InJavaScriptLocalObj(), "local_obj");
        
        WebSettings settings = wb.getSettings();
        if (AppHelper.getAndroidSDKVersion() == 17)
        {
            settings.setDisplayZoomControls(false);
        }
        settings.setLoadWithOverviewMode(true);
        //settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        //各种分辨率适应
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int mDensity = metrics.densityDpi;
        if (mDensity == 120)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        }
        else if (mDensity == 160)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        }
        else if (mDensity == 240)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        }
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        
        wb.setWebChromeClient(new myWebChromeClien());
        wb.setWebViewClient(new MyWebViewClient());
        //        int title = getIntent().getIntExtra("title", R.string.title_context_text);
        //        titleBar.setTitle(title);
        //        String url = getIntent().getStringExtra("url");
        JSONObject params = new JSONObject();
        try
        {
            params.put("mchId", String.valueOf(MainApplication.getMchId()));
            Map<String, String> heards = new HashMap<String, String>();
            
            String language = PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN);
            
            Locale locale = getResources().getConfiguration().locale; //zh-rHK
            String lan = locale.getCountry();
            
            if (!TextUtils.isEmpty(language))
            {
                heards.put("fp-lang", PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN));
            }
            else
            {
                if (lan.equalsIgnoreCase("CN"))
                {
                    heards.put("fp-lang", MainApplication.LANG_CODE_ZH_CN);
                }
                else
                {
                    heards.put("fp-lang", MainApplication.LANG_CODE_ZH_TW);
                }
                
            }
            if (ApiConstant.bankCode.equals("citic_and") || ApiConstant.bankCode.equals("czb_and")
                || ApiConstant.bankCode.equals("czcb_and") || ApiConstant.bankCode.equals("zsy_and"))
            {
                wb.loadUrl(ApiConstant.BASE_URL_PORT + "spay/getTotal?&mchId=" + MainApplication.getMchId()
                    + "&bankCode=CCB", heards);
                
            }
            else if (ApiConstant.bankCode.equals("spdb_and"))
            { //浦发
                wb.loadUrl(ApiConstant.BASE_URL_PORT + "spay/getTotal?&mchId=" + MainApplication.getMchId()
                    + "&bankCode=spdb", heards);
            }
            else if (ApiConstant.bankCode.equals("ceb_and"))
            { // 光大银行
                wb.loadUrl(ApiConstant.BASE_URL_PORT + "spay/getTotal?&mchId=" + MainApplication.getMchId()
                    + "&bankCode=cebbank", heards);
            }
            else if (ApiConstant.bankCode.equalsIgnoreCase("fjnx_and"))
            {
                //福建银行
                wb.loadUrl(ApiConstant.BASE_URL_PORT + "spay/getTotal?mchId=" + MainApplication.getMchId()
                    + "&bankCode=rcc", heards);
            }
            else
            {
                
                wb.loadUrl(ApiConstant.BASE_URL_PORT + "spay/getTotal?&mchId=" + MainApplication.merchantId, heards);
            }
            wb.getSettings().setJavaScriptEnabled(true);
            wb.getSettings().setAllowFileAccessFromFileURLs(false);
            wb.getSettings().setAllowUniversalAccessFromFileURLs(false);
        }
        catch (JSONException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    class myWebChromeClien extends WebChromeClient
    {
        @Override
        public void onProgressChanged(WebView view, int newProgress)
        {
            super.onProgressChanged(view, newProgress);
            
        }
    }
    
    final class MyWebViewClient extends WebViewClient
    {
        public MyWebViewClient()
        {
            super();
        }
        
        /** {@inheritDoc} */
        
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            showLoading(false, R.string.public_loading);
            super.onPageStarted(view, url, favicon);
        }
        
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            Log.i("hehui", "shouldOverrideUrlLoading");
            showLoading(false, R.string.public_loading);
            return super.shouldOverrideUrlLoading(view, url);
            
        }
        
        @Override
        public void onPageFinished(WebView view, String url)
        {
            //            dialog.dismiss();
            dismissLoading();
            super.onPageFinished(view, url);
        }
        
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
        {
            super.onReceivedError(view, errorCode, description, failingUrl);
            dismissLoading();
            wb.loadUrl("file:///android_asset/error.html");
            
        }
    }
    
}
