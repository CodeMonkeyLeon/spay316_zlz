/*
 * 文 件 名:  PersonalManager.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-13
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.logica.shop;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.ChannelRate;
import cn.swiftpass.enterprise.bussiness.model.CountryAndUnitMode;
import cn.swiftpass.enterprise.bussiness.model.MerchantTempDataModel;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.bussiness.model.ShopBankBaseInfo;
import cn.swiftpass.enterprise.bussiness.model.ShopBaseDataInfo;
import cn.swiftpass.enterprise.io.database.access.ShopBankDataDB;
import cn.swiftpass.enterprise.io.database.access.ShopBaseDataDB;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.NetHelper;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.Utils;

/**
 * 资料完善业务处理
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-13]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class PersonalManager
{
    private static final String TAG = PersonalManager.class.getCanonicalName();
    
    private static PersonalManager personalManager;
    
    public static PersonalManager getInstance()
    {
        if (personalManager == null)
        {
            personalManager = new PersonalManager();
        }
        
        return personalManager;
    }
    

    
    /**
     * 根据手机号码查询完善资料信息
     * <功能详细描述>
     * @param userID 用户id
     * @param handleFlag  这是一个handle标识   标识要发给哪个handler的通知   如果为-1 标识不需要发通知 
     * @see [类、类#方法、类#成员]
     */
    public void queryMerchantDataByTel(final String tel, final UINotifyListener<MerchantTempDataModel> listener)
    {
        ThreadHelper.executeWithCallback(new Executable()
        {
            @Override
            public MerchantTempDataModel execute()
            {
                MerchantTempDataModel merchant = null;
                try
                {
                    JSONObject json = new JSONObject();
                    //                    json.put("clientType", String.valueOf(ApiConstant.SPAY));
                    //                    json.put("imei", "no"); //imei
                    if (!StringUtil.isEmptyOrNull(MainApplication.merchantId))
                    {
                        json.put("mchId", MainApplication.merchantId);
                    }
                    else
                    {
                        json.put("mchId", MainApplication.getMchId());
                    }
                    json.put("phone", tel);
                    

                    long spayRs = System.currentTimeMillis();
                    //                    //加签名
                    if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey()))
                    {
                        json.put("spayRs", spayRs);
                        json.put("nns",
                            SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()),
                                MainApplication.getNewSignKey()));
                    }
                    
                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.SHOPDATAQUERY, json, String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    
                    if (!result.hasError())
                    {
                        
                        if (result.data == null)
                        {
                            return null;
                        }

                        int res = Utils.Integer.tryParse(result.data.getString("result"), -1);
                        
                        switch (res)
                        {
                        // 成功
                            case 200:
                                String dataModel = result.data.getString("message");
                                
                                JSONObject js = new JSONObject(dataModel);
                                
                                merchant = new MerchantTempDataModel();
                                
                                merchant.setMerchantName(js.optString("merchantName", ""));
                                merchant.setMerchantTypeName(js.optString("merchantType", ""));
                                merchant.setPhone(js.optString("phone", ""));
                                merchant.setProvince(js.optString("province", ""));
                                merchant.setCity(js.optString("city", ""));
                                merchant.setAddress(js.optString("address", ""));
                                merchant.setEmil(js.optString("email", ""));
                                merchant.setBankUserName(js.optString("accountName", ""));
                                merchant.setIdentityNo(js.optString("accountIdEntity", ""));
                                merchant.setBank(js.optString("bankNumberCode", ""));
                                merchant.setBranchBankName(js.optString("bankName", ""));
                                merchant.setIdCardJustPic(js.optString("linencePhoto", ""));
                                merchant.setLicensePic(js.optString("indentityPhoto", ""));
                                merchant.setIdCode(js.optString("protocolPhoto", ""));
                                merchant.setPrincipalPhone(js.optString("principalPhone", ""));
                                merchant.setPrincipal(js.optString("principal", ""));

                                JSONArray RateArray = new JSONArray(js.getString("centerRate"));
                                ArrayList<ChannelRate> ChannelRateList = new ArrayList<ChannelRate>();

                                for(int i = 0 ; i < RateArray.length() ; i++){
                                    JSONObject object = RateArray.getJSONObject(i);
                                    ChannelRate rate = new ChannelRate();

                                    rate.setPayCenterId(object.optString("payCenterId", ""));
                                    rate.setCenterName(object.optString("centerName", ""));
                                    rate.setPayRate(object.optString("payRate", ""));
                                    rate.setIsClose(object.optString("isClose", ""));

                                    ChannelRateList.add(rate);
                                }
                                merchant.setCenterRate(ChannelRateList);

                                return merchant;
                                // 参数错误
                                //                            case 1:
                                //                                listener.onError("输入数据有误，请重新输入!!");
                                //                                return merchant;
                            default:
                                result.returnErrorMessage();
                                break;
                        
                        }
                    }
                    
                }
                catch (Exception e)
                {
                    return null;
                }
                return null;
            }
        },
            listener);
    }


}
