package cn.swiftpass.enterprise;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import cn.swiftpass.enterprise.util.ScreenAdapterUtil;
import cn.swiftpass.enterprise.widget.ToastUtil;

/**
 * 创建人：caoxiaoya
 * 时间：2019/4/28
 * 描述：全局Application
 * 备注：
 */
public class SpayApplication extends Application {

    private static SpayApplication mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        initLeakCanary();
        ScreenAdapterUtil.resetDensity(this);
        ToastUtil.init(this);
    }

    /**
     * debug模式下内存泄漏监控
     * @return
     */
    private void initLeakCanary() {
        if(BuildConfig.DEBUG){
            mRefWatcher = LeakCanary.install(this);
        }
    }

    public static SpayApplication getAppContext() {
        return mContext;
    }

    private RefWatcher mRefWatcher;

    public static RefWatcher getRefWatcher() {
        return mContext.mRefWatcher;
    }

    /**
     * 65535方法限制去除
     * @param base
     */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}
