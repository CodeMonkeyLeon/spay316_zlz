/*
 * 文 件 名:  ReportActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-8
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.bill;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.bill.BillOrderManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.activity.InstapayInfoActivity;
import cn.swiftpass.enterprise.ui.activity.NoteMarkActivity;
import cn.swiftpass.enterprise.ui.activity.OrderDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.PayResultActivity;
import cn.swiftpass.enterprise.ui.activity.PreAuthActivity;
import cn.swiftpass.enterprise.ui.activity.list.NewPullDownListView;
import cn.swiftpass.enterprise.ui.activity.list.PullToRefreshBase;
import cn.swiftpass.enterprise.ui.activity.list.PullToRefreshListView;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabBill;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.GlideApp;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

import static cn.swiftpass.enterprise.MainApplication.getContext;

/**
 * 根据订单号搜索订单，支持模糊查询
 * @author  Mia_Ai
 * @version  [V2.1.0, 2018-10-26]
 * @since  [2.1.0版本重构此类]
 */
public class OrderSearchActivity extends BaseActivity implements NewPullDownListView.OnRefreshListioner, NewPullDownListView.OnLoadDateRefreshListioner
{
    private static final String TAG = OrderSearchActivity.class.getSimpleName();
    private ViewHolder holder;

    private EditText et_order_search_input;
    private ImageView et_order_search_clear_btn;
    private LinearLayout ll_search_result_back_btn;
    private Button btn_order_search;
    private TextView tv_search_no_result_info;
    private LinearLayout ll_search_result ,ll_search_result_title;
    
    private List<Order> orders = new ArrayList<Order>();
    private PullToRefreshListView pullrefresh_search_result_list;
    private ListView listView;
    private BillStreamAdapter adapter;

    // 服务器返回有这个参数，说明服务器有压力，需要暂停reqFeqTime=对应的秒数才去请求返回
    private Integer reqFeqTime = 0;
    //服务器有压力，是否允许发请求去拉数据
    private boolean CanloadNext = true;
    //是否允许下拉刷新
    private boolean CanPullRefresh = true;
    //是否有数据能请求加载下一页数据
    private boolean CanloadMorePage = true;
    //当前去拉取第几页的数据
    private int pageIndex = 0;

    private Boolean isFristIn = false;

    private String saleOrPreauth;          //上一次选择的是sale 还是 pre_auth,默认是sale

    private List<DynModel> list;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        isFristIn = true;
        setContentView(R.layout.activity_order_serach);
        MainApplication.listActivities.add(this);
        initPayList();
        initview();
        setLister();
        
    }

    private void initview()
    {
        ll_search_result_title = getViewById(R.id.ll_search_result_title);
        ll_search_result_back_btn = getViewById(R.id.ll_search_result_back_btn);
        tv_search_no_result_info = getViewById(R.id.tv_search_no_result_info);
        btn_order_search = getViewById(R.id.btn_order_search);
        et_order_search_input = getViewById(R.id.et_order_search_input);
        showSoftInputFromWindow(OrderSearchActivity.this, et_order_search_input);

        //如果bankcode是AUB，同时已开通的支付方式中含有instapay的情况
        if(ApiConstant.bankCode.contains("aub") && containsInstapay() ){
            et_order_search_input.setHint(getStringById(R.string.instapay_order_search));
        }else{
            et_order_search_input.setHint(getStringById(R.string.tx_bill_stream_hint_order));
        }

        //只有开通预授权的通道才会有更改颜色的逻辑
        if(MainApplication.isPre_authOpen == 1){
            //读取上一次选择的是什么状态，然后根据状态设置title，默认是sale
            saleOrPreauth = PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth","sale");
            if(TextUtils.equals(saleOrPreauth,"sale")){
                btn_order_search.setTextColor(getResources().getColor(R.color.title_bg_new));
            }else if(TextUtils.equals(saleOrPreauth,"pre_auth")) {
                btn_order_search.setTextColor(getResources().getColor(R.color.bg_text_pre_auth));
            }
        }else{
            btn_order_search.setTextColor(getResources().getColor(R.color.title_bg_new));
        }

        et_order_search_clear_btn = getViewById(R.id.et_order_search_clear_btn);

        ll_search_result = getViewById(R.id.ll_search_result);
        pullrefresh_search_result_list = getViewById(R.id.pullrefresh_search_result_list);
        listView = pullrefresh_search_result_list.getRefreshableView();
        adapter = new BillStreamAdapter(orders);
        listView.setAdapter(adapter);

        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        {
            @Override
            public void onExecute(CharSequence s, int start, int before, int count)
            {
            }

            @Override
            public void onAfterTextChanged(Editable s)
            {
                if (et_order_search_input.isFocused())
                {
                    if (et_order_search_input.getText().toString().length() > 0)
                    {
                        et_order_search_clear_btn.setVisibility(View.VISIBLE);
                    } else {
                        et_order_search_clear_btn.setVisibility(View.GONE);
                    }
                }
            }
        });
        et_order_search_input.addTextChangedListener(editTextWatcher);
    }

    private void hideSoftKeyBoard() {
        InputMethodManager im = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        im.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


    @Override
    protected void onResume() {
        super.onResume();

        if(!isFristIn){
            ll_search_result_title.setFocusable(true);
            ll_search_result_title.setFocusableInTouchMode(true);
            ll_search_result_title.requestFocus();
            getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        }
        isFristIn = false;

    }

    private void setLister()
    {
        PullToRefreshBase.OnRefreshListener mOnrefreshListener = new PullToRefreshBase.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadDateTask(pullrefresh_search_result_list.getRefreshType(), false,et_order_search_input.getText().toString());
            }
        };
        pullrefresh_search_result_list.setOnRefreshListener(mOnrefreshListener);

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int lastItemIndex;//当前ListView中最后一个Item的索引

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                lastItemIndex = firstVisibleItem + visibleItemCount - 1 - 1;
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (lastItemIndex >= adapter.getCount() - 2) {
                    ll_search_result_title.setFocusable(true);
                    ll_search_result_title.setFocusableInTouchMode(true);
                    ll_search_result_title.requestFocus();
                    getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                    loadDateTask(3, true,et_order_search_input.getText().toString());
                }
            }
        });

        listView.setOnItemClickListener(new OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
            {
                Order order = orders.get(position-1);
                if (null != order)
                {
                    String orderNo = "";
                    //如果有预授权权限，同时账单页面选择的是预授权
                    if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth","sale"),"pre_auth")){
                        orderNo = order.getAuthNo();
                    }else{
                        orderNo = order.getOutTradeNo();
                    }

                    OrderManager.getInstance().queryOrderDetail(orderNo,
                        MainApplication.getMchId(),
                        true,
                        new UINotifyListener<Order>()
                        {
                            @Override
                            public void onError(Object object)
                            {
                                super.onError(object);
                                dismissLoading();
                                if (object != null)
                                {
                                    toastDialog(OrderSearchActivity.this, object.toString(), null);
                                }
                            }
                            
                            @Override
                            public void onPreExecute()
                            {
                                super.onPreExecute();

                                loadDialog(OrderSearchActivity.this, getStringById(R.string.public_data_loading));

                            }
                            
                            @Override
                            public void onSucceed(Order result)
                            {
                                super.onSucceed(result);
                                dismissLoading();
                                if (result != null)
                                {
                                    if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth","sale"),"pre_auth")){
                                        PreAuthActivity.startActivity(OrderSearchActivity.this,result,2);//预授权解冻 参数1
                                    }else{
                                        OrderDetailsActivity.startActivity(OrderSearchActivity.this, result);
                                    }
                                }
                            }
                            
                        });
                }
            }
        });

        btn_order_search.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (!StringUtil.isEmptyOrNull(et_order_search_input.getText().toString())) {
                    hideSoftKeyBoard();

                    //若等于16位且开头为7777，优先查invoice id，再查平台订单号
                    if(et_order_search_input.getText().toString().trim().length() == 16
                    && et_order_search_input.getText().toString().substring(0,4).equalsIgnoreCase("7777")){

                        queryOrderByInvoiceId(et_order_search_input.getText().toString().trim());

                    }else{//此处分两种情况
                        //1:输入字符若不是16位，按现有逻辑查(此种情况，查invoiceId 订单也查不到结果，故不用查invoiceID接口)
                        //2:若等于16位但开头不是7777,按现有逻辑查(此种情况，不是invoice的单号，查invoiceId 订单也查不到结果，故不用查invoiceID接口)
                        if (!CanloadNext)
                        {
                            toastDialog(OrderSearchActivity.this, R.string.tx_request_more, null);
                        } else {
                            //每次点击搜索的时候重新从第一页开始去拉取数据
                            loadDate(1, false, et_order_search_input.getText().toString());
                        }
                    }
                } else {
                    toastDialog(OrderSearchActivity.this, R.string.tv_serach_but_no_prompt, null);
                }
            }
        });

        ll_search_result_back_btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                OrderSearchActivity.this.finish();
            }
        });

        et_order_search_clear_btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                et_order_search_input.setText("");
                pullrefresh_search_result_list.setVisibility(View.GONE);
                tv_search_no_result_info.setVisibility(View.VISIBLE);
            }
        });


        et_order_search_input.setOnEditorActionListener(new OnEditorActionListener()
        {
            
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if ((actionId == EditorInfo.IME_ACTION_UNSPECIFIED || actionId == EditorInfo.IME_ACTION_SEARCH) && event != null)
                {
                    if (event.getAction() == MotionEvent.ACTION_DOWN)
                    {
                        if (!StringUtil.isEmptyOrNull(et_order_search_input.getText().toString()))
                        {
                            hideSoftKeyBoard();

                            //若等于16位且开头为7777，优先查invoice id，再查平台订单号
                            if(et_order_search_input.getText().toString().trim().length() == 16
                                    && et_order_search_input.getText().toString().substring(0,4).equalsIgnoreCase("7777")){

                                queryOrderByInvoiceId(et_order_search_input.getText().toString().trim());

                            }else{//此处分两种情况
                                //1:输入字符若不是16位，按现有逻辑查(此种情况，查invoiceId 订单也查不到结果，故不用查invoiceID接口)
                                //2:若等于16位但开头不是7777,按现有逻辑查(此种情况，不是invoice的单号，查invoiceId 订单也查不到结果，故不用查invoiceID接口)
                                if (!CanloadNext)
                                {
                                    toastDialog(OrderSearchActivity.this, R.string.tx_request_more, null);
                                } else {
                                    //每次点击搜索的时候重新从第一页开始去拉取数据
                                    loadDate(1, false, et_order_search_input.getText().toString());
                                }
                            }

                            return true;
                        }
                    }
                }
                return false;
            }
        });
    }

    public void queryOrderByInvoiceId(String invoiceId){
        OrderManager.getInstance().queryOrderByInvoiceId(invoiceId, new UINotifyListener<Order>() {
            @Override
            public void onError(Object object) {
                super.onError(object);
                //如果查询invoiceID失败，则去查一次平台订单
                if (!CanloadNext) {

                    toastDialog(OrderSearchActivity.this, R.string.tx_request_more, null);
                } else {
                    //每次点击搜索的时候重新从第一页开始去拉取数据
                    loadDate(1, false, et_order_search_input.getText().toString());
                }
            }

            @Override
            public void onPreExecute() {
                super.onPreExecute();
                loadDialog(OrderSearchActivity.this, getStringById(R.string.public_data_loading));
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(Order result) {
                super.onSucceed(result);

                if (result != null) {
                    //只要用invoiceID能够搜索到这个单，就取invoiceID查到的单的outTradeNo去查单
                    //如果请求回来有outTradeNo ,则请求订单详情数据
                    OrderManager.getInstance().queryOrderDetail(result.getOutTradeNo(),
                            MainApplication.getMchId(),
                            true,
                            new UINotifyListener<Order>()
                            {
                                @Override
                                public void onError(Object object)
                                {
                                    super.onError(object);
                                    dismissLoading();
                                    if (object != null)
                                    {
                                        toastDialog(OrderSearchActivity.this, object.toString(), null);
                                    }
                                }

                                @Override
                                public void onPreExecute()
                                {
                                    super.onPreExecute();
                                }

                                @Override
                                public void onSucceed(Order result)
                                {
                                    super.onSucceed(result);
                                    dismissLoading();
                                    if (result != null)
                                    {
                                        //只要能够搜索到这个单，就直接跳转到订单详情页面
                                        OrderDetailsActivity.startActivity(OrderSearchActivity.this, result);
                                    }

                                }

                            });


                }else{
                    //如果请求不到数据，则去查一次平台订单
                    if (!CanloadNext)
                    {
                        toastDialog(OrderSearchActivity.this, R.string.tx_request_more, null);
                    } else {
                        //每次点击搜索的时候重新从第一页开始去拉取数据
                        loadDate(1, false, et_order_search_input.getText().toString());
                    }
                }
            }
        });
    }


    void loadDateTask(int pullType, final boolean isLoadMore,String orderNum) {
        switch (pullType) {
            case 1:
                CanloadMorePage = true;
                pageIndex = 1;
                loadDate(pageIndex, isLoadMore,orderNum);
                break;
            case 2:
                pullrefresh_search_result_list.onRefreshComplete();
                break;
            case 3:
                pageIndex = pageIndex + 1;
                if (CanloadMorePage) {
                    loadDate(pageIndex, isLoadMore, orderNum);
                } else {
                    pullrefresh_search_result_list.onRefreshComplete();
                }
                break;
            default:
                break;
        }
    }

    private void setListData(final PullToRefreshListView pull, List<Order> list, BillStreamAdapter adapter,
                             List<Order> result, boolean isLoadMore, int page) {
        if (!isLoadMore) {
            list.clear();
        }
        list.addAll(result);
        adapter.notifyDataSetChanged();
        pull.onRefreshComplete();
        if (page == 1) {
            listView.setSelection(0);
        }
    }

    @Override
    public void onLoadMoreDate() {

    }




    @Override
    public void onRefresh() {
        pageIndex = 1;
        if (CanPullRefresh) {
            loadDate(pageIndex, false, et_order_search_input.getText().toString());
        }
    }

    @Override
    public void onLoadMore() {
        pageIndex = pageIndex + 1;
        loadDate(pageIndex, true, et_order_search_input.getText().toString());
    }


    //实际上去拉取列表数据
    void loadDate(final int page, final boolean isLoadMore, final String order)
    {
        BillOrderManager.getInstance().querySpayOrderNew(reqFeqTime, null, null, 0,
            page, order, 0 ,null, null, new UINotifyListener<List<Order>>() {
                @Override
                public void onPreExecute()
                {
                    super.onPreExecute();
                    reqFeqTime = 0;
                    if (page == 1) {
                        CanPullRefresh = false;
                    }
                    if (isLoadMore)
                    {
                        loadDialog(OrderSearchActivity.this, R.string.public_data_loading);
                    }
                }
                
                @Override
                public void onPostExecute()
                {
                    super.onPostExecute();
                }
                
                @Override
                public void onError(final Object object)
                {
                    super.onError(object);
                    dissDialog();
                    pullrefresh_search_result_list.onRefreshComplete();
                    CanPullRefresh = true;
                    if (checkSession()) {
                        return;
                    }
                    if (null != object)
                    {
                        if (object.toString().startsWith("reqFeqTime")) { // 服务器返回有这个参数，说明服务器有压力，需要暂停reqFeqTime=对应的秒数才去请求返回
                            String time = object.toString().substring(object.toString().lastIndexOf("=") + 1);
                            if (!StringUtil.isEmptyOrNull(time)) {
                                Integer count = Integer.parseInt(time);
                                CanloadNext = true;
                                sleep(count);
                            } else {
                                dissDialog();
                            }
                        }else {
                            dissDialog();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (!StringUtil.isEmptyOrNull(object.toString())) {
                                        toastDialog(OrderSearchActivity.this, object.toString(), null);
                                    } else {
                                        toastDialog(OrderSearchActivity.this, R.string.tx_load_fail, null);
                                    }

                                    if (orders.size() == 0) {
                                        tv_search_no_result_info.setVisibility(View.VISIBLE);
                                        pullrefresh_search_result_list.setVisibility(View.GONE);
                                        ll_search_result.setVisibility(View.GONE);
                                    }
                                }
                            });
                        }
                    }
                }
                
                @Override
                public void onSucceed(List<Order> model)
                {
                    dissDialog();
                    CanPullRefresh = true;
                    if (page == 1) {
                        orders.clear();
                    }

                    if (null != model && model.size() > 0) {
                        ll_search_result.setVisibility(View.VISIBLE);
                        pullrefresh_search_result_list.setVisibility(View.VISIBLE);
                        tv_search_no_result_info.setVisibility(View.GONE);

                        reqFeqTime = model.get(0).getReqFeqTime();
                        setListData(pullrefresh_search_result_list, orders, adapter, model, isLoadMore, page);
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                pullrefresh_search_result_list.setVisibility(View.VISIBLE);
                                ll_search_result.setVisibility(View.VISIBLE);
                                tv_search_no_result_info.setVisibility(View.GONE);

                            }
                        });
                        //暂停
                        if (reqFeqTime > 0) {
                            CanloadNext = true;
                            sleep(reqFeqTime);
                        }
                    } else {
                        CanloadMorePage = false;
                        if (orders.size() == 0) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ll_search_result.setVisibility(View.GONE);
                                    pullrefresh_search_result_list.setVisibility(View.GONE);
                                    tv_search_no_result_info.setVisibility(View.VISIBLE);
                                }
                            });
                        } else {
                            pullrefresh_search_result_list.onRefreshComplete();
                        }
                    }
                }
            });
    }
    
    /**
     * 暂停 后 才去请求
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    void sleep(long time)
    {
        try
        {
            TimerTask task = new TimerTask() {
                @Override
                public void run()
                {
                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            CanloadNext = true;
                        }
                    });
                }
            };
            Timer timer = new Timer();
            timer.schedule(task, time * 1000);
        }
        catch (Exception e)
        {
            return;
        }
    }
    

    private class BillStreamAdapter extends BaseAdapter
    {
        private List<Order> orders;
        public BillStreamAdapter(List<Order> orders)
        {
            this.orders = orders;
        }
        
        @Override
        public int getCount()
        {
            return orders.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            return orders.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(OrderSearchActivity.this, R.layout.activity_bill_stream_list_item, null);
                holder = new ViewHolder();
                holder.tv_money = (TextView)convertView.findViewById(R.id.tv_money);
                holder.tv_state = (TextView)convertView.findViewById(R.id.tv_state);
                holder.iv_type = (ImageView)convertView.findViewById(R.id.iv_type);
                holder.tv_time = (TextView)convertView.findViewById(R.id.tv_time);
                holder.iv_cover = (ImageView)convertView.findViewById(R.id.iv_cover);
               // holder.tv_pay_type = (TextView)convertView.findViewById(R.id.tv_pay_type);
                holder.tv_center = (TextView)convertView.findViewById(R.id.tv_center);
            //    holder.tv_right = (TextView)convertView.findViewById(R.id.tv_right);
                holder.view_line = (View)convertView.findViewById(R.id.v_iv);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder)convertView.getTag();
            }

            Order order = orders.get(position);
            if (null != order)
            {
                if (position == (orders.size() - 1))
                {
                    holder.view_line.setVisibility(View.GONE);
                } else {
                    holder.view_line.setVisibility(View.VISIBLE);
                }

                //如果有预授权权限，同时账单页面选择的是预授权
                if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth","sale"),"pre_auth") ){
                    if (!StringUtil.isEmptyOrNull(order.getTradeTimeNew()))
                    {
                        holder.tv_time.setText(order.getFormatTimePay()+" "+order.getTradeTimeNew());
                    }

                    String orderNoMch=StringUtil.paseStrToMarkStr(order.authNo);

                    if (!StringUtil.isEmptyOrNull(orderNoMch))
                    {
                        String seachContent = et_order_search_input.getText().toString().trim();
                        SpannableStringBuilder builder = new SpannableStringBuilder(orderNoMch);

                        if (!TextUtils.isEmpty(seachContent) && orderNoMch.contains(seachContent))
                        {
                            //只筛选尾号后四位
                            for(int index = 8 ; index < orderNoMch.length() ; index ++){
                                String currentStr = null;
                                if(index+seachContent.length() >= orderNoMch.length()){
                                    currentStr =  orderNoMch.substring(index);
                                }else{
                                    currentStr =  orderNoMch.substring(index,index+seachContent.length());
                                }
                                if(seachContent.equals(currentStr)){
                                    builder.setSpan(new ClickableSpan() {
                                        @Override
                                        public void onClick(View widget) {
                                        }

                                        @Override
                                        public void updateDrawState(TextPaint ds) {
                                            super.updateDrawState(ds);
                                            ds.setColor(getResources().getColor(R.color.title_bg_new));
                                            ds.setUnderlineText(false);
                                        }
                                    }, index, index + seachContent.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                                }
                            }
                        }

                        holder.tv_center.setText(builder);
                    }

                    if (order.getMoney() >= 0)
                    {
                        holder.tv_money.setVisibility(View.VISIBLE);
                        holder.tv_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(order.getMoney()));
                    }
                    holder.tv_state.setText(MainApplication.getPreAuthtradeStateMap().get(order.getTradeState() + ""));
                    switch (order.getTradeState()) {
                        case 1:
                            //未授权
                            holder.tv_state.setTextColor(getResources().getColor(R.color.bg_setting_right_text));
                            holder.tv_money.setTextColor(getResources().getColor(R.color.bg_setting_right_text));
                            holder.iv_cover.setVisibility(View.VISIBLE);
                            break;
                        case 2:
                            //已授权
                            holder.tv_state.setTextColor(Color.parseColor("#03C700"));
                            holder.tv_money.setTextColor(getResources().getColor(R.color.black));
                            holder.iv_cover.setVisibility(View.GONE);
                            break;
                        case 3:
                            //已撤销
                            holder.tv_state.setTextColor(getResources().getColor(R.color.bg_setting_right_text));
                            holder.tv_money.setTextColor(getResources().getColor(R.color.bg_setting_right_text));
                            holder.iv_cover.setVisibility(View.VISIBLE);
                            break;
                        default:
                            holder.tv_state.setTextColor(getResources().getColor(R.color.bg_setting_right_text));
                            holder.tv_money.setTextColor(getResources().getColor(R.color.bg_setting_right_text));
                            holder.iv_cover.setVisibility(View.VISIBLE);
                            break;
                    }

                    Object object = SharedPreUtile.readProduct("payTypeIcon" + ApiConstant.bankCode + MainApplication.getMchId());
                    if (object != null) {
                        try {
                            Map<String, String> typePicMap = (Map<String, String>) object;
                            if (typePicMap != null && typePicMap.size() > 0) {
                                String picUrl = typePicMap.get(order.getApiProvider() + "");
                                if (!StringUtil.isEmptyOrNull(picUrl)) {
                                    GlideApp.with(getContext())
                                            .load(picUrl)
                                            .placeholder(R.drawable.icon_general_receivables)
                                            .error(R.drawable.icon_general_receivables)
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .into(holder.iv_type);
                                } else {
                                    holder.iv_type.setImageResource(R.drawable.icon_general_receivables);
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG,Log.getStackTraceString(e));
                        }
                    } else {

                        switch (order.getApiProvider()) {
                            case 1:
                                //微信
                                holder.iv_type.setImageResource(R.drawable.icon_general_wechat);
                                break;
                            case 2:
                                //支付宝
                                holder.iv_type.setImageResource(R.drawable.icon_general_pay);
                                break;
                            default:
                                holder.iv_type.setImageResource(R.drawable.icon_general_receivables);
                                break;
                        }
                    }
                }else{
                    if (!StringUtil.isEmptyOrNull(order.getTradeTimeNew()))
                    {
                        try
                        {
                            holder.tv_time.setText(order.getFormatTimePay()+" "+order.getTradeTimeNew());
                        } catch (Exception e) {
                            Log.e(TAG,Log.getStackTraceString(e));
                        }
                    }


                    String orderNoMch=StringUtil.paseStrToMarkStr(order.getOrderNoMch());
                    if (!StringUtil.isEmptyOrNull(orderNoMch))
                    {
                        String seachContent = et_order_search_input.getText().toString().trim();
                        SpannableStringBuilder builder = new SpannableStringBuilder(orderNoMch);

                        if (!TextUtils.isEmpty(seachContent) && orderNoMch.contains(seachContent))
                        {
                            for(int index = 0 ; index < orderNoMch.length() ; index ++){
                                String currentStr = null;
                                if(index+seachContent.length() >= orderNoMch.length()){
                                    currentStr =  orderNoMch.substring(index);
                                }else{
                                    currentStr =  orderNoMch.substring(index,index+seachContent.length());
                                }
                                if(seachContent.equals(currentStr)){
                                    builder.setSpan(new ClickableSpan() {
                                        @Override
                                        public void onClick(View widget) {
                                        }

                                        @Override
                                        public void updateDrawState(TextPaint ds) {
                                            super.updateDrawState(ds);
                                            ds.setColor(getResources().getColor(R.color.title_bg_new));
                                            ds.setUnderlineText(false);
                                        }
                                    }, index, index + seachContent.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                                }
                            }
                        }
                        holder.tv_center.setText(builder);
                    }

                    if (order.getMoney() >= 0)
                    {
                        holder.tv_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(order.getMoney()));
                    }
                    holder.tv_state.setText(order.getTradeStateText());

                    Object object = SharedPreUtile.readProduct("payTypeIcon" + ApiConstant.bankCode + MainApplication.getMchId());
                    if (object != null)
                    {
                        try
                        {
                            Map<String, String> typePicMap = (Map<String, String>)object;
                            if (typePicMap != null && typePicMap.size() > 0)
                            {
                                String picUrl = typePicMap.get(order.getApiProvider() + "");
                                if (!StringUtil.isEmptyOrNull(picUrl))
                                {
                                    GlideApp.with(getContext())
                                            .load(picUrl)
                                            .placeholder(R.drawable.icon_general_receivables)
                                            .error(R.drawable.icon_general_receivables)
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .into(holder.iv_type);
                                } else {
                                    holder.iv_type.setImageResource(R.drawable.icon_general_receivables);
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG,Log.getStackTraceString(e));
                        }
                    } else {
                        switch (order.getApiProvider()) {
                            case 1: //微信
                                holder.iv_type.setImageResource(R.drawable.icon_general_wechat);
                                break;
                            case 2: //支付宝
                                holder.iv_type.setImageResource(R.drawable.icon_general_pay);
                                break;
                            default:
                                holder.iv_type.setImageResource(R.drawable.icon_general_receivables);
                                break;
                        }
                    }

                    switch (order.getTradeState())
                    {
                        case 2:
                            //支付成功
                            holder.tv_state.setTextColor(Color.parseColor("#03C700"));
                            holder.tv_money.setTextColor(getResources().getColor(R.color.black));
                            holder.iv_cover.setVisibility(View.GONE);
                            break;
                        case 1: //未支付
                            //                        holder.tv_state.setTextColor(getResources().getColor(R.color.bill_item_fail));
                            holder.tv_state.setTextColor(getResources().getColor(R.color.bg_setting_right_text));
                            holder.tv_money.setTextColor(getResources().getColor(R.color.bg_setting_right_text));
                            holder.iv_cover.setVisibility(View.VISIBLE);
                            break;
                        case 3: //关闭
                            //                        holder.tv_state.setTextColor(getResources().getColor(R.color.bill_item_fail));
                            holder.tv_state.setTextColor(getResources().getColor(R.color.bg_setting_right_text));
                            holder.tv_money.setTextColor(getResources().getColor(R.color.bg_setting_right_text));
                            holder.iv_cover.setVisibility(View.VISIBLE);
                            break;
                        case 4: //转入退款
                            //                        holder.tv_state.setTextColor(getResources().getColor(R.color.bill_item_refund));
                            holder.tv_state.setTextColor(getResources().getColor(R.color.pay_fail));
                            holder.tv_money.setTextColor(getResources().getColor(R.color.black));
                            holder.iv_cover.setVisibility(View.GONE);
                            break;
                        case 8:  //已撤销
                            //                        holder.tv_state.setTextColor(getResources().getColor(R.color.bill_item_fail));
                            holder.tv_state.setTextColor(getResources().getColor(R.color.bg_setting_right_text));
                            holder.tv_money.setTextColor(getResources().getColor(R.color.bg_setting_right_text));
                            holder.iv_cover.setVisibility(View.VISIBLE);
                            break;
                        default:
                            holder.tv_state.setTextColor(getResources().getColor(R.color.bg_setting_right_text));
                            holder.tv_money.setTextColor(getResources().getColor(R.color.bg_setting_right_text));
                            holder.iv_cover.setVisibility(View.VISIBLE);
                            break;
                    }
                }
            }
            return convertView;
        }
    }
    
    private class ViewHolder
    {
        private TextView  tv_time,  tv_money, tv_state;
        
        private ImageView iv_type , iv_cover;
        
        private TextView tv_center;

        private View view_line;
    }

    public void initPayList(){
        Object object;
        //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth","sale");
        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(saleOrPreauth,"pre_auth")){
            object = SharedPreUtile.readProduct("dynPayType_pre_auth" + ApiConstant.bankCode + MainApplication.getMchId());
        }else{
            object = SharedPreUtile.readProduct("ActiveDynPayType" + ApiConstant.bankCode + MainApplication.getMchId());
        }
        if (object != null){
            list = (List<DynModel>)object;
        }
    }

    //判断当前的支付列表中是否含有 instapay的支付通道
    public boolean containsInstapay(){
        for(int i = 0 ; i < list.size() ; i++){
            if(getNativePayType(list.get(i).getApiCode()).equalsIgnoreCase(MainApplication.instapayServiceType)){
                return  true;
            }
        }
        return false;
    }



    //通过ApiCode得到当前支付类型的NativePayType
    public String getNativePayType(String ApiCode){
        if (null != list && list.size() > 0) {
            for(int i = 0 ; i < list.size();i++){
                if(list.get(i).getApiCode().equalsIgnoreCase(ApiCode)){
                    return list.get(i).getNativeTradeType();
                }
            }
        } else {
            return null;
        }
        return null;
    }
}
