/*
 * 文 件 名:  AgentManager.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2014-8-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.logica.agent;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.AgentInfo;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.NetHelper;

/**
 * 代理商业务逻辑处理
 * 
 * @author  he_hui
 * @version  [版本号, 2014-8-11]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class AgentManager
{
    
    // 我要申请
    private static String queryAgent = ApiConstant.BASE_URL_PORT + "merchant/AgentAction_getAllAgent";
    
    /**
     * 查询代理联系方式
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    public static void queryAllAgent(final UINotifyListener<ArrayList<AgentInfo>> listener)
    {
        ThreadHelper.executeWithCallback(new Executable()
        {
            @Override
            public List<AgentInfo> execute()
            
            {
                try
                {
                    JSONObject json = new JSONObject();
                    json.put("clientType ", ApiConstant.SPAY_SUB);
                    
                    RequestResult result = NetHelper.httpsPostBanckArrJson(queryAgent, json, null, null);
                    result.setNotifyListener(listener);
                    if (!result.hasError())
                    {
                        
                        return paseJson(result.arr);
                        
                    }
                    
                }
                catch (JSONException e)
                {
                    listener.onError("获取失败，请稍后在试!!");
                    return null;
                }
                return null;
                
            }
            
        }, listener);
    }
    
    private static List<AgentInfo> paseJson(JSONArray data)
        throws JSONException
    {
        List<AgentInfo> list = new ArrayList<AgentInfo>();
        if (data.length() > 0)
        {
            for (int i = 0; i < data.length(); i++)
            {
                JSONObject obj = data.getJSONObject(i);
                AgentInfo info = new AgentInfo();
                info.setAgentPhone(obj.getString("agentPhone"));
                info.setCity(obj.optString("city", ""));
                list.add(info);
            }
        }
        
        return list;
    }
}
