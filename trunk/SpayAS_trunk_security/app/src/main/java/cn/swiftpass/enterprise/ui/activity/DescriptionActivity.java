package cn.swiftpass.enterprise.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import cn.swiftpass.enterprise.bussiness.model.WxCard;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;
import cn.swiftpass.enterprise.ui.widget.TitleBar;

public class DescriptionActivity extends BaseActivity {

    private WxCard orderMode;
    private TextView verification_tv_desc;

    public static void startActivity(Context context, WxCard orderModel) {
        Intent it = new Intent();
        it.setClass(context, DescriptionActivity.class);
        it.putExtra("order", orderModel);
        context.startActivity(it);
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description_details);

        Intent intent = getIntent();
        orderMode = (WxCard) intent.getSerializableExtra("order");

        verification_tv_desc = findViewById(R.id.verification_tv_desc);

        verification_tv_desc.setText(orderMode.getDealDetail());
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle("使用说明");
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButLayClick() {

            }
        });
    }
}
