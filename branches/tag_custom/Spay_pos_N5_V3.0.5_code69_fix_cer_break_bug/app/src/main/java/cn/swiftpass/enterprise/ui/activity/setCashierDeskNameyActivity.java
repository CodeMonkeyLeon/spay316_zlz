/*
 * 文 件 名:  SetCodeMoneyActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-5-13
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.scan.ScanbindActivity;
import cn.swiftpass.enterprise.ui.widget.SetMarkDialog;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 设置二维码收银员
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2016-5-13]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class setCashierDeskNameyActivity extends TemplateActivity {

    private EditText et_monet;

    private Button but_confirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_cashier_desk);
        MainApplication.listActivities.add(this);
        initView();
        but_confirm.getBackground().setAlpha(102);
        setLister();
        showSoftInputFromWindow(setCashierDeskNameyActivity.this, et_monet);
        setButBgAndFont(but_confirm);
    }

    private void setPricePoint(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (et_monet.isFocused()) {
                    if (et_monet.getText().toString().trim().length() > 0) {
                        setButtonBg(but_confirm, true, 0);
                    } else {
                        setButtonBg(but_confirm, false, 0);
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String inputStr = s.toString();
                if (inputStr.length() >= 15){
                    closeKeybord(editText,setCashierDeskNameyActivity.this);
                }

            }

        });

    }

    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b, enable, res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_press_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }

    private void setLister() {
        but_confirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String money = et_monet.getText().toString().trim();
                if (TextUtils.isEmpty(money)) {
                    showToastInfo(R.string.et_hit_desk_name);
                    return;
                } else {
                    start2ScanBindCode(money);
                }
            }
        });



    }

    private void start2ScanBindCode(String deskName){
        Intent intent  = new Intent();
        intent.setClass(this,ScanbindActivity.class);
        intent.putExtra("cashierDeskName",deskName);
        startActivity(intent);
    }

    private void initView() {
        et_monet = getViewById(R.id.et_monet);
        setPricePoint(et_monet);
        but_confirm = getViewById(R.id.but_confirm);
    }

    /**
     * 关闭软键盘
     *
     */
    public static void closeKeybord(EditText mEditText, Context mContext) {
        InputMethodManager imm = (InputMethodManager) mContext
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEditText.getWindowToken(), 0);
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.bind_cashier_desk);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onRightButLayClick() {
            }

            @Override
            public void onLeftButtonClick() {
                setCashierDeskNameyActivity.this.finish();
            }
        });

    }
}
