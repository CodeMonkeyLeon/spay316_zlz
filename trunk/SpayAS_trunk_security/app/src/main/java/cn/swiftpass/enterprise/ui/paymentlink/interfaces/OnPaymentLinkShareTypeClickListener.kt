package cn.swiftpass.enterprise.ui.paymentlink.interfaces

interface OnPaymentLinkShareTypeClickListener {

    fun onChoose(type: Int)

}