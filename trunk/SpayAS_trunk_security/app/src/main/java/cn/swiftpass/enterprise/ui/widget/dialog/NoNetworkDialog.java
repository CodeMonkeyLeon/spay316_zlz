package cn.swiftpass.enterprise.ui.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import cn.swiftpass.enterprise.intl.R;

/**
 * Created by aijingya on 2021/6/17.
 *
 * @Package cn.swiftpass.enterprise.ui.widget.dialog
 * @Description:
 * @date 2021/6/17.16:05.
 */
public class NoNetworkDialog extends Dialog {
    Context mContext;
    TextView tv_content;
    LinearLayout ll_bottom_confirm;
    TextView btnOpen;

    private View.OnClickListener mOpenListener;

    public NoNetworkDialog(@NonNull Context context,View.OnClickListener openListener) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mContext = context;
        mOpenListener = openListener;
        init();
    }

    public void init(){
        View rootView = View.inflate(mContext, R.layout.dialog_no_net, null);
        setContentView(rootView);
        setCanceledOnTouchOutside(false);

        tv_content = (TextView)rootView.findViewById(R.id.tv_content);
        ll_bottom_confirm = (LinearLayout)rootView.findViewById(R.id.ll_bottom_confirm);
        btnOpen = (TextView) rootView.findViewById(R.id.btnOpen);

        btnOpen.setOnClickListener(mOpenListener);
    }

    public void setContentText(String  text){
        tv_content.setText(text);
    }
    public void setContentTextSize(int size){
        tv_content.setTextSize(size);
    }

    public void setContentText(int text){
        tv_content.setText(text);
    }

    public void setConfirmText(int text){
        btnOpen.setText(text);
    }

}
