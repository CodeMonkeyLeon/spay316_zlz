package cn.swiftpass.enterprise.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.swiftpass.enterprise.intl.R;

/**
 * Created by aijingya on 2018/5/11.
 *
 * @Package cn.swiftpass.enterprise.ui.widget
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/5/11.11:42.
 */

public class SpayTitleView extends RelativeLayout {
    private RelativeLayout mTitleLayout;
    private LinearLayout mLeftLayout;
    private LinearLayout mMiddleLayout;
    private LinearLayout mRightLayout;
    private Context mContext;
    private LayoutInflater mInflater;
    private int spaceHor;
    private int spaceVer;

    public SpayTitleView(Context context) {
        super(context);
        init(context);
    }

    public SpayTitleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SpayTitleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }


    public void init(Context context){
        mContext = context;
        spaceHor = mContext.getResources().getDimensionPixelSize(R.dimen.act_space_hor);
        spaceVer = mContext.getResources().getDimensionPixelSize(R.dimen.act_space_ver);
        mInflater = LayoutInflater.from(mContext);
        mTitleLayout = (RelativeLayout) mInflater.inflate(R.layout.spay_title_view, null);
        addView(mTitleLayout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        mLeftLayout = (LinearLayout) getView(R.id.llayout_left);
        mMiddleLayout = (LinearLayout) getView(R.id.llayout_middle);
        mRightLayout = (LinearLayout) getView(R.id.llayout_right);
    }

    public void cleanAllView() {
        mLeftLayout.removeAllViews();
        mMiddleLayout.removeAllViews();
        mRightLayout.removeAllViews();
    }

    public void setTitleBackgroundColor(int id) {
        mTitleLayout.setBackgroundColor(id);
    }

    public void setTitleBackground(int id) {
        mTitleLayout.setBackgroundResource(id);
    }

    public View getView(int id) {
        return mTitleLayout.findViewById(id);
    }

    public ImageView getImageView(int id) {
        ImageView iv = (ImageView) mInflater.inflate(R.layout.title_imageview, null);
        iv.setImageResource(id);
        return iv;
    }

    public TextView getTextView(Object text) {
        TextView tv = (TextView) mInflater.inflate(R.layout.title_textview, null);
        if (text instanceof CharSequence) {
            tv.setText((CharSequence) text);
        } else if (text instanceof String) {
            tv.setText((String) text);
        } else if (text instanceof Integer) {
            tv.setText((Integer) text);
        }
        return tv;
    }

    public TextView addLeftTextView(Object text) {
        TextView tv = getTextView(text);
        int leftPadding = spaceHor;
        tv.setPadding(leftPadding, 0, 0, 0);
        mLeftLayout.addView(tv);
        return tv;
    }

    public TextView addLeftTextView(Object text, OnClickListener mListener) {
        TextView tv = addLeftTextView(text);
        if (mListener != null){
            tv.setOnClickListener(mListener);
        }
        return tv;
    }

    public ImageView addLeftImageView(int id) {
        ImageView iv = getImageView(id);
        int leftPadding = spaceHor;
        int rightPadding = spaceHor;

        iv.setPadding(leftPadding, 0, rightPadding, 0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        mLeftLayout.addView(iv, layoutParams);
        return iv;
    }

    public ImageView addLeftImageView(int id,OnClickListener mListener){
        ImageView iv = addLeftImageView(id);
        if(mListener != null){
            iv.setOnClickListener(mListener);
        }
        return iv;
    }


    public TextView addRightTextView(Object text) {
        TextView tv = getTextView(text);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.font_size_title));
        tv.setPadding(0, 0, spaceHor, 0);
        mRightLayout.addView(tv);
        return tv;
    }

    public TextView addRightTextView(Object text,OnClickListener mListener){
        TextView tv = addRightTextView(text);
        if (mListener != null){
            tv.setOnClickListener(mListener);
        }
        return tv;
    }

    public ImageView addRightImageView(int id) {
        ImageView iv = getImageView(id);
        iv.setPadding(0, 0, spaceHor, 0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        mRightLayout.addView(iv, layoutParams);
        return iv;
    }

    public ImageView addRightImageView(int id, OnClickListener mListener) {
        ImageView iv = null;
        iv = addRightImageView(id);
        if (mListener != null){
            iv.setOnClickListener(mListener);
        }
        return iv;
    }

    public TextView addMiddleTextView(Object text) {
        TextView tv = getTextView(text);
        mMiddleLayout.addView(tv);
        return tv;
    }

    public ImageView addMiddleImageView(int id){
        ImageView iv = getImageView(id);
        int leftPadding = spaceVer;
        iv.setPadding(leftPadding, 0, 0, 0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        mMiddleLayout.addView(iv, layoutParams);
        return iv;
    }

    public TextView addMiddleTextView(Object text,OnClickListener mListener){

        TextView tv = addMiddleTextView(text);
        if (mListener != null){
            tv.setOnClickListener(mListener);
        }
        return tv;
    }


    public void OnClickMiddleLayout(OnClickListener mListener){
        LinearLayout linearLayout_middle = getMiddletLayout();
        if (mListener != null){
            linearLayout_middle.setOnClickListener(mListener);
        }
    }

    public void addMiddleView(View view) {
        mMiddleLayout.addView(view);
    }

    public LinearLayout getMiddletLayout() {
        return mMiddleLayout;
    }

    public ImageView replaceLeftImageView(int position,int id ){
        ImageView iv = getImageView(id);
        int leftPadding = spaceHor;
        int rightPadding = spaceHor;

        iv.setPadding(leftPadding, 0, rightPadding, 0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);

        View view=mLeftLayout.getChildAt(position);
        mLeftLayout.addView(iv, position, layoutParams);
        mLeftLayout.removeView(view);
        return iv;
    }

    public TextView replaceMiddleTextView(int position,Object text ){
        TextView iv = getTextView(text);
        View view=mMiddleLayout.getChildAt(position);
        mMiddleLayout.addView(iv, position);
        mMiddleLayout.removeView(view);
        return iv;
    }

    public ImageView replaceRightImageView(int position,int id){
        ImageView iv = getImageView(id);
        iv.setPadding(0, 0, spaceHor, 0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        View view = mRightLayout.getChildAt(position);
        mRightLayout.addView(iv, position, layoutParams);
        mRightLayout.removeView(view);
        return iv;
    }

    public void removeLeftChildView(){
        mLeftLayout.removeAllViews();
    }

    public void removeMiddleChildView(){
        mMiddleLayout.removeAllViews();
    }

    public void removeRightChildView(){
        mRightLayout.removeAllViews();
    }
}
