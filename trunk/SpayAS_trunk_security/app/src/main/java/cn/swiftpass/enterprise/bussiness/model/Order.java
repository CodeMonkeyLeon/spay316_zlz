package cn.swiftpass.enterprise.bussiness.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.text.TextUtils;

import cn.swiftpass.enterprise.bussiness.enums.PayType;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.database.table.OrderTable;
import cn.swiftpass.enterprise.utils.ToastHelper;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = OrderTable.TABLE_NAME)
public class Order implements java.io.Serializable {
    public static enum WxCardType {
        GROUPON_CARD("GROUPON"), CARSH_CARD("CASH"), DISCOUNT_CARD("DISCOUNT"), GIFT_CARD("GIFT");

        public String cardType;

        private WxCardType(String type) {
            this.cardType = type;
        }

        public static String getCardNameForType(String type) {
            if (null == type) {
                return "UNKNOW";
            }
            if (type.equalsIgnoreCase(GROUPON_CARD.cardType)) {
                return ToastHelper.toStr(R.string.active_groupon_card);
            } else if (type.equalsIgnoreCase(CARSH_CARD.cardType)) {
                return ToastHelper.toStr(R.string.active_carsh_card);
            } else if (type.equalsIgnoreCase(DISCOUNT_CARD.cardType)) {
                return ToastHelper.toStr(R.string.active_discount_card);
            } else if (type.equalsIgnoreCase(GIFT_CARD.cardType)) {
                return ToastHelper.toStr(R.string.active_gift_card);
            } else {
                return "UNKNOW";
            }
        }
    }

    public static enum TradeTypetoStr {
        PAY_WX_MICROPAY("pay.weixin.micropay"), PAY_ZFB_MICROPAY("pay.alipay.micropay"), PAY_QQ_MICROPAY(
                "pay.qq.micropay"), PAY_WX_NATIVE("pay.weixin.native"), PAY_ZFB_NATIVE("pay.alipay.native"), PAY_QQ_NATIVE(
                "pay.qq.jspay"), PAY_WX_JSPAY("pay.weixin.jspay"), PAY_QQ_PROXY_MICROPAY("pay.qq.proxy.micropay"), PAY_ALIPAY_WAP(
                "pay.alipay.wappay"), PAY_WX_WEB_SCAN("pay.weixin.scancode"), // 微信扫码支付
        PAY_WX_APP("pay.weixin.app"), PAY_ALIPAY_APP("pay.alipay.app"), PAY_JINGDONG("pay.jdpay.micropay"),
        ;
        private String tradeType;

        private TradeTypetoStr(String tradeType) {
            this.tradeType = tradeType;
        }

        public static String getTradeNameForType(String tradeType) {
            if (tradeType.equals(PAY_QQ_MICROPAY.tradeType) || tradeType.equals(PAY_QQ_PROXY_MICROPAY.tradeType)) {
                return ToastHelper.toStr(R.string.qq_code_pay);
            } else if (tradeType.equals(PAY_ZFB_MICROPAY.tradeType)) {
                return ToastHelper.toStr(R.string.zfb_code_pay);
            } else if (tradeType.equals(PAY_QQ_NATIVE.tradeType)) {
                return ToastHelper.toStr(R.string.qq_scan_pay);
            } else if (tradeType.equals(PAY_ZFB_NATIVE.tradeType)) {
                return ToastHelper.toStr(R.string.zfb_scan_code_pay);
            } else if (tradeType.equals(PAY_WX_NATIVE.tradeType)) {
                return ToastHelper.toStr(R.string.weChat_pay_type);
            } else if (tradeType.equals(PAY_WX_JSPAY.tradeType)) {// pay.weixin.jspay
                return ToastHelper.toStr(R.string.show_wx_jspay);
            } else if (tradeType.equals(PAY_ALIPAY_WAP.tradeType)) {
                return ToastHelper.toStr(R.string.show_zfb_wappay);
            } else if (tradeType.equalsIgnoreCase(PAY_WX_WEB_SCAN.tradeType)) {
                return ToastHelper.toStr(R.string.show_wx_webpay);
            } else if (tradeType.equalsIgnoreCase(PAY_WX_APP.tradeType)) {
                return ToastHelper.toStr(R.string.show_wx_apppay);
            } else if (tradeType.equalsIgnoreCase(PAY_ALIPAY_APP.tradeType)) {
                return ToastHelper.toStr(R.string.show_zfb_apppay);
            } else if (tradeType.equalsIgnoreCase(PAY_JINGDONG.tradeType)) {
                return ToastHelper.toStr(R.string.show_jd_micpay);
            } else {
                return ToastHelper.toStr(R.string.wechat_car_pay);
            }
        }
    }


    //{
    //	"transactionId": "",
    //	"mchId": "100580000055",
    //	"mchName": "",
    //	"operno": "100580000055",
    //	"outTradeNo": "10058000005519869117512120643",
    //	"money": 101,
    //	"tradeState": 1,
    //	"tradeStateText": "未支付",
    //	"addTime": "",
    //	"tradeTime": "",
    //	"tradeTimeNew": "2022-03-04 15:33:27",
    //	"totalFee": 101,
    //	"orderFee": 100,
    //	"notifyTime": "",
    //	"tradeType": "",
    //	"apiProvider": 1,
    //	"providerName": "",
    //	"tradeName": "",
    //	"orderNoMch": "19925339751202203041276675872",
    //	"body": "",
    //	"deviceInfo": "",
    //	"affirm": 0,
    //	"client": ""
    //}
    //{
    //	"outTradeNo": "10058000005525292513803152277",
    //	"userId": "",
    //	"addTime": 1651802605694,
    //	"mchId": "100580000055",
    //	"tradeState": 1,
    //	"refundState": "",
    //	"refundTime": "",
    //	"page": "",
    //	"pageSize": 10,
    //	"orderNoMch": "10202205061076683126",
    //	"tradeType": "pay.upi.native.intl",
    //	"transactionId": "",
    //	"tradeTime": 1651802605694,
    //	"tradeTimeNew": "2022-05-06 10:03:25",
    //	"notifyTime": 1651802605694,
    //	"body": "www",
    //	"client": "SPAY_AND",
    //	"rufundMark": 0,
    //	"deviceInfo": "",
    //	"mchName": "yinliantest01",
    //	"realName": "",
    //	"userName": "yinliantest01",
    //	"money": 101,
    //	"totalFee": 101,
    //	"refundMoney": 0,
    //	"rfMoneyIng": 0,
    //	"canAffirm": 0,
    //	"affirm": "",
    //	"affirmUser": "",
    //	"barCodePath": "",
    //	"tradeName": "銀聯二維碼-掃碼付款",
    //	"wxVcards": "",
    //	"daMoney": 0,
    //	"isAgainPay": 0,
    //	"tradeStateText": "",
    //	"wxVcardsList": "",
    //	"langCode": "",
    //	"isRefundStream": 0,
    //	"attach": "",
    //	"apiCode": "20",
    //	"surcharge": 1,
    //	"withholdingTax": 2,
    //	"tipFee": 0,
    //	"orderFee": 100,
    //	"cashFee": 0,
    //	"refundRemainFee": 101,
    //	"voucherNo": "15632",
    //	"outMchId": "19925339751",
    //	"upiDeviceId": "15632",
    //	"unsettledDiscountFee": 0,
    //	"discountDetail": "",
    //	"costFee": "",
    //	"invoiceId": ""
    //}
    private static final long serialVersionUID = 1L;

    @DatabaseField(generatedId = true)
    public long id;

    /**
     * 订单ID
     */
    @DatabaseField(columnName = OrderTable.COLUMN_ORDER_NO)
    public String orderNo;

    /**
     * 用户ID
     */
    @DatabaseField(columnName = OrderTable.COLUMN_U_ID)
    public long employeeId;

    @DatabaseField(columnName = OrderTable.COLUMN_M_ID)
    /**商户ID*/
    public String merchantId;

    public String deviceInfo;

    /**
     * 订单状态
     */
    @DatabaseField(columnName = OrderTable.COLUMN_STATE)
    public String state;

    /**
     * 订单 产品
     */
    @DatabaseField(columnName = OrderTable.COLUMN_PRODUCT)
    public String productName;

    /**
     * 订单 金额
     */
    @DatabaseField(columnName = OrderTable.COLUMN_MONEY)
    public long money;

    @DatabaseField(columnName = OrderTable.COLUMN_REMARK)
    public String remark;

    /**
     * 添加时间
     */
    @DatabaseField(columnName = OrderTable.COLUMN_ADD_TIME)
    public long add_time;

    /**
     * 完成订单时间
     */
    @DatabaseField(columnName = OrderTable.COLUMN_FINISH_TIME)
    public long finishTime;

    @DatabaseField(columnName = OrderTable.COLUMN_NOTIFY_TIME)
    public long notify_time;

    @DatabaseField(columnName = OrderTable.COLUMN_CLIENT_TYPE)
    public int clientType;

    @DatabaseField(columnName = OrderTable.COLUMN_USER_NAME)
    public String userName;

    @DatabaseField(columnName = OrderTable.COLUMN_BANK_NAME)
    public String bankTypeDisplay;

    @DatabaseField(columnName = OrderTable.COLUMN_TRANSACTIONID)
    public String transactionId;

    //交易状类型  pos交易 wx交易 优惠券wx支付
    public int transactionType;

    public String notifyTime;

    public String addTime;

    public long uid = 0;

    public String operno;

    public Integer canAffim;//是否可以核销，1，可以，0不可以

    public String apiCode;

    public String mchCreateIp;
    public String timeStart;
    public String subOpenID;
    public String need_query;

    public String orderfinalStatus;//1.已撤销 2.已冲正

    public String getOrderfinalStatus() {
        return orderfinalStatus;
    }

    public void setOrderfinalStatus(String orderfinalStatus) {
        this.orderfinalStatus = orderfinalStatus;
    }

    public String getMchCreateIp() {
        return mchCreateIp;
    }

    public void setMchCreateIp(String mchCreateIp) {
        this.mchCreateIp = mchCreateIp;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getSubOpenID() {
        return subOpenID;
    }

    public void setSubOpenID(String subOpenID) {
        this.subOpenID = subOpenID;
    }

    public String getNeed_query() {
        return need_query;
    }

    public void setNeed_query(String need_query) {
        this.need_query = need_query;
    }

    public String outRequestNo;

    public String getOutRequestNo() {
        return outRequestNo;
    }

    public void setOutRequestNo(String outRequestNo) {
        this.outRequestNo = outRequestNo;
    }

    public String getApiCode() {
        return apiCode;
    }

    public void setApiCode(String apiCode) {
        this.apiCode = apiCode;
    }

    public Integer getCanAffim() {
        return canAffim;
    }

    public void setCanAffim(Integer canAffim) {
        this.canAffim = canAffim;
    }

    public String getOperno() {
        return operno;
    }

    public void setOperno(String operno) {
        this.operno = operno;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public Integer tradeMode;

    public Integer tradeState;

    public String tradeStateText;

    public String payInfo;

    public String partner;

    public String bankType;

    public String bankBillno;

    public Long totalFee;

    public Integer feeType;


    public String notifyId;

    public String outTradeNo;

    public String outAuthNo;

    public String authNo;

    public String timeEnd;

    public Integer transportFee;

    public Integer productFee;

    public String buyerAlias;

    public String merchant;

    public String orderState;

    public String useId;//优惠券ID

    public String tradeTimeNew;

    public Integer apiProvider;

    public String providerName;

    public Map<String, String> map;

    public String addTimeNew;

    public String refundTimeNew;

    public String formartYYMM;

    public String requestNo;

    public String getRequestNo() {
        return requestNo;
    }

    public void setRequestNo(String requestNo) {
        this.requestNo = requestNo;
    }

    public String useTimeNew;

    public Integer reqFeqTime;

    public Integer operationStatus;  //操作状态(0 -初始 1-成功 2-失败)

    public Integer operationType;   //操作类型（1-转支付,2-解冻,3-撤销,4-退款）

    public Integer getOperationStatus() {
        return operationStatus;
    }

    public void setOperationStatus(Integer operationStatus) {
        this.operationStatus = operationStatus;
    }

    public Integer getOperationType() {
        return operationType;
    }

    public void setOperationType(Integer operationType) {
        this.operationType = operationType;
    }


    public long totalFreezeAmount;
    public long restAmount;
    public long totalPayAmount;

    public String invoiceId; //AUB 新增字段 	invoice id

    public long getTotalFreezeAmount() {
        return totalFreezeAmount;
    }

    public void setTotalFreezeAmount(long totalFreezeAmount) {
        this.totalFreezeAmount = totalFreezeAmount;
    }

    public long getRestAmount() {
        return restAmount;
    }

    public void setRestAmount(long restAmount) {
        this.restAmount = restAmount;
    }

    public long getTotalPayAmount() {
        return totalPayAmount;
    }

    public void setTotalPayAmount(long totalPayAmount) {
        this.totalPayAmount = totalPayAmount;
    }

    public Integer getReqFeqTime() {
        return reqFeqTime;
    }

    public void setReqFeqTime(Integer reqFeqTime) {
        this.reqFeqTime = reqFeqTime;
    }

    public String getUseTimeNew() {
        return useTimeNew;
    }

    public void setUseTimeNew(String useTimeNew) {
        this.useTimeNew = useTimeNew;
    }

    public String getFormartYYMM() {
        return formartYYMM;
    }

    public void setFormartYYMM(String formartYYMM) {
        this.formartYYMM = formartYYMM;
    }

    public String getRefundTimeNew() {
        return refundTimeNew;
    }

    public void setRefundTimeNew(String refundTimeNew) {
        this.refundTimeNew = refundTimeNew;
    }

    public String getAddTimeNew() {
        return addTimeNew;
    }

    public void setAddTimeNew(String addTimeNew) {
        this.addTimeNew = addTimeNew;
    }

    public Map<String, String> getMap() {
        return map;
    }

    public void setMap(Map<String, String> map) {
        this.map = map;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public Integer getApiProvider() {
        return apiProvider;
    }

    public void setApiProvider(Integer apiProvider) {
        this.apiProvider = apiProvider;
    }

    public String getTradeTimeNew() {
        return tradeTimeNew;
    }

    public void setTradeTimeNew(String tradeTimeNew) {
        this.tradeTimeNew = tradeTimeNew;
    }

    //临时字段
    public String qrCodeImgUrl;//优惠券领取二维码地址

    public String coupMoney;//优惠券金额

    public String issueyNum;//赠送张数

    public String bankCardName; //银行类型

    public String wxUserName;//交易后的用户名

    public String bankName; // 银行类型

    public boolean isMark; // 标示

    public String termno;// 终端编号

    public String mchNo;

    public String refundTime;

    public long refundMoney;

    public long rfMoneyIng; // 退款中的money

    public int affirm; // 确认订单

    public String cardCode; // 卡券序列号

    public String cardTitle;

    public String status; // 卡券核销状态

    public String nickName;

    public long createTime;

    public Integer useStatus;

    public long useTime;

    public String vcardUseTime;

    public long daMoney; // 优惠金额

    public List<WxCard> wxCardList;

    public WxCard wxCard;

    public Integer isAgainPay;

    public String cardType;// 卡券类型 (groupon:团购劵；cash:代金劵；discount:折扣劵；gift:礼品劵)

    public String gift;// 礼品名称

    public Integer discount; // 折扣额度

    public String attach;

    public long refundFeel;

    public int canAffirm;

    public String formatTimePay;

    public String formatRefund;

    public String fromatCard;

    public Integer pageCount = 0;//页数

    public long cashFeel;//折算人民币

    public boolean isPay = false;

    public String authorizationCode;//授权码
    public String deviceId;
    public String retrievalNumber;//检索参考号
    public String fee3DS;//3DS费用
    public String currency;//订单币种
    public long refundRemainFee;//剩余可退金额
    public String paymentType;//交易类型
    public long orderAmount;//商品金额
    public Integer refundMark;
    public String service;//支付类型

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getRetrievalNumber() {
        return retrievalNumber;
    }

    public void setRetrievalNumber(String retrievalNumber) {
        this.retrievalNumber = retrievalNumber;
    }

    public String getFee3DS() {
        return fee3DS;
    }

    public void setFee3DS(String fee3DS) {
        this.fee3DS = fee3DS;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public long getRefundRemainFee() {
        return refundRemainFee;
    }

    public void setRefundRemainFee(long refundRemainFee) {
        this.refundRemainFee = refundRemainFee;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public long getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(long orderAmount) {
        this.orderAmount = orderAmount;
    }

    public Integer getRefundMark() {
        return refundMark;
    }

    public void setRefundMark(Integer refundMark) {
        this.refundMark = refundMark;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public boolean isPay() {
        return isPay;
    }

    public void setPay(boolean isPay) {
        this.isPay = isPay;
    }

    public String printInfo;

    public String cashFee;

    public String getPrintInfo() {
        return printInfo;
    }

    public void setPrintInfo(String printInfo) {
        this.printInfo = printInfo;
    }

    public long getCashFeel() {
        return cashFeel;
    }

    public void setCashFeel(long cashFeel) {
        this.cashFeel = cashFeel;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public String getFromatCard() {
        return fromatCard;
    }

    public void setFromatCard(String fromatCard) {
        this.fromatCard = fromatCard;
    }

    public String getFormatRefund() {
        return formatRefund;
    }

    public void setFormatRefund(String formatRefund) {
        this.formatRefund = formatRefund;
    }

    public String getFormatTimePay() {
        return formatTimePay;
    }

    public void setFormatTimePay(String formatTimePay) {
        this.formatTimePay = formatTimePay;
    }

    public int getCanAffirm() {
        return canAffirm;
    }

    public void setCanAffirm(int canAffirm) {
        this.canAffirm = canAffirm;
    }

    public long getRefundFeel() {
        return refundFeel;
    }

    public void setRefundFeel(long refundFeel) {
        this.refundFeel = refundFeel;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getGift() {
        return gift;
    }

    public void setGift(String gift) {
        this.gift = gift;
    }

    public Integer getReduceCost() {
        return reduceCost;
    }

    public void setReduceCost(Integer reduceCost) {
        this.reduceCost = reduceCost;
    }

    public Integer reduceCost;// 减免金额(分)

    public Integer getIsAgainPay() {
        return isAgainPay;
    }

    public void setIsAgainPay(Integer isAgainPay) {
        this.isAgainPay = isAgainPay;
    }

    public WxCard getWxCard() {
        return wxCard;
    }

    public void setWxCard(WxCard wxCard) {
        this.wxCard = wxCard;
    }

    public long getDaMoney() {
        return daMoney;
    }

    public List<WxCard> getWxCardList() {
        return wxCardList;
    }

    public void setWxCardList(List<WxCard> wxCardList) {
        this.wxCardList = wxCardList;
    }

    public void setDaMoney(long daMoney) {
        this.daMoney = daMoney;
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public Integer getUseStatus() {
        return useStatus;
    }

    public void setUseStatus(Integer useStatus) {
        this.useStatus = useStatus;
    }

    public long getUseTime() {
        return useTime;
    }

    public void setUseTime(long useTime) {
        this.useTime = useTime;
    }

    public String getVcardUseTime() {
        return vcardUseTime;
    }

    public void setVcardUseTime(String vcardUseTime) {
        this.vcardUseTime = vcardUseTime;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String brandName;

    public String dealDetail;

    public int getAffirm() {
        return affirm;
    }

    public String getDealDetail() {
        return dealDetail;
    }

    public void setDealDetail(String dealDetail) {
        this.dealDetail = dealDetail;
    }

    public void setAffirm(int affirm) {
        this.affirm=affirm;
    }

    public long getRfMoneyIng() {
        return rfMoneyIng;
    }

    public void setRfMoneyIng(long rfMoneyIng) {
        this.rfMoneyIng = rfMoneyIng;
    }

    public long getRefundMoney() {
        return refundMoney;
    }

    public void setRefundMoney(long refundMoney) {
        this.refundMoney = refundMoney;
    }

    public String getRefundTime() {
        return refundTime;
    }

    public void setRefundTime(String refundTime) {
        this.refundTime = refundTime;
    }

    public String getTermno() {
        return termno;
    }

    public void setTermno(String termno) {
        this.termno = termno;
    }

    public String getMchNo() {
        return mchNo;
    }

    public void setMchNo(String mchNo) {
        this.mchNo = mchNo;
    }

    public boolean isMark() {
        return isMark;
    }

    public void setMark(boolean isMark) {
        this.isMark = isMark;
    }

    /**
     * 是否本地生成订单
     */
    @DatabaseField(columnName = OrderTable.COLUMN_CREATE_ORDER_TYPE)
    public int createOrderType;

    public int payType = PayType.PAY_WX.getValue();//交易类型默认微信m

    public String posCardNo;//pos交易有卡号

    public String mchId;

    public String mchName;

    public int userId;

    public String centerId;

    public String tradeTime;

    public Boolean notifyState;

    public String tradeType;

    public String tradeTypeText;

    public String body;

    public String orderNoMch;

    public String code; // 卡券编号

    public String title;//卡券主题

    public String openid;

    public String client; // 终端类型

    public Integer rufundMark; // 退款标志，1代表可以退款，0代表不能退款

    public String outRefundNo;

    public String cardId;

    public long totalRows;

    public String description; // 使用说明

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(long totalRows) {
        this.totalRows = totalRows;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getOutRefundNo() {
        return outRefundNo;
    }

    public void setOutRefundNo(String outRefundNo) {
        this.outRefundNo = outRefundNo;
    }

    public Integer getRufundMark() {
        return rufundMark;
    }

    public void setRufundMark(Integer rufundMark) {
        this.rufundMark = rufundMark;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getOrderNoMch() {
        return orderNoMch;
    }

    public void setOrderNoMch(String orderNoMch) {
        this.orderNoMch = orderNoMch;
    }

    @Override
    public String toString() {
        return "Order [orderNoMch=" + orderNoMch + "]";
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getRefundNo() {
        return refundNo;
    }

    public void setRefundNo(String refundNo) {
        this.refundNo = refundNo;
    }

    public int refundState;

    public String refundNo;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public long getEmployeeId() {
        return employeeId;
    }


    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public long getAdd_time() {
        return add_time;
    }


    public void setAdd_time(long add_time) {
        this.add_time = add_time;
    }

    public long getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(long finishTime) {
        this.finishTime = finishTime;
    }

    public long getNotify_time() {
        return notify_time;
    }

    public void setNotify_time(long notify_time) {
        this.notify_time = notify_time;
    }

    public int getClientType() {
        return clientType;
    }

    public void setClientType(int type) {
        clientType = type;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBankTypeDisplay() {
        return bankTypeDisplay;
    }

    public void setBankTypeDisplay(String bankTypeDisplay) {
        this.bankTypeDisplay = bankTypeDisplay;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public int getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(int transactionType) {
        this.transactionType = transactionType;
    }

    public String getNotifyTime() {
        return notifyTime;
    }

    public void setNotifyTime(String notifyTime) {
        this.notifyTime = notifyTime;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public Integer getTradeMode() {
        return tradeMode;
    }

    public void setTradeMode(Integer tradeMode) {
        this.tradeMode = tradeMode;
    }

    public Integer getTradeState() {
        return tradeState;
    }

    public void setTradeState(Integer tradeState) {
        this.tradeState = tradeState;
    }

    public String getPayInfo() {
        return payInfo;
    }

    public void setPayInfo(String payInfo) {
        this.payInfo = payInfo;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getBankType() {
        return bankType;
    }

    public void setBankType(String bankType) {
        this.bankType = bankType;
    }

    public String getBankBillno() {
        return bankBillno;
    }

    public void setBankBillno(String bankBillno) {
        this.bankBillno = bankBillno;
    }

    public Long getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(Long totalFee) {
        this.totalFee = totalFee;
    }

    public Integer getFeeType() {
        return feeType;
    }

    public void setFeeType(Integer feeType) {
        this.feeType = feeType;
    }

    public String getNotifyId() {
        return notifyId;
    }

    public void setNotifyId(String notifyId) {
        this.notifyId = notifyId;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getOutAuthNo() {
        return outAuthNo;
    }

    public void setOutAuthNo(String outAuthNo) {
        this.outAuthNo = outAuthNo;
    }

    public String getAuthNo() {
        return authNo;
    }

    public void setAuthNo(String authNo) {
        this.authNo = authNo;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public Integer getTransportFee() {
        return transportFee;
    }

    public void setTransportFee(Integer transportFee) {
        this.transportFee = transportFee;
    }

    public Integer getProductFee() {
        return productFee;
    }

    public void setProductFee(Integer productFee) {
        this.productFee = productFee;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getBuyerAlias() {
        return buyerAlias;
    }

    public void setBuyerAlias(String buyerAlias) {
        this.buyerAlias = buyerAlias;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public String getOrderState() {
        return orderState;
    }

    public void setOrderState(String orderState) {
        this.orderState = orderState;
    }

    public String getUseId() {
        return useId;
    }

    public void setUseId(String useId) {
        this.useId = useId;
    }

    public String getQrCodeImgUrl() {
        return qrCodeImgUrl;
    }

    public void setQrCodeImgUrl(String qrCodeImgUrl) {
        this.qrCodeImgUrl = qrCodeImgUrl;
    }


    public String getCoupMoney() {
        return coupMoney;
    }

    public void setCoupMoney(String coupMoney) {
        this.coupMoney = coupMoney;
    }

    public String getIssueyNum() {
        return issueyNum;
    }

    public void setIssueyNum(String issueyNum) {
        this.issueyNum = issueyNum;
    }

    public String getBankCardName() {
        return bankCardName;
    }

    public void setBankCardName(String bankCardName) {
        this.bankCardName = bankCardName;
    }

    public String getWxUserName() {
        return wxUserName;
    }

    public void setWxUserName(String wxUserName) {
        this.wxUserName = wxUserName;
    }

    public int getCreateOrderType() {
        return createOrderType;
    }

    public void setCreateOrderType(int createOrderType) {
        this.createOrderType = createOrderType;
    }

    public int getPayType() {
        return payType;
    }

    public void setPayType(int payType) {
        this.payType = payType;
    }

    public String getPosCardNo() {
        return posCardNo;
    }

    public void setPosCardNo(String posCardNo) {
        this.posCardNo = posCardNo;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getMchName() {
        return mchName;
    }

    public void setMchName(String mchName) {
        this.mchName = mchName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer id) {
        userId = id;
    }

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }

    public String getTradeTime() {
        return tradeTime;
    }

    public void setTradeTime(String tradeTime) {
        this.tradeTime = tradeTime;
    }

    public Boolean getNotifyState() {
        return notifyState;
    }

    public void setNotifyState(Boolean notifyState) {
        this.notifyState = notifyState;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public String getSignType() {
        return signType;
    }

    public void setSignType(String signType) {
        this.signType = signType;
    }

    public void setMoney(Long money) {
        this.money = money;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String tradeName;

    public String charset;

    public String signType;

    public long getMoney() {
        return money;
    }

    public String getBankName() {
        if (TextUtils.isEmpty(bankCardName)) {
            return bankTypeDisplay;
        } else {
            return bankCardName;
        }
    }

    public int getRefundState() {
        return refundState;
    }

    public void setRefundState(int state) {
        refundState = state;
    }

    public String getTradeStateText() {
        return tradeStateText;
    }

    public void setTradeStateText(String tradeStateText) {
        this.tradeStateText = tradeStateText;
    }

    public String getTradeTypeText() {
        return tradeTypeText;
    }

    public void setTradeTypeText(String tradeTypeText) {
        this.tradeTypeText = tradeTypeText;
    }

    /**
     * 附加手续费
     */
    public Long surcharge = 0L;
    /**
     * 预扣税费
     */
    public Long withholdingTax = 0L;

    public Long getOrderFee() {
        return orderFee;
    }

    public void setOrderFee(Long orderFee) {
        this.orderFee = orderFee;
    }

    //商品金额（单位：分）
    public Long orderFee = 0L;

    public Long getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(Long surcharge) {
        this.surcharge = surcharge;
    }

    public Long getWithholdingTax() {
        return withholdingTax;
    }

    public void setWithholdingTax(Long withholdingTax) {
        this.withholdingTax = withholdingTax;
    }

    public Long getTipFee() {
        return tipFee;
    }

    public void setTipFee(Long tipFee) {
        this.tipFee = tipFee;
    }

    /**
     * 小费
     */
    public Long tipFee = 0L;


    public long vat;              //增值税（单位：分）
    public long refundFee;       //通道的退款金额（单位：分）

    public long getVat() {
        return vat;
    }

    public void setVat(long vat) {
        this.vat = vat;
    }

    public long getRefundFee() {
        return refundFee;
    }

    public void setRefundFee(long refundFee) {
        this.refundFee = refundFee;
    }

    public String unFreezeTime;

    public String getUnFreezeTime() {
        return unFreezeTime;
    }

    public void setUnFreezeTime(String unFreezeTime) {
        this.unFreezeTime = unFreezeTime;
    }

    public String getOutTransactionId() {
        return outTransactionId;
    }

    public void setOutTransactionId(String outTransactionId) {
        this.outTransactionId = outTransactionId;
    }

    public String outTransactionId;
    public String operateTime;

    public String operateTimeFormat;

    public String getOperateTime() {
        return operateTime;
    }

    public void setOperateTime(String operateTime) {
        this.operateTime = operateTime;
    }

    public void setOperateTimeFormat(String operateTime) {
        this.operateTimeFormat = operateTime;
    }

    public String getOperateTimeFormat() {
        return operateTimeFormat;
    }

    public String getFinishedTime() {
        return finishedTime;
    }

    public void setFinishedTime(String finishedTime) {
        this.finishedTime = finishedTime;
    }

    public String finishedTime;
    public long totalUnfreezeAmount;
    public String note;//备注

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public long getTotalUnfreezeAmount() {
        return totalUnfreezeAmount;
    }

    public void setTotalUnfreezeAmount(long totalUnfreezeAmount) {
        this.totalUnfreezeAmount = totalUnfreezeAmount;
    }


    public String getTradename() {
        return tradename;
    }

    public void setTradename(String tradename) {
        this.tradename = tradename;
    }

    //预授权反扫下单一次成功的情况
    public String tradename;

    public long getUnsettledDiscountFee() {
        return unsettledDiscountFee;
    }

    public void setUnsettledDiscountFee(long unsettledDiscountFee) {
        this.unsettledDiscountFee = unsettledDiscountFee;
    }

    public long getCostFee() {
        return costFee;
    }

    public void setCostFee(long costFee) {
        this.costFee = costFee;
    }

    /**
     * 未计入结算金额
     */
    public long unsettledDiscountFee;

    /**
     * 消费者实付金额
     */
    public long costFee;


    public ArrayList<UplanDetailsBean> getUplanDetailsBeans() {
        return uplanDetailsBeans;
    }

    public void setUplanDetailsBeans(ArrayList<UplanDetailsBean> uplanDetailsBeans) {
        this.uplanDetailsBeans = uplanDetailsBeans;
    }

    /*
     * uplan 活动优惠详情
     */
    public ArrayList<UplanDetailsBean> uplanDetailsBeans;

    public ArrayList<UplanDetailsBean> discountDetail;

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }
}