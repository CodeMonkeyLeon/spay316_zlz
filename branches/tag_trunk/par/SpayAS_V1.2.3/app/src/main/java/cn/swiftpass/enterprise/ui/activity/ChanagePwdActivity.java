package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 修改密码
 * User: Administrator
 * Date: 13-10-16
 * Time: 下午4:20
 * To change this template use File | Settings | File Templates.
 */
public class ChanagePwdActivity extends TemplateActivity implements View.OnClickListener
{
    
    private EditText etOldPwd, etNewPwd, etNewPwd2;
    
    private ImageView iv_clearOldpwd, iv_clearNewpwd, iv_clearNewpwd2;
    
    private Button btn_login;
    
    private DialogInfo dialogInfo;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        initView();
        
    }
    
    private void initView()
    {
        setContentView(R.layout.activity_changepwd);
        
        btn_login = getViewById(R.id.btn_login);
        
        etOldPwd = getViewById(R.id.et_oldpwd);
        etNewPwd = getViewById(R.id.et_newpwd);
        etNewPwd2 = getViewById(R.id.et_newpwd2);
        
        showSoftInputFromWindow(ChanagePwdActivity.this, etOldPwd);
        btn_login.getBackground().setAlpha(102);
        iv_clearOldpwd = getViewById(R.id.iv_clearOld);
        iv_clearOldpwd.setOnClickListener(this);
        iv_clearNewpwd = getViewById(R.id.iv_clearNewpwd);
        iv_clearNewpwd.setOnClickListener(this);
        iv_clearNewpwd2 = getViewById(R.id.iv_clearNewpwd2);
        iv_clearNewpwd2.setOnClickListener(this);
        
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        {
            
            @Override
            public void onExecute(CharSequence s, int start, int before, int count)
            {
            }
            
            @Override
            public void onAfterTextChanged(Editable s)
            {
                
                if (s != null && s.length() >= 1)
                {
                    if (etOldPwd.isFocused())
                    {
                        iv_clearOldpwd.setVisibility(View.VISIBLE);
                        iv_clearNewpwd.setVisibility(View.GONE);
                        iv_clearNewpwd2.setVisibility(View.GONE);
                    }
                    else if (etNewPwd.isFocused())
                    {
                        iv_clearNewpwd.setVisibility(View.VISIBLE);
                        iv_clearOldpwd.setVisibility(View.GONE);
                        iv_clearNewpwd2.setVisibility(View.GONE);
                    }
                    else if (etNewPwd2.isFocused())
                    {
                        iv_clearNewpwd2.setVisibility(View.VISIBLE);
                        iv_clearNewpwd.setVisibility(View.GONE);
                        iv_clearOldpwd.setVisibility(View.GONE);
                    }
                }
                else
                {
                    if (etOldPwd.isFocused())
                    {
                        iv_clearOldpwd.setVisibility(View.GONE);
                        iv_clearNewpwd.setVisibility(View.GONE);
                        iv_clearNewpwd2.setVisibility(View.GONE);
                    }
                    else if (etNewPwd.isFocused())
                    {
                        iv_clearOldpwd.setVisibility(View.GONE);
                        iv_clearNewpwd.setVisibility(View.GONE);
                        iv_clearNewpwd2.setVisibility(View.GONE);
                    }
                    else if (etNewPwd2.isFocused())
                    {
                        iv_clearOldpwd.setVisibility(View.GONE);
                        iv_clearNewpwd.setVisibility(View.GONE);
                        iv_clearNewpwd2.setVisibility(View.GONE);
                    }
                }
                
                if (!StringUtil.isEmptyOrNull(etOldPwd.getText().toString())
                    && !StringUtil.isEmptyOrNull(etNewPwd.getText().toString())
                    && !StringUtil.isEmptyOrNull(etNewPwd2.getText().toString()))
                {
                    setButtonBg(btn_login, true, 0);
                }
                else
                {
                    setButtonBg(btn_login, false, 0);
                }
            }
        });
        etOldPwd.addTextChangedListener(editTextWatcher);
        etNewPwd.addTextChangedListener(editTextWatcher);
        etNewPwd2.addTextChangedListener(editTextWatcher);
        
        etOldPwd.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View view, boolean b)
            {
                if (b)
                {
                    if (etOldPwd.getText().length() >= 1)
                    {
                        iv_clearOldpwd.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        iv_clearOldpwd.setVisibility(View.GONE);
                    }
                }
                else
                {
                    iv_clearOldpwd.setVisibility(View.GONE);
                }
            }
        });
        
        etNewPwd.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View view, boolean b)
            {
                if (b)
                {
                    if (etNewPwd.getText().length() >= 1)
                    {
                        iv_clearNewpwd.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        iv_clearNewpwd.setVisibility(View.GONE);
                    }
                }
                else
                {
                    iv_clearNewpwd.setVisibility(View.GONE);
                }
            }
        });
        etNewPwd2.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View view, boolean b)
            {
                if (b)
                {
                    if (etNewPwd2.getText().length() >= 1)
                    {
                        iv_clearNewpwd2.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        iv_clearNewpwd2.setVisibility(View.GONE);
                    }
                }
                else
                {
                    iv_clearNewpwd2.setVisibility(View.GONE);
                }
            }
        });
        
        /**
         * 可配置设置
         */
        DynModel dynModel = (DynModel)SharedPreUtile.readProduct("dynModel" + ApiConstant.bankCode);
        if (null != dynModel)
        {
            try
            {
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonColor()))
                {
                    btn_login.setBackgroundColor(Color.parseColor(dynModel.getButtonColor()));
                }
                
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonFontColor()))
                {
                    btn_login.setTextColor(Color.parseColor(dynModel.getButtonFontColor()));
                }
            }
            catch (Exception e)
            {
            }
        }
    }
    
    public static void startActivity(Context context)
    {
        Intent it = new Intent();
        it.setClass(context, ChanagePwdActivity.class);
        context.startActivity(it);
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setTitle(R.string.title_change_pwd);
        titleBar.setLeftButtonVisible(true);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
            }
            
            @Override
            public void onRightLayClick()
            {
                // TODO Auto-generated method stub
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                // TODO Auto-generated method stub
                
            }
        });
    }
    
    public void onChangePwd(View v)
    {
        if (TextUtils.isEmpty(etOldPwd.getText()))
        {
            
            etOldPwd.setFocusable(true);
            toastDialog(ChanagePwdActivity.this, R.string.show_current_psw, null);
            return;
        }
        if (TextUtils.isEmpty(etNewPwd.getText()))
        {
            etNewPwd.setFocusable(true);
            toastDialog(ChanagePwdActivity.this, R.string.show_new_psw, null);
            return;
        }
        if (TextUtils.isEmpty(etNewPwd2.getText()))
        {
            etNewPwd2.setFocusable(true);
            toastDialog(ChanagePwdActivity.this, R.string.show_confirm_pass, null);
            return;
        }


        final String oldPwd = etOldPwd.getText().toString();
        final String newPwd = etNewPwd.getText().toString();
        final String newPwd2 = etNewPwd2.getText().toString();
        
        if (etNewPwd.getText().toString().length() < 8)
        {
            etNewPwd.setFocusable(true);
            toastDialog(ChanagePwdActivity.this, R.string.psw_instruction, null);
            return;
        }

        //校验新密码是否符合8-16位字符
        if(!isContainAll(newPwd)){
            etNewPwd.setFocusable(true);
            toastDialog(ChanagePwdActivity.this, R.string.et_new_pass, null);
            return;
        }

        if (!newPwd.equals(newPwd2))
        {
            etNewPwd2.setFocusable(true);
            toastDialog(ChanagePwdActivity.this, R.string.psw_not_match_instruction, null);
            return;
        }
        
        dialogInfo =
            new DialogInfo(ChanagePwdActivity.this, getString(R.string.public_cozy_prompt),
                getString(R.string.tv_modify_pass), getString(R.string.bt_confirm), getString(R.string.btnCancel),
                DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn()
                {
                    
                    @Override
                    public void handleOkBtn()
                    {
                        LocalAccountManager.getInstance().changePwd(oldPwd,
                            MainApplication.phone,
                            newPwd,
                            newPwd2,
                            new UINotifyListener<Boolean>()
                            {
                                @Override
                                public void onError(final Object object)
                                {
                                    super.onError(object);
                                    if (object != null)
                                    {
                                        runOnUiThread(new Runnable()
                                        {
                                            @Override
                                            public void run()
                                            {
                                                toastDialog(ChanagePwdActivity.this, object.toString(), null);
                                            }
                                        });
                                        
                                    }
                                }
                                
                                @Override
                                public void onPreExecute()
                                {
                                    super.onPreExecute();
                                    showLoading(false, R.string.wait_a_moment);
                                }
                                
                                @Override
                                public void onPostExecute()
                                {
                                    super.onPostExecute();
                                    dismissLoading();
                                }
                                
                                @Override
                                public void onSucceed(Boolean result)
                                {
                                    super.onSucceed(result);
                                    if (result)
                                    {
                                        runOnUiThread(new Runnable()
                                        {
                                            @Override
                                            public void run()
                                            {
                                                //                                                toastDialog(ChanagePwdActivity.this,
                                                //                                                    R.string.show_pass_succ,
                                                //                                                    new NewDialogInfo.HandleBtn()
                                                //                                                    {
                                                //                                                        
                                                //                                                        @Override
                                                //                                                        public void handleOkBtn()
                                                //                                                        {
                                                //                                                            finish();
                                                //                                                        }
                                                //                                                    });
                                                showToastInfo(R.string.show_pass_succ);
                                                PreferenceUtil.removeKey("login_skey");
                                                PreferenceUtil.removeKey("login_sauthid");
                                                showPage(WelcomeActivity.class);
                                                for (Activity a : MainApplication.allActivities)
                                                {
                                                    a.finish();
                                                }
                                                finish();
                                            }
                                        });
                                    }
                                }
                            });
                    }
                    
                    @Override
                    public void handleCancleBtn()
                    {
                        dialogInfo.cancel();
                    }
                }, null);
        
        DialogHelper.resize(ChanagePwdActivity.this, dialogInfo);
        dialogInfo.show();
        
    }

    //用来校验密码，是否包含数字和字母
     public boolean isContainAll(String str){
                boolean isDigit = false;
                /*boolean isLowerCase = false;
                boolean isUpperCase = false;*/
                boolean isLetters = false;

                for(int i = 0 ; i < str.length(); i++){
                    if(Character.isDigit(str.charAt(i))){
                        isDigit = true;
                    }else if(Character.isLowerCase(str.charAt(i))){
//                        isLowerCase = true;
                        isLetters = true;
                    }else if(Character.isUpperCase(str.charAt(i))){
//                        isUpperCase = true;
                        isLetters = true;
                    }
                }
                String regex = "^[a-zA-Z0-9]+$";
//                boolean isCorrect = isDigit&&isLowerCase&&isUpperCase&&str.matches(regex);
                boolean isCorrect = isDigit&&isLetters&&str.matches(regex);
                return isCorrect;
     }



    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.iv_clearOld:
                etOldPwd.setText("");
                break;
            case R.id.iv_clearNewpwd:
                etNewPwd.setText("");
                break;
            case R.id.iv_clearNewpwd2:
                etNewPwd2.setText("");
                break;
        }
    }
}
