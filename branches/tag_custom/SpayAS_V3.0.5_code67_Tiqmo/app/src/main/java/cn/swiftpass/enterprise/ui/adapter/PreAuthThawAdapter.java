package cn.swiftpass.enterprise.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.PreAuthDetailsBean;
import cn.swiftpass.enterprise.bussiness.model.PreAuthThawBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.OrderDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.PayResultActivity;
import cn.swiftpass.enterprise.ui.activity.PreAuthDetailsActivity;
import cn.swiftpass.enterprise.ui.widget.BaseRecycleAdapter;
import cn.swiftpass.enterprise.utils.DateUtil;

public class PreAuthThawAdapter extends BaseRecycleAdapter<Order> {

    private List<Order> mList=new ArrayList<>();
    private Context mContext;
    private int itemType;
    public PreAuthThawAdapter(Context context,List<Order> datas,int itemType) {
        super(datas);
        this.mList = datas;
        mContext=context;
        this.itemType=itemType;
    }

    @Override
    protected void bindData(final BaseViewHolder holder, final int position) {
        if (mList!=null&&mList.size()>0&&mList.get(position)!=null){
                TextView tv_title = (TextView) holder.getView(R.id.tv_title);
                TextView tv_content = (TextView) holder.getView(R.id.tv_content);
                TextView tv_tips = (TextView) holder.getView(R.id.tv_tips);
                TextView tv_ispay=(TextView)holder.getView(R.id.tv_ispay);
                View view_underline=holder.getView(R.id.view_underline);
              final   RelativeLayout rl_per=(RelativeLayout)holder.getView(R.id.rl_per);
              switch (itemType){
                  case 1:
                      //2解冻金额{已解冻、解冻失败}  //1已支付金额{未支付、收款成功}
                      holder.itemView.setOnClickListener(new View.OnClickListener() {
                          @Override
                          public void onClick(View v) {//根据xx值传，下级共有两个页面
                   //   PreAuthDetailsActivity.startActivity(mContext, mList.get(position), 1,1);//跳预授权转支付详情
                                if (listener!=null){
                                    listener.onItemClick(v,position,mList.get(position));
                                }

                          }

                      });

                      if (!TextUtils.isEmpty(mList.get(position).getOrderNo())&&mList.get(position).getOrderNo().length()>4){
                          String frontStr=mList.get(position).getOrderNo();
                          String behindStr=mList.get(position).getOrderNo();
                          tv_title.setText(frontStr.substring(0,4) +"****"+behindStr.substring(behindStr.length()-4,behindStr.length()));
                      }

                      tv_tips.setText(DateUtil.getMoneyWithFeefh(DateUtil.formatMoneyUtils(mList.get(position).getMoney())));

                      if (!TextUtils.isEmpty(mList.get(position).getOperateTime())) {
                          tv_content.setText(mList.get(position).getOperateTime());
                      }

                      if (position==(mList.size()-1)){
                          view_underline.setVisibility(View.INVISIBLE);
                      }
                      if (mList.get(position).getOperationStatus()!=null&&mList.get(position).getOperationStatus()==1){
                          tv_ispay.setText(mContext.getString(R.string.tx_bill_stream_chioce_succ));
                          tv_ispay.setTextColor(Color.parseColor("#23BF3F"));
                      }else {
                          tv_ispay.setText(mContext.getString(R.string.stream_receivable));
                          tv_ispay.setTextColor(Color.parseColor("#999999"));
                      }
                      break;
                  case  2:
                      holder.itemView.setOnClickListener(new View.OnClickListener() {
                          @Override
                          public void onClick(View v) {//根据xx值传，下级共有两个页面
                              if (listener!=null){
                                  listener.onItemClick(v,position,mList.get(position));
                              }
                            //  PreAuthDetailsActivity.startActivity(mContext, mList.get(position), 2,2);//跳预授权转支付详情
                          }

                      });

                      if (!TextUtils.isEmpty(mList.get(position).getOutRequestNo())&&mList.get(position).getOutRequestNo().length()>4){
                          String frontStr=mList.get(position).getOutRequestNo();
                          String behindStr=mList.get(position).getOutRequestNo();
                          tv_title.setText(frontStr.substring(0,4) +"****"+behindStr.substring(behindStr.length()-4,behindStr.length()));
                      }

                      tv_tips.setText(DateUtil.getMoneyWithFeefh(DateUtil.formatMoneyUtils(mList.get(position).getMoney())));

                      if (!TextUtils.isEmpty(mList.get(position).getOperateTime())) {
                          tv_content.setText(mList.get(position).getOperateTime());
                      }

                      if (position==(mList.size()-1)){
                          view_underline.setVisibility(View.INVISIBLE);
                      }
                      if (mList.get(position).getOperationStatus()!=null&&mList.get(position).getOperationStatus()==1){
                          tv_ispay.setText(mContext.getString(R.string.freezen_success));
                          tv_ispay.setTextColor(Color.parseColor("#F99426"));
                      }else {
                          tv_ispay.setText(mContext.getString(R.string.unfreezen_failed                                                                                     ));
                          tv_ispay.setTextColor(Color.parseColor("#999999"));
                      }
                      break;
                      default:
                          break;
              }

        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_preauththaw;
    }


    public  interface OnItemClickListener {
        void onItemClick(View view,int position,Order order);
    }
    private OnItemClickListener listener;
    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener=listener;
    }

}
