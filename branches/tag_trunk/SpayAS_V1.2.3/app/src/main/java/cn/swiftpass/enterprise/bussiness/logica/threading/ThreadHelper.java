package cn.swiftpass.enterprise.bussiness.logica.threading;

import android.os.AsyncTask;
import android.util.Log;

import cn.swiftpass.enterprise.bussiness.logica.threading.Executable.ProgressChangedListener;
import cn.swiftpass.enterprise.utils.Logger;

public class ThreadHelper {
    private static final String TAG = ThreadHelper.class.getSimpleName();

    public static void executeWithCallback(final Executable executable, NotifyListener listener) {
        if (executable == null) {
            return;
        }

        // 默认在UI线程回调
        if (listener == null) {
            executeWithCallbackUI(executable, listener);
        } else if (!(listener instanceof UINotifyListener)) {
            executeWithCallbackInThread(executable, listener);
        } else {
            executeWithCallbackUI(executable, listener);
        }
    }

    private static void executeWithCallbackInThread(final Executable execuable, final NotifyListener listener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                execuable.setOnProgressChangedListener(new ProgressChangedListener() {
                    @Override
                    public void onProgressChanged(int progress) {
                        if (listener != null) {
                            listener.onProgress(progress);
                        }
                    }
                });

                if (listener != null) {
                    listener.onPreExecute();
                }

                Object object = null;
                try {
                    object = execuable.execute();
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                    //object = e;
                }

                if (listener != null) {
                    if (object != null && object instanceof Exception) {
                        listener.onError("发送错误啦" + ((Exception) object).getMessage());
                    } else {
                        listener.onSucceed(object);
                    }
                }
            }
        }).start();
    }

    private static void executeWithCallbackUI(final Executable executable, final NotifyListener listener) {
        new AsyncTask<Void, Integer, Object>() {
            protected void onPreExecute() {
                if (listener != null) {
                    listener.onPreExecute();
                }
            }
            ;
            protected void onPostExecute(Object result) {
                if (listener != null) {
                    if (result != null && result instanceof Exception) {
                        Logger.i("ThreadHelper:" + ((Exception) result).getMessage());
                        //                        listener.onError("对不起，遇见错误了");//+((Exception) result).getMessage());
                    } else if (result != null) {
                        listener.onSucceed(result);
                    }
                }
            }
            ;
            protected void onProgressUpdate(Integer... values) {
                if (listener != null) {
                    listener.onProgress(values[0]);
                }
            }
            ;

            @Override
            protected Object doInBackground(Void... params) {
                try {
                    executable.setOnProgressChangedListener(new ProgressChangedListener() {
                        @Override
                        public void onProgressChanged(int progress) {
                            publishProgress(progress);
                        }
                    });
                    return executable.execute();
                } catch (Exception e) {
                    //                    listener.onError("未知错误，请稍微再试!!");
                    return e;
                }
            }
        }.execute();
    }
}
