package cn.swiftpass.enterprise.mvp.contract.dialog

import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/12/7
 *
 * @没有谁会喜欢孤独
 * @只是害怕失望
 */
class RefundCheckContract {

    interface View : BaseView {

        fun loginAsyncSuccess(response: Boolean)

        fun loginAsyncFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {

        fun loginAsync(
            code: String?,
            username: String?,
            password: String?
        )

    }


}