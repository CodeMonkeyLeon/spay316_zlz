/**
 * @description: 下载线程
 * @version 1.0
 * copyrights reserved by Petfone 2007-2011
 */
package cn.swiftpass.enterprise.bussiness.logica.download;

import android.accounts.NetworkErrorException;
import android.content.Context;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.util.EntityUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

import cn.swiftpass.enterprise.bussiness.logica.download.error.FileAlreadyExistException;
import cn.swiftpass.enterprise.bussiness.logica.download.error.NoMemoryException;
import cn.swiftpass.enterprise.bussiness.logica.download.inteferace.DownloadTaskListener;
import cn.swiftpass.enterprise.utils.FileUtils;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.NetworkUtils;
import cn.swiftpass.enterprise.utils.StorageUtils;

public class DownloadTask extends AsyncTask<Void, Integer, Long> {
    private static final String TAG = DownloadTask.class.getSimpleName();
    // ===========================================================
    // Constants
    // ===========================================================

    public final static int TIME_OUT = 30000;

    private final static int BUFFER_SIZE = 1024 * 8;

    //private static final String TEMP_SUFFIX = ".cache";

    // ===========================================================
    // Fields
    // ===========================================================

    private File file;

    private File tempFile;

    private String url;

    private long downloadId;

    private RandomAccessFile outputStream;

    private DownloadTaskListener listener;

    private Context context;

    /**
     * 下载错误信息
     */
    private Throwable error = null;

    /**
     * 本次下载的byte
     */
    private long downloadSize;

    /**
     * 开始下载时已经下载的大小
     */
    private long previousFileSize;

    /**
     * 总下载大小
     */
    private long totalSize;

    /**
     * 下载速度，byte/ms
     */
    private long networkSpeed;

    /**
     * 开始下载的时间
     */
    private long previousTime;

    /**
     * 下载持续时间
     */
    private long durationTime;

    /**文件后缀*/
    //private String suffix;
    /**
     * 暂停，可能仍会下载一会，判断是否继续下载的标志
     */
    private boolean isInterrupt = false;

    /**
     * 真正停止
     */
    private boolean isDestroyed = false;

    // ===========================================================
    // Constructors
    // ===========================================================
    public DownloadTask(Context context, String url, String path, long id, String fileName) {
        this(context, url, path, id, fileName, null);
    }

    public DownloadTask(Context context, String url, String path, long id, String fileName, DownloadTaskListener listener) {
        super();
        this.url = url;
        this.listener = listener;
        //this.suffix = fileName;
        //String fileName =suffix;
        this.file = new File(path, fileName);
        //        this.tempFile = new File(path, fileName + TEMP_SUFFIX);

        this.tempFile = new File(path, fileName);
        this.context = context;
        this.downloadId = id;

    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    protected void onPreExecute() {
        //Logger.d("onPreExecute " + file.getName());
        previousTime = System.currentTimeMillis();
        if (listener != null) listener.preDownload(this);
    }

    @Override
    protected Long doInBackground(Void... params) {
        //Logger.d("doInBackground " + file.getName());
        long result = -1;
        try {
            result = download();
        } catch (NetworkErrorException e) {
            error = e;
            Log.e(TAG, Log.getStackTraceString(e));
        } catch (FileAlreadyExistException e) {
            //error = e;
            Log.e(TAG, Log.getStackTraceString(e));
            result = 0;
           // Logger.d("error: " + e.getMessage());
        } catch (NoMemoryException e) {
            error = e;
            Log.e(TAG, Log.getStackTraceString(e));
        } catch (IOException e) {
            error = e;
            Log.e(TAG, Log.getStackTraceString(e));
        } finally {
            if (client != null) {
                client.close();
            }
        }

        return result;
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        //Logger.d("onProgressUpdate " + progress + "   ---> " + file.getName());
    }

    @Override
    protected void onPostExecute(Long result) {
        //Logger.d("onPostExecute " + file.getName() + "  ---> result=" + result + "  isInterrupt=" + isInterrupt + "  error=" + error);

        if (result == -1 || isInterrupt || error != null) {
            // 下载失败或暂停
            if (listener != null && error != null) {
                listener.downloadError(this, error);
            }

        } else {

            // 下载成功
            boolean isSuccess = tempFile.renameTo(file);
            //TODO FIX ZHANGXINCHAO 主要是解决扫描漏洞 不处理返回值的问题
            if (isSuccess) {

            } else {

            }
            if (listener != null) {
                listener.downloadSuccess(this);

            }
        }

        // 下载线程停止
        if (listener != null) {
            listener.postDownload(this);
        }
        isDestroyed = true;
    }

    @Override
    public void onCancelled() {
        super.onCancelled();
    }

    public void cancel() {
        isInterrupt = true;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================
    public String getUrl() {
        return url;
    }

    public boolean isInterrupt() {
        return isInterrupt;
    }

    public boolean isDestroyed() {
        return isDestroyed;
    }

    public long getDownloadPercent() {
        return (downloadSize + previousFileSize) * 100 / (totalSize + 1);
    }

    public long getDownloadSize() {
        return downloadSize + previousFileSize;
    }

    public long getTotalSize() {
        return totalSize;
    }

    public long getDownloadSpeed() {
        return this.networkSpeed;
    }

    public long getDurationTime() {
        return this.durationTime;
    }

    public DownloadTaskListener getListener() {
        return this.listener;
    }

    public long getDownloadId() {
        return this.downloadId;
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private final class ProgressReportingRandomAccessFile extends RandomAccessFile {
        private long byteWrited = 0;

        public ProgressReportingRandomAccessFile(File file, String mode) throws FileNotFoundException {
            super(file, mode);

        }

        @Override
        public void write(byte[] buffer, int offset, int count) throws IOException {
            super.write(buffer, offset, count);
            byteWrited += count;

            fileSizeChanged(byteWrited);
        }
    }

    private void fileSizeChanged(long byteWrited) {
        durationTime = System.currentTimeMillis() - previousTime;
        downloadSize = byteWrited;
        networkSpeed = downloadSize / durationTime;

        if (listener != null) listener.updateProcess(this);
    }

    // ===========================================================
    // download相关
    // ===========================================================
    private AndroidHttpClient client;

    private HttpGet httpGet;

    private HttpResponse response;

    private long download() throws NetworkErrorException, IOException, FileAlreadyExistException, NoMemoryException {
        
        /*
         * check net work
         */
        if (!NetworkUtils.isNetworkAvailable(context)) {
            throw new NetworkErrorException("Network blocked.");
        }
        
        /*
         * check file length
         */
        if (isInterrupt) {
            return -1;
        }
        client = AndroidHttpClient.newInstance("DownloadTask");
        //StringBuilder sbParam = new StringBuilder();
        /*String param;
        if (!url.contains("?")) {
        	try {
        		int idx = url.lastIndexOf("=");
        		String id = url.substring(idx, url.length());
        		JSONObject data = new JSONObject();
        		data.put("id", downloadId);
        		param = URLEncoder.encode(data.toString(), "utf-8");
        		sbParam.append("?data=" + param);
        	} catch (Exception e) {
        		// TODO Auto-generated catch block
        		Log.e(TAG,Log.getStackTraceString(e));
        	}
        }*/
        //sbParam.append("jime.apk");
        httpGet = new HttpGet(url);
        // 添加headers
        //httpGet.setHeader("User-Info",MainApplication.getContext().jime.conf().getUserInfo().toString());
        //httpGet.setHeader("Authorization", "5sGphT6l:ZDtv2K@fnYOGBhU;s");
        //List<Cookie> cookies = Jime.conf().getCookieStore().getCookies();
        //Logger.i("DownloadTask", cookies.size() + "");

        StringBuilder sb = new StringBuilder();
        /*for (Cookie c : cookies) {
            sb.append(c.getName()).append("=").append(c.getValue());
        	sb.append(";");
        }*/
        if (sb.length() > 0) {
            sb.setLength(sb.length() - 1);
        }
        String cookieStr = sb.toString();

        httpGet.addHeader("Cookie", cookieStr);
        response = client.execute(httpGet);
        int resCode = response.getStatusLine().getStatusCode();
        if (resCode < 200 || resCode >= 300) {
            BufferedHttpEntity httpEntity = new BufferedHttpEntity(response.getEntity());
            String message = EntityUtils.toString(httpEntity, "UTF-8");
            Logger.i("DownloadTask", "message:" + message);
            throw new NetworkErrorException("Get the total downloadSize failed resCode：" + resCode);
        }
        //        response.getEntity().getContent();
        totalSize = response.getEntity().getContentLength();
        //Logger.d("totalSize = " + totalSize + "(" + FileUtils.getSizeStr(totalSize) + ")");

        if (totalSize <= 0) {
            throw new NetworkErrorException("Get the total downloadSize failed resCode：" + resCode);
        }

        if (file.exists() && totalSize == file.length()) {
            boolean isSuccess = file.delete();
            Log.i(TAG,"delete file "+isSuccess);
            throw new FileAlreadyExistException("Output file already exists. Skipping download.");
        } else if (tempFile.exists()) {
            if (tempFile.length() >= totalSize) {
                boolean isSuccess = tempFile.delete();
                Log.i(TAG,"delete file "+isSuccess);
                previousFileSize = 0;
            } else {
                previousFileSize = tempFile.length();
            }
            //如果文件存在并且有数据，进行跳过直接到后面没下载的进行下载
            httpGet.addHeader("Range", "bytes=" + previousFileSize + "-" + totalSize);
            //Logger.d("previousFileSize = " + previousFileSize + "(" + FileUtils.getSizeStr(previousFileSize) + ")");

            client.close();
            client = AndroidHttpClient.newInstance("DownloadTask");
            response = client.execute(httpGet);
        }
        
        /*
         * check memory
         */
        if (isInterrupt) {
            return -1;
        }
        long storage = StorageUtils.getAvailableStorage();
        if (totalSize - tempFile.length() > storage) {
            throw new NoMemoryException("SD card no memory.");
        }
        
        /*
         * start download
         */
        outputStream = new ProgressReportingRandomAccessFile(tempFile, "rw");

        InputStream input = response.getEntity().getContent();
        int bytesCopied = copy(input, outputStream);

        if ((previousFileSize + bytesCopied) != totalSize && !isInterrupt) {
            throw new IOException("Download uncomplete: " + (previousFileSize + bytesCopied) + " != " + totalSize);
        }

        return bytesCopied;

    }

    public int copy(InputStream input, RandomAccessFile out) throws IOException, NetworkErrorException {

        if (input == null || out == null) {
            return -1;
        }

        byte[] buffer = new byte[BUFFER_SIZE];

        BufferedInputStream in = new BufferedInputStream(input, BUFFER_SIZE);

        int byteTotalReaded = 0, byteReaded = 0;

        try {
            out.seek(previousFileSize);
            //Logger.d("RandomAccessFile seekTo = " + previousFileSize);

            while (!isInterrupt) {
                byteReaded = in.read(buffer, 0, BUFFER_SIZE);
                if (byteReaded == -1) {
                    break;
                }
                out.write(buffer, 0, byteReaded);
                byteTotalReaded += byteReaded;
                
                /*
                 * check network
                 */
                if (!NetworkUtils.isNetworkAvailable(context)) {
                    throw new NetworkErrorException("Network blocked.");
                }

                if (networkSpeed == 0 && System.currentTimeMillis() - previousTime > TIME_OUT) {
                    throw new ConnectTimeoutException("connection time out.");
                }

            }
        } finally {
            // must close client first
            client.close();
            client = null;
            FileUtils.closeIO(out, in, input);
        }
        return byteTotalReaded;
    }

}
