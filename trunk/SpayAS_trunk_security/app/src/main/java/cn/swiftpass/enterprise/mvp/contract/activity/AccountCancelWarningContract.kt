package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.MerchantTempDataModel
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView


/**
 * @author lizheng.zhao
 * @date 2022/11/23
 *
 * @天空吸收着水分
 * @越来越蓝
 * @蓝得醉人
 * @那是画家调不出来的颜色。
 */
class AccountCancelWarningContract {

    interface View : BaseView {

        fun queryMerchantDataByTelSuccess(response: MerchantTempDataModel)

        fun queryMerchantDataByTelFailed(error: Any?)

        fun sendEmailForAccountCancelSuccess(response: Boolean)

        fun sendEmailForAccountCancelFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {

        fun sendEmailForAccountCancel(email: String?)

        fun queryMerchantDataByTel()

    }


}