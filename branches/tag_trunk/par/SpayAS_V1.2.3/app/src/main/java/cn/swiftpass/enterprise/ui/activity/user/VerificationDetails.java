package cn.swiftpass.enterprise.ui.activity.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.WxCard;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.DescriptionActivity;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;

public class VerificationDetails extends TemplateActivity implements OnClickListener
{
    private TextView verification_body_info, verification_pay_mch, verification_tvOrderCode;
    
    private TextView verification_wx_tvOrderCode, verification_pay_method, verification_order, verification_user;
    
    private TextView verification_tv_notifyTime, verification_tv_state, verification_detail_info, verification_client;
    
    private WxCard orderMode;
    
    private Button verification_btn_refund;
    
    private boolean isFromStream = true;
    
    private LinearLayout cashierLay, order_lay;
    
    private TextView verification_tv_desc, verification_detail_type, card_name;
    
    private RelativeLayout lay_desc;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_details);
        
        getIntentValue();
        initView();
        if (isFromStream)
        {
            verification_btn_refund.setVisibility(View.INVISIBLE);
        }
        else
        {
            verification_btn_refund.setVisibility(View.VISIBLE);
        }
        initListener();
        initData();
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initListener()
    {
        verification_btn_refund.setOnClickListener(this);
        lay_desc.setOnClickListener(this);
        
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initData()
    {
        if (orderMode != null)
        {
            if (isAbsoluteNullStr(orderMode.getOutTradeNo()))
            {
                order_lay.setVisibility(View.GONE);
            }
            else
            {
                verification_order.setText(orderMode.getOutTradeNo());
            }
            if (null != orderMode.getDealDetail() && !orderMode.getDealDetail().equals("null"))
            {
                verification_tv_desc.setText(orderMode.getDealDetail());
            }
            verification_body_info.setText(orderMode.getTitle());
            verification_pay_mch.setText(MainApplication.getMchName());
            verification_tvOrderCode.setText(MainApplication.getMchId());
            verification_wx_tvOrderCode.setText(orderMode.getCardCode());
            //            verification_pay_method.setText(text);
            verification_detail_type.setText(Order.WxCardType.getCardNameForType(orderMode.getCardType()));
            if (orderMode.getCardType().equalsIgnoreCase(Order.WxCardType.CARSH_CARD.cardType)) // 代金券
            {
                card_name.setText(R.string.tx_reduce_cost);
                verification_detail_info.setText(MainApplication.getFeeFh()
                    + DateUtil.formatMoneyUtils(orderMode.getReduceCost()));
            }
            else if (orderMode.getCardType().equalsIgnoreCase(Order.WxCardType.DISCOUNT_CARD.cardType))
            {
                card_name.setText(R.string.et_discount);
                verification_detail_info.setText(orderMode.getDiscount() + "%");
            }
            else if (orderMode.getCardType().equalsIgnoreCase(Order.WxCardType.GIFT_CARD.cardType))
            {
                card_name.setText(R.string.tx_gift);
                if (null != orderMode.getGift() && !orderMode.getGift().equals("null"))
                {
                    verification_detail_info.setText(orderMode.getGift());
                }
            }
            else if (orderMode.getCardType().equalsIgnoreCase(Order.WxCardType.GROUPON_CARD.cardType))
            {
                card_name.setText(R.string.tx_group_card);
                if (null != orderMode.getDealDetail() && !orderMode.getDealDetail().equals("null"))
                {
                    verification_tv_desc.setText(orderMode.getDealDetail());
                }
            }
            
            if (null != orderMode.getDealDetail() && !orderMode.getDealDetail().equals("null"))
            {
                verification_detail_info.setText(orderMode.getDealDetail());
            }
            verification_tv_notifyTime.setText(orderMode.getUseTimeNew());
            verification_tv_state.setText(R.string.tx_affirm_succ);
            
            //            if (!isAbsoluteNullStr(orderMode.getClient()))
            //            {
            //                verification_client.setText(ClientEnum.getClienName(orderMode.getClient()));
            //            }
            //            else
            //            {
            //                verification_client.setText(ClientEnum.UNKNOW.name());
            //            }
            
            if (MainApplication.isAdmin.equals("0"))
            {
                cashierLay.setVisibility(View.VISIBLE);
                verification_user.setText(MainApplication.realName + "(" + MainApplication.userId + ")");
            }
            
            else
            {
                if (null != orderMode.getUserName() && !"".equals(orderMode.getUserName())
                    && !"null".equals(orderMode.getUserName()))
                {
                    cashierLay.setVisibility(View.VISIBLE);
                    if (orderMode.getuId() != 0)
                    {
                        
                        verification_user.setText(orderMode.getUserName() + "(" + orderMode.getuId() + ")");
                    }
                    else
                    {
                        verification_user.setText(orderMode.getUserName() + "(" + orderMode.getuId() + ")");
                    }
                }
                else
                {
                    cashierLay.setVisibility(View.GONE);
                }
            }
        }
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void getIntentValue()
    {
        Intent intent = getIntent();
        orderMode = (WxCard)intent.getSerializableExtra("order");
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView()
    {
        lay_desc = getViewById(R.id.lay_desc);
        card_name = getViewById(R.id.card_name);
        verification_detail_type = getViewById(R.id.verification_detail_type);
        order_lay = getViewById(R.id.order_lay);
        cashierLay = getViewById(R.id.cashierLay);
        verification_user = getViewById(R.id.verification_user);
        verification_order = getViewById(R.id.verification_order);
        verification_client = getViewById(R.id.verification_client);
        verification_detail_info = getViewById(R.id.verification_detail_info);
        verification_body_info = getViewById(R.id.verification_body_info);
        verification_pay_mch = getViewById(R.id.verification_pay_mch);
        verification_tvOrderCode = getViewById(R.id.verification_tvOrderCode);
        verification_wx_tvOrderCode = getViewById(R.id.verification_wx_tvOrderCode);
        verification_pay_method = getViewById(R.id.verification_pay_method);
        verification_tv_notifyTime = getViewById(R.id.verification_tv_notifyTime);
        verification_tv_state = getViewById(R.id.verification_tv_state);
        verification_btn_refund = getViewById(R.id.verification_btn_refund);
        
        verification_tv_desc = getViewById(R.id.verification_tv_desc);
    }
    
    public static void startActivity(Context context, WxCard orderModel)
    {
        Intent it = new Intent();
        it.setClass(context, VerificationDetails.class);
        it.putExtra("order", orderModel);
        context.startActivity(it);
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tx_affirm_detail);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
            }
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                
            }
        });
    }
    
    /** {@inheritDoc} */
    
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.verification_btn_refund:
                this.finish();
                break;
            case R.id.lay_desc:
                if (orderMode.getDealDetail() != null && orderMode.getDealDetail().length() > 15)
                {
                    DescriptionActivity.startActivity(VerificationDetails.this, orderMode);
                }
                
                break;
            default:
                break;
        }
    }
    
}
