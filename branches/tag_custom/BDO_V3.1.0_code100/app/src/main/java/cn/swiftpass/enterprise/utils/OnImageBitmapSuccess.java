package cn.swiftpass.enterprise.utils;

import android.net.Uri;

public interface OnImageBitmapSuccess {
    void onImageBitmapCallBack(Uri uri);
}
