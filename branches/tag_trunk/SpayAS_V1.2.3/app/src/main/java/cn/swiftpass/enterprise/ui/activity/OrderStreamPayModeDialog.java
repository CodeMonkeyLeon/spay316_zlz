package cn.swiftpass.enterprise.ui.activity;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import cn.swiftpass.enterprise.intl.R;

/**
 * 对话框
 * User: W.L
 * Date: 14-04-03
 * Time: 上午10:12
 * To change this template use File | Settings | File Templates.
 */
public class OrderStreamPayModeDialog extends Dialog implements View.OnClickListener
{
    
    private Context mContext;
    
    private SelectListener mListener;
    
    public void setmListener(SelectListener mListener)
    {
        this.mListener = mListener;
    }
    
    public OrderStreamPayModeDialog(Context context, int theme)
    {
        super(context, theme);
        mContext = context;
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_shared);
        findViewById(R.id.ll_dialog_pay).setOnClickListener(this);
        findViewById(R.id.ll_dialog_pos).setOnClickListener(this);
        findViewById(R.id.ll_dialog_all).setOnClickListener(this);
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        dismiss();
        return true;
    }
    
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.ll_dialog_pay:
                mListener.okSelect(v);
                break;
            case R.id.ll_dialog_pos:
                mListener.okSelect(v);
                break;
            case R.id.ll_dialog_all:
                mListener.okSelect(v);
                break;
        }
    }
    
    public interface SelectListener
    {
        public void okSelect(View v);
    }
}
