package cn.swiftpass.enterprise.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.enterprise.R;


public class Toolbar extends LinearLayout {

    private TextView tv_left_title, tv_center_title, tv_right_title;
    private boolean isLeftListener;
    private boolean isRightListener;
    private Context mContext;

    public Toolbar(Context context) {
        super(context);
        mContext = context;
    }

    public Toolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView();
    }

    public Toolbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView();
    }

    private void initView() {
        LayoutInflater.from(mContext).inflate(R.layout.toolbar, this);
        tv_left_title = (TextView) findViewById(R.id.tv_left_title);
        tv_center_title = (TextView) findViewById(R.id.tv_center_title);
        tv_right_title = (TextView) findViewById(R.id.tv_right_title);

        tv_left_title.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener == null) {
                    return;
                }
                mClickListener.onLeftListener();
            }
        });

        tv_right_title.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener == null) {
                    return;
                }
                mClickListener.onRightListener();
            }
        });
    }

    public boolean isTitleLeftListener(boolean isOpen) {
        isLeftListener = isOpen;
        return isLeftListener;
    }

    public boolean isTitleRightListener(boolean isOpen) {
        isRightListener = isOpen;
        return isRightListener;
    }

    public void setLeftTitleVisibility(int visibility) {
        tv_left_title.setVisibility(visibility);
    }

    public void setCenterTitleVisibility(int visibility) {
        tv_center_title.setVisibility(visibility);
    }

    public void setRightTitleVisibility(int visibility) {
        tv_right_title.setVisibility(visibility);
    }

    public interface OnToolBarClickListener {
        void onLeftListener();

        void onRightListener();
    }

    private OnToolBarClickListener mClickListener;

    public void setOnToolBarClickListener(OnToolBarClickListener listener) {
        mClickListener = listener;
    }

    public void setLeftTitle(String title, int id) {
        if (title != null) {
            tv_left_title.setText(title);
            tv_left_title.setVisibility(VISIBLE);
        }
        if (id != 0) {
            Drawable drawable = getResources().getDrawable(id);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
            tv_left_title.setCompoundDrawables(drawable, null, null, null);
        }
    }

    public void setCenterTitle(String title, int id) {
        if (title != null) {
            tv_center_title.setText(title);
            tv_center_title.setVisibility(VISIBLE);
        }
        if (id != 0) {
            Drawable drawable = getResources().getDrawable(id);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
            tv_center_title.setCompoundDrawables(null, null, null, drawable);
        }
    }

    public void setRightTitle(String title, int id) {
        if (title != null) {
            tv_right_title.setText(title);
            tv_center_title.setVisibility(VISIBLE);
        }
        if (id != 0) {
            Drawable drawable = getResources().getDrawable(id);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
            tv_right_title.setCompoundDrawables(null, null, drawable, null);
        }
    }

}
