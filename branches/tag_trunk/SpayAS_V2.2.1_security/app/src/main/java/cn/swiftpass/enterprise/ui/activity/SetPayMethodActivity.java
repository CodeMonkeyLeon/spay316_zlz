/*
 * 文 件 名:  SetPayMethodActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-8-5
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity;


/**
 * 设置默认支付方式
 * 
 * @author  he_hui
 * @version  [版本号, 2016-8-5]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class SetPayMethodActivity extends TemplateActivity
{
    //    private RelativeLayout lay_wx, lay_qq, lay_zfb, lay_jd, lay_def;
    //    
    //    private CheckBox radioButton1, radioButton2, radioButton3, radioButton4, radioButton5;
    //    
    //    private View v_wx, v_qq, v_zfb, v_jd;
    //    
    //    Map<String, CheckBox> checkBoxMap = new HashMap<String, CheckBox>();
    //    
    //    @Override
    //    protected void onCreate(Bundle savedInstanceState)
    //    {
    //        super.onCreate(savedInstanceState);
    //        
    ////        setContentView(R.layout.set_pay_method_view);
    //        
    //        initView();
    //        
    //        setLister();
    //    }
    //    
    //    private void setChckBox(Map<String, CheckBox> checkBoxMap)
    //    {
    //        for (Map.Entry<String, CheckBox> entry : checkBoxMap.entrySet())
    //        {
    //            CheckBox box = entry.getValue();
    //            if (box != null)
    //            {
    //                box.setChecked(false);
    //            }
    //        }
    //        checkBoxMap.clear();
    //    }
    //    
    //    private void setLister()
    //    {
    //        
    //        lay_def.setOnClickListener(new View.OnClickListener()
    //        {
    //            
    //            @Override
    //            public void onClick(View v)
    //            {
    //                if (!radioButton5.isChecked())
    //                {
    //                    if (checkBoxMap.size() > 0)
    //                    {
    //                        setChckBox(checkBoxMap);
    //                        checkBoxMap.put(MainApplication.PAY_WX_NATIVE1, radioButton5);
    //                    }
    //                    else
    //                    {
    //                        checkBoxMap.put(MainApplication.PAY_WX_NATIVE1, radioButton5);
    //                    }
    //                    radioButton5.setChecked(true);
    //                }
    //            }
    //        });
    //        
    //        lay_jd.setOnClickListener(new View.OnClickListener()
    //        {
    //            
    //            @Override
    //            public void onClick(View v)
    //            {
    //                if (!radioButton4.isChecked())
    //                {
    //                    if (checkBoxMap.size() > 0)
    //                    {
    //                        setChckBox(checkBoxMap);
    //                        checkBoxMap.put(MainApplication.PAY_JINGDONG_NATIVE, radioButton4);
    //                    }
    //                    else
    //                    {
    //                        checkBoxMap.put(MainApplication.PAY_JINGDONG_NATIVE, radioButton4);
    //                    }
    //                    
    //                    radioButton4.setChecked(true);
    //                }
    //            }
    //        });
    //        
    //        lay_zfb.setOnClickListener(new View.OnClickListener()
    //        {
    //            
    //            @Override
    //            public void onClick(View v)
    //            {
    //                if (!radioButton3.isChecked())
    //                {
    //                    if (checkBoxMap.size() > 0)
    //                    {
    //                        setChckBox(checkBoxMap);
    //                        checkBoxMap.put(MainApplication.PAY_ZFB_NATIVE1, radioButton3);
    //                    }
    //                    else
    //                    {
    //                        checkBoxMap.put(MainApplication.PAY_ZFB_NATIVE1, radioButton3);
    //                    }
    //                    
    //                    radioButton3.setChecked(true);
    //                }
    //            }
    //        });
    //        
    //        lay_qq.setOnClickListener(new View.OnClickListener()
    //        {
    //            
    //            @Override
    //            public void onClick(View v)
    //            {
    //                if (!radioButton2.isChecked())
    //                {
    //                    if (checkBoxMap.size() > 0)
    //                    {
    //                        setChckBox(checkBoxMap);
    //                        checkBoxMap.put(MainApplication.PAY_QQ_NATIVE1, radioButton2);
    //                    }
    //                    else
    //                    {
    //                        checkBoxMap.put(MainApplication.PAY_QQ_NATIVE1, radioButton2);
    //                    }
    //                    
    //                    radioButton2.setChecked(true);
    //                }
    //            }
    //        });
    //        
    //        lay_wx.setOnClickListener(new View.OnClickListener()
    //        {
    //            
    //            @Override
    //            public void onClick(View v)
    //            {
    //                if (!radioButton1.isChecked())
    //                {
    //                    if (checkBoxMap.size() > 0)
    //                    {
    //                        setChckBox(checkBoxMap);
    //                        checkBoxMap.put(MainApplication.PAY_WX_NATIVE, radioButton1);
    //                    }
    //                    else
    //                    {
    //                        checkBoxMap.put(MainApplication.PAY_WX_NATIVE, radioButton1);
    //                    }
    //                    
    //                    radioButton1.setChecked(true);
    //                }
    //                
    //            }
    //        });
    //    }
    //    
    //    private void initView()
    //    {
    //        lay_wx = getViewById(R.id.lay_wx);
    //        lay_qq = getViewById(R.id.lay_qq);
    //        lay_zfb = getViewById(R.id.lay_zfb);
    //        lay_jd = getViewById(R.id.lay_jd);
    //        lay_def = getViewById(R.id.lay_def);
    //        
    //        radioButton1 = getViewById(R.id.radioButton1);
    //        radioButton2 = getViewById(R.id.radioButton2);
    //        radioButton3 = getViewById(R.id.radioButton3);
    //        radioButton4 = getViewById(R.id.radioButton4);
    //        radioButton5 = getViewById(R.id.radioButton5);
    //        
    //        v_wx = getViewById(R.id.v_wx);
    //        v_qq = getViewById(R.id.v_qq);
    //        v_zfb = getViewById(R.id.v_zfb);
    //        v_jd = getViewById(R.id.v_jd);
    //        checkBoxMap.put(MainApplication.PAY_WX_NATIVE1, radioButton5);
    //        if (!MainApplication.serviceType.contains(MainApplication.PAY_WX_NATIVE))
    //        {
    //            lay_wx.setVisibility(View.GONE);
    //            v_wx.setVisibility(View.GONE);
    //        }
    //        if (!(MainApplication.serviceType.contains(MainApplication.PAY_ZFB_NATIVE) || MainApplication.serviceType.contains(MainApplication.PAY_ZFB_NATIVE1)))
    //        {
    //            lay_zfb.setVisibility(View.GONE);
    //            v_zfb.setVisibility(View.GONE);
    //        }
    //        if (!(MainApplication.serviceType.contains(MainApplication.PAY_QQ_NATIVE) || MainApplication.serviceType.contains(MainApplication.PAY_QQ_NATIVE1)))
    //        {
    //            
    //            lay_qq.setVisibility(View.GONE);
    //            v_qq.setVisibility(View.GONE);
    //        }
    //        if (!MainApplication.serviceType.contains(MainApplication.PAY_JINGDONG_NATIVE))
    //        {
    //            lay_jd.setVisibility(View.GONE);
    //            v_jd.setVisibility(View.GONE);
    //        }
    //        
    //        String setPayMethod = PreferenceUtil.getString(MainApplication.userId + "", "");
    //        if (!StringUtil.isEmptyOrNull(setPayMethod))
    //        {
    //            if (setPayMethod.contains(MainApplication.PAY_QQ_TAG))
    //            {
    //                radioButton2.setChecked(true);
    //                
    //                if (checkBoxMap.size() > 0)
    //                {
    //                    setChckBox(checkBoxMap);
    //                }
    //                checkBoxMap.put(MainApplication.PAY_QQ_NATIVE1, radioButton2);
    //            }
    //            else if (setPayMethod.contains(MainApplication.PAY_JD_TAG))
    //            {
    //                radioButton4.setChecked(true);
    //                
    //                if (checkBoxMap.size() > 0)
    //                {
    //                    setChckBox(checkBoxMap);
    //                }
    //                checkBoxMap.put(MainApplication.PAY_JINGDONG_NATIVE, radioButton4);
    //            }
    //            else if (setPayMethod.contains(MainApplication.PAY_ALIPAY_TAG))
    //            {
    //                radioButton3.setChecked(true);
    //                
    //                if (checkBoxMap.size() > 0)
    //                {
    //                    setChckBox(checkBoxMap);
    //                }
    //                checkBoxMap.put(MainApplication.PAY_ZFB_NATIVE1, radioButton3);
    //            }
    //            else if (setPayMethod.equals(MainApplication.PAY_WX_NATIVE))
    //            {
    //                radioButton1.setChecked(true);
    //                
    //                if (checkBoxMap.size() > 0)
    //                {
    //                    setChckBox(checkBoxMap);
    //                }
    //                checkBoxMap.put(MainApplication.PAY_WX_NATIVE, radioButton1);
    //            }
    //            else if (setPayMethod.equals(MainApplication.PAY_WX_NATIVE1))
    //            {
    //                if (checkBoxMap.size() > 0)
    //                {
    //                    setChckBox(checkBoxMap);
    //                }
    //                radioButton5.setChecked(true);
    //                checkBoxMap.put(MainApplication.PAY_WX_NATIVE1, radioButton5);
    //            }
    //        }
    //        
    //    }
    //    
    //    @Override
    //    protected void setupTitleBar()
    //    {
    //        super.setupTitleBar();
    //        titleBar.setLeftButtonVisible(true);
    //        titleBar.setLeftText(R.string.btnCancel);
    //        titleBar.setTitle(R.string.tv_set_pay_method_prompt);
    //        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.save));
    //        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
    //        {
    //            
    //            @Override
    //            public void onRightLayClick()
    //            {
    //                
    //            }
    //            
    //            @Override
    //            public void onRightButtonClick()
    //            {
    //                
    //            }
    //            
    //            @Override
    //            public void onRightButLayClick()
    //            {
    //                if (checkBoxMap.size() > 0)
    //                {
    //                    for (Map.Entry<String, CheckBox> entry : checkBoxMap.entrySet())
    //                    {
    //                        if (entry.getKey().equals(MainApplication.PAY_WX_NATIVE1))
    //                        {
    //                            
    //                            PreferenceUtil.commitString(MainApplication.userId + "", MainApplication.DEF_PAY_METHOD);
    //                            HandlerManager.notifyMessage(HandlerManager.PAY_SET_PAY_METHOD,
    //                                HandlerManager.PAY_SET_PAY_METHOD,
    //                                MainApplication.DEF_PAY_METHOD);
    //                            
    //                        }
    //                        else
    //                        {
    //                            
    //                            PreferenceUtil.commitString(MainApplication.userId + "", entry.getKey());
    //                            HandlerManager.notifyMessage(HandlerManager.PAY_SET_PAY_METHOD,
    //                                HandlerManager.PAY_SET_PAY_METHOD,
    //                                entry.getKey());
    //                        }
    //                    }
    //                }
    //                showToastInfo(R.string.tv_save_pay_method_prompt);
    //                finish();
    //            }
    //            
    //            @Override
    //            public void onLeftButtonClick()
    //            {
    //                finish();
    //            }
    //        });
    //    }
}
