package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.UserModelList
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView


/**
 * @author lizheng.zhao
 * @date 2022/11/22
 *
 * @阳光在天上一闪
 * @又被乌云埋掩
 * @暴雨冲洗着
 * @我灵魂的底片
 */
class UserListContract {


    interface View : BaseView {

        fun queryCashierSuccess(
            response: UserModelList,
            input: String?,
            isLoadCashierData: Boolean,
            what: Int
        )

        fun queryCashierFailed(
            error: Any?,
            isLoadCashierData: Boolean
        )

    }


    interface Presenter : BasePresenter<View> {

        fun queryCashier(
            page: Int,
            pageSize: Int,
            input: String?,
            isLoadCashierData: Boolean,
            isLoadMore: Boolean,
            what: Int
        )


    }


}