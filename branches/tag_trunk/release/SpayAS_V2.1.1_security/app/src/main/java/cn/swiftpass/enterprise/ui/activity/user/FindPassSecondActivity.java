/*
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-14
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.ForgetPSWBean;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 忘记密码
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-14]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class FindPassSecondActivity extends TemplateActivity
{
    
    private TimeCount time;
    
    private EditText ed_code;
    
    private ImageView  iv_clearPwd;
    
    private Button btn_next_step;

    private TextView textView1, tv_title_info;

    private ForgetPSWBean forgetPSWBean;

    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    public static void startActivity(Context context, ForgetPSWBean PSWBean)
    {
        Intent it = new Intent();
        it.setClass(context, FindPassSecondActivity.class);
        it.putExtra("ForgetPSWBean", PSWBean);
        context.startActivity(it);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_pass_second);
        
        MainApplication.listActivities.add(this);
        initView();
        
        setLister();
        btn_next_step.getBackground().setAlpha(102);
        forgetPSWBean = (ForgetPSWBean) getIntent().getSerializableExtra("ForgetPSWBean");
        if (null != forgetPSWBean)
        {
            //进入页面开始倒数
            showToastInfo(R.string.tx_code_send_succ);
            Countdown();

            tv_title_info.setVisibility(View.VISIBLE);
            if (!StringUtil.isEmptyOrNull(forgetPSWBean.getEe()))
            {

                tv_title_info.setText(getString(R.string.tv_find_pass_title_info2) + "\n"+forgetPSWBean.getEe());
            }
        }
    }

    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b, enable, res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_press_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }

    /**
     *验证码倒计时60秒
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void Countdown()
    {
        // 总共60 没间隔1秒
        time = new TimeCount(60000, 1000);
        time.start();
    }
    
    class TimeCount extends CountDownTimer
    {
        public TimeCount(long millisInFuture, long countDownInterval)
        {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }
        
        @Override
        public void onFinish()
        {//计时完毕时触发
            textView1.setEnabled(true);
            textView1.setClickable(true);
            textView1.setText(R.string.bt_code_get);
            textView1.setTextColor(getResources().getColor(R.color.tab_text_select));
        }
        
        @Override
        public void onTick(long millisUntilFinished)
        {//计时过程显示
            textView1.setEnabled(false);
            textView1.setClickable(false);
            textView1.setTextColor(getResources().getColor(R.color.user_edit_color));
            textView1.setText(millisUntilFinished / 1000 + "s");
        }
    }
    
    private void initView()
    {
        iv_clearPwd = getViewById(R.id.iv_clearPwd);
        tv_title_info = getViewById(R.id.tv_title_info);
        ed_code = getViewById(R.id.ed_code);
        textView1 = getViewById(R.id.textView1);
        btn_next_step = getViewById(R.id.btn_next_step);

        ed_code.setFocusable(true);
        ed_code.setFocusableInTouchMode(true);
        ed_code.requestFocus();

        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        {
            
            @Override
            public void onExecute(CharSequence s, int start, int before, int count)
            {
            }
            
            @Override
            public void onAfterTextChanged(Editable s)
            {

                if (ed_code.isFocused())
                {
                    if (ed_code.getText().toString().length() > 0)
                    {
                        iv_clearPwd.setVisibility(View.VISIBLE);

                    }
                    else
                    {
                        iv_clearPwd.setVisibility(View.GONE);
                    }
                }
                
                if (null != forgetPSWBean)
                {
                    if (!StringUtil.isEmptyOrNull(ed_code.getText().toString()))
                    {
                        
                        setButtonBg(btn_next_step, true, R.string.reg_next_step);
                    }
                    else
                    {
                        
                        setButtonBg(btn_next_step, false, R.string.reg_next_step);
                    }

                }
                else
                {
                    
                    if (!StringUtil.isEmptyOrNull(ed_code.getText().toString()))
                    {
                        setButtonBg(btn_next_step, true, R.string.reg_next_step);
                    }
                    else
                    {
                        setButtonBg(btn_next_step, false, R.string.reg_next_step);
                    }
                }
            }
        });
        ed_code.addTextChangedListener(editTextWatcher);
        ed_code.setOnFocusChangeListener(listener);
        showSoftInputFromWindow(this,ed_code);
    }
    
    private final OnFocusChangeListener listener = new OnFocusChangeListener()
    {
        @Override
        public void onFocusChange(View v, boolean hasFocus)
        {
            switch (v.getId())
            {
                case R.id.ed_code:
                    if (hasFocus)
                    {
                        if (ed_code.getText().toString().length() > 0)
                        {
                            iv_clearPwd.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            iv_clearPwd.setVisibility(View.GONE);
                        }
                    }
                    else
                    {
                        iv_clearPwd.setVisibility(View.GONE);
                    }
                    break;
            }
            
        }
    };
    
    void loadGetCode()
    {
        UserManager.getCode(forgetPSWBean.getInput(), new UINotifyListener<ForgetPSWBean>()
        {
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dismissLoading();
                if (object != null)
                {
                    FindPassSecondActivity.this.runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            toastDialog(FindPassSecondActivity.this, object.toString(), null);
                        }
                    });
                }
            }
            
            @Override
            public void onPreExecute()
            {
                super.onPostExecute();
                loadDialog(FindPassSecondActivity.this, R.string.dialog_message);
            }
            
            @Override
            public void onSucceed(ForgetPSWBean result)
            {
                // TODO Auto-generated method stub
                super.onSucceed(result);
                dismissLoading();
                if (null != result)
                {
                    forgetPSWBean.setEe(result.getEe());
                    forgetPSWBean.setToken(result.getToken());
                    showToastInfo(R.string.tx_code_send_succ);
                    Countdown();
                    
                }
            }
        });
        
    }
    
    private void setLister()
    {
        
        iv_clearPwd.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                ed_code.setText("");
                iv_clearPwd.setVisibility(View.GONE);
            }
        });
        
        textView1.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                
                loadGetCode();
            }
        });
        
        btn_next_step.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (StringUtil.isEmptyOrNull(ed_code.getText().toString()))
                {
                    toastDialog(FindPassSecondActivity.this, R.string.tx_ver_code, null);
                    ed_code.setFocusable(true);
                    return;
                }
                checkPhoneCode();
                
            }
        });
        
    }
    /**
     * 关闭软键盘
     *
     */
    public static void closeKeybord(EditText mEditText, Context mContext) {
        InputMethodManager imm = (InputMethodManager) mContext
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEditText.getWindowToken(), 0);
    }

    void checkPhoneCode()
    {
        if (forgetPSWBean == null)
        {
            return;
        }
        closeKeybord(ed_code,FindPassSecondActivity.this);

        if (TextUtils.isEmpty(forgetPSWBean.getEe())){
            FindPassSecondActivity.this.runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    toastDialog(FindPassSecondActivity.this,R.string.tx_email_tx,null);                }
            });
            return;
        }
        UserManager.checkPhoneCode(forgetPSWBean.getToken(), ed_code.getText().toString().trim(), new UINotifyListener<Boolean>()
        {
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dismissLoading();
                if (object != null)
                {
                    FindPassSecondActivity.this.runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            toastDialog(FindPassSecondActivity.this, object.toString(), null);
                        }
                    });
                }
            }
            
            @Override
            public void onPreExecute()
            {
                super.onPostExecute();
                loadDialog(FindPassSecondActivity.this, R.string.reg_next_step_loading);
            }
            
            @Override
            public void onSucceed(Boolean result)
            {
                // TODO Auto-generated method stub
                super.onSucceed(result);
                dismissLoading();
                if (result)
                {
                    forgetPSWBean.setEmailCode(ed_code.getText().toString().trim());
                    FindPassSubmitActivity.startActivity(FindPassSecondActivity.this, forgetPSWBean);
                }
            }
        });
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tv_find_pass_title_info);
    }
    
}
