package cn.swiftpass.enterprise.bussiness.model;

import cn.swiftpass.enterprise.io.database.table.AdsTable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-10-27
 * Time: 下午8:04
 * To change this template use File | Settings | File Templates.
 */
@DatabaseTable(tableName = AdsTable.TABLE_NAME)
public class AdsInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField( columnName = AdsTable.IMG_NAME)
    public String name;

}
