/*
 * 文 件 名:  ReportActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-8
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.bill;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.bill.BillOrderManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.activity.OrderDetailsActivity;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 退款
 * 
 * @author  he_hui
 * @version  [版本号, 2016-11-8]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class OrderSearchActivity extends BaseActivity
{

    private static final String TAG = OrderSearchActivity.class.getSimpleName();
    private ViewHolder holder;
    
    //private Order orderModel;
    
    private EditText refund_search_input;
    
    private ImageView refund_search_clear_btn;
    
    private Button but_cancel;
    
    private ListView search_result_list;
    
    private TextView tv_null_info;
    
    private List<Order> orders = new ArrayList<Order>();
    
    private BillStreamAdapter adapter;
    
    private LinearLayout refund_back_btn;
    
    private Integer reqFeqTime = 0;
    
    private boolean loadNext = false;
    
    public static void startActivity(Context context, Order orderModel)
    {
        Intent it = new Intent();
        it.setClass(context, OrderSearchActivity.class);
        it.putExtra("order", orderModel);
        context.startActivity(it);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_serach);
        MainApplication.listActivities.add(this);
        initview();
        
        setLister();
        adapter = new BillStreamAdapter(orders);
        search_result_list.setAdapter(adapter);
        
    }
    
    private void setLister()
    {
        search_result_list.setOnItemClickListener(new OnItemClickListener()
        {
            
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
            {
                Order order = orders.get(position);
                if (null != order)
                {
                    OrderManager.getInstance().queryOrderDetail(order.getOutTradeNo(),
                        MainApplication.getMchId(),
                        true,
                        new UINotifyListener<Order>()
                        {
                            
                            @Override
                            public void onError(Object object)
                            {
                                super.onError(object);
                                dismissLoading();
                                if (object != null)
                                {
                                    toastDialog(OrderSearchActivity.this, object.toString(), null);
                                }
                            }
                            
                            @Override
                            public void onPreExecute()
                            {
                                super.onPreExecute();
                                
                                loadDialog(OrderSearchActivity.this, getStringById(R.string.public_data_loading));
                                
                            }
                            
                            @Override
                            public void onSucceed(Order result)
                            {
                                
                                super.onSucceed(result);
                                dismissLoading();
                                if (result != null)
                                {
                                    OrderDetailsActivity.startActivity(OrderSearchActivity.this, result);
                                }
                            }
                            
                        });
                }
            }
        });
        
        but_cancel.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (!StringUtil.isEmptyOrNull(refund_search_input.getText().toString()))
                {
                    Logger.i("hehui", "loadNext-->" + loadNext);
                    if (loadNext)
                    {
                        toastDialog(OrderSearchActivity.this, R.string.tx_request_more, null);
                    }
                    else
                    {
                        loadDate(1, true, refund_search_input.getText().toString());
                    }
                    
                }
                else
                {
                    toastDialog(OrderSearchActivity.this, R.string.tv_serach_but_no_prompt, null);
                }
            }
        });
        
        refund_back_btn.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                OrderSearchActivity.this.finish();
            }
        });
        
        refund_search_clear_btn.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                refund_search_input.setText("");
                search_result_list.setVisibility(View.GONE);
                tv_null_info.setVisibility(View.VISIBLE);
            }
        });
        
        refund_search_input.setFocusable(true);
        refund_search_input.setFocusableInTouchMode(true);
        refund_search_input.requestFocus();
        refund_search_input.setOnEditorActionListener(new OnEditorActionListener()
        {
            
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if ((actionId == 0 || actionId == 3) && event != null)
                {
                    if (event.getAction() == MotionEvent.ACTION_DOWN)
                    {
                        if (!StringUtil.isEmptyOrNull(refund_search_input.getText().toString()))
                        {
                            
                            if (loadNext)
                            {
                                toastDialog(OrderSearchActivity.this, R.string.tx_request_more, null);
                            }
                            else
                            {
                                loadDate(1, true, refund_search_input.getText().toString());
                            }
                            
                            return true;
                        }
                    }
                }
                return false;
            }
        });
    }
    
    void loadDate(int page, final boolean isLoadMore, String order)
    {
        BillOrderManager.getInstance().querySpayOrder(0,
            null,
            null,
            0,
            page,
            order,
            null,
            null,
            new UINotifyListener<List<Order>>()
            {
                @Override
                public void onPreExecute()
                {
                    super.onPreExecute();
                    if (isLoadMore)
                    {
                        loadDialog(OrderSearchActivity.this, R.string.public_data_loading);
                    }
                }
                
                @Override
                public void onPostExecute()
                {
                    super.onPostExecute();
                }
                
                @Override
                public void onError(Object object)
                {
                    super.onError(object);
                    dissDialog();
                    if (checkSession())
                    {
                        return;
                    }
                    if (null != object)
                    {
                        toastDialog(OrderSearchActivity.this, object.toString(), null);
                    }
                }
                
                @Override
                public void onSucceed(List<Order> model)
                {
                    dissDialog();
                    if (null != model && model.size() > 0)
                    {
                        search_result_list.setVisibility(View.VISIBLE);
                        tv_null_info.setVisibility(View.GONE);
                        if (orders.size() > 0)
                        {
                            orders.clear();
                            orders.addAll(model);
                            adapter.notifyDataSetChanged();
                        }
                        else
                        {
                            orders.addAll(model);
                            adapter.notifyDataSetChanged();
                        }
                        reqFeqTime = model.get(0).getReqFeqTime();
                        //暂停
                        if (reqFeqTime > 0)
                        {
                            loadNext = true;
                            sleep(reqFeqTime);
                        }
                    }
                    else
                    {
                        search_result_list.setVisibility(View.GONE);
                        tv_null_info.setVisibility(View.VISIBLE);
                        
                    }
                }
            });
    }
    
    /**
     * 暂停 后 才去请求
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    void sleep(long time)
    {
        try
        {
            TimerTask task = new TimerTask()
            {
                @Override
                public void run()
                {
                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            Logger.i("hehui", "sleep-->");
                            loadNext = false;
                        }
                    });
                }
            };
            Timer timer = new Timer();
            timer.schedule(task, time * 1000);
            
        }
        catch (Exception e)
        {
            return;
        }
    }
    
    private void initview()
    {
        refund_back_btn = getViewById(R.id.refund_back_btn);
        tv_null_info = getViewById(R.id.tv_null_info);
        search_result_list = getViewById(R.id.search_result_list);
        but_cancel = getViewById(R.id.but_cancel);
        refund_search_input = getViewById(R.id.refund_search_input);
        //        refund_search_input.setFocusable(true);
        //        refund_search_input.setFocusableInTouchMode(true);
        //        refund_search_input.requestFocus();
        //        InputMethodManager inputManager =
        //            (InputMethodManager)refund_search_input.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        //        
        //        //        inputManager.showSoftInput(refund_search_input, 0);
        //        inputManager.toggleSoftInput(0, InputMethodManager.SHOW_FORCED);
        
        showSoftInputFromWindow(OrderSearchActivity.this, refund_search_input);
        refund_search_clear_btn = getViewById(R.id.refund_search_clear_btn);
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        {
            
            @Override
            public void onExecute(CharSequence s, int start, int before, int count)
            {
            }
            
            @Override
            public void onAfterTextChanged(Editable s)
            {
                if (refund_search_input.isFocused())
                {
                    if (refund_search_input.getText().toString().length() > 0)
                    {
                        refund_search_clear_btn.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        refund_search_clear_btn.setVisibility(View.GONE);
                    }
                }
            }
        });
        refund_search_input.addTextChangedListener(editTextWatcher);
    }
    
    private class BillStreamAdapter extends BaseAdapter
    {
        
        private List<Order> orders;
        
        public BillStreamAdapter()
        {
        }
        
        public BillStreamAdapter(List<Order> orders)
        {
            this.orders = orders;
        }
        
        @Override
        public int getCount()
        {
            return orders.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            return orders.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(OrderSearchActivity.this, R.layout.activity_bill_stream_list_item, null);
                holder = new ViewHolder();
                holder.tv_pay_type = (TextView)convertView.findViewById(R.id.tv_pay_type);
                holder.tv_time = (TextView)convertView.findViewById(R.id.tv_time);
                holder.tv_money = (TextView)convertView.findViewById(R.id.tv_money);
                holder.tv_state = (TextView)convertView.findViewById(R.id.tv_state);
                holder.tv_total = (TextView)convertView.findViewById(R.id.tv_total);
                holder.tv_date = (TextView)convertView.findViewById(R.id.tv_date);
                holder.iv_type = (ImageView)convertView.findViewById(R.id.iv_type);
                holder.ly_title = (RelativeLayout)convertView.findViewById(R.id.ly_title);
                holder.tv_center = (TextView)convertView.findViewById(R.id.tv_center);
                holder.tv_right = (TextView)convertView.findViewById(R.id.tv_right);
                holder.v_iv = (View)convertView.findViewById(R.id.v_iv);

                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            Order order = orders.get(position);
            if (null != order)
            {
                if (position == (orders.size() - 1))
                {
                    holder.v_iv.setVisibility(View.GONE);
                }
                else
                {
                    holder.v_iv.setVisibility(View.VISIBLE);
                }
                
                if (!StringUtil.isEmptyOrNull(order.getTradeTimeNew()))
                {
                    try
                    {
                        holder.tv_time.setText(order.getTradeTimeNew());
                        
                    }
                    catch (Exception e)
                    {
                        Log.e(TAG,Log.getStackTraceString(e));
                    }
                }
                String orderNo = ""; //截取后6位
                if (!StringUtil.isEmptyOrNull(order.getOrderNoMch()))
                {
                    orderNo =
                        order.getOrderNoMch().substring(order.getOrderNoMch().length() - 6,
                            order.getOrderNoMch().length());
                }
                if (!StringUtil.isEmptyOrNull(orderNo))
                {
                    String seachContent = refund_search_input.getText().toString();
                    ForegroundColorSpan nameColor =
                        new ForegroundColorSpan(getResources().getColor(R.color.title_bg_new));
                    SpannableStringBuilder builder = new SpannableStringBuilder(orderNo);
                    if (!TextUtils.isEmpty(seachContent) && orderNo.contains(seachContent))
                    {
                        builder.setSpan(nameColor, orderNo.indexOf(seachContent), orderNo.indexOf(seachContent)
                            + seachContent.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                    holder.tv_center.setVisibility(View.VISIBLE);
                    holder.tv_right.setVisibility(View.VISIBLE);
                    holder.tv_pay_type.setText(MainApplication.getPayTypeMap()
                        .get(String.valueOf(order.getApiProvider()))  + " (");
                    holder.tv_center.setText(builder);
                    
                }
                else
                {
                    
                    holder.tv_pay_type.setText(order.getProviderName());
                }
                
                if (order.getMoney() > 0)
                {
                    holder.tv_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(order.getMoney()));
                }
                holder.tv_state.setText(order.getTradeStateText());
                
                Object object =
                    SharedPreUtile.readProduct("payTypeIcon" + ApiConstant.bankCode + MainApplication.getMchId());
                if (object != null)
                {
                    try
                    {
                        
                        Map<String, String> typePicMap = (Map<String, String>)object;
                        if (typePicMap != null && typePicMap.size() > 0)
                        {
                            String picUrl = typePicMap.get(order.getApiProvider() + "");
                            if (!StringUtil.isEmptyOrNull(picUrl))
                            {
                                Bitmap bitmap =
                                    BitmapFactory.decodeResource(getResources(), R.drawable.icon_general_wechat);
                                if (bitmap != null)
                                {
                                    
                                    MainApplication.finalBitmap.display(holder.iv_type, picUrl, bitmap);
                                }
                                else
                                {
                                    MainApplication.finalBitmap.display(holder.iv_type, picUrl);
                                }
                            }
                            else
                            {
                                holder.iv_type.setImageResource(R.drawable.icon_general_receivables);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Log.e(TAG,Log.getStackTraceString(e));
                    }
                }
                else
                {
                    switch (order.getApiProvider())
                    {
                        case 1: //微信
                            holder.iv_type.setImageResource(R.drawable.icon_general_wechat);
                            break;
                        case 2: //支付宝
                            holder.iv_type.setImageResource(R.drawable.icon_general_pay);
                            break;
                        case 12: //京东
                            holder.iv_type.setImageResource(R.drawable.icon_general_jingdong);
                            break;
                        case 4: //qq
                            holder.iv_type.setImageResource(R.drawable.icon_general_qq);
                            break;
                        default:
                            holder.iv_type.setImageResource(R.drawable.icon_general_receivables);
                            break;
                    }
                }
                
                switch (order.getTradeState())
                {
                    case 2:
                        //支付成功
                        holder.tv_state.setTextColor(getResources().getColor(R.color.bill_item_succ));
                        holder.tv_money.setTextColor(Color.parseColor("#000000"));
                        break;
                    case 1: //未支付
                        //                        holder.tv_state.setTextColor(getResources().getColor(R.color.bill_item_fail));
                        holder.tv_state.setTextColor(getResources().getColor(R.color.bg_setting_right_text));
                        holder.tv_money.setTextColor(getResources().getColor(R.color.bg_setting_right_text));
                        break;
                    case 3: //关闭
                        //                        holder.tv_state.setTextColor(getResources().getColor(R.color.bill_item_fail));
                        holder.tv_state.setTextColor(getResources().getColor(R.color.bg_setting_right_text));
                        holder.tv_money.setTextColor(getResources().getColor(R.color.bg_setting_right_text));
                        break;
                    case 4: //转入退款
                        //                        holder.tv_state.setTextColor(getResources().getColor(R.color.bill_item_refund));
                        holder.tv_state.setTextColor(getResources().getColor(R.color.pay_fail));
                        holder.tv_money.setTextColor(getResources().getColor(R.color.black));
                        break;
                    case 8:  //已撤销
                        //                        holder.tv_state.setTextColor(getResources().getColor(R.color.bill_item_fail));
                        holder.tv_state.setTextColor(getResources().getColor(R.color.bg_setting_right_text));
                        holder.tv_money.setTextColor(getResources().getColor(R.color.bg_setting_right_text));
                        break;
                }
                
            }
            return convertView;
        }
    }
    
    private class ViewHolder
    {
        private TextView tv_total, tv_time, tv_pay_type, tv_money, tv_state, tv_date;
        
        private ImageView iv_type;
        
        private TextView tv_center, tv_right;
        
        private RelativeLayout ly_title;
        
        private View v_iv;
    }
    
}
