package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.CertificateBean
import cn.swiftpass.enterprise.bussiness.model.DynModels
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo
import cn.swiftpass.enterprise.bussiness.model.ErrorMsg
import cn.swiftpass.enterprise.bussiness.model.UserInfo
import cn.swiftpass.enterprise.bussiness.model.WalletList
import cn.swiftpass.enterprise.common.Constant
import cn.swiftpass.enterprise.intl.BuildConfig
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.io.okhttp.OkhttpUtil
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.WelcomeContract
import cn.swiftpass.enterprise.mvp.utils.ParamsUtils
import cn.swiftpass.enterprise.utils.AESHelper
import cn.swiftpass.enterprise.utils.JsonUtil
import cn.swiftpass.enterprise.utils.MD5
import cn.swiftpass.enterprise.utils.SharedPreUtils
import cn.swiftpass.enterprise.utils.SignUtil
import cn.swiftpass.enterprise.utils.StringUtil
import cn.swiftpass.enterprise.utils.ToastHelper
import cn.swiftpass.enterprise.utils.Utils
import com.example.common.entity.EmptyData
import com.igexin.sdk.PushManager
import okhttp3.Call
import java.util.Locale

/**
 * @author lizheng.zhao
 * @date 2022/11/17
 *
 * @沉默
 * @像一朵傍晚的云
 */
class WelcomePresenter : WelcomeContract.Presenter {


    private var mView: WelcomeContract.View? = null


    override fun getVersionCode() {
//        mView?.let { view ->
//
//            AppClient.getVersionCode(
//                AppHelper.getUpdateVersionCode(MainApplication.getInstance()).toString() + "",
//                BuildConfig.bankCode,
//                System.currentTimeMillis().toString(),
//                object : CallBackUtil.CallBackCommonResponse<UpgradeInfo>(UpgradeInfo()) {
//
//                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
//                        commonResponse?.let {
//                            if (TextUtils.equals("400", it.result) || TextUtils.equals(
//                                    "402",
//                                    it.result
//                                )
//                            ) {
//                                view.getVersionCodeSuccess(null)
//                            } else {
//                                view.getVersionCodeFailed(it.result)
//                            }
//                        }
//                    }
//
//                    override fun onResponse(r: CommonResponse?) {
//                        r?.let { response ->
//                            if (TextUtils.equals(response.result, "200")) {
//                                try {
//                                    val upgradeInfoStr = response.message
//                                    val info = UpgradeInfo()
//                                    val json = JSONObject(upgradeInfoStr)
//                                    info.dateTime =
//                                        Utils.Long.tryParse(json.optString("updated_at"), 0)
//                                    info.message = json.optString("subDesc", "")
//                                    info.version =
//                                        Utils.Integer.tryParse(json.optString("verCode"), 0)
//                                    info.mustUpgrade = Utils.Integer.tryParse(
//                                        json.optString("isUpdate"),
//                                        0
//                                    ) == 0
//                                    info.versionName = json.optString("verName")
//                                    info.fileMd5 = json.optString("fileMd5")
//                                    MainApplication.getInstance().setFileMd5(info.fileMd5)
//                                    info.url = json.optString("filePath", "")
//                                    MainApplication.getInstance().setFileMd5(info.fileMd5)
//                                    view.getVersionCodeSuccess(info)
//                                } catch (e: Exception) {
//                                    SentryUtils.uploadNetInterfaceException(
//                                        e,
//                                        MainApplication.getInstance().baseUrl + "spay/upgrade/checkinVersion",
//                                        e.toString(),
//                                        ""
//                                    )
//                                }
//                            }
//                        }
//                    }
//                }
//            )
//
//        }
    }

    override fun apiShowList() {
        mView?.let { view ->

            val param: MutableMap<String, String> = HashMap()
            param[ParamsConstants.MCH_ID] = MainApplication.getInstance().getMchId()
            val spayRs = System.currentTimeMillis()
            //加签名
            if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getNewSignKey())) {
                param[ParamsConstants.SPAY_RS] = spayRs.toString()
                param[ParamsConstants.NNS] =
                    SignUtil.getInstance()
                        .createSign(param, MainApplication.getInstance().getNewSignKey())
            }


            OkhttpUtil.okHttpPost(
                MainApplication.getInstance().baseUrl + "spay/apiShowList",
                param,
                OkhttpUtil.getHeadParam(spayRs.toString()),
                object : CallBackUtil.CallBackCommonResponse<DynModels>(DynModels()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        commonResponse?.let {
                            view.apiShowListFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        r?.let { response ->
                            val dynModels = JsonUtil.jsonToBean(
                                response.message,
                                DynModels::class.java
                            ) as DynModels
                            if (null != dynModels.data && dynModels.data.size > 0) {
                                dynModels.data[0].md5 = dynModels.md5
                            }
                            view.apiShowListSuccess(dynModels.data)
                        }
                    }
                }
            )

        }
    }

    override fun deviceLogin() {
        mView?.let { view ->
            AppClient.deviceLogin(
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<UserInfo>(UserInfo()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        commonResponse?.let {
                            view.deviceLoginFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        r?.let { response ->
                            val u = JsonUtil.jsonToBean(
                                response.message,
                                UserInfo::class.java
                            ) as UserInfo
                            MainApplication.getInstance().userInfo = u
                            ParamsUtils.parseJson(MainApplication.getInstance().userInfo)
                            ParamsUtils.saveLoginCookieToSp(response.message)
                            view.deviceLoginSuccess(true)
                        }
                    }
                }
            )
        }
    }

    /**
     *  CA证书公钥获取接口
     * 请求CA证书公钥获取接口得到最新CA证书公钥
     * 获取证书，使用旧的签名方式（签名Key为时间戳spayRs）
     */
    override fun getCertificateString(isDeviceLogin: Boolean) {

        mView?.let { view ->
            if (!isDeviceLogin) {
                view.showLoading(R.string.show_login_loading, ParamsConstants.COMMON_LOADING)
            }

            //备注：因为获取公钥证书的接口和ECDH秘钥交换接口一样，不用传Skey,
            // 即这个spayDi不用传值，故此处用httpsPostECDHKeyExchange方法调用
            AppClient.getCertificateString(
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<CertificateBean>(CertificateBean()) {


                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { error ->
                            view.getCertificateStringFailed(error.message, isDeviceLogin)
                        }
                    }


                    override fun onResponse(res: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        res?.let { response ->
                            val dynModels = JsonUtil.jsonToBean(
                                response.message,
                                CertificateBean::class.java
                            ) as CertificateBean
                            view.getCertificateStringSuccess(dynModels, isDeviceLogin)
                        }
                    }
                }
            )
        }
    }

    /**
     * 聚合通道支持钱包列表
     * 查询聚合通道支持的钱包类型
     */
    override fun queryWalletList(serviceType: String?) {
        mView?.let { view ->
            AppClient.queryWalletList(
                serviceType,
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<WalletList>(WalletList()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        commonResponse?.let {
                            view.queryWalletListFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        r?.let { response ->
                            val dynModels = JsonUtil.jsonToBean(
                                response.message,
                                WalletList::class.java
                            ) as WalletList
                            MainApplication.getInstance().setWalletListBeans(dynModels.data)
                            view.queryWalletListSuccess(dynModels.data)
                        }
                    }
                }
            )
        }
    }

    /**
     *  0.登录成功
     *  1.商户待审核
     *  2.商户初审通过
     *  3.商户初审不通过
     *  5.商户终审不通过
     *  6.商户失效或过期
     *  7.商户不存在
     *  8.密码错误
     */
    override fun loginAsync(code: String?, username: String?, password: String?) {

        mView?.let { view ->
            view.showLoading(R.string.show_login_loading, ParamsConstants.COMMON_LOADING)
            val priKey = SharedPreUtils.readProduct(ParamsConstants.SECRET_KEY) as String?
            val paseStr = MD5.md5s(priKey).uppercase(Locale.getDefault())

            AppClient.loginAsync(
                username,
                AESHelper.aesEncrypt(password, paseStr.substring(8, 24)),
                Constant.CLIENT,
                MainApplication.getInstance().getSKey(),
                BuildConfig.bankCode,
                "1",
                code,
                PushManager.getInstance().getClientid(MainApplication.getInstance()),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<UserInfo>(UserInfo()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { error ->
                            val ret = Utils.Integer.tryParse(error.result, -1)
                            if (ret == 405) { //Require to renegotiate ECDH key
                                view.loginAsyncFailed("405" + error.message)
                            } else if (ret == 403) { //需要输入验证
                                val c = error.code
                                if (!StringUtil.isEmptyOrNull(code)) {
                                    val msg = ErrorMsg()
                                    msg.message = error.message
                                    msg.content = error.code
                                    view.loginAsyncFailed(msg)
                                } else {
                                    view.loginAsyncFailed("403$c")
                                }
                            } else if (ret in 500..599) {
                                view.loginAsyncFailed(ToastHelper.toStr(R.string.failed_to_connect_server))
                            } else if (TextUtils.isEmpty(error.message)) {
                                view.loginAsyncFailed(ToastHelper.toStr(R.string.failed_to_login_try_later))
                            } else {
                                view.loginAsyncFailed(error.message)
                            }
                        }
                    }

                    override fun onResponse(res: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        res?.let { response ->
                            val u = JsonUtil.jsonToBean(
                                response.message,
                                UserInfo::class.java
                            ) as UserInfo
                            MainApplication.getInstance().userInfo = u
                            ParamsUtils.parseJson(MainApplication.getInstance().userInfo)
                            ParamsUtils.saveLoginCookieToSp(response.message)
                            view.loginAsyncSuccess(true, password)
                        }
                    }
                }
            )
        }
    }

    override fun getCode() {
        AppClient.getCode(
            System.currentTimeMillis().toString(),
            object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {

                override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                    mView?.let { v ->
                        commonResponse?.let { error ->
                            v.getCodeFailed(error.message)
                        }
                    }
                }

                override fun onResponse(response: CommonResponse?) {
                    mView?.let { v ->
                        response?.let {
                            v.getCodeSuccess(it.message)
                        }
                    }
                }
            }
        )
    }

    /**
     * 客户端和服务器公钥交换的接口
     */
    override fun ecdhKeyExchange(
        publicKey: String?,
        isNeedRequestECDHKey: Boolean,
        isInIt: Boolean,
        privateKey: String?
    ) {
        mView?.let {
            if (isNeedRequestECDHKey) {
                it.showLoading(R.string.show_login_loading, ParamsConstants.COMMON_LOADING)
            }
        }

        AppClient.ecdhKeyExchange(
            publicKey,
            System.currentTimeMillis().toString(),
            object : CallBackUtil.CallBackCommonResponse<ECDHInfo>(ECDHInfo()) {

                override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                    mView?.let { v ->
                        v.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { error ->
                            v.ecdhKeyExchangeFailed(error.message, isInIt)
                        }
                    }
                }


                override fun onResponse(res: CommonResponse?) {
                    mView?.let { v ->
                        v.dismissLoading(ParamsConstants.COMMON_LOADING)
                        res?.let { response ->
                            val info = JsonUtil.jsonToBean(
                                response.message, ECDHInfo::class.java
                            ) as ECDHInfo
                            MainApplication.getInstance().setSerPubKey(info.serPubKey)
                            MainApplication.getInstance().setSKey(info.skey)
                            v.ecdhKeyExchangeSuccess(info, isInIt, privateKey)
                        }
                    }
                }
            }
        )
    }

    override fun attachView(view: WelcomeContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}