package cn.swiftpass.enterprise.print;

import android.content.Context;
import android.text.TextUtils;

import java.util.Locale;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;
import cn.swiftplus.enterprise.printsdk.print.PrintClient;

/**
 * Created by aijingya on 2020/8/24.
 *
 * @Package cn.swiftpass.enterprise.print
 * @Description:
 * @date 2020/8/24.19:24.
 */
public class PrintPreAuth {

    /* printType参数1：预授权成功---预授权小票(反扫)
	 * printType参数5：预授权成功---预授权小票(正扫)
	 * printType参数4：预授权订单详情---预授权小票
	 * printType参数2：预授权解冻详情小票
	 * printType参数3：预授权解冻完成小票
     */

    public static void printPreAuthStr(Context context, int printType, Order orderModel) {
        //打印开始前，先bindService
        PrintClient.getInstance().bindDeviceService();

        int minus=1;
        //根据当前的小数点的位数来判断应该除以或者乘以多少
        for(int i = 0; i < MainApplication.numFixed ; i++ ){
            minus = minus * 10;
        }

        switch (printType){
            case 1:
            case 4:
            case 5:
                PrintClient.getInstance().printTitle(context.getString(R.string.pre_auth_receipt));
                break;

            case 2:
            case 3:
                PrintClient.getInstance().printTitle(context.getString(R.string.pre_auth_unfreezed_receipt));
                break;
        }
        PrintClient.getInstance().printTextLeft(orderModel.getPartner());
        PrintClient.getInstance().printTextLeft(context.getString(R.string.tv_pay_client_save));
        PrintClient.getInstance().printDoubleLine();

        if (!TextUtils.isEmpty(MainApplication.getMchName())) {
            PrintClient.getInstance().printTextLeft(context.getString(R.string.shop_name) + "：");
            if (!StringUtil.isEmptyOrNull(MainApplication.getMchName())) {
                PrintClient.getInstance().printTextLeft(MainApplication.getMchName());
            }
        }
        if (!TextUtils.isEmpty(MainApplication.getMchId())) {
            PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "："+ MainApplication.getMchId());
        }

        if (printType==1) {
            PrintClient.getInstance().printTextLeft(context.getString(R.string.pre_auth_time)+ "：");
            PrintClient.getInstance().printTextLeft(orderModel.getTradeTime());
        }else if(printType==4){
            PrintClient.getInstance().printTextLeft(context.getString(R.string.pre_auth_time)+ "：");
            PrintClient.getInstance().printTextLeft(orderModel.getTradeTimeNew());
        }else if(printType==5){
            PrintClient.getInstance().printTextLeft(context.getString(R.string.pre_auth_time)+ "：");
            PrintClient.getInstance().printTextLeft(orderModel.getTimeEnd());
        } else if(printType==2){
            PrintClient.getInstance().printTextLeft(context.getString(R.string.unfreezed_time)+ "：");
            PrintClient.getInstance().printTextLeft(orderModel.getOperateTime());
        }else if(printType==3){
            PrintClient.getInstance().printTextLeft(context.getString(R.string.unfreezed_time)+ "：");
            PrintClient.getInstance().printTextLeft(orderModel.getUnFreezeTime());
        }

        if (printType==1 || printType == 4 || printType == 5){
            //平台预授权订单号
            PrintClient.getInstance().printTextLeft(context.getString(R.string.platform_pre_auth_order_id) + "：");
            PrintClient.getInstance().printTextLeft(orderModel.getAuthNo());

            String language = PreferenceUtil.getString("language","");
            Locale locale = MainApplication.getContext().getResources().getConfiguration().locale; //zh-rHK
            String lan = locale.getCountry();
            if (!TextUtils.isEmpty(language)) {
                if (language.equals(MainApplication.LANG_CODE_EN_US)) {
                    //支付宝单号
                    PrintClient.getInstance().printTextLeft(MainApplication.getPayTypeMap().get("2") + " "+ context.getString(R.string.tx_orderno) + "：");
                } else {
                    //支付宝单号
                    PrintClient.getInstance().printTextLeft(MainApplication.getPayTypeMap().get("2") + context.getString(R.string.tx_orderno) + "：");
                }
            } else {
                if (lan.equalsIgnoreCase("en")) {
                    //支付宝单号
                    PrintClient.getInstance().printTextLeft(MainApplication.getPayTypeMap().get("2") + " "+ context.getString(R.string.tx_orderno) + "：");
                } else {
                    //支付宝单号
                    PrintClient.getInstance().printTextLeft(MainApplication.getPayTypeMap().get("2") + context.getString(R.string.tx_orderno) + "：");
                }
            }

            if(printType==1 || printType == 5){
                PrintClient.getInstance().printTextLeft(orderModel.getOutTransactionId());
            }
            if(printType== 4){
                PrintClient.getInstance().printTextLeft(orderModel.getTransactionId());
            }

        }else if(printType == 2 || printType== 3){
            //解冻订单号
            PrintClient.getInstance().printTextLeft(context.getString(R.string.unfreezed_order_id) + "：");
            PrintClient.getInstance().printTextLeft(orderModel.getOutRequestNo());

            //平台预授权订单号
            PrintClient.getInstance().printTextLeft(context.getString(R.string.platform_pre_auth_order_id) + "：");
            PrintClient.getInstance().printTextLeft(orderModel.getAuthNo());
        }

        if(printType == 5){
            String tradename;
            if (!isAbsoluteNullStr(orderModel.getTradeName())){//先判断大写的Name，再判断小写name
                tradename=orderModel.getTradeName();
            }else {
                tradename=orderModel.getTradename();
            }
            PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_bill_stream_choice_title) + "：");
            PrintClient.getInstance().printTextLeft(tradename);

        }else{
            //支付方式
            PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_bill_stream_choice_title) + "：");
            PrintClient.getInstance().printTextLeft(orderModel.getTradeName());
        }
        if (printType == 3) {
            //办理人
            PrintClient.getInstance().printTextLeft(context.getString(R.string.tv_unfreezen_applicant) + "：");
            PrintClient.getInstance().printTextLeft(orderModel.getUserName());
        }
        //订单状态
        PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_bill_stream_statr) + "：");
        String operationType = "";
        if(printType == 1 || printType == 4){
            operationType = MainApplication.getPreAuthtradeStateMap().get(orderModel.getTradeState() + "");

        }else if(printType == 2){
            switch (orderModel.getOperationType()){
                case 2:
                    operationType = context.getString(R.string.freezen_success);
                    break;
            }
        }else if(printType == 3){
            operationType = context.getString(R.string.unfreezing);
        }else if (printType == 5){
            operationType = context.getString(R.string.pre_auth_authorized);
        }
        PrintClient.getInstance().printTextLeft(operationType);

        switch (printType){
            case 1:
            case 4:
            case 5:
                if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(context.getString(R.string.pay_yuan))){
                    PrintClient.getInstance().printTextLeft(context.getString(R.string.pre_auth_amount)+":");
                    PrintClient.getInstance().printTextRight(DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + context.getString(R.string.pay_yuan));
                }else{
                    long preAuthorizationMoney=0;//用来接收printType判断授权金额来自反扫、正扫
                    if (printType==1){
                        preAuthorizationMoney=orderModel.getRestAmount();//反扫
                    }else if (printType==5){
                        preAuthorizationMoney=orderModel.getTotalFee();//正扫
                    }else if(printType==4){
                        preAuthorizationMoney=orderModel.getTotalFreezeAmount();//预授权详情
                    }
                    //预授权金额
                    PrintClient.getInstance().printTextLeft(context.getString(R.string.pre_auth_amount)+":");
                    PrintClient.getInstance().printTextRight(MainApplication.feeType + DateUtil.formatMoneyUtils(preAuthorizationMoney));
                }
                PrintClient.getInstance().printEmptyLine();
                PrintClient.getInstance().printEmptyLine();

                //二维码
                //打印小票的代码
                PrintClient.getInstance().printQRCode(orderModel.getAuthNo());
                //此二维码用于预授权收款、解冻操作
                PrintClient.getInstance().printTextCenter(context.getString(R.string.scan_to_check));

                break;
            case 2:
                if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(context.getString(R.string.pay_yuan))){
                    PrintClient.getInstance().printTextLeft(context.getString(R.string.unfreezing_amount)+":");
                    PrintClient.getInstance().printTextRight(DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + context.getString(R.string.pay_yuan));
                }else{
                    PrintClient.getInstance().printTextLeft(context.getString(R.string.unfreezing_amount)+":");
                    PrintClient.getInstance().printTextRight(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getMoney()));
                }
                //解冻将原路返回支付账户或银行卡，到账时间已第三方为准
                PrintClient.getInstance().printMultiLines(context.getString(R.string.unfreeze_pos));
                break;
            case 3:
                //授权金额
                if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(context.getString(R.string.pay_yuan))){
                    PrintClient.getInstance().printTextLeft(context.getString(R.string.pre_auth_amount)+":");
                    PrintClient.getInstance().printTextRight(DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + context.getString(R.string.pay_yuan));
                }else{
                    PrintClient.getInstance().printTextLeft(context.getString(R.string.pre_auth_amount)+":");
                    PrintClient.getInstance().printTextRight(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFreezeAmount()));
                }
                //解冻金额
                if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(context.getString(R.string.pay_yuan))){
                    PrintClient.getInstance().printTextLeft(context.getString(R.string.unfreezing_amount)+":");
                    PrintClient.getInstance().printTextRight( DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + context.getString(R.string.pay_yuan));
                }else{
                    PrintClient.getInstance().printTextLeft(context.getString(R.string.unfreezing_amount)+":");
                    PrintClient.getInstance().printTextRight( MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee()));
                }
                //解冻将原路返回支付账户或银行卡，到账时间已第三方为准
                PrintClient.getInstance().printMultiLines(context.getString(R.string.unfreeze_pos));
                break;
            default:
                break;
        }

        PrintClient.getInstance().pintSingleLine();

        PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()));
        PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_sign) + "：");

        PrintClient.getInstance().printEmptyLine();
        PrintClient.getInstance().printEmptyLine();
        PrintClient.getInstance().printEmptyLine();

        PrintClient.getInstance().startPrint();
    }


    public static boolean isAbsoluteNullStr(String str) {
        str = deleteBlank(str);
        if (str == null || str.length() == 0 || "null".equalsIgnoreCase(str)) {
            return true;
        }

        return false;
    }

    /**
     * 去前后空格和去换行符
     *
     * @param str
     * @return*/


    public static String deleteBlank(String str) {
        if (null != str && 0 < str.trim().length()) {
            char[] array = str.toCharArray();
            int start = 0, end = array.length - 1;
            while (array[start] == ' ') start++;
            while (array[end] == ' ') end--;
            return str.substring(start, end + 1).replaceAll("\n", "");

        } else {
            return "";
        }

    }

}
