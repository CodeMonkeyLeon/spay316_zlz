package cn.swiftpass.enterprise.mvp.base


/**
 * @author lizheng.zhao
 * @date 2022/11/15
 *
 * @你应该是一场梦我应该是一阵风
 */
interface BasePresenter<V : BaseView> {

    /**
     * Presenter绑定View，一般在activity的onCreated()会调用
     */
    fun attachView(view: V?)

    /**
     * Presenter解绑View，一般在activity的onDestroy()会调用
     */
    fun detachView()
}