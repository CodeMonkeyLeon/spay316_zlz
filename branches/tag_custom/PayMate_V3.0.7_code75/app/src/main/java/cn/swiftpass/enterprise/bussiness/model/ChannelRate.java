package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created by aijingya on 2018/5/28.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/5/28.15:58.
 */

public class ChannelRate  implements Serializable {

    private String payCenterId;  //1,
    private String centerName;   //微信境外通道（跨境通）
    private String payRate;  //12
    private String isClose; //0


    public String getPayCenterId() {
        return payCenterId;
    }

    public void setPayCenterId(String payCenterId) {
        this.payCenterId = payCenterId;
    }

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }

    public String getPayRate() {
        return payRate;
    }

    public void setPayRate(String payRate) {
        this.payRate = payRate;
    }

    public String getIsClose() {
        return isClose;
    }

    public void setIsClose(String isClose) {
        this.isClose = isClose;
    }



}
