package cn.swiftpass.enterprise.io.database.access;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.text.TextUtils;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.common.DataObserver;
import cn.swiftpass.enterprise.bussiness.logica.common.DataObserverManager;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.io.database.table.OrderTable;
import cn.swiftpass.enterprise.utils.Utils;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-9-23
 * Time: 上午11:40
 */
public class OrderDB
{
    private static Dao<Order, Integer> orderDao;
    
    private OrderDB()
    {
        try
        {
            orderDao = MainApplication.getContext().getHelper().getDao(Order.class);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    
    private static OrderDB instance;
    
    public static OrderDB getInstance()
    {
        if (instance == null)
        {
            instance = new OrderDB();
        }
        return instance;
    }
    
    /***
     * 保存订单信息
     * @param orderModel
     */
    public void saveOrder(Order orderModel)
        throws SQLException
    {
        orderModel.employeeId = LocalAccountManager.getInstance().getUID();
        //        orderModel.notify_time = orderModel.notifyTime > 0 ? orderModel.notifyTime : 0;
        orderDao.create(orderModel);
    }
    
    /***
     * 修改订单信息
     * @param orderModel
     */
    public boolean update(Order orderModel)
        throws SQLException
    {
        int r = orderDao.update(orderModel);
        if (r > 0)
        {
            notifyMemberChanged();
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**修改订单*/
    public boolean updateOrderState(Order o)
    {
        try
        {
            UpdateBuilder<Order, Integer> builder = orderDao.updateBuilder();
            builder.updateColumnValue(OrderTable.COLUMN_STATE, o.state);
            Where<Order, Integer> where = builder.where();
            where.eq(OrderTable.COLUMN_ORDER_NO, o.orderNo);
            where.and();
            where.eq(OrderTable.COLUMN_U_ID, o.employeeId);
            return orderDao.update(builder.prepare()) > 0;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }
    
    /** 获取单个订单 */
    public Order getOrderByOrderNo(String orderNo, long uId)
    {
        QueryBuilder<Order, Integer> builder = orderDao.queryBuilder();
        try
        {
            Where<Order, Integer> where = builder.where();
            where.eq(OrderTable.COLUMN_ORDER_NO, orderNo);
            where.and();
            where.eq(OrderTable.COLUMN_U_ID, uId);
            List<Order> orders = orderDao.query(builder.prepare());
            if (orders != null && orders.size() > 0)
            {
                return orders.get(0);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            
        }
        return null;
    }
    
    /** 模糊查询 */
    public List<Order> queryOrder(String orderNo, long uId, long time)
        throws SQLException
    {
        QueryBuilder<Order, Integer> builder = orderDao.queryBuilder();
        Where<Order, Integer> where = builder.where();
        where.eq(OrderTable.COLUMN_U_ID, uId);
        if (TextUtils.isEmpty(orderNo))
        {
            where.or();
            where.ge(OrderTable.COLUMN_ORDER_NO, orderNo);
            where.and();
        }
        if (time != 0)
        {
            where.or();
            where.lt(OrderTable.COLUMN_ADD_TIME, time);
        }
        
        builder.orderBy(OrderTable.COLUMN_ADD_TIME, false);
        return orderDao.query(builder.prepare());
    }
    
    /**
     * 模糊查询
     * type 0 服务器申请
     *      1 本地生成订单
     *      2.本地生成订单已经上传服务器
     * */
    public List<Order> queryOrderByType(int type, long uId)
        throws SQLException
    {
        QueryBuilder<Order, Integer> builder = orderDao.queryBuilder();
        Where<Order, Integer> where = builder.where();
        where.eq(OrderTable.COLUMN_U_ID, uId);
        where.and();
        where.eq(OrderTable.COLUMN_CREATE_ORDER_TYPE, type);
        builder.orderBy(OrderTable.COLUMN_ADD_TIME, false);
        return orderDao.query(builder.prepare());
    }
    
    /**修改订单*/
    public boolean updateLocalOrderType(List<Order> orders, int type, long uId)
    {
        try
        {
            UpdateBuilder<Order, Integer> builder = orderDao.updateBuilder();
            builder.updateColumnValue(OrderTable.COLUMN_CREATE_ORDER_TYPE, type);
            Where<Order, Integer> where = builder.where();
            List<Long> ids = new ArrayList<Long>();
            for (Order o : orders)
            {
                ids.add(o.id);
            }
            where.in(OrderTable.COLUMN_ID, ids);
            where.and();
            where.eq(OrderTable.COLUMN_U_ID, uId);
            
            return orderDao.update(builder.prepare()) > 0;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }
    
    /**获取本地最后一笔交易*/
    public Order queryOrderByLastTime(long uId)
        throws SQLException
    {
        QueryBuilder<Order, Integer> builder = orderDao.queryBuilder();
        GenericRawResults<String[]> rawResults =
            orderDao.queryRaw("select " + " id,order_no,m_id,u_id,state,finsh_time,money,userName,add_time "
                + " from t_order where add_time = (select max(add_time) from " + OrderTable.TABLE_NAME
                + " where u_Id=?)", uId + "");
        List<String[]> list = rawResults.getResults();
        String[] srt = list.get(0);
        Order orderModel = new Order();
        orderModel.id = Utils.Integer.tryParse(srt[0], 0);
        orderModel.orderNo = srt[1];
        orderModel.merchantId = srt[2];
        orderModel.employeeId = Utils.Integer.tryParse(srt[3], 0);
        //        orderModel.state = Utils.Integer.tryParse(srt[4], 0);
        orderModel.finishTime = Utils.Long.tryParse(srt[5], 0);
        orderModel.money = Utils.Integer.tryParse(srt[6], 0);
        orderModel.userName = srt[7];
        orderModel.add_time = Utils.Long.tryParse(srt[8], 0);
        Date addDateTime = new Date();
        addDateTime.setTime(orderModel.add_time);
        //        orderModel.addTime = addDateTime.getTime();
        Date notifyDateTime = new Date();
        notifyDateTime.setTime(orderModel.finishTime);
        //        orderModel.notifyTime = notifyDateTime.getTime();
        
        return orderModel;
    }
    
    private void notifyMemberChanged()
    {
        DataObserverManager.getInstance().notifyChange(Order.class.getName());
    }
    
    private void notifyMemberChanged1()
    {
        DataObserverManager.getInstance().notifyChange(DataObserver.EVENT_ORDER_STATE_CHANGE);
    }
}
