package cn.swiftpass.enterprise.ui.activity.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.TitleBar;

/**
 * 退款密码管理
 * <功能详细描述>
 * 
 * @author  yaowei
 * @version  [版本号, 2015-8-20]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class TuiKuanPwdSettingActivity extends TemplateActivity
{
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tuikuan_password_setting);
    }
    
    @Override
    protected boolean isLoginRequired()
    {
        return true;
    }
    
    public static void startActivity(Context context)
    {
        Intent it = new Intent();
        it.setClass(context, TuiKuanPwdSettingActivity.class);
        context.startActivity(it);
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setTitle(R.string.title_change_tuikuan_pwd);
        titleBar.setLeftButtonVisible(true);
        
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
            }
            
            @Override
            public void onRightLayClick()
            {
                // TODO Auto-generated method stub
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                // TODO Auto-generated method stub
                
            }
        });
    }
    
}
