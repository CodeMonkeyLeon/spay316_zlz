package cn.swiftpass.enterprise.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.annotation.Nullable;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.FeedbackContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.FeedbackPresenter;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.NetworkUtils;

/**
 * 上传日志
 * User: Alan
 * Date: 13-11-27
 * Time: 下午5:47
 */
public class FeedbackActivity extends BaseActivity<FeedbackContract.Presenter> implements View.OnClickListener, FeedbackContract.View {

    long time;
    private EditText etFeedback;
    private Button btnSend;
    private CheckBox cbLog;

    public static void startActivity(Context mContext) {
        Intent it = new Intent();
        it.setClass(mContext, FeedbackActivity.class);
        mContext.startActivity(it);
    }

    @Override
    protected FeedbackContract.Presenter createPresenter() {
        return new FeedbackPresenter();
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();

    }

    private void initView() {
        setContentView(R.layout.activity_feedback);
        btnSend = getViewById(R.id.btn_send_feedback);
        btnSend.setOnClickListener(this);
        etFeedback = getViewById(R.id.et_feedback);
        cbLog = getViewById(R.id.cb_send_log_box);
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_feedback);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {
                // TODO Auto-generated method stub

            }

            @Override
            public void onRightButLayClick() {
                // TODO Auto-generated method stub

            }
        });
    }

    @Override
    protected boolean isLoginRequired() {
        return false;
    }


    @Override
    public void sendFeedBackSuccess(boolean response) {
        etFeedback.setText("");
        time = System.currentTimeMillis();
        showToastInfo(R.string.mgs_feedback_send_tip);
    }

    @Override
    public void sendFeedBackFailed(@Nullable Object error) {
        etFeedback.setText("");
        time = System.currentTimeMillis();
        showToastInfo(error.toString());
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_send_feedback) {
            //进行网络判断
            if (!NetworkUtils.isNetworkAvailable(this)) {
                showToastInfo("网络不可用，请打开网络连接！");
                return;
            }

            String content = etFeedback.getText().toString();
            if (content.length() == 0) {
                showToastInfo(R.string.mgs_feedback_tip);
                return;
            }
            if (System.currentTimeMillis() - time < 1000 * 30) {
                showToastInfo(R.string.mgs_common_error_tip);
                return;
            }

            if (mPresenter != null) {
                mPresenter.sendFeedBack(content);
            }
        }
    }

}
