package cn.swiftpass.enterprise.ui.widget;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-3-6
 * Time: 下午6:21
 */
public class ProgressInfoDialog extends Dialog
{
    private ViewGroup mRootView;
    
    private Activity activity;
    
    private ProgressInfoDialog.HandleBtn handleBtn;
    
    private ImageView iv_pay_stop;
    
    public interface HandleBtn
    {
        void handleOkBtn();
    }
    
    public ProgressInfoDialog(Activity context, String title, final ProgressInfoDialog.HandleBtn handleBtn)
    {
        super(context, R.style.my_dialog);
        activity = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.spay_dialog_progressinfo, null);
        setContentView(mRootView);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        this.setCanceledOnTouchOutside(false);
        this.handleBtn = handleBtn;
        iv_pay_stop = (ImageView)findViewById(R.id.iv_pay_stop);
        if (null == handleBtn)
        {
            iv_pay_stop.setVisibility(View.GONE);
        }
        else
        {
            iv_pay_stop.setOnClickListener(new View.OnClickListener()
            {
                
                @Override
                public void onClick(View v)
                {
                    handleBtn.handleOkBtn();
                }
            });
        }
        
        TextView tvTitle = (TextView)mRootView.findViewById(R.id.tv_dialog);
        if (title != null)
        {
            tvTitle.setText(title);
        }
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            //这里写你要在用户按下返回键同时执行的动作
            //if(!flag){
            activity.moveTaskToBack(false); //核心代码：屏蔽返回行为
            // }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    //    public static void show(Activity context, String title)
    //    {
    //        ProgressInfoDialog dia = new ProgressInfoDialog(context, title);
    //        dia.show();
    //    }
}