package cn.swiftpass.enterprise.print;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalInfo;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalItemInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * Created by aijingya on 2020/8/18.
 *
 * @Package cn.swiftpass.enterprise.print
 * @Description:
 * @date 2020/8/18.14:30.
 */
public class PrintSummary {

    /***
     * 打印日结小票
     * @param info
     */
    public static void pintSum(Context context,OrderTotalInfo info){
        SunmiPrintHelper.getInstance().setAlign(1);
        SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_blue_print_data_sum)+ "\n", 36, true, false);
        SunmiPrintHelper.getInstance().printText("\n", 28, false, false);

        SunmiPrintHelper.getInstance().setAlign(0);
        SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.shop_name)+ "：" + "\n"+ MainApplication.getMchName() + "\n", 24, false, false);
        SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_blue_print_merchant_no)+ "：" + MainApplication.getMchId() + "\n", 24, false, false);

        if (!StringUtil.isEmptyOrNull(info.getUserName())) {
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_user) + "：" + info.getUserName()+ "\n", 24, false, false);
        }
        SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_blue_print_start_time) + "：" + info.getStartTime() +"\n", 24, false, false);
        SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_blue_print_end_time)+ "：" + info.getEndTime() + "\n", 24, false, false);
        SunmiPrintHelper.getInstance().printText("===========================\n", 28, false, false);
        if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_bill_stream_pay_money) + "：" + DateUtil.formatRMBMoneyUtils(info.getCountTotalFee()) + ToastHelper.toStr(R.string.pay_yuan) + "\n", 24, false, false);
        } else {
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_bill_stream_pay_money) + "：" + MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(info.getCountTotalFee()) + "\n", 24, false, false);
        }
        SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_bill_stream_pay_money_num) + "：" + info.getCountTotalCount() + "\n", 24, false, false);

        if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tv_refund_dialog_refund_money) + DateUtil.formatRMBMoneyUtils(info.getCountTotalRefundFee()) + ToastHelper.toStr(R.string.pay_yuan) + "\n", 24, false, false);
        } else {
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tv_refund_dialog_refund_money) + MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(info.getCountTotalRefundFee()) + "\n", 24, false, false);
        }
        SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_bill_reufun_total_num) + "：" + info.getCountTotalRefundCount() + "\n", 24, false, false);

        if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_blue_print_pay_total) + "：" + DateUtil.formatRMBMoneyUtils((info.getCountTotalFee() - info.getCountTotalRefundFee())) + ToastHelper.toStr(R.string.pay_yuan) + "\n", 24, false, false);
        } else {
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_blue_print_pay_total) + "：" + MainApplication.getFeeFh() + DateUtil.formatMoneyUtils((info.getCountTotalFee() - info.getCountTotalRefundFee())) + "\n", 24, false, false);
        }
        SunmiPrintHelper.getInstance().printText("===========================\n", 28, false, false);

        /**
         * 是否开通小费功能，打印小费金额、总计
         */
        if (MainApplication.isTipOpenFlag()){
            // 小费金额
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tip_amount) + "：" + MainApplication.getFeeType() + DateUtil.formatMoneyUtils(info.getCountTotalTipFee()) + "\n", 24, false, false);
            //小费笔数
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tip_count) + "：" + info.getCountTotalTipFeeCount() + "\n", 24, false, false);
            SunmiPrintHelper.getInstance().printText("===========================\n", 28, false, false);
        }

        List<OrderTotalItemInfo> list = info.getOrderTotalItemInfo();

        Map<Integer, Object> map = new HashMap<Integer, Object>();

        for (OrderTotalItemInfo itemInfo : list) {

            switch (itemInfo.getPayTypeId()) {

                case 1:
                    map.put(0, itemInfo);
                    break;
                case 2:
                    map.put(1, itemInfo);
                    break;

                case 4:
                    map.put(2, itemInfo);
                    break;
                case 12:
                    map.put(3, itemInfo);
                    break;
                default:
                    map.put(itemInfo.getPayTypeId(), itemInfo);
                    break;
            }

        }
        for (Integer key : map.keySet()) {
            OrderTotalItemInfo itemInfo = (OrderTotalItemInfo) map.get(key);
            if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan) )) {
                SunmiPrintHelper.getInstance().printText(MainApplication.getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId())) + ToastHelper.toStr(R.string.tx_money)+DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + ToastHelper.toStr(R.string.pay_yuan) + "\n", 24, false, false);
            } else {
                SunmiPrintHelper.getInstance().printText(MainApplication.getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId()))+"\n", 24, false, false);
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_money)+MainApplication.getFeeType() + DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "\n", 24, false, false);
            }

            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tv_settle_count) + "："+itemInfo.getSuccessCount() + "\n", 24, false, false);
        }
        SunmiPrintHelper.getInstance().printText("---------------------------\n", 28, false, false);
        SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n", 24, false, false);

        SunmiPrintHelper.getInstance().print3Line();

    }
}
