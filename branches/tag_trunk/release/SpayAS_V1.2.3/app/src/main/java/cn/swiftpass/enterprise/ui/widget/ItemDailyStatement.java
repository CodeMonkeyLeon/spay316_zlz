package cn.swiftpass.enterprise.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.CashierReport;
import cn.swiftpass.enterprise.utils.DateUtil;

/**
 * 统计
 * User: Alan
 * Date: 13-9-28
 * Time: 上午10:17
 */
public class ItemDailyStatement extends LinearLayout
{
    private TextView tvDateTime, tvNumber, tvMoney, tvState;
    
    public ItemDailyStatement(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
    
    @Override
    protected void onFinishInflate()
    {
        tvDateTime = (TextView)findViewById(R.id.tv_datetime);
        tvMoney = (TextView)findViewById(R.id.tv_money);
        tvNumber = (TextView)findViewById(R.id.tv_number);
        
    }
    
    public void setData(CashierReport d)
    {
        if (d != null)
        {
            tvDateTime.setText(DateUtil.formatYYMD(Long.parseLong(d.addTime)));//DateUtil.formatMD(d.addTime.getTime())
            double money = d.turnover / 100d;
            double runMoney = d.refundFee / 100d;
            double total = money + runMoney;
            tvMoney.setText("¥ " + (DateUtil.formatMoneyUtil(total)));
            tvNumber.setText("(" + (d.tradeNum + d.refundNum) + "笔)");
        }
    }
    
}
