/*

 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-14
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.ziyeyouhu.library.KeyboardTouchListener;
import com.ziyeyouhu.library.KeyboardUtil;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.ChanagePwdActivity;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.WelcomeActivity;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 忘记密码
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-14]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class FindPassSubmitActivity extends TemplateActivity {
    private static final String TAG = FindPassSubmitActivity.class.getCanonicalName();

    private EditText et_id, et_pass;

    private ImageView iv_clean_input, iv_clean_input_pass;

    private Button btn_next_step;

    private UserModel model;

    @Override
    protected boolean isLoginRequired() {
        return false;
    }

    public static void startActivity(Context context, UserModel model) {
        Intent it = new Intent();
        it.setClass(context, FindPassSubmitActivity.class);
        it.putExtra("userModel", model);
        context.startActivity(it);
    }

//    @Override
//    public boolean equals(Object obj) {
//        return super.equals(obj);
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_pass_submit);

        initView();

        setLister();
        btn_next_step.getBackground().setAlpha(102);
        model = (UserModel) getIntent().getSerializableExtra("userModel");
        MainApplication.listActivities.add(this);
        initMoveKeyBoard();

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                keyboardUtil.showKeyBoardLayout(et_id, KeyboardUtil.INPUTTYPE_ABC, -1);
//            }
//        }, 200);
    }

    private LinearLayout rootView;
    private KeyboardUtil keyboardUtil;
    private ScrollView scrollView;

    //安全键盘
    private void initMoveKeyBoard() {
        rootView = (LinearLayout) findViewById(R.id.rootview);
        scrollView = (ScrollView) findViewById(R.id.scrollview);
        keyboardUtil = new KeyboardUtil(this, rootView, scrollView);
        // monitor the KeyBarod state
        keyboardUtil.setKeyBoardStateChangeListener(new KeyBoardStateListener());
        // monitor the finish or next Key
        keyboardUtil.setInputOverListener(new inputOverListener());
        et_id.setOnTouchListener(new KeyboardTouchListener(keyboardUtil, KeyboardUtil.INPUTTYPE_ABC, -1));
        et_pass.setOnTouchListener(new KeyboardTouchListener(keyboardUtil, KeyboardUtil.INPUTTYPE_ABC, -1));
    }



    class KeyBoardStateListener implements KeyboardUtil.KeyBoardStateChangeListener {

        @Override
        public void KeyBoardStateChange(int state, EditText editText) {

        }
    }

    class inputOverListener implements KeyboardUtil.InputFinishListener {

        @Override
        public void inputHasOver(int onclickType, EditText editText) {

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (keyboardUtil.isShow) {
                keyboardUtil.hideSystemKeyBoard();
                keyboardUtil.hideAllKeyBoard();
                keyboardUtil.hideKeyboardLayout();

                finish();
            } else {
                return super.onKeyDown(keyCode, event);
            }

            return false;
        } else{
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b, enable, res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_press_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }

    //    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
//            if (keyboardUtil.isShow) {
//                keyboardUtil.hideSystemKeyBoard();
//                keyboardUtil.hideAllKeyBoard();
//                keyboardUtil.hideKeyboardLayout();
//            } else {
//                return super.onKeyDown(keyCode, event);
//            }
//
//            return false;
//        } else
//            return super.onKeyDown(keyCode, event);
//    }



    private void initView() {
        et_pass = getViewById(R.id.et_pass);
        et_id = getViewById(R.id.et_id);
        iv_clean_input = getViewById(R.id.iv_clean_input);
        btn_next_step = getViewById(R.id.btn_next_step);
        iv_clean_input_pass = getViewById(R.id.iv_clean_input_pass);
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (et_id.isFocused()) {
                    if (et_id.getText().toString().length() > 0) {
                        iv_clean_input.setVisibility(View.VISIBLE);

                    } else {
                        iv_clean_input.setVisibility(View.GONE);
                    }
                }

                if (et_pass.isFocused()) {
                    if (et_pass.getText().toString().length() > 0) {
                        iv_clean_input_pass.setVisibility(View.VISIBLE);
                    } else {
                        iv_clean_input_pass.setVisibility(View.GONE);
                    }
                }
                if (!StringUtil.isEmptyOrNull(et_id.getText().toString())
                        && !StringUtil.isEmptyOrNull(et_pass.getText().toString())) {
                    setButtonBg(btn_next_step, true, R.string.bt_confirm);
                } else {
                    setButtonBg(btn_next_step, false, R.string.bt_confirm);
                }
            }
        });
        et_pass.addTextChangedListener(editTextWatcher);
        et_id.addTextChangedListener(editTextWatcher);
    }

    private void setLister() {
        iv_clean_input.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                et_id.setText("");
                iv_clean_input.setVisibility(View.GONE);
            }
        });

        iv_clean_input_pass.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                et_pass.setText("");
                iv_clean_input_pass.setVisibility(View.GONE);
            }
        });

        btn_next_step.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                if (StringUtil.isEmptyOrNull(et_id.getText().toString())) {
                    toastDialog(FindPassSubmitActivity.this, R.string.tx_newpass_notnull, null);
                    et_id.setFocusable(true);
                    return;
                }
                if (StringUtil.isEmptyOrNull(et_pass.getText().toString())) {
                    toastDialog(FindPassSubmitActivity.this, R.string.tx_repeatpass_notnull, null);
                    et_pass.setFocusable(true);
                    return;
                }

                if (et_id.getText().toString().length() < 8)
                {
                    et_id.setFocusable(true);
                    toastDialog(FindPassSubmitActivity.this, R.string.show_pass_prompt, null);
                    return;
                }

                //校验新密码是否符合8-16位字符
                if(!isContainAll(et_id.getText().toString())){
                    et_id.setFocusable(true);
                    toastDialog(FindPassSubmitActivity.this, R.string.et_new_pass, null);
                    return;
                }


                if (!et_id.getText().toString().equals(et_pass.getText().toString())) {
                    toastDialog(FindPassSubmitActivity.this, R.string.tx_pass_notdiff, null);
                    et_pass.setFocusable(true);
                    return;
                }

                model.setIDNumber(et_id.getText().toString());

                UserManager.checkData(model, new UINotifyListener<Boolean>() {
                    @Override
                    public void onPreExecute() {
                        super.onPreExecute();
                        loadDialog(FindPassSubmitActivity.this, getString(R.string.tx_confirm_loading));
                    }

                    @Override
                    public void onError(final Object object) {
                        super.onError(object);
                        dismissLoading();
                        if (object != null) {
                            toastDialog(FindPassSubmitActivity.this, object.toString(), null);
                        }
                    }

                    @Override
                    public void onSucceed(Boolean result) {
                        super.onSucceed(result);
                        dismissLoading();
                        if (result) {
                            //toastDialog(FindPassSubmitActivity.this, R.string.tx_modify_succ, null);
                            showPage(SetNewPassSuccActivity.class);
                        }

                    }
                });
            }
        });
    }

    //用来校验密码，是否包含数字和字母
    public boolean isContainAll(String str){
        boolean isDigit = false;
                /*boolean isLowerCase = false;
                boolean isUpperCase = false;*/
        boolean isLetters = false;

        for(int i = 0 ; i < str.length(); i++){
            if(Character.isDigit(str.charAt(i))){
                isDigit = true;
            }else if(Character.isLowerCase(str.charAt(i))){
//                        isLowerCase = true;
                isLetters = true;
            }else if(Character.isUpperCase(str.charAt(i))){
//                        isUpperCase = true;
                isLetters = true;
            }
        }
        String regex = "^[a-zA-Z0-9]+$";
//                boolean isCorrect = isDigit&&isLowerCase&&isUpperCase&&str.matches(regex);
        boolean isCorrect = isDigit&&isLetters&&str.matches(regex);
        return isCorrect;
    }


    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tv_find_pass_title_info1);
    }

}
