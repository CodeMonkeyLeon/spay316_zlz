package cn.swiftplus.enterprise.networksdk.response;

import cn.swiftplus.enterprise.networksdk.interfaces.IResponseCallback;

/**
 * Created by aijingya on 2020/11/25.
 *
 * @Package cn.swiftplus.enterprise.networksdk
 * @Description: 封装回调接口和要转换的实体对象
 * @date 2020/11/25.11:05.
 */
public  class ResposeDataHandle {

    public IResponseCallback mListener = null;
    public Class<?> mClass = null;

    public ResposeDataHandle(IResponseCallback listener) {
        this.mListener = listener;
    }

    public ResposeDataHandle(IResponseCallback listener, Class<?> clazz) {
        this.mListener = listener;
        this.mClass = clazz;
    }

}
