package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView
import cn.swiftpass.enterprise.ui.paymentlink.model.OrderConfig
import cn.swiftpass.enterprise.ui.paymentlink.model.ShareInfo

/**
 * @author lizheng.zhao
 * @date 2022/12/6
 *
 * @在此地
 * @在国际的鸡尾酒里
 * @我仍是一块拒绝溶化的冰
 * @常保持零下的冷和固体的硬度
 */
class ShareOrderContract {


    interface View : BaseView {

        fun sendMessageSuccess(response: String?)

        fun sendMessageFailed(error: Any?)

        fun sendEmailSuccess(response: String?)

        fun sendEmailFailed(error: Any?)

        fun getOrderConfigSuccess(response: OrderConfig?)

        fun getOrderConfigFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {

        fun getOrderConfig()

        fun sendEmail(shareInfo: ShareInfo?)

        fun sendMessage(shareInfo: ShareInfo?)

    }

}