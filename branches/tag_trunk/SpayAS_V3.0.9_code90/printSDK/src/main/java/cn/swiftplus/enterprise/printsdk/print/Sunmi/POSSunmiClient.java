package cn.swiftplus.enterprise.printsdk.print.Sunmi;

import android.content.Context;

import cn.swiftplus.enterprise.printsdk.print.Sunmi.printUtils.SunmiPrintHelper;


/**
 * Created by aijingya on 2020/10/20.
 *
 * @Package cn.swiftplus.enterprise.printsdk.print.sunmi
 * @Description:
 * @date 2020/10/20.21:18.
 */
public class POSSunmiClient {
    private static POSSunmiClient sSunmiClient;
    private Context mContext;

    public static void initSunmi(Context context){
        if (sSunmiClient != null) {
            throw new RuntimeException("SunmiClient Already initialized");
        }
        sSunmiClient = new POSSunmiClient(context);
    }

    public static POSSunmiClient getInstance(){
        if (sSunmiClient == null) {
            throw new RuntimeException("SunmiClient is not initialized");
        }
        return sSunmiClient;
    }

    private POSSunmiClient(Context context){
        mContext = context;
        //初始化 sunmi pos 打印机
        SunmiPrintHelper.getInstance().initSunmiPrinterService(context);
        SunmiPrintHelper.getInstance().initPrinter();
    }

}
