package cn.swiftpass.enterprise.newmvp.config;


/**
 * 创建人：caoxiaoya
 * 时间：2019/3/18
 * 描述：请求参数所有的Key,Key是不可变参数
 * 备注：
 */
public final class RequestKeyConfig {
    /**
     * 公钥
     */
    public final static String PUBLICKEY = "publicKey";
    /**
     * 时间戳
     */
    public final static String SPAYRS = "spayRs";
    public final static String NNS = "nns";
    /**
     * 服务器返回的code
     */
    public final static String RESULT = "result";
    /**
     * 自定义code
     */
    public final static String CODE = "code";
    /**
     * 自定义data
     */
    public final static String DATA = "data";
    /**
     * 自定义错误消息  400的时候，MESSAGE返回
     */
    public final static String ERROR = "error";
    /**
     * 服务器返回的String json
     */
    public final static String MESSAGE = "message";
    /**
     * 密钥标识
     */
    public final static String SKEY = "skey";
    /**
     * android终端类型
     */
    public static final String CLIENT = "client";
    /**
     * 账号
     */
    public static final String USERNAME="username";
    public static final String PASSWORD="password";
    public static final String ANDROIDTRANSPUSH="androidTransPush";
    public static final String PUSHCID="pushCid";
    public static final String BANKCODE="bankCode";


}

