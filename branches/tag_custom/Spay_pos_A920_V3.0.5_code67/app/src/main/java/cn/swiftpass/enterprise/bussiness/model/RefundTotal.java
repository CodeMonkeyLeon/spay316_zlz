package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

public class RefundTotal implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    private Integer tradeNum = 0; // 交易成功总数
    
    private long turnover = 0; // 交易成功总金额
    
    private Integer refundNum = 0; // 退款总数
    
    private long refundFee = 0; // 退款总金额
    
    private Integer noPayNum = 0; // 未支付总笔数
    
    private long noPayMoeny = 0; // 未支付总金额
    
    private long reverseMoney = 0;// 冲正总金额
    
    private Integer reverseNum = 0;// 冲正总笔数
    
    /**
     * @return 返回 tradeNum
     */
    public Integer getTradeNum()
    {
        return tradeNum;
    }
    
    /**
     * @param 对tradeNum进行赋值
     */
    public void setTradeNum(Integer tradeNum)
    {
        this.tradeNum = tradeNum;
    }
    
    /**
     * @return 返回 turnover
     */
    public long getTurnover()
    {
        return turnover;
    }
    
    /**
     * @param 对turnover进行赋值
     */
    public void setTurnover(long turnover)
    {
        this.turnover = turnover;
    }
    
    /**
     * @return 返回 refundNum
     */
    public Integer getRefundNum()
    {
        return refundNum;
    }
    
    /**
     * @param 对refundNum进行赋值
     */
    public void setRefundNum(Integer refundNum)
    {
        this.refundNum = refundNum;
    }
    
    /**
     * @return 返回 refundFee
     */
    public long getRefundFee()
    {
        return refundFee;
    }
    
    /**
     * @param 对refundFee进行赋值
     */
    public void setRefundFee(long refundFee)
    {
        this.refundFee = refundFee;
    }
    
    /**
     * @return 返回 noPayNum
     */
    public Integer getNoPayNum()
    {
        return noPayNum;
    }
    
    /**
     * @param 对noPayNum进行赋值
     */
    public void setNoPayNum(Integer noPayNum)
    {
        this.noPayNum = noPayNum;
    }
    
    /**
     * @return 返回 noPayMoeny
     */
    public long getNoPayMoeny()
    {
        return noPayMoeny;
    }
    
    /**
     * @param 对noPayMoeny进行赋值
     */
    public void setNoPayMoeny(long noPayMoeny)
    {
        this.noPayMoeny = noPayMoeny;
    }
    
    /**
     * @return 返回 reverseMoney
     */
    public long getReverseMoney()
    {
        return reverseMoney;
    }
    
    /**
     * @param 对reverseMoney进行赋值
     */
    public void setReverseMoney(long reverseMoney)
    {
        this.reverseMoney = reverseMoney;
    }
    
    /**
     * @return 返回 reverseNum
     */
    public Integer getReverseNum()
    {
        return reverseNum;
    }
    
    /**
     * @param 对reverseNum进行赋值
     */
    public void setReverseNum(Integer reverseNum)
    {
        this.reverseNum = reverseNum;
    }
    
    /**
     * @return 返回 outTradeNo
     */
    public String getOutTradeNo()
    {
        return outTradeNo;
    }
    
    /**
     * @param 对outTradeNo进行赋值
     */
    public void setOutTradeNo(String outTradeNo)
    {
        this.outTradeNo = outTradeNo;
    }
    
    /**
     * @return 返回 colseMoney
     */
    public long getColseMoney()
    {
        return colseMoney;
    }
    
    /**
     * @param 对colseMoney进行赋值
     */
    public void setColseMoney(long colseMoney)
    {
        this.colseMoney = colseMoney;
    }
    
    /**
     * @return 返回 colseNum
     */
    public Integer getColseNum()
    {
        return colseNum;
    }
    
    /**
     * @param 对colseNum进行赋值
     */
    public void setColseNum(Integer colseNum)
    {
        this.colseNum = colseNum;
    }
    
    private String outTradeNo;// 交易订单号
    
    private long colseMoney = 0;// 关单总金额
    
    private Integer colseNum = 0;// 关单总笔数
    
}
