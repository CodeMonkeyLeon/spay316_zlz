package cn.swiftpass.enterprise.bussiness.model;

import java.util.List;
import java.util.Map;

import android.text.TextUtils;
import cn.swiftpass.enterprise.bussiness.enums.PayType;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.database.table.OrderTable;
import cn.swiftpass.enterprise.utils.ToastHelper;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = OrderTable.TABLE_NAME)
public class Order implements java.io.Serializable
{
    public static enum WxCardType
    {
        GROUPON_CARD("GROUPON"), CARSH_CARD("CASH"), DISCOUNT_CARD("DISCOUNT"), GIFT_CARD("GIFT");
        
        public String cardType;
        
        private WxCardType(String type)
        {
            this.cardType = type;
        }
        
        public static String getCardNameForType(String type)
        {
            if (null == type)
            {
                return "UNKNOW";
            }
            if (type.equalsIgnoreCase(GROUPON_CARD.cardType))
            {
                return ToastHelper.toStr(R.string.active_groupon_card);
            }
            else if (type.equalsIgnoreCase(CARSH_CARD.cardType))
            {
                return ToastHelper.toStr(R.string.active_carsh_card);
            }
            else if (type.equalsIgnoreCase(DISCOUNT_CARD.cardType))
            {
                return ToastHelper.toStr(R.string.active_discount_card);
            }
            else if (type.equalsIgnoreCase(GIFT_CARD.cardType))
            {
                return ToastHelper.toStr(R.string.active_gift_card);
            }
            else
            {
                return "UNKNOW";
            }
        }
    }
    
    public static enum TradeTypetoStr
    {
        PAY_WX_MICROPAY("pay.weixin.micropay"), PAY_ZFB_MICROPAY("pay.alipay.micropay"), PAY_QQ_MICROPAY(
            "pay.qq.micropay"), PAY_WX_NATIVE("pay.weixin.native"), PAY_ZFB_NATIVE("pay.alipay.native"), PAY_QQ_NATIVE(
            "pay.qq.jspay"), PAY_WX_JSPAY("pay.weixin.jspay"), PAY_QQ_PROXY_MICROPAY("pay.qq.proxy.micropay"), PAY_ALIPAY_WAP(
            "pay.alipay.wappay"), PAY_WX_WEB_SCAN("pay.weixin.scancode"), // 微信扫码支付
        PAY_WX_APP("pay.weixin.app"), PAY_ALIPAY_APP("pay.alipay.app"), PAY_JINGDONG("pay.jdpay.micropay"), ;
        private String tradeType;
        
        private TradeTypetoStr(String tradeType)
        {
            this.tradeType = tradeType;
        }
        
        public static String getTradeNameForType(String tradeType)
        {
            if (tradeType.equals(PAY_QQ_MICROPAY.tradeType) || tradeType.equals(PAY_QQ_PROXY_MICROPAY.tradeType))
            {
                return ToastHelper.toStr(R.string.qq_code_pay);
            }
            else if (tradeType.equals(PAY_ZFB_MICROPAY.tradeType))
            {
                return ToastHelper.toStr(R.string.zfb_code_pay);
            }
            else if (tradeType.equals(PAY_QQ_NATIVE.tradeType))
            {
                return ToastHelper.toStr(R.string.qq_scan_pay);
            }
            else if (tradeType.equals(PAY_ZFB_NATIVE.tradeType))
            {
                return ToastHelper.toStr(R.string.zfb_scan_code_pay);
            }
            else if (tradeType.equals(PAY_WX_NATIVE.tradeType))
            {
                return ToastHelper.toStr(R.string.weChat_pay_type);
            }
            else if (tradeType.equals(PAY_WX_JSPAY.tradeType))
            {// pay.weixin.jspay
                return ToastHelper.toStr(R.string.show_wx_jspay);
            }
            else if (tradeType.equals(PAY_ALIPAY_WAP.tradeType))
            {
                return ToastHelper.toStr(R.string.show_zfb_wappay);
            }
            else if (tradeType.equalsIgnoreCase(PAY_WX_WEB_SCAN.tradeType))
            {
                return ToastHelper.toStr(R.string.show_wx_webpay);
            }
            else if (tradeType.equalsIgnoreCase(PAY_WX_APP.tradeType))
            {
                return ToastHelper.toStr(R.string.show_wx_apppay);
            }
            else if (tradeType.equalsIgnoreCase(PAY_ALIPAY_APP.tradeType))
            {
                return ToastHelper.toStr(R.string.show_zfb_apppay);
            }
            else if (tradeType.equalsIgnoreCase(PAY_JINGDONG.tradeType))
            {
                return ToastHelper.toStr(R.string.show_jd_micpay);
            }
            else
            {
                return ToastHelper.toStr(R.string.wechat_car_pay);
            }
        }
    }
    
    private static final long serialVersionUID = 1L;
    
    @DatabaseField(generatedId = true)
    public long id;
    
    /**订单ID*/
    @DatabaseField(columnName = OrderTable.COLUMN_ORDER_NO)
    public String orderNo;
    
    /**用户ID*/
    @DatabaseField(columnName = OrderTable.COLUMN_U_ID)
    public long employeeId;
    
    @DatabaseField(columnName = OrderTable.COLUMN_M_ID)
    /**商户ID*/
    public String merchantId;
    
    /**订单状态*/
    @DatabaseField(columnName = OrderTable.COLUMN_STATE)
    public String state;
    
    /**订单 产品*/
    @DatabaseField(columnName = OrderTable.COLUMN_PRODUCT)
    public String productName;
    
    /**订单 金额*/
    @DatabaseField(columnName = OrderTable.COLUMN_MONEY)
    public long money;
    
    @DatabaseField(columnName = OrderTable.COLUMN_REMARK)
    public String remark;
    
    /**添加时间*/
    @DatabaseField(columnName = OrderTable.COLUMN_ADD_TIME)
    public long add_time;
    
    /**完成订单时间*/
    @DatabaseField(columnName = OrderTable.COLUMN_FINISH_TIME)
    public long finishTime;
    
    @DatabaseField(columnName = OrderTable.COLUMN_NOTIFY_TIME)
    public long notify_time;
    
    @DatabaseField(columnName = OrderTable.COLUMN_CLIENT_TYPE)
    public int clientType;
    
    @DatabaseField(columnName = OrderTable.COLUMN_USER_NAME)
    public String userName;
    
    @DatabaseField(columnName = OrderTable.COLUMN_BANK_NAME)
    public String bankTypeDisplay;
    
    @DatabaseField(columnName = OrderTable.COLUMN_TRANSACTIONID)
    public String transactionId;
    
    //交易状类型  pos交易 wx交易 优惠券wx支付
    public int transactionType;
    
    public String notifyTime;
    
    public String addTime;
    
    private long uid = 0;
    
    private String operno;
    
    private Integer canAffim;//是否可以核销，1，可以，0不可以
    
    private String apiCode;
    
    /**
     * @return 返回 apiCode
     */
    public String getApiCode()
    {
        return apiCode;
    }
    
    /**
     * @param 对apiCode进行赋值
     */
    public void setApiCode(String apiCode)
    {
        this.apiCode = apiCode;
    }
    
    /**
     * @return 返回 canAffim
     */
    public Integer getCanAffim()
    {
        return canAffim;
    }
    
    /**
     * @param 对canAffim进行赋值
     */
    public void setCanAffim(Integer canAffim)
    {
        this.canAffim = canAffim;
    }
    
    /**
     * @return 返回 operno
     */
    public String getOperno()
    {
        return operno;
    }
    
    /**
     * @param 对operno进行赋值
     */
    public void setOperno(String operno)
    {
        this.operno = operno;
    }
    
    /**
     * @return 返回 uid
     */
    public long getUid()
    {
        return uid;
    }
    
    /**
     * @param 对uid进行赋值
     */
    public void setUid(long uid)
    {
        this.uid = uid;
    }
    
    public Integer tradeMode;
    
    public Integer tradeState;
    
    public String tradeStateText;
    
    public String payInfo;
    
    public String partner;
    
    public String bankType;
    
    public String bankBillno;
    
    public Long totalFee;
    
    public Integer feeType;
    
    public String notifyId;
    
    public String outTradeNo;
    
    public String timeEnd;
    
    public Integer transportFee;
    
    public Integer productFee;
    
    public String buyerAlias;
    
    public String merchant;
    
    public String orderState;
    
    public String useId;//优惠券ID
    
    public String tradeTimeNew;
    
    private Integer apiProvider;
    
    private String providerName;
    
    private Map<String, String> map;
    
    private String addTimeNew;
    
    private String refundTimeNew;
    
    private String formartYYMM;
    
    private String useTimeNew;
    
    private Integer reqFeqTime;
    
    /**
     * @return 返回 reqFeqTime
     */
    public Integer getReqFeqTime()
    {
        return reqFeqTime;
    }
    
    /**
     * @param 对reqFeqTime进行赋值
     */
    public void setReqFeqTime(Integer reqFeqTime)
    {
        this.reqFeqTime = reqFeqTime;
    }
    
    /**
     * @return 返回 useTimeNew
     */
    public String getUseTimeNew()
    {
        return useTimeNew;
    }
    
    /**
     * @param 对useTimeNew进行赋值
     */
    public void setUseTimeNew(String useTimeNew)
    {
        this.useTimeNew = useTimeNew;
    }
    
    /**
     * @return 返回 formartYYMM
     */
    public String getFormartYYMM()
    {
        return formartYYMM;
    }
    
    /**
     * @param 对formartYYMM进行赋值
     */
    public void setFormartYYMM(String formartYYMM)
    {
        this.formartYYMM = formartYYMM;
    }
    
    /**
     * @return 返回 refundTimeNew
     */
    public String getRefundTimeNew()
    {
        return refundTimeNew;
    }
    
    /**
     * @param 对refundTimeNew进行赋值
     */
    public void setRefundTimeNew(String refundTimeNew)
    {
        this.refundTimeNew = refundTimeNew;
    }
    
    /**
     * @return 返回 addTimeNew
     */
    public String getAddTimeNew()
    {
        return addTimeNew;
    }
    
    /**
     * @param 对addTimeNew进行赋值
     */
    public void setAddTimeNew(String addTimeNew)
    {
        //        if (!StringUtil.isEmptyOrNull(addTimeNew))
        //        {
        //            try
        //            {
        //                setFormatRefund(DateUtil.formartDateYYMMDD(addTimeNew));
        //                setFormartYYMM(DateUtil.formartDateYYMMDDTo(addTimeNew));
        //            }
        //            catch (ParseException e)
        //            {
        //                // TODO Auto-generated catch block
        //                e.printStackTrace();
        //            }
        //        }
        
        this.addTimeNew = addTimeNew;
    }
    
    /**
     * @return 返回 map
     */
    public Map<String, String> getMap()
    {
        return map;
    }
    
    /**
     * @param 对map进行赋值
     */
    public void setMap(Map<String, String> map)
    {
        this.map = map;
    }
    
    /**
     * @return 返回 providerName
     */
    public String getProviderName()
    {
        return providerName;
    }
    
    /**
     * @param 对providerName进行赋值
     */
    public void setProviderName(String providerName)
    {
        this.providerName = providerName;
    }
    
    /**
     * @return 返回 apiProvider
     */
    public Integer getApiProvider()
    {
        return apiProvider;
    }
    
    /**
     * @param 对apiProvider进行赋值
     */
    public void setApiProvider(Integer apiProvider)
    {
        this.apiProvider = apiProvider;
    }
    
    /**
     * @return 返回 tradeTimeNew
     */
    public String getTradeTimeNew()
    {
        return tradeTimeNew;
    }
    
    /**
     * @param 对tradeTimeNew进行赋值
     */
    public void setTradeTimeNew(String tradeTimeNew)
    {
        //        if (!StringUtil.isEmptyOrNull(tradeTimeNew))
        //        {
        //            try
        //            {
        //                setFormatTimePay(DateUtil.formartDateYYMMDD(tradeTimeNew));
        //                
        //                setFormartYYMM(DateUtil.formartDateYYMMDDTo(tradeTimeNew));
        //            }
        //            catch (ParseException e)
        //            {
        //                // TODO Auto-generated catch block
        //                e.printStackTrace();
        //            }
        //        }
        this.tradeTimeNew = tradeTimeNew;
    }
    
    //临时字段
    public String qrCodeImgUrl;//优惠券领取二维码地址
    
    public String coupMoney;//优惠券金额
    
    public String issueyNum;//赠送张数
    
    public String bankCardName; //银行类型
    
    public String wxUserName;//交易后的用户名
    
    public String bankName; // 银行类型
    
    private boolean isMark; // 标示
    
    private String termno;// 终端编号
    
    private String mchNo;
    
    private String refundTime;
    
    private long refundMoney;
    
    private long rfMoneyIng; // 退款中的money
    
    private int affirm; // 确认订单
    
    private String cardCode; // 卡券序列号
    
    private String status; // 卡券核销状态
    
    private String nickName;
    
    private long createTime;
    
    private Integer useStatus;
    
    private long useTime;
    
    private String vcardUseTime;
    
    private long daMoney; // 优惠金额
    
    private List<WxCard> wxCardList;
    
    private WxCard wxCard;
    
    private Integer isAgainPay;
    
    private String cardType;// 卡券类型 (groupon:团购劵；cash:代金劵；discount:折扣劵；gift:礼品劵)
    
    private String gift;// 礼品名称
    
    private Integer discount; // 折扣额度
    
    private String attach;
    
    private long refundFeel;
    
    private int canAffirm;
    
    private String formatTimePay;
    
    private String formatRefund;
    
    private String fromatCard;
    
    private Integer pageCount = 0;//页数
    
    private long cashFeel;//折算人民币
    
    private boolean isPay = false;
    
    /**
     * @return 返回 isPay
     */
    public boolean isPay()
    {
        return isPay;
    }
    
    /**
     * @param 对isPay进行赋值
     */
    public void setPay(boolean isPay)
    {
        this.isPay = isPay;
    }
    
    private String printInfo;
    
    /**
     * @return 返回 printInfo
     */
    public String getPrintInfo()
    {
        return printInfo;
    }
    
    /**
     * @param 对printInfo进行赋值
     */
    public void setPrintInfo(String printInfo)
    {
        this.printInfo = printInfo;
    }
    
    /**
     * @return 返回 cashFeel
     */
    public long getCashFeel()
    {
        return cashFeel;
    }
    
    /**
     * @param 对cashFeel进行赋值
     */
    public void setCashFeel(long cashFeel)
    {
        this.cashFeel = cashFeel;
    }
    
    /**
     * @return 返回 pageCount
     */
    public Integer getPageCount()
    {
        return pageCount;
    }
    
    /**
     * @param 对pageCount进行赋值
     */
    public void setPageCount(Integer pageCount)
    {
        this.pageCount = pageCount;
    }
    
    /**
     * @return 返回 fromatCard
     */
    public String getFromatCard()
    {
        return fromatCard;
    }
    
    /**
     * @param 对fromatCard进行赋值
     */
    public void setFromatCard(String fromatCard)
    {
        this.fromatCard = fromatCard;
    }
    
    /**
     * @return 返回 formatRefund
     */
    public String getFormatRefund()
    {
        return formatRefund;
    }
    
    /**
     * @param 对formatRefund进行赋值
     */
    public void setFormatRefund(String formatRefund)
    {
        this.formatRefund = formatRefund;
    }
    
    /**
     * @return 返回 formatTimePay
     */
    public String getFormatTimePay()
    {
        return formatTimePay;
    }
    
    /**
     * @param 对formatTimePay进行赋值
     */
    public void setFormatTimePay(String formatTimePay)
    {
        this.formatTimePay = formatTimePay;
    }
    
    /**
     * @return 返回 canAffirm
     */
    public int getCanAffirm()
    {
        return canAffirm;
    }
    
    /**
     * @param 对canAffirm进行赋值
     */
    public void setCanAffirm(int canAffirm)
    {
        this.canAffirm = canAffirm;
    }
    
    /**
     * @return 返回 refundFeel
     */
    public long getRefundFeel()
    {
        return refundFeel;
    }
    
    /**
     * @param 对refundFeel进行赋值
     */
    public void setRefundFeel(long refundFeel)
    {
        this.refundFeel = refundFeel;
    }
    
    /**
     * @return 返回 cardType
     */
    public String getCardType()
    {
        return cardType;
    }
    
    /**
     * @param 对cardType进行赋值
     */
    public void setCardType(String cardType)
    {
        this.cardType = cardType;
    }
    
    /**
     * @return 返回 gift
     */
    public String getGift()
    {
        return gift;
    }
    
    /**
     * @param 对gift进行赋值
     */
    public void setGift(String gift)
    {
        this.gift = gift;
    }
    
    /**
     * @return 返回 reduceCost
     */
    public Integer getReduceCost()
    {
        return reduceCost;
    }
    
    /**
     * @param 对reduceCost进行赋值
     */
    public void setReduceCost(Integer reduceCost)
    {
        this.reduceCost = reduceCost;
    }
    
    private Integer reduceCost;// 减免金额(分)
    
    /**
     * @return 返回 isAgainPay
     */
    public Integer getIsAgainPay()
    {
        return isAgainPay;
    }
    
    /**
     * @param 对isAgainPay进行赋值
     */
    public void setIsAgainPay(Integer isAgainPay)
    {
        this.isAgainPay = isAgainPay;
    }
    
    /**
     * @return 返回 wxCard
     */
    public WxCard getWxCard()
    {
        return wxCard;
    }
    
    /**
     * @param 对wxCard进行赋值
     */
    public void setWxCard(WxCard wxCard)
    {
        this.wxCard = wxCard;
    }
    
    /**
     * @return 返回 daMoney
     */
    public long getDaMoney()
    {
        return daMoney;
    }
    
    /**
     * @return 返回 wxCardList
     */
    public List<WxCard> getWxCardList()
    {
        return wxCardList;
    }
    
    /**
     * @param 对wxCardList进行赋值
     */
    public void setWxCardList(List<WxCard> wxCardList)
    {
        this.wxCardList = wxCardList;
    }
    
    /**
     * @param 对daMoney进行赋值
     */
    public void setDaMoney(long daMoney)
    {
        this.daMoney = daMoney;
    }
    
    /**
     * @return 返回 cardCode
     */
    public String getCardCode()
    {
        return cardCode;
    }
    
    /**
     * @param 对cardCode进行赋值
     */
    public void setCardCode(String cardCode)
    {
        this.cardCode = cardCode;
    }
    
    /**
     * @return 返回 status
     */
    public String getStatus()
    {
        return status;
    }
    
    /**
     * @param 对status进行赋值
     */
    public void setStatus(String status)
    {
        this.status = status;
    }
    
    /**
     * @return 返回 nickName
     */
    public String getNickName()
    {
        return nickName;
    }
    
    /**
     * @param 对nickName进行赋值
     */
    public void setNickName(String nickName)
    {
        this.nickName = nickName;
    }
    
    /**
     * @return 返回 createTime
     */
    public long getCreateTime()
    {
        return createTime;
    }
    
    /**
     * @param 对createTime进行赋值
     */
    public void setCreateTime(long createTime)
    {
        this.createTime = createTime;
    }
    
    /**
     * @return 返回 useStatus
     */
    public Integer getUseStatus()
    {
        return useStatus;
    }
    
    /**
     * @param 对useStatus进行赋值
     */
    public void setUseStatus(Integer useStatus)
    {
        this.useStatus = useStatus;
    }
    
    /**
     * @return 返回 useTime
     */
    public long getUseTime()
    {
        return useTime;
    }
    
    /**
     * @param 对useTime进行赋值
     */
    public void setUseTime(long useTime)
    {
        this.useTime = useTime;
    }
    
    /**
     * @return 返回 vcardUseTime
     */
    public String getVcardUseTime()
    {
        return vcardUseTime;
    }
    
    /**
     * @param 对vcardUseTime进行赋值
     */
    public void setVcardUseTime(String vcardUseTime)
    {
        this.vcardUseTime = vcardUseTime;
    }
    
    /**
     * @return 返回 brandName
     */
    public String getBrandName()
    {
        return brandName;
    }
    
    /**
     * @param 对brandName进行赋值
     */
    public void setBrandName(String brandName)
    {
        this.brandName = brandName;
    }
    
    private String brandName;
    
    private String dealDetail;
    
    //    public String tradeName;
    
    /**
     * @return 返回 affirm
     */
    public int getAffirm()
    {
        return affirm;
    }
    
    /**
     * @return 返回 dealDetail
     */
    public String getDealDetail()
    {
        return dealDetail;
    }
    
    /**
     * @param 对dealDetail进行赋值
     */
    public void setDealDetail(String dealDetail)
    {
        this.dealDetail = dealDetail;
    }
    
    /**
     * @param 对affirm进行赋值
     */
    public void setAffirm(int affirm)
    {
        this.affirm = affirm;
    }
    
    /**
     * @return 返回 rfMoneyIng
     */
    public long getRfMoneyIng()
    {
        return rfMoneyIng;
    }
    
    /**
     * @param 对rfMoneyIng进行赋值
     */
    public void setRfMoneyIng(long rfMoneyIng)
    {
        this.rfMoneyIng = rfMoneyIng;
    }
    
    public long getRefundMoney()
    {
        return refundMoney;
    }
    
    public void setRefundMoney(long refundMoney)
    {
        this.refundMoney = refundMoney;
    }
    
    /**
     * @return 返回 refundTime
     */
    public String getRefundTime()
    {
        return refundTime;
    }
    
    /**
     * @param 对refundTime进行赋值
     */
    public void setRefundTime(String refundTime)
    {
        this.refundTime = refundTime;
    }
    
    /**
     * @return 返回 termno
     */
    public String getTermno()
    {
        return termno;
    }
    
    /**
     * @param 对termno进行赋值
     */
    public void setTermno(String termno)
    {
        this.termno = termno;
    }
    
    /**
     * @return 返回 mchNo
     */
    public String getMchNo()
    {
        return mchNo;
    }
    
    /**
     * @param 对mchNo进行赋值
     */
    public void setMchNo(String mchNo)
    {
        this.mchNo = mchNo;
    }
    
    /**
     * @return 返回 isMark
     */
    public boolean isMark()
    {
        return isMark;
    }
    
    /**
     * @param 对isMark进行赋值
     */
    public void setMark(boolean isMark)
    {
        this.isMark = isMark;
    }
    
    /**是否本地生成订单*/
    @DatabaseField(columnName = OrderTable.COLUMN_CREATE_ORDER_TYPE)
    public int createOrderType;
    
    public int payType = PayType.PAY_WX.getValue();//交易类型默认微信m
    
    public String posCardNo;//pos交易有卡号
    
    private String mchId;
    
    private String mchName;
    
    private Integer userId;
    
    private String centerId;
    
    private String tradeTime;
    
    private Boolean notifyState;
    
    private String tradeType;
    
    private String body;
    
    private String orderNoMch;
    
    private String code; // 卡券编号
    
    private String title;//卡券主题
    
    private String openid;
    
    private String client; // 终端类型
    
    private Integer rufundMark; // 退款标志，1代表可以退款，0代表不能退款
    
    private String outRefundNo;
    
    private String cardId;
    
    private long totalRows;
    
    private String description; // 使用说明
    
    /**
     * @return 返回 description
     */
    public String getDescription()
    {
        return description;
    }
    
    /**
     * @param 对description进行赋值
     */
    public void setDescription(String description)
    {
        this.description = description;
    }
    
    /**
     * @return 返回 totalRows
     */
    public long getTotalRows()
    {
        return totalRows;
    }
    
    /**
     * @param 对totalRows进行赋值
     */
    public void setTotalRows(long totalRows)
    {
        this.totalRows = totalRows;
    }
    
    /**
     * @return 返回 cardId
     */
    public String getCardId()
    {
        return cardId;
    }
    
    /**
     * @param 对cardId进行赋值
     */
    public void setCardId(String cardId)
    {
        this.cardId = cardId;
    }
    
    /**
     * @return 返回 outRefundNo
     */
    public String getOutRefundNo()
    {
        return outRefundNo;
    }
    
    /**
     * @param 对outRefundNo进行赋值
     */
    public void setOutRefundNo(String outRefundNo)
    {
        this.outRefundNo = outRefundNo;
    }
    
    /**
     * @return 返回 rufundMark
     */
    public Integer getRufundMark()
    {
        return rufundMark;
    }
    
    /**
     * @param 对rufundMark进行赋值
     */
    public void setRufundMark(Integer rufundMark)
    {
        this.rufundMark = rufundMark;
    }
    
    /**
     * @return 返回 client
     */
    public String getClient()
    {
        return client;
    }
    
    /**
     * @param 对client进行赋值
     */
    public void setClient(String client)
    {
        this.client = client;
    }
    
    /**
     * @return 返回 openid
     */
    public String getOpenid()
    {
        return openid;
    }
    
    /**
     * @param 对openid进行赋值
     */
    public void setOpenid(String openid)
    {
        this.openid = openid;
    }
    
    /**
     * @return 返回 title
     */
    public String getTitle()
    {
        return title;
    }
    
    /**
     * @param 对title进行赋值
     */
    public void setTitle(String title)
    {
        this.title = title;
    }
    
    /**
     * @return 返回 code
     */
    public String getCode()
    {
        return code;
    }
    
    /**
     * @param 对code进行赋值
     */
    public void setCode(String code)
    {
        this.code = code;
    }
    
    /**
     * @return 返回 orderNoMch
     */
    public String getOrderNoMch()
    {
        return orderNoMch;
    }
    
    /**
     * @param 对orderNoMch进行赋值
     */
    public void setOrderNoMch(String orderNoMch)
    {
        this.orderNoMch = orderNoMch;
    }
    
    /** {@inheritDoc} */
    
    @Override
    public String toString()
    {
        return "Order [orderNoMch=" + orderNoMch + "]";
    }
    
    /**
     * @return 返回 body
     */
    public String getBody()
    {
        return body;
    }
    
    /**
     * @param 对body进行赋值
     */
    public void setBody(String body)
    {
        this.body = body;
    }
    
    /**
     * @return 返回 refundNo
     */
    public String getRefundNo()
    {
        return refundNo;
    }
    
    /**
     * @param 对refundNo进行赋值
     */
    public void setRefundNo(String refundNo)
    {
        this.refundNo = refundNo;
    }
    
    private int refundState;
    
    private String refundNo;
    
    /**
     * @return 返回 id
     */
    public long getId()
    {
        return id;
    }
    
    /**
     * @param 对id进行赋值
     */
    public void setId(long id)
    {
        this.id = id;
    }
    
    /**
     * @return 返回 orderNo
     */
    public String getOrderNo()
    {
        return orderNo;
    }
    
    /**
     * @param 对orderNo进行赋值
     */
    public void setOrderNo(String orderNo)
    {
        this.orderNo = orderNo;
    }
    
    /**
     * @return 返回 employeeId
     */
    public long getEmployeeId()
    {
        return employeeId;
    }
    
    /**
     * @param 对employeeId进行赋值
     */
    public void setEmployeeId(long employeeId)
    {
        this.employeeId = employeeId;
    }
    
    /**
     * @return 返回 merchantId
     */
    public String getMerchantId()
    {
        return merchantId;
    }
    
    /**
     * @param 对merchantId进行赋值
     */
    public void setMerchantId(String merchantId)
    {
        this.merchantId = merchantId;
    }
    
    /**
     * @return 返回 state
     */
    public String getState()
    {
        return state;
    }
    
    /**
     * @param 对state进行赋值
     */
    public void setState(String state)
    {
        this.state = state;
    }
    
    /**
     * @return 返回 productName
     */
    public String getProductName()
    {
        return productName;
    }
    
    /**
     * @param 对productName进行赋值
     */
    public void setProductName(String productName)
    {
        this.productName = productName;
    }
    
    /**
     * @return 返回 remark
     */
    public String getRemark()
    {
        return remark;
    }
    
    /**
     * @param 对remark进行赋值
     */
    public void setRemark(String remark)
    {
        this.remark = remark;
    }
    
    /**
     * @return 返回 add_time
     */
    public long getAdd_time()
    {
        return add_time;
    }
    
    /**
     * @param 对add_time进行赋值
     */
    public void setAdd_time(long add_time)
    {
        this.add_time = add_time;
    }
    
    /**
     * @return 返回 finishTime
     */
    public long getFinishTime()
    {
        return finishTime;
    }
    
    /**
     * @param 对finishTime进行赋值
     */
    public void setFinishTime(long finishTime)
    {
        this.finishTime = finishTime;
    }
    
    /**
     * @return 返回 notify_time
     */
    public long getNotify_time()
    {
        return notify_time;
    }
    
    /**
     * @param 对notify_time进行赋值
     */
    public void setNotify_time(long notify_time)
    {
        this.notify_time = notify_time;
    }
    
    /**
     * @return 返回 clientType
     */
    public int getClientType()
    {
        return clientType;
    }
    
    /**
     * @param 对clientType进行赋值
     */
    public void setClientType(int clientType)
    {
        this.clientType = clientType;
    }
    
    /**
     * @return 返回 userName
     */
    public String getUserName()
    {
        return userName;
    }
    
    /**
     * @param 对userName进行赋值
     */
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    /**
     * @return 返回 bankTypeDisplay
     */
    public String getBankTypeDisplay()
    {
        return bankTypeDisplay;
    }
    
    /**
     * @param 对bankTypeDisplay进行赋值
     */
    public void setBankTypeDisplay(String bankTypeDisplay)
    {
        this.bankTypeDisplay = bankTypeDisplay;
    }
    
    /**
     * @return 返回 transactionId
     */
    public String getTransactionId()
    {
        return transactionId;
    }
    
    /**
     * @param 对transactionId进行赋值
     */
    public void setTransactionId(String transactionId)
    {
        this.transactionId = transactionId;
    }
    
    /**
     * @return 返回 transactionType
     */
    public int getTransactionType()
    {
        return transactionType;
    }
    
    /**
     * @param 对transactionType进行赋值
     */
    public void setTransactionType(int transactionType)
    {
        this.transactionType = transactionType;
    }
    
    /**
     * @return 返回 notifyTime
     */
    public String getNotifyTime()
    {
        return notifyTime;
    }
    
    /**
     * @param 对notifyTime进行赋值
     */
    public void setNotifyTime(String notifyTime)
    {
        this.notifyTime = notifyTime;
    }
    
    /**
     * @return 返回 addTime
     */
    public String getAddTime()
    {
        return addTime;
    }
    
    /**
     * @param 对addTime进行赋值
     */
    public void setAddTime(String addTime)
    {
        this.addTime = addTime;
    }
    
    /**
     * @return 返回 tradeMode
     */
    public Integer getTradeMode()
    {
        return tradeMode;
    }
    
    /**
     * @param 对tradeMode进行赋值
     */
    public void setTradeMode(Integer tradeMode)
    {
        this.tradeMode = tradeMode;
    }
    
    /**
     * @return 返回 tradeState
     */
    public Integer getTradeState()
    {
        return tradeState;
    }
    
    /**
     * @param 对tradeState进行赋值
     */
    public void setTradeState(Integer tradeState)
    {
        this.tradeState = tradeState;
    }
    
    /**
     * @return 返回 payInfo
     */
    public String getPayInfo()
    {
        return payInfo;
    }
    
    /**
     * @param 对payInfo进行赋值
     */
    public void setPayInfo(String payInfo)
    {
        this.payInfo = payInfo;
    }
    
    /**
     * @return 返回 partner
     */
    public String getPartner()
    {
        return partner;
    }
    
    /**
     * @param 对partner进行赋值
     */
    public void setPartner(String partner)
    {
        this.partner = partner;
    }
    
    /**
     * @return 返回 bankType
     */
    public String getBankType()
    {
        return bankType;
    }
    
    /**
     * @param 对bankType进行赋值
     */
    public void setBankType(String bankType)
    {
        this.bankType = bankType;
    }
    
    /**
     * @return 返回 bankBillno
     */
    public String getBankBillno()
    {
        return bankBillno;
    }
    
    /**
     * @param 对bankBillno进行赋值
     */
    public void setBankBillno(String bankBillno)
    {
        this.bankBillno = bankBillno;
    }
    
    /**
     * @return 返回 totalFee
     */
    public Long getTotalFee()
    {
        return totalFee;
    }
    
    /**
     * @param 对totalFee进行赋值
     */
    public void setTotalFee(Long totalFee)
    {
        this.totalFee = totalFee;
    }
    
    /**
     * @return 返回 feeType
     */
    public Integer getFeeType()
    {
        return feeType;
    }
    
    /**
     * @param 对feeType进行赋值
     */
    public void setFeeType(Integer feeType)
    {
        this.feeType = feeType;
    }
    
    /**
     * @return 返回 notifyId
     */
    public String getNotifyId()
    {
        return notifyId;
    }
    
    /**
     * @param 对notifyId进行赋值
     */
    public void setNotifyId(String notifyId)
    {
        this.notifyId = notifyId;
    }
    
    /**
     * @return 返回 outTradeNo
     */
    public String getOutTradeNo()
    {
        return outTradeNo;
    }
    
    /**
     * @param 对outTradeNo进行赋值
     */
    public void setOutTradeNo(String outTradeNo)
    {
        this.outTradeNo = outTradeNo;
    }
    
    /**
     * @return 返回 attach
     */
    public String getAttach()
    {
        return attach;
    }
    
    /**
     * @param 对attach进行赋值
     */
    public void setAttach(String attach)
    {
        this.attach = attach;
    }
    
    /**
     * @return 返回 timeEnd
     */
    public String getTimeEnd()
    {
        return timeEnd;
    }
    
    /**
     * @param 对timeEnd进行赋值
     */
    public void setTimeEnd(String timeEnd)
    {
        this.timeEnd = timeEnd;
    }
    
    /**
     * @return 返回 transportFee
     */
    public Integer getTransportFee()
    {
        return transportFee;
    }
    
    /**
     * @param 对transportFee进行赋值
     */
    public void setTransportFee(Integer transportFee)
    {
        this.transportFee = transportFee;
    }
    
    /**
     * @return 返回 productFee
     */
    public Integer getProductFee()
    {
        return productFee;
    }
    
    /**
     * @param 对productFee进行赋值
     */
    public void setProductFee(Integer productFee)
    {
        this.productFee = productFee;
    }
    
    /**
     * @return 返回 discount
     */
    public Integer getDiscount()
    {
        return discount;
    }
    
    /**
     * @param 对discount进行赋值
     */
    public void setDiscount(Integer discount)
    {
        this.discount = discount;
    }
    
    /**
     * @return 返回 buyerAlias
     */
    public String getBuyerAlias()
    {
        return buyerAlias;
    }
    
    /**
     * @param 对buyerAlias进行赋值
     */
    public void setBuyerAlias(String buyerAlias)
    {
        this.buyerAlias = buyerAlias;
    }
    
    /**
     * @return 返回 merchant
     */
    public String getMerchant()
    {
        return merchant;
    }
    
    /**
     * @param 对merchant进行赋值
     */
    public void setMerchant(String merchant)
    {
        this.merchant = merchant;
    }
    
    /**
     * @return 返回 orderState
     */
    public String getOrderState()
    {
        return orderState;
    }
    
    /**
     * @param 对orderState进行赋值
     */
    public void setOrderState(String orderState)
    {
        this.orderState = orderState;
    }
    
    /**
     * @return 返回 useId
     */
    public String getUseId()
    {
        return useId;
    }
    
    /**
     * @param 对useId进行赋值
     */
    public void setUseId(String useId)
    {
        this.useId = useId;
    }
    
    /**
     * @return 返回 qrCodeImgUrl
     */
    public String getQrCodeImgUrl()
    {
        return qrCodeImgUrl;
    }
    
    /**
     * @param 对qrCodeImgUrl进行赋值
     */
    public void setQrCodeImgUrl(String qrCodeImgUrl)
    {
        this.qrCodeImgUrl = qrCodeImgUrl;
    }
    
    /**
     * @return 返回 coupMoney
     */
    public String getCoupMoney()
    {
        return coupMoney;
    }
    
    /**
     * @param 对coupMoney进行赋值
     */
    public void setCoupMoney(String coupMoney)
    {
        this.coupMoney = coupMoney;
    }
    
    /**
     * @return 返回 issueyNum
     */
    public String getIssueyNum()
    {
        return issueyNum;
    }
    
    /**
     * @param 对issueyNum进行赋值
     */
    public void setIssueyNum(String issueyNum)
    {
        this.issueyNum = issueyNum;
    }
    
    /**
     * @return 返回 bankCardName
     */
    public String getBankCardName()
    {
        return bankCardName;
    }
    
    /**
     * @param 对bankCardName进行赋值
     */
    public void setBankCardName(String bankCardName)
    {
        this.bankCardName = bankCardName;
    }
    
    /**
     * @return 返回 wxUserName
     */
    public String getWxUserName()
    {
        return wxUserName;
    }
    
    /**
     * @param 对wxUserName进行赋值
     */
    public void setWxUserName(String wxUserName)
    {
        this.wxUserName = wxUserName;
    }
    
    /**
     * @return 返回 createOrderType
     */
    public int getCreateOrderType()
    {
        return createOrderType;
    }
    
    /**
     * @param 对createOrderType进行赋值
     */
    public void setCreateOrderType(int createOrderType)
    {
        this.createOrderType = createOrderType;
    }
    
    /**
     * @return 返回 payType
     */
    public int getPayType()
    {
        return payType;
    }
    
    /**
     * @param 对payType进行赋值
     */
    public void setPayType(int payType)
    {
        this.payType = payType;
    }
    
    /**
     * @return 返回 posCardNo
     */
    public String getPosCardNo()
    {
        return posCardNo;
    }
    
    /**
     * @param 对posCardNo进行赋值
     */
    public void setPosCardNo(String posCardNo)
    {
        this.posCardNo = posCardNo;
    }
    
    /**
     * @return 返回 mchId
     */
    public String getMchId()
    {
        return mchId;
    }
    
    /**
     * @param 对mchId进行赋值
     */
    public void setMchId(String mchId)
    {
        this.mchId = mchId;
    }
    
    /**
     * @return 返回 mchName
     */
    public String getMchName()
    {
        return mchName;
    }
    
    /**
     * @param 对mchName进行赋值
     */
    public void setMchName(String mchName)
    {
        this.mchName = mchName;
    }
    
    /**
     * @return 返回 userId
     */
    public Integer getUserId()
    {
        return userId;
    }
    
    /**
     * @param 对userId进行赋值
     */
    public void setUserId(Integer userId)
    {
        this.userId = userId;
    }
    
    /**
     * @return 返回 centerId
     */
    public String getCenterId()
    {
        return centerId;
    }
    
    /**
     * @param 对centerId进行赋值
     */
    public void setCenterId(String centerId)
    {
        this.centerId = centerId;
    }
    
    /**
     * @return 返回 tradeTime
     */
    public String getTradeTime()
    {
        return tradeTime;
    }
    
    /**
     * @param 对tradeTime进行赋值
     */
    public void setTradeTime(String tradeTime)
    {
        this.tradeTime = tradeTime;
    }
    
    /**
     * @return 返回 notifyState
     */
    public Boolean getNotifyState()
    {
        return notifyState;
    }
    
    /**
     * @param 对notifyState进行赋值
     */
    public void setNotifyState(Boolean notifyState)
    {
        this.notifyState = notifyState;
    }
    
    /**
     * @return 返回 tradeType
     */
    public String getTradeType()
    {
        return tradeType;
    }
    
    /**
     * @param 对tradeType进行赋值
     */
    public void setTradeType(String tradeType)
    {
        this.tradeType = tradeType;
    }
    
    /**
     * @return 返回 tradeName
     */
    public String getTradeName()
    {
        return tradeName;
    }
    
    /**
     * @param 对tradeName进行赋值
     */
    public void setTradeName(String tradeName)
    {
        this.tradeName = tradeName;
    }
    
    /**
     * @return 返回 charset
     */
    public String getCharset()
    {
        return charset;
    }
    
    /**
     * @param 对charset进行赋值
     */
    public void setCharset(String charset)
    {
        this.charset = charset;
    }
    
    /**
     * @return 返回 signType
     */
    public String getSignType()
    {
        return signType;
    }
    
    /**
     * @param 对signType进行赋值
     */
    public void setSignType(String signType)
    {
        this.signType = signType;
    }
    
    /**
     * @param 对money进行赋值
     */
    public void setMoney(Long money)
    {
        this.money = money;
    }
    
    /**
     * @param 对bankName进行赋值
     */
    public void setBankName(String bankName)
    {
        this.bankName = bankName;
    }
    
    public String tradeName;
    
    private String charset;
    
    private String signType;
    
    public long getMoney()
    {
        return money;
    }
    
    public String getBankName()
    {
        if (TextUtils.isEmpty(bankCardName))
        {
            return bankTypeDisplay;
        }
        else
        {
            return bankCardName;
        }
    }
    
    public int getRefundState()
    {
        return refundState;
    }
    
    public void setRefundState(int refundState)
    {
        this.refundState = refundState;
    }
    
    public String getTradeStateText()
    {
        return tradeStateText;
    }
    
    public void setTradeStateText(String tradeStateText)
    {
        this.tradeStateText = tradeStateText;
    }


    /** 附加手续费*/
    private Long surcharge=0L;
    /** 预扣税费*/
    private Long withholdingTax=0L;

    public Long getOrderFee() {
        return orderFee;
    }

    public void setOrderFee(Long orderFee) {
        this.orderFee = orderFee;
    }

    //商品金额（单位：分）
    private Long orderFee=0L;
    public Long getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(Long surcharge) {
        this.surcharge = surcharge;
    }

    public Long getWithholdingTax() {
        return withholdingTax;
    }

    public void setWithholdingTax(Long withholdingTax) {
        this.withholdingTax = withholdingTax;
    }

    public Long getTipFee() {
        return tipFee;
    }

    public void setTipFee(Long tipFee) {
        this.tipFee = tipFee;
    }

    /**
     * 小费
     */
    private Long tipFee=0L;


    public long vat;              //增值税（单位：分）
    public long refundFee;       //通道的退款金额（单位：分）

    public long getVat() {
        return vat;
    }

    public void setVat(long vat) {
        this.vat = vat;
    }

    public long getRefundFee() {
        return refundFee;
    }

    public void setRefundFee(long refundFee) {
        this.refundFee = refundFee;
    }
}