package cn.swiftpass.enterprise.partyking.test

import cn.swiftpass.enterprise.partyking.entity.PtkSummaryItemEntity
import cn.swiftpass.enterprise.partyking.entity.VoucherInfoItemEntity

object TestDataUtils {


    /**
     * 核销确认测试数据
     */
    fun getWriteOffConfirmTestData(): ArrayList<VoucherInfoItemEntity> {
        val list = ArrayList<VoucherInfoItemEntity>()
        list.clear()

        val itemV1 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Campaign Name",
            midContent = "Campaign Name"
        )

        val itemV2 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Voucher ID",
            midContent = "ICO-******-AQU6"
        )

        val itemH = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_HORIZONTAL,
            midTitle = "Order status",
            midContent = "Active",
            midContentHorizontalColor = VoucherInfoItemEntity.COLOR_MID_CONTENT_SUCCESS
        )


        val itemV3 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Package ID",
            midContent = "XXXXX"
        )

        val itemV4 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Review Date",
            midContent = "9/17/2021  11:25:30"
        )

        val itemV5 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Operator ID",
            midContent = "XXXXXX"
        )


        val itemB = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_BOTTOM,
            bottomType = VoucherInfoItemEntity.BOTTOM_BLUE_BUTTON,
            bottomButtonText = "Redeem",
            bottomPageType = VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_CONFIRM
        )


        list.add(itemV1)
        list.add(itemV2)
        list.add(itemH)
        list.add(itemV3)
        list.add(itemV4)
        list.add(itemV5)
        list.add(itemB)

        return list
    }


    /**
     * 不可核销测试数据
     */
    fun getCanNotWriteOffTestData(): ArrayList<VoucherInfoItemEntity> {
        val list = ArrayList<VoucherInfoItemEntity>()
        list.clear()


        val itemV1 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Campaign Name",
            midContent = "2020 Event"
        )

        val itemV2 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Voucher ID",
            midContent = "ICO-******-AQU6"
        )

        val itemH = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_HORIZONTAL,
            midTitle = "Order status",
            midContent = "Not Available",
            midContentHorizontalColor = VoucherInfoItemEntity.COLOR_MID_CONTENT_FAIL
        )


        val itemV3 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Package ID",
            midContent = "XXXXX"
        )


        val itemV4 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Review Date",
            midContent = "9/17/2021  11:25:30"
        )


        val itemV5 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Operator ID",
            midContent = "XXXXXX"
        )


        val itemB = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_BOTTOM,
            bottomType = VoucherInfoItemEntity.BOTTOM_BLUE_BUTTON,
            bottomButtonText = "Redeem",
            bottomPageType = VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_CAN_NOT_WRITE_OFF
        )


        list.add(itemV1)
        list.add(itemV2)
        list.add(itemH)
        list.add(itemV3)
        list.add(itemV4)
        list.add(itemV5)
        list.add(itemB)

        return list
    }


    /**
     * 核销成功测试数据
     */
    fun getWriteOffSuccessTestData(): ArrayList<VoucherInfoItemEntity> {
        val list = ArrayList<VoucherInfoItemEntity>()
        list.clear()

        val itemTop = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_TOP,
            topTitle = "Redemption Succeed",
            topIconType = VoucherInfoItemEntity.TOP_ICON_SUCCESS
        )


        val itemContentV1 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Campaign Name",
            midContent = "2022积分兑换活动"
        )

        val itemContentV2 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Voucher ID",
            midContent = "ICO-******-AQU6"
        )

        val itemContentV3 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Redemption Time",
            midContent = "25/05/2022  17:15:31"
        )

        val itemBottom = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_BOTTOM,
            bottomType = VoucherInfoItemEntity.BOTTOM_WHITE_BUTTON_AND_GUIDE,
            bottomGuideText = "Redemption Record >",
            bottomButtonText = "Finish",
            bottomPageType = VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_WRITE_OFF_SUCCESS
        )

        list.add(itemTop)
        list.add(itemContentV1)
        list.add(itemContentV2)
        list.add(itemContentV3)
        list.add(itemBottom)

        return list
    }


    /**
     * 核销失败测试数据
     */
    fun getWriteOffFailTestData(): ArrayList<VoucherInfoItemEntity> {
        val list = ArrayList<VoucherInfoItemEntity>()
        list.clear()


        val itemTop = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_TOP,
            topTitle = "Redemption Failed",
            topIconType = VoucherInfoItemEntity.TOP_ICON_FAIL
        )

        val itemV1 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Campaign Name",
            midContent = "2020 B Event"
        )

        val itemV2 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Voucher ID",
            midContent = "ICO-******-AQU6"
        )

        val itemV3 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Error Code",
            midContent = "201"
        )

        val itemV4 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Redemption Time",
            midContent = "Redemption Time"
        )

        val itemB = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_BOTTOM,
            bottomType = VoucherInfoItemEntity.BOTTOM_WHITE_BUTTON_AND_GUIDE,
            bottomGuideText = "Redemption Record >",
            bottomButtonText = "Finish",
            bottomPageType = VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_WRITE_OFF_FAIL
        )

        list.add(itemTop)
        list.add(itemV1)
        list.add(itemV2)
        list.add(itemV3)
        list.add(itemV4)
        list.add(itemB)
        return list
    }


    /**
     * 核销超时测试数据
     */
    fun getWriteOffTimeoutTestData(): ArrayList<VoucherInfoItemEntity> {
        val list = ArrayList<VoucherInfoItemEntity>()
        list.clear()

        val itemTop = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_TOP,
            topTitle = "Timeout",
            topIconType = VoucherInfoItemEntity.TOP_ICON_TIMEOUT
        )

        val itemV1 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Campaign Name",
            midContent = "2020 B Event"
        )

        val itemV2 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Voucher ID",
            midContent = "ICO-******-AQU6"
        )


        val itemB = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_BOTTOM,
            bottomType = VoucherInfoItemEntity.BOTTOM_BLUE_BUTTON_AND_GUIDE,
            bottomGuideText = "Redemption Record >",
            bottomButtonText = "Retry",
            bottomPageType = VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_WRITE_OFF_TIMEOUT
        )
        list.add(itemTop)
        list.add(itemV1)
        list.add(itemV2)
        list.add(itemB)
        return list
    }


    /**
     * 撤回成功测试数据
     */
    fun getRollbackSuccessTestData(): ArrayList<VoucherInfoItemEntity> {
        val list = ArrayList<VoucherInfoItemEntity>()
        list.clear()


        val itemTop = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_TOP,
            topTitle = "Redemption Rollbacked",
            topIconType = VoucherInfoItemEntity.TOP_ICON_ROLLBACK
        )

        val itemV1 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Campaign Name",
            midContent = "2020 B Event"
        )

        val itemV2 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Voucher ID",
            midContent = "ICO-******-AQU6"
        )

        val itemV3 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Rollback Time",
            midContent = "9/17/2021  11:25:30"
        )


        val itemB = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_BOTTOM,
            bottomType = VoucherInfoItemEntity.BOTTOM_WHITE_BUTTON_AND_GUIDE,
            bottomGuideText = "Redemption Record >",
            bottomButtonText = "Finish",
            bottomPageType = VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_ROLLBACK_SUCCESS
        )

        list.add(itemTop)
        list.add(itemV1)
        list.add(itemV2)
        list.add(itemV3)
        list.add(itemB)

        return list
    }


    /**
     * 汇总测试数据
     */
    fun getPtkSummaryTestData(): ArrayList<PtkSummaryItemEntity> {
        val list = ArrayList<PtkSummaryItemEntity>()
        list.clear()


        val itemSuccess = PtkSummaryItemEntity(
            title = "2022 S Event",
            time = "26/05/2022  17:36:42",
            status = "Succeed",
            statusType = PtkSummaryItemEntity.STATUS_TYPE_SUCCEED
        )

        val itemFailed = PtkSummaryItemEntity(
            title = "2022 F Event",
            time = "11/04/2022  13:23:56",
            status = "Failed",
            statusType = PtkSummaryItemEntity.STATUS_TYPE_FAILED
        )

        val itemTimeout = PtkSummaryItemEntity(
            title = "2021 T Event",
            time = "07/11/2021  09:12:54",
            status = "Timeout",
            statusType = PtkSummaryItemEntity.STATUS_TYPE_TIMEOUT
        )

        val itemRollback = PtkSummaryItemEntity(
            title = "2020 R Event",
            time = "17/01/2020  12:46:19",
            status = "Rollbacked",
            statusType = PtkSummaryItemEntity.STATUS_TYPE_ROLLBACK
        )

        list.add(itemSuccess)
        list.add(itemFailed)
        list.add(itemTimeout)
        list.add(itemRollback)

        list.add(itemSuccess)
        list.add(itemFailed)
        list.add(itemTimeout)
        list.add(itemRollback)

        list.add(itemSuccess)
        list.add(itemFailed)
        list.add(itemTimeout)
        list.add(itemRollback)
        return list
    }


    /**
     * 核销记录成功测试数据
     */
    fun getWriteOffRecordSuccessTestData(): ArrayList<VoucherInfoItemEntity> {
        val list = ArrayList<VoucherInfoItemEntity>()
        list.clear()


        val itemV1 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Campaign Name",
            midContent = "2020 B Event"
        )

        val itemH = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_HORIZONTAL,
            midTitle = "Order status",
            midContent = "Succeed",
            midContentHorizontalColor = VoucherInfoItemEntity.COLOR_MID_CONTENT_SUCCESS
        )


        val itemV2 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Voucher ID",
            midContent = "ICO-******-AQU1"
        )


        val itemV3 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Redemption Time",
            midContent = "9/17/2021  11:25:30"
        )


        list.add(itemV1)
        list.add(itemH)
        list.add(itemV2)
        list.add(itemV3)
        return list
    }


    /**
     * 核销记录失败测试数据
     */
    fun getWriteOffRecordFailTestData(): ArrayList<VoucherInfoItemEntity> {
        val list = ArrayList<VoucherInfoItemEntity>()
        list.clear()


        val itemV1 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Campaign Name",
            midContent = "2020 B Event"
        )

        val itemH = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_HORIZONTAL,
            midTitle = "Order status",
            midContent = "Failed",
            midContentHorizontalColor = VoucherInfoItemEntity.COLOR_MID_CONTENT_FAIL
        )


        val itemV2 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Voucher ID",
            midContent = "ICO-******-AQU2"
        )


        val itemV3 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Error Code",
            midContent = "201"
        )


        val itemV4 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Redemption Time",
            midContent = "9/17/2021  11:25:30"
        )


        list.add(itemV1)
        list.add(itemH)
        list.add(itemV2)
        list.add(itemV3)
        list.add(itemV4)
        return list
    }


    /**
     * 核销记录超时测试数据
     */
    fun getWriteOffRecordTimeoutTestData(): ArrayList<VoucherInfoItemEntity> {
        val list = ArrayList<VoucherInfoItemEntity>()
        list.clear()


        val itemV1 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Campaign Name",
            midContent = "2020 B Event"
        )

        val itemH = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_HORIZONTAL,
            midTitle = "Order status",
            midContent = "Failed",
            midContentHorizontalColor = VoucherInfoItemEntity.COLOR_MID_CONTENT_TIMEOUT
        )


        val itemV2 = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL,
            midTitle = "Voucher ID",
            midContent = "ICO-******-AQU3"
        )


        val itemB = VoucherInfoItemEntity(
            type = VoucherInfoItemEntity.ITEM_TYPE_BOTTOM,
            bottomType = VoucherInfoItemEntity.BOTTOM_BLUE_BUTTON,
            bottomButtonText = "Update",
            bottomPageType = VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_RECORD_TIMEOUT
        )

        list.add(itemV1)
        list.add(itemH)
        list.add(itemV2)
        list.add(itemB)
        return list
    }


}