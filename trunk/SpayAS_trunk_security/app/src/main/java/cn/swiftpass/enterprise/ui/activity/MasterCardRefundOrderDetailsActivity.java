package cn.swiftpass.enterprise.ui.activity;

import static cn.swiftpass.enterprise.intl.BuildConfig.IS_POS_VERSION;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.common.sentry.SentryUtils;

import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;
import cn.swiftpass.enterprise.print.PrintOrder;
import cn.swiftpass.enterprise.ui.activity.print.BluePrintUtil;
import cn.swiftpass.enterprise.ui.activity.print.BluetoothSettingActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftplus.enterprise.printsdk.print.PrintClient;

/**
 * Created by aijingya on 2021/3/23.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description:
 * @date 2021/3/23.20:49.
 */
public class MasterCardRefundOrderDetailsActivity extends BaseActivity {

    public static final String TAG = "MasterCardRefundOrderDetailsActivity";

    private LinearLayout ll_refund_money, ll_refund_amount, ll_surcharge, ll_refund_status, ll_refund_processor, ll_refund_complete_time, ll_refund_attach;
    private TextView tv_refund_money, tv_parser_money, tv_refund_amount, tv_surcharge_money, tv_refund_status, tv_refund_order_id;
    private TextView tv_refund_apply_time, tv_refund_user, tv_plantform_order_id, tv_refund_complete_time, tv_refund_instruction, tv_attach;
    private View view_line_refund_processor, line_time_comlete_refund;
    private Button btn_print;
    private Order orderModel;

    public static void startActivity(Context context, Order orderModel) {
        Intent it = new Intent();
        it.setClass(context, MasterCardRefundOrderDetailsActivity.class);
        it.putExtra("order", orderModel);
        context.startActivity(it);
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainApplication.getInstance().getListActivities().add(this);

        setContentView(R.layout.activity_card_payment_refund_order_details);

        orderModel = (Order) getIntent().getSerializableExtra("order");

        initView();
        initUIDate();
        setListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印的绑定操作
        if (IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)) {
            PrintClient.getInstance().bindDeviceService();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印的解绑操作
        if (IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)) {
            PrintClient.getInstance().unbindDeviceService();
        }
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }


    public void initView() {
        ll_refund_money = findViewById(R.id.ll_refund_money);
        ll_refund_amount = findViewById(R.id.ll_refund_amount);
        ll_surcharge = findViewById(R.id.ll_surcharge);
        ll_refund_status = findViewById(R.id.ll_refund_status);
        ll_refund_processor = findViewById(R.id.ll_refund_processor);
        ll_refund_complete_time = findViewById(R.id.ll_refund_complete_time);
        ll_refund_attach = findViewById(R.id.ll_refund_attach);

        tv_refund_money = findViewById(R.id.tv_refund_money);
        tv_parser_money = findViewById(R.id.tv_parser_money);
        tv_refund_amount = findViewById(R.id.tv_refund_amount);
        tv_surcharge_money = findViewById(R.id.tv_surcharge_money);
        tv_refund_status = findViewById(R.id.tv_refund_status);
        tv_refund_order_id = findViewById(R.id.tv_refund_order_id);

        tv_refund_apply_time = findViewById(R.id.tv_refund_apply_time);
        tv_refund_user = findViewById(R.id.tv_refund_user);
        tv_plantform_order_id = findViewById(R.id.tv_plantform_order_id);
        tv_refund_complete_time = findViewById(R.id.tv_refund_complete_time);
        tv_refund_instruction = findViewById(R.id.tv_refund_instruction);
        tv_attach = findViewById(R.id.tv_attach);

        view_line_refund_processor = findViewById(R.id.view_line_refund_processor);
        line_time_comlete_refund = findViewById(R.id.line_time_comlete_refund);
        btn_print = findViewById(R.id.btn_print);
    }

    public void initUIDate() {
        if (orderModel != null) {
            if (orderModel.getRefundMoney() > 0) {
                tv_refund_money.setVisibility(View.VISIBLE);
                tv_refund_money.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(orderModel.getRefundMoney()));
            } else {
                tv_refund_money.setVisibility(View.GONE);
            }

            if (orderModel.getCashFeel() > 0) {
                tv_parser_money.setVisibility(View.VISIBLE);
                tv_parser_money.setText(getString(R.string.tx_about) + getString(R.string.tx_mark) + DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()));
            } else {
                tv_parser_money.setVisibility(View.GONE);
            }

            if (!MainApplication.getInstance().isSurchargeOpen()) {
                ll_surcharge.setVisibility(View.GONE);
                ll_refund_amount.setVisibility(View.GONE);
            } else {
                if (orderModel != null) {
                    if (orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") || orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")) {
                        ll_surcharge.setVisibility(View.GONE);
                        ll_refund_amount.setVisibility(View.GONE);
                    } else {
                        tv_refund_amount.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(orderModel.getOrderFee()));
                        tv_surcharge_money.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(orderModel.getSurcharge()));
                    }
                }

            }

            tv_refund_order_id.setText(orderModel.getRefundNo());
            tv_refund_apply_time.setText(orderModel.getAddTime());

            //退款处理人
            if (!"".equals(orderModel.getUserName()) && null != orderModel.getUserName() && !orderModel.getUserName().equals("null")) {
                tv_refund_user.setText(orderModel.getUserName() + "(" + orderModel.getUseId() + ")");

            } else {
                if (MainApplication.getInstance().isAdmin(1) || MainApplication.getInstance().isOrderAuth("1")) {
                    if (!TextUtils.isEmpty(orderModel.getMchName()) && !TextUtils.isEmpty(orderModel.getUseId()) && !orderModel.getUseId().equals("null")) {
                        tv_refund_user.setText(orderModel.getMchName() + "(" + orderModel.getUseId() + ")");
                    }
                }
            }
            if (TextUtils.isEmpty(tv_refund_user.getText().toString())) {
                ll_refund_processor.setVisibility(View.GONE);
                view_line_refund_processor.setVisibility(View.GONE);
            }

            //平台订单号
            tv_plantform_order_id.setText(orderModel.getOrderNoMch());
            tv_refund_complete_time.setText(orderModel.getRefundTime());
        }

        switch (orderModel.getRefundState()) {
            case 1:
                btn_print.setVisibility(View.VISIBLE);
                tv_refund_status.setText(R.string.refund_accepted);
                tv_refund_status.setTextColor(getResources().getColor(R.color.pay_fail));
                ll_refund_complete_time.setVisibility(View.VISIBLE);
                line_time_comlete_refund.setVisibility(View.VISIBLE);
                tv_refund_instruction.setVisibility(View.VISIBLE);
                break;
            case 2:
                btn_print.setVisibility(View.GONE);
                tv_refund_status.setText(R.string.refund_failure);
                tv_refund_status.setTextColor(getResources().getColor(R.color.pay_fail));
                ll_refund_complete_time.setVisibility(View.GONE);
                line_time_comlete_refund.setVisibility(View.GONE);
                break;
            case 0:
                btn_print.setVisibility(View.GONE);
                tv_refund_status.setText(R.string.tx_verify_loading);
                tv_refund_status.setTextColor(getResources().getColor(R.color.paytype_title_reset));
                ll_refund_complete_time.setVisibility(View.GONE);
                line_time_comlete_refund.setVisibility(View.GONE);
                break;
            case 3:
                btn_print.setVisibility(View.VISIBLE);
                tv_refund_status.setText(R.string.refund_accepted);
                tv_refund_status.setTextColor(getResources().getColor(R.color.pay_fail));
                tv_refund_instruction.setVisibility(View.VISIBLE);
                ll_refund_complete_time.setVisibility(View.VISIBLE);
                line_time_comlete_refund.setVisibility(View.VISIBLE);

                break;
        }
        if (MainApplication.getInstance().getRefundStateMap() != null && !TextUtils.isEmpty(orderModel.getRefundState() + "")) {
            tv_refund_status.setText(MainApplication.getInstance().getRefundStateMap().get(orderModel.getRefundState() + ""));
        } else {
            tv_refund_status.setText("");
        }


        if (null != orderModel && !TextUtils.isEmpty(orderModel.getAttach())) {
            ll_refund_attach.setVisibility(View.VISIBLE);
            tv_attach.setText(orderModel.getAttach());
        } else {
            ll_refund_attach.setVisibility(View.GONE);
        }
    }

    public void setListener() {
        btn_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bluePrint();
            }
        });
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tx_refund_detail);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButLayClick() {

            }
        });
    }

    public void bluePrint() {
        orderModel.setPrintInfo(getString(R.string.tv_refunding_info));
        orderModel.setPay(false);
        orderModel.setAddTimeNew(orderModel.getRefundTime());
        orderModel.setPartner(getString(R.string.tv_pay_user_stub));

        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印
        if (IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)) {
            if (BuildConfig.posTerminal.equalsIgnoreCase("A8") || BuildConfig.posTerminal.equalsIgnoreCase("BBPos")) {
                orderModel.setPrintInfo(getString(R.string.tv_refunding_info_A8));
            }
            PrintOrder.printOrderDetails(MasterCardRefundOrderDetailsActivity.this, false, orderModel);
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    try {
                        orderModel.setPay(false);
                        orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
                        PrintOrder.printOrderDetails(MasterCardRefundOrderDetailsActivity.this, false, orderModel);
                    } catch (Exception e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                    }
                }
            };
            Timer timer = new Timer();
            timer.schedule(task, PrintClient.getInstance().getPrintDelay());

        } else {
            bluePrintWithBlueToothPermission();
        }
    }


    /**
     * 获得蓝牙权限后进行打印
     */
    private void bluePrintWithBlueToothPermission() {
        if (!MainApplication.getInstance().getBluePrintSetting() && !BuildConfig.IS_POS_VERSION) {//关闭蓝牙打印
            showDialog();
            return;
        }
        if (MainApplication.getInstance().getBluetoothSocket() != null && MainApplication.getInstance().getBluetoothSocket().isConnected()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        BluePrintUtil.print(false, orderModel);
                        //打印第二联
                        try {
                            Thread thread = Thread.currentThread();
                            thread.sleep(3000);
                            orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
                            BluePrintUtil.print(false, orderModel);
                        } catch (InterruptedException e) {
                        }
                    } catch (Exception e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                    }
                }
            });
        } else {
            String bluetoothDeviceAddress = MainApplication.getInstance().getBlueDeviceAddress();
            if (!StringUtil.isEmptyOrNull(bluetoothDeviceAddress)) {
                boolean isSucc = BluePrintUtil.blueConnent(bluetoothDeviceAddress, MasterCardRefundOrderDetailsActivity.this);
                if (isSucc) {
                    try {
                        BluePrintUtil.print(false, orderModel);
                        Thread thread = Thread.currentThread();
                        thread.sleep(3000);
                        orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
                        BluePrintUtil.print(false, orderModel);
                    } catch (InterruptedException e) {
                    }
                }
            } else {
                showDialog();
            }
        }
    }

    public void showDialog() {
        dialog = new DialogInfo(MasterCardRefundOrderDetailsActivity.this,
                getString(R.string.public_cozy_prompt),
                getString(R.string.tx_blue_set), getString(R.string.title_setting),
                getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG,
                new DialogInfo.HandleBtn() {
                    @Override
                    public void handleOkBtn() {
                        dialog.cancel();
                        dialog.dismiss();
                        showPage(BluetoothSettingActivity.class);
                    }

                    @Override
                    public void handleCancelBtn() {
                        dialog.cancel();
                    }
                }, null);

        DialogHelper.resize(MasterCardRefundOrderDetailsActivity.this, dialog);
        dialog.show();
    }

}
