package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.SettlementBean
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/12/2
 *
 * @走得突然
 * @我们来不及告别
 * @这样也好
 * @因为我们永远不告别
 */
class SettlementListContract {


    interface View : BaseView {

        fun queryDailySettlementListSuccess(response: ArrayList<SettlementBean>)

        fun queryDailySettlementListFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {

        fun queryDailySettlementList(
            startDate: String?,
            endDate: String?
        )

    }

}