package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.CashierAddBean
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo
import cn.swiftpass.enterprise.bussiness.model.UserModel
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.CashierAddContract
import cn.swiftpass.enterprise.utils.AESHelper
import cn.swiftpass.enterprise.utils.JsonUtil
import cn.swiftpass.enterprise.utils.MD5
import cn.swiftpass.enterprise.utils.SharedPreUtils
import com.example.common.entity.EmptyData
import okhttp3.Call
import java.util.*

/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @所以对于我来说
 * @真实未必在现实里
 * @梦往往是最值得信赖的
 * @它不说谎
 */
class CashierAddPresenter : CashierAddContract.Presenter {

    private var mView: CashierAddContract.View? = null


    override fun cashierResetPsw(userId: Long) {
        mView?.let { view ->
            view.showLoading(R.string.reset_psw, ParamsConstants.COMMON_LOADING)
            AppClient.cashierResetPsw(
                userId.toString(),
                MainApplication.getInstance().getMchId(),
                "2",
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<CashierAddBean>(CashierAddBean()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.cashierResetPswFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val dynModels = JsonUtil.jsonToBean(
                                response.message,
                                CashierAddBean::class.java
                            ) as CashierAddBean
                            val priKey =
                                SharedPreUtils.readProduct(ParamsConstants.SECRET_KEY) as String?
                            val paseStr = MD5.md5s(priKey).uppercase(Locale.getDefault())
                            dynModels.setPassWord(
                                AESHelper.aesDecrypt(
                                    dynModels.passWord,
                                    paseStr.substring(8, 24)
                                )
                            )
                            view.cashierResetPswSuccess(dynModels)
                        }

                    }
                }
            )
        }
    }

    override fun cashierDelete(userId: Long) {
        mView?.let { view ->
            view.showLoading(R.string.show_delete_loading, ParamsConstants.COMMON_LOADING)
            AppClient.cashierDelete(
                userId.toString(),
                MainApplication.getInstance().getMchId(),
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.cashierDeleteFailed(it.message)
                        }
                    }

                    override fun onResponse(response: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        view.cashierDeleteSuccess(true)
                    }
                }
            )
        }
    }

    override fun ecdhKeyExchange(
        publicKey: String?,
        userModel: UserModel?,
        isTag: Boolean
    ) {
        mView?.let { view ->
            AppClient.ecdhKeyExchange(
                publicKey,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<ECDHInfo>(ECDHInfo()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        commonResponse?.let {
                            view.ecdhKeyExchangeFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        r?.let { response ->
                            val info = JsonUtil.jsonToBean(
                                response.message,
                                ECDHInfo::class.java
                            ) as ECDHInfo
                            MainApplication.getInstance().serPubKey = info.serPubKey
                            MainApplication.getInstance().setSKey(info.skey)
                            view.ecdhKeyExchangeSuccess(info, userModel, isTag)
                        }
                    }
                }
            )
        }
    }

    override fun cashierAdd(
        u: UserModel?,
        isAddCashier: Boolean,
        isTag: Boolean
    ) {
        mView?.let { view ->

            view.showLoading(R.string.show_save_loading, ParamsConstants.COMMON_LOADING)

            u?.let { userModel ->

                AppClient.cashierAdd(
                    userModel.getRealname(),
                    userModel.getUserName(),
                    userModel.getRefundLimit(),
                    userModel.getIsRefundAuth(),
                    userModel.getIsOrderAuth(),
                    userModel.getIsUnfreezeAuth(),
                    userModel.getEnabled().toString(),
                    userModel.getIsTotalAuth(),
                    MainApplication.getInstance().getMchId(),
                    userModel.getId().toString(),
                    "2",
                    System.currentTimeMillis().toString(),
                    isAddCashier,
                    object : CallBackUtil.CallBackCommonResponse<CashierAddBean>(CashierAddBean()) {

                        override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            commonResponse?.let {

                                if (TextUtils.equals("405", it.result)) {
                                    view.cashierAddFailed("405${it.message}", isTag, userModel)
                                } else {
                                    view.cashierAddFailed(it.message, isTag, userModel)
                                }
                            }
                        }


                        override fun onResponse(r: CommonResponse?) {
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            r?.let { response ->
                                val dynModels = JsonUtil.jsonToBean(
                                    response.message,
                                    CashierAddBean::class.java
                                ) as CashierAddBean
                                val priKey = SharedPreUtils.readProduct(ParamsConstants.SECRET_KEY) as String
                                val paseStr = MD5.md5s(priKey).uppercase(Locale.getDefault())
                                dynModels.setPassWord(
                                    AESHelper.aesDecrypt(
                                        dynModels.passWord,
                                        paseStr.substring(8, 24)
                                    )
                                )
                                view.cashierAddSuccess(dynModels, isTag)
                            }
                        }
                    }
                )
            }
        }
    }

    override fun attachView(view: CashierAddContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}