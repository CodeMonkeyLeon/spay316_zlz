package cn.swiftpass.enterprise.bussiness.model;

import java.util.List;

public class BillMordel
{
    private long mchFeeTotal;//手续费
    
    private long payNetFeeTotal;//应结算金额
    
    private long payRemitFeeTotal;// 划账金额
    
    private List<BillItemModel> list;
    
    /**
     * @return 返回 mchFeeTotal
     */
    public long getMchFeeTotal()
    {
        return mchFeeTotal;
    }
    
    /**
     * @param 对mchFeeTotal进行赋值
     */
    public void setMchFeeTotal(long mchFeeTotal)
    {
        this.mchFeeTotal = mchFeeTotal;
    }
    
    /**
     * @return 返回 payNetFeeTotal
     */
    public long getPayNetFeeTotal()
    {
        return payNetFeeTotal;
    }
    
    /**
     * @param 对payNetFeeTotal进行赋值
     */
    public void setPayNetFeeTotal(long payNetFeeTotal)
    {
        this.payNetFeeTotal = payNetFeeTotal;
    }
    
    /**
     * @return 返回 payRemitFeeTotal
     */
    public long getPayRemitFeeTotal()
    {
        return payRemitFeeTotal;
    }
    
    /**
     * @param 对payRemitFeeTotal进行赋值
     */
    public void setPayRemitFeeTotal(long payRemitFeeTotal)
    {
        this.payRemitFeeTotal = payRemitFeeTotal;
    }
    
    /**
     * @return 返回 list
     */
    public List<BillItemModel> getList()
    {
        return list;
    }
    
    /**
     * @param 对list进行赋值
     */
    public void setList(List<BillItemModel> list)
    {
        this.list = list;
    }
}
