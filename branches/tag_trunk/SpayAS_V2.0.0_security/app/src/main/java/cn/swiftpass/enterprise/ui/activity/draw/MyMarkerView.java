package cn.swiftpass.enterprise.ui.activity.draw;

import android.content.Context;
import android.widget.ImageView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;

import cn.swiftpass.enterprise.intl.R;

/**
 * Custom implementation of the MarkerView.
 * 
 * @author Philipp Jahoda
 */
public class MyMarkerView extends MarkerView
{
    
    //private TextView tvContent;
    
    //private RelativeLayout ly_content;
    
    private Context context;
    
    private ImageView iv_solid;
    
    public MyMarkerView(Context context, int layoutResource)
    {
        
        super(context, layoutResource);
        this.context = context;
        //        tvContent = (TextView)findViewById(com.github.mikephil.charting.R.id.tvContent);
        
        //ly_content = (RelativeLayout)findViewById(R.id.ly_content);
        iv_solid = (ImageView)findViewById(R.id.iv_solid);
        //        ly_content.addView(new DrawCircleView(context));
    }
    
    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight)
    {
        
        if (e instanceof CandleEntry)
        {
            
            //CandleEntry ce = (CandleEntry)e;
            

        }
        else
        {
            float valu = e.getVal();
            if (valu == 0)
            {
                iv_solid.setImageResource(R.drawable.icon_charts_round_hollow);
            }
            else
            {
                iv_solid.setImageResource(R.drawable.icon_charts_round_solid);
            }
        }
    }
    
    @Override
    public int getXOffset()
    {
        // this will center the marker-view horizontally
        return -(getWidth() / 2);
    }
    
    @Override
    public int getYOffset()
    {
        // this will cause the marker-view to be above the selected value
        return dip2px(context, -4f);
    }
    
    /** 
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素) 
     */
    public int dip2px(Context context, float dpValue)
    {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int)(dpValue * scale + 0.5f);
    }
    
    //    @Override
    //    public void refreshContent(Entry e, Highlight highlight)
    //    {
    //        // TODO Auto-generated method stub
    //        
    //    }
    
}
