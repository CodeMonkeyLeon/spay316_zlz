package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.BaseInfo
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.SettingSwitchLanContract
import cn.swiftpass.enterprise.utils.JsonUtil
import cn.swiftpass.enterprise.utils.ToastHelper
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @路也许很窄
 * @但总是会有路
 * @也许很长
 * @但总会到头
 * @马不是好马
 * @那就能走就走
 * @马不听指挥
 * @那就随处停留
 */
class SettingSwitchLanPresenter : SettingSwitchLanContract.Presenter {

    private var mView: SettingSwitchLanContract.View? = null


    override fun getBase() {
        mView?.let { view ->

            view.showLoading(R.string.public_loading, ParamsConstants.COMMON_LOADING)
            AppClient.getBase(
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<BaseInfo>(BaseInfo()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.getBaseFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val info = JsonUtil.jsonToBean(
                                response.message,
                                BaseInfo::class.java
                            ) as BaseInfo
                            MainApplication.getInstance().refundStateMap = info.refund_state
                            MainApplication.getInstance().payTypeMap = info.pay_type
                            MainApplication.getInstance().setTradeStateMap(info.trade_state)

                            //V3.1.0版本新增卡通道的支付状态的集合2021/03/26
                            MainApplication.getInstance().cardPaymentTradeStateMap =
                                info.card_trade_type

                            //后台没有返回，前端写死
                            //授权状态(1未授权，2已授权，3已撤销，4解冻)
                            MainApplication.getInstance().preAuthTradeStateMap["1"] =
                                ToastHelper.toStr(R.string.unauthorized)
                            MainApplication.getInstance().preAuthTradeStateMap["2"] =
                                ToastHelper.toStr(R.string.pre_auth_authorized)
                            MainApplication.getInstance().preAuthTradeStateMap["3"] =
                                ToastHelper.toStr(R.string.pre_auth_closed)
                            MainApplication.getInstance().preAuthTradeStateMap["4"] =
                                ToastHelper.toStr(R.string.pre_auth_unfreeze)
                            MainApplication.getInstance().preAuthTradeStateMap =
                                MainApplication.getInstance().preAuthTradeStateMap
                            view.getBaseSuccess(true)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: SettingSwitchLanContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}