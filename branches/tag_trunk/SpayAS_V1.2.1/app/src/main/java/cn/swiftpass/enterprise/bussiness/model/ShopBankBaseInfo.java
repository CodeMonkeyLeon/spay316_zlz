/*
 * 文 件 名:  ShopBaseDataInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2014-3-20
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

import cn.swiftpass.enterprise.io.database.table.ShopBankDataTab;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2014-3-20]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
@DatabaseTable(tableName = ShopBankDataTab.TABLE_NAME)
public class ShopBankBaseInfo implements Serializable
{
    
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    @DatabaseField(generatedId = true)
    public long id;
    
    @DatabaseField(columnName = ShopBankDataTab.COLUMN_BANK_CODE)
    public String bankNumberCode;
    
    @DatabaseField(columnName = ShopBankDataTab.COLUMN_BANK_ID)
    public String bankId;
    
    @DatabaseField(columnName = ShopBankDataTab.COLUMN_BANK_NAME)
    public String bankName;
    
    @DatabaseField(columnName = ShopBankDataTab.COLUMN_CODE)
    public String code;
    
}
