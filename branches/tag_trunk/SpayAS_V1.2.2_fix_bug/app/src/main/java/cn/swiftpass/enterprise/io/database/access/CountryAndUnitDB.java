package cn.swiftpass.enterprise.io.database.access;

import java.sql.SQLException;
import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.CountryAndUnitMode;
import cn.swiftpass.enterprise.io.database.table.CountryAndUnitTable;

import com.j256.ormlite.dao.Dao;

/**
 * 国家与货币
 
 * To change this template use File | Settings | File Templates.
 */
public class CountryAndUnitDB
{
    private static Dao<CountryAndUnitMode, Integer> countryAndUnitDB;
    
    private CountryAndUnitDB()
    {
        try
        {
            countryAndUnitDB = MainApplication.getContext().getHelper().getDao(CountryAndUnitMode.class);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    
    private static CountryAndUnitDB instance;
    
    public static CountryAndUnitDB getInstance()
    {
        if (instance == null)
        {
            instance = new CountryAndUnitDB();
        }
        return instance;
    }
    
    /***
     * 保存
     */
    public void save(CountryAndUnitMode countryAndUnitMode)
    {
        
        CountryAndUnitMode result = getByCountryId(countryAndUnitMode.countryId);
        
        if (result != null)
        {
            try
            {
                countryAndUnitDB.update(countryAndUnitMode);
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            try
            {
                countryAndUnitDB.create(countryAndUnitMode);
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }
        
    }
    
    public List<CountryAndUnitMode> findAll()
    {
        List<CountryAndUnitMode> result = null;
        try
        {
            result = countryAndUnitDB.queryForAll();
        }
        catch (SQLException e)
        {
            
            e.printStackTrace();
        }
        
        if (result != null && result.size() > 0)
        {
            return result;
        }
        
        return null;
    }
    
    public CountryAndUnitMode getByCountryId(Integer countryId)
    {
        List<CountryAndUnitMode> result = null;
        try
        {
            result = countryAndUnitDB.queryForEq(CountryAndUnitTable.COLUMN_COUNTRY_ID, countryId);
        }
        catch (SQLException e)
        {
            
            e.printStackTrace();
        }
        
        if (result != null && result.size() == 1)
        {
            return result.get(0);
        }
        
        return null;
    }
    
    //    /**
    //     * 查询自己的店铺信息
    //     * 添加时间1个之前的
    //     * */
    //    public ShopModel queryShopInfo(long uId)
    //    {
    //        QueryBuilder<ShopModel, Integer> builder = shopInfoDB.queryBuilder();
    //        try
    //        {
    //            Where<ShopModel, Integer> where = builder.where();
    //            where.eq(ShopTable.COLUMN_UID, uId);
    //            where.and();
    //            //long time = System.currentTimeMillis() - 1 * 60 * 60 * 1000;
    //            // where.le(ShopTable.COLUMN_ADD_TIME, time);
    //            builder.orderBy(ShopTable.COLUMN_ADD_TIME, false);
    //            List<ShopModel> list = shopInfoDB.query(builder.prepare());
    //            if (list != null && list.size() > 0)
    //            {
    //                ShopModel shopModel = list.get(0);
    //                long currentTime = System.currentTimeMillis();
    //                long time = ((currentTime / 1000) - (shopModel.addTime / 1000)) / 60;
    //                if (time >= 60)
    //                {
    //                    return null;
    //                }
    //                return shopModel;
    //            }
    //        }
    //        catch (SQLException e)
    //        {
    //            e.printStackTrace();
    //            
    //        }
    //        return null;
    //    }
}
