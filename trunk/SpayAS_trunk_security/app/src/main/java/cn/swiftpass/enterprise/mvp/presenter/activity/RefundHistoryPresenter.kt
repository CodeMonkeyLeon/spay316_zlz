package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.bussiness.model.OrderList
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.RefundHistoryContract
import cn.swiftpass.enterprise.utils.JsonUtil
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @人的一生有一个半童年
 * @一个童年在自己小时候
 * @而半个童年在自己孩子的小时候
 */
class RefundHistoryPresenter : RefundHistoryContract.Presenter {

    private var mView: RefundHistoryContract.View? = null


    override fun queryRefundDetail(
        orderNo: String?,
        mchId: String?
    ) {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)

            AppClient.queryRefundDetail(
                orderNo,
                mchId,
                MainApplication.getInstance().getUserId().toString(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.queryRefundDetailFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"",
                                "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                Order::class.java
                            ) as Order
                            order.useId = order.userId.toString()
                            order.orderNoMch = order.orderNo
                            order.cashFeel =
                                if (TextUtils.isEmpty(order.cashFee)) 0 else order.cashFee.toLong()
                            view.queryRefundDetailSuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun queryUserRefundOrder(
        outTradeNo: String?,
        refundTime: String?,
        mchId: String?,
        refundState: String?,
        page: Int,
        isLoadMore: Boolean
    ) {
        mView?.let { view ->

            AppClient.queryUserRefundOrder(
                outTradeNo,
                refundState,
                refundTime,
                mchId,
                page.toString(),
                "10",
                MainApplication.getInstance().getUserId().toString(),
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<OrderList>(OrderList()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        commonResponse?.let {
                            view.queryUserRefundOrderFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        r?.let { response ->

                            val settle = JsonUtil.jsonToBean(
                                response.message,
                                OrderList::class.java
                            ) as OrderList
                            for (order in settle.data) {
                                order.money = order.totalFee
                                order.add_time =
                                    if (TextUtils.isEmpty(order.addTime)) 0 else order.addTime.toLong()
                                order.useId = order.userId.toString()
                            }
                            view.queryUserRefundOrderSuccess(settle.data, refundState, isLoadMore)
                        }
                    }
                }
            )

        }
    }

    override fun attachView(view: RefundHistoryContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}