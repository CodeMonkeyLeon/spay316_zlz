package cn.swiftpass.enterprise.http.interceptor;


import android.os.Build;
import android.text.TextUtils;

import java.io.IOException;
import java.util.Locale;

import cn.swiftpass.enterprise.SpayApplication;
import cn.swiftpass.enterprise.config.AppConfig;
import cn.swiftpass.enterprise.config.LanguageConfig;
import cn.swiftpass.enterprise.config.RequestValueConfig;
import cn.swiftpass.enterprise.config.SPConfig;
import cn.swiftpass.enterprise.util.AppHelper;
import cn.swiftpass.enterprise.util.DateUtil;
import cn.swiftpass.enterprise.util.SharedPreferencesUtil;
import cn.swiftpass.enterprise.util.TimeZoneUtil;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 创建人：caoxiaoya
 * 时间：2019/4/22
 * 描述：Spay头部全局Header
 * 备注：
 */

public class HeaderInterceptor implements Interceptor {

    private String language;
    /**
     * 默认英文
     */
    private String langu = LanguageConfig.LANG_CODE_EN_US;
    private String key, value;

    @Override
    public Response intercept(Chain chain) throws IOException {
        language = SharedPreferencesUtil.getInstance().getString(SPConfig.LANGUAGE, "");
        if (!TextUtils.isEmpty(language)) {
            langu = language;
        } else {
            String lan = Locale.getDefault().toString();

            if (lan.equalsIgnoreCase(LanguageConfig.LANG_CODE_ZH_CN) || lan.equalsIgnoreCase(LanguageConfig.LANG_CODE_ZH_CN_HANS)) {
                langu = LanguageConfig.LANG_CODE_ZH_CN;
            } else if (lan.equalsIgnoreCase(LanguageConfig.LANG_CODE_ZH_HK) || lan.equalsIgnoreCase(LanguageConfig.LANG_CODE_ZH_MO) ||
                    lan.equalsIgnoreCase(LanguageConfig.LANG_CODE_ZH_TW) || lan.equalsIgnoreCase(LanguageConfig.LANG_CODE_ZH_HK_HANT)) {
                langu = LanguageConfig.LANG_CODE_ZH_TW;
            } else if (lan.equalsIgnoreCase(LanguageConfig.LANG_CODE_JA_JP)) {
                langu = LanguageConfig.LANG_CODE_JA_JP;
            } else {
                langu = LanguageConfig.LANG_CODE_EN_US;
            }
        }

        try {
            key = "sp-" + "Android" + "-" + AppHelper.getImei(SpayApplication.getAppContext());
            value = AppHelper.getVerCode(SpayApplication.getAppContext()) + "," + AppConfig.BANKCODE
                    + "," + langu + "," + AppHelper.getAndroidSDKVersionName() + "," +
                    DateUtil.formatTime(System.currentTimeMillis()) + "," + "Android";
        } catch (Exception e) {
            e.printStackTrace();
        }

        //添加cookic
        String login_skey = SharedPreferencesUtil.getInstance().getString(SPConfig.LOGIN_SKEY, "");
        String login_sauthid = SharedPreferencesUtil.getInstance().getString(SPConfig.LOGIN_SAUTHID, "");
       //添加所有头部参数
        Request request = chain.request()
                .newBuilder()
                .addHeader("User-Agent", String.format("%s/%s (Linux; Android %s; %s Build/%s)",
                        AppConfig.APK_NAME, "3.5", Build.VERSION.RELEASE, Build.MANUFACTURER, Build.ID))
                .addHeader("fp-lang", language)
                .addHeader(key, value)
                .addHeader("Accept-Encoding", "gzip")
                //增加版本号
                .addHeader("interface-version", "2")
                //带上手机默认时区
                .addHeader("timezone", TimeZoneUtil.getCurrentTimeZone())
                //值为2表示使用SHA256签名方式，为空或其他值为MD5；
                .addHeader("spaySt", "2")
                //当前时间
                //.addHeader("spayRs",String.valueOf(System.currentTimeMillis()))
                .build();

        if (!TextUtils.isEmpty(login_skey) && !TextUtils.isEmpty(login_sauthid)) {
            request.newBuilder()
                    .addHeader("SKEY", login_skey)
                    .addHeader("SAUTHID", login_sauthid)
                    .addHeader("ELETYPE", "terminal");
        }
         //登录的时候需要，交换秘钥不需要加
        if (!TextUtils.isEmpty(RequestValueConfig.SKEY)) {
            request.newBuilder().addHeader("spayDi", RequestValueConfig.SKEY);
        }
        return chain.proceed(request);
    }

}
