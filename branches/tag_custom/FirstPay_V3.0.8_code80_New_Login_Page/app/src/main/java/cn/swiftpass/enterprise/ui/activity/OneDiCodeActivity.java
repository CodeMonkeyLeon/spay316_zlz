package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.CreateOneDiCodeUtil;
import cn.swiftpass.enterprise.utils.DisplayUtil;
import cn.swiftpass.enterprise.utils.ImageUtil;

/**
 * 横屏全屏展示一维码
 * <功能详细描述>
 *
 * @author Administrator
 * @version [版本号, 2015-8-13]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class OneDiCodeActivity extends BaseActivity {

    private ImageView iv_code;

    private TextView tv_code;

    private String orderNoMch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        orderNoMch = getIntent().getStringExtra("orderNoMch");

        if (TextUtils.isEmpty(orderNoMch)) {
            finish();
            return;
        }

        setContentView(R.layout.activity_onedicode);

        initView();
    }

    private void initView() {
        iv_code = getViewById(R.id.iv_code);
        tv_code = getViewById(R.id.tv_code);

        WindowManager wm = this.getWindowManager();
        int width = wm.getDefaultDisplay().getWidth();
        int w = (int) (width * 0.35);
        int height = wm.getDefaultDisplay().getHeight();
        int h = (int) (height * 0.75);
        Bitmap tempBitmap = CreateOneDiCodeUtil.createCode(orderNoMch, h, w);
        Bitmap codeBitmap = ImageUtil.adjustPhotoRotation(tempBitmap, 90);
        if (!tempBitmap.isRecycled()) {
            tempBitmap.recycle();
        }
        iv_code.setImageBitmap(codeBitmap);
        tv_code.setText(orderNoMch);
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                tv_code.getLayoutParams().height = DisplayUtil.dip2Px(getApplicationContext(), 310);
                tv_code.setLayoutParams(tv_code.getLayoutParams());
            }
        });
    }

    public static void startActivity(Activity activity, String orderNoMch) {
        Intent intent = new Intent(activity, OneDiCodeActivity.class);
        intent.putExtra("orderNoMch", orderNoMch);
        activity.startActivity(intent);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        finish();
        return super.onTouchEvent(event);
    }

    @Override
    protected boolean isLoginRequired() {
        return false;
    }

}
