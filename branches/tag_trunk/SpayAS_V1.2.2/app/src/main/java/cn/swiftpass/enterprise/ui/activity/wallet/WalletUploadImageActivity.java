/*
 * 文 件 名:  WalletMainActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-1
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.wallet;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.xclcharts.common.DensityUtil;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.wallet.WalletManager;
import cn.swiftpass.enterprise.bussiness.model.WalletModel;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.shop.ImagePase;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.SelectPicPopupWindow;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 开通电子钱包 上传身份证照片
 * 
 * @author  he_hui
 * @version  [版本号, 2016-11-1]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class WalletUploadImageActivity extends TemplateActivity
{
    
    private Button btn_next_step;
    
    private TextView tx_peop, tv_bank_name;
    
    private EditText et_tel;
    
    private ImageView iv_phone, iv_choice_bank;
    
    private List<WalletModel> model;
    
    private String bankNum; //银行卡号
    
    private SelectPicPopupWindow pop = null;
    
    private RelativeLayout ly_chioce_bank;
    
    private ImageView iv_pic_one, iv_pic_two;
    
    private File tempFile;
    
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x1001;
    
    public static final int REQUEST_CODE_CAPTURE_PICTURE = 0x1002;
    
    /** 拍照保存照片的uri */
    private Uri originalUri = null;
    
    private boolean isFirstPic = true;
    
    private String picOnePath, picTowPath;
    
    private List<WalletModel> walletModel;
    
    private LinearLayout ly_two, ly_one, ly_second, ly_first;
    
    private RelativeLayout ly_phone_one, ly_two_phone;
    
    public static void startActivity(Context context, List<WalletModel> model)
    {
        Intent it = new Intent();
        it.setClass(context, WalletUploadImageActivity.class);
        it.putExtra("walletModel", (Serializable)model);
        context.startActivity(it);
    }
    
    @Override
    protected void onPause()
    {
        super.onPause();
        if (null != btn_next_step)
        {
            //            btn_next_step.setText(R.string.reg_next_step);
            //            btn_next_step.setEnabled(true);
            // setButtonBg(btn_next_step, true, R.string.tx_wallet_upload_confirm);
        }
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_upload_image_layout);
        
        initView();
        setLister();
        
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)ly_phone_one.getLayoutParams();
        params.height = DensityUtil.dip2px(WalletUploadImageActivity.this, 170);
        
        LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams)ly_two_phone.getLayoutParams();
        params1.height = DensityUtil.dip2px(WalletUploadImageActivity.this, 170);
        
        btn_next_step.getBackground().setAlpha(102);
        MainApplication.listActivities.add(this);
        
        walletModel = (List<WalletModel>)getIntent().getSerializableExtra("walletModel");
        
    }
    
    String imageUrl;
    
    /**
     * 拍照 <功能详细描述>
     * 
     * @throws Exception
     * @see [类、类#方法、类#成员]
     */
    private void startCamrae(View view)
        throws Exception
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String root = AppHelper.getImgCacheDir();
        imageUrl = root + String.valueOf(new Date().getTime()) + ".jpg";
        tempFile = new File(imageUrl);
        originalUri = Uri.fromFile(tempFile);
        
        intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, originalUri);
        startActivityForResult(intent, REQUEST_CODE_CAPTURE_PICTURE);
    }
    
    @Override
    protected void onResume()
    {
        // TODO Auto-generated method stub
        super.onResume();
        //        if (getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
        //        {
        //            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        //        }
    }
    
    /**
     * 获取相册图片
     * 
     * @throws Exception
     */
    public void takeImg(View view)
        throws Exception
    {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setDataAndType(MediaStore.Images.Media.INTERNAL_CONTENT_URI, "image/*");
        
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
    }
    
    @SuppressWarnings("deprecation")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
        {
            switch (requestCode)
            {
            // 相册获取图片
                case REQUEST_CODE_TAKE_PICTURE:
                    //                    if (ShopkeeperNextActivity.IMAGE_PREVIEW)
                    //                    {
                    //                        originalUri = data.getData();
                    //                        Intent intent = new Intent();
                    //                        intent.setData(originalUri);
                    //                        intent.setClass(this, ImagePreviewActivity.class);
                    //                        
                    //                        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
                    //                        return;
                    //                    }
                    //                    else
                    //                    {
                    //                        ShopkeeperNextActivity.IMAGE_PREVIEW = true;
                    //                    }
                    //                    if (originalUri == null)
                    //                    {
                    //                        break;
                    //                    }
                    //                    // 取消
                    //                    if (!ShopkeeperNextActivity.cancel_perview)
                    //                    {
                    //                        ShopkeeperNextActivity.cancel_perview = true;
                    //                        break;
                    //                    }
                    if (data.getData() == null)
                    {
                        return;
                    }
                    try
                    {
                        String path = getPicPath(data.getData());
                        Bitmap bitmap = null;
                        if (path != null)
                        {
                            File file = new File(path);
                            int MAX_POST_SIZE = 5 * 1024 * 1024;
                            if (file.exists() && file.length() > MAX_POST_SIZE)
                            {
                                //                                bitmap = ImagePase.createBitmap(path, ImagePase.bitmapSize_Image);
                                
                                toastDialog(WalletUploadImageActivity.this, R.string.tv_pic_than_5MB, null);
                                return;
                            }
                            else
                            {
                                bitmap = ImagePase.readBitmapFromStream(path);
                            }
                            
                            if (bitmap != null)
                            {
                                if (isFirstPic)
                                {
                                    picOnePath = path;
                                    LayoutParams params = iv_pic_one.getLayoutParams();
                                    params.width = DensityUtil.dip2px(WalletUploadImageActivity.this, 370);
                                    params.height = DensityUtil.dip2px(WalletUploadImageActivity.this, 220);
                                    //                                    iv_pic_one.setImageBitmap(bitmap);
                                    ly_phone_one.setBackgroundDrawable(new BitmapDrawable(bitmap));
                                    ly_first.setVisibility(View.GONE);
                                    if (!StringUtil.isEmptyOrNull(picOnePath) && !StringUtil.isEmptyOrNull(picTowPath))
                                    {
                                        setButtonBg(btn_next_step, true, R.string.tx_wallet_upload_confirm);
                                    }
                                }
                                else
                                {
                                    picTowPath = path;
                                    LayoutParams params = iv_pic_two.getLayoutParams();
                                    params.width = DensityUtil.dip2px(WalletUploadImageActivity.this, 370);
                                    params.height = DensityUtil.dip2px(WalletUploadImageActivity.this, 220);
                                    ly_second.setVisibility(View.GONE);
                                    ly_two_phone.setBackgroundDrawable(new BitmapDrawable(bitmap));
                                    //                                    iv_pic_two.setImageBitmap(bitmap);
                                    if (!StringUtil.isEmptyOrNull(picOnePath) && !StringUtil.isEmptyOrNull(picTowPath))
                                    {
                                        setButtonBg(btn_next_step, true, R.string.tx_wallet_upload_confirm);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Log.e("hehui", "" + e);
                    }
                    break;
                // 拍照
                case REQUEST_CODE_CAPTURE_PICTURE:
                    try
                    {
                        String pathPhoto = getPicPath(originalUri);
                        Bitmap bitmap_pci = null;
                        File file = new File(pathPhoto);
                        if (file.exists() && file.length() / 1024 > 100)
                        {
                            bitmap_pci = ImagePase.createBitmap(pathPhoto, ImagePase.bitmapSize_Image);
                        }
                        else
                        {
                            File f = new File(imageUrl);
                            if (f.exists())
                            {
                                pathPhoto = imageUrl;
                                bitmap_pci = ImagePase.createBitmap(imageUrl, ImagePase.bitmapSize_Image);
                            }
                        }
                        
                        if (bitmap_pci != null)
                        {
                            if (isFirstPic)
                            {
                                picOnePath = pathPhoto;
                                //                                iv_pic_one.setImageBitmap(bitmap_pci);
                                if (!StringUtil.isEmptyOrNull(picOnePath) && !StringUtil.isEmptyOrNull(picTowPath))
                                {
                                    setButtonBg(btn_next_step, true, R.string.tx_wallet_upload_confirm);
                                }
                                
                                LayoutParams params = iv_pic_one.getLayoutParams();
                                params.width = DensityUtil.dip2px(WalletUploadImageActivity.this, 370);
                                params.height = DensityUtil.dip2px(WalletUploadImageActivity.this, 220);
                                ly_phone_one.setBackgroundDrawable(new BitmapDrawable(bitmap_pci));
                                ly_first.setVisibility(View.GONE);
                                
                            }
                            else
                            {
                                picTowPath = pathPhoto;
                                if (!StringUtil.isEmptyOrNull(picOnePath) && !StringUtil.isEmptyOrNull(picTowPath))
                                {
                                    setButtonBg(btn_next_step, true, R.string.tx_wallet_upload_confirm);
                                }
                                LayoutParams params = iv_pic_two.getLayoutParams();
                                params.width = DensityUtil.dip2px(WalletUploadImageActivity.this, 370);
                                params.height = DensityUtil.dip2px(WalletUploadImageActivity.this, 220);
                                ly_second.setVisibility(View.GONE);
                                ly_two_phone.setBackgroundDrawable(new BitmapDrawable(bitmap_pci));
                                
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        // TODO: handle exception
                        Log.e("hehui", "" + e);
                    }
                    break;
                default:
                    break;
            }
        }
    }
    
    /**
     * 
     * 返回图片地址
     * 
     * @param originalUri
     * @return String
     */
    @SuppressLint("NewApi")
    public String getPicPath(Uri originalUri)
    {
        ContentResolver mContentResolver = WalletUploadImageActivity.this.getContentResolver();
        String originalPath = null;
        if ((originalUri + "").contains("/sdcard"))
        {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/sdcard"));
        }
        else if ((originalUri + "").contains("/data"))
        {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/data"));
        }
        else if ((originalUri + "").contains("/storage")) // 小米 手机 适配
        {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/storage"));
        }
        else
        {
            
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT
                && originalUri.toString().contains("documents"))
            {
                String wholeID = DocumentsContract.getDocumentId(originalUri);
                String id = wholeID.split(":")[1];
                String[] column = {MediaStore.Images.Media.DATA};
                String sel = MediaStore.Images.Media._ID + " = ?";
                Cursor cursor =
                    mContentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column,
                        sel,
                        new String[] {id},
                        null);
                int columnIndex = cursor.getColumnIndex(column[0]);
                if (cursor.moveToFirst())
                {
                    originalPath = cursor.getString(columnIndex);
                }
                cursor.close();
            }
            else
            {
                
                Cursor cursor = mContentResolver.query(originalUri, null, null, null, null);
                if (cursor != null)
                {
                    cursor.moveToFirst();
                    originalPath = cursor.getString(1);
                    cursor.close();
                }
            }
            
        }
        
        return originalPath;
    }
    
    void openDialog(final View v)
    {
        final String status = Environment.getExternalStorageState();
        SelectPicPopupWindow pop =
            new SelectPicPopupWindow(WalletUploadImageActivity.this, new SelectPicPopupWindow.HandleTv()
            {
                
                @Override
                public void takePic()
                {
                    try
                    {
                        if (status.equals(Environment.MEDIA_MOUNTED))
                        {
                            startCamrae(v);
                        }
                        else
                        {
                            
                        }
                        
                    }
                    catch (Exception e)
                    {
                        // TODO: handle exception
                    }
                }
                
                @Override
                public void choicePic()
                {
                    try
                    {
                        if (status.equals(Environment.MEDIA_MOUNTED))
                        {
                            takeImg(v);
                        }
                        else
                        {
                            toastDialog(WalletUploadImageActivity.this, R.string.tx_sd_pic, null);
                        }
                    }
                    catch (Exception e)
                    {
                        
                    }
                    
                }
            });
        
        pop.showAtLocation(iv_pic_one, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0); //设置layout在PopupWindow中显示的位置
    }
    
    private void setLister()
    {
        
        ly_two_phone.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(final View v)
            {
                isFirstPic = false;
                String updateImage = PreferenceUtil.getString("updateImage", "");
                if (StringUtil.isEmptyOrNull(updateImage))
                {
                    NewDialogInfo info =
                        new NewDialogInfo(WalletUploadImageActivity.this, null, null, NewDialogInfo.WALLRT_PHONE_PROMT,
                            null, new NewDialogInfo.HandleBtn()
                            {
                                
                                @Override
                                public void handleOkBtn()
                                {
                                    PreferenceUtil.commitString("updateImage", "updateImage");
                                    openDialog(v);
                                }
                                
                            });
                    
                    DialogHelper.resize(WalletUploadImageActivity.this, info);
                    
                    info.show();
                }
                else
                {
                    openDialog(v);
                }
                
            }
        });
        
        ly_phone_one.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(final View v)
            {
                isFirstPic = true;
                String updateImage = PreferenceUtil.getString("updateImage", "");
                if (StringUtil.isEmptyOrNull(updateImage))
                {
                    NewDialogInfo info =
                        new NewDialogInfo(WalletUploadImageActivity.this, null, null, NewDialogInfo.WALLRT_PHONE_IMAGE,
                            null, new NewDialogInfo.HandleBtn()
                            {
                                
                                @Override
                                public void handleOkBtn()
                                {
                                    PreferenceUtil.commitString("updateImage", "updateImage");
                                    openDialog(v);
                                }
                                
                            });
                    
                    DialogHelper.resize(WalletUploadImageActivity.this, info);
                    
                    info.show();
                }
                else
                {
                    openDialog(v);
                } //设置layout在PopupWindow中显示的位置
            }
        });
        
        btn_next_step.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (StringUtil.isEmptyOrNull(picOnePath) || StringUtil.isEmptyOrNull(picTowPath))
                {
                    return;
                }
                
                WalletManager.getInstance().ewalletUpload(walletModel.get(0).getIsUpdateFlag(),
                    walletModel.get(0).getModifyidCards(),
                    picOnePath,
                    picTowPath,
                    new UINotifyListener<WalletModel>()
                    {
                        
                        @Override
                        public void onPreExecute()
                        {
                            super.onPreExecute();
                            loadDialog(WalletUploadImageActivity.this, R.string.public_loading);
                        }
                        
                        @Override
                        public void onPostExecute()
                        {
                            // TODO Auto-generated method stub
                            super.onPostExecute();
                            
                        }
                        
                        @Override
                        public void onError(final Object object)
                        {
                            super.onError(object);
                            dissDialog();
                            if (checkSession())
                            {
                                return;
                            }
                            if (null != object)
                            {
                                WalletUploadImageActivity.this.runOnUiThread(new Runnable()
                                {
                                    
                                    @Override
                                    public void run()
                                    {
                                        //                            btn_next_step.setText(R.string.reg_next_step);
                                        toastDialog(WalletUploadImageActivity.this, object.toString(), null);
                                    }
                                });
                            }
                            
                        }
                        
                        @Override
                        public void onSucceed(WalletModel model)
                        {
                            dissDialog();
                            //                            ly_one.setVisibility(View.VISIBLE);
                            //                            ly_two.setVisibility(View.VISIBLE);
                            //                btn_next_step.setEnabled(true);
                            //                btn_next_step.setText(R.string.reg_next_step);
                            WalletOpenNextActivity.startActivity(WalletUploadImageActivity.this, walletModel);
                        }
                        
                    });
            }
        });
        
    }
    
    private void initView()
    {
        ly_first = getViewById(R.id.ly_first);
        ly_second = getViewById(R.id.ly_second);
        
        ly_phone_one = getViewById(R.id.ly_phone_one);
        ly_two_phone = getViewById(R.id.ly_two_phone);
        
        ly_one = getViewById(R.id.ly_one);
        ly_two = getViewById(R.id.ly_two);
        btn_next_step = getViewById(R.id.btn_next_step);
        iv_pic_one = getViewById(R.id.iv_pic_one);
        iv_pic_two = getViewById(R.id.iv_pic_two);
        
        //        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)iv_pic_one.getLayoutParams();
        //        params.width = DensityUtil.dip2px(WalletUploadImageActivity.this, 650);
        //        params.height = DensityUtil.dip2px(WalletUploadImageActivity.this, 150);
        
        //        LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams)iv_pic_two.getLayoutParams();
        //        params1.width = DensityUtil.dip2px(WalletUploadImageActivity.this, 650);
        //        params1.height = DensityUtil.dip2px(WalletUploadImageActivity.this, 150);
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        
        titleBar.setTitle(R.string.tx_wallet_upload_pic);
        titleBar.setLeftButtonIsVisible(true);
        titleBar.setLeftButtonVisible(true);
        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.tv_update_img_title));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                final NewDialogInfo info =
                    new NewDialogInfo(WalletUploadImageActivity.this, null, null, NewDialogInfo.WALLRT_PHONE_IMAGE,
                        null, new NewDialogInfo.HandleBtn()
                        {
                            
                            @Override
                            public void handleOkBtn()
                            {
                            }
                            
                        });
                
                DialogHelper.resize(WalletUploadImageActivity.this, info);
                
                info.show();
            }
        });
    }
}
