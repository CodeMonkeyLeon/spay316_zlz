package cn.swiftpass.enterprise.utils;

import android.annotation.SuppressLint;
import android.util.Log;

import com.example.common.sentry.SentryUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@SuppressLint("SimpleDateFormat")
public class DateTimeUtil {
    private static final String TAG = DateTimeUtil.class.getSimpleName();

    /**
     * 得到一个月内的日期 getThirtyDays:
     *
     * @return
     * @author he_hui
     */
    public static List<String> getThirtyDays() {
        List<String> dateArr = new ArrayList<String>();
        List<String> arrs = new ArrayList<String>();
        SimpleDateFormat sdf1 = new SimpleDateFormat("MM.dd");
        for (int i = 0; i < 30; i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, -(i + 1));
            dateArr.add(sdf1.format(calendar.getTime()));
        }
        Collections.reverse(dateArr);
        try {

            for (int i = 0; i <= dateArr.size(); i++) {
                arrs.add(dateArr.get(i * 5));
            }
        } catch (Exception e2) {
            arrs.add(DateUtil.formatMMDD(DateUtil.getBeforeDate().getTime()));
            return arrs;
        }
        return arrs;
    }

    /**
     * 得到一个月内的日期 getThirtyDays:
     *
     * @return
     * @author he_hui
     */
    public static List<String> getThirtyDaysForYYYYDDMM() {
        List<String> dateArr = new ArrayList<String>();
        List<String> arrs = new ArrayList<String>();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        for (int i = 0; i < 30; i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, -(i + 1));
            dateArr.add(sdf1.format(calendar.getTime()));
        }
        Collections.reverse(dateArr);
        try {

            for (int i = 0; i <= dateArr.size(); i++) {
                arrs.add(dateArr.get(i * 5));
            }
        } catch (Exception e2) {
            return arrs;
        }
        return arrs;
    }

    /**
     * 得到一个月内的日期 getThirtyDays:
     *
     * @return
     * @author he_hui
     */
    public static List<String> getMonthThirtyDays() {
        List<String> dateArr = new ArrayList<String>();
        List<String> arrs = new ArrayList<String>();
        SimpleDateFormat sdf1 = new SimpleDateFormat("MM-dd");
        for (int i = 0; i < 30; i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, -(i + 1));
            dateArr.add(sdf1.format(calendar.getTime()));
        }
        Collections.reverse(dateArr);
        try {

            for (int i = 0; i <= dateArr.size(); i++) {
                //                arrs.add(dateArr.get(i * 5) + "-" + dateArr.get((i + 1) * 5 - 1));
                arrs.add(dateArr.get(i));
            }
        } catch (Exception e2) {
            arrs.add(DateUtil.formatMD(DateUtil.getBeforeDate().getTime()));
            return arrs;
        }
        return arrs;
    }

    /**
     * 得到一个月内的日期 getThirtyDays:
     *
     * @return
     * @author he_hui
     */
    public static List<String> getAllThirtyDays() {
        List<String> dateArr = new ArrayList<String>();
        List<String> arrs = new ArrayList<String>();
        SimpleDateFormat sdf1 = new SimpleDateFormat("MM-dd");
        for (int i = 0; i < 30; i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, -(i + 1));
            dateArr.add(sdf1.format(calendar.getTime()));
        }
        Collections.reverse(dateArr);
        try {

            for (int i = 0; i <= dateArr.size(); i++) {
                //                arrs.add(dateArr.get(i * 5) + "-" + dateArr.get((i + 1) * 5 - 1));
                arrs.add(dateArr.get(i));
            }
        } catch (Exception e2) {
            return arrs;
        }
        return arrs;
    }

    /**
     * 得到6个月
     *
     * @return
     * @author he_hui
     */
    public static List<String> getMonthSixMonths() {
        List<String> dateArr = new ArrayList<String>();
        List<String> arrs = new ArrayList<String>();
        SimpleDateFormat sdf1 = new SimpleDateFormat("MM月");
        for (int i = 1; i < 7; i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, -(i));
            dateArr.add(sdf1.format(calendar.getTime()));
        }
        Collections.reverse(dateArr);
        try {

            for (int i = 0; i <= dateArr.size(); i++) {
                if (dateArr.get(i).startsWith("0")) {
                    arrs.add(dateArr.get(i).substring(1, dateArr.get(i).length()));
                } else {
                    arrs.add(dateArr.get(i));
                }
            }
        } catch (Exception e2) {
            return arrs;
        }
        return arrs;
    }

    /**
     * <功能详细描述>
     *
     * @param one
     * @param two
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static String formatMonetToStr(long one, long two) {
        try {

            BigDecimal decimal1 = new BigDecimal(one);
            BigDecimal decimal2 = new BigDecimal(two);

            BigDecimal num3 = decimal1.divide(decimal2, 2, RoundingMode.HALF_DOWN);
            return String.valueOf(num3);
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.i(TAG, "formatMonetToStr-->" + e);
            return null;
        }
    }

    /**
     * 得到一个月的第一天
     * <功能详细描述>
     *
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static String getMothdFirst() {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -30);
        return sdf1.format(calendar.getTime());
    }

    public static List<String> getListTime() {
        Date date = new Date();
        List<Date> ds = listTimeFormt(date);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        List<String> list = new ArrayList<String>();
        for (Date d : ds) {
            list.add(sdf.format(d));
        }

        list.add(nextDate() + ":00");

        return list;
    }

    /**
     * 下一天
     * <功能详细描述>
     *
     * @return
     * @see [类、类#方法、类#成员]
     */
    private static String nextDate() {
        SimpleDateFormat sdf1 = new SimpleDateFormat("HH");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.HOUR, 1);
        return sdf1.format(c.getTime());
    }

    /**
     * 得到一个月的所以天数
     * <功能详细描述>
     *
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static List<String> getListDate() {
        Date date = new Date();
        List<Date> ds = listTimeFormt(date);
        SimpleDateFormat sdf = new SimpleDateFormat("dd");
        List<String> list = new ArrayList<String>();
        for (Date d : ds) {
            list.add(sdf.format(d));
        }

        return list;
    }

    static List<Date> listTimeFormt(Date date) {
        Date d = new Date();

        Date start = dayStartDate(date);//转换为天的起始date
        Date nextDayDate = nextDay(start);//下一天的date

        List<Date> result = new ArrayList<Date>();
        while (nextDayDate.compareTo(start) < 0) {
            result.add(start);
            start = addFiveMin(start, 60);
            if (start.compareTo(d) == 1) {
                break;
            }
        }
        return result;
    }

    private static Date addFiveMin(Date start, int offset) {
        Calendar c = Calendar.getInstance();
        c.setTime(start);
        c.add(Calendar.MINUTE, offset);
        return c.getTime();
    }

    private static Date nextDay(Date start) {
        Calendar c = Calendar.getInstance();
        c.setTime(start);
        c.add(Calendar.DATE, -1);
        return c.getTime();
    }

    private static Date dayStartDate(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }
}
