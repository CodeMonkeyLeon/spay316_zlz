package cn.swiftpass.enterprise.io.database.access;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.AdsInfo;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import java.sql.SQLException;
import java.util.List;

/**
 * User: alan
 * Date: 13-10-27
 * Time: 下午8:03
 */
public class AdsInfoDB {

    private static Dao<AdsInfo,Integer> adsInfoDao;
    private AdsInfoDB()
    {
        try
        {
            adsInfoDao  = MainApplication.getContext().getHelper().getDao(AdsInfo.class);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    private static AdsInfoDB instance;
    public static AdsInfoDB getInstance()
    {
        if(instance == null)
        {
            instance = new AdsInfoDB();
        }
        return instance;
    }

    /**创建广告 */
    public int save(AdsInfo adsInfo) throws SQLException{
        return adsInfoDao.create(adsInfo);
    }
    /**修改广告信息*/
    public int update(AdsInfo adsInfo) throws SQLException
    {
        return adsInfoDao.update(adsInfo);
    }
    /**获取信息*/
    public List<AdsInfo> query()throws  SQLException
    {
        QueryBuilder<AdsInfo,Integer> builder = adsInfoDao.queryBuilder();
        return adsInfoDao.query(builder.prepare());
    }

}
