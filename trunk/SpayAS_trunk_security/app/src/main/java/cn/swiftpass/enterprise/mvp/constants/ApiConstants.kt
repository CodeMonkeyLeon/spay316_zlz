package cn.swiftpass.enterprise.mvp.constants

/**
 * @author lizheng.zhao
 * @date 2022/12/15
 *
 * @一眨眼
 * @算不算少年
 * @一辈子
 * @算不算永远
 */
object ApiConstants {

    private const val URL_SPLIT = "##"
    const val PARAM_VALUE_NULL = "param_value_null"
    const val TRUE = true
    const val FALSE = false
    const val JSON_STRING_FLAG = "json_string_flag"

    const val URL_QUERY_WALLET_LIST = "spay/showWalletList"
    const val URL_GET_CODE = "spay/user/refreshLoginCode"
    const val URL_GET_CERTIFICATE_STRING = "spay/key/getCertificate"
    const val URL_ECDH_KEY_EXCHANGE = "spay/key/pKeyExchange"
    const val URL_DEVICE_LOGIN = "spay/user/deviceLogin"
    const val URL_GET_VERSION_CODE = "spay/upgrade/checkinVersion"
    const val URL_LOGIN_ASYNC = "spay/user/spayLoginWithCodeV2"
    const val URL_DO_COVER_ORDER_AUTH = "spay/unifiedMicroAuth"
    const val URL_DO_COVER_ORDER_PAY = "spay/unifiedMicroPay"

    //    const val URL_DO_COVER_ORDER = URL_DO_COVER_ORDER_AUTH + URL_SPLIT + URL_DO_COVER_ORDER_PAY
    const val URL_MASTER_CARD_NATIVE_PAY = "spay/card/payment"
    const val URL_QUERY_CARD_PAYMENT_ORDER_DETAILS = "spay/card/orderDetail"
    const val URL_QUERY_ORDER_BY_ORDER_NO_AUTH = "spay/unifiedQueryAuth"
    const val URL_QUERY_ORDER_BY_ORDER_NO_PAY = "spay/payQueryOrder"

    //    const val URL_QUERY_ORDER_BY_ORDER_NO =
//        URL_QUERY_ORDER_BY_ORDER_NO_AUTH + URL_SPLIT + URL_QUERY_ORDER_BY_ORDER_NO_PAY
    const val URL_ORDER_DETAIL_AUTH = "spay/authDetail"
    const val URL_ORDER_DETAIL_QUERY = "spay/queryOrderDetail"

    //    const val URL_QUERY_ORDER_DETAIL = URL_ORDER_DETAIL_AUTH + URL_SPLIT +
//            URL_ORDER_DETAIL_QUERY + URL_SPLIT + URL_ORDER_DETAIL_QUERY
//    const val URL_SCAN_QUERY_ORDER_DETAIL =
//        URL_ORDER_DETAIL_AUTH + URL_SPLIT + URL_ORDER_DETAIL_QUERY
    const val URL_UNIFIED_NATIVE_PAY_AUTH = "spay/unifiedNativeAuth"
    const val URL_UNIFIED_NATIVE_PAY_PAY = "spay/unifiedNativePay"

    //    const val URL_UNIFIED_NATIVE_PAY =
//        URL_UNIFIED_NATIVE_PAY_AUTH + URL_SPLIT + URL_UNIFIED_NATIVE_PAY_PAY
    const val URL_UNIFIED_PAY_REVERSE_AUTH = "spay/authReverse"
    const val URL_UNIFIED_PAY_REVERSE_PAY = "spay/unifiedPayReverse"

    //    const val URL_UNIFIED_PAY_REVERSE =
//        URL_UNIFIED_PAY_REVERSE_AUTH + URL_SPLIT + URL_UNIFIED_PAY_REVERSE_PAY
    const val URL_PAYMENT_LINK_ORDER = "spay/paymentLinkApp/order"
    const val URL_UPLOAD_IMAGE = "spay/paymentLinkApp/uploadImg"
    const val URL_LOAD_CARD_PAYMENT_DATE = "spay/card/transList"
    const val URL_QUERY_DAILY_STATICS = "spay/todayStatistics"
    const val URL_QUERY_PRE_AUTH_DAILY_STATICS = "spay/preAuthorizationStatistics"
    const val URL_QUERY_REFUND_DETAIL = "spay/queryRefundDetail"
    const val URL_QUERY_SPAY_ORDER_NEW_REFUND_LIST = "spay/refundList"
    const val URL_QUERY_SPAY_ORDER_NEW_AUTH_ORDER_LIST = "spay/authOrderList"
    const val URL_QUERY_SPAY_ORDER_NEW_ORDER_LIST = "spay/order/orderList"
    const val URL_QUERY_SPAY_ORDER_NEW_AUTH_OPERATE_QUERY = "spay/authOperateQuery"
    const val URL_QUERY_SPAY_ORDER_WX_CARD = "spay/wxCardGetUserListV2"

    //    const val URL_QUERY_SPAY_ORDER_NEW =
//        URL_QUERY_SPAY_ORDER_NEW_REFUND_LIST + URL_SPLIT + URL_QUERY_SPAY_ORDER_NEW_AUTH_ORDER_LIST + URL_SPLIT + URL_QUERY_SPAY_ORDER_NEW_ORDER_LIST + URL_SPLIT + URL_QUERY_SPAY_ORDER_NEW_AUTH_OPERATE_QUERY
//    const val URL_QUERY_SPAY_ORDER =
//        URL_QUERY_SPAY_ORDER_NEW_REFUND_LIST + URL_SPLIT + URL_QUERY_SPAY_ORDER_NEW_AUTH_ORDER_LIST + URL_SPLIT + URL_QUERY_SPAY_ORDER_NEW_ORDER_LIST
//    const val URL_QUERY_ORDER_WX_CARD =
//        URL_QUERY_SPAY_ORDER_NEW_REFUND_LIST + URL_SPLIT + URL_QUERY_SPAY_ORDER_NEW_ORDER_LIST + URL_SPLIT + URL_QUERY_SPAY_ORDER_WX_CARD
    const val URL_CUSTOMER_LIST = "spay/paymentLinkApp/customer"
    const val URL_PRODUCT_LIST = "spay/paymentLinkApp/goods"
    const val URL_GET_CODE_LIST = "spay/qrCodeQuery"
    const val URL_QUERY_MERCHANT_DATA_BY_TEL = "spay/mch/get"
    const val URL_ORDER_COUNT = "spay/orderCount"
    const val URL_QUERY_TRANSACTION_REPORT = "spay/transactionReport"
    const val URL_CHANGE_PWD = "spay/user/resetPwdV2"
    const val URL_GET_ORDER_CONFIG = "spay/paymentLinkApp/config"
    const val URL_QUERY_ORDER_BY_INVOICE_ID = "spay/invoiceIdQuery"
    const val URL_PAY_REVERSE = "spay/payReverse"
    const val URL_QUERY_CASHIER = "spay/user/queryPageCashier"
    const val URL_QUERY_ORDER_DATA = "spay/order/querySpayOrder"
    const val URL_QUERY_USER_REFUND_ORDER = "spay/searchRefundOrder"
    const val URL_API_SHOW_LIST = "spay/apiShowList"
    const val URL_REFUND_LOGIN = "spay/user/spayLoginV2"
    const val URL_BIND_CODE = "spay/qrBind"
    const val URL_BIND_CODE_DETAILS = "spay/qrCodeInfo"
    const val URL_QUERY_TRANSACTION_REPORT_DETAILS = "spay/transactionReportDetail"
    const val URL_CHECK_VERIFY_CODE_AND_PASSWORD = "spay/user/logoutVerify"
    const val URL_SEND_EMAIL_FOR_ACCOUNT_CANCEL = "spay/user/sendEmailVerifyCode"
    const val URL_QUERY_CARD_DETAIL = "spay/queryCardDetail"
    const val URL_CASHIER_RESET_PSW = "spay/user/resetPwd2Default"
    const val URL_CASHIER_ADD_SAVE = "spay/user/saveCashier"
    const val URL_CASHIER_ADD_UPDATE = "spay/user/saveOrUpdateSpayUserV2"
//    const val URL_CASHIER_ADD = URL_CASHIER_ADD_SAVE + URL_SPLIT + URL_CASHIER_ADD_UPDATE
    const val URL_CASHIER_DELETE = "spay/user/deleteSpayUser"
    const val URL_CHECK_FORGET_PWD_INPUT = "spay/user/forgetPwdAndGetEmailCode"
    const val URL_CHECK_PHONE_CODE = "spay/user/checkEmailCodeV2"
    const val URL_CHECK_DATA = "spay/user/setPasswordForEmailV2"
    const val URL_QUERY_BODY = "spay/findTradeRemark"
    const val URL_UPDATE_OR_ADD_BODY = "spay/addTradeRemark"
    const val URL_SYNC_MASTER_CARD_ORDER_STATUS_BY_ORDER_NO = "spay/card/inquiry"
    const val URL_QUERY_MASTER_CARD_ORDER_BY_ORDER_NO = "spay/card/query"
    const val URL_BIND_SPEAKER = "spay/speaker/bindSpeaker"
    const val URL_UNBIND_SPEAKER = "spay/speaker/unbindSpeaker"
    const val URL_GET_SPEAKER_LIST = "spay/speaker/getShowList"
    const val URL_AUTH_PAY_QUERY = "spay/authPayQuery"
    const val URL_AUTH_UNFREEZE = "spay/authUnfreeze"
    const val URL_CARD_PAYMENT_REFUND = "spay/card/refund"
    const val URL_REGIS_REFUNDS = "spay/spayRefundV2"
    const val URL_CHECK_CAN_REFUND_OR_NOT = "spay/calcRefund"
    const val URL_UNIFIED_AUTH_PAY = "spay/unifiedAuthPay"
    const val URL_REGIS_REFUND = "spay/spayRefund"
    const val URL_LOGOUT = "spay/user/logout"
    const val URL_GET_BASE = "spay/config/getBase"
    const val URL_QUERY_DAILY_SETTLEMENT_LIST = "spay/dailySettlement"
    const val URL_GET_ORDER_TOTAL = "spay/settlementToTal"
    const val URL_SUBMIT_TIPS_SETTING = "spay/saveOrUpdateTips"
    const val URL_GET_TIPS_SETTING = "spay/queryTipsConf"
    const val URL_GET_EXCHANGE_RATE = "spay/mch/getExchangeRate"

}