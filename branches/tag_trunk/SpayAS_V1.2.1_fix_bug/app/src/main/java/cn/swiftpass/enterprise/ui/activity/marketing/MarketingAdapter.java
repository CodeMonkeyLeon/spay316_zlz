package cn.swiftpass.enterprise.ui.activity.marketing;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.AwardModel;
import cn.swiftpass.enterprise.utils.DateUtil;

public class MarketingAdapter extends BaseAdapter
{
    private Context context;
    
    private List<AwardModel> scanList;
    
    private ViewHolder holder;
    
    public MarketingAdapter(Context context, List<AwardModel> scanList)
    {
        this.context = context;
        
        this.scanList = scanList;
    }
    
    @Override
    public int getCount()
    {
        return scanList.size();
    }
    
    @Override
    public Object getItem(int position)
    {
        return scanList.get(position);
    }
    
    @Override
    public long getItemId(int position)
    {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            convertView = View.inflate(context, R.layout.activity_maketing_list_item, null);
            holder = new ViewHolder();
            holder.tv_title = (TextView)convertView.findViewById(R.id.tv_title);
            holder.tv_active_name = (TextView)convertView.findViewById(R.id.tv_active_name);
            holder.tv_time = (TextView)convertView.findViewById(R.id.tv_time);
            holder.tv_peop_num = (TextView)convertView.findViewById(R.id.tv_peop_num);
            holder.tv_num = (TextView)convertView.findViewById(R.id.tv_num);
            holder.iv_icon = (ImageView)convertView.findViewById(R.id.iv_icon);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder)convertView.getTag();
        }
        AwardModel awardModel = scanList.get(position);
        if (awardModel != null)
        {
            holder.tv_active_name.setText(awardModel.getActiveName());
            //            holder.tv_time.setText(awardModel.getStartDate() + "-" + awardModel.getEndDate());
            holder.tv_title.setText(DateUtil.formatMoneyUtils(awardModel.getFullMoney()));
            holder.tv_num.setText(awardModel.getMaxNum() + "");
            switch (awardModel.getActiveStatus())
            {
                case 0://未开始
                    holder.iv_icon.setImageResource(R.drawable.icon_marketing_notstarted);
                    holder.tv_title.setTextColor(Color.parseColor("#000000"));
                    holder.tv_num.setTextColor(Color.parseColor("#000000"));
                    break;
                case 1://进行中
                    holder.iv_icon.setImageResource(R.drawable.icon_marketing_ongoing);
                    holder.tv_title.setTextColor(Color.parseColor("#4182E2"));
                    holder.tv_num.setTextColor(Color.parseColor("#000000"));
                    break;
                case 2://已停止
                    holder.iv_icon.setImageResource(R.drawable.icon_marketing_end);
                    holder.tv_title.setTextColor(Color.parseColor("#cccccc"));
                    holder.tv_num.setTextColor(Color.parseColor("#cccccc"));
                    break;
                default:
                    break;
            }
        }
        
        return convertView;
    }
    
    private class ViewHolder
    {
        private TextView tv_title, tv_time, tv_active_name, tv_num, tv_peop_num;
        
        private ImageView iv_icon;
    }
    
}
