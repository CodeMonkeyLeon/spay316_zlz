package cn.swiftpass.enterprise.io.okhttp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.swiftpass.enterprise.utils.Logger;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;

/**
 * Created by congwei.li on 2022/4/25.
 *
 * @Description:
 */
public class CookieJarManager  implements CookieJar {

    private final String TAG = "CookieJarManager";

    private final HashMap<String, List<Cookie>> cookieStore = new HashMap<>();

    @Override
    public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
        if (null == url || null == cookies || cookies.size() <= 0) {
            return;
        }

        cookieStore.put(url.host(), cookies);

        for (Cookie cookie : cookies) {
            Logger.d(TAG, "cookie Name:" + cookie.name());
            Logger.d(TAG, "cookie Value:" + cookie.value());

//            if ("SKEY".equals(cookie.name())) {
//                PreferenceUtil.commitString("login_skey", cookie.value());
//            }
//
//            if ("SAUTHID".equals(cookie.name())) {
//                PreferenceUtil.commitString("login_sauthid", cookie.value());
//            }

        }
    }

    @Override
    public List<Cookie> loadForRequest(HttpUrl url) {
        if (null != url) {
            List<Cookie> cookies = cookieStore.get(url.host());
            return cookies != null ? cookies : new ArrayList<Cookie>();
        } else {
            return new ArrayList<Cookie>();
        }
    }
}