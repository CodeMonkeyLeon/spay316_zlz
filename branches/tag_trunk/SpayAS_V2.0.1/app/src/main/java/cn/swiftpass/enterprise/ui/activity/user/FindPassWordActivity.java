/*
 * 文 件 名:
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-14
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.user;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.WelcomeActivity;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.VerifyUtil;

/**
 * 忘记密码
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-14]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class FindPassWordActivity extends TemplateActivity
{
    
    private EditText userPhone;
    
    private EditText shop_name, newPwd, repeatPwd;
    
    private EditText shop_id;
    
    private EditText code, mch_id;
    
    private ImageView iv_clearPhone;
    
    private ImageView iv_clearName;
    
    private ImageView iv_clearID, iv_mch_id;
    
    private ImageView iv_tel_promt, iv_id_promt, iv_shop_promt, iv_repeatPwd, iv_newPwd, iv_newPwd_promt,
        iv_repeatPwd_promt;;
    
    private Button confirm;
    
    private Button getCode;
    
    private TimeCount time;
    
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.findpassword);
        
        initView();
        
        setLister();
        
    }
    
    private void setLister()
    {
        iv_clearPhone.setOnClickListener(clearImage);
        iv_clearName.setOnClickListener(clearImage);
        iv_clearID.setOnClickListener(clearImage);
        iv_mch_id.setOnClickListener(clearImage);
        
        confirm.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (!isAbsoluteNullStr(userPhone.getText().toString()))
                {
                    if (!VerifyUtil.verifyValid(userPhone.getText().toString(), GlobalConstant.MOBILEPHOE))
                    {
                        showToastInfo(R.string.tx_phone_formart);
                        userPhone.setFocusable(true);
                        return;
                    }
                }
                else
                {
                    showToastInfo(R.string.tx_phone_notnull);
                    userPhone.setFocusable(true);
                    return;
                }
                
                //                if (isAbsoluteNullStr(shop_name.getText().toString()))
                //                {
                //                    ToastHelper.showInfo("店主姓名不能为空!");
                //                    shop_name.setFocusable(true);
                //                    return;
                //                }
                
                //                if (!isAbsoluteNullStr(shop_id.getText().toString()))
                //                {
                //                    // 验证省份证有效性
                //                    //                    Pattern idNumPattern = Pattern.compile("(\\d{14}[0-9a-zA-Z])|(\\d{17}[0-9a-zA-Z])");
                //                    //                    Matcher m = idNumPattern.matcher(shop_id.getText().toString());
                //                    //                    boolean flag = m.matches();
                //                    if (!VerifyUtil.verifyValid(shop_id.getText().toString(), GlobalConstant.IDCARD))
                //                    {
                //                        ToastHelper.showInfo("身份证号码格式不正确!!");
                //                        shop_id.setFocusable(true);
                //                        return;
                //                    }
                //                }
                //                else
                //                {
                //                    ToastHelper.showInfo("身份证号码不能为空!!");
                //                    shop_id.setFocusable(true);
                //                    return;
                //                }
                //                showPage(Update密码.class);
                
                if (isAbsoluteNullStr(code.getText().toString()))
                {
                    showToastInfo(R.string.tx_ver_code);
                    return;
                }
                
                //                if (isAbsoluteNullStr(mch_id.getText().toString()))
                //                {
                //                    showToastInfo("威富通商户号不能为空");
                //                    
                //                    return;
                //                }
                
                if (isAbsoluteNullStr(newPwd.getText().toString()))
                {
                    showToastInfo(R.string.tx_newpass_notnull);
                    return;
                }
                
                if (isAbsoluteNullStr(repeatPwd.getText().toString()))
                {
                    showToastInfo(R.string.tx_repeatpass_notnull);
                    return;
                }
                
                if (newPwd.getText().toString().length() < 8)
                {
                    showToastInfo(R.string.show_pass_prompt);
                    return;
                }
                
                if (!newPwd.getText().toString().equals(repeatPwd.getText().toString()))
                {
                    showToastInfo(R.string.tx_pass_notdiff);
                    return;
                }
                
                UserModel userModel = new UserModel();
                userModel.setCode(code.getText().toString());
                userModel.setIDNumber(newPwd.getText().toString());
                //                userModel.setName(shop_name.getText().toString());
                userModel.setPhone(userPhone.getText().toString());
                userModel.setMchId(mch_id.getText().toString());
                findPass(userModel);
                
            }
            
        });
        
        // 获取验证码
        getCode.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                String telNumber = userPhone.getText().toString();
                if ("".equals(telNumber) || telNumber == null)
                {
                    showToastInfo(R.string.tx_phone_notnull);
                    userPhone.setFocusable(true);
                    
                    return;
                }
                else
                {
                    if (!VerifyUtil.verifyValid(userPhone.getText().toString(), GlobalConstant.MOBILEPHOE))
                    {
                        showToastInfo(R.string.tx_phone_formart);
                        userPhone.setFocusable(true);
                        return;
                    }
                }
                
                getCode.setEnabled(false);
                UserManager.getRegisterCode(telNumber, "pwdCode", new UINotifyListener<Integer>()
                {
                    @Override
                    public void onPreExecute()
                    {
                        super.onPreExecute();
                        titleBar.setRightLodingVisible(true);
                    }
                    
                    @Override
                    public void onError(Object object)
                    {
                        super.onError(object);
                        if (object != null)
                        {
                            showToastInfo(object.toString());
                        }
                        FindPassWordActivity.this.runOnUiThread(new Runnable()
                        {
                            
                            @Override
                            public void run()
                            {
                                titleBar.setRightLodingVisible(false, false);
                            }
                        });
                        getCode.setEnabled(true);
                        
                    }
                    
                    @Override
                    public void onSucceed(Integer result)
                    {
                        super.onSucceed(result);
                        titleBar.setRightLodingVisible(false, false);
                        getCode.setEnabled(true);
                        getCode.setClickable(true);
                        switch (result)
                        {
                        // 成功
                            case 0:
                                Countdown();
                                showToastInfo(R.string.tx_vercode_message);
                                return;
                                // 手机号码以注册
                            case 2:
                                showToastInfo(R.string.tx_vercode_fail);
                                getCode.setEnabled(true);
                                getCode.setClickable(true);
                                getCode.setText(R.string.tx_vercode_again);
                                if (time != null)
                                {
                                    time.onFinish();
                                    time.cancel();
                                    
                                }
                                return;
                            case RequestResult.RESULT_TIMEOUT_ERROR: // 请求超时
                                showToastInfo(R.string.connection_timeout);
                                return;
                                
                        }
                    }
                });
                
            }
        });
    }
    
    /**
     *验证码倒计时60秒
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void Countdown()
    {
        // 总共60 没间隔1秒
        time = new TimeCount(60000, 1000);
        time.start();
    }
    
    class TimeCount extends CountDownTimer
    {
        public TimeCount(long millisInFuture, long countDownInterval)
        {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }
        
        @Override
        public void onFinish()
        {//计时完毕时触发
            getCode.setEnabled(true);
            getCode.setClickable(true);
            getCode.setText(R.string.tx_vercode_again);
        }
        
        @Override
        public void onTick(long millisUntilFinished)
        {//计时过程显示
            getCode.setEnabled(false);
            getCode.setClickable(false);
            getCode.setText(getString(R.string.tx_vercode_again) + millisUntilFinished / 1000
                + getString(R.string.tx_second));
        }
    }
    
    private void findPass(UserModel userModel)
    {
        UserManager.checkData(userModel, new UINotifyListener<Boolean>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                showLoading(false, getString(R.string.tx_confirm_loading));
            }
            
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dismissLoading();
                if (object != null)
                {
                    showToastInfo(object.toString());
                }
            }
            
            @Override
            public void onSucceed(Boolean result)
            {
                super.onSucceed(result);
                dismissLoading();
                if (result)
                {
                    showToastInfo(R.string.tx_modify_succ);
                    showPage(WelcomeActivity.class);
                    finish();
                }
                
            }
        });
    }
    
    /***
     * 检查手机号码格式
     * <功能详细描述>
     * @param mobile
     * @return
     * @see [类、类#方法、类#成员]
     */
    //    private boolean checkMobile(String mobile)
    //    {
    //        Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0-9])|(147))\\d{8}$");
    //        Matcher m = p.matcher(mobile);
    //        boolean flag = m.matches();
    //        if (!flag)
    //        {
    //            return false;
    //        }
    //        
    //        return true;
    //    }
    
    private OnClickListener clearImage = new OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            switch (v.getId())
            {
                case R.id.iv_clearPhone:
                    userPhone.setText("");
                    break;
                case R.id.iv_clearName:
                    shop_name.setText("");
                    break;
                case R.id.iv_clearID:
                    shop_id.setText("");
                    break;
                case R.id.iv_newPwd:
                    newPwd.setText("");
                    break;
                case R.id.iv_repeatPwd:
                    repeatPwd.setText("");
                    break;
                case R.id.iv_mch_id:
                    mch_id.setText("");
                    break;
            }
        }
    };
    
    private void initView()
    {
        newPwd = getViewById(R.id.newPwd);
        
        repeatPwd = getViewById(R.id.repeatPwd);
        
        userPhone = getViewById(R.id.userPhone);
        
        shop_name = getViewById(R.id.shop_name);
        
        shop_id = getViewById(R.id.shop_id);
        
        code = getViewById(R.id.code);
        
        confirm = getViewById(R.id.confirm);
        
        iv_tel_promt = getViewById(R.id.iv_tel_promt);
        iv_id_promt = getViewById(R.id.iv_id_promt);
        
        iv_clearPhone = getViewById(R.id.iv_clearPhone);
        iv_clearName = getViewById(R.id.iv_clearName);
        iv_clearID = getViewById(R.id.iv_clearID);
        iv_shop_promt = getViewById(R.id.iv_shop_promt);
        
        iv_repeatPwd = getViewById(R.id.iv_repeatPwd);
        
        iv_newPwd = getViewById(R.id.iv_newPwd);
        iv_newPwd_promt = getViewById(R.id.iv_newPwd_promt);
        iv_repeatPwd_promt = getViewById(R.id.iv_repeatPwd_promt);
        iv_newPwd.setOnClickListener(clearImage);
        iv_repeatPwd.setOnClickListener(clearImage);
        
        getCode = getViewById(R.id.getCode);
        
        mch_id = getViewById(R.id.mch_id);
        iv_mch_id = getViewById(R.id.iv_mch_id);
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        {
            
            @Override
            public void onExecute(CharSequence s, int start, int before, int count)
            {
            }
            
            @Override
            public void onAfterTextChanged(Editable s)
            {
                if (s != null && s.length() >= 1)
                {
                    if (newPwd.hasFocusable())
                    {
                        if (newPwd.getText().length() > 0)
                        {
                            iv_newPwd.setVisibility(View.VISIBLE);
                            iv_newPwd_promt.setVisibility(View.GONE);
                        }
                        else
                        {
                            iv_newPwd.setVisibility(View.GONE);
                            //                            iv_newPwd_promt.setVisibility(View.VISIBLE);
                        }
                    }
                    
                    if (repeatPwd.hasFocusable())
                    {
                        if (repeatPwd.getText().length() > 0)
                        {
                            iv_repeatPwd.setVisibility(View.VISIBLE);
                            iv_repeatPwd_promt.setVisibility(View.GONE);
                        }
                        else
                        {
                            iv_repeatPwd.setVisibility(View.GONE);
                            //                            iv_repeatPwd_promt.setVisibility(View.VISIBLE);
                        }
                    }
                    
                    if (userPhone.isFocused())
                    {
                        iv_clearPhone.setVisibility(View.VISIBLE);
                        iv_clearName.setVisibility(View.GONE);
                        iv_clearID.setVisibility(View.GONE);
                        
                        iv_tel_promt.setVisibility(View.GONE);
                    }
                    else if (shop_name.isFocused())
                    {
                        iv_clearName.setVisibility(View.VISIBLE);
                        iv_clearPhone.setVisibility(View.GONE);
                        iv_clearID.setVisibility(View.GONE);
                        
                        iv_shop_promt.setVisibility(View.GONE);
                    }
                    else if (shop_id.isFocused())
                    {
                        iv_clearID.setVisibility(View.VISIBLE);
                        iv_clearName.setVisibility(View.GONE);
                        iv_clearPhone.setVisibility(View.GONE);
                        
                        iv_id_promt.setVisibility(View.GONE);
                    }
                }
                else
                {
                    if (userPhone.isFocused())
                    {
                        iv_clearName.setVisibility(View.GONE);
                        iv_clearPhone.setVisibility(View.GONE);
                        iv_clearID.setVisibility(View.GONE);
                        //                        iv_tel_promt.setVisibility(View.VISIBLE);
                    }
                    else if (shop_name.isFocused())
                    {
                        iv_clearName.setVisibility(View.GONE);
                        iv_clearPhone.setVisibility(View.GONE);
                        iv_clearID.setVisibility(View.GONE);
                        //                        iv_shop_promt.setVisibility(View.VISIBLE);
                        
                    }
                    else if (shop_id.isFocused())
                    {
                        iv_clearName.setVisibility(View.GONE);
                        iv_clearPhone.setVisibility(View.GONE);
                        iv_clearID.setVisibility(View.GONE);
                        //                        iv_id_promt.setVisibility(View.VISIBLE);
                        
                    }
                }
                
            }
            
        });
        
        newPwd.addTextChangedListener(editTextWatcher);
        repeatPwd.addTextChangedListener(editTextWatcher);
        
        userPhone.addTextChangedListener(editTextWatcher);
        shop_name.addTextChangedListener(editTextWatcher);
        shop_id.addTextChangedListener(editTextWatcher);
        
        userPhone.setOnFocusChangeListener(listenerFocus);
        shop_name.setOnFocusChangeListener(listenerFocus);
        shop_id.setOnFocusChangeListener(listenerFocus);
        
    }
    
    private final OnFocusChangeListener listenerFocus = new OnFocusChangeListener()
    {
        @Override
        public void onFocusChange(View v, boolean hasFocus)
        {
            switch (v.getId())
            {
                case R.id.userPhone:
                    if (hasFocus)
                    {
                        if (userPhone.getText().length() >= 1)
                        {
                            iv_clearPhone.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            iv_clearPhone.setVisibility(View.GONE);
                        }
                    }
                    else
                    {
                        iv_clearPhone.setVisibility(View.GONE);
                    }
                    break;
                case R.id.shop_name:
                    //                    sp.edit().putString("userPwd", userPwd.getText().toString()).commit();
                    if (hasFocus)
                    {
                        if (shop_name.getText().length() >= 1)
                        {
                            iv_clearName.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            iv_clearName.setVisibility(View.GONE);
                        }
                    }
                    else
                    {
                        iv_clearName.setVisibility(View.GONE);
                    }
                    break;
                case R.id.shop_id:
                    //                    sp.edit().putString("shop_id", shop_id.getText().toString()).commit();
                    if (hasFocus)
                    {
                        if (shop_id.getText().length() >= 1)
                        {
                            iv_clearID.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            iv_clearID.setVisibility(View.GONE);
                        }
                    }
                    else
                    {
                        iv_clearID.setVisibility(View.GONE);
                    }
                    break;
            }
            
        }
    };
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_forget_pwd);
    }
    
}
