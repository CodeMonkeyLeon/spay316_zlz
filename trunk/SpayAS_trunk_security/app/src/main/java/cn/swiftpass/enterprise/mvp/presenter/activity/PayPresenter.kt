package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.bussiness.model.RateInfo
import cn.swiftpass.enterprise.common.Constant
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.PayContract
import cn.swiftpass.enterprise.utils.JsonUtil
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/11/18
 *
 * @小巷又弯又长
 * @没有门没有窗
 * @我拿把旧钥匙
 * @敲着厚厚的墙
 */
class PayPresenter : PayContract.Presenter {

    private var mView: PayContract.View? = null


    override fun getExchangeRate() {
        mView?.let { view ->


            AppClient.getExchangeRate(
                MainApplication.getInstance().getMchId(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<RateInfo>(RateInfo()) {

                    override fun onFailure(call: Call?, error: CommonResponse?) {
                        MainApplication.getInstance().setExchangeRate("1")
                        error?.let { e ->
                            view.getExchangeRateFailed(e.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        r?.let { response ->
                            val rateInfo = JsonUtil.jsonToBean(
                                response.message,
                                RateInfo::class.java
                            ) as RateInfo
                            if (!TextUtils.isEmpty(rateInfo.rate)) {
                                MainApplication.getInstance().setExchangeRate(rateInfo.rate)
                            }
                            if (!TextUtils.isEmpty(rateInfo.vatRate)) {
                                MainApplication.getInstance()
                                    .setVatRate(rateInfo.vatRate.toDouble())
                            }
                            if (!TextUtils.isEmpty(rateInfo.surchargeRate)) {
                                MainApplication.getInstance()
                                    .setSurchargeRate(rateInfo.surchargeRate.toDouble())
                            }
                            if (!TextUtils.isEmpty(rateInfo.taxRate)) {
                                MainApplication.getInstance()
                                    .setTaxRate(rateInfo.taxRate.toDouble())
                            }
                            if (!TextUtils.isEmpty(rateInfo.usdToRmbExchangeRate)) {
                                MainApplication.getInstance()
                                    .setUsdToRmbExchangeRate(rateInfo.usdToRmbExchangeRate.toDouble())
                            }
                            if (!TextUtils.isEmpty(rateInfo.sourceToUsdExchangeRate)) {
                                MainApplication.getInstance()
                                    .setSourceToUsdExchangeRate(rateInfo.sourceToUsdExchangeRate.toDouble())
                            }

                            if (!TextUtils.isEmpty(rateInfo.alipayPayRate)) {
                                MainApplication.getInstance()
                                    .setAlipayPayRate(rateInfo.alipayPayRate.toDouble())
                            }
                            view.getExchangeRateSuccess(true)
                        }
                    }


                }
            )


        }
    }

    override fun getVersionCode() {
//        mView?.let { view ->
//
//            AppClient.getVersionCode(
//                AppHelper.getUpdateVersionCode(MainApplication.getInstance()).toString() + "",
//                BuildConfig.bankCode,
//                System.currentTimeMillis().toString(),
//                object : CallBackUtil.CallBackCommonResponse<UpgradeInfo>(UpgradeInfo()) {
//
//                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
//                        commonResponse?.let { error ->
//                            if ("400" == commonResponse.result || "402" == commonResponse.result) {
//                                view.getVersionCodeSuccess(null)
//                            } else {
//                                view.getVersionCodeFailed(error.result)
//                            }
//                        }
//                    }
//
//
//                    override fun onResponse(r: CommonResponse?) {
//                        r?.let { response ->
//
//                            if (TextUtils.equals(response.result, "200")) {
//                                try {
//                                    val upgradeInfoStr = response.message
//                                    val info = UpgradeInfo()
//                                    val json = JSONObject(upgradeInfoStr)
//                                    info.dateTime =
//                                        Utils.Long.tryParse(json.optString("updated_at"), 0)
//                                    info.message = json.optString("subDesc", "")
//                                    info.version =
//                                        Utils.Integer.tryParse(json.optString("verCode"), 0)
//                                    info.mustUpgrade = Utils.Integer.tryParse(
//                                        json.optString("isUpdate"),
//                                        0
//                                    ) == 0
//                                    info.versionName = json.optString("verName")
//                                    info.fileMd5 = json.optString("fileMd5")
//                                    MainApplication.getInstance().setFileMd5(info.fileMd5)
//                                    info.url = json.optString("filePath", "")
//                                    MainApplication.getInstance().setFileMd5(info.fileMd5)
//                                    view.getVersionCodeSuccess(info)
//                                } catch (e: Exception) {
//                                    SentryUtils.uploadNetInterfaceException(
//                                        e,
//                                        MainApplication.getInstance().baseUrl + "spay/upgrade/checkinVersion",
//                                        e.toString(),
//                                        ""
//                                    )
//                                }
//                            }
//                        }
//                    }
//                }
//            )
//        }
    }

    /**
     * 冲正接口
     */
    override fun payReverse(
        orderNo: String?,
        payType: String?
    ) {

        mView?.let { view ->

            view.showLoading(
                R.string.public_data_loading,
                ParamsConstants.PAY_ACTIVITY_PAY_REVERSE_LOADING
            )

            val service = when (payType) {
                Constant.PAY_WX_MICROPAY -> {
                    "pay.weixin.micropay.reverse"
                }

                Constant.PAY_ZFB_MICROPAY -> {
                    "pay.alipay.micropay.reverse"
                }

                else -> {
                    ""
                }
            }

            AppClient.payReverse(
                orderNo,
                "1",
                service,
                MainApplication.getInstance().getUserId().toString() + "",
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.PAY_ACTIVITY_PAY_REVERSE_LOADING)
                        commonResponse?.let { error ->
                            view.payReverseFailed(error.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.PAY_ACTIVITY_PAY_REVERSE_LOADING)
                        r?.let { response ->
                            //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"",
                                "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                Order::class.java
                            ) as Order
                            order.state = order.tradeState.toString()
                            view.payReverseSuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: PayContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}