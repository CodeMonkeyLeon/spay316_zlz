package com.bigkoo.pickerview.interfaces;

import android.view.View;

import java.util.Date;

public interface OnTimeLongSelectListener {
    void onTimeSelect(Date startDate, Date endDate, View v);
}
