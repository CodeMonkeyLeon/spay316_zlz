package cn.swiftpass.enterprise.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;

import cn.swiftpass.enterprise.MainApplication;

/***
 * 图片工具管理
 */
public class ImageUtil {
    private static final String TAG = ImageUtil.class.getSimpleName();

    public static int getConstellationImgId(Context mContext, String pic) {
        return mContext.getResources().getIdentifier(pic, "drawable", mContext.getPackageName());
    }

    // 生成圆角图片
    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, float radius) {
        try {
            Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
            Canvas canvas = new Canvas(output);
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            final RectF rectF = new RectF(new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()));
            final float roundPx = radius;
            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(Color.WHITE);
            canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
            paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
            final Rect src = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            canvas.drawBitmap(bitmap, src, rect, paint);
            return output;
        } catch (Exception e) {
            return bitmap;
        }
    }

    /**
     * 将图片变为灰度图
     *
     * @param old
     * @return
     */
    public static Bitmap getGreyImage(Bitmap old) {
        int width, height;
        height = old.getHeight();
        width = old.getWidth();
        Bitmap grayImg = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        Canvas c = new Canvas(grayImg);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(old, 0, 0, paint);
        return grayImg;
    }

    /**
     * @param bitmap 需要处理的图像
     * @param outImg 相框
     *               相框与图像叠加的偏移量。X轴方向。
     * @return 合成后的图像
     * @author huangdonghua
     * @see 合成相框方法。会根据相框宽度拉伸或者压缩来源图片。 GlobalConstant.java中有常用形式的偏移量。
     */
    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, Bitmap outImg, int x, int y, float density) {

        if (bitmap == null || outImg == null) return null;

        int offsetX = (int) (x * density + 0.5f);
        int offsetY = (int) (y * density + 0.5f);

        int width = outImg.getWidth();
        int height = outImg.getHeight();
        //float roundPx = (width - offsetX * 2) / 2;

        // 创建画布。以相框大小为基准。
        Bitmap output = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        // 剪裁圆形区域
        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect src = new Rect(0, 0, bitmap.getWidth(), bitmap.getWidth()); // 来源图片尺寸
        final Rect dst = new Rect(offsetX, offsetY, width - offsetX, width - offsetX * 2 + offsetY); // 需要最终大小的尺寸
        final RectF rectF = new RectF(dst);
        paint.setAntiAlias(true);

        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        //canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawBitmap(bitmap, src, rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, src, dst, paint);

        // 添加相框
        final Rect outR = new Rect(0, 0, width, height);
        final Paint outP = new Paint();
        outP.setXfermode(new PorterDuffXfermode(Mode.SRC_OVER));
        canvas.drawBitmap(outImg, outR, outR, outP);

        return output;
    }

    /**
     * 放大图
     *
     * @param bitmap
     * @param width
     * @param height
     * @return
     */
    public static Bitmap magnifyBitmap(Bitmap bitmap, int width, int height) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        int sw = width;
        int sh = height;
        if (w * h > width * height) {
            sw = width;
            sh = height;
        }
        Matrix matrix = new Matrix();
        matrix.postScale((float) sw / w, (float) sh / h);
        return Bitmap.createBitmap(bitmap, 0, 0, w, h, matrix, true);
    }

    
   /* public static Bitmap decodeFile2(String imageFile)
    {
        BitmapFactory.Options opts;
        try
        {
            if (!new File(imageFile).exists())
            {
                return null;
            }
            opts = new BitmapFactory.Options();
            opts.inJustDecodeBounds = true;
            Bitmap bitmap = BitmapFactory.decodeFile(imageFile);
            return bitmap;
        }
        catch (Exception e)
        {

            Log.e(TAG,Log.getStackTraceString(e));
            opts = new BitmapFactory.Options();
            opts.inSampleSize = 2;
            Bitmap bitmap = BitmapFactory.decodeFile(imageFile, opts);
            return bitmap;
        }
        
    }*/

//    private static int computeSampleSize(BitmapFactory.Options options, int minSideLength, int maxNumOfPixels) {
//        int initialSize = computeInitialSampleSize(options, minSideLength, maxNumOfPixels);
//        int roundedSize;
//        if (initialSize <= 8) {
//            roundedSize = 1;
//            while (roundedSize < initialSize) {
//                roundedSize <<= 1;
//            }
//        } else {
//            roundedSize = (initialSize + 7) / 8 * 8;
//        }
//        return roundedSize;
//    }

//    private static int computeInitialSampleSize(BitmapFactory.Options options, int minSideLength, int maxNumOfPixels) {
//        double w = options.outWidth;
//        double h = options.outHeight;
//        int lowerBound = (maxNumOfPixels == -1) ? 1 : (int) Math.ceil(Math.sqrt(w * h / maxNumOfPixels));
//        int upperBound = (minSideLength == -1) ? 128 : (int) Math.min(Math.floor(w / minSideLength), Math.floor(h / minSideLength));
//        if (upperBound < lowerBound) {
//            return lowerBound;
//        }
//        if ((maxNumOfPixels == -1) && (minSideLength == -1)) {
//            return 1;
//        } else if (minSideLength == -1) {
//            return lowerBound;
//        } else {
//            return upperBound;
//        }
//    }

    /**
     * 比例缩小图片
     *
     * @param f
     * @param isInSample
     * @param requiredSize
     * @return
     */
   /* public static Bitmap decodeFile(File f, boolean isInSample, int requiredSize)
    {
        if (!f.exists() || f.length() == 0)
            return null;
        try
        {
            int scale = 1;
            if (isInSample)
            {
                // decode image size
                BitmapFactory.Options o = new BitmapFactory.Options();
                o.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(new FileInputStream(f), null, o);
                
                // Find the correct scale value. It should be the power of 2.
                int REQUIRED_SIZE = requiredSize;
                int width_tmp = o.outWidth, height_tmp = o.outHeight;
                while (true)
                {
                    if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                        break;
                    width_tmp /= 2;
                    height_tmp /= 2;
                    scale *= 2;
                }
            }
            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        }
        catch (FileNotFoundException e)
        {
            Log.e(TAG,Log.getStackTraceString(e));
        }
        catch (Exception e)
        {
            Log.e(TAG,Log.getStackTraceString(e));
        }
        return null;
    }*/

    /**
     * 适配缩放图片大小
     *
     * @param bitmap
     * @param width
     * @param height
     * @return
     */
    public static Bitmap zoomAdjustBitmap(Bitmap bitmap, int width, int height) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        int sw = w > width ? width : w;
        int sh = h > height ? height : h;
        if (w * h > width * height) {
            sw = width;
            sh = height;
        }
        Matrix matrix = new Matrix();
        matrix.postScale((float) sw / w, (float) sh / h);
        return Bitmap.createBitmap(bitmap, 0, 0, w, h, matrix, true);
    }

    /**
     * 将二进制数组保存为bitmap
     */
    public static Bitmap BytesToBimap(byte[] b) {
        if (b.length != 0) {
            return BitmapFactory.decodeByteArray(b, 0, b.length);
        } else {
            return null;
        }
    }

    /**
     * 下载图片
     */
    
   /* public static Bitmap getHttpLoadPicToBitmap(String url, String name)
    {
        URL myFileUrl = null;
        Bitmap bitmap = null;
        try
        {
            myFileUrl = new URL(url);
        }
        catch (MalformedURLException e)
        {
            Log.e(TAG,Log.getStackTraceString(e));
        }
        try
        {
            HttpURLConnection conn = (HttpURLConnection)myFileUrl.openConnection();
            conn.setConnectTimeout(0);
            conn.setDoInput(true);
            conn.connect();
            InputStream is = conn.getInputStream();
            bitmap = BitmapFactory.decodeStream(is);
            if (bitmap != null)
            {
                String path = AppHelper.getImgCacheDir() + name + ".jpg";
                try
                {
                    saveSimpleImag(path, bitmap, Bitmap.CompressFormat.JPEG);
                }
                catch (Exception e)
                {
                }
            }
            is.close();
        }
        catch (Exception e)
        {
            Log.e(TAG,Log.getStackTraceString(e));
        }
        return bitmap;
    }*/

    /**
     * 保存图片到指定文件
     */
    public static boolean saveSimpleImag(String imgFile, Bitmap bitmap, CompressFormat format) {
        File file = new File(imgFile);
        BufferedOutputStream dos = null;
        try {
            dos = new BufferedOutputStream(new FileOutputStream(file));

            return bitmap.compress(format, 100, dos);
        } catch (FileNotFoundException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        } finally {
            if (dos != null) {
                try {
                    dos.close();
                } catch (IOException e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
                dos = null;
            }
        }
        return false;
    }

    /**
     * 保存图片到指定文件
     */
    public static boolean saveImag2(String imgFile, Bitmap bitmap, CompressFormat format) {
        File file = new File(imgFile);
        BufferedOutputStream dos = null;
        try {
            dos = new BufferedOutputStream(new FileOutputStream(file));

            int w = bitmap.getWidth();
            int h = bitmap.getHeight();

            Bitmap newb = Bitmap.createBitmap(w, h, Config.ARGB_8888);// 创建一个新的和SRC长度宽度一
            newb.setDensity(bitmap.getDensity());

            Canvas canvas = new Canvas(newb);
            canvas.drawColor(Color.WHITE);
            canvas.drawBitmap(bitmap, 0, 0, null); // 通过Canvas绘制Bitmap
            bitmap = newb;

            // 画背景
            return bitmap.compress(format, 100, dos);
        } catch (FileNotFoundException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        } finally {
            if (dos != null) {
                try {
                    dos.close();
                } catch (IOException e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
                dos = null;
            }
        }
        return false;
    }

    /**
     * 保存图片到指定文件
     */
    public static boolean saveImag(String imgFile, Bitmap bitmap, CompressFormat format) {
        File file = new File(imgFile);
        BufferedOutputStream dos = null;
        try {
            dos = new BufferedOutputStream(new FileOutputStream(file));

            int w = bitmap.getWidth();
            int h = bitmap.getHeight();

            Bitmap newb = Bitmap.createBitmap(w + 100, h + 100, Config.ARGB_8888);// 创建一个新的和SRC长度宽度一
            newb.setDensity(bitmap.getDensity());

            Canvas canvas = new Canvas(newb);
            canvas.drawColor(Color.WHITE);
            canvas.drawBitmap(bitmap, 0, 10, null); // 通过Canvas绘制Bitmap
            bitmap = newb;

            // 画背景
            return bitmap.compress(format, 100, dos);
        } catch (FileNotFoundException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        } finally {
            if (dos != null) {
                try {
                    dos.close();
                } catch (IOException e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
                dos = null;
            }
        }
        return false;
    }

    /**
     * 检查合法后缀名
     *
     * @param filePath
     * @return
     */
    /*public static String getFileSuffix(String filePath)
    {
        String[] suffix =
            new String[] {".amr", ".rar", ".txt", ".mp3", ".mp4", ".3gp", ".jpg", ".ico", ".png", ".gif", ".zip",
                ".pdf", ".xml", ".apk", ".log"};
        String ret = filePath.trim().toLowerCase();
        for (int i = 0; i < suffix.length; i++)
        {
            if (ret.endsWith(suffix[i]))
            {
                return suffix[i];
            }
        }
        return null;
    }*/

    /**
     * bitmap转换stream
     *
     * @param bitmap
     * @return
     */
    public static byte[] getBitmapStream(Bitmap bitmap) {

        return getBitmapStream(bitmap, CompressFormat.PNG, 100);
    }

    /**
     * bitmap转换stream
     *
     * @param bitmap
     * @return
     */
    public static byte[] getBitmapStream(Bitmap bitmap, CompressFormat paramCompressFormat, int quality) {

        if (bitmap != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(paramCompressFormat, quality, stream);
            byte[] byteArray = stream.toByteArray();
            return byteArray;
        } else {
            return null;
        }
    }

    /**
     * 获取屏幕装饰栏的大小
     */
    public static int[] getScreenTitleWidthHiehgt(Context mContext) {
        int[] d = new int[2];
        try {
            Class c = Class.forName("com.android.internal.R$dimen");
            Object obj;
            obj = c.newInstance();
            Field field = c.getField("status_bar_height");
            int x = Integer.parseInt(field.get(obj).toString());
            int y = mContext.getResources().getDimensionPixelSize(x);
            d[0] = x;
            d[1] = y;
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return d;
    }

    /**
     * 转换bitmap
     */
    public static Bitmap drawableToBitmap(Drawable drawable) {
        // 取 drawable 的长宽
        int w = drawable.getIntrinsicWidth();
        int h = drawable.getIntrinsicHeight();
        // 取 drawable 的颜色格式
        Config config = drawable.getOpacity() != PixelFormat.OPAQUE ? Config.ARGB_8888 : Config.RGB_565;
        // 建立对应 bitmap
        Bitmap bitmap = Bitmap.createBitmap(w, h, config);
        // 建立对应 bitmap 的画布
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, w, h);
        // 把 drawable 内容画到画布中
        drawable.draw(canvas);
        return bitmap;
    }

    /**
     * 合并 图
     **/
   /* public static Bitmap createBitmap(Bitmap src, Bitmap watermark, Bitmap b2) {
        //String tag = "createBitmap";
        // Log.d(tag, "create a new bitmap");
        if (src == null) {
            return null;
        }

        int w = src.getWidth();
        int h = src.getHeight();
        int ww = watermark.getWidth();
        //int wh = watermark.getHeight();
        // create the new blank bitmap
        Bitmap newb = Bitmap.createBitmap(w, h, Config.ARGB_8888);// 创建一个新的和SRC长度宽度一样的位图
        Canvas cv = new Canvas(newb);
        // draw src into
        cv.drawBitmap(src, 0, 0, null);// 在 0，0坐标开始画入src
        // draw watermark into
        // 在src的右下角画watermark
        //cv.drawBitmap(watermark, w - ww - 5, h - wh - 5, null);// 设置ic_launcher的位置
        // cv.drawBitmap(watermark, 0, 0, null);
        // save all clip
        cv.drawBitmap(watermark, 0, h / 3, null);//设置ic_launcher的位置
        cv.drawBitmap(b2, w - ww, h / 3, null);//设置ic_launcher的位置
        cv.save(Canvas.ALL_SAVE_FLAG);// 保存
        // store
        cv.restore();// 存储
        return newb;

    }
*/
    /**
     * 缩小图
     */
    public static Bitmap zoomBitmap(Bitmap src, int destWidth, int destHeigth) {
        //String tag = "lessenBitmap";
        if (src == null) {
            return null;
        }

        int w = src.getWidth();// 源文件的大小
        int h = src.getHeight();

        // calculate the scale - in this case = 0.4f
        float scaleWidth = ((float) destWidth) / w;// 宽度缩小比例
        float scaleHeight = ((float) destHeigth) / h;// 高度缩小比例

        Matrix m = new Matrix();// 矩阵
        m.postScale(scaleWidth, scaleHeight);// 设置矩阵比例
        Bitmap resizedBitmap = Bitmap.createBitmap(src, 0, 0, w, h, m, true);// 直接按照矩阵的比例把源文件画入进行
        return resizedBitmap;
    }

    /**
     * 压缩图
     */
    public static Bitmap transImage(String fromFile, int width, int height) {
        Bitmap bitmap = null;
        Bitmap resizeBitmap = null;
        try {

            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(fromFile, opts);
            bitmap = BitmapFactory.decodeFile(fromFile);
            if (opts.outWidth * opts.outHeight < width * height) {
                // 指定太大无法压缩，原图返回
                return bitmap;
            }

            int bitmapWidth = bitmap.getWidth();
            int bitmapHeight = bitmap.getHeight();
            // 缩放图片的尺寸
            float scaleWidth = (float) width / bitmapWidth;
            float scaleHeight = (float) height / bitmapHeight;
            Matrix matrix = new Matrix();
            matrix.postScale(scaleWidth, scaleHeight);
            // 产生缩放后的Bitmap对象
            resizeBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmapWidth, bitmapHeight, matrix, true);
            return resizeBitmap;
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
            if (bitmap != null && !bitmap.isRecycled()) {
                // 记得释放资源，否则会内存溢出
                bitmap.recycle();
                bitmap = null;
            }
            return null;
        }
    }

    /**
     * 根据intent得到文件路径
     *
     * @param data
     * @param context
     * @return
     */
    public static String getFilePath(Intent data, Context context) {
        ContentResolver cr = null;
        Uri uri = null;
        Cursor cursor = null;
        if (null != data) {
            uri = data.getData();
            String path = uri.getPath();
            if (path.lastIndexOf(".") == -1) {
                cr = context.getContentResolver();
                cursor = cr.query(uri, null, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();
                    return cursor.getString(1);// 得到多媒体路径
                }
            } else {
                return path;
            }
        }
        return null;
    }

    public static Bitmap getimage(String srcPath, int target) {
        Bitmap sourceBitmap = null;
        do {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(srcPath, options);
            if (options.mCancel || options.outWidth == -1 || options.outHeight == -1) {
                return null;
            }
            options.inSampleSize = computeSampleSize(options, target);
            options.inJustDecodeBounds = false;
            options.inDither = false;

            // options.inPreferredConfig = Bitmap.Config.ARGB_4444;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            InputStream is = null;
            try {
                is = new FileInputStream(srcPath);
                sourceBitmap = BitmapFactory.decodeStream(is, null, options);
                options = null;
                if (null == sourceBitmap) {
                    break;
                }
                int width = sourceBitmap.getWidth();
                int height = sourceBitmap.getHeight();
                int max = Math.max(width, height);
                if (max > target) {
                    Matrix matrix = new Matrix();
                    float scale = ((float) target) / max;
                    matrix.postScale(scale, scale);
                    sourceBitmap = Bitmap.createBitmap(sourceBitmap, 0, 0, width, height, matrix, true);
                }
            } catch (OutOfMemoryError e) {
                try {
                    if (is != null) {
                        is.close();
                        is = null;
                    }

                } catch (IOException ex) {
                    // TODO Auto-generated catch block
                    Log.e(TAG, Log.getStackTraceString(ex));
                }
                if (null != sourceBitmap && !sourceBitmap.isRecycled()) {
                    sourceBitmap.recycle();
                    sourceBitmap = null;
                }
                Log.e(TAG, Log.getStackTraceString(e));
            } catch (FileNotFoundException e) {
                try {
                    if (is != null) {
                        is.close();
                        is = null;
                    }
                } catch (IOException ex) {
                    Log.e(TAG, Log.getStackTraceString(ex));
                }
                if (null != sourceBitmap && !sourceBitmap.isRecycled()) {
                    sourceBitmap.recycle();
                    sourceBitmap = null;
                }
                Log.e(TAG, Log.getStackTraceString(e));
            } finally {
                try {
                    if (is != null) {
                        is.close();
                        is = null;
                    }
                } catch (IOException e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }

            }
        } while (false);
        return sourceBitmap;
    }

    /**
     * 计算缩放比
     *
     * @param options
     * @param target
     * @return
     */
    public static int computeSampleSize(BitmapFactory.Options options, int target) {
        int w = options.outWidth;
        int h = options.outHeight;
        int candidateW = w / target;
        int candidateH = h / target;

        int candidate = Math.max(candidateW, candidateH);
        if (candidate == 0) return 1;
        return candidate;
    }

    public static Bitmap compressImage(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);//质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        int options = 100;
        while (baos.toByteArray().length / 1024 > 100) { //循环判断如果压缩后图片是否大于100kb,大于继续压缩
            baos.reset();//重置baos即清空baos
            options -= 10;//每次都减少10
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);//这里压缩options%，把压缩后的数据存放到baos中

        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());//把压缩后的数据baos存放到ByteArrayInputStream中
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);//把ByteArrayInputStream数据生成图片
        return bitmap;
    }

    /**
     * 从文件中读取图片,引导页
     *
     * @param picPath 文件存放路径
     */
    public static Bitmap readBitmapFromStream(String picPath) {
        Bitmap sourceBitmap = null;
        if (TextUtils.isEmpty(picPath)) {
            sourceBitmap = null;
        }
        do {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(picPath, options);
            if (options.mCancel || options.outWidth == -1 || options.outHeight == -1) {
                return null;
            }
            options.inJustDecodeBounds = false;
            options.inDither = false;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            InputStream is = null;
            try {
                is = new FileInputStream(picPath);
                sourceBitmap = BitmapFactory.decodeStream(is, null, options);
                options = null;
                if (null == sourceBitmap) {
                    break;
                }
            } catch (OutOfMemoryError e) {
                try {
                    if (is != null) {
                        is.close();
                        is = null;
                    }
                } catch (IOException ex) {
                    Log.e(TAG, Log.getStackTraceString(ex));
                }
                if (null != sourceBitmap && !sourceBitmap.isRecycled()) {
                    sourceBitmap.recycle();
                    sourceBitmap = null;
                }
                Log.e(TAG, Log.getStackTraceString(e));
            } catch (FileNotFoundException e) {
                if (null != sourceBitmap && !sourceBitmap.isRecycled()) {
                    sourceBitmap.recycle();
                    sourceBitmap = null;
                }
                Log.e(TAG, Log.getStackTraceString(e));
            } finally {
                try {
                    if (is != null) {
                        is.close();
                        is = null;
                    }
                } catch (IOException e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
        } while (false);
        return sourceBitmap;
    }

    public static Bitmap adjustPhotoRotation(Bitmap bm, final int orientationDegree) {
        Matrix m = new Matrix();
        m.setRotate(orientationDegree, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);

        try {
            Bitmap bm1 = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m, true);
            return bm1;
        } catch (OutOfMemoryError ex) {
            Log.e(TAG, Log.getStackTraceString(ex));
        }
        return null;
    }

    public static String saveViewBitmapFile(String fileName, View v) {
        Bitmap bitmap = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        v.draw(canvas);
        File file;
        FileOutputStream out = null;
        try {
            File dir = new File(getSDcardPath() + "/swiftpass_pay", "DCIM");
            if (!dir.exists()) {
                boolean isSuccess = dir.mkdirs();
                //TODO FIX ZHANGXINCHAO 主要是解决扫描漏洞 不处理返回值的问题
                if (isSuccess) {

                } else {

                }
            }
            file = new File(dir, fileName + ".jpg");
            out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
            MediaScannerConnection.scanFile(MainApplication.getContext(), new String[]{file.getPath()}, null, null);

            return file.getPath();
        } catch (FileNotFoundException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        } catch (IOException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        } finally {
            if (!bitmap.isRecycled()) {
                bitmap.recycle();
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ioe) {
                    Log.e(TAG, Log.getStackTraceString(ioe));
                }
            }
        }
        return null;
    }
    
   /* public static String saveViewBitmapFile(View v)
    {
        String fileName = "QCODE_" + Utils.formatYM(new Date().getTime()) + ".jpg";
        return saveViewBitmapFile(fileName, v);
    }*/

    public static String getSDcardPath() {
        String state = Environment.getExternalStorageState();
        if (TextUtils.equals(Environment.MEDIA_MOUNTED,state)) {
            File sdcardDir = Environment.getExternalStorageDirectory();
            return sdcardDir.getPath();
        }
        return null;
    }

    /**
     * 将字符互传转换成Bitmap
     * @param opts
     * @return Bitmap
     */
    public static Bitmap getPicFromBytes(String strPic, BitmapFactory.Options opts) {
        if (TextUtils.isEmpty(strPic)){
            return null;
        }
        byte[] pic = strPic.getBytes();
        if (pic != null) {
            if (opts != null) {
                return BitmapFactory.decodeByteArray(pic, 0, pic.length,
                        opts);
            } else {
                return BitmapFactory.decodeByteArray(pic, 0, pic.length);
            }
        }
        return null;
    }

    /**
     * base64转为bitmap
     *
     * @param base64Data
     * @return
     */
    public static Bitmap base64ToBitmap(String base64Data) {
        byte[] bytes = Base64.decode(base64Data, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }
}
