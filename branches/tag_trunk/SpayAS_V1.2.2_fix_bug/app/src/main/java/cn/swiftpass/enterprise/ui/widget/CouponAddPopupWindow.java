package cn.swiftpass.enterprise.ui.widget;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import cn.swiftpass.enterprise.intl.R;

/**
 * 添加优惠券指引
 * User: Alan
 * Date: 14-2-28
 * Time: 下午8:56
 * To change this template use File | Settings | File Templates.
 */
public class CouponAddPopupWindow extends PopupWindow
{
    private View view;
    
    private OnClikPopListener onClickPopListener;
    
    public CouponAddPopupWindow(Context context, final int idx)
    {
        super(context);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ColorDrawable dw = null;
        if (idx == 1)
        {
            view = inflater.inflate(R.layout.popupindow_add_coupon, null);
            dw = new ColorDrawable(0xb0000000);//#8c8c8c  0xb0000000
        }
        else if (idx == 2)
        {
            view = inflater.inflate(R.layout.popupindow_coupon, null);
            dw = new ColorDrawable(0);//#8c8c8c  0xb0000000
        }
        this.setContentView(view);
        this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setFocusable(true);//可点击
        //实例化一个ColorDrawable颜色为半透明
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        view.setOnTouchListener(new View.OnTouchListener()
        {
            
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                dismiss();
                if (idx == 2)
                {
                    onClickPopListener.onClikPop();
                }
                return true;
            }
        });
    }
    
    public void setOnClickPopListener(OnClikPopListener onClickPopListener)
    {
        this.onClickPopListener = onClickPopListener;
    }
    
    public interface OnClikPopListener
    {
        public void onClikPop();
    }
    
}
