package cn.swiftpass.enterprise.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.enums.OrderStatusEnum;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.RefundModel;
import cn.swiftpass.enterprise.utils.DateUtil;

/**
 * 订单 退款信息
 * User: Alan
 * Date: 13-9-28
 * Time: 上午10:17
 * To change this template use File | Settings | File Templates.
 */
public class ItemOrder extends LinearLayout
{
    private TextView tvDateTime, tvOrderNo, tvMoney, tvBank;
    
    public ItemOrder(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
    
    @Override
    protected void onFinishInflate()
    {
        tvDateTime = (TextView)findViewById(R.id.tv_3);
        tvOrderNo = (TextView)findViewById(R.id.tv_1);
        tvMoney = (TextView)findViewById(R.id.tv_2);
        tvBank = (TextView)findViewById(R.id.tv_4);
    }
    
    public void setData(Order orderModel)
    {
        if (orderModel != null)
        {
            
            if (orderModel.tradeState.equals(OrderStatusEnum.PAY_SUCCESS.getValue()))//&& orderModel.transactionType == PayType.PAY_WX.getValue()
            {
                
                if (orderModel.notifyTime != null)
                {
                    tvDateTime.setText(DateUtil.formatTimeUtil(Long.parseLong(orderModel.notifyTime)));
                }
                else
                {
                    tvDateTime.setText("");
                }
            }
            else
            {
                tvDateTime.setText(DateUtil.formatTimeUtil(Long.parseLong(orderModel.addTime)));
            }
            String id = orderModel.outTradeNo;
            tvOrderNo.setText(id.substring(12));
            double money = orderModel.money / 100d;
            tvMoney.setText("¥ " + DateUtil.formatMoneyUtil(money));
            tvMoney.setVisibility(VISIBLE);
            //String bankName = orderModel.bankTypeDisplay;//原来
            //            Integer bankName = orderModel.transactionType;
            //            if (bankName.intValue() == PayType.PAY_POS.getValue())
            //            {
            //                tvBank.setText(PayType.PAY_POS.getDisplayName());
            //            }
            
            if (orderModel.getTradeType().equalsIgnoreCase("pay.qq.jspay"))
            {
                tvBank.setText("手Q扫码支付");
            }
            else if (orderModel.getTradeType().equalsIgnoreCase("pay.alipay.native"))
            {
                tvBank.setText("支付宝扫码支付");
            }
            else if (orderModel.getTradeType().equalsIgnoreCase("pay.weixin.native"))
            {
                tvBank.setText("微信扫码支付");
            }
            else if (orderModel.getTradeType().equalsIgnoreCase("pay.weixin.micropay"))
            {
                tvBank.setText("微信刷卡支付");
            }
            else if (orderModel.getTradeType().equalsIgnoreCase(MainApplication.PAY_ZFB_MICROPAY))
            {
                tvBank.setText(orderModel.getTradeName());
            }
            else if (orderModel.getTradeType().equals(MainApplication.PAY_ZFB_MICROPAY))
            {
                tvBank.setText("支付宝付款码支付");
            }
            else
            {
                tvBank.setText("微信支付");
            }
            
            //            if (!"".equals(orderModel.tradeName) && null != orderModel.tradeName)
            //            {
            //                tvBank.setText(orderModel.tradeName);
            //            }
            //            else
            //            {
            //                tvBank.setText(PayType.PAY_WX.getDisplayName());
            //            }
            //            else if (bankName.intValue() == PayType.PAY_WX.getValue())
            //            {
            //                tvBank.setText(PayType.PAY_WX.getDisplayName());
            //            }
            tvBank.setVisibility(VISIBLE);
        }
    }
    
    public void setRefundData(RefundModel refund)
    {
        if (refund != null)
        {
            String id = refund.orderNo;
            if (id.length() >= 30)
            {
                tvOrderNo.setText(id.substring(22, id.length()));
            }
            tvDateTime.setText(DateUtil.formatTimeMMSS(refund.addTime.getTime()));
            double money = refund.totalFee / 100d;
            tvMoney.setText("¥ " + money);
            tvMoney.setVisibility(VISIBLE);
        }
    }
    
}
