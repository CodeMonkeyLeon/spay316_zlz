package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.ForgetPSWBean
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView


/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @我想在大地上画满窗子
 * @让所有习惯黑暗的眼睛
 * @都习惯光明
 */
class FindPassFirstContract {


    interface View : BaseView {

        fun checkForgetPwdInputSuccess(response: ForgetPSWBean?)

        fun checkForgetPwdInputFailed(error: Any?)

    }


    interface Presenter : BasePresenter<View> {
        fun checkForgetPwdInput(input: String?)
    }


}