package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-10-27
 * Time: 下午8:04
 * To change this template use File | Settings | File Templates.
 */
public class GoodsMode implements Serializable
{
    private static final long serialVersionUID = 1L;

    //{
    //	"id": 12186,
    //	"mchId": "100580000055",
    //	"mchName": "",
    //	"remark": "",
    //	"commodityName": "www",
    //	"commodityNum": "1",
    //	"createdAd": 1651087059000,
    //	"updateTime": 1529390642000,
    //	"goodsDetail": "{\"goods_detail\":[{\"goods_name\":\"www\",\"quantity\":1}]}"
    //}
    public String id;
    public String mchId;
    public String mchName;
    public String remark;
    public String commodityName;
    public String commodityNum;
    public String createdAd;
    public String updateTime;
}
