/**
 * @description:
 * @author chenshiqiang E-mail:csqwyyx@163.com
 * @date 2012-12-24 上午09:00:56   
 * @version 1.0
 * copyrights reserved by Petfone 2007-2011   
 */
package cn.swiftpass.enterprise.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.TextUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ApkUtil
{
    
    public static boolean launchApk(Context context, String pkg)
    {
        PackageManager packageManager = context.getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage(pkg);
        if (intent != null)
        {
            context.startActivity(intent);
            return true;
        }
        else
        {
            ToastHelper.showInfo("没安装不能启动！");
            return false;
        }
    }

//    @Override
//    public boolean equals(Object obj) {
//        return super.equals(obj);
//    }

    /**安装apk*/
    public static void onInstallApk(Context context, String apkPath)
    {
        context.startActivity(getApkInstallIntent(context, apkPath));
    }
    
    public static Intent getApkInstallIntent(Context context, String strApkPath)
    {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(new File(strApkPath)), "application/vnd.android.package-archive");
        //去掉Intent.FLAG_ACTIVITY_NEW_TASK，解决按Home下次进入会弹出以前安装任务的bug
        //        if (!(context instanceof Activity))
        //        {
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //        }
        return intent;
    }
    
    /**获取已安装apk*/
    public static List<PackageInfo> getAllApps(Context context)
    {
        List<PackageInfo> apps = new ArrayList<PackageInfo>();
        PackageManager pManager = context.getPackageManager();
        //获取手机内所有应用   
        List<PackageInfo> paklist = pManager.getInstalledPackages(0);
        for (int i = 0; i < paklist.size(); i++)
        {
            PackageInfo pak = (PackageInfo)paklist.get(i);
            //判断是否为非系统预装的应用程序   
            if ((pak.applicationInfo.flags & pak.applicationInfo.FLAG_SYSTEM) <= 0)
            {
                // customs applications
                //pak.applicationInfo.packageName
                
                apps.add(pak);
            }
        }
        return apps;
    }
    
    /**获取已安装apk*/
    public static boolean isInstallMyApp(Context context, String packageName)
    {
        boolean result = false;
        PackageManager pManager = context.getPackageManager();
        //获取手机内所有应用   
        List<PackageInfo> paklist = pManager.getInstalledPackages(0);
        for (int i = 0; i < paklist.size(); i++)
        {
            PackageInfo pak = (PackageInfo)paklist.get(i);
            //判断是否为非系统预装的应用程序
            if ((pak.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) <= 0)
            {
                if (TextUtils.equals(pak.packageName,packageName))
                {
                    result = true;
                }
            }
        }
        return result;
    }
    
    public static List<String> getInstallApp(Context context)
    {
        List<PackageInfo> list = getAllApps(context);
        List<String> data = new ArrayList<String>();
        for (PackageInfo p : list)
        {
            data.add(p.applicationInfo.packageName);
        }
        return data;
        
    }
    
    /**自己程序是否已经启用*/
    public static boolean isRunApp(Context context)
    {
        ActivityManager am = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(3);
        //PackageManager pManager = context.getPackageManager();
        
        for (ActivityManager.RunningTaskInfo info : list)
        {
            if (TextUtils.equals(info.topActivity.getPackageName(),context.getPackageName()))
            {
                return true;
            }
        }
        return false;
        
    }


    public static String getProcessName(Context cxt, int pid) {
        ActivityManager am = (ActivityManager) cxt.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningApps = am.getRunningAppProcesses();
        if (runningApps == null) {
            return null;
        }
        for (ActivityManager.RunningAppProcessInfo procInfo : runningApps) {
            if (procInfo.pid == pid) {
                return procInfo.processName;
            }
        }
        return null;
    }

    /**
     * 判断当前设置是否是中文
     * @param context
     * @return
     */
    public static  boolean isZhLanguage(Context context) {
        Locale locale = context.getResources().getConfiguration().locale;
        String language = locale.getLanguage();
        if (language.endsWith("zh"))
            return true;
        else
            return false;
    }
    
}
