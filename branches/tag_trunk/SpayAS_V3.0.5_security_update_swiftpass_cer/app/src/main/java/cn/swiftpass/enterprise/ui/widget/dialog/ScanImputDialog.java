package cn.swiftpass.enterprise.ui.widget.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

public class ScanImputDialog extends Dialog
{
    private EditText edit_input;
    
    private TextView ok, cancel;
    
    private ConfirmListener confirmListener;
    
    Activity context;
    
    public ScanImputDialog(Activity context, ConfirmListener confirmListener)
    {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_input_scan);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        this.setCanceledOnTouchOutside(false);
        this.confirmListener = confirmListener;
        this.context = context;
        initView();
        
        setLister();
    }
    
    private void setLister()
    {
        // TODO Auto-generated method stub
        cancel.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });
        
        ok.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (StringUtil.isEmptyOrNull(edit_input.getText().toString()))
                {
                    ToastHelper.showInfo(context, ToastHelper.toStr(R.string.tv_input_code_null));
                    return;
                }
                dismiss();
                confirmListener.ok(edit_input.getText().toString());
            }
        });
    }
    
    private void initView()
    {
        edit_input = (EditText)findViewById(R.id.edit_input);
        ok = (TextView)findViewById(R.id.ok);
        cancel = (TextView)findViewById(R.id.cancel);
    }
    
    public interface ConfirmListener
    {
        public void ok(String code);
        
    }
    
}
