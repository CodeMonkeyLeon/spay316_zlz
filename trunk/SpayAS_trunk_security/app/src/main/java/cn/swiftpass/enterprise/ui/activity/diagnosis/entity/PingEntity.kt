package cn.swiftpass.enterprise.ui.activity.diagnosis.entity

import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.utils.KotlinUtils
import java.io.Serializable

data class PingEntity(
    var sendPackage: String = "",
    var receivePackage: String = "",
    var lostRate: String = "",
    var minRtt: String = "",
    var maxRtt: String = "",
    var aveRtt: String = ""
) : Serializable {


    fun copy(e: PingEntity) {
        sendPackage = e.sendPackage
        receivePackage = e.receivePackage
        lostRate = e.lostRate
        minRtt = e.minRtt
        maxRtt = e.maxRtt
        aveRtt = e.aveRtt
    }


    fun dnsAnalysisFailed() {
        sendPackage = ""
        receivePackage = ""
        lostRate = ""
        minRtt = ""
        maxRtt = ""
        aveRtt = ""
    }


    fun error() {
        sendPackage = "error"
        receivePackage = "error"
        lostRate = "error"
        minRtt = "error"
        maxRtt = "error"
        aveRtt = "error"
    }


    fun notReachable(ip: String) {
        sendPackage = "$ip is not reachable"
        receivePackage = "$ip is not reachable"
        lostRate = "$ip is not reachable"
        minRtt = "$ip is not reachable"
        maxRtt = "$ip is not reachable"
        aveRtt = "$ip is not reachable"
    }


    private val tvSendPackage by lazy {
        KotlinUtils.getString(R.string.string_sent_packets)
    }
    private val tvReceivePackage by lazy {
        KotlinUtils.getString(R.string.string_received_packets)
    }
    private val tvLossRate by lazy {
        KotlinUtils.getString(R.string.string_packets_loss_rate)
    }
    private val tvMinRtt by lazy {
        KotlinUtils.getString(R.string.string_minimum_rtt)
    }
    private val tvMaxRtt by lazy {
        KotlinUtils.getString(R.string.string_maximum_rtt)
    }
    private val tvAvgRtt by lazy {
        KotlinUtils.getString(R.string.string_average_rtt)
    }

    fun display() =
        "$tvSendPackage: $sendPackage\n$tvReceivePackage: $receivePackage\n$tvLossRate: $lostRate\n$tvMinRtt: $minRtt\n$tvMaxRtt: $maxRtt\n$tvAvgRtt: $aveRtt"

    fun displayRate() =
        "$tvSendPackage: $sendPackage\n$tvReceivePackage: $receivePackage\n$tvLossRate: $lostRate\n"


    fun displayStatistics() = "$tvMinRtt: $minRtt ms\n$tvMaxRtt: $maxRtt ms\n$tvAvgRtt: $aveRtt ms"
}
