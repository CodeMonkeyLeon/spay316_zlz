package cn.swiftpass.enterprise.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.common.sentry.SentryUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.SettlementBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.SettlementListContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.SettlementListPresenter;
import cn.swiftpass.enterprise.ui.activity.list.PullToRefreshBase;
import cn.swiftpass.enterprise.ui.activity.list.PullToRefreshListView;
import cn.swiftpass.enterprise.ui.widget.SelectDatePopupWindow;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.StringUtil;


/**
 * Created by aijingya on 2019/5/20.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(日结列表的页面)
 * @date 2019/5/20 17:06.
 */
public class SettlementListActivity extends BaseActivity<SettlementListContract.Presenter> implements SettlementListContract.View {

    TextView tv_tip_settlement_msg;
    RelativeLayout rl_settlement_list;
    PullToRefreshListView pull_to_refresh;
    TextView tv_no_settlement_data;
    private ViewHolder holder;
    private ListView listView;
    private SettlementAdapter settlementAdapter;
    private List<SettlementBean> settlementBeanList = new ArrayList<SettlementBean>();
    private String startDate; //日结列表的北京开始时间---String格式
    private String endDate; //日结列表的北京结束时间-----String格式
    private String selectTime;//日期选择控件的初始时间
    private TimeZone timeZoneBeijing;//北京时间的时区
    private Date currentStartBeijingTime;//日结列表的北京开始时间---Date格式
    private Date currentEndBeijingTime;//日结列表的北京结束时间-----Date格式
    private SelectDatePopupWindow selectDatePopupWindow;

    /**
     * 根据不同时区，转换时间
     *
     * @param date
     * @return
     */
    public static Date transformTime(Date date, TimeZone oldZone, TimeZone newZone) {
        Date finalDate = null;
        if (date != null) {
            int timeOffset = oldZone.getOffset(date.getTime()) - newZone.getOffset(date.getTime());
            finalDate = new Date(date.getTime() - timeOffset);
        }
        return finalDate;
    }

    @Override
    protected SettlementListContract.Presenter createPresenter() {
        return new SettlementListPresenter();
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settlement_list);
        initView();

        if (settlementBeanList.size() == 0) {
            settlementBeanList.clear();

            formatBeijingTime();
            //去请求当前是日结数据
            getDailySettlement(startDate, endDate);
        }

        //日期选择控件默认是当前的时间转成的北京时间
        selectTime = endDate;
    }

    //把当前的机构时间转换成北京时间
    public void formatBeijingTime() {
        //手机时间---机构当地时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date dateEnd = null;
        try {
            Calendar ca = Calendar.getInstance();//得到一个Calendar的实例 ，今天的前一天
            ca.add(Calendar.DAY_OF_MONTH, -1);
            dateEnd = simpleDateFormat.parse(DateUtil.formatYYMD(ca.getTimeInMillis()));
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }

        //北京时间的时区
        timeZoneBeijing = TimeZone.getTimeZone("GMT+8");
        //得到当前的北京时间
        currentEndBeijingTime = transformTime(dateEnd, TimeZone.getDefault(), timeZoneBeijing);
        endDate = DateUtil.dateToString(currentEndBeijingTime, "yyyy-MM-dd");

        //手机时间---机构当地时间往前推90天数据
        Date dateStart = null;
        try {
            Calendar ca = Calendar.getInstance();//得到一个Calendar的实例  90天
            ca.add(Calendar.DAY_OF_MONTH, -90);
            dateStart = simpleDateFormat.parse(DateUtil.formatYYMD(ca.getTimeInMillis()));
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
        //得到当前的北京时间
        currentStartBeijingTime = transformTime(dateStart, TimeZone.getDefault(), timeZoneBeijing);
        startDate = DateUtil.dateToString(currentStartBeijingTime, "yyyy-MM-dd");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (selectDatePopupWindow != null) {
            selectDatePopupWindow.dismiss();
        }
    }

    private void initView() {
        tv_tip_settlement_msg = findViewById(R.id.tv_tip_settlement_msg);
        rl_settlement_list = findViewById(R.id.rl_settlement_list);
        pull_to_refresh = findViewById(R.id.pull_to_refresh);
        tv_no_settlement_data = findViewById(R.id.tv_no_settlement_data);

        listView = pull_to_refresh.getRefreshableView();
        settlementAdapter = new SettlementAdapter(settlementBeanList);
        listView.setAdapter(settlementAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //带数据跳转到详情页
                Intent intent = new Intent();
                intent.putExtra("SettlementBean", settlementBeanList.get(position - 1));
                intent.setClass(SettlementListActivity.this, SettlementDetailsActivity.class);
                startActivity(intent);

            }
        });

        PullToRefreshBase.OnRefreshListener mOnrefreshListener = new PullToRefreshBase.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (pull_to_refresh.getRefreshType() == PullToRefreshBase.MODE_PULL_DOWN_TO_REFRESH) {
                    settlementBeanList.clear();
                    getDailySettlement(startDate, endDate);
                } else if (pull_to_refresh.getRefreshType() == PullToRefreshBase.MODE_PULL_UP_TO_REFRESH) {
                    pull_to_refresh.onRefreshComplete();
                }

            }
        };
        pull_to_refresh.setOnRefreshListener(mOnrefreshListener);
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(getStringById(R.string.report_settlement));
        titleBar.setLeftButtonVisible(true);
        titleBar.setRightButtonVisible(false);
        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.public_search));

        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onRightLayClick() {


            }

            @Override
            public void onRightButLayClick() {
                //点击search按钮之后，弹出日历选择框
                selectDatePopupWindow = new SelectDatePopupWindow(SettlementListActivity.this,
                        selectTime, currentStartBeijingTime.getTime(), currentEndBeijingTime.getTime(), new SelectDatePopupWindow.HandleBtn() {

                    @Override
                    public void handleOkBtn(long time) {
                        if (time == 0) {
                            if (!StringUtil.isEmptyOrNull(selectTime)) {

                                if (getStringIndex(selectTime) == -1) {
                                    toastDialog(SettlementListActivity.this, "no data", null);
                                } else {
                                    Intent intent = new Intent();
                                    intent.putExtra("SettlementBean", settlementBeanList.get(getStringIndex(selectTime)));
                                    intent.setClass(SettlementListActivity.this, SettlementDetailsActivity.class);
                                    startActivity(intent);
                                }
                                return;
                            }
                        }

                        //得到当前可选的最大时间
                        long nowTime = currentEndBeijingTime.getTime();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                        try {
                            time = simpleDateFormat.parse(DateUtil.formatYYMD(time)).getTime();
                        } catch (Exception e) {
                            SentryUtils.INSTANCE.uploadTryCatchException(
                                    e,
                                    SentryUtils.INSTANCE.getClassNameAndMethodName()
                            );
                        }
                        //如果大于当前选择的时间则提示----理论上此逻辑不会进，因为限制了可选范围为90天
                        if (time > nowTime) {
                            toastDialog(SettlementListActivity.this, R.string.tx_date, null);
                            return;
                        }

                        long day_90_later = currentStartBeijingTime.getTime();
                        if (time < day_90_later) {
                            toastDialog(SettlementListActivity.this, R.string.tx_choice_date, null);
                            return;
                        }

                        String currentTime = DateUtil.formatYYMD(time);
                        //最后如果选择的时间符合日期
                        if (getStringIndex(currentTime) == -1) {
                            toastDialog(SettlementListActivity.this, R.string.tv_bill, null);
                        } else {
                            Intent intent = new Intent();
                            intent.putExtra("SettlementBean", settlementBeanList.get(getStringIndex(currentTime)));
                            selectTime = currentTime;
                            intent.setClass(SettlementListActivity.this, SettlementDetailsActivity.class);
                            startActivity(intent);
                        }
                    }

                });
                selectDatePopupWindow.showAtLocation(tv_tip_settlement_msg, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
            }
        });
    }


    public int getStringIndex(String date) {
        for (int index = 0; index < settlementBeanList.size(); index++) {
            if (TextUtils.equals(settlementBeanList.get(index).getSettlementDate(), date)) {
                return index;
            }
        }
        return -1;
    }


    @Override
    public void queryDailySettlementListSuccess(@NonNull ArrayList<SettlementBean> response) {
        if (settlementBeanList != null) {
            settlementBeanList.clear();
        }

        if (response != null && response.size() > 0) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tv_no_settlement_data.setVisibility(View.GONE);
                    pull_to_refresh.setVisibility(View.VISIBLE);
                    rl_settlement_list.setVisibility(View.VISIBLE);

                    settlementBeanList.addAll(response);
                    settlementAdapter.notifyDataSetChanged();
                    pull_to_refresh.onRefreshComplete();
                    listView.setSelection(0);
                }
            });
        } else {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    tv_no_settlement_data.setVisibility(View.VISIBLE);
                    pull_to_refresh.setVisibility(View.GONE);
                    rl_settlement_list.setVisibility(View.GONE);
                    pull_to_refresh.onRefreshComplete();
                }
            });
        }
    }

    @Override
    public void queryDailySettlementListFailed(@Nullable Object error) {
        pull_to_refresh.onRefreshComplete();
        if (checkSession()) {
            return;
        }

        if (error != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!StringUtil.isEmptyOrNull(error.toString())) {
                        toastDialog(SettlementListActivity.this, error.toString(), null);
                    } else {
                        toastDialog(SettlementListActivity.this, R.string.tx_load_fail, null);
                    }

                    if (settlementBeanList.size() == 0) {
                        tv_no_settlement_data.setVisibility(View.VISIBLE);
                        pull_to_refresh.setVisibility(View.GONE);
                        rl_settlement_list.setVisibility(View.GONE);
                    }
                }
            });
        }
    }

    public void getDailySettlement(String startDate, String endDate) {
        if (mPresenter != null) {
            mPresenter.queryDailySettlementList(startDate, endDate);
        }
    }

    private class SettlementAdapter extends BaseAdapter {
        private List<SettlementBean> settlementBeanList;

        public SettlementAdapter() {

        }

        public SettlementAdapter(List<SettlementBean> settlementBeanList) {
            this.settlementBeanList = settlementBeanList;
        }

        @Override
        public int getCount() {
            return settlementBeanList.size();
        }

        @Override
        public Object getItem(int position) {
            return settlementBeanList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(SettlementListActivity.this, R.layout.item_settlement_list, null);
                holder = new ViewHolder();
                holder.tv_datetime = convertView.findViewById(R.id.tv_datetime);
                holder.tv_money = convertView.findViewById(R.id.tv_money);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            SettlementBean reportBean = settlementBeanList.get(position);
            if (reportBean != null) {
                if (!TextUtils.isEmpty(reportBean.getSettlementDate())) {
                    holder.tv_datetime.setText(reportBean.getSettlementDate());
                }
                if (!TextUtils.isEmpty(DateUtil.formatMoneyUtils(reportBean.getSettlementAmount()))) {
                    holder.tv_money.setText(MainApplication.getInstance().getFeeFh() + " " +
                            DateUtil.formatMoneyUtils(reportBean.getSettlementAmount()));
                }
            }

            return convertView;
        }
    }

    private class ViewHolder {
        private TextView tv_datetime, tv_money;
    }
}
