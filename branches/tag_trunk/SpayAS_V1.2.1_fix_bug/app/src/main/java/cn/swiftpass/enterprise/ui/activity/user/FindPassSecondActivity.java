/*
 * 文 件 名:  FindPassWordActivity.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-14
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 忘记密码
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-14]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class FindPassSecondActivity extends TemplateActivity
{
    
    private TimeCount time;
    
    private EditText et_id, ed_code;
    
    private ImageView iv_clean_input, iv_clearPwd;
    
    private Button btn_next_step;
    
    private TextView textView1, tv_tel, tv_title_info;
    
    private UserModel model;
    
    private LinearLayout ly_title;
    
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    public static void startActivity(Context context, UserModel model)
    {
        Intent it = new Intent();
        it.setClass(context, FindPassSecondActivity.class);
        it.putExtra("userModel", model);
        context.startActivity(it);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_pass_second);
        
        MainApplication.listActivities.add(this);
        initView();
        
        setLister();
        btn_next_step.getBackground().setAlpha(102);
        model = (UserModel)getIntent().getSerializableExtra("userModel");
        if (null != model)
        {
            //            {
            //            if (model.getIsMch() == 1)
            //                tv_title_info.setVisibility(View.GONE);
            //                ly_title.setVisibility(View.VISIBLE);
            //                tv_tel.setText(model.getTel7());
            //            }
            //            else
            //            {
            loadGetCode();
            //收银员
            ly_title.setVisibility(View.GONE);
            
            tv_title_info.setVisibility(View.VISIBLE);
            if (!StringUtil.isEmptyOrNull(model.getEe()))
            {
                
                //                    tv_title_info.setText(getString(R.string.tv_find_pass_title_info2) + tel.substring(0, 3) + "****"
                //                        + tel.substring(7, tel.length()));
                
                tv_title_info.setText(getString(R.string.tv_find_pass_title_info2) + model.getEe());
            }
            //            }
        }
    }
    
    /**
     *验证码倒计时60秒
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void Countdown()
    {
        // 总共60 没间隔1秒
        time = new TimeCount(60000, 1000);
        time.start();
    }
    
    class TimeCount extends CountDownTimer
    {
        public TimeCount(long millisInFuture, long countDownInterval)
        {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }
        
        @Override
        public void onFinish()
        {//计时完毕时触发
            textView1.setEnabled(true);
            textView1.setClickable(true);
            textView1.setText(R.string.bt_code_get);
            textView1.setTextColor(getResources().getColor(R.color.title_bg_new));
        }
        
        @Override
        public void onTick(long millisUntilFinished)
        {//计时过程显示
            textView1.setEnabled(false);
            textView1.setClickable(false);
            textView1.setTextColor(getResources().getColor(R.color.user_edit_color));
            textView1.setText(millisUntilFinished / 1000 + "s" + getString(R.string.bt_code_get));
        }
    }
    
    private void initView()
    {
        iv_clearPwd = getViewById(R.id.iv_clearPwd);
        ly_title = getViewById(R.id.ly_title);
        tv_title_info = getViewById(R.id.tv_title_info);
        tv_tel = getViewById(R.id.tv_tel);
        ed_code = getViewById(R.id.ed_code);
        textView1 = getViewById(R.id.textView1);
        et_id = getViewById(R.id.et_id);
        iv_clean_input = getViewById(R.id.iv_clean_input);
        btn_next_step = getViewById(R.id.btn_next_step);
        
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        {
            
            @Override
            public void onExecute(CharSequence s, int start, int before, int count)
            {
            }
            
            @Override
            public void onAfterTextChanged(Editable s)
            {
                if (et_id.isFocused())
                {
                    if (et_id.getText().toString().length() > 0)
                    {
                        iv_clean_input.setVisibility(View.VISIBLE);
                        iv_clearPwd.setVisibility(View.GONE);
                    }
                    else
                    {
                        iv_clean_input.setVisibility(View.GONE);
                        iv_clearPwd.setVisibility(View.GONE);
                    }
                }
                if (ed_code.isFocused())
                {
                    if (ed_code.getText().toString().length() > 0)
                    {
                        iv_clearPwd.setVisibility(View.VISIBLE);
                        iv_clean_input.setVisibility(View.GONE);
                    }
                    else
                    {
                        iv_clearPwd.setVisibility(View.GONE);
                        iv_clean_input.setVisibility(View.GONE);
                    }
                }
                
                if (null != model)
                {
                    //                    if (model.getIsMch() == 1)
                    //                    {
                    //                        if (!StringUtil.isEmptyOrNull(et_id.getText().toString())
                    //                            && !StringUtil.isEmptyOrNull(ed_code.getText().toString()))
                    //                        {
                    //                            setButtonBg(btn_next_step, true, R.string.reg_next_step);
                    //                        }
                    //                        else
                    //                        {
                    //                            setButtonBg(btn_next_step, false, R.string.reg_next_step);
                    //                        }
                    //                    }
                    //                    else
                    //                    {
                    if (!StringUtil.isEmptyOrNull(ed_code.getText().toString()))
                    {
                        
                        setButtonBg(btn_next_step, true, R.string.reg_next_step);
                    }
                    else
                    {
                        
                        setButtonBg(btn_next_step, false, R.string.reg_next_step);
                    }
                    
                    //                    }
                }
                else
                {
                    
                    if (!StringUtil.isEmptyOrNull(et_id.getText().toString())
                        && !StringUtil.isEmptyOrNull(ed_code.getText().toString()))
                    {
                        setButtonBg(btn_next_step, true, R.string.reg_next_step);
                    }
                    else
                    {
                        setButtonBg(btn_next_step, false, R.string.reg_next_step);
                    }
                }
            }
        });
        ed_code.addTextChangedListener(editTextWatcher);
        et_id.addTextChangedListener(editTextWatcher);
        ed_code.setOnFocusChangeListener(listener);
        et_id.setOnFocusChangeListener(listener);
        showSoftInputFromWindow(FindPassSecondActivity.this, et_id);
        //        showSoftInputFromWindow(FindPassSecondActivity.this, ed_code);
    }
    
    private final OnFocusChangeListener listener = new OnFocusChangeListener()
    {
        @Override
        public void onFocusChange(View v, boolean hasFocus)
        {
            switch (v.getId())
            {
                case R.id.ed_code:
                    if (hasFocus)
                    {
                        if (ed_code.getText().toString().length() > 0)
                        {
                            iv_clearPwd.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            iv_clearPwd.setVisibility(View.GONE);
                        }
                    }
                    else
                    {
                        iv_clearPwd.setVisibility(View.GONE);
                    }
                    break;
                case R.id.et_id:
                    if (hasFocus)
                    {
                        if (et_id.getText().toString().length() > 0)
                        {
                            iv_clean_input.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            iv_clean_input.setVisibility(View.GONE);
                        }
                    }
                    else
                    {
                        iv_clean_input.setVisibility(View.GONE);
                    }
                    break;
            }
            
        }
    };
    
    void loadGetCode()
    {
        UserManager.getCode(model.getTel(), et_id.getText().toString(), new UINotifyListener<String>()
        {
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dismissLoading();
                if (object != null)
                {
                    FindPassSecondActivity.this.runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            toastDialog(FindPassSecondActivity.this, object.toString(), null);
                        }
                    });
                }
            }
            
            @Override
            public void onPreExecute()
            {
                super.onPostExecute();
                loadDialog(FindPassSecondActivity.this, R.string.dialog_message);
            }
            
            @Override
            public void onSucceed(String result)
            {
                // TODO Auto-generated method stub
                super.onSucceed(result);
                dismissLoading();
                if (null != result)
                {
                    //                            if(model.getIsMch() == 1){/
                    //                                
                    //                            }
                    showToastInfo(R.string.tx_code_send_succ);
                    model.setTelephone(result);
                    Countdown();
                    
                }
            }
        });
        
    }
    
    private void setLister()
    {
        iv_clean_input.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                et_id.setText("");
                iv_clean_input.setVisibility(View.GONE);
            }
        });
        
        iv_clearPwd.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                ed_code.setText("");
                iv_clearPwd.setVisibility(View.GONE);
            }
        });
        
        textView1.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                String tel = null;
                //                if (model != null)
                //                {
                ////                    if (model.getIsMch() == 1)
                ////                    {
                ////                        //                        tel = MainApplication.merchantId;
                ////                        //商户号
                ////                        if (StringUtil.isEmptyOrNull(et_id.getText().toString()))
                ////                        {
                ////                            toastDialog(FindPassSecondActivity.this, R.string.tv_find_pass_hint_info5, null);
                ////                            et_id.setFocusable(true);
                ////                            return;
                ////                        }
                ////                    }
                //                    //                    else
                //                    //                    {
                //                    //                        tel = model.getTel();
                //                    //                    }
                //                }
                //                else
                //                {
                //                    return;
                //                }
                
                loadGetCode();
            }
        });
        
        btn_next_step.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (StringUtil.isEmptyOrNull(ed_code.getText().toString()))
                {
                    toastDialog(FindPassSecondActivity.this, R.string.tx_ver_code, null);
                    ed_code.setFocusable(true);
                    return;
                }
                model.setCode(ed_code.getText().toString());
                checkPhoneCode();
                
            }
        });
        
    }
    
    void checkPhoneCode()
    {
        //        if (StringUtil.isEmptyOrNull(ed_code.getText().toString()))
        //        {
        //            toastDialog(FindPassSecondActivity.this, R.string.tv_first_get_code, null);
        //            return;
        //        }
        
        if (model == null)
        {
            return;
        }
        String emal = "";
        if (model.getIsMch() == 1)
        {
            emal = model.getTelephone();
            model.setEmail(emal);
        }
        else
        {
            emal = model.getTel();
            model.setEmail(emal);
        }
        
        UserManager.checkPhoneCode(emal, ed_code.getText().toString(), new UINotifyListener<Boolean>()
        {
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dismissLoading();
                if (object != null)
                {
                    FindPassSecondActivity.this.runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            toastDialog(FindPassSecondActivity.this, object.toString(), null);
                        }
                    });
                }
            }
            
            @Override
            public void onPreExecute()
            {
                super.onPostExecute();
                loadDialog(FindPassSecondActivity.this, R.string.reg_next_step_loading);
            }
            
            @Override
            public void onSucceed(Boolean result)
            {
                // TODO Auto-generated method stub
                super.onSucceed(result);
                dismissLoading();
                if (result)
                {
                    //                    model.setEmail(model.getTel());
                    model.setEmailCode(ed_code.getText().toString());
                    FindPassSubmitActivity.startActivity(FindPassSecondActivity.this, model);
                }
                //                else
                //                {
                //                    toastDialog(FindPassFirstActivity.this, content, handleBtn)
                //                }
            }
        });
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tv_find_pass_title_info);
    }
    
}
