/*
 * 文 件 名:  AgentActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2014-8-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.agent;

import java.util.ArrayList;

import android.os.Bundle;
import android.widget.ListView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.AgentInfo;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.adapter.AgentAdapter;

/**
 * 代理商
 * 
 * @author  he_hui
 * @version  [版本号, 2014-8-11]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class AgentActivity extends TemplateActivity
{
    private ListView listView;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.agent);
        
        initVew();
        
        initValue();
        
    }
    
    @SuppressWarnings("unchecked")
    private void initValue()
    {
        Bundle bundle = getIntent().getBundleExtra("bundle");
        ArrayList<AgentInfo> agentList = new ArrayList<AgentInfo>();
        if (bundle != null)
        {
            agentList = (ArrayList<AgentInfo>)bundle.getSerializable("agentList");
        }
        AgentAdapter adapter = new AgentAdapter(this, agentList);
        listView.setAdapter(adapter);
        
    }
    
    private void initVew()
    {
        listView = getViewById(R.id.agent_list);
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.agent_title);
    }
    
}
