package cn.swiftpass.enterprise.utils;

import com.example.common.sentry.SentryUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * Created by aijingya on 2018/11/2.
 *
 * @Package cn.swiftpass.enterprise.utils
 * @Description: ${TODO}(SHA256加密)
 * @date 2018/11/2.17:07.
 */

public class SHA256Utils {

    private static final String TAG = SHA256Utils.class.getSimpleName();

    /**
     * SHA加密
     *
     * @param strSrc 明文
     * @return 加密之后的密文
     */
    public static String shaEncrypt(String strSrc) {
        MessageDigest digest = null;
        String strDes = null;
        byte[] bytes = strSrc.getBytes();
        try {
            digest = MessageDigest.getInstance("SHA-256");// 将此换成SHA-1、SHA-512、SHA-384等参数
            digest.update(bytes);
            strDes = bytes2Hex(digest.digest()); // to HexString
        } catch (NoSuchAlgorithmException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            return null;
        }
        return strDes;
    }

    /**
     * byte数组转换为16进制字符串
     *
     * @param bts 数据源
     * @return 16进制字符串
     */


    public static String bytes2Hex(byte[] bts) {
        String des = "";
        String tmp = null;
        for (int i = 0; i < bts.length; i++) {
            tmp = (Integer.toHexString(bts[i] & 0xFF));
            if (tmp.length() == 1) {
                des += "0";
            }
            des += tmp;
        }
        return des;
    }
}
