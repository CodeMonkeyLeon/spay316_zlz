package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Alan
 * Date: 13-11-7
 * Time: 上午11:33
 * To change this template use File | Settings | File Templates.
 */
public class QRcodeInfo implements Serializable
{
    
    private long discountAmount;
    
    private boolean isWitch = true; // 是否切换
    
    private String apiCode;

    public String service;

    public String codeImg;

    /**
     * @return 返回 apiCode
     */
    public String getApiCode()
    {
        return apiCode;
    }

    public void setApiCode(String apiCode)
    {
        this.apiCode = apiCode;
    }

    public boolean isWitch()
    {
        return isWitch;
    }

    public void setWitch(boolean isWitch)
    {
        this.isWitch = isWitch;
    }

    public long getDiscountAmount()
    {
        return discountAmount;
    }

    public void setDiscountAmount(long discountAmount)
    {
        this.discountAmount = discountAmount;
    }

    public String getPayType()
    {
        return payType;
    }

    public void setPayType(String payType)
    {
        this.payType = payType;
    }

    private static final long serialVersionUID = 1L;
    
    public String uuId;

    public String uuid;

    public String appId;
    
    public String reqKey;
    
    public String orderNo;

    public String outTradeNo;

    public String transactionNo;
    
    private String city;
    
    public String service_uuid; // 服务器环境 
    
    public boolean isQpay;
    
    public String payType;
    
    private Integer isMark = 0;
    
    private boolean iswxCard; //是否带微信卡券

    public String getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(String surcharge) {
        this.surcharge = surcharge;
    }

    private String surcharge;

    public boolean isIswxCard()
    {
        return iswxCard;
    }

    public void setIswxCard(boolean iswxCard)
    {
        this.iswxCard = iswxCard;
    }

    public Integer getIsMark()
    {
        return isMark;
    }

    public void setIsMark(Integer isMark)
    {
        this.isMark = isMark;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }
    
    //交易金额
    public String totalMoney;

    public String money;
    
    //有优惠券 消费后直接打开优惠券
    //public String couponUrl;
    //次此消费是否有优惠券
    public boolean isCoupon;
    
    public Order order;

    public String  outAuthNo;


    public String getOutAuthNo() {
        return outAuthNo;
    }

    public void setOutAuthNo(String outAuthNo) {
        this.outAuthNo = outAuthNo;
    }

    public String  authNo;
    public String getAuthNo() {
        return authNo;
    }

    public void setAuthNo(String authNo) {
        authNo = authNo;
    }

    //V3.0.7 instapay 版本新增以下两个字段
    //AUB instapay 版本新增
    public  String  invoiceId;
    public  String  expirationDate;

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }
}
