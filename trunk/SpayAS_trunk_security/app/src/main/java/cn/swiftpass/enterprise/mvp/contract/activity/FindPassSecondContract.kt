package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.ForgetPSWBean
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @花谢的时候
 * @并不伤心
 * @生命要在死亡里休息
 * @变得干净
 */
class FindPassSecondContract {


    interface View : BaseView {


        fun getCodeSuccess(response: ForgetPSWBean?)

        fun getCodeFailed(error: Any?)

        fun checkPhoneCodeSuccess(response: Boolean)

        fun checkPhoneCodeFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {

        fun checkPhoneCode(
            token: String?,
            code: String?
        )

        fun getCode(input: String?)

    }


}