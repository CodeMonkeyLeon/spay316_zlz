package cn.swiftpass.enterprise.mvp.utils

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.UserInfo
import cn.swiftpass.enterprise.common.Constant
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.utils.*
import com.example.common.sentry.SentryUtils.getClassNameAndMethodName
import com.example.common.sentry.SentryUtils.uploadTryCatchException
import com.example.common.sp.PreferenceUtil
import org.json.JSONObject

object ParamsUtils {

    const val TAG = "ParamsUtils"

    fun saveLoginCookieToSp(message: String?) {
        if (TextUtils.isEmpty(message)) {
            return
        }
        try {
            val resultJson = JSONObject(message)
            val cookieJson = JSONObject(resultJson.optString("cookieMap"))
            if (cookieJson != null) {
                PreferenceUtil.commitString("login_skey", cookieJson.optString("SKEY", ""))
                PreferenceUtil.commitString("login_sauthid", cookieJson.optString("SAUTHID", ""))
            }
        } catch (e: java.lang.Exception) {
            uploadTryCatchException(
                e,
                getClassNameAndMethodName()
            )
            Logger.e(TAG, "登录 cookie 解析异常")
        }
    }


    fun addParam(spayRs: String, param: MutableMap<String?, String?>) {
        //加签名
        param["spayRs"] = spayRs
        if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getSKey())) {
            val priKey = SharedPreUtils.readProduct(ParamsConstants.SECRET_KEY) as String
            param["nns"] = SignUtil.getInstance().createSign(
                param, MD5.md5s(priKey).substring(0, 16)
            )
        } else {
            param["nns"] = SignUtil.getInstance().createSign(
                param, MD5.md5s(spayRs)
            )
        }
    }

    fun updateType(payType: String?): String? {
        var p = payType
        payType?.let {
            if (it.startsWith(Constant.PAY_ZFB_NATIVE)) {
                p = Constant.PAY_ZFB_NATIVE1
            }
        }
        return p
    }


    fun parseToJson(
        listStr: ArrayList<String?>?,
        tradeType: ArrayList<Int>,
        payType: ArrayList<Int>
    ) {
        if (listStr != null && listStr.size > 0) {
            for (s in listStr) {
                if (s == "wx") {
                    tradeType.add(1)
                } else if (s == "zfb") {
                    tradeType.add(2)
                } else if (s == "jd") {
                    tradeType.add(12)
                } else if (s == "qq") {
                    tradeType.add(4)
                } else if (s == "2") {
                    payType.add(2)
                } else if (s == "1") {
                    payType.add(1)
                } else if (s == "3") {
                    payType.add(3)
                } else if (s == "4") {
                    payType.add(4)
                } else if (s == "8") {
                    payType.add(8)
                }
            }
        }
    }


    fun parseJson(userInfo: UserInfo) {
        MainApplication.getInstance().userId =
            if (TextUtils.isEmpty(userInfo.id)) 0 else userInfo.id.toLong()
        val url = MainApplication.getInstance().baseUrl + "spay/mch/download?url="
        //获取当前币种的最小单位，即小数点的位数，获取不到默认是两位
        MainApplication.getInstance().setNumFixed(userInfo.numFixed)
        //当前登录的密码是否默认密码：1是
        MainApplication.getInstance().setIsDefault(userInfo.isDefault)
        if (TextUtils.isEmpty(MainApplication.getInstance().isDefault)) {
            MainApplication.getInstance().setIsDefault("0")
        }
        /**
         * 新增一些费率信息
         */
        if (!TextUtils.isEmpty(userInfo.rate)) {
            MainApplication.getInstance().setExchangeRate(userInfo.rate)
        }
        if (!TextUtils.isEmpty(userInfo.vatRate)) {
            MainApplication.getInstance().setVatRate(userInfo.vatRate.toDouble())
        }
        if (!TextUtils.isEmpty(userInfo.surchargeRate)) {
            MainApplication.getInstance().setSurchargeRate(userInfo.surchargeRate.toDouble())
        }
        if (!TextUtils.isEmpty(userInfo.taxRate)) {
            MainApplication.getInstance().setTaxRate(userInfo.taxRate.toDouble())
        }
        if (!TextUtils.isEmpty(userInfo.usdToRmbExchangeRate)) {
            MainApplication.getInstance()
                .setUsdToRmbExchangeRate(userInfo.usdToRmbExchangeRate.toDouble())
        }
        if (!TextUtils.isEmpty(userInfo.sourceToUsdExchangeRate)) {
            MainApplication.getInstance()
                .setSourceToUsdExchangeRate(userInfo.sourceToUsdExchangeRate.toDouble())
        }
        if (!TextUtils.isEmpty(userInfo.alipayPayRate)) {
            MainApplication.getInstance().setAlipayPayRate(userInfo.alipayPayRate.toDouble())
        }
        if (!TextUtils.isEmpty(userInfo.paymentLinkPermsJson)) {
            MainApplication.getInstance().setPaymentLinkAuthority(userInfo.paymentLinkPermsJson)
        }
        MainApplication.getInstance().setSurchargeOpen("1" == userInfo.isSurchargeOpen.toString())
        MainApplication.getInstance().setTipOpenFlag("1" == userInfo.tipOpenFlag.toString())
        //屏蔽预扣税功能
        MainApplication.getInstance().setTaxRateOpen("1" == userInfo.isTaxRateOpen.toString())
        //屏蔽小费功能
//        MainApplication.setTipOpen(jsonObject.optString("isTipOpen").equals("1"));
        MainApplication.getInstance().mchId = MainApplication.getInstance().userInfo.mchId
        MainApplication.getInstance().setMchName(MainApplication.getInstance().userInfo.mchName)
        MainApplication.getInstance().setUserId(MainApplication.getInstance().getUserId())
        MainApplication.getInstance().setSignKey(MainApplication.getInstance().userInfo.signKey)
        MainApplication.getInstance().feeType = MainApplication.getInstance().userInfo.feeType
        if (TextUtils.isEmpty(MainApplication.getInstance().userInfo.feeFh)) {
            MainApplication.getInstance().userInfo.feeFh = ToastHelper.toStr(R.string.tx_mark)
        }
        if (!TextUtils.isEmpty(MainApplication.getInstance().userInfo.mchLogo)) {
            MainApplication.getInstance().userInfo.mchLogo =
                url + MainApplication.getInstance().userInfo.mchLogo
        }
        try {
            MainApplication.getInstance().feeFh = MainApplication.getInstance().userInfo.feeFh
            MainApplication.getInstance().payTypeMap = userInfo.payTypeMap
            MainApplication.getInstance().setTradeStateMap(userInfo.tradeStateMap)
            MainApplication.getInstance().setRefundStateMap(userInfo.refundStateMap)
            MainApplication.getInstance().apiProviderMap = userInfo.apiProviderMap
            MainApplication.getInstance().payTypeMap = userInfo.payTypeMap
            MainApplication.getInstance().setTradeStateMap(userInfo.tradeStateMap)
            MainApplication.getInstance().refundStateMap = userInfo.refundStateMap

            //V3.1.0版本新增卡通道的支付状态的集合2021/03/26
            MainApplication.getInstance().cardPaymentTradeStateMap = userInfo.cardTradeTypeMap

            //后台没有返回，前端写死
            //授权状态(1未授权，2已授权，3已撤销，4解冻)
            MainApplication.getInstance().preAuthTradeStateMap["1"] =
                ToastHelper.toStr(R.string.unauthorized)
            MainApplication.getInstance().preAuthTradeStateMap["2"] =
                ToastHelper.toStr(R.string.pre_auth_authorized)
            MainApplication.getInstance().preAuthTradeStateMap["3"] =
                ToastHelper.toStr(R.string.pre_auth_closed)
            MainApplication.getInstance().preAuthTradeStateMap["4"] =
                ToastHelper.toStr(R.string.pre_auth_unfreeze)
            MainApplication.getInstance().preAuthTradeStateMap =
                MainApplication.getInstance().preAuthTradeStateMap
        } catch (e: Exception) {
            uploadTryCatchException(
                e,
                getClassNameAndMethodName()
            )
        }
        val secretKey = SharedPreUtils.readProduct(ParamsConstants.SECRET_KEY) as String
        if (!TextUtils.isEmpty(secretKey)) {
            MainApplication.getInstance().newSignKey = MD5.md5s(secretKey).substring(0, 16)
        }
        MainApplication.getInstance().setNewSignKey(MainApplication.getInstance().getNewSignKey())
    }


}