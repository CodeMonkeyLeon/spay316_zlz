package cn.swiftpass.enterprise.mvp.annotation

import android.text.TextUtils
import androidx.annotation.NonNull
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.mvp.constants.ApiConstants
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.param.HttpParams
import cn.swiftpass.enterprise.utils.SignUtil
import java.lang.reflect.Method

/**
 * @author lizheng.zhao
 * @date 2022/12/14
 *
 * @description 将  方法/参数   注解解析到HttpParam中
 */
object AnnotationParser {

    @NonNull
    fun parse(method: Method, args: Array<Any>): HttpParams {
        val params = HttpParams()
        parseMethodAnnotation(method.annotations, params)
        val annotations = method.parameterAnnotations
        for (i in annotations.indices) {
            parseParamsAnnotation(annotations[i], args[i], params)
        }
        return params
    }


    /**
     * 解析方法注解
     */
    private fun parseMethodAnnotation(annotationArray: Array<Annotation>?, httpParams: HttpParams) {
        annotationArray?.let { annotations ->
            if (annotations.isNotEmpty()) {
                for (annotation in annotations) {
                    when (annotation) {
                        is Address -> {
                            httpParams.mUrl =
                                MainApplication.getInstance().baseUrl + annotation.value
                        }

                        is MethodType -> {
                            httpParams.mMethodType = annotation.value
                        }

                        is IsNeedSpayId -> {
                            httpParams.mIsNeedSpayId = annotation.value
                        }

                        is IsNeedCook -> {
                            httpParams.mIsNeedCook = annotation.value
                        }

                        is IsNeedVerifyCerts -> {
                            httpParams.mIsNeedVerifyCerts = annotation.value
                        }
                    }
                }
            }
        }
    }

    /**
     * 解析参数注解
     */
    private fun parseParamsAnnotation(
        annotationArray: Array<Annotation>?,
        params: Any?,
        httpParams: HttpParams
    ) {
        annotationArray?.let { annotations ->
            params?.let { paramValue ->
                for (annotation in annotations) {
                    when (annotation) {
                        is Param -> {
                            val paramKey = annotation.value
                            if (paramValue is String) {

                                if (paramValue.contains(ApiConstants.JSON_STRING_FLAG)) {
                                    //jsonStr
                                    httpParams.mJsonStr =
                                        paramValue.replace(ApiConstants.JSON_STRING_FLAG, "")
                                } else {
                                    //param
                                    dealWithParamsMap(
                                        paramKey,
                                        paramValue,
                                        httpParams
                                    )
                                }
                            } else {
                                throw RuntimeException("接口请求参数不是String类型")
                            }
                        }

                        else -> {

                        }
                    }
                }

            }
        }
    }

    private fun putMapValue(key: String?, value: String?, httpParams: HttpParams) {
        key?.let { k ->
            if (!TextUtils.isEmpty(value)) {
                httpParams.mParamsMap[k] = value
            }
        }
    }


    /**
     * 处理参数
     */
    private fun dealWithParamsMap(
        paramKey: String?,
        paramValue: String?,
        httpParams: HttpParams
    ) {
        if (TextUtils.equals(paramKey, ParamsConstants.SPAY_RS)) {
            //统一spayRs
            httpParams.mSpayRs = paramValue
        }

        if (TextUtils.equals(paramKey, ParamsConstants.SIGN)) {
            //统一sign
            if (!TextUtils.isEmpty(MainApplication.getInstance().getSignKey())) {
                putMapValue(
                    paramKey,
                    SignUtil.getInstance()
                        .createSign(
                            httpParams.mParamsMap,
                            paramValue
                        ),
                    httpParams
                )
            }
        } else if (TextUtils.equals(paramKey, ParamsConstants.DA_MONEY)) {
            //统一da_money
            paramValue?.let {
                if (it.toLong() > 0) {
                    putMapValue(paramKey, it, httpParams)
                }
            }
        } else {
            putMapValue(paramKey, paramValue, httpParams)
        }
    }

}