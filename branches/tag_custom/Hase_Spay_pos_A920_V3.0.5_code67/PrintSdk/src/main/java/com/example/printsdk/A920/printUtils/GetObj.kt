package com.example.printsdk.A920.printUtils

import android.util.Log
import com.example.printsdk.A920.POSA920Client
import com.pax.dal.IDAL

object GetObj {

    private var dal: IDAL? = null

    // 获取IDal dal对象
    fun getDal(): IDAL? {
        dal = POSA920Client.idal
        if (dal == null) {
            Log.e("NeptuneLiteDemo", "dal is null")
        }
        return dal
    }

}