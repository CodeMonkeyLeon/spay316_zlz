package cn.swiftpass.enterprise.ui.activity

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.bussiness.model.OrderTotalInfo
import cn.swiftpass.enterprise.bussiness.model.OrderTotalItemInfo
import cn.swiftpass.enterprise.intl.BuildConfig
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.print.PrintOrder
import cn.swiftpass.enterprise.print.PrintSummary
import cn.swiftpass.enterprise.ui.activity.print.BluePrintUtil
import cn.swiftpass.enterprise.ui.view.ItemTicketView
import cn.swiftpass.enterprise.ui.widget.TitleBar
import cn.swiftpass.enterprise.utils.*
import cn.swiftpass.enterprise.utils.permissionutils.Permission
import cn.swiftplus.enterprise.printsdk.print.PrintClient
import com.example.common.sentry.SentryUtils
import java.util.*

/**
 * @author lizheng.zhao
 * @date 2022/08/01
 * @description 小票预览
 */
class TicketPreviewActivity : BaseActivity<BasePresenter<*>>(), View.OnClickListener {


    override fun createPresenter() = null

    private fun getLayoutId() = R.layout.act_ticket_preview


    //收款退款订单数据数据
    private var mOrder: Order? = null

    //汇总订单数据数据
    private var mOrderTotal: OrderTotalInfo? = null

    private var mIsCashierVisible: Boolean = false
    private lateinit var mBluePrint: Button
    private var mTicketType = ""

    private lateinit var mTvReceiptTypeTitle: TextView
    private lateinit var mLlContent1: LinearLayout
    private lateinit var mLlContent2: LinearLayout
    private lateinit var mLlContent3: LinearLayout
    private lateinit var mLlContent4: LinearLayout

    private lateinit var mTvDivider1: TextView
    private lateinit var mTvDivider2: TextView
    private lateinit var mTvDivider3: TextView

    private lateinit var mBtnDownload: TextView


    private lateinit var mTvTimeCreate: TextView


    private lateinit var mPermissions: Array<String>


    companion object {
        const val TAG = "TicketPreviewActivity"

        //收款退款订单数据数据
        private const val ORDER_DATA = "order_data"

        //汇总订单数据数据
        private const val ORDER_TOTAL_DATA = "order_total_data"

        //收款退款小票
//        private const val TICKET_TYPE_RECEIVE_REFUND = "ticket_receive_refund"
        private const val TICKET_TYPE_RECEIVE = "ticket_receive"
        private const val TICKET_TYPE_REFUND = "ticket_refund"

        //汇总小票
        private const val TICKET_TYPE_TOTAL = "ticket_total"

        private const val TICKET_TYPE = "ticket_type"

        private const val EXTERNAL_STORAGE_REQUEST_PERMISSION = 1234
        private const val EXTERNAL_STORAGE_REQUEST_DOWNLOAD_PERMISSION = 1235


        private var mTitleStrId: Int = 0

        /**
         * 收款退款小票
         */
        fun startTicketPreviewActivity(fromActivity: Activity, order: Order) {
            val intent = Intent(fromActivity, TicketPreviewActivity::class.java)
            intent.putExtra(ORDER_DATA, order)
            if (order.isPay) {
                //收款
                mTitleStrId = R.string.tx_blue_print_pay_note
                intent.putExtra(TICKET_TYPE, TICKET_TYPE_RECEIVE)
            } else {
                //退款
                mTitleStrId = R.string.tx_blue_print_refund_note
                intent.putExtra(TICKET_TYPE, TICKET_TYPE_REFUND)
            }
            fromActivity.startActivity(intent)
        }

        /**
         * 汇总小票
         */
        fun startTicketPreviewActivity(fromActivity: Activity, orderTotalInfo: OrderTotalInfo) {
            val intent = Intent(fromActivity, TicketPreviewActivity::class.java)
            intent.putExtra(ORDER_TOTAL_DATA, orderTotalInfo)
            mTitleStrId = R.string.tx_blue_print_data_sum
            intent.putExtra(TICKET_TYPE, TICKET_TYPE_TOTAL)
            fromActivity.startActivity(intent)
        }
    }


    override fun useToolBar() = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())


        mPermissions = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            //android13
            Permission.Group.STORAGE_API_33
        } else {
            Permission.Group.STORAGE
        }


        intent?.let {
            mTicketType = it.getStringExtra(TICKET_TYPE).toString()
            initView()
            if (TextUtils.equals(mTicketType, TICKET_TYPE_RECEIVE) || TextUtils.equals(
                    mTicketType,
                    TICKET_TYPE_REFUND
                )
            ) {
                //收款退款小票
                mOrder = it.getSerializableExtra(ORDER_DATA) as Order
                mOrder?.let { orderModel ->
                    Logger.i(TAG, "收退订单数据 : ${KotlinUtils.jsonToString(orderModel)}")
                    mIsCashierVisible =
                        !TextUtils.isEmpty(orderModel.getUserName()) && TextUtils.equals(
                            orderModel.getUserName(),
                            "null"
                        ) && !TextUtils.isEmpty(orderModel.getUseId())
                    setTicketReceiveRefundView(orderModel)
                }
            } else {
                //汇总小票
                mOrderTotal = it.getSerializableExtra(ORDER_TOTAL_DATA) as OrderTotalInfo
                mOrderTotal?.let { summaryOrderInfo ->
                    Logger.i(TAG, "汇总订单数据 : ${KotlinUtils.jsonToString(summaryOrderInfo)}")
                    setTicketTotalView(summaryOrderInfo)
                }
            }
        }
    }


    /**
     * 初始化界面
     */
    private fun initView() {
        mBluePrint = findViewById(R.id.bl_print)
        mBtnDownload = findViewById(R.id.btn_download)
        mBluePrint.setOnClickListener(this)
        mBtnDownload.setOnClickListener(this)
        mTvReceiptTypeTitle = findViewById(R.id.id_tv_ticket_title)
        mLlContent1 = findViewById(R.id.id_ll_content_1)
        mLlContent2 = findViewById(R.id.id_ll_content_2)
        mLlContent3 = findViewById(R.id.id_ll_content_3)
        mLlContent4 = findViewById(R.id.id_ll_content_4)
        mTvDivider1 = findViewById(R.id.id_tv_divider_1)
        mTvDivider2 = findViewById(R.id.id_tv_divider_2)
        mTvDivider3 = findViewById(R.id.id_tv_divider_3)
        mTvTimeCreate = findViewById(
            R.id.id_tv_create_time_value
        )


        if (TextUtils.equals(mTicketType, TICKET_TYPE_TOTAL)) {
            //日结小票
            mTvDivider2.visibility = View.VISIBLE
            mLlContent3.visibility = View.VISIBLE
            mTvDivider3.visibility = View.VISIBLE
            mLlContent4.visibility = View.VISIBLE


            setDividerTopMargin(mTvDivider1, 16)
            setDividerTopMargin(mTvDivider2, 16)
            setDividerTopMargin(mTvDivider3, 16)
        }
    }


    /**
     * 设置分割线的 topMargin
     */
    private fun setDividerTopMargin(dividerView: TextView?, topMargin: Int) {
        dividerView?.let { divider ->
            val layoutParam = divider.layoutParams as LinearLayout.LayoutParams
            layoutParam.topMargin =
                KotlinUtils.dp2px(topMargin.toFloat(), this@TicketPreviewActivity)
            divider.layoutParams = layoutParam
        }
    }


    /**
     * 设置动态内容区的 topMargin
     */
    private fun setContentTopMargin(contentView: LinearLayout?, topMargin: Int) {
        contentView?.let { content ->
            val layoutParam = content.layoutParams as LinearLayout.LayoutParams
            layoutParam.topMargin =
                KotlinUtils.dp2px(topMargin.toFloat(), this@TicketPreviewActivity)
            content.layoutParams = layoutParam
        }
    }


    /**
     * 设置汇总小票显示
     */
    private fun setTicketTotalView(summaryOrderInfo: OrderTotalInfo) {
        LocaleUtils.switchLanguage(this@TicketPreviewActivity)
        //日结小票
        mTvReceiptTypeTitle.text = getString(R.string.tx_blue_print_data_sum)


        /**************  动态内容区 1  **************/
        //商户名称
        addHorizontalItemViewToContentLayout1(
            getString(R.string.shop_name), MainApplication.getInstance().getMchName(), 18
        )

        //商户编号
        addHorizontalItemViewToContentLayout1(
            getString(R.string.tx_blue_print_merchant_no),
            MainApplication.getInstance().getMchId(),
            16
        )

        //收银员
        addHorizontalItemViewToContentLayout1(
            getString(R.string.tx_user), summaryOrderInfo.userName, 16
        )

        //开始时间
        addHorizontalItemViewToContentLayout1(
            getString(R.string.date_start_time), summaryOrderInfo.startTime, 16
        )

        //结束时间
        addHorizontalItemViewToContentLayout1(
            getString(R.string.date_end_time), summaryOrderInfo.endTime, 16
        )


        /**************  动态内容区 2  **************/
        //收款金额
        if (MainApplication.getInstance().getFeeFh()
                .equals("¥", ignoreCase = true) && MainApplication.getInstance().getFeeType()
                .equals(ToastHelper.toStr(R.string.pay_yuan), ignoreCase = true)
        ) {
            addHorizontalItemViewToContentLayout2(
                ToastHelper.toStr(R.string.tx_pay_money), DateUtil.formatMoneyUtils(
                    summaryOrderInfo.countTotalFee.toDouble()
                ) + ToastHelper.toStr(R.string.pay_yuan), 18
            )
        } else {
            addHorizontalItemViewToContentLayout2(
                ToastHelper.toStr(R.string.tx_pay_money),
                MainApplication.getInstance().getFeeType() + DateUtil.formatMoneyUtils(
                    summaryOrderInfo.countTotalFee.toDouble()
                ),
                18
            )
        }

        //收款笔数
        addHorizontalItemViewToContentLayout2(
            ToastHelper.toStr(R.string.tx_bill_stream_pay_money_num),
            summaryOrderInfo.countTotalCount.toString(),
            16
        )


        //退款金额
        if (MainApplication.getInstance().getFeeFh()
                .equals("¥", ignoreCase = true) && MainApplication.getInstance().getFeeType()
                .equals(ToastHelper.toStr(R.string.pay_yuan), ignoreCase = true)
        ) {
            addHorizontalItemViewToContentLayout2(
                ToastHelper.toStr(R.string.tx_bill_stream_refund_money), DateUtil.formatMoneyUtils(
                    summaryOrderInfo.countTotalRefundFee.toDouble()
                ) + ToastHelper.toStr(R.string.pay_yuan), 16
            )
        } else {
            addHorizontalItemViewToContentLayout2(
                ToastHelper.toStr(R.string.tx_bill_stream_refund_money),
                MainApplication.getInstance().getFeeType() + DateUtil.formatMoneyUtils(
                    summaryOrderInfo.countTotalRefundFee.toDouble()
                ),
                16
            )
        }

        //退款笔数
        addHorizontalItemViewToContentLayout2(
            ToastHelper.toStr(R.string.tx_bill_reufun_total_num),
            summaryOrderInfo.countTotalRefundCount.toString(),
            16
        )


        //收款总计
        if (MainApplication.getInstance().getFeeFh()
                .equals("¥", ignoreCase = true) && MainApplication.getInstance().getFeeType()
                .equals(ToastHelper.toStr(R.string.pay_yuan), ignoreCase = true)
        ) {
            addHorizontalItemViewToContentLayout2(
                ToastHelper.toStr(R.string.tx_blue_print_pay_total),
                DateUtil.formatMoneyUtils((summaryOrderInfo.countTotalFee - summaryOrderInfo.countTotalRefundFee).toDouble()) + ToastHelper.toStr(
                    R.string.pay_yuan
                ),
                16,
                isValueTextBold = true
            )
        } else {
            addHorizontalItemViewToContentLayout2(
                ToastHelper.toStr(R.string.tx_blue_print_pay_total),
                MainApplication.getInstance()
                    .getFeeType() + DateUtil.formatMoneyUtils((summaryOrderInfo.countTotalFee - summaryOrderInfo.countTotalRefundFee).toDouble()),
                16,
                isValueTextBold = true
            )
        }


        /**************  动态内容区 3  **************/
        if (summaryOrderInfo.countTotalTipFeeCount != 0L) {
            //小费金额
            addHorizontalItemViewToContentLayout3(
                ToastHelper.toStr(R.string.tip_amount),
                MainApplication.getInstance().getFeeType() + DateUtil.formatMoneyUtils(
                    summaryOrderInfo.countTotalTipFee.toDouble()
                ),
                18
            )

            //小费笔数
            addHorizontalItemViewToContentLayout3(
                ToastHelper.toStr(R.string.tip_count),
                summaryOrderInfo.countTotalTipFeeCount.toString(),
                16
            )
        } else {
            //没有小费
            mLlContent3.visibility = View.GONE
            mTvDivider2.visibility = View.GONE
        }


        /**************  动态内容区 4  **************/
        val list = summaryOrderInfo.orderTotalItemInfo
        if (list != null && list.size > 0) {
            val map = HashMap<Int, Any>()
            for (itemInfo in list) {
                when (itemInfo.payTypeId) {
                    1 -> map[0] = itemInfo
                    2 -> map[1] = itemInfo
                    4 -> map[2] = itemInfo
                    12 -> map[3] = itemInfo
                    else -> map[itemInfo.payTypeId] = itemInfo
                }
            }
            for (key in map.keys) {
                val itemInfo = map[key] as OrderTotalItemInfo
                //金额
                if (MainApplication.getInstance().getFeeFh().equals(
                        "¥", ignoreCase = true
                    ) && MainApplication.getInstance().getFeeType()
                        .equals(ToastHelper.toStr(R.string.pay_yuan), ignoreCase = true)
                ) {
                    addHorizontalItemViewToContentLayout4(
                        MainApplication.getInstance()
                            .getPayTypeMap()[itemInfo.payTypeId.toString()],
                        "null",
                        20,
                        isTitleTextBold = true,
                        isHideValueTextView = true
                    )
                    addHorizontalItemViewToContentLayout4(
                        ToastHelper.toStr(R.string.et_code_moeny),
                        DateUtil.formatMoneyUtils(itemInfo.successFee.toDouble()) + ToastHelper.toStr(
                            R.string.pay_yuan
                        ),
                        16
                    )
                } else {
                    addHorizontalItemViewToContentLayout4(
                        MainApplication.getInstance()
                            .getPayTypeMap()[itemInfo.payTypeId.toString()],
                        "null",
                        20,
                        isTitleTextBold = true,
                        isHideValueTextView = true
                    )
                    addHorizontalItemViewToContentLayout4(
                        ToastHelper.toStr(R.string.et_code_moeny),
                        MainApplication.getInstance().getFeeType() + DateUtil.formatMoneyUtils(
                            itemInfo.successFee.toDouble()
                        ),
                        16
                    )
                }
                //笔数
                addHorizontalItemViewToContentLayout4(
                    ToastHelper.toStr(R.string.tv_settle_count),
                    itemInfo.successCount.toString(),
                    16
                )
            }
        } else {
            mLlContent4.visibility = View.GONE
            mTvDivider3.visibility = View.GONE
        }

        mTvTimeCreate.text = DateUtil.formatTime(System.currentTimeMillis())
    }


    /**
     * 设置收款退款小票显示
     */
    private fun setTicketReceiveRefundView(orderModel: Order) {
        LocaleUtils.switchLanguage(this@TicketPreviewActivity)
        if (orderModel.isPay) {
            //收款小票
            mTvReceiptTypeTitle.text = getString(R.string.tx_blue_print_pay_note)
        } else {
            //退款小票
            mTvReceiptTypeTitle.text = getString(R.string.tx_blue_print_refund_note)
        }

        /**************  动态内容区 1  **************/
        //商户名称
        addHorizontalItemViewToContentLayout1(
            getString(R.string.shop_name), MainApplication.getInstance().getMchName(), 25
        )

        //商户编号
        addHorizontalItemViewToContentLayout1(
            getString(R.string.tx_blue_print_merchant_no),
            MainApplication.getInstance().getMchId(),
            18
        )


        //收银员
        if (orderModel.isPay) {
            if (MainApplication.getInstance().isAdmin(0) && !mIsCashierVisible) {
                addHorizontalItemViewToContentLayout1(
                    getString(R.string.tx_user), MainApplication.getInstance().getRealName(), 18
                )
            }

            if (mIsCashierVisible) {
                addHorizontalItemViewToContentLayout1(
                    getString(R.string.tx_user), orderModel.getUserName(), 18
                )
            }
        }


        if (orderModel.isPay) {
            //收款时间
            addHorizontalItemViewToContentLayout1(
                getString(R.string.tx_bill_stream_time), orderModel.addTimeNew, 18
            )
        } else {
            //退款时间
            addHorizontalItemViewToContentLayout1(
                getString(R.string.tx_blue_print_refund_time), orderModel.addTimeNew, 18
            )

        }


        /**************  动态内容区 2  **************/
        //退款单号
        if (!orderModel.isPay) {
            addVerticalItemViewToContentLayout2(
                getString(R.string.refund_odd_numbers), orderModel.refundNo, 25
            )
        }

        //平台订单号
        addVerticalItemViewToContentLayout2(
            getString(R.string.tx_order_no), orderModel.orderNoMch, 25
        )



        if (orderModel.isPay) {
            //收款
            if (MainApplication.getInstance()
                    .getPayTypeMap() != null && MainApplication.getInstance().getPayTypeMap()
                    .isNotEmpty()
            ) {
                addVerticalItemViewToContentLayout2(
                    MainApplication.getInstance()
                        .getPayTypeMap()[orderModel.apiCode] + " " + getString(R.string.tx_orderno),
                    orderModel.getTransactionId(),
                    24
                )
            }

            //支付方式
            addHorizontalItemViewToContentLayout2(
                getString(R.string.tx_bill_stream_choice_title), orderModel.getTradeName(), 24
            )
//            addVerticalItemViewToContentLayout2(
//                getString(R.string.tx_bill_stream_choice_title),
//                orderModel.getTradeName(),
//                24
//            )

            //订单状态
            addHorizontalItemViewToContentLayout2(
                getString(R.string.tx_bill_stream_statr),
                MainApplication.getInstance().getTradeTypeMap()[orderModel.getTradeState()
                    .toString()],
                18
            )
//            addVerticalItemViewToContentLayout2(
//                getString(R.string.tx_bill_stream_statr),
//                MainApplication.getTradeTypeMap()[orderModel.getTradeState()],
//                24
//            )

            //备注
            addHorizontalItemViewToContentLayout2(
                getString(R.string.tx_bill_stream_attach), orderModel.attach, 18
            )



            if (MainApplication.getInstance().getFeeFh()
                    .equals("¥", ignoreCase = true) && MainApplication.getInstance().getFeeType()
                    .equals(getString(R.string.pay_yuan), ignoreCase = true)
            ) {
                if (orderModel.daMoney > 0) {
                    val actualMoney = orderModel.daMoney + orderModel.orderFee
                    addHorizontalItemViewToContentLayout2(
                        getString(R.string.tx_blue_print_money),
                        DateUtil.formatRMBMoneyUtils(actualMoney.toDouble()) + getString(R.string.pay_yuan),
                        20,
                        isValueTextBold = true
                    )
//                    addVerticalItemViewToContentLayout2(
//                        getString(R.string.tx_blue_print_money),
//                        DateUtil.formatRMBMoneyUtils(actualMoney.toDouble()) + getString(R.string.pay_yuan),
//                        24,
//                        isValueTextGravityRight = true,
//                        isValueTextBold = true
//                    )
                } else {
                    addHorizontalItemViewToContentLayout2(
                        getString(R.string.tx_blue_print_money),
                        DateUtil.formatRMBMoneyUtils(orderModel.orderFee.toDouble()) + getString(R.string.pay_yuan),
                        18,
                        isValueTextBold = true
                    )
//                    addVerticalItemViewToContentLayout2(
//                        getString(R.string.tx_blue_print_money),
//                        DateUtil.formatRMBMoneyUtils(orderModel.orderFee.toDouble()) + getString(R.string.pay_yuan),
//                        24,
//                        isValueTextGravityRight = true,
//                        isValueTextBold = true
//                    )
                }
            } else {
                if (MainApplication.getInstance().isSurchargeOpen()) {
                    if (!TextUtils.equals(
                            orderModel.tradeType,
                            "pay.alipay.auth.micropay.freeze"
                        ) && !TextUtils.equals(
                            orderModel.tradeType,
                            "pay.alipay.auth.native.freeze"
                        )
                    ) {
                        //金额
                        addHorizontalItemViewToContentLayout2(
                            getString(R.string.tx_blue_print_money),
                            MainApplication.getInstance().getFeeType() + DateUtil.formatMoneyUtils(
                                orderModel.orderFee.toDouble()
                            ),
                            20,
                            isValueTextBold = true
                        )
//                        addVerticalItemViewToContentLayout2(
//                            getString(R.string.tx_blue_print_money),
//                            MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.orderFee.toDouble()),
//                            24,
//                            isValueTextGravityRight = true
//                        )

                        //附加费
                        addHorizontalItemViewToContentLayout2(
                            getString(R.string.tx_surcharge),
                            MainApplication.getInstance().getFeeType() + DateUtil.formatMoneyUtils(
                                orderModel.surcharge.toDouble()
                            ),
                            18,
                            isValueTextBold = true
                        )
//                        addVerticalItemViewToContentLayout2(
//                            getString(R.string.tx_surcharge),
//                            MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.surcharge.toDouble()),
//                            24,
//                            isValueTextGravityRight = true
//                        )
                    }
                }


                if (!TextUtils.isEmpty(orderModel.tradeType) && !TextUtils.equals(
                        orderModel.tradeType,
                        "pay.alipay.auth.micropay.freeze"
                    ) && !TextUtils.equals(orderModel.tradeType, "pay.alipay.auth.native.freeze")
                ) {
                    // 优惠详情 --- 如果列表有数据，则展示，有多少条，展示多少条
                    if (orderModel.getUplanDetailsBeans() != null && orderModel.getUplanDetailsBeans().size > 0) {
                        for (i in 0 until orderModel.getUplanDetailsBeans().size) {
                            //如果是UPlan Discount或者Instant Discount，则采用本地词条，带不同语言的翻译
                            val stringUPlanDetails =
                                if (orderModel.getUplanDetailsBeans()[i].getDiscountNote()
                                        .equals("Uplan discount", ignoreCase = true)
                                ) {
                                    getString(R.string.uplan_Uplan_discount)
                                } else if (orderModel.getUplanDetailsBeans()[i].getDiscountNote()
                                        .equals("Instant Discount", ignoreCase = true)
                                ) {
                                    getString(R.string.uplan_Uplan_instant_discount)
                                } else {
                                    //否则，后台传什么展示什么
                                    orderModel.getUplanDetailsBeans()[i].getDiscountNote()
                                }
                            addHorizontalItemViewToContentLayout2(
                                stringUPlanDetails,
                                MainApplication.getInstance()
                                    .getFeeType() + " " + "-" + orderModel.getUplanDetailsBeans()[i].getDiscountAmt(),
                                18,
                                isValueTextBold = true
                            )
//                            addVerticalItemViewToContentLayout2(
//                                stringUPlanDetails,
//                                MainApplication.feeType + " " + "-" + orderModel.getUplanDetailsBeans()[i].getDiscountAmt(),
//                                24,
//                                isValueTextGravityRight = true
//                            )
                        }
                    }


                    // 消费者实付金额 --- 如果不为0 ，就展示
                    if (orderModel.costFee != 0L) {
                        addHorizontalItemViewToContentLayout2(
                            getString(R.string.uplan_Uplan_actual_paid_amount),
                            MainApplication.getInstance()
                                .getFeeType() + " " + DateUtil.formatMoneyUtils(
                                orderModel.costFee.toDouble()
                            ),
                            18,
                            isValueTextBold = true
                        )

//                        addVerticalItemViewToContentLayout2(
//                            getString(R.string.uplan_Uplan_actual_paid_amount),
//                            MainApplication.feeType + " " + DateUtil.formatMoneyUtils(orderModel.costFee.toDouble()),
//                            24,
//                            isValueTextGravityRight = true,
//                            isValueTextBold = true
//                        )
                    }
                }

                //总额
                addHorizontalItemViewToContentLayout2(
                    getString(R.string.tv_charge_total),
                    MainApplication.getInstance().getFeeType() + DateUtil.formatMoneyUtils(
                        orderModel.getTotalFee().toDouble()
                    ),
                    20,
                    isValueTextBold = true
                )

//                addVerticalItemViewToContentLayout2(
//                    getString(R.string.tv_charge_total),
//                    MainApplication.feeType + DateUtil.formatMoneyUtils(
//                        orderModel.getTotalFee().toDouble()
//                    ),
//                    24,
//                    isValueTextGravityRight = true,
//                    isValueTextBold = true
//                )


                if (orderModel.cashFeel > 0) {
                    addVerticalItemViewToContentLayout2(
                        "null",
                        getString(R.string.pay_yuan).toString() + DateUtil.formatRMBMoneyUtils(
                            orderModel.cashFeel.toDouble()
                        ),
                        8,
                        isValueTextGravityRight = true,
                        isHideTitleTextView = true,
                        isValueTextBold = true
                    )
                }
            }
        } else {
            //退款
            //支付方式
            addHorizontalItemViewToContentLayout2(
                getString(R.string.tx_bill_stream_choice_title), orderModel.getTradeName(), 24
            )
//            addVerticalItemViewToContentLayout2(
//                getString(R.string.tx_bill_stream_choice_title),
//                orderModel.getTradeName(),
//                24
//            )

//            //办理人
//            if (!TextUtils.isEmpty(orderModel.getUserName())) {
//                addHorizontalItemViewToContentLayout2(
//                    getString(R.string.tv_refund_peop),
//                    orderModel.getUserName(),
//                    18
//                )
////                addVerticalItemViewToContentLayout2(
////                    getString(R.string.tv_refund_peop),
////                    orderModel.getUserName(),
////                    24
////                )
//            }


            //退款状态
            if (MainApplication.getInstance()
                    .getRefundStateMap() != null && MainApplication.getInstance()
                    .getRefundStateMap()
                    .isNotEmpty()
            ) {
                addHorizontalItemViewToContentLayout2(
                    getString(R.string.tv_refund_state),
                    MainApplication.getInstance().getRefundStateMap()[orderModel.getTradeState()
                        .toString() + ""],
                    24
                )
//                addVerticalItemViewToContentLayout2(
//                    getString(R.string.tv_refund_state),
//                    MainApplication.getInstance().getRefundStateMap()[orderModel.getTradeState() + ""],
//                    24
//                )
            }



            if (MainApplication.getInstance().getFeeFh()
                    .equals("¥", ignoreCase = true) && MainApplication.getInstance().getFeeType()
                    .equals(getString(R.string.pay_yuan), ignoreCase = true)
            ) {
                //金额
                addHorizontalItemViewToContentLayout2(
                    getString(R.string.tx_blue_print_money), DateUtil.formatRMBMoneyUtils(
                        orderModel.getTotalFee().toDouble()
                    ) + getString(R.string.pay_yuan), 20, isValueTextBold = true
                )
//                addVerticalItemViewToContentLayout2(
//                    getString(R.string.tx_blue_print_money),
//                    DateUtil.formatRMBMoneyUtils(
//                        orderModel.getTotalFee().toDouble()
//                    ) + getString(R.string.pay_yuan),
//                    24,
//                    isValueTextGravityRight = true,
//                    isValueTextBold = true
//                )

                //退款金额
                addHorizontalItemViewToContentLayout2(
                    getString(R.string.tx_bill_stream_refund_money),
                    DateUtil.formatRMBMoneyUtils(orderModel.refundMoney.toDouble()) + getString(
                        R.string.pay_yuan
                    ),
                    18,
                    isValueTextBold = true
                )
//                addVerticalItemViewToContentLayout2(
//                    getString(R.string.tx_bill_stream_refund_money),
//                    DateUtil.formatRMBMoneyUtils(orderModel.refundMoney.toDouble()) + getString(
//                        R.string.pay_yuan
//                    ),
//                    24,
//                    isValueTextGravityRight = true,
//                    isValueTextBold = true
//                )
            } else {
                //总额
                addHorizontalItemViewToContentLayout2(
                    getString(R.string.tv_charge_total),
                    MainApplication.getInstance().getFeeType() + DateUtil.formatMoneyUtils(
                        orderModel.getTotalFee().toDouble()
                    ),
                    20,
                    isValueTextBold = true
                )
//                addVerticalItemViewToContentLayout2(
//                    getString(R.string.tv_charge_total),
//                    MainApplication.feeType + DateUtil.formatMoneyUtils(
//                        orderModel.getTotalFee().toDouble()
//                    ),
//                    24,
//                    isValueTextGravityRight = true,
//                    isValueTextBold = true
//                )

                //退款金额
                addHorizontalItemViewToContentLayout2(
                    getString(R.string.tx_bill_stream_refund_money),
                    MainApplication.getInstance().getFeeType() + DateUtil.formatMoneyUtils(
                        orderModel.refundMoney.toDouble()
                    ),
                    18,
                    isValueTextBold = true
                )
//                addVerticalItemViewToContentLayout2(
//                    getString(R.string.tx_bill_stream_refund_money),
//                    MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.refundMoney.toDouble()),
//                    24,
//                    isValueTextGravityRight = true,
//                    isValueTextBold = true
//                )

                if (orderModel.cashFeel > 0) {
                    addVerticalItemViewToContentLayout2(
                        "null",
                        getString(R.string.pay_yuan).toString() + DateUtil.formatMoneyUtils(
                            orderModel.cashFeel.toDouble()
                        ),
                        8,
                        isValueTextGravityRight = true,
                        isHideTitleTextView = true,
                        isValueTextBold = true
                    )
                }
            }

            if (!TextUtils.isEmpty(orderModel.printInfo)) {
                addMultiLinesItemViewToContentLayout2(
                    orderModel.printInfo, 32
                )
            }
        }

        if (!TextUtils.isEmpty(orderModel.orderNoMch) && orderModel.isPay) {
            //二维码
            val qrCode = orderModel.orderNoMch
            val text = getString(R.string.refound_QR_code_no_thanks)

            addImageItemViewToContentLayout2(
                text, qrCode, 40
            )
        }


        mTvTimeCreate.text = DateUtil.formatTime(System.currentTimeMillis())

    }


    /**
     * 向   动态内容区1   添加横向item
     */
    private fun addHorizontalItemViewToContentLayout1(
        titleText: String, valueText: String?, topMargin: Int
    ) {
        addItemView(
            mLlContent1,
            ItemTicketView.LAYOUT_TYPE_HORIZONTAL,
            titleText,
            valueText,
            topMargin,
            isValueTextGravityRight = true
        )
    }


    /**
     * 向   动态内容区2   添加横向item
     */
    private fun addHorizontalItemViewToContentLayout2(
        titleText: String?, valueText: String?, topMargin: Int, isValueTextBold: Boolean = false
    ) {
        addItemView(
            mLlContent2,
            ItemTicketView.LAYOUT_TYPE_HORIZONTAL,
            titleText,
            valueText,
            topMargin,
            isValueTextGravityRight = true,
            isValueTextBold = isValueTextBold
        )
    }


    /**
     * 向   动态内容区3   添加横向item
     */
    private fun addHorizontalItemViewToContentLayout3(
        titleText: String?, valueText: String?, topMargin: Int, isValueTextBold: Boolean = false
    ) {
        addItemView(
            mLlContent3,
            ItemTicketView.LAYOUT_TYPE_HORIZONTAL,
            titleText,
            valueText,
            topMargin,
            isValueTextGravityRight = true,
            isValueTextBold = isValueTextBold
        )
    }


    /**
     * 向   动态内容区3   添加横向item
     */
    private fun addHorizontalItemViewToContentLayout4(
        titleText: String?,
        valueText: String?,
        topMargin: Int,
        isValueTextBold: Boolean = false,
        isTitleTextBold: Boolean = false,
        isHideValueTextView: Boolean = false
    ) {
        addItemView(
            mLlContent4,
            ItemTicketView.LAYOUT_TYPE_HORIZONTAL,
            titleText,
            valueText,
            topMargin,
            isValueTextGravityRight = true,
            isValueTextBold = isValueTextBold,
            isTitleTextBold = isTitleTextBold,
            isHideValueTextView = isHideValueTextView
        )
    }


    /**
     * 向   动态内容区2   添加横向item
     */
    private fun addVerticalItemViewToContentLayout2(
        titleText: String?,
        valueText: String?,
        topMargin: Int,
        isValueTextGravityRight: Boolean = false,
        isHideTitleTextView: Boolean = false,
        isValueTextBold: Boolean = false
    ) {
        addItemView(
            mLlContent2,
            ItemTicketView.LAYOUT_TYPE_VERTICAL,
            titleText,
            valueText,
            topMargin,
            isValueTextGravityRight,
            isHideTitleTextView,
            isValueTextBold
        )
    }


    /**
     * 向   动态内容区2   添加多行item
     */
    private fun addMultiLinesItemViewToContentLayout2(
        valueText: String?, topMargin: Int
    ) {
        addItemView(
            mLlContent2,
            ItemTicketView.LAYOUT_TYPE_MULTI_LINES,
            "***",
            valueText,
            topMargin,
            isHideTitleTextView = true
        )
    }


    /**
     * 向   动态内容区2   添加二维码
     */
    private fun addImageItemViewToContentLayout2(
        titleText: String,
        qrCode: String?,
        topMargin: Int,
    ) {
        addQrCodeItemView(
            mLlContent2, titleText, qrCode, topMargin
        )
    }


    /**
     * 添加二维码条目
     */
    private fun addQrCodeItemView(
        rootView: LinearLayout,
        titleText: String,
        qrCode: String?,
        topMargin: Int,
    ) {
        qrCode?.let { qr ->
            val itemView =
                ItemTicketView(ItemTicketView.LAYOUT_TYPE_OR_CODE, this@TicketPreviewActivity)
            itemView.setTitleText(titleText)
            itemView.setQrCode(qr)
            rootView.addView(itemView)
            itemView.setQrLayoutParam(topMargin)
        }

    }


    /**
     * 动态添加条目
     */
    private fun addItemView(
        rootView: LinearLayout,
        itemLayoutType: String,
        titleText: String?,
        valueText: String?,
        topMargin: Int,
        isValueTextGravityRight: Boolean = false,
        isHideTitleTextView: Boolean = false,
        isValueTextBold: Boolean = false,
        isTitleTextBold: Boolean = false,
        isHideValueTextView: Boolean = false
    ) {
        if (!TextUtils.equals(titleText, "***") && isHideTitleTextView) {
            if (TextUtils.isEmpty(valueText)) {
                return
            }
        } else {
            if (TextUtils.isEmpty(titleText) || TextUtils.isEmpty(valueText)) {
                return
            }
        }

        val itemView = ItemTicketView(itemLayoutType, this@TicketPreviewActivity)
        itemView.setTitleText(titleText)
        itemView.setValueText(valueText)
        itemView.setValueTextRightGravity(isValueTextGravityRight)
        itemView.setValueTextBold(isValueTextBold)
        itemView.setTitleTextBold(isTitleTextBold)
        itemView.hideTitleTextView(isHideTitleTextView)
        itemView.hideValueTextView(isHideValueTextView)
        rootView.addView(itemView)
        itemView.setLayoutParams(topMargin)
    }


    /**
     * 控件点击事件
     */
    override fun onClick(view: View?) {
        view?.let { v ->
            when (v.id) {
                R.id.bl_print -> {
                    print()
                }
                R.id.btn_download -> {
                    //保存图片
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        //Android 6.0 以上需要动态申请权限
                        requestStoragePermission(true)
                    } else {
                        downloadView()
                    }
                }
                else -> {
                    //viewId分支
                }
            }
        }
    }


    /**
     * 打印
     */
    private fun print() {
        when (mTicketType) {
            TICKET_TYPE_RECEIVE -> {
                //收款打印
                mOrder?.let { order ->
                    bluePrintReceive(order)
                }
            }
            TICKET_TYPE_REFUND -> {
                //退款打印
                mOrder?.let { order ->
                    bluePrintRefund(order)
                }
            }
            TICKET_TYPE_TOTAL -> {
                //汇总打印
                mOrderTotal?.let { orderTotal ->
                    bluePrintSummary(orderTotal)
                }
            }
            else -> {
                //打印分支
            }
        }
    }


    /**
     * 设置toolBar
     */
    override fun setupTitleBar() {
        super.setupTitleBar()
        titleBar.setLeftButtonVisible(true)
//        when (mTicketType) {
//            TICKET_TYPE_RECEIVE -> titleBar.setTitle(R.string.tx_blue_print_pay_note)
//            TICKET_TYPE_REFUND -> titleBar.setTitle(R.string.tx_blue_print_refund_note)
//            TICKET_TYPE_TOTAL -> titleBar.setTitle(R.string.tx_blue_print_data_sum)
//        }
        titleBar.setTitle(mTitleStrId)
        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.string_ticket_share))
        titleBar.setOnTitleBarClickListener(object : TitleBar.OnTitleBarClickListener {
            override fun onLeftButtonClick() {
                finish()
            }

            override fun onRightButtonClick() {

            }

            override fun onRightLayClick() {

            }

            override fun onRightButLayClick() {
                Logger.i(TAG, "点击订单分享")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    //Android 6.0 以上需要动态申请权限
                    requestStoragePermission()
                } else {
                    shareView()
                }
            }
        })
    }

    /**
     * 获取存储权限
     *
     * Android 6.0 以上需要动态申请权限
     */
    private fun requestStoragePermission(isDownloadView: Boolean = false) {
        if (!KotlinUtils.isPermissionsGranted(this, mPermissions)) {
            if (KotlinUtils.shouldShowRequestPermissionRationale(this, mPermissions)) {
                KotlinUtils.showMissingPermissionDialogStartAppSetting(
                    this@TicketPreviewActivity,
                    getString(R.string.setting_permission_storage_lack),
                    getStringById(R.string.title_setting),
                    getString(R.string.btnCancel),
                    mPermissions
                )
            } else {
                val requestCode = if (isDownloadView) EXTERNAL_STORAGE_REQUEST_DOWNLOAD_PERMISSION
                else EXTERNAL_STORAGE_REQUEST_PERMISSION
                ActivityCompat.requestPermissions(
                    this, mPermissions, requestCode
                )
            }
        } else {
            if (isDownloadView) {
                downloadView()
            } else {
                shareView()
            }
        }
    }


    /**
     * 权限申请结果
     */
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            EXTERNAL_STORAGE_REQUEST_PERMISSION -> {
                //存储权限, 分享小票
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    //拒绝
                    Logger.i(TAG, "存储权限被拒绝")
                } else {
                    //通过
                    shareView()
                }
            }
            EXTERNAL_STORAGE_REQUEST_DOWNLOAD_PERMISSION -> {
                //存储权限, 保存小票
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    //拒绝
                    Logger.i(TAG, "存储权限被拒绝")
                } else {
                    //通过
                    downloadView()
                }
            }
            else -> {

            }
        }
    }


    /**
     * 是否隐藏半圆黑点
     *
     * 分享图片时要隐藏
     */
    private fun hideHalfCircular(isHide: Boolean) {
        val tvRight = findViewById<TextView>(R.id.id_tv_half_circular_right)
        val tvLeft = findViewById<TextView>(R.id.id_tv_half_circular_left)

        if (isHide) {
            //分享时隐藏黑点
            tvRight.visibility = View.INVISIBLE
            tvLeft.visibility = View.INVISIBLE
        } else {
            tvRight.visibility = View.VISIBLE
            tvLeft.visibility = View.VISIBLE
        }
    }


    /**
     * 保存小票到本地
     */
    private fun downloadView() {
        hideHalfCircular(true)
        val isSaveSuccess = ImageUtil.saveTicketPreViewBitmapFile(
            this@TicketPreviewActivity, "db_spay", findViewById<LinearLayout>(R.id.id_ll_share_view)
        )
        hideHalfCircular(false)
        if (isSaveSuccess) {
            showToastInfo(ToastHelper.toStr(R.string.instapay_save_success))
        } else {
            showToastInfo(ToastHelper.toStr(R.string.instapay_save_failed))
        }
    }


    /**
     * 分享小票
     */
    private fun shareView() {
        val fileName = "db_share"
        hideHalfCircular(true)
        KotlinUtils.shareImage(
            this@TicketPreviewActivity, ImageUtil.saveTicketPreViewBitmapFileDeleteOld(
                this@TicketPreviewActivity,
                fileName,
                findViewById<LinearLayout>(R.id.id_ll_share_view)
            )
        )
        hideHalfCircular(false)
    }


//    /**
//     * 获取分享图片的uri
//     */
//    private fun getSharePictureUri(): Uri? {
//        val shareView = findViewById<LinearLayout>(R.id.id_ll_share_view)
//        val shareBitmap = KotlinUtils.createBitmapByView(shareView)
//        val uri = KotlinUtils.tempSaveImageToSdCardByScopedStorageByShareImage(
//            this@TicketPreviewActivity,
//            shareBitmap
//        )
//        Logger.i("TAG_ZLZ", "图片uri : $uri")
//        return uri
//    }


    /**
     * 蓝牙打印 - 汇总
     */
    private fun bluePrintSummary(summaryOrderInfo: OrderTotalInfo) {
        LocaleUtils.switchLanguage(this@TicketPreviewActivity)
        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印
        if (BuildConfig.IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)) {
            PrintSummary.PrintSummaryText(this@TicketPreviewActivity, summaryOrderInfo)
        } else { //Android版本，则走蓝牙打印机的流程
            if (!MainApplication.getInstance()
                    .getBluePrintSetting() && !BuildConfig.IS_POS_VERSION
            ) { //关闭蓝牙打印
                KotlinUtils.showBlueSettingDialog(this@TicketPreviewActivity)
                return
            }
            if (MainApplication.getInstance()
                    .getBluetoothSocket() != null && MainApplication.getInstance()
                    .getBluetoothSocket().isConnected
            ) {
                BluePrintUtil.printDateSum(summaryOrderInfo)
            } else {
                val bluetoothDeviceAddress = MainApplication.getInstance().getBlueDeviceAddress()
                if (!StringUtil.isEmptyOrNull(bluetoothDeviceAddress)) {
                    val isSucc = BluePrintUtil.blueConnent(
                        bluetoothDeviceAddress, this@TicketPreviewActivity
                    )
                    if (isSucc) {
                        BluePrintUtil.printDateSum(summaryOrderInfo)
                    }
                } else {
                    KotlinUtils.showBlueSettingDialog(this@TicketPreviewActivity)
                }
            }
        }
    }


    /**
     * 蓝牙打印 - 退款
     */
    private fun bluePrintRefund(orderModel: Order) {
        LocaleUtils.switchLanguage(this@TicketPreviewActivity)
        orderModel.setPartner(getString(R.string.tv_pay_user_stub))
        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印
        if (BuildConfig.IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)) {
            PrintOrder.printOrderDetails(
                this@TicketPreviewActivity, false, orderModel
            )
            val task: TimerTask = object : TimerTask() {
                override fun run() {
                    try {
                        orderModel.setPartner(getString(R.string.tv_pay_user_stub))
                        PrintOrder.printOrderDetails(
                            this@TicketPreviewActivity, false, orderModel
                        )
                    } catch (e: java.lang.Exception) {
                        SentryUtils.uploadTryCatchException(
                            e, SentryUtils.getClassNameAndMethodName()
                        )
                    }
                }
            }
            val timer = Timer()
            timer.schedule(task, PrintClient.getInstance().printDelay)
        } else {
            if (!MainApplication.getInstance()
                    .getBluePrintSetting() && !BuildConfig.IS_POS_VERSION
            ) { //关闭蓝牙打印
                KotlinUtils.showBlueSettingDialog(this@TicketPreviewActivity)
                return
            }
            if (MainApplication.getInstance()
                    .getBluetoothSocket() != null && MainApplication.getInstance()
                    .getBluetoothSocket().isConnected
            ) {
                runOnUiThread(Runnable {
                    try {
                        BluePrintUtil.print(false, orderModel)
                        //打印第二联
                        try {
                            Thread.sleep(3000)
                            orderModel.setPartner(getString(R.string.tv_pay_mch_stub))
                            BluePrintUtil.print(false, orderModel)
                        } catch (e: InterruptedException) {
                            SentryUtils.uploadTryCatchException(
                                e, SentryUtils.getClassNameAndMethodName()
                            )
                        }
                    } catch (e: java.lang.Exception) {
                        SentryUtils.uploadTryCatchException(
                            e, SentryUtils.getClassNameAndMethodName()
                        )
                    }
                })
            } else {
                val bluetoothDeviceAddress = MainApplication.getInstance().getBlueDeviceAddress()
                if (!StringUtil.isEmptyOrNull(bluetoothDeviceAddress)) {
                    val isSucc = BluePrintUtil.blueConnent(
                        bluetoothDeviceAddress, this@TicketPreviewActivity
                    )
                    if (isSucc) {
                        try {
                            BluePrintUtil.print(false, orderModel)
                            val thread = Thread.currentThread()
                            Thread.sleep(3000)
                            orderModel.setPartner(getString(R.string.tv_pay_mch_stub))
                            BluePrintUtil.print(false, orderModel)
                        } catch (e: InterruptedException) {
                            SentryUtils.uploadTryCatchException(
                                e, SentryUtils.getClassNameAndMethodName()
                            )
                        }
                    }
                } else {
                    KotlinUtils.showBlueSettingDialog(this@TicketPreviewActivity)
                }
            }
        }
    }


    /**
     * 蓝牙打印 - 收款
     */
    private fun bluePrintReceive(orderModel: Order) {
        LocaleUtils.switchLanguage(this@TicketPreviewActivity)
        orderModel.isPay = true
        orderModel.setPartner(getString(R.string.tv_pay_user_stub))
        if (BuildConfig.IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)) {
            //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印
            PrintOrder.printOrderDetails(
                this@TicketPreviewActivity, mIsCashierVisible, orderModel
            )

            val task: TimerTask = object : TimerTask() {
                override fun run() {
                    try {
                        orderModel.isPay = true
                        orderModel.setPartner(getString(R.string.tv_pay_user_stub))
                        PrintOrder.printOrderDetails(
                            this@TicketPreviewActivity, mIsCashierVisible, orderModel
                        )
                    } catch (e: Exception) {
                        SentryUtils.uploadTryCatchException(
                            e, SentryUtils.getClassNameAndMethodName()
                        )
                    }
                }
            }
            val timer = Timer()
            timer.schedule(task, PrintClient.getInstance().printDelay)
        } else {
            //关闭蓝牙打印
            if (!MainApplication.getInstance()
                    .getBluePrintSetting() && !BuildConfig.IS_POS_VERSION
            ) {
                KotlinUtils.showBlueSettingDialog(this@TicketPreviewActivity)
                return
            }

            if (MainApplication.getInstance()
                    .getBluetoothSocket() != null && MainApplication.getInstance()
                    .getBluetoothSocket().isConnected
            ) {
                this@TicketPreviewActivity.runOnUiThread {
                    try {
                        BluePrintUtil.print(mIsCashierVisible, orderModel)
                        Handler().postDelayed({
                            orderModel.setPartner(getString(R.string.tv_pay_mch_stub))
                            BluePrintUtil.print(mIsCashierVisible, orderModel)
                        }, 3000)

                    } catch (e: Exception) {
                        SentryUtils.uploadTryCatchException(
                            e, SentryUtils.getClassNameAndMethodName()
                        )
                        Log.e(TAG, Log.getStackTraceString(e))
                    }
                }
            } else {
                val bluetoothDeviceAddress = MainApplication.getInstance().getBlueDeviceAddress()
                if (TextUtils.isEmpty(bluetoothDeviceAddress)) {
                    KotlinUtils.showBlueSettingDialog(this@TicketPreviewActivity)
                } else {
                    val isSuc = BluePrintUtil.blueConnent(
                        bluetoothDeviceAddress, this@TicketPreviewActivity
                    )

                    if (isSuc) {
                        try {
                            BluePrintUtil.print(mIsCashierVisible, orderModel)
                            Handler().postDelayed({
                                orderModel.setPartner(getString(R.string.tv_pay_mch_stub))
                                BluePrintUtil.print(mIsCashierVisible, orderModel)
                            }, 3000)
                        } catch (e: Exception) {
                            SentryUtils.uploadTryCatchException(
                                e, SentryUtils.getClassNameAndMethodName()
                            )
                        }
                    }
                }
            }
        }
    }
}