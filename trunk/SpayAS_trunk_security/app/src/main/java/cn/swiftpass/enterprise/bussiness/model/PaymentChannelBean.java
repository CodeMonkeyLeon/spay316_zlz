package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created by aijingya on 2019/4/22.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: ${TODO}(报表饼状图的基本数据)
 * @date 2019/4/22.16:48.
 */
public class PaymentChannelBean implements Serializable {
    public Long getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Long transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public int getTransactionCount() {
        return transactionCount;
    }

    public void setTransactionCount(int transactionCount) {
        this.transactionCount = transactionCount;
    }

    public int getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(int channelCode) {
        this.channelCode = channelCode;
    }

    public double getAmountProportion() {
        return amountProportion;
    }

    public void setAmountProportion(double amountProportion) {
        this.amountProportion = amountProportion;
    }

    public double getCountProportion() {
        return countProportion;
    }

    public void setCountProportion(double countProportion) {
        this.countProportion = countProportion;
    }

    public Long transactionAmount; //  通道总交易金额
    public int transactionCount; //  通道总交易笔数
    public int channelCode; //  通道名称
    public double amountProportion; //  金额比值 所占百分比
    public double countProportion; //  笔数比值 所占百分比

    public int colorVal; //当前饼状图的颜色值，不是服务端返回，前端写死16个色值，循环

    public int getColorVal() {
        return colorVal;
    }

    public void setColorVal(int colorVal) {
        this.colorVal = colorVal;
    }


    public float	startAngleAmount;	// Amount开始绘制的角度
    public float	startAngleCount;	// Count开始绘制的角度
    public float	sweepAngleAmount;	// Amount扫过的角度
    public float	sweepAngleCount;	// Count扫过的角度
    public boolean	selected;	// true为选中

}
