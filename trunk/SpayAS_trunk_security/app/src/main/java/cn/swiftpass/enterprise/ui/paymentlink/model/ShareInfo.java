package cn.swiftpass.enterprise.ui.paymentlink.model;

import android.text.TextUtils;

import java.io.Serializable;

/**
 * Created by congwei.li on 2021/9/17.
 *
 * @Description: 附加费用
 */
public class ShareInfo implements Serializable {
    public String orderNo;
    public String custEmail;
    public String emailSubject;
    public String emailContent;
    public String custMobile;

    public boolean isEmailAllFull() {
        return !TextUtils.isEmpty(orderNo) && !TextUtils.isEmpty(custEmail)
                && !TextUtils.isEmpty(emailSubject)
                && !TextUtils.isEmpty(emailContent);
    }

    public boolean isMsgAllFull() {
        return !TextUtils.isEmpty(orderNo) && !TextUtils.isEmpty(custMobile);
    }
}
