package cn.swiftpass.enterprise.bussiness.model;

import java.util.ArrayList;

public class OrderList implements java.io.Serializable {
    //{
    //	"data": [],
    //	"currentPage": 1,
    //	"perPage": 20,
    //	"pageCount": 0,
    //	"totalRows": 10000,
    //	"reqFeqTime": ""
    //}
    public ArrayList<Order> data;
    public int currentPage;
    public int perPage;
    public int pageCount;
    public int totalRows;
    public String reqFeqTime;
}