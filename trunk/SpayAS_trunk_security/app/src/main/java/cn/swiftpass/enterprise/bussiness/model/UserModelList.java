package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by congwei.li on 2022/4/26.
 *
 * @Description:
 */
public class UserModelList implements Serializable {
    //	"currentPage": 0,
    //	"perPage": 10,
    //	"pageCount": 5,
    //	"totalRows": 50,
    //	"reqFeqTime": ""
    public ArrayList<UserModel> data;
    public int currentPage;
    public int perPage;
    public int pageCount;
    public int totalRows;
    public String reqFeqTime;
}
