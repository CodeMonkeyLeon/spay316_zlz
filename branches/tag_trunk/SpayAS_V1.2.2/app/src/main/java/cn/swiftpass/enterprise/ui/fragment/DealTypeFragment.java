/*
 * 文 件 名:  DealTypeFragment.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  yan_shuo
 * 修改时间:  2015-4-27
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ListView;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.adapter.DealTypeAdapter;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DisplayUtil;
import cn.swiftpass.enterprise.utils.PreferenceUtil;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendPosition;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  Administrator
 * @version  [版本号, 2015-4-27]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
@SuppressLint("SetJavaScriptEnabled")
public class DealTypeFragment extends BaseFragment implements OnChartValueSelectedListener
{
    private PieChart mChart;
    
    private ListView deal_type_listview;
    
    private DealTypeAdapter adapter;
    
    private TextView deal_type_money_tv;
    
    private WebView wb;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        //        View v = View.inflate(getActivity(), R.layout.activity_contenttext, null);
        View v = View.inflate(getActivity(), R.layout.deal_type_fragment_layout, null);
        v.setLayoutParams(new LayoutParams(-1, -1));
        return v;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        //        initViews(getView());
        initView(getView());
    }
    
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @SuppressLint("JavascriptInterface")
    private void initViews(View v)
    {
        wb = (WebView)v.findViewById(R.id.webview);
        wb.getSettings().setJavaScriptEnabled(true);
        wb.getSettings().setAllowFileAccessFromFileURLs(false);
        wb.getSettings().setAllowUniversalAccessFromFileURLs(false);
        
        WebSettings settings = wb.getSettings();
        if (AppHelper.getAndroidSDKVersion() == 17)
        {
            settings.setDisplayZoomControls(false);
        }
        settings.setLoadWithOverviewMode(true);
        //settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        //各种分辨率适应
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int mDensity = metrics.densityDpi;
        if (mDensity == 120)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        }
        else if (mDensity == 160)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        }
        else if (mDensity == 240)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        }
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        
        wb.setWebChromeClient(new myWebChromeClien());
        wb.setWebViewClient(new MyWebViewClient());
        //        int title = getIntent().getIntExtra("title", R.string.title_context_text);
        //        titleBar.setTitle(title);
        //        String url = getIntent().getStringExtra("url");
        JSONObject params = new JSONObject();
        try
        {
            params.put("mchId", String.valueOf(MainApplication.merchantId));
            Map<String, String> heards = new HashMap<String, String>();
            
            String language = PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN);
            
            Locale locale = getResources().getConfiguration().locale; //zh-rHK
            String lan = locale.getCountry();
            
            if (!TextUtils.isEmpty(language))
            {
                heards.put("fp-lang", PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN));
            }
            else
            {
                if (lan.equalsIgnoreCase("CN"))
                {
                    heards.put("fp-lang", MainApplication.LANG_CODE_ZH_CN);
                }
                else
                {
                    heards.put("fp-lang", MainApplication.LANG_CODE_ZH_TW);
                }
                
            }
            if (ApiConstant.bankCode.equals("citic_and") || ApiConstant.bankCode.equals("czb_and")
                || ApiConstant.bankCode.equals("czcb_and") || ApiConstant.bankCode.equals("zsy_and"))
            {
                wb.loadUrl(ApiConstant.BASE_URL_PORT + "spay/getTotalByTerminal?&tradeType=1&mchId="
                    + MainApplication.merchantId + "&bankCode=CCB", heards);
                
            }
            else if (ApiConstant.bankCode.equals("spdb_and"))
            {
                wb.loadUrl(ApiConstant.BASE_URL_PORT + "spay/getTotalByTerminal?&tradeType=1&mchId="
                    + MainApplication.merchantId + "&bankCode=spdb", heards);
            }
            else if (ApiConstant.bankCode.equalsIgnoreCase("fjnx_and"))
            {
                //福建银行
                wb.loadUrl(ApiConstant.BASE_URL_PORT + "spay/getTotalByTerminal?tradeType=1&mchId="
                    + MainApplication.merchantId + "&bankCode=rcc", heards);
            }
            else if (ApiConstant.bankCode.equals("ceb_and"))
            { // 浙商银行
                wb.loadUrl(ApiConstant.BASE_URL_PORT + "spay/getTotalByTerminal?&tradeType=1&mchId="
                    + MainApplication.merchantId + "&bankCode=cebbank", heards);
            }
            else
            {
                
                wb.loadUrl(ApiConstant.BASE_URL_PORT + "spay/getTotalByTerminal?&tradeType=1&mchId="
                    + MainApplication.merchantId, heards);
            }
        }
        catch (JSONException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    class myWebChromeClien extends WebChromeClient
    {
        @Override
        public void onProgressChanged(WebView view, int newProgress)
        {
            super.onProgressChanged(view, newProgress);
            
        }
    }
    
    final class MyWebViewClient extends WebViewClient
    {
        public MyWebViewClient()
        {
            super();
        }
        
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            showLoading(true, "加载中...");
            super.onPageStarted(view, url, favicon);
        }
        
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            Log.i("hehui", "shouldOverrideUrlLoading");
            showLoading(true, "加载中...");
            return super.shouldOverrideUrlLoading(view, url);
            
        }
        
        @Override
        public void onPageFinished(WebView view, String url)
        {
            //            dialog.dismiss();
            dismissLoading();
            super.onPageFinished(view, url);
        }
        
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
        {
            super.onReceivedError(view, errorCode, description, failingUrl);
            dismissLoading();
            wb.loadUrl("file:///android_asset/error.html");
            
        }
    }
    
    final class InJavaScriptLocalObj
    {
        public void showSource(String html)
        {
            Log.d("HTML", html);
        }
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @param view
     * @see [类、类#方法、类#成员]
     */
    private void initView(View view)
    {
        mChart = (PieChart)view.findViewById(R.id.chart1);
        deal_type_money_tv = (TextView)view.findViewById(R.id.deal_type_money_tv);
        deal_type_listview = (ListView)view.findViewById(R.id.deal_type_listview);
        
        mChart.setUsePercentValues(true);
        mChart.setDescription("");
        mChart.setDrawHoleEnabled(false);//设置是实心圆饼还是空心圆饼
        mChart.setHoleColorTransparent(true);//设置圆饼中心是否透明
        
        //mChart.setTransparentCircleColor(Color.WHITE);//如果圆饼中心不透明，指定设置圆饼中心的颜色
        mChart.setHoleRadius(75f);//孔半径
        mChart.setTransparentCircleRadius(77f);//设置透明孔区域半径
        mChart.setTransparentCircleColor(Color.parseColor("#e1e1e1"));//设置透明区域的颜色
        
        mChart.setRotationAngle(0);//设置旋转角度
        mChart.setRotationEnabled(true);//设置是否旋转
        
        // mChart.setUnit(" 1");
        // mChart.setDrawUnitsInChart(true);
        
        mChart.setOnChartValueSelectedListener(this);
        mChart.setDrawCenterText(false);//是否显示圆饼中心的文本
        //mChart.setCenterText("123456.00 \n 收款总额");//圆饼中间显示的字
        //mChart.setCenterTextSize(15f);
        //mChart.setCenterTextColor(Color.parseColor("#007f00"));
        deal_type_money_tv.setMaxWidth((int)(DisplayUtil.getScreenWidth(getActivity()) / 2.5));
        
        //这里模拟数据
        List<Float> test = new ArrayList<Float>();
        test.add(0.25f);
        test.add(0.25f);
        test.add(0.25f);
        test.add(0.25f);
        //设置数据
        setData(test);
        mChart.animateY(1500, Easing.EasingOption.EaseInOutQuad);
        
        Legend l = mChart.getLegend();
        l.setPosition(LegendPosition.RIGHT_OF_CHART);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(5f);
        
        test.add(0.22f);
        adapter = new DealTypeAdapter(getActivity(), test);
        deal_type_listview.setAdapter(adapter);
        
    }
    
    @Override
    public void onHiddenChanged(boolean hidden)
    {
        super.onHiddenChanged(hidden);
        //initView(getView());
    }
    
    //动态设置数据
    private void setData(List<Float> datas)
    {
        if (datas == null)
        {
            return;
        }
        ArrayList<Entry> yVals1 = new ArrayList<Entry>();
        ArrayList<String> xVals = new ArrayList<String>();
        for (int a = 0; a < datas.size(); a++)
        {
            yVals1.add(new Entry(datas.get(a), a));
            xVals.add("");
        }
        
        PieDataSet dataSet = new PieDataSet(yVals1, "");
        // dataSet.setSliceSpace(3f);//圆饼每个item的问距
        dataSet.setSelectionShift(5f);//点击圆饼item时 每个item的位移
        // add a lot of colors
        
        int a = Color.parseColor("#45c01a");
        int b = Color.parseColor("#f2c81b");
        int c = Color.parseColor("#ff7f00");
        int d = Color.parseColor("#2d9cb5");
        
        ArrayList<Integer> colors = new ArrayList<Integer>();
        
        colors.add(a);
        colors.add(b);
        colors.add(c);
        colors.add(d);
        colors.add(ColorTemplate.getHoloBlue());
        
        dataSet.setColors(colors);
        
        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(0f);//显示在圆饼每个item上的字体大小，如果不想显示就设置字体为0
        data.setValueTextColor(Color.parseColor("#00ffffff"));//显示在圆饼每个item上的字体颜色
        //        data.setValueTypeface(tf);//显示在圆饼每个item上字的字体
        mChart.setDrawSliceText(false);//回饼的item上只显示百分比
        mChart.setData(data);
        mChart.setRotationEnabled(true);//是否可以旋转
        
        // undo all highlights
        mChart.highlightValues(null);
        mChart.setBackgroundColor(Color.parseColor("#f0f0f0"));
        mChart.invalidate();
    }
    
    /** {@inheritDoc} */
    
    @Override
    public void onNothingSelected()
    {
        
    }
    
    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h)
    {
        // TODO Auto-generated method stub
        
    }
    
}
