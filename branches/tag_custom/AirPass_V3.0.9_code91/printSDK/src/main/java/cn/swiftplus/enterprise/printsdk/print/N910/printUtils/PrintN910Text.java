package cn.swiftplus.enterprise.printsdk.print.N910.printUtils;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.landicorp.android.eptapi.DeviceService;
import com.newland.me.ConnUtils;
import com.newland.me.DeviceManager;
import com.newland.mtype.ConnectionCloseEvent;
import com.newland.mtype.Device;
import com.newland.mtype.ModuleType;
import com.newland.mtype.conn.DeviceConnParams;
import com.newland.mtype.event.DeviceEventListener;
import com.newland.mtype.module.common.printer.PrintContext;
import com.newland.mtype.module.common.printer.Printer;
import com.newland.mtypex.nseries.NSConnV100ConnParams;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeUnit;

import cn.swiftplus.enterprise.printsdk.print.PrintClient;


/**
 * Created by aijingya on 2020/11/10.
 *
 * @Package cn.swiftplus.enterprise.printsdk.print.N910.printUtils
 * @Description:
 * @date 2020/11/10.15:07.
 */
public class PrintN910Text {
    private static PrintN910Text mPrintN910Text;
    private DeviceControllerInterface controller;
    private Printer printer;
    private static Context mContext;
    private StringBuffer scriptBuffer;
    private StringBuffer scriptBuffer_end;
    private String orderNoMch;
    private boolean startLastText = false;

    private PrintN910Text(){
        initMe3xDevice(mContext,"com.newland.me.K21Driver", new NSConnV100ConnParams());
    }

    public static void initContext(Context context){
        mContext = context;
    }

    public static PrintN910Text getInstance(){
        if(mPrintN910Text == null){
            mPrintN910Text = new PrintN910Text();
        }
        return mPrintN910Text;
    }

    public void init(){
        bindDeviceService();

        printer = (Printer) getDeviceManager().getDevice().getStandardModule(ModuleType.COMMON_PRINTER);
        printer.init();
        printer.setLineSpace(2);
    }

    private static DeviceManager deviceManager = ConnUtils.getDeviceManager();

    public static DeviceManager getDeviceManager() {
        return deviceManager;
    }


    public void printTitle(String text, PrintClient.RECEIPT_TYPE receipt_type){
        scriptBuffer = new StringBuffer();
        scriptBuffer_end = new StringBuffer();

        scriptBuffer.append(" " + "\n");
        scriptBuffer_end.append(" " + "\n");

        //因为没有居中方法，只能手动调节空格，让标题居中
        switch (receipt_type){
            case summary_receipt:
                scriptBuffer.append("\n           " + text + "\n");
                break;
            case order_receipt:
                scriptBuffer.append("\n            " + text + "\n");
                break;
            case refund_receipt:
                scriptBuffer.append("\n         " + text + "\n");
                break;
            case pre_auth_receipt:
                scriptBuffer.append("\n      " + text + "\n");
                break;
            case unfreeze_receipt:
                scriptBuffer.append("\n   " + text + "\n");
                break;
        }

        scriptBuffer.append(" " + "\n");
    }

    public void printTextLine(String text){
        if(startLastText){
            scriptBuffer_end.append(text+ "\n");
        }else{
            scriptBuffer.append(text+ "\n");
        }

    }

    public void printMultiLines(String text){
        if(startLastText){
            scriptBuffer_end.append(text+ "\n");
        }else{
            scriptBuffer.append(text+ "\n");
        }
    }

    public void printEmptyLine(){
        if(startLastText){
            scriptBuffer_end.append(" " + "\n");
        }else{
            scriptBuffer.append(" " + "\n");
        }
    }

    public void printDoubleLine(){
        if(startLastText){
            scriptBuffer_end.append("\n===============================\n");
        }else{
            scriptBuffer.append("\n===============================\n");
        }
    }

    public void printSingleLine(){
        if(startLastText){
            scriptBuffer_end.append("-------------------------------\n");
        }else{
            scriptBuffer.append("-------------------------------\n");
        }
    }

    public void printQRCode(String sOrderNoMch){
        this.orderNoMch = sOrderNoMch;
        this.startLastText = true;
    }

    /**
     *  打印图片
     */
    public void printBitmap(Bitmap logoBitmap){
        if(logoBitmap != null){

        }
    }

    public void startPrint(PrintClient.RECEIPT_TYPE receipt_type){
        switch (receipt_type){
            case summary_receipt:
                //日结小票只有一整段文案
                printer.print(scriptBuffer.toString(), 30, TimeUnit.SECONDS);
                pintLine();

                scriptBuffer = null;
                scriptBuffer_end = null;
                startLastText = false;

                break;

            case order_receipt:
                //先打印上半部分的文案
                scriptBuffer.append(" " + "\n");
                printer.print(scriptBuffer.toString(), 30, TimeUnit.SECONDS);
                //再打印QR code部分
                if(!TextUtils.isEmpty(orderNoMch)){
                    //打印小票的代码
                    try {
                        printer.printByScript(PrintContext.defaultContext(), ("!qrcode 150 2\n *qrcode c " + orderNoMch + "\n").getBytes("GBK"), 60, TimeUnit.SECONDS);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                //最后打印下半部分的文案
                printer.print(scriptBuffer_end.toString(), 30, TimeUnit.SECONDS);
                pintLine();

                scriptBuffer = null;
                scriptBuffer_end = null;
                startLastText = false;

                break;

            case refund_receipt:
                printer.print(scriptBuffer.toString(), 30, TimeUnit.SECONDS);
                pintLine();

                scriptBuffer = null;
                scriptBuffer_end = null;
                startLastText = false;

                break;

            case pre_auth_receipt:
                //先打印上半部分的文案
                scriptBuffer.append(" " + "\n");
                printer.print(scriptBuffer.toString(), 30, TimeUnit.SECONDS);
                //再打印QR code部分
                if(!TextUtils.isEmpty(orderNoMch)){
                    //打印小票的代码
                    try {
                        printer.printByScript(PrintContext.defaultContext(), ("!qrcode 150 2\n *qrcode c " + orderNoMch + "\n").getBytes("GBK"), 60, TimeUnit.SECONDS);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                //最后打印下半部分的文案
                printer.print(scriptBuffer_end.toString(), 30, TimeUnit.SECONDS);
                pintLine();

                scriptBuffer = null;
                scriptBuffer_end = null;
                startLastText = false;

                break;

            case unfreeze_receipt:
                //预授权解冻只有一整段文案
                printer.print(scriptBuffer.toString(), 30, TimeUnit.SECONDS);
                pintLine();

                scriptBuffer = null;
                scriptBuffer_end = null;
                startLastText = false;

                break;
        }


    }

    public DeviceControllerInterface initMe3xDevice(Context context,String driverPath, DeviceConnParams params) {
        controller = DeviceControllerInterfaceImpl.getInstance(driverPath);
        controller.init(context, driverPath, params, new DeviceEventListener<ConnectionCloseEvent>() {
            @Override
            public void onEvent(ConnectionCloseEvent event, Handler handler) {
                if (event.isSuccess()) {
                    Log.i("PRINT_SDK", "设备被客户主动断开");
                }
                if (event.isFailed()) {
                    Log.i("PRINT_SDK", "设备链接异常断开");
                }
            }

            @Override
            public Handler getUIHandler() {
                return null;
            }
        });
        return controller;
    }

    public void connectDevice() {
        try {
            controller.connect();
            Log.i("PRINT_SDK", "K21设备成功");
        } catch (Exception e1) {
            Log.i("PRINT_SDK", "K21连接异常");
            e1.printStackTrace();
        }
    }

    public void bindDeviceService() {
        initMe3xDevice(mContext,"com.newland.me.K21Driver", new NSConnV100ConnParams());
        connectDevice();
    }

    public void unbindDeviceService() {

    }


    public void pintLine() {
        String lin = "\n\n\n\n";
        lin += "                 ";
        printer.print(lin, 30, TimeUnit.SECONDS);
    }

}
