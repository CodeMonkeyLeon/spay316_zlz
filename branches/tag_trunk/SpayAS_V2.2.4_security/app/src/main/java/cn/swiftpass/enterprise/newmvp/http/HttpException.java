package cn.swiftpass.enterprise.newmvp.http;

import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonSyntaxException;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;

import java.io.NotSerializableException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.ParseException;


public class HttpException extends Exception {

    //对应HTTP的状态码
    private static final String UNAUTHORIZED = "401";
    private static final String FORBIDDEN = "403";
    private static final String NOT_FOUND = "404";
    private static final String REQUEST_TIMEOUT = "408";
    private static final String INTERNAL_SERVER_ERROR = "500";
    private static final String BAD_GATEWAY = "502";
    private static final String SERVICE_UNAVAILABLE = "503";
    private static final String GATEWAY_TIMEOUT = "504";

    private final String code;
    private String message;

    public HttpException(Throwable throwable, String code) {
        super(throwable);
        this.code = code;
        this.message = throwable.getMessage();
    }

    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public static HttpException handleException(Throwable e) {

        Throwable throwable = e;
        //获取最根源的异常
        while (throwable.getCause() != null) {
            e = throwable;
            throwable = throwable.getCause();
        }

        HttpException ex;
        //HTTP错误
        if (e instanceof retrofit2.HttpException) {
            retrofit2.HttpException httpException = (retrofit2.HttpException) e;
            ex = new HttpException(e, String.valueOf(httpException.code()));
            switch (String.valueOf(httpException.code())) {
                case UNAUTHORIZED:
                case FORBIDDEN:
                    //权限错误，需要实现重新登录操作
//                    onPermissionError(ex);
                    break;
                case NOT_FOUND:
                case REQUEST_TIMEOUT:
                case GATEWAY_TIMEOUT:
                case INTERNAL_SERVER_ERROR:
                case BAD_GATEWAY:
                case SERVICE_UNAVAILABLE:
                default:
                    //均视为网络错误
                    ex.message = "默认网络异常";
                    break;
            }
            return ex;
        } else if (e instanceof SocketTimeoutException) {
            ex = new HttpException(e, ERROR.TIMEOUT_ERROR);
            ex.message = "网络连接超时，请检查您的网络状态，稍后重试！";
            return ex;
        } else if (e instanceof ConnectException) {
            ex = new HttpException(e, ERROR.TIMEOUT_ERROR);
            ex.message = "网络连接异常，请检查您的网络状态，稍后重试！";
            return ex;
        } else if (e instanceof ConnectTimeoutException) {
            ex = new HttpException(e, ERROR.TIMEOUT_ERROR);
            ex.message = "网络连接超时，请检查您的网络状态，稍后重试！";
            return ex;
        } else if (e instanceof UnknownHostException) {
            ex = new HttpException(e, ERROR.TIMEOUT_ERROR);
            ex.message = "网络连接异常，请检查您的网络状态，稍后重试！";
            return ex;
        } else if (e instanceof NullPointerException) {
            ex = new HttpException(e, ERROR.NULL_POINTER_EXCEPTION);
            ex.message = "空指针异常";
            return ex;
        } else if (e instanceof javax.net.ssl.SSLHandshakeException) {
            ex = new HttpException(e, ERROR.SSL_ERROR);
            ex.message = "证书验证失败";
            return ex;
        } else if (e instanceof ClassCastException) {
            ex = new HttpException(e, ERROR.CAST_ERROR);
            ex.message = "类型转换错误";
            return ex;
        } else if (e instanceof JsonParseException
                || e instanceof JSONException
                //Gson解析异常
                || e instanceof JsonSyntaxException
                || e instanceof JsonSerializer
                || e instanceof NotSerializableException
                || e instanceof ParseException) {
            ex = new HttpException(e, ERROR.PARSE_ERROR);
            ex.message = "解析错误";
            return ex;
        } else if (e instanceof IllegalStateException) {
            ex = new HttpException(e, ERROR.ILLEGAL_STATE_ERROR);
            ex.message = e.getMessage();
            return ex;
        } else {
            ex = new HttpException(e, ERROR.UNKNOWN);
            ex.message = "未知错误";
            return ex;
        }
    }

    /**
     * 约定异常
     */
    public static class ERROR {
        /**
         * 未知错误
         */
        public static final String UNKNOWN = "1000";
        /**
         * 连接超时
         */
        public static final String TIMEOUT_ERROR = "1001";
        /**
         * 空指针错误
         */
        public static final String NULL_POINTER_EXCEPTION = "1002";

        /**
         * 证书出错
         */
        public static final String SSL_ERROR = "1003";

        /**
         * 类转换错误
         */
        public static final String CAST_ERROR = "1004";

        /**
         * 解析错误
         */
        public static final String PARSE_ERROR = "1005";

        /**
         * 非法数据异常
         */
        public static final String ILLEGAL_STATE_ERROR = "1006";

    }

}



