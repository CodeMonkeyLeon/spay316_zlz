package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created by aijingya on 2019/4/24.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2019/4/24.17:09.
 */
public class TransactionTotalBean implements Serializable {

    /*    "  transactionAmountTotal":"tom",
            "transactionCountTotal":"323",
            "refundAmountTotal":"343"
            "refundCountTotal":"343"
            "transactionsRetainedProfits":"343"
            "transactionRelativeRatio":"343"*/

    public Long transactionAmountTotal; //交易总金额
    public int transactionCountTotal; //交易总笔数
    public Long refundAmountTotal; //退款总金额
    public int refundCountTotal; //退款总比数
    public Long transactionsRetainedProfits; //交易净额
    public double transactionRelativeRatio; //环比


    public Long getTransactionAmountTotal() {
        return transactionAmountTotal;
    }

    public void setTransactionAmountTotal(Long transactionAmountTotal) {
        this.transactionAmountTotal = transactionAmountTotal;
    }

    public int getTransactionCountTotal() {
        return transactionCountTotal;
    }

    public void setTransactionCountTotal(int transactionCountTotal) {
        this.transactionCountTotal = transactionCountTotal;
    }

    public Long getRefundAmountTotal() {
        return refundAmountTotal;
    }

    public void setRefundAmountTotal(Long refundAmountTotal) {
        this.refundAmountTotal = refundAmountTotal;
    }

    public int getRefundCountTotal() {
        return refundCountTotal;
    }

    public void setRefundCountTotal(int refundCountTotal) {
        this.refundCountTotal = refundCountTotal;
    }

    public Long getTransactionsRetainedProfits() {
        return transactionsRetainedProfits;
    }

    public void setTransactionsRetainedProfits(Long transactionsRetainedProfits) {
        this.transactionsRetainedProfits = transactionsRetainedProfits;
    }

    public double getTransactionRelativeRatio() {
        return transactionRelativeRatio;
    }

    public void setTransactionRelativeRatio(double transactionRelativeRatio) {
        this.transactionRelativeRatio = transactionRelativeRatio;
    }




}
