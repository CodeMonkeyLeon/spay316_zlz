package cn.swiftpass.enterprise.ui.paymentlink.model;

import java.io.Serializable;
import java.util.ArrayList;

public class PaymentLinkCustomerModel implements Serializable {
    //message":"{\"data\":[],\"currentPage\":1,\"perPage\":20,\"pageCount\":100,\"totalRows\":10000,\"reqFeqTime\":\"\"}
    public String currentPage;
    public String perPage;
    public String pageCount;
    public String totalRows;
    public String reqFeqTime;
    public ArrayList<PaymentLinkCustomer> data;
}
