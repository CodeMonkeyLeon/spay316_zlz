package cn.swiftpass.enterprise.utils.json;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.utils.JsonUtil;

/**
 * Created by congwei.li on 2022/5/6.
 *
 * @Description:
 */
public class ArrayListTypeAdapter<T> implements JsonDeserializer<List<T>>,
        JsonSerializer<List<T>> {
    public List<T> deserialize(JsonElement json, Type typeOfT,
                               JsonDeserializationContext context)
            throws JsonParseException {
        List<T> runtime;
        try {
            runtime = (List<T>) JsonUtil.jsonToArrayList(json,
                    new TypeToken<List<T>>() {
                    }.getType());
        } catch (Exception e) {
            runtime = new ArrayList<>();
        }
        return runtime;
    }

    public JsonElement serialize(List<T> src, Type typeOfSrc,
                                 JsonSerializationContext context) {
        return new JsonPrimitive(JsonUtil.objectToJson(src));
    }

}
