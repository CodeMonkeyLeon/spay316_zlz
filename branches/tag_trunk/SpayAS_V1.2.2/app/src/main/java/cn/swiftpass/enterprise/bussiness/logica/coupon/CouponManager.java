package cn.swiftpass.enterprise.bussiness.logica.coupon;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.enums.CacheEnum;
import cn.swiftpass.enterprise.bussiness.enums.CouponStatusEnum;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.CacheModel;
import cn.swiftpass.enterprise.bussiness.model.CouponModel;
import cn.swiftpass.enterprise.bussiness.model.CouponOrderInfo;
import cn.swiftpass.enterprise.bussiness.model.CouponUseModel;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.bussiness.model.SlideViewModel;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.NetHelper;
import cn.swiftpass.enterprise.utils.ImageUtil;
import cn.swiftpass.enterprise.utils.Utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 优惠券管理
 * User: Alan
 * Date: 13-12-24
 * Time: 下午5:41
 */
public class CouponManager
{
    
    public CouponManager()
    {
        // couponModels = new ArrayList<CouponModel>();
    }
    
    public static CouponManager getInstance()
    {
        
        return Container.instance;
    }
    
    private static class Container
    {
        public static CouponManager instance = new CouponManager();
    }
    
    //public  List<CouponModel> couponModels;
    
    /**发布优惠券*/
    public void CouponCURD(final CouponModel couponModel, final boolean isAdd, final String picPath,
        final UINotifyListener<Boolean> listener)
    {
        ThreadHelper.executeWithCallback(new Executable()
        {
            @Override
            public Boolean execute()
                throws Exception
            {
                
                UserModel userModel = LocalAccountManager.getInstance().getLoggedUser();
                JSONObject json = new JSONObject();
                json.put("merId", userModel.merId + "");
                json.put("userId", userModel.uId + "");
                json.put("shopId", couponModel.shopId + "");
                json.put("couponName", couponModel.couponName);
                json.put("couponCount", couponModel.couponCount + "");
                json.put("startDate", couponModel.strStartDate);
                json.put("strEndDate", couponModel.strEndDate);
                json.put("couponType", couponModel.couponType + "");
                json.put("couponDetail", couponModel.couponDetail);
                json.put("couponPic", couponModel.couponPic);
                json.put("color", couponModel.color + "");
                json.put("discount", couponModel.discount + ""); //分为单位
                json.put("otherShop", couponModel.otherShop);
                json.put("issueX", couponModel.issueX + "");
                json.put("issueY", couponModel.issueY + "");
                json.put("useDays", couponModel.useDays);
                if (couponModel.useDaysDefined != null)
                    json.put("useDaysDefined", couponModel.useDaysDefined);
                json.put("useHours", couponModel.useHours);
                if (couponModel.useHoursDefined != null)
                    json.put("useHoursDefined", couponModel.useHoursDefined);
                json.put("otherLimt", couponModel.otherLimt);
                if (couponModel.otherLimtDefined != null)
                    json.put("otherLimtDefined", couponModel.otherLimtDefined);
                json.put("issueReceive", couponModel.issueReceive + "");
                json.put("minAmount", couponModel.minAmount + "");
                json.put("validDays", couponModel.validDays + "");
                json.put("endDate", couponModel.strEndDate + ""); //W.l 添加endDate，对照db里面的字段
                json.put("oncePiece", couponModel.oncePiece + "");
                String url;
                if (isAdd)
                {
                    url = ApiConstant.BASE_URL_PORT + ApiConstant.COUPONS_ADD;
                    if (LocalAccountManager.getInstance().isManager())
                    { //管理员修改通过
                        if (couponModel.couponStatus == CouponStatusEnum.WAIT.getValue())
                        {
                            json.put("status", CouponStatusEnum.ISSUE.getValue() + "");
                        }
                    }
                }
                else
                {
                    url = ApiConstant.BASE_URL_PORT + ApiConstant.COUPON_EDIT;
                    json.put("couponId", couponModel.couponId + "");// W.l 优惠券id 2014-02-19 20:55
                    if (LocalAccountManager.getInstance().isManager())
                    { //管理员修改通过
                        if (couponModel.couponStatus == CouponStatusEnum.WAIT.getValue())
                        {
                            json.put("status", CouponStatusEnum.ISSUE.getValue() + "");
                        }
                        else if (couponModel.couponStatus == CouponStatusEnum.NOPASS.getValue())
                        {
                            json.put("status", CouponStatusEnum.NOPASS.getValue() + "");
                        }
                        
                    }
                }
                RequestResult result = NetHelper.httpsPost(url, json, couponModel.couponPic, picPath);
                if (!result.hasError())
                {
                    
                    int res = Utils.Integer.tryParse(result.data.getString("result"), -1);
                    switch (res)
                    {
                        case 0:
                            //CouponDB.getInstance().save(couponModel);
                            //couponModels.clear();
                            if (isAdd)
                            {
                                return true;
                            }
                            else if (couponModel.couponStatus == CouponStatusEnum.WAIT.getValue())
                            {
                                listener.onError("恭喜您，发布成功！");
                            }
                            else if (couponModel.couponStatus == CouponStatusEnum.NOPASS.getValue())
                            {
                                listener.onError("恭喜您，驳回成功！");
                            }
                            else
                            {
                                listener.onError("恭喜您，修改优惠券成功！");
                            }
                            return true;
                        case -1:
                            listener.onError("发布失败了！");
                            return false;
                        case 1:
                            listener.onError("参数错误！");
                            return false;
                        case 4:
                            listener.onError("发布失败，请稍候再试！");
                            return false;
                    }
                }
                else
                {
                    switch (result.resultCode)
                    {
                        case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                            listener.onError("网络连接不可用，请检查你网络连接!");
                            return null;
                        case RequestResult.RESULT_TIMEOUT_ERROR:
                            listener.onError("请求连接超时，请稍候再试!");
                            return null;
                        case RequestResult.RESULT_READING_ERROR:
                            listener.onError("请求服务连接超时，请稍候再试!");
                            return null;
                        case RequestResult.RESULT_NEED_LOGIN:
                            String message = result.data.getString("message");
                            listener.onError(message);
                            return null;
                    }
                }
                
                return false;
            }
        }, listener);
    }
    
    /**删除优惠券*/
    public void deleteCoupon(final int couponId, final UINotifyListener<Boolean> listener)
    {
        ThreadHelper.executeWithCallback(new Executable()
        {
            @Override
            public Boolean execute()
                throws Exception
            {
                UserModel userModel = LocalAccountManager.getInstance().getLoggedUser();
                JSONObject json = new JSONObject();
                json.put("merId", userModel.merId + "");
                json.put("userId", userModel.uId + "");
                json.put("couponId", couponId + "");
                RequestResult result =
                    NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.COUPONS_DELETE, json);
                if (!result.hasError())
                {
                    
                    int res = Utils.Integer.tryParse(result.data.getString("result"), -1);
                    switch (res)
                    {
                        case 0:
                            listener.onError("优惠券删除成功!");
                            //couponModels.clear();
                            return true;
                        case -1:
                            listener.onError("对不起,请稍候在试，服务系统繁忙中!");
                            return false;
                        case 1:
                            listener.onError("发生参数错误!");
                            return false;
                        case 2:
                            listener.onError("对不起,您无法审核此优惠卷!");
                            return false;
                        case 3:
                            listener.onError("对不起,您商户信息错误!");
                            return false;
                        case 4:
                            listener.onError("对不起，您操作失败!");
                            return false;
                        case 5:
                            listener.onError("优惠券已经发布不能进行删除!");
                            return false;
                    }
                }
                else
                {
                    switch (result.resultCode)
                    {
                        case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                            listener.onError("网络连接不可用，请检查你网络连接!");
                            return null;
                        case RequestResult.RESULT_TIMEOUT_ERROR:
                            listener.onError("请求连接超时，请稍候再试!");
                            return null;
                        case RequestResult.RESULT_READING_ERROR:
                            listener.onError("请求服务连接超时，请稍候再试!");
                            return null;
                        case RequestResult.RESULT_NEED_LOGIN:
                            String message = result.data.getString("message");
                            listener.onError(message);
                            return null;
                    }
                }
                
                return false;
            }
        },
            listener);
    }
    
    /**审核优惠券*/
    public void verifyCoupon(final int couponId, final CouponStatusEnum couponStatusEnum,
        final UINotifyListener<Boolean> listener)
    {
        ThreadHelper.executeWithCallback(new Executable()
        {
            @Override
            public Boolean execute()
                throws Exception
            {
                UserModel userModel = LocalAccountManager.getInstance().getLoggedUser();
                JSONObject json = new JSONObject();
                json.put("merId", userModel.merId + "");
                json.put("userId", userModel.uId + "");
                json.put("couponId", couponId + "");
                json.put("status", couponStatusEnum.getValue() + "");
                RequestResult result =
                    NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.COUPONS_VERIFY, json);
                if (!result.hasError())
                {
                    switch (result.resultCode)
                    {
                        case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                            listener.onError("网络连接不可用，请检查你网络连接!");
                            return false;
                        case RequestResult.RESULT_TIMEOUT_ERROR:
                            listener.onError("请求连接超时，请稍候再试!");
                            return false;
                        case RequestResult.RESULT_READING_ERROR:
                            listener.onError("请求服务连接超时，请稍候再试!");
                            return false;
                    }
                    int res = Utils.Integer.tryParse(result.data.getString("result"), -1);
                    switch (res)
                    {
                        case 0:
                            listener.onError("操作成功！");
                            // couponModels.clear();
                            return true;
                        case -1:
                            listener.onError("发布失败了！");
                            return false;
                        case 1:
                            listener.onError("发生参数错误啦。");
                            return false;
                        case 2:
                            listener.onError("对不起,您无法审核此优惠卷。");
                            return false;
                        case 3:
                            listener.onError("对不起,您商户信息错误啦。");
                            return false;
                        case 4:
                            listener.onError("对不起您,审核失败,请联系管理员。");
                            return false;
                        case 5:
                            listener.onError("优惠券已经发布不能进行审核。");
                            return false;
                    }
                }
                
                return false;
            }
        },
            listener);
    }
    
    /**查询店铺*/
    public void searchList(final int currentPage, final Integer shopId, final Integer couponId,
        final UINotifyListener<List<SlideViewModel>> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<List<SlideViewModel>>()
        {
            @SuppressWarnings("unchecked")
            @Override
            public List<SlideViewModel> execute()
                throws Exception
            {
                UserModel userModel = LocalAccountManager.getInstance().getLoggedUser();
                CacheModel cacheModel = new CacheModel();
                cacheModel.addTime = System.currentTimeMillis();
                cacheModel.type = CacheEnum.SHOP_TYPE.getValue();
                cacheModel.uId = LocalAccountManager.getInstance().getUID();
                
                JSONObject json = new JSONObject();
                json.put("userId", userModel.uId + "");
                json.put("shopId", shopId + "");
                json.put("merId", userModel.merId + "");
                json.put("pageSize", ApiConstant.PAGE_SIZE + "");
                /* if(currentPage>0){
                     json.put("currentPage", currentPage+"");
                 }else{
                     json.put("currentPage", null);
                 }*/
                json.put("currentPage", currentPage + "");
                if (couponId != null)
                {
                    json.put("couponId", couponId + "");
                }
                RequestResult result =
                    NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.COUPON_SEARCH, json, null, null);
                if (!result.hasError())
                {
                    switch (result.resultCode)
                    {
                        case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                            listener.onError("网络连接不可用，请检查你网络连接!");
                            return null;
                        case RequestResult.RESULT_TIMEOUT_ERROR:
                            listener.onError("请求连接超时，请稍候再试!");
                            return null;
                        case RequestResult.RESULT_READING_ERROR:
                            listener.onError("请求服务连接超时，请稍候再试!");
                            return null;
                    }
                    int res = Utils.Integer.tryParse(result.data.getString("result"), -1);
                    switch (res)
                    {
                        case 0:
                            String strList = result.data.getString("list");
                            String rowCount = result.data.getString("rowCount");
                            Gson gson = new Gson();
                            List<CouponModel> models = gson.fromJson(strList, new TypeToken<List<CouponModel>>()
                            {
                            }.getType());
                            if (models.size() > 0)
                            {
                                List<SlideViewModel> slideViewModel = new ArrayList<SlideViewModel>(models.size());
                                for (CouponModel c : models)
                                {
                                    SlideViewModel sv = new SlideViewModel();
                                    sv.c = c;
                                    slideViewModel.add(sv);
                                }
                                return slideViewModel;
                            }
                            /*  for (CouponModel c : models)
                              {
                                   CouponDB.getInstance().save(c);
                                 // couponModels.add(c);
                              }*/
                            //Collections.sort(couponModels, Comparators.getComparator());
                            //保存统计数据
                            return null;
                        case -1:
                            listener.onError("保存店铺失败！");
                            break;
                        case 1:
                            listener.onError("参数错误！");
                            break;
                        case 2:
                            listener.onError("用户无效！");
                            break;
                        case 3:
                            listener.onError("商户无效！");
                            break;
                        case 4:
                            listener.onError("操作失败，稍候再试！");
                            break;
                    }
                }
                return null;
            }
        },
            listener);
    }
    
    /**查询有哪些人领用人 ，获取头像，号码，名称*/
    public void searchCoupONUserList(final int currentPage, final Integer couponId,
        final UINotifyListener<List<CouponUseModel>> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<List<CouponUseModel>>()
        {
            @SuppressWarnings("unchecked")
            @Override
            public List<CouponUseModel> execute()
                throws Exception
            {
                JSONObject json = new JSONObject();
                json.put("pageSize", ApiConstant.PAGE_SIZE + "");
                json.put("currentPage", currentPage + "");
                if (couponId != null)
                {
                    json.put("couponId", couponId + "");
                }
                RequestResult result =
                    NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.COUPON_SEARCH_USER, json, null, null);
                if (!result.hasError())
                {
                    switch (result.resultCode)
                    {
                        case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                            listener.onError("网络连接不可用，请检查你网络连接!");
                            return null;
                        case RequestResult.RESULT_TIMEOUT_ERROR:
                            listener.onError("请求连接超时，请稍候再试!");
                            return null;
                        case RequestResult.RESULT_READING_ERROR:
                            listener.onError("请求服务连接超时，请稍候再试!");
                            return null;
                    }
                    int res = Utils.Integer.tryParse(result.data.getString("result"), -1);
                    switch (res)
                    {
                        case 0:
                            String strList = result.data.getString("list");
                            Gson gson = new Gson();
                            List<CouponUseModel> models = gson.fromJson(strList, new TypeToken<List<CouponUseModel>>()
                            {
                            }.getType());
                            if (models.size() > 0)
                            {
                                return models;
                            }
                            return null;
                        case 1:
                            listener.onError("用户不存在！");
                            break;
                        case 2:
                            listener.onError("用户无效！");
                            break;
                    }
                }
                return null;
            }
        },
            listener);
    }
    
    /**扫描后马上请求服务器获取折扣金额*/
    public void scanQRCode(final String useId, final String money, final UINotifyListener<CouponOrderInfo> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<CouponOrderInfo>()
        {
            @Override
            public CouponOrderInfo execute()
                throws Exception
            {
                UserModel userModel = LocalAccountManager.getInstance().getLoggedUser();
                JSONObject json = new JSONObject();
                json.put("userId", userModel.uId + "");
                json.put("useId", useId);
                json.put("money", money);
                
                RequestResult result =
                    NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.COUPON_CHECKCOUPONS, json);
                if (!result.hasError())
                {
                    int res = Utils.Integer.tryParse(result.data.getString("result"), -1);
                    CouponOrderInfo cInfo;
                    switch (res)
                    {
                        case 0:
                            cInfo = new CouponOrderInfo();
                            cInfo.beforeMoney = Utils.Integer.tryParse(money, 0);
                            cInfo.discountMoney = Utils.Integer.tryParse(result.data.getString("discountMoney"), 0);
                            cInfo.afterMoney = Utils.Integer.tryParse(result.data.getString("afterMoney"), 0);
                            cInfo.useId = useId;
                            return cInfo;
                        case -1:
                            listener.onError("保存店铺失败。");
                            break;
                        case 1:
                            listener.onError("参数错误。");
                            break;
                        case 2:
                            listener.onError("用户无效。");
                            break;
                        case 3:
                            listener.onError("此优惠劵不存在");//折扣金额需要大于消费金额！
                            cInfo = new CouponOrderInfo();
                            cInfo.beforeMoney = Utils.Integer.tryParse(money, 0);
                            cInfo.discountMoney = 0;
                            cInfo.useId = useId;
                            cInfo.afterMoney = cInfo.beforeMoney;
                            return cInfo;
                        case 4:
                            listener.onError("操作失败，稍候再试。");
                            break;
                        case 5:
                            listener.onError("该优惠券已经过期。");
                            break;
                        case 6:
                            listener.onError("该优惠券其他人正在领用");
                            break;
                        case 7:
                            listener.onError("该优惠券已经被使用过");
                            break;
                        case 8:
                            listener.onError("消费金额不够最低使用条件");
                            break;
                        case 9:
                            listener.onError("消费金额低于优惠金额");
                            break;
                        case 10:
                            listener.onError("优惠券已被其他人正在使用，请稍候再试。");
                            break;
                    }
                }
                return null;
            }
        },
            listener);
    }
    
    /**
     * 异步读取图片
     * 为了防止大图片内存溢出
    * @param tempFile
     * @param strBitmapPath
     * @param listener
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static void checkBitmapData(final File tempFile, final String strBitmapPath,
        final UINotifyListener<Bitmap> listener)
    {
        ThreadHelper.executeWithCallback(new Executable()
        {
            
            @Override
            public Bitmap execute()
            {
                Bitmap bitmap;
                if (tempFile.exists() && tempFile.length() / 1024 > 100)
                {
                    bitmap = ImageUtil.getimage(strBitmapPath, 500);
                }
                else
                {
                    bitmap = ImageUtil.readBitmapFromStream(strBitmapPath);
                }
                return bitmap;
            }
            
        }, listener);
        
    }
    
    /*   *//**5分钟内容不刷新*/
    /*
    public boolean isUpdate()
    {
    long currentTime = System.currentTimeMillis();
    long  time = (( currentTime / 1000) - (searchTime /1000 )) /60;
    return time >= 5;
    }
    */
    public void savePopupFlag(int i)
    {
        MainApplication.getContext().getApplicationPreferences();
        SharedPreferences sp = MainApplication.getContext().getApplicationPreferences();
        sp.edit().putInt("UID_" + LocalAccountManager.getInstance().getUID(), i).commit();
    }
    
    public int getIsFristPopup()
    {
        MainApplication.getContext().getApplicationPreferences();
        SharedPreferences sp = MainApplication.getContext().getApplicationPreferences();
        return sp.getInt("UID_" + LocalAccountManager.getInstance().getUID(), 0);
    }
    
    //保存sn，核对pos机是不是当前，如果不是需要重写密钥
    public void saveSNnumber(String i)
    {
        MainApplication.getContext().getApplicationPreferences();
        SharedPreferences sp = MainApplication.getContext().getApplicationPreferences();
        sp.edit().putString("UID_SN" + LocalAccountManager.getInstance().getUID(), i).commit();
    }
    
    public String getISNnumber()
    {
        MainApplication.getContext().getApplicationPreferences();
        SharedPreferences sp = MainApplication.getContext().getApplicationPreferences();
        return sp.getString("UID_SN" + LocalAccountManager.getInstance().getUID(), "");
    }
}
