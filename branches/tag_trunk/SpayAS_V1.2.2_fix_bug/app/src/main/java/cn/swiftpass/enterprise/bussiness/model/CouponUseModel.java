package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 优惠券使用表Model 表名：TB_COU_USE
 * @author yinpeng
 *
 */
public class CouponUseModel implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer useId; //id
	private Integer couponId; //优惠券id
	private String collerPhone; //领用人手机号码
	private String collerOpenId; //领用人微信Id(openid是公众号的普通用户的一个唯一的标识，只针对当前的公众号有效)
	private Date collerTime; //领用时间
	private Date shareCollerTime; //分享领用时间
	private Integer status; //UseStatusEnum
	private Integer beforeStatus; //锁定之前的状态
	private Date lockTime;  //锁定时间
	private Integer collerCount; //领用数量
	
	//2014-03-17
	private String head; //头像
	private String nickname; //昵称
	
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public Date getLockTime() {
		return lockTime;
	}
	public void setLockTime(Date lockTime) {
		this.lockTime = lockTime;
	}
	public Integer getBeforeStatus() {
		return beforeStatus;
	}
	public void setBeforeStatus(Integer beforeStatus) {
		this.beforeStatus = beforeStatus;
	}
	public Date getCollerTime() {
		return collerTime;
	}
	public void setCollerTime(Date collerTime) {
		this.collerTime = collerTime;
	}
	public Integer getUseId() {
		return useId;
	}
	public void setUseId(Integer useId) {
		this.useId = useId;
	}
	public Integer getCouponId() {
		return couponId;
	}
	public void setCouponId(Integer couponId) {
		this.couponId = couponId;
	}
	public String getCollerPhone() {
		return collerPhone;
	}
	public void setCollerPhone(String collerPhone) {
		this.collerPhone = collerPhone;
	}
	public String getCollerOpenId() {
		return collerOpenId;
	}
	public void setCollerOpenId(String collerOpenId) {
		this.collerOpenId = collerOpenId;
	}
	public Date getShareCollerTime() {
		return shareCollerTime;
	}
	public void setShareCollerTime(Date shareCollerTime) {
		this.shareCollerTime = shareCollerTime;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getCollerCount() {
		return collerCount;
	}
	public void setCollerCount(Integer collerCount) {
		this.collerCount = collerCount;
	}

}
