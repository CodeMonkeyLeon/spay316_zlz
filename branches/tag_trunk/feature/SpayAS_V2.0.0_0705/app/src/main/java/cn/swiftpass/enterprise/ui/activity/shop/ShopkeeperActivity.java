/*
 * 文 件 名:  ShopkeeperActivity.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.shop;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.tsz.afinal.FinalBitmap;


import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.MerchantTempDataModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.HandlerManager;


/**
 * 店主资料完善 <功能详细描述>
 * 
 * @author he_hui
 * @version [版本号, 2013-3-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class ShopkeeperActivity extends TemplateActivity
{
    private static final String TAG = ShopkeeperActivity.class.getSimpleName();
    //private List<City> shopBaseTwo = null;

    // 商品名称
    private EditText shop_name;
    
    // 商家地址
    private EditText shop_address;

    // 收款人姓名
    private EditText bayee_name;

    // 固话
    private EditText tel;

    private SharedPreferences sharedPreferences;
    
    private RelativeLayout bunisLayout;

    
    private TextView bunisOne;

    private MerchantTempDataModel dataModel;

    
    private ImageView iv_shop_name, iv_shop_address, iv_tel,  iv_bayee_name;
    

    private FinalBitmap finalBitmap;
    
    private TextView et_tel, tv_mch;

    private LinearLayout ll_rate_details;
    private TextView tv_channel_rate;

    
    // 这个是判断是否激活过。。。。
    Handler handler = new Handler()
    {
        public void handleMessage(android.os.Message msg)
        {
            switch (msg.what)
            {
            // 选择省，自动连动式级数据
                case 0:

                    break;
                // 银行 选择省，自动连动式级数据
                case 1:

                    break;
                case 2: // 商户资料基本信息
                    String parent_id = (String)msg.obj;
                    bunisOne.setText(parent_id);
                    sharedPreferences.edit().putString("bunisOne", parent_id).commit();
                    break;
                
                case 3: // 银行类型

                    break;
                case 4:

                    break;
                case 5:
                    initPromt();
                    break;
                case 6: // 行业
                    String merchantTypeSonId = (String)msg.obj; // 获取子类型
                    sharedPreferences.edit().putString("bunisOne", merchantTypeSonId).commit();
                    break;
                default:
                    break;
            }
        };
    };
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.personal_data);

        sharedPreferences = this.getSharedPreferences("dataOnes", 0);
        
        finalBitmap = FinalBitmap.create(ShopkeeperActivity.this);//
        try {
            finalBitmap.configDiskCachePath(AppHelper.getImgCacheDir());
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        
        initView();
        
        initValue();
        
        HandlerManager.registerHandler(HandlerManager.SHOPKEEPID, handler);
        
        Bundle bundle = this.getIntent().getBundleExtra("bundle");
        
        // 从商户信息 跳转到资料完善
        if (bundle != null)
        {
            dataModel = (MerchantTempDataModel)bundle.get("merchantData");
            if (dataModel != null)
            {
                // 进行激活验证
                loadData(dataModel);
                
                //
                setViewEnable();
            }
            
        }
        handler.sendEmptyMessage(5);
    }
    
    /**
     * <一句话功能简述> <功能详细描述>
     * 
     * @see [类、类#方法、类#成员]
     */
    private void setViewEnable()
    {
        
        switch (Integer.parseInt(MainApplication.remark))
        {
            case 1:
                setEnbaled(false);
                break;
            case -1: // 测试账号
                if (dataModel != null)
                {
                    setEnbaled(false);
                }
                break;
            case 0: // 审核通过
                setEnbaled(false);
                break;
            case -3:// 审核未通过
                break;
            case -2:// 审核中

                setEnbaled(false);
                break;
            default:
                setEnbaled(false);
                break;
        }
    }
    
    private void setEnbaled(boolean b)
    {
        shop_name.setEnabled(b);
        shop_address.setEnabled(b);
        bayee_name.setEnabled(b);
        tel.setEnabled(b);
        bunisOne.setEnabled(b);
        bunisLayout.setEnabled(b);

    }

    
    /**
     * 加载 填充数据 <功能详细描述>
     * 
     * @see [类、类#方法、类#成员]
     */
    private void loadData(MerchantTempDataModel dataModel)
    {
        shop_name.setText(dataModel.getMerchantName());
        
        shop_address.setText(dataModel.getAddress());

        et_tel.setText(dataModel.getPrincipalPhone());
        bayee_name.setText(dataModel.getPrincipal());
        //String identityNo = dataModel.getIdentityNo();

        if (dataModel.getEmil() != null)
        {
            tel.setText(dataModel.getEmil());
        }
        bunisOne.setText(dataModel.getMerchantTypeName());

        //String url = ApiConstant.BASE_URL_PORT + ApiConstant.DONLOAD_IMAGE;

        initChannelRateList(dataModel);
    }

    public void initChannelRateList(MerchantTempDataModel dataModel){
        if(dataModel.getCenterRate() != null && dataModel.getCenterRate().size() > 0){
            ll_rate_details.removeAllViews();
            ll_rate_details.setVisibility(View.VISIBLE);
            tv_channel_rate.setVisibility(View.VISIBLE);
            for(int i = 0 ; i < dataModel.getCenterRate().size() ; i++){
                LinearLayout channel_item_view = (LinearLayout) this.getLayoutInflater().inflate(R.layout.item_channel_rate, null);
                TextView tv_channel_title = (TextView)channel_item_view.findViewById(R.id.tv_channel_rate_title);
                TextView tv_channel_content = (TextView)channel_item_view.findViewById(R.id.tv_channel_rate_content);
                tv_channel_title.setText(dataModel.getCenterRate().get(i).getCenterName());
                ll_rate_details.addView(channel_item_view);
            }
        }else{
            ll_rate_details.removeAllViews();
            ll_rate_details.setVisibility(View.GONE);
            tv_channel_rate.setVisibility(View.GONE);
        }

    }




    /**
     * 记录输入的值 <功能详细描述>
     * 
     * @see [类、类#方法、类#成员]
     */
    private void initValue()
    {
        shop_name.setText(sharedPreferences.getString("shop_name", ""));
        shop_address.setText(sharedPreferences.getString("shop_address", ""));

        bayee_name.setText(sharedPreferences.getString("bayee_name", ""));
        
        bunisOne.setText(sharedPreferences.getString("bunisOne", ""));
        
    }

    
    // 点击清空内容
    private OnClickListener clearImage = new OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            switch (v.getId())
            {
                case R.id.iv_shop_name:
                    shop_name.setText("");
                    break;
                case R.id.iv_shop_address:
                    shop_address.setText("");
                    break;
                case R.id.iv_tel:
                    tel.setText("");
                    break;
                case R.id.iv_bayee_name:
                    bayee_name.setText("");
                    break;

            }
        }
    };

    
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        // saveData(); //保存数据
    }
    
    /**
     * 页面参数初始化 <功能详细描述>
     * 
     * @see [类、类#方法、类#成员]
     */
    private void initView()
    {

        et_tel = getViewById(R.id.tv_tel);
        tv_mch = getViewById(R.id.tv_mch);
        tv_mch.setText(MainApplication.getMchId());

        shop_name = (EditText)findViewById(R.id.shop_name);
        
        shop_address = (EditText)findViewById(R.id.shop_address);

        
        bayee_name = (EditText)findViewById(R.id.bayee_name);

        
        tel = (EditText)findViewById(R.id.tel);
        
        bunisOne = getViewById(R.id.bunisOne);
        
        bunisLayout = getViewById(R.id.bunisLayout);
        
        iv_shop_name = getViewById(R.id.iv_shop_name);
        iv_shop_address = getViewById(R.id.iv_shop_address);
        iv_tel = getViewById(R.id.iv_tel);

        iv_bayee_name = getViewById(R.id.iv_bayee_name);

        ll_rate_details = getViewById(R.id.ll_rate_details);
        tv_channel_rate = getViewById(R.id.tv_channel_rate);

        iv_shop_name.setOnClickListener(clearImage);
        iv_shop_address.setOnClickListener(clearImage);
        iv_tel.setOnClickListener(clearImage);

        iv_bayee_name.setOnClickListener(clearImage);

        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        {
            
            @Override
            public void onExecute(CharSequence s, int start, int before, int count)
            {
            }
            
            @Override
            public void onAfterTextChanged(Editable s)
            {
                if (shop_name.isFocused())
                {
                    if (shop_name.getText().toString().length() > 0)
                    {
                        iv_shop_name.setVisibility(View.VISIBLE);
                        // iv_shopname_promt.setVisibility(View.GONE);
                    }
                    else
                    {
                        iv_shop_name.setVisibility(View.GONE);
                        // iv_shopname_promt.setVisibility(View.VISIBLE);
                    }
                }
                if (shop_address.isFocused())
                {
                    if (shop_address.getText().toString().length() > 0)
                    {
                        iv_shop_address.setVisibility(View.VISIBLE);
                        //iv_shopaddress_promt.setVisibility(View.GONE);
                    }
                    else
                    {
                        iv_shop_address.setVisibility(View.GONE);
                        //iv_shopaddress_promt.setVisibility(View.VISIBLE);
                    }
                }
                if (tel.isFocused())
                {
                    if (tel.getText().toString().length() > 0)
                    {
                        iv_tel.setVisibility(View.VISIBLE);
                        //iv_shophone_promt.setVisibility(View.GONE);
                    }
                    else
                    {
                        iv_tel.setVisibility(View.GONE);
                        //iv_shophone_promt.setVisibility(View.VISIBLE);
                    }
                }

                if (bayee_name.isFocused())
                {
                    if (bayee_name.getText().toString().length() > 0)
                    {
                        iv_bayee_name.setVisibility(View.VISIBLE);
                        //iv_bayeeName_promt.setVisibility(View.GONE);
                    }
                    else
                    {
                        iv_bayee_name.setVisibility(View.GONE);
                        //iv_bayeeName_promt.setVisibility(View.VISIBLE);
                    }
                }


                
            }
            
        });
        
        shop_name.addTextChangedListener(editTextWatcher);
        shop_address.addTextChangedListener(editTextWatcher);
        tel.addTextChangedListener(editTextWatcher);

        bayee_name.addTextChangedListener(editTextWatcher);
        
    }
    
    /**
     * <一句话功能简述>初始化必填项图标 <功能详细描述>
     * 
     * @see [类、类#方法、类#成员]
     */
    private void initPromt()
    {
        if ("".equals(shop_name.getText().toString().trim()))
        {
            //iv_shopname_promt.setVisibility(View.VISIBLE);
        }
        else
        {
            //iv_shopname_promt.setVisibility(View.GONE);
        }
        
        if ("".equals(shop_address.getText().toString().trim()))
        {
            
            //iv_shopaddress_promt.setVisibility(View.VISIBLE);
        }
        else
        {
            
            //iv_shopaddress_promt.setVisibility(View.GONE);
        }
        


        
        if ("".equals(bayee_name.getText().toString().trim()))
        {
            
            //iv_bayeeName_promt.setVisibility(View.VISIBLE);
        }
        else
        {
            
            //iv_bayeeName_promt.setVisibility(View.GONE);
        }

        
        if ("".equals(tel.getText().toString().trim()))
        {
            //iv_shophone_promt.setVisibility(View.VISIBLE);//
        }
        
        else
        {
            
            //iv_shophone_promt.setVisibility(View.GONE);
        }

    }


    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_add_shop);
    }
}
