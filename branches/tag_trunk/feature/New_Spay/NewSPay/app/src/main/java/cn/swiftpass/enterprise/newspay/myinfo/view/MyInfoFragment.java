package cn.swiftpass.enterprise.newspay.myinfo.view;


import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import cn.swiftpass.enterprise.R;
import cn.swiftpass.enterprise.base.BaseFragment;
import cn.swiftpass.enterprise.widget.Toolbar;


public class MyInfoFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tv_aboutus)
    TextView tv_aboutus;

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_my_info;
    }

    @Override
    protected void initView() {
        mToolbar.setCenterTitle(getString(R.string.tab_setting_title), 0);
        initListener();
    }

    private void initListener() {
        tv_aboutus.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_aboutus:
                startActivity(new Intent(mActivity, AboutUsActivity.class));
                break;
        }
    }
}
