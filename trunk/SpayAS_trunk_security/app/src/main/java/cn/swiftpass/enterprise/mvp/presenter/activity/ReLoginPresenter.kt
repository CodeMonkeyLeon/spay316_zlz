package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.CertificateBean
import cn.swiftpass.enterprise.bussiness.model.DynModels
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo
import cn.swiftpass.enterprise.bussiness.model.UserInfo
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.io.okhttp.OkhttpUtil
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.ReLoginContract
import cn.swiftpass.enterprise.mvp.utils.ParamsUtils
import cn.swiftpass.enterprise.utils.JsonUtil
import cn.swiftpass.enterprise.utils.MD5
import cn.swiftpass.enterprise.utils.SharedPreUtils
import cn.swiftpass.enterprise.utils.SignUtil
import com.example.common.entity.EmptyData
import okhttp3.Call


/**
 * @author lizheng.zhao
 * @date 2022/11/18
 *
 * @一切都明明白白
 * @但我们仍匆匆错过
 * @因为你相信命运
 * @因为我怀疑生活。
 */
class ReLoginPresenter : ReLoginContract.Presenter {


    private var mView: ReLoginContract.View? = null


    override fun apiShowList() {
        mView?.let { view ->
            AppClient.apiShowList(
                MainApplication.getInstance().getMchId(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<DynModels>(DynModels()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        commonResponse?.let { error ->
                            view.apiShowListFailed(error.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        r?.let { response ->
                            val dynModels = JsonUtil.jsonToBean(
                                response.message,
                                DynModels::class.java
                            ) as DynModels
                            if (null != dynModels.data && dynModels.data.size > 0) {
                                dynModels.data[0].md5 = dynModels.md5
                            }
                            view.apiShowListSuccess(dynModels.data)
                        }
                    }
                }
            )
        }
    }

    override fun deviceLogin() {

        mView?.let { view ->
            AppClient.deviceLogin(
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<UserInfo>(UserInfo()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        commonResponse?.let { error ->
                            view.deviceLoginFailed(error.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        r?.let { response ->
                            val u = JsonUtil.jsonToBean(
                                response.message,
                                UserInfo::class.java
                            ) as UserInfo
                            MainApplication.getInstance().userInfo = u
                            ParamsUtils.parseJson(MainApplication.getInstance().userInfo)
                            ParamsUtils.saveLoginCookieToSp(response.message)
                            view.deviceLoginSuccess(true)
                        }
                    }
                }
            )
        }
    }

    override fun getCertificateString(isDeviceLogin: Boolean) {
        mView?.run {
            showLoading(R.string.show_login_loading, ParamsConstants.COMMON_LOADING)
            val param: MutableMap<String, String> = java.util.HashMap()
            val spayRs = System.currentTimeMillis()
            //加签名
            param[ParamsConstants.SPAY_RS] = spayRs.toString()
            // 获取证书，使用旧的签名方式（签名Key为时间戳spayRs）
            param[ParamsConstants.NNS] =
                SignUtil.getInstance().createSign(param, MD5.md5s(spayRs.toString()))


            AppClient.getCertificateString(
                spayRs.toString(),
                object : CallBackUtil.CallBackCommonResponse<CertificateBean>(CertificateBean()) {
                    override fun onResponse(r: CommonResponse?) {
                        dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val dynModels = JsonUtil.jsonToBean(
                                response.message,
                                CertificateBean::class.java
                            ) as CertificateBean
                            getCertificateStringSuccess(dynModels, isDeviceLogin)
                        }
                    }

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { error ->
                            getCertificateStringFailed(error.message, isDeviceLogin)
                        }
                    }
                }
            )
        }
    }

    override fun logout() {

        mView?.let { view ->
            view.showLoading(R.string.public_loading, ParamsConstants.COMMON_LOADING)

            val param: MutableMap<String, String> = java.util.HashMap()
            val spayRs = System.currentTimeMillis()
            //加签名
            val priKey = SharedPreUtils.readProduct(ParamsConstants.SECRET_KEY) as String?
            param[ParamsConstants.SPAY_RS] = spayRs.toString()

            param.put(
                ParamsConstants.NNS, SignUtil.getInstance().createSign(
                    param,
                    MD5.md5s(priKey).substring(0, 16)
                )
            )


            OkhttpUtil.okHttpPost(
                MainApplication.getInstance().baseUrl + "spay/user/logout",
                param,
                OkhttpUtil.getHeadParam(spayRs.toString()),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { error ->
                            view.logoutFailed(error.message)
                        }

                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        view.logoutSuccess(true)
                    }
                }
            )

        }
    }

    override fun ecdhKeyExchange(publicKey: String?) {
        mView?.let { view ->
            view.showLoading(R.string.show_login_loading, ParamsConstants.COMMON_LOADING)

            AppClient.ecdhKeyExchange(
                publicKey,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<ECDHInfo>(ECDHInfo()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { error ->
                            view.ecdhKeyExchangeFailed(error.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val info = JsonUtil.jsonToBean(
                                response.message,
                                ECDHInfo::class.java
                            ) as ECDHInfo
                            MainApplication.getInstance().setSerPubKey(info.serPubKey)
                            MainApplication.getInstance().setSKey(info.skey)
                            view.ecdhKeyExchangeSuccess(info)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: ReLoginContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}