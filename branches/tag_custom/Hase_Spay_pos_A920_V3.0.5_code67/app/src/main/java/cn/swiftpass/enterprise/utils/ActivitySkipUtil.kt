package cn.swiftpass.enterprise.utils

import android.app.Activity
import android.content.Intent
import cn.swiftpass.enterprise.intl.R
import java.io.Serializable

object ActivitySkipUtil {

    enum class ANIM_TYPE {
        NONE,
        RIGHT_IN,
        RIGHT_OUT,
        LEFT_IN,
        LEFT_OUT
    }

    /**
     * 开启 activity
     */
    fun startAnotherActivity(
        fromActivity: Activity,
        cls: Class<out Activity?>?,
        hashMap: HashMap<String, out Any?>? = null,
        animType: ANIM_TYPE = ANIM_TYPE.NONE
    ) {
        val intent = Intent(fromActivity, cls)
        hashMap?.let {
            putDataToIntent(intent, it)
        }
        fromActivity.startActivity(intent)
        startActivityWithAnim(fromActivity, animType)
    }

    /**
     * 开启 activity 返回时带结果
     */
    fun startAnotherActivityForResult(
        fromActivity: Activity,
        cls: Class<out Activity?>?,
        requestCode: Int,
        hashMap: HashMap<String, out Any?>? = null,
        animType: ANIM_TYPE = ANIM_TYPE.NONE
    ) {
        val intent = Intent(fromActivity, cls)
        hashMap?.let {
            putDataToIntent(intent, it)
        }
        fromActivity.startActivityForResult(intent, requestCode)
        startActivityWithAnim(fromActivity, animType)
    }


    private fun putDataToIntent(intent: Intent, hashMap: HashMap<String, out Any?>) {
        for (e in hashMap.entries) {
            when (e.value) {
                is String -> intent.putExtra(e.key, e.value as String)
                is Boolean -> intent.putExtra(e.key, e.value as Boolean)
                is Int -> intent.putExtra(e.key, e.value as Int)
                is Byte -> intent.putExtra(e.key, e.value as Byte)
                is Long -> intent.putExtra(e.key, e.value as Long)
                is Float -> intent.putExtra(e.key, e.value as Float)
                is Double -> intent.putExtra(e.key, e.value as Double)
                is Serializable -> intent.putExtra(e.key, e.value as Serializable)
            }
        }
    }


    private fun startActivityWithAnim(fromActivity: Activity, animType: ANIM_TYPE) {
        val startAnimId = getAnimationId(animType)
        if (startAnimId != 0) {
            fromActivity.overridePendingTransition(startAnimId, R.anim.none)
        }
    }


    /**
     * 获取动画资源ID
     */
    private fun getAnimationId(animType: ANIM_TYPE): Int {
        return when (animType) {
            ANIM_TYPE.LEFT_IN -> startFromLeftInAnim()
            ANIM_TYPE.RIGHT_IN -> startFromRightInAnim()
            ANIM_TYPE.LEFT_OUT -> finishFromLeftOutAnim()
            ANIM_TYPE.RIGHT_OUT -> finishFromRightOutAnim()
            else -> 0
        }
    }


    /**
     * 从屏幕的右边进入
     */
    private fun startFromRightInAnim() = R.anim.from_right_in_act


    /**
     * 从屏幕的右边移出
     */
    private fun finishFromRightOutAnim() = R.anim.from_right_out_act


    /**
     * 从屏幕的左边进入
     */
    private fun startFromLeftInAnim() = R.anim.from_left_in_act


    /**
     * 从屏幕的左边移出
     */
    private fun finishFromLeftOutAnim() = R.anim.from_left_out_act

}