/*
 * 文 件 名:  MyGridView.java
 * 版    权:   All rights reserved.
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013年10月30日
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.shop;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.GridView;
import android.widget.ScrollView;

/**
 * 图片显示自定义控件
 * 
 * @author  he+hui
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class PersonalGridView extends GridView
{
    ScrollView parentScrollView;
    
    public ScrollView getParentScrollView()
    {
        return parentScrollView;
    }
    
    public void setParentScrollView(ScrollView parentScrollView)
    {
        this.parentScrollView = parentScrollView;
    }
    
    private int maxHeight;
    
    public int getMaxHeight()
    {
        return maxHeight;
    }
    
    public void setMaxHeight(int maxHeight)
    {
        this.maxHeight = maxHeight;
    }
    
    public PersonalGridView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
    
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
            MeasureSpec.AT_MOST);
    super.onMeasure(widthMeasureSpec, expandSpec);
    }
    
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev)
    {
        
        //        switch (ev.getAction())
        //        {
        //            case MotionEvent.ACTION_DOWN:
        //                
        //                setParentScrollAble(false);
        //            case MotionEvent.ACTION_MOVE:
        //                
        //                break;
        //            case MotionEvent.ACTION_UP:
        //                
        //            case MotionEvent.ACTION_CANCEL:
        //                setParentScrollAble(true);
        //                break;
        //            default:
        //                break;
        //        }
        return super.onInterceptTouchEvent(ev);
    }
    
    /**
     * @param flag
     */
    private void setParentScrollAble(boolean flag)
    {
        
        parentScrollView.requestDisallowInterceptTouchEvent(!flag);
    }
    
}
