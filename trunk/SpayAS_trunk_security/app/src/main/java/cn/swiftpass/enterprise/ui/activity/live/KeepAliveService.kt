package cn.swiftpass.enterprise.ui.activity.live

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.IBinder
import android.util.Log
import cn.swiftpass.enterprise.intl.BuildConfig
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.utils.KotlinUtils
import cn.swiftpass.enterprise.utils.permissionutils.Permission
import kotlin.concurrent.thread

/**
 * @author lizheng.zhao
 * @date 2023/06/30
 * @description 保活服务，通过循环播放一段无声 mp3实现保活。
 * Android 8.0以上后台应用开启前台服务时，必须开启前台通知。以及 Android 12对后台应有开启前台服务的限制。
 * 详情可以参考我的博客《Android后台应用开启前台服务---android8到android12梳理》--- KillerNoBlood
 */
class KeepAliveService : Service() {


    companion object {
        const val SETTINGS_ACTION = "android.settings.APPLICATION_DETAILS_SETTINGS"
        const val NOTIFICATION_ID = 0x11
        const val TAG = "KeepAliveService"
    }

    private var mMediaPlayer: MediaPlayer? = null


    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.isDebug) {
            Log.i(TAG, "保活服务KeepAliveService --- create")
        }
        //引用无声mp3音源
        //循环播放
        mMediaPlayer = MediaPlayer.create(applicationContext, R.raw.silent).apply {
            isLooping = true
        }
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (BuildConfig.isDebug) {
            Log.i(TAG, "保活服务KeepAliveService --- start")
        }
        sendForegroundNotification()
        thread {
            startPlayMusic()
        }
        return START_STICKY
    }


    override fun onDestroy() {
        super.onDestroy()
        if (BuildConfig.isDebug) {
            Log.i(TAG, "保活服务KeepAliveService --- destroy")
        }
        stopPlayMusic()
        val intent = Intent(applicationContext, KeepAliveService::class.java)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            //android 13需要申请通知权限才能发通知
            if (KotlinUtils.isPermissionsGranted(
                    applicationContext,
                    arrayOf(Permission.POST_NOTIFICATIONS)
                )
            ) {
                //能发出前台通知才可以保活
                startForegroundService(intent)
            }
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //android8.0以上禁止后台默默运行服务
            //若在后台运行服务，必须在通知栏显示，告诉用户应用在消耗资源
            startForegroundService(intent)
        } else {
            startService(intent)
        }
    }


    private val sendForegroundNotification = {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (BuildConfig.isDebug) {
                Log.i(TAG, "保活服务KeepAliveService --- android 8.0以上发送前台通知")
            }
            //Android 8.0 及以上
            val builder = Notification.Builder(this).apply {
                setSmallIcon(R.drawable.icon_app_logo)
                setContentTitle(getString(R.string.app_name) + " " + getString(R.string.tv_notification_title))
                setContentText(getString(R.string.tv_notification_content))
            }

            val broadcastIntent = Intent().apply {
                action = SETTINGS_ACTION
                data = Uri.fromParts("package", packageName, null)
            }

            val pendingIntent =
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S)
                    PendingIntent.getActivity(
                        this,
                        0,
                        broadcastIntent,
                        PendingIntent.FLAG_IMMUTABLE
                    )
                else
                    PendingIntent.getActivity(
                        this,
                        0,
                        broadcastIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                    )

            builder.setContentIntent(pendingIntent)

            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            val notificationChannel = NotificationChannel(
                NOTIFICATION_ID.toString(),
                "spay",
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(notificationChannel)
            val notification =
                Notification.Builder(applicationContext, NOTIFICATION_ID.toString()).build()
            startForeground(1, notification)
        } else {
            //Android 8.0 以下
            //无需发送前台通知
        }


    }


    private val startPlayMusic = {
        mMediaPlayer?.run {
            if (!isPlaying) {
                start()
            }
        }
    }

    private val stopPlayMusic = {
        mMediaPlayer?.run {
            stop()
            reset()
            release()
        }
    }


    override fun onBind(intent: Intent?): IBinder? {
        return null
    }
}