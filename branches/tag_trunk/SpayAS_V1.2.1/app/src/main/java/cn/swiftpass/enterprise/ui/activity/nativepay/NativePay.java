/*
 * 文 件 名:  NativePay.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2014-7-16
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.nativepay;

import java.util.SortedMap;
import java.util.TreeMap;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.qrcode.MaxCardManager;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;

/**
 * native生产二维码
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2014-7-16]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class NativePay extends BaseActivity
{
    
    /** {@inheritDoc} */
    
    @Override
    protected boolean isLoginRequired()
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nativepay);
        
        EditText money = (EditText)findViewById(R.id.money);
        
        Button but = (Button)findViewById(R.id.but);
        
        final ImageView img = getViewById(R.id.img);
        
        String parm = "weixin://wxpay/bizpayurl?";
        
        but.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                SortedMap<String, String> signParams = new TreeMap<String, String>();
                signParams.put("appid", "wx9aa65bfd4861bc6e");
                signParams.put("nonceStr", Sha1Util.getNonceStr());
                signParams.put("productid", "1234567890");
                signParams.put("timestamp", Sha1Util.getTimeStamp());
                
                try
                {
                    String sign = Sha1Util.createSHA1Sign(signParams);
                    
                    String str = Sha1Util.createSign(signParams);
                    
                    String weixin_sign = str + "&sign=" + sign;
                    
                    Bitmap bitmap = MaxCardManager.getInstance().create2DCode(weixin_sign, 400, 400);
                    img.setImageBitmap(bitmap);
                    
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            
        });
    }
}
