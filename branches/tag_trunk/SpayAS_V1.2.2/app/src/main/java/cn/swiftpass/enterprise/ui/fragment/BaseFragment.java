package cn.swiftpass.enterprise.ui.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.utils.DialogHelper;

public class BaseFragment extends Fragment
{
    protected ProgressDialog loadingDialog = null;
    
    protected boolean isResumed = false;
    
    private Dialog dialogNew = null;
    
    public void showLoading(boolean cancelble, String str)
    {
        if (!cancelble)
            return;
        if (loadingDialog == null)
        {
            loadingDialog = new ProgressDialog(getActivity());
            loadingDialog.setCancelable(cancelble);
            
        }
        loadingDialog.show();
        loadingDialog.setMessage(str);
    }
    
    public void dismissMyLoading()
    {
        if (loadingDialog != null)
        {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }
    
    public void dismissLoading()
    {
        //        dismissMyLoading();
        
        dissDialog();
    }
    
    public void loadDialog(Activity context, String str)
    {
        // 加载样式
        if (null == dialogNew)
        {
            dialogNew = new Dialog(context, R.style.my_dialog);
            dialogNew.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogNew.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogNew.setCanceledOnTouchOutside(false);
            LayoutInflater inflater = LayoutInflater.from(context);
            View v = inflater.inflate(R.layout.load_progress_info, null);// 得到加载view dialog_common
            TextView tv = (TextView)v.findViewById(R.id.tv_dialog);
            tv.setText(str);
            // dialog透明
            WindowManager.LayoutParams lp = dialogNew.getWindow().getAttributes();
            lp.alpha = 0.8f;
            dialogNew.getWindow().setAttributes(lp);
            dialogNew.setContentView(v);
            dialogNew.setOnKeyListener(new OnKeyListener()
            {
                
                @Override
                public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2)
                {
                    
                    if (keycode == KeyEvent.KEYCODE_BACK)
                    {
                        return true;
                    }
                    return false;
                }
            });
        }
        DialogHelper.resizeNew(context, dialogNew);
        dialogNew.show();
    }
    
    public void loadDialog(Activity context, int res)
    {
        // 加载样式
        try
        {
            
            if (null == dialogNew)
            {
                dialogNew = new Dialog(context, R.style.my_dialog);
                dialogNew.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogNew.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialogNew.setCanceledOnTouchOutside(false);
                LayoutInflater inflater = LayoutInflater.from(context);
                View v = inflater.inflate(R.layout.load_progress_info, null);// 得到加载view dialog_common
                TextView tv = (TextView)v.findViewById(R.id.tv_dialog);
                tv.setText(res);
                // dialog透明
                WindowManager.LayoutParams lp = dialogNew.getWindow().getAttributes();
                lp.alpha = 0.8f;
                dialogNew.getWindow().setAttributes(lp);
                dialogNew.setContentView(v);
                dialogNew.setOnKeyListener(new OnKeyListener()
                {
                    
                    @Override
                    public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2)
                    {
                        
                        if (keycode == KeyEvent.KEYCODE_BACK)
                        {
                            return true;
                        }
                        return false;
                    }
                });
            }
            DialogHelper.resizeNew(context, dialogNew);
            dialogNew.show();
        }
        catch (Exception e)
        {
            Log.e("hehui", "" + e);
        }
    }
    
    public void dissDialog()
    {
        if (null != dialogNew)
        {
            getActivity().runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    try
                    {
                        dialogNew.dismiss();
                        dialogNew = null;
                    }
                    catch (Exception e)
                    {
                    }
                }
            });
        }
    }
    
    public void toastDialog(final Activity context, final String content, final NewDialogInfo.HandleBtn handleBtn)
    {
        context.runOnUiThread(new Runnable()
        {
            
            @Override
            public void run()
            {
                
                try
                {
                    
                    NewDialogInfo info = new NewDialogInfo(context, content, null, 2, null, handleBtn);
                    info.setOnKeyListener(new OnKeyListener()
                    {
                        
                        @Override
                        public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2)
                        {
                            
                            if (keycode == KeyEvent.KEYCODE_BACK)
                            {
                                return true;
                            }
                            return false;
                        }
                    });
                    DialogHelper.resize(context, info);
                    
                    info.show();
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                    Log.e("hehui", "" + e);
                }
            }
        });
        
    }
    
    public void toastDialog(final Activity context, final Integer content, final NewDialogInfo.HandleBtn handleBtn)
    {
        context.runOnUiThread(new Runnable()
        {
            
            @Override
            public void run()
            {
                try
                {
                    
                    NewDialogInfo info =
                        new NewDialogInfo(context, context.getString(content), null, 2, null, handleBtn);
                    info.setOnKeyListener(new OnKeyListener()
                    {
                        
                        @Override
                        public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2)
                        {
                            
                            if (keycode == KeyEvent.KEYCODE_BACK)
                            {
                                return true;
                            }
                            return false;
                        }
                    });
                    DialogHelper.resize(context, info);
                    
                    info.show();
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                    Log.e("hehui", "" + e);
                }
            }
        });
        
    }
    
}
