package cn.swiftpass.enterprise.mymould.base.mvp;

import java.lang.ref.WeakReference;

public class BasePresenter<V extends BaseView> {

    private WeakReference<V> viewRef;

    /**
     * 绑定view，一般在初始化中调用该方法
     *
     * @param view view
     */
    public void attachView(V view) {
        viewRef = new WeakReference<V>(view);
    }

    /**
     * 获取界面的引用
     */
    protected V getView() {
        return viewRef == null ? null : viewRef.get();
    }


    /**
     * 解除绑定view，一般在onDestroy中调用
     */

    public void detachView() {
        if (viewRef != null) {
            viewRef.clear();
            viewRef = null;
        }
    }


    /**
     * 判断界面是否销毁
     */
    protected boolean isViewAttached() {
        return viewRef != null && viewRef.get() != null;
    }


}
