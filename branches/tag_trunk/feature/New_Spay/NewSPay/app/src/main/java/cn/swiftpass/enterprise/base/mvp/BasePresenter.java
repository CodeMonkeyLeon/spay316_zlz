package cn.swiftpass.enterprise.base.mvp;

import com.trello.rxlifecycle2.LifecycleProvider;
import com.trello.rxlifecycle2.android.ActivityEvent;

import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class BasePresenter<V extends IView> {

    private WeakReference<V> viewRef;
    private LifecycleProvider<ActivityEvent> mProvider;

    /**
     * 绑定view，一般在初始化中调用该方法
     *
     * @param view
     */
    public void attachView(V view) {
        viewRef = new WeakReference<>(view);
    }

    /**
     * 获取界面的引用
     */
    protected V getView() {
        return viewRef.get();
    }

    /**
     * 解除绑定view，一般在onDestroy中调用
     */
    public void detachView() {
        if (viewRef != null) {
            viewRef.clear();
            viewRef = null;
        }
    }

    /**
     * 判断界面是否销毁
     */
    protected boolean isViewAttached() {
        return viewRef != null && viewRef.get() != null;
    }

    /**
     * 为了给BaseMultiplePresenter，实例化该无参构造方法
     */
    public BasePresenter() {
    }

    /**
     * BasePresenter构建的时候传入Life
     *
     * @param provider
     */
    public BasePresenter(LifecycleProvider<ActivityEvent> provider) {
        this.mProvider = provider;
    }

    /**
     * 获取Life
     *
     * @return
     */
    public LifecycleProvider<ActivityEvent> getProvider() {
        return mProvider;
    }

    /**
     * 设置Life
     *
     * @param provider
     */
    public void setProvider(LifecycleProvider<ActivityEvent> provider) {
        this.mProvider = provider;
    }


}
