package com.example.common.sentry

import android.content.Context
import android.text.TextUtils
import io.sentry.Sentry
import io.sentry.UserFeedback
import io.sentry.android.core.SentryAndroid

object SentryUtils {

    private const val ENVIRONMENT_PRODUCTION = "production"
    private const val ENVIRONMENT_DEBUG = "debug"
    private const val TYPE_TRY_CATCH = "try-catch"
    private const val TYPE_TRY_CATCH_THROWABLE = "try-catch throwable"
    private const val TYPE_TRY_CATCH_ERROR = "try-catch  error"
    private const val TYPE_TRY_CATCH_MSG = "内部try-catch"

    //是否为调试模式
    private var mIsDebug = true

    //是否上传本地try-catch异常
    private var mIsUploadLocalTryCatchException = false


    /**
     * 设置初始化参数
     */
    fun init(
        context: Context?,
        bankCode: String,
        versionName: String,
        environment: String,
        isUploadLocalTryCatchException: Boolean = false
    ) {
        mIsDebug = !TextUtils.equals(environment, ENVIRONMENT_PRODUCTION)
        mIsUploadLocalTryCatchException = isUploadLocalTryCatchException
        context?.let { con ->
            SentryAndroid.init(
                con
            ) { options ->
                //Release --- 不同项目判断标识
                options.release = "$bankCode  $versionName"
                //SampleRate --- 错误事件的采样率 100%
                options.sampleRate = 1.0
                //TracesSampleRate --- 自动监控性能+健康+点击事件收集采样率 10%
                options.tracesSampleRate = 0.1
                //Environment --- 区分开发阶段标识
                //开发+测试+SIT+UAT情况设置 options.environment = "debug"
                //灰度上线+生产上线情况设置 options.environment = "production"
                options.environment = environment
            }
        }
    }


    private fun setSentryUploadData(data: SentryInfoData) {
        var apiName = ""
        var infoNumber = ""
        var infoName = ""
        var errorMsg = ""
        var errorCode = ""
        var exception = ""
        data.apiName?.let {
            apiName = it
        }
        data.infoNumber?.let {
            infoNumber = it
        }
        data.infoName?.let {
            infoName = it
        }
        data.errorMsg?.let {
            errorMsg = it
        }
        data.errorCode?.let {
            errorCode = it
        }
        data.exception?.let {
            exception = it
        }
        //设置标签字段
        Sentry.setTag(SentryInfoData.PARAM_API_NAME, apiName)
        Sentry.setTag(SentryInfoData.PARAM_INFO_NUMBER, infoNumber)
        Sentry.setTag(SentryInfoData.PARAM_INFO_NAME, infoName)
        Sentry.setTag(SentryInfoData.PARAM_ERROR_MSG, errorMsg)
        Sentry.setTag(SentryInfoData.PARAM_ERROR_CODE, errorCode)
        Sentry.setTag(SentryInfoData.PARAM_EXCEPTION, exception)
        //设置其他信息
        Sentry.setExtra(SentryInfoData.PARAM_API_NAME, apiName)
        Sentry.setExtra(SentryInfoData.PARAM_INFO_NUMBER, infoNumber)
        Sentry.setExtra(SentryInfoData.PARAM_INFO_NAME, infoName)
        Sentry.setExtra(SentryInfoData.PARAM_ERROR_MSG, errorMsg)
        Sentry.setExtra(SentryInfoData.PARAM_ERROR_CODE, errorCode)
        Sentry.setExtra(SentryInfoData.PARAM_EXCEPTION, exception)


//        //设置警报等级
//        Sentry.setLevel(SentryLevel.FATAL)
    }


    /**
     * 上传try - catch 异常
     */
    fun uploadTryCatchException(
        exception: Exception?,
        apiName: String
    ) {
        if (mIsUploadLocalTryCatchException) {
            exception?.let { e ->
                val data = SentryInfoData(apiName, e.message, TYPE_TRY_CATCH)
                setSentryUploadData(data)
                Sentry.captureMessage("$TYPE_TRY_CATCH_MSG  ${e.stackTraceToString()}      ${e.message}")
                if (mIsDebug) {
                    e.printStackTrace()
                }
            }
        }
    }


    /**
     * 上传try - catch 异常
     */
    fun uploadTryCatchThrowable(
        throwable: Throwable?,
        apiName: String
    ) {
        if (mIsUploadLocalTryCatchException) {
            throwable?.let { t ->
                val data = SentryInfoData(apiName, t.message, TYPE_TRY_CATCH_THROWABLE)
                setSentryUploadData(data)
                Sentry.captureMessage("$TYPE_TRY_CATCH_MSG  ${t.stackTraceToString()}      ${t.message}")
                if (mIsDebug) {
                    t.printStackTrace()
                }
            }
        }
    }


    /**
     * 上传try - catch 异常
     */
    fun uploadTryCatchError(
        error: Error?,
        apiName: String
    ) {
        if (mIsUploadLocalTryCatchException) {
            error?.let { e ->
                val data = SentryInfoData(apiName, e.message, TYPE_TRY_CATCH_ERROR)
                setSentryUploadData(data)
                Sentry.captureMessage("$TYPE_TRY_CATCH_MSG  ${e.stackTraceToString()}      ${e.message}")
                if (mIsDebug) {
                    e.printStackTrace()
                }
            }
        }
    }

    /**
     * 上传网络接异常
     */
    fun uploadNetInterfaceException(
        exception: Exception?,
        apiName: String,
        errorMsg: String?,
        errorCode: String?
    ) {
        exception?.let { e ->
            val data = SentryInfoData(apiName, errorMsg, errorCode, e.message)
            setSentryUploadData(data)
            Sentry.captureException(e)
            if (mIsDebug) {
                e.printStackTrace()
            }
        }
    }


    /**
     * 网络层特殊报错
     * 网络超时 / 服务器连接失败 / 证书签名失败 / CA证书校验失败"
     */
    fun uploadNetworkLayerException(
        exception: Exception?,
        apiName: String,
        errorMsg: String?
    ) {
        exception?.let { e ->
            val data = SentryInfoData(apiName, errorMsg, "HTTP400", e.message)
            setSentryUploadData(data)
            Sentry.captureException(e)
            if (mIsDebug) {
                e.printStackTrace()
            }
        }
    }


    /**
     * 上传未捕获的异常
     */
    fun uploadUncaughtThrowable(throwable: Throwable?, apiName: String) {
        throwable?.let {
            val data = SentryInfoData(apiName, it.message, "未捕获异常")
            setSentryUploadData(data)
//            Sentry.captureException(it)
            Sentry.captureMessage("${it.stackTraceToString()}\n${it.localizedMessage}")
            if (mIsDebug) {
                it.printStackTrace()
            }
        }
    }


    fun sendUserFeedBack(uuid: String? = "", message: String? = "") {
        message?.let { msg ->
            val msgId = Sentry.captureMessage("Network Diagnostics - Android")
            Sentry.captureUserFeedback(
                UserFeedback(
                    msgId,
                    uuid,
                    "",
                    msg
                )
            )
        }
    }


    /**
     * 获取当前类名+方法名
     */
    fun getClassNameAndMethodName(): String {
        return "类名: ${Throwable().stackTrace[1].className}   方法名: ${Throwable().stackTrace[1].methodName}"
    }

}