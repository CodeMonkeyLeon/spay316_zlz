package cn.swiftpass.enterprise.ui.activity;

import static cn.swiftpass.enterprise.MainApplication.getInstance;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.common.sentry.SentryUtils;
import com.example.common.sp.PreferenceUtil;
import com.google.zxing.WriterException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.MaxCardManager;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.MasterCardInfo;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.QRcodeInfo;
import cn.swiftpass.enterprise.bussiness.model.WalletListBean;
import cn.swiftpass.enterprise.common.Constant;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.MasterCardQrCodeContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.MasterCardQrCodePresenter;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.SelectPicPopupWindow;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.DisplayUtil;
import cn.swiftpass.enterprise.utils.LocaleUtils;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.StringUtil;


/**
 * Created by aijingya on 2021/3/10.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description:
 * @date 2021/3/10.14:01.
 */
public class MasterCardQrCodeActivity extends BaseActivity<MasterCardQrCodeContract.Presenter> implements MasterCardQrCodeContract.View {
    private ScrollView sv_master_card_native_pay_static_code;
    private LinearLayout has_surcharge_fee_layout;
    private TextView tv_amount, tv_surcharge_money;
    private TextView tv_money_total;
    private ImageView iv_qr_code_img;
    private ImageView iv_payment_channel_small_icon;
    private TextView tv_payment_channel, tv_switch_channel;
    private TimeCount timeCount;
    private Handler mHandler;
    private DialogInfo dialogInfo;
    private SelectPicPopupWindow menuWindow, menuLiquidWindow;
    private Context mContext;
    private MasterCardInfo masterCardInfo;//MasterCard下单的对象
    private QRcodeInfo qrcodeInfo;//普通下单的对象
    private String totalMoney;
    private String mark = "";
    private double minus = 1;
    private long CountTimes = 5;
    private Runnable myRunnable = new Runnable() {
        @Override
        public void run() {
            if (CountTimes > 0 && dialogInfo != null) {
                dialogInfo.setBtnOkText(getString(R.string.btnOk) + "(" + getString(R.string.show_close) + CountTimes + getString(R.string.tv_second) + ")");
                CountTimes -= 1;
                mHandler.postDelayed(this, 1000);
            } else {
                mHandler.removeCallbacks(this);
                if (dialogInfo != null && dialogInfo.isShowing()) {
                    dialogInfo.dismiss();
                }
                dialogInfo.setBtnOkText(getString(R.string.dialog_close));
                finish();
            }
        }
    };

    public static void startActivity(Context context, MasterCardInfo masterCardInfo) {
        Intent it = new Intent();
        it.putExtra("MasterCardInfo", masterCardInfo);
        it.setClass(context, MasterCardQrCodeActivity.class);
        context.startActivity(it);
    }

    @Override
    protected MasterCardQrCodeContract.Presenter createPresenter() {
        return new MasterCardQrCodePresenter();
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();

        //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth", "sale");
        if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(saleOrPreauth, "pre_auth")) {
            titleBar.setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
        } else {
            titleBar.setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
        }
        titleBar.setRightButLayVisible(false, null);
        titleBar.setLeftButtonVisible(true);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {
            }

            @Override
            public void onRightButLayClick() {
            }
        });
    }


    @Override
    protected boolean useToolBar() {
        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_payment_qrcode);
        MainApplication.getInstance().getListActivities().add(this);

        //根据当前的小数点的位数来判断应该除以多少
        for (int i = 0; i < MainApplication.getInstance().getNumFixed(); i++) {
            minus = minus * 10;
        }

        masterCardInfo = (MasterCardInfo) getIntent().getSerializableExtra("MasterCardInfo");
        mark = getIntent().getStringExtra("mark");

        initViews();

        initUIDate();

        //开始扫码查单
        Countdown();
    }

    private void initViews() {
        mContext = this;
        sv_master_card_native_pay_static_code = findViewById(R.id.sv_master_card_native_pay_static_code);
        has_surcharge_fee_layout = findViewById(R.id.has_surcharge_fee_layout);
        tv_amount = findViewById(R.id.tv_amount);
        tv_surcharge_money = findViewById(R.id.tv_surcharge_money);

        tv_money_total = findViewById(R.id.tv_money_total);
        iv_qr_code_img = findViewById(R.id.iv_qr_code_img);
        iv_payment_channel_small_icon = findViewById(R.id.iv_payment_channel_small_icon);

        tv_payment_channel = findViewById(R.id.tv_payment_channel);
        tv_switch_channel = findViewById(R.id.tv_switch_channel);

        //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth", "sale");
        if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(saleOrPreauth, "pre_auth")) {
            sv_master_card_native_pay_static_code.setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
            tv_switch_channel.setTextColor(getResources().getColor(R.color.bg_text_pre_auth));
        } else {
            sv_master_card_native_pay_static_code.setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
            tv_switch_channel.setTextColor(getResources().getColor(R.color.title_bg_new));
        }

        mHandler = new Handler();

        /**
         * 切换支付方式，展示不同支付方式的二维码
         */
        tv_switch_channel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //点击扫码，不管是否是liquid pay 通道，先展示一级列表
                menuWindow = new SelectPicPopupWindow(MasterCardQrCodeActivity.this, new SelectPicPopupWindow.HandleNative() {
                    @Override
                    public void toPay(String type, String secondType, boolean isSecondPay, WalletListBean bean) {
                        if (getNativePayType(type).equalsIgnoreCase(Constant.NATIVE_LIQUID)) {//如果是点击的小钱包通道
                            menuLiquidWindow = new SelectPicPopupWindow(MasterCardQrCodeActivity.this, type, true, new SelectPicPopupWindow.HandleNative() {
                                @Override
                                public void toPay(String type, String secondType, boolean isSecondPay, WalletListBean bean) {
                                    //生成新的二维码之前，停止查询旧的单
                                    if (timeCount != null) {
                                        timeCount.cancel();
                                    }

                                    toNativePay(totalMoney, type, secondType, mark, isSecondPay, false, false, bean);
                                }
                            });
                            //显示窗口
                            menuLiquidWindow.showAtLocation(tv_switch_channel, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0); //设置layout在PopupWindow中显示的位置

                        } else if (getNativePayType(type).equalsIgnoreCase(Constant.NATIVE_INSTAPAY)) {//如果是点击的instapay的通道
                            //生成新的二维码之前，停止查询旧的单
                            if (timeCount != null) {
                                timeCount.cancel();
                            }

                            //是instapay通道，下单成功，则直接跳转到instapay页面
                            toNativePay(totalMoney, type, secondType, mark, false, true, false, bean);

                        } else if (getNativePayType(type).equalsIgnoreCase(Constant.NATIVE_INSTAPAY_V2)) {//如果是点击的instapay的QRPayment通道
                            //生成新的二维码之前，停止查询旧的单
                            if (timeCount != null) {
                                timeCount.cancel();
                            }

                            //是instapay通道，下单成功，则直接跳转到instapay页面
                            toNativePay(totalMoney, type, secondType, mark, false, false, true, bean);

                        } else if (isMasterCardServiceType(getNativePayType(type))) {//如果当前选择的通道是MasterCard卡通道
                            //生成新的二维码之前，停止查询旧的单
                            if (timeCount != null) {
                                timeCount.cancel();
                            }

                            //则走masterCard下单逻辑，下单成功直接跳转到MasterCard卡收单二维码收款界面
                            toMasterCardNewPay(totalMoney, type);
                        } else {
                            //生成新的二维码之前，停止查询旧的单
                            if (timeCount != null) {
                                timeCount.cancel();
                            }

                            //不是小钱包通道，则直接生成二维码
                            toNativePay(totalMoney, type, secondType, mark, false, false, false, bean);
                        }
                    }
                });

                //显示窗口
                menuWindow.showAtLocation(tv_switch_channel, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0); //设置layout在PopupWindow中显示的位置
            }
        });
    }

    private void initUIDate() {
        if (masterCardInfo != null) {
            if (!TextUtils.isEmpty(masterCardInfo.getApiCode())) {
                titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()));
            }
            //下单金额
            totalMoney = masterCardInfo.getOrderAmount() + "";

            //如果开通附加费，才展示下面的附加费金额
            if (MainApplication.getInstance().isSurchargeOpen()) {
                has_surcharge_fee_layout.setVisibility(View.VISIBLE);
            } else {
                has_surcharge_fee_layout.setVisibility(View.GONE);
            }

            //计算金额的展示
            BigDecimal orderMoneyDecimal = new BigDecimal(DateUtil.formatMoneyUtils(masterCardInfo.getOrderAmount()));//订单金额（不包含附加费的金额）
            BigDecimal SurchargeMoneyDecimal = new BigDecimal(DateUtil.formatMoneyUtils(masterCardInfo.getSurcharge()));//订单附加费

            //如果没有开通附加费
            if (!MainApplication.getInstance().isSurchargeOpen()) {
                tv_money_total.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(masterCardInfo.getMoney())); //订单总额就是订单金额
                has_surcharge_fee_layout.setVisibility(View.GONE);
            } else {
                tv_money_total.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(masterCardInfo.getMoney()));
                has_surcharge_fee_layout.setVisibility(View.VISIBLE);
            }

            paseMoneyWithSurcharge(orderMoneyDecimal, SurchargeMoneyDecimal);
            //生成二维码
            GenerateQRCodeView(null, masterCardInfo.getCashierUrl(), false, false, true, null);
        }
    }

    private void paseMoneyWithSurcharge(BigDecimal moneyDecimal, BigDecimal bigSurcharge) {
        try {
            if (!MainApplication.getInstance().getFeeType().equalsIgnoreCase("CNY")) {
                BigDecimal paseBigDecimal = null;
                if (moneyDecimal != null) {
                    if (MainApplication.getInstance().getSourceToUsdExchangeRate() > 0 && MainApplication.getInstance().getUsdToRmbExchangeRate() > 0) {
                        BigDecimal sourceToUsd = new BigDecimal(MainApplication.getInstance().getSourceToUsdExchangeRate());
                        BigDecimal usdRate = new BigDecimal(MainApplication.getInstance().getUsdToRmbExchangeRate());
                        paseBigDecimal = bigSurcharge.add(moneyDecimal).multiply(sourceToUsd).setScale(MainApplication.getInstance().getNumFixed(), BigDecimal.ROUND_HALF_UP)
                                .multiply(usdRate).setScale(MainApplication.getInstance().getNumFixed(), BigDecimal.ROUND_DOWN);
                    } else {
                        BigDecimal rate = new BigDecimal(MainApplication.getInstance().getExchangeRate());
                        paseBigDecimal = bigSurcharge.add(moneyDecimal).multiply(rate).setScale(MainApplication.getInstance().getNumFixed(), BigDecimal.ROUND_FLOOR);
                    }
                    String totalStr = MainApplication.getInstance().getFeeFh() + DateUtil.formatPaseMoney(moneyDecimal);
                    String surchargeFee = MainApplication.getInstance().getFeeFh() + DateUtil.formatPaseMoney(bigSurcharge);
                    tv_amount.setText(totalStr);
                    tv_surcharge_money.setText(surchargeFee);
                }
            }
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
    }

    //生成二维码的view
    public void GenerateQRCodeView(final String uuId, final String cashierUrl, boolean isLiquidType, boolean isInstapayQRPayment, boolean isMasterCardQR, WalletListBean walletBean) {
        // 生成二维码
        try {
            Object object = SharedPreUtils.readProduct("payTypeNameMap" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            if (object != null) {
                Map<String, DynModel> payTypeNameMap = (Map<String, DynModel>) object;
                //根据当前是否是masterCard去获取model
                DynModel dynModel = isMasterCardQR ? payTypeNameMap.get(masterCardInfo.getApiCode()) : payTypeNameMap.get(qrcodeInfo.payType);

                if (null != dynModel && !StringUtil.isEmptyOrNull(dynModel.getProviderName())) {
                    try {
                        String language = LocaleUtils.getLocaleLanguage();
                        if (language.equalsIgnoreCase(Constant.LANG_CODE_ZH_CN)) {
                            if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                                if (isLiquidType && walletBean != null) {
                                    titleBar.setTitle(walletBean.getName() + getString(R.string.choose_pre_auth));
                                    tv_payment_channel.setText(walletBean.getName() + getString(R.string.choose_pre_auth) + ",");
                                } else {
                                    if (isMasterCardQR) {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()) + getString(R.string.choose_pre_auth));
                                        tv_payment_channel.setText(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()) + getString(R.string.choose_pre_auth) + ",");
                                    } else {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType) + getString(R.string.choose_pre_auth));
                                        tv_payment_channel.setText(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType) + getString(R.string.choose_pre_auth) + ",");
                                    }

                                }
                            } else {
                                if (isLiquidType && walletBean != null) {
                                    titleBar.setTitle(walletBean.getName());
                                    tv_payment_channel.setText(walletBean.getName() + ",");
                                } else {
                                    if (isMasterCardQR) {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()));
                                        tv_payment_channel.setText(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()) + ",");
                                    } else {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType));
                                        tv_payment_channel.setText(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType) + ",");
                                    }
                                }
                            }

                        } else if (language.equalsIgnoreCase(Constant.LANG_CODE_ZH_TW)) {
                            if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                                if (isLiquidType && walletBean != null) {
                                    titleBar.setTitle(walletBean.getName() + getString(R.string.choose_pre_auth));
                                    tv_payment_channel.setText(walletBean.getName() + getString(R.string.choose_pre_auth) + ",");
                                } else {
                                    if (isMasterCardQR) {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()) + getString(R.string.choose_pre_auth));
                                        tv_payment_channel.setText(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()) + getString(R.string.choose_pre_auth) + ",");
                                    } else {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType) + getString(R.string.choose_pre_auth));
                                        tv_payment_channel.setText(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType) + getString(R.string.choose_pre_auth) + ",");
                                    }
                                }

                            } else {
                                if (isLiquidType && walletBean != null) {
                                    titleBar.setTitle(walletBean.getName());
                                    tv_payment_channel.setText(walletBean.getName() + ",");
                                } else {
                                    if (isMasterCardQR) {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()));
                                        tv_payment_channel.setText(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()) + ",");
                                    } else {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType));
                                        tv_payment_channel.setText(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType) + ",");
                                    }
                                }
                            }
                        } else {
                            if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {

                                if (isLiquidType && walletBean != null) {
                                    titleBar.setTitle(walletBean.getName() + " " + getString(R.string.choose_pre_auth));
                                    tv_payment_channel.setText(walletBean.getName() + " " + getString(R.string.choose_pre_auth) + ",");
                                } else {
                                    if (isMasterCardQR) {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()) + " " + getString(R.string.choose_pre_auth));
                                        tv_payment_channel.setText(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()) + " " + getString(R.string.choose_pre_auth) + ",");
                                    } else {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType) + " " + getString(R.string.choose_pre_auth));
                                        tv_payment_channel.setText(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType) + " " + getString(R.string.choose_pre_auth) + ",");
                                    }
                                }
                            } else {
                                if (isLiquidType && walletBean != null) {
                                    titleBar.setTitle(walletBean.getName());
                                    tv_payment_channel.setText(walletBean.getName() + ",");
                                } else {
                                    if (isMasterCardQR) {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()));
                                        tv_payment_channel.setText(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()) + ",");
                                    } else {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType));
                                        tv_payment_channel.setText(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType) + ",");
                                    }
                                }
                            }
                        }

                    } catch (Exception e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                    }
                }
            }

            if (isLiquidType && walletBean != null) {
                if (!TextUtils.isEmpty(walletBean.getImageUrl())) {
                    if (getInstance() != null) {
                        Glide.with(getInstance())
                                .load(walletBean.getImageUrl())
                                .placeholder(R.drawable.icon_general_receivables)
                                .error(R.drawable.icon_general_receivables)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .into(iv_payment_channel_small_icon);
                    }
                } else {
                    iv_payment_channel_small_icon.setImageResource(R.drawable.icon_general_receivables);
                }
            } else {
                Object object_icon = SharedPreUtils.readProduct("payTypeIcon" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
                if (object_icon != null) {
                    try {
                        Map<String, String> typePicMap = (Map<String, String>) object_icon;
                        if (typePicMap != null && typePicMap.size() > 0) {
                            String picUrl = isMasterCardQR ? typePicMap.get(masterCardInfo.getApiCode()) : typePicMap.get(qrcodeInfo.payType);
                            if (!StringUtil.isEmptyOrNull(picUrl)) {
                                if (getInstance() != null) {
                                    Glide.with(getInstance())
                                            .load(picUrl)
                                            .placeholder(R.drawable.icon_general_receivables)
                                            .error(R.drawable.icon_general_receivables)
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .into(iv_payment_channel_small_icon);
                                }
                            } else {
                                iv_payment_channel_small_icon.setImageResource(R.drawable.icon_general_receivables);
                            }
                        }
                    } catch (Exception e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                    }
                }
            }

            MasterCardQrCodeActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int w = DisplayUtil.dip2Px(MasterCardQrCodeActivity.this, 280);
                    int h = DisplayUtil.dip2Px(MasterCardQrCodeActivity.this, 280);
                    float scale = MasterCardQrCodeActivity.this.getResources().getDisplayMetrics().density;
                    if (scale == 1.0f) {
                        w = DisplayUtil.dip2Px(MasterCardQrCodeActivity.this, 380);
                        h = DisplayUtil.dip2Px(MasterCardQrCodeActivity.this, 380);
                    }

                    if (isInstapayQRPayment && BuildConfig.bankCode.contains("aub")) {//如果是instapay的QRPayment的通道，则在中间添加一个logo
                        try {
                            Bitmap firstBitmap;
                            Bitmap bitmap_logo = BitmapFactory.decodeResource(getInstance().getResources(), R.drawable.logo_instapay_white);
                            firstBitmap = MaxCardManager.getInstance().createQRCodeWithLogo(true, false, MasterCardQrCodeActivity.this, uuId, w, h, bitmap_logo);
                            iv_qr_code_img.setImageBitmap(firstBitmap);
                        } catch (WriterException e) {
                            SentryUtils.INSTANCE.uploadTryCatchException(
                                    e,
                                    SentryUtils.INSTANCE.getClassNameAndMethodName()
                            );
                        }
                    } else if (isMasterCardQR) {//如果是MasterCard的支付通道
                        Bitmap bitmap;
                        try {
                            bitmap = MaxCardManager.getInstance().create2DCode(cashierUrl, w, h);
                            iv_qr_code_img.setImageBitmap(bitmap);
                        } catch (WriterException e) {
                            SentryUtils.INSTANCE.uploadTryCatchException(
                                    e,
                                    SentryUtils.INSTANCE.getClassNameAndMethodName()
                            );
                        }
                    } else {
                        Bitmap bitmap;
                        try {
                            bitmap = MaxCardManager.getInstance().create2DCode(uuId, w, h);
                            iv_qr_code_img.setImageBitmap(bitmap);
                        } catch (WriterException e) {
                            SentryUtils.INSTANCE.uploadTryCatchException(
                                    e,
                                    SentryUtils.INSTANCE.getClassNameAndMethodName()
                            );
                        }
                    }
                }
            });

            Countdown();

        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
    }


    @Override
    public void unifiedNativePaySuccess(QRcodeInfo response, String type, boolean isInstapay, boolean isSecond, boolean isInstapayQRPayment, WalletListBean walletBean) {

        if (response == null) {
            return;
        }

        //只有普通的非MasterCard正扫下单成功，这个对象才有值
        qrcodeInfo = new QRcodeInfo();
        qrcodeInfo.outAuthNo = response.outAuthNo;
        qrcodeInfo.orderNo = response.orderNo;
        qrcodeInfo.payType = type;
        qrcodeInfo.setInvoiceId(response.getInvoiceId());
        qrcodeInfo.setExpirationDate(response.getExpirationDate());

        if (isInstapay) {//如果是instapay下单，则直接跳转到instapay的页面
            InstapayInfoActivity.startActivity(MasterCardQrCodeActivity.this, qrcodeInfo);

            //停止查单，结束当前界面
            if (timeCount != null) {
                timeCount.cancel();
            }
            finish();

        } else {//否则还是按照以前的逻辑，生成固码----instapayQRPayment也是按照原来的逻辑生成固码，只是中间要加一个logo
            GenerateQRCodeView(response.uuId, masterCardInfo.getCashierUrl(), isSecond, isInstapayQRPayment, false, walletBean);
        }
    }

    @Override
    public void unifiedNativePayFailed(Object error) {
        if (checkSession()) {
            return;
        }

        if (error != null) {
            MasterCardQrCodeActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    toastDialog(MasterCardQrCodeActivity.this, error.toString(), null);
                }
            });
        }

        MasterCardQrCodeActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //如果生成码的过程失败了，则还是重新开始查单，查的还是上一次的单
                Countdown();
            }
        });
    }

    //选择新的支付方式，正扫下单
    private void toNativePay(final String money, final String type, final String tradeType, final String note, final boolean isSecond,
                             boolean isInstapay, boolean isInstapayQRPayment,
                             final WalletListBean walletBean) {
        if (mPresenter != null) {
            mPresenter.unifiedNativePay(money, type, tradeType, 0, null, note, isInstapay, isSecond, isInstapayQRPayment, walletBean);
        }
    }


    @Override
    public void masterCardNativePaySuccess(MasterCardInfo response, String apiCode) {
        //masterCard下单完成，之前下单的qrcodeInfo不保存
        masterCardInfo = response;
        masterCardInfo.setApiCode(apiCode);
        qrcodeInfo = null;

        //下单成功，直接生成二维码
        GenerateQRCodeView(null, masterCardInfo.getCashierUrl(), false, false, true, null);

    }

    @Override
    public void masterCardNativePayFailed(Object error) {
        if (checkSession()) {
            return;
        }

        if (error != null && !TextUtils.isEmpty(error.toString())) {
            MasterCardQrCodeActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    toastDialog(MasterCardQrCodeActivity.this, error.toString(), null);
                }
            });
        } else {
            toastDialog(MasterCardQrCodeActivity.this, getStringById(R.string.generate_code_failed), null);
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //如果生成码的过程失败了，则还是重新开始查单，查的还是上一次的单
                Countdown();
            }
        });
    }

    /* switch收银台下单*/
    public void toMasterCardNewPay(String money, String apiCode) {
        if (mPresenter != null) {
            mPresenter.masterCardNativePay(money, apiCode, mark);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (timeCount != null) {
            timeCount.cancel();
        }

        if (dialogInfo != null) {
            dialogInfo.dismiss();
        }
    }

    //轮询查单的逻辑
    private void Countdown() {
        // 总共2分钟 间隔2秒
        timeCount = new TimeCount(1000 * 60 * 2, 2000);
        timeCount.start();
    }


    @Override
    public void queryMasterCardOrderByOrderNoSuccess(Order response) {
        if (response == null) {
            return;
        }

        //2：支付成功
        if (response.getTradeState() == 2) {
            //停止查询
            if (timeCount != null) {
                timeCount.cancel();
            }
            //停止查询的时候把这个字段手动置空，不查询
            masterCardInfo.setOutTradeNo(null);

            //清掉备注
            NoteMarkActivity.setNoteMark("");
            //跳转到成功页面
            MasterCardPayResultActivity.startActivity(MasterCardQrCodeActivity.this, response);
            //finish当前页面
            finish();
        }
    }

    @Override
    public void queryMasterCardOrderByOrderNoFailed(Object error) {

    }

    public void queryMasterCardOrderStatus(String service, String outTradeNo) {
        if (mPresenter != null) {
            mPresenter.queryMasterCardOrderByOrderNo(service, outTradeNo);
        }
    }


    @Override
    public void queryOrderByOrderNoSuccess(Order response) {
        if (response == null) {
            return;
        }

        if (response.state.equals("2")) {
            if (timeCount != null) {
                timeCount.cancel();
            }
            qrcodeInfo.orderNo = null;
            qrcodeInfo.outAuthNo = null;

            NoteMarkActivity.setNoteMark("");

            if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                PreAutFinishActivity.startActivity(MasterCardQrCodeActivity.this, response, 1);
            } else {
                PayResultActivity.startActivity(MasterCardQrCodeActivity.this, response);
            }
            finish();
        }
    }

    @Override
    public void queryOrderByOrderNoFailed(Object error) {

    }

    //切换到非MasterCard的查单
    private void queryOrderGetStuts() {
        String orderNoStr;
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth", "sale");
        if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(saleOrPreauth, "pre_auth")) {
            orderNoStr = qrcodeInfo.outAuthNo;
        } else {
            orderNoStr = qrcodeInfo.orderNo;
        }

        if (mPresenter != null) {
            mPresenter.queryOrderByOrderNo(orderNoStr);
        }
    }

    //通过ApiCode得到当前支付类型的NativePayType
    public String getNativePayType(String ApiCode) {
        //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth", "sale");
        if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(saleOrPreauth, "pre_auth")) {
            Object object = SharedPreUtils.readProduct("dynPayType_pre_auth" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            if (object != null) {
                List<DynModel> list = (List<DynModel>) object;
                if (null != list && list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getApiCode().equalsIgnoreCase(ApiCode)) {
                            return list.get(i).getNativeTradeType();
                        }
                    }
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            Object object = SharedPreUtils.readProduct("ActiveDynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            Object object_CardPayment = SharedPreUtils.readProduct("cardPayment_ActiveDynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());

            List<DynModel> list = new ArrayList<>();
            List<DynModel> list_CardPayment = new ArrayList<>();
            if (object != null) {
                list = (List<DynModel>) object;
            }
            if (object_CardPayment != null) {
                list_CardPayment = (List<DynModel>) object_CardPayment;
            }

            //把两个数组拼接起来
            List<DynModel> listAll = new ArrayList<>();
            if (list != null) {
                listAll.addAll(list);
            }
            if (list_CardPayment != null) {
                listAll.addAll(list_CardPayment);
            }

            if (null != listAll && listAll.size() > 0) {
                for (int i = 0; i < listAll.size(); i++) {
                    if (listAll.get(i).getApiCode().equalsIgnoreCase(ApiCode)) {
                        return listAll.get(i).getNativeTradeType();
                    }
                }
            } else {
                return null;
            }
        }
        return null;
    }

    //V3.1.0 版本新增MasterCard交易通道
    //判断当前的交易类型是否是卡交易类型
    //如果是MasterCard交易通道则走新的下单和查单的逻辑
    public boolean isMasterCardServiceType(String NativePayType) {
        if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getUserInfo().cardServiceType)) {
            String[] arrPays = MainApplication.getInstance().getUserInfo().cardServiceType.split("\\|");
            for (int i = 0; i < arrPays.length; i++) {
                if (NativePayType.equalsIgnoreCase(arrPays[i])) {
                    return true;
                }
            }
        }
        return false;
    }

    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }

        @Override
        public void onFinish() {//计时完毕时触发
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String mmsg = getString(R.string.show_request_outtime);
                    try {
                        dialogInfo = new DialogInfo(MasterCardQrCodeActivity.this,
                                getStringById(R.string.public_cozy_prompt), mmsg,
                                getStringById(R.string.btnOk), DialogInfo.REGISTFLAG,
                                null, null);
                        DialogHelper.resize(MasterCardQrCodeActivity.this, dialogInfo);
                        dialogInfo.setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {
                                if (keycode == KeyEvent.KEYCODE_BACK) {
                                    return true;
                                }
                                return false;
                            }
                        });
                        if (dialogInfo != null) {
                            dialogInfo.show();
                        }
                        mHandler.post(myRunnable);
                    } catch (Exception e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                    }
                }
            });
        }

        @Override
        public void onTick(long millisUntilFinished) {//计时过程每隔 间隔时间触发一次
            //每次2S 查询一次订单状态，查询前判断是否有值
            //要做个判断，是查询masterCard的单还是非masterCard的单
            //如果是MasterCard下单,则去查MasterCard的单
            if (masterCardInfo != null && qrcodeInfo == null) {
                if (!TextUtils.isEmpty(masterCardInfo.getOutTradeNo())) {
                    queryMasterCardOrderStatus(masterCardInfo.getService(), masterCardInfo.getOutTradeNo());
                }
            } else if (qrcodeInfo != null) {//只有普通的下单成功，这个qrcodeInfo才有值，则轮询查单
                String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth", "sale");
                if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(saleOrPreauth, "pre_auth")) {
                    if (null != qrcodeInfo.outAuthNo && !"".equals(qrcodeInfo.outAuthNo)) {
                        queryOrderGetStuts();
                    }
                } else {
                    if (null != qrcodeInfo.orderNo && !"".equals(qrcodeInfo.orderNo)) {
                        queryOrderGetStuts();
                    }
                }
            }
        }
    }


}
