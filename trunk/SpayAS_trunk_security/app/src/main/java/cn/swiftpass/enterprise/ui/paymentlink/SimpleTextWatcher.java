package cn.swiftpass.enterprise.ui.paymentlink;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by congwei.li on 2021/9/9.
 *
 * @Description:
 */
public class SimpleTextWatcher implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
