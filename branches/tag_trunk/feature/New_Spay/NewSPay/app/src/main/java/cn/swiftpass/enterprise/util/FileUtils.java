package cn.swiftpass.enterprise.util;

/**
 * Created with IntelliJ IDEA.
 * User: ALAN
 * Date: 13-9-20
 * Time: 下午7:39
 * To change this template use File | Settings | File Templates.
 */

import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileUtils {
    private static final String TAG = FileUtils.class.getSimpleName();

    /**
     * Description: 通过文件长度获取文件大小字符串（如xxMB）
     *
     * @param fileLength
     * @return
     */
    public static String getSizeStr(long fileLength) {
        String strSize = "";
        try {
            if (fileLength >= 1024 * 1024 * 1024) {
                strSize = (float) Math.round(10 * fileLength / (1024 * 1024 * 1024)) / 10 + "G";
            } else if (fileLength >= 1024 * 1024) {
                strSize = (float) Math.round(10 * fileLength / (1024 * 1024 * 1.0)) / 10 + "M";
            } else if (fileLength >= 1024) {
                strSize = (float) Math.round(10 * fileLength / (1024)) / 10 + "K";
            } else if (fileLength >= 0) {
                strSize = fileLength + "B";
            } else {
                strSize = "0B";
            }
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
            strSize = "0B";
        }
        return strSize;
    }

    /**
     * Description: 通过文件修改时间获取文件修改时间字符串（如xxMB）
     *
     * @return
     */
    public static String getModifyTimeStr(long modifyTime) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(new Date(modifyTime));
    }

    /**
     * Description: 删除文件
     *
     * @param fileName
     */
    public static void deleteFile(String fileName) {
        File file = new File(fileName);
        if (file.exists()) {
            boolean isSuccess = file.delete();
            Log.i(TAG,"delete file "+isSuccess);
        }
    }
    
 /*   *//**指定目录删除指定文件后缀*//*
    public static void deleteFileBySuffix(String path, String suffix)
    {
        File demifile = new File(getRootPath());
        if (demifile.isDirectory())
        {
            demifile.delete();
        }
        File file = new File(path);
        File tempFile = null;
        File[] files = file.listFiles();
        if (files == null)
        {
            return;
        }
        for (int i = 0; i < files.length; i++)
        {
            tempFile = files[i];
            if (tempFile.exists() && tempFile.getName().endsWith(suffix))
            {
                tempFile.delete();
            }
        }
    }*/

    /**
     * 返回跟目录
     */
    public static String getRootPath() {
        return Environment.getExternalStorageDirectory().getAbsolutePath() +"/swiftpass_pay/";
    }

    /**
     * 判断SDcard是否存在
     */
    public static boolean isSdcardExist() {
        return TextUtils.equals(Environment.getExternalStorageState(),android.os.Environment.MEDIA_MOUNTED);
    }

    /**
     * 如果SDcard存在，则返回SDcard上的普通文件的目录 否则，失败返回null
     */
    public static String makeDir(String dir) {
        if (!isSdcardExist()) {
            return null;
        }
        File file = new File(dir);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                return null;
            }
        }
        return dir;
    }

    public static File makeDirFile(String dir) {
        String dirPath = makeDir(dir);
        if (dirPath != null) {
            return new File(dirPath);
        } else {
            return null;
        }
    }

/*    public static void delFileOrDirectory(String filepath) throws java.io.IOException {
        File f = new File(filepath);//定义文件路径
        if (f.exists() && f.isDirectory()) {//判断是文件还是目录
            if (f.listFiles().length == 0) {//若目录下没有文件则直接删除
                f.delete();
            } else {//若有则把文件放进数组，并判断是否有下级目录
                File delFile[] = f.listFiles();
                int i = f.listFiles().length;
                for (int j = 0; j < i; j++) {
                    if (delFile[j].isDirectory()) {
                        delFileOrDirectory(delFile[j].getAbsolutePath());//递归调用del方法并取得子目录路径
                    }
                    delFile[j].delete();//删除文件
                }
            }
        }
    }*/

    public static String getAppFiledImg(long fileId) {
        return getRootPath() + "/download/cache/" + fileId;
    }

    public static String getAppFiled(String fileId) {
        return getRootPath() + "/download/cache/" + fileId;
    }

    public static String getAppCache() {
        return getRootPath() + "download/cache/";
    }

    public static String getAppPath() {
        //        return getRootPath() + "/download/app/";
        return defaultDownloadPath;
    }

    public static String getAppLog() {
        return getRootPath() + "/logs/";
    }

    /**
     * 下载地址
     */
    public static final String DOWNLOAD_URL = "http://jime-files.b0.upaiyun.com/v1/";

    /**
     * 默认下载存储路径
     */
    public static final String defaultDownloadPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/swiftpass_pay/";

    /**
     * 获取应用下log文件
     */
    public static File getLogFile(String logName) {
        String root = FileUtils.makeDir(FileUtils.getRootPath() + "/logs/");
        File file = new File(root + logName);
        return file;
    }
    
 /*   *//**
     * 读取文件
     *
     * @param file
     * @return
     * @throws IOException
     *//*
    public static String readTextFile(File file)
        throws IOException
    {
        String text = null;
        InputStream is = null;
        try
        {
            is = new FileInputStream(file);
            text = readTextInputStream(is);
        }
        finally
        {
            closeIO(is);
        }
        return text;
    }*/
    
/*    *//**
     * 从流中读取文件
     *
     * @param is
     * @return
     * @throws IOException
     *//*
    public static String readTextInputStream(InputStream is)
        throws IOException
    {
        StringBuffer strbuffer = new StringBuffer();
        String line;
        BufferedReader reader = null;
        try
        {
            reader = new BufferedReader(new InputStreamReader(is));
            while ((line = reader.readLine()) != null)
            {
                strbuffer.append(line).append("\r\n");
            }
        }
        finally
        {
            closeIO(reader);
        }
        return strbuffer.toString();
    }*/
    
   /* *//**
     * 将文本内容写入文件
     * @param file
     * @param str
     * @throws IOException
     *//*
    public static void writeTextFile(File file, String str)
        throws IOException
    {
        DataOutputStream out = null;
        try
        {
            out = new DataOutputStream(new FileOutputStream(file));
            out.write(str.getBytes());
        }
        finally
        {
            closeIO(out);
        }
    }*/

    /**
     * 关闭流,普通不严重的可采用此函数关闭
     */
    public static void closeIO(Closeable... closeables) {
        if (null == closeables || closeables.length <= 0) {
            return;
        }
        for (Closeable cb : closeables) {
            try {
                if (null == cb) {
                    continue;
                }
                cb.close();
            } catch (Exception e) {
                Log.e(TAG, Log.getStackTraceString(e));
            }
        }
    }
}
