package cn.swiftpass.enterprise.io.okhttp;

import android.text.TextUtils;

import com.example.common.sentry.SentryUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants;
import cn.swiftpass.enterprise.mvp.param.HttpParams;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.KotlinUtils;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.MD5;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;
import okio.BufferedSink;
import okio.ForwardingSink;
import okio.Okio;
import okio.Sink;

public class RequestUtil {

    /**
     * 超时时间 s
     */
    public static final int CONNECT_TIME_OUT = 30;
    public static final int READ_TIME_OUT = 30;
    public static final int WRITE_TIME_OUT = 30;


    /**
     * NNS方式
     */
    public static final int NNS_NONE = -1;
    public static final int NNS_1 = 1101;
    public static final int NNS_2 = 1102;
    public static final int NNS_3 = 1103;
    public static final int NNS_4 = 1104;


    private String mMethodType;//请求方式，目前只支持get和post
    private String mUrl;//接口
    private Map<String, String> mParamsMap;//键值对类型的参数，只有这一种情况下区分post和get。
    private String mJsonStr;//json类型的参数，post方式
    private File mFile;//文件的参数，post方式,只有一个文件
    private List<File> mFileList;//文件集合，这个集合对应一个key，即mfileKey
    private String mFileKey;//上传服务器的文件对应的key
    private Map<String, File> mFileMap;//文件集合，每个文件对应一个key
    private String mFileType;//文件类型的参数，与file同时存在
    private Map<String, String> mHeaderMap;//头参数
    private CallBackUtil mCallBack;//回调接口
    private OkHttpClient mOkHttpClient;//OKhttpClient对象
    private Request mOkHttpRequest;//请求对象
    private Request.Builder mRequestBuilder;//请求对象的构建者
    private boolean mIsNeedCookie = true; //是否设置cookie

    private boolean mIsNeedVerifyCerts = true; //是否验证证书


    public RequestUtil(HttpParams httpParams) {
        this(
                httpParams.getMIsNeedVerifyCerts(),
                httpParams.getMMethodType(),
                httpParams.getMUrl(),
                httpParams.getMJsonStr(),
                null,
                null,
                null,
                null,
                null,
                httpParams.getMParamsMap(),
                OkhttpUtil.getHeadParam(httpParams.getMSpayRs(), httpParams.getMIsNeedSpayId(), httpParams.getMIsNeedCook())
        );
    }


//    RequestUtil(String methodType, String url, Map<String, String> paramsMap,
//                Map<String, String> headerMap) {
//        this(methodType, url, null, null, null, null, null,
//                null, paramsMap, headerMap);
//    }


    RequestUtil(String methodType, String url, Map<String, String> paramsMap,
                Map<String, String> headerMap, CallBackUtil callBack) {
        this(methodType, url, null, null, null, null, null,
                null, paramsMap, headerMap, callBack);
    }

    RequestUtil(String methodType, String url, String jsonStr, Map<String, String> headerMap,
                CallBackUtil callBack) {
        this(methodType, url, jsonStr, null, null, null, null, null,
                null, headerMap, callBack);
    }

    RequestUtil(String methodType, String url, String jsonStr, Map<String, String> headerMap,
                boolean isNeedCookie, CallBackUtil callBack) {
        this(methodType, url, jsonStr, null, null, null, null, null,
                null, headerMap, callBack, isNeedCookie);
    }

    RequestUtil(String methodType, String url, Map<String, String> paramsMap, File file,
                String fileKey, String fileType, Map<String, String> headerMap,
                CallBackUtil callBack) {
        this(methodType, url, null, file, null, fileKey, null, fileType,
                paramsMap, headerMap, callBack);
    }

    RequestUtil(String methodType, String url, Map<String, String> paramsMap, List<File> fileList,
                String fileKey, String fileType, Map<String, String> headerMap,
                CallBackUtil callBack) {
        this(methodType, url, null, null, fileList, fileKey, null,
                fileType, paramsMap, headerMap, callBack);
    }

    RequestUtil(String methodType, String url, Map<String, String> paramsMap, Map<String,
            File> fileMap, String fileType, Map<String, String> headerMap,
                CallBackUtil callBack) {
        this(methodType, url, null, null, null, null, fileMap,
                fileType, paramsMap, headerMap, callBack);
    }


    private RequestUtil(
            boolean isNeedVerifyCerts,
            String methodType,
            String url,
            String jsonStr,
            File file,
            List<File> fileList,
            String fileKey,
            Map<String, File> fileMap,
            String fileType,
            Map<String, String> paramsMap,
            Map<String, String> headerMap,
            boolean setCookie
    ) {
        mIsNeedVerifyCerts = isNeedVerifyCerts;
        mMethodType = methodType;
        mUrl = url;
        if (!TextUtils.isEmpty(jsonStr)) {
            mJsonStr = jsonStr;
        } else {
            mJsonStr = null;
        }
        mFile = file;
        mFileList = fileList;
        mFileKey = fileKey;
        mFileMap = fileMap;
        mFileType = fileType;
        if (paramsMap != null && paramsMap.size() > 0) {
            mParamsMap = paramsMap;
        } else {
            mParamsMap = null;
        }
        mHeaderMap = headerMap;
        mIsNeedCookie = setCookie;
    }

    private RequestUtil(String methodType, String url, String jsonStr, File file,
                        List<File> fileList, String fileKey, Map<String, File> fileMap,
                        String fileType, Map<String, String> paramsMap,
                        Map<String, String> headerMap, CallBackUtil callBack, boolean setCookie) {
        mMethodType = methodType;
        mUrl = url;
        mJsonStr = jsonStr;
        mFile = file;
        mFileList = fileList;
        mFileKey = fileKey;
        mFileMap = fileMap;
        mFileType = fileType;
        mParamsMap = paramsMap;
        mHeaderMap = headerMap;
        mCallBack = callBack;
        mIsNeedCookie = setCookie;
        getInstance();

    }


    private RequestUtil(
            boolean isNeedVerifyCerts,
            String methodType,
            String url,
            String jsonStr,
            File file,
            List<File> fileList,
            String fileKey,
            Map<String, File> fileMap,
            String fileType,
            Map<String, String> paramsMap,
            Map<String, String> headerMap
    ) {
        this(
                isNeedVerifyCerts,
                methodType,
                url,
                jsonStr,
                file,
                fileList,
                fileKey,
                fileMap,
                fileType,
                paramsMap,
                headerMap,
                true
        );
    }


    private RequestUtil(String methodType, String url, String jsonStr, File file,
                        List<File> fileList, String fileKey, Map<String, File> fileMap,
                        String fileType, Map<String, String> paramsMap,
                        Map<String, String> headerMap, CallBackUtil callBack) {
        this(methodType, url, jsonStr, file, fileList, fileKey, fileMap, fileType, paramsMap,
                headerMap, callBack, true);
    }

    private static String bodyToString(final Request request) {

        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "error";
        }
    }

    //工具方法，从Body读取内容成String
    private static String readContentFromRequestBody(RequestBody body) {
        Buffer buffer = null;
        try {
            if (body != null && body.contentLength() > 0) {
                buffer = new Buffer();
                body.writeTo(buffer);
                return buffer.readString(StandardCharsets.UTF_8);
            }
        } catch (IOException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        } finally {
            if (buffer != null) {
                buffer.close();
            }
        }
        return "";
    }


    private void addNNS(int nnsType) {
        String priKey = (String) SharedPreUtils.readProduct(ParamsConstants.SECRET_KEY);
        switch (nnsType) {
            case NNS_1:
                mParamsMap.put(ParamsConstants.NNS,
                        SignUtil.getInstance().createSign(
                                mParamsMap, MD5.md5s(priKey).substring(0, 16))
                );
                break;
            case NNS_2:
                if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getSKey())) {
                    mParamsMap.put(
                            ParamsConstants.NNS,
                            SignUtil.getInstance().createSign(
                                    mParamsMap, MD5.md5s(priKey).substring(0, 16)
                            )
                    );
                } else {
                    mParamsMap.put(
                            ParamsConstants.NNS, SignUtil.getInstance().createSign(
                                    mParamsMap,
                                    MD5.md5s(mParamsMap.get(ParamsConstants.SPAY_RS))
                            )
                    );
                }
                break;
            case NNS_3:
                mParamsMap.put(ParamsConstants.NNS,
                        SignUtil.getInstance().createSign(
                                mParamsMap, MD5.md5s(mParamsMap.get(ParamsConstants.SPAY_RS))
                        ));
                break;
            case NNS_4:
                mParamsMap.put(ParamsConstants.NNS,
                        SignUtil.getInstance().createSign(mParamsMap, MainApplication.getInstance().getNewSignKey()));
                break;
        }
    }


    public RequestUtil addCallBack(CallBackUtil callBack, int nnsType) {
        if (TextUtils.isEmpty(mJsonStr)) {
            addNNS(nnsType);
        }
        mCallBack = callBack;
        getInstance();
        return this;
    }

    /**
     * 创建OKhttpClient实例。
     */
    private void getInstance() {
        if (mIsNeedVerifyCerts) {
            //需要验证证书
            mOkHttpClient = getOkHttpClientWithCerts();
        } else {
            //无需验证证书
            mOkHttpClient = getOkHttpClientWithoutCerts();
        }
        mRequestBuilder = new Request.Builder();
        if (mFile != null || mFileList != null || mFileMap != null) {//先判断是否有文件，
            setFile();
        } else {
            //设置参数
            switch (mMethodType) {
                case OkhttpUtil.METHOD_GET:
                    setGetParams();
                    break;
                case OkhttpUtil.METHOD_POST:
                    mRequestBuilder.post(getRequestBody());
                    break;
                case OkhttpUtil.METHOD_PUT:
                    mRequestBuilder.put(getRequestBody());
                    break;
                case OkhttpUtil.METHOD_DELETE:
                    mRequestBuilder.delete(getRequestBody());
                    break;
            }
        }
        mRequestBuilder.url(mUrl);
        if (mHeaderMap != null) {
            setHeader();
        }
        //mRequestBuilder.addHeader("Authorization","Bearer "+"token");可以把token添加到这儿
        mOkHttpRequest = mRequestBuilder.build();
        //打印
        displayRequestLog();
    }


    private void displayRequestLog() {
        Logger.logD("----------------------------------------------------------------------------------------------------------------------------");
        Logger.logD("| 打印请求内容");
        Logger.logD("| url: " + mUrl);
        Logger.logD("| ======header: \n");
        displayHeaders();
        Logger.logD("| ======param: \n");
        displayParams();
        Logger.logD("| ======jsonStr: \n");
        Logger.logD("| " + mJsonStr + "\n");
        Logger.logD("----------------------------------------------------------------------------------------------------------------------------");
    }


    private void displayHeaders() {
        if (mHeaderMap != null && mHeaderMap.size() > 0) {
            for (String key : mHeaderMap.keySet()) {
                Logger.logD("| " + key + ": " + mHeaderMap.get(key) + "\n");
            }
        }
    }


    private void displayParams() {
        if (mParamsMap != null && mParamsMap.size() > 0) {
            for (String key : mParamsMap.keySet()) {
                Logger.logD("| " + key + ": " + mParamsMap.get(key) + "\n");
            }
        }
    }


    /**
     * 证书验证
     *
     * @return
     */
    public OkHttpClient getOkHttpClientWithCerts() {
        X509TrustManager trustManager;
        SSLSocketFactory sslSocketFactory;
        try {
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null);

            ArrayList<Certificate> localCerts = KotlinUtils.INSTANCE.getCertificates();
            for (int i = 0; i < localCerts.size(); i++) {
                keyStore.setCertificateEntry("cert_alias_" + i, localCerts.get(i));
            }

            SSLContext sslContext = SSLContext.getInstance("TLS");
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(keyStore);
            sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
            sslSocketFactory = sslContext.getSocketFactory();

            trustManager = new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            };


        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            throw new RuntimeException(e);
        }

        HostnameVerifier hostnameVerifier = new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
        if (mIsNeedCookie) {
            return new OkHttpClient.Builder()
                    .connectTimeout(CONNECT_TIME_OUT, TimeUnit.SECONDS)
                    .readTimeout(READ_TIME_OUT, TimeUnit.SECONDS)
                    .writeTimeout(WRITE_TIME_OUT, TimeUnit.SECONDS)
                    .sslSocketFactory(sslSocketFactory, trustManager)
                    .cookieJar(new CookieJarManager())
                    .hostnameVerifier(hostnameVerifier)
                    .build();
        } else {
            return new OkHttpClient.Builder()
                    .connectTimeout(CONNECT_TIME_OUT, TimeUnit.SECONDS)
                    .readTimeout(READ_TIME_OUT, TimeUnit.SECONDS)
                    .writeTimeout(WRITE_TIME_OUT, TimeUnit.SECONDS)
                    .sslSocketFactory(sslSocketFactory, trustManager)
                    .hostnameVerifier(hostnameVerifier)
                    .build();
        }
    }


    /**
     * 忽略证书验证
     *
     * @return
     */
    private OkHttpClient getOkHttpClientWithoutCerts() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        try {
            final X509TrustManager trustManager = new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType) {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) {
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    X509Certificate[] x509Certificates = new X509Certificate[0];
                    return x509Certificates;
                }
            };
            SSLContext sslContext = null;
            sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, new TrustManager[]{trustManager}, new SecureRandom());


            builder.sslSocketFactory(sslContext.getSocketFactory(), trustManager);
        } catch (Exception e) {
            e.printStackTrace();
        }
        HostnameVerifier hostnameVerifier = new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
        builder.hostnameVerifier(hostnameVerifier);

        builder.connectTimeout(CONNECT_TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIME_OUT, TimeUnit.SECONDS);

        if (mIsNeedCookie) {
            return builder
                    .cookieJar(new CookieJarManager())
                    .build();
        } else {
            return builder
                    .build();
        }
    }


    /**
     * 得到body对象
     */
    private RequestBody getRequestBody() {

        FormBody.Builder formBody = new FormBody.Builder();

        if (!TextUtils.isEmpty(mJsonStr)) {
            /**
             * 首先判断mJsonStr是否为空，由于mJsonStr与mParamsMap不可能同时存在，所以先判断mJsonStr
             */
            formBody.add("data", mJsonStr);
        } else {
            /**
             * post,put,delete都需要body，但也都有body等于空的情况，此时也应该有body对象，但body中的内容为空
             *
             */
            formBody.add("data", JsonUtil.mapToJson(mParamsMap));
        }

        return formBody.build();
    }

    /**
     * get请求，只有键值对参数
     */
    private void setGetParams() {
        if (mParamsMap != null) {
            mUrl = mUrl + "?";
            for (String key : mParamsMap.keySet()) {
                mUrl = mUrl + key + "=" + mParamsMap.get(key) + "&";
            }
            mUrl = mUrl.substring(0, mUrl.length() - 1);
        }
    }

    /**
     * 设置上传文件
     */
    private void setFile() {
        if (mFile != null) {//只有一个文件，且没有文件名
            if (mParamsMap == null) {
                setPostFile();
            } else {
                setPostParamAndFile();
            }
        } else if (mFileList != null) {//文件集合，只有一个文件名。所以这个也支持单个有文件名的文件
            setPostParamAndListFile();
        } else if (mFileMap != null) {//多个文件，每个文件对应一个文件名
            setPostParamAndMapFile();
        }

    }

    /**
     * 只有一个文件，且提交服务器时不用指定键，没有参数
     */
    private void setPostFile() {
        if (mFile != null && mFile.exists()) {
            MediaType fileType = MediaType.parse(mFileType);
            RequestBody body = RequestBody.create(fileType, mFile);//json数据，
            mRequestBuilder.post(new ProgressRequestBody(body, mCallBack));
        }
    }

    /**
     * 只有一个文件，且提交服务器时不用指定键，带键值对参数
     */
    private void setPostParamAndFile() {
        if (mParamsMap != null && mFile != null) {
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            for (String key : mParamsMap.keySet()) {
                builder.addFormDataPart(key, mParamsMap.get(key));
            }
            builder.addFormDataPart(mFileKey, mFile.getName(),
                    RequestBody.create(MediaType.parse(mFileType), mFile));
            mRequestBuilder.post(new ProgressRequestBody(builder.build(), mCallBack));
        }
    }

    /**
     * 文件集合，可能带有键值对参数
     */
    private void setPostParamAndListFile() {
        if (mFileList != null) {
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            if (mParamsMap != null) {
                for (String key : mParamsMap.keySet()) {
                    builder.addFormDataPart(key, mParamsMap.get(key));
                }
            }
            for (File f : mFileList) {
                builder.addFormDataPart(mFileKey, f.getName(),
                        RequestBody.create(MediaType.parse(mFileType), f));
            }
            mRequestBuilder.post(builder.build());
        }
    }

    /**
     * 文件Map，可能带有键值对参数
     */
    private void setPostParamAndMapFile() {
        if (mFileMap != null) {
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            if (mParamsMap != null) {
                for (String key : mParamsMap.keySet()) {
                    builder.addFormDataPart(key, mParamsMap.get(key));
                }
            }

            for (String key : mFileMap.keySet()) {
                builder.addFormDataPart(key, mFileMap.get(key).getName(),
                        RequestBody.create(MediaType.parse(mFileType), mFileMap.get(key)));
            }
            mRequestBuilder.post(builder.build());
        }
    }

    /**
     * 设置头参数
     */
    private void setHeader() {
        if (mHeaderMap != null) {
            for (String key : mHeaderMap.keySet()) {
                mRequestBuilder.addHeader(key, mHeaderMap.get(key));
            }
        }
    }

    public void execute() {
        mOkHttpClient.newCall(mOkHttpRequest).enqueue(new Callback() {
            @Override
            public void onFailure(final Call call, final IOException e) {
                if (mCallBack != null) {
                    mCallBack.onError(call, e);
                }
            }

            @Override
            public void onResponse(final Call call, final Response response) throws IOException {
                if (mCallBack != null) {
                    mCallBack.onSuccess(call, response, mUrl);
                }
            }

        });
    }

    /**
     * 自定义RequestBody类，得到文件上传的进度
     */
    private static class ProgressRequestBody extends RequestBody {
        //实际的待包装请求体
        private final RequestBody requestBody;
        //包装完成的BufferedSink
        private BufferedSink bufferedSink;
        private CallBackUtil callBack;

        ProgressRequestBody(RequestBody requestBody, CallBackUtil callBack) {
            this.requestBody = requestBody;
            this.callBack = callBack;
        }

        /**
         * 重写调用实际的响应体的contentType
         */
        @Override
        public MediaType contentType() {
            return requestBody.contentType();
        }

        /**
         * 重写调用实际的响应体的contentLength ，这个是文件的总字节数
         */
        @Override
        public long contentLength() throws IOException {
            return requestBody.contentLength();
        }

        /**
         * 重写进行写入
         */
        @Override
        public void writeTo(BufferedSink sink) throws IOException {
            if (bufferedSink == null) {
                bufferedSink = Okio.buffer(sink(sink));
            }
            requestBody.writeTo(bufferedSink);
            //必须调用flush，否则最后一部分数据可能不会被写入
            bufferedSink.flush();
        }

        /**
         * 写入，回调进度接口
         */
        private Sink sink(BufferedSink sink) {
            return new ForwardingSink(sink) {
                //当前写入字节数
                long bytesWritten = 0L;
                //总字节长度，避免多次调用contentLength()方法
                long contentLength = 0L;

                @Override
                public void write(Buffer source, long byteCount) throws IOException {
                    super.write(source, byteCount);//这个方法会循环调用，byteCount是每次调用上传的字节数。
                    if (contentLength == 0) {
                        //获得总字节长度
                        contentLength = contentLength();
                    }
                    //增加当前写入的字节数
                    bytesWritten += byteCount;
                    final float progress = bytesWritten * 1.0f / contentLength;
                    CallBackUtil.mMainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            callBack.onProgress(progress, contentLength);
                        }
                    });
                }
            };
        }
    }
}