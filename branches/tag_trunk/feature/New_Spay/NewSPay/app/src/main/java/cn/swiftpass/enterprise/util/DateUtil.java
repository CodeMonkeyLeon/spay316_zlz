/*******************************************************
 * @作者: huangdh
 * @日期: 2012-6-5
 * @描述: TODO
 * @声明: copyrights reserved by Petfone 2007-2011
 *******************************************************/
package cn.swiftpass.enterprise.util;

import android.annotation.SuppressLint;
import android.util.Log;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author : huangdh
 * @version :13-7-12
 * @Copyright : copyrights reserved by personal 20012-2013
 * @see : 日期工具栏
 */
@SuppressLint("SimpleDateFormat")
public class DateUtil {
    public static String formartDateToYYMMDD(String date)
            throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(df.parse(date));
    }

    private static final String TAG = "DateUtil";

    private static final String DATA_FORMAT = "yyyy-MM-dd";

    //将指定日期转化为秒数
    public static long getSecondsFromDate(String expireDate) {
        if (expireDate == null || expireDate.trim().equals(""))
            return 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = sdf.parse(expireDate);
            return date.getTime();
        } catch (ParseException e) {
            Log.e(TAG, Log.getStackTraceString(e));
            return 0;
        }
    }

    public static String[] paseTime(String time) {
        String[] times = new String[5];

        String[] a = time.split(" ");

        String[] c = a[0].split("/");
        //月，日
        times[0] = c[1];
        times[1] = c[2];
        if (time.contains(":")) {
            String[] d = a[1].split(":");
            times[2] = d[0];
            times[3] = d[1];
            times[4] = d[2];
        }

        return times;
    }

    //将指定日期转化为date
    public static Date getStrFromDate(String expireDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        try {
            return sdf.parse(expireDate);
        } catch (ParseException e) {
            Log.e(TAG, Log.getStackTraceString(e));
            return null;
        }
    }

    //将指定日期转化为date
    public static Date getStrToDate(String expireDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return sdf.parse(expireDate);
        } catch (ParseException e) {
            Log.e(TAG, Log.getStackTraceString(e));
            return null;
        }
    }

    //将指定日期转化为date
    public static Date getStrFromDateYYMMDD(String expireDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        try {
            return sdf.parse(expireDate);
        } catch (ParseException e) {
            Log.e(TAG, Log.getStackTraceString(e));
            return null;
        }
    }

    /**
     * 获取两个日期之间的间隔天数
     *
     * @return
     */
    public static int getGapCount(Date startDate, Date endDate) {
        Calendar fromCalendar = Calendar.getInstance();
        fromCalendar.setTime(startDate);
        fromCalendar.set(Calendar.HOUR_OF_DAY, 0);
        fromCalendar.set(Calendar.MINUTE, 0);
        fromCalendar.set(Calendar.SECOND, 0);
        fromCalendar.set(Calendar.MILLISECOND, 0);

        Calendar toCalendar = Calendar.getInstance();
        toCalendar.setTime(endDate);
        toCalendar.set(Calendar.HOUR_OF_DAY, 0);
        toCalendar.set(Calendar.MINUTE, 0);
        toCalendar.set(Calendar.SECOND, 0);
        toCalendar.set(Calendar.MILLISECOND, 0);

        return (int) ((toCalendar.getTime().getTime() - fromCalendar.getTime().getTime()) / (1000 * 60 * 60 * 24));
    }

    /**
     * 获取两个日期之间的间隔天数
     *
     * @return
     */
    public static int getGapCounts(Date startDate, Date endDate) {
        Calendar fromCalendar = Calendar.getInstance();
        fromCalendar.setTime(startDate);
        fromCalendar.set(Calendar.HOUR_OF_DAY, 0);
        //        fromCalendar.set(Calendar.MINUTE, 0);
        //        fromCalendar.set(Calendar.SECOND, 0);
        //        fromCalendar.set(Calendar.MILLISECOND, 0);

        Calendar toCalendar = Calendar.getInstance();
        toCalendar.setTime(endDate);
        toCalendar.set(Calendar.HOUR_OF_DAY, 0);
        //        toCalendar.set(Calendar.MINUTE, 0);
        //        toCalendar.set(Calendar.SECOND, 0);
        //        toCalendar.set(Calendar.MILLISECOND, 0);

        return (int) ((toCalendar.getTime().getTime() - fromCalendar.getTime().getTime()) / (1000 * 60 * 60 * 24));
    }

    /**
     * 比较是否通一天
     *
     * @param localTime  本地时间
     * @param remoteTime 服务器或者其他地方获取的时间
     * @return
     */
    public static boolean isSameDay(long localTime, long remoteTime) {
        Calendar todayC = Calendar.getInstance(Locale.CHINA);
        todayC.setTimeInMillis(localTime);
        int todayYear = todayC.get(Calendar.YEAR);
        int todayMonth = todayC.get(Calendar.MONTH) + 1;
        int todayDay = todayC.get(Calendar.DAY_OF_MONTH);


        Calendar compareTime = Calendar.getInstance();
        compareTime.setTimeInMillis(remoteTime);
        int year = compareTime.get(Calendar.YEAR);
        int month = compareTime.get(Calendar.MONTH) + 1;
        int day = compareTime.get(Calendar.DAY_OF_MONTH);


        if (year == todayYear && month == todayMonth && day == todayDay) {
            return true;
        }
        return false;
    }

    /**
     * 获取当天日期
     *
     * @return yyyy-mm-dd 00：00：00
     */
    public static String getTodayDate() {
        Calendar todayC = Calendar.getInstance(Locale.CHINA);
        todayC.setTimeInMillis(System.currentTimeMillis());
        int todayYear = todayC.get(Calendar.YEAR);
        int todayMonth = todayC.get(Calendar.MONTH) + 1;
        int todayDay = todayC.get(Calendar.DAY_OF_MONTH);
        todayC.get(Calendar.HOUR);
        todayC.get(Calendar.MINUTE);
        todayC.get(Calendar.SECOND);
        return todayYear + "/" + todayMonth + "/" + todayDay + " 00:00:00";
    }

    public static String getTodayDateTime() {
        Calendar todayC = Calendar.getInstance(Locale.CHINA);
        todayC.setTimeInMillis(System.currentTimeMillis());
        int todayYear = todayC.get(Calendar.YEAR);
        int todayMonth = todayC.get(Calendar.MONTH) + 1;
        int todayDay = todayC.get(Calendar.DAY_OF_MONTH);
        int hour = todayC.get(Calendar.HOUR);
        int minute = todayC.get(Calendar.MINUTE);
        int second = todayC.get(Calendar.SECOND);
        return todayYear + "/" + todayMonth + "/" + todayDay + " " + hour + ":" + minute + ":" + second;
    }

    /**
     * 获取指定时间
     *
     * @param date
     * @return
     */
    public static boolean isToday(String date) {
        int year, month, day;
        String[] value = date.split("-");
        year = Integer.parseInt(value[0]);
        month = Integer.parseInt(value[1]);
        day = Integer.parseInt(value[2]);

        Calendar todayC = Calendar.getInstance(Locale.CHINA);
        todayC.setTimeInMillis(System.currentTimeMillis());
        int todayYear = todayC.get(Calendar.YEAR);
        int todayMonth = todayC.get(Calendar.MONTH) + 1;
        int todayDay = todayC.get(Calendar.DAY_OF_MONTH);
        if (year == todayYear && month == todayMonth && day == todayDay) {
            return true;
        }
        return false;
    }

    /**
     * 保留小数点后几位
     *
     * @param f
     * @return
     */
    public static String subDecimal(double f, int p) {
        String s = String.valueOf(f);
        int pointIdx = s.indexOf(".");
        String result = null;
        int end = pointIdx + 1 + p;
        if (s.length() >= end) {
            result = s.substring(0, end);
        } else {
            result = s;
        }
        return result;
    }

    /**
     * 是否是电话号码
     */
    public static boolean isPhoneNumberValid(String phoneNumber) {
        // (^(\d{2,4}[-_－—]?)?\d{3,8}([-_－—]?\d{3,8})?([-_－—]?\d{1,7})?$)|(^0?1[35]\d{9}$)
        String expression1 =
                "((^(13|15|18)[0-9]{9}$)|(^0[1,2]{1}\\d{1}-?\\d{8}$)|(^0[3-9] {1}\\d{2}-?\\d{7,8}$)|(^0[1,2]{1}\\d{1}-?\\d{8}-(\\d{1,4})$)|(^0[3-9]{1}\\d{2}-? \\d{7,8}-(\\d{1,4})$))";
        String expression2 =
                "((\\d{11})|^((\\d{7,8})|(\\d{4}|\\d{3})-(\\d{7,8})|(\\d{4}|\\d{3})-(\\d{7,8})-(\\d{4}|\\d{3}|\\d{2}|\\d{1})|(\\d{7,8})-(\\d{4}|\\d{3}|\\d{2}|\\d{1}))$)";
        CharSequence inputStr = phoneNumber;
        Pattern pattern1 = Pattern.compile(expression1);
        Matcher matcher1 = pattern1.matcher(inputStr);
        Pattern pattern2 = Pattern.compile(expression2);
        Matcher matcher2 = pattern2.matcher(inputStr);
        return matcher1.matches() || matcher2.matches();
    }

    /**
     * 格式化为yyyy年MM月dd日
     */
    public static String formatYMD(long timestamp) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATA_FORMAT);
        return sdf.format(new Date(timestamp));
    }

    /**
     * 格式化为yyyy年MM月
     */
    public static String formatYM(long timestamp) {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy年MM月");
        return sdf1.format(new Date(timestamp));
    }

    /**
     * 格式化为MM月 dd日
     */
    public static String formatMD(long timestamp) {
        SimpleDateFormat sdf1 = new SimpleDateFormat("MM-dd");
        return sdf1.format(timestamp);
    }

    /**
     * 格式化为HH:mm:ss
     */
    public static String formatHHmm(long timestamp) {
        SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");
        return sdf1.format(timestamp);
    }

    /**
     * 格式化为YYYYMMdd
     */
    public static String formatYYYYMD(long time) {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd");
        return sdf1.format(time);
    }

    /**
     * 格式化为yyyy/MM/dd
     */
    public static String formatsYYYYMD(long time) {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd");
        return sdf1.format(time) + " 00:00:00";
    }

    /**
     * 格式化为yyyy/MM/dd HH:mm:ss
     */
    public static String formatNowTime(long time) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return df.format(new Date(time));
    }

    /**
     * 格式化为YYYYMMdd
     */
    public static String formatByYYMD(long time) {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyMMdd");
        return sdf1.format(time);
    }

    /**
     * 格式化为MM月 dd日
     */
    public static String formatMMDDNew(long timestamp) {
        SimpleDateFormat sdf1 = new SimpleDateFormat("MM月dd日");
        return sdf1.format(new Date(timestamp));
    }

    /**
     * 格式化为MM月 dd日
     */
    public static String formatYYMD(long timestamp) {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        return sdf1.format(new Date(timestamp));
    }

    /**
     * 格式化为yyyy-MM-dd HH:mm:ss
     */
    public static String formatTime(long time) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(new Date(time));
    }

    /**
     * 格式化为yyyy/MM/dd HH:mm:ss
     */
    public static String formatTimeUtil(long time) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return df.format(new Date(time));
    }

    /**
     * 格式化为yyyy-MM-dd HH:mm:ss
     */
    public static String formatTime(long time, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(new Date(time));
    }

    /**
     * 格式化为yyyy-MM-dd HH:mm:ss
     */
    public static String formatTime2(long time) {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        return df.format(new Date(time));
    }

    /**
     * 格式化为yyyy年MM月dd日
     */
    public static String formatYMD(Date timestamp) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATA_FORMAT);
        return sdf.format(timestamp);
    }

    /**
     * 获取相差 小时
     */
    public static long isdiffDay(long backTime) {

        long l = System.currentTimeMillis() - backTime;
        long day = l / (24 * 60 * 60 * 1000);
        long hour = (l / (60 * 60 * 1000) - day * 24);
        long min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
        long s = (l / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        Log.i("DateUtil", "" + day + "天" + hour + "小时" + min + "分" + s + "秒");
        return hour;
    }

    public static void isdiffDay() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now;
        try {
            now = df.parse("2004-03-26 13:31:40");

            Date date = df.parse("2004-03-01 11:30:24");
            long l = now.getTime() - date.getTime();
            long day = l / (24 * 60 * 60 * 1000);
            long hour = (l / (60 * 60 * 1000) - day * 24);
            long min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
            long s = (l / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
            Log.i(TAG, "" + day + "天" + hour + "小时" + min + "分" + s + "秒");
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            Log.e(TAG, Log.getStackTraceString(e));

        }
    }

    public static String formatAM(long time) {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        return formatter.format(time);
    }

    public static String formatTimeMMSS(long time) {
        SimpleDateFormat sdf2 = new SimpleDateFormat(DATA_FORMAT_2, Locale.CHINESE);
        return sdf2.format(time);
    }

    public static String formatTimeHHMM(long time) {
        SimpleDateFormat sdf2 = new SimpleDateFormat(DATA_FORMAT_2, Locale.CHINESE);
        return sdf2.format(time);
    }

    private static final String DATA_FORMAT_2 = "HH:mm:ss";


}
