package cn.swiftpass.enterprise.bussiness.logica.refund;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.RefundModel;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.NetHelper;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;
import cn.swiftpass.enterprise.utils.Utils;

/**
 * 退款管理
 * User: Administrator
 * Date: 13-10-11
 * Time: 下午4:31
 */
public class RefundManager {

    private static RefundManager instant;

    public static RefundManager getInstant() {
        if (instant == null) {
            instant = new RefundManager();
        }
        return instant;
    }

    /**
     * 根据单号查询退款详情
     *
     * @param orderNo
     * @param mchId
     * @param listener
     */
    public void queryRefundDetail(final String orderNo, final String mchId, final UINotifyListener<Order> listener) {
        ThreadHelper.executeWithCallback(new Executable<Order>() {
            @Override
            public Order execute() throws Exception {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("outRefundNo", orderNo);
                jsonObject.put("mchId", mchId);
                if (MainApplication.isAdmin.equals("0") && MainApplication.isOrderAuth.equals("0")) // 
                {
                    jsonObject.put("userId", MainApplication.userId);
                }
                long spayRs = System.currentTimeMillis();
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                    jsonObject.put("spayRs", spayRs);
                    jsonObject.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(jsonObject.toString()), MainApplication.getNewSignKey()));
                }
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/queryRefundDetail", jsonObject, String.valueOf(spayRs), null);
                result.setNotifyListener(listener);
                if (!result.hasError()) {
                    int resCode = Integer.parseInt(result.data.getString("result"));
                    if (resCode == 200) {
                        try {
                            JSONObject json = new JSONObject(result.data.getString("message"));
                            Order order = new Order();
                            order.setTransactionId(json.optString("transactionId"));
                            order.setMchName(json.optString("mchName"));
                            order.setTradeType(json.optString("tradeType", ""));
                            order.setOutTradeNo(json.optString("outTradeNo"));
                            order.setTotalFee(json.optLong("totalFee", 0));
                            order.setRefundMoney(json.optLong("refundMoney", 0));
                            order.setTradeTime(json.optString("tradeTime"));
                            order.setTradeName(json.optString("tradeName"));
                            order.setAddTime(json.optString("addTime", ""));
                            order.setTradeState(json.optInt("tradeState", -1));
                            order.setBody(json.optString("body", ""));
                            order.setClient(json.optString("client", ""));
                            order.setRefundNo(json.optString("refundNo", ""));
                            order.setUserName(json.optString("userName", ""));
                            order.setUseId(json.optString("userId", ""));
                            order.setRefundState(json.optInt("refundState", 0));
                            order.setTradeStateText(json.optString("tradeStateText"));
                            order.setRefundTime(json.optString("refundTime", ""));
                            order.setOrderNoMch(json.optString("orderNo", ""));
                            order.setAttach(json.optString("attach", ""));
                            order.setApiCode(json.optString("apiCode", ""));
                            order.setCashFeel(json.optLong("cashFee", 0));
                            order.setSurcharge(json.optLong("surcharge",0));
                            order.setWithholdingTax(json.optLong("withholdingTax",0));
                            order.setOrderFee(json.optLong("orderFee", 0));
                            return order;

                        } catch (Exception e) {
                            listener.onError(ToastHelper.toStr(R.string.failed_query_order));
                            return null;
                        }

                    } else {
                        listener.onError(result.data.getString("message"));
                    }
                }

                return null;
                // return OrderDB.getInstance().queryOrder(orderNo, uid, time);
            }
        }, listener);
    }

    /**
     * 申请退款
     * 0.申请退款成功
     * 1.订单无效
     * 2.订单状态异常
     * 3.已经申请退款
     * 4.申请退款失败
     * 5.退款异
     * 6.退款已经申请成功，请等待管理员审核。
     * 8.申请财付通退款成功
     * 9.只能退款当天订单
     */
    public void regisRefunds(final String orderNo, final String transaction, final long money, final long refundMoney, final UINotifyListener<Order> listener) {
        ThreadHelper.executeWithCallback(new Executable<Order>() {
            @Override
            public Order execute() throws Exception {
                Map<String, String> params = new HashMap<String, String>();
                JSONObject param = new JSONObject();
                //                param.put("uId", MainApplication.userId);
                param.put("outTradeNo", orderNo);
                param.put("client", MainApplication.CLIENT);
                //                param.put("money", value)
                param.put("refundMoney", refundMoney); // 退款金额
                param.put("totalFee", money); // 总金额

                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    params.put("mchId", MainApplication.merchantId);
                    param.put("mchId", MainApplication.merchantId);
                } else {
                    params.put("mchId", MainApplication.getMchId());
                    param.put("mchId", MainApplication.getMchId());
                }

                params.put("refundMoney", String.valueOf(refundMoney));
                params.put("totalFee", String.valueOf(money));
                if (MainApplication.isAdmin.equals("0")) {
                    param.put("userId", MainApplication.userId);// 普通用户才需要查自己的数据，管理员可以查询全部
                    params.put("userId", MainApplication.userId + "");
                    param.put("userName", MainApplication.realName); // 收银员名称
                    params.put("userName", MainApplication.realName);
                } else {
                    param.put("userName", MainApplication.mchName); // 商户名称
                    params.put("userName", MainApplication.mchName);
                }

                if (null != MainApplication.body && !"".equals(MainApplication.body) && !MainApplication.body.equals("null")) {
                    param.put("body", MainApplication.body);
                    params.put("body", MainApplication.body);
                } else {
                    param.put("body", ApiConstant.body);
                    params.put("body", ApiConstant.body);
                }

                params.put("outTradeNo", orderNo);
                params.put("client", MainApplication.CLIENT);

                //验证身份返回的token
                param.put("token", MainApplication.refundToken);

                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey())) {
                    param.put("sign", SignUtil.getInstance().createSign(params, MainApplication.getSignKey()));

                }

                long spayRs = System.currentTimeMillis();
                //                    //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                    param.put("spayRs", spayRs);
                    param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MainApplication.getNewSignKey()));
                }

                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.REFUND_REGIS_NEW, param, String.valueOf(spayRs), null);
                result.setNotifyListener(listener);
                if (!result.hasError()) {
                    String res = result.data.getString("result");
                    int ret = Utils.Integer.tryParse(res, -1);
                    if (ret == 200) {
                        JSONObject jsonObject = new JSONObject(result.data.getString("message"));
                        Order order = new Order();
                        order.setRefundNo(jsonObject.optString("refundNo", ""));
                        order.setAddTimeNew(jsonObject.optString("addTimeNew", ""));

                        order.setSurcharge(jsonObject.optLong("surcharge"));
                        order.setCashFeel(jsonObject.optLong("cashFee"));
                        order.setRefundFee(jsonObject.optLong("refundMoney"));
                        order.setVat(jsonObject.optLong("vat"));
                        order.setOrderFee(jsonObject.optLong("orderFee"));

                        return order;
                    } else {
                        listener.onError(result.data.getString("message"));
                    }
                }
                return null;
            }
        }, listener);
    }

    /**
     * 查询是否可以申请退款
     */
    public void checkCanRefundOrNot(final String orderNo, final long money, final long refundMoney, final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable<Boolean>() {
            @Override
            public Boolean execute() throws Exception {
                Map<String, String> params = new HashMap<String, String>();
                JSONObject param = new JSONObject();
                //                param.put("uId", MainApplication.userId);
                param.put("outTradeNo", orderNo);
                param.put("client", MainApplication.CLIENT);
                //                param.put("money", value)
                param.put("refundMoney", refundMoney); // 退款金额
                param.put("totalFee", money); // 总金额

                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    params.put("mchId", MainApplication.merchantId);
                    param.put("mchId", MainApplication.merchantId);
                } else {
                    params.put("mchId", MainApplication.getMchId());
                    param.put("mchId", MainApplication.getMchId());
                }

                params.put("refundMoney", String.valueOf(refundMoney));
                params.put("totalFee", String.valueOf(money));
                if (MainApplication.isAdmin.equals("0")) {
                    param.put("userId", MainApplication.userId);// 普通用户才需要查自己的数据，管理员可以查询全部
                    params.put("userId", MainApplication.userId + "");
                    param.put("userName", MainApplication.realName); // 收银员名称
                    params.put("userName", MainApplication.realName);
                } else {
                    param.put("userName", MainApplication.mchName); // 商户名称
                    params.put("userName", MainApplication.mchName);
                }

                if (null != MainApplication.body && !"".equals(MainApplication.body) && !MainApplication.body.equals("null")) {
                    param.put("body", MainApplication.body);
                    params.put("body", MainApplication.body);
                } else {
                    param.put("body", ApiConstant.body);
                    params.put("body", ApiConstant.body);
                }

                params.put("outTradeNo", orderNo);
                params.put("client", MainApplication.CLIENT);

                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey())) {
                    param.put("sign", SignUtil.getInstance().createSign(params, MainApplication.getSignKey()));

                }

                long spayRs = System.currentTimeMillis();
                //                    //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                    param.put("spayRs", spayRs);
                    param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MainApplication.getNewSignKey()));
                }

                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.REFUND_CANORNOT, param, String.valueOf(spayRs), null);
                result.setNotifyListener(listener);
                if (!result.hasError()) {
                    String res = result.data.getString("result");
                    int ret = Utils.Integer.tryParse(res, -1);
                    if (ret == 200) {
                        return true;
                    } else {
                        if(result.data!= null && result.data.has("message")){
                            listener.onError(result.data.getString("message"));
                        }else{
                            listener.onError(result.resultCode);
                        }
                    }
                }else {
                    listener.onError(result.resultCode);
                }
                return false;
            }
        }, listener);
    }



    /**
     * 申请退款
     * 0.申请退款成功
     * 1.订单无效
     * 2.订单状态异常
     * 3.已经申请退款
     * 4.申请退款失败
     * 5.退款异
     * 6.退款已经申请成功，请等待管理员审核。
     * 8.申请财付通退款成功
     * 9.只能退款当天订单
     */
    public void regisRefund(final String orderNo, final String transaction, final long money, final long refundMoney, final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable<Boolean>() {
            @Override
            public Boolean execute() throws Exception {
                Map<String, String> params = new HashMap<String, String>();
                JSONObject param = new JSONObject();
                //                param.put("uId", MainApplication.userId);
                param.put("outTradeNo", orderNo);
                param.put("client", MainApplication.CLIENT);
                //                param.put("money", value)
                param.put("refundMoney", refundMoney); // 退款金额
                param.put("totalFee", money); // 总金额

                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    params.put("mchId", MainApplication.merchantId);
                    param.put("mchId", MainApplication.merchantId);
                } else {
                    params.put("mchId", MainApplication.getMchId());
                    param.put("mchId", MainApplication.getMchId());
                }

                params.put("refundMoney", String.valueOf(refundMoney));
                params.put("totalFee", String.valueOf(money));
                if (MainApplication.isAdmin.equals("0")) {
                    param.put("userId", MainApplication.userId);// 普通用户才需要查自己的数据，管理员可以查询全部
                    params.put("userId", MainApplication.userId + "");
                    param.put("userName", MainApplication.realName); // 收银员名称
                    params.put("userName", MainApplication.realName);
                } else {
                    param.put("userName", MainApplication.mchName); // 商户名称
                    params.put("userName", MainApplication.mchName);
                }

                if (null != MainApplication.body && !"".equals(MainApplication.body) && !MainApplication.body.equals("null")) {
                    param.put("body", MainApplication.body);
                    params.put("body", MainApplication.body);
                } else {
                    param.put("body", ApiConstant.body);
                    params.put("body", ApiConstant.body);
                }

                params.put("outTradeNo", orderNo);
                params.put("client", MainApplication.CLIENT);

                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey())) {
                    param.put("sign", SignUtil.getInstance().createSign(params, MainApplication.getSignKey()));

                }
                long spayRs = System.currentTimeMillis();
                //                    //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                    param.put("spayRs", spayRs);
                    param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MainApplication.getNewSignKey()));
                }

                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.REFUND_REGIS, param, null, null);
                result.setNotifyListener(listener);
                if (!result.hasError()) {
                    String res = result.data.getString("result");
                    int ret = Utils.Integer.tryParse(res, -1);

                    if (ret == 200) {
                        return true;
                    } else {
                        listener.onError(result.data.getString("message"));
                    }
                }
                return false;
            }
        }, listener);
    }

}
