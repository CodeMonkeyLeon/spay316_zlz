package cn.swiftpass.enterprise.io.okhttp;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by congwei.li on 2022/4/19.
 *
 * @Description:
 */
public class CommonResponse implements Parcelable {
    public static final Creator<CommonResponse> CREATOR = new Creator<CommonResponse>() {
        @Override
        public CommonResponse createFromParcel(Parcel in) {
            return new CommonResponse(in);
        }

        @Override
        public CommonResponse[] newArray(int size) {
            return new CommonResponse[size];
        }
    };
    //{"result":"402","message":"没有检测到新版本","code":""}
    public String result;
    public String message;
    public String code;
    public String url;

    protected CommonResponse(Parcel in) {
        result = in.readString();
        message = in.readString();
        code = in.readString();
        url = in.readString();
    }

    protected CommonResponse(String url) {
        result = ErrorCode.RESULT_NO_ERROR;
        message = "";
        code = "";
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(result);
        dest.writeString(message);
        dest.writeString(code);
        dest.writeString(url);
    }

    @Override
    public String toString() {
        return "CommonResponse{" +
                "result='" + result + '\'' +
                ", message='" + message + '\'' +
                ", code='" + code + '\'' +
                ", url=" + url + '\'' +
                '}';
    }
}
