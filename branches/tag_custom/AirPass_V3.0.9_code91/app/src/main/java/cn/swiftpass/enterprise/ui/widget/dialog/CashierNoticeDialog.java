package cn.swiftpass.enterprise.ui.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import cn.swiftpass.enterprise.intl.R;

/**
 * Created by aijingya on 2018/5/29.
 *
 * @Package cn.swiftpass.enterprise.ui.widget.dialog
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/5/29.13:53.
 */

public class CashierNoticeDialog extends Dialog{
    private static final String TAG = "CashierNoticeDialog";
    Context mContext;
    TextView tv_content;
    LinearLayout ll_cofirm_button;
    TextView tv_known;

    private ButtonConfirmCallBack mConfirmListener;

    public CashierNoticeDialog(@NonNull Context context) {
        super(context, R.style.simpleDialog);
        mContext = context;
        init();
    }

    public void init(){
        View rootView = View.inflate(mContext, R.layout.dialog_cashier_pop, null);
        setContentView(rootView);

        tv_content = (TextView)rootView.findViewById(R.id.tv_content);
        ll_cofirm_button = (LinearLayout)rootView.findViewById(R.id.ll_cofirm_button);
        tv_known = (TextView) rootView.findViewById(R.id.tv_known);


        ll_cofirm_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mConfirmListener == null){
                    Log.i(TAG, "mConfirmListener == null");
                }else{
                    mConfirmListener.onClickConfirmCallBack();
                }
            }
        });

    }

    public void setContentText(String  text){
        tv_content.setText(text);
    }
    public void setContentTextSize(int size){
        tv_content.setTextSize(size);
    }

    public void setContentText(int text){
        tv_content.setText(text);
    }

    public void setConfirmText(int text){
        tv_known.setText(text);
    }

    /**
     * 定义接口用来响应button的监听
     */
    public interface ButtonConfirmCallBack{

        void onClickConfirmCallBack();
    }

    public void setCurrentListener(ButtonConfirmCallBack listener){

        mConfirmListener = listener;
    }



}
