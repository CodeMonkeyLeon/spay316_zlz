package cn.swiftpass.enterprise.bussiness.model;

import cn.swiftpass.enterprise.io.database.table.CacheInfoTable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * 缓存实体
 * User: Administrator
 * Date: 13-11-18
 * Time: 下午6:21
 */
@DatabaseTable(tableName = CacheInfoTable.TABLE_NAME)
public class CacheModel implements Serializable {

    private static final long serialVersionUID = 1L;
    @DatabaseField(generatedId = true)
    public long id;
    @DatabaseField( columnName = CacheInfoTable.COLUMN_TYPE)
    public int type;
    @DatabaseField( columnName = CacheInfoTable.COLUMN_ADDTIME)
    public long addTime;
    @DatabaseField( columnName = CacheInfoTable.COLUMN_UID)
    public long uId;

}
