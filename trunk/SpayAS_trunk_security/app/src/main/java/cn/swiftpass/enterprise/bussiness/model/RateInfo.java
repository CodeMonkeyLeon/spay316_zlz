package cn.swiftpass.enterprise.bussiness.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by congwei.li on 2022/4/25.
 *
 * @Description:
 */
public class RateInfo implements Parcelable {
    //{
    //	"feeType": "",
    //	"mchId": "",
    //	"rate": "0.000000000000",
    //	"rateTime": "",
    //	"surchargeRate": "0.005000000000",
    //	"taxRate": "0.020000000000",
    //	"vatRate": "0.000000000000",
    //	"usdToRmbExchangeRate": "6.550559000000",
    //	"sourceToUsdExchangeRate": "0.123685837972",
    //	"alipayPayRate": ""
    //}

    public String feeType;
    public String mchId;
    public String rate;
    public String rateTime;
    public String surchargeRate;
    public String taxRate;
    public String vatRate;
    public String usdToRmbExchangeRate;
    public String sourceToUsdExchangeRate;
    public String alipayPayRate;

    public RateInfo(){

    }

    protected RateInfo(Parcel in) {
        feeType = in.readString();
        mchId = in.readString();
        rate = in.readString();
        rateTime = in.readString();
        surchargeRate = in.readString();
        taxRate = in.readString();
        vatRate = in.readString();
        usdToRmbExchangeRate = in.readString();
        sourceToUsdExchangeRate = in.readString();
        alipayPayRate = in.readString();
    }

    public static final Creator<RateInfo> CREATOR = new Creator<RateInfo>() {
        @Override
        public RateInfo createFromParcel(Parcel in) {
            return new RateInfo(in);
        }

        @Override
        public RateInfo[] newArray(int size) {
            return new RateInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(feeType);
        dest.writeString(mchId);
        dest.writeString(rate);
        dest.writeString(rateTime);
        dest.writeString(surchargeRate);
        dest.writeString(taxRate);
        dest.writeString(vatRate);
        dest.writeString(usdToRmbExchangeRate);
        dest.writeString(sourceToUsdExchangeRate);
        dest.writeString(alipayPayRate);
    }
}
