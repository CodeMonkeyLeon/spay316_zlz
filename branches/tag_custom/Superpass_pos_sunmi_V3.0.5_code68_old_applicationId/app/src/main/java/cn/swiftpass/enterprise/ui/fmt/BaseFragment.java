package cn.swiftpass.enterprise.ui.fmt;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.tencent.stat.StatService;

import java.io.Serializable;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.SummaryActivity;
import cn.swiftpass.enterprise.ui.activity.TransactionDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.spayMainTabActivity;
import cn.swiftpass.enterprise.ui.widget.CommonConfirmDialog;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.MyToast;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.Utils;

/**
 * Created by aijingya on 2018/5/4.
 *
 * @Package cn.swiftpass.enterprise.ui.fmt
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/5/4.14:50.
 */

public abstract class BaseFragment extends Fragment{
    private static final String TAG = BaseFragment.class.getSimpleName();
    public enum ANIM_TYPE {
        NONE, RIGHT_IN, LEFT_IN, LEFT_OUT, RIGHT_OUT,
    }

    protected Context mContext;
    private FragmentActivity mActivity;
    protected LayoutInflater mInflater = null;
    protected Intent mIntent;
    boolean isStartActivity = false;
    private int finishAnimId = 0;
    private int startAnimId = 0;

    private Dialog dialogNew = null;
    private DialogInfo dialogInfo;
    protected boolean isResumed = false;
    MyToast myToast;

    protected abstract int getLayoutId();

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof spayMainTabActivity){
            mActivity=(spayMainTabActivity) activity;
        }else if(activity instanceof SummaryActivity){
            mActivity=(SummaryActivity) activity;
        }else if(activity instanceof TransactionDetailsActivity){
            mActivity=(TransactionDetailsActivity) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = mActivity;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), null);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        isResumed = true;
        if(mContext==null){
            mContext=mActivity;
        }
        isStartActivity = false;
        StatService.onResume(mContext);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }

    @Override
    public void onPause() {
        super.onPause();
        isResumed = false;
        dismissLoading();
        StatService.onPause(mContext);
    }

    protected boolean isLoginRequired() {
        return true;
    }

    public void redirectLoginActivity() {

        Intent intent = new Intent();
        intent.setClassName(mActivity, "cn.swiftpass.enterprise.ui.activity.WelcomeActivity");
        startActivity(intent);

        finish();
        for (Activity a : MainApplication.allActivities) {
            a.finish();
        }
        //        MainApplication.getContext().exit();
    }

    /**
     * 启动带一个外部数据的activity <功能详细描述>
     *
     * @param key
     * @param value
     * @param cla
     * @see [类、类#方法、类#成员]
     */
    public void startActivity(String key, Object value, Class<? extends Activity> cla) {
        if (isStartActivity){
            return;
        }
        isStartActivity = true;
        initIntent(cla);
        putExtra(key, value);
        startActivity();
    }

    public void initIntent(Class<? extends Activity> cla) {
        if (mIntent == null) {
            mIntent = new Intent();
        }
        if (cla != null) {
            mIntent.setClass(mContext, cla);
        }
    }

    /**
     * intent装载外部数据，可以使用int String boolean Serializable long double
     * 最后需要调用startActivity(Class)方法启动
     *
     * @param key
     * @param value
     * @see [类、类#方法、类#成员]
     */
    public void putExtra(String key, Object value) {
        initIntent(null);
        if (mIntent != null && key != null && value != null) {
            if (value instanceof Integer) {
                mIntent.putExtra(key, (Integer) value);
            } else if (value instanceof String) {
                mIntent.putExtra(key, (String) value);
            } else if (value instanceof Serializable) {
                mIntent.putExtra(key, (Serializable) value);
            } else if (value instanceof Boolean) {
                mIntent.putExtra(key, (Boolean) value);
            } else if (value instanceof Long) {
                mIntent.putExtra(key, (Long) value);
            } else if (value instanceof Double) {
                mIntent.putExtra(key, (Double) value);
            }
        }
    }

    private void startActivity() {
        if (mIntent != null){
            startActivity(mIntent);
        }

        if (startAnimId != 0) {
            mActivity.overridePendingTransition(startAnimId, R.anim.none);
        }
    }

    /**
     * 启动带一个外部数据的activity <功能详细描述>
     *
     * @param key
     * @param value
     * @param cla
     * @see [类、类#方法、类#成员]
     */
    public void startActivityForResult(String key, Object value, Class<? extends Activity> cla, int requestCode) {
        if (isStartActivity){
            return;
        }

        isStartActivity = true;
        initIntent(cla);
        putExtra(key, value);
        startActivityForResult(requestCode);
    }

    /**
     * 直接启动一个activity <功能详细描述>
     *
     * @param cla
     * @see [类、类#方法、类#成员]
     */
    public void startActivityForResult(Class<? extends Activity> cla, int requestCode) {
        if (isStartActivity){
            return;
        }
        isStartActivity = true;
        initIntent(cla);
        startActivityForResult(requestCode);
    }

    private void startActivityForResult(int requestCode) {
        if (mIntent != null){
            startActivityForResult(mIntent, requestCode);
        }
        if (startAnimId != 0) {
            mActivity.overridePendingTransition(startAnimId, R.anim.none);
        }
    }



    /**
     *
     * @Title: startFromLeftInAnim
     * @Description: 从屏幕的左边进入
     * @throws
     */
    public void startFromLeftInAnim() {
        startAnimId = R.anim.push_left_in;
    }

    /**
     * 结束此activity时动画 从屏幕的右边移出
     *
     * @see [类、类#方法、类#成员]
     */
    public void finishFromRightOutAnim() {
        finishAnimId = R.anim.from_right_out;
    }

    /**
     * 结束此activity时动画 从屏幕的左边移出
     *
     * @see [类、类#方法、类#成员]
     */
    public void finishFromLeftOutAnim() {
        finishAnimId = R.anim.push_left_out;
    }

    public void finish() {
        // TODO Auto-generated method stub
        if (finishAnimId != 0) {
            mActivity.overridePendingTransition(R.anim.none, finishAnimId);
        }
    }

    /**
     *
     * @Title: finish
     * @Description: TODO
     * @param animType
     *            带动画效果的结束
     * @throws
     */
    public void finish(ANIM_TYPE animType) {
        switch (animType) {
            case LEFT_OUT:
                finishFromLeftOutAnim();
                break;
            case RIGHT_OUT:
                finishFromRightOutAnim();
                break;
            default:
                finishAnimId = 0;
                break;
        }
        finish();
    }

    protected void runOnUiThreadSafety(Runnable runnable) {
        Utils.runOnUiThreadSafety(mActivity, runnable);
    }

    protected Dialog progressDialog;
    protected void toCloseProgressMsg() {
        runOnUiThreadSafety(new Runnable() {
            @Override
            public void run() {
                closeProgressDialog();
            }
        });
    }

   /* protected Dialog getProgressDialog(String msg) {
        HZinsProgressDialog fddProgressDialog = new HZinsProgressDialog(getActivity());
        fddProgressDialog.setMessage(msg);
        return fddProgressDialog;
    }*/

    private void closeProgressDialog() {
        Utils.clossDialog(progressDialog);
    }

   /* protected void toShowProgressMsg(final String msg) {
        runOnUiThreadSafety(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    if (progressDialog instanceof ProgressDialog) {
                        ((ProgressDialog) progressDialog).setMessage(msg);
                    } else {
                        try {
                            progressDialog.getClass()
                                    .getMethod("setMessage", String.class)
                                    .invoke(progressDialog, msg);
                        } catch (Exception e) {
                            Log.e(TAG,Log.getStackTraceString(e));
                        }
                    }
                } else {
                    progressDialog = getProgressDialog(msg);
                    progressDialog.show();
                }
            }
        });
    }
*/
    /*public void setProgressCloseVisible(boolean isVisible){
        if(progressDialog!=null){
            ((HZinsProgressDialog)progressDialog).setProgressCloseVisible(isVisible);
        }
    }
*/
    private boolean isProgressDialogCancelable=true;

    public void setProgressDialogCancelable(boolean isCancelable){
        isProgressDialogCancelable=isCancelable;
        if(progressDialog!=null){
            progressDialog.setCancelable(isProgressDialogCancelable);
        }

    }

    public void loadDialog(Activity context, String str) {
        // 加载样式
        if (null == dialogNew) {
            dialogNew = new Dialog(context, R.style.my_dialog);
            dialogNew.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogNew.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogNew.setCanceledOnTouchOutside(false);
            LayoutInflater inflater = LayoutInflater.from(context);
            View v = inflater.inflate(R.layout.load_progress_info, null);// 得到加载view dialog_common
            TextView tv = (TextView) v.findViewById(R.id.tv_dialog);
            tv.setText(str);
            // dialog透明
            WindowManager.LayoutParams lp = dialogNew.getWindow().getAttributes();
            lp.alpha = 0.6f;
            dialogNew.getWindow().setAttributes(lp);
            dialogNew.setContentView(v);
            dialogNew.setOnKeyListener(new DialogInterface.OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                    if (keycode == KeyEvent.KEYCODE_BACK) {
                        return true;
                    }
                    return false;
                }
            });
        }
        DialogHelper.resizeNew(context, dialogNew);
        dialogNew.show();
    }

    public void loadDialog(Activity context, int res) {
        // 加载样式
        try {

            if (null == dialogNew) {
                dialogNew = new Dialog(context, R.style.my_dialog);
                dialogNew.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogNew.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialogNew.setCanceledOnTouchOutside(false);
                LayoutInflater inflater = LayoutInflater.from(context);
                View v = inflater.inflate(R.layout.load_progress_info, null);// 得到加载view dialog_common
                TextView tv = (TextView) v.findViewById(R.id.tv_dialog);
                tv.setText(res);
                // dialog透明
                WindowManager.LayoutParams lp = dialogNew.getWindow().getAttributes();
                lp.alpha = 0.8f;
                dialogNew.getWindow().setAttributes(lp);
                dialogNew.setContentView(v);
                dialogNew.setOnKeyListener(new DialogInterface.OnKeyListener() {

                    @Override
                    public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                        if (keycode == KeyEvent.KEYCODE_BACK) {
                            return true;
                        }
                        return false;
                    }
                });
            }
            DialogHelper.resizeNew(context, dialogNew);
            dialogNew.show();
        } catch (Exception e) {
        }
    }

    public void dissDialog() {
        if (null != dialogNew) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        dialogNew.dismiss();
                        dialogNew = null;
                    } catch (Exception e) {
                        Log.e(TAG,Log.getStackTraceString(e));
                    }
                }
            });
        }
    }

    public void dismissLoading() {
        dismissMyLoading();
    }
    public void dismissMyLoading() {
        //        if (loadingDialog != null)
        //        {
        //            Logger.i("hehui", "dismissMyLoading-->");
        //            loadingDialog.dismiss();
        //            loadingDialog = null;
        //        }
        dissDialog();
    }

    /**
     * 带内容以及btn内容设置
     * @param context
     * @param content
     * @param btStr
     * @param handleBtn
     */
    public void toastDialog(final Activity context, final String content, final String btStr, final NewDialogInfo.HandleBtn handleBtn) {
        context.runOnUiThread(new Runnable() {

            @Override
            public void run() {

                try {
                    if (StringUtil.isEmptyOrNull(content)) {
                        return;
                    }

                    NewDialogInfo info = new NewDialogInfo(context, content, null, 2, null, handleBtn);
                    info.setOnKeyListener(new DialogInterface.OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                            if (keycode == KeyEvent.KEYCODE_BACK) {
                                return true;
                            }
                            return false;
                        }
                    });
                    DialogHelper.resize(context, info);

                    info.show();
                } catch (Exception e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
        });

    }

    /**
     * 只带内容
     * @param context
     * @param content
     * @param handleBtn
     */
    public void toastDialog(final Activity context, final String content, final NewDialogInfo.HandleBtn handleBtn) {
        context.runOnUiThread(new Runnable() {

            @Override
            public void run() {

                try {
                    if (StringUtil.isEmptyOrNull(content)) {
                        return;
                    }

                    NewDialogInfo info = new NewDialogInfo(context, content, null, 2, null, handleBtn);
                    info.setOnKeyListener(new DialogInterface.OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                            if (keycode == KeyEvent.KEYCODE_BACK) {
                                return true;
                            }
                            return false;
                        }
                    });
                    DialogHelper.resize(context, info);

                    info.show();
                } catch (Exception e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
        });

    }

    /**
     * 带标题，内容以及btn内容设置
     * @param context
     * @param content
     * @param title
     * @param handleBtn
     */
    public void toastDialog(final Activity context,final String btnok, final String content, final Integer title,final NewDialogInfo.HandleBtn handleBtn) {
        context.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                try {

                    NewDialogInfo info = new NewDialogInfo(context, content, btnok, 2, context.getString(title), handleBtn);
                    info.setOnKeyListener(new DialogInterface.OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                            if (keycode == KeyEvent.KEYCODE_BACK) {
                                return true;
                            }
                            return false;
                        }
                    });
                    DialogHelper.resize(context, info);

                    info.show();
                } catch (Exception e) {
                }
            }
        });

    }

    public void toastDialog(final Activity context, final Integer content, final NewDialogInfo.HandleBtn handleBtn) {
        context.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                try {

                    NewDialogInfo info = new NewDialogInfo(context, context.getString(content), null, 2, null, handleBtn);
                    info.setOnKeyListener(new DialogInterface.OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                            if (keycode == KeyEvent.KEYCODE_BACK) {
                                return true;
                            }
                            return false;
                        }
                    });
                    DialogHelper.resize(context, info);

                    info.show();
                } catch (Exception e) {
                }
            }
        });

    }

    public void showUpgradeInfoDialog(UpgradeInfo result, CommonConfirmDialog.ConfirmListener listener) {
        StringBuilder sb = new StringBuilder();
        sb.append(getString(R.string.show_version) + result.versionName + "\n");
//        if (result.dateTime != 0) {
//            sb.append(getString(R.string.show_version_time) + DateUtil.formatYMD(result.dateTime) + "\n");
//        } else {
//            sb.append(getString(R.string.show_version_time_unknow) + "\n");
//        }
        if (!TextUtils.isEmpty(result.message)) {
            if (result.message.equals("null")) {
                result.message = "";
            }
            sb.append(getString(R.string.show_version_update_content) + "\n" + result.message + "\n\n");
        }
        sb.append(getString(R.string.show_is_update));

        CommonConfirmDialog.show(mContext, getString(R.string.show_find_new_version), sb.toString(), listener, true, result);
    }

    protected boolean checkSession() {
        if (isLoginRequired() && MainApplication.getContext().isNeedLogin()) {
            MainApplication.getContext().setNeedLogin(false);
            //            showNeedLoginToast();
            toastDialog(mActivity, R.string.msg_need_login, new NewDialogInfo.HandleBtn() {

                @Override
                public void handleOkBtn() {
                    PreferenceUtil.removeKey("login_skey");
                    PreferenceUtil.removeKey("login_sauthid");
                    MainApplication.isSessionOutTime = true;
                    redirectLoginActivity();
                }
            });

            return true;
        }
        return false;
    }


    public String getStringById(int id) {
        try {
            return this.getResources().getString(id);
        } catch (Exception e) {
            return "";
        }

    }

    public void showExitDialog(final Activity context) {
        dialogInfo = new DialogInfo(context, getString(R.string.public_cozy_prompt), getString(R.string.show_sign_out), getString(R.string.btnOk), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                finish();
                MainApplication.getContext().exit();
//                                System.exit(0);
                MainApplication.isSessionOutTime = true;

            }

            @Override
            public void handleCancleBtn() {
                dialogInfo.cancel();
            }
        }, null);

        DialogHelper.resize(context, dialogInfo);
        dialogInfo.show();
    }

    public void showPage(Class clazz) {
        Intent intent = new Intent(mContext, clazz);
        this.startActivity(intent);
    }

    /**
     * 跳转页面
     *
     * @param clazz
     * @see [类、类#方法、类#成员]
     */
    @SuppressWarnings("rawtypes")
    public void showPage(Class clazz, Bundle bundle) {
        Intent intent = new Intent(mContext, clazz);
        intent.putExtra("bundle", bundle);
        this.startActivity(intent);
    }

    /**
     * 跳转页面
     *
     * @param clazz
     * @see [类、类#方法、类#成员]
     */
    @SuppressWarnings("rawtypes")
    public void showPage(Class clazz, String param) {
        Intent intent = new Intent(mContext, clazz);
        intent.putExtra("picUrl", param);
        this.startActivity(intent);
    }

    public void showLoading(boolean cancelble, int id) {
        showLoading(cancelble, getString(id));
    }

    public void showLoading(boolean cancelble, String str) {
        if (!isResumed){
            return;
        }

        //        if (loadingDialog == null)
        //        {
        //            loadingDialog = new ProgressDialog(this);
        //            loadingDialog.setCancelable(cancelble);
        //
        //        }
        //        loadingDialog.setMessage(str);
        //        loadingDialog.show();
        loadDialog(mActivity, str);
    }


    /**
     * 管理其它窗体
     */
    public void showDialog(Dialog dialog) {
        if (dialog != null) {
            // 先关闭之前的窗体
            hideDialog();
            if (!isResumed){
                return;
            }

            this.dialog = dialog;
            // 没有显现，则显现
            if (!dialog.isShowing()) {
                dialog.show();
            }
        }
    }

    /**
     * 任意线程都可显示toast
     */
    protected void showToastInfo(final int resId) {

        showToastInfo(resId, true);
    }

    /**
     * 任意线程都可显示toast
     */
    protected void showToastInfo(final String text, final boolean isLong) {
        if (checkSession()) {
            return;
        }
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                /*  Toast.makeText(BaseActivity.this, text,
                          isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();*/

                myToast = new MyToast();
                myToast.showToast(mContext, text);
            }
        });
    }

        /**
         * 任意线程都可显示toast
         */
    protected void showToastInfo(final int resId, final boolean isLong) {

        if (checkSession()) {
            return;
        }

        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                /*Toast.makeText(BaseActivity.this,
                        getResources().getString(resId),
                        isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();*/

                myToast = new MyToast();
                myToast.showToast(mContext, getString(resId));
            }
        });
    }


    protected Dialog dialog;

    /**
     * 关闭自定义弹出窗体
     */
    private void hideDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
    }

    /**
     * 判断当前语言环境是否是英文环境
     * @return
     */
    public boolean isEnglish(){
        boolean isEnglish = false;
        String language = PreferenceUtil.getString("language", "");
        if (!TextUtils.isEmpty(language) && language.equals(MainApplication.LANG_CODE_EN_US)) {
            isEnglish = true;
        }
        return isEnglish;
    }

}
