package cn.swiftpass.enterprise.newmvp.loginmodule.model.bean;

import com.google.gson.annotations.SerializedName;

public class LoginBean  {


    /**
     * id : 30005098
     * empId :
     * username : 107590000001
     * realname : 107590000001
     * deptname :
     * remark : 1
     * isAdmin : 1
     * enabled : true
     * mchId : 107590000001
     * isSysAdmin :
     * isRefundAuth : 1
     * isOrderAuth : 1
     * isTotalAuth : 1
     * isActivityAuth : 1
     * isUnfreezeAuth : 1
     * showEwallet : 0
     * mchName : 交易商户
     * mchLogo :
     * tradeName :
     * serviceType : pay.alipay.native.intl|pay.weixin.jspay|pay.weixin.native.intl|pay.weixin.micropay|pay.alipay.micropay
     * authServiceType :
     * isAuthFreezeOpen : 0
     * signKey : b689ca39c2b6f24d429d63825fa66f11
     * cookieMap : {"SKEY":"spay3_session_pre_ee189848-6195-4602-89d7-59ef81b61aef","SAUTHID":"30005098"}
     * roleType : 8
     * delete : false
     * bankCode : intl_and
     * feeType : HKD
     * feeFh : HK$
     * merchantShortName : 商户
     * themeMd5 :
     * apiShowListMd5 : fbbf0e6a60bb8775fcc9ffad122d0a92
     * apiProviderMap : {"2":"支付宝","1":"微信"}
     * refundStateMap : {"0":"Pending","1":"Successful","2":"Failure","3":"Successful","5":"Successful"}
     * tradeStateMap : {"1":"Unpaid","2":"Successful","3":"Closed","4":"Refund","7":"Reversing","8":"Revoked","9":"Revoked"}
     * payTypeMap : {"1":"Wechat Pay","2":"Alipay","3":"Tenpay","4":"QQ wallet","5":"E-card","6":"實名認證(騰付通)","7":"實名認證(銀商)","8":"支付寶(受理版)","9":"實名認證(銀盛)","10":"QQ wallet","11":"銀通","12":"JD wallet","13":"百度錢包","15":"翼支付","18":"Wechat wallet HK","20":"UnionPay","23":"FPS","25":"UnionPay","29":"GCash","999":"UnKnow"}
     * isSurchargeOpen : 0
     * isTaxRateOpen : 0
     * isTipOpen : 0
     * email :
     * timeZone :
     * rate : 0.855900000000
     * isFixCode : 0
     * rateTime :
     * surchargeRate : 0.000000000000
     * taxRate : 0.000000000000
     * vatRate : 0.000000000000
     * numFixed : 2
     * usdToRmbExchangeRate : 0.000000000000
     * sourceToUsdExchangeRate : 0.000000000000
     * isCardOpen : 0
     * alipayPayRate : 0.855426000000
     * token :
     * isDefault :
     * appId :
     * machineId :
     * createdAt :
     */

    private String id;
    private String empId;
    private String username;
    private String realname;
    private String deptname;
    private String remark;
    private int isAdmin;
    private boolean enabled;
    private String mchId;
    private String isSysAdmin;
    private String isRefundAuth;
    private String isOrderAuth;
    private String isTotalAuth;
    private String isActivityAuth;
    private String isUnfreezeAuth;
    private int showEwallet;
    private String mchName;
    private String mchLogo;
    private String tradeName;
    private String serviceType;
    private String authServiceType;
    private int isAuthFreezeOpen;
    private String signKey;
    private CookieMapBean cookieMap;
    private int roleType;
    private boolean delete;
    private String bankCode;
    private String feeType;
    private String feeFh;
    private String merchantShortName;
    private String themeMd5;
    private String apiShowListMd5;
    private ApiProviderMapBean apiProviderMap;
    private RefundStateMapBean refundStateMap;
    private TradeStateMapBean tradeStateMap;
    private PayTypeMapBean payTypeMap;
    private int isSurchargeOpen;
    private int isTaxRateOpen;
    private int isTipOpen;
    private String email;
    private String timeZone;
    private String rate;
    private int isFixCode;
    private String rateTime;
    private String surchargeRate;
    private String taxRate;
    private String vatRate;
    private int numFixed;
    private String usdToRmbExchangeRate;
    private String sourceToUsdExchangeRate;
    private int isCardOpen;
    private String alipayPayRate;
    private String token;
    private String isDefault;
    private String appId;
    private String machineId;
    private String createdAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(int isAdmin) {
        this.isAdmin = isAdmin;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getIsSysAdmin() {
        return isSysAdmin;
    }

    public void setIsSysAdmin(String isSysAdmin) {
        this.isSysAdmin = isSysAdmin;
    }

    public String getIsRefundAuth() {
        return isRefundAuth;
    }

    public void setIsRefundAuth(String isRefundAuth) {
        this.isRefundAuth = isRefundAuth;
    }

    public String getIsOrderAuth() {
        return isOrderAuth;
    }

    public void setIsOrderAuth(String isOrderAuth) {
        this.isOrderAuth = isOrderAuth;
    }

    public String getIsTotalAuth() {
        return isTotalAuth;
    }

    public void setIsTotalAuth(String isTotalAuth) {
        this.isTotalAuth = isTotalAuth;
    }

    public String getIsActivityAuth() {
        return isActivityAuth;
    }

    public void setIsActivityAuth(String isActivityAuth) {
        this.isActivityAuth = isActivityAuth;
    }

    public String getIsUnfreezeAuth() {
        return isUnfreezeAuth;
    }

    public void setIsUnfreezeAuth(String isUnfreezeAuth) {
        this.isUnfreezeAuth = isUnfreezeAuth;
    }

    public int getShowEwallet() {
        return showEwallet;
    }

    public void setShowEwallet(int showEwallet) {
        this.showEwallet = showEwallet;
    }

    public String getMchName() {
        return mchName;
    }

    public void setMchName(String mchName) {
        this.mchName = mchName;
    }

    public String getMchLogo() {
        return mchLogo;
    }

    public void setMchLogo(String mchLogo) {
        this.mchLogo = mchLogo;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getAuthServiceType() {
        return authServiceType;
    }

    public void setAuthServiceType(String authServiceType) {
        this.authServiceType = authServiceType;
    }

    public int getIsAuthFreezeOpen() {
        return isAuthFreezeOpen;
    }

    public void setIsAuthFreezeOpen(int isAuthFreezeOpen) {
        this.isAuthFreezeOpen = isAuthFreezeOpen;
    }

    public String getSignKey() {
        return signKey;
    }

    public void setSignKey(String signKey) {
        this.signKey = signKey;
    }

    public CookieMapBean getCookieMap() {
        return cookieMap;
    }

    public void setCookieMap(CookieMapBean cookieMap) {
        this.cookieMap = cookieMap;
    }

    public int getRoleType() {
        return roleType;
    }

    public void setRoleType(int roleType) {
        this.roleType = roleType;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getFeeFh() {
        return feeFh;
    }

    public void setFeeFh(String feeFh) {
        this.feeFh = feeFh;
    }

    public String getMerchantShortName() {
        return merchantShortName;
    }

    public void setMerchantShortName(String merchantShortName) {
        this.merchantShortName = merchantShortName;
    }

    public String getThemeMd5() {
        return themeMd5;
    }

    public void setThemeMd5(String themeMd5) {
        this.themeMd5 = themeMd5;
    }

    public String getApiShowListMd5() {
        return apiShowListMd5;
    }

    public void setApiShowListMd5(String apiShowListMd5) {
        this.apiShowListMd5 = apiShowListMd5;
    }

    public ApiProviderMapBean getApiProviderMap() {
        return apiProviderMap;
    }

    public void setApiProviderMap(ApiProviderMapBean apiProviderMap) {
        this.apiProviderMap = apiProviderMap;
    }

    public RefundStateMapBean getRefundStateMap() {
        return refundStateMap;
    }

    public void setRefundStateMap(RefundStateMapBean refundStateMap) {
        this.refundStateMap = refundStateMap;
    }

    public TradeStateMapBean getTradeStateMap() {
        return tradeStateMap;
    }

    public void setTradeStateMap(TradeStateMapBean tradeStateMap) {
        this.tradeStateMap = tradeStateMap;
    }

    public PayTypeMapBean getPayTypeMap() {
        return payTypeMap;
    }

    public void setPayTypeMap(PayTypeMapBean payTypeMap) {
        this.payTypeMap = payTypeMap;
    }

    public int getIsSurchargeOpen() {
        return isSurchargeOpen;
    }

    public void setIsSurchargeOpen(int isSurchargeOpen) {
        this.isSurchargeOpen = isSurchargeOpen;
    }

    public int getIsTaxRateOpen() {
        return isTaxRateOpen;
    }

    public void setIsTaxRateOpen(int isTaxRateOpen) {
        this.isTaxRateOpen = isTaxRateOpen;
    }

    public int getIsTipOpen() {
        return isTipOpen;
    }

    public void setIsTipOpen(int isTipOpen) {
        this.isTipOpen = isTipOpen;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public int getIsFixCode() {
        return isFixCode;
    }

    public void setIsFixCode(int isFixCode) {
        this.isFixCode = isFixCode;
    }

    public String getRateTime() {
        return rateTime;
    }

    public void setRateTime(String rateTime) {
        this.rateTime = rateTime;
    }

    public String getSurchargeRate() {
        return surchargeRate;
    }

    public void setSurchargeRate(String surchargeRate) {
        this.surchargeRate = surchargeRate;
    }

    public String getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(String taxRate) {
        this.taxRate = taxRate;
    }

    public String getVatRate() {
        return vatRate;
    }

    public void setVatRate(String vatRate) {
        this.vatRate = vatRate;
    }

    public int getNumFixed() {
        return numFixed;
    }

    public void setNumFixed(int numFixed) {
        this.numFixed = numFixed;
    }

    public String getUsdToRmbExchangeRate() {
        return usdToRmbExchangeRate;
    }

    public void setUsdToRmbExchangeRate(String usdToRmbExchangeRate) {
        this.usdToRmbExchangeRate = usdToRmbExchangeRate;
    }

    public String getSourceToUsdExchangeRate() {
        return sourceToUsdExchangeRate;
    }

    public void setSourceToUsdExchangeRate(String sourceToUsdExchangeRate) {
        this.sourceToUsdExchangeRate = sourceToUsdExchangeRate;
    }

    public int getIsCardOpen() {
        return isCardOpen;
    }

    public void setIsCardOpen(int isCardOpen) {
        this.isCardOpen = isCardOpen;
    }

    public String getAlipayPayRate() {
        return alipayPayRate;
    }

    public void setAlipayPayRate(String alipayPayRate) {
        this.alipayPayRate = alipayPayRate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getMachineId() {
        return machineId;
    }

    public void setMachineId(String machineId) {
        this.machineId = machineId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public static class CookieMapBean {
        /**
         * SKEY : spay3_session_pre_ee189848-6195-4602-89d7-59ef81b61aef
         * SAUTHID : 30005098
         */

        private String SKEY;
        private String SAUTHID;

        public String getSKEY() {
            return SKEY;
        }

        public void setSKEY(String SKEY) {
            this.SKEY = SKEY;
        }

        public String getSAUTHID() {
            return SAUTHID;
        }

        public void setSAUTHID(String SAUTHID) {
            this.SAUTHID = SAUTHID;
        }
    }

    public static class ApiProviderMapBean {
        /**
         * 2 : 支付宝
         * 1 : 微信
         */

        @SerializedName("2")
        private String a2;
        @SerializedName("1")
        private String a1;

        public String getA2() {
            return a2;
        }

        public void setA2(String a2) {
            this.a2 = a2;
        }

        public String getA1() {
            return a1;
        }

        public void setA1(String a1) {
            this.a1 = a1;
        }
    }

    public static class RefundStateMapBean {
        /**
         * 0 : Pending
         * 1 : Successful
         * 2 : Failure
         * 3 : Successful
         * 5 : Successful
         */

        @SerializedName("0")
        private String r0;
        @SerializedName("1")
        private String r1;
        @SerializedName("2")
        private String r2;
        @SerializedName("3")
        private String r3;
        @SerializedName("5")
        private String r5;

        public String getR0() {
            return r0;
        }

        public void setR0(String r0) {
            this.r0 = r0;
        }

        public String getR1() {
            return r1;
        }

        public void setR1(String r1) {
            this.r1 = r1;
        }

        public String getR2() {
            return r2;
        }

        public void setR2(String r2) {
            this.r2 = r2;
        }

        public String getR3() {
            return r3;
        }

        public void setR3(String r3) {
            this.r3 = r3;
        }

        public String getR5() {
            return r5;
        }

        public void setR5(String r5) {
            this.r5 = r5;
        }
    }

    public static class TradeStateMapBean {
        /**
         * 1 : Unpaid
         * 2 : Successful
         * 3 : Closed
         * 4 : Refund
         * 7 : Reversing
         * 8 : Revoked
         * 9 : Revoked
         */

        @SerializedName("1")
        private String t1;
        @SerializedName("2")
        private String t2;
        @SerializedName("3")
        private String t3;
        @SerializedName("4")
        private String t4;
        @SerializedName("7")
        private String t7;
        @SerializedName("8")
        private String t8;
        @SerializedName("9")
        private String t9;

        public String getT1() {
            return t1;
        }

        public void setT1(String t1) {
            this.t1 = t1;
        }

        public String getT2() {
            return t2;
        }

        public void setT2(String t2) {
            this.t2 = t2;
        }

        public String getT3() {
            return t3;
        }

        public void setT3(String t3) {
            this.t3 = t3;
        }

        public String getT4() {
            return t4;
        }

        public void setT4(String t4) {
            this.t4 = t4;
        }

        public String getT7() {
            return t7;
        }

        public void setT7(String t7) {
            this.t7 = t7;
        }

        public String getT8() {
            return t8;
        }

        public void setT8(String t8) {
            this.t8 = t8;
        }

        public String getT9() {
            return t9;
        }

        public void setT9(String t9) {
            this.t9 = t9;
        }
    }

    public static class PayTypeMapBean {
        /**
         * 1 : Wechat Pay
         * 2 : Alipay
         * 3 : Tenpay
         * 4 : QQ wallet
         * 5 : E-card
         * 6 : 實名認證(騰付通)
         * 7 : 實名認證(銀商)
         * 8 : 支付寶(受理版)
         * 9 : 實名認證(銀盛)
         * 10 : QQ wallet
         * 11 : 銀通
         * 12 : JD wallet
         * 13 : 百度錢包
         * 15 : 翼支付
         * 18 : Wechat wallet HK
         * 20 : UnionPay
         * 23 : FPS
         * 25 : UnionPay
         * 29 : GCash
         * 999 : UnKnow
         */

        @SerializedName("1")
        private String p1;
        @SerializedName("2")
        private String p2;
        @SerializedName("3")
        private String p3;
        @SerializedName("4")
        private String p4;
        @SerializedName("5")
        private String p5;
        @SerializedName("6")
        private String p6;
        @SerializedName("7")
        private String p7;
        @SerializedName("8")
        private String p8;
        @SerializedName("9")
        private String p9;
        @SerializedName("10")
        private String p10;
        @SerializedName("11")
        private String p11;
        @SerializedName("12")
        private String p12;
        @SerializedName("13")
        private String p13;
        @SerializedName("15")
        private String p15;
        @SerializedName("18")
        private String p18;
        @SerializedName("20")
        private String p20;
        @SerializedName("23")
        private String p23;
        @SerializedName("25")
        private String p25;
        @SerializedName("29")
        private String p29;
        @SerializedName("999")
        private String p999;

        public String getP1() {
            return p1;
        }

        public void setP1(String p1) {
            this.p1 = p1;
        }

        public String getP2() {
            return p2;
        }

        public void setP2(String p2) {
            this.p2 = p2;
        }

        public String getP3() {
            return p3;
        }

        public void setP3(String p3) {
            this.p3 = p3;
        }

        public String getP4() {
            return p4;
        }

        public void setP4(String p4) {
            this.p4 = p4;
        }

        public String getP5() {
            return p5;
        }

        public void setP5(String p5) {
            this.p5 = p5;
        }

        public String getP6() {
            return p6;
        }

        public void setP6(String p6) {
            this.p6 = p6;
        }

        public String getP7() {
            return p7;
        }

        public void setP7(String p7) {
            this.p7 = p7;
        }

        public String getP8() {
            return p8;
        }

        public void setP8(String p8) {
            this.p8 = p8;
        }

        public String getP9() {
            return p9;
        }

        public void setP9(String p9) {
            this.p9 = p9;
        }

        public String getP10() {
            return p10;
        }

        public void setP10(String p10) {
            this.p10 = p10;
        }

        public String getP11() {
            return p11;
        }

        public void setP11(String p11) {
            this.p11 = p11;
        }

        public String getP12() {
            return p12;
        }

        public void setP12(String p12) {
            this.p12 = p12;
        }

        public String getP13() {
            return p13;
        }

        public void setP13(String p13) {
            this.p13 = p13;
        }

        public String getP15() {
            return p15;
        }

        public void setP15(String p15) {
            this.p15 = p15;
        }

        public String getP18() {
            return p18;
        }

        public void setP18(String p18) {
            this.p18 = p18;
        }

        public String getP20() {
            return p20;
        }

        public void setP20(String p20) {
            this.p20 = p20;
        }

        public String getP23() {
            return p23;
        }

        public void setP23(String p23) {
            this.p23 = p23;
        }

        public String getP25() {
            return p25;
        }

        public void setP25(String p25) {
            this.p25 = p25;
        }

        public String getP29() {
            return p29;
        }

        public void setP29(String p29) {
            this.p29 = p29;
        }

        public String getP999() {
            return p999;
        }

        public void setP999(String p999) {
            this.p999 = p999;
        }
    }
}
