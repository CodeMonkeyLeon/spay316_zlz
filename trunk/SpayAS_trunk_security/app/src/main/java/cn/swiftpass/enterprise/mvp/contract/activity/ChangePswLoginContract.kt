package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.ECDHInfo
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView


/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @世界小得像一条街的布景
 * @我们相遇了
 * @你点点头
 * @省略了所有的往事
 * @省略了问候。
 */
class ChangePswLoginContract {


    interface View : BaseView {

        fun changePwdSuccess(response: Boolean)


        fun changePwdFailed(error: Any?)


        fun ecdhKeyExchangeSuccess(response: ECDHInfo?)


        fun ecdhKeyExchangeFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {


        fun ecdhKeyExchange(publicKey: String?)


        fun changePwd(
            oldPwd: String?,
            phone: String?,
            newPwd: String?,
            repPassword: String?
        )

    }


}