package cn.swiftpass.enterprise.ui.paymentlink.adapter;


import cn.swiftpass.enterprise.intl.R;

/**
 * 通用加载更多布局
 *
 * @author shican
 */
public class CommonLoadMoreView extends BaseLoadMoreView {
    @Override
    public int getLayoutId() {
        return R.layout.view_loadmore;
    }

    @Override
    protected int getLoadingViewId() {
        return R.id.loading_more;
    }

    @Override
    protected int getLoadFailViewId() {
        return R.id.load_more_fail;
    }

    @Override
    protected int getLoadEndViewId() {
        return R.id.load_more_end;
    }

    @Override
    protected int getLoadClickViewId() {
        return R.id.load_more_click;
    }
}
