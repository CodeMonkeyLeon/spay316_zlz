package cn.swiftpass.enterprise.ui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.SettlementAdapterBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;

/**
 * Created by aijingya on 2019/5/20.
 *
 * @Package cn.swiftpass.enterprise.ui.adapter
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2019/5/20.22:39.
 */
public class SettlementRecycleviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private List<SettlementAdapterBean> settlementAdapterBeanList;
    double min = 1;

    public SettlementRecycleviewAdapter(Context context, List<SettlementAdapterBean> datas) {
        mContext=context;
        settlementAdapterBeanList = datas;

        for (int i = 0; i < MainApplication.getInstance().getNumFixed(); i++) {
            min = min * 10;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if (viewType == 0) {  //根据不同的viewtype,加载不同的布局
            view = LayoutInflater.from(mContext).inflate(R.layout.item_settlement_detail_header, parent,false);
            return new MyViewHolderHeader(view);
        } else {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_settlement_detail_normal, parent, false);
            return new MyViewHolderContent(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case 0:  //不同的布局，做不同的事
                final MyViewHolderHeader holderOne = (MyViewHolderHeader) holder;
                if(!TextUtils.isEmpty(settlementAdapterBeanList.get(position).getSettlementDate())){
                    holderOne.tv_settlement_datetime.setText(settlementAdapterBeanList.get(position).getSettlementDate());
                }
                holderOne.tv_settlement_Amount_title.setText(mContext.getString(R.string.report_settlement_amount));
                holderOne.tv_settlement_Amount.setText(MainApplication.getInstance().getFeeFh() + " " +
                        DateUtil.formatMoneyUtil(settlementAdapterBeanList.get(position).getSettlementAmount() / min));
                holderOne.tv_local_time.setText(mContext.getString(R.string.report_local_time));
                holderOne.tv_local_time_start_to_end.setText(settlementAdapterBeanList.get(position).getOrganizationBeginTime()
                +" ~ "+settlementAdapterBeanList.get(position).getOrganizationEndTime());
                break;
            case 1:
                final MyViewHolderContent holderTwo = (MyViewHolderContent) holder;
                if(!TextUtils.isEmpty(settlementAdapterBeanList.get(position).getTitle())){
                    holderTwo.tv_settlement_detail_title.setText(settlementAdapterBeanList.get(position).getTitle());
                }

                holderTwo.tv_settlement_detail_money.setText(MainApplication.getInstance().getFeeFh() + " "
                        + DateUtil.formatMoneyUtil(settlementAdapterBeanList.get(position).getAmount() / min));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return settlementAdapterBeanList.size();
    }

    @Override
    public int getItemViewType(int position) {
        //根据你的条件，返回不同的type
        if (position == 0) {
            //则是第一种布局
            return 0;
        } else if (position >= 1) {
            //则是第二种布局
            return 1;
        }
        return 0;
    }


    public class MyViewHolderHeader extends RecyclerView.ViewHolder {

        TextView tv_settlement_datetime,tv_settlement_Amount_title,tv_settlement_Amount,tv_local_time,tv_local_time_start_to_end;  //日期

        public MyViewHolderHeader(View view) {
            super(view);
            tv_settlement_datetime = (TextView) view.findViewById(R.id.tv_settlement_datetime);
            tv_settlement_Amount_title = (TextView) view.findViewById(R.id.tv_settlement_Amount_title);
            tv_settlement_Amount = (TextView) view.findViewById(R.id.tv_settlement_Amount);
            tv_local_time = (TextView) view.findViewById(R.id.tv_local_time);
            tv_local_time_start_to_end = (TextView) view.findViewById(R.id.tv_local_time_start_to_end);
        }
    }


    public class MyViewHolderContent extends RecyclerView.ViewHolder {

        TextView tv_settlement_detail_title;    //标题
        TextView tv_settlement_detail_money;    //金额

        public MyViewHolderContent(View view) {
            super(view);
            tv_settlement_detail_title = (TextView) view.findViewById(R.id.tv_settlement_detail_title);
            tv_settlement_detail_money = (TextView) view.findViewById(R.id.tv_settlement_detail_money);
        }
    }

}
