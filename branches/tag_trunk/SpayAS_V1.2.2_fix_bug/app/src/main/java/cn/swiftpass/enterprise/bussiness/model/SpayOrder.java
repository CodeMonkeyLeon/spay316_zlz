package cn.swiftpass.enterprise.bussiness.model;

import java.util.Date;

/**
 * ClassName: PayOrder 
 * Function:订单实体  
 * date: 2014年10月10日 下午1:37:08 
 * @author   陈鹏
 */
public class SpayOrder
{
    private static final long serialVersionUID = 1L;
    
    private String transactionId;
    
    private Integer mchId;
    
    private String mchName;
    
    private Integer userId;
    
    private String outTradeNo;
    
    private Integer money;
    
    private Integer refundMoney;
    
    private Integer tradeState;
    
    private Date addTime;
    
    private String centerId;
    
    private Date tradeTime;
    
    private Integer totalFee;
    
    private Boolean notifyState;
    
    private Date notifyTime;
    
    private String tradeType;
    
    private String tradeName;
    
    private String charset;
    
    private String signType;
    
    /**
     * @return 返回 transactionId
     */
    public String getTransactionId()
    {
        return transactionId;
    }
    
    /**
     * @param 对transactionId进行赋值
     */
    public void setTransactionId(String transactionId)
    {
        this.transactionId = transactionId;
    }
    
    /**
     * @return 返回 mchId
     */
    public Integer getMchId()
    {
        return mchId;
    }
    
    /**
     * @param 对mchId进行赋值
     */
    public void setMchId(Integer mchId)
    {
        this.mchId = mchId;
    }
    
    /**
     * @return 返回 mchName
     */
    public String getMchName()
    {
        return mchName;
    }
    
    /**
     * @param 对mchName进行赋值
     */
    public void setMchName(String mchName)
    {
        this.mchName = mchName;
    }
    
    /**
     * @return 返回 userId
     */
    public Integer getUserId()
    {
        return userId;
    }
    
    /**
     * @param 对userId进行赋值
     */
    public void setUserId(Integer userId)
    {
        this.userId = userId;
    }
    
    /**
     * @return 返回 outTradeNo
     */
    public String getOutTradeNo()
    {
        return outTradeNo;
    }
    
    /**
     * @param 对outTradeNo进行赋值
     */
    public void setOutTradeNo(String outTradeNo)
    {
        this.outTradeNo = outTradeNo;
    }
    
    /**
     * @return 返回 money
     */
    public Integer getMoney()
    {
        return money;
    }
    
    /**
     * @param 对money进行赋值
     */
    public void setMoney(Integer money)
    {
        this.money = money;
    }
    
    /**
     * @return 返回 refundMoney
     */
    public Integer getRefundMoney()
    {
        return refundMoney;
    }
    
    /**
     * @param 对refundMoney进行赋值
     */
    public void setRefundMoney(Integer refundMoney)
    {
        this.refundMoney = refundMoney;
    }
    
    /**
     * @return 返回 tradeState
     */
    public Integer getTradeState()
    {
        return tradeState;
    }
    
    /**
     * @param 对tradeState进行赋值
     */
    public void setTradeState(Integer tradeState)
    {
        this.tradeState = tradeState;
    }
    
    /**
     * @return 返回 addTime
     */
    public Date getAddTime()
    {
        return addTime;
    }
    
    /**
     * @param 对addTime进行赋值
     */
    public void setAddTime(Date addTime)
    {
        this.addTime = addTime;
    }
    
    /**
     * @return 返回 centerId
     */
    public String getCenterId()
    {
        return centerId;
    }
    
    /**
     * @param 对centerId进行赋值
     */
    public void setCenterId(String centerId)
    {
        this.centerId = centerId;
    }
    
    /**
     * @return 返回 tradeTime
     */
    public Date getTradeTime()
    {
        return tradeTime;
    }
    
    /**
     * @param 对tradeTime进行赋值
     */
    public void setTradeTime(Date tradeTime)
    {
        this.tradeTime = tradeTime;
    }
    
    /**
     * @return 返回 totalFee
     */
    public Integer getTotalFee()
    {
        return totalFee;
    }
    
    /**
     * @param 对totalFee进行赋值
     */
    public void setTotalFee(Integer totalFee)
    {
        this.totalFee = totalFee;
    }
    
    /**
     * @return 返回 notifyState
     */
    public Boolean getNotifyState()
    {
        return notifyState;
    }
    
    /**
     * @param 对notifyState进行赋值
     */
    public void setNotifyState(Boolean notifyState)
    {
        this.notifyState = notifyState;
    }
    
    /**
     * @return 返回 notifyTime
     */
    public Date getNotifyTime()
    {
        return notifyTime;
    }
    
    /**
     * @param 对notifyTime进行赋值
     */
    public void setNotifyTime(Date notifyTime)
    {
        this.notifyTime = notifyTime;
    }
    
    /**
     * @return 返回 tradeType
     */
    public String getTradeType()
    {
        return tradeType;
    }
    
    /**
     * @param 对tradeType进行赋值
     */
    public void setTradeType(String tradeType)
    {
        this.tradeType = tradeType;
    }
    
    /**
     * @return 返回 tradeName
     */
    public String getTradeName()
    {
        return tradeName;
    }
    
    /**
     * @param 对tradeName进行赋值
     */
    public void setTradeName(String tradeName)
    {
        this.tradeName = tradeName;
    }
    
    /**
     * @return 返回 charset
     */
    public String getCharset()
    {
        return charset;
    }
    
    /**
     * @param 对charset进行赋值
     */
    public void setCharset(String charset)
    {
        this.charset = charset;
    }
    
    /**
     * @return 返回 signType
     */
    public String getSignType()
    {
        return signType;
    }
    
    /**
     * @param 对signType进行赋值
     */
    public void setSignType(String signType)
    {
        this.signType = signType;
    }
}