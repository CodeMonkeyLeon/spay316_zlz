/*
 * 文 件 名:  ShopKeeperDetialActivity.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.shop;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.shop.PersonalManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.GridViewBean;
import cn.swiftpass.enterprise.bussiness.model.MerchantTempDataModel;
import cn.swiftpass.enterprise.bussiness.model.ShopBankInfo;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.io.database.access.UserInfoDB;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.utils.CleanManager;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.HandlerManager;

/**
 * 资料详情
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-11]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class ShopKeeperDetialActivity extends TemplateActivity
{
    private final static String TAG = ShopKeeperDetialActivity.class.getCanonicalName();
    
    private Button next_but;
    
    private Button btn_quite;
    
    private List<GridViewBean> gridList = new ArrayList<GridViewBean>();
    
    private PersonalGridView img_grid;
    
    private SharedPreferences sharedPreferences;
    
    private SharedPreferences sharedPreferencesOne;
    
    private ScrollView scrollView;
    
    private LinearLayout grid_layout;
    
    private EditText shop_name;
    
    private EditText shop_address;
    
    private EditText shop_bank;
    
    private EditText shop_branch;
    
    private EditText branch_name;
    
    private EditText bank_num;
    
    private EditText personal_id;
    
    private EditText shop_tel;
    
    private ShopBankInfo info;
    
    private MerchantTempDataModel dataModel;
    
    /** 把图片用map给存储进去*/
    private Map<String, String> mapPicUrls = new HashMap<String, String>();
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        Bundle bundle = this.getIntent().getBundleExtra("bundle");
        
        if (bundle != null)
        {
            dataModel = (MerchantTempDataModel)bundle.get("merchantDataModel");
        }
        else
        {
            sharedPreferences = this.getSharedPreferences("dataTwo", 0);
            
            sharedPreferencesOne = this.getSharedPreferences("dataOnes", 0);
            
        }
        setContentView(R.layout.personal_data_detail);
        
        initView();
        
        initValue();
        
        setLister();
        
    }
    
    @Override
    protected void onResume()
    {
        super.onResume();
    }
    
    /**
     * 初始化
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initValue()
    {
        
        String id_up_photo_Ulr = "";
        String id_down_photo_Ulr = "";
        String id_people_photo_Ulr = "";
        String id_ird_Ulr = "";
        String id_code_Ulr = "";
        if (sharedPreferences != null)
        {
            id_up_photo_Ulr = sharedPreferences.getString("id_up_photo", "");
            id_down_photo_Ulr = sharedPreferences.getString("id_down_photo", "");
            id_people_photo_Ulr = sharedPreferences.getString("id_people_photo", "");
            id_ird_Ulr = sharedPreferences.getString("id_ird", "");
            id_code_Ulr = sharedPreferences.getString("id_code", "");
            
        }
        if (dataModel != null)
        {
            id_up_photo_Ulr = dataModel.getLicensePic();
            id_down_photo_Ulr = dataModel.getIdCardTrunmPic();
            id_people_photo_Ulr = dataModel.getIdentityPic();
            id_ird_Ulr = dataModel.getTaxRegCertPic();
            id_code_Ulr = dataModel.getOrgaCodeCertPic();
            
            // 把 上一步和确认提交给隐藏掉
            
            next_but.setVisibility(View.GONE);
            btn_quite.setVisibility(View.GONE);
        }
        
        info = new ShopBankInfo();
        info.setId_code_Ulr(id_code_Ulr);
        info.setId_down_photo_Ulr(id_down_photo_Ulr);
        info.setId_people_photo_Ulr(id_people_photo_Ulr);
        info.setId_up_photo_Ulr(id_up_photo_Ulr);
        info.setId_ird_Ulr(id_ird_Ulr);
        
        GridViewBean bean;
        
        gridList.clear();
        
        try
        {
            if (!"".equals(id_up_photo_Ulr) && null != id_up_photo_Ulr)
            {
                String path = id_up_photo_Ulr;
                bean = new GridViewBean();
                bean.setImageUrl(path);
                bean.setText("申请人手持身份证照片");
                
                mapPicUrls.put("one", path);
                
                gridList.add(bean);
            }
            
            if (!"".equals(id_down_photo_Ulr) && null != id_down_photo_Ulr)
            {
                String path = id_down_photo_Ulr;
                bean = new GridViewBean();
                bean.setImageUrl(path);
                bean.setText("身份证反面照");
                
                mapPicUrls.put("two", path);
                gridList.add(bean);
            }
            if (!"".equals(id_people_photo_Ulr) && null != id_people_photo_Ulr)
            {
                String path = id_people_photo_Ulr;
                bean = new GridViewBean();
                bean.setImageUrl(path);
                bean.setText("营业执照副本照");
                
                mapPicUrls.put("three", path);
                gridList.add(bean);
            }
            if (!"".equals(id_ird_Ulr) && null != id_ird_Ulr)
            {
                String path = id_ird_Ulr;
                bean = new GridViewBean();
                bean.setImageUrl(path);
                bean.setText("税务登记证照片");
                mapPicUrls.put("four", path);
                gridList.add(bean);
            }
            if (!"".equals(id_code_Ulr) && null != id_code_Ulr)
            {
                String path = id_code_Ulr;
                bean = new GridViewBean();
                bean.setImageUrl(path);
                bean.setText("结构代码照片");
                
                mapPicUrls.put("five", path);
                gridList.add(bean);
            }
            
        }
        catch (Exception e)
        {
            Log.e(TAG, "get root cacher dir falied " + e.getMessage());
        }
        
        // 给gridView填值
        Personal_GridViewAdapter adapter = new Personal_GridViewAdapter(ShopKeeperDetialActivity.this, gridList);
        
        img_grid.setAdapter(adapter);
        
        //必须从新设置gridview高度
        int height = grid_layout.getLayoutParams().height;
        img_grid.setMaxHeight(height);
        img_grid.setParentScrollView(scrollView);
        
        // 填充基本数据
        initInfo(info);
        
    }
    
    /**
     * 填充基本数据
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initInfo(ShopBankInfo info)
    {
        if (sharedPreferencesOne != null)
        {
            shop_name.setText(sharedPreferencesOne.getString("shop_name", ""));
            shop_address.setText(sharedPreferencesOne.getString("shop_address", ""));
            shop_bank.setText(sharedPreferencesOne.getString("bank_name", ""));
            shop_branch.setText(sharedPreferencesOne.getString("branch", ""));
            branch_name.setText(sharedPreferencesOne.getString("bayee_name", ""));
            bank_num.setText(sharedPreferencesOne.getString("bank_num", ""));
            personal_id.setText(sharedPreferencesOne.getString("personal_id", ""));
            shop_tel.setText(sharedPreferencesOne.getString("tel", ""));
            
            info.setBayee_name(sharedPreferencesOne.getString("bayee_name", ""));
            info.setTel(sharedPreferencesOne.getString("tel", ""));
            info.setMerchantType(sharedPreferencesOne.getString("bunisOne", ""));
            info.setMerchantTypeName(sharedPreferencesOne.getString("twoText", ""));
            
            info.setBankTypeName(sharedPreferencesOne.getString("bank_name", ""));
            if (sharedPreferencesOne.getString("bank_id", "") != null
                && !"".equals(sharedPreferencesOne.getString("bank_id", "")))
            {
                info.setBankType(Integer.parseInt(sharedPreferencesOne.getString("bank_id", "")));
            }
        }
        
        if (dataModel != null)
        {
            shop_name.setText(dataModel.getMerchantName());
            shop_address.setText(dataModel.getAddress());
            shop_bank.setText(dataModel.getBankTypeName());
            shop_branch.setText(dataModel.getBranchBankName());
            branch_name.setText(dataModel.getBankUserName());
            bank_num.setText(dataModel.getBank());
            personal_id.setText(dataModel.getIdentityNo());
            shop_tel.setText(dataModel.getTelephone());
            info.setBankType(dataModel.getBankType());
            
        }
        
        //给info赋值
        info.setShopName(shop_name.getText().toString());
        info.setBank_num(bank_num.getText().toString());
        info.setBranch_name(branch_name.getText().toString());
        info.setPersonal_id(personal_id.getText().toString());
        info.setShop_bank(shop_bank.getText().toString());
        info.setShopAddress(shop_address.getText().toString());
        info.setShop_branch(shop_branch.getText().toString());
        
    }
    
    /**
     * 设置监听
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void setLister()
    {
        // 上一步
        next_but.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                //showPage(ShopkeeperNextActivity.class);
                finish();
            }
        });
        
        // 提交
        btn_quite.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                
                //生成一个对话框 
                DialogInfo dialogInfo =
                    new DialogInfo(ShopKeeperDetialActivity.this, "提示框", "您的资料已填写完成，确认提交审核通过后，将不能修改，是否确认并提交审核！", "确定",
                        DialogInfo.SUBMITSHOPINFO, new DialogInfo.HandleBtn()
                        {
                            
                            @Override
                            public void handleOkBtn()
                            {
                                MainApplication.listActivities.add(ShopKeeperDetialActivity.this);
                                // 提交完善资料
                                MerchantTempDataModel merchant = new MerchantTempDataModel();
                                // 店铺名称
                                merchant.setMerchantName(info.getShopName());
                                // 地址
                                merchant.setAddress(info.getShopAddress());
                                // 卡号
                                merchant.setBank(info.getBank_num());
                                // 银行类型
                                merchant.setBankType(info.getBankType());
                                // 银行类型名称
                                merchant.setBankTypeName(info.getShop_bank());
                                // 开户名
                                merchant.setBankUserName(info.getBayee_name());
                                // 网点支行名称
                                merchant.setBranchBankName(info.getShop_branch());
                                
                                merchant.setTelephone(info.getTel());
                                // 身份证
                                merchant.setIdentityNo(info.getPersonal_id());
                                
                                if (info.getMerchantType() != null && !"".equals(info.getMerchantType()))
                                {
                                    merchant.setMerchantType(Integer.parseInt(info.getMerchantType()));
                                }
                                
                                merchant.setMerchantTypeName(info.getMerchantTypeName());
                                submitData(merchant, MainApplication.userId);
                            }
                            
                            @Override
                            public void handleCancleBtn()
                            {
                                // TODO Auto-generated method stub
                                
                            }
                        }, null);
                DialogHelper.resize(ShopKeeperDetialActivity.this, dialogInfo);
                dialogInfo.show();
                
                // 提交完善资料信息
                
                //                CleanManager.cleanSharedPreference(ShopKeeperDetialActivity.this, "dataOnes");
                //                CleanManager.cleanSharedPreference(ShopKeeperDetialActivity.this, "dataTwo");
                
                //清空SharedPreferences缓存数据
                //跳转  使用clear_top方式跳转  可以清空上面的Activity栈
                //                Intent intent = new Intent(ShopKeeperDetialActivity.this, ShopkeeperActivity.class);
                //                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //启动标识 把上面的Activity全部清掉
                //                ShopKeeperDetialActivity.this.startActivity(intent);
                
                //                showToastInfo("已提交成功，等待代理商审核!!");
            }
        });
        
    }
    
    /**
     * 提交资料完善信息
     * <功能详细描述>
     * @param merchant
     * @param userID
     * @see [类、类#方法、类#成员]
     */
    private void submitData(final MerchantTempDataModel merchant, long userID)
    {
        if (userID == 0)
        {
            return;
        }
        
        // 提交完善资料
        PersonalManager.shopAddData(String.valueOf(userID), merchant, new UINotifyListener<Boolean>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                
                showLoading(false, "提交中...");
            }
            
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                dismissLoading();
                if (object != null)
                {
                    showToastInfo(object.toString());
                }
            }
            
            @Override
            public void onSucceed(Boolean result)
            {
                super.onSucceed(result);
                dismissLoading();
                if (result)
                {
                    // 清除sharedPreferences缓存信息
                    CleanManager.cleanSharedPreference(ShopKeeperDetialActivity.this, "dataOnes");
                    CleanManager.cleanSharedPreference(ShopKeeperDetialActivity.this, "dataTwo");
                    
                    Editor editor = sharedPreferences.edit();
                    editor.clear();
                    editor.commit();
                    Editor editorOne = sharedPreferencesOne.edit();
                    editorOne.clear();
                    editorOne.commit();
                    
                    showToastInfo("已提交成功，等待代理商审核!");
                    // 把数据更给 商户添加界面
                    
                    HandlerManager.notifyMessage(HandlerManager.SHOPDATA, HandlerManager.SHOPDATA, merchant);
                    
                    // 更新本地数据库记录
                    UserModel userModel;
                    try
                    {
                        sharedPreferencesOne.edit().clear().commit();
                        userModel = UserInfoDB.getInstance().queryUserByName(MainApplication.userName);
                        userModel.setExistData(1);
                        UserInfoDB.getInstance().update(userModel);
                        
                        // 关闭所有的资料完善的activity
                        for (Activity a : MainApplication.listActivities)
                        {
                            a.finish();
                        }
                        
                        // 上传图片
                        if (mapPicUrls.size() > 0)
                        {
                            uploadPic(mapPicUrls);
                            sharedPreferences.edit().clear().commit();
                        }
                    }
                    catch (SQLException e)
                    {
                        Log.e(TAG, "update local data failed -->" + e.getMessage());
                        return;
                    }
                }
                
            }
        });
    }
    
    /**
     * 上传图片
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void uploadPic(Map<String, String> mapPicUrls)
    {
        //        int len = gridList.size();
        //        for (int i = 0; i < len; i++)
        //        {
        //        GridViewBean bean = gridList.get(i);
        PersonalManager.getInstance().uploadPhoto(mapPicUrls, null, new UINotifyListener<Boolean>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                titleBar.setRightLodingVisible(true);
            }
            
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                if (object != null)
                {
                    showToastInfo(object.toString());
                }
            }
            
            @Override
            public void onSucceed(Boolean result)
            {
                super.onSucceed(result);
                if (result)
                {
                    titleBar.setRightLodingVisible(false, false);
                }
                
            }
        });
        //        }
        
    }
    
    /**
     * 页面参数初始化
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView()
    {
        
        next_but = (Button)findViewById(R.id.next_but);
        
        btn_quite = (Button)findViewById(R.id.btn_quite);
        
        img_grid = (PersonalGridView)findViewById(R.id.img_grid);
        
        scrollView = (ScrollView)findViewById(R.id.scroll_view);
        
        grid_layout = (LinearLayout)findViewById(R.id.grid_layout);
        
        shop_name = (EditText)findViewById(R.id.shop_name);
        
        shop_address = (EditText)findViewById(R.id.shop_address);
        
        shop_bank = (EditText)findViewById(R.id.shop_bank);
        
        shop_branch = (EditText)findViewById(R.id.shop_branch);
        
        branch_name = (EditText)findViewById(R.id.branch_name);
        
        bank_num = (EditText)findViewById(R.id.bank_num);
        
        personal_id = (EditText)findViewById(R.id.personal_id);
        
        shop_tel = (EditText)findViewById(R.id.shop_tel);
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        Log.i("hehui", "dataModel-->" + dataModel);
        if (dataModel != null)
        {
            titleBar.setTitle(R.string.data_title_look);
        }
        else
        {
            titleBar.setTitle(R.string.btn_finish);
        }
    }
    
}
