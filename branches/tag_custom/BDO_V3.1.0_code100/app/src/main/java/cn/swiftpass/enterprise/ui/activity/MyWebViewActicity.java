package cn.swiftpass.enterprise.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Executors;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.camera.CameraActivity;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.widget.ProgressWebView;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.Base64;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.ImageUtil;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

import static android.webkit.WebView.enableSlowWholeDocumentDraw;
import static cn.swiftpass.enterprise.camera.CameraActivity.KEY_IMAGE_PATH;

/**
 * Created by aijingya on 2020/9/17.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description:
 * @date 2020/9/17.14:52.
 */
public class MyWebViewActicity extends TemplateActivity {
    private ProgressWebView mWebView;
    private TextView tv_test;
    private String loadUrl;//加载的url
    //登录态信息（如果请求需要带登录态的话）
    private String login_skey;
    private String login_sauthid;
    private String photoName;//拍照生成的图片名
    private Uri photoUri;//拍照生成的图片Uri
    private int mApiCode;//1：常规相机 2：特殊相机 3：注册成功，返回登录页 4：截图当前屏幕所有内容
    private int mCamereType;//1：拍正面时：Take a photo of the front of your ID. 2：拍反面时：Take a photo of the back of your ID.
    private String mCallbackName;//执行js后，回调方法，
    private String PicSign = null;   //图片名称的唯一标识，根据图片的内容生成
    private String viewPicSavePath; //保存的图片的完整路径名称

    public static int REQUEST_CODE_CAMERA_TAKE_PICTURE = 2005;
    public static int REQUEST_CODE_OPEN_PHONE_ALBUM = 2006;

    static final String[] PERMISSIONS_CAMERA = new String[]{android.Manifest.permission.CAMERA};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //在activity的onCreate方法中的setContentView前加上一句enableSlowWholeDocumentDraw();
        // 意思为取消系统的智能绘制，当然这样之后性能会有所下降，是系统在5.0+版本上，Android对webview做了优化，
        // 为了减少内存占用以提高性能，因此在默认情况下会智能的绘制html中需要绘制的部分，其实就是当前屏幕展示的html内容，因此会出现未显示的图像是空白的
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            enableSlowWholeDocumentDraw();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_webview);
        loadUrl = getIntent().getStringExtra("loadUrl");
        login_skey = getIntent().getStringExtra("login_skey");
        login_sauthid = getIntent().getStringExtra("login_sauthid");

        initViews();
    }

    public void initViews() {
        mWebView = findViewById(R.id.web_view);
        tv_test = findViewById(R.id.tv_test);
        //设置ProgressBar样式
        mWebView.setProgressbarDrawable(getResources().getDrawable(R.drawable.web_view_progress));
        //设置ProgressBar高度
        mWebView.setProgressbarHeight(5);
        mWebView.setDrawingCacheEnabled(true);

        //加载url网页
        setWebView(loadUrl);

        tv_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyWebviewAndJsMutual mutual = new MyWebviewAndJsMutual(MyWebViewActicity.this);
                mutual.JsCallAndroidFunction(5, -1, "");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*if(mWebView != null){
            mWebView.onResume();
        }*/
    }

    @Override
    protected void onPause() {
       /* if(mWebView!=null){
            mWebView.pauseTimers();
            mWebView.onPause();
        }*/
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        //将webview清空，防止内存泄漏
        if (mWebView != null) {
            mWebView.clearCache(true);
            mWebView.stopLoading();
            mWebView.removeAllViews();
            mWebView.setWebViewClient(null);
            mWebView.setWebChromeClient(null);
            unregisterForContextMenu(mWebView);
            mWebView.destroy();
            mWebView = null;
        }
        super.onDestroy();
    }


    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setVisibility(View.GONE);
    }


    private void setWebView(String Url) {
        WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true); //开启JavaScript支持
        settings.setJavaScriptCanOpenWindowsAutomatically(true); // 支持通过JS打开新窗口
        settings.setAllowFileAccess(true); // 是否可访问本地文件，默认值 true
        settings.setSupportMultipleWindows(true);// 是否支持多窗口，默认值false
        settings.setAllowContentAccess(true); // 是否可访问Content Provider的资源，默认值 true
        settings.setAllowFileAccessFromFileURLs(false);//是否允许通过file url加载的Javascript读取本地文件，默认值 false
        settings.setAllowUniversalAccessFromFileURLs(false);//是否允许通过file url加载的Javascript读取全部资源(包括文件,http,https)，默认值 false
        settings.setSupportZoom(true); // 支持缩放
        settings.setCacheMode(WebSettings.LOAD_NORMAL);// 不使用缓存
        settings.setDefaultTextEncodingName("UTF-8");
        settings.setDomStorageEnabled(true);
        settings.setBuiltInZoomControls(true); // 支持缩放
        settings.setLoadWithOverviewMode(true); // 初始加载时，是web页面自适应屏幕
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // 5.0以上允许加载http和https混合的页面(5.0以下默认允许，5.0+默认禁止)
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        //各种分辨率适应
        int screenDensity = getResources().getDisplayMetrics().densityDpi;
        WebSettings.ZoomDensity zoomDensity = WebSettings.ZoomDensity.MEDIUM;
        switch (screenDensity) {
            case DisplayMetrics.DENSITY_LOW:
                zoomDensity = WebSettings.ZoomDensity.CLOSE;
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                zoomDensity = WebSettings.ZoomDensity.MEDIUM;
                break;
            case DisplayMetrics.DENSITY_HIGH:
                zoomDensity = WebSettings.ZoomDensity.FAR;
                break;
        }
        settings.setDefaultZoom(zoomDensity);
        settings.setUseWideViewPort(true);

        mWebView.requestFocus();
        mWebView.requestFocusFromTouch();
        //设置监听
        mWebView.setOnReceivedStateListener(new ProgressWebView.OnReceivedStateListener() {
            @Override
            public void onPageStart() {
                showNewLoading(true, getString(R.string.public_loading));
            }

            @Override
            public void onOverrideUrlLoading(WebView view, String url) {
                //调用拨号程序
                if (url.startsWith("mailto:") || url.startsWith("geo:") || url.startsWith("tel:")) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                } else {
                    view.loadUrl(url);
                }
            }

            @Override
            public void onTitleReceived(String title) {
                dismissLoading();
              /*  if (title != null) {
                    titleBar.setTitle(title);
                }*/
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                dismissLoading();
//                titleBar.setTitle("");
            }
        });

        //添加header信息
        Map<String, String> heards = new HashMap<String, String>();
        heards = initHeaderInfo();
        mWebView.loadUrl(Url, heards);

        mWebView.addJavascriptInterface(new MyWebviewAndJsMutual(this), "spay_android");
    }

    public Map<String, String> initHeaderInfo() {
        String language = PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN);
        Map<String, String> heards = new HashMap<String, String>();
        Locale locale = getResources().getConfiguration().locale; //zh-rHK
        String lan = locale.getCountry();
        String langu = "zh_cn";
        if (!TextUtils.isEmpty(language)) {
            if (language.equalsIgnoreCase(MainApplication.LANG_CODE_JA_JP)) {//如果是日语，默认设置成英文
                heards.put("fp-lang", MainApplication.LANG_CODE_EN_US);
                langu = MainApplication.LANG_CODE_EN_US;
            } else {
                heards.put("fp-lang", language);
                langu = language;
            }
        } else {
            if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_HANS)) {
                heards.put("fp-lang", MainApplication.LANG_CODE_ZH_CN);
                langu = MainApplication.LANG_CODE_ZH_CN;
            } else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO) ||
                    lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_HANT)) {
                heards.put("fp-lang", MainApplication.LANG_CODE_ZH_TW);
                langu = MainApplication.LANG_CODE_ZH_TW;
            } else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_JA_JP)) {
                heards.put("fp-lang", MainApplication.LANG_CODE_EN_US);
//                langu = MainApplication.LANG_CODE_JA_JP;
                langu = MainApplication.LANG_CODE_EN_US;//默认写死英文
            } else {
                heards.put("fp-lang", MainApplication.LANG_CODE_EN_US);
                langu = MainApplication.LANG_CODE_EN_US;
            }

        }
        heards.put("SKEY", login_skey);
        heards.put("SAUTHID", login_sauthid);
        String key = "";
        try {
            if (Build.VERSION.SDK_INT >= 29) {//Android 10.0版本以及以上
                key = "sp-" + "Android" + "-" + AppHelper.getUUID();
            } else {
                key = "sp-" + "Android" + "-" + AppHelper.getImei(MainApplication.getContext());
            }
        } catch (Exception e) {
        }

        String value = AppHelper.getVerCode(MainApplication.getContext()) + "," + ApiConstant.bankCode + "," + langu + ","
                + AppHelper.getAndroidSDKVersionName() + "," + DateUtil.formatTime(System.currentTimeMillis()) + ","
                + "Android";
        heards.put(key, value);

        return heards;
    }

    @Override
    public void onBackPressed() {
        //返回键处理
        if (mWebView != null && mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            finish();
        }
    }


    //Android调用js方法:第一种通过loadUrl 第二种通过evaluateJavascript（此方法只可用在4.4及以上版本）
    public void AndroidCallJs_Upload_Image(String callbackName, byte[] image) {
        String imageData64String = new String(Base64.encode(image));
        JSONObject json = new JSONObject();
        try {
            json.put("image", imageData64String);
        } catch (Exception e) {
        }

        if (callbackName != null) {
            String url = "javascript:" + callbackName + "(" + json + ")";
            // 因为该方法在 Android 4.4 版本才可使用，所以使用时需进行版本判断
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                //第一种方法
                mWebView.loadUrl(url);
                mWebView.refreshDrawableState();
            } else {
                //第二种方法
                mWebView.evaluateJavascript(url, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String value) {
                        //value js 的返回值
//                            Toast.makeText(MyWebViewActicity.this,value,Toast.LENGTH_SHORT).show();
                    }
                });
                mWebView.refreshDrawableState();
            }
        }
    }

    /**
     * 调用自定义的camera，可以移植到任何地方
     */
    public void gotoCamera(int apiCode, int camereType) {
        //点击前先检查camera权限
        if (Build.VERSION.SDK_INT >= 23) {
            checkPermissions(PERMISSIONS_CAMERA);
        }

        //授权了camera权限才能跳转
        if (isGranted(Manifest.permission.CAMERA)) {
            CameraActivity.startCamera(MyWebViewActicity.this, REQUEST_CODE_CAMERA_TAKE_PICTURE, apiCode, camereType);
        }
    }


    //跳转到登录页
    public void startWelcomeActivity() {
        //结束掉webview就可以，因为是从登录页直接打开的webview流程
        finish();
    }

    //截取当前webview屏幕的所有内容并生成图片
    public void getCurrentPageBitmap() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        Map<String, String> params = new HashMap<String, String>();
        params.put("pic", "bitmap" + timeStamp);
        PicSign = SignUtil.getInstance().createSign(params, MainApplication.getSignKey());

        if (TextUtils.isEmpty(PicSign)) {
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            // 适配android 10
            if (ImageUtil.isContainsCurrentImageFile(MyWebViewActicity.this, PicSign)) {
                showToastInfo(ToastHelper.toStr(R.string.instapay_save_success));
                return;
            }

        } else {
            // 适配android 9.0以及以下
            if (!TextUtils.isEmpty(viewPicSavePath)) {
                if (new File(viewPicSavePath).exists() && viewPicSavePath.contains(PicSign)) {
                    showToastInfo(ToastHelper.toStr(R.string.instapay_save_success));
                    return;
                }
            }
        }
        showLoading(false, ToastHelper.toStr(R.string.loading));

        Executors.newSingleThreadExecutor().submit(new Runnable() {
            @Override
            public void run() {
                viewPicSavePath = ImageUtil.saveWebViewBitmapFile(PicSign, mWebView, MyWebViewActicity.this);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dismissMyLoading();
                    }
                });
                if (!TextUtils.isEmpty(viewPicSavePath)) {
                    showToastInfo(ToastHelper.toStr(R.string.instapay_save_success) + ":" + viewPicSavePath);
                } else {
                    showToastInfo(ToastHelper.toStr(R.string.instapay_save_failed));
                }
            }
        });
    }


    public class MyWebviewAndJsMutual {
        private Context context;

        public MyWebviewAndJsMutual(Context context) {
            this.context = context;
        }

        /**
         * 前端代码嵌入js：
         * takePhoto 名应和js函数方法名一致
         * 定义JS需要调用的方法，被JS调用的方法必须加入@JavascriptInterface注解
         */
        @JavascriptInterface
        public void JsCallAndroidFunction(int apiCode, int camereType, String callbackName) {
            mApiCode = apiCode;
            mCamereType = camereType;
            mCallbackName = callbackName;

            ;//1：常规相机 2：特殊相机 3：注册成功，返回登录页 4：截图当前屏幕所有内容 5: 打开系统相册，选择一张图片返回
            switch (mApiCode) {
                case 1:
                    //1：常规相机
                    //调用系统相机的代码--可以移植到任何地方
                  /* String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                   photoName = "camera" + timeStamp;
                   photoUri = ImageUtil.openSystemCamera(MyWebViewActicity.this ,photoName);*/

                    //调用自定义相机的代码----可以移植到任何地方
                    gotoCamera(mApiCode, mCamereType);

                    break;
                case 2:
                    //2：特殊相机
                    //调用自定义相机的代码----可以移植到任何地方
                    gotoCamera(mApiCode, mCamereType);

                    break;
                case 3:
                    //3：注册成功，返回登录页
                    startWelcomeActivity();

                    break;
                case 4:
                    //4：截图当前屏幕所有内容,保存webview的长图
                    getCurrentPageBitmap();

                    break;
                case 5:
                    //4：截图当前屏幕所有内容,保存webview的长图
                    gotoPhotoAlbum();

                    break;
            }
        }
    }

    private void gotoPhotoAlbum() {
        Intent intent = new Intent(Intent.ACTION_PICK, null);
        intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(intent, REQUEST_CODE_OPEN_PHONE_ALBUM);
    }

    //调用自定义相机的回调代码----测试用，可以移植到任何地方
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_CAMERA_TAKE_PICTURE) {
            if (data != null) {
                String path = data.getStringExtra(KEY_IMAGE_PATH);
                Uri photoUri = Uri.parse(path);
                handleUrlImage(photoUri);
            }
        } else if (resultCode == Activity.RESULT_FIRST_USER && requestCode == REQUEST_CODE_CAMERA_TAKE_PICTURE) {
            showToastInfo(getStringById(R.string.instapay_save_failed));
        }
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_OPEN_PHONE_ALBUM) {
            // 从相册返回的数据
            Logger.e(this.getClass().getName(), "Result:" + data.toString());
            if (data != null) {
                // 得到图片的全路径
                Uri uri = data.getData();
                Logger.e(this.getClass().getName(), "Uri:" + String.valueOf(uri));
                handleUrlImage(uri);
            }
        }
    }

    public void handleUrlImage(Uri photoUri) {
        if (photoUri != null) {
            //若生成了uri，则表示该文件添加成功
            //使用流将该uri中的内容写入字节数组即可
            byte[] image = null;
            try {
                InputStream inputStream = getContentResolver().openInputStream(photoUri);
                if (null == inputStream || 0 == inputStream.available()) {
                    return;
                }
                image = new byte[inputStream.available()];
                inputStream.read(image);
                inputStream.close();

                if (image != null && image.length > 0) {
                    //如果上传的拖大于5M，则弹框提示不让上传
                    if (image.length > 5 * 1024 * 1024) {
                        showToastInfo(getStringById(R.string.bdo_check_image_size));
                    } else {
                        //拍照成功上传数据给H5
                        if (mCallbackName != null) {
                            AndroidCallJs_Upload_Image(mCallbackName, image);
                        }
                    }
                }
            } catch (IOException e) {

            }
        }
    }

}
