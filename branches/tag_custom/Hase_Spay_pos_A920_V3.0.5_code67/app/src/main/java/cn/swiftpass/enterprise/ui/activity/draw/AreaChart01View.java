package cn.swiftpass.enterprise.ui.activity.draw;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import org.xclcharts.chart.AreaChart;
import org.xclcharts.chart.AreaData;
import org.xclcharts.common.DensityUtil;
import org.xclcharts.common.IFormatterDoubleCallBack;
import org.xclcharts.common.IFormatterTextCallBack;
import org.xclcharts.event.click.PointPosition;
import org.xclcharts.renderer.XEnum;
import org.xclcharts.renderer.info.AnchorDataPoint;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.DailyTransactionDataBean;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalItemInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;


/**
 * @author XiongChuanLiang<br/>(xcl_168@aliyun.com)
 * @ClassName AreaChart01View
 * @Description 面积图例子
 */

public class AreaChart01View extends DemoView implements Runnable {
    private String TAG = "AreaChart01View";
    private Context context;

    //标签集合
    private Map<String, String> map = new HashMap<String, String>();
    private AreaChart chart = new AreaChart(map);

    private LinkedList<String> mLabels = new LinkedList<String>();
    //数据集合
    private LinkedList<AreaData> mDataset = new LinkedList<AreaData>();

    private Paint mPaintTooltips = new Paint(Paint.ANTI_ALIAS_FLAG);

    private List<DailyTransactionDataBean> dayStatistics;
    private List<Double> MoneyList = new ArrayList<>();
    private List<Double> CountList = new ArrayList<>();

    private String mPageType;//是金额还是笔数
    private String mreportType;//是月报还是周报


    public AreaChart01View(Context context, List<DailyTransactionDataBean> dayStatistics,String pageType,String reportType) {
        super(context);
        this.context = context;
        this.dayStatistics = dayStatistics;
        this.mPageType = pageType;
        this.mreportType=reportType;

        MoneyList.clear();
        CountList.clear();

        for (DailyTransactionDataBean info : dayStatistics) {
            if(mPageType.equalsIgnoreCase("1")){
                if (info.getTransactionAmount() >= 0) {
                    double money = DateUtil.StringToMoney(DateUtil.formatMoneyUtils(info.getTransactionAmount()));
                    MoneyList.add(money);
                }
            }else if(mPageType.equalsIgnoreCase("2")){
                if (info.getTransactionCount() >= 0) {
                    CountList.add((double)info.getTransactionCount());
                }
            }
        }
        initView();
    }


    double setAxisStepsLen(double max, double min) {


        if (min >= 100 && min < 1000) {
            chart.getDataAxis().setAxisSteps(50);
            return 50;
        } else if (min >= 1000 && min < 10000) {
            chart.getDataAxis().setAxisSteps(500);
            return 500;
        } else if (min >= 10000 && min < 100000) {
            chart.getDataAxis().setAxisSteps(5000);
            return 5000;
        } else if (min >= 100000 && min < 1000000) {
            chart.getDataAxis().setAxisSteps(100000);
            return 50000;
        } else if (min >= 1000000 && min < 10000000) {
            chart.getDataAxis().setAxisSteps(1000000);
            return 50000;
        } else if (min >= 10000000 && min < 10000000) {
            chart.getDataAxis().setAxisSteps(10000000);
            return 50000;
        } else if (min >= 100000000) {
            chart.getDataAxis().setAxisSteps(10000000);
            return 50000;
        } else if (min < 100 && min >= 50) {
            chart.getDataAxis().setAxisSteps(10);
            return 5;
        } else if (min >= 10 && min <= 50) {
            chart.getDataAxis().setAxisSteps(10);
            return 5;
        } else if (min > 0 && min < 0.1) {
            chart.getDataAxis().setAxisSteps(0.02);
            return 0.01;
        } else if (min >= 0.1 && min < 1) {
            chart.getDataAxis().setAxisSteps(0.1);
            return 0.1;
        } else if (min > 0 && min < 10) {
            chart.getDataAxis().setAxisSteps(1);
            return 1;
        } else {
            chart.getDataAxis().setAxisSteps(1);
            return 1;
        }
    }


    public AreaChart01View(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public AreaChart01View(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
    }

    private void initView() {
        chartLabels();
        chartDataSet();
        chartRender();

        new Thread(this).start();

        //綁定手势滑动事件
        this.bindTouch(this, chart);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        //图所占范围大小
        chart.setChartRange(w, h);
    }


    private void chartRender() {
        try {
            //设置绘图区默认缩进px值,留置空间显示Axis,Axistitle....
            int[] ltrb = getBarLnDefaultSpadding();
            chart.setPadding(DensityUtil.dip2px(getContext(), 60), ltrb[1], ltrb[2], ltrb[3]);

            //轴数据源
            //标签轴
            chart.setCategories(mLabels);
            //数据轴
            chart.setDataSource(mDataset);
            chart.setCrurveLineStyle(XEnum.CrurveLineStyle.BEELINE);

            chart.getDataAxis().setAxisMin(0);
            double max = 0d;
            if(mPageType.equalsIgnoreCase("1")){
                if (MoneyList.size() > 0){
                    max = Collections.max(MoneyList);
                    if (max < 4) {
                        max = 4;
                        chart.getDataAxis().setAxisMax(max);
                    } else {
                        chart.getDataAxis().setAxisMax(max);
                    }
                    chart.getDataAxis().setAxisSteps(max / 4d + 0.00249d);
                    chart.getDataAxis().setTickLabelMargin(DensityUtil.dip2px(getContext(), 30));
                }

            }else if(mPageType.equalsIgnoreCase("2")){
                if (CountList.size() > 0){
                    max = Collections.max(CountList);
                    if (max < 4) {
                        max = 4;
                        chart.getDataAxis().setAxisMax(max);
                    } else {
                        chart.getDataAxis().setAxisMax(max);
                    }
                    chart.getDataAxis().setAxisSteps(max / 4 + 0.00249d);
                    chart.getDataAxis().setTickLabelMargin(DensityUtil.dip2px(getContext(), 30));
                }
            }

            //网格
            chart.getPlotGrid().showHorizontalLines();
            chart.getPlotGrid().hideVerticalLines();

            chart.getPlotGrid().getHorizontalLinePaint().setStrokeWidth(1.0f);
            chart.getPlotGrid().setHorizontalLineStyle(XEnum.LineStyle.SOLID);
            chart.getPlotGrid().getHorizontalLinePaint().setColor(Color.parseColor("#E4E4E4"));

            //把顶轴和右轴隐藏
            //chart.hideTopAxis();
            //chart.hideRightAxis();
            //把轴线和刻度线给隐藏起来
            chart.getDataAxis().hideAxisLine();
            chart.getDataAxis().showAxisLabels();
            chart.getDataAxis().hideTickMarks();
            chart.getDataAxis().getTickLabelPaint().setTextSize(getResources().getDimension(R.dimen.sp_10));
            chart.getDataAxis().getTickLabelPaint().setColor(Color.parseColor("#999999"));
            chart.getCategoryAxis().setPaintLine(true);

            chart.getCategoryAxis().showAxisLine();
            chart.getCategoryAxis().hideTickMarks();
            chart.getCategoryAxis().showAxisLabels();

            if (mreportType.equalsIgnoreCase("3")) {
                chart.setShow(false);
            } else {
                chart.setShow(true);
            }

            chart.getCategoryAxis().getAxisPaint().setColor(Color.parseColor("#E4E4E4"));
            chart.getCategoryAxis().getAxisPaint().setStrokeWidth(0.5f);
            chart.getCategoryAxis().setTickLabelMargin(DensityUtil.dip2px(getContext(), 31));
            chart.getCategoryAxis().getTickLabelPaint().setTextSize(getResources().getDimension(R.dimen.sp_10));
            chart.getCategoryAxis().getTickLabelPaint().setColor(Color.parseColor("#999999"));

            chart.getCategoryAxis().setLabelFormatter(new IFormatterTextCallBack(){
                @Override
                public String textFormatter(String value) {
                    if(!StringUtil.isEmptyOrNull(value)){
                        try {
                            Date date = new Date();
                            date= stringToDate(value,"yyyy-MM-dd");
                            return DateUtil.dateToString(date,"MM-dd");
                        }catch (Exception e){
                        }
                    }
                    return null;
                }
            });

            //透明度
            chart.setAreaAlpha(200);
            chart.disablePanMode();
            chart.disableScale();

            //激活点击监听
            chart.ActiveListenItemClick();
            //为了让触发更灵敏，可以扩大5px的点击监听范围
            chart.extPointClickRange(25);

            //定义数据轴标签显示格式
            chart.getDataAxis().setLabelFormatter(new IFormatterTextCallBack() {

                @Override
                public String textFormatter(String value) {
                    // TODO Auto-generated method stub
                    if(mPageType.equalsIgnoreCase("1")){
                        double valueDouble = DateUtil.convertToDouble(value,0);
                        BigDecimal BigValue = new BigDecimal(valueDouble);
                        BigDecimal res = BigValue.divide(new BigDecimal(1),2,BigDecimal.ROUND_HALF_UP);
                        try {
                            String temp = formatMoneyToString(res.doubleValue());
                            return temp;
                        } catch (Exception e) {
                            e.printStackTrace();
                            return value;
                        }
                    }else if(mPageType.equalsIgnoreCase("2")){
                        double valueDouble = DateUtil.convertToDouble(value,0);
                        int count = (int)valueDouble;
                        try {
                            String temp = formatCountToString(count);
                            return temp;
                        } catch (Exception e) {
                            e.printStackTrace();
                            return value;
                        }
                    }
                    return null;

                }

            });

            //设定交叉点标签显示格式
            chart.setItemLabelFormatter(new IFormatterDoubleCallBack() {
                @Override
                public String doubleFormatter(Double value) {
                    DecimalFormat df = new DecimalFormat("#0");
                    String label = df.format(value).toString();
                    return label;
                }
            });

            chart.showDyLine();
            chart.getDyLine().getLinePaint().setColor(Color.parseColor("#AA000000"));
            chart.getDyLine().getLinePaint().setStrokeWidth(2.0f);
            chart.getDyLine().setDyLineStyle(XEnum.DyLineStyle.Vertical);

            //chart.disablePanMode();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.e(TAG, e.toString());
        }
    }

    private void chartDataSet() {
        //将标签与对应的数据集分别绑定
        List<Double> dataSeries1 = new LinkedList<Double>();
        dataSeries1.addAll(MoneyList);
        List<Double> dataSeries2 = new LinkedList<Double>();
        dataSeries2.addAll(CountList);

        if(mPageType.equalsIgnoreCase("1")){
            AreaData lineAmount = new AreaData("", dataSeries1, Color.parseColor("#A9D0FF"), Color.parseColor("#CBE3FF"));
            //设置线上每点对应标签的颜色
            lineAmount.getDotLabelPaint().setColor(Color.parseColor("#CBE3FF"));
            //设置点标签
            lineAmount.setLabelVisible(false);
            lineAmount.getLinePaint().setStrokeWidth(DensityUtil.dip2px(context, 2));
            lineAmount.getDotPaint().setColor(Color.parseColor("#A9D0FF"));
            lineAmount.setDotStyle(XEnum.DotStyle.RING);
            lineAmount.setDotRadius(DensityUtil.dip2px(context, 3));
            lineAmount.setApplayGradient(true);
            lineAmount.setAreaBeginColor(Color.parseColor("#CBE3FF"));
            lineAmount.setAreaEndColor(Color.parseColor("#FFFFFFFF"));
            mDataset.add(lineAmount);
        }else{
            AreaData lineCount = new AreaData("", dataSeries2, Color.parseColor("#A9D0FF"), Color.parseColor("#CBE3FF"));
            //设置线上每点对应标签的颜色
            lineCount.getDotLabelPaint().setColor(Color.parseColor("#CBE3FF"));
            //设置点标签
            lineCount.setLabelVisible(false);
            lineCount.getLinePaint().setStrokeWidth(DensityUtil.dip2px(context, 2));
            lineCount.setDotRadius(DensityUtil.dip2px(context, 3));
            lineCount.getDotPaint().setColor(Color.parseColor("#A9D0FF"));
            lineCount.setDotStyle(XEnum.DotStyle.RING);
            lineCount.setApplayGradient(true);
            lineCount.setAreaBeginColor(Color.parseColor("#CBE3FF"));
            lineCount.setAreaEndColor(Color.parseColor("#FFFFFFFF"));
            mDataset.add(lineCount);
        }
    }



    private void chartLabels() {
        for (DailyTransactionDataBean info : dayStatistics) {
            String transactionDate = info.getTransactionStartDate();
            mLabels.add(transactionDate);
        }
    }

    public static Date stringToDate(String strTime, String formatType) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(formatType);
        Date date = null;
        date = formatter.parse(strTime);
        return date;
    }

    @Override
    public void render(Canvas canvas) {
        try {
            chart.render(canvas);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        super.onTouchEvent(event);
        chart.hideDyLine();
        if (event.getAction() == MotionEvent.ACTION_UP) {
            triggerClick(event.getX(), event.getY());
        }
        return true;
    }


    //触发监听
    private void triggerClick(float x, float y) {

         if (!chart.getListenItemClickStatus()) {
            //交叉线
            if (chart.getDyLineVisible())
                this.invalidate();
        } else {

            PointPosition record = chart.getPositionRecord(x, y);
            if (record == null) {
                chart.hideDyLine();
                return;
            } else {
                chart.showDyLine();
                if (chart.getDyLineVisible())
                    chart.getDyLine().setCurrentXY(record.getPosition().x, record.getPosition().y);
            }
            AreaData lData = mDataset.get(record.getDataID());
            Double lValue = lData.getLinePoint().get(record.getDataChildID());

            float r = record.getRadius();
            chart.showFocusPointF(record.getPosition(), r + r * 0.5f);
            //chart.getFocusPaint().setStyle(Style.STROKE);
            chart.getFocusPaint().setStrokeWidth(3);


            //在点击处显示tooltip
            mPaintTooltips.setColor(Color.WHITE);
            mPaintTooltips.setTextSize(getResources().getDimension(R.dimen.sp_10));
            chart.getToolTip().getBackgroundPaint().setColor(Color.parseColor("#000000"));
            chart.getToolTip().getBackgroundPaint().setAlpha(170);
            //chart.getToolTip().setCurrentXY(x,y);
            chart.getToolTip().setCurrentXY(record.getPosition().x + 10, record.getPosition().y);
            if ((record.getPosition().x+100 >= chart.getLastX()) || (record.getPosition().x+100 >= chart.getLastTowX())) {
                chart.getToolTip().setAlign(Align.LEFT);
            } else {
                chart.getToolTip().setAlign(Align.RIGHT);
            }
            chart.getToolTip().setStyle(XEnum.DyInfoStyle.RECT);

//            chart.getToolTip().addToolTip("   Key:" + lData.getLineKey(), mPaintTooltips);

            Map<String, String> map = chart.getMap();
            if(mPageType.equalsIgnoreCase("1")){
                String val = map.get(record.getPosition().x + "");
                for (DailyTransactionDataBean info : dayStatistics) {
                    if (info.getTransactionStartDate().equalsIgnoreCase(val)) {
                        chart.getToolTip().addToolTip(" " + info.getTransactionStartDate() + " ", mPaintTooltips);
                    }
                }
                chart.getToolTip().addToolTip(" " + MainApplication.getFeeFh() + DateUtil.formatMoneyUtil(lValue) + " ", mPaintTooltips);

            }else if(mPageType.equalsIgnoreCase("2")){
                String vaule = map.get(record.getPosition().x + "");
                for (DailyTransactionDataBean info : dayStatistics) {
                    if (info.getTransactionStartDate().equalsIgnoreCase(vaule)) {
                        chart.getToolTip().addToolTip(" " + info.getTransactionStartDate() + " ", mPaintTooltips);
                    }
                }
                chart.getToolTip().addToolTip(" " + Math.round(lValue), mPaintTooltips);
            }

            this.invalidate();
        }

    }


    @Override
    public void run() {
        // TODO Auto-generated method stub
        try {
            chartAnimation();
        } catch (Exception e) {
            Thread.currentThread().interrupt();
        }
    }

    private void chartAnimation() {
        try {

            chart.getPlotLegend().hide();

            int[] ltrb = getBarLnDefaultSpadding();
            for (int i = 8; i > 0; i--) {
                Thread.sleep(100);
                // chart.setPadding(ltrb[0],ltrb[1], i *  ltrb[2], ltrb[3]);

                //if(1 == i)drawTitle();
                postInvalidate();
            }

        } catch (Exception e) {
            Thread.currentThread().interrupt();
        }
    }

    private void drawTitle() {
        //标题
        chart.setTitle("区域图(Area Chart)");
        chart.addSubtitle("(XCL-Charts Demo)");
        //轴标题
        chart.getAxisTitle().setLowerTitle("(年份)");

        //显示图例
        chart.getPlotLegend().show();

        //激活点击监听
        chart.ActiveListenItemClick();
        //为了让触发更灵敏，可以扩大5px的点击监听范围
        chart.extPointClickRange(25);
        chart.showClikedFocus();

        //批注
        List<AnchorDataPoint> mAnchorSet = new ArrayList<AnchorDataPoint>();

        AnchorDataPoint an2 = new AnchorDataPoint(1, 3, XEnum.AnchorStyle.CIRCLE);
        an2.setBgColor(Color.GRAY);
        an2.setAnchorStyle(XEnum.AnchorStyle.CIRCLE);


        AnchorDataPoint an3 = new AnchorDataPoint(1, 4, XEnum.AnchorStyle.CIRCLE);
        an3.setBgColor(Color.RED);
        an3.setAnchorStyle(XEnum.AnchorStyle.RECT);
        an3.setAreaStyle(XEnum.DataAreaStyle.STROKE);

        //从点到底的标识线
        AnchorDataPoint an4 = new AnchorDataPoint(2, 2, XEnum.AnchorStyle.TOBOTTOM);
        an4.setBgColor(Color.YELLOW);
        an4.setLineWidth(15);

        AnchorDataPoint an5 = new AnchorDataPoint(2, 1, XEnum.AnchorStyle.TORIGHT);
        an5.setBgColor(Color.WHITE);
        an5.setLineWidth(15);

        //AnchorDataPoint an6 = new AnchorDataPoint(2,1,XEnum.AnchorStyle.HLINE);
        //an6.setBgColor(Color.WHITE);
        //an6.setLineWidth(15);

        mAnchorSet.add(an2);
        mAnchorSet.add(an3);
        mAnchorSet.add(an4);
        mAnchorSet.add(an5);
        //mAnchorSet.add(an6);
        chart.setAnchorDataPoint(mAnchorSet);
    }

    //對數據進行處理---按照以下算法和格式
//    交易数据图表纵坐标逻辑：
//    若a为实际数据，b为展示数据
//    if  0<=a<1000:
//    b=a
//    elif  1000<=a<1,000,000:
//    b= “a/1000” + “K”
//    elif  1,000,000<=a<1,000,000,000:
//    b= “a/1,000,000 ” + “M”
//            else:
//    b= “a/1,000,000,000” + “B”
//
//    此处b保留一位小数

    public String formatMoneyToString(Double Money){
        String formatMoney;
        double divideNum = 1.0f;
        if(0 <= Money && Money < 1000 ){
            divideNum = 1.0f;
        }else if(Money >= 1000 && Money < 1000000){
            divideNum = 1000.0f;
        }else if(Money >= 1000000 && Money < 1000000000){
            divideNum = 1000000.0f;
        }else{
            divideNum = 1000000000.0f;
        }
        BigDecimal BigDecimalDivideNum = new BigDecimal(divideNum);
        BigDecimal BigDecimalMoney = new BigDecimal(Money).divide(BigDecimalDivideNum,1,BigDecimal.ROUND_HALF_UP);

        if(0 <= Money && Money < 1000 ){
            formatMoney = Money+"";
        }else if(Money >= 1000 && Money < 1000000){
            formatMoney = BigDecimalMoney.toString()+"K";
        }else if(Money >= 1000000 && Money < 1000000000){
            formatMoney = BigDecimalMoney.toString()+"M";
        }else{
            formatMoney = BigDecimalMoney.toString()+"B";
        }
        return formatMoney;
    }

    public String formatCountToString(int count){
        String formatCount;
        int divideNum = 1;
        if(0 <= count && count < 1000 ){
            divideNum = 1;
        }else if(count >= 1000 && count < 1000000){
            divideNum = 1000;
        }else if(count >= 1000000 && count < 1000000000){
            divideNum = 1000000;
        }else{
            divideNum = 1000000000;
        }
        BigDecimal BigDecimalDivideNum = new BigDecimal(divideNum);
        BigDecimal BigDecimalCount = new BigDecimal(count).divide(BigDecimalDivideNum,0,BigDecimal.ROUND_HALF_UP);

        if(0 <= count && count < 1000 ){
            formatCount = count+"";
        }else if(count >= 1000 && count < 1000000){
            formatCount = BigDecimalCount.toString()+"K";
        }else if(count >= 1000000 && count < 1000000000){
            formatCount = BigDecimalCount.toString()+"M";
        }else{
            formatCount = BigDecimalCount.toString()+"B";
        }
        return formatCount;
    }
}
