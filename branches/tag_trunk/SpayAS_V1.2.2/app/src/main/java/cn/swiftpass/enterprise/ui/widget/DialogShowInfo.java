/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.WelcomeActivity;

/**
 * 弹出提示框
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class DialogShowInfo extends Dialog {

    private Context context;

    private TextView title;

    private TextView content;

    private Button btnOk;

    private TextView btnCancel;

    private ViewGroup mRootView;

    private DialogShowInfo.HandleBtn handleBtn;

    private DialogShowInfo.HandleBtnCancle cancleBtn;

    private View line_img, line_img_bt;

    // 更多
    public static final int FLAG = 0;

    // 最终提交
    public static final int SUBMIT = 1;

    // 注册成功
    public static final int REGISTFLAG = 2;

    // 微信支付 是否授权激活
    public static final int EXITAUTH = 3;

    // 微信支付 授权激活 重新登录
    public static final int EXITAUTHLOGIN = 4;

    //提交商户信息
    public static final int SUBMITSHOPINFO = 5;

    //判断网络连接状态
    public static final int NETWORKSTATUE = 8;

    //优惠卷列表，长按提示删除提示框
    public static final int SUBMIT_DEL_COUPON_INFO = 6;

    //优惠卷发布
    public static final int SUBMIT_COUPON_INFO = 7;

    public static final int SUBMIT_SHOP = 9;

    public static final int SCAN_PAY = 10;

    public static final int VARD = 11;

    public static final int UNIFED_DIALOG = 12;

    private OnItemLongDelListener mDelListener;

    private int position;

    private OnSubmitCouponListener mOnSubmitCouponListener;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public DialogShowInfo(Context context, String titleStr, String contentStr, String btnOkStr, String btnCancle, int flagMore, DialogShowInfo.HandleBtn handleBtn, DialogShowInfo.HandleBtnCancle cancleBtn) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_info, null);
        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);

        this.context = context;
        this.handleBtn = handleBtn;
        this.cancleBtn = cancleBtn;

        initView(titleStr, contentStr, btnOkStr, btnCancle, flagMore);

        setLinster(flagMore);

    }

    public void setBtnOkText(String string) {
        if (btnOk != null) {
            btnOk.setText(string);
        }
    }

    public DialogShowInfo(Context context, String titleStr, String contentStr, String btnOkStr, int flagMore, DialogShowInfo.HandleBtn handleBtn, DialogShowInfo.HandleBtnCancle cancleBtn) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_info, null);
        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);

        this.context = context;
        this.handleBtn = handleBtn;
        this.cancleBtn = cancleBtn;

        initView(titleStr, contentStr, btnOkStr, null, flagMore);

        setLinster(flagMore);

    }

    public void setMessage(String msg) {
        this.content.setText(msg);
    }

    public void setmDelListener(OnItemLongDelListener mDelListener, int position) {
        this.mDelListener = mDelListener;
        this.position = position;
    }

    public void setmOnSubmitCouponListener(OnSubmitCouponListener mOnSubmitCouponListener) {
        this.mOnSubmitCouponListener = mOnSubmitCouponListener;
    }

    /**
     * 设置监听
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void setLinster(final int flagMore) {

        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                dismiss();
                handleBtn.handleCancleBtn();
            }

        });
        btnOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                dismiss();
                handleBtn.handleOkBtn();

            }
        });
    }

    /**
     * 初始化
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initView(String titleStr, String contentStr, String btnOkStr, String btnCancelStr, int flag) {
        title = (TextView) findViewById(R.id.title);

        content = (TextView) findViewById(R.id.content);

        btnOk = (Button) findViewById(R.id.btnOk);

        btnCancel = (TextView) findViewById(R.id.btnCancel);

        line_img = (View) findViewById(R.id.line_img);

        line_img_bt = (View) findViewById(R.id.line_img_bt);
        switch (flag) {
            case UNIFED_DIALOG:
                line_img.setVisibility(View.VISIBLE);
                btnCancel.setVisibility(View.VISIBLE);
                break;

            case REGISTFLAG: // 注册成功
                btnCancel.setVisibility(View.GONE);

                title.setTextColor(context.getResources().getColor(R.color.prompt_title));

                content.setTextColor(context.getResources().getColor(R.color.content_title));
                line_img.setVisibility(View.GONE);

                break;
            case EXITAUTHLOGIN: // 激活从新登录
                btnOk.setTextColor(Color.BLUE);
                break;

            case EXITAUTH:
                if (null == btnCancelStr) {

                    btnCancel.setText(R.string.tx_go_on);
                } else {
                    btnCancel.setText(btnCancelStr);
                }
                btnCancel.setVisibility(View.VISIBLE);
                line_img.setVisibility(View.VISIBLE);
                //                btnOk.setTextColor(Color.BLUE);
                break;
            case FLAG:
                btnCancel.setVisibility(View.GONE);
                line_img.setVisibility(View.GONE);
                break;
            default:
                break;
        }

        title.setText(titleStr);
        content.setText(contentStr);
        btnOk.setText(btnOkStr);
        if (flag == SCAN_PAY) {
            btnOk.setVisibility(View.GONE);
            line_img.setVisibility(View.GONE);
            line_img_bt.setVisibility(View.GONE);
        }
    }

    public void showPage(Class clazz) {
        Intent intent = new Intent(context, clazz);
        context.startActivity(intent);
    }

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    public interface HandleBtn {
        void handleOkBtn();

        void handleCancleBtn();
    }

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    public interface HandleBtnCancle {
        void handleCancleBtn();
    }

    /***
     *
     * 长按删除优惠
     *
     * @author wang_lin
     * @version [2.1.0, 2014-4-23]
     */
    public interface OnItemLongDelListener {
        public void onItemLongDelMessage(int position);
    }

    /***
     *
     * 发布和修改优惠价提示
     * @author wang_lin
     * @version [2.1.0, 2014-4-23]
     */
    public interface OnSubmitCouponListener {
        public void onSubmitCouponListenerOk();

        public void onSubmitCouponListenerCancel();
    }
}
