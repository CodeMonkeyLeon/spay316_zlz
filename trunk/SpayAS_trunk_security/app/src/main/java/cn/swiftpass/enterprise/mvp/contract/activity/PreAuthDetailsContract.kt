package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView


/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @烟囱犹如平地耸立起来的巨人
 * @望着布满灯火的大地
 * @不断地吸着烟卷
 * @思索着一种谁也不知道的事情
 */
class PreAuthDetailsContract {


    interface View : BaseView {

        fun synchronizeOrderSuccess(response: Order?)

        fun synchronizeOrderFailed(error: Any?)


        fun querySpayOrderSuccess(response: ArrayList<Order>?)

        fun querySpayOrderFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {


        fun querySpayOrder(
            listStr: ArrayList<String?>?,
            payTypeList: ArrayList<String?>?,
            isRefund: Int,
            page: Int,
            order: String?,
            startDate: String?
        )


        fun synchronizeOrder(
            authNo: String?,
            outRequestNo: String?
        )

    }


}