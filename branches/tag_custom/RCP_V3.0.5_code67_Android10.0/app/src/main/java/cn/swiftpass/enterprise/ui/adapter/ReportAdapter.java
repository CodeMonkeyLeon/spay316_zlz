package cn.swiftpass.enterprise.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.CashierReport;
import cn.swiftpass.enterprise.ui.widget.ItemDailyStatement;

/**
 * 统计
 * User: Alan
 * Date: 13-9-28
 * Time: 上午10:02
 */
public class ReportAdapter extends ArrayListAdapter<CashierReport>
{
    
    public ReportAdapter(Context context)
    {
        super(context);
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ItemDailyStatement itemOrder = null;
        if (convertView != null && convertView instanceof ItemDailyStatement)
        {
            itemOrder = (ItemDailyStatement)convertView;
        }
        if (convertView == null)
        {
            itemOrder =
                (ItemDailyStatement)LayoutInflater.from(mContext).inflate(R.layout.listitem_dailystatement, null);
        }
        CashierReport d = (CashierReport)getItem(position);
        if(itemOrder != null){
            itemOrder.setData(d);
        }
        return itemOrder;
    }
    
}
