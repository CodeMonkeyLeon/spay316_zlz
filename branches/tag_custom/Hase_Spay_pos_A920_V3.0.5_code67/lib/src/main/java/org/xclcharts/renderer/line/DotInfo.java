package org.xclcharts.renderer.line;

/**
 * @ClassName DotInfo
 * @Description  用于暂存点信息的类
 * @author XiongChuanLiang<br/>(xcl_168@aliyun.com)
 *  
 */
public class DotInfo {
	
	public Double mValue = 0d;
	
	public Double mXValue = 0d;
	public Double mYValue = 0d;
			
	public float mX = 0.f;
	public float mY = 0.f;
	
	public DotInfo(){}
	
	public DotInfo(Double value,float x,float y)
	{
		 mValue = value ;
		 mX = x ;
		 mY = y ;			
	}
	
	
	public DotInfo(Double xValue,Double yValue,float x,float y)
	{
		 mXValue = xValue ;
		 mYValue = yValue ;
		 mX = x ;
		 mY = y ;			
	}
	
	public String getLabel()
	{
		return Double.toString(mXValue)+","+ Double.toString(mYValue);
	}

}
