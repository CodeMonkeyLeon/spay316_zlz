/*
 * 文 件 名:  ShowPicActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-5-18
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import cn.swiftpass.enterprise.intl.R;

/**
 * 引导指引图
 * 
 * @author  he_hui
 * @version  [版本号, 2016-5-18]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class ShowPicActivity extends BaseActivity
{
    
    private ImageView micropay_help_img;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.micropay_help_layout);
        
        micropay_help_img = getViewById(R.id.micropay_help_img);
        
        String payType = getIntent().getStringExtra("payType");
        
        //        if (!TextUtils.isEmpty(payType))
        //        {
        //            if (payType.equals(MainApplication.QQ_SACN_TYPE)
        //                || payType.equalsIgnoreCase(cn.swiftpass.enterprise.MainApplication.PAY_QQ_NATIVE1))
        //            {
        //                //手Q
        //                micropay_help_img.setImageResource(R.drawable.micropay_help_qq);
        //            }
        //            else if (payType.equals(MainApplication.ZFB_SCAN_TYPE) || payType.equals(MainApplication.ZFB_SCAN_TYPE2))
        //            {
        //                //支付宝扫码
        //                micropay_help_img.setImageResource(R.drawable.micropay_help_zfb);
        //            }
        //            else
        //            {
        //                //微信扫码
        //                micropay_help_img.setImageResource(R.drawable.micropay_help_weixin);
        //            }
        //        }
        
        micropay_help_img.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ShowPicActivity.this.finish();
            }
        });
    }
    
    public static void startActivity(Context context, String type)
    {
        Intent it = new Intent();
        it.setClass(context, ShowPicActivity.class);
        it.putExtra("payType", type);
        context.startActivity(it);
    }
}
