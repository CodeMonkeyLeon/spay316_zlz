package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.PreferenceUtil;

import static cn.swiftpass.enterprise.utils.PreferenceUtil.getBoolean;

/**
 * Created by aijingya on 2018/6/12.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(引导页)
 * @date 2018/6/12.14:29.
 */

public class GuideActivity extends Activity implements ViewPager.OnPageChangeListener{


    private static final String TAG = GuideActivity.class.getSimpleName();
    private ViewPager mViewPager;
    private LinearLayout ll_dots;
    private TextView tv_skip,tv_next;

    private int [] ViewIdLists = {R.layout.item_guide_view, R.layout.item_guide_view2, R.layout.item_guide_view3};

    View[] ViewLists ;

    private int currentItem = 0;
    private int pageCount;

    /**
     * 装点点的ImageView数组
     */
    private ImageView[] dots;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        if(getIsFirstLaunch()){//如果是第一次启动APP
            setContentView(R.layout.activity_guide_layout);
            mViewPager = (ViewPager)findViewById(R.id.vp_banner);
            ll_dots = (LinearLayout)findViewById(R.id.dots);
            tv_skip = (TextView)findViewById(R.id.tv_skip);
            tv_next = (TextView)findViewById(R.id.tv_next);

            tv_skip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startWelcomeActivity();
                    setIsFirstLaunch(false);
                }
            });

            tv_next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(currentItem == pageCount-1){
                        startWelcomeActivity();
                        setIsFirstLaunch(false);
                    }else{
                        currentItem++;
                        mViewPager.setCurrentItem(currentItem);
                    }
                }
            });

            //Load all the View to the array
            pageCount = ViewIdLists.length;
            ViewLists = new View[pageCount];
            for(int i = 0 ;i< ViewIdLists.length;i++){
                LayoutInflater inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
                View ItemView = inflater.inflate(ViewIdLists[i], null);
                ViewLists[i] = ItemView;
            }

            //将点点加入到ViewGroup中
            dots = new ImageView[pageCount];
            for(int i=0; i<dots.length; i++){
                ImageView imageView = new ImageView(this);
                imageView.setLayoutParams(new ViewGroup.LayoutParams(20,20));
                dots [i] = imageView;
                if(i == 0){
                    dots[i].setBackgroundResource(R.drawable.icon_circle1);
                }else{
                    dots[i].setBackgroundResource(R.drawable.icon_circle);
                }

                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                layoutParams.leftMargin = 15;
                layoutParams.rightMargin = 15;
                ll_dots.addView(imageView, layoutParams);
            }

            mViewPager.setAdapter(new ImagePagerAdapter());
            //设置监听，主要是设置点点的背景
            mViewPager.addOnPageChangeListener(this);

            currentItem = 0;
            mViewPager.setCurrentItem(currentItem);
        }else{ //否则直接启动登录页
            startWelcomeActivity();
        }
    }

    public void startWelcomeActivity(){
        Intent intent = new Intent(GuideActivity.this, WelcomeActivity.class);
        startActivity(intent);
        finish();
    }



    /**
     *
     * @Title: setIsFirstLaunch
     * @Description: TODO
     * @param isFirstLaunch
     * @throws
     */
    public void setIsFirstLaunch(boolean isFirstLaunch) {
        String version= getCurrentAppVersionName(GuideActivity.this);
        PreferenceUtil.commitBoolean("is_first_launch_"+version, isFirstLaunch);
    }


    /**
     *
     * @Title: getIsFirstLaunch
     * @Description: 是否是当前版本的第一次打开
     * @return
     * @throws
     */
    public boolean getIsFirstLaunch() {
        String version= getCurrentAppVersionName(GuideActivity.this);
        return PreferenceUtil.getBoolean("is_first_launch_" + version, true);
    }

    /**
     * 获取当前app的版本名字.
     *
     * @param context
     * @return
     */
    public static String getCurrentAppVersionName(Context context) {
        PackageInfo info = getCurrentAppPackageInfo(context);
        String version = info.versionName;
        return version;
    }

    /**
     * 获取当前app包信息对象.
     *
     * @param context
     * @return
     * @throws PackageManager.NameNotFoundException
     */
    private static PackageInfo getCurrentAppPackageInfo(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            String packageName = context.getPackageName();
            PackageInfo info = manager.getPackageInfo(packageName, 0);
            return info;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG,Log.getStackTraceString(e));
            throw new RuntimeException(e);
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
    }


    public class ImagePagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return pageCount;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {

            View mView = ViewLists[position % pageCount];
            try {
                ((ViewPager)container).addView(mView);
            }catch(Exception e){
                Log.e(TAG,Log.getStackTraceString(e));
            }

            return mView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        currentItem = position;
        if(currentItem == pageCount-1){
            tv_skip.setVisibility(View.INVISIBLE);
            tv_next.setText(getString(R.string.guide_login));
        }else{
            tv_skip.setVisibility(View.VISIBLE);
            tv_next.setText(getString(R.string.guide_next));
        }
        setImageBackground(position % pageCount);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * 设置选中的dot的背景
     * @param selectItems
     */
    private void setImageBackground(int selectItems){
        for(int i=0; i<dots.length; i++){
            if(i == selectItems){
                dots[i].setBackgroundResource(R.drawable.icon_circle1);
            }else{
                dots[i].setBackgroundResource(R.drawable.icon_circle);
            }
        }
    }
}
