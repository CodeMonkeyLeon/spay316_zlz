package cn.swiftpass.enterprise.ui.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tencent.stat.StatService;

import java.util.Locale;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.shop.PersonalManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.upgrade.UpgradeManager;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.MerchantTempDataModel;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.scan.CaptureActivity;
import cn.swiftpass.enterprise.ui.activity.setting.SettingAboutActivity;
import cn.swiftpass.enterprise.ui.activity.shop.ShopkeeperActivity;
import cn.swiftpass.enterprise.ui.activity.user.GoodsNameSetting;
import cn.swiftpass.enterprise.ui.widget.CommonConfirmDialog;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 设置 会自动检查版本 User: ALAN Date: 13-10-28 Time: 下午5:11 To change this template use
 * File | Settings | File Templates.
 */
public class SettingActivity extends TemplateActivity implements View.OnClickListener {

    private static final String TAG = SettingActivity.class.getSimpleName();

    private Context mContext;

    //private TextView tvVersion;

    // private TextView tvUser;

    private LinearLayout llChangePwd, llShopInfo,  empManager, lay_scan;
    //private LinearLayout llCoupon;

    private View lineChangePwd;

    //private ImageView ivNewVersion;

    private TextView tv_debug, textView1, textView2;

    //private LayoutInflater inflater;

    private View ll_shopinfo, sum_id, empManager_id, ll_shopinfo_id;

    //private View view, bodyLay_id, refundLay_id, view_manager, view_last;

    private Button  but_push;

    private SharedPreferences sp;

    private LinearLayout bodyLay, write_off, ll_static_code, lay_prize;

    private View marketing_id;

    private LinearLayout marketingLay, lay_choise,  lay_push_money, ly_pay_method;

    private TextView tv_mch_name, tv_mch_id;

    //private TextView tv_balance;

    //private View vi_pay_method;

    private ImageView iv_title;

    DynModel dynModel;

    Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {

            //            MerchantTempDataModel dataModel = (MerchantTempDataModel)msg.obj;
            //            Integer merchantType = dataModel.getMerchantType();
            //            Intent intent = new Intent(SettingActivity.this, ShopkeeperActivity.class);
            //            intent.putExtra("merchantType", merchantType);
            //            SettingActivity.this.startActivity(intent);
            if (msg.what == HandlerManager.PAY_ACTIVE_SETTING_SCAN) { //扫码激活,结算的出现
                //                sum_id.setVisibility(View.VISIBLE);

            }
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sp = this.getSharedPreferences("login", 0);

        initViews();

        HandlerManager.registerHandler(HandlerManager.PAY_ACTIVE_SETTING_SCAN, handler);

        dynModel = (DynModel) SharedPreUtile.readProduct("dynModel" + ApiConstant.bankCode);
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonIsVisible(false);
        titleBar.setTitle(R.string.tv_tab_manager);
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        HandlerManager.unregisterHandler(HandlerManager.GETSHOPINFODONE, handler);
    }

    private void initViews() {
        setContentView(R.layout.activity_setting_new);
        mContext = this;
        iv_title = getViewById(R.id.iv_title);


        tv_mch_name = getViewById(R.id.tv_mch_name);
        tv_mch_id = getViewById(R.id.tv_mch_id);

        //vi_pay_method = getViewById(R.id.vi_pay_method);
        ly_pay_method = getViewById(R.id.ly_pay_method);
        ly_pay_method.setOnClickListener(this);
        lay_push_money = getViewById(R.id.lay_push_money);
        lay_push_money.setVisibility(View.GONE);
        //tv_balance = getViewById(R.id.tv_balance);
        but_push = getViewById(R.id.but_push);
        but_push.setOnClickListener(this);
        sum_id = getViewById(R.id.sum_id);
        lay_choise = getViewById(R.id.lay_choise);
        lay_choise.setOnClickListener(this);
        marketingLay = getViewById(R.id.marketingLay);
        lay_prize = getViewById(R.id.lay_prize);
        marketingLay.setOnClickListener(this);
        marketing_id = getViewById(R.id.marketing_id);
        lay_prize.setOnClickListener(this);
        //view_last = getViewById(R.id.view_last);
        //view_manager = getViewById(R.id.view_manager);
        ll_shopinfo_id = getViewById(R.id.ll_shopinfo_id);
        empManager_id = getViewById(R.id.empManager_id);
        //refundLay_id = getViewById(R.id.refundLay_id);
        ll_static_code = getViewById(R.id.ll_static_code);
        ll_static_code.setOnClickListener(this);
        lay_scan = getViewById(R.id.lay_scan);
        lay_scan.setOnClickListener(this);
        //tvVersion = getViewById(R.id.tv_version);
        //        tvVersion.setText("V" + AppHelper.getVerName(mContext));
        //bodyLay_id = getViewById(R.id.bodyLay_id);
        // tvUser = getViewById(R.id.tv_user);

        tv_debug = getViewById(R.id.tv_debug);
        tv_debug.setText((GlobalConstant.isDebug == true ? "(" + getString(R.string.tx_try_version) + ")" : ""));
        tv_debug.setTextColor(Color.BLUE);

        empManager = getViewById(R.id.empManager);
        empManager.setOnClickListener(this);

        textView1 = getViewById(R.id.textView1);
        bodyLay = getViewById(R.id.bodyLay);
        bodyLay.setOnClickListener(this);
        write_off = getViewById(R.id.write_off);
        write_off.setOnClickListener(this);

        String userNmae = sp.getString("user_name", "");
        textView1.setText(getString(R.string.tx_welcome) + userNmae);

        textView2 = getViewById(R.id.textView2);
        if (MainApplication.isAdmin.equals("1")) {

            if (MainApplication.mchName != null && !MainApplication.mchName.equals("null")) {
                textView2.setText(MainApplication.mchName);
            }
        } else {
            if (!TextUtils.isEmpty(MainApplication.realName)) {
                textView2.setText(MainApplication.realName);
            }
        }

        llChangePwd = getViewById(R.id.ll_changePwd);
        lineChangePwd = getViewById(R.id.line_changePwd);
        //ivNewVersion = getViewById(R.id.iv_newVersion);
        llChangePwd.setOnClickListener(this);
        ll_shopinfo = getViewById(R.id.ll_shopinfo);
        // llCoupon = getViewById(R.id.ll_coupon);
        llShopInfo = getViewById(R.id.ll_shopinfo);
        if ("".equals(MainApplication.pwd_tag)) {
            llChangePwd.setVisibility(View.GONE);
            lineChangePwd.setVisibility(View.GONE);
            // llCoupon.setVisibility(View.GONE);
            // getViewById(R.id.line_shopinfo).setVisibility(View.GONE);
            // llShopInfo.setVisibility(View.GONE);
        }
        if (MainApplication.isAdmin.equals("0")) {
            tv_mch_name.setText(MainApplication.realName);
            tv_mch_id.setText(PreferenceUtil.getString("user_name", MainApplication.userName));
            iv_title.setVisibility(View.GONE);
            ll_shopinfo.setEnabled(false);

        } else {
            ll_shopinfo.setEnabled(true);
            tv_mch_name.setText(MainApplication.getMchName());
            tv_mch_id.setText(MainApplication.getMchId());
        }
        llShopInfo.setOnClickListener(this);
        checkVersion(true);


        // 收银员 商户资料和收银员管理，退款管理 隐藏
        if (MainApplication.isAdmin.equals("0")) {
            sum_id.setVisibility(View.GONE);
            empManager.setVisibility(View.GONE);
            llShopInfo.setVisibility(View.GONE);
            bodyLay.setVisibility(View.GONE);
            ll_shopinfo.setVisibility(View.VISIBLE);
            //            bodyLay_id.setVisibility(View.GONE);
            empManager_id.setVisibility(View.GONE);
            ll_shopinfo_id.setVisibility(View.GONE);
            //            if (!MainApplication.isRefundAuth.equals("") && MainApplication.isRefundAuth.equals("1"))
            //            {
            //                refundLay.setVisibility(View.VISIBLE);
            //                refundLay_id.setVisibility(View.GONE);
            //            }
            //            else
            //            {
            //                refundLay_id.setVisibility(View.GONE);
            //                refundLay.setVisibility(View.GONE);
            //                view_manager.setVisibility(View.GONE);
            //                view_last.setVisibility(View.GONE);
            //            }
            //            //如果有活动权限则显示
            //            if (MainApplication.isActivityAuth == 0)
            //            {
            marketingLay.setVisibility(View.GONE);
            marketing_id.setVisibility(View.GONE);
            //            }
        }
        //        else
        //        {
        //            refundLay.setVisibility(View.VISIBLE);
        //        }

        //        if (MainApplication.payMethodSize == 0 || MainApplication.payMethodSize == 1)
        //        {
        //            vi_pay_method.setVisibility(View.GONE);
        //            ly_pay_method.setVisibility(View.GONE);
        //        }

    }

    // @Override
    // protected void setupTitleBar()
    // {
    // super.setupTitleBar();
    // titleBar.setLeftButtonVisible(false);
    // titleBar.setTitle(R.string.title_setting);
    // }
    //private Blance blance;

    /**
     * 退出
     */
    public void onExit(View v) {
        showExitDialog(SettingActivity.this);
    }

    /**
     * 关于我们
     */
    public void onAboutus(View v) {
        // ContentTextActivity.startActivity(mContext,
        // ApiConstant.URL_ABOUT_US,R.string.title_aboutus);
        AboutUsActivity.startActivity(mContext);
    }

    /**
     * 修改密码
     */
    public void onChangePwd() {
        ChanagePwdActivity.startActivity(mContext);
    }

    /**
     * 检查版本
     */
    public void onCheckVersion(View v) {
        try {
            StatService.trackCustomEvent(SettingActivity.this, "SPConstTapSetManager", "设置");
        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }
        // 进行网络判断
        //        if (!NetworkUtils.isNetworkAvailable(this))
        //        {
        //            showToastInfo(R.string.network_exception);
        //            return;
        //        }
        //        //检查过了 有数据了直接跳出询问用户是否需要升级 暂时屏蔽掉
        //        if (upVerInfo != null)
        //        {
        //            showUpgradeInfoDialog(upVerInfo, new ComDialogListener(upVerInfo));
        //        }
        //        else
        //        {
        //            checkVersion(false);
        //        }
        showPage(SettingMoreActivity.class);
    }

    /**
     * 进入帮助页
     */
    public void onHelpPage(View v) {
        try {
            StatService.trackCustomEvent(SettingActivity.this, "SPConstTapFQAManager", "常见问题");
        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }

        if (null != dynModel && !StringUtil.isEmptyOrNull(dynModel.getQuestionLink())) {
            ContentTextActivity.startActivity(mContext, dynModel.getQuestionLink(), R.string.title_common_question);
        } else {
            try {
                String language = PreferenceUtil.getString("language", "");
                if (!TextUtils.isEmpty(language)) {
                    if(language.equalsIgnoreCase(MainApplication.LANG_CODE_JA_JP)){//如果是日语
                        ContentTextActivity.startActivity(mContext, "https://app.wepayez.com/web/faq/index.html?language=ja_jp" + "&bank="+ApiConstant.bankName, R.string.title_common_question);
                    }else{
                        ContentTextActivity.startActivity(mContext, "https://app.wepayez.com/web/faq/index.html?language=" + language + "&bank="+ApiConstant.bankName, R.string.title_common_question);
                    }
                } else {
                    String lan = Locale.getDefault().toString();
                    if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_HANS)) {
                        ContentTextActivity.startActivity(mContext, "https://app.wepayez.com/web/faq/index.html?language=zh_cn&bank="+ApiConstant.bankName, R.string.title_common_question);
                    } else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO) ||
                            lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_HANT)) {
                        ContentTextActivity.startActivity(mContext, "https://app.wepayez.com/web/faq/index.html?language=zh_tw&bank="+ApiConstant.bankName, R.string.title_common_question);
                    } else if(lan.equalsIgnoreCase(MainApplication.LANG_CODE_JA_JP) ){
                        ContentTextActivity.startActivity(mContext, "https://app.wepayez.com/web/faq/index.html?language=ja_jp&bank="+ApiConstant.bankName, R.string.title_common_question);

                    } else {
                        ContentTextActivity.startActivity(mContext, "https://app.wepayez.com/web/faq/index.html?language=en_us&bank="+ApiConstant.bankName, R.string.title_common_question);
                    }

                }

            } catch (Exception e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }
        }

        //        String language = PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN);
        //        
        //        Locale locale = getResources().getConfiguration().locale; //zh-rHK
        //        String lan = locale.getCountry();
        //        
        //        if (!TextUtils.isEmpty(language))
        //        {
        //            
        //            if (language.equals(MainApplication.LANG_CODE_ZH_TW))
        //            {
        //            }
        //            else
        //            {
        //                ContentTextActivity.startActivity(mContext, ApiConstant.BASE_URL_PORT
        //                    + "spay/public/question_zh_cn.html?bankCode=" + ApiConstant.bankCode + "&tel="
        //                    + R.string.qustion_company_phone, R.string.title_common_question);
        //            }
        //            
        //        }
        //        else
        //        {
        //            if (lan.equalsIgnoreCase("CN"))
        //            {
        //                ContentTextActivity.startActivity(mContext, ApiConstant.BASE_URL_PORT
        //                    + "spay/public/question_zh_cn.html?bankCode=" + ApiConstant.bankCode + "&tel="
        //                    + R.string.qustion_company_phone, R.string.title_common_question);
        //            }
        //            else
        //            {
        //                ContentTextActivity.startActivity(mContext, ApiConstant.BASE_URL_PORT
        //                    + "spay/public/question_zh_tw.html?bankCode=" + ApiConstant.bankCode + "&tel="
        //                    + R.string.qustion_company_phone, R.string.title_common_question);
        //            }
        //        }

    }

    //private UpgradeInfo upVerInfo;

    /**
     * 检查版本显示提示图标
     */
    public void checkVersion(final boolean isDisplayIcon) {
        // 不用太平凡的检测升级 所以使用时间间隔区分
        UpgradeManager.getInstance().getVersonCode(new UINotifyListener<UpgradeInfo>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                // titleBar.setRightLodingVisible(true);
                showLoading(false, R.string.show_new_version_loading);
            }

            @Override
            public void onError(Object object) {
                super.onError(object);
                dismissLoading();
            }

            @Override
            public void onSucceed(UpgradeInfo result) {
                super.onSucceed(result);
                dismissLoading();
                if (null != result) {
                    if (isDisplayIcon) {
                        //upVerInfo = result;
                        //                        ivNewVersion.setVisibility(View.VISIBLE);
                    } else {
                        showUpgradeInfoDialog(result, new ComDialogListener(result));
                    }
                } else {
                    if (!isDisplayIcon) {
                        showToastInfo(R.string.show_no_version);
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //设置默认支付方式
            case R.id.ly_pay_method:
                showPage(SetPayMethodActivity.class);

                break;
            //提现
            case R.id.but_push:
                //                if (blance != null)
                //                {
//                PushMoneyActivity.startActivity(SettingActivity.this, blance);
                //                }
                break;

            //关于我们
            case R.id.lay_choise:
                showPage(SettingAboutActivity.class);
                //                LanguageDialogInfo dialog =
                //                    new LanguageDialogInfo(SettingActivity.this, new LanguageDialogInfo.HandleBtn()
                //                    {
                //                        
                //                        @Override
                //                        public void handleOkBtn(Boolean lang)
                //                        {
                //                            if (lang)
                //                            { // 中文
                //                                switchLanguage(MainApplication.LANG_CODE_ZH_CN); // 中文
                //                            }
                //                            else
                //                            {//繁体
                //                                                switchLanguage(MainApplication.LANG_CODE_ZH_TW);
                //                            }
                //                            finish();
                //                                            MainActivity.startActivity(SettingActivity.this, "SettingActivity");
                //                                            LocalAccountManager.getInstance().onDestory();
                //                        }
                //                        
                //                    }, new LanguageDialogInfo.HandleBtnCancle()
                //                    {
                //                        
                //                        @Override
                //                        public void handleCancleBtn()
                //                        {
                //                            // 繁体字
                //                            //                            switchLanguage(MainApplication.LANG_CODE_ZH_TW);
                //                            //                            finish();
                //                            //                            //                                LoginActivity.startActivity(mContext);
                //                            //                            MainActivity.startActivity(SettingActivity.this, "SettingActivity");
                //                            //                            LocalAccountManager.getInstance().onDestory();
                //                            
                //                        }
                //                        
                //                    });
                //                DialogHelper.resize(SettingActivity.this, dialog);
                //                dialog.show();
                break;
            //营销规则
            case R.id.marketingLay:

                break;

            // 历史排名榜
            case R.id.lay_prize:

                break;
            //固定二维码收款
            case R.id.ll_static_code:
                //                if (null != MainApplication.serviceType
                //                    && MainApplication.serviceType.contains(MainApplication.PAY_WX_SJPAY))
                //                {

                try {
                    StatService.trackCustomEvent(SettingActivity.this, "SPConstTapFixedQRcode", "收款二维码");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                showPage(StaticCodeActivity.class);
                //                }
                //                else
                //                {
                //                    showToastInfo(R.string.show_static_code);
                //                }
                break;

            // 扫一扫退款
            case R.id.lay_scan:
                CaptureActivity.startActivity(SettingActivity.this, MainApplication.PAY_TYPE_REFUND);
                break;
            //卡券核销
            case R.id.write_off:
                CaptureActivity.startActivity(SettingActivity.this, MainApplication.PAY_TYPE_WRITE_OFF);
                break;

            case R.id.bodyLay://修改商品名称
                //                if (MainApplication.isAdmin.equals("1") && MainApplication.remark.equals("1"))
                //                {

                //                if (null == MainApplication.body)
                //                {
                //                    showPage(GoodsNameSetting.class);
                //                    return;
                //                }

                showPage(GoodsNameSetting.class);

                break;
            case R.id.ll_changePwd:

                onChangePwd();

                break;

            case R.id.empManager:
                try {
                    StatService.trackCustomEvent(SettingActivity.this, "SPConstTapCaisherManager", "收银员管理");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
                //                if (MainApplication.isAdmin.equals("1") && MainApplication.remark.equals("1"))
                //                {
                showPage(CashierManager.class);
                //                }
                //                else
                //                {
                //                    showToastInfo(R.string.show_mch_user);
                //                }

                break;
            // case R.id.ll_coupon:
            // if (!MainApplication.isRegisterUser)
            // {
            // showToastInfo("体验用户无权限进入优惠券！");
            // break;
            // }
            // else
            // {
            // // 检查资料是否完善
            // if (MainApplication.roleID != 103)
            // {
            // existAuth();
            //
            // checkExitAuth(true);
            //
            // }
            // else
            // {
            // loadShopData(true);
            // }
            // break;
            // }
            case R.id.ll_shopinfo:

                PersonalManager.getInstance().queryMerchantDataByTel(MainApplication.phone, new UINotifyListener<MerchantTempDataModel>() {
                    @Override
                    public void onPreExecute() {
                        super.onPreExecute();
                        // titleBar.setRightLodingVisible(true);
                        showLoading(false, R.string.public_loading);
                    }

                    @Override
                    public void onError(Object object) {
                        super.onError(object);
                        dismissLoading();
                        if (checkSession()) {
                            return;
                        }
                        if (object != null) {
                            toastDialog(SettingActivity.this, String.valueOf(object), null);
                        }
                    }

                    @Override
                    public void onSucceed(MerchantTempDataModel result) {
                        super.onSucceed(result);
                        dismissLoading();
                        if (null != result) {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("merchantData", result);
                            showPage(ShopkeeperActivity.class, bundle);
                        }
                        //                            else
                        //                            {
                        //                                showPage(ShopkeeperActivity.class);
                        //                            }

                    }
                });

                // 体验用户
                // if (!MainApplication.isRegisterUser)
                // {
                // showToastInfo("体验用户无权查看商户信息！");
                // return;
                // }
                // try
                // {
                // UserModel userModel =
                // UserInfoDB.getInstance().queryUserByName(MainApplication.userName);
                //
                // if (userModel != null)
                // {
                // // 资料是否完善
                // if (userModel.isExistData() == 0)
                // {
                // // DialogInfo dialogInfo =
                // // new DialogInfo(SettingActivity.this, "提示框",
                // "商户信息资料尚未完善，请先完善资料！！", "确定",
                // // DialogInfo.FLAG);
                // // dialogInfo.show();
                // // 调接口去验证
                // getShopDataInfo();
                // 如果是 普通收银员用户
                // if (MainApplication.roleID != 103)
                // {
                // existAuth();
                //
                // checkExitAuth(false);
                //
                // }
                // else
                // {
                // loadShopData(false);
                // }
                // }
                // else
                // {
                // loadShopData(false);
                // //ShopAddActivity.startActivity(mContext);
                // }
                // }
                // else
                // // 本地没有记录直接调用服务端检查
                // {
                // checkExitAuth(userModel);
                // }
                // }
                // catch (SQLException e)
                // {
                // Log.e("Setting", "onSettingDown() queryUserByName failed ");
                // }

                // 验证资料尚未完善 优先从本地取

                // if (true)
                // {
                // // 跳转到店主资料完善界面
                //
                // // DialogInfo dialogInfo =
                // // new DialogInfo(SettingActivity.this, "提示框",
                // "商户信息资料尚未完善，请先完善资料！！", "确定", DialogInfo.FLAG);
                // // dialogInfo.show();
                // // showPage(ShopkeeperActivity.class);
                // }
                // else
                // {
                // loadShopData(false);
                // }

                break;
        }
    }

//    /**
//     * 检查是否授权 和 资料是否完善 <功能详细描述>
//     *
//     * @param isCoupon 表示的是优惠券信息
//     * @see [类、类#方法、类#成员]
//     */
//    private void checkExitAuth(final boolean isCoupon) {
//        // 授权验证
//        if (MainApplication.userId != 0) {
//            UserManager.existAuth(MainApplication.userId, 1, new UINotifyListener<Integer>() {
//                @Override
//                public void onPreExecute() {
//                    super.onPreExecute();
//                    // titleBar.setRightLodingVisible(true);
//                    showLoading(false, "资料未完善检查中...");
//                }
//
//                @Override
//                public void onError(Object object) {
//                    super.onError(object);
//                    if (object != null) {
//                        showToastInfo(object.toString());
//                    }
//                }
//
//                @Override
//                public void onSucceed(Integer result) {
//                    super.onSucceed(result);
//
//                    switch (result) {
//
//                        // 出错
//                        case 11:
//                            showToastInfo("资料未完善检查失败，请稍微再试!!");
//                            return;
//                        // 未激活
//                        case 3:
//
//                            DialogInfo dialogInfo = new DialogInfo(SettingActivity.this, getResources().getString(R.string.title_prompt), "商户信息资料尚未完善，请先完善资料!", "确定", DialogInfo.FLAG, new DialogInfo.HandleBtn() {
//                                @Override
//                                public void handleOkBtn() {
//                                    HandlerManager.registerHandler(HandlerManager.GETSHOPINFODONE, handler);
//                                    // 获取商户数据
//                                    getShopDataInfo(HandlerManager.GETSHOPINFODONE);
//
//                                    // showPage(ShopkeeperActivity.class);
//
//                                }
//
//                                @Override
//                                public void handleCancleBtn() {
//                                    // TODO Auto-generated method stub
//
//                                }
//                            }, null);
//                            DialogHelper.resize(SettingActivity.this, dialogInfo);
//                            dialogInfo.show();
//                            return;
//                        case RequestResult.RESULT_TIMEOUT_ERROR: // 请求超时
//                            showToastInfo("连接超时，请稍微再试!");
//                            return;
//                        default:
//                            break;
//                    }
//                    dismissMyLoading();
//
//                }
//            });
//        } else {
//            // 用户id为空
//            // showToastInfo("连接已断开，请重新再登录!!");
//            return;
//        }
//    }

    /**
     * 升级点击事件
     */
    class ComDialogListener implements CommonConfirmDialog.ConfirmListener {
        private UpgradeInfo result;

        public ComDialogListener(UpgradeInfo result) {
            this.result = result;
        }

        @Override
        public void ok() {
            new UpgradeDailog(SettingActivity.this, result, new UpgradeDailog.UpdateListener() {
                @Override
                public void cancel() {

                }
            }).show();
        }

        @Override
        public void cancel() {
            // LocalAccountManager.getInstance().saveCheckVersionTime();

        }

    }

    MerchantTempDataModel merchantInfo = null;


    boolean isAuth = false;


    @Override
    protected boolean isLoginRequired() {
        return false;
    }
}