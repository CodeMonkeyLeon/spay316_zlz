/*
 * 文 件 名:  BillSumManager.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-7-14
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.logica.bill;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.BaseManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.BillItemModel;
import cn.swiftpass.enterprise.bussiness.model.BillMordel;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.NetHelper;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 结算汇总
 * 
 * @author  he_hui
 * @version  [版本号, 2016-7-14]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class BillSumManager extends BaseManager
{
    
    @Override
    public void init()
    {
    }
    
    @Override
    public void destory()
    {
    }
    
    public static BillSumManager getInstance()
    {
        return Container.instance;
    }
    
    private static class Container
    {
        public static BillSumManager instance = new BillSumManager();
    }
    
    String billSum = "spay/merchant/interface/getCheckBillMerchant";//结算汇总
    
    public void chexkMerchantBind(final UINotifyListener<Boolean> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<Boolean>()
        {
            @Override
            public Boolean execute()
                throws Exception
            {
                JSONObject jsonObject = new JSONObject();
                
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId))
                {
                    jsonObject.put("mchNo", MainApplication.merchantId);
                }
                else
                {
                    jsonObject.put("mchNo", MainApplication.getMchId());
                }
                
                RequestResult result =
                    NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/merchant/interface/chexkMerchantBind",
                        jsonObject,
                        null,
                        null);
                try
                {
                    Log.i("hehui", "chexkMerchantBind result data-->" + result.data.getString("message"));
                    
                    result.setNotifyListener(listener);
                    
                    if (!result.hasError())
                    {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        Log.i("hehui", "chexkMerchantBind-->" + res);
                        if (res == 200) //代表 已经激活成功
                        {
                            MainApplication.isActive = true;
                            return true;
                        }
                        else if (res == 400)
                        { // 还未激活
                            return false;
                        }
                        
                        else
                        {
                            listener.onError(result.data.getString("message"));
                        }
                    }
                    else
                    {
                        switch (result.resultCode)
                        {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                        }
                    }
                }
                catch (Exception e)
                {
                    listener.onError("");
                }
                return false;
            }
        },
            listener);
    }
    
    public void qrCodeBind(final String code, final UINotifyListener<Boolean> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<Boolean>()
        {
            @Override
            public Boolean execute()
                throws Exception
            {
                JSONObject jsonObject = new JSONObject();
                
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId))
                {
                    jsonObject.put("mchNo", MainApplication.merchantId);
                }
                else
                {
                    jsonObject.put("mchNo", MainApplication.getMchId());
                }
                
                jsonObject.put("useState", "2");
                if (!StringUtil.isEmptyOrNull(MainApplication.mchName))
                {
                    jsonObject.put("mchName", MainApplication.mchName);
                }
                else
                {
                    jsonObject.put("mchName", MainApplication.getMchName());
                }
                String url = "";
                if (code.contains("&"))
                {
                    url = code.substring(code.indexOf("qrId=") + 5, code.indexOf("&"));
                }
                else
                {
                    url = code.substring(code.indexOf("qrId=") + 5);
                }
                
                jsonObject.put("qrId", url);
                Log.i("hehui", "qrCodeBind param-->" + jsonObject);
                RequestResult result =
                    NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/merchant/interface/qrCodeBind",
                        jsonObject,
                        null,
                        null);
                try
                {
                    Log.i("hehui", "qrCodeBind result data-->" + result.data.getString("message"));
                    
                    result.setNotifyListener(listener);
                    
                    if (!result.hasError())
                    {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200)
                        {
                            return true;
                        }
                        else
                        {
                            listener.onError(result.data.getString("message"));
                        }
                    }
                    else
                    {
                        switch (result.resultCode)
                        {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                        }
                    }
                }
                catch (Exception e)
                {
                    listener.onError("服务器错误");
                }
                return false;
            }
        },
            listener);
    }
    
    public void getCheckBillMerchant(final String startTime, final String endTiem,
        final UINotifyListener<BillMordel> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<BillMordel>()
        {
            @Override
            public BillMordel execute()
                throws Exception
            {
                JSONObject jsonObject = new JSONObject();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId))
                {
                    jsonObject.put("mchNo", MainApplication.merchantId);
                }
                else
                {
                    jsonObject.put("mchNo", MainApplication.getMchId());
                }
                jsonObject.put("checkStartDate", startTime);
                jsonObject.put("checkEndDate", endTiem);
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + billSum, jsonObject, null, null);
                try
                {
                    Log.i("hehui", "getCheckBillMerchant result data-->" + result.data.getString("message"));
                    
                    result.setNotifyListener(listener);
                    
                    if (!result.hasError())
                    {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200)
                        {
                            BillMordel billMordel = new BillMordel();
                            
                            JSONObject jsObject = new JSONObject(result.data.getString("message"));
                            billMordel.setMchFeeTotal(jsObject.optLong("mchFeeTotal", 0));
                            billMordel.setPayNetFeeTotal(jsObject.optLong("payNetFeeTotal", 0));
                            billMordel.setPayRemitFeeTotal(jsObject.optLong("payRemitFeeTotal", 0));
                            if (!jsObject.getString("data").equalsIgnoreCase("[]"))
                            {
                                JSONArray array = new JSONArray(jsObject.getString("data"));
                                List<BillItemModel> list = new ArrayList<BillItemModel>();
                                for (int i = 0; i < array.length(); i++)
                                {
                                    JSONObject js = array.getJSONObject(i);
                                    BillItemModel bill = new BillItemModel();
                                    bill.setMchFee(js.optLong("mchFee", 0));
                                    bill.setPayRemitFee(js.optLong("payRemitFee", 0));
                                    bill.setPayTradeTime(js.optLong("payTradeTime", 0));
                                    bill.setPayNetFee(js.optLong("payNetFee", 0));
                                    bill.setPayTypaName(js.optString("payTypeName", ""));
                                    list.add(bill);
                                }
                                if (list.size() > 0)
                                {
                                    billMordel.setList(list);
                                }
                            }
                            
                            return billMordel;
                            
                        }
                        else
                        {
                            listener.onError(result.data.getString("message"));
                        }
                    }
                    else
                    {
                        switch (result.resultCode)
                        {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                        }
                    }
                }
                catch (Exception e)
                {
                    listener.onError("获取结算数据失败");
                }
                return null;
            }
        },
            listener);
    }
}
