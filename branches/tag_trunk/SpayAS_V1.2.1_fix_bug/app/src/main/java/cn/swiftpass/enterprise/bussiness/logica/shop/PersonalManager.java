/*
 * 文 件 名:  PersonalManager.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-13
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.logica.shop;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.CountryAndUnitMode;
import cn.swiftpass.enterprise.bussiness.model.MerchantTempDataModel;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.bussiness.model.ShopBankBaseInfo;
import cn.swiftpass.enterprise.bussiness.model.ShopBaseDataInfo;
import cn.swiftpass.enterprise.io.database.access.ShopBankDataDB;
import cn.swiftpass.enterprise.io.database.access.ShopBaseDataDB;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.NetHelper;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.Utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 资料完善业务处理
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-13]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class PersonalManager
{
    private final static String TAG = PersonalManager.class.getCanonicalName();
    
    private static PersonalManager personalManager;
    
    // 上传图片
    private static String uploadUrl = ApiConstant.BASE_URL_PORT + ApiConstant.UPLOADPHOTO;
    
    // 完善资料添加
    
    private static String shop_dataAdd = ApiConstant.BASE_URL_PORT + ApiConstant.SHOPDATAADD;
    
    // 行业 和 银行 基本数据
    private static String shop_baseData = ApiConstant.BASE_URL_PORT + ApiConstant.SHOPBASEDATA;
    
    // 查询 资料完善信息
    private static String shop_PersonalData = ApiConstant.BASE_URL_PORT + ApiConstant.SHOPDATAQUERY;
    
    //注册获取国家与货币
    private static String countryAndUnitQuery = ApiConstant.BASE_URL_PORT + ApiConstant.GET_COUNTRY_AND_UNIT;
    
    public static PersonalManager getInstance()
    {
        if (personalManager == null)
        {
            personalManager = new PersonalManager();
        }
        
        return personalManager;
    }
    
    /**
     * 获取银行 和 行业基础信息 ，
     * 
     * <功能详细描述>
     * @param type 2：银行数据，1：行业数据
     * @see [类、类#方法、类#成员]
     */
    public static void getShopAction_baseData(final String type, final UINotifyListener<Boolean> listener)
    {
        ThreadHelper.executeWithCallback(new Executable()
        {
            @Override
            public Boolean execute()
            {
                try
                {
                    String url = ApiConstant.BASE_URL_PORT;
                    //                    JSONObject json = new JSONObject();
                    if (type.equals("1"))
                    {
                        url = url + "spay/mch/type";
                    }
                    else
                    {
                        url = url + "spay/bank/queryAllBankInfo";
                    }
                    //                    json.put("clientType", String.valueOf(ApiConstant.SPAY));
                    //                    json.put("imei", "no"); //imei
                    //                    json.put("imsi", "no");
                    //                    json.put("dataType", type);
                    Gson gson = new Gson();
                    
                    RequestResult result = NetHelper.httpsPost(url, null);
                    Log.e(TAG, "getShopAction_baseData " + url);
                    if (!result.hasError())
                    {
                        String jsonData = result.data.getString("message");
                        // 商业 行业数据
                        if (type.equals("1"))
                        {
                            List<ShopBaseDataInfo> listDatas =
                                gson.fromJson(jsonData, new TypeToken<List<ShopBaseDataInfo>>()
                                {
                                }.getType());
                            
                            for (ShopBaseDataInfo info : listDatas)
                            {
                                try
                                {
                                    ShopBaseDataDB.getInstance().save(info);
                                }
                                catch (SQLException e)
                                {
                                    Log.e(TAG, "getShopAction_baseData() save data failed ");
                                    return false;
                                }
                            }
                        }
                        else
                        // 银行数据
                        {
                            List<ShopBankBaseInfo> bankInfo =
                                gson.fromJson(jsonData, new TypeToken<List<ShopBankBaseInfo>>()
                                {
                                }.getType());
                            
                            for (ShopBankBaseInfo info : bankInfo)
                            {
                                try
                                {
                                    ShopBankDataDB.getInstance().save(info);
                                }
                                catch (SQLException e)
                                {
                                    Log.e(TAG, "getShopAction_bank Data() save data failed ");
                                    return false;
                                }
                            }
                        }
                        
                    }
                    
                }
                catch (JSONException e)
                {
                    Log.e(TAG, "update shop failed ");
                }
                return false;
                
            }
        },
            listener);
    }
    
    /**
     * 添加完善资料
     * @param 
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    public static void shopAddData(final String userID, final MerchantTempDataModel dataModle,
        final UINotifyListener<Boolean> listener)
    {
        ThreadHelper.executeWithCallback(new Executable()
        {
            @Override
            public Boolean execute()
            {
                
                try
                {
                    JSONObject json = new JSONObject();
                    //                    json.put("clientType", String.valueOf(ApiConstant.SPAY));
                    //                    json.put("imei", "no"); //imei
                    //                    json.put("imsi", "no");
                    //                    json.put("userId", userID);
                    json.put("merchantName", dataModle.getMerchantName());
                    json.put("merchantType", dataModle.getMerchantTypeName());
                    json.put("merchantId", MainApplication.merchantId);
                    json.put("phone", MainApplication.phone);
                    json.put("province", dataModle.getProvince());
                    json.put("city", dataModle.getCity());
                    json.put("address", dataModle.getAddress());
                    json.put("email", dataModle.getEmil());
                    json.put("accountName", dataModle.getBankUserName());
                    json.put("accountIdEntity", dataModle.getIdentityNo());
                    json.put("bankNumberCode", dataModle.getBank());
                    json.put("bankName", dataModle.getBranchBankName());
                    json.put("linencePhoto", dataModle.getIdCardJustPic());
                    json.put("indentityPhoto", dataModle.getLicensePic());
                    json.put("channelId", MainApplication.channelId);
                    json.put("protocolPhoto", dataModle.getIdCode());
                    
                    //                    Gson gson = new Gson();
                    //                    json.put("merchantTempDataModel", gson.toJson(dataModle));
                    
                    Log.i(TAG, "shopAddData() json -->" + json.toString() + ",url-->" + shop_dataAdd);
                    
                    RequestResult result = NetHelper.httpsPost(shop_dataAdd, json);
                    result.setNotifyListener(listener);
                    Log.i(TAG, "shopAddData() result.data -->" + result.data.toString());
                    
                    if (!result.hasError())
                    {
                        
                        if (result.data == null)
                        {
                            return false;
                        }
                        
                        int res;
                        
                        res = Utils.Integer.tryParse(result.data.getString("result"), -1);
                        
                        if (res == 200)
                        {
                            return true;
                        }
                        else
                        {
                            listener.onError(result.data.getString("message"));
                        }
                        //                        switch (res)
                        //                        {
                        //                        // 成功
                        //                            case 200:
                        //                                return true;
                        //                                // 参数错误
                        //                            case 1:
                        //                                listener.onError("输入数据有误，请重新输入!");
                        //                                return false;
                        //                            case 2: // 用户无效
                        //                                listener.onError("用户无效!");
                        //                                return false;
                        //                            case 3: // 商户无效
                        //                                listener.onError("商户无效!");
                        //                                return false;
                        //                            default:
                        //                                break;
                        //                        }
                    }
                }
                catch (JSONException e)
                {
                    listener.onError("资料完善提交失败，请稍后在试!!");
                    return false;
                }
                return false;
            }
        }, listener);
    }
    
    /**
     * 根据手机号码查询完善资料信息
     * <功能详细描述>
     * @param userID 用户id
     * @param handleFlag  这是一个handle标识   标识要发给哪个handler的通知   如果为-1 标识不需要发通知 
     * @see [类、类#方法、类#成员]
     */
    public void queryMerchantDataByTel(final String tel, final UINotifyListener<MerchantTempDataModel> listener)
    {
        ThreadHelper.executeWithCallback(new Executable()
        {
            @Override
            public MerchantTempDataModel execute()
            {
                MerchantTempDataModel merchant = null;
                try
                {
                    JSONObject json = new JSONObject();
                    //                    json.put("clientType", String.valueOf(ApiConstant.SPAY));
                    //                    json.put("imei", "no"); //imei
                    if (!StringUtil.isEmptyOrNull(MainApplication.merchantId))
                    {
                        json.put("mchId", MainApplication.merchantId);
                    }
                    else
                    {
                        json.put("mchId", MainApplication.getMchId());
                    }
                    json.put("phone", tel);
                    
                    Log.i(TAG, "queryMerchantDataByTel() json -->" + json.toString() + ",url-->" + shop_dataAdd);
                    
                    long spayRs = System.currentTimeMillis();
                    //                    //加签名
                    if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey()))
                    {
                        json.put("spayRs", spayRs);
                        json.put("nns",
                            SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()),
                                MainApplication.getNewSignKey()));
                    }
                    
                    RequestResult result = NetHelper.httpsPost(shop_PersonalData, json, String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    
                    if (!result.hasError())
                    {
                        
                        if (result.data == null)
                        {
                            return null;
                        }
                        Log.i(TAG, "queryMerchantDataByTel() result.data -->" + result.data.toString());
                        int res = Utils.Integer.tryParse(result.data.getString("result"), -1);
                        
                        switch (res)
                        {
                        // 成功
                            case 200:
                                String dataModel = result.data.getString("message");
                                
                                JSONObject js = new JSONObject(dataModel);
                                
                                merchant = new MerchantTempDataModel();
                                
                                merchant.setMerchantName(js.optString("merchantName", ""));
                                merchant.setMerchantTypeName(js.optString("merchantType", ""));
                                merchant.setPhone(js.optString("phone", ""));
                                merchant.setProvince(js.optString("province", ""));
                                merchant.setCity(js.optString("city", ""));
                                merchant.setAddress(js.optString("address", ""));
                                merchant.setEmil(js.optString("email", ""));
                                merchant.setBankUserName(js.optString("accountName", ""));
                                merchant.setIdentityNo(js.optString("accountIdEntity", ""));
                                merchant.setBank(js.optString("bankNumberCode", ""));
                                merchant.setBranchBankName(js.optString("bankName", ""));
                                merchant.setIdCardJustPic(js.optString("linencePhoto", ""));
                                merchant.setLicensePic(js.optString("indentityPhoto", ""));
                                merchant.setIdCode(js.optString("protocolPhoto", ""));
                                merchant.setPrincipalPhone(js.optString("principalPhone", ""));
                                merchant.setPrincipal(js.optString("principal", ""));
                                return merchant;
                                // 参数错误
                                //                            case 1:
                                //                                listener.onError("输入数据有误，请重新输入!!");
                                //                                return merchant;
                            default:
                                result.returnErrorMessage();
                                break;
                        
                        }
                    }
                    
                }
                catch (Exception e)
                {
                    return null;
                }
                return null;
            }
        },
            listener);
    }
    
    /**
     * 根据用户id查询完善资料信息
     * <功能详细描述>
     * @param userID 用户id
     * @param handleFlag  这是一个handle标识   标识要发给哪个handler的通知   如果为-1 标识不需要发通知 
     * @see [类、类#方法、类#成员]
     */
    public static void queryMerchantData(final String userID, final UINotifyListener<MerchantTempDataModel> listener,
        final int handleFlag)
    {
        ThreadHelper.executeWithCallback(new Executable()
        {
            @Override
            public MerchantTempDataModel execute()
            {
                MerchantTempDataModel merchant = null;
                
                try
                {
                    JSONObject json = new JSONObject();
                    json.put("clientType", String.valueOf(ApiConstant.SPAY));
                    json.put("imei", "no"); //imei
                    json.put("imsi", "no");
                    json.put("userId", userID);
                    Gson gson = new Gson();
                    
                    Log.i(TAG, "queryMerchantData() json -->" + json.toString() + ",url-->" + shop_dataAdd);
                    
                    RequestResult result = NetHelper.httpsPost(shop_PersonalData, json);
                    result.setNotifyListener(listener);
                    
                    if (!result.hasError())
                    {
                        
                        if (result.data == null)
                        {
                            return null;
                        }
                        Log.i(TAG, "queryMerchantData() result.data -->" + result.data.toString());
                        int res = Utils.Integer.tryParse(result.data.getString("result"), -1);
                        
                        switch (res)
                        {
                        // 成功
                            case 0:
                                String dataModel = result.data.getString("merchantTempDataModel");
                                merchant = gson.fromJson(dataModel, new TypeToken<MerchantTempDataModel>()
                                {
                                }.getType());
                                //获取到了商户信息 发送通知给界面更新 
                                if (handleFlag != -1)
                                {
                                    HandlerManager.notifyMessage(handleFlag, handleFlag, merchant);
                                    //获取商户信息完成通知
                                    //                                    HandlerManager.notifyMessage(HandlerManager.GETSHOPINFODONE,
                                    //                                        HandlerManager.GETSHOPINFODONE,
                                    //                                        merchant);
                                }
                                
                                return merchant;
                                // 参数错误
                            default:
                                result.returnErrorMessage();
                                break;
                        
                        }
                    }
                    
                }
                catch (Exception e)
                {
                    return merchant;
                }
                return merchant;
            }
        }, listener);
    }
    
    /**
     * 修改
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    public static void updateShop(final String userID, final MerchantTempDataModel dataModle)
    {
        JSONObject json = new JSONObject();
        try
        {
            json.put("clientType", String.valueOf(ApiConstant.SPAY));
            json.put("imei", "no"); //imei
            json.put("imsi", "no");
            json.put("userId", userID);
            Gson gson = new Gson();
            json.put("merchantTempDataModel", gson.toJson(dataModle));
            
            RequestResult result = NetHelper.httpsPost(shop_dataAdd, json);
            
            Log.i(TAG, "updateShop() result.data -->" + result.data.toString());
        }
        catch (JSONException e)
        {
            Log.e(TAG, "update shop failed ");
        }
        
    }
    
    /**
     * 上传图片
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    public static void uploadPhoto(final Map<String, String> mapPicUrls, final MerchantTempDataModel merchant,
        final UINotifyListener<Boolean> listener)
    {
        ThreadHelper.executeWithCallback(new Executable()
        {
            @Override
            public Boolean execute()
            {
                RequestResult result = null;
                try
                {
                    JSONObject json = new JSONObject();
                    //                    json.put("clientType", String.valueOf(ApiConstant.SPAY));
                    //                    json.put("imei", "no"); //imei
                    //                    json.put("imsi", "no");
                    //                    json.put("dataType", String.valueOf(ApiConstant.SPAY));
                    //                    MerchantTempDataModel merModel = new MerchantTempDataModel();
                    for (String key : mapPicUrls.keySet())
                    {
                        
                        // 第一张 身份证 正面照上传
                        String valueUlr = mapPicUrls.get(key);
                        
                        String fileName = valueUlr.substring(valueUlr.lastIndexOf('/') + 1);
                        json.put("pictitle", fileName);
                        
                        if (key.equals("one"))
                        {
                            result = NetHelper.httpsPost(uploadUrl, json, null, valueUlr);
                            if (result == null)
                            {
                                continue;
                            }
                            if (!result.hasError())
                            {
                                int res;
                                res = Utils.Integer.tryParse(result.data.getString("result"), -1);
                                switch (res)
                                {
                                    case 200: // 上传成功，把图片的路径更新修改
                                        merchant.setIdCardJustPic(result.data.optString("message", ""));
                                        Log.i("hehui", "one -->" + result.data.optString("message", ""));
                                        shopAddData("", merchant, listener);
                                        Log.i("shopAddData", "one -->" + result.data.optString("message", ""));
                                        //                                        merModel.setIdentityPic(fileName);
                                        //                                        updateShop(String.valueOf(MainApplication.userId), merModel);
                                        //                                        File fileOne = new File(valueUlr);
                                        //                                        if (fileOne.exists())
                                        //                                        {
                                        //                                            fileOne.delete();
                                        //                                        }
                                        // 清空缓存
                                        break;
                                    default:
                                        Log.e("hehui", "upload pic " + result.data.getString("message"));
                                        break;
                                }
                            }
                        }// 第二张  身份证反面照
                        else if (key.equals("two"))
                        {
                            result = NetHelper.httpsPost(uploadUrl, json, null, valueUlr);
                            Log.i("hehui", "two -->" + result.data.optString("message", ""));
                            if (result == null)
                            {
                                continue;
                            }
                            if (!result.hasError())
                            {
                                int res;
                                res = Utils.Integer.tryParse(result.data.getString("result"), -1);
                                switch (res)
                                {
                                    case 200: // 上传成功，把图片的路径更新修改
                                        //                                        merModel.setIdCardTrunmPic(fileName);
                                        //                                        updateShop(String.valueOf(MainApplication.userId), merModel);
                                        //                                        File filetwo = new File(valueUlr);
                                        //                                        if (filetwo.exists())
                                        //                                        {
                                        //                                            filetwo.delete();
                                        //                                        }
                                        merchant.setLicensePic(result.data.optString("message", ""));
                                        shopAddData("", merchant, listener);
                                        Log.i("shopAddData", "two -->" + result.data.optString("message", ""));
                                        break;
                                    //                                    case 1: // 上传失败
                                    //                                        Log.e(TAG, "upload two pic failed ");
                                    //                                        break;
                                    default:
                                        Log.e("hehui", "upload pic " + result.data.getString("message"));
                                        break;
                                }
                            }
                        }// 第三张  申请人手持照
                        else if (key.equals("three"))
                        {
                            result = NetHelper.httpsPost(uploadUrl, json, null, valueUlr);
                            if (result == null)
                            {
                                continue;
                            }
                            if (!result.hasError())
                            {
                                int res;
                                res = Utils.Integer.tryParse(result.data.getString("result"), -1);
                                switch (res)
                                {
                                    case 200: // 上传成功，把图片的路径更新修改
                                        merchant.setIdCode(result.data.optString("message", ""));
                                        shopAddData("", merchant, listener);
                                        //                                        merModel.setLicensePic(fileName);
                                        //                                        updateShop(String.valueOf(MainApplication.userId), merModel);
                                        //                                        File file = new File(valueUlr);
                                        //                                        if (file.exists())
                                        //                                        {
                                        //                                            file.delete();
                                        //                                        }
                                        break;
                                    case 1: // 上传失败
                                        Log.e(TAG, "upload three pic failed ");
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }// 第四张  税务登记照
                         //                        else if (key.equals("four"))
                         //                        {
                         //                            result = NetHelper.httpsPost(uploadUrl, json, null, valueUlr);
                         //                            if (result == null)
                         //                            {
                         //                                continue;
                         //                            }
                         //                            if (!result.hasError())
                         //                            {
                         //                                int res;
                         //                                res = Utils.Integer.tryParse(result.data.getString("result"), -1);
                         //                                switch (res)
                         //                                {
                         //                                    case 0: // 上传成功，把图片的路径更新修改
                         //                                        merModel.setTaxRegCertPic(fileName);
                         //                                        updateShop(String.valueOf(MainApplication.userId), merModel);
                         //                                        //                                        File file = new File(valueUlr);
                         //                                        //                                        if (file.exists())
                         //                                        //                                        {
                         //                                        //                                            file.delete();
                         //                                        //                                        }
                         //                                        break;
                         //                                    case 1: // 上传失败
                         //                                        Log.e(TAG, "upload four pic failed ");
                         //                                        break;
                         //                                    default:
                         //                                        break;
                         //                                }
                         //                            }
                         //                        }// 第五张  机构照
                         //                        else if (key.equals("five"))
                         //                        {
                         //                            result = NetHelper.httpsPost(uploadUrl, json, null, valueUlr);
                         //                            if (result == null)
                         //                            {
                         //                                continue;
                         //                            }
                         //                            if (!result.hasError())
                         //                            {
                         //                                int res;
                         //                                res = Utils.Integer.tryParse(result.data.getString("result"), -1);
                         //                                switch (res)
                         //                                {
                         //                                    case 0: // 上传成功，把图片的路径更新修改
                         //                                        merModel.setOrgaCodeCertPic(fileName);
                         //                                        updateShop(String.valueOf(MainApplication.userId), merModel);
                         //                                        //                                        File file = new File(valueUlr);
                         //                                        //                                        if (file.exists())
                         //                                        //                                        {
                         //                                        //                                            file.delete();
                         //                                        //                                        }
                         //                                        break;
                         //                                    case 1: // 上传失败
                         //                                        Log.e(TAG, "upload five pic failed ");
                         //                                        break;
                         //                                    default:
                         //                                        break;
                         //                                }
                         //                            }
                         //                        }
                        
                    }
                    
                }
                catch (JSONException e)
                {
                    Log.e(TAG, "upload pic fialed -->" + e.getMessage());
                    // 上传失败
                    return false;
                }
                
                return false;
                
            }
        }, listener);
    }
    
    /** 获取国家与对应的货币
     * <功能详细描述>
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public static void getCountryAndUnit(final UINotifyListener<List<CountryAndUnitMode>> listener)
    {
        ThreadHelper.executeWithCallback(new Executable()
        {
            
            @Override
            public List<CountryAndUnitMode> execute()
                throws Exception
            {
                
                RequestResult result = NetHelper.httpsPost(countryAndUnitQuery, new JSONObject());
                if (!result.hasError())
                {
                    switch (result.resultCode)
                    {
                        case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                            listener.onError("网络连接不可用，请检查你网络连接!");
                            return null;
                        case RequestResult.RESULT_TIMEOUT_ERROR:
                            listener.onError("请求连接超时，请稍候再试!");
                            return null;
                        case RequestResult.RESULT_READING_ERROR:
                            listener.onError("请求服务连接超时，请稍候再试!");
                            return null;
                    }
                    
                    if (result.data == null)
                    {
                        return null;
                    }
                    int resCode = Integer.parseInt(result.data.getString("result"));
                    
                    if (resCode == 200)
                    {
                        //JSONObject jsonObject = new JSONObject(result.data.getString("message"));
                        String jsonData = result.data.getString("message");
                        Gson gson = new Gson();
                        List<CountryAndUnitMode> results =
                            gson.fromJson(jsonData, new TypeToken<List<CountryAndUnitMode>>()
                            {
                            }.getType());
                        return results;
                    }
                    else
                    {
                        listener.onError(result.data.getString("message"));
                    }
                    
                }
                return null;
            }
            
        },
            listener);
    }
}
