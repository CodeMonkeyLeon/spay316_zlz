package cn.swiftpass.enterprise.mymould.contract;


import cn.swiftpass.enterprise.mymould.bean.BaseBean;
import cn.swiftpass.enterprise.mymould.base.mvp.BaseView;
import cn.swiftpass.enterprise.mymould.bean.QueryMsgBean;
import io.reactivex.Observable;

/***
 * 契约类，用于各个层的管理
 */
public interface QueryContract {

    interface Model{//M相关
        Observable queryData(String data, String spayRs);
    }

    interface View extends BaseView{//用P跟V的连接
        @Override
        void onError(Throwable throwable);

        void onSuccess(BaseBean<QueryMsgBean> bean);
    }

    interface Presenter {//P相关
        void queryData(String data, String spayRs);
    }
}
