package cn.swiftpass.enterprise.bussiness.logica.wallet;

import java.util.List;

import org.json.JSONObject;

import android.util.Log;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.BaseManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.bussiness.model.WalletModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.NetHelper;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

public class WalletManager extends BaseManager
{
    
    private Integer timeOut = 60000;
    
    private static class Container
    {
        public static WalletManager instance = new WalletManager();
    }
    
    public static WalletManager getInstance()
    {
        return Container.instance;
    }
    
    @Override
    public void init()
    {
    }
    
    @Override
    public void destory()
    {
    }
    
    /**
     * 余额装出 提示信息
     * <功能详细描述>
     * @param money
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void accountConfig(final UINotifyListener<String> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<String>()
        {
            @Override
            public String execute()
                throws Exception
            {
                JSONObject json = new JSONObject();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId))
                {
                    json.put("mchId", MainApplication.merchantId);
                }
                else
                {
                    json.put("mchId", MainApplication.getMchId());
                }
                json.put("accountId", MainApplication.accountId);
                
                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey()))
                {
                    json.put("sign",
                        SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()),
                            MainApplication.getSignKey()));
                    
                }
                Log.i("hehui", "accountConfig params-->" + json);
                String url = ApiConstant.pushMoneyUrl + "spay/ewallet/ewalletQueryBalanc";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                
                try
                {
                    RequestResult result = NetHelper.httpsPost(url, json, null, null, timeOut);
                    if (!result.hasError())
                    {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200)
                        {
                            Log.i("hehui", "accountConfig-->" + result.data.getString("message"));
                            JSONObject object = new JSONObject(result.data.getString("message"));
                            String str = object.optString("tipCrash", "");
                            return str;
                        }
                        else
                        {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }
                        
                    }
                    else
                    {
                        switch (result.resultCode)
                        {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return null;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return null;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return null;
                        }
                    }
                    
                }
                catch (Exception e)
                {
                    Log.e("hehui", "" + e);
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }
                
                return null;
            }
        },
            listener);
        
    }
    
    /**
     * 提现 查询是否已经提现成功
     * <功能详细描述>
     * @param money
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void ewalletQueryOrder(final String orderNo, final UINotifyListener<String> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<String>()
        {
            @Override
            public String execute()
                throws Exception
            {
                JSONObject json = new JSONObject();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId))
                {
                    json.put("mchId", MainApplication.merchantId);
                }
                else
                {
                    json.put("mchId", MainApplication.getMchId());
                }
                json.put("accountId", MainApplication.accountId);
                json.put("order_no", orderNo);
                
                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey()))
                {
                    json.put("sign",
                        SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()),
                            MainApplication.getSignKey()));
                    
                }
                Log.i("hehui", "ewalletQueryOrder params-->" + json);
                String url = ApiConstant.pushMoneyUrl + "spay/ewallet/ewalletQueryOrder";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                
                try
                {
                    RequestResult result = NetHelper.httpsPost(url, json, null, null, timeOut);
                    result.setNotifyListener(listener);
                    if (!result.hasError())
                    {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200)
                        {
                            Log.i("hehui", "ewalletQueryOrder-->" + result.data.getString("message"));
                            JSONObject jsonObject = new JSONObject(result.data.getString("message"));
                            return jsonObject.optString("trans_status", "0");
                        }
                        else
                        {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }
                        
                    }
                    else
                    {
                        switch (result.resultCode)
                        {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return null;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return null;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return null;
                        }
                    }
                    
                }
                catch (Exception e)
                {
                    Log.e("hehui", "" + e);
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }
                
                return null;
            }
        },
            listener);
        
    }
    
    /**
     * 电子钱包 加载余额显示
     * <功能详细描述>
     * @param money
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void ewalletQueryBalanc(final UINotifyListener<WalletModel> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<WalletModel>()
        {
            @Override
            public WalletModel execute()
                throws Exception
            {
                JSONObject json = new JSONObject();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId))
                {
                    json.put("mchId", MainApplication.merchantId);
                }
                else
                {
                    json.put("mchId", MainApplication.getMchId());
                }
                json.put("accountId", MainApplication.accountId);
                
                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey()))
                {
                    json.put("sign",
                        SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()),
                            MainApplication.getSignKey()));
                    
                }
                Log.i("hehui", "ewalletQueryBalanc params-->" + json);
                String url = ApiConstant.pushMoneyUrl + "spay/ewallet/ewalletQueryBalance";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                
                try
                {
                    RequestResult result = NetHelper.httpsPost(url, json, null, null, timeOut);
                    result.setNotifyListener(listener);
                    if (!result.hasError())
                    {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200)
                        {
                            Log.i("hehui", "ewalletQueryBalanc-->" + result.data.getString("message"));
                            return (WalletModel)JsonUtil.jsonToBean(result.data.getString("message"), WalletModel.class);
                        }
                        else
                        {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }
                        
                    }
                    else
                    {
                        switch (result.resultCode)
                        {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return null;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return null;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return null;
                        }
                    }
                    
                }
                catch (Exception e)
                {
                    Log.e("hehui", "" + e);
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }
                
                return null;
            }
        },
            listener);
        
    }
    
    /**
     * 提现
     * <功能详细描述>
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void ewalletWithdrawals(final String money, final String cardno, final UINotifyListener<String> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<String>()
        {
            @Override
            public String execute()
                throws Exception
            {
                try
                {
                    JSONObject json = new JSONObject();
                    json.put("accountId", MainApplication.accountId);
                    if (!StringUtil.isEmptyOrNull(money))
                    {
                        json.put("amount", money);
                        //                        if (money.contains("."))
                        //                        {
                        //                        }
                        //                        else
                        //                        {
                        //                            json.put("amount", Long.parseLong(money) * 100 + "");
                        //                        }
                    }
                    if (!StringUtil.isEmptyOrNull(MainApplication.merchantId))
                    {
                        json.put("mchId", MainApplication.merchantId);
                    }
                    else
                    {
                        json.put("mchId", MainApplication.getMchId());
                    }
                    json.put("cardno", cardno);
                    json.put("msg_send_flag", "0");
                    if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey()))
                    {
                        json.put("sign",
                            SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()),
                                MainApplication.getSignKey()));
                        
                    }
                    Log.i("hehui", "ewalletWithdrawals params-->" + json);
                    String url = ApiConstant.pushMoneyUrl + "spay/ewallet/ewalletWithdrawals";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                    
                    RequestResult result = NetHelper.httpsPost(url, json, null, null, timeOut);
                    result.setNotifyListener(listener);
                    if (!result.hasError())
                    {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200)
                        {
                            String order = "spay";
                            Log.i("hehui", "ewalletWithdrawals-->" + result.data.getString("message"));
                            JSONObject object = new JSONObject(result.data.getString("message"));
                            String trans_status = object.optString("trans_status", "");
                            String order_no = object.optString("order_no", "");
                            if (!StringUtil.isEmptyOrNull(order_no))
                            {
                                order = order + order_no;
                            }
                            switch (Integer.parseInt(trans_status))
                            {
                                case 1: //成功
                                    return order;
                                case 2: //失败
                                    listener.onError("fail" + order_no);
                                    return null;
                                case 0: //未知
                                    listener.onError(order);
                                    return null;
                                    
                            }
                            
                        }
                        else
                        {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }
                        
                    }
                    else
                    {
                        switch (result.resultCode)
                        {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError("timeout");
                                return null;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError("timeout");
                                return null;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError("timeout");
                                return null;
                        }
                    }
                    
                }
                catch (Exception e)
                {
                    Log.e("hehui", "" + e);
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                    return null;
                }
                
                return null;
            }
        },
            listener);
        
    }
    
    /**
     * 电子钱包实名验证（四要素验证）
     * <功能详细描述>
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void certification(final String phone, final String isUpdateFlag, final String modifyidCards,
        final UINotifyListener<Boolean> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<Boolean>()
        {
            @Override
            public Boolean execute()
                throws Exception
            {
                JSONObject json = new JSONObject();
                json.put("accountId", MainApplication.accountId);
                json.put("mobile_phone", phone);
                if (!StringUtil.isEmptyOrNull(isUpdateFlag))
                {
                    json.put("isUpdateFlag", isUpdateFlag);
                    if (!StringUtil.isEmptyOrNull(modifyidCards))
                    {
                        
                        json.put("modifyidCards", modifyidCards);
                    }
                }
                else
                {
                    json.put("isUpdateFlag", "1");
                }
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId))
                {
                    json.put("mchId", MainApplication.merchantId);
                }
                else
                {
                    json.put("mchId", MainApplication.getMchId());
                }
                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey()))
                {
                    json.put("sign",
                        SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()),
                            MainApplication.getSignKey()));
                    
                }
                Log.i("hehui", "certification params-->" + json);
                String url = ApiConstant.pushMoneyUrl + "spay/certification";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                
                try
                {
                    RequestResult result = NetHelper.httpsPost(url, json, null, null, timeOut);
                    result.setNotifyListener(listener);
                    if (!result.hasError())
                    {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200)
                        {
                            Log.i("hehui", "certification-->" + result.data.getString("message"));
                            
                            return true;
                        }
                        else
                        {
                            listener.onError(result.data.getString("message"));
                            return false;
                        }
                        
                    }
                    else
                    {
                        switch (result.resultCode)
                        {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return false;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return false;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return false;
                        }
                    }
                    
                }
                catch (Exception e)
                {
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }
                
                return false;
            }
        },
            listener);
        
    }
    
    /**
     * 电子钱包申请开通 再一次确认
     * <功能详细描述>
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void ewalletConfirm(final String msg_code, final String phone, final UINotifyListener<Boolean> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<Boolean>()
        {
            @Override
            public Boolean execute()
                throws Exception
            {
                JSONObject json = new JSONObject();
                json.put("accountId", MainApplication.accountId);
                json.put("msg_send_flag", "0");
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId))
                {
                    json.put("mchId", MainApplication.merchantId);
                }
                else
                {
                    json.put("mchId", MainApplication.getMchId());
                }
                json.put("msg_code", msg_code);
                json.put("phone", phone);
                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey()))
                {
                    json.put("sign",
                        SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()),
                            MainApplication.getSignKey()));
                    
                }
                Log.i("hehui", "ewalletConfirm params-->" + json);
                String url = ApiConstant.pushMoneyUrl + "spay/ewallet/ewalletConfirm";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                
                try
                {
                    RequestResult result = NetHelper.httpsPost(url, json, null, null, timeOut);
                    result.setNotifyListener(listener);
                    if (!result.hasError())
                    {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200)
                        {
                            Log.i("hehui", "ewalletConfirm-->" + result.data.getString("message"));
                            
                            return true;
                        }
                        else
                        {
                            listener.onError(result.data.getString("message"));
                            return false;
                        }
                        
                    }
                    else
                    {
                        switch (result.resultCode)
                        {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return false;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return false;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return false;
                        }
                    }
                    
                }
                catch (Exception e)
                {
                    Log.e("hehui", "" + e);
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }
                
                return false;
            }
        },
            listener);
        
    }
    
    /**
     * 电子钱包申请开通 接受短信验证码
     * <功能详细描述>
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void ewalletSignApply(final String phone, final UINotifyListener<Boolean> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<Boolean>()
        {
            @Override
            public Boolean execute()
                throws Exception
            {
                JSONObject json = new JSONObject();
                json.put("accountId", MainApplication.accountId);
                json.put("msg_send_flag", "2");
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId))
                {
                    json.put("mchId", MainApplication.merchantId);
                }
                else
                {
                    json.put("mchId", MainApplication.getMchId());
                }
                json.put("phone", phone);
                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey()))
                {
                    json.put("sign",
                        SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()),
                            MainApplication.getSignKey()));
                    
                }
                Log.i("hehui", "ewalletSignApply params-->" + json);
                String url = ApiConstant.pushMoneyUrl + "spay/ewallet/ewalletSignApply";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                
                try
                {
                    RequestResult result = NetHelper.httpsPost(url, json, null, null, timeOut);
                    result.setNotifyListener(listener);
                    if (!result.hasError())
                    {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200)
                        {
                            Log.i("hehui", "ewalletSignApply-->" + result.data.getString("message"));
                            
                            return true;
                        }
                        else
                        {
                            listener.onError(result.data.getString("message"));
                            return false;
                        }
                        
                    }
                    else
                    {
                        switch (result.resultCode)
                        {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return false;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return false;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return false;
                        }
                    }
                    
                }
                catch (Exception e)
                {
                    Log.e("hehui", "" + e);
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }
                
                return false;
            }
        },
            listener);
        
    }
    
    /**
     * 提现记录查询
     * <功能详细描述>
     * @param money
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void ewalletList(final Integer page_no, final UINotifyListener<List<WalletModel>> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<List<WalletModel>>()
        {
            @Override
            public List<WalletModel> execute()
                throws Exception
            {
                JSONObject json = new JSONObject();
                json.put("accountId", MainApplication.accountId);
                json.put("limit", "99");
                json.put("page_no", page_no);
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId))
                {
                    json.put("mchId", MainApplication.merchantId);
                }
                else
                {
                    json.put("mchId", MainApplication.getMchId());
                }
                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey()))
                {
                    json.put("sign",
                        SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()),
                            MainApplication.getSignKey()));
                    
                }
                String url = ApiConstant.pushMoneyUrl + "spay/ewallet/ewalletList";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                
                try
                {
                    Log.i("hehui", "ewalletList param-->" + json);
                    RequestResult result = NetHelper.httpsPost(url, json, null, null, timeOut);
                    result.setNotifyListener(listener);
                    if (!result.hasError())
                    {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200)
                        {
                            Log.i("hehui", "ewalletList-->" + result.data.getString("message"));
                            
                            JSONObject jsonParam = new JSONObject(result.data.getString("message"));
                            
                            return ((List<WalletModel>)JsonUtil.jsonToList(jsonParam.getString("order_detail"),
                                new com.google.gson.reflect.TypeToken<List<WalletModel>>()
                                {
                                }.getType()));
                        }
                        else
                        {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }
                        
                    }
                    else
                    {
                        switch (result.resultCode)
                        {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return null;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return null;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return null;
                        }
                    }
                    
                }
                catch (Exception e)
                {
                    Log.e("hehui", "" + e);
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }
                
                return null;
            }
        }, listener);
        
    }
    
    /**
     * 获取银行列表
     * <功能详细描述>
     * @param money
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void ewalletCardslist(final String suffixIdCard, final String isUpdateFlag,
        final UINotifyListener<List<WalletModel>> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<List<WalletModel>>()
        {
            @Override
            public List<WalletModel> execute()
                throws Exception
            {
                JSONObject json = new JSONObject();
                json.put("accountId", MainApplication.accountId);
                json.put("suffixIdCard", suffixIdCard);
                if (!StringUtil.isEmptyOrNull(isUpdateFlag))
                {
                    json.put("isUpdateFlag", isUpdateFlag);
                }
                else
                {
                    json.put("isUpdateFlag", "1");
                }
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId))
                {
                    json.put("mchId", MainApplication.merchantId);
                }
                else
                {
                    json.put("mchId", MainApplication.getMchId());
                }
                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey()))
                {
                    json.put("sign",
                        SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()),
                            MainApplication.getSignKey()));
                    
                }
                Log.i("hehui", "ewalletCardslist param->" + json);
                String url = ApiConstant.pushMoneyUrl + "spay/ewallet/ewalletCardslist";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                
                try
                {
                    RequestResult result = NetHelper.httpsPost(url, json, null, null, timeOut);
                    result.setNotifyListener(listener);
                    if (!result.hasError())
                    {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200)
                        {
                            Log.i("hehui", "ewalletCardslist-->" + result.data.getString("message"));
                            
                            JSONObject jsonParam = new JSONObject(result.data.getString("message"));
                            
                            return ((List<WalletModel>)JsonUtil.jsonToList(jsonParam.getString("order_detail"),
                                new com.google.gson.reflect.TypeToken<List<WalletModel>>()
                                {
                                }.getType()));
                        }
                        else
                        {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }
                        
                    }
                    else
                    {
                        switch (result.resultCode)
                        {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return null;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return null;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return null;
                        }
                    }
                    
                }
                catch (Exception e)
                {
                    Log.e("hehui", "" + e);
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }
                
                return null;
            }
        }, listener);
        
    }
    
    /**
     * 验证身份证
     * <功能详细描述>
     * @param money
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void ewalletIdentity(final String suffixIdCard, final String isUpdateFlag,
        final UINotifyListener<List<WalletModel>> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<List<WalletModel>>()
        {
            @Override
            public List<WalletModel> execute()
                throws Exception
            {
                JSONObject json = new JSONObject();
                json.put("accountId", MainApplication.accountId);
                json.put("suffixIdCard", suffixIdCard);
                if (!StringUtil.isEmptyOrNull(isUpdateFlag))
                {
                    json.put("isUpdateFlag", isUpdateFlag);
                }
                else
                {
                    json.put("isUpdateFlag", "1");
                }
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId))
                {
                    json.put("mchId", MainApplication.merchantId);
                }
                else
                {
                    json.put("mchId", MainApplication.getMchId());
                }
                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey()))
                {
                    json.put("sign",
                        SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()),
                            MainApplication.getSignKey()));
                    
                }
                String url = ApiConstant.pushMoneyUrl + "spay/ewallet/ewalletIdentity";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                
                try
                {
                    RequestResult result = NetHelper.httpsPost(url, json, null, null, timeOut);
                    result.setNotifyListener(listener);
                    if (!result.hasError())
                    {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200)
                        {
                            Log.i("hehui", "ewalletIdentity-->" + result.data.getString("message"));
                            
                            JSONObject jsonParam = new JSONObject(result.data.getString("message"));
                            
                            return ((List<WalletModel>)JsonUtil.jsonToList(jsonParam.getString("cards"),
                                new com.google.gson.reflect.TypeToken<List<WalletModel>>()
                                {
                                }.getType()));
                        }
                        else
                        {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }
                        
                    }
                    else
                    {
                        switch (result.resultCode)
                        {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return null;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return null;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return null;
                        }
                    }
                    
                }
                catch (Exception e)
                {
                    Log.e("hehui", "" + e);
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }
                
                return null;
            }
        }, listener);
        
    }
    
    /**
     * 上传图片
     * <功能详细描述>
     * @param money
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void ewalletUpload(final String isUpdateFlag, final String suffixIdCard, final String fileOne,
        final String fileTwo, final UINotifyListener<WalletModel> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<WalletModel>()
        {
            @Override
            public WalletModel execute()
                throws Exception
            {
                JSONObject json = new JSONObject();
                json.put("mchId", MainApplication.getMchId());
                json.put("accountId", MainApplication.accountId);
                json.put("suffixIdCard", suffixIdCard);
                //                json.put("picfilezm", new File(fileOne));
                //                json.put("picfilefm", new File(fileTwo));
                json.put("isFileZm", "0");
                json.put("isFileFm", "0");
                
                if (!StringUtil.isEmptyOrNull(isUpdateFlag))
                {
                    json.put("isUpdateFlag", isUpdateFlag);
                }
                else
                {
                    json.put("isUpdateFlag", "1");
                }
                
                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey()))
                {
                    json.put("sign",
                        SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()),
                            MainApplication.getSignKey()));
                    
                }
                String url = ApiConstant.pushMoneyUrl + "spay/ewallet/ewalletUpload";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                
                //                String url = ApiConstant.pushMoneyUrl + "spay/uploadaccount";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                
                try
                {
                    Log.i("hehui", "ewalletUpload param-->" + json);
                    RequestResult result = NetHelper.httpPostLoadPic(url, json, fileOne, fileTwo);
                    result.setNotifyListener(listener);
                    if (!result.hasError())
                    {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200)
                        {
                            Log.i("hehui", "ewalletUpload-->" + result.data.getString("message"));
                            return (WalletModel)JsonUtil.jsonToBean(result.data.getString("message"), WalletModel.class);
                        }
                        else
                        {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }
                        
                    }
                    else
                    {
                        switch (result.resultCode)
                        {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                break;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                break;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                break;
                        }
                    }
                    
                }
                catch (Exception e)
                {
                    Log.e("hehui", "" + e);
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }
                
                return null;
            }
        },
            listener);
        
    }
    
    /**
     * 是否开通电子钱包
     * <功能详细描述>
     * @param money
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void isHaveEwallet(final UINotifyListener<WalletModel> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<WalletModel>()
        {
            @Override
            public WalletModel execute()
                throws Exception
            {
                JSONObject json = new JSONObject();
                json.put("mchId", MainApplication.getMchId());
                
                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey()))
                {
                    json.put("sign",
                        SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()),
                            MainApplication.getSignKey()));
                    
                }
                String url = ApiConstant.pushMoneyUrl + "spay/ewallet/isHaveEwallet";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                Log.i("hehui", "isHaveEwallet-->");
                try
                {
                    RequestResult result = NetHelper.httpsPost(url, json, null, null, timeOut);
                    result.setNotifyListener(listener);
                    if (!result.hasError())
                    {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200)
                        {
                            Log.i("hehui", "isHaveEwallet-->" + result.data.getString("message"));
                            return (WalletModel)JsonUtil.jsonToBean(result.data.getString("message"), WalletModel.class);
                        }
                        else
                        {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }
                        
                    }
                    else
                    {
                        switch (result.resultCode)
                        {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                break;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                break;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                break;
                        }
                    }
                    
                }
                catch (Exception e)
                {
                    Log.e("hehui", "" + e);
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }
                
                return null;
            }
        },
            listener);
        
    }
}
