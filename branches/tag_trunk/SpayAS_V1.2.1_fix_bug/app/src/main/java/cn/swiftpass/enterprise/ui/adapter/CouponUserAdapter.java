package cn.swiftpass.enterprise.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.CouponUseModel;
import cn.swiftpass.enterprise.ui.widget.ItemUserCoupon;
import cn.swiftpass.enterprise.ui.widget.ImageCache.ImageLoader;

/**
 * 优惠劵用户列表适配 User: w.l Date: 14-03-18 Time: 下午12:29
 */
public class CouponUserAdapter extends ArrayListAdapter<CouponUseModel>
{
    private ListView mListView;
    
    private Context mContext;
    
    private ImageLoader mImageLoader;
    
    public CouponUserAdapter(Context context)
    {
        super(context);
    }
    
    public CouponUserAdapter(Context context, ListView mListView)
    {
        super(context);
        this.mContext = context;
        this.mListView = mListView;
        mImageLoader = new ImageLoader();
    }
    
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        ItemUserCoupon itemUserCoupon = null;
        if (convertView == null)
        {
            convertView = (ItemUserCoupon)LayoutInflater.from(mContext).inflate(R.layout.listitem_user_coupon, null);
            itemUserCoupon = new ItemUserCoupon(mContext, convertView);
            convertView.setTag(itemUserCoupon);
        }
        else
        {
            itemUserCoupon = (ItemUserCoupon)convertView.getTag();
        }
        CouponUseModel model = (CouponUseModel)getItem(position);
        itemUserCoupon.ivUserUogo.setTag(model.getHead());
        itemUserCoupon.setData(model, mListView, mImageLoader);
        return convertView;
    }
}
