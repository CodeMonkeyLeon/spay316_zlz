package com.example.printsdk.A8.printUtils

import android.content.Context
import com.landicorp.android.eptapi.DeviceService
import com.landicorp.android.eptapi.device.Printer
import com.landicorp.android.eptapi.device.Printer.Progress
import com.landicorp.android.eptapi.exception.RequestException
import com.landicorp.android.eptapi.utils.QrCode

class PrintA8Text {


    private var isDeviceServiceLogined = false
    private var mContext: Context? = null
    private var mPrinter: Printer? = null
    private var format: Printer.Format? = null
    private val mList_PrintBean: MutableList<PrintA8Bean> = ArrayList()

    companion object {
        private var printA8Text: PrintA8Text? = null
        fun getInstance(): PrintA8Text? {
            if (printA8Text == null) {
                printA8Text = PrintA8Text()
            }
            return printA8Text
        }
    }

    private val progress: Progress = object : Progress() {
        @Throws(Exception::class)
        override fun doPrint(arg0: Printer) {
        }

        override fun onFinish(code: Int) {
            if (code == Printer.ERROR_NONE) {
            } else {
            }
        }

        override fun onCrash() {
            bindDeviceService()
        }
    }

    fun bindDeviceService() {
        try {
            isDeviceServiceLogined = false
            DeviceService.login(mContext)
            isDeviceServiceLogined = true
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun unbindDeviceService() {
        DeviceService.logout()
        isDeviceServiceLogined = false
    }

    /**
     * Search card and show all track info
     */
    fun startPrint() {
        try {
            progress.start()
        } catch (e: RequestException) {
            bindDeviceService()
        }
    }


    fun init(context: Context?) {
        mContext = context
        progress.addStep { printer ->
            mPrinter = printer
            mPrinter?.let {
                it.setAutoTrunc(true)
                it.setMode(Printer.MODE_VIRTUAL)
            }


            //根据打印需求列表，去映射具体的打印信息
            for (i in mList_PrintBean.indices) {
                getPrintFunction(
                    mList_PrintBean[i].functionName,
                    mList_PrintBean[i].printText
                )
            }
            mPrinter?.let {
                it.printText("\r\n")
            }
            mList_PrintBean.clear()
        }
    }

    private fun getPrintFunction(
        function_name: PrintA8Bean.PRINT_FUNCTION_NAME?,
        content: String?
    ) {
        when (function_name) {
            PrintA8Bean.PRINT_FUNCTION_NAME.TITLE -> printTitle(content)
            PrintA8Bean.PRINT_FUNCTION_NAME.TEXT_LINE_LEFT -> printTextLine(
                Printer.Alignment.LEFT,
                content
            )
            PrintA8Bean.PRINT_FUNCTION_NAME.TEXT_LINE_RIGHT -> printTextLine(
                Printer.Alignment.RIGHT,
                content
            )
            PrintA8Bean.PRINT_FUNCTION_NAME.TEXT_LINE_CENTER -> printTextLine(
                Printer.Alignment.CENTER,
                content
            )
            PrintA8Bean.PRINT_FUNCTION_NAME.TEXT_MULTI_LINES -> printMultiLines(content)
            PrintA8Bean.PRINT_FUNCTION_NAME.SINGLE_LINE -> printSingleLine()
            PrintA8Bean.PRINT_FUNCTION_NAME.DOUBLE_LINE -> printDoubleLine()
            PrintA8Bean.PRINT_FUNCTION_NAME.EMPTY_LINE -> printEmptyLine()
            PrintA8Bean.PRINT_FUNCTION_NAME.QR_CODE -> printQRCode(content)
        }
    }

    fun printInfo(function_name: PrintA8Bean.PRINT_FUNCTION_NAME?, content: String?) {
        val bean = PrintA8Bean()
        bean.functionName = function_name
        bean.printText = content
        mList_PrintBean.add(bean)
    }

    private fun printTitle(text: String?) {
        try {
            format = Printer.Format()
            format?.let {
                it.ascSize = Printer.Format.ASC_DOT24x12
                it.ascScale = Printer.Format.ASC_SC1x2
                it.hzSize = Printer.Format.HZ_DOT24x24
                it.hzScale = Printer.Format.HZ_SC1x2
                it
            }

            mPrinter?.let {
//                it.setFormat(format)
                // 标题
                it.printText(
                    Printer.Alignment.CENTER, """
     $text
     
     """.trimIndent()
                )
                it.printText("\r\n")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun printTextLine(align: Printer.Alignment, text: String?) {
        try {

            format?.let {
                it.ascSize = Printer.Format.ASC_DOT24x12
                it.ascScale = Printer.Format.ASC_SC1x1
                it.hzSize = Printer.Format.HZ_DOT24x24
                it.hzScale = Printer.Format.HZ_SC1x1
            }

            mPrinter?.let {
//                it.setFormat(format)
                it.printText(
                    align, """
     $text
     
     """.trimIndent()
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun printMultiLines(text: String?) {
        try {
            mPrinter!!.printText(
                """
                $text
                
                """.trimIndent()
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun printSingleLine() {
        try {
            mPrinter?.let {
                it.printText("-------------------------------\n")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun printDoubleLine() {
        try {
            mPrinter?.let {
                it.printText("===============================\n")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun printQRCode(orderNoMch: String?) {
        try {
            mPrinter?.let {
                it.printQrCode(
                    Printer.Alignment.CENTER,
                    QrCode(orderNoMch, QrCode.ECLEVEL_Q),
                    184
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun printEmptyLine() {
        try {
            mPrinter?.let {
                it.printText(
                    """ 
"""
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}