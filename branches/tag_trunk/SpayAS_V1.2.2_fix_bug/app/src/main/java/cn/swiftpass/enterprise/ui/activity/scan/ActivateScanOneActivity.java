/*
 * 文 件 名:  ActivateScanOneActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-7-13
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.scan;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ViewFlipper;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.bill.BillSumManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.ScanActiveDialogInfo;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.StringUtil;

/* 固定二维码激活
* 
* @author  he_hui
* @version  [版本号, 2016-7-13]
* @see  [相关类/方法]
* @since  [产品/模块版本]
*/
public class ActivateScanOneActivity extends TemplateActivity implements OnGestureListener
{
    private View[] views = new View[2];
    
    private View firstView, secondView;
    
    private ViewFlipper page;
    
    private LinearLayout lay_next;
    
    private Button but_activate;
    
    private GestureDetector gestureDetector;
    
    private String code = null;
    
    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_main_introduction_one);
        page = getViewById(R.id.viewflipper);
        
        initView();
        //        page.setOrientation(DirectionalViewPager.VERTICAL);
        //        page.setAdapter(new MyAdapter());
        //        page.setCurrentItem(0);
        gestureDetector = new GestureDetector(this);
        setLinster();
        code = getIntent().getStringExtra("code");
    }
    
    public static void startActivity(Context context, String code)
    {
        Intent it = new Intent();
        it.setClass(context, ActivateScanOneActivity.class);
        it.putExtra("code", code);
        context.startActivity(it);
    }
    
    public boolean onTouchEvent(MotionEvent event)
    {
        
        return gestureDetector.onTouchEvent(event); // 注册手势事件  
    }
    
    ScanActiveDialogInfo dialogInfos;
    
    private void setLinster()
    {
        but_activate = (Button)secondView.findViewById(R.id.but_activate);
        setButBgAndFont(but_activate);
        but_activate.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                //                ActivateScanOneActivity.this.finish();
                
                dialogInfos =
                    new ScanActiveDialogInfo(ActivateScanOneActivity.this, new ScanActiveDialogInfo.HandleBtn()
                    {
                        
                        @Override
                        public void handleOkBtn()
                        {
                            if (StringUtil.isEmptyOrNull(code))
                            {
                                return;
                            }
                            BillSumManager.getInstance().qrCodeBind(code, new UINotifyListener<Boolean>()
                            {
                                @Override
                                public void onPreExecute()
                                {
                                    loadDialog(ActivateScanOneActivity.this, R.string.public_data_loading);
                                    super.onPreExecute();
                                }
                                
                                @Override
                                public void onError(Object object)
                                {
                                    dismissLoading();
                                    if (checkSession())
                                    {
                                        return;
                                    }
                                    if (object != null)
                                    {
                                        toastDialog(ActivateScanOneActivity.this,
                                            object.toString(),
                                            new NewDialogInfo.HandleBtn()
                                            {
                                                
                                                @Override
                                                public void handleOkBtn()
                                                {
                                                    finish();
                                                }
                                                
                                            });
                                    }
                                    super.onError(object);
                                }
                                
                                @Override
                                public void onSucceed(Boolean result)
                                {
                                    dismissLoading();
                                    super.onSucceed(result);
                                    if (result)
                                    {
                                        
                                        //                                        showToastInfo(R.string.tv_active_success);
                                        toastDialog(ActivateScanOneActivity.this,
                                            R.string.tv_active_success,
                                            new NewDialogInfo.HandleBtn()
                                            {
                                                
                                                @Override
                                                public void handleOkBtn()
                                                {
                                                    for (Activity a : MainApplication.listActivities)
                                                    {
                                                        a.finish();
                                                    }
                                                    
                                                    ActivateScanOneActivity.this.finish();
                                                }
                                                
                                            });
                                        //                        HandlerManager.notifyMessage(HandlerManager.PAY_ACTIVE_SCAN, HandlerManager.PAY_ACTIVE_SCAN);
                                        //                        MainApplication.isActive = true;
                                        //                        HandlerManager.notifyMessage(HandlerManager.PAY_ACTIVE_SETTING_SCAN,
                                        //                            HandlerManager.PAY_ACTIVE_SETTING_SCAN);
                                    }
                                }
                            });
                        }
                        
                    }, new ScanActiveDialogInfo.HandleBtnCancle()
                    {
                        
                        @Override
                        public void handleCancleBtn()
                        {
                            dialogInfos.dismiss();
                            
                            finish();
                        }
                    });
                
                DialogHelper.resize(ActivateScanOneActivity.this, dialogInfos);
                dialogInfos.show();
                
                //CaptureActivity.startActivity(ActivateScanOneActivity.this, MainApplication.PAY_TYPE_SCAN_OPNE);
            }
        });
    }
    
    private void initView()
    {
        LayoutInflater inflater = LayoutInflater.from(this);
        firstView = inflater.inflate(R.layout.activity_code_introduction_one, null);
        page.addView(firstView, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
        lay_next = (LinearLayout)firstView.findViewById(R.id.lay_next);
        
        lay_next.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                //                page.setCurrentItem(1);
                Animation lInAnim = AnimationUtils.loadAnimation(ActivateScanOneActivity.this, R.anim.push_down_in); // 向左滑动左侧进入的渐变效果（alpha 0.1  -> 1.0）  
                Animation lOutAnim = AnimationUtils.loadAnimation(ActivateScanOneActivity.this, R.anim.push_down_out); // 向左滑动右侧滑出的渐变效果（alpha 1.0  -> 0.1）  
                
                page.setInAnimation(lInAnim);
                page.setOutAnimation(lOutAnim);
                page.showPrevious(); // 调用该函数来显示FrameLayout里面的下一个View。
            }
        });
        
        secondView = inflater.inflate(R.layout.activity_code_introduction_two, null);
        page.addView(secondView, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
    }
    
    public class MyAdapter extends PagerAdapter
    {
        
        @Override
        public int getCount()
        {
            if (views.length < 3)
            {
                return views.length;
            }
            else
            {
                return Integer.MAX_VALUE;
            }
        }
        
        @Override
        public boolean isViewFromObject(View arg0, Object arg1)
        {
            return arg0 == arg1;
        }
        
        @Override
        public void destroyItem(View container, int position, Object object)
        {
            //            ((ViewPager)container).removeView(mImageViews[position % mImageViews.length]);
            
        }
        
        /**
         * 载入图片进去，用当前的position 除以 图片数组长度取余数是关键
         */
        @Override
        public Object instantiateItem(View container, int position)
        {
            try
            {
                
                ((ViewPager)container).addView(views[position % views.length], 0);
            }
            catch (Exception e)
            {
            }
            return views[position % views.length];
        }
        
        @Override
        public void finishUpdate(View arg0)
        {
            // TODO Auto-generated method stub
            
        }
        
        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1)
        {
            // TODO Auto-generated method stub
            
        }
        
        @Override
        public Parcelable saveState()
        {
            // TODO Auto-generated method stub
            return null;
        }
        
        @Override
        public void startUpdate(View arg0)
        {
            // TODO Auto-generated method stub
            
        }
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tv_active_new_sacn);
    }
    
    @Override
    public boolean onDown(MotionEvent e)
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        // TODO Auto-generated method stub
        
        if (e2.getY() - e1.getY() > 120)
        { // 从左向右滑动（左进右出）  
            Animation rInAnim = AnimationUtils.loadAnimation(ActivateScanOneActivity.this, R.anim.page_push_up_in); // 向右滑动左侧进入的渐变效果（alpha  0.1 -> 1.0）  
            Animation rOutAnim = AnimationUtils.loadAnimation(ActivateScanOneActivity.this, R.anim.page_push_up_out); // 向右滑动右侧滑出的渐变效果（alpha 1.0  -> 0.1）  
            
            page.setInAnimation(rInAnim);
            page.setOutAnimation(rOutAnim);
            page.showNext(); // 调用该函数来显示FrameLayout里面的上一个View。
            return true;
        }
        else if (e2.getY() - e1.getY() < -120)
        { // 从右向左滑动（右进左出）  
            Animation lInAnim = AnimationUtils.loadAnimation(ActivateScanOneActivity.this, R.anim.push_down_in); // 向左滑动左侧进入的渐变效果（alpha 0.1  -> 1.0）  
            Animation lOutAnim = AnimationUtils.loadAnimation(ActivateScanOneActivity.this, R.anim.push_down_out); // 向左滑动右侧滑出的渐变效果（alpha 1.0  -> 0.1）  
            
            page.setInAnimation(lInAnim);
            page.setOutAnimation(lOutAnim);
            page.showPrevious(); // 调用该函数来显示FrameLayout里面的下一个View。
            return true;
        }
        
        return true;
    }
    
    @Override
    public void onLongPress(MotionEvent e)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void onShowPress(MotionEvent e)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        // TODO Auto-generated method stub
        return false;
    }
}
