package cn.swiftpass.enterprise.ui.widget;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tencent.stat.StatService;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.MainActivity;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * User: Alan
 * Date: 13-10-28
 * Time: 下午5:24
 */
public class BottomButtonView extends LinearLayout implements View.OnClickListener {
    private static final String TAG = BottomButtonView.class.getCanonicalName();

    private LinearLayout ll_main, ll_order, ll_search, ll_settings;

    private LinearLayout ll_main_m, ll_order_m,  ll_settings_m;

    private ImageView img_pay, img_order, img_settings;

    private TextView tvPay, tvOrder,  tvSetting;

    // private ImageView ivOrder,ivSearch,ivSetting; //W.l去掉 布局里面没有
    private MainActivity activity;

    private String textColor = "#999999";//W.l 修改字体颜色

    private String textDownClolor = ToastHelper.toStr(R.color.title_bg_new); //W.l 添加按下颜色

    private Context context;

    private MyToast myToast;

    private ImageView layTitle;

    protected ProgressDialog loadingDialog = null;

    public BottomButtonView(Context context) {
        super(context);
        this.context = context;
        initViews();
    }

    public BottomButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initViews();
    }

    private void initViews() {
        View row = inflate(getContext(), R.layout.view_bottom, null);
        addView(row, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

        ll_main_m = (LinearLayout) findViewById(R.id.ll_pay_m);

        img_pay = (ImageView) findViewById(R.id.img_pay);
        //        layTitle = (ImageView)findViewById(R.id.layTitle);
        //        layTitle.setVisibility(View.GONE);
        /* ivPay = (ImageView)findViewById(R.id.iv_1);
         ivPay.setImageResource(R.drawable.bg_man_on);*/
        ll_main_m.setOnClickListener(this);
        tvPay = (TextView) findViewById(R.id.tv_pay);

        ll_order_m = (LinearLayout) findViewById(R.id.ll_order_m);
        img_order = (ImageView) findViewById(R.id.img_order);
        //ivOrder = (ImageView)findViewById(R.id.iv_2);
        ll_order_m.setOnClickListener(this);
        tvOrder = (TextView) findViewById(R.id.tv_order);

        // ivSearch = (ImageView)findViewById(R.id.iv_3);

        ll_settings_m = (LinearLayout) findViewById(R.id.ll_settings_m);
        img_settings = (ImageView) findViewById(R.id.img_settings);
        //  ivSetting = (ImageView)findViewById(R.id.iv_4);
        ll_settings_m.setOnClickListener(this);
        tvSetting = (TextView) findViewById(R.id.tv_settings);
    }

    public void setActivity(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_pay_m:
                onPayDown();
                break;
            case R.id.ll_order_m:
                // 如果是收银登录进来
                if (MainApplication.isAdmin.equals("0")) {
                    //                    if (MainApplication.isOrderAuth.equals("0"))
                    //                    {
                    //                        MyToast toast = new MyToast();
                    //                        toast.showToast(context, ToastHelper.toStr(R.string.show_user_stream));
                    //                    }
                    //                    else
                    //                    {
                    onOrderDown();
                    //                    }

                } else {

                    if (!MainApplication.remark.equals("") && !MainApplication.remark.equals("1")) {
                        MyToast toast = new MyToast();
                        toast.showToast(context, ToastHelper.toStr(R.string.show_mch_stream));
                    } else if (MainApplication.remark.equals("0")) {
                        if (!MainApplication.isOrderAuth.equals("") && MainApplication.isOrderAuth.equals("1")) {
                            onOrderDown();
                        }
                    } else {
                        onOrderDown();
                    }
                }
                break;

            case R.id.ll_settings_m:
                onSettingDown();
                break;
        }
    }

    private void onPayDown() {

        activity.setTabView(MainActivity.TAB_PAY);
        img_pay.setImageResource(R.drawable.icon_tab_collection_choose);
        // ivPay.setImageResource(R.drawable.bg_man_on);
        tvPay.setTextColor(Color.parseColor(textDownClolor));

        img_order.setImageResource(R.drawable.icon_tab_bill_default);
        //  ivOrder.setImageResource(0);
        tvOrder.setTextColor(Color.parseColor(textColor));

        img_settings.setImageResource(R.drawable.icon_tab_me_default);
        //  ivSetting.setImageResource(0);
        tvSetting.setTextColor(Color.parseColor(textColor));
    }

    private void onOrderDown() {

        HandlerManager.notifyMessage(HandlerManager.STREAM_BILL, HandlerManager.PAY_SWITCH_TAB);

        try {
            StatService.trackCustomEvent(context, "SPConstTapBillTab", "账单");
        } catch (Exception e) {
        }

        activity.setTabView(MainActivity.TAB_ORDER);
        img_pay.setImageResource(R.drawable.icon_tab_collection_default);
        //  ivPay.setImageResource(0);
        tvPay.setTextColor(Color.parseColor(textColor));
        img_order.setImageResource(R.drawable.icon_tab_bill_choose);
        // ivOrder.setImageResource(R.drawable.bg_man_on);
        tvOrder.setTextColor(Color.parseColor(textDownClolor));

        img_settings.setImageResource(R.drawable.icon_tab_me_default);
        //ivSetting.setImageResource(0);
        tvSetting.setTextColor(Color.parseColor(textColor));
    }


    private void onSettingDown() {
        try {
            StatService.trackCustomEvent(context, "SPConstTapMyTab", "我的");
        } catch (Exception e) {
        }
        activity.setTabView(MainActivity.TAB_SETTING);
        img_pay.setImageResource(R.drawable.icon_tab_collection_default);
        //ivPay.setImageResource(0);
        tvPay.setTextColor(Color.parseColor(textColor));
        img_order.setImageResource(R.drawable.icon_tab_bill_default);
        // ivOrder.setImageResource(0);
        tvOrder.setTextColor(Color.parseColor(textColor));

        img_settings.setImageResource(R.drawable.icon_tab_me_choose);
        //ivSetting.setImageResource(R.drawable.bg_man_on);
        tvSetting.setTextColor(Color.parseColor(textDownClolor));
    }

    public void showLoading(boolean cancelble, String str) {
        //        if (!isResumed)
        //            return;
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(context);
            loadingDialog.setCancelable(cancelble);

        }
        loadingDialog.show();
        loadingDialog.setMessage(str);
    }

    public void dismissMyLoading() {
        if (loadingDialog != null) {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }

    /**
     * 任意线程都可显示toast
     */
    protected void showToastInfo(final String text) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                /*  Toast.makeText(BaseActivity.this, text,
                          isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();*/

                myToast = new MyToast();
                myToast.showToast(context, text);
            }
        });
    }

}
