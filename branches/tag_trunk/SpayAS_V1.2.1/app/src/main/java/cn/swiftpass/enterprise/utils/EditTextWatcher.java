package cn.swiftpass.enterprise.utils;

import android.text.Editable;
import android.text.TextWatcher;

public class EditTextWatcher implements TextWatcher
{
	public interface OnTextChanged
	{
		public void onExecute(CharSequence s, int start, int before, int count);
		public void onAfterTextChanged(Editable s);
	}
	private OnTextChanged onTextChanged;
	public void setOnTextChanaged(OnTextChanged onTextChanged)
	{
		this.onTextChanged = onTextChanged; 
	}
	
	@Override
	public void afterTextChanged(Editable s)
	{
		onTextChanged.onAfterTextChanged(s);
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after)
	{
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count)
	{
		onTextChanged.onExecute( s,  start,  before,  count);
	}
}
