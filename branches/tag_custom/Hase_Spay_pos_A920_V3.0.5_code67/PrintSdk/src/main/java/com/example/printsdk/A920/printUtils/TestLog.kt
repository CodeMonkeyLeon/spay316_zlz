package com.example.printsdk.A920.printUtils

import android.util.Log

open class TestLog {

    private var childName = ""

    init {
        childName = javaClass.simpleName + "."
    }

    fun logTrue(method: String) {
        Log.i("IPPITest", childName + method)
    }

    fun logErr(method: String, errString: String) {
        Log.e("IPPITest", "$childName$method   出错信息：$errString")
    }

}