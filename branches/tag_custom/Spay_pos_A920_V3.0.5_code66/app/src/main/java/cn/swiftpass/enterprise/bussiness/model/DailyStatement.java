package cn.swiftpass.enterprise.bussiness.model;

import java.util.Date;

/**
 *当日统计
 * User: Alan
 * Date: 13-10-14
 * Time: 下午9:29
 */
public class DailyStatement {

    public int id;
    public int cgCount;
    public int cgMoney;
    public int sbCount;
    public int sbMoney;
    public int tkCount;
    public int tkMoney;
    public int state;
    public Date addTime;
}
