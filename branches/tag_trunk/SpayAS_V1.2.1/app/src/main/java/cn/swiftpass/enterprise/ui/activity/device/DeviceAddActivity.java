/*
 * 文 件 名:  DeviceAddActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-9-5
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.device;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 设备号添加
 * 
 * @author  he_hui
 * @version  [版本号, 2016-9-5]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class DeviceAddActivity extends TemplateActivity
{
    private EditText goods_name_edit;
    
    private Button goods_name_submit_btn;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_add);
        
        goods_name_edit = getViewById(R.id.goods_name_edit);
        goods_name_submit_btn = getViewById(R.id.goods_name_submit_btn);
        showSoftInputFromWindow(DeviceAddActivity.this, goods_name_edit);
        goods_name_submit_btn.getBackground().setAlpha(102);
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        {
            
            @Override
            public void onExecute(CharSequence s, int start, int before, int count)
            {
            }
            
            @Override
            public void onAfterTextChanged(Editable s)
            {
                if (goods_name_edit.isFocused())
                {
                    if (goods_name_edit.getText().toString().length() > 0)
                    {
                        setButtonBg(goods_name_submit_btn, true, 0);
                    }
                    else
                    {
                        setButtonBg(goods_name_submit_btn, false, 0);
                    }
                }
            }
        });
        goods_name_edit.addTextChangedListener(editTextWatcher);
        
        goods_name_submit_btn.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (StringUtil.isEmptyOrNull(goods_name_edit.getText().toString()))
                {
                    toastDialog(DeviceAddActivity.this, R.string.tv_device_input, null);
                    return;
                }
                
                UserManager.updateOrAddDevice(goods_name_edit.getText().toString(), new UINotifyListener<Boolean>()
                {
                    @Override
                    public void onPreExecute()
                    {
                        showNewLoading(true, getStringById(R.string.public_submitting));
                        super.onPreExecute();
                    }
                    
                    public void onError(Object object)
                    {
                        dismissLoading();
                        if (object != null)
                            toastDialog(DeviceAddActivity.this, object.toString(), null);
                    };
                    
                    @Override
                    public void onSucceed(Boolean result)
                    {
                        dismissLoading();
                        super.onSucceed(result);
                        
                        if (result)
                        {
                            toastDialog(DeviceAddActivity.this,
                                R.string.submit_success_auditing,
                                new NewDialogInfo.HandleBtn()
                                {
                                    
                                    @Override
                                    public void handleOkBtn()
                                    {
                                        HandlerManager.notifyMessage(HandlerManager.VICE_SWITCH,
                                            HandlerManager.DEVICE_SWITCH,
                                            goods_name_edit.getText().toString());
                                        
                                        finish();
                                        
                                    }
                                });
                        }
                    }
                });
            }
        });
        
        loadDevice();
        
        /**
         * 可配置设置
         */
        DynModel dynModel = (DynModel)SharedPreUtile.readProduct("dynModel" + ApiConstant.bankCode);
        if (null != dynModel)
        {
            try
            {
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonColor()))
                {
                    goods_name_submit_btn.setBackgroundColor(Color.parseColor(dynModel.getButtonColor()));
                }
                
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonFontColor()))
                {
                    goods_name_submit_btn.setTextColor(Color.parseColor(dynModel.getButtonFontColor()));
                }
            }
            catch (Exception e)
            {
                Log.e("hehui", "TemplateActivity setContentView " + e);
            }
        }
    }
    
    @Override
    protected void onResume()
    {
        super.onResume();
        
    }
    
    private void loadDevice()
    {
        UserManager.queryMchDevice(new UINotifyListener<String>()
        {
            @Override
            public void onPreExecute()
            {
                showNewLoading(true, getStringById(R.string.public_submitting));
                super.onPreExecute();
            }
            
            public void onError(Object object)
            {
                dismissLoading();
                //                if (object != null)
                //                    showToastInfo(object.toString());
            };
            
            @Override
            public void onSucceed(String result)
            {
                dismissLoading();
                super.onSucceed(result);
                
                if (!StringUtil.isEmptyOrNull(result))
                {
                    if (null != goods_name_edit)
                    {
                        goods_name_edit.setText(result);
                        goods_name_submit_btn.setText(R.string.btn_update);
                        
                        goods_name_edit.setFocusable(true);
                        goods_name_edit.setFocusableInTouchMode(true);
                        goods_name_edit.requestFocus();
                        goods_name_edit.setSelection(goods_name_edit.getText().length());
                        
                        if (MainApplication.isAdmin.equals("0"))
                        {
                            goods_name_submit_btn.setVisibility(View.GONE);
                            titleBar.setRightButLayVisibleForTotal(false, getString(R.string.tv_device_title_list));
                        }
                    }
                    
                }
            }
        });
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setTitle(getStringById(R.string.tv_device_info));
        titleBar.setLeftButtonVisible(true);
        titleBar.setRightButtonVisible(false);
        titleBar.setRightButLayVisibleForTotal(false, getString(R.string.tv_device_title_list));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                //                finish();
                showPage(DeviceManagerActivity.class);
            }
            
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
        });
    }
}
