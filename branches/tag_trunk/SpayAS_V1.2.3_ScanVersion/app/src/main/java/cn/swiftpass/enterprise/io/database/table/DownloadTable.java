/**
 * @description: 下载信息表结构
 * @author chenshiqiang E-mail:csqwyyx@163.com
 * @date 2012-12-4 下午04:08:41   
 * @version 1.0 
 * copyrights reserved by Petfone 2007-2011   
 */
package cn.swiftpass.enterprise.io.database.table;

import android.database.sqlite.SQLiteDatabase;

public class DownloadTable extends TableBase {
	
	// 表名
	public static final String TABLE_DOWNLOAD = "tb_download";
	/**downloadId*/
	public static final String COLUMN_DOWNLOAD_ID = "downloadId";
		
	// 下载名字 
	public static final String COLUMN_DOWN_NAME = "down_name";
	
	// url 
	public static final String COLUMN_DOWN_URL = "down_url";
	
	// 保存路径 
	public static final String COLUMN_DOWN_SAVE_PATH = "down_save_path";
	
	// 下载状态
	public static final String COLUMN_DOWN_STATUS = "down_status";
	
	// 总字节数 
	public static final String COLUMN_DOWN_TOTAL_BYTES = "down_total_bytes";
	
	// 当前下载的字节数 
	public static final String COLUMN_DOWN_CURRENT_BYTES = "down_current_bytes";
	
	// 扩展1 
	public static final String COLUMN_DOWN_EXTEND1 = "down_extend1";
	
	// 扩展2
	public static final String COLUMN_DOWN_EXTEND2 = "down_extend2";
	
	// 扩展3
	public static final String COLUMN_DOWN_EXTEND3 = "down_extend3";
	
	// 扩展4
	public static final String COLUMN_DOWN_EXTEND4 = "down_extend4";
	
	public DownloadTable() {
	}
	
	@Override
	public void createTable(SQLiteDatabase db) {
		StringBuffer sb = new StringBuffer();
		sb.append("CREATE TABLE  IF NOT EXISTS ");
		sb.append(TABLE_DOWNLOAD);
		sb.append("( ");
		sb.append(COLUMN_ID);
		sb.append(" INTEGER PRIMARY KEY AUTOINCREMENT,");
		sb.append(COLUMN_DOWNLOAD_ID);
		sb.append(" Long,");
		sb.append(COLUMN_DOWN_NAME);
		sb.append(" TEXT,");
		sb.append(COLUMN_DOWN_URL);
		sb.append(" TEXT,");
		sb.append(COLUMN_DOWN_SAVE_PATH);
		sb.append(" TEXT,");
		sb.append(COLUMN_DOWN_STATUS);
		sb.append(" Integer,");
		sb.append(COLUMN_DOWN_TOTAL_BYTES);
		sb.append(" BIGINT,");
		sb.append(COLUMN_DOWN_CURRENT_BYTES);
		sb.append(" BIGINT,");

		sb.append(COLUMN_DOWN_EXTEND1);
		sb.append(" TEXT,");
		sb.append(COLUMN_DOWN_EXTEND2);
		sb.append(" TEXT,");
		sb.append(COLUMN_DOWN_EXTEND3);
		sb.append(" Integer,");
		sb.append(COLUMN_DOWN_EXTEND4);
		sb.append(" Integer");

		sb.append(");");
		
		db.execSQL(sb.toString());
	}
	public void deleteTable(SQLiteDatabase db)
	{
		String sql = "drop table if exists "+TABLE_DOWNLOAD;
		db.execSQL(sql);
	}
}
