/*
 * 文 件 名:  UserManager.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-17
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.logica.user;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.ForgetPSWBean;
import cn.swiftpass.enterprise.bussiness.model.GoodsMode;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.NetHelper;
import cn.swiftpass.enterprise.utils.AESHelper;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.MD5;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;
import cn.swiftpass.enterprise.utils.Utils;

/**
 * 注册，忘记密码，等操作业务类
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-17]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class UserManager {


    private final static String TAG = UserManager.class.getCanonicalName();

    /**
     * 查询收银员列表
     * <功能详细描述>
     *
     * @param telNum    商户手机号码
     * @param shop_name 商户名称
     * @param code      验证码
     * @see [类、类#方法、类#成员]
     */
    public static void queryCashier(final int page, final int pageSize, final String input, final UINotifyListener<List<UserModel>> listener) {
        ThreadHelper.executeWithCallback(new Executable() {
            @Override
            public List<UserModel> execute()

            {
                try {
                    JSONObject json = new JSONObject();
                    Map<String, String> params = new HashMap<String, String>();

                    if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                        json.put("mchId", MainApplication.merchantId);
                        params.put("mchId", MainApplication.merchantId);
                    } else {
                        json.put("mchId", MainApplication.getMchId());
                        params.put("mchId", MainApplication.getMchId());

                    }
                    json.put("page", page);
                    params.put("page", page + "");
                    if (null != input) {
                        json.put("searchContent", input);
                        params.put("searchContent", input);
                    }
                    if (pageSize > 0) {

                        params.put("pageSize", pageSize + "");
                        json.put("pageSize", pageSize);
                    }
                    if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey())) {
                        json.put("sign", SignUtil.getInstance().createSign(params, MainApplication.getSignKey()));
                    }

                    long spayRs = System.currentTimeMillis();
                    //                    //加签名
                    if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                        json.put("spayRs", spayRs);
                        json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                    }
                    Log.i("hehui", "sign-->" + json);

                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/queryPageCashier", json, String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);

                    //                    Log.i("hehui", "queryCashier-->" + result.data);
                    if (!result.hasError()) {

                        if (result.data == null) {
                            return null;
                        }

                        int resCode = Integer.parseInt(result.data.getString("result"));
                        Log.i("hehui", "queryCashier-->" + result.data);
                        if (resCode == 200) {
                            JSONObject object = new JSONObject(result.data.getString("message"));

                            int pageCount = object.getInt("pageCount");
                            Gson gson = new Gson();
                            List<UserModel> userModels = gson.fromJson(object.getString("data"), new TypeToken<List<UserModel>>() {
                            }.getType());
                            if (userModels != null && userModels.size() > 0) {
                                UserModel userModel = userModels.get(0);
                                userModel.setPageCount(pageCount);
                                userModels.set(0, userModel);
                            } else {
                                listener.onSucceed(null);
                            }
                            return userModels;
                        } else {
                            listener.onError(result.data.getString("message"));
                        }
                    }

                } catch (JSONException e) {
                    listener.onError("收银员列表加载失败，请稍后在试!");
                    return null;
                }
                return null;

            }

        }, listener);
    }

    /**
     * 收银员删除
     * <功能详细描述>
     *
     * @param telNum    商户手机号码
     * @param shop_name 商户名称
     * @param code      验证码
     * @see [类、类#方法、类#成员]
     */
    public static void cashierDelete(final long userId, final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable() {
            @Override
            public Boolean execute()

            {
                try {
                    JSONObject json = new JSONObject();
                    Map<String, String> params = new HashMap<String, String>();
                    json.put("id", userId + "");
                    params.put("id", userId + "");

                    if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                        params.put("mchId", MainApplication.merchantId);
                        json.put("mchId", MainApplication.merchantId);
                    } else {
                        json.put("mchId", MainApplication.getMchId());
                        params.put("mchId", MainApplication.getMchId());
                    }

                    if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey())) {
                        json.put("sign", SignUtil.getInstance().createSign(params, MainApplication.getSignKey()));

                    }

                    long spayRs = System.currentTimeMillis();
                    //                    //加签名
                    if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                        json.put("spayRs", spayRs);
                        json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                    }

                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/deleteSpayUser", json, String.valueOf(spayRs), null);

                    result.setNotifyListener(listener);
                    if (!result.hasError()) {

                        if (result.data == null) {
                            return false;
                        }

                        int resCode = Integer.parseInt(result.data.getString("result"));

                        if (resCode == 200) {
                            return true;
                        } else {
                            listener.onError(result.data.getString("message"));
                        }
                    }

                } catch (JSONException e) {
                    listener.onError("添加收银员失败，请稍后在试!");
                    return false;
                }
                return false;

            }

        }, listener);
    }

    /**
     * 收银员重置密码
     * <功能详细描述>
     * @param userId
     * 收银员ID
     */
    public static void cashierResetPsw(final long userId, final UINotifyListener<Boolean> listener){
        ThreadHelper.executeWithCallback(new Executable() {
            @Override
            public Boolean execute()

            {
                try {
                    JSONObject json = new JSONObject();
                    Map<String, String> params = new HashMap<String, String>();
                    json.put("userId", userId + "");
                    params.put("userId", userId + "");

                    if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                        params.put("mchId", MainApplication.merchantId);
                        json.put("mchId", MainApplication.merchantId);
                    } else {
                        json.put("mchId", MainApplication.getMchId());
                        params.put("mchId", MainApplication.getMchId());
                    }

                    long spayRs = System.currentTimeMillis();
                    if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                        json.put("spayRs", spayRs);
                        json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                    }

                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/resetPwd2Default", json, String.valueOf(spayRs), null);

                    result.setNotifyListener(listener);
                    if (!result.hasError()) {

                        if (result.data == null) {
                            return false;
                        }

                        int resCode = Integer.parseInt(result.data.getString("result"));

                        if (resCode == 200) {
                            return true;
                        } else {
                            listener.onError(result.data.getString("message"));
                        }
                    }

                } catch (JSONException e) {
                    listener.onError(ToastHelper.toStr(R.string.reset_psw_faild));
                    return false;
                }
                return false;

            }

        }, listener);
    }

    /**
     * 收银员添加
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    public static void cashierAdd(final UserModel userModel, final boolean isAddCashier,final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable() {
            @Override
            public Boolean execute()

            {
                try {
                    JSONObject json = new JSONObject();
                    Map<String, String> params = new HashMap<String, String>();
                    json.put("realname", userModel.getRealname());
                    json.put("username", userModel.getUserName());
                    json.put("isRefundAuth", userModel.getIsRefundAuth());
                    json.put("isOrderAuth", userModel.getIsOrderAuth());
                    json.put("isUnfreezeAuth", userModel.getIsUnfreezeAuth());
                    json.put("enabled", userModel.getEnabled());

                    if(!isAddCashier){
                        json.put("isTotalAuth", userModel.getIsTotalAuth());
                    }

                    long spayRs = System.currentTimeMillis();
                    if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                        json.put("mchId", MainApplication.merchantId);
                    } else {
                        json.put("mchId", MainApplication.getMchId());
                    }

                    if (userModel.getId() != 0) {
                        json.put("id", userModel.getId() + "");
                    }

                    //加签名
                    if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                        json.put("spayRs", spayRs);
                        json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                    }

                    String Cashier_url = "";
                    if(isAddCashier){
                        Cashier_url = ApiConstant.CASHIERCREATE;
                    }else{
                        Cashier_url = ApiConstant.CASHIERADDNEW;
                    }
                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + Cashier_url, json, String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    if (!result.hasError()) {
                        if (result.data == null) {
                            return false;
                        }

                        int resCode = Integer.parseInt(result.data.getString("result"));

                        if (resCode == 200) {
                            return true;
                        }else if(resCode == 405){//Require to renegotiate ECDH key
                            listener.onError(405+result.data.getString("message"));
                        } else {
                            listener.onError(result.data.getString("message"));
                        }
                    }

                } catch (JSONException e) {
                    listener.onError("Failed to create cashier");
                    return false;
                }
                return false;

            }

        }, listener);
    }

    /**
     * 检查是商户还是手机号码
     * <功能详细描述>
     *
     * @param userModel
     * @param code
     * @param listener
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static void checkForgetPwdInput(final String input, final UINotifyListener<ForgetPSWBean> listener) {
        ThreadHelper.executeWithCallback(new Executable() {

            @Override
            public ForgetPSWBean execute() {
                JSONObject json = new JSONObject();
                try {
                    json.put("email", input);

                    long spayRs = System.currentTimeMillis();
                    //                    //加签名
                    json.put("spayRs", String.valueOf(spayRs));

                    if(!StringUtil.isEmptyOrNull(MainApplication.skey)){
                        String priKey = (String) SharedPreUtile.readProduct("secretKey");
                        json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(priKey).substring(0,16)));

                    }else{
                        json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(String.valueOf(spayRs))));
                    }
//                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/checkForgetPwdInput", json, String.valueOf(spayRs), null);
                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/forgetPwdAndGetEmailCode", json, String.valueOf(spayRs), null);

                    result.setNotifyListener(listener);
                    if (!result.hasError()) {

                        if (result.data == null) {
                            return null;
                        }
                        int resCode = Integer.parseInt(result.data.getString("result"));

                        switch (resCode) {
                            case 200://成功
                                return (ForgetPSWBean) JsonUtil.jsonToBean(result.data.getString("message"), ForgetPSWBean.class);
                            default:
                                listener.onError(result.data.getString("message"));
                                return null;
                        }
                    } else {

                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return null;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return null;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return null;
                        }
                        listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                    }
                    return null;
                } catch (JSONException e) {

                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                    return null;
                }

            }

        }, listener);

    }

    /**
     * 检查验证码是否正确
     * <功能详细描述>
     *
     * @param userModel
     * @param code
     * @param listener
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static void checkPhoneCode(final String token, final String code, final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable() {

            @Override
            public Boolean execute() {
                JSONObject json = new JSONObject();
                try {
                    json.put("token", token);
                    json.put("emailCode", code);

                    long spayRs = System.currentTimeMillis();
                    json.put("spayRs", String.valueOf(spayRs));
                    //加签名
                  /*  String priKey = (String) SharedPreUtile.readProduct("secretKey");
                    json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(priKey).substring(0,16)));*/
                    if(!StringUtil.isEmptyOrNull(MainApplication.skey)){
                        String priKey = (String) SharedPreUtile.readProduct("secretKey");
                        json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(priKey).substring(0,16)));

                    }else{
                        json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(String.valueOf(spayRs))));
                    }

//                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/checkEmailCode", json, String.valueOf(spayRs), null);
                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/checkEmailCodeV2", json, String.valueOf(spayRs), null);

                    result.setNotifyListener(listener);
                    if (!result.hasError()) {

                        if (result.data == null) {
                            return false;
                        }
                        int resCode = Integer.parseInt(result.data.getString("result"));
                        switch (resCode) {
                            case 200://成功
                                return true;
                            default:
                                result.returnErrorMessage();
                                break;
                        }
                    }
                } catch (JSONException e) {

                    return false;
                }

                return null;
            }

        }, listener);

    }

    /**
     * 忘记密码检查
     * <功能详细描述>
     *
     * @param token
     * @param emailcode
     * @param password
     * @param listener
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static void checkData(final String token, final String emailcode,final String password,final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable() {

            @Override
            public Boolean execute() {
                JSONObject json = new JSONObject();
                try {
                    long spayRs = System.currentTimeMillis();
//                    String paseStr = MD5.md5s(spayRs + "2017SWIFTpassSPAYCCC").toUpperCase();
                    String priKey = (String) SharedPreUtile.readProduct("secretKey");
                    String paseStr = MD5.md5s(priKey).toUpperCase();
                    json.put("token",token);
                    json.put("emailCode",emailcode);
                    json.put("skey",MainApplication.skey);
                    json.put("password", AESHelper.aesEncrypt(password, paseStr.substring(8, 24)));
                    json.put("repPassword", AESHelper.aesEncrypt(password, paseStr.substring(8, 24)));

                    json.put("spayRs", String.valueOf(spayRs));
                  /*  json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(priKey).substring(0,16)));*/

                    if(!StringUtil.isEmptyOrNull(MainApplication.skey)){
                        json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(priKey).substring(0,16)));
                    }else{
                        json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(String.valueOf(spayRs))));
                    }
//                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/setPasswordForEmail", json, String.valueOf(spayRs), null);
                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/setPasswordForEmailV2", json, String.valueOf(spayRs), null);

                    result.setNotifyListener(listener);
                    if (!result.hasError()) {

                        if (result.data == null) {
                            return false;
                        }
                        int resCode = Integer.parseInt(result.data.getString("result"));
                        switch (resCode) {
                            case 200://成功
                                return true;
                            case 405:
                                listener.onError("405"+result.data.getString("message"));
                                return false;
                            default:
                                result.returnErrorMessage();
                                break;
                        }
                    }
                } catch (JSONException e) {

                    return false;
                }

                return null;
            }

        }, listener);

    }

    /**
     * 修改增加查询商品名称
     * <功能详细描述>
     *
     * @param telNumber
     * @param dataType
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public static void queryBody(final UINotifyListener<GoodsMode> listener) {
        ThreadHelper.executeWithCallback(new Executable() {

            @Override
            public GoodsMode execute() throws Exception {
                JSONObject json = new JSONObject();

                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);
                } else {
                    json.put("mchId", MainApplication.getMchId());
                }

                long spayRs = System.currentTimeMillis();
                //                    //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                    json.put("spayRs", spayRs);
                    json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                }

                GoodsMode goodsMode = new GoodsMode();
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/findTradeRemark", json, String.valueOf(spayRs), null);
                result.setNotifyListener(listener);
                if (!result.hasError()) {

                    if (result.data == null) {
                        return null;
                    }
                    int resCode = Integer.parseInt(result.data.getString("result"));

                    if (resCode == 200) {
                        JSONObject jsonObject = new JSONObject(result.data.getString("message"));
                        goodsMode.setBody(jsonObject.optString("commodityName", ""));
                        goodsMode.setCount(jsonObject.optString("commodityNum", ""));
                        return goodsMode;
                    } else {
                        listener.onError(result.data.getString("message"));
                    }

                } else {

                    switch (result.resultCode) {
                        case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                            listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                        case RequestResult.RESULT_TIMEOUT_ERROR:
                            listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                        case RequestResult.RESULT_READING_ERROR:
                            listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                    }
                }
                return null;
            }

        }, listener);

    }

    /**
     * 修改增加商品名称
     * <功能详细描述>
     *
     * @param telNumber
     * @param dataType
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public static void updateOrAddBody(final String body, final String count, final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable() {

            @Override
            public Boolean execute() throws Exception {
                JSONObject json = new JSONObject();

                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);
                } else {
                    json.put("mchId", MainApplication.getMchId());
                }
                json.put("commodityName", body);
                json.put("commodityNum", count);
                long spayRs = System.currentTimeMillis();
                //                    //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                    json.put("spayRs", spayRs);
                    json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                }

                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/addTradeRemark", json, String.valueOf(spayRs), null);
                result.setNotifyListener(listener);
                if (!result.hasError()) {

                    if (result.data == null) {
                        return false;
                    }
                    int resCode = Integer.parseInt(result.data.getString("result"));

                    if (resCode == 200) {
                        return true;
                    } else {
                        listener.onError(result.data.getString("message"));
                    }
                    return false;

                }
                switch (result.resultCode) {
                    case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                        listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                    case RequestResult.RESULT_TIMEOUT_ERROR:
                        listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                    case RequestResult.RESULT_READING_ERROR:
                        listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                }
                return false;
            }

        }, listener);

    }

    /**
     * 获取验证码
     * <功能详细描述>
     *
     * @param telNumber 手机号码
     * @param listener
     * @param dataType  1、注册，2、忘记密码 等获取密码
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static void getCode(final String input, final UINotifyListener<ForgetPSWBean> listener) {
        ThreadHelper.executeWithCallback(new Executable() {

            @Override
            public ForgetPSWBean execute() throws Exception {
                JSONObject json = new JSONObject();

                json.put("email", input);
                long spayRs = System.currentTimeMillis();
                //加签名
                String priKey = (String) SharedPreUtile.readProduct("secretKey");
                json.put("spayRs", String.valueOf(spayRs));

                if(!StringUtil.isEmptyOrNull(MainApplication.skey)){
                    json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(priKey).substring(0,16)));

                }else{
                    json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(String.valueOf(spayRs))));
                }


//                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/getEmailCode", json, String.valueOf(spayRs), null);
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/forgetPwdAndGetEmailCode", json, String.valueOf(spayRs), null);

                //                result.setNotifyListener(listener);
                if (!result.hasError()) {
                    if (result.data == null) {
                        return null;
                    }
                    int resCode = Integer.parseInt(result.data.getString("result"));

                    switch (resCode) {
                        case 200:
                           /* JSONObject object = new JSONObject(result.data.optString("message", ""));
                            String tel = object.optString("telephone", "");
                            if (StringUtil.isEmptyOrNull(tel)) {
                                return object.optString("email", "");
                            }
                            return tel;*/
                        return (ForgetPSWBean) JsonUtil.jsonToBean(result.data.getString("message"), ForgetPSWBean.class);
                        default:
                            listener.onError(result.data.getString("message"));
                            break;
                    }
                    return null;

                } else {
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }
                return null;
            }

        }, listener);

    }
}
