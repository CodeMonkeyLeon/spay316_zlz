/*
 * 文 件 名:  TotalActivityAdapter.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  yan_shuo
 * 修改时间:  2015-4-30
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.adapter;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import cn.swiftpass.enterprise.intl.R;

/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  Administrator
 * @version  [版本号, 2015-4-30]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class TotalActivityAdapter extends BaseAdapter
{
    private Context context;
    
    private List<Map<String, String>> list;
    
    public TotalActivityAdapter(Context context, List<Map<String, String>> list)
    {
        this.context = context;
        this.list = list;
    }
    
    @Override
    public int getCount()
    {
        return list.size();
    }
    
    @Override
    public Object getItem(int position)
    {
        return list.get(position);
    }
    
    @Override
    public long getItemId(int position)
    {
        return position;
    }
    
    /** {@inheritDoc} */
    
    @Override
    public boolean isEnabled(int position)
    {
        // TODO Auto-generated method stub
        //        return super.isEnabled(position);
        return false;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        convertView = View.inflate(context, R.layout.total_listview_item_layout, null);
        return convertView;
    }
    
}
