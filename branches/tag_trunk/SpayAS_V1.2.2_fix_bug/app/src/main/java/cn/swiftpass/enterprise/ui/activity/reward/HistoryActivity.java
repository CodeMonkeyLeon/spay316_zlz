package cn.swiftpass.enterprise.ui.activity.reward;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.order.HistoryRankingManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.AwardModel;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.adapter.HistoryActivityAdapter;
import cn.swiftpass.enterprise.ui.widget.TitleBar;

/**
 * 
 * 历史排名榜
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2015-12-17]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class HistoryActivity extends TemplateActivity implements OnItemClickListener
{
    private ListView cashier_listview_hostory;
    
    private HistoryActivityAdapter adapter;
    
    private List<String> list;
    
    private LinearLayout lay_activity;
    
    List<AwardModel> awardModelList = new ArrayList<AwardModel>();
    
    public static void startActivity(Context context)
    {
        Intent it = new Intent();
        it.setClass(context, HistoryActivity.class);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(it);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.history_activity_ranking);
        
        initViews();
        
        //        initObject();
        
        loadDate();
    }
    
    private void initObject()
    {
        // TODO Auto-generated method stub
        //        list = new ArrayList<String>();
        //        list.add("4444");
        //        list.add("dfgsfdg");
        //        list.add("dfgsfdg234");
        //        list.add("222dfgsfdg234");
        //        adapter = new HistoryActivityAdapter(HistoryActivity.this, list);
        //        cashier_listview_hostory.setAdapter(adapter);
        //        cashier_listview_hostory.setOnItemClickListener(this);
    }
    
    private void loadDate()
    {
        HistoryRankingManager.getInstance().queryHistoryActive("0", new UINotifyListener<List<AwardModel>>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                
                showNewLoading(true, getString(R.string.public_data_loading));
            }
            
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dismissLoading();
                if (checkSession())
                {
                    return;
                }
                if (object != null)
                {
                    
                    showToastInfo(object.toString());
                }
            }
            
            @Override
            public void onSucceed(List<AwardModel> result)
            {
                super.onSucceed(result);
                dismissLoading();
                
                if (result != null && result.size() > 0)
                {
                    awardModelList = result;
                    adapter = new HistoryActivityAdapter(HistoryActivity.this, result);
                    cashier_listview_hostory.setAdapter(adapter);
                }
                else
                {
                    lay_activity.setVisibility(View.VISIBLE);
                }
                
            }
        });
        
    }
    
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }
    
    private void initViews()
    {
        cashier_listview_hostory = (ListView)findViewById(R.id.cashier_history_list);
        lay_activity = getViewById(R.id.lay_activity);
        cashier_listview_hostory.setOnItemClickListener(this);
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tx_history_rank);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
            }
            
            @Override
            public void onLeftButtonClick()
            {
                //                showPage(CashierManager.class);
                finish();
            }
        });
    }
    
    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
    {
        AwardModel awardModel = awardModelList.get(position);
        CashierHistoryActivity.startActivity(HistoryActivity.this, awardModel.getActiveId(), awardModel.getActiveName());
    }
}
