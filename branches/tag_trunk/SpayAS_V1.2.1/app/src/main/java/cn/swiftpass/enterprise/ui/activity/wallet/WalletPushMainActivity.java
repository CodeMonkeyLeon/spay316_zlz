/*
 * 文 件 名:  WalletMainActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-1
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.wallet;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.wallet.WalletManager;
import cn.swiftpass.enterprise.bussiness.model.WalletModel;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.TitleBar;

/**
 * 电子钱包 提现住界面
 * 
 * @author  he_hui
 * @version  [版本号, 2016-11-1]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class WalletPushMainActivity extends TemplateActivity
{
    
    private Button btn_next_step;
    
    private TextView tx_peop;
    
    private EditText et_id;
    
    /**
     * 加载是否开通电子钱包
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void loadisHaveEwallet()
    {
        WalletManager.getInstance().isHaveEwallet(new UINotifyListener<WalletModel>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                loadDialog(WalletPushMainActivity.this, R.string.public_data_loading);
            }
            
            @Override
            public void onPostExecute()
            {
                super.onPostExecute();
            }
            
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                dissDialog();
                if (checkSession())
                {
                    return;
                }
                if (null != object)
                {
                    toastDialog(WalletPushMainActivity.this, object.toString(), null);
                }
            }
            
            @Override
            public void onSucceed(WalletModel model)
            {
                dissDialog();
                if (null != model)
                {
                    MainApplication.isHasEwallet = model.getIsHasEwallet();
                }
            }
        });
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_push_main_layout);
        
        initView();
        setLister();
    }
    
    private void setLister()
    {
        btn_next_step.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                showPage(PushMoneyActivity.class);
            }
        });
    }
    
    private void initView()
    {
        btn_next_step = getViewById(R.id.btn_open);
        tx_peop = getViewById(R.id.tx_peop);
        et_id = getViewById(R.id.et_id);
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        
        titleBar.setTitle(R.string.bt_apply_open);
        titleBar.setLeftButtonIsVisible(false);
        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.tx_apply_balance_list));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                //流水
                showPage(WalletPushStreamActivity.class);
            }
        });
    }
}
