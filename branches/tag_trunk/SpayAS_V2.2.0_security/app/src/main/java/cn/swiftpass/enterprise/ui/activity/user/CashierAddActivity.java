/*
 * 文 件 名:  EmpManagerActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2015-3-12
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.user;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.tencent.stat.StatService;
import com.ziyeyouhu.library.KeyboardTouchListener;
import com.ziyeyouhu.library.KeyboardUtil;

import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.CashierManager;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.WelcomeActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.ui.widget.dialog.CashierNoticeDialog;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.ECDHUtils;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.VerifyUtil;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2015-3-12]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class CashierAddActivity extends TemplateActivity implements View.OnClickListener {

    private static final String TAG = CashierAddActivity.class.getSimpleName();
    private EditText userTel, cashierName, openAdress;

    private ImageView iv_clearTel, iv_cashierName, iv_openUser,  iv_open_promt;
    //private ImageView iv_tel_promt, iv_cashier_promt;

    private RelativeLayout openManager, refundManger, streamManger, totalLay, lay_marketing,rl_pre_auth;

    private ImageView iv_notice;

    private CheckBox radioButton1, radioButton2, radioButton3, totalBox, radioButton4;

    private TextView refundSwitch, userSwitch, streamSwitch, totalSwitch, tv_marketing;

    private Button addCashier, cashierDelete;

    private UserModel user;

    private DialogInfo dialogInfo;

    private TextView tx_openAdress, tx_cashierName, tx_userTel,tx_cashierNum;

    private ImageView iv_user, iv_refund, iv_total, iv_stream,iv_change_Pre_auth;

    private String isStrem = "0", isTotal = "0", isReund = "0",isUnfreezen = "0";

    private LinearLayout ly_cash_title, ly_ed_cash_title, ly_user;
    private TextView tv_tel, tv_name, tv_add_user_title;

    private LinearLayout ly_name;

    private EditText tx_user_login, et_pwd_one, et_pwd_two;

    private ImageView iv_user_login, iv_pwd_one, iv_pwd_two;

    private TextView tv_email;

    private LinearLayout ly_pwd;

    private CashierNoticeDialog NoticeDialog ,AddSuccessDialog;

    private boolean isUser = true;

    void setViewEable(boolean res) {
        openManager.setEnabled(res);
        refundManger.setEnabled(res);
        streamManger.setEnabled(res);
        rl_pre_auth.setEnabled(res);
        totalLay.setEnabled(res);

    }

    void setImageBg(boolean res, ImageView iView) {
        if (res) {
            iView.setImageResource(R.drawable.button_configure_switch_default);
        } else {
            iView.setImageResource(R.drawable.button_configure_switch_close);
        }
    }

//    @Override
//    public boolean equals(Object obj) {
//        return super.equals(obj);
//    }

    Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == HandlerManager.CASH_NAME) {
                String name = (String) msg.obj;
                tv_name.setText(name);
            }
        }

        ;
    };

    private LinearLayout rootView;
    private KeyboardUtil keyboardUtil;
    private ScrollView scrollView;

    //安全键盘
    private void initMoveKeyBoard() {
        rootView = (LinearLayout) findViewById(R.id.rootview);
        scrollView = (ScrollView) findViewById(R.id.scrollview);
        keyboardUtil = new KeyboardUtil(this, rootView, scrollView);

        keyboardUtil.setOtherEdittext(cashierName);
        keyboardUtil.setOtherEdittext(tx_user_login);
        keyboardUtil.setOtherEdittext(userTel);

        // monitor the KeyBarod state
        keyboardUtil.setKeyBoardStateChangeListener(new CashierAddActivity.KeyBoardStateListener());
        // monitor the finish or next Key
        keyboardUtil.setInputOverListener(new CashierAddActivity.inputOverListener());
        et_pwd_one.setOnTouchListener(new KeyboardTouchListener(keyboardUtil, KeyboardUtil.INPUTTYPE_ABC, -1));
        et_pwd_two.setOnTouchListener(new KeyboardTouchListener(keyboardUtil, KeyboardUtil.INPUTTYPE_ABC, -1));
    }

    class KeyBoardStateListener implements KeyboardUtil.KeyBoardStateChangeListener {

        @Override
        public void KeyBoardStateChange(int state, EditText editText) {

        }
    }

    class inputOverListener implements KeyboardUtil.InputFinishListener {

        @Override
        public void inputHasOver(int onclickType, EditText editText) {

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (keyboardUtil.isShow) {
                keyboardUtil.hideSystemKeyBoard();
                keyboardUtil.hideAllKeyBoard();
                keyboardUtil.hideKeyboardLayout();

                finish();
            } else {
                return super.onKeyDown(keyCode, event);
            }

            return false;
        } else{
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.cashier_add_view);

        initView();
        addCashier.getBackground().setAlpha(102);
        HandlerManager.registerHandler(HandlerManager.CASH_NAME, handler);
        Bundle bundle = this.getIntent().getBundleExtra("bundle");
        if (bundle != null) {
             cashierDelete.setVisibility(View.VISIBLE);
            iv_open_promt.setVisibility(View.INVISIBLE);
            //            iv_tel_promt.setVisibility(View.GONE);
            user = (UserModel) bundle.get("userModel");
        }
        if (user != null) {
            titleBar.setRightButLayVisibleForTotal(false, getString(R.string.tx_user));
            tv_tel.setText(user.getUsername());
            tv_name.setText(user.getRealname());
            titleBar.setTitle(R.string.tv_cash_detail);
            addCashier.setVisibility(View.GONE);
            addCashier.setText(R.string.tx_save_or_update);
            ly_cash_title.setVisibility(View.VISIBLE);
            ly_ed_cash_title.setVisibility(View.GONE);
//            tv_add_user_title.setVisibility(View.GONE);
            //            iv_cashier_promt.setVisibility(View.VISIBLE);
            initValue(user);
            ly_user.setVisibility(View.VISIBLE);
            openManager.setVisibility(View.VISIBLE);
            setViewEable(true);
            ly_name.setClickable(true);
            ly_pwd.setVisibility(View.GONE);

        } else {
            titleBar.setRightButLayVisibleForTotal(false, getString(R.string.tv_cash_update));
            addCashier.setVisibility(View.VISIBLE);
            titleBar.setTitle(R.string.tx_user_add);
            ly_cash_title.setVisibility(View.GONE);
            ly_ed_cash_title.setVisibility(View.VISIBLE);
        }
        setLister();

        initMoveKeyBoard();

    }

    @Override
    protected void onPause() {
        super.onPause();
        et_pwd_one.setText("");
        et_pwd_two.setText("");
    }

    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b, enable, res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_press_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        HideKeyboard();//隐藏键盘
    }

    //隐藏键盘
    private void HideKeyboard() {
        new Timer().schedule(new TimerTask()
        {
            @Override
            public void run() {
                InputMethodManager manager = ((InputMethodManager)getSystemService(INPUT_METHOD_SERVICE));
                if(CashierAddActivity.this.getCurrentFocus() != null){
                    manager.hideSoftInputFromWindow(CashierAddActivity.this.getCurrentFocus().getWindowToken(), 0);
                }
            }
        }, 10);
    }



//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
//            if (keyboardUtil.isShow) {
//                keyboardUtil.hideSystemKeyBoard();
//                keyboardUtil.hideAllKeyBoard();
//                keyboardUtil.hideKeyboardLayout();
//            } else {
//                return super.onKeyDown(keyCode, event);
//            }
//
//            return false;
//        } else
//            return super.onKeyDown(keyCode, event);
//    }


    /**
     * {@inheritDoc}
     */

  /*  @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            if (keyboardUtil.isShow) {
//                keyboardUtil.hideSystemKeyBoard();
//                keyboardUtil.hideAllKeyBoard();
//                keyboardUtil.hideKeyboardLayout();
//            }
            //showPage(CashierManager.class);
            CashierAddActivity.this.finish();
        }

        return super.onKeyDown(keyCode, event);

    }
*/
    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initValue(UserModel user) {
        tx_openAdress.setVisibility(View.VISIBLE);
        tx_cashierName.setVisibility(View.VISIBLE);
        tx_userTel.setVisibility(View.VISIBLE);
        userTel.setText(user.getUsername());
        userTel.setEnabled(false);
        tx_cashierNum.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(String.valueOf(user.getId()))){
            tx_cashierNum.setText(String.valueOf(user.getId()));
        }
        tv_email.setText(user.getEmail());
        cashierName.setText(user.getRealname());
        cashierName.setMaxEms(25);
        openAdress.setText(user.getDeptname());

        if (null != user.getIsActivityAuth() && user.getIsActivityAuth().equals("1")) {
            tv_marketing.setText(R.string.open);
            tv_marketing.setTextColor(getResources().getColor(R.color.cashier_add));
            radioButton4.setChecked(true);

        } else {
            tv_marketing.setText(R.string.no_open);
            tv_marketing.setTextColor(getResources().getColor(R.color.user_edit_color));
            radioButton4.setChecked(false);
        }

        if (null != user.getIsTotalAuth() && user.getIsTotalAuth().equals("1")) {
            totalSwitch.setText(R.string.open);
            totalSwitch.setTextColor(getResources().getColor(R.color.cashier_add));
            totalBox.setChecked(true);
            isTotal = "1";
            setImageBg(true, iv_total);
            //            isStrem = "1", isTotal = "1", isReund = "1", isUser = "1"; iv_user, iv_refund, iv_total, iv_stream;
        } else {
            totalSwitch.setText(R.string.no_open);
            totalSwitch.setTextColor(getResources().getColor(R.color.user_edit_color));
            totalBox.setChecked(false);
            isTotal = "0";
            setImageBg(false, iv_total);
        }

        if (user.getIsOrderAuth() != null && user.getIsOrderAuth().equals("1")) {
            streamSwitch.setText(R.string.open);
            streamSwitch.setTextColor(getResources().getColor(R.color.cashier_add));
            radioButton2.setChecked(true);
            isReund = "1";
            setImageBg(true, iv_stream);
        } else {
            streamSwitch.setText(R.string.no_open);
            streamSwitch.setTextColor(getResources().getColor(R.color.user_edit_color));
            radioButton2.setChecked(false);
            isReund = "0";
            setImageBg(false, iv_stream);
        }

        if (user.getIsRefundAuth() != null && user.getIsRefundAuth().equals("1")) {
            refundSwitch.setTextColor(getResources().getColor(R.color.cashier_add));
            radioButton1.setChecked(true);
            refundSwitch.setText(R.string.open);
            isReund = "1";
            setImageBg(true, iv_refund);
        } else {
            refundSwitch.setTextColor(getResources().getColor(R.color.user_edit_color));
            radioButton1.setChecked(false);
            refundSwitch.setText(R.string.no_open);
            isReund = "0";
            setImageBg(false, iv_refund);
        }

        if (user.getIsUnfreezeAuth() != null && user.getIsUnfreezeAuth().equals("1")) {
            isUnfreezen = "1";
            setImageBg(true, iv_change_Pre_auth);
        } else {
            isUnfreezen = "0";
            setImageBg(false, iv_change_Pre_auth);
        }

        if (user.getEnabled()) {
            userSwitch.setTextColor(getResources().getColor(R.color.cashier_add));
            radioButton3.setChecked(true);
            userSwitch.setText(R.string.open);
            isUser = true;
            setImageBg(false, iv_user);
        } else {
            userSwitch.setTextColor(getResources().getColor(R.color.user_edit_color));
            userSwitch.setText(R.string.no_open);
            radioButton3.setChecked(false);
            isUser = false;
            setImageBg(true, iv_user);
        }
    }

    private boolean checkExitisNull() {
        if (isAbsoluteNullStr(cashierName.getText().toString())) {
            toastDialog(CashierAddActivity.this, R.string.tx_surname_notnull, null);
//            cashierName.setFocusable(true);
            return false;
        }
        if (isAbsoluteNullStr(userTel.getText().toString())) {
            toastDialog(CashierAddActivity.this, R.string.tx_email, null);
//            userTel.setFocusable(true);
            return false;
        }
        //tx_user_login, et_pwd_one, et_pwd_two
        if (isAbsoluteNullStr(tx_user_login.getText().toString())) {
            toastDialog(CashierAddActivity.this, R.string.tx_user_login_promt, null);
//            tx_user_login.setFocusable(true);
            return false;
        }

        if (isAbsoluteNullStr(et_pwd_one.getText().toString())) {
            toastDialog(CashierAddActivity.this, R.string.ed_input_pass_hint, null);
//            et_pwd_one.setFocusable(true);
            return false;
        }

        if (isAbsoluteNullStr(et_pwd_two.getText().toString())) {
            toastDialog(CashierAddActivity.this, R.string.tv_find_pass_hint_info4, null);
//            et_pwd_two.setFocusable(true);
            return false;
        }
        if (et_pwd_one.getText().toString().length() < 8) {
            toastDialog(CashierAddActivity.this, R.string.show_pass_prompt, null);
//            et_pwd_one.setFocusable(true);
            return false;
        }

        if (!VerifyUtil.verifyValid(userTel.getText().toString(), GlobalConstant.EMAIL)) {
//            userTel.setFocusable(true);
            showToastInfo(R.string.tx_email_detai);
            return false;
        }

        if (!et_pwd_one.getText().toString().equals(et_pwd_two.getText().toString())) {
            toastDialog(CashierAddActivity.this, R.string.show_pass_diff, null);
//            et_pwd_two.setFocusable(true);
            return false;
        }

        //        if (isAbsoluteNullStr(openAdress.getText().toString()))
        //        {
        //            showToastInfo(R.string.tx_store_notnull);
        //            openAdress.setFocusable(true);
        //            return false;
        //        }

        return true;
    }

    private void submit(final UserModel usemModel, final boolean isTag) {
        UserManager.cashierAdd(usemModel, new UINotifyListener<Boolean>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                showLoading(false, getString(R.string.show_save_loading));
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                dismissLoading();
                if (checkSession()) {
                    return;
                }
                if (object != null) {

                  CashierAddActivity.this.runOnUiThread(new Runnable() {
                      @Override
                      public void run() {
                          if(object.toString().startsWith("405")){//Require to renegotiate ECDH key
                              ECDHKeyExchange(usemModel,isTag);
                          }else{
                              toastDialog(CashierAddActivity.this, object.toString(), null);
                          }
                      }
                  });

                    //                    CashierAddActivity.this.runOnUiThread(new Runnable()
                    //                    {
                    //                        @Override
                    //                        public void run()
                    //                        {
                    //                            
                    //                            DialogInfo dialogInfo =
                    //                                new DialogInfo(CashierAddActivity.this, getStringById(R.string.public_cozy_prompt),
                    //                                    object.toString(), getStringById(R.string.btnOk), DialogInfo.REGISTFLAG, null, null);
                    //                            DialogHelper.resize(CashierAddActivity.this, dialogInfo);
                    //                            dialogInfo.show();
                    //                        }
                    //                    });
                }
            }

            @Override
            public void onSucceed(Boolean result) {
                super.onSucceed(result);
                dismissLoading();

                if (result) {
                    if (isTag)
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(AddSuccessDialog == null)
                                {
                                    AddSuccessDialog = new CashierNoticeDialog(CashierAddActivity.this);
                                    AddSuccessDialog.setContentText(R.string.show_add_succ);
                                    AddSuccessDialog.setCanceledOnTouchOutside(false);
                                    AddSuccessDialog.setCurrentListener(new CashierNoticeDialog.ButtonConfirmCallBack() {
                                        @Override
                                        public void onClickConfirmCallBack() {
                                            if(AddSuccessDialog != null && AddSuccessDialog.isShowing()){
                                                AddSuccessDialog.dismiss();
                                                finish();
                                            }
                                        }
                                    });
                                }
                                if(!AddSuccessDialog.isShowing())
                                {
                                    AddSuccessDialog.show();
                                }
                            }
                        });

                    }

                }

            }
        });
    }

    private void ECDHKeyExchange(final UserModel usemModel, final boolean isTag){
        try {
            ECDHUtils.getInstance().getAppPubKey();
        }catch (Exception e){
            Log.e(TAG,Log.getStackTraceString(e));
        }
        final String publicKey =  SharedPreUtile.readProduct("pubKey").toString();
        final String privateKey =  SharedPreUtile.readProduct("priKey").toString();

        LocalAccountManager.getInstance().ECDHKeyExchange(publicKey,new UINotifyListener<ECDHInfo>(){
            @Override
            public void onError(Object object) {
                super.onError(object);
                dismissLoading();
                /*if(object!= null){
                    toastDialog(CashierAddActivity.this, object.toString(), null);
                }*/
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(ECDHInfo result) {
                super.onSucceed(result);
                if(result != null){
                    try {
                        String secretKey = ECDHUtils.getInstance().ecdhGetShareKey(MainApplication.serPubKey,privateKey);
                        SharedPreUtile.saveObject(secretKey, "secretKey");
                    }catch (Exception e){
                        Log.e(TAG,Log.getStackTraceString(e));
                    }
                    submit(usemModel,isTag);
                }
            }
        });
    }

    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void setLister() {
        iv_user_login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                tx_user_login.setText("");
            }
        });

        iv_pwd_one.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                et_pwd_one.setText("");
            }
        });

        iv_pwd_two.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                et_pwd_two.setText("");
            }
        });

        ly_name.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    StatService.trackCustomEvent(CashierAddActivity.this, "kMTASPayMeCashierUpdate", "修改收银员姓名按钮");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
                user.setRealname(tv_name.getText().toString());
                CashierNameModifyActivity.startActivity(CashierAddActivity.this, user);
            }
        });

        addCashier.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!checkExitisNull())
                    return;
                final UserModel usemModel = new UserModel();
                usemModel.setPhone(userTel.getText().toString());
                usemModel.setRealname(cashierName.getText().toString());
                usemModel.setDeptname(openAdress.getText().toString());
                //                isStrem =1,isTotal=1,isReund=1,isUser=1;
                usemModel.setIsOrderAuth(isStrem);
                usemModel.setIsRefundAuth(isReund);
                usemModel.setIsUnfreezeAuth(isUnfreezen);
                usemModel.setEnabled(isUser);
                usemModel.setIsTotalAuth(isTotal);
                usemModel.setIsActivityAuth(radioButton4.isChecked() == true ? "1" : "0");
                usemModel.setUserName(tx_user_login.getText().toString());
                usemModel.setPassword(et_pwd_one.getText().toString());

                if (user != null) {
                    usemModel.setId(user.getId());

                    dialogInfo =
                            new DialogInfo(CashierAddActivity.this, getString(R.string.public_cozy_prompt),
                                    getString(R.string.dialog_modify), getString(R.string.btnOk),
                                    getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

                                @Override
                                public void handleOkBtn() {
                                    submit(usemModel, true);
                                }

                                @Override
                                public void handleCancleBtn() {
                                    dialogInfo.cancel();
                                }
                            }, null);

                    DialogHelper.resize(CashierAddActivity.this, dialogInfo);
                    dialogInfo.show();
                } else {
                    submit(usemModel, true);
                }

            }
        });

        radioButton1.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    refundSwitch.setText(R.string.open);
                    refundSwitch.setTextColor(getResources().getColor(R.color.cashier_add));
                } else {
                    refundSwitch.setTextColor(getResources().getColor(R.color.user_edit_color));
                    refundSwitch.setText(R.string.no_open);
                }
            }

        });

        radioButton2.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    streamSwitch.setText(R.string.open);
                    streamSwitch.setTextColor(getResources().getColor(R.color.cashier_add));
                } else {
                    streamSwitch.setTextColor(getResources().getColor(R.color.user_edit_color));
                    streamSwitch.setText(R.string.no_open);
                }
            }

        });

        radioButton3.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    userSwitch.setText(R.string.open);
                    userSwitch.setTextColor(getResources().getColor(R.color.cashier_add));
                } else {
                    userSwitch.setTextColor(getResources().getColor(R.color.user_edit_color));
                    userSwitch.setText(R.string.no_open);
                }
            }

        });//totalLay

        totalBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    totalSwitch.setText(R.string.open);
                    totalSwitch.setTextColor(getResources().getColor(R.color.cashier_add));
                } else {
                    totalSwitch.setTextColor(getResources().getColor(R.color.user_edit_color));
                    totalSwitch.setText(R.string.no_open);
                }
            }

        });

        radioButton4.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    tv_marketing.setText(R.string.open);
                    tv_marketing.setTextColor(getResources().getColor(R.color.cashier_add));
                } else {
                    tv_marketing.setTextColor(getResources().getColor(R.color.user_edit_color));
                    tv_marketing.setText(R.string.no_open);
                }
            }

        });

        lay_marketing.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (radioButton4.isChecked()) {
                    tv_marketing.setTextColor(getResources().getColor(R.color.user_edit_color));
                    tv_marketing.setText(R.string.no_open);
                    radioButton4.setChecked(false);
                } else {
                    tv_marketing.setTextColor(getResources().getColor(R.color.cashier_add));
                    tv_marketing.setText(R.string.open);
                    radioButton4.setChecked(true);
                }

            }
        });

        totalLay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //                isStrem =1,isTotal=1,isReund=1,isUser=1;

                if (user != null) {
                    if (user.getIsTotalAuth().equals("0")) {
                        isTotal = "1";
                        iv_total.setImageResource(R.drawable.button_configure_switch_default);
                    } else {
                        isTotal = "0";
                        iv_total.setImageResource(R.drawable.button_configure_switch_close);
                    }
                } else {
                    if (isTotal.equals("1")) {
                        isTotal = "0";
                        iv_total.setImageResource(R.drawable.button_configure_switch_close);
                    } else {
                        isTotal = "1";
                        iv_total.setImageResource(R.drawable.button_configure_switch_default);
                    }
                }

                if (user != null) {
                    user.setIsTotalAuth(isTotal);
                    submit(user, false);
                }

                //                if (totalBox.isChecked())
                //                {
                //                    totalSwitch.setTextColor(getResources().getColor(R.color.user_edit_color));
                //                    totalSwitch.setText(R.string.no_open);
                //                    totalBox.setChecked(false);
                //                }
                //                else
                //                {
                //                    totalSwitch.setTextColor(getResources().getColor(R.color.cashier_add));
                //                    totalSwitch.setText(R.string.open);
                //                    totalBox.setChecked(true);
                //                }
            }
        });

        openManager.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //                if (radioButton3.isChecked())
                //                {
                //                    userSwitch.setTextColor(getResources().getColor(R.color.user_edit_color));
                //                    userSwitch.setText(R.string.no_open);
                //                    radioButton3.setChecked(false);
                //                }
                //                else
                //                {
                //                    userSwitch.setTextColor(getResources().getColor(R.color.cashier_add));
                //                    userSwitch.setText(R.string.open);
                //                    radioButton3.setChecked(true);
                //                }

                //              isStrem =1,isTotal=1,isReund=1,isUser=1;
                if (isUser) {
                    isUser = false;
                    iv_user.setImageResource(R.drawable.button_configure_switch_default);
                } else {
                    iv_user.setImageResource(R.drawable.button_configure_switch_close);
                    isUser = true;
                }

                if (user != null) {
                    user.setEnabled(isUser);
                    submit(user, false);
                }

            }
        });

        refundManger.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //                if (radioButton1.isChecked())
                //                {
                //                    refundSwitch.setTextColor(getResources().getColor(R.color.user_edit_color));
                //                    refundSwitch.setText(R.string.no_open);
                //                    radioButton1.setChecked(false);
                //                }
                //                else
                //                {
                //                    refundSwitch.setTextColor(getResources().getColor(R.color.cashier_add));
                //                    refundSwitch.setText(R.string.open);
                //                    radioButton1.setChecked(true);
                //                }

                //              isStrem =1,isTotal=1,isReund=1,isUser=1;

                if (user != null) {
                    if (user.getIsRefundAuth().equals("0")) {
                        isReund = "1";
                        iv_refund.setImageResource(R.drawable.button_configure_switch_default);
                    } else {
                        isReund = "0";
                        iv_refund.setImageResource(R.drawable.button_configure_switch_close);
                    }
                } else {

                    if (isReund.equals("1")) {
                        isReund = "0";
                        iv_refund.setImageResource(R.drawable.button_configure_switch_close);
                    } else {
                        isReund = "1";
                        iv_refund.setImageResource(R.drawable.button_configure_switch_default);
                    }
                }

                if (user != null) {
                    user.setIsRefundAuth(isReund);
                    submit(user, false);
                }
            }
        });

        streamManger.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //                if (radioButton2.isChecked())
                //                {
                //                    streamSwitch.setTextColor(getResources().getColor(R.color.user_edit_color));
                //                    streamSwitch.setText(R.string.no_open);
                //                    radioButton2.setChecked(false);
                //                }
                //                else
                //                {
                //                    streamSwitch.setTextColor(getResources().getColor(R.color.cashier_add));
                //                    streamSwitch.setText(R.string.open);
                //                    radioButton2.setChecked(true);
                //                }

                //              isStrem =1,isTotal=1,isReund=1,isUser=1;
                if (user != null) {
                    if (user.getIsOrderAuth().equals("0")) {
                        isStrem = "1";
                        iv_stream.setImageResource(R.drawable.button_configure_switch_default);
                    } else {
                        isStrem = "0";
                        iv_stream.setImageResource(R.drawable.button_configure_switch_close);
                    }
                } else {

                    if (isStrem.equals("1")) {
                        isStrem = "0";
                        iv_stream.setImageResource(R.drawable.button_configure_switch_close);
                    } else {
                        isStrem = "1";
                        iv_stream.setImageResource(R.drawable.button_configure_switch_default);
                    }
                }

                if (user != null) {
                    user.setIsOrderAuth(isStrem);
                    submit(user, false);
                }
            }
        });

        rl_pre_auth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user != null) {
                    if(!TextUtils.isEmpty(user.getIsUnfreezeAuth())){
                        if (user.getIsUnfreezeAuth().equals("0")) {
                            isUnfreezen = "1";
                            iv_change_Pre_auth.setImageResource(R.drawable.button_configure_switch_default);
                        }else {
                            isUnfreezen = "0";
                            iv_change_Pre_auth.setImageResource(R.drawable.button_configure_switch_close);
                        }
                    }else{
                        if (isUnfreezen.equals("1")) {
                            isUnfreezen = "0";
                            iv_change_Pre_auth.setImageResource(R.drawable.button_configure_switch_close);
                        } else {
                            isUnfreezen = "1";
                            iv_change_Pre_auth.setImageResource(R.drawable.button_configure_switch_default);
                        }
                    }
                } else {
                    if (isUnfreezen.equals("1")) {
                        isUnfreezen = "0";
                        iv_change_Pre_auth.setImageResource(R.drawable.button_configure_switch_close);
                    } else {
                        isUnfreezen = "1";
                        iv_change_Pre_auth.setImageResource(R.drawable.button_configure_switch_default);
                    }
                }

                if (user != null) {
                    user.setIsUnfreezeAuth(isUnfreezen);
                    submit(user, false);
                }
            }
        });


        iv_notice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(NoticeDialog == null)
                {
                    NoticeDialog = new CashierNoticeDialog(CashierAddActivity.this);
                    String str = "";
                    //如果开通预授权，则文案变成预授权的文案，否则是默认的文案
                    if(MainApplication.isPre_authOpen == 1){
                        str = getStringById(R.string.cashier_authority_pre_auth);
                    }else{
                        str = getStringById(R.string.cashier_authority);
                    }
                    NoticeDialog.setContentText(str);
                    NoticeDialog.setConfirmText(R.string.bt_know);
                    NoticeDialog.setCanceledOnTouchOutside(false);
                    NoticeDialog.setCurrentListener(new CashierNoticeDialog.ButtonConfirmCallBack() {
                        @Override
                        public void onClickConfirmCallBack() {
                            if(NoticeDialog != null && NoticeDialog.isShowing()){
                                NoticeDialog.dismiss();
                            }
                        }
                    });
                }
                if(!NoticeDialog.isShowing())
                {
                    NoticeDialog.show();
                }
            }
        });

        cashierDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                try {
                    StatService.trackCustomEvent(CashierAddActivity.this, "kMTASPayMeCashierDelete", "收银员管理删除按钮");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
                dialogInfo = new DialogInfo(CashierAddActivity.this, getString(R.string.public_cozy_prompt),
                                getString(R.string.dialog_delete), getString(R.string.btnOk), getString(R.string.btnCancel),
                                DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

                            @Override
                            public void handleOkBtn() {
                                UserManager.cashierDelete(user.getId(), new UINotifyListener<Boolean>() {
                                    @Override
                                    public void onPreExecute() {
                                        super.onPreExecute();

                                        showLoading(false, getString(R.string.show_delete_loading));
                                    }

                                    @Override
                                    public void onError(final Object object) {
                                        super.onError(object);
                                        dismissLoading();
                                        if (object != null) {
                                            showToastInfo(object.toString());
                                        }
                                    }

                                    @Override
                                    public void onSucceed(Boolean result) {
                                        super.onSucceed(result);
                                        dismissLoading();

                                        if (result) {
                                            showToastInfo(R.string.show_delete_succ);
                                            showPage(CashierManager.class);
                                            finish();
                                        }

                                    }
                                });
                            }

                            @Override
                            public void handleCancleBtn() {
                                dialogInfo.cancel();
                            }
                        }, null);

                DialogHelper.resize(CashierAddActivity.this, dialogInfo);
                dialogInfo.show();

                //                DialogHelper.showDialog(getString(R.string.title_dialog),
                //                    "确定是否删除？",
                //                    R.string.btnCancel,
                //                    R.string.btnOk,
                //                    CashierAddActivity.this,
                //                    new DialogInterface.OnClickListener()
                //                    {
                //                        @Override
                //                        public void onClick(DialogInterface dialogInterface, int i)
                //                        {
                //                            UserManager.cashierDelete(user.getId(), new UINotifyListener<Boolean>()
                //                            {
                //                                @Override
                //                                public void onPreExecute()
                //                                {
                //                                    super.onPreExecute();
                //                                    
                //                                    showLoading(false, "删除中...");
                //                                }
                //                                
                //                                @Override
                //                                public void onError(final Object object)
                //                                {
                //                                    super.onError(object);
                //                                    dismissLoading();
                //                                    if (object != null)
                //                                    {
                //                                        showToastInfo(object.toString());
                //                                    }
                //                                }
                //                                
                //                                @Override
                //                                public void onSucceed(Boolean result)
                //                                {
                //                                    super.onSucceed(result);
                //                                    dismissLoading();
                //                                    
                //                                    if (result)
                //                                    {
                //                                        showToastInfo("删除成功");
                //                                        showPage(CashierManager.class);
                //                                        finish();
                //                                    }
                //                                    
                //                                }
                //                            });
                //                        }
                //                    }).show();
            }
        });

    }

    /**
     * {@inheritDoc}
     */

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_clearTel:
                userTel.setText("");
                break;
            case R.id.iv_cashierName:
                cashierName.setText("");
                break;
            case R.id.iv_openUser:
                openAdress.setText("");
                break;

            default:
                break;
        }
    }

    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initView() {
        //iv_user,iv_refund,iv_total,iv_stream; tv_tel,tv_name
        ly_pwd = getViewById(R.id.ly_pwd);
        tv_email = getViewById(R.id.tv_email);
        tx_user_login = getViewById(R.id.et_user_login);
        tx_user_login.clearFocus();
        et_pwd_one = getViewById(R.id.et_pwd_one);
        et_pwd_two = getViewById(R.id.et_pwd_two);
        iv_user_login = getViewById(R.id.iv_user_login);
        iv_pwd_one = getViewById(R.id.iv_pwd_one);
        iv_pwd_two = getViewById(R.id.iv_pwd_two);

        et_pwd_two.clearFocus();
        et_pwd_one.clearFocus();

        ly_user = getViewById(R.id.ly_user);
        tv_add_user_title = getViewById(R.id.tv_add_user_title);
        ly_name = getViewById(R.id.ly_name);
        tv_tel = getViewById(R.id.tv_tel);
        tv_name = getViewById(R.id.tv_name);
        ly_cash_title = getViewById(R.id.ly_cash_title);
        ly_ed_cash_title = getViewById(R.id.ly_ed_cash_title);
        iv_user = getViewById(R.id.iv_user);
        iv_refund = getViewById(R.id.iv_refund);
        iv_total = getViewById(R.id.iv_total);
        iv_stream = getViewById(R.id.iv_stream);
        iv_change_Pre_auth = getViewById(R.id.iv_change_Pre_auth);

        lay_marketing = getViewById(R.id.lay_marketing);
        radioButton4 = getViewById(R.id.radioButton4);
        tv_marketing = getViewById(R.id.tv_marketing);
        tx_openAdress = getViewById(R.id.tx_openAdress);
        tx_cashierName = getViewById(R.id.tx_cashierName);
        tx_userTel = getViewById(R.id.tx_userTel);
        //增加收银员
        tx_cashierNum = getViewById(R.id.tv_cashier_num);

        totalLay = getViewById(R.id.totalLay);
        totalBox = getViewById(R.id.totalBox);
        totalSwitch = getViewById(R.id.totalSwitch);

        cashierDelete = getViewById(R.id.cashierDelete);
        addCashier = getViewById(R.id.addCashier);

        refundSwitch = getViewById(R.id.refundSwitch);
        userSwitch = getViewById(R.id.userSwitch);
        streamSwitch = getViewById(R.id.streamSwitch);

        streamManger = getViewById(R.id.streamManger);
        openManager = getViewById(R.id.openManager);
        refundManger = getViewById(R.id.refundManger);
        rl_pre_auth = getViewById(R.id.rl_pre_auth);

        //只有开通预授权通道，才会显示预授权开关
        if(MainApplication.isPre_authOpen == 1){
            rl_pre_auth.setVisibility(View.VISIBLE);
        }else{
            rl_pre_auth.setVisibility(View.GONE);
        }

        iv_notice = getViewById(R.id.iv_notice);

        userTel = getViewById(R.id.userTel);
        cashierName = getViewById(R.id.cashierName);
        openAdress = getViewById(R.id.openAdress);

        userTel.clearFocus();
        cashierName.clearFocus();
        openAdress.clearFocus();

        //        userTel.setOnFocusChangeListener(listener);
        //        cashierName.setOnFocusChangeListener(listener);
        //        openAdress.setOnFocusChangeListener(listener);

        //        iv_tel_promt = getViewById(R.id.iv_tel_promt);
        //iv_cashier_promt = getViewById(R.id.iv_cashier_promt);
        iv_open_promt = getViewById(R.id.iv_open_promt);

        iv_clearTel = getViewById(R.id.iv_clearTel);
        iv_cashierName = getViewById(R.id.iv_cashierName);
        iv_openUser = getViewById(R.id.iv_openUser);
        iv_clearTel.setOnClickListener(this);
        iv_cashierName.setOnClickListener(this);
        iv_openUser.setOnClickListener(this);

        radioButton1 = getViewById(R.id.radioButton1);
        radioButton2 = getViewById(R.id.radioButton2);
        radioButton3 = getViewById(R.id.radioButton3);

        EditTextWatcher editTextWatcher = new EditTextWatcher();

        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {

                if (userTel.isFocused()) {
                    if (userTel.getText().length() >= 1) {
                        iv_clearTel.setVisibility(View.VISIBLE);
                        //                        iv_tel_promt.setVisibility(View.GONE);
                    } else {
                        iv_clearTel.setVisibility(View.GONE);
                        //                        iv_tel_promt.setVisibility(View.VISIBLE);
                    }
                }
                if (cashierName.isFocused()) {
                    if (cashierName.getText().length() >= 1) {
                        iv_cashierName.setVisibility(View.VISIBLE);
                        //                        iv_cashier_promt.setVisibility(View.GONE);
                    } else {
                        iv_cashierName.setVisibility(View.GONE);
                        //                        iv_cashier_promt.setVisibility(View.VISIBLE);
                    }
                }
                if (openAdress.isFocused()) {
                    if (openAdress.getText().length() >= 1) {
                        iv_openUser.setVisibility(View.VISIBLE);
                        //                        iv_open_promt.setVisibility(View.GONE);
                    } else {
                        iv_openUser.setVisibility(View.GONE);
                        //                        iv_open_promt.setVisibility(View.VISIBLE);
                    }
                }
                if (tx_user_login.isFocusable()) {
                    if (tx_user_login.getText().length() > 0) {
                        iv_user_login.setVisibility(View.VISIBLE);
                    } else {
                        iv_user_login.setVisibility(View.GONE);
                    }

                }
                if (et_pwd_one.isFocusable()) {
                    if (et_pwd_one.getText().length() > 0) {
                        iv_pwd_one.setVisibility(View.VISIBLE);
                    } else {
                        iv_pwd_one.setVisibility(View.GONE);
                    }

                }
                if (et_pwd_two.isFocusable()) {
                    if (et_pwd_two.getText().length() > 0) {
                        iv_pwd_two.setVisibility(View.VISIBLE);
                    } else {
                        iv_pwd_two.setVisibility(View.GONE);
                    }

                }

                //                private TextView tx_user_login, et_pwd_one, et_pwd_two;

                if (!StringUtil.isEmptyOrNull(userTel.getText().toString())
                        && !StringUtil.isEmptyOrNull(cashierName.getText().toString())
                        && !StringUtil.isEmptyOrNull(tx_user_login.getText().toString())
                        && !StringUtil.isEmptyOrNull(et_pwd_one.getText().toString())
                        && !StringUtil.isEmptyOrNull(et_pwd_two.getText().toString())) {
                    setButtonBg(addCashier, true, 0);
                } else {
                    setButtonBg(addCashier, false, 0);
                }
            }

        });
//        showSoftInputFromWindow(CashierAddActivity.this, cashierName);

        /**
         * 可配置设置
         */
        DynModel dynModel = (DynModel) SharedPreUtile.readProduct("dynModel" + ApiConstant.bankCode);
        if (null != dynModel) {
            try {
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonColor())) {
                    addCashier.setBackgroundColor(Color.parseColor(dynModel.getButtonColor()));
                }

                if (!StringUtil.isEmptyOrNull(dynModel.getButtonFontColor())) {
                    addCashier.setTextColor(Color.parseColor(dynModel.getButtonFontColor()));
                }
            } catch (Exception e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }
        }

        userTel.addTextChangedListener(editTextWatcher);
        cashierName.addTextChangedListener(editTextWatcher);
        tx_user_login.addTextChangedListener(editTextWatcher);
        et_pwd_two.addTextChangedListener(editTextWatcher);
        et_pwd_one.addTextChangedListener(editTextWatcher);
//        initMoveKeyBoard();
        //        openAdress.addTextChangedListener(editTextWatcher);

    }

//    private final OnFocusChangeListener listener = new OnFocusChangeListener() {
//        @Override
//        public void onFocusChange(View v, boolean hasFocus) {
//            switch (v.getId()) {
//                case R.id.userTel:
//                    if (hasFocus) {
//                        if (userTel.getText().length() > 1) {
//                            iv_clearTel.setVisibility(View.VISIBLE);
//                            iv_tel_promt.setVisibility(View.GONE);
//                        } else {
//                            iv_clearTel.setVisibility(View.GONE);
//                            iv_tel_promt.setVisibility(View.VISIBLE);
//                        }
//                    }
//                    break;
//                case R.id.cashierName:
//                    if (hasFocus) {
//                        if (cashierName.getText().length() > 1) {
//                            iv_cashierName.setVisibility(View.VISIBLE);
//                            //                            iv_cashier_promt.setVisibility(View.GONE);
//                        } else {
//                            iv_cashierName.setVisibility(View.GONE);
//                            //                            iv_cashier_promt.setVisibility(View.VISIBLE);
//                        }
//                    }
//                    break;
//                case R.id.openAdress:
//                    if (hasFocus) {
//                        if (openAdress.getText().length() > 1) {
//                            iv_openUser.setVisibility(View.VISIBLE);
//                            iv_open_promt.setVisibility(View.GONE);
//                        } else {
//                            iv_openUser.setVisibility(View.GONE);
//                            iv_open_promt.setVisibility(View.VISIBLE);
//                        }
//                    }
//                    break;
//            }
//
//        }
//    };

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onRightButLayClick() {
                //                titleBar.setRightButLayVisibleForTotal(true, getString(R.string.tv_cash_update));
                //                addCashier.setVisibility(View.VISIBLE);
                //                addCashier.setText(R.string.save);
                //                setViewEable(true);
                //                ly_cash_title.setVisibility(View.GONE);
                //                ly_ed_cash_title.setVisibility(View.VISIBLE);
                //                if (!StringUtil.isEmptyOrNull(userTel.getText().toString()))
                //                {
                //                    iv_clearTel.setVisibility(View.VISIBLE);
                //                }
                //                if (!StringUtil.isEmptyOrNull(cashierName.getText().toString()))
                //                {
                //                    iv_cashierName.setVisibility(View.VISIBLE);
                //                }

            }

            @Override
            public void onLeftButtonClick() {
                CashierAddActivity.this.finish();
            }
        });
        if (user != null) {
            titleBar.setTitle(R.string.tv_cash_detail);
        } else {
            titleBar.setTitle(R.string.tx_user_add);
        }
    }

}
