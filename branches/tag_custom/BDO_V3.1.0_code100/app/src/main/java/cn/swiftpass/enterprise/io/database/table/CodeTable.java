package cn.swiftpass.enterprise.io.database.table;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-10-31
 * Time: 下午12:10
 * To change this template use File | Settings | File Templates.
 */
public class CodeTable extends TableBase{
    public static final String TABLE_NAME="tb_code";
    public static final String CODEVALUE="value";
    @Override
    public void createTable(SQLiteDatabase db) {
        final String SQL_CREATE_TABLE = "create table "+TABLE_NAME+"("
                + COLUMN_ID + " INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT  ," +
                CODEVALUE + " INT "+")";
        db.execSQL(SQL_CREATE_TABLE);
    }
}
