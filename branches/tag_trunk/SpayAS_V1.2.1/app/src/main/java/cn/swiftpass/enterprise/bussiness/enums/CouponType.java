package cn.swiftpass.enterprise.bussiness.enums;

/**
 * 优惠卷类型
 * User: Alan
 * Date: 14-2-19
 * Time: 上午11:17
 * To change this template use File | Settings | File Templates.
 */
public enum CouponType
{
    COUPON_SALE(2, "优惠券"), //优惠券
    COUPON_GIVE(1, "赠送券");//赠送券
    private final Integer value;
    
    private String displayName = "";
    
    private CouponType(Integer v, String displayName)
    {
        this.value = v;
        this.displayName = displayName;
    }
    
    public String getDisplayName()
    {
        return displayName;
    }
    
    public Integer getValue()
    {
        return value;
    }
}
