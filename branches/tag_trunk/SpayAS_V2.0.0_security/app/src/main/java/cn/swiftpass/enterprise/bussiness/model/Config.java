/**************************************************************
 * @作者: zengsb 
 * @创建时间: 2012-8-30 上午10:55:48 
 * @功能描述: 基本配置类
 * @版权声明:本程序版权归 深圳市时代纬科技有限公司所有 Copy right 2010-2012
 **************************************************************/
package cn.swiftpass.enterprise.bussiness.model;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Config
{

    private static final String TAG = Config.class.getSimpleName();

    public static final String CONFIG_NAME = "config.xml";
    
    @Element
    protected String serverAddr;

    @Element
    protected String serverAddrBack;
    
    @Element
    protected String serverPort;
    
    // 开发相关设置
    @Element
    protected boolean isDebug;
    
    @Element
    protected String fileSaveRoot;
    
    @Element
    protected String appName;
    
    @Element
    protected int clientType;
    
    @Element
    protected boolean isCreateLocaltionQRcode;
    
    @Element
    protected boolean isOverseasPay; //W.l添加境外支付
    
    @Element
    protected String payGateway;
    
    @Element
    protected String body;
    
    @Element
    private Integer bankType;

    @Element
    private String bankName;
    
    @Element
    private String wxCardUrl; // 卡券地址
    
    @Element
    private String appKey; // 继承腾讯云appkey
    
    @Element
    private String Channel;

    @Element
    private String pushMoneyUrl;
    
    @Element
    private String imie;
    
    /**
     * @return 返回 imie
     */
    public String getImie()
    {
        return imie;
    }
    
    /**
     * @param 对imie进行赋值
     */
    public void setImie(String imie)
    {
        this.imie = imie;
    }
    
    /**
     * @return 返回 serverAddrTest
     */
    public String getServerAddrTest()
    {
        return serverAddrTest;
    }
    
    /**
     * @param 对serverAddrTest进行赋值
     */
    public void setServerAddrTest(String serverAddrTest)
    {
        this.serverAddrTest = serverAddrTest;
    }
    
    /**
     * @return 返回 serverAddrPrd
     */
    public String getServerAddrPrd()
    {
        return serverAddrPrd;
    }
    
    /**
     * @param 对serverAddrPrd进行赋值
     */
    public void setServerAddrPrd(String serverAddrPrd)
    {
        this.serverAddrPrd = serverAddrPrd;
    }
    
    /**
     * @return 返回 pushMoneyUrlTest
     */
    public String getPushMoneyUrlTest()
    {
        return pushMoneyUrlTest;
    }
    
    /**
     * @param 对pushMoneyUrlTest进行赋值
     */
    public void setPushMoneyUrlTest(String pushMoneyUrlTest)
    {
        this.pushMoneyUrlTest = pushMoneyUrlTest;
    }
    
    /**
     * @return 返回 pushMoneyUrlPrd
     */
    public String getPushMoneyUrlPrd()
    {
        return pushMoneyUrlPrd;
    }
    
    /**
     * @param 对pushMoneyUrlPrd进行赋值
     */
    public void setPushMoneyUrlPrd(String pushMoneyUrlPrd)
    {
        this.pushMoneyUrlPrd = pushMoneyUrlPrd;
    }

    @Element
    private String serverAddrTest;//测试环境地址
    
    @Element
    private String serverAddrPrd;//预上线环境地址
    
    @Element
    private String pushMoneyUrlTest;
    
    @Element
    private String pushMoneyUrlPrd;
    
    @Element
    private String serverAddrDev;
    
    /**
     * @return 返回 serverAddrDev
     */
    public String getServerAddrDev()
    {
        return serverAddrDev;
    }
    
    /**
     * @param 对serverAddrDev进行赋值
     */
    public void setServerAddrDev(String serverAddrDev)
    {
        this.serverAddrDev = serverAddrDev;
    }
    
    /**
     * @return 返回 pushMoneyUrl
     */
    public String getPushMoneyUrl()
    {
        return pushMoneyUrl;
    }
    
    /**
     * @param 对pushMoneyUrl进行赋值
     */
    public void setPushMoneyUrl(String pushMoneyUrl)
    {
        this.pushMoneyUrl = pushMoneyUrl;
    }
    
    /**
     * @return 返回 channel
     */
    public String getChannel()
    {
        return Channel;
    }
    
    /**
     * @param 对channel进行赋值
     */
    public void setChannel(String channel)
    {
        Channel = channel;
    }
    
    /**
     * @return 返回 appKey
     */
    public String getAppKey()
    {
        return appKey;
    }
    
    /**
     * @param 对appKey进行赋值
     */
    public void setAppKey(String appKey)
    {
        this.appKey = appKey;
    }
    
    /**
     * @return 返回 redPack
     */
    public String getRedPack()
    {
        return redPack;
    }
    
    /**
     * @param 对redPack进行赋值
     */
    public void setRedPack(String redPack)
    {
        this.redPack = redPack;
    }
    
    @Element
    private String redPack; // 是否发送红包
    
    /**
     * @return 返回 wxCardUrl
     */
    public String getWxCardUrl()
    {
        return wxCardUrl;
    }
    
    /**
     * @param 对wxCardUrl进行赋值
     */
    public void setWxCardUrl(String wxCardUrl)
    {
        this.wxCardUrl = wxCardUrl;
    }
    
    /**
     * @return 返回 bankCode
     */
    public String getBankCode()
    {
        return bankCode;
    }
    
    /**
     * @param 对bankCode进行赋值
     */
    public void setBankCode(String bankCode)
    {
        this.bankCode = bankCode;
    }
    
    @Element
    private String bankCode;


   /**返回banknName字段，用来显示处理H5页面带来源的问题**/
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
    
    /**
     * @return 返回 bankType
     */
    public Integer getBankType()
    {
        return bankType;
    }
    
    /**
     * @param 对bankType进行赋值
     */
    public void setBankType(Integer bankType)
    {
        this.bankType = bankType;
    }
    
    /**
     * @return 返回 body
     */
    public String getBody()
    {
        return body;
    }
    
    /**
     * @param 对body进行赋值
     */
    public void setBody(String body)
    {
        this.body = body;
    }
    
    /**
     * @return 返回 payGateway
     */
    public String getPayGateway()
    {
        return payGateway;
    }
    
    /**
     * @param 对payGateway进行赋值
     */
    public void setPayGateway(String payGateway)
    {
        this.payGateway = payGateway;
    }
    
    public int getClientType()
    {
        return clientType;
    }
    
    public void setClientType(int clientType)
    {
        this.clientType = clientType;
    }
    
    public String getAppName()
    {
        return appName;
    }
    
    public void setAppName(String appName)
    {
        this.appName = appName;
    }
    
    public String getServerPort()
    {
        return serverPort;
    }
    
    public void setServerPort(String serverPort)
    {
        this.serverPort = serverPort;
    }
    
    public String getServerAddr()
    {
        return serverAddr;
    }
    
    public void setServerAddr(String serverAddr)
    {
        this.serverAddr = serverAddr;
    }

    public String getServerAddrBack() {
        return serverAddrBack;
    }

    public void setServerAddrBack(String serverAddrBack) {
        this.serverAddrBack = serverAddrBack;
    }
    
    public String getFileSaveRoot()
    {
        return Environment.getExternalStorageDirectory().getAbsolutePath() + fileSaveRoot;
    }
    
    public void setFileSaveRoot(String fileSaveRoot)
    {
        this.fileSaveRoot = fileSaveRoot;
    }
    
    public void setDebug(boolean isDebug)
    {
        this.isDebug = isDebug;
    }
    
    public boolean isCreateLocaltionQRcode()
    {
        return isCreateLocaltionQRcode;
    }
    
    public void setCreateLocaltionQRcode(boolean createLocaltionQRcode)
    {
        isCreateLocaltionQRcode = createLocaltionQRcode;
    }
    
    public boolean isDebug()
    {
        return isDebug;
    }
    
    public boolean isOverseasPay()
    {
        return isOverseasPay;
    }
    
    public void setOverseasPay(boolean isOverseasPay)
    {
        this.isOverseasPay = isOverseasPay;
    }
    
    public static void saveConfig(Context context, Config config)
    {
        OutputStream os = null;
        try
        {
            os = context.openFileOutput(CONFIG_NAME, Context.MODE_PRIVATE);
            Serializer serializer = new Persister();
            serializer.write(config, os);
        }
        catch (Exception e)
        {
            Log.e(TAG,Log.getStackTraceString(e));
        }
        finally
        {
            if (os != null)
            {
                try
                {
                    os.close();
                }
                catch (IOException e)
                {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
            }
        }
    }
    
    public static Config readConfig(Context context)
    {
        Serializer serializer = new Persister();
        InputStream fs = null;
        try
        {
            
            if (context.getFileStreamPath(CONFIG_NAME).exists())
            {
                fs = new FileInputStream(context.getFileStreamPath(CONFIG_NAME));
            }
            else
            {
                fs = context.getResources().getAssets().open(CONFIG_NAME);
            }
            Config config = serializer.read(Config.class, fs);
            return config;
        }
        catch (Exception e)
        {
            Log.e(TAG,Log.getStackTraceString(e));
        }
        finally
        {
            if (fs != null)
            {
                try
                {
                    fs.close();
                }
                catch (IOException e)
                {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
            }
        }
        
        return new Config();
    }
}
