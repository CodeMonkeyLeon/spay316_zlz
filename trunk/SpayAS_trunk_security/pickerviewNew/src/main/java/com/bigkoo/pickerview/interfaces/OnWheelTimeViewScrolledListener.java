package com.bigkoo.pickerview.interfaces;

public interface OnWheelTimeViewScrolledListener {
    void onWheelTimeViewScrolled(int type);
}
