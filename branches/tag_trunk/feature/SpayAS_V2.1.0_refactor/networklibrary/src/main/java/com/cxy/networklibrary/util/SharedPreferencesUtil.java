package com.cxy.networklibrary.util;

import android.content.Context;
import android.content.SharedPreferences;
import com.cxy.networklibrary.CoreComponent;

import java.util.Map;
import java.util.Set;

/**
 * 创建人：caoxiaoya
 * 时间：2018/6/13
 * 描述：SharedPreferences缓存管理
 * 备注：
 */
public class SharedPreferencesUtil {

    public static final String mTAG = "oa_sp_data";
    private SharedPreferences mSharedPreferences;
    private static SharedPreferencesUtil mSharedPreferencesUtil;

    //构造方法
    public SharedPreferencesUtil() {
//        this.mSharedPreferences = CoreComponent.application().getSharedPreferences(mTAG,Context.MODE_PRIVATE);
        this.mSharedPreferences=android.preference.PreferenceManager.getDefaultSharedPreferences(CoreComponent.application());
    }

    //单例模式
    public static SharedPreferencesUtil getInstance() {
        if (mSharedPreferencesUtil == null) {
            synchronized (SharedPreferencesUtil.class) {
                if (mSharedPreferencesUtil == null) {
                    mSharedPreferencesUtil = new SharedPreferencesUtil();
                }
            }
        }
        return  mSharedPreferencesUtil;
    }

    public void put(String key, String value) {
        if (mSharedPreferences == null) {
            return;
        }
        mSharedPreferences.edit().putString(key, value).apply();
    }

    public void put(String key, boolean value) {
        if (mSharedPreferences == null) {
            return;
        }
        mSharedPreferences.edit().putBoolean(key, value).apply();
    }

    public void put(String key, float value) {
        if (mSharedPreferences == null) {
            return;
        }
        mSharedPreferences.edit().putFloat(key, value).apply();
    }

    public void put(String key, int value) {
        if (mSharedPreferences == null) {
            return;
        }
        mSharedPreferences.edit().putInt(key, value).apply();
    }

    public void put(String key, long value) {
        if (mSharedPreferences == null) {
            return;
        }
        mSharedPreferences.edit().putLong(key, value).apply();
    }

    public void put(String key, Set<String> value) {
        if (mSharedPreferences == null) {
            return;
        }
        mSharedPreferences.edit().putStringSet(key, value).apply();
    }

    public String getString(String key, String defValue) {
        return mSharedPreferences == null ? null : mSharedPreferences.getString(key, defValue);
    }

    public boolean getBoolean(String key, boolean defValue) {
        return mSharedPreferences == null ? defValue : mSharedPreferences.getBoolean(key, defValue);
    }

    public float getFloat(String key, float defValue) {
        return mSharedPreferences == null ? defValue : mSharedPreferences.getFloat(key, defValue);
    }

    public int getInt(String key, int defValue) {
        return mSharedPreferences == null ? defValue : mSharedPreferences.getInt(key, defValue);
    }

    public long getLong(String key, long defValue) {
        return mSharedPreferences == null ? defValue : mSharedPreferences.getLong(key, defValue);
    }

    public Set<String> getStringSet(String key, Set<String> defValue) {
        return mSharedPreferences == null ? defValue : mSharedPreferences.getStringSet(key, defValue);
    }

    public void remove(String key) {
        if (mSharedPreferences == null) {
            return;
        }
        mSharedPreferences.edit().remove(key).apply();
    }

    public Map<String, ?> getAll() {
        return mSharedPreferences == null ? null : mSharedPreferences.getAll();
    }

    public void clear() {
        mSharedPreferences.edit().clear().apply();
    }
}
