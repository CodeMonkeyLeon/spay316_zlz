package cn.swiftpass.enterprise.newmvp.loginmodule.ui;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.newmvp.base.BaseActivity;
import cn.swiftpass.enterprise.newmvp.config.RequestTypeConfig;
import cn.swiftpass.enterprise.newmvp.loginmodule.contract.LoginContract;
import cn.swiftpass.enterprise.newmvp.loginmodule.model.bean.ExchangeKeyBean;
import cn.swiftpass.enterprise.newmvp.loginmodule.model.bean.LoginBean;
import cn.swiftpass.enterprise.newmvp.loginmodule.presenter.LoginPresenter;

/**
 * 创建人：caoxiaoya
 * 时间：2019/5/5
 * 描述：请求示例-登录
 * 备注：
 */
public class LoginDemoActivity extends BaseActivity<LoginPresenter> implements LoginContract.View {

    private Button btn_login_test;
    private TextView tv_data;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login_demo;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        btn_login_test = (Button) findViewById(R.id.btn_login_test);
        tv_data = (TextView) findViewById(R.id.tv_data);
        initPermission();
        mPresenter = new LoginPresenter(this, this);
        btn_login_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.exchangeServiceKey();
            }
        });
    }

    private void initPermission() {
        //8.0动态权限   临时获取
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int checkPermission = checkSelfPermission(Manifest.permission.READ_PHONE_STATE);
            if (checkPermission != PackageManager.PERMISSION_GRANTED) {
                //后面的1为请求码
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
            }
        }

    }

    @Override
    public void onExchangeKeySuccess(ExchangeKeyBean result) {
        mPresenter.login("107590000001", "admin123456");
        tv_data.setText("交换秘钥成功"+result.getSerPubKey());
        Toast.makeText(mContext, "交换秘钥成功", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoginSuccess(LoginBean bean) {
        tv_data.setText("username: "+bean.getUsername());
    }

    @Override
    public void onError(int fromRequest, String code, String errMsg) {
        switch (fromRequest){
            //不管错误来自哪里，每个接口都会根据RequestTypeConfig某个值做标记来做处理
            case RequestTypeConfig.LOGIN_TYPE:
                //如果来自登录login请求失败的时候，做失败的处理
                //例如code=400已在此做处理，如网络无连接处理
                break;
            case RequestTypeConfig.EXCHANGE_TYPE:
                //如果来自交换秘钥请求失败的时候，做失败的处理
                //例如code=400已在此做处理，如网络无连接处理
                break;
            default:
                break;
        }
    }
}
