package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.contract.activity.InstapayInfoContract
import cn.swiftpass.enterprise.utils.JsonUtil
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @如果大地早已冰封
 * @就让我们面对着暖流
 * @走向海
 */
class InstapayInfoPresenter : InstapayInfoContract.Presenter {

    private var mView: InstapayInfoContract.View? = null


    override fun queryOrderByInvoiceId(invoiceId: String?) {
        mView?.let { view ->
            AppClient.queryOrderByInvoiceId(
                MainApplication.getInstance().getUserId().toString(),
                invoiceId,
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        commonResponse?.let {
                            view.queryOrderByInvoiceIdFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        r?.let { response ->
                            //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"",
                                "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                Order::class.java
                            ) as Order
                            order.tradeState =
                                if (TextUtils.isEmpty(order.state)) 0 else order.state.toInt()
                            order.tradeType = order.service
                            order.openid = order.subOpenID
                            order.cashFeel =
                                if (TextUtils.isEmpty(order.cashFee)) 0 else order.cashFee.toLong()
                            order.setUplanDetailsBeans(order.discountDetail)
                            view.queryOrderByInvoiceIdSuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: InstapayInfoContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}