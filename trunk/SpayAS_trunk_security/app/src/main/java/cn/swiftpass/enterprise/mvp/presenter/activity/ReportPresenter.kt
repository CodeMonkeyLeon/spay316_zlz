package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.OrderTotalInfo
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.ReportContract
import cn.swiftpass.enterprise.utils.JsonUtil
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @愿你低声啜泣
 * @但不要彻夜不眠
 */
class ReportPresenter : ReportContract.Presenter {


    private var mView: ReportContract.View? = null


    override fun orderCount(
        startTime: String?,
        endTime: String?,
        countMethod: Int,
        userId: String?,
        countDayKind: String?,
        isTrade: Boolean
    ) {
        mView?.let { view ->

            if (isTrade) {
                view.showLoading(
                    R.string.public_data_loading,
                    ParamsConstants.COMMON_LOADING
                )
            }

            AppClient.orderCount(
                MainApplication.getInstance().getMchId(),
                "1",
                startTime,
                endTime,
                countMethod.toString(),
                MainApplication.getInstance().getUserId().toString(),
                countDayKind,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<OrderTotalInfo>(OrderTotalInfo()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.orderCountFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果dataList值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"dataList\":\"\"",
                                "\"dataList\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                OrderTotalInfo::class.java
                            ) as OrderTotalInfo
                            order.orderTotalItemInfo = order.dataList
                            view.orderCountSuccess(order, countMethod)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: ReportContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}