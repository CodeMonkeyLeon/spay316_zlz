/*
 * 文 件 名:  MarketingActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-1-12
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.marketing;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.order.HistoryRankingManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.AwardModel;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.adapter.RefundViewPagerAdapter;
import cn.swiftpass.enterprise.ui.widget.PullDownListView;
import cn.swiftpass.enterprise.ui.widget.TitleBar;

/**
 * 营销规则
 * 
 * @author  he_hui
 * @version  [版本号, 2016-1-12]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class MarketingActivity extends TemplateActivity implements PullDownListView.OnRefreshListioner,
    OnPageChangeListener, PullDownListView.OnLoadDateRefreshListioner
{
    private ViewPager viewpager;
    
    private List<AwardModel> scanList, fulfilList;
    
    private MarketingAdapter sacnAdapter, fulfilAdapter;
    
    private List<View> pageList;
    
    private PullDownListView scanListView, fulfilListView;
    
    private ListView fulfillistViewAuditing;
    
    private PullDownListView scanlistViewAuditing;
    
    private RefundViewPagerAdapter viewPagerAdapter;
    
    private TextView tv_scan_minus, tv_full_send, tv_add;
    
    private RelativeLayout lay_back;
    
    private boolean isTag = false; // 默认是扫码立减
    
    private LinearLayout lay_switch, lay_start;
    
    List<AwardModel> stop = new ArrayList<AwardModel>();
    
    List<AwardModel> noStart;
    
    private TextView tv_noStart, tv_stop, tv_start_text;
    
    private LinearLayout lay_no_award;
    
    private RelativeLayout ly_no_start;
    
    private String activeType;
    
    private ListView listView;
    
    private int pageFulfil = 1;
    
    private TextView tx_no_start;
    
    private Integer totalPageCount = 0;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marketing);
        initData();
        initView();
        activeType = getIntent().getStringExtra("activeType");
        setLister();
        loadDate(1, true);
    }
    
    public static void startActivity(Context context, String activeType)
    {
        Intent it = new Intent();
        it.setClass(context, MarketingActivity.class);
        it.putExtra("activeType", activeType);
        context.startActivity(it);
    }
    
    private boolean isFirst = true;
    
    @Override
    protected void onResume()
    {
        super.onResume();
        if (!isFirst)
        {
            //            isFirst = false;
            loadDate(1, false);
        }
    }
    
    private void loadDate(int page, final boolean isLoadMore)
    {
        HistoryRankingManager.getInstance().getActiveList("1", null, page, new UINotifyListener<List<AwardModel>>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                if (isLoadMore)
                {
                    showNewLoading(true, getString(R.string.public_data_loading));
                }
            }
            
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dismissLoading();
                if (checkSession())
                {
                    return;
                }
                //                if (object != null)
                //                {
                //                    showToastInfo(object.toString());
                //                }
            }
            
            @Override
            public void onSucceed(List<AwardModel> result)
            {
                super.onSucceed(result);
                dismissLoading();
                
                if (result != null && result.size() > 0)
                {
                    isFirst = false;
                    noStart = new ArrayList<AwardModel>();
                    totalPageCount = result.get(0).getTotalPageCount();
                    //                    List<AwardModel> start = new ArrayList<AwardModel>();
                    
                    //                    for (AwardModel a : result)
                    //                    {
                    //                        switch (a.getActiveStatus())
                    //                        {
                    //                            case 0: //未开始的活动
                    //                                noStart.add(a);
                    //                                break;
                    //                            case 1://已经开始的活动
                    //                                start.add(a);
                    //                            case 2: //
                    //                                stop.add(a);// 已停止的活动
                    //                                break;
                    //                            default:
                    //                                break;
                    //                        }
                    //                    }
                    
                    //                    if (noStart.size() > 0)
                    //                    {
                    //                        lay_no_award.setVisibility(View.GONE);
                    //                        tv_noStart.setText(getString(R.string.tx_market_same) + noStart.size()
                    //                            + getString(R.string.tx_market_nostart));
                    //                    }
                    //                    else
                    //                    {
                    //                        tv_noStart.setText(R.string.tx_no_active);
                    //                    }
                    
                    //                    if (start.size() > 0)
                    //                    {
                    MarketingActivity.this.runOnUiThread(new Runnable()
                    {
                        
                        @Override
                        public void run()
                        {
                            lay_start.setVisibility(View.GONE);
                            viewpager.setVisibility(View.VISIBLE);
                            lay_no_award.setVisibility(View.GONE);
                            //                    scanList.addAll(result);
                            //                    sacnAdapter.notifyDataSetChanged();
                            lay_start.setVisibility(View.GONE);
                            tx_no_start.setVisibility(View.VISIBLE);
                        }
                    });
                    //                    tv_start_text.setVisibility(View.VISIBLE);
                    
                    mySetListData(scanlistViewAuditing, scanList, sacnAdapter, result, isLoadMore);
                    
                }
                else
                {
                    MarketingActivity.this.runOnUiThread(new Runnable()
                    {
                        
                        @Override
                        public void run()
                        {
                            lay_start.setVisibility(View.VISIBLE);
                            tx_no_start.setVisibility(View.GONE);
                            viewpager.setVisibility(View.GONE);
                        }
                    });
                }
                
            }
        });
        
    }
    
    private void setLister()
    {
        lay_back.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
        
        //扫码立减
        tv_scan_minus.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (isTag)
                {
                    isTag = false;
                    viewpager.setCurrentItem(0);
                    tv_scan_minus.setBackgroundResource(R.drawable.button_switch_01);
                    tv_full_send.setBackgroundResource(R.drawable.button_switch_02);
                }
            }
        });
        //满额送
        tv_full_send.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                isTag = true;
                viewpager.setCurrentItem(1);
                tv_full_send.setBackgroundResource(R.drawable.button_switch_04);
                tv_scan_minus.setBackgroundResource(R.drawable.button_switch_03);
            }
        });
        
        ly_no_start.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (noStart != null && noStart.size() > 0)
                {
                    showPage(NoStartActivity.class);
                }
                else
                {
                    showToastInfo(R.string.tx_no_active);
                }
            }
        });
    }
    
    private void initData()
    {
        scanList = new ArrayList<AwardModel>();
        fulfilList = new ArrayList<AwardModel>();
        //        for (int i = 0; i < 16; i++)
        //        {
        //            Order order = new Order();
        //            order.setCardNO("111" + i);
        //            scanList.add(order);
        //            fulfilList.add(order);
        //        }
        sacnAdapter = new MarketingAdapter(this, scanList);
        fulfilAdapter = new MarketingAdapter(this, fulfilList);
    }
    
    /**  
     * 重新计算ListView的高度，解决ScrollView和ListView两个View都有滚动的效果，在嵌套使用时起冲突的问题  
     * @param listView  
     */
    public void setListViewHeight(ListView listView)
    {
        
        // 获取ListView对应的Adapter    
        
        ListAdapter listAdapter = listView.getAdapter();
        
        if (listAdapter == null)
        {
            return;
        }
        int totalHeight = 0;
        for (int i = 0, len = listAdapter.getCount(); i < len; i++)
        { // listAdapter.getCount()返回数据项的数目    
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0); // 计算子项View 的宽高    
            totalHeight += listItem.getMeasuredHeight(); // 统计所有子项的总高度    
        }
        
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tv_marketing_title);
        //        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.tx_add));
        titleBar.setRightButLayVisible(true, 0);
        titleBar.setRightButLayBackground(R.drawable.icon_add);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                //历史排行榜
                showPage(MarketingAddActivity.class);
                
                //                MainActivity mainActivity = new MainActivity();
                //                mainActivity.setTabView(MainActivity.TAB_ORDER);
                //                MainActivity.startActivity(MarketingActivity.this, "OrderDetailsActivity");
                //                Intent intent =
                //                    new Intent(MarketingActivity.this, OrderStreamNewActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //                //把一个Activity转换成一个View  
                //                Window w = BaseActivity.group.getLocalActivityManager().startActivity("OrderStreamNewActivity", intent);
                //                View view = w.getDecorView();
                //                //把View添加大ActivityGroup中  
                //                BaseActivity.group.setContentView(view);
                
            }
            
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
        });
    }
    
    private Handler mHandler = new Handler();
    
    private void mySetListData(final PullDownListView pull, List<AwardModel> list, MarketingAdapter adapter,
        List<AwardModel> result, boolean isLoadMore)
    {
        if (!isLoadMore)
        {
            pull.onfinish(getStringById(R.string.refresh_successfully));// 刷新完成
            
            mHandler.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    // 这里表示刷新处理完成后把上面的加载刷新界面隐藏
                    pull.onRefreshComplete();
                }
            }, 800);
            
            list.clear();
        }
        else
        {
            mHandler.postDelayed(new Runnable()
            {
                
                @Override
                public void run()
                {
                    pull.onLoadMoreComplete();
                    // pull.setMore(true);
                }
            }, 1000);
        }
        list.addAll(result);
        adapter.notifyDataSetChanged();
        
    }
    
    private void initView()
    {
        tx_no_start = getViewById(R.id.tx_no_start);
        viewpager = getViewById(R.id.viewpager);
        ly_no_start = getViewById(R.id.ly_no_start);
        tv_start_text = getViewById(R.id.tv_start_text);
        tv_noStart = getViewById(R.id.tv_noStart);
        lay_no_award = getViewById(R.id.lay_no_award);
        pageList = new ArrayList<View>();
        lay_switch = getViewById(R.id.lay_switch);
        lay_back = getViewById(R.id.lay_back);
        tv_add = getViewById(R.id.tv_add);
        tv_full_send = getViewById(R.id.tv_full_send);
        tv_scan_minus = getViewById(R.id.tv_scan_minus);
        lay_start = getViewById(R.id.lay_start);
        View v1 = View.inflate(this, R.layout.activity_marketing_item, null);
        pageList.add(v1);
        
        View v2 = View.inflate(this, R.layout.activity_marketing_item, null);
        //        pageList.add(v2);
        
        scanlistViewAuditing = (PullDownListView)v1.findViewById(R.id.markteing_pulldown);
        listView = scanlistViewAuditing.mListView;
        listView.setAdapter(sacnAdapter);
        scanlistViewAuditing.setAutoLoadMore(true);
        scanlistViewAuditing.setRefreshListioner(this);
        scanlistViewAuditing.setOnLoadDateRefreshListioner(this);
        
        //        fulfillistViewAuditing = (ListView)v2.findViewById(R.id.markteing_pulldown);
        
        tv_stop = getViewById(R.id.tv_stop);
        tv_stop.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (stop.size() > 0)
                {
                    
                    NoStartActivity.startActivity(MarketingActivity.this, "2");
                }
                else
                {
                    showToastInfo(R.string.tx_stop_active);
                }
            }
        });
        //        scanListView.setAutoLoadMore(true);
        //        fulfilListView.setAutoLoadMore(true);
        
        //        scanlistViewAuditing = scanListView.mListView;
        //        fulfillistViewAuditing = fulfilListView.mListView;
        
        //        scanListView.setRefreshListioner(this);
        //        fulfilListView.setRefreshListioner(this);
        
        //        scanlistViewAuditing.setAdapter(sacnAdapter);
        //        fulfillistViewAuditing.setAdapter(fulfilAdapter);
        
        //        setListViewHeight(scanlistViewAuditing);
        //        setListViewHeight(fulfillistViewAuditing);
        
        viewPagerAdapter = new RefundViewPagerAdapter(pageList);
        viewpager.setAdapter(viewPagerAdapter);
        viewpager.setOnPageChangeListener(this);
        viewpager.setCurrentItem(0);
        
        listView.setOnItemClickListener(new OnItemClickListener()
        {
            
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int poiston, long arg3)
            {
                AwardModel awardModel = scanList.get(poiston - 1);
                if (awardModel != null)
                {
                    //                    MarketingDetailActivity.startActivity(MarketingActivity.this, awardModel);
                    HistoryRankingManager.getInstance().queryActiveDetais(awardModel.getActiveId(),
                        new UINotifyListener<AwardModel>()
                        {
                            @Override
                            public void onPreExecute()
                            {
                                super.onPreExecute();
                                
                                showNewLoading(true, getString(R.string.public_data_loading));
                            }
                            
                            @Override
                            public void onError(final Object object)
                            {
                                super.onError(object);
                                dismissLoading();
                                if (checkSession())
                                {
                                    return;
                                }
                                if (object != null)
                                {
                                    toastDialog(MarketingActivity.this, object.toString(), null);
                                    //                                    showToastInfo(object.toString());
                                }
                            }
                            
                            @Override
                            public void onSucceed(AwardModel result)
                            {
                                super.onSucceed(result);
                                dismissLoading();
                                
                                if (result != null)
                                {
                                    //                                    MarketingAddActivity.startActivity(MarketingActivity.this, result);
                                    MarketingDetailActivity.startActivity(MarketingActivity.this, result);
                                }
                                
                            }
                        });
                }
            }
            
        });
        
    }
    
    @Override
    protected boolean isLoginRequired()
    {
        return true;
    }
    
    @Override
    public void onRefresh()
    {
        pageFulfil = 1;
        loadDate(pageFulfil, false);
    }
    
    @Override
    public void onLoadMore()
    {
        pageFulfil = pageFulfil + 1;
        if (pageFulfil <= totalPageCount)
        {
            loadDate(pageFulfil, true);
        }
        else
        {
            scanlistViewAuditing.onLoadMoreComplete();
        }
    }
    
    @Override
    public void onPageScrollStateChanged(int arg0)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2)
    {
        
    }
    
    @Override
    public void onPageSelected(int page)
    {
        if (pageList == null || pageList.size() < 1)
        {
            return;
        }
        //        changeTextColor(page);
        switch (page)
        {
            case 0:
                isTag = false;
                tv_scan_minus.setBackgroundResource(R.drawable.button_switch_01);
                tv_full_send.setBackgroundResource(R.drawable.button_switch_02);
                break;
            case 1:
                isTag = true;
                tv_full_send.setBackgroundResource(R.drawable.button_switch_04);
                tv_scan_minus.setBackgroundResource(R.drawable.button_switch_03);
                break;
            default:
                break;
        }
    }
    
    @Override
    public void onLoadMoreDate()
    {
        
    }
}
