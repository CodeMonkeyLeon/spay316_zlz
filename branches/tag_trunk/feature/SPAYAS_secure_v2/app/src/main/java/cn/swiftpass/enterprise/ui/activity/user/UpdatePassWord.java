/*

 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-14
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.user;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.WelcomeActivity;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 重置密码
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-14]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class UpdatePassWord extends TemplateActivity
{
    private Button previous;
    
    private Button confirm;
    
    private EditText newPwd, repeatPwd;
    
    private ImageView iv_repeatPwd, iv_newPwd, iv_newPwd_promt, iv_repeatPwd_promt;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_password);
        
        initView();
        
        setLister();
        
    }

//    @Override
//    public boolean equals(Object obj) {
//        return super.equals(obj);
//    }

    private void setLister()
    {
        previous.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
        
        // 确认提交
        confirm.setOnClickListener(new OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (isAbsoluteNullStr(newPwd.getText().toString()))
                {
                    ToastHelper.showInfo("新密码不能为空!!");
                    return;
                }
                
                if (isAbsoluteNullStr(repeatPwd.getText().toString()))
                {
                    ToastHelper.showInfo("重复密码不能为空!!");
                    return;
                }
                
                if (!newPwd.getText().toString().equals(repeatPwd.getText().toString()))
                {
                    ToastHelper.showInfo("两次密码不一致，请从新输入!!");
                    return;
                }
                
                updatePass(newPwd.getText().toString());
            }
            
        });
    }
    
    /**
     * 修改密码
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void updatePass(String newPwd)
    {
        if (MainApplication.userId == 0)
        {
            ToastHelper.showInfo("修改密码失败!!");
            return;
        }
        UserManager.updatePdw(newPwd, String.valueOf(MainApplication.userId), new UINotifyListener<Boolean>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                showLoading(false, "数据加载中...");
            }
            
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                if (object != null)
                {
                    showToastInfo(object.toString());
                }
                dismissLoading();
            }
            
            @Override
            public void onSucceed(Boolean result)
            {
                super.onSucceed(result);
                dismissLoading();
                if (result)
                {
                    showPage(WelcomeActivity.class);
                    MainApplication.updatePwdActivities.add(UpdatePassWord.this);
                    for (Activity a : MainApplication.updatePwdActivities)
                    {
                        a.finish();
                    }
                }
                else
                {
                    //                    ToastHelper.showInfo("信息输入不正确,请重新输入!!");
                }
                
            }
        });
    }
    
    private void initView()
    {
        previous = getViewById(R.id.previous);
        
        confirm = getViewById(R.id.confirm);
        
        newPwd = getViewById(R.id.newPwd);
        
        repeatPwd = getViewById(R.id.repeatPwd);
        
        iv_repeatPwd = getViewById(R.id.iv_repeatPwd);
        
        iv_newPwd = getViewById(R.id.iv_newPwd);
        iv_newPwd_promt = getViewById(R.id.iv_newPwd_promt);
        iv_repeatPwd_promt = getViewById(R.id.iv_repeatPwd_promt);
        
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        {
            
            @Override
            public void onExecute(CharSequence s, int start, int before, int count)
            {
                if (newPwd.hasFocusable())
                {
                    if (newPwd.getText().length() > 0)
                    {
                        iv_newPwd.setVisibility(View.VISIBLE);
                        iv_newPwd_promt.setVisibility(View.GONE);
                    }
                    else
                    {
                        iv_newPwd.setVisibility(View.GONE);
                        iv_newPwd_promt.setVisibility(View.VISIBLE);
                    }
                }
                
                if (repeatPwd.hasFocusable())
                {
                    if (repeatPwd.getText().length() > 0)
                    {
                        iv_repeatPwd.setVisibility(View.VISIBLE);
                        iv_repeatPwd_promt.setVisibility(View.GONE);
                    }
                    else
                    {
                        iv_repeatPwd.setVisibility(View.GONE);
                        iv_repeatPwd_promt.setVisibility(View.VISIBLE);
                    }
                }
            }
            
            @Override
            public void onAfterTextChanged(Editable s)
            {
            }
            
        });
        
        newPwd.addTextChangedListener(editTextWatcher);
        repeatPwd.addTextChangedListener(editTextWatcher);
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle("忘记密码");
    }
    
}
