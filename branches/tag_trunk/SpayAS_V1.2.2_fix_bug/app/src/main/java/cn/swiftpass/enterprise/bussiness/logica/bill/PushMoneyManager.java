/*
 * 文 件 名:  PushMoneyManager.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-8-12
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.logica.bill;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.BaseManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.Blance;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.NetHelper;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2016-8-12]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class PushMoneyManager extends BaseManager
{
    
    @Override
    public void init()
    {
        
    }
    
    @Override
    public void destory()
    {
    }
    
    public static PushMoneyManager getInstance()
    {
        return Container.instance;
    }
    
    private static class Container
    {
        public static PushMoneyManager instance = new PushMoneyManager();
    }
    
    /**
     * 提现记录列表
     * <功能详细描述>
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void accountList(final UINotifyListener<List<Blance>> listener, final Integer page)
    {
        ThreadHelper.executeWithCallback(new Executable<List<Blance>>()
        {
            @Override
            public List<Blance> execute()
                throws Exception
            {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("mchId", "100520000228");
                jsonObject.put("startItem", page);
                jsonObject.put("itemNum", "10");
                jsonObject.put("accountType", "ceb");
                jsonObject.put("trsType", "4");
                
                RequestResult result =
                    NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/accountList", jsonObject, null, null);
                try
                {
                    
                    result.setNotifyListener(listener);
                    
                    if (!result.hasError())
                    {
                        Log.i("hehui", "accountList result data-->" + result.data.getString("message"));
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200) //代表 已经激活成功
                        {
                            JSONObject jsObject = new JSONObject(result.data.getString("message"));
                            List<Blance> bList = new ArrayList<Blance>();
                            
                            JSONArray array = new JSONArray(jsObject.getString("list"));
                            for (int i = 0; i < array.length(); i++)
                            {
                                Blance blance = new Blance();
                                JSONObject o = array.getJSONObject(i);
                                blance.setCoPatrnerJnlNo(o.optString("coPatrnerJnlNo", ""));
                                blance.setTrsTime(o.optString("trsTime", ""));
                                blance.setAmount(o.optString("amount", ""));
                                bList.add(blance);
                            }
                            
                            return bList;
                            
                        }
                        else
                        {
                            listener.onError(result.data.getString("message"));
                        }
                    }
                    else
                    {
                        switch (result.resultCode)
                        {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                            default:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                break;
                        }
                    }
                }
                catch (Exception e)
                {
                    listener.onError("服务器出错，请稍后在试");
                }
                return null;
            }
        },
            listener);
        
    }
    
    /**
     * 提现接口
     * <功能详细描述>
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void accountWithdrawals(final UINotifyListener<Blance> listener, final long amount)
    {
        ThreadHelper.executeWithCallback(new Executable<Blance>()
        {
            @Override
            public Blance execute()
                throws Exception
            {
                JSONObject jsonObject = new JSONObject();
                
                jsonObject.put("accountType", "ceb");
                jsonObject.put("service", "unified.spay.merchant");
                
                Map<String, String> params = new HashMap<String, String>();
                params.put("accountType", "ceb");
                params.put("service", "unified.spay.merchant");
                
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId))
                {
                    jsonObject.put("mchId", MainApplication.merchantId);
                    params.put("mchId", MainApplication.merchantId);
                }
                else
                {
                    jsonObject.put("mchId", MainApplication.getMchId());
                    params.put("mchId", MainApplication.getMchId());
                }
                
                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey()))
                {
                    jsonObject.put("sign", SignUtil.getInstance().createSign(params, MainApplication.getSignKey()));
                }
                RequestResult result =
                    NetHelper.httpsPost(ApiConstant.pushMoneyUrl + "spay/accountAppVerify", jsonObject, null, null);
                try
                {
                    
                    result.setNotifyListener(listener);
                    
                    if (!result.hasError())
                    {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200) //代表 已经激活成功
                        {
                            Log.i("hehui", "accountWithdrawals result data-->" + result.data.getString("message"));
                            return (Blance)JsonUtil.jsonToBean(result.data.getString("message"), Blance.class);
                            
                        }
                        else
                        {
                            listener.onError(result.data.getString("message"));
                        }
                    }
                    else
                    {
                        switch (result.resultCode)
                        {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                            default:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                break;
                        }
                    }
                }
                catch (Exception e)
                {
                    listener.onError("");
                }
                return null;
            }
        },
            listener);
        
    }
    
    public void accBalQuery(final UINotifyListener<Blance> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<Blance>()
        {
            @Override
            public Blance execute()
                throws Exception
            {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("mchId", "100520000228");
                jsonObject.put("accountType", "ceb");
                RequestResult result =
                    NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/accBalQuery", jsonObject, null, null);
                try
                {
                    
                    result.setNotifyListener(listener);
                    
                    if (!result.hasError())
                    {
                        Log.i("hehui", "accBalQuery result data-->" + result.data.getString("message"));
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200) //代表 已经激活成功
                        {
                            JSONObject jsObject = new JSONObject(result.data.getString("message"));
                            if (jsObject.optInt("ishaveAccount", 0) == 1)
                            { // 代表有电子账户
                            
                                return (Blance)JsonUtil.jsonToBean(result.data.getString("message"), Blance.class);
                            }
                            else
                            {
                                return null;
                            }
                            
                        }
                        else
                        {
                            listener.onError(result.data.getString("message"));
                        }
                    }
                    else
                    {
                        switch (result.resultCode)
                        {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                            default:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                break;
                        }
                    }
                }
                catch (Exception e)
                {
                    listener.onError("");
                }
                return null;
            }
        },
            listener);
        
    }
}
