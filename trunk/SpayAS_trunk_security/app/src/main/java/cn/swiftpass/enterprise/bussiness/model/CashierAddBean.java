package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created by aijingya on 2021/2/4.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description:
 * @date 2021/2/4.14:42.
 */
public class CashierAddBean implements Serializable {
    public String passWord;

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }
}
