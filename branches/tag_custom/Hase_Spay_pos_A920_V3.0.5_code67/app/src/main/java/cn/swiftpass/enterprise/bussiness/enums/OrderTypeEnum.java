package cn.swiftpass.enterprise.bussiness.enums;

/**
 * 订单交易类型
 * User: Alan
 * Date: 13-11-4
 * Time: 下午3:33
 *
 */
public enum OrderTypeEnum {
    ONLINE(0, "在线"),
    OFFLINE(1, "离线"),
    OFFLINE_FINISH(2, "离线订单已完成上传");

    private final Integer value;
    private String displayName = "";

    private OrderTypeEnum(Integer v ,String displayName)
    {
        this.value = v;
        this.displayName = displayName;

    }
    public static String getDisplayNameByVaue(Integer v){
        if(v!=null){
            for (OrderTypeEnum ose : values()) {
                if (ose.value.equals(v)){
                    return ose.getDisplayName();
                }
            }
        }
        return "";
    }
    public String getDisplayName() {
        return displayName;
    }

    public Integer getValue() {
        return value;
    }
}
