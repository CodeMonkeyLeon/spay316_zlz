package cn.swiftpass.enterprise.print;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.text.TextUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import CTOS.CtPrint;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalInfo;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalItemInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.print.BluePrintUtil;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * Created by aijingya on 2020/3/12.
 *
 * @Package cn.swiftpass.enterprise.print
 * @Description: Castles打印机适配打印类(用一句话描述该文件做什么)
 * @date 2020/3/12.17:45.
 */
public class CastlesPrintUtils {
    public static int font_Size_Big = 40;
    public static int font_Size_Middle = 30;
    public static int font_Size_Small = 24;
    public static int font_Size_min = 22;

    public static int print_title_start_x = 60;
    public static int print_title_start_x2 = 100;
    public static int print_content_start_x = 5;
    public static int print_content_start_x2 = 10;
    public static int print_start_y = 40;
    public static int Currently_high = 30;
    public static int rollPaper = 10;
    public static Bitmap bitmap;


    public static void printDailySummary(OrderTotalInfo info){
        CtPrint print = new CtPrint();
        print.setHeatLevel(2);
        print.initPage(1100);

        print.drawText(print_title_start_x, print_start_y,  ToastHelper.toStr(R.string.tx_blue_print_data_sum) , font_Size_Big, 1, true, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);


        print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.shop_name) + "：" , font_Size_Small, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, MainApplication.getMchName()  , font_Size_Small, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        if (!StringUtil.isEmptyOrNull(info.getUserName())) {
            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_user) + ":" + info.getUserName(),
                    font_Size_Small, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);
        }

        print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_blue_print_start_time) + ":" + info.getStartTime(),
                font_Size_Small, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_blue_print_end_time) + ":" + info.getEndTime(),
                font_Size_Small, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, "===============================",
                font_Size_Small, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_pay_money) + ":"+DateUtil.formatMoneyUtils(info.getCountTotalFee()) + ToastHelper.toStr(R.string.pay_yuan),
                    font_Size_Small, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);
        } else {
            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_pay_money) + ":"+MainApplication.getFeeType() + DateUtil.formatMoneyUtils(info.getCountTotalFee()),
                    font_Size_Small, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);
        }

        print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_bill_stream_pay_money_num) + ":"+ info.getCountTotalCount(),
                font_Size_Small, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        if (MainApplication.feeFh.equalsIgnoreCase("¥")&& MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tv_refund_dialog_refund_money)+DateUtil.formatMoneyUtils(info.getCountTotalRefundFee()) + ToastHelper.toStr(R.string.pay_yuan),
                    font_Size_Small, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);
        } else {
            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tv_refund_dialog_refund_money)+MainApplication.getFeeType() + DateUtil.formatMoneyUtils(info.getCountTotalRefundFee()) ,
                    font_Size_Small, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);
        }

        print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_bill_reufun_total_num) + ":" + info.getCountTotalRefundCount(),
                font_Size_Small, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {//如果是人民币
            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_blue_print_pay_total) +":"+DateUtil.formatMoneyUtils((info.getCountTotalFee() - info.getCountTotalRefundFee())) + ToastHelper.toStr(R.string.pay_yuan) ,
                    font_Size_Small, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);
        }else {
            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_blue_print_pay_total) + ":"+MainApplication.getFeeType() + DateUtil.formatMoneyUtils((info.getCountTotalFee() - info.getCountTotalRefundFee())) ,
                    font_Size_Small, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);
        }

        print.drawText(print_content_start_x, print_start_y + Currently_high, "===============================",
                font_Size_Small, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        /**
         * 是否开通小费功能，打印小费金额、总计
         */
        if (MainApplication.isTipOpenFlag()){
            // 小费金额
            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tip_amount) +":"+MainApplication.getFeeType() + DateUtil.formatMoneyUtils(info.getCountTotalTipFee()) ,
                    font_Size_Small, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            //小费笔数
            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tip_count) +":" + info.getCountTotalTipFeeCount() ,
                    font_Size_Small, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);
        }

        List<OrderTotalItemInfo> list = info.getOrderTotalItemInfo();

        Map<Integer, Object> map = new HashMap<Integer, Object>();

        for (OrderTotalItemInfo itemInfo : list) {

            switch (itemInfo.getPayTypeId()) {

                case 1:
                    map.put(0, itemInfo);
                    break;
                case 2:
                    map.put(1, itemInfo);
                    break;

                case 4:
                    map.put(2, itemInfo);
                    break;
                case 12:
                    map.put(3, itemInfo);
                    break;
                default:
                    map.put(itemInfo.getPayTypeId(), itemInfo);
                    break;
            }

        }

        for (Integer key : map.keySet()) {
            OrderTotalItemInfo itemInfo = (OrderTotalItemInfo) map.get(key);
            if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan) )) {
                print.drawText(print_content_start_x, print_start_y + Currently_high, MainApplication.getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId())) + ToastHelper.toStr(R.string.tx_money)+DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + ToastHelper.toStr(R.string.pay_yuan),
                        font_Size_Small, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);
            } else {
                print.drawText(print_content_start_x, print_start_y + Currently_high, MainApplication.getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId()))+"",
                        font_Size_Small, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);

                print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_money)+ MainApplication.getFeeType() + DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()),
                        font_Size_Small, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);
            }

            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tv_settle_count) + "："+ itemInfo.getSuccessCount() ,
                    font_Size_Small, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);
        }
        print.drawText(print_content_start_x, print_start_y + Currently_high, "-------------------------------",
                font_Size_Big, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) ,
                font_Size_Small, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_blue_print_sign) + "：",
                font_Size_Small, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        //恢复初始值
        Currently_high = 30;

        print.printPage();
        print.roll(rollPaper);
    }


    public static void printOrder(boolean isCashierShow, Order orderModel){
        CtPrint print = new CtPrint();
        print.setHeatLevel(2);


        if(orderModel.isPay()){//收款
            print.initPage(1500);
            print.drawText(print_title_start_x2, print_start_y, ToastHelper.toStr(R.string.tx_blue_print_pay_note) , font_Size_Big, 1, true, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);
        }else{
            print.initPage(1400);
            print.drawText(print_title_start_x, Currently_high,  ToastHelper.toStr(R.string.tx_blue_print_refund_note) , font_Size_Big, 1, true, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);
        }

        print.drawText(print_content_start_x, print_start_y + Currently_high, orderModel.getPartner() , font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tv_pay_client_save), font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tv_pay_client_save1), font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, "===============================",
                font_Size_Small, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.shop_name) + ":", font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, MainApplication.getMchName(), font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + ":", font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, MainApplication.getMchId(), font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        if(orderModel.isPay()){//收款
            if (MainApplication.isAdmin.equals("0") && !isCashierShow) { //收银员
                print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.tx_user) + ":" + MainApplication.realName ,
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);
            }
            if (isCashierShow){
                print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.tx_user) + ":",
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);

                print.drawText(print_content_start_x, print_start_y + Currently_high,orderModel.getUserName(),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);
            }

            try {
                print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.tx_bill_stream_time) + ":",
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);

                print.drawText(print_content_start_x, print_start_y + Currently_high,orderModel.getAddTimeNew(),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);
            } catch (Exception e) {
                print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.tx_bill_stream_time) + ":",
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);

                print.drawText(print_content_start_x, print_start_y + Currently_high,orderModel.getTradeTimeNew() ,
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);
            }

        }else{
            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_blue_print_refund_time) + ":"  , font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            print.drawText(print_content_start_x, print_start_y + Currently_high, orderModel.getAddTimeNew() , font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.refund_odd_numbers) + ":"  , font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            print.drawText(print_content_start_x, print_start_y + Currently_high, orderModel.getRefundNo(), font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);
        }

        print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_order_no) + ":"  , font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, orderModel.getOrderNoMch(), font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        if (orderModel.isPay()) {
            if (MainApplication.getPayTypeMap() != null && MainApplication.getPayTypeMap().size() > 0) {
                print.drawText(print_content_start_x, print_start_y + Currently_high, MainApplication.getPayTypeMap().get(orderModel.getApiCode())+ " " + ToastHelper.toStr(R.string.tx_orderno) + ":",
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);
            }
            print.drawText(print_content_start_x, print_start_y + Currently_high, orderModel.getTransactionId()  ,
                    font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + ":"  , font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            print.drawText(print_content_start_x, print_start_y + Currently_high, orderModel.getTradeName(), font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_bill_stream_statr)+ ":"  , font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            print.drawText(print_content_start_x, print_start_y + Currently_high,MainApplication.getTradeTypeMap().get(orderModel.getTradeState() + ""), font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            if (!StringUtil.isEmptyOrNull(orderModel.getAttach())) {
                print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.tx_bill_stream_attach) + ":" + orderModel.getAttach(), font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);
            }

            if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
                if (orderModel.getDaMoney() > 0) {
                    long actualMoney = orderModel.getDaMoney() + orderModel.getOrderFee();

                    print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.tx_blue_print_money) + ":" +DateUtil.formatRMBMoneyUtils(actualMoney) + ToastHelper.toStr(R.string.pay_yuan),
                            font_Size_min, 1, false, (float) 0, false, false);
                    Currently_high += print_start_y;
                    print.roll(rollPaper);
                } else {
                    print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.tx_blue_print_money) + ":" +DateUtil.formatRMBMoneyUtils(orderModel.getOrderFee()) + ToastHelper.toStr(R.string.pay_yuan),
                            font_Size_min, 1, false, (float) 0, false, false);
                    Currently_high += print_start_y;
                    print.roll(rollPaper);

                }
            } else {
                if (MainApplication.isSurchargeOpen()) {
                    if(!orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") &&  !orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")){
                        print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.tx_blue_print_money)+ ":" +MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getOrderFee()),
                                font_Size_min, 1, false, (float) 0, false, false);
                        Currently_high += print_start_y;
                        print.roll(rollPaper);


                        print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.tx_surcharge)+ ":" +MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getSurcharge()),
                                font_Size_min, 1, false, (float) 0, false, false);
                        Currently_high += print_start_y;
                        print.roll(rollPaper);

                    }
                }

                print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tv_charge_total)+ ":" +MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee()) ,
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);

                if (orderModel.getCashFeel() > 0) {
                    print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.pay_yuan)+ DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()),
                            font_Size_min, 1, false, (float) 0, false, false);
                    Currently_high += print_start_y;
                    print.roll(rollPaper);
                }
            }
        } else {
            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + ":",
                    font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            print.drawText(print_content_start_x, print_start_y + Currently_high, orderModel.getTradeName() ,
                    font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            if (!StringUtil.isEmptyOrNull(orderModel.getUserName())) {
                print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.tv_refund_peop) + ":",
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);

                print.drawText(print_content_start_x, print_start_y + Currently_high, orderModel.getUserName(),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);
            }
            if (MainApplication.getRefundStateMap() != null && MainApplication.getRefundStateMap().size() > 0) {
                print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.tv_refund_state) + ":",
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);

                print.drawText(print_content_start_x, print_start_y + Currently_high, MainApplication.getRefundStateMap().get(orderModel.getRefundState() + ""),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);

            }
            if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
                print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.tx_blue_print_money) + ":"
                        +DateUtil.formatRMBMoneyUtils(orderModel.getTotalFee()) + ToastHelper.toStr(R.string.pay_yuan),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);


                print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.tx_bill_stream_refund_money)  + ":"
                                +DateUtil.formatRMBMoneyUtils(orderModel.getRefundMoney()) + ToastHelper.toStr(R.string.pay_yuan),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);
            } else {
                print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.tv_charge_total) + ":"
                                + MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee()),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);

                print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + ":"
                                + MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getRefundMoney()),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);

                if(orderModel.getCashFeel() > 0){
                    print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.pay_yuan)+ DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()),
                            font_Size_min, 1, false, (float) 0, false, false);
                    Currently_high += print_start_y;
                    print.roll(rollPaper);
                }
            }
            if (!StringUtil.isEmptyOrNull(orderModel.getPrintInfo1())) {
                print.drawText(print_content_start_x, print_start_y + Currently_high,orderModel.getPrintInfo1(),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);
            }

            if (!StringUtil.isEmptyOrNull(orderModel.getPrintInfo2())) {
                print.drawText(print_content_start_x, print_start_y + Currently_high,orderModel.getPrintInfo2(),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);
            }

            if (!StringUtil.isEmptyOrNull(orderModel.getPrintInfo3())) {
                print.drawText(print_content_start_x, print_start_y + Currently_high,orderModel.getPrintInfo3(),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);
            }

            if (!StringUtil.isEmptyOrNull(orderModel.getPrintInfo4())) {
                print.drawText(print_content_start_x, print_start_y + Currently_high,orderModel.getPrintInfo4(),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);
            }

            if (!StringUtil.isEmptyOrNull(orderModel.getPrintInfo5())) {
                print.drawText(print_content_start_x, print_start_y + Currently_high,orderModel.getPrintInfo5(),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);
            }
        }
        if(!StringUtil.isEmptyOrNull(orderModel.getOrderNoMch()) && orderModel.isPay()){
            bitmap = print.encodeToBitmap(orderModel.getOrderNoMch(), print.QR_CODE, 200, 200);

            // 计算左边位置
            int left = 200 - 200 / 2;
            // 计算上边位置
            int top = Currently_high;
            Rect mDestRect = new Rect(left, top, left + 200, top + 200);
            print.drawImage(bitmap,new Rect(0,0,200,200),mDestRect);
            Currently_high+= 200;

            print.roll(rollPaper);

            print.drawText(print_content_start_x2+10, print_start_y + Currently_high, ToastHelper.toStr(R.string.refound_QR_code) , font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            print.drawText(print_title_start_x2+30, print_start_y + Currently_high, ToastHelper.toStr(R.string.refound_QR_code1) , font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y+10;
            print.roll(rollPaper);
        }

        print.drawText(print_content_start_x, print_start_y + Currently_high, "-------------------------------",
                font_Size_Big, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_blue_print_time) + ":" + DateUtil.formatTime(System.currentTimeMillis()) , font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_blue_print_sign) + ":", font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        //恢复初始值
        Currently_high = 30;

        print.printPage();
        print.roll(rollPaper);
    }


    /**
     * printType参数1：预授权成功---预授权小票
     * printType参数4：预授权订单详情---预授权小票
     * printType参数2：预授权解冻详情小票
     * printType参数3：预授权解冻完成小票
     * printType参数5：预授权成功---预授权小票---预授权反扫完下单一次成功
     */
    public static void printPreAut(int printType,Order orderModel){
        int minus=1;
        //根据当前的小数点的位数来判断应该除以或者乘以多少
        for(int i = 0 ; i < MainApplication.numFixed ; i++ ){
            minus = minus * 10;
        }

        CtPrint print = new CtPrint();
        print.setHeatLevel(2);

        switch (printType){
            case 1:
            case 4:
            case 5:
                //此二维码用于预授权收款、解冻操作
                print.initPage(1500);
                print.drawText(print_title_start_x-10, print_start_y, ToastHelper.toStr(R.string.pre_auth_receipt) , font_Size_Big, 1, true, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);
                break;

            case 2:
            case 3:
                print.initPage(1400);
                //此二维码用于预授权收款、解冻操作
                print.drawText(0, print_start_y, ToastHelper.toStr(R.string.pre_auth_unfreezed_receipt) , font_Size_Middle, 1, true, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);

                break;
        }
        print.drawText(print_content_start_x, print_start_y + Currently_high, orderModel.getPartner() , font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tv_pay_client_save), font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tv_pay_client_save1), font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, "===============================",
                font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.shop_name) + ":", font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high,MainApplication.getMchName() , font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        //商户编号
        print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + ":", font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high,MainApplication.getMchId() , font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        if (printType==1) {
           //交易时间
            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.pre_auth_time)+ ":", font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            print.drawText(print_content_start_x, print_start_y + Currently_high,orderModel.getTradeTime() , font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

        }else if(printType==4){
            //交易时间
            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.pre_auth_time)+ ":", font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            print.drawText(print_content_start_x, print_start_y + Currently_high,orderModel.getTradeTimeNew() , font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

        }else if(printType==5){
            //交易时间
            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.pre_auth_time)+ ":", font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            print.drawText(print_content_start_x, print_start_y + Currently_high,orderModel.getTimeEnd() , font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);
        }
        else if(printType==2){
            //解冻时间
            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.unfreezed_time)+ ":", font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            print.drawText(print_content_start_x, print_start_y + Currently_high,orderModel.getOperateTime() , font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

        }else if(printType==3){
            //解冻时间
            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.unfreezed_time)+ ":", font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            print.drawText(print_content_start_x, print_start_y + Currently_high,orderModel.getUnFreezeTime() , font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);
        }

        if (printType==1 || printType == 4 || printType == 5){
            //平台预授权订单号
            print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.platform_pre_auth_order_id)+ ":", font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            print.drawText(print_content_start_x, print_start_y + Currently_high,orderModel.getAuthNo() , font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            String language = PreferenceUtil.getString("language","");
            Locale locale = MainApplication.getContext().getResources().getConfiguration().locale; //zh-rHK
            String lan = locale.getCountry();
            if (!TextUtils.isEmpty(language)) {
                if (language.equals(MainApplication.LANG_CODE_EN_US)) {
                    //支付宝单号
                    print.drawText(print_content_start_x, print_start_y + Currently_high,MainApplication.getPayTypeMap().get("2") + " "+ ToastHelper.toStr(R.string.tx_orderno) +":" ,
                            font_Size_min, 1, false, (float) 0, false, false);
                    Currently_high += print_start_y;
                    print.roll(rollPaper);
                } else {
                    //支付宝单号
                    print.drawText(print_content_start_x, print_start_y + Currently_high,MainApplication.getPayTypeMap().get("2") + ToastHelper.toStr(R.string.tx_orderno) +":" ,
                            font_Size_min, 1, false, (float) 0, false, false);
                    Currently_high += print_start_y;
                    print.roll(rollPaper);
                }
            } else {
                if (lan.equalsIgnoreCase("en")) {
                    //支付宝单号
                    print.drawText(print_content_start_x, print_start_y + Currently_high,MainApplication.getPayTypeMap().get("2") + " "+ToastHelper.toStr(R.string.tx_orderno)  +":" ,
                            font_Size_min, 1, false, (float) 0, false, false);
                    Currently_high += print_start_y;
                    print.roll(rollPaper);
                } else {
                    //支付宝单号
                    print.drawText(print_content_start_x, print_start_y + Currently_high,MainApplication.getPayTypeMap().get("2") + ToastHelper.toStr(R.string.tx_orderno) +":" ,
                            font_Size_min, 1, false, (float) 0, false, false);
                    Currently_high += print_start_y;
                    print.roll(rollPaper);
                }
            }

            if(printType==1 || printType == 5){
                print.drawText(print_content_start_x, print_start_y + Currently_high,orderModel.getOutTransactionId(),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);
            }

            if(printType== 4){
                print.drawText(print_content_start_x, print_start_y + Currently_high,orderModel.getTransactionId() ,
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);
            }

        }else if(printType == 2 || printType== 3){

            //解冻订单号
            print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.unfreezed_order_id) +":" ,
                    font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            print.drawText(print_content_start_x, print_start_y + Currently_high,orderModel.getOutRequestNo() ,
                    font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            //平台预授权订单号
            print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.platform_pre_auth_order_id)+":" ,
                    font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            print.drawText(print_content_start_x, print_start_y + Currently_high,orderModel.getAuthNo() ,
                    font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

        }

        if(printType == 5){
            String tradename;
            if (!isAbsoluteNullStr(orderModel.getTradeName())){//先判断大写的Name，再判断小写name
                tradename=orderModel.getTradeName();
            }else {
                tradename=orderModel.getTradename();
            }

            //支付方式
            print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.tx_bill_stream_choice_title) +":" ,
                    font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            print.drawText(print_content_start_x, print_start_y + Currently_high,tradename,
                    font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);
        }else{
            //支付方式
            print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.tx_bill_stream_choice_title)+":" ,
                    font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            print.drawText(print_content_start_x, print_start_y + Currently_high,orderModel.getTradeName(),
                    font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);
        }


        if (printType == 3) {
            //办理人
            print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.tv_unfreezen_applicant)+":" ,
                    font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

            print.drawText(print_content_start_x, print_start_y + Currently_high,orderModel.getUserName(),
                    font_Size_min, 1, false, (float) 0, false, false);
            Currently_high += print_start_y;
            print.roll(rollPaper);

        }

        //订单状态
        print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.tx_bill_stream_statr) +":" ,
                font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);


        String operationType = "";
        if(printType == 1 || printType == 4){
            operationType = MainApplication.getPreAuthtradeStateMap().get(orderModel.getTradeState() + "");

        }else if(printType == 2){
            switch (orderModel.getOperationType()){
                case 2:
                    operationType = ToastHelper.toStr(R.string.freezen_success);
                    break;
            }
        }else if(printType == 3){
            operationType = ToastHelper.toStr(R.string.unfreezing);
        }else if (printType == 5){
            operationType = ToastHelper.toStr(R.string.pre_auth_authorized);
        }
        print.drawText(print_content_start_x, print_start_y + Currently_high,operationType,
                font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        switch (printType){
            case 1:
            case 4:
            case 5:
                if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))){
                    print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.pre_auth_amount)+":"+
                                    DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan),
                            font_Size_min, 1, false, (float) 0, false, false);
                    Currently_high += print_start_y;
                    print.roll(rollPaper);

                }else{
                    long preAuthorizationMoney=0;//用来接收printType判断授权金额来自反扫、正扫
                    if (printType==1){
                        preAuthorizationMoney=orderModel.getRestAmount();//反扫
                    }else if (printType==5){
                        preAuthorizationMoney=orderModel.getTotalFee();//正扫
                    }else if(printType==4){
                        preAuthorizationMoney=orderModel.getTotalFreezeAmount();//预授权详情
                    }

                    //预授权金额
                    print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.pre_auth_amount)+":"+
                                    MainApplication.feeType + DateUtil.formatMoneyUtils(preAuthorizationMoney),
                            font_Size_min, 1, false, (float) 0, false, false);
                    Currently_high += print_start_y;
                    print.roll(rollPaper);
                }

                //二维码
                bitmap = print.encodeToBitmap(orderModel.getAuthNo(), print.QR_CODE, 200, 200);

                // 计算左边位置
                int left = 200 - 200 / 2;
                // 计算上边位置
                int top = Currently_high;
                Rect mDestRect = new Rect(left, top, left + 200, top + 200);
                print.drawImage(bitmap,new Rect(0,0,200,200),mDestRect);
                Currently_high+= 200;

                print.roll(rollPaper);

                //此二维码用于预授权收款、解冻操作
                print.drawText(print_title_start_x-10, print_start_y + Currently_high, ToastHelper.toStr(R.string.scan_to_check)  , font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);

                print.drawText(print_title_start_x2+30, print_start_y + Currently_high, ToastHelper.toStr(R.string.scan_to_check2)  , font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y+10;
                print.roll(rollPaper);

                break;
            case 2:
                if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))){
                    print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.unfreezing_amount)+":"+
                                    DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan),
                            font_Size_min, 1, false, (float) 0, false, false);
                    Currently_high += print_start_y;
                    print.roll(rollPaper);
                }else{
                    print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.unfreezing_amount)+":"+
                                    MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getMoney()),
                            font_Size_min, 1, false, (float) 0, false, false);
                    Currently_high += print_start_y;
                    print.roll(rollPaper);
                }
                //解冻将原路返回支付账户或银行卡，到账时间已第三方为准
                //解冻将原路返回支付账户或银行卡，到账时间已第三方为准
                print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.unfreeze1),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);

                print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.unfreeze2),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);

                print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.unfreeze3),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);

                print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.unfreeze4),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);

                break;
            case 3:
                //授权金额
                if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))){
                    print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.pre_auth_amount)+":"+
                                    DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan),
                            font_Size_min, 1, false, (float) 0, false, false);
                    Currently_high += print_start_y;
                    print.roll(rollPaper);

                }else{
                    print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.pre_auth_amount)+":"+
                                    MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFreezeAmount()),
                            font_Size_min, 1, false, (float) 0, false, false);
                    Currently_high += print_start_y;
                    print.roll(rollPaper);
                }
                //解冻金额
                if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))){
                    print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.unfreezing_amount)+":"+
                                    DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan),
                            font_Size_min, 1, false, (float) 0, false, false);
                    Currently_high += print_start_y;
                    print.roll(rollPaper);

                }else{
                    print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.unfreezing_amount)+":"+
                                    MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee()),
                            font_Size_min, 1, false, (float) 0, false, false);
                    Currently_high += print_start_y;
                    print.roll(rollPaper);
                }
                //解冻将原路返回支付账户或银行卡，到账时间已第三方为准
                print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.unfreeze1),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);

                print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.unfreeze2),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);

                print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.unfreeze3),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);

                print.drawText(print_content_start_x, print_start_y + Currently_high,ToastHelper.toStr(R.string.unfreeze4),
                        font_Size_min, 1, false, (float) 0, false, false);
                Currently_high += print_start_y;
                print.roll(rollPaper);
                break;
            default:
                break;
        }

        print.drawText(print_content_start_x, print_start_y + Currently_high, "-------------------------------",
                font_Size_Big, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_blue_print_time) + ":" + DateUtil.formatTime(System.currentTimeMillis()) , font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        print.drawText(print_content_start_x, print_start_y + Currently_high, ToastHelper.toStr(R.string.tx_blue_print_sign) + ":", font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);

        //恢复初始值
        Currently_high = 30;

        print.printPage();
        print.roll(rollPaper);
    }

    public static boolean isAbsoluteNullStr(String str) {
        str = deleteBlank(str);
        return str == null || str.length() == 0 || "null".equalsIgnoreCase(str);

    }


    /**
     * 去前后空格和去换行符
     *
     * @param str
     * @return
     */
    public static   String deleteBlank(String str) {
        if (null != str && 0 < str.trim().length()) {
            char[] array = str.toCharArray();
            int start = 0, end = array.length - 1;
            while (array[start] == ' ') start++;
            while (array[end] == ' ') end--;
            return str.substring(start, end + 1).replaceAll("\n", "");

        } else {
            return "";
        }

    }




}
