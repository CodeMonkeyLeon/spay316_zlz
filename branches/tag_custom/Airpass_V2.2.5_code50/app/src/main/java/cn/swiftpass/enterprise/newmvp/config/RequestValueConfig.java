package cn.swiftpass.enterprise.newmvp.config;

/**
 * 创建人：caoxiaoya
 * 时间：2019/4/10
 * 描述：请求参数所有的Value,Value是可以可变参数
 * 备注：只用于存放静态变量
 */
public class RequestValueConfig {

    /**
     * 密钥标识
     */
    public static String SKEY = "";
    /**
     * 私钥
     */
    public static String PRIKEY = "";
    /**
     * 公钥
     */
    public static String SERPUBKEY = "";
    public static String CLIENT = "SPAY_AND";
}
