package cn.swiftpass.enterprise.newmvp.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trello.rxlifecycle2.components.support.RxFragment;

import cn.swiftpass.enterprise.newmvp.base.mvp.BasePresenter;

public abstract class BaseFragment<P extends BasePresenter> extends RxFragment {

    protected Activity mActivity;
    /**
     * 根布局视图
     */
    private View mContentView;
    protected P mPresenter;

    /**
     * 返回页面布局id
     * @return
     */
    protected abstract int getContentLayout();

    /**
     * 做视图相关的初始化工作
     */
    protected abstract void initView();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (Activity) context;
    }

    @SuppressLint("RestrictedApi")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mContentView == null) {
            try {
                mContentView = inflater.inflate(getContentLayout(), container, false);
                initView();
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }
        }
        return mContentView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.detachView();
            mPresenter = null;
        }

    }
}
