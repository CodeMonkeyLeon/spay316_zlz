package cn.swiftpass.enterprise.demo;

import java.lang.ref.WeakReference;

import cn.swiftpass.enterprise.base.mvp.BaseView;
import cn.swiftpass.enterprise.base.mvp.Call;

/**
 * Created by YZX on 2019年04月18日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */
public class LifecycleMVPResultCallback<T> implements Call<T> {

    private WeakReference<BaseView> mLifeCycleDependence;
    private Call<T> mCall;
    private boolean isEnableLoading;

    public LifecycleMVPResultCallback(BaseView lifeCycleDependence, Call<T> call) {
        this(lifeCycleDependence, call, true);
    }

    public LifecycleMVPResultCallback(BaseView lifeCycleDependence, Call<T> call, boolean isEnableLoading) {
        mLifeCycleDependence = new WeakReference<>(lifeCycleDependence);
        mCall = call;
        this.isEnableLoading = isEnableLoading;
        if (isEnableLoading) {
            lifeCycleDependence.showLoadingDialog();
        }
    }

    @Override
    public void onSuccess(T data) {
        BaseView baseView = mLifeCycleDependence.get();
        if (baseView == null) {
            return;
        }
        if (isEnableLoading) {
            baseView.dismissLoadingDialog();
        }
        if (mCall != null) {
            mCall.onSuccess(data);
        }
    }

    @Override
    public void onError(int fromPersenter, int code, String errorMsg) {
        BaseView baseView = mLifeCycleDependence.get();
        if (baseView == null) {
            return;
        }
        if (isEnableLoading) {
            baseView.dismissLoadingDialog();
        }
        baseView.showLoadingDialog();
    }
}
