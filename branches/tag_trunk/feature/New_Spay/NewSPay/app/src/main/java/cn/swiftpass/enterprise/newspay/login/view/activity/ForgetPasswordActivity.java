package cn.swiftpass.enterprise.newspay.login.view.activity;


import android.os.Bundle;
import android.view.View;

import butterknife.BindView;
import cn.swiftpass.enterprise.R;
import cn.swiftpass.enterprise.base.BaseActivity;
import cn.swiftpass.enterprise.widget.Toolbar;

public class ForgetPasswordActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    public int getLayoutId() {
        return R.layout.activity_forget_password;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        showLoadingDialog();
        mToolbar.setLeftTitle(null, R.mipmap.toolbar_back);
        mToolbar.setLeftTitleVisibility(View.VISIBLE);
        mToolbar.setOnToolBarClickListener(new Toolbar.OnToolBarClickListener() {
            @Override
            public void onLeftListener() {
                finish();
            }

            @Override
            public void onRightListener() {

            }
        });
    }
}
