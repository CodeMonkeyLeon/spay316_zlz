/*
 * 文 件 名:  MyX509TrustManager.java
 * 版    权:  copyright: 2013 Pactera. All rights reserved.
 * 描    述:  <描述>
 * 修 改 人:  jiaohongyun
 * 修改时间:  2013年11月11日
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.utils;

import android.util.Log;

import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/**
 * <一句话功能简述> <功能详细描述>
 * 
 * @author jiaohongyun
 * @version [版本号, 2013年11月11日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class MyX509TrustManager implements X509TrustManager
{

    /*
     * The default X509TrustManager returned by SunX509. We'll delegate
     * decisions to it, and fall back to the logic in this class if the default
     * X509TrustManager doesn't trust it.
     */
    X509TrustManager myJSSEX509TrustManager;

    public MyX509TrustManager()
    {
        try{
            KeyStore ks = KeyStore.getInstance("BKS");
            // ks.load(new FileInputStream("trustedCerts"),
            // "passphrase".toCharArray()); //----> 这是加载自己的数字签名证书文件和密码，在这里这里没有，所以不需要
            TrustManagerFactory tmf = TrustManagerFactory.getInstance("X509");
            tmf.init(ks);
            TrustManager tms[] = tmf.getTrustManagers();
            for (int i = 0; i < tms.length; i++)
            {
                if (tms[i] instanceof X509TrustManager)
                {
                    myJSSEX509TrustManager = (X509TrustManager) tms[i];
                    return;
                }
            }
        }catch (Exception e){
            Log.e("MyX509TrustManager", Log.getStackTraceString(e));
        }
    }

    /*
     * Delegate to the default trust manager.
     */
    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException
    {
        // try
        // {
        // myJSSEX509TrustManager.checkClientTrusted(chain, authType);
        // }
        // catch (CertificateException excep)
        // {
        // // do any special handling here, or rethrow exception.
        // }
    }

    /*
     * Delegate to the default trust manager.
     */
    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException
    {
        // try
        // {
        // sunJSSEX509TrustManager.checkServerTrusted(chain, authType);
        // }
        // catch (CertificateException excep)
        // {
        // /*
        // * Possibly pop up a dialog box asking whether to trust the cert
        // * chain.
        // */
        // }
    }

    /*
     * Merely pass this through.
     */
    public X509Certificate[] getAcceptedIssuers()
    {
        // return sunJSSEX509TrustManager.getAcceptedIssuers();
        return null;
    }

}
