package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @穿过长满冰霜的玻璃
 * @晨光已铺满大地
 * @万盏灯火结束了工作
 * @明亮的太阳正在升起。
 */
class RefundOrderDetailsContract {

    interface View : BaseView {

        fun regisRefundSuccess(response: Boolean)


        fun regisRefundFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {

        fun regisRefund(
            orderNo: String?,
            money: Long,
            refundMoney: Long
        )

    }


}