package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.CustAndProdSelectContract
import cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkCustomerModel
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkProductModel
import cn.swiftpass.enterprise.utils.JsonUtil
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/6
 *
 * @前尘隔海
 * @古屋不再
 */
class CustAndProdSelectPresenter : CustAndProdSelectContract.Presenter {

    private var mView: CustAndProdSelectContract.View? = null


    override fun getProductList(
        pageNumber: String?,
        goodsId: String?,
        goodsName: String?,
        goodsCode: String?
    ) {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
            AppClient.getProductList(
                "1",
                goodsId,
                goodsName,
                goodsCode,
                pageNumber,
                PaymentLinkConstant.DEFAULT_PAGE_SIZE.toString(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<PaymentLinkProductModel>(
                    PaymentLinkProductModel()
                ) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.getProductListFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果orderRelateGoodsList值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"data\":\"\"",
                                "\"data\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                PaymentLinkProductModel::class.java
                            ) as PaymentLinkProductModel
                            view.getProductListSuccess(order)
                        }
                    }
                }
            )

        }
    }

    override fun getCustomerList(
        pageNumber: String?,
        custName: String?,
        custMobile: String?,
        custEmail: String?
    ) {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
            AppClient.getCustomerList(
                "1",
                custName,
                custMobile,
                custEmail,
                pageNumber,
                PaymentLinkConstant.DEFAULT_PAGE_SIZE.toString(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<PaymentLinkCustomerModel>(
                    PaymentLinkCustomerModel()
                ) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.getCustomerListFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果orderRelateGoodsList值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"data\":\"\"",
                                "\"data\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                PaymentLinkCustomerModel::class.java
                            ) as PaymentLinkCustomerModel
                            view.getCustomerListSuccess(order)
                        }
                    }
                }
            )

        }
    }

    override fun attachView(view: CustAndProdSelectContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}