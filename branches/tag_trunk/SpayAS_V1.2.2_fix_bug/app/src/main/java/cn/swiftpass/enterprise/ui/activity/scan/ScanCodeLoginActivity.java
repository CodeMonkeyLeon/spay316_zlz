/*
 * 文 件 名:  ScanCodeLoginActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-10-25
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.scan;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 扫一扫pc二维码登录
 * 
 * @author  he_hui
 * @version  [版本号, 2016-10-25]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class ScanCodeLoginActivity extends TemplateActivity
{
    private String code = null;
    
    public static void startActivity(Context context, String code)
    {
        Intent it = new Intent();
        it.setClass(context, ScanCodeLoginActivity.class);
        it.putExtra("code", code);
        context.startActivity(it);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_login);
        code = getIntent().getStringExtra("code");
        
        getViewById(R.id.but_login).setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (StringUtil.isEmptyOrNull(code))
                {
                    return;
                }
                LocalAccountManager.getInstance().scanLogin(code, new UINotifyListener<Boolean>()
                {
                    @Override
                    public void onError(Object object)
                    {
                        super.onError(object);
                        dissDialog();
                        if (checkSession())
                        {
                            return;
                        }
                        
                        if (object != null)
                        {
                            toastDialog(ScanCodeLoginActivity.this, object.toString(), null);
                        }
                    }
                    
                    @Override
                    public void onPostExecute()
                    {
                        super.onPostExecute();
                    }
                    
                    @Override
                    public void onPreExecute()
                    {
                        // TODO Auto-generated method stub
                        super.onPreExecute();
                        showLoading(false, getString(R.string.loading));
                    }
                    
                    @Override
                    public void onSucceed(Boolean result)
                    {
                        super.onSucceed(result);
                        dissDialog();
                        if (result)
                        {
                            toastDialog(ScanCodeLoginActivity.this,
                                R.string.tv_pc_login_succ,
                                new NewDialogInfo.HandleBtn()
                                {
                                    
                                    @Override
                                    public void handleOkBtn()
                                    {
                                        finish();
                                    }
                                    
                                });
                        }
                    }
                });
            }
        });
        
        getViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
    }
}
