package cn.swiftpass.enterprise.utils;

import android.content.Context;

import java.io.File;

/**
 * 
 * 清除应用程序缓存
 * <功能详细描述>
 * 
 * @author  hehui
 * @version  [版本号, 2013-8-13]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class CleanManager
{
    
    /**
     *  清除本应用SharedPreference(/data/data/com.xxx.xxx/shared_prefs)
     * <功能详细描述>
     * @param context
     * @see [类、类#方法、类#成员]
     */
    public static void cleanSharedPreference(Context context, String fileName)
    {
        deleteFilesByDirectory(new File("/data/data/" + context.getPackageName() + "/shared_prefs" + "/" + fileName
            + ".xml"));
    }
    
    /**
     * 清除本应用内部缓存(/data/data/com.xxx.xxx/cache) 
     * <功能详细描述>
     * @param context
     * @see [类、类#方法、类#成员]
     */
    
    public static boolean cleanInternalCache(Context context)
    {
        return deleteFilesByDirectory(context.getCacheDir());
        
    }
    
    /**
     * 删除方法 这里只会删除某个文件夹下的文件，如果传入的directory是个文件，将不做处理 
     * <功能详细描述>
     * @param directory
     * @see [类、类#方法、类#成员]
     */
    private static boolean deleteFilesByDirectory(File directory) {
        if (directory != null && directory.exists()) {
            if (directory.isFile()) {
                directory.delete();
                return true;
            } else if (directory.isDirectory()) { // 否则如果它是一个目录
                for (File item : directory.listFiles()) {
                    if (!item.delete()) {
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        }
        return false;

    }
    
}