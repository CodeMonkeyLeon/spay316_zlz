/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.WelcomeActivity;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 弹出提示框
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-11]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class NewDialogInfo extends Dialog
{
    
    private Context context;
    
    private TextView title;
    
    private TextView content, content1, content2;
    
    private Button btnOk;
    
    private TextView btnCancel;
    
    private ViewGroup mRootView;
    
    private NewDialogInfo.HandleBtn handleBtn;
    
    private NewDialogInfo.HandleBtnCancle cancleBtn;
    
    private View line_img, line_img_bt;
    
    // 更多
    public static final int FLAG = 0;
    
    // 最终提交
    public static final int SUBMIT = 1;
    
    // 注册成功
    public static final int REGISTFLAG = 2;
    
    // 微信支付 是否授权激活
    public static final int EXITAUTH = 3;
    
    // 微信支付 授权激活 重新登录
    public static final int EXITAUTHLOGIN = 4;
    
    //提交商户信息
    public static final int SUBMITSHOPINFO = 5;
    
    //判断网络连接状态
    public static final int NETWORKSTATUE = 8;
    
    //优惠卷列表，长按提示删除提示框
    public static final int SUBMIT_DEL_COUPON_INFO = 6;
    
    //优惠卷发布
    public static final int SUBMIT_COUPON_INFO = 7;
    
    public static final int SUBMIT_SHOP = 9;
    
    public static final int SCAN_PAY = 10;
    
    public static final int VARD = 11;
    
    public static final int UNIFED_DIALOG = 12;
    
    public static final int WALLRT_PHONE_PROMT = 13;
    
    public static final int WALLRT_PHONE_IMAGE = 14;
    
    private OnItemLongDelListener mDelListener;
    
    private int position;
    
    private OnSubmitCouponListener mOnSubmitCouponListener;
    
    private TextView tv_title;
    
    /**
     * title 
     * content 提示内容
     * <默认构造函数>
     */
    public NewDialogInfo(Context context, String contentStr, String btnOkStr, int flagMore, String tv_title,
        NewDialogInfo.HandleBtn handleBtn)
    {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        if (flagMore == WALLRT_PHONE_PROMT)
        {
            mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.wallet_dialog_phone_promt, null);
        }
        else if (flagMore == WALLRT_PHONE_IMAGE)
        {
            mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.wallet_dialog_uploadimage_promt, null);
        }
        else
        {
            mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.new_dialog_info, null);
        }
        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);
        
        this.context = context;
        if (null != handleBtn)
        {
            this.handleBtn = handleBtn;
        }
        initView(contentStr, btnOkStr, flagMore, tv_title);
        
        setLinster(flagMore);
        
    }
    
    public void setBtnOkText(String string)
    {
        if (btnOk != null)
        {
            btnOk.setText(string);
        }
    }
    
    public void setMessage(String msg)
    {
        this.content.setText(msg);
    }
    
    public void setmDelListener(OnItemLongDelListener mDelListener, int position)
    {
        this.mDelListener = mDelListener;
        this.position = position;
    }
    
    public void setmOnSubmitCouponListener(OnSubmitCouponListener mOnSubmitCouponListener)
    {
        this.mOnSubmitCouponListener = mOnSubmitCouponListener;
    }
    
    /**
     * 设置监听
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void setLinster(final int flagMore)
    {
        
        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                dismiss();
                switch (flagMore)
                {
                    case SUBMIT_COUPON_INFO:
                        mOnSubmitCouponListener.onSubmitCouponListenerCancel();
                        break;
                    case EXITAUTH:
                        cancleBtn.handleCancleBtn();
                        break;
                }
            }
            
        });
        btnOk.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                dismiss();
                switch (flagMore)
                {
                    case WALLRT_PHONE_IMAGE:
                        handleBtn.handleOkBtn();
                        cancel();
                        break;
                    
                    case UNIFED_DIALOG:
                        handleBtn.handleOkBtn();
                        cancel();
                        break;
                    // 注册欢迎提示
                    case REGISTFLAG:
                        if (null != handleBtn)
                        {
                            handleBtn.handleOkBtn();
                        }
                        cancel();
                        break;
                    
                    // 是否授权弹出框
                    case EXITAUTH: // 调转到服务热线
                        handleBtn.handleOkBtn();
                        cancel();
                        break;
                    
                    // 是否授权弹出框
                    case SUBMIT_SHOP: // 调转到服务热线
                        handleBtn.handleOkBtn();
                        cancel();
                        break;
                    
                    case SUBMITSHOPINFO:
                    case NETWORKSTATUE:
                    case FLAG:
                        cancel();
                        ((Activity)context).finish();
                        break;
                    
                    case EXITAUTHLOGIN: // 激活从新登录
                        Intent login_intent = new Intent(context, WelcomeActivity.class);
                        context.startActivity(login_intent);
                        Activity activity = (Activity)context;
                        activity.finish();
                        break;
                    case SUBMIT_DEL_COUPON_INFO:
                        mDelListener.onItemLongDelMessage(position);
                        break;
                    case SUBMIT_COUPON_INFO:
                        mOnSubmitCouponListener.onSubmitCouponListenerOk();
                        break;
                    case VARD:
                        handleBtn.handleOkBtn();
                        break;
                }
            }
        });
    }
    
    /**
     * 初始化
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView(String contentStr, String btnOkStr, int flag, String title)
    {
        tv_title = (TextView)findViewById(R.id.tv_title);
        content = (TextView)findViewById(R.id.content);
        content1 = (TextView)findViewById(R.id.tv_content2);
        content2 = (TextView)findViewById(R.id.tv_content3);
        btnOk = (Button)findViewById(R.id.btnOk);
        
        btnCancel = (TextView)findViewById(R.id.btnCancel);
        
        line_img = (View)findViewById(R.id.line_img);
        
        line_img_bt = (View)findViewById(R.id.line_img_bt);
        if (!StringUtil.isEmptyOrNull(btnOkStr))
        {
            btnOk.setText(btnOkStr);
        }
        if (!StringUtil.isEmptyOrNull(contentStr))
        {
            content.setText(contentStr);
        }
        //        else
        //        {
        //            content.setText("");
        //        }
        if (flag == 1)
        {
            //            content1.setVisibility(View.VISIBLE);
            //            content2.setVisibility(View.VISIBLE);
        }
        if (!StringUtil.isEmptyOrNull(title))
        {
            tv_title.setVisibility(View.VISIBLE);
            tv_title.setText(title);
        }
    }
    
    public void showPage(Class clazz)
    {
        Intent intent = new Intent(context, clazz);
        context.startActivity(intent);
    }
    
    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作  
     * @author Administrator  
     *
     */
    public interface HandleBtn
    {
        void handleOkBtn();
    }
    
    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作  
     * @author Administrator  
     *
     */
    public interface HandleBtnCancle
    {
        void handleCancleBtn();
    }
    
    /***
     * 
     * 长按删除优惠
     * 
     * @version  [2.1.0, 2014-4-23]
     */
    public interface OnItemLongDelListener
    {
        public void onItemLongDelMessage(int position);
    }
    
    /***
     * 
     * 发布和修改优惠价提示
     * @author  wang_lin
     * @version  [2.1.0, 2014-4-23]
     */
    public interface OnSubmitCouponListener
    {
        public void onSubmitCouponListenerOk();
        
        public void onSubmitCouponListenerCancel();
    }
}
