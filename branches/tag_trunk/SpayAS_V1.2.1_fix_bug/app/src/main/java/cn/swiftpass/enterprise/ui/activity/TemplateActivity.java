package cn.swiftpass.enterprise.ui.activity;

import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

public abstract class TemplateActivity extends BaseActivity
{
    protected TitleBar titleBar;
    
    protected LinearLayout bottomBar;
    
    protected ViewGroup content;
    
    /**
     * 可配置设置
     */
    protected void setButBgAndFont(Button b)
    {
        DynModel dynModel = (DynModel)SharedPreUtile.readProduct("dynModel" + ApiConstant.bankCode);
        if (null != dynModel)
        {
            try
            {
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonColor()))
                {
                    b.setBackgroundColor(Color.parseColor(dynModel.getButtonColor()));
                }
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonFontColor()))
                {
                    b.setTextColor(Color.parseColor(dynModel.getButtonFontColor()));
                }
            }
            catch (Exception e)
            {
                Log.e("hehui", "TemplateActivity setContentView " + e);
            }
        }
    }
    
    @Override
    public void setContentView(int layoutResID)
    {
        super.setContentView(R.layout.activity_template);
        
        titleBar = getViewById(R.id.titleBar);
        bottomBar = getViewById(R.id.bottomBar);
        content = getViewById(R.id.containerLay);
        
        View view = getLayoutInflater().inflate(layoutResID, null);
        
        content.addView(view, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT));
        setupBottomBar();
        setupTitleBar();
        setOnCentreTitleBarClickListener();
        //定制化版本 初始化
        DynModel dynModel = (DynModel)SharedPreUtile.readProduct("dynModel" + ApiConstant.bankCode);
        if (null != dynModel)
        {
            try
            {
                if (!this.getComponentName().getClassName().equals("cn.swiftpass.enterprise.ui.activity.PayActivity"))
                {
                    if (!StringUtil.isEmptyOrNull(dynModel.getHeaderColor()))
                    {
                        titleBar.setTiteBackgroundColor(dynModel.getHeaderColor());
                    }
                    if (!StringUtil.isEmptyOrNull(dynModel.getHeaderFontColor()))
                    {
                        titleBar.setTiteBarForntColor(dynModel.getHeaderFontColor());
                    }
                }
                
            }
            catch (Exception e)
            {
                Log.e("hehui", "TemplateActivity setContentView " + e);
            }
            if (this.getComponentName()
                .getClassName()
                .equals("cn.swiftpass.enterprise.ui.activity.scan.CaptureActivity"))
            {
                Log.i("hehui", "titleBar.setTiteBackgroundColor");
                titleBar.setTiteBackgroundColor("#00000000");
            }
            
        }
    }
    
    protected void showBackground(boolean show)
    {
        if (show)
        {
            // findViewById(R.id.imgBg).setVisibility(View.VISIBLE);
        }
        else
        {
            
        }
    }
    
    protected void showTitleBar(boolean show)
    {
        if (show)
        {
            titleBar.setVisibility(View.VISIBLE);
        }
        else
        {
            titleBar.setVisibility(View.GONE);
        }
    }
    
    protected void setOnCentreTitleBarClickListener()
    {
        titleBar.setOnCentreTitleBarClickListener(new TitleBar.OnCentreTitleBarClickListener()
        {
            
            @Override
            public void onCentreButtonClick(View v)
            {
            }
            
            @Override
            public void onTitleRepotLeftClick()
            {
                // TODO Auto-generated method stub
                
            }
            
            @Override
            public void onTitleRepotRigthClick()
            {
                // TODO Auto-generated method stub
                
            }
        });
    }
    
    protected void setupTitleBar()
    {
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                
            }
        });
    }
    
    protected void setupBottomBar()
    {
        
    }
    
}
