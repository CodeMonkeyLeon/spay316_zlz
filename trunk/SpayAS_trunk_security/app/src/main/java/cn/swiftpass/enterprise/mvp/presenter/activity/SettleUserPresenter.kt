package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.SettleModel
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.SettleUserContract
import cn.swiftpass.enterprise.utils.JsonUtil
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @多想爱
 * @走累了
 * @走进深秋
 * @寺院间泛滥的落叶
 * @把我覆盖
 */
class SettleUserPresenter : SettleUserContract.Presenter {


    private var mView: SettleUserContract.View? = null


    override fun getOrderTotal(startTime: String?, endTime: String?, mobile: String?) {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)

            AppClient.getOrderTotal(
                MainApplication.getInstance().getMchId(),
                startTime,
                endTime,
                mobile.toString(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<SettleModel>(SettleModel()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.getOrderTotalFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val settle = JsonUtil.jsonToBean(
                                response.message,
                                SettleModel::class.java
                            ) as SettleModel
                            view.getOrderTotalSuccess(settle)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: SettleUserContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}