package cn.swiftpass.enterprise.ui.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;

/***
 * 退款对话框
 */
public class RefundDialog extends Dialog implements View.OnClickListener
{
    private ViewGroup mRootView;
    
    private TextView btnOk, btnCancel;
    
    private ConfirmListener btnListener;
    
    private TextView etMeoney, tvMoney;
    
    double m;
    
    long toteFeel = 0;
    
    public RefundDialog(Context context, String title, long money, long ms, ConfirmListener btnListener)
    {
        super(context);
        this.btnListener = btnListener;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //  getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_refund, null);
        setContentView(R.layout.dialog_refund);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        this.setCanceledOnTouchOutside(false);
        etMeoney = (TextView)findViewById(R.id.edit_input);
        m = money / 100d;
        etMeoney.setText(MainApplication.getFeeFh() + m);
        tvMoney = (TextView)findViewById(R.id.tvMoney);
        tvMoney.setText(MainApplication.getFeeFh() + (ms / 100d));
        TextView tvTitle = (TextView)findViewById(R.id.title);
        toteFeel = money;
        if (title != null)
        {
            tvTitle.setText(title);
        }
        
        btnOk = (TextView)findViewById(R.id.ok);
        btnCancel = (TextView)findViewById(R.id.cancel);
        
        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }
    
    public static void show(Context context, String title, int money, int ms, ConfirmListener btnListener)
    {
        RefundDialog dia = new RefundDialog(context, title, money, ms, btnListener);
        dia.show();
    }
    
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.ok:
                if (btnListener != null)
                {
                    btnListener.ok(toteFeel);
                }
                break;
            case R.id.cancel:
                if (btnListener != null)
                {
                    btnListener.cancel();
                }
                break;
            default:
                break;
        }
        dismiss();
    }
    
    public interface ConfirmListener
    {
        public void ok(long money);
        
        public void cancel();
    }
}
