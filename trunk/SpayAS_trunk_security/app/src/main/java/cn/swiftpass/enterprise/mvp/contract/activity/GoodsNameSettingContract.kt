package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.GoodsMode
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @那些因为缘分而来的东西
 * @终有缘尽而别的时候
 */
class GoodsNameSettingContract {


    interface View : BaseView {

        fun queryBodySuccess(response: GoodsMode?)

        fun queryBodyFailed(error: Any?)


        fun updateOrAddBodySuccess(
            response: Boolean,
            body: String?
        )

        fun updateOrAddBodyFailed(error: Any?)

    }


    interface Presenter : BasePresenter<View> {

        fun updateOrAddBody(
            body: String?,
            count: String?
        )


        fun queryBody()
    }


}