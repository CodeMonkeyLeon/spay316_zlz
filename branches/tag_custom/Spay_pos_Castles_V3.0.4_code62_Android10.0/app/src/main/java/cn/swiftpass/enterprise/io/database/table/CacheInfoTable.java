package cn.swiftpass.enterprise.io.database.table;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-11-18
 * Time: 下午6:29
 * To change this template use File | Settings | File Templates.
 */
public class CacheInfoTable extends TableBase{
    public static final String TABLE_NAME="tb_cache";
    public static final String COLUMN_TYPE="type";
    public static final String COLUMN_ADDTIME="add_time";
    public static final String COLUMN_UID="uId";
    @Override
    public void createTable(SQLiteDatabase db) {
        final String SQL_CREATE_TABLE = "create table "+TABLE_NAME+"("
                + COLUMN_ID + " INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT  ,"
                + COLUMN_TYPE + " INT, "
                + COLUMN_ADDTIME + " LONG, "
                + COLUMN_UID + " LONG "
                +")";
        db.execSQL(SQL_CREATE_TABLE);
    }

}
