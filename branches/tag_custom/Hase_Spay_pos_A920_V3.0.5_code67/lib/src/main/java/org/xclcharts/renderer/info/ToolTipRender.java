package org.xclcharts.renderer.info;

import android.graphics.Canvas;

/**
 * @ClassName ToolTip
 * @Description tooltip绘制类
 * @author XiongChuanLiang<br/>(xcl_168@aliyun.com)
 *  
 */
public class ToolTipRender extends ToolTip{

	public ToolTipRender()
	{
		
	}
	
	public void renderInfo(Canvas canvas) 
	{
		drawInfo(canvas);
		clear();
	}
	
}
