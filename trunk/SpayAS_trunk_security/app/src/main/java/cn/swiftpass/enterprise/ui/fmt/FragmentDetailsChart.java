package cn.swiftpass.enterprise.ui.fmt;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.bussiness.model.ReportTransactionDetailsBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;
import cn.swiftpass.enterprise.ui.activity.TransactionDetailsActivity;
import cn.swiftpass.enterprise.ui.widget.SlidingTabLayout;

/**
 * Created by aijingya on 2019/4/23.
 *
 * @Package cn.swiftpass.enterprise.ui.fmt
 * @Description: ${TODO}(报表详情的柱状图，饼状图，折线图的Fragment)
 * @date 2019/4/23.16:07.
 */
public class FragmentDetailsChart extends BaseFragment implements ViewPager.OnPageChangeListener {
    private static final String TAG = FragmentDetailsChart.class.getSimpleName();
    private TransactionDetailsActivity mActivity;

    private TextView tv_chart_title;
    private String chart_title_str;
    private String  chartType;
    private String reportType;//当前选的报表类型，日报，周报还是月报，不同类型展示不同的详情
    /*viewpager*/
    private ViewPager mViewPager ;

    /*自定义的 tabLayout*/
    private SlidingTabLayout mSlidingTabLayout ;
    /*每个 tab 的 item*/
    private List<BaseFragment> mFragments = new ArrayList<>() ;

    private ReportTransactionDetailsBean reportTransactionDetailsBean;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof TransactionDetailsActivity){
            mActivity=(TransactionDetailsActivity) activity;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details_chart, container, false);
        initViews(view);
        initData();
        return view;

    }

    public void initViews(View view){
        tv_chart_title = view.findViewById(R.id.tv_chart_title);
        if(tv_chart_title != null && !TextUtils.isEmpty(chart_title_str)){
            tv_chart_title.setText(chart_title_str);
        }

        mViewPager = view.findViewById(R.id.id_viewpager_chart);
        mSlidingTabLayout = view.findViewById(R.id.SlidingTabLayout_chart);
    }

    public void setChartTitle(String title){
        this.chart_title_str = title;
    }

    public void setChartType(String type){
        this.chartType = type;
    }

    public void setReportType(String type){
        this.reportType = type;
    }

    public void setTransactionDetails(ReportTransactionDetailsBean transactionDetails){
        this.reportTransactionDetailsBean = transactionDetails;
    }


    @Override
    protected int getLayoutId() {
        return 0;
    }


    public void initData(){
        FragmentChart ChartAmount = new FragmentChart();
        ChartAmount.setChartType(chartType);
        ChartAmount.setPageType("1");
        ChartAmount.setReportType(reportType);
        ChartAmount.setReportTransactionDetailsBean(reportTransactionDetailsBean);
        mFragments.add(ChartAmount);

        FragmentChart ChartCount = new FragmentChart();
        ChartCount.setChartType(chartType);
        ChartCount.setPageType("2");
        ChartCount.setReportType(reportType);
        ChartCount.setReportTransactionDetailsBean(reportTransactionDetailsBean);
        mFragments.add(ChartCount);

        ArrayList<String> stringArrayList = new ArrayList<String>();
        stringArrayList.add(getStringById(R.string.report_amount));
        stringArrayList.add(getStringById(R.string.report_counts));

        mViewPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager()));

        /*需要先为 viewpager 设置 adapter*/
        mSlidingTabLayout.setTitleList(stringArrayList);
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setViewPagerOnChangeListener(this);

        mViewPager.setCurrentItem(0);

    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public BaseFragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            return super.instantiateItem(container, position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        mViewPager.requestLayout();
    }

    @Override
    public void onPageSelected(int position) {

        //设置position对应的集合中的Fragment
        mViewPager.setCurrentItem(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }
}
