/*
 * 文 件 名:  WalletMainActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-1
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.wallet;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.wallet.WalletManager;
import cn.swiftpass.enterprise.bussiness.model.WalletModel;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.PullDownListView;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 电子钱包 流水
 * 
 * @author  he_hui
 * @version  [版本号, 2016-11-1]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class WalletPushStreamActivity extends TemplateActivity implements PullDownListView.OnRefreshListioner,
    PullDownListView.OnLoadDateRefreshListioner
{
    
    private PullDownListView cashier_list;
    
    private ViewHolder holder;
    
    private ListView listView;
    
    private List<WalletModel> list = new ArrayList<WalletModel>();
    
    private List<WalletModel> listUser;
    
    private Handler mHandler = new Handler();
    
    private CashierAdapter cashierAdapter;
    
    private int pageFulfil = 1;
    
    private int pageCount = 0;
    
    private TextView tv_prompt;
    
    private boolean isTag = true;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_push_record);
        
        cashier_list = (PullDownListView)findViewById(R.id.cashier_manager_id);
        listUser = new ArrayList<WalletModel>();
        cashierAdapter = new CashierAdapter(listUser);
        tv_prompt = getViewById(R.id.tv_prompt);
        cashier_list.setAutoLoadMore(true);
        cashier_list.setRefreshListioner(this);
        //        cashier_list.setOnLoadDateRefreshListioner(this);
        listView = cashier_list.mListView;
        listView.setAdapter(cashierAdapter);
        
        listView.setOnItemClickListener(new OnItemClickListener()
        {
            
            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3)
            {
                if (listUser != null && listUser.size() > 0)
                {
                    try
                    {
                        WalletModel w = listUser.get(position - 1);
                        if (null != w)
                        {
                            BlanceRecordDetailActivity.startActivity(WalletPushStreamActivity.this, w);
                        }
                        
                    }
                    catch (Exception e)
                    {
                        WalletModel w = listUser.get(position);
                        if (null != w)
                        {
                            BlanceRecordDetailActivity.startActivity(WalletPushStreamActivity.this, w);
                        }
                        Log.e("hehui", "" + e);
                    }
                }
            }
        });
        
        loadData(1, true);
        
    }
    
    /** {@inheritDoc} */
    
    @Override
    protected void onResume()
    {
        // TODO Auto-generated method stub
        super.onResume();
        
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void loadData(final int page, final boolean isLoadMore)
    {
        
        WalletManager.getInstance().ewalletList(page, new UINotifyListener<List<WalletModel>>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                if (isLoadMore)
                {
                    loadDialog(WalletPushStreamActivity.this, R.string.public_data_loading);
                }
            }
            
            @Override
            public void onPostExecute()
            {
                super.onPostExecute();
            }
            
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                dissDialog();
                if (checkSession())
                {
                    return;
                }
                if (null != object)
                {
                    WalletPushStreamActivity.this.runOnUiThread(new Runnable()
                    {
                        
                        @Override
                        public void run()
                        {
                            
                            cashier_list.setVisibility(View.GONE);
                            tv_prompt.setVisibility(View.VISIBLE);
                        }
                    });
                    toastDialog(WalletPushStreamActivity.this, object.toString(), new NewDialogInfo.HandleBtn()
                    {
                        
                        @Override
                        public void handleOkBtn()
                        {
                            if (listUser.size() == 0)
                            {
                                finish();
                            }
                        }
                        
                    });
                }
            }
            
            @Override
            public void onSucceed(List<WalletModel> model)
            {
                dissDialog();
                if (null != model && model.size() > 0)
                {
                    isTag = true;
                    list = model;
                    //                    listView.setAdapter(new CashierAdapter(result));
                    mySetListData(cashier_list, listUser, cashierAdapter, model, isLoadMore);
                    //                    if (isLoadMore)
                    //                    {
                    //                        pageFulfil += 1;
                    //                    }
                    //                    mHandler.postDelayed(new Runnable()
                    //                    {
                    //                        @Override
                    //                        public void run()
                    //                        {
                    //                            // 这里表示刷新处理完成后把上面的加载刷新界面隐藏
                    //                    cashier_list.setMore(true);
                    //                        }
                    //                    }, 800);
                    
                }
                else
                {
                    isTag = false;
                    WalletPushStreamActivity.this.runOnUiThread(new Runnable()
                    {
                        
                        @Override
                        public void run()
                        {
                            if (listUser.size() == 0)
                            {
                                
                                cashier_list.setVisibility(View.GONE);
                                tv_prompt.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                    //                    mHandler.postDelayed(new Runnable()
                    //                    {
                    //                        @Override
                    //                        public void run()
                    //                        {
                    //                            // 这里表示刷新处理完成后把上面的加载刷新界面隐藏
                    //                            if (listUser.size() == 0)
                    //                                cashier_list.setMore(false);
                    //                        }
                    //                    }, 800);
                }
            }
        });
        
    }
    
    private void mySetListData(final PullDownListView pull, List<WalletModel> list, CashierAdapter adapter,
        List<WalletModel> result, boolean isLoadMore)
    {
        if (!isLoadMore)
        {
            pull.onfinish(getStringById(R.string.refresh_successfully));// 刷新完成
            
            mHandler.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    // 这里表示刷新处理完成后把上面的加载刷新界面隐藏
                    pull.onRefreshComplete();
                }
            }, 800);
            
            list.clear();
        }
        else
        {
            mHandler.postDelayed(new Runnable()
            {
                
                @Override
                public void run()
                {
                    pull.onLoadMoreComplete();
                    // pull.setMore(true);
                }
            }, 1000);
        }
        list.addAll(result);
        adapter.notifyDataSetChanged();
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        
        titleBar.setTitle(R.string.tx_apply_balance_list);
        titleBar.setLeftButtonIsVisible(true);
        titleBar.setLeftButtonVisible(true);
        titleBar.setRightButLayVisibleForTotal(false, getString(R.string.tx_apply_balance_list));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
            }
        });
    }
    
    private class CashierAdapter extends BaseAdapter
    {
        
        private List<WalletModel> userModels;
        
        public CashierAdapter()
        {
        }
        
        public CashierAdapter(List<WalletModel> userModels)
        {
            this.userModels = userModels;
        }
        
        @Override
        public int getCount()
        {
            return userModels.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            return userModels.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(WalletPushStreamActivity.this, R.layout.blance_list_item, null);
                holder = new ViewHolder();
                holder.tv_type = (TextView)convertView.findViewById(R.id.tv_type);
                holder.tv_time = (TextView)convertView.findViewById(R.id.tv_time);
                holder.tv_money = (TextView)convertView.findViewById(R.id.tv_money);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            
            WalletModel walletModel = userModels.get(position);
            if (null != walletModel)
            {
                if (!StringUtil.isEmptyOrNull(walletModel.getTrans_type()))
                {
                    switch (Integer.parseInt(walletModel.getTrans_type()))
                    {
                        case 1:
                            holder.tv_type.setText(R.string.tv_list_type_out);
                            holder.tv_money.setTextColor(Color.parseColor(getString(R.color.user_text_color)));
                            if (!StringUtil.isEmptyOrNull(walletModel.getAmount()))
                            {
                                if (walletModel.getAmount().startsWith("-"))
                                {
                                    holder.tv_money.setText(DateUtil.formatMoneyUtils(Long.parseLong(walletModel.getAmount())));
                                    
                                }
                                else
                                {
                                    holder.tv_money.setText("-"
                                        + DateUtil.formatMoneyUtils(Long.parseLong(walletModel.getAmount())));
                                }
                            }
                            break;
                        case 2:
                            holder.tv_type.setText(R.string.tv_list_type_in);
                            holder.tv_money.setTextColor(Color.parseColor(getString(R.color.title_bg_new)));
                            if (!StringUtil.isEmptyOrNull(walletModel.getAmount()))
                            {
                                holder.tv_money.setText("+"
                                    + DateUtil.formatMoneyUtils(Long.parseLong(walletModel.getAmount())));
                            }
                            break;
                        case 3:
                            holder.tv_type.setText(R.string.tv_list_type_other);
                            break;
                    }
                }
                
                if (!StringUtil.isEmptyOrNull(walletModel.getBank_trans_time()))
                {
                    
                    try
                    {
                        holder.tv_time.setText(walletModel.getBank_trans_time());
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                
            }
            
            return convertView;
        }
    }
    
    private class ViewHolder
    {
        private TextView tv_type, tv_time, tv_money;
        
    }
    
    @Override
    public void onRefresh()
    {
        Log.i("hehui", "onRefresh()");
        pageFulfil = 1;
        loadData(pageFulfil, false);
    }
    
    @Override
    public void onLoadMore()
    {
        Log.i("hehui", "onLoadMore()");
        pageFulfil = pageFulfil + 1;
        //        cashier_list.setMore(true);
        if (isTag)
        {
            loadData(pageFulfil, true);
        }
        
    }
    
    @Override
    public void onLoadMoreDate()
    {
        Log.i("hehui", "onLoadMoreDate()");
    }
}
