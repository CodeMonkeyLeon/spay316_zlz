package cn.swiftpass.enterprise.ui.fmt;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.bussiness.model.ReportTransactionDetailsBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TransactionDetailsActivity;
import cn.swiftpass.enterprise.ui.widget.PieContentView;
import cn.swiftpass.enterprise.ui.widget.SlidingTabLayout;
import cn.swiftpass.enterprise.utils.WrapContentHeightViewPager;

/**
 * Created by aijingya on 2019/6/5.
 *
 * @Package cn.swiftpass.enterprise.ui.fmt
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2019/6/5.19:19.
 */
public class FragmentDetailsChartPie extends BaseFragment {
    private TransactionDetailsActivity mActivity;
    private TextView tv_chart_title;
    private String chart_title_str;
    /*viewpager*/
    private WrapContentHeightViewPager mViewPager ;

    /*自定义的 tabLayout*/
    private SlidingTabLayout mSlidingTabLayout ;

    private ReportTransactionDetailsBean reportTransactionDetailsBean;
    private List<View> viewLists = new ArrayList<>();

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof TransactionDetailsActivity){
            mActivity=(TransactionDetailsActivity) activity;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details_chart_pie, container, false);
        initViews(view);
        initData();
        return view;

    }

    public void initViews(View view){
        tv_chart_title = view.findViewById(R.id.tv_chart_title);
        if(tv_chart_title != null && !TextUtils.isEmpty(chart_title_str)){
            tv_chart_title.setText(chart_title_str);
        }

        mViewPager = view.findViewById(R.id.id_viewpager_chart);
        mSlidingTabLayout = view.findViewById(R.id.SlidingTabLayout_chart);
    }

    public void setChartTitle(String title){
        this.chart_title_str = title;
    }

    public void setTransactionDetails(ReportTransactionDetailsBean transactionDetails){
        this.reportTransactionDetailsBean = transactionDetails;
    }



    @Override
    protected int getLayoutId() {
        return 0;
    }


    public void initData(){
        viewLists.clear();
        PieContentView pieContentView1 = new PieContentView(mContext,reportTransactionDetailsBean,"1");
        viewLists.add(pieContentView1);

        PieContentView pieContentView2 = new PieContentView(mContext,reportTransactionDetailsBean,"2");
        viewLists.add(pieContentView2);

        ArrayList<String> stringArrayList = new ArrayList<String>();
        stringArrayList.add(getStringById(R.string.report_amount));
        stringArrayList.add(getStringById(R.string.report_counts));

        ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(viewLists);
        mViewPager.setAdapter(pagerAdapter);

        /*需要先为 viewpager 设置 adapter*/
        mSlidingTabLayout.setTitleList(stringArrayList);
        mSlidingTabLayout.setViewPager(mViewPager);
        mViewPager.setCurrentItem(0);
        mViewPager.requestLayout();

    }

    private class ViewPagerAdapter extends PagerAdapter {

        private List<View> views;
        public ViewPagerAdapter(List<View> views) {
            this.views = views;
        }

        @Override
        public int getCount() {
            return views.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view==object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view=views.get(position);
            container.addView(view);
            return  view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(views.get(position));
        }
    }
}
