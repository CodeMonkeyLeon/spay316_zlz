package cn.swiftpass.enterprise.utils.interfaces

interface OnOkClickListener {
    fun onOkClick()
}