package cn.swiftpass.enterprise.io.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Vector;

public class HttpRequester
{
    private String defaultContentEncoding;
    
    public HttpRequester()
    {
        this.defaultContentEncoding = Charset.defaultCharset().name();
    }
    
    public HttpRespons sendGet(String urlString)
        throws IOException
    {
        return this.send(urlString, "GET", null, null);
    }
    
    public HttpRespons sendGet(String urlString, Map<String, String> params)
        throws IOException
    {
        return this.send(urlString, "GET", params, null);
    }
    
    public HttpRespons sendGet(String urlString, Map<String, String> params, Map<String, String> propertys)
        throws IOException
    {
        return this.send(urlString, "GET", params, propertys);
    }
    
    public HttpRespons sendPost(String urlString)
        throws IOException
    {
        return this.send(urlString, "POST", null, null);
    }
    
    public HttpRespons sendPost(String urlString, Map<String, String> params)
        throws IOException
    {
        return this.send(urlString, "POST", params, null);
    }
    
    public HttpRespons sendPost(String urlString, Map<String, String> params, Map<String, String> propertys)
        throws IOException
    {
        return this.send(urlString, "POST", params, propertys);
    }
    
    /***
     *
     * @param urlString
     * @param method
     * @param parameters
     * @param propertys
     * @param connections
     * @param timeout 0 默认
     * @return
     * @throws IOException
     */
    public HttpRespons sendRequest(String urlString, String method, Map<String, String> parameters,
        Map<String, String> propertys, HttpURLConnection[] connections, int timeout)
        throws IOException
    {
        System.setProperty("http.keepAlive", "false");
        HttpURLConnection urlConnection = null;
        if (method.equalsIgnoreCase("GET") && parameters != null)
        {
            StringBuffer param = new StringBuffer();
            int i = 0;
            for (String key : parameters.keySet())
            {
                if (i == 0)
                    param.append("?");
                else
                    param.append("&");
                param.append(key).append("=").append(parameters.get(key));
                i++;
            }
            urlString += param;
        }
        URL url = new URL(urlString);
        urlConnection = (HttpURLConnection)url.openConnection();
        if (connections != null)
        {
            connections[0] = urlConnection;
        }
        urlConnection.setRequestMethod(method);
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);
        urlConnection.setUseCaches(false);
        if (timeout != 0)
        {
            urlConnection.setConnectTimeout(timeout);
            urlConnection.setReadTimeout(timeout);
        }
        if (propertys != null)
            for (String key : propertys.keySet())
            {
                urlConnection.addRequestProperty(key, propertys.get(key));
            }
        
        if (method.equalsIgnoreCase("POST") && parameters != null)
        {
            StringBuffer param = new StringBuffer();
            for (String key : parameters.keySet())
            {
                param.append("&");
                param.append(key).append("=").append(parameters.get(key));
            }
            urlConnection.getOutputStream().write(param.toString().getBytes());
            urlConnection.getOutputStream().flush();
        }
        
        return this.makeContent(urlString, urlConnection);
    }
    
    private HttpRespons send(String urlString, String method, Map<String, String> parameters,
        Map<String, String> propertys)
        throws IOException
    {
        HttpURLConnection urlConnection = null;
        
        if (method.equalsIgnoreCase("GET") && parameters != null)
        {
            StringBuffer param = new StringBuffer();
            int i = 0;
            for (String key : parameters.keySet())
            {
                if (i == 0)
                    param.append("?");
                else
                    param.append("&");
                param.append(key).append("=").append(parameters.get(key));
                i++;
            }
            urlString += param;
        }
        URL url = new URL(urlString);
        urlConnection = (HttpURLConnection)url.openConnection();
        
        urlConnection.setRequestMethod(method);
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);
        urlConnection.setUseCaches(false);
        
        if (propertys != null)
            for (String key : propertys.keySet())
            {
                urlConnection.addRequestProperty(key, propertys.get(key));
            }
        
        if (method.equalsIgnoreCase("POST") && parameters != null)
        {
            StringBuffer param = new StringBuffer();
            for (String key : parameters.keySet())
            {
                param.append("&");
                param.append(key).append("=").append(parameters.get(key));
            }
            urlConnection.getOutputStream().write(param.toString().getBytes());
            urlConnection.getOutputStream().flush();
            urlConnection.getOutputStream().close();
        }
        
        return this.makeContent(urlString, urlConnection);
    }
    
    private HttpRespons makeContent(String urlString, HttpURLConnection urlConnection)
        throws IOException
    {
        HttpRespons httpResponser = new HttpRespons();
        int code = urlConnection.getResponseCode();
        try
        {
            //            if (code == HttpURLConnection.HTTP_OK)
            //            {
            InputStream in = urlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
            httpResponser.contentCollection = new Vector<String>();
            StringBuffer temp = new StringBuffer();
            String line = bufferedReader.readLine();
            while (line != null)
            {
                httpResponser.contentCollection.add(line);
                temp.append(line).append("\r\n");
                line = bufferedReader.readLine();
            }
            bufferedReader.close();
            
            String ecod = urlConnection.getContentEncoding();
            if (ecod == null)
                ecod = this.defaultContentEncoding;
            
            httpResponser.urlString = urlString;
            
            httpResponser.defaultPort = urlConnection.getURL().getDefaultPort();
            httpResponser.file = urlConnection.getURL().getFile();
            httpResponser.host = urlConnection.getURL().getHost();
            httpResponser.path = urlConnection.getURL().getPath();
            httpResponser.port = urlConnection.getURL().getPort();
            httpResponser.protocol = urlConnection.getURL().getProtocol();
            httpResponser.query = urlConnection.getURL().getQuery();
            httpResponser.ref = urlConnection.getURL().getRef();
            httpResponser.userInfo = urlConnection.getURL().getUserInfo();
            
            httpResponser.content = new String(temp.toString().getBytes(), ecod);
            httpResponser.contentEncoding = ecod;
            httpResponser.code = urlConnection.getResponseCode();
            httpResponser.message = urlConnection.getResponseMessage();
            httpResponser.contentType = urlConnection.getContentType();
            httpResponser.method = urlConnection.getRequestMethod();
            httpResponser.connectTimeout = urlConnection.getConnectTimeout();
            httpResponser.readTimeout = urlConnection.getReadTimeout();
            //            }
            //            else
            //            {
            //                httpResponser.code = code;
            //            }
            return httpResponser;
        }
        catch (IOException e)
        {
            throw e;
        }
        finally
        {
            if (urlConnection != null)
                urlConnection.disconnect();
            
        }
    }
    
    public String getDefaultContentEncoding()
    {
        return this.defaultContentEncoding;
    }
    
    public void setDefaultContentEncoding(String defaultContentEncoding)
    {
        this.defaultContentEncoding = defaultContentEncoding;
    }
    
    public static void main(String[] args)
    {
        String url =
            "https://gw.tenpay.com/gateway/verifynotifyid.xml?input_charset=UTF-8"
                + "&notify_id=jZiXQZ2SX230TqkuuEWeY9HL1Yidi8TVtx1Qg7mCYvH3os-dFLdvrz4IY6hfhZWBc6nk0m91HXvfKs1T2QWbdgLtEOiWvf76"
                + "&partner=1217216001&sign=519752B269A2B407302C6B0036BD4DEC";
        HttpRequester request = new HttpRequester();
        try
        {
            request.setDefaultContentEncoding("UTF-8");
            HttpRespons response = request.sendPost(url);
            
            System.out.println(response.getContent());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}