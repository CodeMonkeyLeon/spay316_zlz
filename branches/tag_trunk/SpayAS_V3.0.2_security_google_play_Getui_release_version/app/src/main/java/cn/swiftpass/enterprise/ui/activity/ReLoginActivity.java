package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.ECDHUtils;
import cn.swiftpass.enterprise.utils.NetworkUtils;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * Created by aijingya on 2019/7/4.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(免密登录异常之后跳转的页面)
 * @date 2019/7/4.18:48.
 */
public class ReLoginActivity extends BaseActivity implements View.OnClickListener{
    private TextView tv_show_login_account;
    private Button btn_Relogin,btn_logout;
    private SharedPreferences sp;
//    private boolean isNeedDeviceLogin = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relogin);

        sp = this.getSharedPreferences("login", 0);
        initView();
        initData();
        initListener();
    }

    public void initView(){
        tv_show_login_account = findViewById(R.id.tv_show_login_account);
        btn_Relogin = findViewById(R.id.btn_Relogin);
        btn_logout = findViewById(R.id.btn_logout);

    }

    public void initListener(){
        btn_Relogin.setOnClickListener(this);
        btn_logout.setOnClickListener(this);
    }



    public void initData(){
        String Mch_id = sp.getString("user_name", "");
        tv_show_login_account.setText(Mch_id);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_Relogin:
                //不管怎么样先去调用ECDH交换接口，然后再调用设备登录接口
                if (!TextUtils.isEmpty(AppHelper.getImei(MainApplication.getContext()))) {
                    ECDHKeyExchange();
                }
                break;
            case R.id.btn_logout:
                logout();
                break;
        }
    }

    private void deviceLogin() {
        LocalAccountManager.getInstance().deviceLogin(new UINotifyListener<Boolean>() {
            @Override
            public void onError(Object object) {
                // TODO Auto-generated method stub
                super.onError(object);
                dismissLoading();
                if (MainApplication.getContext().isNeedLogin()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MainApplication.setNeedLogin(false);

                            PreferenceUtil.removeKey("login_skey");
                            PreferenceUtil.removeKey("login_sauthid");

                            Intent it = new Intent();
                            it.setClass(getApplicationContext(), WelcomeActivity.class);
                            it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(it);

                            finish();
                        }
                    });
                    return;
                } else {
                    if (object != null) {
                        if(object.toString().contains("401")){
                            //如果是会话已过期，则必须重新登录
                            PreferenceUtil.removeKey("login_skey");
                            PreferenceUtil.removeKey("login_sauthid");

                            Intent it = new Intent();
                            it.setClass(getApplicationContext(), WelcomeActivity.class);
                            it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(it);

                            finish();
                        }else{
                            toastDialog(ReLoginActivity.this, object.toString(), new NewDialogInfo.HandleBtn() {

                                @Override
                                public void handleOkBtn() {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            Intent it = new Intent();
                                            it.setClass(getApplicationContext(), WelcomeActivity.class);
                                            it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(it);

                                            finish();
                                        }
                                    });
                                }

                            });
                        }
                    }
                }
            }

            @Override
            public void onSucceed(Boolean result) {
                super.onSucceed(result);
                if (result) {
                    loginSuccToLoad();
                }
            }
        });
    }


    void loginSuccToLoad() {
       //加载保存登录用户信息
        sp.edit().putString("user_name", MainApplication.userName).commit();

        //主动做一次蓝牙连接
        try {
            connentBlue();
        } catch (Exception e) {
        }

        loadPayType();

        Intent it = new Intent();
        it.setClass(ReLoginActivity.this, spayMainTabActivity.class);
        startActivity(it);
        finish();
    }

    /**
     * 动态加载支付类型
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void loadPayType() {
        LocalAccountManager.getInstance().apiShowList(new UINotifyListener<List<DynModel>>() {
            @Override
            public void onError(Object object) {
                super.onError(object);
                if (checkSession()) {
                    return;
                }
            }

            @Override
            public void onSucceed(List<DynModel> model) {
                if (null != model && model.size() > 0) {

                    PreferenceUtil.removeKey("payTypeMd5" + ApiConstant.bankCode + MainApplication.getMchId());

                    PreferenceUtil.commitString("payTypeMd5" + ApiConstant.bankCode + MainApplication.getMchId(), model.get(0).getMd5());
                    //显示所有的支付方式
                    List<DynModel> allLst = new ArrayList<DynModel>();
                    saveAllpayList(model, allLst);
                    SharedPreUtile.saveObject(allLst, "dynallPayType" + ApiConstant.bankCode + MainApplication.getMchId());

                    //过滤支持类型
                    List<DynModel> filterLst = new ArrayList<DynModel>();
                    Map<String, String> payTypeMap = new HashMap<String, String>();
                    filterList(model, filterLst, payTypeMap);
                    SharedPreUtile.saveObject(filterLst, "dynPayType" + ApiConstant.bankCode + MainApplication.getMchId());
                    List<DynModel> filterStaticLst = new ArrayList<DynModel>();

                    //过滤已开通的支持类型
                    List<DynModel> filterList_active = new ArrayList<DynModel>();
                    Map<String, String> payTypeMap_active = new HashMap<String, String>();
                    filterList_active(model, filterList_active, payTypeMap_active);
                    SharedPreUtile.saveObject(filterList_active, "ActiveDynPayType" + ApiConstant.bankCode + MainApplication.getMchId());


                    //过滤预授权支持类型
                   /* List<DynModel> filterLst_pre_auth = new ArrayList<DynModel>();
                    Map<String, String> payTypeMap_auth = new HashMap<String, String>();
                    filterList_pre_auth(model, filterLst_pre_auth, payTypeMap_auth);
                    SharedPreUtile.saveObject(filterLst_pre_auth, "dynPayType_pre_auth" + ApiConstant.bankCode + MainApplication.getMchId());*/

                    //过滤预授权支持类型---写死在前端，写死成支付宝
                    List<DynModel> filterLst_pre_auth = new ArrayList<DynModel>();
                    Map<String, String> payTypeMap_auth = new HashMap<String, String>();
                    DynModel dynModel = new DynModel();
                    dynModel.setNativeTradeType(MainApplication.authServiceType);
                    dynModel.setApiCode("2");
                    filterLst_pre_auth.add(dynModel);
                    payTypeMap_auth.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                    SharedPreUtile.saveObject(filterLst_pre_auth, "dynPayType_pre_auth" + ApiConstant.bankCode + MainApplication.getMchId());


                    //流水过滤条件筛选
                    List<DynModel> filterLstStream = new ArrayList<DynModel>();
                    filterListStream(model, filterLstStream);
                    SharedPreUtile.saveObject(filterLstStream, "filterLstStream" + ApiConstant.bankCode + MainApplication.getMchId());

                    //过滤固定二维码支付类型
                    filterStacticList(model, filterStaticLst);
                    SharedPreUtile.saveObject(filterStaticLst, "dynPayTypeStatic" + ApiConstant.bankCode + MainApplication.getMchId());

                    Map<String, String> typePicMap = new HashMap<String, String>();
                    // 颜色map集合
                    Map<String, String> colorMap = new HashMap<String, String>();

                    Map<String, DynModel> payTypeNameMap = new HashMap<String, DynModel>();

                    for (DynModel d : model) {
                        if (!StringUtil.isEmptyOrNull(d.getSmallIconUrl())) {
                            typePicMap.put(d.getApiCode(), d.getSmallIconUrl());
                        }

                        if (!StringUtil.isEmptyOrNull(d.getColor())) {

                            colorMap.put(d.getApiCode(), d.getColor());

                        }
                        if (!StringUtil.isEmptyOrNull(d.getProviderName())) {
                            payTypeNameMap.put(d.getApiCode(), d);
                        }

                    }
                    SharedPreUtile.saveObject(payTypeNameMap, "payTypeNameMap" + ApiConstant.bankCode + MainApplication.getMchId());
                    SharedPreUtile.saveObject(payTypeMap, "payTypeMap" + ApiConstant.bankCode + MainApplication.getMchId());
                    SharedPreUtile.saveObject(payTypeMap_auth, "payTypeMap_pre_auth" + ApiConstant.bankCode + MainApplication.getMchId());
                    //图片
                    SharedPreUtile.saveObject(typePicMap, "payTypeIcon" + ApiConstant.bankCode + MainApplication.getMchId());
                    //                    //颜色值
                    SharedPreUtile.saveObject(colorMap, "payTypeColor" + ApiConstant.bankCode + MainApplication.getMchId());

                }
            }
        });
    }

    /**
     * 获取所有的支付方式
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void saveAllpayList(List<DynModel> list, List<DynModel> filterLst) {
        for (DynModel dynModel : list) {
            filterLst.add(dynModel);
        }
    }

    /**
     * 过滤支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */

    private void filterListStream(List<DynModel> list, List<DynModel> filterLst) {

        for (DynModel dynModel : list) {
            if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                for (String key : MainApplication.apiProviderMap.keySet()) {
                    if (key.equals(dynModel.getApiCode())) {
                        filterLst.add(dynModel);
                        break;
                    }

                }
            }
        }
    }

    /**
     * 过滤固定二维码支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void filterStacticList(List<DynModel> list, List<DynModel> filterLst) {

        if (!StringUtil.isEmptyOrNull(MainApplication.serviceType)) {
            String[] arrPays = MainApplication.serviceType.split("\\|");

            for (DynModel dynModel : list) {
                if (!StringUtil.isEmptyOrNull(dynModel.getFixedCodeTradeType())) {
                    for (String key : MainApplication.apiProviderMap.keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if (dynModel.getFixedCodeTradeType().contains(",")) {
                                    String[] arrFixedCodeTradeType = dynModel.getFixedCodeTradeType().split(",");
                                    for (int j = 0; j < arrFixedCodeTradeType.length; j++) {
                                        if (arrFixedCodeTradeType[j].equals(arrPays[i])) {
                                            filterLst.add(dynModel);
                                            break;
                                        }
                                    }
                                } else if (arrPays[i].equals(dynModel.getFixedCodeTradeType())) {
                                    filterLst.add(dynModel);
                                    break;
                                }

                               /* if (arrPays[i].startsWith(dynModel.getFixedCodeTradeType()) || dynModel.getFixedCodeTradeType().contains(arrPays[i]) || arrPays[i].equals(dynModel.getFixedCodeTradeType())) {
                                    filterLst.add(dynModel);
                                    break;
                                }*/
                            }
                        }
                    }

                } else {
                    continue;
                }
            }
        }

    }

    /**
     * 过滤支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void filterList(List<DynModel> list, List<DynModel> filterLst, Map<String, String> payTypeMap) {

        if (!StringUtil.isEmptyOrNull(MainApplication.serviceType)) {
            String[] arrPays = MainApplication.serviceType.split("\\|");

            for (DynModel dynModel : list) {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                    for (String key : MainApplication.apiProviderMap.keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if (dynModel.getNativeTradeType().contains(",")) {
                                    String[] arrNativeTradeType = dynModel.getNativeTradeType().split(",");
                                    for (int j = 0; j < arrNativeTradeType.length; j++) {
                                        if (arrNativeTradeType[j].equals(arrPays[i])) {
                                            filterLst.add(dynModel);
                                            payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);
                                            break;
                                        }
                                    }
                                } else if (arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    filterLst.add(dynModel);
                                    payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    break;
                                }
                                /*if (arrPays[i].startsWith(dynModel.getNativeTradeType()) || dynModel.getNativeTradeType().contains(arrPays[i]) || arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    filterLst.add(dynModel);
                                    if (dynModel.getNativeTradeType().contains(",")) {
                                        payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType().substring(0, dynModel.getNativeTradeType().lastIndexOf(",")));
                                    } else {
                                        payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    }
                                    break;
                                }*/
                            }

                        }
                    }

                } else {
                    continue;
                }
            }
        }

    }


    /**
     * 过滤支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void filterList_active(List<DynModel> list, List<DynModel> filterLst, Map<String, String> payTypeMap) {

        if (!StringUtil.isEmptyOrNull(MainApplication.activateServiceType)) {
            String[] arrPays = MainApplication.activateServiceType.split("\\|");

            for (DynModel dynModel : list) {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                    for (String key : MainApplication.apiProviderMap.keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if (dynModel.getNativeTradeType().contains(",")) {
                                    String[] arrNativeTradeType = dynModel.getNativeTradeType().split(",");
                                    for (int j = 0; j < arrNativeTradeType.length; j++) {
                                        if (arrNativeTradeType[j].equals(arrPays[i])) {
                                            filterLst.add(dynModel);
                                            payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);
                                            break;
                                        }
                                    }
                                } else if (arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    filterLst.add(dynModel);
                                    payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    break;
                                }
                                /*if (arrPays[i].startsWith(dynModel.getNativeTradeType()) || dynModel.getNativeTradeType().contains(arrPays[i]) || arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    filterLst.add(dynModel);
                                    if (dynModel.getNativeTradeType().contains(",")) {
                                        payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType().substring(0, dynModel.getNativeTradeType().lastIndexOf(",")));
                                    } else {
                                        payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    }
                                    break;
                                }*/
                            }

                        }
                    }

                } else {
                    continue;
                }
            }
        }

    }


    private void ECDHKeyExchange() {
        try {
            ECDHUtils.getInstance().getAppPubKey();
        } catch (Exception e) {
        }
        final String publicKey = SharedPreUtile.readProduct("pubKey").toString();
        final String privateKey = SharedPreUtile.readProduct("priKey").toString();

        LocalAccountManager.getInstance().ECDHKeyExchange(publicKey, new UINotifyListener<ECDHInfo>() {
            @Override
            public void onError(Object object) {
                super.onError(object);
                dismissLoading();
                if(object != null && !object.toString().contains("401")){
                    toastDialog(ReLoginActivity.this, object.toString(), null);
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showLoading(false, getString(R.string.show_login_loading));
            }

            @Override
            protected void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(ECDHInfo result) {
                super.onSucceed(result);
                if (result != null) {
                    try {
                        String secretKey = ECDHUtils.getInstance().ecdhGetShareKey(MainApplication.serPubKey, privateKey);
                        SharedPreUtile.saveObject(secretKey, "secretKey");

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //此时直接调用设备登录接口
                                deviceLogin();
                            }
                        });

                    } catch (Exception e) {
                    }
                }
            }
        });
    }


    public void logout(){
        LocalAccountManager.getInstance().Logout(new UINotifyListener<Boolean>(){
            @Override
            public void onError(Object object) {
                super.onError(object);
                dismissLoading();
                if (null != object)
                {
                    toastDialog(ReLoginActivity.this, object.toString(), null);
                    ClearAPPState();
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showLoading(false, R.string.public_loading);

            }

            @Override
            protected void onPostExecute() {
                super.onPostExecute();
                dismissLoading();
            }

            @Override
            public void onSucceed(Boolean result) {
                super.onSucceed(result);
                dismissLoading();
                ClearAPPState();
            }
        });
    }


    //APP的本地退出操作，清掉本地缓存
    public void ClearAPPState(){
        //删掉本地缓存，清除登录状态
        deleteSharedPre();

        PreferenceUtil.removeKey("login_skey");
        PreferenceUtil.removeKey("login_sauthid");
        PreferenceUtil.removeKey("choose_pay_or_pre_auth");
        PreferenceUtil.removeKey("bill_list_choose_pay_or_pre_auth");

        LocalAccountManager.getInstance().onDestory();
        SharedPreferences.Editor editor = sp.edit().remove("userPwd");
        editor.commit();

        Intent it = new Intent();
        it.setClass(getApplicationContext(), WelcomeActivity.class);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(it);

        for (Activity a : MainApplication.allActivities) {
            a.finish();
        }

        finish();
    }

}
