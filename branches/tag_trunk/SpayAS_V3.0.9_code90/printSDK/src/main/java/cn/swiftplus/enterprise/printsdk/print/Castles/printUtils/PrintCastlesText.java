package cn.swiftplus.enterprise.printsdk.print.Castles.printUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;

import CTOS.CtPrint;
import cn.swiftplus.enterprise.printsdk.print.PrintClient;


/**
 * Created by aijingya on 2020/10/30.
 *
 * @Package cn.swiftplus.enterprise.printsdk.print.Castles.printUtils
 * @Description:
 * @date 2020/10/30.14:52.
 */
public class PrintCastlesText {

    private static PrintCastlesText mPrintCastlesText;
    private CtPrint print;
    private Context mContext;
    private PrintClient.RECEIPT_TYPE mReceiptType;

    public static int font_Size_Big = 40;
    public static int font_Size_Middle = 30;
    public static int font_Size_Small = 24;
    public static int font_Size_min = 22;

    public static int print_title_start_x = 60;
    public static int print_title_start_x2 = 100;
    public static int print_content_start_x = 5;
    public static int print_content_start_x2 = 10;
    public static int print_start_y = 40;
    public static int Currently_high = 30;
    public static int rollPaper = 10;
    public static Bitmap bitmap;

    private PrintCastlesText(){
        print = new CtPrint();
    }

    public static PrintCastlesText getInstance(){
        if(mPrintCastlesText == null){
            mPrintCastlesText = new PrintCastlesText();
        }
        return mPrintCastlesText;
    }

    public void init(Context context, PrintClient.RECEIPT_TYPE receipt_type){
        this.mContext = context;
        this.mReceiptType = receipt_type;
        print.setHeatLevel(2);

        switch (receipt_type){
            case summary_receipt:
                print.initPage(1100);
                break;
            case order_receipt:
                print.initPage(1500);
                break;
            case refund_receipt:
                print.initPage(1400);
                break;
            case pre_auth_receipt:
                print.initPage(1500);
                break;
            case unfreeze_receipt:
                print.initPage(1400);
                break;
        }
    }

    public void printTitle(String text){
        int startX = 60;
        switch (mReceiptType){
            case summary_receipt:
                startX = print_title_start_x;
                break;
            case order_receipt:
                startX = print_title_start_x2;
                break;
            case refund_receipt:
                startX = print_title_start_x;
                break;
            case pre_auth_receipt:
                startX = print_title_start_x-10;
                break;
            case unfreeze_receipt:
                startX = 0;
                break;
        }

        print.drawText(startX, print_start_y,  text, font_Size_Big, 1, true, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);
    }

    public void printTextLine(String text){
        print.drawText(print_content_start_x, print_start_y + Currently_high, text, font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);
    }

    public void printMultiLines(String text){
        print.drawText(print_content_start_x, print_start_y + Currently_high, text, font_Size_min, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);
    }

    public void printEmptyLine(){
        print.roll(rollPaper);
    }

    public void printDoubleLine(){
        print.drawText(print_content_start_x, print_start_y + Currently_high, "===============================",
                font_Size_Small, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);
    }

    public void printSingleLine(){
        print.drawText(print_content_start_x, print_start_y + Currently_high, "-------------------------------",
                font_Size_Big, 1, false, (float) 0, false, false);
        Currently_high += print_start_y;
        print.roll(rollPaper);
    }

    public void printQRCode(String orderNoMch){
        //打印小票的代码
        bitmap = print.encodeToBitmap(orderNoMch, print.QR_CODE, 200, 200);

        // 计算左边位置
        int left = 200 - 200 / 2;
        // 计算上边位置
        int top = Currently_high;
        Rect mDestRect = new Rect(left, top, left + 200, top + 200);
        print.drawImage(bitmap,new Rect(0,0,200,200),mDestRect);
        Currently_high+= 200;

        print.roll(rollPaper);
    }

    /**
     *  打印图片
     */
    public void printBitmap(Bitmap logoBitmap){
        if (logoBitmap != null) {

        }
    }

    public void startPrint(){
        //恢复初始值
        Currently_high = 30;

        print.printPage();
        print.roll(rollPaper);
    }


}
