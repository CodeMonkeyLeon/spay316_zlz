/**
 * Automatically generated file. DO NOT MODIFY
 */
package cn.swiftpass.enterprise.intl.test;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "cn.swiftpass.enterprise.intl.test";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 34;
  public static final String VERSION_NAME = "2.0.0";
  // Fields from build type: debug
  public static final boolean IS_POS_VERSION = false;
}
