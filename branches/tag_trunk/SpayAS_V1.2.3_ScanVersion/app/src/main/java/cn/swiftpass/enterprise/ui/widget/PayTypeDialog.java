/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.widget;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 弹出提示框
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-11]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class PayTypeDialog extends Dialog
{
    
    private Context context;
    
    //private TextView title;
    
    private TextView content;

    //private TextView content1, content2;
    
    private Button btnOk;
    
    //private TextView btnCancel;

    private ViewGroup mRootView;
    
    private PayTypeDialog.HandleBtn handleBtn;
    
    //private PayTypeDialog.HandleBtnCancle cancleBtn;
    
    //private View line_img, line_img_bt;
    
    private PayTypeDialog.HandleItemBtn handleItemBtn;
    
    // 更多
    public static final int FLAG = 0;
    
    // 最终提交
    public static final int SUBMIT = 1;
    
    // 注册成功
    public static final int REGISTFLAG = 2;
    
    // 微信支付 是否授权激活
    public static final int EXITAUTH = 3;
    
    // 微信支付 授权激活 重新登录
    public static final int EXITAUTHLOGIN = 4;
    
    //提交商户信息
    public static final int SUBMITSHOPINFO = 5;
    
    //判断网络连接状态
    public static final int NETWORKSTATUE = 8;
    
    //优惠卷列表，长按提示删除提示框
    public static final int SUBMIT_DEL_COUPON_INFO = 6;
    
    //优惠卷发布
    public static final int SUBMIT_COUPON_INFO = 7;
    
    public static final int SUBMIT_SHOP = 9;
    
    public static final int SCAN_PAY = 10;
    
    public static final int VARD = 11;
    
    public static final int UNIFED_DIALOG = 12;
    
    public static final int WALLRT_PHONE_PROMT = 13;
    
    //private OnItemLongDelListener mDelListener;
    
    //private int position;
    
    //private OnSubmitCouponListener mOnSubmitCouponListener;
    
    //private TextView tv_title;
    
    private LinearLayout tv_close;
    
    private LinearLayout ly_jd, ly_wx, ly_qq, ly_zfb;
    
    //private TextView tv_jd, tv_wx, tv_qq, tv_zfb;
    
    String arrPays[];
    
    private List<String> payMeths = new ArrayList<String>();
    
    private View v_qq, v_wx, v_zfb;
    
    private String money;
    
    TextView tv_dia_no;
    
    private ListView lv_pay;
    
    private ViewHolder holder;
    
    private PayTypeAdape payTypeAdape;
    
    private List<DynModel> list;
    
    /**
     * title 
     * content 提示内容
     * <默认构造函数>
     */
    public PayTypeDialog(Context context, String money, PayTypeDialog.HandleBtn handleBtn,
        PayTypeDialog.HandleItemBtn handleItemBtn)
    {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        
        mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.pay_type_dialog_info, null);
        
        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);
        this.handleBtn = handleBtn;
        this.handleItemBtn = handleItemBtn;
        this.money = money;
        this.context = context;
        initView();
        
        initValue();
        setLinster();
        
    }
    
    private void initValue()
    {
        
        Object object = SharedPreUtile.readProduct("dynPayType" + ApiConstant.bankCode + MainApplication.getMchId());
        
        if (object != null)
        {
            list = (List<DynModel>)object;
            if (null != list && list.size() > 0)
            {
                payTypeAdape = new PayTypeAdape(context, list);
                lv_pay.setAdapter(payTypeAdape);
                return;
            }
        }
        
        if (!StringUtil.isEmptyOrNull(MainApplication.serviceType))
        {
            arrPays = MainApplication.serviceType.split("\\|");
            //            MainApplication.serviceType = "|pay.jdpay.native|pay.alipay.wappayv2|pay.alipay.native|pay.qq.jspay";
            //            arrPays = MainApplication.serviceType.split("\\|");
            
            for (int s = 0; s < arrPays.length; s++)
            {
                if (arrPays[s].equalsIgnoreCase(MainApplication.PAY_QQ_NATIVE)
                    || arrPays[s].equalsIgnoreCase(MainApplication.PAY_QQ_NATIVE1))
                {
                    payMeths.add(arrPays[s]);
                    if (payMeths.contains(MainApplication.PAY_QQ_NATIVE)
                        && payMeths.contains(MainApplication.PAY_QQ_NATIVE1))
                    {
                        payMeths.remove(MainApplication.PAY_QQ_NATIVE1);
                    }
                    // 手Q支付
                    ly_qq.setVisibility(View.VISIBLE);
                    v_qq.setVisibility(View.VISIBLE);
                }
                else if (arrPays[s].startsWith(MainApplication.PAY_WX_NATIVE))
                {
                    //微信支付
                    payMeths.add(arrPays[s]);
                    ly_wx.setVisibility(View.VISIBLE);
                    v_wx.setVisibility(View.VISIBLE);
                }
                else if (arrPays[s].equals(MainApplication.PAY_ZFB_NATIVE)
                    || arrPays[s].equals(MainApplication.PAY_ZFB_NATIVE1))
                {
                    //支付宝支付
                    payMeths.add(arrPays[s]);
                    if (payMeths.contains(MainApplication.PAY_ZFB_NATIVE)
                        && payMeths.contains(MainApplication.PAY_ZFB_NATIVE1))
                    {
                        payMeths.remove(MainApplication.PAY_ZFB_NATIVE);
                    }
                    ly_zfb.setVisibility(View.VISIBLE);
                    v_zfb.setVisibility(View.VISIBLE);
                }
                else if (arrPays[s].equalsIgnoreCase(MainApplication.PAY_JINGDONG_NATIVE))
                {
                    payMeths.add(arrPays[s]);
                    ly_jd.setVisibility(View.VISIBLE);
                }
            }
            
            if (payMeths.size() == 0)
            {
                tv_dia_no.setVisibility(View.VISIBLE);
            }
            else
            {
                tv_dia_no.setVisibility(View.GONE);
            }
        }
    }
    
    public void setBtnOkText(String string)
    {
        if (btnOk != null)
        {
            btnOk.setText(string);
        }
    }
    
    public void setMessage(String msg)
    {
        this.content.setText(msg);
    }
    
    public void setmDelListener(OnItemLongDelListener mDelListener, int position)
    {
//        this.mDelListener = mDelListener;
//        this.position = position;
    }
    
    public void setmOnSubmitCouponListener(OnSubmitCouponListener mOnSubmitCouponListener)
    {
        //this.mOnSubmitCouponListener = mOnSubmitCouponListener;
    }
    
    /**
     * 设置监听
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void setLinster()
    {
        lv_pay.setOnItemClickListener(new OnItemClickListener()
        {
            
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
            {
                dismiss();
                if (list.size() > 0)
                {
                    DynModel dynModel = list.get(position);
                    if (dynModel != null && !StringUtil.isEmptyOrNull(money))
                    {
                        handleItemBtn.toPay(money, dynModel.getApiCode());
                    }
                }
            }
        });
        
        //        ly_jd, ly_wx, ly_qq, ly_zfb;
        ly_wx.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                dismiss();
                if (!StringUtil.isEmptyOrNull(money))
                {
                    handleBtn.toWxPay(money, MainApplication.PAY_WX_NATIVE);
                }
            }
        });
        
        ly_qq.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                dismiss();
                if (!StringUtil.isEmptyOrNull(money))
                {
                    if (payMeths.contains(MainApplication.PAY_QQ_NATIVE))
                    {
                        handleBtn.toWxPay(money, MainApplication.PAY_QQ_NATIVE);
                        
                    }
                    else
                    {
                        handleBtn.toWxPay(money, MainApplication.PAY_QQ_NATIVE1);
                    }
                    
                }
            }
        });
        
        ly_zfb.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                dismiss();
                if (!StringUtil.isEmptyOrNull(money))
                {
                    
                    if (payMeths.contains(MainApplication.PAY_ZFB_NATIVE1))
                    {
                        handleBtn.toWxPay(money, MainApplication.PAY_ZFB_NATIVE1);
                    }
                    else
                    {
                        handleBtn.toWxPay(money, MainApplication.PAY_ZFB_NATIVE);
                    }
                }
            }
        });
        
        ly_jd.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                dismiss();
                if (!StringUtil.isEmptyOrNull(money))
                {
                    handleBtn.toWxPay(money, MainApplication.PAY_JINGDONG_NATIVE);
                }
            }
        });
        
        //        btnCancel.setOnClickListener(new View.OnClickListener()
        //        {
        //            
        //            @Override
        //            public void onClick(View v)
        //            {
        //                dismiss();
        //                switch (flagMore)
        //                {
        //                    case SUBMIT_COUPON_INFO:
        //                        mOnSubmitCouponListener.onSubmitCouponListenerCancel();
        //                        break;
        //                    case EXITAUTH:
        //                        cancleBtn.handleCancleBtn();
        //                        break;
        //                }
        //            }
        //            
        //        });
        //        btnOk.setOnClickListener(new View.OnClickListener()
        //        {
        //            
        //            @Override
        //            public void onClick(View v)
        //            {
        //                dismiss();
        //                //                switch (flagMore)
        //                //                {
        //                
        //                //                    case UNIFED_DIALOG:
        //                //                        handleBtn.handleOkBtn();
        //                //                        cancel();
        //                //                        break;
        //                //                    // 注册欢迎提示
        //                //                    case REGISTFLAG:
        //                //                        if (null != handleBtn)
        //                //                        {
        //                //                            handleBtn.handleOkBtn();
        //                //                        }
        //                //                        cancel();
        //                //                        break;
        //                //                    
        //                //                    // 是否授权弹出框
        //                //                    case EXITAUTH: // 调转到服务热线
        //                //                        handleBtn.handleOkBtn();
        //                //                        cancel();
        //                //                        break;
        //                //                    
        //                //                    // 是否授权弹出框
        //                //                    case SUBMIT_SHOP: // 调转到服务热线
        //                //                        handleBtn.handleOkBtn();
        //                //                        cancel();
        //                //                        break;
        //                //                    
        //                //                    case SUBMITSHOPINFO:
        //                //                    case NETWORKSTATUE:
        //                //                    case FLAG:
        //                //                        cancel();
        //                //                        ((Activity)context).finish();
        //                //                        break;
        //                //                    
        //                //                    case EXITAUTHLOGIN: // 激活从新登录
        //                //                        Intent login_intent = new Intent(context, WelcomeActivity.class);
        //                //                        context.startActivity(login_intent);
        //                //                        Activity activity = (Activity)context;
        //                //                        activity.finish();
        //                //                        break;
        //                //                    case SUBMIT_DEL_COUPON_INFO:
        //                //                        mDelListener.onItemLongDelMessage(position);
        //                //                        break;
        //                //                    case SUBMIT_COUPON_INFO:
        //                //                        mOnSubmitCouponListener.onSubmitCouponListenerOk();
        //                //                        break;
        //                //                    case VARD:
        //                //                        handleBtn.handleOkBtn();
        //                //                        break;
        //                //                }
        //            }
        //        });
    }
    
    /**
     * 初始化
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView()
    {
        tv_dia_no = (TextView)findViewById(R.id.tv_dia_no);
        v_qq = (View)findViewById(R.id.v_qq);
        v_wx = (View)findViewById(R.id.v_wx);
        v_zfb = (View)findViewById(R.id.v_zfb);
        
        tv_close = (LinearLayout)findViewById(R.id.tv_close);
        //        ly_jd,ly_wx,ly_qq,ly_zfb
        ly_jd = (LinearLayout)findViewById(R.id.ly_jd);
        ly_wx = (LinearLayout)findViewById(R.id.ly_wx);
        ly_qq = (LinearLayout)findViewById(R.id.ly_qq);
        ly_zfb = (LinearLayout)findViewById(R.id.ly_zfb);
        //tv_jd,tv_wx,tv_qq,tv_zfb;
        //tv_jd = (TextView)findViewById(R.id.tv_jd);
        //tv_wx = (TextView)findViewById(R.id.tv_wx);
        //tv_qq = (TextView)findViewById(R.id.tv_qq);
        //tv_zfb = (TextView)findViewById(R.id.tv_zfb);
        
        lv_pay = (ListView)findViewById(R.id.lv_pay);
        
        tv_close.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });
        
    }
    
    class PayTypeAdape extends BaseAdapter
    {
        
        private List<DynModel> list;
        
        private Context context;
        
        private PayTypeAdape(Context context, List<DynModel> list)
        {
            this.context = context;
            this.list = list;
        }
        
        @Override
        public int getCount()
        {
            return list.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            return list.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(context, R.layout.activity_paytype_list_item, null);
                holder = new ViewHolder();
                holder.v_line = (View)convertView.findViewById(R.id.v_line);
                holder.iv_image = (ImageView)convertView.findViewById(R.id.iv_image);
                holder.tv_pay_name = (TextView)convertView.findViewById(R.id.tv_pay_name);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            if ((position + 1) == list.size())
            {
                holder.v_line.setVisibility(View.GONE);
            }
            else
            {
                holder.v_line.setVisibility(View.VISIBLE);
            }
            
            DynModel dynModel = list.get(position);
            if (null != dynModel)
            {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType()))
                {
                    holder.tv_pay_name.setText(MainApplication.getPayTypeMap().get(dynModel.getApiCode()));
                    Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_general_receivables);
                    if (bitmap != null)
                    {
                        
                        MainApplication.finalBitmap.display(holder.iv_image, dynModel.getSmallIconUrl(), bitmap);
                    }
                    else
                    {
                        MainApplication.finalBitmap.display(holder.iv_image, dynModel.getSmallIconUrl());
                    }
                }
            }
            
            return convertView;
        }
        
    }
    
    class ViewHolder
    {
        private View v_line;
        
        private ImageView iv_image;
        
        TextView tv_pay_name;
    }
    
    public void showPage(Class clazz)
    {
        Intent intent = new Intent(context, clazz);
        context.startActivity(intent);
    }
    
    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作  
     * @author Administrator  
     *
     */
    public interface HandleBtn
    {
        void toWxPay(String money, String payType);
        
        void toZfbPay(String money, String payType);
        
        void toQqPay(String money, String payType);
        
        void toJdPay(String money, String payType);
    }
    
    public interface HandleItemBtn
    {
        void toPay(String money, String payType);
    }
    
    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作  
     * @author Administrator  
     *
     */
    public interface HandleBtnCancle
    {
        void handleCancleBtn();
    }
    
    /***
     * 
     * 长按删除优惠
     * 
     * @author  wang_lin
     * @version  [2.1.0, 2014-4-23]
     */
    public interface OnItemLongDelListener
    {
        public void onItemLongDelMessage(int position);
    }
    
    /***
     * 
     * 发布和修改优惠价提示
     * @author  wang_lin
     * @version  [2.1.0, 2014-4-23]
     */
    public interface OnSubmitCouponListener
    {
        public void onSubmitCouponListenerOk();
        
        public void onSubmitCouponListenerCancel();
    }
}
