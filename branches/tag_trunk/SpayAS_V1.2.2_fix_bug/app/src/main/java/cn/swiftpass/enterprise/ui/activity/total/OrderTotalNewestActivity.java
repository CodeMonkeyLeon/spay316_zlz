/*
 * 文 件 名:  OrderTotalNewestActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-9-18
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.total;

import java.util.ArrayList;
import java.util.List;

import org.xclcharts.common.DensityUtil;
import org.xclcharts.test.LineChart01View;
import org.xclcharts.test.OrderTotalModel;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.City;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.chart.RoundProgressBar;
import cn.swiftpass.enterprise.ui.viewpage.NewPage;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateTimeUtil;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.HelpUI;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 最新统计
 * 
 * @author  he_hui
 * @version  [版本号, 2016-9-18]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class OrderTotalNewestActivity extends TemplateActivity
{
    private ViewHolder holder;
    
    private NewPage page;
    
    private View[] views = new View[2];
    
    private View firstView, secondView;
    
    private LinearLayout line, line1;
    
    private HorizontalScrollView horizontalScrollView1, horizontalScrollView2;
    
    private ListView list1, list2;
    
    private List<OrderTotalModel> orderListForThirty = new ArrayList<OrderTotalModel>();
    
    boolean defaultThirty = true; // 默认30 第一次会查询，以后都不需要在次查询
    
    Handler handler = new Handler()
    {
        
        public void handleMessage(android.os.Message msg)
        {
            switch (msg.what)
            {
                case HandlerManager.PAY_TOTAL_TYPE:
                    String type = (String)msg.obj;
                    if (type.equals("0"))
                    { //实时统计
                        page.setCurrentItem(0);
                    }
                    else
                    {
                        page.setCurrentItem(1);
                    }
                    break;
                default:
                    break;
            }
        };
    };
    
    /**
     * 加载统计数据
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    void loadOrderTotal(String startTime, String endTime, final String countMethod, String userName)
    {
        OrderManager.getInstance().getNewOrderTotal(startTime,
            endTime,
            countMethod,
            userName,
            new UINotifyListener<List<OrderTotalModel>>()
            {
                @Override
                public void onError(Object object)
                {
                    dismissLoading();
                    super.onError(object);
                    if (null != object)
                    {
                        ToastHelper.showInfo(object.toString());
                    }
                }
                
                @Override
                public void onPreExecute()
                {
                    super.onPreExecute();
                    
                    showNewLoading(true, ToastHelper.toStr(R.string.public_data_loading));
                }
                
                public void onSucceed(List<OrderTotalModel> result)
                {
                    dismissLoading();
                    if (result != null && result.size() > 0)
                    {
                        if (countMethod.equals("4"))
                        { // 5分钟 画折线图
                            createLine(DateTimeUtil.getListTime(), result);
                        }
                        else
                        {// 30天画折线图
                            orderListForThirty.addAll(result);
                            defaultThirty = false;
                            createLineForThirtyDay(DateTimeUtil.getMonthThirtyDays(), result);
                        }
                    }
                };
            });
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpage_new);
        
        page = getViewById(R.id.pager);
        HandlerManager.registerHandler(HandlerManager.PAY_TOTAL_TYPE, handler);
        initView();
        
        loadOrderTotal(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00",
            DateUtil.formatTime(System.currentTimeMillis()),
            "4",
            null);
    }
    
    @Override
    protected void setOnCentreTitleBarClickListener()
    {
        super.setOnCentreTitleBarClickListener();
        titleBar.setOnCentreTitleBarClickListener(new TitleBar.OnCentreTitleBarClickListener()
        {
            @Override
            public void onCentreButtonClick(View v)
            {
                List<City> list = new ArrayList<City>();
                City city = new City();
                city.setCity(getString(R.string.tv_total_title_now));
                city.setNumber("0");
                City city1 = new City();
                city1.setCity(getString(R.string.tv_total_title_thirty));
                city1.setNumber("1");
                list.add(city);
                list.add(city1);
                HelpUI.initTotalPopWindow(OrderTotalNewestActivity.this, list, 1, v, handler);
            }
            
            @Override
            public void onTitleRepotLeftClick()
            {
                // TODO Auto-generated method stub
                
            }
            
            @Override
            public void onTitleRepotRigthClick()
            {
                // TODO Auto-generated method stub
                
            }
        });
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setTitle(R.string.tv_total_title_now);
        titleBar.setTitleImage(true);
        titleBar.setRightButLayVisible(false, null);
        titleBar.setLeftButtonIsVisible(false);
        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.tx_distribution));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                showPage(PayDistributionActivity.class);
            }
        });
    }
    
    boolean isAdd = false;
    
    /**
     * 画折线图5分钟
     * <功能详细描述>
     * @param list
     * @see [类、类#方法、类#成员]
     */
    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    private void createLine(List<String> list, List<OrderTotalModel> result)
    {
        LineChart01View lineChart01View = new LineChart01View(getApplicationContext(), list, result);
        line.removeAllViews();
        line.addView(lineChart01View);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)lineChart01View.getLayoutParams();
        params.width = DensityUtil.dip2px(OrderTotalNewestActivity.this, 750);
        params.height = DensityUtil.dip2px(OrderTotalNewestActivity.this, 220);
        horizontalScrollView1.setOverScrollMode(View.OVER_SCROLL_NEVER);
        
        horizontalScrollView1.post(new Runnable()
        {
            @Override
            public void run()
            {
                horizontalScrollView1.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        });
        
        list1.setAdapter(new OrderTotalNewestActivityAdapter(result));
    }
    
    /**
     * 画折线图30天
     * <功能详细描述>
     * @param list
     * @see [类、类#方法、类#成员]
     */
    @SuppressLint("NewApi")
    private void createLineForThirtyDay(List<String> list, List<OrderTotalModel> result)
    {
        LineChart01View lineChart01View = new LineChart01View(getApplicationContext(), list, result);
        line1.addView(lineChart01View);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)lineChart01View.getLayoutParams();
        params.width = DensityUtil.dip2px(OrderTotalNewestActivity.this, 990);
        params.height = DensityUtil.dip2px(OrderTotalNewestActivity.this, 220);
        horizontalScrollView2.setOverScrollMode(View.OVER_SCROLL_NEVER);
        
        horizontalScrollView2.post(new Runnable()
        {
            @Override
            public void run()
            {
                horizontalScrollView2.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        });
        
        list2.setAdapter(new OrderTotalNewestActivityAdapter(result));
    }
    
    @SuppressLint("NewApi")
    private void initView()
    {
        LayoutInflater inflater = LayoutInflater.from(this);
        firstView = inflater.inflate(R.layout.order_total_new_layout_now, null);
        views[0] = firstView;
        horizontalScrollView1 = (HorizontalScrollView)firstView.findViewById(R.id.horizontalScrollView1);
        line = (LinearLayout)firstView.findViewById(R.id.line);
        list1 = (ListView)firstView.findViewById(R.id.list1);
        
        secondView = inflater.inflate(R.layout.order_total_new_layout_thirty, null);
        views[1] = secondView;
        horizontalScrollView2 = (HorizontalScrollView)secondView.findViewById(R.id.horizontalScrollView2);
        line1 = (LinearLayout)secondView.findViewById(R.id.line1);
        list2 = (ListView)secondView.findViewById(R.id.list2);
        
        page.setAdapter(new MyAdapter());
        page.setCurrentItem(0);
        page.setOnPageChangeListener(new OnPageChangeListener()
        {
            
            @Override
            public void onPageSelected(int position)
            {
                switch (position)
                {
                    case 0:
                        titleBar.setTitle(R.string.tv_total_title_now);
                        loadOrderTotal(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00",
                            DateUtil.formatTime(System.currentTimeMillis()),
                            "4",
                            null);
                        break;
                    case 1:
                        if (defaultThirty)
                        {
                            loadOrderTotal(DateTimeUtil.getMothdFirst() + " 00:00:00",
                                DateUtil.formatTime(System.currentTimeMillis()),
                                "1",
                                null);
                        }
                        titleBar.setTitle(R.string.tv_total_title_thirty);
                        break;
                    default:
                        break;
                }
            }
            
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2)
            {
                // TODO Auto-generated method stub
                
            }
            
            @Override
            public void onPageScrollStateChanged(int arg0)
            {
                // TODO Auto-generated method stub
                
            }
        });
        
    }
    
    public class MyAdapter extends PagerAdapter
    {
        
        @Override
        public int getCount()
        {
            if (views.length < 3)
            {
                return views.length;
            }
            else
            {
                return Integer.MAX_VALUE;
            }
        }
        
        @Override
        public boolean isViewFromObject(View arg0, Object arg1)
        {
            return arg0 == arg1;
        }
        
        @Override
        public void destroyItem(View container, int position, Object object)
        {
            //            ((ViewPager)container).removeView(mImageViews[position % mImageViews.length]);
            
        }
        
        /**
         * 载入图片进去，用当前的position 除以 图片数组长度取余数是关键
         */
        @Override
        public Object instantiateItem(View container, int position)
        {
            try
            {
                
                ((ViewPager)container).addView(views[position % views.length], 0);
            }
            catch (Exception e)
            {
            }
            return views[position % views.length];
        }
        
        @Override
        public void finishUpdate(View arg0)
        {
            // TODO Auto-generated method stub
            
        }
        
        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1)
        {
            // TODO Auto-generated method stub
            
        }
        
        @Override
        public Parcelable saveState()
        {
            // TODO Auto-generated method stub
            return null;
        }
        
        @Override
        public void startUpdate(View arg0)
        {
            // TODO Auto-generated method stub
            
        }
        
    }
    
    private class OrderTotalNewestActivityAdapter extends BaseAdapter
    {
        
        private List<OrderTotalModel> result;
        
        private OrderTotalNewestActivityAdapter(List<OrderTotalModel> result)
        {
            this.result = result;
        }
        
        @Override
        public int getCount()
        {
            return result.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            return result.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(OrderTotalNewestActivity.this, R.layout.order_total_new_list_item, null);
                holder = new ViewHolder();
                holder.tv_total_money = (TextView)convertView.findViewById(R.id.tv_total_money);
                holder.tv_num = (TextView)convertView.findViewById(R.id.tv_num);
                holder.tv_time = (TextView)convertView.findViewById(R.id.tv_time);
                holder.tv_date_text = (TextView)convertView.findViewById(R.id.tv_date_text);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            
            OrderTotalModel orderTotalModel = result.get(position);
            if (null != orderTotalModel)
            {
                holder.tv_total_money.setText(MainApplication.getFeeFh()
                    + DateUtil.formatMoneyUtils(orderTotalModel.getSuccessFee()));
                holder.tv_num.setText(orderTotalModel.getSuccessCount() + getString(R.string.stream_cases));
                
                if (orderTotalModel.isTag())
                {
                    holder.tv_date_text.setText(R.string.public_today);
                    holder.tv_time.setText(orderTotalModel.getTime());
                }
                else
                {
                    if (!StringUtil.isEmptyOrNull(orderTotalModel.getWeek()))
                    {
                        holder.tv_date_text.setText(orderTotalModel.getWeek());
                    }
                    
                    holder.tv_time.setText(orderTotalModel.getDate());
                }
            }
            
            return convertView;
        }
    }
    
    private class ViewHolder
    {
        private TextView tv_total_money, tv_num, tv_time, tv_date_text;
        
        private RoundProgressBar roundProgressBar1;
        
    }
    
}
