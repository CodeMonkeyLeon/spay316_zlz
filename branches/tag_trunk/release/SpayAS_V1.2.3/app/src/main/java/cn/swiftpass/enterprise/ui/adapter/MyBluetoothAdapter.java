package cn.swiftpass.enterprise.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.BlueDevice;

/**
 * 蓝牙
 * User: Alan
 * Date: 14-1-20
 * Time: 下午7:33
 * To change this template use File | Settings | File Templates.
 */
public class MyBluetoothAdapter extends ArrayListAdapter<BlueDevice>
{
    public MyBluetoothAdapter(Context context)
    {
        super(context);
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder viewHolder = null;
        if (convertView == null)
        {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_bluetooth, null);
            viewHolder = new ViewHolder();
            viewHolder.tvState = (TextView)convertView.findViewById(R.id.tv_state);
            viewHolder.tvName = (TextView)convertView.findViewById(R.id.tv_name);
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        BlueDevice item = (BlueDevice)getItem(position);
        viewHolder.bindData(item);
        return convertView;
    }
    
    class ViewHolder
    {
        
        public TextView tvName;
        
        public TextView tvState;
        
        public void bindData(BlueDevice b)
        {
            tvName.setText(b.getDeviceName());
            tvState.setText(b.getBondState());
        }
    }
}
