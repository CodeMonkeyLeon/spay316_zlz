package cn.swiftpass.enterprise.http.interceptor;

import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import cn.swiftpass.enterprise.config.RequestKeyConfig;
import okhttp3.Interceptor;
import okhttp3.Response;
import okhttp3.ResponseBody;
/**
 * 创建人：caoxiaoya
 * 时间：2019/4/22
 * 描述：请求返回结果处理赋值给其他T
 * 备注：
 */

public class ResultInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());
        ResponseBody responseBody = response.body();
        if (responseBody == null) {
            return response;
        }
        String jsonResponse = responseBody.string();
        if (TextUtils.isEmpty(jsonResponse)) {
            return response;
        }
        String responseJson;
        try {
            JSONObject srcJsonObject = new JSONObject(jsonResponse);
            JSONObject descJsonObject = new JSONObject();
            //服务器返回result 200成功
            String code = srcJsonObject.getString(RequestKeyConfig.RESULT);
            String message = srcJsonObject.getString(RequestKeyConfig.MESSAGE);
            descJsonObject.put(RequestKeyConfig.CODE,code);
            if ("200".equals(code)) {
                descJsonObject.put(RequestKeyConfig.DATA,message);
            } else {
                descJsonObject.put(RequestKeyConfig.ERROR,message);
            }
            responseJson =  descJsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return response;
        }
        if (TextUtils.isEmpty(responseJson)) {
            return response;
        }

        return response.newBuilder()
                .body(ResponseBody.create(responseBody.contentType(), responseJson))
                .build();
    }
}
