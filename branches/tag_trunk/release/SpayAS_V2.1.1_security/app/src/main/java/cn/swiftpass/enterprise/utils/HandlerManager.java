/*
 * 文 件 名:  MeetingPlace.java
 * 版    权:  copyright: 2013 Pactera. All rights reserved.
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-11-19
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.utils;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-11-21]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class HandlerManager
{
    /** 日志记录常量*/
    private static final String TAG = HandlerManager.class.getSimpleName();
    
    /**
     * 用来发接收消息的Handler列表
     */
    public static HashMap<Integer, ArrayList<Handler>> systemHandler = new HashMap<Integer, ArrayList<Handler>>();
    
    /** 省 市 级 连动*/
    public static final int SHOPKEEPID = 0;
    
    /** 查看图片*/
    public static final int SHOWPIC = 1;
    
    /** 把隐藏的商户号显示出来*/
    public static final int SHOWMERCHANT = 2;
    
    /** 资料完善修改*/
    public static final int SHOPDATA = 3;
    
    /**注册成功欢迎提示*/
    public static final int REGISTERPROMT = 4;
    
    /**获取商户信息完成通知*/
    public static final int GETSHOPINFODONE = 5;
    
    // 支付类型
    public static final int PAY_TYPE = 6;
    
    // 反扫切换
    public static final int SCANCODE = 7;
    
    public static final int MAINACTIVITY = 8;
    
    public static final int SETCODEMONEY = 9;
    
    public static final int PAY_TYPE_REFUND = 10;
    
    public static final int PAY_ACTIVE_SCAN = 11;
    
    public static final int PAY_ACTIVE_SETTING_SCAN = 12;
    
    public static final int PAY_SET_PAY_METHOD = 13;
    
    public static final int PAY_PUSH_MONEY = 14;
    
    public static final int PAY_TOTAL_TYPE = 15;
    
    public static final int NOTICE_TYPE = 16;
    
    public static final int SETTLE_TYPE = 17;
    
    public static final int PAY_TOTAL_TYPE_DISTRIBUTION = 18;
    
    public static final int PAY_TOTAL_TYPE_DISTRIBUTION_USER = 19;
    
    public static final int WALLET_REFERSH = 20;
    
    public static final int WALLET_OPEN_SUCCESS = 21;
    
    public static final int BILL_CHOICE_USER = 22;
    
    public static final int BILL_CHOICE_TIME = 23;
    
    public static final int VICE_SWITCH = 24;
    
    public static final int BODY_SWITCH = 25;
    
    public static final int DEVICE_SWITCH = 26;
    
    public static final int CASH_NAME = 12;
    
    public static final int STREAM_BILL = 27;
    
    public static final int STREAM_BILL_REFUND = 28;
    
    public static final int PAY_FINISH = 29;
    
    public static final int PAY_FINISH_REFUND = 30;
    
    public static final int PAY_SWITCH_TAB = 31;
    
    public static final int PAY_DETAIL_TO_REFRESH = 32;
    
    public static final int BLUE_CONNET = 34;
    
    public static final int BLUE_CONNET_STUTS = 35;
    
    public static final int BLUE_CONNET_STUTS_CLOSED = 36;//蓝牙关闭

    public static final int CHOICE_CASHIER = 37;//从账单列表选中收银员
    /**
     * 绑定二维码成功
     */
    public static final int BIND_CODE_FINISH = 38;
    /**
     *从二维码列表选中收银员
     */
    public static final int CHOICE_CASHIER_STATIC_CODE = 39;


    /**通知店铺更新数据*/
    //    public static final int SHOPINFO = 5;
    
    /**
     * 注册用来接收消息的Handler对象
     * @param handler    用来接收业务更新的handler
     */
    public static void registerHandler(int handlerId, Handler handler)
    {
        if (systemHandler == null)
        {
            systemHandler = new HashMap<Integer, ArrayList<Handler>>();
        }
        ArrayList<Handler> arrayHandler = systemHandler.get(handlerId);
        if (null == arrayHandler)
        {
            arrayHandler = new ArrayList<Handler>();
        }
        arrayHandler.add(handler);
        systemHandler.put(handlerId, arrayHandler);
    }
    
    /**
     * 去注册用来接收消息的Handler对象
     * @param handler    用来接收业务更新的handler
     */
    public static void unregisterHandler(int handlerId, Handler handler)
    {
        if (systemHandler == null)
        {
            systemHandler = new HashMap<Integer, ArrayList<Handler>>();
        }
        ArrayList<Handler> arrayHandler = systemHandler.get(handlerId);
        if (null != arrayHandler)
        {
            arrayHandler.remove(handler);
            if (arrayHandler.size() <= 0)
            {
                systemHandler.remove(handlerId);
            }
        }
    }
    
    /**
     * 消息通知
     */
    public static void notifyMessage(int handlerId, Message msg)
    {
        
        if (systemHandler == null)
        {
            systemHandler = new HashMap<Integer, ArrayList<Handler>>();
        }
        
        synchronized (systemHandler)
        {
            // 通知已经注册的所有的Handler去发送消
            ArrayList<Handler> handlerList = systemHandler.get(handlerId);
            if (null != handlerList)
            {
                for (Handler handler : handlerList)
                {
                    if (handler != null)
                    {
                        try
                        {
                            handler.sendMessage(handler.obtainMessage(msg.what, msg.arg1, msg.arg2, msg.obj));
                        }
                        catch (Exception iex)
                        {
                            Log.e(TAG,Log.getStackTraceString(iex));
                        }
                    }
                }
            }
        }
    }
    
    /**
     * 消息通知
     */
    public static void notifyMessage(int handlerId, int msgType, Object obj)
    {
        // Log.d("Love", "notifyMessage obj:" + obj.toString());
        if (systemHandler == null)
        {
            systemHandler = new HashMap<Integer, ArrayList<Handler>>();
        }
        
        synchronized (systemHandler)
        {
            // 通知已经注册的所有的Handler去发送消
            ArrayList<Handler> handlerList = systemHandler.get(handlerId);
            if (null != handlerList)
            {
                for (Handler handler : handlerList)
                {
                    if (handler != null)
                    {
                        handler.sendMessage(handler.obtainMessage(msgType, obj));
                    }
                }
            }
        }
    }
    
    /**
     * 消息通知
     */
    public static void notifyMessage(int handlerId, int msgType)
    {
        if (systemHandler == null)
        {
            systemHandler = new HashMap<Integer, ArrayList<Handler>>();
        }
        
        synchronized (systemHandler)
        {
            // 通知已经注册的所有的Handler去发送消
            ArrayList<Handler> handlerList = systemHandler.get(handlerId);
            if (null != handlerList)
            {
                for (Handler handler : handlerList)
                {
                    if (handler != null)
                    {
                        handler.sendEmptyMessage(msgType);
                    }
                }
            }
        }
    }
}
