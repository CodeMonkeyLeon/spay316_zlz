/*
 * 文 件 名:  CashierUpdateActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2015-3-16
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.logica.user;

import android.os.Bundle;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;

/**
 *收银员修改
 * 
 * @author  he_hui
 * @version  [版本号, 2015-3-16]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class CashierUpdateActivity extends TemplateActivity
{
    
    /** {@inheritDoc} */
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.cashier_view);
        
    }
    
    /** {@inheritDoc} */
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle("收银员");
        
    }
}
