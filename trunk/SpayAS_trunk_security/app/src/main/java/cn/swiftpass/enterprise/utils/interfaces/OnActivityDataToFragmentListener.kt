package cn.swiftpass.enterprise.utils.interfaces

import android.content.Intent

interface OnActivityDataToFragmentListener {


    fun onResult(requestCode: Int, resultCode: Int, data: Intent?)


    fun onPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>?,
        grantResults: IntArray?
    )

}