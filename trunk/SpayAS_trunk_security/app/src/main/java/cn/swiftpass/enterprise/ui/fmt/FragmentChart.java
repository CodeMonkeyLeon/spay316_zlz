package cn.swiftpass.enterprise.ui.fmt;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.xclcharts.common.DensityUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cn.swiftpass.enterprise.bussiness.model.CashierTop5Bean;
import cn.swiftpass.enterprise.bussiness.model.DailyTransactionDataBean;
import cn.swiftpass.enterprise.bussiness.model.ReportTransactionDetailsBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;
import cn.swiftpass.enterprise.ui.activity.TransactionDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.draw.AreaChart01View;
import cn.swiftpass.enterprise.ui.activity.draw.BarChart02ViewNew;

/**
 * Created by aijingya on 2019/4/23.
 *
 * @Package cn.swiftpass.enterprise.ui.fmt
 * @Description: ${TODO}(最终图标的类---曲线图和柱状图)
 * @date 2019/4/23.18:09.
 */
public class FragmentChart extends BaseFragment {
    private TransactionDetailsActivity mActivity;
    public String ChartType;
    public String PageType;
    public String ReportType;
    public String Type_Transaction = "1";
    public String Type_CashierTop5="3";

    public LinearLayout ll_transaction_date,ll_cashier_top5;
    public LinearLayout ll_transaction_no_data,ll_TransactionChart,ll_area_time;
    private TextView tv_two,tv_middle;

    public LinearLayout ll_bar_no_data,ll_barChart;

    public ReportTransactionDetailsBean reportTransactionDetailsBean;

    public void setChartType(String type){
        this.ChartType = type;
    }

    public void setReportType(String type){
        this.ReportType = type;
    }

    public void setPageType(String type){
        this.PageType = type;
    }


    public void setReportTransactionDetailsBean(ReportTransactionDetailsBean transactionDetailsBean){
        this.reportTransactionDetailsBean = transactionDetailsBean;
    }

    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof TransactionDetailsActivity){
            mActivity=(TransactionDetailsActivity) activity;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_chart, container, false);
        initViews(view);
        return view;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (reportTransactionDetailsBean != null) {
            initData();
        }

    }

    public void initViews(View view){
        ll_transaction_date = view.findViewById(R.id.ll_transaction_date);
        ll_cashier_top5 = view.findViewById(R.id.ll_cashier_top5);

        ll_transaction_no_data = view.findViewById(R.id.ll_transaction_no_data);
        ll_TransactionChart = view.findViewById(R.id.ll_TransactionChart);
        ll_area_time = view.findViewById(R.id.ll_area_time);
        tv_two = view.findViewById(R.id.tv_two);
        tv_middle = view.findViewById(R.id.tv_middle);

        ll_bar_no_data = view.findViewById(R.id.ll_bar_no_data);
        ll_barChart = view.findViewById(R.id.ll_barChart);
    }

    public void initData(){
        if(ChartType.equalsIgnoreCase(Type_Transaction)){//如果是折线图
            ll_transaction_date.setVisibility(View.VISIBLE);
            ll_cashier_top5.setVisibility(View.GONE);

            if(reportTransactionDetailsBean != null && reportTransactionDetailsBean.getDayStatistics() != null
                &&reportTransactionDetailsBean.getDayStatistics().size() > 0){
                ll_transaction_no_data.setVisibility(View.GONE);
                ll_TransactionChart.setVisibility(View.VISIBLE);
                createArea(reportTransactionDetailsBean.getDayStatistics(),PageType,ReportType);
            }else{
                ll_transaction_no_data.setVisibility(View.VISIBLE);
                ll_TransactionChart.setVisibility(View.GONE);
            }

        } else if(ChartType.equalsIgnoreCase(Type_CashierTop5)){ //如果是柱状图
            ll_transaction_date.setVisibility(View.GONE);
            ll_cashier_top5.setVisibility(View.VISIBLE);

            if(PageType.equalsIgnoreCase("1")){
                if(reportTransactionDetailsBean != null && reportTransactionDetailsBean.getStatisticsByAmount() != null
                && reportTransactionDetailsBean.getStatisticsByAmount().size() > 0){
                    createBarChart(reportTransactionDetailsBean.getStatisticsByAmount(),PageType);
                }else{
                    ll_bar_no_data.setVisibility(View.VISIBLE);
                    ll_barChart.setVisibility(View.GONE);
                }
            }else if(PageType.equalsIgnoreCase("2")){
                if(reportTransactionDetailsBean != null && reportTransactionDetailsBean.getStatisticsByCount() != null
                &&reportTransactionDetailsBean.getStatisticsByCount().size() > 0){
                    createBarChart(reportTransactionDetailsBean.getStatisticsByCount(),PageType);
                }else{
                    ll_bar_no_data.setVisibility(View.VISIBLE);
                    ll_barChart.setVisibility(View.GONE);
                }
            }
        }
    }

    /**
     * 折线图
     */
    void createArea(List<DailyTransactionDataBean> dayStatistics,String pageType ,String ReportType) {
        if (dayStatistics != null && dayStatistics.size() > 0) {
            ll_transaction_no_data.setVisibility(View.GONE);
            ll_TransactionChart.setVisibility(View.VISIBLE);
            AreaChart01View areaChart01View = new AreaChart01View(mContext, dayStatistics, pageType,ReportType);
            ll_TransactionChart.removeAllViews();
            ll_TransactionChart.addView(areaChart01View);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) areaChart01View.getLayoutParams();
            params.width = LinearLayout.LayoutParams.MATCH_PARENT;
            params.height = DensityUtil.dip2px(mContext, 280);

            if(ReportType.equalsIgnoreCase("3")){
                ll_area_time.setVisibility(View.VISIBLE);
                tv_two.setText(dayStatistics.size()+"");
                tv_middle.setText(dayStatistics.size()/2 +"");
            }else{
                ll_area_time.setVisibility(View.GONE);
            }
        } else {
            ll_transaction_no_data.setVisibility(View.VISIBLE);
            ll_TransactionChart.setVisibility(View.GONE);
        }
    }


    /**
     * 柱状图
     */
    void createBarChart(ArrayList<CashierTop5Bean> cashierTop5InfoList,String pageType) {
        if (cashierTop5InfoList != null && cashierTop5InfoList.size() > 0) {
            ll_bar_no_data.setVisibility(View.GONE);
            ll_barChart.setVisibility(View.VISIBLE);
            List<CashierTop5Bean> list = cashierTop5InfoList;
            Collections.reverse(list);
            BarChart02ViewNew barChart02ViewNew = new BarChart02ViewNew(mActivity, list,pageType);
            Collections.reverse(list);
            ll_barChart.removeAllViews();
            ll_barChart.addView(barChart02ViewNew);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) barChart02ViewNew.getLayoutParams();
            params.width = LinearLayout.LayoutParams.MATCH_PARENT;
            params.height = DensityUtil.dip2px(mActivity, 300);

        } else {
            ll_bar_no_data.setVisibility(View.VISIBLE);
            ll_barChart.setVisibility(View.GONE);
        }
    }


    @Override
    protected BasePresenter createPresenter() {
        return null;
    }
}
