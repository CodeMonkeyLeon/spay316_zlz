package com.cxy.networklibrary.di.module;


import com.cxy.networklibrary.cache.CacheConfig;
import com.cxy.networklibrary.other.OtherConfig;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


/**
 * author:  ljy
 * date:    2018/3/11
 * description: 各模块配置的供应Module
 */

@Module
public class ConfigModule {

    @Provides
    @Singleton
    CacheConfig cacheConfig() {
        return new CacheConfig();
    }

    @Provides
    @Singleton
    OtherConfig crashDiaryConfig() {
        return new OtherConfig();
    }


}
