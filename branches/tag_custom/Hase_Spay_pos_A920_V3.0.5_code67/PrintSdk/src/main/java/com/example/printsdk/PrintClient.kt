package com.example.printsdk

import android.content.Context
import android.graphics.Bitmap
import com.example.printsdk.A8.POSA8Client
import com.example.printsdk.A8.printUtils.PrintA8Bean
import com.example.printsdk.A8.printUtils.PrintA8Text
import com.example.printsdk.A920.POSA920Client
import com.example.printsdk.A920.printUtils.PrintA920Text
import com.pax.gl.imgprocessing.IImgProcessing

class PrintClient {

    private var sPosTerminal: String? = null
    private var mAppContext: Context? = null
    private var printDelay: Long = 0
    var receipt_type: RECEIPT_TYPE? = null

    enum class RECEIPT_TYPE {
        summary_receipt, order_receipt, refund_receipt, pre_auth_receipt, unfreeze_receipt
    }

    companion object {
        private var sPrintClient: PrintClient? = null
        fun initClient(context: Context) {
            if (sPrintClient == null) {
                sPrintClient = PrintClient(context.applicationContext)
            }
        }

        fun getInstance(): PrintClient? {
            if (sPrintClient == null) {
                throw RuntimeException("PrintClient is not initialized")
            }
            return sPrintClient
        }
    }

    constructor(context: Context) {
        mAppContext = context
    }

    fun setPrintDelay(printDelay: Long) {
        this.printDelay = printDelay
    }

    fun getPrintDelay() = printDelay

    fun initPrintSDK(posTerminal: String?) {
        sPosTerminal = posTerminal
        //根据不同的POS机型号，初始化不同的打印机SDK
        if (sPosTerminal.equals(Constants.TYPE_A8, ignoreCase = true)) {
            mAppContext?.let {
                POSA8Client.initA8(it)
                setPrintDelay(POSA8Client.DELAY_TIME)
            }
        } else if (sPosTerminal.equals(Constants.TYPE_A920, ignoreCase = true)) {
            mAppContext?.let {
                POSA920Client.initA920(it)
                setPrintDelay(POSA920Client.DELAY_TIME)
            }
        }
        //        else if (sPosTerminal.equals("sunmi", ignoreCase = true)) {
//            POSSunmiClient.initSunmi(mAppContext)
//            setPrintDelay(4000L)
//        } else if (sPosTerminal.equals("N5", ignoreCase = true)) {
//            POSN5Client.initN5(mAppContext)
//            setPrintDelay(5000L)
//        } else if (sPosTerminal.equals("tradelinkA8", ignoreCase = true)) {
//            POSTradelinkA8Client.initTradelinkA8(mAppContext)
//            setPrintDelay(6000L)
//        } else if (sPosTerminal.equals("castles", ignoreCase = true)) {
//            POSCastlesClient.initCastles(mAppContext)
//            setPrintDelay(3000L)
//        } else if (sPosTerminal.equals("KS8223", ignoreCase = true)) {
//            POSKS8223Client.initKS8223(mAppContext)
//            setPrintDelay(6000L)
//        } else if (sPosTerminal.equals("N910", ignoreCase = true) ||
//            sPosTerminal.equals("N900", ignoreCase = true)
//        ) {
//            POSN910Client.initN910(mAppContext)
//            setPrintDelay(3000L)
//        }
    }


    fun printTitle(text: String?) {
        if (sPosTerminal.equals(Constants.TYPE_A8, ignoreCase = true)) {
            PrintA8Text.getInstance()?.let {
                it.printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.TITLE, text)
            }
        } else if (sPosTerminal.equals(Constants.TYPE_A920, ignoreCase = true)) {
            PrintA920Text.getInstance()?.let {
                it.init(mAppContext)
                it.printTitle(text)
                it.printEmptyLine()
            }
        }
        //        else if (sPosTerminal.equals("sunmi", ignoreCase = true)) {
//            PrintSunmiText.getInstance().init(mAppContext)
//            PrintSunmiText.getInstance().printTitle(text)
//        } else if (sPosTerminal.equals("N5", ignoreCase = true)) {
//            PrintN5Text.getInstance().init(mAppContext)
//            PrintN5Text.getInstance().printTitle(text)
//        } else if (sPosTerminal.equals("tradelinkA8", ignoreCase = true)) {
//            PrintTradelinkA8Text.getInstance().init(mAppContext)
//            PrintTradelinkA8Text.getInstance().printTitle(text)
//        } else if (sPosTerminal.equals("castles", ignoreCase = true)) {
//            PrintCastlesText.getInstance().init(mAppContext, getReceipt_type())
//            PrintCastlesText.getInstance().printTitle(text)
//        } else if (sPosTerminal.equals("KS8223", ignoreCase = true)) {
//            PrintKS8223Text.getInstance().init(mAppContext)
//            PrintKS8223Text.getInstance().printTitle(text)
//        } else if (sPosTerminal.equals("N910", ignoreCase = true) ||
//            sPosTerminal.equals("N900", ignoreCase = true)
//        ) {
//            PrintN910Text.initContext(mAppContext)
//            PrintN910Text.getInstance().init()
//            PrintN910Text.getInstance().printTitle(text, getReceipt_type())
//        }
    }

    fun printTextLeft(text: String?) {
        if (sPosTerminal.equals(Constants.TYPE_A8, ignoreCase = true)) {
            PrintA8Text.getInstance()?.let {
                it.printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.TEXT_LINE_LEFT, text)
            }
        } else if (sPosTerminal.equals(Constants.TYPE_A920, ignoreCase = true)) {
            PrintA920Text.getInstance()?.let {
                it.printTextLine(text, 24, IImgProcessing.IPage.EAlign.LEFT)
            }
        }
        //        else if (sPosTerminal.equals("sunmi", ignoreCase = true)) {
//            PrintSunmiText.getInstance().printTextLine(text, 24, 0)
//        } else if (sPosTerminal.equals("N5", ignoreCase = true)) {
//            PrintN5Text.getInstance().printTextLine(text, FONT_SIZE_NORMAL, AlignEnum.LEFT)
//        } else if (sPosTerminal.equals("tradelinkA8", ignoreCase = true)) {
//            PrintTradelinkA8Text.getInstance().printTextLine(text, Printing.ALIGN_START)
//        } else if (sPosTerminal.equals("castles", ignoreCase = true)) {
//            PrintCastlesText.getInstance().printTextLine(text)
//        } else if (sPosTerminal.equals("KS8223", ignoreCase = true)) {
//            PrintKS8223Text.getInstance().printTextLine(text, Printer.Align.LEFT)
//        } else if (sPosTerminal.equals("N910", ignoreCase = true) ||
//            sPosTerminal.equals("N900", ignoreCase = true)
//        ) {
//            PrintN910Text.getInstance().printTextLine(text)
//        }
    }

    fun printTextRight(text: String?) {
        if (sPosTerminal.equals(Constants.TYPE_A8, ignoreCase = true)) {
            PrintA8Text.getInstance()?.let {
                it.printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.TEXT_LINE_RIGHT, text)
            }
        } else if (sPosTerminal.equals(Constants.TYPE_A920, ignoreCase = true)) {
            PrintA920Text.getInstance()?.let {
                it.printTextLine(text, 24, IImgProcessing.IPage.EAlign.RIGHT)
            }
        }
        //        else if (sPosTerminal.equals("sunmi", ignoreCase = true)) {
//            PrintSunmiText.getInstance().printTextLine(text, 24, 2)
//        } else if (sPosTerminal.equals("N5", ignoreCase = true)) {
//            PrintN5Text.getInstance().printTextLine(text, FONT_SIZE_NORMAL, AlignEnum.RIGHT)
//        } else if (sPosTerminal.equals("tradelinkA8", ignoreCase = true)) {
//            PrintTradelinkA8Text.getInstance().printTextLine(text, Printing.ALIGN_END)
//        } else if (sPosTerminal.equals("castles", ignoreCase = true)) {
//            PrintCastlesText.getInstance().printTextLine(text)
//        } else if (sPosTerminal.equals("KS8223", ignoreCase = true)) {
//            PrintKS8223Text.getInstance().printTextLine(text, Printer.Align.RIGHT)
//        } else if (sPosTerminal.equals("N910", ignoreCase = true) ||
//            sPosTerminal.equals("N900", ignoreCase = true)
//        ) {
//            PrintN910Text.getInstance().printTextLine(text)
//        }
    }

    fun printTextCenter(text: String?) {
        if (sPosTerminal.equals(Constants.TYPE_A8, ignoreCase = true)) {
            PrintA8Text.getInstance()?.let {
                it.printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.TEXT_LINE_CENTER, text)
            }
        } else if (sPosTerminal.equals(Constants.TYPE_A920, ignoreCase = true)) {
            PrintA920Text.getInstance()?.let {
                it.printTextLine(text, 24, IImgProcessing.IPage.EAlign.CENTER)
            }
        }
        //        else if (sPosTerminal.equals("sunmi", ignoreCase = true)) {
//            PrintSunmiText.getInstance().printTextLine(text, 24, 1)
//        } else if (sPosTerminal.equals("N5", ignoreCase = true)) {
//            PrintN5Text.getInstance().printTextLine(text, FONT_SIZE_NORMAL, AlignEnum.CENTER)
//        } else if (sPosTerminal.equals("tradelinkA8", ignoreCase = true)) {
//            PrintTradelinkA8Text.getInstance().printTextLine(text, Printing.ALIGN_CENTER)
//        } else if (sPosTerminal.equals("castles", ignoreCase = true)) {
//            PrintCastlesText.getInstance().printTextLine(text)
//        } else if (sPosTerminal.equals("KS8223", ignoreCase = true)) {
//            PrintKS8223Text.getInstance().printTextLine(text, Printer.Align.CENTER)
//        } else if (sPosTerminal.equals("N910", ignoreCase = true) ||
//            sPosTerminal.equals("N900", ignoreCase = true)
//        ) {
//            PrintN910Text.getInstance().printTextLine(text)
//        }
    }

    fun printMultiLines(text: String?) {
        if (sPosTerminal.equals(Constants.TYPE_A8, ignoreCase = true)) {
            PrintA8Text.getInstance()
                ?.printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.TEXT_MULTI_LINES, text)
        } else if (sPosTerminal.equals(Constants.TYPE_A920, ignoreCase = true)) {
            PrintA920Text.getInstance()?.let {
                it.printTextLine(text, 24, IImgProcessing.IPage.EAlign.LEFT)
            }
        }
        //        else if (sPosTerminal.equals("sunmi", ignoreCase = true)) {
//            PrintSunmiText.getInstance().printTextLine(text, 24, 0)
//        } else if (sPosTerminal.equals("N5", ignoreCase = true)) {
//            PrintN5Text.getInstance().printTextLine(text, FONT_SIZE_NORMAL, AlignEnum.LEFT)
//        } else if (sPosTerminal.equals("tradelinkA8", ignoreCase = true)) {
//            PrintTradelinkA8Text.getInstance().printMultiLines(text)
//        } else if (sPosTerminal.equals("castles", ignoreCase = true)) {
//            PrintCastlesText.getInstance().printMultiLines(text)
//        } else if (sPosTerminal.equals("KS8223", ignoreCase = true)) {
//            PrintKS8223Text.getInstance().printMultiLines(text, Printer.Align.LEFT)
//        } else if (sPosTerminal.equals("N910", ignoreCase = true) ||
//            sPosTerminal.equals("N900", ignoreCase = true)
//        ) {
//            PrintN910Text.getInstance().printMultiLines(text)
//        }
    }

    fun printBitmapLogo(bitmap: Bitmap?) {
        if (sPosTerminal.equals(Constants.TYPE_A8, ignoreCase = true)) {
        } else if (sPosTerminal.equals(Constants.TYPE_A920, ignoreCase = true)) {
        }
        //        else if (sPosTerminal.equals("sunmi", ignoreCase = true)) {
//            PrintSunmiText.getInstance().printBitmap(bitmap)
//        } else if (sPosTerminal.equals("N5", ignoreCase = true)) {
//            PrintN5Text.getInstance().printBitmap(bitmap)
//        } else if (sPosTerminal.equals("tradelinkA8", ignoreCase = true)) {
//        } else if (sPosTerminal.equals("castles", ignoreCase = true)) {
//        } else if (sPosTerminal.equals("KS8223", ignoreCase = true)) {
//            PrintKS8223Text.getInstance().printBitmap(bitmap)
//        } else if (sPosTerminal.equals("N910", ignoreCase = true) ||
//            sPosTerminal.equals("N900", ignoreCase = true)
//        ) {
//        }
    }

    fun printQRCode(orderNoMch: String?) {
        if (sPosTerminal.equals(Constants.TYPE_A8, ignoreCase = true)) {
            PrintA8Text.getInstance()?.let {
                it.printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.QR_CODE, orderNoMch)
            }
        } else if (sPosTerminal.equals(Constants.TYPE_A920, ignoreCase = true)) {
            PrintA920Text.getInstance()?.let {
                it.printQRCode(orderNoMch)
            }
        }
        //        else if (sPosTerminal.equals("sunmi", ignoreCase = true)) {
//            PrintSunmiText.getInstance().printQRCode(orderNoMch)
//        } else if (sPosTerminal.equals("N5", ignoreCase = true)) {
//            PrintN5Text.getInstance().printQRCode(orderNoMch)
//        } else if (sPosTerminal.equals("tradelinkA8", ignoreCase = true)) {
//            PrintTradelinkA8Text.getInstance().printQRCode(orderNoMch)
//        } else if (sPosTerminal.equals("castles", ignoreCase = true)) {
//            PrintCastlesText.getInstance().printQRCode(orderNoMch)
//        } else if (sPosTerminal.equals("KS8223", ignoreCase = true)) {
//            PrintKS8223Text.getInstance().printQRCode(orderNoMch)
//        } else if (sPosTerminal.equals("N910", ignoreCase = true) ||
//            sPosTerminal.equals("N900", ignoreCase = true)
//        ) {
//            PrintN910Text.getInstance().printQRCode(orderNoMch)
//        }
    }

    fun printDoubleLine() {
        if (sPosTerminal.equals(Constants.TYPE_A8, ignoreCase = true)) {
            PrintA8Text.getInstance()?.let {
                it.printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.DOUBLE_LINE, "")
            }
        } else if (sPosTerminal.equals(Constants.TYPE_A920, ignoreCase = true)) {
            PrintA920Text.getInstance()?.let {
                it.printDoubleLine()
            }
        }
        //        else if (sPosTerminal.equals("sunmi", ignoreCase = true)) {
//            PrintSunmiText.getInstance().printDoubleLine()
//        } else if (sPosTerminal.equals("N5", ignoreCase = true)) {
//            PrintN5Text.getInstance().printDoubleLine()
//        } else if (sPosTerminal.equals("tradelinkA8", ignoreCase = true)) {
//            PrintTradelinkA8Text.getInstance().printDoubleLine()
//        } else if (sPosTerminal.equals("castles", ignoreCase = true)) {
//            PrintCastlesText.getInstance().printDoubleLine()
//        } else if (sPosTerminal.equals("KS8223", ignoreCase = true)) {
//            PrintKS8223Text.getInstance().printDoubleLine()
//        } else if (sPosTerminal.equals("N910", ignoreCase = true) ||
//            sPosTerminal.equals("N900", ignoreCase = true)
//        ) {
//            PrintN910Text.getInstance().printDoubleLine()
//        }
    }

    fun pintSingleLine() {
        if (sPosTerminal.equals(Constants.TYPE_A8, ignoreCase = true)) {
            PrintA8Text.getInstance()?.let {
                it.printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.SINGLE_LINE, "")
            }
        } else if (sPosTerminal.equals(Constants.TYPE_A920, ignoreCase = true)) {
            PrintA920Text.getInstance()?.let {
                it.printSingleLine()
            }
        }
        //        else if (sPosTerminal.equals("sunmi", ignoreCase = true)) {
//            PrintSunmiText.getInstance().printSingleLine()
//        } else if (sPosTerminal.equals("N5", ignoreCase = true)) {
//            PrintN5Text.getInstance().printSingleLine()
//        } else if (sPosTerminal.equals("tradelinkA8", ignoreCase = true)) {
//            PrintTradelinkA8Text.getInstance().printSingleLine()
//        } else if (sPosTerminal.equals("castles", ignoreCase = true)) {
//            PrintCastlesText.getInstance().printSingleLine()
//        } else if (sPosTerminal.equals("KS8223", ignoreCase = true)) {
//            PrintKS8223Text.getInstance().printSingleLine()
//        } else if (sPosTerminal.equals("N910", ignoreCase = true) ||
//            sPosTerminal.equals("N900", ignoreCase = true)
//        ) {
//            PrintN910Text.getInstance().printSingleLine()
//        }
    }

    fun printEmptyLine() {
        if (sPosTerminal.equals(Constants.TYPE_A8, ignoreCase = true)) {
            PrintA8Text.getInstance()?.let {
                it.printInfo(PrintA8Bean.PRINT_FUNCTION_NAME.EMPTY_LINE, "")
            }
        } else if (sPosTerminal.equals(Constants.TYPE_A920, ignoreCase = true)) {
            PrintA920Text.getInstance()?.let {
                it.printEmptyLine()
            }
        }
        //        else if (sPosTerminal.equals("sunmi", ignoreCase = true)) {
//            PrintSunmiText.getInstance().printEmptyLine()
//        } else if (sPosTerminal.equals("N5", ignoreCase = true)) {
//            PrintN5Text.getInstance().printEmptyLine()
//        } else if (sPosTerminal.equals("tradelinkA8", ignoreCase = true)) {
//            PrintTradelinkA8Text.getInstance().printEmptyLine()
//        } else if (sPosTerminal.equals("castles", ignoreCase = true)) {
//            PrintCastlesText.getInstance().printEmptyLine()
//        } else if (sPosTerminal.equals("KS8223", ignoreCase = true)) {
//            PrintKS8223Text.getInstance().printEmptyLine()
//        } else if (sPosTerminal.equals("N910", ignoreCase = true) ||
//            sPosTerminal.equals("N900", ignoreCase = true)
//        ) {
//            PrintN910Text.getInstance().printEmptyLine()
//        }
    }

    fun startPrint() {
        if (sPosTerminal.equals(Constants.TYPE_A8, ignoreCase = true)) {
            PrintA8Text.getInstance()?.let {
                it.init(mAppContext)
                it.startPrint()
            }
        } else if (sPosTerminal.equals(Constants.TYPE_A920, ignoreCase = true)) {
            PrintA920Text.getInstance()?.let {
                it.startPrint()
            }
        }
        //        else if (sPosTerminal.equals("sunmi", ignoreCase = true)) {
//        } else if (sPosTerminal.equals("N5", ignoreCase = true)) {
//            PrintN5Text.getInstance().startPrint()
//        } else if (sPosTerminal.equals("tradelinkA8", ignoreCase = true)) {
//            PrintTradelinkA8Text.getInstance().startPrint()
//        } else if (sPosTerminal.equals("castles", ignoreCase = true)) {
//            PrintCastlesText.getInstance().startPrint()
//        } else if (sPosTerminal.equals("KS8223", ignoreCase = true)) {
//            PrintKS8223Text.getInstance().startPrint()
//        } else if (sPosTerminal.equals("N910", ignoreCase = true) ||
//            sPosTerminal.equals("N900", ignoreCase = true)
//        ) {
//            PrintN910Text.getInstance().startPrint(getReceipt_type())
//        }
    }

    fun bindDeviceService() {
        if (sPosTerminal.equals(Constants.TYPE_A8, ignoreCase = true)) {
            PrintA8Text.getInstance()?.let {
                it.bindDeviceService()
            }
        } else if (sPosTerminal.equals(Constants.TYPE_A920, ignoreCase = true)) {
        }
        //        else if (sPosTerminal.equals("sunmi", ignoreCase = true)) {
//        } else if (sPosTerminal.equals("N5", ignoreCase = true)) {
//        } else if (sPosTerminal.equals("tradelinkA8", ignoreCase = true)) {
//            PrintTradelinkA8Text.getInstance().bindDeviceService(mAppContext)
//        } else if (sPosTerminal.equals("castles", ignoreCase = true)) {
//        } else if (sPosTerminal.equals("KS8223", ignoreCase = true)) {
//            PrintKS8223Text.getInstance().bindDeviceService()
//        } else if (sPosTerminal.equals("N910", ignoreCase = true) ||
//            sPosTerminal.equals("N900", ignoreCase = true)
//        ) {
//            PrintN910Text.getInstance().bindDeviceService()
//        }
    }


    fun unbindDeviceService() {
        if (sPosTerminal.equals(Constants.TYPE_A8, ignoreCase = true)) {
            PrintA8Text.getInstance()?.let {
                it.unbindDeviceService()
            }
        } else if (sPosTerminal.equals(Constants.TYPE_A920, ignoreCase = true)) {
        }
        //        else if (sPosTerminal.equals("sunmi", ignoreCase = true)) {
//        } else if (sPosTerminal.equals("N5", ignoreCase = true)) {
//        } else if (sPosTerminal.equals("tradelinkA8", ignoreCase = true)) {
//            PrintTradelinkA8Text.getInstance().unbindDeviceService()
//        } else if (sPosTerminal.equals("castles", ignoreCase = true)) {
//        } else if (sPosTerminal.equals("KS8223", ignoreCase = true)) {
//            PrintKS8223Text.getInstance().unbindDeviceService()
//        } else if (sPosTerminal.equals("N910", ignoreCase = true) ||
//            sPosTerminal.equals("N900", ignoreCase = true)
//        ) {
//            PrintN910Text.getInstance().unbindDeviceService()
//        }
    }

}