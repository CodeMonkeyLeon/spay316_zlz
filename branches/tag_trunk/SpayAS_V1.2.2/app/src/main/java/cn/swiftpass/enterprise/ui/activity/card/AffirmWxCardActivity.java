/*
 * 文 件 名:  FindPassWordActivity.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-14
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.card;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.WxCard;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.ConfirmPayActivity;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.user.VerificationDetails;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 核销微信卡券
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-14]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class AffirmWxCardActivity extends TemplateActivity
{
    
    private TextView tv_title, tv_time, tv_card_id, tv_card_detail;
    
    private Button btn_go_on, btn_tp_pay;
    
    private WxCard wxCard;
    
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    public static void startActivity(Context context, WxCard wxCard)
    {
        Intent it = new Intent();
        it.setClass(context, AffirmWxCardActivity.class);
        it.putExtra("wxCard", wxCard);
        context.startActivity(it);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wx_card);
        
        initView();
        wxCard = (WxCard)getIntent().getSerializableExtra("wxCard");
        
        initValue();
        
        setLister();
        
        MainApplication.listActivities.add(this);
    }
    
    private void setLister()
    {
        btn_go_on.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                AffirmWxCardActivity.this.finish();
            }
        });
        
        btn_tp_pay.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                if (wxCard.getCardType().equalsIgnoreCase("groupon") || wxCard.getCardType().equalsIgnoreCase("gift"))
                {
                    //礼品券和团购券
                    
                    if (wxCard == null)
                    {
                        return;
                    }
                    OrderManager.getInstance().queryVardDetail(wxCard.getCardId(),
                        wxCard.getCardCode(),
                        new UINotifyListener<WxCard>()
                        {
                            
                            @Override
                            public void onError(Object object)
                            {
                                super.onError(object);
                                dismissLoading();
                                if (object != null)
                                {
                                    //                                showToastInfo(object.toString());
                                    toastDialog(AffirmWxCardActivity.this, object.toString(), null);
                                }
                            }
                            
                            @Override
                            public void onPreExecute()
                            {
                                super.onPreExecute();
                                
                                loadDialog(AffirmWxCardActivity.this, R.string.public_data_loading);
                            }
                            
                            @Override
                            public void onSucceed(WxCard result)
                            {
                                super.onSucceed(result);
                                dismissLoading();
                                if (result != null)
                                {
                                    VerificationDetails.startActivity(AffirmWxCardActivity.this, result);
                                    for (Activity a : MainApplication.listActivities)
                                    {
                                        a.finish();
                                    }
                                }
                            }
                            
                        });
                }
                else
                {
                    List<WxCard> list = new ArrayList<WxCard>();
                    list.add(wxCard);
                    ConfirmPayActivity.startActivity(AffirmWxCardActivity.this, list);
                }
                
            }
        });
    }
    
    private void initValue()
    {
        if (null != wxCard)
        {
            tv_title.setText(wxCard.getTitle());
            tv_card_id.setText(wxCard.getCardCode());
            tv_card_detail.setText(wxCard.getDealDetail());
            if (wxCard.getCardType().equalsIgnoreCase("groupon"))
            {
                //礼品券和团购券
                btn_go_on.setVisibility(View.GONE);
                btn_tp_pay.setText(R.string.bt_card_user);
                titleBar.setTitle(R.string.bt_card_type_goup);
            }
            else if (wxCard.getCardType().equalsIgnoreCase("gift"))
            {
                btn_go_on.setVisibility(View.GONE);
                btn_tp_pay.setText(R.string.bt_card_user);
                titleBar.setTitle(R.string.bt_card_type_gif);
            }
            else if (wxCard.getCardType().equalsIgnoreCase("cash"))
            {
                btn_go_on.setVisibility(View.GONE);
                btn_tp_pay.setText(R.string.tx_to_receivables);
                titleBar.setTitle(R.string.bt_card_type_cash);
                tv_card_detail.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(wxCard.getReduceCost()));
            }
            else
            {
                btn_go_on.setVisibility(View.GONE);
                titleBar.setTitle(R.string.bt_card_type_discount);
                float m = (100 - wxCard.getDiscount()) / 10f;
                btn_tp_pay.setText(R.string.tx_to_receivables);
                tv_card_detail.setText(m + getString(R.string.tv_card_discount_title));
            }
            
            if (wxCard.getType().equals("DATE_TYPE_FIX_TERM"))
            {
                tv_time.setText(getString(R.string.bt_card_time) + ":" + wxCard.getFixedTerm()
                    + getString(R.string.tx_day));
            }
            else
            {
                if (!StringUtil.isEmptyOrNull(wxCard.getBeginTimestamp())
                    && !StringUtil.isEmptyOrNull(wxCard.getEndTimestamp()))
                {
                    
                    tv_time.setText(getString(R.string.bt_card_time) + ":" + wxCard.getBeginTimeStr() + " "
                        + getString(R.string.tv_least) + wxCard.getEndTimeStr());
                }
                
            }
        }
    }
    
    private void initView()
    {
        tv_title = (TextView)findViewById(R.id.tv_title);
        tv_time = (TextView)findViewById(R.id.tv_time);
        tv_card_id = (TextView)findViewById(R.id.tv_card_id);
        tv_card_detail = (TextView)findViewById(R.id.tv_card_detail);
        
        btn_go_on = (Button)findViewById(R.id.btn_go_on);
        btn_tp_pay = (Button)findViewById(R.id.btn_tp_pay);
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_coupon);
    }
    
}
