/*
 * 文 件 名:  BlanceRecordActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-8-15
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.wallet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.WalletModel;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 提现记录列表
 * 
 * @author  he_hui
 * @version  [版本号, 2016-8-15]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class BlanceRecordDetailActivity extends TemplateActivity
{
    
    private WalletModel walletModel;
    
    private TextView tv_type, tv_moeny, tv_detail_type, tv_time, tv_order_no, tv_order;
    
    private LinearLayout ly_order;
    
    public static void startActivity(Context context, WalletModel walletModel)
    {
        Intent it = new Intent();
        it.setClass(context, BlanceRecordDetailActivity.class);
        it.putExtra("walletModel", walletModel);
        context.startActivity(it);
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonIsVisible(true);
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tx_apply_detail);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push_detail);
        walletModel = (WalletModel)getIntent().getSerializableExtra("walletModel");
        initView();
        
    }
    
    private void initView()
    {
        tv_order = getViewById(R.id.tv_order);
        Object object = SharedPreUtile.readProduct("dynModel" + ApiConstant.bankCode);
        if (object != null)
        {
            
            DynModel dynModel = (DynModel)object;
            if (dynModel != null && !StringUtil.isEmptyOrNull(dynModel.getServiceTel()))
            {
                tv_order.setText(dynModel.getServiceTel());
            }
        }
        tv_type = getViewById(R.id.tv_type);
        tv_moeny = getViewById(R.id.tv_moeny);
        tv_detail_type = getViewById(R.id.tv_detail_type);
        tv_time = getViewById(R.id.tv_time);
        tv_order_no = getViewById(R.id.tv_order_no);
        ly_order = getViewById(R.id.ly_order);
        if (walletModel != null)
        {
            if (StringUtil.isEmptyOrNull(walletModel.getTransaction_id()))
            {
                ly_order.setVisibility(View.GONE);
            }
            tv_order_no.setText(walletModel.getTransaction_id());
            if (!StringUtil.isEmptyOrNull(walletModel.getTrans_type()))
            {
                switch (Integer.parseInt(walletModel.getTrans_type()))
                {
                    case 1:
                        tv_type.setText(R.string.tv_list_type_out);
                        tv_detail_type.setText(R.string.tv_list_type_out);
                        if (!StringUtil.isEmptyOrNull(walletModel.getAmount()))
                        {
                            if (walletModel.getAmount().startsWith("-"))
                            {
                                tv_moeny.setText(MainApplication.feeFh
                                    + DateUtil.formatMoneyUtils(Long.parseLong(walletModel.getAmount().substring(1,
                                        walletModel.getAmount().length()))));
                            }
                            else
                            {
                                
                                tv_moeny.setText(MainApplication.feeFh
                                    + DateUtil.formatMoneyUtils(Long.parseLong(walletModel.getAmount())));
                            }
                            
                        }
                        break;
                    case 2:
                        tv_type.setText(R.string.tv_list_type_in);
                        tv_detail_type.setText(R.string.tv_list_type_in);
                        if (!StringUtil.isEmptyOrNull(walletModel.getAmount()))
                        {
                            
                            if (walletModel.getAmount().startsWith("-"))
                            {
                                tv_moeny.setText(MainApplication.feeFh
                                    + DateUtil.formatMoneyUtils(Long.parseLong(walletModel.getAmount().substring(1,
                                        walletModel.getAmount().length()))));
                            }
                            else
                            {
                                
                                tv_moeny.setText(MainApplication.feeFh
                                    + DateUtil.formatMoneyUtils(Long.parseLong(walletModel.getAmount())));
                            }
                            
                        }
                        break;
                
                }
            }
            if (!StringUtil.isEmptyOrNull(walletModel.getBank_trans_time()))
            {
                
                try
                {
                    tv_time.setText(walletModel.getBank_trans_time());
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            
        }
    }
    
}
