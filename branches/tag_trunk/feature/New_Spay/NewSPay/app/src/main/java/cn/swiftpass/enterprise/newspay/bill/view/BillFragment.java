package cn.swiftpass.enterprise.newspay.bill.view;


import butterknife.BindView;
import cn.swiftpass.enterprise.R;
import cn.swiftpass.enterprise.base.BaseFragment;
import cn.swiftpass.enterprise.widget.Toolbar;


public class BillFragment extends BaseFragment {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_bill;
    }

    @Override
    protected void initView() {
        mToolbar.setCenterTitle(getString(R.string.tab_bill_title), 0);
        mToolbar.setLeftTitle(null,R.mipmap.icon_scan);
        mToolbar.setRightTitle(getString(R.string.cashier_transaction_tx),0);
    }
}
