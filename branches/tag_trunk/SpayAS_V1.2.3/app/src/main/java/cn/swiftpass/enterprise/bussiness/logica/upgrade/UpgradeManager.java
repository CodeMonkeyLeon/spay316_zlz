package cn.swiftpass.enterprise.bussiness.logica.upgrade;

import android.content.SharedPreferences;

import org.json.JSONObject;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.BaseManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.NotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.NetHelper;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.MD5;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.Utils;

/**
 * User: Alan
 * Date: 13-9-21
 * Time: 下午9:03
 */
public class UpgradeManager extends BaseManager {

    @Override
    public void init() {
    }

    private UpgradeManager() {
        sp = MainApplication.getContext().getApplicationPreferences();
    }

    @Override
    public void destory() {

    }

    private static UpgradeManager instance;

    public static UpgradeManager getInstance() {
        if (instance == null) {
            instance = new UpgradeManager();
        }
        return instance;
    }

    /**
     * 获取服务器版本
     */
    public void getVersonCode(final NotifyListener<UpgradeInfo> listener) {
        ThreadHelper.executeWithCallback(new Executable<UpgradeInfo>() {
            @Override
            public UpgradeInfo execute() throws Exception {
                JSONObject param = new JSONObject();
                //                param.put("verName", AppHelper.getAndroidSDKVersionName());
                param.put("verCode", AppHelper.getVerCode(MainApplication.getContext()));
                param.put("bankCode", ApiConstant.bankCode);

                long spayRs = System.currentTimeMillis();
                //                    //加签名
                param.put("spayRs", String.valueOf(spayRs));
                param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MD5.md5s(String.valueOf(spayRs))));
                param.remove("spayRs");

                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.UPGRADE_CHECKIN, param, String.valueOf(spayRs), null);
                UpgradeInfo info;
                if (result.data == null) {
                    return null;
                }
                Integer rse = Integer.parseInt(result.data.getString("result"));
                //                //测试账号配置信息
                switch (rse) {
                    case 200:
                        String upgradeinfo = result.data.getString("message");
                        info = new UpgradeInfo();
                        JSONObject json = new JSONObject(upgradeinfo);
                        info.dateTime = Utils.Long.tryParse(json.optString("updated_at"), 0);
                        info.message = json.optString("subDesc", "");
                        info.version = Utils.Integer.tryParse(json.optString("verCode"), 0);
                        info.mustUpgrade = Utils.Integer.tryParse(json.optString("isUpdate"), 0) == 0 ? true : false;
                        info.versionName = json.optString("verName");
                        info.url = json.optString("filePath", "");
                        return info;
                    case 400:
                        listener.onSucceed(null);
                        return null;
                    case 402:
                        listener.onSucceed(null);
                        return null;
                    default:
                        listener.onError(rse);
                        return null;
                }

            }
        }, listener);
    }

    /**
     * 下载完成告诉服务器
     */
    public void submitDownloadCount(final int vCode, final NotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable<Boolean>() {
            @Override
            public Boolean execute() throws Exception {
                JSONObject param = new JSONObject();
                //                param.put("imei", AppHelper.getImei(MainApplication.getContext()));
                //                param.put("type", ApiConstant.SPAY_SUB + "");
                param.put("verCode", vCode);
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.UPGRADE_DONWLOAD_FINSH, param);
                if (result.data == null) {
                    return false;
                }
                int rse = Integer.parseInt(result.data.getString("result"));
                if (rse == 200) {

                    return true;
                }
                return false;
            }
        }, listener);
    }

    private SharedPreferences sp;

    /**
     * 测试账号信息
     */
    public void saveTestUserInfo(String name, String pwd, String mId) {
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(GlobalConstant.FILED_T_USERNAME, name);
        edit.putString(GlobalConstant.FILED_T_PWD, pwd);
        edit.putString(GlobalConstant.FILED_T_MID, mId);
        edit.commit();
    }

}
