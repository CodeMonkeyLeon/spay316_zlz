package cn.swiftpass.enterprise.ui.widget;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.PopupWindow;
import cn.swiftpass.enterprise.intl.R;

//欢迎界面的指引图
public class PicPopupWindow extends PopupWindow
{
    
    private View view;
    
    private ImageView iv1, iv2, iv3;
    
    public PicPopupWindow(Context context, int idx)
    {
        super(context);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.popupwindow_welcome, null);
        
        iv1 = (ImageView)view.findViewById(R.id.iv_direction1);
        iv2 = (ImageView)view.findViewById(R.id.iv_direction2);
        iv3 = (ImageView)view.findViewById(R.id.iv_direction3);
        
        switch (idx)
        {
            case 1:
                iv1.setVisibility(View.VISIBLE);
                iv2.setVisibility(View.GONE);
                iv3.setVisibility(View.GONE);
                break;
            case 2:
                iv1.setVisibility(View.GONE);
                iv2.setVisibility(View.VISIBLE);
                iv3.setVisibility(View.GONE);
                break;
            case 3:
                iv1.setVisibility(View.GONE);
                iv2.setVisibility(View.GONE);
                iv3.setVisibility(View.VISIBLE);
                break;
        }
        this.setContentView(view);
        this.setWidth(LayoutParams.MATCH_PARENT);
        this.setHeight(LayoutParams.MATCH_PARENT);
        this.setFocusable(true);//可点击
        //实例化一个ColorDrawable颜色为半透明  
        ColorDrawable dw = new ColorDrawable(0xb0000000);//#8c8c8c  0xb0000000
        //设置SelectPicPopupWindow弹出窗体的背景  
        this.setBackgroundDrawable(dw);
        view.setOnTouchListener(new OnTouchListener()
        {
            
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                dismiss();
                return true;
            }
        });
        
    }
    
}
