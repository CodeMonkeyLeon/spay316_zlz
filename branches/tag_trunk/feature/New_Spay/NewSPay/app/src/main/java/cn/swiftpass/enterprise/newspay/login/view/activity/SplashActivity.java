package cn.swiftpass.enterprise.newspay.login.view.activity;


import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.MainActivity;
import cn.swiftpass.enterprise.R;
import cn.swiftpass.enterprise.newspay.login.view.adapter.GuideAdapter;
import cn.swiftpass.enterprise.util.DisplayUtil;
import cn.swiftpass.enterprise.util.SharedPreferencesUtil;
import cn.swiftpass.enterprise.util.WeakHandler;

public class SplashActivity extends AppCompatActivity {

    private WeakHandler mWeakHandler;
    private final String IS_FIRST_APP = "is_first_launch";
    private ViewPager mViewPager;
    private List<View> mViewList;
    private LinearLayout ll_indicator;
    private TextView tv_skip, tv_next;
    private View view_indicator;
    private int pWidth;
    private int mViewPagerPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //去除title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //去掉Activity上面的状态栏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        initSplash();//进入引导页
//        if (getIsFirstLaunch()) {
//            initSplash();//进入闪屏页
//        } else {
//            initGuide();//进入引导页
//        }
    }

    private void initGuide() {
        setContentView(R.layout.activity_splash);
        tv_next = (TextView) findViewById(R.id.tv_next);
        tv_skip = (TextView) findViewById(R.id.tv_skip);
        mViewPager = (ViewPager) findViewById(R.id.vp_guide);
        ll_indicator = (LinearLayout) findViewById(R.id.ll_indicator);
        view_indicator = (View) findViewById(R.id.view_indicator);

        mViewList = new ArrayList<>();
        LayoutInflater layoutInflater = getLayoutInflater().from(this);

        for (int i = 0; mViewList.size() <= 2; i++) {
            View view_guide = layoutInflater.inflate(R.layout.view_guide, null);
            ImageView iv_guide = (ImageView) view_guide.findViewById(R.id.iv_guide);
            TextView tv_title = (TextView) view_guide.findViewById(R.id.tv_title);
            TextView tv_msg = (TextView) view_guide.findViewById(R.id.tv_msg);
            switch (i) {
                case 0:
                    iv_guide.setBackgroundResource(R.mipmap.img_page1);
                    tv_title.setText(getString(R.string.tv_guid_first_info1));
                    tv_msg.setText(getString(R.string.guide_instruction_content1));
                    break;
                case 1:
                    iv_guide.setBackgroundResource(R.mipmap.img_page2);
                    tv_title.setText(getString(R.string.guide_instruction_title2));
                    tv_msg.setText(getString(R.string.guide_instruction_content2));
                    break;
                case 2:
                    iv_guide.setBackgroundResource(R.mipmap.img_page3);
                    tv_title.setText(getString(R.string.guide_instruction_title3));
                    tv_msg.setText(getString(R.string.guide_instruction_content3));
                    break;
            }
            mViewList.add(view_guide);
        }

        initData();

        mViewPager.setAdapter(new GuideAdapter(mViewList));
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //获取两个点间的距离，获取一次即可
                if (pWidth == 0) {
                    pWidth = ll_indicator.getChildAt(1).getLeft() -
                            ll_indicator.getChildAt(0).getLeft();

                }
                //获取点要移动的距离
                int leftMargin = (int) (pWidth * (position + positionOffset));
                //给选中点设置参数
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view_indicator.getLayoutParams();
                params.leftMargin = leftMargin;
                view_indicator.setLayoutParams(params);
            }

            @Override
            public void onPageSelected(int i) {
                mViewPagerPosition = i;
                if (i == 2) {
                    tv_skip.setVisibility(View.INVISIBLE);
                    tv_next.setText(getString(R.string.guide_login));
                } else {
                    tv_skip.setVisibility(View.VISIBLE);
                    tv_next.setText(R.string.reg_next_step);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        tv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewPagerPosition < 2) {
                    mViewPagerPosition++;
                    mViewPager.setCurrentItem(mViewPagerPosition);
                } else {
                    startNewActivity();
                }
            }
        });
        tv_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNewActivity();
            }
        });

    }

    private void startNewActivity() {
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
        setIsFirstLaunch(true);
        finish();
    }

    private void initData() {
        int[] viewResIDs = {R.layout.view_guide, R.layout.view_guide, R.layout.view_guide};
        //点
        View view;
        // 参数类
        LinearLayout.LayoutParams params;
        for (int i = 0; i < viewResIDs.length; i++) {
            // 根据图片的个数, 每循环一次向LinearLayout中添加一个点
            view = new View(this);
            view.setBackgroundResource(R.drawable.indicator_unchecked);
            // 设置参数
            params = new LinearLayout.LayoutParams(DisplayUtil.dip2px(this, 8),
                    DisplayUtil.dip2px(this, 8));
            //设置点与点之间的间隔
            if (i != 0) {
                params.leftMargin = 10;
            }
            // 添加参数
            view.setLayoutParams(params);
            ll_indicator.addView(view);

        }

    }

    private void initSplash() {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0, 320, 0, 0);
        lp.gravity = Gravity.CENTER;
        ImageView splashImage = new ImageView(this);
        splashImage.setImageResource(R.mipmap.start_app);
        splashImage.setLayoutParams(lp);
        setContentView(splashImage);
        splashImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
            }
        });
        mWeakHandler = new WeakHandler();
        mWeakHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
               // finish();
            }
        }, 500);
    }

    private boolean getIsFirstLaunch() {
        return SharedPreferencesUtil.getInstance().getBoolean(IS_FIRST_APP, false);
    }

    private void setIsFirstLaunch(boolean isFirstLaunch) {
        SharedPreferencesUtil.getInstance().put(IS_FIRST_APP, isFirstLaunch);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mWeakHandler != null) {
            mWeakHandler.removeCallbacksAndMessages(null);
            mWeakHandler = null;
        }

    }
}
