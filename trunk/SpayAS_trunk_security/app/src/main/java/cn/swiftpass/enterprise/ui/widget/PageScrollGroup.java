/*********************************************************
 * @author : hehui
 * @version : 2013-5-2
 * @see : TODO 横滑动视图
 * @Copyright :  @copy 2011-2012
 **********************************************************/
package cn.swiftpass.enterprise.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.Scroller;

public class PageScrollGroup extends ViewGroup {
    //private static final String TAG = "PageScrollGroup";

    // 速度跟踪器
    private VelocityTracker mVelocityTracker;

    // 设置惯性跳页的速度
    private static final int SNAP_VELOCITY = 600;

    public Scroller mScroller;

    // 当钱页的索引
    public int mCurScreen;

    // 设置默认页为第二页
    public int mDefaultScreen = 0;

    // 记录最后接收到的X坐标
    private float mLastMotionX;

    private int mTouchSlop;

    private boolean minClipRect = false;

    /**
     * 非滑动状态
     */
    public static final int TOUCH_STATE_REST = 0;

    /**
     * 滑动状态
     */
    public static final int TOUCH_STATE_SCROLLING = 1;

    public int mTouchState = TOUCH_STATE_REST;

    // 是否正在滑动
    private boolean bScrolling = false;

    // 页面改变时的回调
    private OnViewChangeListener mOnViewChangeListener;

    // 滑屏触屏拦截判断接口,方便处理一个界面内有左右滑动事件响应
    private OnViewInterceptTouchListener mViewInterceptTouchListener = null;

    // 手势检测对象
    private GestureDetector mGestureDetector;

    public PageScrollGroup(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
        init(context);

    }

    public PageScrollGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        init(context);
    }

    public PageScrollGroup(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub

        init(context);
    }

    /**
     * description: 初始化成员变量
     *
     * @param context
     * @throws
     */
    private void init(Context context) {
        mCurScreen = mDefaultScreen;
        mTouchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();
        mScroller = new Scroller(context);
        mGestureDetector = new GestureDetector(new YScrollDetector());
        setFadingEdgeLength(0);
    }

    public void setDefaultScreen(int mDefaultScreen) {
        this.mDefaultScreen = mDefaultScreen;
        mCurScreen = mDefaultScreen;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int childLeft = 0;
        final int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View childView = getChildAt(i);
            if (childView.getVisibility() != View.GONE) {
                final int childWidth = childView.getMeasuredWidth();
                childView.layout(childLeft, 0, childLeft + childWidth, childView.getMeasuredHeight());
                childLeft += childWidth;
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // TODO Auto-generated method stub
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        final int width = MeasureSpec.getSize(widthMeasureSpec);
        // final int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        // final int height = MeasureSpec.getSize(heightMeasureSpec);

        final int count = getChildCount();
        for (int i = 0; i < count; i++) {
            getChildAt(i).measure(widthMeasureSpec, heightMeasureSpec);
        }

        scrollTo(mCurScreen * width, 0);

    }

    public void snapToDestination() {
        final int screenWidth = getWidth();

        final int destScreen = (getScrollX() + screenWidth / 2) / screenWidth;
        //        if (destScreen == 3)
        //        {
        //            snapToScreen(0);
        //        }
        //        else
        //        {
        //        }
        snapToScreen(destScreen);
    }

    /**
     * description: 滑动到whichScreen页
     *
     * @param whichScreen：页面索引
     * @throws
     */
    public void snapToScreen(int whichScreen) {
        // get the valid layout page
        whichScreen = Math.max(0, Math.min(whichScreen, getChildCount() - 1));
        if (getScrollX() != (whichScreen * getWidth())) {

            final int delta = whichScreen * getWidth() - getScrollX();

            mScroller.startScroll(getScrollX(), 0, delta, 0, Math.abs(delta) * 2);

            int nOldscreen = mCurScreen;
            mCurScreen = whichScreen;
            invalidate(); // Redraw the layout

            if (nOldscreen != mCurScreen && mOnViewChangeListener != null) {
                mOnViewChangeListener.OnViewChange(mCurScreen);
            }
        }
    }

    public void snapToScreenAnyWay(int whichScreen) {

        // get the valid layout page
        whichScreen = Math.max(0, Math.min(whichScreen, getChildCount() - 1));

        final int delta = whichScreen * getWidth() - getScrollX();

        mScroller.startScroll(getScrollX(), 0, delta, 0, Math.abs(delta) * 2);

        int nOldscreen = mCurScreen;
        mCurScreen = whichScreen;
        invalidate(); // Redraw the layout

        if (nOldscreen != mCurScreen && mOnViewChangeListener != null) {
            mOnViewChangeListener.OnViewChange(mCurScreen);
        }
    }

    /**
     * description: 滑动到whichScreen页，无动画过程
     *
     * @param whichScreen 页面索引
     * @throws
     */
    public void snapToScreenEx(int whichScreen) {

        // get the valid layout page
        whichScreen = Math.max(0, Math.min(whichScreen, getChildCount() - 1));

        final int delta = whichScreen * getWidth() - getScrollX();

        if (Math.abs(delta) > 0) {
            mScroller.startScroll(getScrollX(), 0, delta, 0, 5);
        }

        int nOldscreen = mCurScreen;
        mCurScreen = whichScreen;
        invalidate(); // Redraw the layout

        if (nOldscreen != mCurScreen && mOnViewChangeListener != null) {
            mOnViewChangeListener.OnViewChange(mCurScreen);
        }
    }

    @Override
    public void computeScroll() {
        // TODO Auto-generated method stub
        if (mScroller.computeScrollOffset()) {
            scrollTo(mScroller.getCurrX(), mScroller.getCurrY());
            postInvalidate();
        }
    }

    private boolean isScroolAble = true;

    public void setScroolAble(boolean scroolAble) {
        this.isScroolAble = scroolAble;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        if (!isScroolAble) {
            return false;
        }

        final int action = event.getAction();
        final float x = event.getX();
        //final float y = event.getY();

        switch (action) {
            case MotionEvent.ACTION_DOWN:

                if (mVelocityTracker == null) {
                    mVelocityTracker = VelocityTracker.obtain();
                    mVelocityTracker.addMovement(event);
                }

                if (!mScroller.isFinished()) {
                    mScroller.abortAnimation();
                }

                mLastMotionX = x;
                break;

            case MotionEvent.ACTION_MOVE:
                int deltaX = (int) (mLastMotionX - x);

                if (isCanMove(deltaX)) {
                    if (mVelocityTracker != null) {
                        mVelocityTracker.addMovement(event);
                    }

                    mLastMotionX = x;

                    if (!bScrolling) {
                        bScrolling = true;
                        if (mViewInterceptTouchListener != null) {
                            mViewInterceptTouchListener.OnCheckScrollState(bScrolling);
                        }
                    }
                    scrollBy(deltaX, 0);
                }

                break;

            case MotionEvent.ACTION_UP:

                int velocityX = 0;
                if (mVelocityTracker != null) {
                    mVelocityTracker.addMovement(event);
                    mVelocityTracker.computeCurrentVelocity(1000);
                    velocityX = (int) mVelocityTracker.getXVelocity();
                }

                if (bScrolling) {
                    bScrolling = false;
                    if (mViewInterceptTouchListener != null) {
                        mViewInterceptTouchListener.OnCheckScrollState(bScrolling);
                    }
                }

                //                if (velocityX > SNAP_VELOCITY && mCurScreen > 0)
                //                {
                //                    // Fling enough to move left
                //                    Log.i("hehui", "MotionEvent.ACTION_UP-snap left-");
                //                    Log.e(TAG, "snap left");
                //                    snapToScreen(mCurScreen - 1);
                //                }
                //                else if (velocityX < -SNAP_VELOCITY && mCurScreen < getChildCount() - 1)
                //                {
                //                    Log.i("hehui", "MotionEvent.ACTION_UP-else if-");
                //                    snapToScreen(mCurScreen + 1);
                //                }

                if (velocityX > SNAP_VELOCITY) {
                    // Fling enough to move left

                    snapToScreen(mCurScreen - 1);
                } else if (velocityX < -SNAP_VELOCITY) {

                    snapToScreen(mCurScreen + 1);
                } else {

                    snapToDestination();
                }

                if (mVelocityTracker != null) {
                    mVelocityTracker.recycle();
                    mVelocityTracker = null;
                }

                break;
        }

        return true;
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        // TODO Auto-generated method stub
        if (!isScroolAble) {
            return false;
        }

        final int action = ev.getAction();
        if (minClipRect
                || (mViewInterceptTouchListener != null && !mViewInterceptTouchListener.OnViewInterceptTouch(ev))) {
            if (!minClipRect && action == MotionEvent.ACTION_DOWN) {
                minClipRect = true;
            }
            if (action == MotionEvent.ACTION_UP) {
                minClipRect = false;
            }
            return false;
        }

        if ((action == MotionEvent.ACTION_MOVE) && (mTouchState != TOUCH_STATE_REST)) {

            return mGestureDetector.onTouchEvent(ev);
        }

        final float x = ev.getX();
        //final float y = ev.getY();
        switch (action) {
            case MotionEvent.ACTION_MOVE:
                final int xDiff = (int) Math.abs(mLastMotionX - x);
                if (xDiff > mTouchSlop) {
                    mTouchState = TOUCH_STATE_SCROLLING;
                }
                break;

            case MotionEvent.ACTION_DOWN:
                mLastMotionX = x;

                mTouchState = mScroller.isFinished() ? TOUCH_STATE_REST : TOUCH_STATE_SCROLLING;
                if (mVelocityTracker == null) {
                    mVelocityTracker = VelocityTracker.obtain();
                    mVelocityTracker.addMovement(ev);
                }
                break;

            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                mTouchState = TOUCH_STATE_REST;
                break;
        }

//        if (mTouchState != TOUCH_STATE_REST)
//        {
//
//        }

        return (mGestureDetector.onTouchEvent(ev) && (mTouchState != TOUCH_STATE_REST));
    }

    public void setOnViewChangeListener(OnViewChangeListener listener) {
        mOnViewChangeListener = listener;
    }

    public void setOnViewInterceptTouchListener(OnViewInterceptTouchListener listener) {
        mViewInterceptTouchListener = listener;
    }

    private boolean isCanMove(int deltaX) {

        /*
         * if (getScrollX() <= 0 && deltaX < 0) { return false; }
         *
         * if (getScrollX() >= (getChildCount() - 1) * getWidth() && deltaX > 0)
         * { return false; }
         */

        return true;
    }

    /**
     * description: 判断当前滑动是否在X轴滑动
     */
    class YScrollDetector extends SimpleOnGestureListener {
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            float fAbsX = Math.abs(distanceX);
            float fAbsY = Math.abs(distanceY);
            if (fAbsY > 0.01f && fAbsX > 0.01f) {
                if (fAbsY / fAbsX < 0.57f)
                    return true;
            }
            return false;
        }
    }

    /***********************************************/
    public interface OnViewChangeListener {
        //pagescroll页面改变通知接口
        public void OnViewChange(int view);
    }

    public interface OnViewInterceptTouchListener {

        // 条件过滤viewgroup Touch事件接口
        public boolean OnViewInterceptTouch(MotionEvent ev);

        // 检测pagescrollgroup状态接口
        public void OnCheckScrollState(boolean bScroll);
    }
    /***********************************************/

}
