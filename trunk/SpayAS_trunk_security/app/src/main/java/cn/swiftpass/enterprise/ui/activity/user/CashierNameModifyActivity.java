/*
 * 文 件 名:  EmpManagerActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2015-3-12
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.example.common.sentry.SentryUtils;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.CashierAddBean;
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants;
import cn.swiftpass.enterprise.mvp.contract.activity.CashierNameModifyContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.CashierNameModifyPresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.ECDHUtils;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 修改名字
 *
 * @author he_hui
 * @version [版本号, 2015-3-12]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class CashierNameModifyActivity extends BaseActivity<CashierNameModifyContract.Presenter> implements CashierNameModifyContract.View {


    private static final String TAG = CashierNameModifyActivity.class.getSimpleName();
    private EditText cashierName;
    private ImageView iv_cashierName;
    private Button bt_changeName;
    private UserModel userModel;

    public static void startActivity(Context context, UserModel userModel) {
        Intent it = new Intent();
        it.setClass(context, CashierNameModifyActivity.class);
        it.putExtra("userModel", userModel);
        context.startActivity(it);
    }

    @Override
    protected CashierNameModifyContract.Presenter createPresenter() {
        return new CashierNameModifyPresenter();
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.cashier_modify_name_view);
        userModel = (UserModel) getIntent().getSerializableExtra("userModel");

        initView();

        if (userModel != null) {
            iv_cashierName.setVisibility(View.VISIBLE);
            cashierName.setText(userModel.getRealname());
            if (!StringUtil.isEmptyOrNull(userModel.getRealname())) {
                cashierName.setSelection(userModel.getRealname().length());
            }

            if (cashierName.getText().toString().length() > 0) {
                iv_cashierName.setVisibility(View.VISIBLE);
                setButtonBg(bt_changeName, true, 0);
            } else {
                iv_cashierName.setVisibility(View.GONE);
                setButtonBg(bt_changeName, false, 0);
            }
        }

    }

    private void initView() {
        cashierName = getViewById(R.id.cashierName);
        iv_cashierName = getViewById(R.id.iv_cashierName);
        bt_changeName = getViewById(R.id.bt_changeName);

        EditTextWatcher editTextWatcher = new EditTextWatcher();

        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (cashierName.isFocused() && cashierName.getText().toString().length() > 0) {
                    iv_cashierName.setVisibility(View.VISIBLE);
                    setButtonBg(bt_changeName, true, 0);
                } else {
                    iv_cashierName.setVisibility(View.GONE);
                    setButtonBg(bt_changeName, false, 0);
                }
            }

        });

        cashierName.addTextChangedListener(editTextWatcher);

        iv_cashierName.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                cashierName.setText("");
            }
        });

        bt_changeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StringUtil.isEmptyOrNull(cashierName.getText().toString())) {
                    toastDialog(CashierNameModifyActivity.this, R.string.tx_surname_notnull, null);
                    cashierName.setFocusable(true);
                    return;
                }

                userModel.setRealname(cashierName.getText().toString());
                submit(userModel);
            }
        });
    }

    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b, enable, res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_press_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }

    /**
     * {@inheritDoc}
     */

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            CashierNameModifyActivity.this.finish();
        }

        return super.onKeyDown(keyCode, event);

    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.et_name);
//        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.save));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onRightButLayClick() {
              /*  if (StringUtil.isEmptyOrNull(cashierName.getText().toString()))
                {
                    toastDialog(CashierNameModifyActivity.this, R.string.tx_surname_notnull, null);
                    cashierName.setFocusable(true);
                    return;
                }
                
                userModel.setRealname(cashierName.getText().toString());
                submit(userModel);*/
            }

            @Override
            public void onLeftButtonClick() {
                CashierNameModifyActivity.this.finish();
            }
        });

    }


    @Override
    public void cashierAddSuccess(@Nullable CashierAddBean response) {
        if (response != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    HandlerManager.notifyMessage(HandlerManager.CASH_NAME,
                            HandlerManager.CASH_NAME,
                            userModel.getRealname());

                    finish();
                }
            });

        }
    }

    @Override
    public void cashierAddFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }
        if (error != null) {

            CashierNameModifyActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (error.toString().startsWith("405")) {//Require to renegotiate ECDH key
                        ECDHKeyExchange(userModel);
                    } else {
                        toastDialog(CashierNameModifyActivity.this, error.toString(), null);
                    }
                }
            });
        }
    }

    private void submit(final UserModel userModel) {
        if (mPresenter != null) {
            mPresenter.cashierAdd(userModel, false);
        }
    }


    @Override
    public void ecdhKeyExchangeSuccess(@Nullable ECDHInfo response, UserModel userModel) {
        if (response != null) {
            try {
                final String privateKey = SharedPreUtils.readProduct(ParamsConstants.APP_PRIVATE_KEY).toString();
                String secretKey = ECDHUtils.getInstance().ecdhGetShareKey(MainApplication.getInstance().getSerPubKey(), privateKey);
                SharedPreUtils.saveObject(secretKey, ParamsConstants.SECRET_KEY);
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                Log.e(TAG, Log.getStackTraceString(e));
            }
            submit(userModel);
        }
    }

    @Override
    public void ecdhKeyExchangeFailed(@Nullable Object error) {

    }

    private void ECDHKeyExchange(final UserModel userModel) {
        try {
            ECDHUtils.getInstance().getAppPubKey();
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
        }
        final String publicKey = SharedPreUtils.readProduct(ParamsConstants.APP_PUBLIC_KEY).toString();

        if (mPresenter != null) {
            mPresenter.ecdhKeyExchange(publicKey, userModel);
        }
    }
}
