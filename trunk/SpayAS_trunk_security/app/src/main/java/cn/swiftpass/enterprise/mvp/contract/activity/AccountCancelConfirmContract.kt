package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView


/**
 * @author lizheng.zhao
 * @date 2022/11/23
 *
 * @生活是一次机会
 * @仅仅一次
 * @谁校对时间
 * @谁就会突然老去
 */
class AccountCancelConfirmContract {


    interface View : BaseView {

        fun checkVerifyCodeAndPasswordSuccess(response: Boolean)

        fun checkVerifyCodeAndPasswordFailed(error: Any?)

    }


    interface Presenter : BasePresenter<View> {


        fun checkVerifyCodeAndPassword(
            emailVerifyCode: String?,
            password: String?
        )
    }

}