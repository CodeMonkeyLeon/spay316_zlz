package cn.swiftpass.enterprise.mvp.contract.fragment

import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView
import cn.swiftpass.enterprise.ui.paymentlink.model.ImageUrl
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkOrder

/**
 * @author lizheng.zhao
 * @date 2022/11/16
 *
 * @你应该是一场梦我应该是一阵风
 */
class PaymentLinkContract {


    interface View : BaseView {

        fun uploadImgSuccess(response: ImageUrl)

        fun uploadImgFailed(error: Any?)


        fun addNewOrderSuccess(response: PaymentLinkOrder)


        fun addNewOrderFailed(error: Any?)


        fun editOrderSuccess(response: String)

        fun editOrderFailed(error: Any?)

    }


    interface Presenter : BasePresenter<View> {


        fun editOrder(order: PaymentLinkOrder?)


        fun addNewOrder(order: PaymentLinkOrder?)


        fun uploadImg(
            imgBase64: String?,
            imgType: String?
        )
    }


}