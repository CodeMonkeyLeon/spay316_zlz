/*
 * 文 件 名:  MarketingActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-1-12
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.marketing;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.order.HistoryRankingManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.AwardModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 详情
 * 
 * @author  he_hui
 * @version  [版本号, 2016-1-12]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class MarketingDetailActivity extends TemplateActivity
{
    
    private LinearLayout logo_lay, ly_state;
    
    private TextView tv_active_title, tv_time, tv_moeny, tv_max_money, tv_min_money, tv_total_money, tv_set_peo_num;
    
    AwardModel awardModel;
    
    private Button bt_confirm;
    
    private TextView tv_state;
    
    private DialogInfo dialogInfo;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marketing_detail);
        
        initView();
        
        awardModel = (AwardModel)getIntent().getSerializableExtra("awardModel");
        
        initValue(awardModel);
        
        setLister();
    }
    
    private void setLister()
    {
        bt_confirm.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                String str = getString(R.string.tx_is_activation);
                if (awardModel.getActiveStatus() == 1)
                {
                    str = getString(R.string.tx_is_stop);
                }
                dialogInfo =
                    new DialogInfo(MarketingDetailActivity.this, getString(R.string.public_cozy_prompt), str,
                        getString(R.string.btnOk), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG,
                        new DialogInfo.HandleBtn()
                        {
                            
                            @Override
                            public void handleOkBtn()
                            {
                                HistoryRankingManager.getInstance().updateActiveStatus(awardModel.getActiveId(),
                                    awardModel.getActiveStatus(),
                                    new UINotifyListener<Boolean>()
                                    {
                                        @Override
                                        public void onPreExecute()
                                        {
                                            super.onPreExecute();
                                            
                                            showNewLoading(true, getString(R.string.public_data_loading));
                                        }
                                        
                                        @Override
                                        public void onError(final Object object)
                                        {
                                            super.onError(object);
                                            dismissLoading();
                                            if (checkSession())
                                            {
                                                return;
                                            }
                                            if (object != null)
                                            {
                                                toastDialog(MarketingDetailActivity.this, object.toString(), null);
                                            }
                                        }
                                        
                                        @Override
                                        public void onSucceed(Boolean result)
                                        {
                                            super.onSucceed(result);
                                            dismissLoading();
                                            
                                            if (result)
                                            {
                                                if (awardModel.getActiveStatus() == 0)
                                                {
                                                    
                                                    //                                                    toastDialog(MarketingDetailActivity.this,
                                                    //                                                        getString(R.string.tx_active) + "'"
                                                    //                                                            + awardModel.getActiveName() + "'"
                                                    //                                                            + getString(R.string.tx_activation),
                                                    //                                                        new NewDialogInfo.HandleBtn()
                                                    //                                                        {
                                                    //                                                            
                                                    //                                                            @Override
                                                    //                                                            public void handleOkBtn()
                                                    //                                                            {
                                                    //                                                                // TODO Auto-generated method stub
                                                    //                                                            }
                                                    //                                                            
                                                    //                                                        });
                                                    finish();
                                                    //                                                    showToastInfo(getString(R.string.tx_active) + "'"
                                                    //                                                        + awardModel.getActiveName() + "'"
                                                    //                                                        + getString(R.string.tx_activation));
                                                }
                                                else if (awardModel.getActiveStatus() == 1)
                                                {
                                                    //                                                    showToastInfo(getString(R.string.tx_active) + "'"
                                                    //                                                        + awardModel.getActiveName() + "'"
                                                    //                                                        + getString(R.string.tx_stop));
                                                    
                                                    //                                                    toastDialog(MarketingDetailActivity.this,
                                                    //                                                        getString(R.string.tx_active) + "'"
                                                    //                                                            + awardModel.getActiveName() + "'"
                                                    //                                                            + getString(R.string.tx_stop),
                                                    //                                                        new NewDialogInfo.HandleBtn()
                                                    //                                                        {
                                                    //                                                            
                                                    //                                                            @Override
                                                    //                                                            public void handleOkBtn()
                                                    //                                                            {
                                                    //                                                            }
                                                    //                                                            
                                                    //                                                        });
                                                    finish();
                                                }
                                                
                                            }
                                            
                                        }
                                    });
                            }
                            
                            @Override
                            public void handleCancleBtn()
                            {
                                dialogInfo.cancel();
                            }
                        }, null);
                
                DialogHelper.resize(MarketingDetailActivity.this, dialogInfo);
                dialogInfo.show();
            }
        });
    }
    
    private void initValue(AwardModel awardModel)
    {
        if (awardModel != null)
        {
            titleBar.setTitle(R.string.tx_active_detai);
            tv_active_title.setText(awardModel.getActiveName());
            tv_moeny.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(awardModel.getOrderFullMoney()));
            tv_max_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(awardModel.getMaxMoney()));
            tv_min_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(awardModel.getMinMoney()));
            tv_total_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(awardModel.getFullMoney()));
            tv_set_peo_num.setText(awardModel.getMaxNum() + "");
            
            tv_time.setText(awardModel.getStartDate() + " " + getString(R.string.tv_least) + " "
                + awardModel.getEndDate());
            ly_state.setVisibility(View.VISIBLE);
            switch (awardModel.getActiveStatus())
            {
                case 0: //未开始
                    logo_lay.setVisibility(View.VISIBLE);
                    tv_state.setText(R.string.tx_no_start);
                    tv_state.setTextColor(getResources().getColor(R.color.pay_qrcode_color));
                    bt_confirm.setText(R.string.tv_activation);
                    bt_confirm.setBackgroundResource(R.drawable.btn_register);
                    break;
                case 1://进行中
                    logo_lay.setVisibility(View.GONE);
                    tv_state.setText(R.string.tx_conduct);
                    tv_state.setTextColor(getResources().getColor(R.color.title_bg_new));
                    bt_confirm.setText(R.string.bt_active_stop);
                    bt_confirm.setBackgroundResource(R.drawable.btn_wihte);
                    bt_confirm.setTextColor(Color.parseColor("#000000"));
                    titleBar.setRightButLayVisibleForTotal(true, getString(R.string.tv_report));
                    break;// 已结束
                case 2:
                    logo_lay.setVisibility(View.GONE);
                    tv_state.setTextColor(getResources().getColor(R.color.pay_qrcode_color));
                    tv_state.setText(R.string.tv_active_over);
                    bt_confirm.setVisibility(View.GONE);
                    titleBar.setRightButLayVisibleForTotal(true, getString(R.string.tv_report));
                    break;
                default:
                    break;
            }
            
        }
    }
    
    private void initView()
    {
        tv_state = getViewById(R.id.tv_state);
        ly_state = getViewById(R.id.ly_state);
        bt_confirm = getViewById(R.id.bt_confirm);
        logo_lay = getViewById(R.id.logo_lay);
        tv_active_title = getViewById(R.id.tv_active_title);
        tv_time = getViewById(R.id.tv_time);
        tv_moeny = getViewById(R.id.tv_moeny);
        tv_max_money = getViewById(R.id.tv_max_money);
        tv_min_money = getViewById(R.id.tv_min_money);
        tv_total_money = getViewById(R.id.tv_total_money);
        tv_set_peo_num = getViewById(R.id.tv_set_peo_num);
    }
    
    public static void startActivity(Context context, AwardModel awardModel)
    {
        Intent it = new Intent();
        it.setClass(context, MarketingDetailActivity.class);
        it.putExtra("awardModel", awardModel);
        context.startActivity(it);
    }
    
    @Override
    protected void onResume()
    {
        super.onResume();
    }
    
    private void loadDate(int page, final boolean isLoadMore)
    {
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.show_add_succ);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                //报表
                //                showPage(MarketingAddActivity.class);
                if (awardModel != null && !StringUtil.isEmptyOrNull(awardModel.getActiveId()))
                {
                    MarketingReportActivity.startActivity(MarketingDetailActivity.this, awardModel);
                }
            }
            
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
        });
    }
    
}
