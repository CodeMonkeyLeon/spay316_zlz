package cn.swiftpass.enterprise.ui.paymentlink.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.paymentlink.model.OtherFee;
import cn.swiftpass.enterprise.ui.widget.BaseRecycleAdapter;
import cn.swiftpass.enterprise.utils.DateUtil;

/**
 * Created by congwei.li on 2021/9/16.
 *
 * @Description: 确定订单信息弹窗 其他费用adapter
 */
public class OtherFeeConfirmAdapter extends BaseRecycleAdapter<OtherFee> {

    private Context mContext;

    public OtherFeeConfirmAdapter(Context context, List<OtherFee> data) {
        super(data);
        mContext = context;
    }

    @Override
    protected void bindData(BaseViewHolder holder, int position) {
        OtherFee data = datas.get(position);

        TextView tvName = (TextView) holder.getView(R.id.tv_desc1);
        TextView tvAmount = (TextView) holder.getView(R.id.tv_desc2);
        View line = holder.getView(R.id.view_line);

        line.setVisibility(View.GONE);
        tvName.setText(data.extraCostDesc);
        tvAmount.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(data.extraCost));
    }

    @Override
    public int getLayoutId() {
        return R.layout.simple_item_view;
    }
}
