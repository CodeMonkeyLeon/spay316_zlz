/**************************************************************
 * @作者: zengsb 
 * @创建时间: 2012-8-30 上午10:55:48 
 * @功能描述: 基本配置类
 * @版权声明:本程序版权归 深圳市时代纬科技有限公司所有 Copy right 2010-2012
 **************************************************************/
package cn.swiftpass.enterprise.utils;

//import org.dom4j.Element;

import android.os.Environment;

public class NewConfig
{
    public static final String CONFIG_NAME = "config.xml";
    
    protected String serverAddr;
    
    protected String serverPort;
    
    // 开发相关设置
    private boolean isDebug;
    
    private String fileSaveRoot;
    
    private String appName;
    
    private int clientType;
    
    private boolean isCreateLocaltionQRcode;
    
    private boolean isOverseasPay; //W.l添加境外支付
    
    private String payGateway;
    
    private String body;
    
    private Integer bankType;
    
    /**
     * @return 返回 bankCode
     */
    public String getBankCode()
    {
        return bankCode;
    }
    
    /**
     * @param 对bankCode进行赋值
     */
    public void setBankCode(String bankCode)
    {
        this.bankCode = bankCode;
    }
    
    private String bankCode;
    
    /**
     * @return 返回 bankType
     */
    public Integer getBankType()
    {
        return bankType;
    }
    
    /**
     * @param 对bankType进行赋值
     */
    public void setBankType(Integer bankType)
    {
        this.bankType = bankType;
    }
    
    /**
     * @return 返回 body
     */
    public String getBody()
    {
        return body;
    }
    
    /**
     * @param 对body进行赋值
     */
    public void setBody(String body)
    {
        this.body = body;
    }
    
    /**
     * @return 返回 payGateway
     */
    public String getPayGateway()
    {
        return payGateway;
    }
    
    /**
     * @param 对payGateway进行赋值
     */
    public void setPayGateway(String payGateway)
    {
        this.payGateway = payGateway;
    }
    
    public int getClientType()
    {
        return clientType;
    }
    
    public void setClientType(int clientType)
    {
        this.clientType = clientType;
    }
    
    public String getAppName()
    {
        return appName;
    }
    
    public void setAppName(String appName)
    {
        this.appName = appName;
    }
    
    public String getServerPort()
    {
        return serverPort;
    }
    
    public void setServerPort(String serverPort)
    {
        this.serverPort = serverPort;
    }
    
    public String getServerAddr()
    {
        return serverAddr;
    }
    
    public void setServerAddr(String serverAddr)
    {
        this.serverAddr = serverAddr;
    }
    
    public String getFileSaveRoot()
    {
        return Environment.getExternalStorageDirectory().getAbsolutePath() + fileSaveRoot;
    }
    
    public void setFileSaveRoot(String fileSaveRoot)
    {
        this.fileSaveRoot = fileSaveRoot;
    }
    
    public void setDebug(boolean isDebug)
    {
        this.isDebug = isDebug;
    }
    
    public boolean isCreateLocaltionQRcode()
    {
        return isCreateLocaltionQRcode;
    }
    
    public void setCreateLocaltionQRcode(boolean createLocaltionQRcode)
    {
        isCreateLocaltionQRcode = createLocaltionQRcode;
    }
    
    public boolean isDebug()
    {
        return isDebug;
    }
    
    public boolean isOverseasPay()
    {
        return isOverseasPay;
    }
    
    public void setOverseasPay(boolean isOverseasPay)
    {
        this.isOverseasPay = isOverseasPay;
    }
    
    /** 
     * 解析Bean节点 
     * @param obj_el 
     * @return 
     */
   /* private static NewConfig parseObj(Element obj_el)
    {
        Log.i("hehi", "obj_el-->" + obj_el);
        NewConfig obj = new NewConfig();
        obj.setAppName(obj_el.attributeValue("appName"));
        obj.setBankCode(obj_el.attributeValue("bankCode"));
        return obj;
    }*/
    
    //    public static NewConfig readConfig(Context context)
    //    {
    //        Log.i("hehui", "readConfig NewConfig start");
    //        
    //        Document doc = null;
    //        SAXReader reader = new SAXReader();
    //        NewConfig config = new NewConfig();
    //        try
    //        {
    //            InputStream in = context.getResources().getAssets().open("config.xml");
    //            InputStream is = IOHelper.fromInputStreamToInputStreamInCharset(in,"utf-8");
    //            SAXReader sr = new SAXReader();// 获取读取xml的对象。
    //            Document document = sr.read(is);
    //            Element root = document.getRootElement();
    //            
    //            if (list != null && list.size() > 0)
    //            {
    //                for (int i = 0; i < list.size(); i++)
    //                {
    //                    Element obj_el = (Element)list.get(i);
    //                    Log.i("hehui", "Element-->" + obj_el);
    //                    //解析obj 节点  
    //                    config = parseObj(obj_el);
    //                }
    //            }
    //        }
    //        catch (DocumentException e)
    //        {
    //            // TODO Auto-generated catch block
    //            Log.e("hehui", "readConfig-->" + e);
    //        }
    //        
    //        return new NewConfig();
    //    }
}
