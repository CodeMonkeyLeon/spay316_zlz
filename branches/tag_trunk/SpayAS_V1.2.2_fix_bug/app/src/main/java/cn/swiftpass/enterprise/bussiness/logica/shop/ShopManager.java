package cn.swiftpass.enterprise.bussiness.logica.shop;

import java.util.List;

import org.json.JSONObject;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.bussiness.model.ShopModel;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.io.database.access.ShopInfoDB;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.NetHelper;
import cn.swiftpass.enterprise.utils.Utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 店铺管理
 * User: Alan
 * Date: 13-12-23
 * Time: 下午1:46
 * To change this template use File | Settings | File Templates.
 */
public class ShopManager
{
    
    public static ShopManager getInstance()
    {
        return Container.instance;
    }
    
    private static class Container
    {
        public static ShopManager instance = new ShopManager();
    }
    
    /**添加店铺*/
    public void addShow(final ShopModel shopModel, final boolean isAdd, final String picPath,
        final UINotifyListener<Boolean> listener)
    {
        ThreadHelper.executeWithCallback(new Executable()
        {
            @Override
            public Boolean execute()
                throws Exception
            {
                UserModel userModel = LocalAccountManager.getInstance().getLoggedUser();
                //删除之前的缓存数据
                ShopInfoDB.getInstance().deleteById();
                JSONObject json = new JSONObject();
                json.put("userId", userModel.uId + "");
                json.put("merId", userModel.merId + "");
                json.put("shopName", shopModel.shopName);
                json.put("shopTypeId", shopModel.shopTypeId + "");
                json.put("startTime", shopModel.strStartTime); //MM-DD营业开始时间
                json.put("endTime", shopModel.strEndTime); //MM-DD营业开始时间
                json.put("address", shopModel.address);
                json.put("shopPic", shopModel.shopPic);
                json.put("linkPhone", shopModel.linkPhone + "");
                json.put("city", shopModel.city);
                json.put("trafficInfo", shopModel.trafficInfo);
                json.put("attachInfo", shopModel.attachInfo);
                String url;
                if (isAdd)
                {
                    url = ApiConstant.BASE_URL_PORT + ApiConstant.SHOP_ADD;
                }
                else
                {
                    url = ApiConstant.BASE_URL_PORT + ApiConstant.SHOP_UPDATE;
                    json.put("shopId", shopModel.shopId + "");
                }
                RequestResult result = NetHelper.httpsPost(url, json, shopModel.shopPic, picPath);
                result.setNotifyListener(listener);
                if (!result.hasError())
                {
                    
                    int res = Utils.Integer.tryParse(result.data.getString("result"), -1);
                    switch (res)
                    {
                        case 0:
                            if (isAdd)
                            {
                                MainApplication.shopID = result.data.getString("shopId");
                            }
                            
                            return true;
                        case -1:
                            listener.onError("保存店铺失败！");
                            return false;
                        case 1:
                            listener.onError("参数错误！");
                            return false;
                        case 2:
                            listener.onError("用户无效！");
                            return false;
                        case 3:
                            //                            listener.onError("商户无效，请联系人代理商！");
                            listener.onError("商户还未终审通过不能够完善资料。");
                            return false;
                        case 4:
                            listener.onError("操作失败，稍候再试！");
                            return false;
                    }
                }
                
                return false;
            }
        }, listener);
    }
    
    /**查询店铺*/
    public void searchShop(final int currentPage, final Integer shopId, final String shopName,
        final UINotifyListener<ShopModel> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<ShopModel>()
        {
            @Override
            public ShopModel execute()
                throws Exception
            {
                UserModel userModel = LocalAccountManager.getInstance().getLoggedUser();
                /*
                CacheModel cacheModel = new CacheModel();
                 cacheModel.addTime = System.currentTimeMillis();
                 cacheModel.type = CacheEnum.SHOP_TYPE.getValue();
                 cacheModel.uId = LocalAccountManager.getInstance().getUID();
                 //本地有缓存先去取本地数据
                 boolean isCache = CacheInfoManager.getInstence().isUpdate(cacheModel);
                 if (isCache) {
                 }*/
                
                ShopModel shopModel = null;
                //                = ShopInfoDB.getInstance().queryShopInfo(userModel.uId);
                //                if (shopModel != null)
                //                {
                //                    return shopModel;
                //                }
                
                JSONObject json = new JSONObject();
                json.put("userId", userModel.uId + "");
                json.put("merId", userModel.merId + "");
                json.put("pageSize", ApiConstant.PAGE_SIZE + "");
                json.put("currentPage", currentPage + "");
                if (shopName != null)
                {
                    json.put("shopName", shopName);
                }
                if (shopId != null)
                {
                    json.put("shopId", shopId); //查询店铺详细的时候需要
                }
                RequestResult result =
                    NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.SHOP_SEARCH, json, null, null);
                result.setNotifyListener(listener);
                if (!result.hasError())
                {
                    int res = Utils.Integer.tryParse(result.data.getString("result"), -1);
                    switch (res)
                    {
                        case 0:
                            String strList = result.data.getString("list");
                            Gson gson = new Gson();
                            List<ShopModel> shopModels = gson.fromJson(strList, new TypeToken<List<ShopModel>>()
                            {
                            }.getType());
                            //缓存下
                            if (shopModels.size() > 0)
                            {
                                shopModel = shopModels.get(0);
                                //                                try
                                //                                {
                                //                                    ShopInfoDB.getInstance().save(shopModels.get(0));
                                //                                }
                                //                                catch (Exception e)
                                //                                {
                                //                                    e.printStackTrace();
                                //                                }
                                //                                HandlerManager.notifyMessage(HandlerManager.SHOPDATA, HandlerManager.SHOPDATA, obj) ;
                            }
                            
                            return shopModel;
                        case -1:
                            listener.onError("保存店铺失败！");
                            break;
                        case 1:
                            listener.onError("参数错误！");
                            break;
                        case 2:
                            listener.onError("用户无效！");
                            break;
                        case 3:
                            listener.onError("商户无效！");
                            break;
                        case 4:
                            listener.onError("操作失败，稍候再试！");
                            break;
                    }
                    
                }
                return null;
            }
        },
            listener);
    }
    
    /**删除店铺*/
    public void deleteShop(final int shopId, final UINotifyListener<Boolean> listener)
    {
        ThreadHelper.executeWithCallback(new Executable<Boolean>()
        {
            @Override
            public Boolean execute()
                throws Exception
            {
                UserModel userModel = LocalAccountManager.getInstance().getLoggedUser();
                JSONObject json = new JSONObject();
                json.put("uId", userModel.uId + "");
                json.put("shopId", shopId + "");
                RequestResult result =
                    NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.SHOP_SEARCH, json, null, null);
                result.setNotifyListener(listener);
                if (!result.hasError())
                {
                    int res = Utils.Integer.tryParse(result.data.getString("result"), -1);
                    switch (res)
                    {
                        case 0:
                            return true;
                        case -1:
                            //                            listener.onError(MainApplication.getContext()
                            //                                .getString(cn.swiftpass.enterprise.R.string.msg_delete_shop_fail_3));
                            return false;
                            //                        case 1:
                            //                            listener.onError(MainApplication.getContext().getString(R.string.msg_delete_shop_fail_1));
                            //                            return false;
                            //                        case 2:
                            //                            listener.onError(MainApplication.getContext().getString(R.string.msg_delete_shop_fail_2));
                            //                            return false;
                            //                        case 3:
                            //                            listener.onError(MainApplication.getContext().getString(R.string.msg_delete_shop_fail_3));
                            //                            return false;
                    }
                    
                }
                return false;
            }
        },
            listener);
    }
    
}
