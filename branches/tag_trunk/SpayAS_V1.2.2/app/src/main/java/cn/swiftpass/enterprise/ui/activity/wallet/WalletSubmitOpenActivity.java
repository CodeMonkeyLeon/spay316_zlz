/*
 * 文 件 名:  WalletMainActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-1
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.wallet;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.wallet.WalletManager;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.total.PayDistributionActivity;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 开通电子钱包 提交
 * 
 * @author  he_hui
 * @version  [版本号, 2016-11-1]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class WalletSubmitOpenActivity extends TemplateActivity
{
    
    private Button btn_next_step;
    
    private TextView tx_peop, tv_pass, bt_code, tv_tel;
    
    private EditText et_id;
    
    private TimeCount time;
    
    private String phone;
    
    /**
     *验证码倒计时60秒
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void Countdown()
    {
        // 总共60 没间隔1秒
        time = new TimeCount(60000, 1000);
        time.start();
    }
    
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        
        if (null != time)
        {
            time.cancel();
        }
    }
    
    class TimeCount extends CountDownTimer
    {
        public TimeCount(long millisInFuture, long countDownInterval)
        {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }
        
        @Override
        public void onFinish()
        {//计时完毕时触发
            bt_code.setEnabled(true);
            bt_code.setClickable(true);
            bt_code.setText(R.string.bt_code_get);
            bt_code.setTextColor(getResources().getColor(R.color.title_bg_new));
            tv_tel.setTextColor(getResources().getColor(R.color.title_bg_new));
        }
        
        @Override
        public void onTick(long millisUntilFinished)
        {//计时过程显示
            bt_code.setEnabled(false);
            bt_code.setClickable(false);
            tv_tel.setTextColor(getResources().getColor(R.color.user_edit_color));
            bt_code.setText(millisUntilFinished / 1000 + "s" + getString(R.string.bt_code_get1));
        }
    }
    
    public static void startActivity(Context context, String tel)
    {
        Intent it = new Intent();
        it.setClass(context, WalletSubmitOpenActivity.class);
        it.putExtra("tel", tel);
        context.startActivity(it);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_apply_open_submit_layout);
        
        initView();
        setLister();
        btn_next_step.getBackground().setAlpha(102);
        //        Countdown();
        MainApplication.listActivities.add(this);
        
        phone = getIntent().getStringExtra("tel");
        
        tv_tel.setText(phone);
        
        loadewalletSignApply();
    }
    
    /**
     * 提交再一次确认开通
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void toSumit()
    {
        WalletManager.getInstance().ewalletConfirm(et_id.getText().toString(), phone, new UINotifyListener<Boolean>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                //                btn_next_step.setText(R.string.bt_finishing);
                //                btn_next_step.setEnabled(false);
                setButtonBg(btn_next_step, false, R.string.bt_finishing);
                loadDialog(WalletSubmitOpenActivity.this, R.string.reg_next_step_loading);
            }
            
            @Override
            public void onPostExecute()
            {
                super.onPostExecute();
            }
            
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                dissDialog();
                if (checkSession())
                {
                    return;
                }
                WalletSubmitOpenActivity.this.runOnUiThread(new Runnable()
                {
                    
                    @Override
                    public void run()
                    {
                        //                        btn_next_step.setEnabled(true);
                        //                        btn_next_step.setText(R.string.bt_complete);
                        setButtonBg(btn_next_step, true, R.string.bt_complete);
                    }
                });
                if (null != object)
                {
                    toastDialog(WalletSubmitOpenActivity.this, object.toString(), null);
                }
                
            }
            
            @Override
            public void onSucceed(Boolean model)
            {
                dissDialog();
                if (model)
                {
                    
                    if (MainApplication.listActivities.size() > 0)
                    {
                        for (Activity activity : MainApplication.listActivities)
                        {
                            activity.finish();
                        }
                    }
                    showPage(WalletApplySuccActivity.class);
                }
            }
        });
    }
    
    private void setLister()
    {
        btn_next_step.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (null != time)
                {
                    time.cancel();
                }
                
                if (StringUtil.isEmptyOrNull(et_id.getText().toString()))
                {
                    toastDialog(WalletSubmitOpenActivity.this, getString(R.string.bt_code_not_null), null);
                    et_id.setFocusable(true);
                    return;
                }
                
                //                
                
                toSumit();
                
            }
        });
        
        tv_pass.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                NewDialogInfo info =
                    new NewDialogInfo(WalletSubmitOpenActivity.this, null, null, NewDialogInfo.WALLRT_PHONE_PROMT,
                        null, new NewDialogInfo.HandleBtn()
                        {
                            
                            @Override
                            public void handleOkBtn()
                            {
                            }
                            
                        });
                
                DialogHelper.resize(WalletSubmitOpenActivity.this, info);
                
                info.show();
            }
        });
        
        bt_code.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                loadewalletSignApply();
            }
        });
    }
    
    private void loadewalletSignApply()
    {
        if (StringUtil.isEmptyOrNull(phone))
        {
            return;
        }
        WalletManager.getInstance().ewalletSignApply(phone, new UINotifyListener<Boolean>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                loadDialog(WalletSubmitOpenActivity.this, R.string.dialog_message);
            }
            
            @Override
            public void onPostExecute()
            {
                super.onPostExecute();
            }
            
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                dissDialog();
                if (checkSession())
                {
                    return;
                }
                if (null != object)
                {
                    WalletSubmitOpenActivity.this.runOnUiThread(new Runnable()
                    {
                        
                        @Override
                        public void run()
                        {
                            bt_code.setEnabled(true);
                            bt_code.setClickable(true);
                            bt_code.setText(R.string.bt_code_get);
                            bt_code.setTextColor(getResources().getColor(R.color.tx_blue));
                        }
                    });
                    toastDialog(WalletSubmitOpenActivity.this, object.toString(), null);
                }
            }
            
            @Override
            public void onSucceed(Boolean model)
            {
                dissDialog();
                if (model)
                {
                    Countdown();
                    // toastDialog(WalletSubmitOpenActivity.this, R.string.tx_vercode_message, null);
                }
            }
        });
    }
    
    private void initView()
    {
        btn_next_step = getViewById(R.id.btn_next_step);
        tx_peop = getViewById(R.id.tx_peop);
        et_id = getViewById(R.id.et_id);
        tv_pass = getViewById(R.id.tv_pass);
        bt_code = getViewById(R.id.textView1);
        tv_tel = getViewById(R.id.tv_tel);
        
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        {
            
            @Override
            public void onExecute(CharSequence s, int start, int before, int count)
            {
            }
            
            @Override
            public void onAfterTextChanged(Editable s)
            {
                if (et_id.isFocused())
                {
                    if (et_id.getText().toString().length() > 0)
                    {
                        //                        btn_next_step.setBackgroundResource(R.drawable.btn_register);
                        //                        btn_next_step.setEnabled(true);
                        setButtonBg(btn_next_step, true, R.string.bt_complete);
                    }
                    else
                    {
                        //                        btn_next_step.setEnabled(false);
                        //                        btn_next_step.setBackgroundResource(R.drawable.general_button_grey_default);
                        setButtonBg(btn_next_step, false, R.string.bt_complete);
                    }
                }
            }
        });
        
        et_id.addTextChangedListener(editTextWatcher);
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        
        titleBar.setTitle(R.string.tv_code_info);
        titleBar.setLeftButtonIsVisible(true);
        titleBar.setLeftButtonVisible(true);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                showPage(PayDistributionActivity.class);
            }
        });
    }
}
