package com.bigkoo.pickerview.utils

import com.example.common.sentry.SentryUtils
import com.example.common.sentry.SentryUtils.uploadTryCatchException
import java.text.SimpleDateFormat
import java.util.*

object Utils {

    const val TYPE_YEAR = 1000
    const val TYPE_MONTH = 1001
    const val TYPE_DAY = 1002
    const val TYPE_HOURS = 1003
    const val TYPE_MINUTE = 1004
    const val TYPE_SECOND = 1005


    /**
     * 获取当天零点时间
     *
     * @return
     */
    fun getTodayZero(): Date? {
        val date = Date()
        val l = (24 * 60 * 60 * 1000).toLong() //每天的毫秒数
        //date.getTime()是现在的毫秒数，它 减去 当天零点到现在的毫秒数（ 现在的毫秒数%一天总的毫秒数，取余。），理论上等于零点的毫秒数，不过这个毫秒数是UTC+0时区的。
        //减8个小时的毫秒值是为了解决时区的问题。
        val zeroTime = date.time - date.time % l - 8 * 60 * 60 * 1000
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm")
        return format.parse(format.format(Date(zeroTime)))
    }


    fun stringToDate(strTime: String): Date {
        try {
            val sd = SimpleDateFormat("yyyy-MM-dd HH:mm")
            return sd.parse(strTime)
        } catch (e: Exception) {
            uploadTryCatchException(
                e,
                SentryUtils.getClassNameAndMethodName()
            )
        }
        return Date()
    }


    /**
     * long类型转换成日期
     *
     * @param lo 毫秒数
     * @return String yyyy-MM-dd
     */
    fun longToDate(lo: Long): Date? {
        val sd = SimpleDateFormat("yyyy-MM-dd HH:mm")
        //long转Date
        return sd.parse(sd.format(Date(lo)))
    }


    /**
     *
     * @return String
     */
    fun dateToString(date: Date): String? {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm")
        return simpleDateFormat.format(date)
    }


    fun longToString(time: Long): String? {
        val date = longToDate(time)
        date?.let { d ->
            return dateToString(d)
        }
        return ""
    }
}