package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import androidx.annotation.Nullable;

import com.example.common.sentry.SentryUtils;
import com.example.common.sp.PreferenceUtil;
import com.ziyeyouhu.library.KeyboardTouchListener;
import com.ziyeyouhu.library.KeyboardUtil;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants;
import cn.swiftpass.enterprise.mvp.contract.activity.ChangePwdContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.ChangePwdPresenter;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.ECDHUtils;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.KotlinUtils;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 修改密码
 * User: Administrator
 * Date: 13-10-16
 * Time: 下午4:20
 * To change this template use File | Settings | File Templates.
 */
public class ChangePwdActivity extends BaseActivity<ChangePwdContract.Presenter> implements View.OnClickListener, ChangePwdContract.View {
    private static final String TAG = ChangePwdActivity.class.getSimpleName();

    private EditText etOldPwd, etNewPwd, etNewPwd2;

    private ImageView iv_clearOldpwd, iv_clearNewpwd, iv_clearNewpwd2;

    private Button btn_login;

    private DialogInfo dialogInfo;


    private LinearLayout rootView;
    private KeyboardUtil keyboardUtil;
    private ScrollView scrollView;

    public static void startActivity(Context context) {
        Intent it = new Intent();
        it.setClass(context, ChangePwdActivity.class);
        context.startActivity(it);
    }

    //安全键盘
    private void initMoveKeyBoard() {
        rootView = findViewById(R.id.rootview);
        scrollView = findViewById(R.id.scrollview);
        keyboardUtil = new KeyboardUtil(this, rootView, scrollView);
        // monitor the KeyBarod state
        keyboardUtil.setKeyBoardStateChangeListener(new ChangePwdActivity.KeyBoardStateListener());
        // monitor the finish or next Key
        keyboardUtil.setInputOverListener(new ChangePwdActivity.inputOverListener());
        etOldPwd.setOnTouchListener(new KeyboardTouchListener(keyboardUtil, KeyboardUtil.INPUT_TYPE_ABC, -1));
        etNewPwd.setOnTouchListener(new KeyboardTouchListener(keyboardUtil, KeyboardUtil.INPUT_TYPE_ABC, -1));
        etNewPwd2.setOnTouchListener(new KeyboardTouchListener(keyboardUtil, KeyboardUtil.INPUT_TYPE_ABC, -1));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (keyboardUtil.isShow) {
                keyboardUtil.hideSystemKeyBoard();
                keyboardUtil.hideAllKeyBoard();
                keyboardUtil.hideKeyboardLayout();

                finish();
            } else {
                return super.onKeyDown(keyCode, event);
            }

            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();

        initMoveKeyBoard();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                keyboardUtil.showKeyBoardLayout(etOldPwd, KeyboardUtil.INPUT_TYPE_ABC, -1);
            }
        }, 200);

    }

    @Override
    protected void onPause() {
        super.onPause();
        etOldPwd.setText("");
        etNewPwd.setText("");
        etNewPwd2.setText("");
        etOldPwd.requestFocus();
    }

    private void initView() {
        setContentView(R.layout.activity_changepwd);

        btn_login = getViewById(R.id.btn_login);

        etOldPwd = getViewById(R.id.et_oldpwd);
        etNewPwd = getViewById(R.id.et_newpwd);
        etNewPwd2 = getViewById(R.id.et_newpwd2);

        btn_login.getBackground().setAlpha(102);
        iv_clearOldpwd = getViewById(R.id.iv_clearOld);
        iv_clearOldpwd.setOnClickListener(this);
        iv_clearNewpwd = getViewById(R.id.iv_clearNewpwd);
        iv_clearNewpwd.setOnClickListener(this);
        iv_clearNewpwd2 = getViewById(R.id.iv_clearNewpwd2);
        iv_clearNewpwd2.setOnClickListener(this);

        EditTextWatcher editTextWatcher = new EditTextWatcher();

        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {

                if (s != null && s.length() >= 1) {
                    if (etOldPwd.isFocused()) {
                        iv_clearOldpwd.setVisibility(View.VISIBLE);
                        iv_clearNewpwd.setVisibility(View.GONE);
                        iv_clearNewpwd2.setVisibility(View.GONE);
                    } else if (etNewPwd.isFocused()) {
                        iv_clearNewpwd.setVisibility(View.VISIBLE);
                        iv_clearOldpwd.setVisibility(View.GONE);
                        iv_clearNewpwd2.setVisibility(View.GONE);
                    } else if (etNewPwd2.isFocused()) {
                        iv_clearNewpwd2.setVisibility(View.VISIBLE);
                        iv_clearNewpwd.setVisibility(View.GONE);
                        iv_clearOldpwd.setVisibility(View.GONE);
                    }
                } else {
                    if (etOldPwd.isFocused()) {
                        iv_clearOldpwd.setVisibility(View.GONE);
                        iv_clearNewpwd.setVisibility(View.GONE);
                        iv_clearNewpwd2.setVisibility(View.GONE);
                    } else if (etNewPwd.isFocused()) {
                        iv_clearOldpwd.setVisibility(View.GONE);
                        iv_clearNewpwd.setVisibility(View.GONE);
                        iv_clearNewpwd2.setVisibility(View.GONE);
                    } else if (etNewPwd2.isFocused()) {
                        iv_clearOldpwd.setVisibility(View.GONE);
                        iv_clearNewpwd.setVisibility(View.GONE);
                        iv_clearNewpwd2.setVisibility(View.GONE);
                    }
                }

                if (!StringUtil.isEmptyOrNull(etOldPwd.getText().toString())
                        && !StringUtil.isEmptyOrNull(etNewPwd.getText().toString())
                        && !StringUtil.isEmptyOrNull(etNewPwd2.getText().toString())) {
                    setButtonBg(btn_login, true, 0);
                } else {
                    setButtonBg(btn_login, false, 0);
                }
            }
        });
        etOldPwd.addTextChangedListener(editTextWatcher);
        etNewPwd.addTextChangedListener(editTextWatcher);
        etNewPwd2.addTextChangedListener(editTextWatcher);

        etOldPwd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    if (etOldPwd.getText().length() >= 1) {
                        iv_clearOldpwd.setVisibility(View.VISIBLE);
                    } else {
                        iv_clearOldpwd.setVisibility(View.GONE);
                    }
                } else {
                    iv_clearOldpwd.setVisibility(View.GONE);
                }
            }
        });

        etNewPwd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    if (etNewPwd.getText().length() >= 1) {
                        iv_clearNewpwd.setVisibility(View.VISIBLE);
                    } else {
                        iv_clearNewpwd.setVisibility(View.GONE);
                    }
                } else {
                    iv_clearNewpwd.setVisibility(View.GONE);
                }
            }
        });
        etNewPwd2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    if (etNewPwd2.getText().length() >= 1) {
                        iv_clearNewpwd2.setVisibility(View.VISIBLE);
                    } else {
                        iv_clearNewpwd2.setVisibility(View.GONE);
                    }
                } else {
                    iv_clearNewpwd2.setVisibility(View.GONE);
                }
            }
        });

        /**
         * 可配置设置
         */
        DynModel dynModel = (DynModel) SharedPreUtils.readProduct("dynModel" + BuildConfig.bankCode);
        if (null != dynModel) {
            try {
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonColor())) {
                    btn_login.setBackgroundColor(Color.parseColor(dynModel.getButtonColor()));
                }

                if (!StringUtil.isEmptyOrNull(dynModel.getButtonFontColor())) {
                    btn_login.setTextColor(Color.parseColor(dynModel.getButtonFontColor()));
                }
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                Log.e(TAG, Log.getStackTraceString(e));
            }
        }
    }

    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b, enable, res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_press_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(R.string.title_change_pwd);
        titleBar.setLeftButtonVisible(true);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {
                // TODO Auto-generated method stub

            }

            @Override
            public void onRightButLayClick() {
                // TODO Auto-generated method stub

            }
        });
    }

    public void onChangePwd(View v) {

        if (TextUtils.isEmpty(etOldPwd.getText())) {

            etOldPwd.setFocusable(true);
            toastDialog(ChangePwdActivity.this, R.string.show_current_psw, null);
            return;
        }
        if (TextUtils.isEmpty(etNewPwd.getText())) {
            etNewPwd.setFocusable(true);
            toastDialog(ChangePwdActivity.this, R.string.show_new_psw, null);
            return;
        }
        if (TextUtils.isEmpty(etNewPwd2.getText())) {
            etNewPwd2.setFocusable(true);
            toastDialog(ChangePwdActivity.this, R.string.show_confirm_pass, null);
            return;
        }


        final String oldPwd = etOldPwd.getText().toString();
        final String newPwd = etNewPwd.getText().toString();
        final String newPwd2 = etNewPwd2.getText().toString();

        if (etNewPwd.getText().toString().length() < 8) {
            etNewPwd.setFocusable(true);
            toastDialog(ChangePwdActivity.this, R.string.psw_instruction, null);
            return;
        }

        //校验新密码是否符合8-16位字符
        if (!KotlinUtils.INSTANCE.isNewVersion() && !isContainAll(newPwd)) {
            etNewPwd.setFocusable(true);
            toastDialog(ChangePwdActivity.this, R.string.et_new_pass, null);
            return;
        }

        if (!newPwd.equals(newPwd2)) {
            etNewPwd2.setFocusable(true);
            toastDialog(ChangePwdActivity.this, R.string.psw_not_match_instruction, null);
            return;
        }

        if (oldPwd.equals(newPwd2)) {
            etNewPwd2.setFocusable(true);
            etNewPwd.setFocusable(true);
            toastDialog(ChangePwdActivity.this, R.string.old_psw_match_instruction, null);
            return;
        }
        dialogInfo = new DialogInfo(ChangePwdActivity.this, getString(R.string.public_cozy_prompt),
                getString(R.string.tv_modify_pass), getString(R.string.bt_confirm), getString(R.string.btnCancel),
                DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {
            @Override
            public void handleOkBtn() {
                changePsw();
            }

            @Override
            public void handleCancelBtn() {
                dialogInfo.cancel();
            }
        }, null);

        DialogHelper.resize(ChangePwdActivity.this, dialogInfo);
        dialogInfo.show();

    }


    @Override
    public void changePwdSuccess(boolean response) {
        if (response) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showToastInfo(R.string.show_pass_succ);
                    PreferenceUtil.removeKey("login_skey");
                    PreferenceUtil.removeKey("login_sauthid");
                    MainApplication.getInstance().setUserInfo(null);
                    showPage(WelcomeActivity.class);
                    for (Activity a : MainApplication.getInstance().getAllActivities()) {
                        a.finish();
                    }
                    finish();
                }
            });
        }
    }

    @Override
    public void changePwdFailed(@Nullable Object error) {
        if (error != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (error.toString().startsWith("405")) {//Require to renegotiate ECDH key
                        ECDHKeyExchange();
                    } else {
                        toastDialog(ChangePwdActivity.this, error.toString(), null);
                    }

                }
            });

        }
    }

    public void changePsw() {
        final String oldPwd = etOldPwd.getText().toString();
        final String newPwd = etNewPwd.getText().toString();
        final String newPwd2 = etNewPwd2.getText().toString();

        if (mPresenter != null) {
            mPresenter.changePwd(oldPwd,
                    MainApplication.getInstance().getUserInfo().phone,
                    newPwd,
                    newPwd2);
        }
    }


    @Override
    public void ecdhKeyExchangeSuccess(ECDHInfo response) {
        if (response != null) {
            try {
                final String privateKey = SharedPreUtils.readProduct(ParamsConstants.APP_PRIVATE_KEY).toString();
                String secretKey = ECDHUtils.getInstance().ecdhGetShareKey(MainApplication.getInstance().getSerPubKey(), privateKey);
                SharedPreUtils.saveObject(secretKey, ParamsConstants.SECRET_KEY);
                //再去请求一次修改密码接口
                changePsw();
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                Log.e(TAG, Log.getStackTraceString(e));
            }
        }
    }

    @Override
    public void ecdhKeyExchangeFailed(@Nullable Object error) {

    }

    private void ECDHKeyExchange() {
        try {
            ECDHUtils.getInstance().getAppPubKey();
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
        }
        final String publicKey = SharedPreUtils.readProduct(ParamsConstants.APP_PUBLIC_KEY).toString();

        if (mPresenter != null) {
            mPresenter.ecdhKeyExchange(publicKey);
        }
    }

    //用来校验密码，是否包含数字和字母
    public boolean isContainAll(String str) {
        boolean isDigit = false;
                /*boolean isLowerCase = false;
                boolean isUpperCase = false;*/
        boolean isLetters = false;

        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))) {
                isDigit = true;
            } else if (Character.isLowerCase(str.charAt(i))) {
//                        isLowerCase = true;
                isLetters = true;
            } else if (Character.isUpperCase(str.charAt(i))) {
//                        isUpperCase = true;
                isLetters = true;
            }
        }
        String regex = "^[a-zA-Z0-9]+$";
//                boolean isCorrect = isDigit&&isLowerCase&&isUpperCase&&str.matches(regex);
        boolean isCorrect = isDigit && isLetters && str.matches(regex);
        return isCorrect;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_clearOld:
                etOldPwd.setText("");
                break;
            case R.id.iv_clearNewpwd:
                etNewPwd.setText("");
                break;
            case R.id.iv_clearNewpwd2:
                etNewPwd2.setText("");
                break;
        }
    }

    @Override
    protected ChangePwdContract.Presenter createPresenter() {
        return new ChangePwdPresenter();
    }

    class KeyBoardStateListener implements KeyboardUtil.KeyBoardStateChangeListener {

        @Override
        public void KeyBoardStateChange(int state, EditText editText) {

        }
    }

    class inputOverListener implements KeyboardUtil.InputFinishListener {

        @Override
        public void inputHasOver(int onclickType, EditText editText) {

        }
    }
}
