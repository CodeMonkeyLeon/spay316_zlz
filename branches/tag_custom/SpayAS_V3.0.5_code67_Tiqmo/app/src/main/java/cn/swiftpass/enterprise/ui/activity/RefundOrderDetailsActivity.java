package cn.swiftpass.enterprise.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.tsz.afinal.FinalBitmap;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.enums.ClientEnum;
import cn.swiftpass.enterprise.bussiness.enums.OrderStatusEnum;
import cn.swiftpass.enterprise.bussiness.logica.refund.RefundManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.RefundModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.widget.RefundCheckDialog;
import cn.swiftpass.enterprise.ui.widget.RefundDialog;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.ui.widget.WxCardDialog;
import cn.swiftpass.enterprise.utils.CreateOneDiCodeUtil;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.FileUtils;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 订单详细 跟退款详细 User: hehui Date: 13-9-29 Time: 下午2:32
 */
public class RefundOrderDetailsActivity extends TemplateActivity {

    private static final String TAG = RefundOrderDetailsActivity.class.getSimpleName();
    //private TextView tvRefund, tvUser,tvBankType, tvTransactionId,tvBankTypeTitle;

    private TextView tvOrderCode, tvMoney, tvAddTime, tvNotifyTime, tvState,  tvRefundCode,  tvTitleTime, wx_tvOrderCode, wx_title_info;

    private LinearLayout llBanktype, llNotifyTime;

    //private LinearLayout llTransactionId, ly_refund_money;

    private LinearLayout llRefund, lr_wx;

    private ImageView ivPayType, iv_pay_img;

    private Order orderModel;

    private RefundModel refundModel;

    private Context context;

    private Button btnRefund;

    //private ImageView signatureView; // 获取签名图片

    //private Bitmap bmp;

    String refundNum = null; // 退款单号

    private LinearLayout cashierLay, logo_lay, ll_pay_top, ly_refund, ly_refundMoney, money_lay, discount_lay, card_lay;

    private RelativeLayout pay_top_re;

    //private TextView tv_order_no;

    private TextView cashierText, pay_mch, pay_method,  body_info, tv_transNo, tv_marketing;

    private ImageView logo_title, iv_code;

    private FinalBitmap finalBitmap;

    //private TextView refundMoney;

    private TextView spay_pay_client,  pay_mchId;

    private TextView tv_code, tx_refund_money, tv_rfmoney, tx_discount, tx_receiv, tx_vcard, tx_receivable;

    private ImageView line_01;

    private LinearLayout ly_attach;

    private TextView tv_attach;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        finalBitmap = FinalBitmap.create(RefundOrderDetailsActivity.this);
        try {
            finalBitmap.configDiskCachePath(FileUtils.getAppCache());
        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }
        setupInitViews();
        // bottomBar.setVisibility(View.GONE);
        setLister();
    }

    private void setLister() {
        card_lay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                WxCardDialog wxCardDialgo = new WxCardDialog(RefundOrderDetailsActivity.this, orderModel.getWxCardList(), new WxCardDialog.ConfirmListener() {

                    @Override
                    public void cancel() {

                    }
                });
                DialogHelper.resize(RefundOrderDetailsActivity.this, wxCardDialgo);
                wxCardDialgo.show();
            }
        });
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_order_detail);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {
                // TODO Auto-generated method stub

            }

            @Override
            public void onRightButLayClick() {
                // TODO Auto-generated method stub

            }
        });
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    private void setupInitViews() {
        context = this;
        setContentView(R.layout.activity_refound_order_details);
        ly_attach = getViewById(R.id.ly_attach);
        tv_attach = getViewById(R.id.tv_attach);
        tv_marketing = getViewById(R.id.tv_marketing);
        tx_vcard = getViewById(R.id.tx_vcard);
        card_lay = getViewById(R.id.card_lay);
        line_01 = getViewById(R.id.line_01);
        tx_receiv = getViewById(R.id.tx_receiv);
        money_lay = getViewById(R.id.money_lay);
        discount_lay = getViewById(R.id.discount_lay);
        tx_receivable = getViewById(R.id.tx_receivable);
        tx_discount = getViewById(R.id.tx_discount);
        tv_rfmoney = getViewById(R.id.tv_rfmoney);
        ly_refundMoney = getViewById(R.id.ly_refundMoney);
        tx_refund_money = getViewById(R.id.tx_refund_money);
        pay_mchId = getViewById(R.id.pay_mchId);
        //ly_refund_money = getViewById(R.id.ly_refund_money);
        //refundMoney = getViewById(R.id.refundMoney);
        ly_refund = getViewById(R.id.ly_refund);
        spay_pay_client = getViewById(R.id.spay_pay_client);
        tv_transNo = getViewById(R.id.tv_transNo);
        logo_lay = getViewById(R.id.logo_lay);
        ll_pay_top = getViewById(R.id.ll_pay_top);
        logo_title = getViewById(R.id.logo_title);
        body_info = getViewById(R.id.body_info);
        pay_mch = getViewById(R.id.pay_mch);
        pay_method = getViewById(R.id.pay_method);
        tvOrderCode = getViewById(R.id.tvOrderCode);
        llBanktype = getViewById(R.id.ll_banktype);
        //tvBankTypeTitle = getViewById(R.id.tv_banktypeTitle);
        tvMoney = getViewById(R.id.tv_money);
        tvAddTime = getViewById(R.id.tv_addtime);
        ivPayType = getViewById(R.id.iv_payType);

        tvState = getViewById(R.id.tv_state);
        btnRefund = getViewById(R.id.btn_refund);
        iv_pay_img = getViewById(R.id.iv_pay_img);

        llRefund = getViewById(R.id.ll_refund_code);
        tvRefundCode = getViewById(R.id.tv_refund_code);
        // tvUser = getViewById(R.id.tv_user);

        wx_tvOrderCode = getViewById(R.id.wx_tvOrderCode);
        pay_top_re = getViewById(R.id.pay_top_re);

        //tvBankType = getViewById(R.id.tv_banktype);

        orderModel = (Order) getIntent().getSerializableExtra("order");
        refundModel = (RefundModel) getIntent().getSerializableExtra("refund");

        //llTransactionId = getViewById(R.id.ll_transactionId);
        //tvTransactionId = getViewById(R.id.tv_transactionId);

        llNotifyTime = getViewById(R.id.ll_notifytime);
        tvNotifyTime = getViewById(R.id.tv_notifyTime);

        wx_title_info = getViewById(R.id.wx_title_info);

        lr_wx = (LinearLayout) getViewById(R.id.lr_wx);

        tvTitleTime = getViewById(R.id.tv_titletime);
        //signatureView = getViewById(R.id.signature_pic);

        cashierText = getViewById(R.id.cashierText);

        cashierLay = getViewById(R.id.cashierLay);

        iv_code = getViewById(R.id.iv_code);
        tv_code = getViewById(R.id.tv_code);

        //付款备注
        if (orderModel != null && !StringUtil.isEmptyOrNull(orderModel.getAttach())) {
            ly_attach.setVisibility(View.VISIBLE);
            tv_attach.setText(orderModel.getAttach());
        }


        if (orderModel != null && !StringUtil.isEmptyOrNull(orderModel.getUserName()) && !StringUtil.isEmptyOrNull(orderModel.getUseId())) {
            cashierText.setText(orderModel.getUserName() + "(" + orderModel.getUseId() + ")");
        } else {
            cashierLay.setVisibility(View.GONE);
        }

        if (orderModel != null) {
            pay_mchId.setText(MainApplication.getMchId());
            // 生成一维码
            if (!isAbsoluteNullStr(orderModel.getOrderNoMch())) {
                WindowManager wm = this.getWindowManager();
                int width = wm.getDefaultDisplay().getWidth();
                int w = (int) (width * 0.85);
                iv_code.setImageBitmap(CreateOneDiCodeUtil.createCode(orderModel.getOrderNoMch(), w, 180));
                tv_code.setText(orderModel.getOrderNoMch());
            }

            if (!isAbsoluteNullStr(orderModel.getClient())) {
                spay_pay_client.setText(ClientEnum.getClienName(orderModel.getClient()));
            } else {
                spay_pay_client.setText(ClientEnum.UNKNOW.clientType);
            }

            if (!StringUtil.isEmptyOrNull(MainApplication.mchLogo)) {
                logo_lay.setVisibility(View.VISIBLE);
                ll_pay_top.setVisibility(View.GONE);
                finalBitmap.display(logo_title, MainApplication.mchLogo);
            }

            if (!StringUtil.isEmptyOrNull(orderModel.getBody())) {
                body_info.setText(orderModel.getBody());
            }

            tv_transNo.setText(orderModel.getOrderNoMch());
            pay_method.setText(orderModel.tradeName);
            pay_mch.setText(MainApplication.mchName);
            llRefund.setVisibility(View.GONE);
            if (orderModel.getRufundMark() == 0) {
                btnRefund.setVisibility(View.GONE);
            } else if (orderModel.getRufundMark() == 2) {
                ly_refund.setVisibility(View.VISIBLE);
            } else if (orderModel.getRufundMark() == 1) {
                btnRefund.setVisibility(View.VISIBLE);
                if (orderModel.money - (orderModel.getRefundMoney() + orderModel.getRfMoneyIng()) <= 0) {
                    btnRefund.setVisibility(View.GONE);
                } else {
                    btnRefund.setVisibility(View.VISIBLE);
                }
            }

            if (orderModel.getRefundMoney() > 0) {
                double m = (double) orderModel.getRefundMoney();
                ly_refundMoney.setVisibility(View.VISIBLE);
                tx_refund_money.setText(DateUtil.getMoneyWithFeefh(DateUtil.formatMoneyUtils(m)));
            }
            if (orderModel.getRfMoneyIng() > 0) {
                double m = (double) orderModel.getRfMoneyIng();
                ly_refund.setVisibility(View.VISIBLE);
                tv_rfmoney.setText("(" + DateUtil.getMoneyWithFeefh( DateUtil.formatMoneyUtils(m)) + ")" + getString(R.string.refund_auditing));
            }

            double money = orderModel.money / 100d;
            tvOrderCode.setText(orderModel.outTradeNo);
            wx_tvOrderCode.setText(orderModel.transactionId);
            tvAddTime.setText(DateUtil.formatTime(orderModel.add_time));
            //            tvMoney.setText("¥ " + DateUtil.formatMoneyUtil(money));

            if (orderModel.getDaMoney() > 0) { //优惠金额大于零
                discount_lay.setVisibility(View.VISIBLE);
                money_lay.setVisibility(View.VISIBLE);
                tx_receiv.setText(R.string.tx_real_money);

                double discount = (double) orderModel.getDaMoney();
                tv_marketing.setText(getString(R.string.tx_markteing));
                tx_discount.setText(DateUtil.getMoneyWithFeefh(DateUtil.formatMoneyUtils(discount)));
                tx_receivable.setText(DateUtil.getMoneyWithFeefh(DateUtil.formatMoneyUtil(money)));
                // 实际金额 = 应收金额+优惠金额
                long discountM = orderModel.getDaMoney() + orderModel.money;
                tvMoney.setText(DateUtil.getMoneyWithFeefh(DateUtil.formatMoneyUtils((double) discountM)));
                card_lay.setVisibility(View.VISIBLE);

                if (orderModel.getWxCardList() != null && orderModel.getWxCardList().size() > 0) {
                    tx_vcard.setText(getString(R.string.tx_wxcard_use) + orderModel.getWxCardList().size() + getString(R.string.tx_wxcard_use_size));
                } else {
                    card_lay.setVisibility(View.GONE);
                }
                //                tx_vcard.setText("本次使用了" + orderModel.getWxCardList().size() + "张优惠券，点击查看");
            } else {
                discount_lay.setVisibility(View.GONE);
                money_lay.setVisibility(View.GONE);
                line_01.setVisibility(View.GONE);
                card_lay.setVisibility(View.GONE);
                tx_receivable.setText(DateUtil.getMoneyWithFeefh(DateUtil.formatMoneyUtil(money)));
            }

            if (orderModel.notifyTime != null) {
                tvNotifyTime.setText(DateUtil.formatTime(Long.parseLong(orderModel.notifyTime)));
            }
            if (orderModel.tradeState.equals(OrderStatusEnum.PAY_FAILL.getValue())) // 判断状态
            {
                btnRefund.setVisibility(View.GONE);
                lr_wx.setVisibility(View.GONE);
                tvState.setText(R.string.tx_payment);
            } else if (orderModel.tradeState.equals(OrderStatusEnum.PAY_SUCCESS.getValue()) || orderModel.tradeState.equals(OrderStatusEnum.RETURN_PRODUCT.getValue())) {
                if (orderModel.getTradeType().equalsIgnoreCase(MainApplication.PAY_QQ_NATIVE1) || orderModel.getTradeType().equalsIgnoreCase(MainApplication.PAY_QQ_NATIVE) || orderModel.getTradeType().equalsIgnoreCase(MainApplication.PAY_QQ_WAP) || orderModel.getTradeType().contains(MainApplication.PAY_QQ_TAG)) {
                    wx_title_info.setText(R.string.qq_odd_numbers);
                    pay_method.setText(R.string.qq_scan_pay);
                } else if (orderModel.getTradeType().startsWith(MainApplication.PAY_ZFB_NATIVE) || orderModel.getTradeType().contains(MainApplication.PAY_ALIPAY_TAG)) {
                    wx_title_info.setText(R.string.zfb_odd_numbers);
                    //                    pay_method.setText("支付宝扫码支付");
                } else if (orderModel.getTradeType().equals(MainApplication.PAY_ZFB_MICROPAY)) {
                    wx_title_info.setText(R.string.zfb_odd_numbers);
                    //                    pay_method.setText("支付宝付款码支付");
                }
                //                else if (orderModel.getTradeType().equals(com.switfpass.pay.MainApplication.WX_SACN_TYPE))
                //                {
                ////                    pay_method.setText("微信扫码支付");
                //                }
                else if (orderModel.getTradeType().equals(MainApplication.PAY_QQ_MICROPAY) || orderModel.getTradeType().equals(MainApplication.PAY_QQ_PROXY_MICROPAY) || orderModel.getTradeType().equalsIgnoreCase(MainApplication.PAY_QQ_WAP)) {
                    wx_title_info.setText(R.string.qq_odd_numbers);
                    pay_method.setText(R.string.qq_code_pay);
                    iv_pay_img.setImageResource(R.drawable.icon_qq_color);
                } else if (orderModel.getTradeType().equalsIgnoreCase(MainApplication.PAY_JINGDONG) || orderModel.getTradeType().equalsIgnoreCase(MainApplication.PAY_JINGDONG_NATIVE) || orderModel.getTradeType().contains(MainApplication.PAY_JD_TAG)) {
                    wx_title_info.setText(R.string.tx_pay_jd_order);
                    iv_pay_img.setImageResource(R.drawable.icon_list_jd);
                }
                //                else if (orderModel.getTradeType().equals("pay.weixin.micropay"))
                //                {
                //                    pay_method.setText(R.string.wechat_car_pay);
                //                }
                // btnRefund.setVisibility(View.VISIBLE);
                tvState.setText(OrderStatusEnum.getDisplayNameByVaue(orderModel.tradeState));
                if (!TextUtils.isEmpty(orderModel.getTradeName())) {
                    pay_method.setText(orderModel.getTradeName());
                } else {
                    pay_method.setText(Order.TradeTypetoStr.getTradeNameForType(orderModel.getTradeType()));
                }
            } else {
                tvState.setText(OrderStatusEnum.getDisplayNameByVaue(3));
                btnRefund.setVisibility(View.GONE);
                lr_wx.setVisibility(View.GONE);
                tvState.setText(R.string.tx_payment);
            }

            // if (LocalAccountManager.getInstance().getLoggedUser().role ==
            // UserRole.manager)
            // {
            // tvUser.setText(orderModel.userName);
            // btnRefund.setVisibility(View.GONE);
            // }
            if (orderModel.tradeState.equals(OrderStatusEnum.PAY_SUCCESS.getValue()) || orderModel.tradeState.equals(OrderStatusEnum.RETURN_PRODUCT.getValue())) {
                // llTransactionId.setVisibility(View.VISIBLE);
                // tvTransactionId.setVisibility(View.VISIBLE);
                // tvTransactionId.setText(orderModel.transactionId + "");
                llNotifyTime.setVisibility(View.VISIBLE);
                // btnRefund.setVisibility(View.VISIBLE);
                // 根据不同的支付类型来设置相应的图片
                if (orderModel.getTradeType().startsWith(MainApplication.PAY_WX_MICROPAY) || orderModel.getTradeType().equals(MainApplication.PAY_WX_SJPAY)) {
                    // ivPayType.setImageResource(R.drawable.picture_pay_barcode);
                    iv_pay_img.setImageResource(R.drawable.icon_cancel_wechat);
                } else if (orderModel.getTradeType().equalsIgnoreCase(MainApplication.PAY_QQ_NATIVE) || orderModel.getTradeType().equals(MainApplication.PAY_QQ_MICROPAY) || orderModel.getTradeType().equals(MainApplication.PAY_QQ_PROXY_MICROPAY) || orderModel.getTradeType().equalsIgnoreCase(MainApplication.PAY_QQ_NATIVE1) || orderModel.getTradeType().equalsIgnoreCase(MainApplication.PAY_QQ_WAP) || orderModel.getTradeType().contains(MainApplication.PAY_QQ_TAG)) {
                    // ivPayType.setImageResource(R.drawable.qq_logo);
                    iv_pay_img.setImageResource(R.drawable.icon_qq_color);
                } else if (orderModel.getTradeType().startsWith(MainApplication.PAY_ZFB_NATIVE) || orderModel.getTradeType().equals(MainApplication.PAY_ZFB_MICROPAY) || orderModel.getTradeType().startsWith(MainApplication.PAY_ALIPAY_WAP) || orderModel.getTradeType().contains(MainApplication.PAY_ALIPAY_TAG)) {
                    // ivPayType.setImageResource(R.drawable.zfb_logo);
                    iv_pay_img.setImageResource(R.drawable.icon_alipay_color);
                } else if (orderModel.getTradeType().equalsIgnoreCase(MainApplication.PAY_JINGDONG) || orderModel.getTradeType().equalsIgnoreCase(MainApplication.PAY_JINGDONG_NATIVE) || orderModel.getTradeType().contains(MainApplication.PAY_JD_TAG)) {
                    iv_pay_img.setImageResource(R.drawable.icon_list_jd);
                } else {
                    // ivPayType.setImageResource(R.drawable.n_pay_weixin);
                    iv_pay_img.setImageResource(R.drawable.icon_cancel_wechat);
                }
            } else {
                ivPayType.setVisibility(View.GONE);
                llBanktype.setVisibility(View.GONE);
                // 隐藏 支付成功 的标题
                pay_top_re.setVisibility(View.GONE);
            }

        } else if (refundModel != null) {
            tvRefundCode.setText(refundModel.refundNo);
            double money = refundModel.totalFee / 100d;
            tvOrderCode.setText(refundModel.orderNo);
            if (refundModel.orderTime != null) {
                tvAddTime.setText(DateUtil.formatTime(refundModel.orderTime.getTime()));
            }
            tvMoney.setText(money + getString(R.string.pay_yuan));
            tvTitleTime.setText(R.string.tuikuan_time_accept);
            if (refundModel.addTime.getTime() != 0) {
                tvNotifyTime.setText(DateUtil.formatTime(refundModel.addTime.getTime()));
            }
            // tvUser.setText(refundModel.userName);
            tvState.setText(OrderStatusEnum.getDisplayNameByVaue(refundModel.orderState));
            btnRefund.setText(R.string.audit_through);
        }
    }

    public static void startActivity(Context context, Order orderModel) {
        Intent it = new Intent();
        it.setClass(context, RefundOrderDetailsActivity.class);
        it.putExtra("order", orderModel);
        context.startActivity(it);
    }

    public static void startActivity(Context context, RefundModel refundModel) {
        Intent it = new Intent();
        it.setClass(context, RefundOrderDetailsActivity.class);
        it.putExtra("refund", refundModel);
        context.startActivity(it);
    }

    /**
     * 暂时不支持退款
     */
    //
    public void onRefund(View v) {
        /*
         * if (ApiConstant.ISOVERSEASY) { showToastInfo("暂不支持境外退款，谢谢合作！"); }
         * else {
         */
        //
        // if (orderModel.payType == PayType.PAY_POS.getValue())
        // {
        // showToastInfo("暂不支持POS退款，谢谢合作！");
        // return;
        // }
        if (orderModel != null) {
            // 目前只能退当天
            // String notifyTime =
            // DateUtil.formatYYMD(Long.parseLong(orderModel.notifyTime));
            // String nowTime = DateUtil.formatYYMD(System.currentTimeMillis());
            // if (!notifyTime.equalsIgnoreCase(nowTime))
            // {
            // showConfirm("退款只能退当天的!", new OnConfirmListener()
            // {
            // @Override
            // public void onOK()
            // {
            // OrderDetailsActivity.this.finish();
            // }
            //
            // @Override
            // public void onCancel()
            // {
            // }
            // });
            // return;
            // }

            // 退款需要输入密码
            RefundCheckDialog dialog1 = new RefundCheckDialog(this, RefundCheckDialog.REFUND, getString(R.string.public_cozy_prompt), orderModel.outTradeNo, String.valueOf(orderModel.money), orderModel, new RefundCheckDialog.ConfirmListener() {
                @Override
                public void ok(String code) {
                    dialog = new RefundDialog(context, getString(R.string.dialog_is_refund), Integer.parseInt(code), orderModel.money, btnListener);
                    dialog.show();
                }

                @Override
                public void cancel() {

                }
            });
            dialog1.show();

        }
        // else if (refundModel != null)
        // {
        // //审核
        // RefundManager.getInstant().checkRefund(refundModel.id, new
        // UINotifyListener<Boolean>()
        // {
        // @Override
        // public void onError(Object object)
        // {
        // super.onError(object);
        // if (object != null)
        // {
        // showToastInfo(object.toString());
        // }
        // }
        //
        // @Override
        // public void onPreExecute()
        // {
        // super.onPreExecute();
        // showLoading(true, "请稍候...");
        // }
        //
        // @Override
        // public void onPostExecute()
        // {
        // super.onPostExecute();
        // dismissLoading();
        // }
        //
        // @Override
        // public void onSucceed(Boolean result)
        // {
        // super.onSucceed(result);
        // if (result)
        // {
        // showToastInfo("操作成功！");
        // }
        // }
        // });
        // }
        // }
    }

    private RefundDialog.ConfirmListener btnListener = new RefundDialog.ConfirmListener() {
        @Override
        public void ok(long money) {
            // if (orderModel.transactionType != PayType.PAY_WX.getValue()) //
            // 微信反扫
            // {
            // refundNum = orderModel.outTradeNo;
            // }
            // else
            // {
            refundNum = orderModel.outTradeNo;
            // }
            regisRefund(money);
        }

        @Override
        public void cancel() {

        }
    };

    private void regisRefund(long money) {
        String transaction = null;
        RefundManager.getInstant().regisRefund(refundNum, transaction, orderModel.money, money, new UINotifyListener<Boolean>() {
            @Override
            public void onError(final Object object) {
                dismissLoading();
                super.onError(object);
                if (object != null) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showToastInfo(object.toString());
                            finish();
                        }
                    });

                }
            }

            @Override
            public void onPreExecute() {
                super.onPreExecute();

                showLoading(getString(R.string.refunding_wait));
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
                dismissLoading();
            }

            @Override
            public void onSucceed(Boolean result) {
                dismissLoading();
                super.onSucceed(result);
                if (result) {
                    showToastInfo(R.string.refund_audit_through);
                    btnRefund.setEnabled(false);

                    // OrderStreamActivity.startActivity(OrderDetailsActivity.this,
                    // true);
                    //                    MainActivity.startActivity(RefundOrderDetailsActivity.this, "OrderDetailsActivity"); // 退款成功跳转
                    //                    MainApplication.isRefund = true;
                    finish();
                }
            }
        });
    }

    public void showBigOneCode(View view) {
        if (orderModel != null) {
//            OneDiCodeActivity.startActivity(RefundOrderDetailsActivity.this, orderModel.getOrderNoMch());
            RotateCanvasViewActivity.startActivity(RefundOrderDetailsActivity.this, orderModel.getOrderNoMch());
        }
    }
}
