package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.IntDef;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.common.sentry.SentryUtils;
import com.example.common.sp.PreferenceUtil;

import java.io.File;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo;
import cn.swiftpass.enterprise.common.Constant;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;
import cn.swiftpass.enterprise.mvp.base.BaseView;
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants;
import cn.swiftpass.enterprise.ui.activity.print.BluePrintUtil;
import cn.swiftpass.enterprise.ui.widget.CommonConfirmDialog;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.MyToast;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.KotlinUtils;
import cn.swiftpass.enterprise.utils.LocaleUtils;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;
import cn.swiftpass.enterprise.utils.permissionutils.PermissionSettingPage;
import cn.swiftpass.enterprise.utils.permissionutils.PermissionsManage;

/**
 * 基类
 * 有一个抽象方法，是否需要登录
 * User: Alan
 * Date: 13-9-21
 * Time: 上午11:52
 * To change this template use File | Settings | File Templates.
 */
public abstract class BaseActivity<P extends BasePresenter> extends AppCompatActivity implements BaseView {
    public static final int SYSTEM_UI_MODE_NONE = 0;//默认显示，无沉侵
    public static final int SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS = 1;//沉侵状态栏，内容延申到状态栏
    public static final int SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS = 2;//沉侵状态栏，内容延申到状态栏，并且状态栏内容(如时间)是黑色的
    public static final int SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS_AND_NAVIGATION = 3;//沉侵状态栏，内容延申到状态栏和导航栏
    public static final int SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS_AND_NAVIGATION = 4;//沉侵状态栏，内容延申到状态栏和导航栏，并且状态栏和导航栏内容是黑色的
    public static final int SYSTEM_UI_MODE_FULLSCREEN = 5;//全屏
    public static final int SYSTEM_UI_MODE_LIGHT_BAR = 6;//状态栏内容(如时间)是黑色的
    protected static final int PERMISSON_REQUESTCODE = 0;
    private static final String TAG = BaseActivity.class.getCanonicalName();
    private final static int RESULT_TAKE_PHOTO = 10002;
    protected ProgressDialog loadingDialog = null;
    protected boolean isResumed = false;
    protected String picturePath = null;
    protected Dialog dialog;
    protected String avatarImgPath;
    protected P mPresenter = null;
    MyToast myToast;
    private DialogInfo dialogInfo;
    private Dialog dialogNew = null;
    private long addTime;
    private Calendar c = null;

    protected TitleBar titleBar;

    protected LinearLayout bottomBar;

    protected ViewGroup content;


    /**
     * 可配置设置
     */
    protected void setButBgAndFont(Button b) {
        DynModel dynModel = (DynModel) SharedPreUtils.readProduct("dynModel" + BuildConfig.bankCode);
        if (null != dynModel) {
            try {
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonColor())) {
                    b.setBackgroundColor(Color.parseColor(dynModel.getButtonColor()));
                }
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonFontColor())) {
                    b.setTextColor(Color.parseColor(dynModel.getButtonFontColor()));
                }
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                Log.e(TAG, Log.getStackTraceString(e));
            }
        }
    }

    protected boolean useToolBar() {
        return false;
    }

    @Override
    public void setContentView(int layoutResID) {
        if (!useToolBar()) {
            super.setContentView(layoutResID);
            return;
        }

        super.setContentView(R.layout.activity_template);

        titleBar = getViewById(R.id.titleBar);
        bottomBar = getViewById(R.id.bottomBar);
        content = getViewById(R.id.containerLay);

        View view = getLayoutInflater().inflate(layoutResID, null);

        content.addView(view, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        setupBottomBar();
        setupTitleBar();
        setOnCentreTitleBarClickListener();
        //定制化版本 初始化
        DynModel dynModel = (DynModel) SharedPreUtils.readProduct("dynModel" + BuildConfig.bankCode);
        if (null != dynModel) {
            try {
                if (!this.getComponentName().getClassName().equals("cn.swiftpass.enterprise.ui.activity.PayActivity")) {
                    if (!StringUtil.isEmptyOrNull(dynModel.getHeaderColor())) {
                        titleBar.setTiteBackgroundColor(dynModel.getHeaderColor());
                    }
                    if (!StringUtil.isEmptyOrNull(dynModel.getHeaderFontColor())) {
                        titleBar.setTiteBarForntColor(dynModel.getHeaderFontColor());
                    }
                }

            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                Log.e(TAG, Log.getStackTraceString(e));
            }
            if (TextUtils.equals(this.getComponentName().getClassName(), "cn.swiftpass.enterprise.ui.activity.scan.CaptureActivity")) {
                titleBar.setTiteBackgroundColor("#00000000");
            }

        }
    }


    protected void setOnCentreTitleBarClickListener() {
        titleBar.setOnCentreTitleBarClickListener(new TitleBar.OnCentreTitleBarClickListener() {

            @Override
            public void onCentreButtonClick(View v) {
            }

            @Override
            public void onTitleRepotLeftClick() {
                // TODO Auto-generated method stub

            }

            @Override
            public void onTitleRepotRigthClick() {
                // TODO Auto-generated method stub

            }
        });
    }

    protected void setupTitleBar() {
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButLayClick() {

            }
        });
    }

    protected void setupBottomBar() {

    }


    /**
     * 判断字符串是否空串包括str="null"这种情况
     * <功能详细描述>
     *
     * @param str
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static boolean isAbsoluteNullStr(String str) {
        str = deleteBlank(str);
        if (str == null || str.length() == 0 || "null".equalsIgnoreCase(str)) {
            return true;
        }

        return false;
    }

    /**
     * 去前后空格和去换行符
     *
     * @param str
     * @return
     */
    public static String deleteBlank(String str) {
        if (null != str && 0 < str.trim().length()) {
            char[] array = str.toCharArray();
            int start = 0, end = array.length - 1;
            while (array[start] == ' ') start++;
            while (array[end] == ' ') end--;
            return str.substring(start, end + 1).replaceAll("\n", "");

        } else {
            return "";
        }

    }

    private static Intent getTakePickIntent(File f) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE, null);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        return intent;
    }

    /**
     *
     */
    //private int flag = 2;
    protected abstract P createPresenter();

    //设置系统状态栏的样式，比如全屏，或者沉侵式状态栏等等，具体类型可以见每个status的定义，有相关注释
    public void setSystemUiMode(@SystemUiMode int mode) {
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                | WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        switch (mode) {
            case SYSTEM_UI_MODE_NONE:
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    TypedValue value = new TypedValue();
                    getTheme().resolveAttribute(R.attr.colorPrimary, value, true);
                    window.setStatusBarColor(value.data);
                }
                break;
            case SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS:
            case SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS:
                if (mode == SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                } else {
                    window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(Color.TRANSPARENT);
                }
                break;
            case SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS_AND_NAVIGATION:
            case SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS_AND_NAVIGATION:
                if (mode == SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS_AND_NAVIGATION && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                                | View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
                    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                    } else {
                        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
                    }
                } else {
                    window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setNavigationBarColor(Color.TRANSPARENT);
                    window.setStatusBarColor(Color.TRANSPARENT);
                }
                break;
            case SYSTEM_UI_MODE_FULLSCREEN:
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
                break;
            case SYSTEM_UI_MODE_LIGHT_BAR:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                }
                break;
        }
    }

    /**
     * 转成RMB
     *
     * @author admin
     */
    public void paseRMB(BigDecimal bigDecimal, TextView tv_pase) {
        try {

            if (!MainApplication.getInstance().getFeeType().equalsIgnoreCase("CNY")) {
                tv_pase.setVisibility(View.VISIBLE);
                BigDecimal b2 = new BigDecimal(MainApplication.getInstance().getExchangeRate());
                if (bigDecimal != null && b2 != null) {
                    BigDecimal paseBigDecimal = bigDecimal.multiply(b2).setScale(MainApplication.getInstance().getNumFixed(), BigDecimal.ROUND_FLOOR);
                    tv_pase.setText(getString(R.string.tx_about) + getString(R.string.tx_mark) + DateUtil.formatPaseMoney(paseBigDecimal));
                }
            } else {
                tv_pase.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Logger.e("hehui", "parserToRmb-->" + e);
        }
    }

    /**
     * 清除缓存
     * deleteSharedPre:(这里用一句话描述这个方法的作用).
     *
     * @author admin
     */
    public void deleteSharedPre() {
        try {
            SharedPreUtils.removeKey("showMpayInMySettingMD5" + MainApplication.getInstance().getMchId() + MainApplication.getInstance().getUserId());
            PreferenceUtil.removeKey("payTypeMd5" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            SharedPreUtils.removeKey("funcGridInfo" + MainApplication.getInstance().getMchId() + MainApplication.getInstance().getUserId());
            PreferenceUtil.removeKey("payTypeMd5" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            NoteMarkActivity.setNoteMark("");
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Logger.e(TAG, "deleteSharedPre-->" + e);
        }
    }

    //APP的本地退出操作，清掉本地缓存
    protected void clearAPPState() {
        //删掉本地缓存，清除登录状态
        deleteSharedPre();

        PreferenceUtil.removeKey("userPwd");
        PreferenceUtil.removeKey("login_skey");
        PreferenceUtil.removeKey("login_sauthid");
        PreferenceUtil.removeKey("choose_pay_or_pre_auth");
        PreferenceUtil.removeKey("bill_list_choose_pay_or_pre_auth");
        PreferenceUtil.removeKey("mchId");
        PreferenceUtil.removeKey("mchName");
        MainApplication.getInstance().setUserInfo(null);

        for (Activity a : MainApplication.getInstance().getAllActivities()) {
            a.finish();
        }

        Intent it = new Intent();
        it.setClass(getApplicationContext(), WelcomeActivity.class);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(it);

        finish();
    }

    public void setButtonBg(Button b, boolean enable, int res) {
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_register);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.bg_general_button_main_default_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }

    /**
     * @since 2.5.0
     * requestPermissions方法是请求某一权限，
     */
    protected void checkPermissions(String... permissions) {
        List<String> needRequestPermissonList = findDeniedPermissions(permissions);
        if (null != needRequestPermissonList && needRequestPermissonList.size() > 0) {
            ActivityCompat.requestPermissions(this,
                    needRequestPermissonList.toArray(new String[needRequestPermissonList.size()]),
                    PERMISSON_REQUESTCODE);
        }
    }

    public boolean isGranted(String permission) {
        return !isMarshmallow() || isGranted_(permission);
    }

    private boolean isMarshmallow() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    private boolean isGranted_(String permission) {
        int checkSelfPermission = ActivityCompat.checkSelfPermission(this, permission);
        return checkSelfPermission == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * 检测是否所有的权限都已经授权
     *
     * @param grantResults
     * @return
     * @since 2.5.0
     */
    protected boolean verifyPermissions(int[] grantResults) {
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    /**
     * 启动应用的设置
     *
     * @since 2.5.0
     */
    public void startAppSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:" + getPackageName()));
        startActivity(intent);
    }


    protected Activity getActivity() {
        return this;
    }


    public void startPermissionActivity(List<String> permissions) {
        Activity activity = getActivity();
        if (activity != null) {
            PermissionsManage.startPermissionActivity(activity, permissions);
            return;
        }
        Intent intent = PermissionSettingPage.getSmartPermissionIntent(getActivity(), permissions);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    /**
     * 获取权限集中需要申请权限的列表
     *
     * @param permissions
     * @return
     * @since 2.5.0
     * checkSelfPermission方法是在用来判断是否app已经获取到某一个权限
     * shouldShowRequestPermissionRationale方法用来判断是否
     * 显示申请权限对话框，如果同意了或者不在询问则返回false
     */
    protected List<String> findDeniedPermissions(String[] permissions) {
        List<String> needRequestPermissonList = new ArrayList<String>();
        for (String perm : permissions) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                needRequestPermissonList.add(perm);
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, perm)) {
                    needRequestPermissonList.add(perm);
                }
            }
        }
        return needRequestPermissonList;
    }


    protected boolean isWindowFeatureNoTitle() {
        return false;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isWindowFeatureNoTitle()) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
        }
        mPresenter = createPresenter();
        if (mPresenter != null) {
            mPresenter.attachView(this);
        }
        KotlinUtils.INSTANCE.isReStartApp(savedInstanceState, this);
        setSystemUiMode(SYSTEM_UI_MODE_NONE);
        //初始化PreferenceUtil
        PreferenceUtil.init(this);
        //根据上次的语言设置，重新设置语言
        LocaleUtils.switchLanguage(this);
        //添加
        ToastHelper.getInstance(this);
        MainApplication.getInstance().getAllActivities().add(this);

    }

    /**
     * 连接默认上一次
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void connentBlue() {
        boolean connState = PreferenceUtil.getBoolean("connState" + BuildConfig.bankCode, true);
        if (connState && !StringUtil.isEmptyOrNull(MainApplication.getInstance().getBlueDeviceAddress())) {
            //异步去连接蓝牙
            new Thread(new Runnable() {
                @Override
                public void run() {
                    boolean isConnect = BluePrintUtil.blueConnent(MainApplication.getInstance().getBlueDeviceAddress());
                    Logger.i(TAG, " connentBlue isConnect-->" + isConnect);
                    if (!isConnect) {
                        MainApplication.getInstance().setBlueState(false);
                        HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET, HandlerManager.BLUE_CONNET);
                    } else {
                        MainApplication.getInstance().setBlueState(true);
                    }
                }
            }).start();
        }
    }

    /**
     * EditText获取焦点并显示软键盘
     */
    public void showSoftInputFromWindow(Activity activity, EditText editText) {
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    public void redirectLoginActivity() {

        Intent intent = new Intent();
        intent.setClassName(this, "cn.swiftpass.enterprise.ui.activity.WelcomeActivity");
        startActivity(intent);

        finish();
        for (Activity a : MainApplication.getInstance().getAllActivities()) {
            a.finish();
        }
        //        MainApplication.getContext().exit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isResumed = true;
    }

    @Override
    protected void onPause() {
        isResumed = false;
        dismissLoading(ParamsConstants.ALL_LOADING);
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    protected <T extends View> T getViewById(int id) {
        View view = findViewById(id);
        return (T) view;
    }

    protected <T extends View> T inflate(int res) {
        return (T) LayoutInflater.from(this).inflate(res, null);
    }


    //    public void showMyLoading(String msg) {
//        showLoading(true, msg);
//    }
//
//    public void showLoading(boolean cancelble, int id) {
//        showLoading(cancelble, getString(id));
//    }
//
//    public void showLoading(boolean cancelble, String str) {
//        if (!isResumed) return;
//        loadDialog(this, str);
//    }
//
//    public void showNewLoading(boolean cancelble, String str) {
//        if (!cancelble) return;
//        loadDialog(this, str);
//    }
//
//    ;
//
//    public void showNewLoading(boolean cancelble, String str, Context context) {
//        try {
//            if (!cancelble) return;
//            loadDialog(this, str);
//        } catch (Exception e) {
//            SentryUtils.INSTANCE.uploadTryCatchException(
//                    e,
//                    SentryUtils.INSTANCE.getClassNameAndMethodName()
//            );
//            Log.e(TAG, Log.getStackTraceString(e));
//        }
//
//    }

//    public void dismissMyLoading() {
//        dissDialog();
//    }
//
////    public void showLoading(String msg) {
////        showMyLoading(msg);
////    }
//
//    public void dismissLoading() {
//        dismissMyLoading();
//    }

    public void updateLoading(String msg, int progress) {
        if (loadingDialog != null) {
            loadingDialog.setMessage(msg);
            loadingDialog.setProgress(progress);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 通知释放空间
        //System.gc();
        Runtime.getRuntime().gc();

        android.os.Debug.stopMethodTracing();

        if (mPresenter != null) {
            mPresenter.detachView();
        }
    }

    protected boolean needExitOnLogout() {
        return true;
    }

    protected boolean isLoginRequired() {
        return true;
    }

    public void exit() {
        // ActionHelper.backHome();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    protected void takePhoto() {
        picturePath = GlobalConstant.FILE_CACHE_ROOT;

        File tempFile = new File(picturePath, System.currentTimeMillis() + ".jpg");
        final Intent ci = getTakePickIntent(tempFile);
        startActivityForResult(ci, RESULT_TAKE_PHOTO);
    }

    /**
     * 管理其它窗体
     */
    public void showDialog(Dialog dialog) {
        if (dialog != null) {
            // 先关闭之前的窗体
            hideDialog();
            if (!isResumed) return;
            this.dialog = dialog;
            // 没有显现，则显现
            if (!dialog.isShowing()) {
                dialog.show();
            }
        }
    }

    /**
     * 关闭自定义弹出窗体
     */
    private void hideDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && null != data) {
            if (requestCode != RESULT_TAKE_PHOTO) {
                picturePath = null;
            }
        } else {
            picturePath = null;
        }
    }

    /**
     * 显示一个确认对话框
     *
     * @param msg               　提示信息
     * @param onConfirmListener 　“确定”和“取消”按钮的回调
     */
    public void showConfirm(String msg, final OnConfirmListener onConfirmListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.public_cozy_prompt);
        builder.setMessage(msg);
        builder.setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (onConfirmListener != null) {
                    dialog.dismiss();
                    onConfirmListener.onOK();
                }
            }
        });

        builder.setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (onConfirmListener != null) {
                    dialog.dismiss();
                    onConfirmListener.onCancel();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onAttachedToWindow() {
        // getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
        super.onAttachedToWindow();
    }

    /**
     * 任意线程都可显示toast
     */
    protected void showToastInfo(final String text) {

        showToastInfo(text, true);
    }

    /**
     * 任意线程都可显示toast
     */
    protected void showToastInfo(final int resId) {

        showToastInfo(resId, true);
    }

    /**
     * 任意线程都可显示toast
     */
    protected void showToastInfo(final String text, final boolean isLong) {
        if (checkSession()) {
            return;
        }
        runOnUiThread(new Runnable() {
            public void run() {
                /*  Toast.makeText(BaseActivity.this, text,
                          isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();*/

                myToast = new MyToast();
                myToast.showToast(BaseActivity.this, text);
            }
        });
    }

    protected boolean checkSession() {
        if (isLoginRequired() && MainApplication.getInstance().isNeedLogin()) {
            MainApplication.getInstance().setNeedLogin(false);
            toastDialog(BaseActivity.this, R.string.msg_need_login,
                    new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            PreferenceUtil.removeKey("login_skey");
                            PreferenceUtil.removeKey("login_sauthid");
                            MainApplication.getInstance().setUserInfo(null);
                            redirectLoginActivity();
                        }
                    });

            return true;
        }
        return false;
    }

    /**
     * 任意线程都可显示toast
     */
    protected void showToastInfo(final int resId, final boolean isLong) {

        if (checkSession()) {
            return;
        }

        runOnUiThread(new Runnable() {
            public void run() {
                myToast = new MyToast();
                myToast.showToast(BaseActivity.this, getString(resId));
            }
        });
    }

    public void showExitDialog(final Activity context) {
        dialogInfo = new DialogInfo(context, getString(R.string.public_cozy_prompt),
                getString(R.string.show_sign_out), getString(R.string.btnOk),
                getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG,
                new DialogInfo.HandleBtn() {

                    @Override
                    public void handleOkBtn() {
                        finish();
                        MainApplication.getInstance().exit();

                    }

                    @Override
                    public void handleCancelBtn() {
                        dialogInfo.cancel();
                    }
                }, null);

        DialogHelper.resize(context, dialogInfo);
        dialogInfo.show();
    }


    @Override
    public void showLoading(int id, int type) {
        if (type == ParamsConstants.ALL_LOADING || type == ParamsConstants.COMMON_LOADING) {
            showCommonLoading(id);
        }
    }


    @Override
    public void dismissLoading(int type) {
        if (type == ParamsConstants.ALL_LOADING || type == ParamsConstants.COMMON_LOADING) {
            dismissCommonLoading();
        }
    }


    protected void showCommonLoading(int id) {
        if (!isResumed) return;

        // 加载样式
        try {
            if (null == dialogNew) {
                dialogNew = new Dialog(this, R.style.my_dialog);
                dialogNew.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogNew.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialogNew.setCanceledOnTouchOutside(false);
                LayoutInflater inflater = LayoutInflater.from(this);
                View v = inflater.inflate(R.layout.load_progress_info, null);// 得到加载view dialog_common
                TextView tv = (TextView) v.findViewById(R.id.tv_dialog);
                tv.setText(id);
                // dialog透明
                WindowManager.LayoutParams lp = dialogNew.getWindow().getAttributes();
                lp.alpha = 0.8f;
                dialogNew.getWindow().setAttributes(lp);
                dialogNew.setContentView(v);
                dialogNew.setOnKeyListener(new OnKeyListener() {

                    @Override
                    public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                        if (keycode == KeyEvent.KEYCODE_BACK) {
                            return true;
                        }
                        return false;
                    }
                });
            }
            DialogHelper.resizeNew(this, dialogNew);
            dialogNew.show();
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
        }
    }


    protected void dismissCommonLoading() {
        if (null != dialogNew) {
            BaseActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (dialogNew != null) {
                            dialogNew.dismiss();
                            dialogNew = null;
                        }
                    } catch (Exception e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                        Log.e(TAG, Log.getStackTraceString(e));
                    }
                }
            });
        }
    }

//    public void dissDialog() {
//
//    }

    public void toastDialog(final Activity context, final String content, final String btStr, final NewDialogInfo.HandleBtn handleBtn) {
        context.runOnUiThread(new Runnable() {

            @Override
            public void run() {

                try {
                    if (StringUtil.isEmptyOrNull(content)) {
                        return;
                    }

                    NewDialogInfo info = new NewDialogInfo(context, content, btStr, 2, null, handleBtn);
                    info.setOnKeyListener(new OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                            if (keycode == KeyEvent.KEYCODE_BACK) {
                                return true;
                            }
                            return false;
                        }
                    });
                    DialogHelper.resize(context, info);

                    info.show();
                } catch (Exception e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
        });

    }

    public void toastDialog(final Activity context, final String content, final NewDialogInfo.HandleBtn handleBtn) {
        context.runOnUiThread(new Runnable() {

            @Override
            public void run() {

                try {
                    if (StringUtil.isEmptyOrNull(content)) {
                        return;
                    }

                    NewDialogInfo info = new NewDialogInfo(context, content, null, 2, null, handleBtn);
                    info.setOnKeyListener(new OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                            if (keycode == KeyEvent.KEYCODE_BACK) {
                                return true;
                            }
                            return false;
                        }
                    });
                    DialogHelper.resize(context, info);

                    info.show();
                } catch (Exception e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
        });

    }

    public void toastDialog(final Activity context, final Integer content, final NewDialogInfo.HandleBtn handleBtn) {
        context.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                try {

                    NewDialogInfo info = new NewDialogInfo(context, context.getString(content), null, 2, null, handleBtn);
                    info.setOnKeyListener(new OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                            if (keycode == KeyEvent.KEYCODE_BACK) {
                                return true;
                            }
                            return false;
                        }
                    });
                    DialogHelper.resize(context, info);

                    info.show();
                } catch (Exception e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
        });

    }

    /**
     * 跳转页面
     *
     * @param clazz
     * @see [类、类#方法、类#成员]
     */
    @SuppressWarnings("rawtypes")
    public void showPage(Class clazz) {
        Intent intent = new Intent(this, clazz);
        this.startActivity(intent);
    }

    /**
     * 跳转页面
     *
     * @param clazz
     * @see [类、类#方法、类#成员]
     */
    @SuppressWarnings("rawtypes")
    public void showPage(Class clazz, Bundle bundle) {
        Intent intent = new Intent(this, clazz);
        intent.putExtra("bundle", bundle);
        this.startActivity(intent);
    }

    /**
     * 跳转页面
     *
     * @param clazz
     * @see [类、类#方法、类#成员]
     */
    @SuppressWarnings("rawtypes")
    public void showPage(Class clazz, String param) {
        Intent intent = new Intent(this, clazz);
        intent.putExtra("picUrl", param);
        this.startActivity(intent);
    }

    public void showUpgradeInfoDialog(UpgradeInfo result, CommonConfirmDialog.ConfirmListener listener) {
        StringBuilder sb = new StringBuilder();
        sb.append(getString(R.string.show_version) + result.versionName + "\n");
//        if (result.dateTime != 0) {
//            sb.append(getString(R.string.show_version_time) + DateUtil.formatYMD(result.dateTime) + "\n");
//        } else {
//            sb.append(getString(R.string.show_version_time_unknow) + "\n");
//        }
        if (!TextUtils.isEmpty(result.message)) {
            if (result.message.equals("null")) {
                result.message = "";
            }
            sb.append(getString(R.string.show_version_update_content) + "\n" + result.message + "\n\n");
        }
        sb.append(getString(R.string.show_is_update));

        CommonConfirmDialog.show(this, getString(R.string.show_find_new_version), sb.toString(), listener, true, result);
    }

    /**
     * 得到时间和日期一系列方法
     *
     * @return
     */
    public int getYear() {
        return c.get(Calendar.YEAR);
    }

    public int getMonth() {
        return c.get(Calendar.MONTH) + 1;//系统日期从0开始算起
    }

    public int getDay() {
        return c.get(Calendar.DAY_OF_MONTH);
    }

    public int getHonor() {
        return c.get(Calendar.HOUR_OF_DAY);
    }

    public int getMin() {
        return c.get(Calendar.MINUTE);
    }

    public String getStringById(int id) {
        try {
            return this.getResources().getString(id);
        } catch (Exception e) {
            return "";
        }

    }

    /**
     * 判断当前语言环境是否是英文环境
     *
     * @return
     */
    public boolean isEnglish() {
        boolean isEnglish = false;
        String language = PreferenceUtil.getString("language", "");
        if (!TextUtils.isEmpty(language) && language.equals(Constant.LANG_CODE_EN_US)) {
            isEnglish = true;
        }
        return isEnglish;
    }

    @IntDef({SYSTEM_UI_MODE_NONE
            , SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS
            , SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS
            , SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS_AND_NAVIGATION
            , SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS_AND_NAVIGATION
            , SYSTEM_UI_MODE_FULLSCREEN
            , SYSTEM_UI_MODE_LIGHT_BAR})
    @Retention(RetentionPolicy.SOURCE)
    private @interface SystemUiMode {
    }

    public interface OnConfirmListener {
        public void onOK();

        public void onCancel();
    }
}
