package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.MasterCardInfo
import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.bussiness.model.QRcodeInfo
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView


/**
 * @author lizheng.zhao
 * @date 2022/12/2
 *
 * @锁上我的记忆
 * @锁上我的忧伤
 * @不再想你
 * @怎么可能再想你
 * @快乐是禁地
 * @生死之后
 * @找不到进去的钥匙
 */
class ShowQrCodeContract {

    interface View : BaseView {


        fun unifiedNativePaySuccess(
            response: QRcodeInfo?,
            payType: String?,
            isInstapay: Boolean,
            isSecond: Boolean
        )

        fun unifiedNativePayFailed(error: Any?)


        fun masterCardNativePaySuccess(
            response: MasterCardInfo?,
            apiCode: String?
        )

        fun masterCardNativePayFailed(error: Any?)


        fun queryMasterCardOrderByOrderNoSuccess(response: Order?)

        fun queryMasterCardOrderByOrderNoFailed(error: Any?)


        fun queryOrderByOrderNoSuccess(response: Order?)

        fun queryOrderByOrderNoFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {


        fun queryOrderByOrderNo(orderNo: String?)


        fun queryMasterCardOrderByOrderNo(
            service: String?,
            outTradeNo: String?
        )


        fun masterCardNativePay(
            money: String?,
            apiCode: String?,
            attach: String?
        )


        fun unifiedNativePay(
            money: String?,
            payType: String?,
            traType: String?,
            adm: Long,
            outTradeNo: String?,
            note: String?,
            isInstaPay: Boolean,
            isSecond: Boolean
        )

    }


}