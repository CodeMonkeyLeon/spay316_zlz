package cn.swiftpass.enterprise.ui.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.baoyz.swipemenulistview.BadgeView;

import net.tsz.afinal.FinalBitmap;

import org.xclcharts.common.DensityUtil;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.upgrade.UpgradeManager;
import cn.swiftpass.enterprise.bussiness.model.MerchantTempDataModel;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.QRcodeInfo;
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.scan.CaptureActivity;
import cn.swiftpass.enterprise.ui.view.MyTextView;
import cn.swiftpass.enterprise.ui.widget.CommonConfirmDialog;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.DialogInfo.HandleBtn;
import cn.swiftpass.enterprise.ui.widget.ProgressInfoDialog;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.FileUtils;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.NetworkUtils;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.Utils;

/**
 * 输入 金额 微信支付
 * User: alan
 * Date: 13-8-20
 * Time: 下午3:32
 */
@SuppressLint("ResourceAsColor")
public class PayActivity extends BaseActivity implements View.OnClickListener, DialogInfo.HandleBtn, OnPageChangeListener {
    private static final String TAG = PayActivity.class.getSimpleName();
    //
    private MyTextView etMoney;

    private TextView tvMoneyUppercase;

    private Context context;

    private String orderCode = "";

    //private Activity act;
    private String sum = "";
    private String two = "";


    //小费的编辑输入
    private String sumTips = "";
    private String twoTips = "";
    private String strTotalMoney;
    private String strSurchargelMoney;
    private String strTipMoney = "";
    private boolean isSubmitData = false;
    public static final int REQUEST_2CODE = 0x12;
    public static final int REQUEST_EPAYLINKS = 0x13;
    private SharedPreferences sp;
    private String userName;
    public static final int HANDLER_MSG = 0x10001; //隐藏优惠劵提示图片

    public static final int HANDLER_MSGSHOW = 0x10002;//显示优惠劵提示图片

    private ImageView ivPictureRotating, right_icon_ashing_jd, left_icon_ashing_jd, right_alipay, zfb_left_img; //优惠劵图片

    //private int isClickNumer; //统计时间

    private LayoutInflater inflater;

    private View view, wxView, zfbView, qqView, jdView;

    private Context mContext;

    private int lastIndex;

    private TextView btn_ic_money, id_title_surcharge;

    private ImageView buttonPoint, button1, button2, button3, button4, button5, button6, button7, button8, button9, button0;

    private LinearLayout ivInputNumberDel;



    private LinearLayout onClickListener1, onClickListener2, onClickListener3,
            onClickListener4, onClickListener5, onClickListener6, onClickListener7,
            onClickListener8, onClickListener9, onClickListenerPoint, onClickListenerClear,
            onClickListener0,id_lin_add;

    private LinearLayout lay_Clear, ly_one, ly_two, ly_three;

    private List<View> views;

    private ViewPager viewPager;

    private String payType = MainApplication.PAY_WX_MICROPAY;

    private List<String> payMeths = new ArrayList<String>();

    private LinearLayout cib_lay;

    private ViewFlipper pay_left_flipper, pay_right_flipper;

    //private BarcodeManager mBarcodeManager;

    //public static final String SCANNER_READ = "SCANNER_READ";

    //public static final boolean CODE_RESULT_HANDLE_SYNC = true;

    private AlertDialog dialogInfo;

    private boolean isStop = true; // 强行中断交易

    private ImageView right_icon_qq, right_icon_wechat, left_icon_qq, left_icon_wechat, iv_icon;

    private FrameLayout lay_rotating;

    private TextView prompt_txt, tx_time;
    MyTextView tv_pase;

    private FinalBitmap finalBitmap;

    private LinearLayout ly_to_scan_pay;

    //private String url;

    private TimeCount time;

    private String payNatieType = MainApplication.PAY_WX_NATIVE;

    boolean setDefPayMethod = true; //设置默认支付方式

    private MyAdapter myAdapter;

    private boolean isWitch = true; // 是否切换

    private FrameLayout lay_notify; //显示公告

    //private TextView tv_notify_title, tv_one;

    private TextView id_tips, id_title_addtips;

    //private String noticeContentUrl, title = "";

    private String noticeId = "";

    //private ImageView tv_etc;

    private ImageView iv_clear, button10, tv_reduce;

    LinearLayout onClickListener10;

    private RelativeLayout id_rel_surcharge, id_rel_tip;


    private View id_line_second, id_line_first;

    private String maxNum = "999999999.99";
    private boolean isPay = true; //默认是收款

    private boolean mLoadRateSuccess = false;//获取附加税率接口

    private long MAX_SALSE_VALUE = 5000000L;//根据税额
    private BadgeView mBadgeView;

    private LinearLayout mInputLayout,mExFeeLayout;
    // 注册欢迎提示
    public Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case HANDLER_MSG:
                    //isClickNumer = 11;
                    ivPictureRotating.setVisibility(View.GONE);
                    lay_rotating.setVisibility(View.GONE);
                    break;
                case HANDLER_MSGSHOW:
                    ivPictureRotating.setBackgroundResource(R.drawable.n_picture_rotating);
                    ivPictureRotating.setVisibility(View.VISIBLE);
                    prompt_txt.setVisibility(View.VISIBLE);
                    lay_rotating.setVisibility(View.VISIBLE);
                    lay_rotating.setOnClickListener(PayActivity.this);
                    break;
                case 0:
                    if (etMoney != null) {
                        clearData();
                    }
                    break;
                case HandlerManager.PAY_SET_PAY_METHOD: // 设置默认支付方式
                    String typeStr = (String) msg.obj;

                    if (viewPager != null) {
                        setDefPayMethod = false;

                        setPayMehotd(typeStr);
                    }
                    break;
                case HandlerManager.NOTICE_TYPE:
                    //                    iv_point.setVisibility(View.GONE);

                    break;
                default:
                    break;
            }
        }

    };

    private int currentMode = 0;//1代表输入小费模式 0 其他


//    /**
//     * 倒计时
//     * <功能详细描述>
//     *
//     * @see [类、类#方法、类#成员]
//     */
//    private void Countdown(long times) {
//        time = new TimeCount(times * 1000, 1000);
//        time.start();
//    }

    class TimeCount extends CountDownTimer {
        final long oneDay = 24 * 60 * 60 * 1000;

        final long oneHour = 60 * 60 * 1000;

        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }

        @Override
        public void onFinish() {//计时完毕时触发
            showToastInfo(getString(R.string.show_active_end));
            tx_time.setVisibility(View.GONE);
        }

        @Override
        public void onTick(long millisUntilFinished) {//计时过程显示
            //long days = millisUntilFinished / oneDay;
            long temp = millisUntilFinished % oneDay; //时分钞
            long hour = temp / oneHour; //小时
            long temp1 = temp % oneHour; //分和秒
            long minute = temp1 / 60000; //分
            long temp2 = temp1 % 60000;
            long second = temp2 / 1000;//秒

            tx_time.setText(getString(R.string.tx_active_down) + formatNumber(hour) + getString(R.string.tx_hour) + formatNumber(minute) + getString(R.string.tx_minute) + formatNumber(second) + getString(R.string.tx_second));

        }
    }

    /**
     * 格式化数字
     *
     * @param n
     * @return
     * @see [类、类#方法、类#成员]
     */
    private String formatNumber(long n) {
        NumberFormat nf = NumberFormat.getInstance();
        //设置是否使用分组
        nf.setGroupingUsed(false);
        //设置最大整数位数
        nf.setMaximumIntegerDigits(2);
        //设置最小整数位数
        nf.setMinimumIntegerDigits(2);
        return nf.format(n);
    }

    boolean flag = true;

    int count = 0;

    /**
     * 查询订单获取状态
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    /*private void queryOrderGetStuts(final String orderNo) {
        if (dialog1 != null) {
            dialog1.dismiss();
        }
        dialog1 = new ProgressInfoDialog(PayActivity.this, getStringById(R.string.confirming_waiting), new ProgressInfoDialog.HandleBtn() {

            @Override
            public void handleOkBtn() {
                showConfirm(getStringById(R.string.interrupt_trading), dialog1);
            }

        });
        OrderManager.getInstance().queryOrderByOrderNo(orderNo, payType, new UINotifyListener<Order>() {
            @Override
            public void onError(final Object object) {
                super.onError(object);
                if (dialogInfo != null) {
                    dialogInfo.dismiss();
                }
                if (dialog1 != null) {
                    dialog1.dismiss();
                }
                if (!isStop) {
                    return;
                } else {
                    isStop = true;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (object != null) {

                            if (dialog != null) {
                                dialog1.dismiss();
                            }
                            //                            dismissLoading();
                            //                            showToastInfo(object.toString());
                        }
                    }
                });
            }

            @Override
            public void onPreExecute() {
                super.onPreExecute();
                DialogHelper.resize(PayActivity.this, dialog1);
                dialog1.show();
                //                showLoading("支付确认中，请稍候...");
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(Order result) {
                super.onSucceed(result);
                if (dialogInfo != null) {
                    dialogInfo.dismiss();
                }
                if (result != null) {
                    if (!isStop) {
                        return;
                    } else {
                        isStop = true;
                    }
                    Logger.i("hehui", "result.state-->" + result.state);
                    if (result.state.equals("2")) {
                        dialog1.dismiss();
                        PayResultActivity.startActivity(context, result);
                    } else {
                        try {
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                            Log.e(TAG,Log.getStackTraceString(e));
                        }
                        if (count < 3) {
                            Logger.i("hehui", "count-->" + count);
                            count++;
                            queryOrderGetStuts(orderNo);
                        } else {
                            // 3次5秒查询后，如果还未返回成功，调用冲正接口
                            Logger.i("hehui", "count-->" + count);
                            if (!result.state.equals("2")) {

                                count = 0;
                                dialog1.dismiss();
                                payReverse(orderNo);
                            }
                        }

                    }
                }
            }
        });
    }
*/
    /**
     * 冲正接口
     * <功能详细描述>
     *
     * @param orderNo
     * @see [类、类#方法、类#成员]
     */
    private void payReverse(final String orderNo) {
        dialog1 = new ProgressInfoDialog(PayActivity.this, getStringById(R.string.order_czing_waiting), new ProgressInfoDialog.HandleBtn() {

            @Override
            public void handleOkBtn() {
                showConfirm(getStringById(R.string.interrupt_trading), dialog1);
            }

        });
        OrderManager.getInstance().payReverse(orderNo, payType, new UINotifyListener<Order>() {
            @Override
            public void onError(final Object object) {
                super.onError(object);
                dialog1.dismiss();
                if (dialogInfo != null) {
                    dialogInfo.dismiss();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (object != null) {

                            DialogInfo dialogInfo = new DialogInfo(PayActivity.this, getStringById(R.string.public_cozy_prompt), "" + object.toString(), getStringById(R.string.btnOk), DialogInfo.FLAG, null, null);
                            DialogHelper.resize(PayActivity.this, dialogInfo);
                            dialogInfo.show();
                        }
                    }
                });
            }

            @Override
            public void onPreExecute() {
                super.onPreExecute();
                DialogHelper.resize(PayActivity.this, dialog1);
                dialog1.show();
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(Order result) {
                super.onSucceed(result);
                dialog1.dismiss();
                if (dialogInfo != null) {
                    dialogInfo.dismiss();
                }
                if (result != null && isStop) {
                    showToastInfo(getStringById(R.string.order_cz_success_1));
                } else {
                    isStop = true;
                }
            }
        });
    }

    private void showConfirm(String msg, final ProgressInfoDialog dialogs) {
        AlertDialog.Builder builder = new AlertDialog.Builder(PayActivity.this);
        builder.setTitle(R.string.public_cozy_prompt);
        builder.setMessage(msg);
        builder.setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isStop = false;
                if (dialogs != null) {
                    dialogs.dismiss();
                }
            }
        });

        builder.setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialogInfo = builder.show();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_new_new);
        //加装是否有活动
        context = this;
        finalBitmap = FinalBitmap.create(PayActivity.this);//
        try {
            finalBitmap.configDiskCachePath(FileUtils.getAppCache());
        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }

        //act = this;
        sp = this.getSharedPreferences("login", 0);
        userName = sp.getString("user_name", "");

        HandlerManager.registerHandler(HandlerManager.PAY_SET_PAY_METHOD, handler);
        initView();
        setLister();
        HandlerManager.registerHandler(HandlerManager.NOTICE_TYPE, handler);
        HandlerManager.registerHandler(HandlerManager.PAY_FINISH, handler);

        if (MainApplication.isUpdateShow) {
            if (NetworkUtils.isNetWorkValid(PayActivity.this)) {
                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                checkVersion();

                            }
                        });
                    }
                };
                Timer timer = new Timer();
                timer.schedule(task, 200);
            } else {
                DialogInfo dialogInfo = new DialogInfo(mContext, getString(R.string.no_network), getString(R.string.to_open_network), getString(R.string.to_open), DialogInfo.NETWORKSTATUE, new HandleBtn() {

                    @Override
                    public void handleOkBtn() {
                        Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                        mContext.startActivity(intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                    }

                    @Override
                    public void handleCancleBtn() {
                    }
                }, null);
                DialogHelper.resize(this, dialogInfo);
                dialogInfo.show();

            }
        }
    }public void checkVersion() {
        //不用太平凡的检测升级 所以使用时间间隔区分
        LocalAccountManager.getInstance().saveCheckVersionTime();
        UpgradeManager.getInstance().getVersonCode(new UINotifyListener<UpgradeInfo>() {
            @Override
            public void onError(Object object) {
                super.onError(object);
            }

            @Override
            public void onSucceed(UpgradeInfo result) {
                super.onSucceed(result);
                if (null != result) {
                    //                    if (result.version > PreferenceUtil.getInt("update", 0))
                    //                    {
                    // "发现新版本"
                    LocalAccountManager.getInstance().saveCheckVersionFlag(result.mustUpgrade);
                    showUpgradeInfoDialog(result, new ComDialogListener(result));
                    //                    }
                }
            }
        });
    }



    class ComDialogListener implements CommonConfirmDialog.ConfirmListener {
        private UpgradeInfo result;

        public ComDialogListener(UpgradeInfo result) {
            this.result = result;
        }

        @Override
        public void ok() {
            new UpgradeDailog(PayActivity.this, result, new UpgradeDailog.UpdateListener() {
                @Override
                public void cancel() {

                }
            }).show();
        }

        @Override
        public void cancel() {

            PreferenceUtil.commitInt("update", result.version); // 保存更新，下次进来不更新

        }

    }

    /**
     * 公告小圆点
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void isShowPoint(ImageView iv) {
        String noticePre = PreferenceUtil.getString("noticePre", "");
        if (!StringUtil.isEmptyOrNull(noticePre)) {
            Map<String, String> mapNotice = JsonUtil.jsonToMap(noticePre);
            for (Map.Entry<String, String> entry : mapNotice.entrySet()) {
                String value = entry.getValue();
                if (value.equals("0")) { //代表还有公开没有被打开查看详情过
                    iv.setVisibility(View.VISIBLE);
                }
            }
        }

    }

    /**
     * 公告小圆点
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void isHidePoint(ImageView iv) {
        boolean tag = true;
        String noticePre = PreferenceUtil.getString("noticePre", "");
        if (!StringUtil.isEmptyOrNull(noticePre)) {
            Map<String, String> mapNotice = JsonUtil.jsonToMap(noticePre);
            for (Map.Entry<String, String> entry : mapNotice.entrySet()) {
                String value = entry.getValue();
                String key = entry.getKey();
                if (value.equals("0")) { //代表还有公开没有被打开查看详情过
                    iv.setVisibility(View.VISIBLE);
                    tag = false;
                }

                if (!StringUtil.isEmptyOrNull(noticeId) && noticeId.equals(key) && value.equals("1")) {
                    lay_notify.setVisibility(View.GONE);
                }
            }
            if (tag) {
                iv.setVisibility(View.GONE);
            }
        }

    }

    private void initView() {
        tv_pase = getViewById(R.id.tv_pase);

        id_line_second = getViewById(R.id.id_line_second);
        id_line_first = getViewById(R.id.id_line_first);
        //ll_textview_view = getViewById(R.id.ll_textview_view);
        id_rel_surcharge = getViewById(R.id.id_rel_surcharge);
        id_rel_tip = getViewById(R.id.id_rel_tip);
        //keyboard_view = getViewById(R.id.keyboard_view);

//        ly_notify = getViewById(R.id.ly_notify);
        iv_icon = getViewById(R.id.iv_icon);
        id_title_addtips = getViewById(R.id.id_title_addtips);
//        bt_code = getViewById(R.id.bt_code);
//        btn_sacn = getViewById(R.id.btn_sacn);
        button10 = getViewById(R.id.button10);
//        tv_reduce = getViewById(R.id.tv_reduce);
        id_title_surcharge = getViewById(R.id.id_change_surcharge);
        onClickListener10 = getViewById(R.id.onClickListener10);
        id_tips = getViewById(R.id.id_tips);
        ly_one = getViewById(R.id.ly_one);
//        ly_two = getViewById(R.id.ly_two);
        ly_three = getViewById(R.id.ly_three);
        iv_clear = getViewById(R.id.iv_clear);
        lay_notify = getViewById(R.id.lay_notify);
        lay_notify.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //公告

            }
        });
        right_icon_qq = getViewById(R.id.right_icon_qq);
        right_icon_wechat = getViewById(R.id.right_icon_wechat);
        left_icon_qq = getViewById(R.id.left_icon_qq);
        left_icon_wechat = getViewById(R.id.left_icon_wechat);
        right_alipay = getViewById(R.id.right_alipay);
        zfb_left_img = getViewById(R.id.zfb_left_img);
        right_icon_ashing_jd = getViewById(R.id.right_icon_ashing_jd);
        left_icon_ashing_jd = getViewById(R.id.left_icon_ashing_jd);
        btn_ic_money = getViewById(R.id.btn_ic_money);
        btn_ic_money.setText(MainApplication.feeFh);
        lay_Clear = getViewById(R.id.lay_Clear);
        tx_time = getViewById(R.id.tx_time);
        prompt_txt = getViewById(R.id.prompt_txt);
        lay_rotating = getViewById(R.id.lay_rotating);
        lay_rotating.setOnClickListener(PayActivity.this);
        pay_left_flipper = getViewById(R.id.pay_left_flipper);
        pay_right_flipper = getViewById(R.id.pay_right_flipper);
        id_lin_add = getViewById(R.id.id_lin_add);
        etMoney = getViewById(R.id.et_money);
        etMoney.setFocusable(true);
        tvMoneyUppercase = (TextView) findViewById(R.id.tv_money_uppercase);
        ivPictureRotating = getViewById(R.id.iv_n_picture_rotating);
        cib_lay = getViewById(R.id.cib_lay);
        ivInputNumberDel = getViewById(R.id.fht_iv_input_number_del);
        ivInputNumberDel.setOnClickListener(this);
        button1 = getViewById(R.id.button1);
        button2 = getViewById(R.id.button2);
        button3 = getViewById(R.id.button3);
        button4 = getViewById(R.id.button4);
        button5 = getViewById(R.id.button5);
        button6 = getViewById(R.id.button6);
        button7 = getViewById(R.id.button7);
        button8 = getViewById(R.id.button8);
        button9 = getViewById(R.id.button9);
        buttonPoint = getViewById(R.id.buttonPoint);
        ly_to_scan_pay = getViewById(R.id.ly_to_scan_pay);
        button0 = getViewById(R.id.button0);

        onClickListener1 = getViewById(R.id.onClickListener1);
        setBgHeiht(ly_one);
        setBgHeiht(ly_three);
        onClickListener2 = getViewById(R.id.onClickListener2);
        onClickListener3 = getViewById(R.id.onClickListener3);
        onClickListener4 = getViewById(R.id.onClickListener4);
        onClickListener5 = getViewById(R.id.onClickListener5);
        onClickListener6 = getViewById(R.id.onClickListener6);
        onClickListener7 = getViewById(R.id.onClickListener7);
        onClickListener8 = getViewById(R.id.onClickListener8);
        onClickListener9 = getViewById(R.id.onClickListener9);
        onClickListenerPoint = getViewById(R.id.onClickListenerPoint);
//        onClickListenerClear = getViewById(R.id.onClickListenerClear);
        onClickListener0 = getViewById(R.id.onClickListener0);
        mBadgeView = getViewById(R.id.fee_message);
        mBadgeView.setBackground(8,getResources().getColor(R.color.title_bg_new));
        mBadgeView.setBadgeCount(2);
        mInputLayout = getViewById(R.id.ly_pay_inuput);
        mExFeeLayout = getViewById(R.id.extrafee_layout);
        //收款
        ly_to_scan_pay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                AppHelper.execVibrator(context);
                if (isPay) {
                    if (!NetworkUtils.isNetworkAvailable(PayActivity.this)) {
                        toastDialog(PayActivity.this, R.string.network_exception, null);
                        return;
                    }
                    toCaptureActivity();
                } else {//等号操作
                    if (currentMode == 1) {
                        if (sumTips.contains("+") && !sumTips.endsWith("+")) {
                            //相加
                            String arr[] = sumTips.split("\\+");
                            sumTips = Utils.subtractSum(arr[0], arr[1]);
                            twoTips = "";
                            fullValue();
                            setToPayView();
                        } else if (sumTips.contains("+") && sumTips.endsWith("+")) {
                            fullValue();
                            setToPayView();
                        } else if (sumTips.contains("-") && !sumTips.endsWith("-") && !sumTips.startsWith("-")) {
                            String arr[] = sumTips.split("\\-");
                            sumTips = Utils.subtract(arr[0], arr[1]);
                            twoTips = "";
                            fullValue();
                            setToPayView();
                        }

                    } else {
                        if (sum.contains("+") && !sum.endsWith("+")) {
                            //相加
                            String arr[] = sum.split("\\+");
                            sum = Utils.subtractSum(arr[0], arr[1]);
                            two = "";
                            fullValue();
                            setToPayView();
                        } else if (sum.contains("+") && sum.endsWith("+")) {
                            fullValue();
                            setToPayView();
                        } else if (sum.contains("-") && !sum.endsWith("-") && !sum.startsWith("-")) {
                            String arr[] = sum.split("\\-");
                            sum = Utils.subtract(arr[0], arr[1]);
                            two = "";
                            fullValue();
                            setToPayView();
                        }
                    }


                }
            }
        });

        initViewSurcharge();
        getViewById(R.id.ly_scan).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(context);
                if (!NetworkUtils.isNetworkAvailable(PayActivity.this)) {
                    toastDialog(PayActivity.this, R.string.network_exception, null);
                    return;
                }
                if (!StringUtil.isEmptyOrNull(etMoney.getText().toString()) && !TextUtils.equals(etMoney.getText(),"0.") || TextUtils.equals(etMoney.getText(),"0.0") || TextUtils.equals(etMoney.getText(),"0") || TextUtils.equals(etMoney.getText(),"0.00")) {
                    toCaptureActivity();
                } else {
                    CaptureActivity.startActivity(PayActivity.this, "pay");
                }

            }
        });
    }


    private void initViewSurcharge() {
        //默认设置
        id_title_surcharge.setText(MainApplication.feeFh + "0.00");
    }

    void setBgHeiht(LinearLayout ly) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) ly.getLayoutParams();
        params.height = DensityUtil.dip2px(PayActivity.this, 1);
    }

    long intTotlaMoney = 0;

    private boolean checkMoeny() {
        if (!NetworkUtils.isNetworkAvailable(PayActivity.this)) {
            toastDialog(PayActivity.this, R.string.network_exception, null);
            return false;
        }

        if (TextUtils.isEmpty(etMoney.getText())) {
            toastDialog(PayActivity.this, R.string.please_input_amount, null);
            return false;
        }
        if ( TextUtils.equals(etMoney.getText(),"0.") || TextUtils.equals(etMoney.getText(),"0.0")) {
            toastDialog(PayActivity.this, R.string.tv_money_error, null);
            return false;
        }

        if (sum.contains("+") && !sum.endsWith("+")) {
            //相加
            String arr[] = sum.split("\\+");
            sum = Utils.subtractSum(arr[0], arr[1]);
            two = "";
            fullValue();

        }

        String money = etMoney.getText().toString();

        if (money.endsWith(".")) {
            toastDialog(PayActivity.this, R.string.tv_money_error, null);
            return false;
        }
        if (money.contains(",")) {
            strTotalMoney = OrderManager.getInstance().getMoney(money.replace(",", ""));
        } else {
            strTotalMoney = OrderManager.getInstance().getMoney(money);
        }

        String surchargeMoney = id_title_surcharge.getText().toString();
        if (!surchargeMoney.isEmpty()) {

            if (surchargeMoney.endsWith(".")) {
                toastDialog(PayActivity.this, R.string.tv_money_error, null);
                return false;
            }

            if (surchargeMoney.contains(MainApplication.feeFh)) {
                strSurchargelMoney = surchargeMoney.replace(MainApplication.feeFh, "");
            }

            if (strSurchargelMoney.contains(",")) {
                strSurchargelMoney = OrderManager.getInstance().getMoney(strSurchargelMoney.replace(",", ""));
            } else {
                strSurchargelMoney = OrderManager.getInstance().getMoney(strSurchargelMoney);
            }
        }
        intTotlaMoney = Utils.Long.tryParse(strTotalMoney, 0);
        if (intTotlaMoney == 0) {
            //            showToastInfo(getStringById(R.string.please_input_amount));
            toastDialog(PayActivity.this, R.string.please_input_amount, null);
            return false;
        }

        if (intTotlaMoney < 0) {
            toastDialog(PayActivity.this, R.string.tv_pay_less_money, null);
            return false;
        }

        long surcharge = Utils.Long.tryParse(strSurchargelMoney, 0);
        long Totalmoney = intTotlaMoney+surcharge;
        if (Totalmoney > MAX_SALSE_VALUE) {
            toastDialog(PayActivity.this, R.string.tv_pay_exceed_money, null);
            return false;
        }

        return true;
    }

    /**
     * 生成二维码
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void toNewPay(String money, final String payType) {
        OrderManager.getInstance().unifiedNativePay(money, payType, 0, null, null, new UINotifyListener<QRcodeInfo>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                loadDialog(PayActivity.this, R.string.tv_pay_prompt);
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                dismissLoading();
                if (checkSession()) {
                    return;
                }

                if (object != null) {
                    PayActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toastDialog(PayActivity.this, object.toString(), null);
                        }
                    });
                }

            }

            @Override
            public void onSucceed(QRcodeInfo result) {
                super.onSucceed(result);
                dismissLoading();
                if (result != null) {
                    result.setPayType(payType);
                    result.setWitch(isWitch);
                    ShowQRcodeActivity.startActivity(result, PayActivity.this);
                }

            }
        });
    }

    private void onClickPay(final String payType) {

        AppHelper.execVibrator(context);

        if (!checkMoeny()) {
            return;
        }
        String msg = getString(R.string.spanning_pay_wechat_code);
        if (payType.equals(MainApplication.PAY_QQ_NATIVE) || payType.equalsIgnoreCase(MainApplication.PAY_QQ_NATIVE1)) {
            msg = getString(R.string.spanning_pay_qq_code);
        } else if (payType.equals(MainApplication.PAY_ZFB_NATIVE) || payType.startsWith(MainApplication.PAY_ZFB_NATIVE1)) {
            msg = getString(R.string.spanning_pay_zfb_code);
        } else if (payType.equals(MainApplication.PAY_JINGDONG_NATIVE)) {
            msg = getString(R.string.spanning_pay_jd_code);
        }

        dialog1 = new ProgressInfoDialog(PayActivity.this, msg, new ProgressInfoDialog.HandleBtn() {

            @Override
            public void handleOkBtn() {
                showConfirm(getStringById(R.string.interrupt_trading), dialog1);
            }

        });

        OrderManager.getInstance().unifiedNativePay(strTotalMoney, payType, 0, null, null, new UINotifyListener<QRcodeInfo>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                DialogHelper.resize(PayActivity.this, dialog1);
                if (!dialog1.isShowing()) {
                    dialog1.show();
                }
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                dialog1.dismiss();
                if (dialogInfo != null) {
                    dialogInfo.dismiss();
                }

                if (checkSession()) {
                    return;
                }

                if (!isStop) {
                    return;
                }
                if (object != null) {
                    PayActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            DialogInfo dialogInfo = new DialogInfo(PayActivity.this, getStringById(R.string.public_cozy_prompt), object.toString(), getStringById(R.string.btnOk), DialogInfo.REGISTFLAG, null, null);
                            DialogHelper.resize(PayActivity.this, dialogInfo);
                            dialogInfo.show();
                        }
                    });
                }

            }

            @Override
            public void onSucceed(QRcodeInfo result) {
                super.onSucceed(result);
                dialog1.dismiss();
                if (dialogInfo != null) {
                    dialogInfo.dismiss();
                }
                if (result != null && isStop) {
                    result.setPayType(payType);
                    result.setWitch(isWitch);
                    ShowQRcodeActivity.startActivity(result, PayActivity.this);
                    //clearData();
                } else {
                    isStop = true;
                }
            }
        });

    }


//    String isOpenFisrtpayType() {
//        Object object = SharedPreUtile.readProduct("dynPayType" + ApiConstant.bankCode + MainApplication.getMchId());
//
//        if (object != null) {
//            List<DynModel> list = (List<DynModel>) object;
//            if (null != list && list.size() > 0 && list.size() == 1) {
//                return list.get(0).getApiCode();
//            } else {
//                return null;
//            }
//        } else {
//            return null;
//        }
//    }

//    private String setType() {
//        if (!StringUtil.isEmptyOrNull(MainApplication.serviceType)) {
//            String[] arrPays = MainApplication.serviceType.split("\\|");
//            payMeths.clear();
//            for (int s = 0; s < arrPays.length; s++) {
//                if (arrPays[s].equalsIgnoreCase(MainApplication.PAY_QQ_NATIVE) || arrPays[s].equalsIgnoreCase(MainApplication.PAY_QQ_NATIVE1)) {
//                    payMeths.add(arrPays[s]);
//                    if (payMeths.contains(MainApplication.PAY_QQ_NATIVE) && payMeths.contains(MainApplication.PAY_QQ_NATIVE1)) {
//                        payMeths.remove(MainApplication.PAY_QQ_NATIVE1);
//                    }
//
//                } else if (arrPays[s].startsWith(MainApplication.PAY_WX_NATIVE)) {
//                    //微信支付
//                    payMeths.add(arrPays[s]);
//                } else if (arrPays[s].equals(MainApplication.PAY_ZFB_NATIVE) || arrPays[s].equals(MainApplication.PAY_ZFB_NATIVE1)) {
//                    //支付宝支付
//                    payMeths.add(arrPays[s]);
//                    if (payMeths.contains(MainApplication.PAY_ZFB_NATIVE) && payMeths.contains(MainApplication.PAY_ZFB_NATIVE1)) {
//                        payMeths.remove(MainApplication.PAY_ZFB_NATIVE);
//                    }
//                } else if (arrPays[s].equalsIgnoreCase(MainApplication.PAY_JINGDONG_NATIVE)) {
//                    payMeths.add(arrPays[s]);
//                }
//            }
//
//            if (payMeths.size() > 1) {
//                return null;
//            } else {
//                if (payMeths.size() > 0) {
//                    return payMeths.get(0);
//                } else {
//                    return null;
//                }
//            }
//        } else {
//            return null;
//        }
//    }

    private void setLister() {
        iv_clear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lay_notify.setVisibility(View.GONE);

            }
        });

        lay_Clear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(context);
                clearData();
            }
        });

        onClickListener0.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(context);
                executeValueNew(buttonPoint.getTag().toString());
                fullValue();
            }
        });

        onClickListener10 = getViewById(R.id.onClickListener10);

        //加号
        onClickListener10.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                AppHelper.execVibrator(context);
                executeValueNew(button10.getTag().toString());
                fullValue();
                if (!(etMoney.getText().toString().equalsIgnoreCase("0.0") || etMoney.getText().toString().equalsIgnoreCase("0.00"))) {
                    setToEqView();
                }
            }
        });
        //等号

        onClickListener1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(context);
                executeValueNew(button1.getTag().toString());
                fullValue();
            }
        });
        onClickListener2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(context);
                executeValueNew(button2.getTag().toString());
                fullValue();
            }
        });
        onClickListener3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(context);
                executeValueNew(button3.getTag().toString());
                fullValue();
            }
        });
        onClickListener4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(context);
                executeValueNew(button4.getTag().toString());
                fullValue();
            }
        });
        onClickListener5.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(context);
                executeValueNew(button5.getTag().toString());
                fullValue();
            }
        });
        onClickListener6.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(context);
                executeValueNew(button6.getTag().toString());
                fullValue();
            }
        });
        onClickListener7.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(context);
                executeValueNew(button7.getTag().toString());
                fullValue();
            }
        });
        onClickListener8.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(context);
                executeValueNew(button8.getTag().toString());
                fullValue();
            }
        });
        onClickListener9.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(context);
                executeValueNew(button9.getTag().toString());
                //                fullValueNew();
                fullValue();
            }
        });
        onClickListenerPoint.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(context);
                executeValueNew(button0.getTag().toString());
                fullValue();
            }
        });

        //新增点击Charge进扫码
        mInputLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppHelper.execVibrator(context);
                if (isPay) {
                    if (!NetworkUtils.isNetworkAvailable(PayActivity.this)) {
                        toastDialog(PayActivity.this, R.string.network_exception, null);
                        return;
                    }
                    toCaptureActivity();
                } else {//等号操作
                    if (currentMode == 1) {
                        if (sumTips.contains("+") && !sumTips.endsWith("+")) {
                            //相加
                            String arr[] = sumTips.split("\\+");
                            sumTips = Utils.subtractSum(arr[0], arr[1]);
                            twoTips = "";
                            fullValue();
                            setToPayView();
                        } else if (sumTips.contains("+") && sumTips.endsWith("+")) {
                            fullValue();
                            setToPayView();
                        } else if (sumTips.contains("-") && !sumTips.endsWith("-") && !sumTips.startsWith("-")) {
                            String arr[] = sumTips.split("\\-");
                            sumTips = Utils.subtract(arr[0], arr[1]);
                            twoTips = "";
                            fullValue();
                            setToPayView();
                        }

                    } else {
                        if (sum.contains("+") && !sum.endsWith("+")) {
                            //相加
                            String arr[] = sum.split("\\+");
                            sum = Utils.subtractSum(arr[0], arr[1]);
                            two = "";
                            fullValue();
                            setToPayView();
                        } else if (sum.contains("+") && sum.endsWith("+")) {
                            fullValue();
                            setToPayView();
                        } else if (sum.contains("-") && !sum.endsWith("-") && !sum.startsWith("-")) {
                            String arr[] = sum.split("\\-");
                            sum = Utils.subtract(arr[0], arr[1]);
                            two = "";
                            fullValue();
                            setToPayView();
                        }
                    }


                }
            }
        });
        //新增查看费率信息
        mExFeeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*final Intent it = new Intent();
                it.setClass(context, FeeTipsActivity.class);
                it.putExtra("money", strTotalMoney);
                it.putExtra("surcharge", strSurchargelMoney);
                it.putExtra("tips", strTipMoney);
                it.putExtra("payType", payType);
                startActivity(it);*/
            }
        });

//        onClickListenerClear.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                etNumberClear();
//
//            }
//        });

        id_tips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMode = 1;
                sum = "";
                id_line_first.setBackgroundColor(getResources().getColor(R.color.tv_no_focus));
                id_line_second.setBackgroundColor(getResources().getColor(R.color.white));
                btn_ic_money.setTextColor(getResources().getColor(R.color.tv_no_focus));
                etMoney.setTextColor(getResources().getColor(R.color.tv_no_focus));
                id_tips.setTextColor(getResources().getColor(R.color.white));
                id_title_addtips.setTextColor(getResources().getColor(R.color.white));
            }
        });

//        etMoney.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                currentMode = 0;
//                id_line_first.setBackgroundColor(getResources().getColor(R.color.white));
//                id_line_second.setBackgroundColor(getResources().getColor(R.color.tv_no_focus));
//                btn_ic_money.setTextColor(getResources().getColor(R.color.white));
//                etMoney.setTextColor(getResources().getColor(R.color.white));
//                id_tips.setTextColor(getResources().getColor(R.color.tv_no_focus));
//                id_title_addtips.setTextColor(getResources().getColor(R.color.tv_no_focus));
//            }
//        });
    }



    void setToEqView() {
        isPay = false;
        iv_icon.setVisibility(View.GONE);
        id_lin_add.setVisibility(View.VISIBLE);
    }

    void setToPayView() {
        isPay = true;
        iv_icon.setVisibility(View.VISIBLE);
        id_lin_add.setVisibility(View.GONE);
    }


    private void toPay(final String payType) {

        onClickPay(payType);

    }

    public class MyAdapter extends PagerAdapter {

        private int mChildCount = 0;

        @Override
        public void notifyDataSetChanged() {
            mChildCount = getCount();
            super.notifyDataSetChanged();
        }

        @Override
        public int getItemPosition(Object object) {
            if (mChildCount > 0) {
                mChildCount--;
                return POSITION_NONE;
            }
            return super.getItemPosition(object);
        }

        @Override
        public int getCount() {
            if (views.size() < 3) {
                return views.size();
            } else {
                return Integer.MAX_VALUE;
            }
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public void destroyItem(View container, int position, Object object) {
            //            ((ViewPager)container).removeView(mImageViews[position % mImageViews.length]);

        }

        /**
         * 载入图片进去，用当前的position 除以 图片数组长度取余数是关键
         */
        @Override
        public Object instantiateItem(View container, int position) {
            try {
                ((ViewPager) container).addView(views.get(position % views.size()), 0);
            } catch (Exception e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }
            return views.get(position % views.size());
        }

        @Override
        public void finishUpdate(View arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {
            // TODO Auto-generated method stub

        }

        @Override
        public Parcelable saveState() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void startUpdate(View arg0) {
            // TODO Auto-generated method stub

        }
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
        //               Log.i("leo", "");

    }

    /**
     * {@inheritDoc}
     */
    @SuppressLint("NewApi")
    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {

    }

    /**
     * {@inheritDoc}
     */

    @Override
    public void onPageSelected(int arg0) {
        int postion = arg0 % views.size();
        Logger.i("hehui", "setDefPayMethod-->" + setDefPayMethod + ",arg0-->" + arg0);
        if (arg0 < lastIndex) {
            //从左向右滑
            pay_left_flipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.in_left_right));
            pay_left_flipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.out_center_right));
            pay_left_flipper.showNext();

            pay_right_flipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.in_left_right));
            pay_right_flipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.out_center_right));
            pay_right_flipper.showPrevious();

        } else if (arg0 > lastIndex) {
            //从右向左滑
            pay_right_flipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.in_right_left));
            pay_right_flipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.out_center_left));
            pay_right_flipper.showNext();

            pay_left_flipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.in_right_left));
            pay_left_flipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.out_center_left));
            pay_left_flipper.showPrevious();
        }
        mySetItem(postion);
        lastIndex = arg0;
    }

    private void mySetItem(int postion) {
        try {
            int index = (Integer) views.get(postion).getTag();

            switch (index) {
                case 0: // 微信支付

                    //                    titleBar.setRightLayVisible(true, R.drawable.icon_pay_sweep_wechat);
                    //buy_lay.setBackgroundResource(R.drawable.btn_wx_pay);
                    payType = MainApplication.PAY_WX_MICROPAY;
                    break;
                case 1: //手Q支付

                    //                    titleBar.setRightLayVisible(true, R.drawable.icon_pay_sweep_wechat);
                    //buy_lay.setBackgroundResource(R.drawable.btn_qq_pay);
                    payType = MainApplication.PAY_QQ_NATIVE;
                    break;

                case 2: // 支付宝支付
                    //                    titleBar.setRightLayVisible(true, R.drawable.icon_pay_sweep_pay);
                    //buy_lay.setBackgroundResource(R.drawable.btn_pay_chose);
                    payType = MainApplication.PAY_ZFB_MICROPAY;
                    break;
                case 3: // 京东钱包支付
                    //                    titleBar.setRightLayVisible(true, R.drawable.icon_pay_sweep_pay);
                    //buy_lay.setBackgroundResource(R.drawable.btn_pay_chose);
                    payType = MainApplication.PAY_JINGDONG_NATIVE;
                    break;

            }
        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }

    }

    boolean isFirstWx, isFirstzfb, isFirstJd, isFirstQq = true;

    /*
     * 设置默认支付方式
     */
    private void setPayMehotd(String payType) {
        if (payMeths.size() == 4) {
            views.clear();
            if (payType.equalsIgnoreCase(MainApplication.PAY_QQ_NATIVE1)) {//设置qq默认支付方式
                views.add(0, qqView);
                views.add(1, wxView);
                views.add(2, zfbView);
                views.add(3, jdView);

                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(left_icon_ashing_jd);
                pay_left_flipper.addView(zfb_left_img);
                pay_left_flipper.addView(left_icon_wechat);
                pay_left_flipper.addView(left_icon_qq);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_wechat);
                pay_right_flipper.addView(right_alipay);
                pay_right_flipper.addView(right_icon_ashing_jd);
                pay_right_flipper.addView(right_icon_qq);
                setDefPayMethod = true;
            } else if (payType.equals(MainApplication.PAY_ZFB_NATIVE1)) { // 支付宝默认支付
                views.add(0, zfbView);
                views.add(1, wxView);
                views.add(2, qqView);
                views.add(3, jdView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(left_icon_ashing_jd);
                pay_left_flipper.addView(left_icon_qq);
                pay_left_flipper.addView(left_icon_wechat);
                pay_left_flipper.addView(zfb_left_img);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_wechat);
                pay_right_flipper.addView(right_icon_qq);
                pay_right_flipper.addView(right_icon_ashing_jd);
                pay_right_flipper.addView(right_alipay);
                setDefPayMethod = true;
            } else if (payType.equals(MainApplication.PAY_JINGDONG_NATIVE)) {
                views.add(0, jdView);
                views.add(1, wxView);
                views.add(2, qqView);
                views.add(3, zfbView);

                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(zfb_left_img);
                pay_left_flipper.addView(left_icon_qq);
                pay_left_flipper.addView(left_icon_wechat);
                pay_left_flipper.addView(left_icon_ashing_jd);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_wechat);
                pay_right_flipper.addView(right_icon_qq);
                pay_right_flipper.addView(right_alipay);
                pay_right_flipper.addView(right_icon_ashing_jd);
                setDefPayMethod = true;
            } else {
                views.add(0, wxView);
                views.add(1, qqView);
                views.add(2, jdView);
                views.add(3, zfbView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(zfb_left_img);
                pay_left_flipper.addView(left_icon_ashing_jd);
                pay_left_flipper.addView(left_icon_qq);
                pay_left_flipper.addView(left_icon_wechat);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_qq);
                pay_right_flipper.addView(right_icon_ashing_jd);
                pay_right_flipper.addView(right_alipay);
                pay_right_flipper.addView(right_icon_wechat);
                setDefPayMethod = true;
            }
            myAdapter.notifyDataSetChanged();
            lastIndex = (views.size()) * 100;
            viewPager.setCurrentItem(lastIndex);
        } else if (payMeths.size() == 3) {
            setPayDefMethod(payType);
        } else { //2种支付类型
            if (payMeths.contains(MainApplication.PAY_WX_NATIVE) && (payMeths.contains(MainApplication.PAY_QQ_NATIVE) || payMeths.contains(MainApplication.PAY_QQ_NATIVE1))) {//微信 qq
                if (payType.equals(MainApplication.PAY_QQ_NATIVE1)) {
                    viewPager.setCurrentItem(1);
                } else {
                    viewPager.setCurrentItem(0);
                }
            } else if (payMeths.contains(MainApplication.PAY_WX_NATIVE) && (payMeths.contains(MainApplication.PAY_ZFB_NATIVE) || payMeths.contains(MainApplication.PAY_ZFB_NATIVE1))) { // 微信，支付宝
                if (payType.equals(MainApplication.PAY_ZFB_NATIVE1)) {
                    viewPager.setCurrentItem(1);
                } else {
                    viewPager.setCurrentItem(0);
                }
            } else if (payMeths.contains(MainApplication.PAY_WX_NATIVE) && payMeths.contains(MainApplication.PAY_JINGDONG_NATIVE)) { // 微信 京东
                if (payType.equals(MainApplication.PAY_JINGDONG_NATIVE)) {
                    viewPager.setCurrentItem(1);
                } else {
                    viewPager.setCurrentItem(0);
                }
            } else if ((payMeths.contains(MainApplication.PAY_QQ_NATIVE) || payMeths.contains(MainApplication.PAY_QQ_NATIVE1)) && (payMeths.contains(MainApplication.PAY_ZFB_NATIVE) || payMeths.contains(MainApplication.PAY_ZFB_NATIVE1))) {
                //qq 支付宝
                if (payType.equals(MainApplication.PAY_ZFB_NATIVE1)) {
                    viewPager.setCurrentItem(0);
                } else {
                    viewPager.setCurrentItem(1);
                }
            } else if ((payMeths.contains(MainApplication.PAY_QQ_NATIVE) || payMeths.contains(MainApplication.PAY_QQ_NATIVE1)) && payMeths.contains(MainApplication.PAY_JINGDONG_NATIVE)) {
                if (payType.equals(MainApplication.PAY_QQ_NATIVE1)) {
                    viewPager.setCurrentItem(0);
                } else {
                    viewPager.setCurrentItem(1);
                }
            } else {
                // 支付宝 京东
                if (payType.equals(MainApplication.PAY_ZFB_NATIVE1)) {
                    viewPager.setCurrentItem(0);
                } else {
                    viewPager.setCurrentItem(1);
                }
            }
        }
    }

    private void setPayDefMethod(String payType) {

        // 有微信支付
        if ((payMeths.contains(MainApplication.PAY_WX_NATIVE) || payMeths.contains(MainApplication.PAY_WX_NATIVE_INTL)) && (payMeths.contains(MainApplication.PAY_QQ_NATIVE) || payMeths.contains(MainApplication.PAY_QQ_NATIVE1)) && (payMeths.contains(MainApplication.PAY_ZFB_NATIVE) || payMeths.contains(MainApplication.PAY_ZFB_NATIVE1))) { // 微信支付宝，手Q，支付宝){
            if (payType.equalsIgnoreCase(MainApplication.PAY_QQ_NATIVE1)) {
                views.clear();
                views.add(0, qqView);
                views.add(1, wxView);
                views.add(2, zfbView);
                myAdapter.notifyDataSetChanged();
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(zfb_left_img);
                pay_left_flipper.addView(left_icon_wechat);
                pay_left_flipper.addView(left_icon_qq);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_wechat);
                pay_right_flipper.addView(right_alipay);
                pay_right_flipper.addView(right_icon_qq);
            } else if (payType.equals(MainApplication.PAY_ZFB_NATIVE1)) {
                views.clear();
                views.add(0, zfbView);
                views.add(1, wxView);
                views.add(2, qqView);
                myAdapter.notifyDataSetChanged();
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(left_icon_qq);
                pay_left_flipper.addView(left_icon_wechat);
                pay_left_flipper.addView(zfb_left_img);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_wechat);
                pay_right_flipper.addView(right_icon_qq);
                pay_right_flipper.addView(right_alipay);
            } else {
                views.clear();
                views.add(0, wxView);
                views.add(1, qqView);
                views.add(2, zfbView);
                myAdapter.notifyDataSetChanged();

                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(zfb_left_img);
                pay_left_flipper.addView(left_icon_qq);
                pay_left_flipper.addView(left_icon_wechat);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_qq);
                pay_right_flipper.addView(right_alipay);
                pay_right_flipper.addView(right_icon_wechat);
            }

            setDefPayMethod = true;
        } else if ((payMeths.contains(MainApplication.PAY_WX_NATIVE) || payMeths.contains(MainApplication.PAY_WX_NATIVE_INTL)) && (payMeths.contains(MainApplication.PAY_QQ_NATIVE) || payMeths.contains(MainApplication.PAY_QQ_NATIVE1)) && payMeths.contains(MainApplication.PAY_JINGDONG_NATIVE)) { //  微信支付宝，手Q，jd
            views.clear();
            if (payType.equalsIgnoreCase(MainApplication.PAY_QQ_NATIVE1)) {
                views.add(0, qqView);
                views.add(1, wxView);
                views.add(2, jdView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(left_icon_ashing_jd);
                pay_left_flipper.addView(left_icon_wechat);
                pay_left_flipper.addView(left_icon_qq);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_wechat);
                pay_right_flipper.addView(right_icon_ashing_jd);
                pay_right_flipper.addView(right_icon_qq);
            } else if (payType.equals(MainApplication.PAY_JINGDONG_NATIVE)) {
                views.add(0, jdView);
                views.add(1, wxView);
                views.add(2, qqView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(left_icon_qq);
                pay_left_flipper.addView(left_icon_wechat);
                pay_left_flipper.addView(left_icon_ashing_jd);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_wechat);
                pay_right_flipper.addView(right_icon_qq);
                pay_right_flipper.addView(right_icon_ashing_jd);
            } else {
                views.add(0, wxView);
                views.add(1, qqView);
                views.add(2, jdView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(left_icon_ashing_jd);
                pay_left_flipper.addView(left_icon_qq);
                pay_left_flipper.addView(left_icon_wechat);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_qq);
                pay_right_flipper.addView(right_icon_ashing_jd);
                pay_right_flipper.addView(right_icon_wechat);
            }
            myAdapter.notifyDataSetChanged();
            setDefPayMethod = true;
        } else if ((payMeths.contains(MainApplication.PAY_WX_NATIVE) || payMeths.contains(MainApplication.PAY_WX_NATIVE_INTL)) && (payMeths.contains(MainApplication.PAY_ZFB_NATIVE) || payMeths.contains(MainApplication.PAY_ZFB_NATIVE1)) && payMeths.contains(MainApplication.PAY_JINGDONG_NATIVE)) { //  微信支付宝，支付宝，jd
            views.clear();
            if (payType.equals(MainApplication.PAY_ZFB_NATIVE1)) {
                views.add(0, zfbView);
                views.add(1, wxView);
                views.add(2, jdView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(left_icon_ashing_jd);
                pay_left_flipper.addView(left_icon_wechat);
                pay_left_flipper.addView(zfb_left_img);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_wechat);
                pay_right_flipper.addView(right_icon_ashing_jd);
                pay_right_flipper.addView(right_alipay);
            } else if (payType.equals(MainApplication.PAY_JINGDONG_NATIVE)) {
                views.add(0, jdView);
                views.add(1, wxView);
                views.add(2, zfbView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(zfb_left_img);
                pay_left_flipper.addView(left_icon_wechat);
                pay_left_flipper.addView(left_icon_ashing_jd);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_wechat);
                pay_right_flipper.addView(right_alipay);
                pay_right_flipper.addView(right_icon_ashing_jd);
            } else {
                views.add(0, wxView);
                views.add(1, jdView);
                views.add(2, zfbView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(zfb_left_img);
                pay_left_flipper.addView(left_icon_ashing_jd);
                pay_left_flipper.addView(left_icon_wechat);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_ashing_jd);
                pay_right_flipper.addView(right_alipay);
                pay_right_flipper.addView(right_icon_wechat);
            }

            myAdapter.notifyDataSetChanged();
            setDefPayMethod = true;
        } else {//手Q，支付宝，jd
            views.clear();
            if (payType.equals(MainApplication.PAY_QQ_NATIVE1)) {
                views.add(0, qqView);
                views.add(1, jdView);
                views.add(2, zfbView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(zfb_left_img);
                pay_left_flipper.addView(left_icon_ashing_jd);
                pay_left_flipper.addView(left_icon_qq);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_ashing_jd);
                pay_right_flipper.addView(right_alipay);
                pay_right_flipper.addView(right_icon_qq);
            } else if (payType.equals(MainApplication.PAY_JINGDONG_NATIVE)) {
                views.add(0, jdView);
                views.add(1, qqView);
                views.add(2, zfbView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(zfb_left_img);
                pay_left_flipper.addView(left_icon_qq);
                pay_left_flipper.addView(left_icon_ashing_jd);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_qq);
                pay_right_flipper.addView(right_alipay);
                pay_right_flipper.addView(right_icon_ashing_jd);
            } else {
                views.add(0, zfbView);
                views.add(1, qqView);
                views.add(2, jdView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(left_icon_ashing_jd);
                pay_left_flipper.addView(left_icon_qq);
                pay_left_flipper.addView(zfb_left_img);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_qq);
                pay_right_flipper.addView(right_icon_ashing_jd);
                pay_right_flipper.addView(right_alipay);
            }
            myAdapter.notifyDataSetChanged();
            setDefPayMethod = true;
        }

        lastIndex = (views.size()) * 100;
        viewPager.setCurrentItem(lastIndex);
    }

//    /**
//     * 修改公开状态
//     * <功能详细描述>
//     *
//     * @see [类、类#方法、类#成员]
//     */
//    private void updatePointState(String noticeId) {
//        String noticePre = PreferenceUtil.getString("noticePre", "");
//        if (!StringUtil.isEmptyOrNull(noticePre) && !StringUtil.isEmptyOrNull(noticeId)) {
//            PreferenceUtil.removeKey("noticePre");
//            Map<String, String> mapNotice = JsonUtil.jsonToMap(noticePre);
//            for (Map.Entry<String, String> entry : mapNotice.entrySet()) {
//                String value = entry.getKey();
//                if (noticeId.equals(value)) { //代表还有公开没有被打开查看详情过
//                    mapNotice.put(value, "1");
//                }
//            }
//            HandlerManager.notifyMessage(HandlerManager.NOTICE_TYPE, HandlerManager.NOTICE_TYPE);
//            PreferenceUtil.commitString("noticePre", JsonUtil.mapToJson(mapNotice));
//        }
//
//    }


    public View getView() {
        this.inflater = LayoutInflater.from(mContext);
        view = inflater.inflate(R.layout.activity_pay, null);

        return view;
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    /**
     * {@inheritDoc}
     */

   /* @Override
    public boolean onLongClick(View v) {
        switch (v.getId()) {
            case R.id.rl_wxpay:
                ApiConstant.isLocaltionQRcode = true;
                //                checkData();
                break;

            case R.id.pay_first_chose: // 手q支付
                if (null != ApiConstant.bankType && !"".equals(ApiConstant.bankType) && ApiConstant.bankType != 1) {
                    toLongPay();
                }
                break;
            case R.id.pay_second_chose: // 微信支付
                if (null != ApiConstant.bankType && !"".equals(ApiConstant.bankType) && ApiConstant.bankType != 1) {
                    toLongPay();
                }
                break;
            case R.id.pay_three_chose: // 支付宝支付
                if (null != ApiConstant.bankType && !"".equals(ApiConstant.bankType) && ApiConstant.bankType != 1) {
                    toLongPay();
                }
                break;
        }
        return true;
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.rl_wxpay:
//                ApiConstant.isLocaltionQRcode = false;
//                checkData(true);
//                break;
            /* case R.id.bt_scan:
                 toCaptureActivity();
                 break;*/
//            case R.id.rl_posPay:
//                //                toNewpostech(v); // pos刷卡
//                checkData(false);
//                break;
            case R.id.fht_iv_input_number_del:
                clearData();
                break;
            case R.id.iv_n_picture_rotating:
                if (ApiConstant.TEMP_USER) {
                    showToastInfo(getStringById(R.string.pay_not_permission));
                    break;
                }

//                checkExitAuth(true);
                //                loadShopData(true);
                break;
//            case R.id.pay_four_chose: // 京东扫码支付
//                toPay(MainApplication.PAY_JINGDONG_NATIVE);
//                break;
//            case R.id.pay_first_chose: // 手q支付
//                if (payMeths.contains(MainApplication.PAY_QQ_NATIVE)) {
//                    toPay(MainApplication.PAY_QQ_NATIVE);
//                } else {
//                    toPay(MainApplication.PAY_QQ_NATIVE1);
//                }
//                break;
            case R.id.pay_second_chose: // 微信支付
                //                sharPreference.edit()
                //                    .putString("payMethStr", payMeth[0] + "," + payMeth[1] + "," + payMeth[2])
                //                    .commit();
                //                sharPreference.edit().putString("payIconId", payIcon[0] + "," + payIcon[1] + "," + payIcon[2]).commit();

                toPay(payNatieType);
                break;
            case R.id.pay_three_chose: // 支付宝支付
                //                sharPreference.edit()
                //                    .putString("payMethStr", payMeth[0] + "," + payMeth[1] + "," + payMeth[2])
                //                    .commit();
                //                sharPreference.edit().putString("payIconId", payIcon[0] + "," + payIcon[1] + "," + payIcon[2]).commit();
                if (payMeths.contains(MainApplication.PAY_ZFB_NATIVE1)) {
                    toPay(MainApplication.PAY_ZFB_NATIVE1);
                } else {
                    toPay(MainApplication.PAY_ZFB_NATIVE);
                }
                break;
            case R.id.lay_rotating: // 活动详情
                lay_rotating.setVisibility(View.GONE);
//                AwardDetiActivity.startActivity(PayActivity.this, url);
                break;
        }
    }

    /**
     * 扫描二维码
     */
    private void toCaptureActivity() {
        if (!checkMoeny()) {
            return;
        }
        Intent it = new Intent();
        it.setClass(context, CaptureActivity.class);
        it.putExtra("money", strTotalMoney);
        it.putExtra("surcharge", strSurchargelMoney);
        it.putExtra("tips", strTipMoney);
        it.putExtra("payType", payType);
        startActivity(it);

    }

    private int deletePoint;
    private int deletePointTips;

    private void executeValueNew(String tag) {
        if (currentMode == 1) {
            if (tag.equals("")) {
                sumTips = "0";
            } else {
                if (sumTips.equals(tag)) {
                    if (sumTips.equals("0")) {
                        sumTips = tag;
                    } else {
                        sumTips = sumTips + tag;
                    }
                } else {

                    if (StringUtil.isEmptyOrNull(sumTips) && (tag.equals("00") || tag.equals("."))) {
                        return;
                    }
                    if (sumTips.contains("+")) {
                        if (tag.equals("+")) {//
                            if (sumTips.endsWith("+")) {
                                return;
                            } else {//相加
                                String arr[] = sumTips.split("\\+");
                                sumTips = Utils.subtractSum(arr[0], arr[1]) + "+";
                                twoTips = "";
                            }
                        } else {
                            if (StringUtil.isEmptyOrNull(twoTips) && tag.equals(".")) {
                                return;
                            }
                            if (twoTips.contains(".") && tag.equals(".")) {
                                return;
                            }
                            if (twoTips.contains(".")) {
                                if (twoTips.substring(twoTips.indexOf(".") + 1, twoTips.length()).length() >= 2) {
                                    return;
                                } else {
                                    if (twoTips.equals("0.0") && tag.equals("0")) {
                                        return;
                                    }
                                    twoTips = twoTips + tag;
                                    if (Utils.compareTo(twoTips, maxNum)) {//大于10亿
                                        twoTips = twoTips.substring(0, twoTips.length() - 1);
                                    }
                                    String pase = sumTips.substring(0, sumTips.indexOf("+") + 1);
                                    sumTips = pase + twoTips;
                                }
                            } else {
                                if (twoTips.startsWith("0") && tag.equals("0")) {
                                    return;
                                }
                                twoTips = twoTips + tag;
                                if (Utils.compareTo(twoTips, maxNum)) {//大于10亿
                                    if (twoTips.length() == 11) {
                                        twoTips = twoTips.substring(0, twoTips.length() - 2);
                                    } else {
                                        twoTips = twoTips.substring(0, twoTips.length() - 1);
                                    }
                                }
                                String pase = sumTips.substring(0, sumTips.indexOf("+") + 1);
                                sumTips = pase + twoTips;
                            }
                        }
                    } else if (sumTips.contains("-")) //
                    {
                        if (tag.equals("-")) {
                            if (sumTips.endsWith("-")) {
                                return;
                            } else {//相加
                                String arr[] = sumTips.split("\\-");
                                //                                long m = Long.parseLong(arr[0]) - Long.parseLong(arr[1]);
                                sumTips = Utils.subtract(arr[0], arr[1]) + "-";
                                twoTips = "";
                            }
                        } else if (tag.equals("+")) {
                            if (sumTips.endsWith("-")) {
                                sumTips = sumTips.substring(0, sumTips.length() - 1) + "+";
                                return;
                            }
                            if (sumTips.startsWith("-")) {//负数的时候
                                sumTips = sumTips + "+";
                            } else {
                                String arr[] = sumTips.split("\\-");
                                //                            long m = Long.parseLong(arr[0]) - Long.parseLong(arr[1]);
                                sumTips = Utils.subtract(arr[0], arr[1]) + "+";
                            }
                            twoTips = "";
                        } else {

                            if (StringUtil.isEmptyOrNull(twoTips) && tag.equals(".")) {
                                return;
                            }
                            if (twoTips.contains(".") && tag.equals(".")) {
                                return;
                            }
                            if (twoTips.contains(".")) {
                                if (twoTips.substring(twoTips.indexOf(".") + 1, twoTips.length()).length() >= 2) {
                                    return;
                                } else {
                                    if (twoTips.equals("0.0") && tag.equals("0")) {
                                        return;
                                    }
                                    twoTips = twoTips + tag;
                                    if (Double.parseDouble(twoTips) > 50000) {//大于5万
                                        twoTips = twoTips.substring(0, twoTips.length() - 1);
                                    }
                                    String pase = sumTips.substring(0, sumTips.indexOf("-") + 1);
                                    sumTips = pase + twoTips;

                                }
                            } else {
                                if (twoTips.startsWith("0") && tag.equals("0")) {
                                    return;
                                }
                                twoTips = twoTips + tag;
                                if (Double.parseDouble(twoTips) > 50000) {//大于5万
                                    twoTips = twoTips.substring(0, twoTips.length() - 1);
                                }
                                String pase = sumTips.substring(0, sumTips.indexOf("-") + 1);
                                sumTips = pase + twoTips;
                            }
                        }

                    } else {
                        if (tag.equals("+")) { //加号
                            sumTips = sumTips + tag;
                        } else if (tag.equals("-")) {
                            sumTips = sumTips + tag;
                        } else {//

                            if (sumTips.contains(".") && tag.equals(".")) {
                                return;
                            }
                            formFloat(tag);
                        }
                    }

                }

            }
        } else {//---------------没有小费的情况
            if (tag.equals("")) {
                sum = "0";
            } else {
                if (sum.equals(tag)) {
                    if (sum.equals("0")) {
                        sum = tag;
                    } else {
                        sum = sum + tag;
                    }
                } else {

                    if (StringUtil.isEmptyOrNull(sum) && (tag.equals("00") || tag.equals("."))) {
                        return;
                    }
                    if (sum.contains("+")) {
                        if (tag.equals("+")) {//如果当前输入的就是+号
                            if (sum.endsWith("+")) {
                                return;
                            } else {//相加
                                String arr[] = sum.split("\\+");
                                //                                long m = Long.parseLong(arr[0]) + Long.parseLong(arr[1]);
                                sum = Utils.subtractSum(arr[0], arr[1]) + "+";
                                two = "";
                            }
                        } else {
                            if (StringUtil.isEmptyOrNull(two) && tag.equals(".")) {
                                return;
                            }
                            if (two.contains(".") && tag.equals(".")) {
                                return;
                            }
                            if (two.contains(".")) {
                                if (two.substring(two.indexOf(".") + 1, two.length()).length() >= 2) {
                                    return;
                                } else {
                                    if (two.equals("0.0") && tag.equals("0")) {
                                        return;
                                    }
                                    two = two + tag;
                                    if (Utils.compareTo(two, maxNum)) {//大于999 999 999 .99
                                        two = two.substring(0, two.length() - 1);
                                    }
                                    String pase = sum.substring(0, sum.indexOf("+") + 1);

                                    sum = pase + two;
                                }
                            } else {
                                if (two.startsWith("0") && tag.equals("0")) {
                                    return;
                                }
                                two = two + tag;

                                if (Utils.compareTo(two, maxNum)) {//大于999 999 999 .99
                                    two = two.substring(0, two.length() - 1);
                                    /*if (two.length() == 11) {
                                        two = two.substring(0, two.length() - 2);

                                    } else {
                                        two = two.substring(0, two.length() - 1);
                                    }*/
                                }
                                String pase = sum.substring(0, sum.indexOf("+") + 1);

                                sum = pase + two;
                            }

                        }
                    } else if (sum.contains("-")) {
                        if (tag.equals("-")) {//如果当前输入的是-号
                            if (sum.endsWith("-")) {
                                return;
                            } else {//相加
                                String arr[] = sum.split("\\-");
                                //                                long m = Long.parseLong(arr[0]) - Long.parseLong(arr[1]);
                                sum = Utils.subtract(arr[0], arr[1]) + "-";
                                two = "";
                            }
                        } else if (tag.equals("+")) {
                            if (sum.endsWith("-")) {
                                sum = sum.substring(0, sum.length() - 1) + "+";
                                return;
                            }
                            if (sum.startsWith("-")) {//负数的时候
                                sum = sum + "+";
                            } else {

                                String arr[] = sum.split("\\-");
                                //                            long m = Long.parseLong(arr[0]) - Long.parseLong(arr[1]);
                                sum = Utils.subtract(arr[0], arr[1]) + "+";
                            }
                            two = "";
                        } else {

                            if (StringUtil.isEmptyOrNull(two) && tag.equals(".")) {
                                return;
                            }
                            if (two.contains(".") && tag.equals(".")) {
                                return;
                            }
                            if (two.contains(".")) {
                                if (two.substring(two.indexOf(".") + 1, two.length()).length() >= 2) {
                                    return;
                                } else {
                                    if (two.equals("0.0") && tag.equals("0")) {
                                        return;
                                    }
                                    two = two + tag;
                                    if (Double.parseDouble(two) > 50000) {//大于5万
                                        two = two.substring(0, two.length() - 1);
                                    }
                                    String pase = sum.substring(0, sum.indexOf("-") + 1);

                                    sum = pase + two;

                                }
                            } else {
                                if (two.startsWith("0") && tag.equals("0")) {
                                    return;
                                }

                                two = two + tag;
                                if (Double.parseDouble(two) > 50000) {//大于5万
                                    two = two.substring(0, two.length() - 1);
                                }
                                String pase = sum.substring(0, sum.indexOf("-") + 1);

                                sum = pase + two;
                            }
                        }

                    } else {
                        if (tag.equals("+")) { //加号
                            sum = sum + tag;
                        } else if (tag.equals("-")) {
                            sum = sum + tag;
                        } else {//

                            if (sum.contains(".") && tag.equals(".")) {
                                return;
                            }
                            formFloat(tag);

                        }
                    }

                }

            }

        }

    }

    void formFloat(String tag) {

        if (currentMode == 1) {
            if (sumTips.contains(".")) {
                if (sumTips.substring(sumTips.indexOf(".") + 1, sumTips.length()).length() >= 2) {
                    return;
                } else {
                    if (sumTips.equals("0.0") && (tag.equals("0") || tag.equals("00"))) {
                        return;
                    }
                    sumTips = sumTips + tag;
                }
            } else {
                sumTips = sumTips + tag;
                if (Utils.compareTo(sumTips, maxNum)) {//大于10亿

                    if (sumTips.length() == 11) {
                        sumTips = sumTips.substring(0, sumTips.length() - 2);
                    } else {
                        sumTips = sumTips.substring(0, sumTips.length() - 1);
                    }
                }
            }
        } else {

            if (sum.contains(".")) {
                if (sum.substring(sum.indexOf(".") + 1, sum.length()).length() >= 2) {
                    return;
                } else {
                    if (sum.equals("0.0") && (tag.equals("0") || tag.equals("00"))) {
                        return;
                    }
                    sum = sum + tag;
                    if (Utils.compareTo(sum, maxNum)) {//大于999 999 999.99
                        sum = sum.substring(0, sum.length() - 1);
                    }
                }
            } else {
                sum = sum + tag;
                if (Utils.compareTo(sum, maxNum)) {//大于999 999 999.99
                    sum = sum.substring(0, sum.length() - 1);
                }
            }
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (time != null) {
            time.cancel();
        }
    }

    public void changeTextSize(TextView textView , String showString){
        int textLength = showString.length();
        if(textLength < 6){
            textView.setTextSize(40);
            btn_ic_money.setTextSize(40);

        }else if(textLength >=6  && textLength <10){
            textView.setTextSize(35);
            btn_ic_money.setTextSize(35);
        }else{
            textView.setTextSize(30);
            btn_ic_money.setTextSize(30);
        }
    }


    /**
     * 转成RMB
     *
     * @author admin
     */
    void parserToRmb(BigDecimal bigDecimal) {
        if (currentMode == 1) {
            return;
        }
        try {
            if (!MainApplication.getFeeType().equalsIgnoreCase("CNY")) {
                tv_pase.setVisibility(View.VISIBLE);
                if (bigDecimal != null) {
                    BigDecimal surchargeRate = new BigDecimal(MainApplication.getSurchargeRate()).setScale(6, BigDecimal.ROUND_HALF_UP);
                    BigDecimal additionalCharge = bigDecimal.multiply(surchargeRate).setScale(2, BigDecimal.ROUND_HALF_UP);
                    strSurchargelMoney = DateUtil.formatPaseMoney(additionalCharge);
                    id_title_surcharge.setText(MainApplication.feeFh + DateUtil.formatPaseMoney(additionalCharge));
                    BigDecimal totalCharge = null;
                    if (MainApplication.getSourceToUsdExchangeRate() > 0 && MainApplication.getUsdToRmbExchangeRate() > 0) {
                        BigDecimal sourceToUsd = new BigDecimal(MainApplication.getSourceToUsdExchangeRate());
                        BigDecimal usdRate = new BigDecimal(MainApplication.getUsdToRmbExchangeRate());
                        totalCharge = additionalCharge.add(bigDecimal).multiply(sourceToUsd).setScale(2, BigDecimal.ROUND_HALF_UP).multiply(usdRate).setScale(2, BigDecimal.ROUND_DOWN);
                    } else {
                        BigDecimal rate = new BigDecimal(MainApplication.getExchangeRate());
                        totalCharge = additionalCharge.add(bigDecimal).multiply(rate).setScale(2, BigDecimal.ROUND_FLOOR);
                    }
                    BigDecimal totalOriginalCharge = bigDecimal.add(additionalCharge).setScale(2, BigDecimal.ROUND_FLOOR);
                    tv_pase.setText(DateUtil.formatPaseMoney(totalOriginalCharge));
                }
            } else {
                tv_pase.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            Logger.e(TAG, "parserToRmb-->" + e);
        }
    }

    private void fullValue() {
        if (currentMode == 1) {
            try {
                Logger.i("hehui", "sum-->" + sumTips);
                boolean isInt = Utils.Integer.isInteger(sumTips);
                if (isInt) {
                    BigDecimal bigDecimal = new BigDecimal(sumTips);
                    parserToRmb(bigDecimal);
                    id_tips.setText(DateUtil.formatPaseMoneyUtil(bigDecimal));
                } else {
                    if (sumTips.contains("+")) {
                        if (sumTips.endsWith("+")) {
                            String paseSum = sumTips.substring(0, sumTips.length() - 1);
                            BigDecimal bigDecimal = new BigDecimal(paseSum);
                            if (paseSum.contains(".")) {
                                if (paseSum.endsWith(".00")) {
                                    id_tips.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + ".00");
                                    parserToRmb(bigDecimal);
                                } else {
                                    id_tips.setText(DateUtil.formatPaseMoneyUtil(bigDecimal));
                                    parserToRmb(bigDecimal);
                                }
                            } else {
                                id_tips.setText(DateUtil.formatPaseMoneyUtil(bigDecimal));
                                parserToRmb(bigDecimal);
                            }
                        } else {
                            String paseSum = sumTips.substring(sumTips.indexOf("+") + 1);
                            if (paseSum.contains(".")) {
                                if (paseSum.endsWith(".")) {
                                    String one = paseSum.substring(0, paseSum.lastIndexOf("."));
                                    BigDecimal bigDecimal = new BigDecimal(one);
                                    id_tips.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + ".");
                                    parserToRmb(bigDecimal);
                                } else {
                                    String one = paseSum.substring(0, paseSum.lastIndexOf("."));
                                    String two = paseSum.substring(paseSum.lastIndexOf(".") + 1, paseSum.length());
                                    BigDecimal bigDecimal = new BigDecimal(one);
                                    BigDecimal bigDecimalTwo = new BigDecimal(paseSum);
                                    id_tips.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + "." + two);
                                    parserToRmb(bigDecimalTwo);
                                }

                            } else {
                                BigDecimal bigDecimal = new BigDecimal(paseSum);
                                id_tips.setText(DateUtil.formatPaseMoneyUtil(bigDecimal));
                                parserToRmb(bigDecimal);
                            }
                        }
                    } else if (sumTips.contains("-") && !sumTips.startsWith("-")) {
                        if (sumTips.endsWith("-")) {
                            id_tips.setText(sumTips.substring(0, sumTips.length() - 1));
                        } else {
                            id_tips.setText(sumTips.substring(sumTips.indexOf("-") + 1));
                        }
                    } else {
                        if (StringUtil.isEmptyOrNull(sumTips)) {
                            id_tips.setText("0.00");
                        } else {
                            if (sumTips.contains(".")) {
                                if (sumTips.endsWith(".")) {
                                    String paseSum = sumTips.substring(0, sumTips.lastIndexOf("."));
                                    BigDecimal bigDecimal = new BigDecimal(paseSum);
                                    id_tips.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + ".");
                                    parserToRmb(bigDecimal);
                                } else {
                                    String one = sumTips.substring(0, sumTips.lastIndexOf("."));
                                    String two = sumTips.substring(sumTips.lastIndexOf(".") + 1, sumTips.length());
                                    BigDecimal bigDecimal = new BigDecimal(one);
                                    BigDecimal bigDecimalTwo = new BigDecimal(sumTips);
                                    id_tips.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + "." + two);
                                    parserToRmb(bigDecimalTwo);
                                }
                            } else {
                                BigDecimal bigDecimal = new BigDecimal(sumTips);
                                id_tips.setText(DateUtil.formatPaseMoneyUtil(bigDecimal));
                                parserToRmb(bigDecimal);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }
        } else {
            try {
                Logger.i("hehui", "sum-->" + sum);
                boolean isInt = Utils.Integer.isInteger(sum);
                if (isInt) {
                    BigDecimal bigDecimal = new BigDecimal(sum);
                    parserToRmb(bigDecimal);

                    changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal));
                    etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal));

                } else { //如果不是整数，则是可能是带符号的情况或者带小数的情况
                    if (sum.contains("+")) {
                        if (sum.endsWith("+")) {
                            String paseSum = sum.substring(0, sum.length() - 1);
                            BigDecimal bigDecimal = new BigDecimal(paseSum);
                            if (paseSum.contains(".")) {
                                if (paseSum.endsWith(".00")) {
                                    changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal) + ".00");
                                    etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + ".00");
                                    parserToRmb(bigDecimal);
                                } else {
                                    changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal));
                                    etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal));
                                    parserToRmb(bigDecimal);
                                }
                            } else {
                                changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal));
                                etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal));
                                parserToRmb(bigDecimal);
                            }
                        } else {
                            String paseSum = sum.substring(sum.indexOf("+") + 1);
                            if (paseSum.contains(".")) {
                                if (paseSum.endsWith(".")) {
                                    String one = paseSum.substring(0, paseSum.lastIndexOf("."));
                                    BigDecimal bigDecimal = new BigDecimal(one);
                                    changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal) + ".");
                                    etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + ".");
                                    parserToRmb(bigDecimal);
                                } else {
                                    String one = paseSum.substring(0, paseSum.lastIndexOf("."));
                                    String two = paseSum.substring(paseSum.lastIndexOf(".") + 1, paseSum.length());
                                    BigDecimal bigDecimal = new BigDecimal(one);
                                    BigDecimal bigDecimalTwo = new BigDecimal(paseSum);
                                    changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal) + "." + two);
                                    etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + "." + two);
                                    parserToRmb(bigDecimalTwo);
                                }

                            } else {
                                BigDecimal bigDecimal = new BigDecimal(paseSum);
                                changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal));
                                etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal));
                                parserToRmb(bigDecimal);
                            }
                        }
                    } else if (sum.contains("-") && !sum.startsWith("-")) {
                        if (sum.endsWith("-")) {
                            changeTextSize(etMoney,sum.substring(0, sum.length() - 1));
                            etMoney.setText(sum.substring(0, sum.length() - 1));
                        } else {
                            changeTextSize(etMoney,sum.substring(sum.indexOf("-") + 1));
                            etMoney.setText(sum.substring(sum.indexOf("-") + 1));

                        }
                    } else {
                        if (StringUtil.isEmptyOrNull(sum)) {
                            changeTextSize(etMoney,"0.00");
                            etMoney.setText("0.00");
                            initViewSurcharge();
                            tv_pase.setText("0.00");
                        } else {
                            if (sum.contains(".")) {
                                if (sum.endsWith(".")) {
                                    String paseSum = sum.substring(0, sum.lastIndexOf("."));
                                    BigDecimal bigDecimal = new BigDecimal(paseSum);
                                    changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal) + ".");
                                    etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + ".");
                                    parserToRmb(bigDecimal);
                                } else {
                                    String one = sum.substring(0, sum.lastIndexOf("."));
                                    String two = sum.substring(sum.lastIndexOf(".") + 1, sum.length());
                                    BigDecimal bigDecimal = new BigDecimal(one);
                                    BigDecimal bigDecimalTwo = new BigDecimal(sum);
                                    changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal) + "." + two);
                                    etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + "." + two);
                                    parserToRmb(bigDecimalTwo);
                                }
                            } else {
                                BigDecimal bigDecimal = new BigDecimal(sum);
                                changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal));
                                etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal));
                                parserToRmb(bigDecimal);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }

        }

    }


    private void etNumberClear() {
        if (currentMode == 1) {
            try {
                AppHelper.execVibrator(context);
                String ret = "";
                ret = id_tips.getText().toString();
                if (sumTips.length() > 0) {
                    if (ret.length() == deletePointTips) {
                        deletePointTips = 0;
                        sumTips = ret.substring(0, ret.length() - 2);
                        BigDecimal bigDecimal = new BigDecimal(sumTips);
                        parserToRmb(bigDecimal);
                    } else {
                        if (ret.length() > 1) {
                            id_tips.setText(ret.substring(0, ret.length() - 1));
                            sumTips = sumTips.substring(0, sumTips.length() - 1);
                            if (!StringUtil.isEmptyOrNull(twoTips)) {
                                twoTips = twoTips.substring(0, twoTips.length() - 1);
                            }
                            BigDecimal bigDecimal = new BigDecimal(sumTips);
                            parserToRmb(bigDecimal);
                        } else {
                            if (sumTips.contains("+")) {
                                sumTips = sumTips.substring(0, sumTips.indexOf("+"));
                                id_tips.setText(sumTips);
                                BigDecimal bigDecimal = new BigDecimal(sumTips);
                                parserToRmb(bigDecimal);
                            } else if (sumTips.contains("-")) {
                                sumTips = sumTips.substring(0, sumTips.indexOf("-"));
                                id_tips.setText(sumTips);
                                BigDecimal bigDecimal = new BigDecimal(sumTips);
                                parserToRmb(bigDecimal);
                            } else {
                                sumTips = sumTips.substring(0, sumTips.length() - 1);
                                if (sumTips.equals("")) {
                                    id_tips.setText("0.00");

                                    twoTips = "";
                                    sumTips = "";
                                } else {
                                    BigDecimal bigDecimal = new BigDecimal(sumTips);
                                    parserToRmb(bigDecimal);
                                    id_tips.setText(sumTips);
                                }
                            }
                        }
                    }
                } else {
                    id_tips.setText("0.00");
                    sum = "";
                    if (tv_pase != null) {
                        tv_pase.setText("0.00");
                    }
                }
            } catch (Exception e) {
                Logger.i(e);
            }
        } else {
            try {
                AppHelper.execVibrator(context);
                String ret = "";
                ret = etMoney.getText().toString();
                Pattern p = Pattern.compile("[^0-9.]");//去掉当前字符串中除去0-9以及小数点的其他字符
                Matcher m = p.matcher(ret);
                ret = m.replaceAll("");

                if (sum.length() > 0) {
                    if (ret.length() == deletePoint) {
                        deletePoint = 0;
                        sum = ret.substring(0, ret.length() - 2);
                        BigDecimal bigDecimal = new BigDecimal(sum);
                        parserToRmb(bigDecimal);
                    } else {
                        if(ret.equals("0.00") || ret.equals("0.0") || ret.equals(0)){
                            return;
                        }
                        if (ret.length() > 1) {
                            BigDecimal bigDecimal_text = new BigDecimal(ret.substring(0, ret.length() - 1));
                            changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal_text));
                            etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal_text));
                            if(sum.endsWith(".")){
                                sum = sum.substring(0, sum.length() - 2);
                            }else{
                                sum = sum.substring(0, sum.length() - 1);
                            }

                            if (!StringUtil.isEmptyOrNull(two)) {
                                two = two.substring(0, two.length() - 1);
                            }

//                            BigDecimal bigDecimal = new BigDecimal(sum);
                            parserToRmb(bigDecimal_text);
                        } else if(ret.length() == 1){
                            changeTextSize(etMoney,"0.00");
                            etMoney.setText("0.00");
                            sum = sum.substring(0, sum.length() - 1);
                            two = "";
                            initViewSurcharge();
                            BigDecimal bigDecimal = new BigDecimal("0.00");
                            parserToRmb(bigDecimal);
                            if (tv_pase != null) {
                                tv_pase.setText("0.00");
                            }
                        } else {
                            if (sum.contains("+")) {
                                sum = sum.substring(0, sum.indexOf("+"));
                                BigDecimal bigDecimal_text = new BigDecimal(sum);
                                changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal_text));
                                etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal_text));
//                                BigDecimal bigDecimal = new BigDecimal(sum);
                                parserToRmb(bigDecimal_text);
                            } else if (sum.contains("-")) {
                                sum = sum.substring(0, sum.indexOf("-"));
                                BigDecimal bigDecimal_text = new BigDecimal(sum);
                                changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal_text));
                                etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal_text));
//                                BigDecimal bigDecimal = new BigDecimal(sum);
                                parserToRmb(bigDecimal_text);
                            } else {
                                sum = sum.substring(0, sum.length() - 1);
                                if (sum.equals("")) {
                                    changeTextSize(etMoney,"0.00");
                                    etMoney.setText("0.00");
                                    initViewSurcharge();
                                    two = "";
                                    sum = "";
                                    if (tv_pase != null) {
                                        tv_pase.setText("0.00");
                                    }
                                } else {
                                    BigDecimal bigDecimal = new BigDecimal(sum);
                                    parserToRmb(bigDecimal);
                                    changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal));
                                    etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal));
                                }
                            }
                        }
                    }
                } else {
                    changeTextSize(etMoney,"0.00");
                    etMoney.setText("0.00");
                    initViewSurcharge();
                    sum = "";
                    if (tv_pase != null) {
                        tv_pase.setText("0.00");
                    }
                }
            } catch (Exception e) {
                Logger.i(e);
            }

        }

    }


    /**
     * 清空文本框
     */
    private void clearData() {

        if (currentMode == 1) {
            //小费模式
            id_tips.setText("0.00");
            twoTips = "";
            sumTips = "";
            initViewSurcharge();
        } else {
            two = "";
            sum = "";
            changeTextSize(etMoney,"0.00");
            etMoney.setText("0.00");
            initViewSurcharge();
            tvMoneyUppercase.setText("");
            tv_pase.setText("0.00");
        }
        setToPayView();
//        btnPay.setBackgroundResource(R.drawable.bottom_gray_reg);
//        btnWXPay.setOnClickListener(null);
        ivInputNumberDel.setVisibility(View.GONE);
        //isClickNumer = 1;

    }

    private void checkData(boolean isQpay) {

        if (!NetworkUtils.isNetworkAvailable(this)) {
            showToastInfo(getString(R.string.network_exception));
            return;
        }

        if (TextUtils.isEmpty(etMoney.getText())) {
            showToastInfo(getString(R.string.please_input_amount));
            return;
        }
        //提交我们后台进行生成订单
        Order order = new Order();
        order.orderNo = orderCode;
        strTotalMoney = OrderManager.getInstance().getMoney(etMoney.getText().toString());
        long intTotlaMoney = Utils.Long.tryParse(strTotalMoney, 0);
        if (intTotlaMoney < 1) {
            showToastInfo(R.string.please_input_amount);
            return;
        }
        // 单笔最高不能超过5万
        if (intTotlaMoney > MAX_SALSE_VALUE) {
            showToastInfo(getStringById(R.string.not_exceed_5w));
            return;
        }

        order.money = intTotlaMoney;//Integer.parseInt(strTotalMoney);
        order.add_time = System.currentTimeMillis();
        order.clientType = ApiConstant.pad;

        Logger.i(TAG, "ApiConstant.isLocaltionQRcode-->" + ApiConstant.isLocaltionQRcode);

    }

    private Dialog dialog;

    @Override
    protected void onResume() {
        super.onResume();
        if (isSubmitData) {
            clearData();
            isSubmitData = false;
        }
        if (!mLoadRateSuccess) {
            //获取附加税率等信息
            getExchangeRate();
        }
    }

    /**
     * 获取实时费率
     * getExchangeRate:
     *
     * @author admin
     */
    private void getExchangeRate() {
        LocalAccountManager.getInstance().getExchangeRate(new UINotifyListener<Boolean>() {
            @Override
            public void onSucceed(Boolean result) {
                super.onSucceed(result);
                if (result) {
                    mLoadRateSuccess = true;
                    if (MainApplication.getSurchargeRate() == -1) {
                        //附加手续费率 不存在
                    }
                    if (MainApplication.getTaxRate() == -1) {
                        //预扣税费率 不存在
                    }
                    if (MainApplication.getVatRate() == -1) {
                        // 增值税率  不存在
                    }
                    //判断是否有DCC
                    BigDecimal b2 = new BigDecimal(MainApplication.getExchangeRate());
                    BigDecimal usdtoRmb = new BigDecimal(MainApplication.getUsdToRmbExchangeRate());
                    BigDecimal usd = new BigDecimal(MainApplication.getSourceToUsdExchangeRate());

                    if (usdtoRmb.doubleValue() != 0 && usd.doubleValue() != 0) {
                        //含dcc功能
//                        MAX_SALSE_VALUE = (int) (50000.0 / usdtoRmb.doubleValue() / usd.doubleValue() / (1 + b2.doubleValue())) * 100;
//                        MAX_SALSE_VALUE = (int) ((50000.00d / usdtoRmb.doubleValue() / usd.doubleValue())*100);

                        BigDecimal tep = new BigDecimal(50000).divide(usdtoRmb,12,BigDecimal.ROUND_HALF_UP).
                                divide(usd,12,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));

                        MAX_SALSE_VALUE = tep.setScale(0,BigDecimal.ROUND_HALF_UP).longValue();

                    } else {
                        if (0.0 != b2.doubleValue()){
                            MAX_SALSE_VALUE = (int) ((50000.00d / b2.doubleValue()) * 100);
                            BigDecimal tep = new BigDecimal(50000).divide(b2,12,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
                            MAX_SALSE_VALUE = tep.setScale(0,BigDecimal.ROUND_HALF_UP).longValue();
                        }
                    }

                    if (!MainApplication.isSurchargeOpen()) {
                        id_rel_surcharge.setVisibility(View.GONE);
                    } else {
                        id_rel_surcharge.setVisibility(View.VISIBLE);
                    }

                    if (MAX_SALSE_VALUE <= 0) {
                        MAX_SALSE_VALUE = Integer.MAX_VALUE;
                    }

//                    if (!MainApplication.isTaxRateOpen()) {
//
//                    }
                    if (!MainApplication.isTipOpen()) {
                        id_rel_tip.setVisibility(View.GONE);
                        id_line_second.setVisibility(View.GONE);
                    } else {
                        id_rel_tip.setVisibility(View.VISIBLE);
                        id_line_second.setVisibility(View.VISIBLE);
                    }
                }

            }
        });
    }

    public String getPointDecime(String ids) {
        int dotPoint = ids.indexOf("."); //判断是否为小数
        String moneyStr;
        if (dotPoint != -1) {
            moneyStr = ids.substring(ids.indexOf(".") + 1, ids.length());
            return moneyStr;
        } else {
            return "";
        }
    }

    /**
     * 华智融
     */
    /*private void toNewpostech(final View view) {

        view.setEnabled(false);
        if (TextUtils.isEmpty(etMoney.getText())) {
            showToastInfo(getStringById(R.string.please_input_amount));
            view.setEnabled(true);
            return;
        }
        strTotalMoney = OrderManager.getInstance().getMoney(etMoney.getText().toString());
        long intTotlaMoney = Utils.Long.tryParse(strTotalMoney, 0);
        if (intTotlaMoney < 1) {
            showToastInfo(getStringById(R.string.please_input_amount));
            view.setEnabled(true);
            return;
        }
        try {
            if (!checkLoclExistAuth()) //体验用户无法进去
            {
                checkExitAuth();
            } else {
                //先申请订单号
                OrderManager.getInstance().getOrderNum(null, strTotalMoney, new UINotifyListener<String>() {
                    @Override
                    public void onPostExecute() {
                        super.onPostExecute();
                        view.setEnabled(true);
                    }

                    @Override
                    public void onPreExecute() {
                        super.onPreExecute();
                        showLoading(getStringById(R.string.checking_bluetooth));
                    }

                    @Override
                    public void onError(Object object) {
                        super.onError(object);
                    }

                    @Override
                    public void onSucceed(String result) {
                        super.onSucceed(result);
                        dismissLoading();
                        //BlueToothDialog.startActivity(context ,strTotalMoney,result);

                        isSubmitData = true;
                    }
                });
            }
        } catch (SQLException e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }

    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case REQUEST_2CODE: {
                if (data == null) {
                    return;
                }
                //String strCode = data.getStringExtra("code");

                break;
            }
            case REQUEST_EPAYLINKS: {
                break;
            }

        }
    }

    /**
     * 检查是否授权 和 资料是否完善
     * <功能详细描述>
     *
     * @param isCoupon 表示的是优惠券信息
     * @see [类、类#方法、类#成员]
     */
    /*private void checkExitAuth(final boolean isCoupon) {
        // 授权验证
        if (MainApplication.userId != 0) {
            UserManager.existAuth(MainApplication.userId, 1, new UINotifyListener<Integer>() {
                @Override
                public void onPreExecute() {
                    super.onPreExecute();
                    //                titleBar.setRightLodingVisible(true);
                    showLoading(false, getStringById(R.string.wait_a_moment));
                }

                @Override
                public void onError(Object object) {
                    super.onError(object);
                    if (object != null) {
                        showToastInfo(object.toString());
                    }
                }

                @Override
                public void onSucceed(Integer result) {
                    super.onSucceed(result);
                    dismissMyLoading();

                    switch (result) {

                        // 出错
                        case 11:
                            showToastInfo(getStringById(R.string.check_failure));
                            return;
                        //未激活
                        case 3:
                            DialogInfo dialogInfo = new DialogInfo(PayActivity.this, getResources().getString(R.string.title_prompt), getStringById(R.string.please_perfect_data), getStringById(R.string.btnOk), DialogInfo.FLAG, new DialogInfo.HandleBtn() {
                                @Override
                                public void handleOkBtn() {
                                    showPage(ShopkeeperActivity.class);
                                }

                                @Override
                                public void handleCancleBtn() {
                                    // TODO Auto-generated method stub

                                }
                            }, null);
                            DialogHelper.resize(PayActivity.this, dialogInfo);
                            dialogInfo.show();
                            return;
                        case RequestResult.RESULT_TIMEOUT_ERROR: // 请求超时
                            showToastInfo(getStringById(R.string.connection_timeout));
                            return;
                        default:
                            break;
                    }

                }
            });
        } else {
            // 用户id为空
            //            showToastInfo("连接已断开，请重新再登录!!");
            return;
        }
    }
*/
    ProgressInfoDialog dialog1;

    String message = "";

    /**
     * 请求威富通服务器提交订单
     */
   /* private void submitWFTOrder(final boolean isMoveNative) {
        String msg = "";
        if (isMoveNative) {
            msg = getStringById(R.string.spanning_pay_wechat_code);
        } else {
            msg = getStringById(R.string.spanning_pay_qq_code);
        }
        dialog1 = new ProgressInfoDialog(this, msg, null);
        //dialog =  createDialog(context,"正在请求微信二维码");
        OrderManager.getInstance().getServiceUUID(strTotalMoney, isMoveNative, new UINotifyListener<QRcodeInfo>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                dialog1.show();
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //                        boolean iscode = Utils.Integer.isInteger(object.toString());
                        String mmsg = null;
                        if ("20".equals(object.toString())) {
                            mmsg = getStringById(R.string.single_limit_exceeded);

                        } else {
                            if ("timeout".equals(object.toString())) {
                                mmsg = getStringById(R.string.spanning_code_timeout);
                            } else {
                                mmsg = object.toString();//iscode ? "对不起，服务器繁忙！请稍候" + "  错误码：" + object : "对不起，服务器繁忙！请稍候";
                            }
                        }
                        showConfirm(mmsg, new OnConfirmListener() {
                            @Override
                            public void onOK() {
                                dialog1.dismiss();
                            }

                            @Override
                            public void onCancel() {
                                dialog1.dismiss();
                            }
                        });
                    }
                });

            }

            @Override
            public void onSucceed(QRcodeInfo result) {
                super.onSucceed(result);
                dialog1.dismiss();
                if (result == null) {
                    return;
                }
                //                if (isMoveNative)
                //                {
                result.totalMoney = strTotalMoney;
                ShowQRcodeActivity.startActivity(result, context);
                //                }
                //                else
                //                {
                //                    //必须保证订单号是唯一的 如果没有进行本地生
                //                    WxPayActivity.startActivity(result, context);
                //                }
            }
        });
    }*/

    @Override
    public boolean onKeyDown(int keycode, KeyEvent event) {
        if (keycode == KeyEvent.KEYCODE_BACK) {
            showExitDialog(this);
            return true;
        }
        return super.onKeyDown(keycode, event);
    }


    boolean isAuth = false;


    MerchantTempDataModel merchantInfo = null;

    @Override
    public void handleOkBtn() {
        Uri uri = Uri.parse("tel:" + message);
        Intent intent = new Intent(Intent.ACTION_DIAL, uri);
        context.startActivity(intent);
    }

    @Override
    public void handleCancleBtn() {
        // TODO Auto-generated method stub

    }

    /** {@inheritDoc} */

}
