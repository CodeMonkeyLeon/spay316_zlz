package cn.swiftpass.enterprise.mvp.presenter.fragment

import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.OrderTotalInfo
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.fragment.TabSummaryContract
import cn.swiftpass.enterprise.utils.JsonUtil
import okhttp3.Call


/**
 * @author lizheng.zhao
 * @date 2022/11/16
 *
 * @你应该是一场梦我应该是一阵风
 */
class TabSummaryPresenter : TabSummaryContract.Presenter {

    private var mView: TabSummaryContract.View? = null


    override fun orderCount(
        startTime: String?,
        endTime: String?,
        countMethod: Int,
        userId: String?,
        countDayKind: String?,
        isShowLoading: Boolean
    ) {

        mView?.let {
            if (isShowLoading) {
                it.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
            }
        }

        var uId = ""
        //如果是收银员登录的同时收银员也没有账单权限
        if (MainApplication.getInstance().isAdmin(0) && MainApplication.getInstance()
                .isOrderAuth("0")
        ) {
            uId = MainApplication.getInstance().getUserId().toString()
        }
        userId?.let {
            uId = it
        }

        AppClient.orderCount(
            MainApplication.getInstance().getMchId(),
            "1",
            startTime,
            endTime,
            countMethod.toString(),
            uId,
            countDayKind,
            System.currentTimeMillis().toString(),
            object : CallBackUtil.CallBackCommonResponse<OrderTotalInfo>(OrderTotalInfo()) {
                override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                    mView?.let { view ->
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { error ->
                            view.orderCountFailed(
                                error.message,
                                startTime,
                                endTime,
                                countMethod,
                                userId,
                                countDayKind,
                                isShowLoading
                            )
                        }
                    }
                }

                override fun onResponse(res: CommonResponse?) {
                    mView?.let { view ->
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        res?.let { response ->
                            //如果dataList值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"dataList\":\"\"", "\"dataList\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message, OrderTotalInfo::class.java
                            ) as OrderTotalInfo
                            order.orderTotalItemInfo = order.dataList
                            view.orderCountSuccess(order, countMethod)
                        }
                    }
                }
            }
        )

    }

    override fun attachView(view: TabSummaryContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}