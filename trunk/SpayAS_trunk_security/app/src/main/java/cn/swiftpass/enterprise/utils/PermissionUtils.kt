package cn.swiftpass.enterprise.utils

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.Settings
import android.view.KeyEvent
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.ui.widget.DialogInfo
import cn.swiftpass.enterprise.utils.interfaces.OnPermissionGrantedListener

/**
 * @author lizheng,zhao
 * @date 2022/08/24
 * @description 权限申请
 */
object PermissionUtils {

    /**
     * EXTERNAL_BLUETOOTH_REQUEST_PERMISSION 蓝牙权限
     */
    const val EXTERNAL_BLUETOOTH_REQUEST_PERMISSION = 1011


    /**
     * 申请蓝牙权限
     * android 12 需要手动申请蓝牙权限
     */
    fun requestBlueToothPermission(con: Activity?, listener: OnPermissionGrantedListener?) {
        con?.let { context ->
            if (ContextCompat.checkSelfPermission(
                    context,
                    Manifest.permission.BLUETOOTH_SCAN
                ) == PackageManager.PERMISSION_DENIED
                || ContextCompat.checkSelfPermission(
                    context,
                    Manifest.permission.BLUETOOTH_CONNECT
                ) == PackageManager.PERMISSION_DENIED
            ) {
                //蓝牙权限被拒绝
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        context,
                        Manifest.permission.BLUETOOTH_SCAN
                    )
                    || ActivityCompat.shouldShowRequestPermissionRationale(
                        context,
                        Manifest.permission.BLUETOOTH_CONNECT
                    )
                ) {
                    //被拒绝且选择不再询问，需要手动申请
                    showMissingPermissionDialog(
                        context,
                        context.getString(R.string.setting_permission_bluetooth_12)
                    )
                } else {
                    //申请权限
                    ActivityCompat.requestPermissions(
                        context, arrayOf(
                            Manifest.permission.BLUETOOTH_SCAN,
                            Manifest.permission.BLUETOOTH_CONNECT
                        ),
                        EXTERNAL_BLUETOOTH_REQUEST_PERMISSION
                    )
                }
            } else {
                //蓝牙权限通过
                listener?.let {
                    it.onPermissionGranted()
                }
            }
        }
    }


    private fun showMissingPermissionDialog(context: Activity, msg: String) {
        val dialogInfo = DialogInfo(
            context,
            null,
            msg,
            context.getString(R.string.title_setting),
            context.getString(R.string.btnCancel),
            DialogInfo.UNIFED_DIALOG,
            object : DialogInfo.HandleBtn {
                override fun handleOkBtn() {
                    startAppSettings(context)
                }

                override fun handleCancelBtn() {

                }

            },
            null
        )

        dialogInfo.setOnKeyListener { dialog, keyCode, event ->
            keyCode == KeyEvent.KEYCODE_BACK
        }

        DialogHelper.resize(context, dialogInfo)
        dialogInfo.show()
    }


    /**
     * 开启设置界面
     */
    private fun startAppSettings(context: Activity) {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        intent.data = Uri.parse("package:" + context.packageName)
        context.startActivity(intent)
    }


}