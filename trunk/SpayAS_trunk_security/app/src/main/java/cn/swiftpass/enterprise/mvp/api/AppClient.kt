package cn.swiftpass.enterprise.mvp.api

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.intl.BuildConfig
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.RequestUtil.NNS_1
import cn.swiftpass.enterprise.io.okhttp.RequestUtil.NNS_2
import cn.swiftpass.enterprise.io.okhttp.RequestUtil.NNS_3
import cn.swiftpass.enterprise.io.okhttp.RequestUtil.NNS_4
import cn.swiftpass.enterprise.io.okhttp.RequestUtil.NNS_NONE
import cn.swiftpass.enterprise.mvp.annotation.ApiHelper
import cn.swiftpass.enterprise.mvp.constants.ApiConstants
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.utils.SharedPreUtils
import com.example.common.sentry.SentryUtils
import com.example.common.sentry.SentryUtils.getClassNameAndMethodName
import com.example.common.sp.PreferenceUtil

/**
 * @author lizheng.zhao
 * @date 2022/12/14
 *
 * @一眨眼
 * @算不算少年
 * @一辈子
 * @算不算永远
 */
object AppClient {

    private var mSpayApi: SpayApi = ApiHelper.getApi(SpayApi::class.java)


    fun getTipsSetting(
        mch_id: String?,
        user_id: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.getTipsSetting(
            mch_id,
            user_id,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun submitTipsSetting(
        spayRs: String?,
        json_str: String?,
        callback: CallBackUtil<*>
    ) {
        val json = ApiConstants.JSON_STRING_FLAG + json_str
        mSpayApi.submitTipsSetting(
            spayRs,
            json
        ).addCallBack(callback, NNS_NONE).execute()
    }


    fun sendMessage(
        action_type: String?,
        order_no: String?,
        cust_mobile: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.sendMessage(
            action_type,
            order_no,
            cust_mobile,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun sendEmail(
        action_type: String?,
        order_no: String?,
        cust_email: String?,
        email_subject: String?,
        email_content: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.sendEmail(
            action_type,
            order_no,
            cust_email,
            email_subject,
            email_content,
            spayRs
        ).addCallBack(callback, NNS_2).execute()

    }


    fun getOrderTotal(
        mch_id: String?,
        start_time: String?,
        end_time: String?,
        mobile: String,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.getOrderTotal(
            mch_id,
            start_time,
            end_time,
            mobile,
            spayRs
        ).addCallBack(callback, NNS_NONE).execute()
    }


    fun queryDailySettlementList(
        start_date: String?,
        end_date: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.queryDailySettlementList(
            start_date,
            end_date,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun getBase(
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.getBase(
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun logout(
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.logout(
            spayRs
        ).addCallBack(callback, NNS_1).execute()
    }


    fun regisRefund(
        out_trade_no: String?,
        client: String?,
        refund_money: String?,
        total_fee: String?,
        mch_id: String?,
        user_id: String?,
        user_name: String?,
        body: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        val uid = if (MainApplication.getInstance().isAdmin(0)) user_id else ""
        mSpayApi.regisRefund(
            out_trade_no,
            client,
            refund_money,
            total_fee,
            mch_id,
            uid,
            user_name,
            body,
            sign,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun authOperateQuery(
        mch_id: String?,
        user_id: String?,
        auth_no: String?,
        operation_type: String?,
        page: String?,
        page_size: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.authOperateQuery(
            mch_id,
            user_id,
            auth_no,
            operation_type,
            page,
            page_size,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun synchronizeOrder(
        service: String?,
        user_id: String?,
        auth_no: String?,
        out_request_no: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.synchronizeOrder(
            service,
            user_id,
            auth_no,
            out_request_no,
            sign,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun unifiedAuthPay(
        mch_id: String?,
        user_id: String?,
        auth_no: String?,
        body: String?,
        money: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.unifiedAuthPay(
            mch_id,
            user_id,
            auth_no,
            body,
            money,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun checkCanRefundOrNot(
        out_trade_no: String?,
        client: String?,
        refund_money: String?,
        total_fee: String?,
        mch_id: String?,
        user_id: String?,
        user_name: String?,
        body: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        val uid = if (MainApplication.getInstance().isAdmin(0)) user_id else ""
        mSpayApi.checkCanRefundOrNot(
            out_trade_no,
            client,
            refund_money,
            total_fee,
            mch_id,
            uid,
            user_name,
            body,
            sign,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun regisRefunds(
        spayRs: String?,
        json_str: String?,
        callback: CallBackUtil<*>
    ) {
        val json = ApiConstants.JSON_STRING_FLAG + json_str
        mSpayApi.regisRefunds(
            spayRs,
            json
        ).addCallBack(callback, NNS_NONE).execute()
    }


    fun cardPaymentRefund(
        body: String?,
        mch_id: String?,
        user_name: String?,
        refund_money: String?,
        total_fee: String?,
        client: String?,
        out_trade_no: String?,
        order_no_mch: String?,
        token: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.cardPaymentRefund(
            body,
            mch_id,
            user_name,
            refund_money,
            total_fee,
            client,
            out_trade_no,
            order_no_mch,
            token,
            sign,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun authUnfreeze(
        mch_id: String?,
        user_id: String?,
        auth_no: String?,
        token: String?,
        money: String?,
        remark: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.authUnfreeze(
            mch_id,
            user_id,
            auth_no,
            token,
            money,
            remark,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun authPayQuery(
        mch_id: String?,
        user_id: String?,
        out_request_no: String?,
        order_no: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.authPayQuery(
            mch_id,
            user_id,
            out_request_no,
            order_no,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun getSpeakerList(
        mch_id: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.getSpeakerList(
            mch_id,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun unbindSpeaker(
        user_id: String?,
        device_name: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.unbindSpeaker(
            user_id,
            device_name,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun bindSpeaker(
        device_name: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.bindSpeaker(
            device_name,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun queryMasterCardOrderByOrderNo(
        out_trade_no: String?,
        service: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.queryMasterCardOrderByOrderNo(
            out_trade_no,
            service,
            sign,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }

    fun syncMasterCardOrderStatusByOrderNo(
        out_trade_no: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.syncMasterCardOrderStatusByOrderNo(
            out_trade_no,
            sign,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }

    fun updateOrAddBody(
        mch_id: String?,
        commodity_name: String?,
        commodity_num: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.updateOrAddBody(
            mch_id,
            commodity_name,
            commodity_num,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }

    fun queryBody(
        mch_id: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.queryBody(
            mch_id,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun checkData(
        token: String?,
        email_code: String?,
        s_key: String?,
        password: String?,
        rep_password: String?,
        new_pwd_flag: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.checkData(
            token,
            email_code,
            s_key,
            password,
            rep_password,
            new_pwd_flag,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun checkPhoneCode(
        token: String?,
        email_code: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.checkPhoneCode(
            token,
            email_code,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun checkForgetPwdInputAndGetCode(
        email: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.checkForgetPwdInputAndGetCode(
            email,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun sendFeedBack(
        u_id: String?,
        content: String?,
        client_type: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.sendFeedBack(
            u_id,
            content,
            client_type,
            spayRs
        ).addCallBack(callback, NNS_NONE).execute()
    }


    fun editProductDetail(
        action_type: String?,
        goods_id: String?,
        goods_name: String?,
        goods_code: String?,
        goods_desc: String?,
        goods_price: String?,
        goods_pic: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.editProductDetail(
            action_type,
            goods_id,
            goods_name,
            goods_code,
            goods_desc,
            goods_price,
            goods_pic,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun addNewProduct(
        action_type: String?,
        goods_name: String?,
        goods_code: String?,
        goods_desc: String,
        goods_price: String?,
        goods_pic: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.addNewProduct(
            action_type,
            goods_name,
            goods_code,
            goods_desc,
            goods_price,
            goods_pic,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun addNewCustomer(
        action_type: String?,
        cust_name: String?,
        cust_mobile: String?,
        cust_email: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.addNewCustomer(
            action_type,
            cust_name,
            cust_mobile,
            cust_email,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun editCustomerDetail(
        action_type: String?,
        cust_id: String?,
        cust_name: String?,
        cust_mobile: String?,
        cust_email: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.editCustomerDetail(
            action_type,
            cust_id,
            cust_name,
            cust_mobile,
            cust_email,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }

    fun cashierDelete(
        id: String?,
        mch_id: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.cashierDelete(
            id,
            mch_id,
            sign,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun cashierAdd(
        real_name: String?,
        user_name: String?,
        refund_limit: String?,
        is_refund_auth: String?,
        is_order_auth: String?,
        is_unfreeze_auth: String?,
        enabled: String?,
        is_total_auth: String?,
        mch_id: String?,
        id: String?,
        is_encrypt_pwd: String?,
        spayRs: String?,
        isAddCashier: Boolean,
        callback: CallBackUtil<*>
    ) {
        val ita = if (!isAddCashier) is_total_auth else ""
        var idd = ""
        id?.let {
            if (it.toLong() != 0L) {
                idd = it
            }
        }
        val iep = if (isAddCashier) is_encrypt_pwd else ""



        if (isAddCashier) {
            mSpayApi.cashierAddSave(
                real_name,
                user_name,
                refund_limit,
                is_refund_auth,
                is_order_auth,
                is_unfreeze_auth,
                enabled,
                ita,
                mch_id,
                idd,
                iep,
                spayRs
            ).addCallBack(callback, NNS_4).execute()
        } else {
            mSpayApi.cashierAddUpdate(
                real_name,
                user_name,
                refund_limit,
                is_refund_auth,
                is_order_auth,
                is_unfreeze_auth,
                enabled,
                ita,
                mch_id,
                idd,
                iep,
                spayRs
            ).addCallBack(callback, NNS_4).execute()
        }
    }


    fun cashierResetPsw(
        user_id: String?,
        mch_id: String?,
        is_encrypt_pwd: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.cashierResetPsw(
            user_id,
            mch_id,
            is_encrypt_pwd,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }

    fun queryCardDetail(
        mch_id: String?,
        u_id: String?,
        card_id: String?,
        card_code: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        val requestUtil = mSpayApi.queryCardDetail(
            mch_id,
            u_id,
            card_id,
            card_code,
            spayRs
        ).addCallBack(callback, NNS_NONE)
        requestUtil.execute()
    }


    fun sendEmailForAccountCancel(
        email: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        val requestUtil = mSpayApi.sendEmailForAccountCancel(
            email,
            spayRs
        ).addCallBack(callback, NNS_4)
        requestUtil.execute()
    }


    fun checkVerifyCodeAndPassword(
        email_verify_code: String?,
        password: String?,
        s_key: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.checkVerifyCodeAndPassword(
            email_verify_code,
            password,
            s_key,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun queryTransactionReportDetails(
        report_type: String?,
        start_date: String?,
        end_date: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.queryTransactionReportDetails(
            report_type,
            start_date,
            end_date,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun bindCodeDetails(
        mch_id: String?,
        user_id: String?,
        qr_code_id: String?,
        cashier_desk: String?,
        client: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.bindCodeDetails(
            mch_id,
            user_id,
            qr_code_id,
            cashier_desk,
            client,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun bindCode(
        mch_id: String?,
        user_id: String?,
        qr_code_id: String?,
        cashier_desk: String?,
        client: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.bindCode(
            mch_id,
            user_id,
            qr_code_id,
            cashier_desk,
            client,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun refundLogin(
        spayRs: String?,
        json_str: String?,
        callback: CallBackUtil<*>
    ) {
        val json = ApiConstants.JSON_STRING_FLAG + json_str
        mSpayApi.refundLogin(
            spayRs,
            json
        ).addCallBack(callback, NNS_1).execute()
    }


    fun apiShowList(
        mch_id: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.apiShowList(
            mch_id,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun queryUserRefundOrder(
        out_refund_no: String?,
        trade_state: String?,
        add_time: String?,
        mch_id: String?,
        page: String?,
        page_size: String?,
        user_id: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        val ts = if (!TextUtils.equals(trade_state, "-1")) trade_state else ""
        val uid =
            if (MainApplication.getInstance().isAdmin(0) && MainApplication.getInstance()
                    .isOrderAuth("0")
            ) user_id else ""

        mSpayApi.queryUserRefundOrder(
            out_refund_no,
            ts,
            add_time,
            mch_id,
            page,
            page_size,
            uid,
            sign,
            spayRs
        ).addCallBack(callback, NNS_NONE).execute()
    }


    fun queryRefundOrder(
        mch_id: String?,
        page: String?,
        trade_state: String?,
        is_refund_stream: String?,
        order_no_mch: String?,
        user_id: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        val uid =
            if (MainApplication.getInstance().isAdmin(0) && MainApplication.getInstance()
                    .isOrderAuth("0")
            ) user_id else ""
        var ts = ""
        try {
            trade_state?.let {
                if (it.toInt() != 0) {
                    ts = it
                }
            }
        } catch (e: Exception) {
            SentryUtils.uploadTryCatchException(e, getClassNameAndMethodName())
        }

        mSpayApi.queryRefundOrder(
            mch_id,
            page,
            ts,
            is_refund_stream,
            order_no_mch,
            uid,
            sign,
            spayRs
        ).addCallBack(callback, NNS_NONE).execute()
    }


    fun queryOrderData(
        order_no_mch: String?,
        mch_id: String?,
        user_id: String?,
        add_time: String?,
        page: String?,
        trade_state: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {

        val uid =
            if (MainApplication.getInstance().isAdmin(0) && MainApplication.getInstance()
                    .isOrderAuth("0")
            ) user_id else ""
        var ts = ""
        try {
            trade_state?.let {
                if (it.toInt() != 0) {
                    ts = it
                }
            }
        } catch (e: Exception) {
            SentryUtils.uploadTryCatchException(e, getClassNameAndMethodName())
        }



        mSpayApi.queryOrderData(
            order_no_mch,
            mch_id,
            uid,
            add_time,
            page,
            ts,
            sign,
            spayRs
        ).addCallBack(callback, NNS_NONE).execute()
    }


    fun queryCashier(
        mch_id: String?,
        page: String?,
        search_content: String?,
        page_size: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.queryCashier(
            mch_id,
            page,
            search_content,
            page_size,
            sign,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun payReverse(
        out_trade_no: String?,
        money: String?,
        service: String?,
        user_id: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.payReverse(
            out_trade_no,
            money,
            service,
            user_id,
            sign,
            spayRs
        ).addCallBack(callback, NNS_NONE).execute()
    }


    fun queryOrderByInvoiceId(
        user_id: String?,
        invoice_id: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.queryOrderByInvoiceId(
            user_id,
            invoice_id,
            sign,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun getUpdateOrderDetail(
        action_type: String?,
        order_no: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.getUpdateOrderDetail(
            action_type,
            order_no,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun getOrderConfig(
        action_type: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.getOrderConfig(
            action_type,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun changePwd(
        s_key: String?,
        telephone: String?,
        old_password: String?,
        password: String?,
        rep_password: String?,
        new_pwd_flag: String?,
        mch_id: String?,
        user_id: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.changePwd(
            s_key,
            telephone,
            old_password,
            password,
            rep_password,
            new_pwd_flag,
            mch_id,
            user_id,
            sign,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun queryTransactionReport(
        report_type: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.queryTransactionReport(
            report_type,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun orderCount(
        mch_id: String?,
        free_query_time: String?,
        start_time: String?,
        end_time: String?,
        count_method: String?,
        user_id: String?,
        count_day_kind: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.orderCount(
            mch_id,
            free_query_time,
            start_time,
            end_time,
            count_method,
            user_id,
            count_day_kind,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun queryMerchantDataByTel(
        mch_id: String?,
        phone: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.queryMerchantDataByTel(
            mch_id,
            phone,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun getCodeList(
        mch_id: String?,
        user_id: String?,
        bind_user_id: String?,
        page: String?,
        page_size: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.getCodeList(
            mch_id,
            user_id,
            bind_user_id,
            page,
            page_size,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun getProductList(
        action_type: String?,
        goods_id: String?,
        goods_name: String?,
        goods_code: String?,
        page_number: String?,
        page_size: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.getProductList(
            action_type,
            goods_id,
            goods_name,
            goods_code,
            page_number,
            page_size,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun getOrderList(
        action_type: String?,
        order_no: String?,
        real_plat_order_no: String?,
        cust_name: String?,
        order_remark: String?,
        page_number: String?,
        page_size: String?,
        order_status: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.getOrderList(
            action_type,
            order_no,
            real_plat_order_no,
            cust_name,
            order_remark,
            page_number,
            page_size,
            order_status,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun getCustomerList(
        action_type: String?,
        cust_name: String?,
        cust_mobile: String?,
        cust_email: String?,
        page_number: String?,
        page_size: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.getCustomerList(
            action_type,
            cust_name,
            cust_mobile,
            cust_email,
            page_number,
            page_size,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun deleteOrQueryProduct(
        action_type: String?,
        goods_id: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.deleteOrQueryProduct(
            action_type,
            goods_id,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun deleteOrQueryCustomer(
        action_type: String?,
        cust_id: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.deleteOrQueryCustomer(
            action_type,
            cust_id,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun querySpayOrder(
        spayRs: String?,
        json_str: String?,
        isRefund: Int,
        callback: CallBackUtil<*>
    ) {
        val json = ApiConstants.JSON_STRING_FLAG + json_str
        when (isRefund) {
            0 -> {
                //流水
                //如果有预授权权限，同时账单页面选择的是预授权
                if (MainApplication.getInstance().userInfo.isAuthFreezeOpen == 1 &&
                    TextUtils.equals(
                        PreferenceUtil.getString(
                            ParamsConstants.BILL_LIST_CHOOSE_PAY_OR_PRE_AUTH,
                            "sale"
                        ), ParamsConstants.PRE_AUTH
                    )
                ) {
                    mSpayApi.querySpayOrderAuthOrderList(
                        spayRs,
                        json
                    ).addCallBack(callback, NNS_NONE).execute()
                } else {
                    mSpayApi.querySpayOrderOrderList(
                        spayRs,
                        json
                    ).addCallBack(callback, NNS_NONE).execute()
                }
            }

            1 -> {
                mSpayApi.querySpayOrderRefundList(
                    spayRs,
                    json
                ).addCallBack(callback, NNS_NONE).execute()
            }
        }
    }


    fun querySpayOrderNew(
        spayRs: String?,
        json_str: String?,
        isRefund: Int,
        isUnfrozen: Int,
        callback: CallBackUtil<*>
    ) {
        val json = ApiConstants.JSON_STRING_FLAG + json_str


        if (isRefund == 1 && isUnfrozen == 0) {
            //退款列表
            mSpayApi.querySpayOrderNewRefundList(
                spayRs,
                json
            ).addCallBack(callback, NNS_NONE).execute()
        } else if (isRefund == 0 && isUnfrozen == 0) {
            //流水列表
            //如果有预授权权限，同时账单页面选择的是预授权
            if (MainApplication.getInstance().userInfo.isAuthFreezeOpen == 1 && TextUtils.equals(
                    PreferenceUtil.getString(
                        ParamsConstants.BILL_LIST_CHOOSE_PAY_OR_PRE_AUTH, "sale"
                    ), "pre_auth"
                )
            ) {
                mSpayApi.querySpayOrderNewAuthOrderList(
                    spayRs,
                    json
                ).addCallBack(callback, NNS_NONE).execute()
            } else {
                mSpayApi.querySpayOrderNewOrderList(
                    spayRs,
                    json
                ).addCallBack(callback, NNS_NONE).execute()
            }
        } else if (isUnfrozen == 1) {
            //预授权解冻列表
            MainApplication.getInstance().baseUrl + ApiConstants.URL_QUERY_SPAY_ORDER_NEW_AUTH_OPERATE_QUERY
            mSpayApi.querySpayOrderNewAuthOperateQuery(
                spayRs,
                json
            ).addCallBack(callback, NNS_NONE).execute()
        }
    }


    fun querySpayOrderWxCard(
        spayRs: String?,
        json_str: String?,
        isRefund: Int,
        callback: CallBackUtil<*>
    ) {
        val json = ApiConstants.JSON_STRING_FLAG + json_str
        when (isRefund) {
            1 -> {
                mSpayApi.querySpayOrderWxCardRefundList(
                    spayRs,
                    json
                ).addCallBack(callback, NNS_NONE).execute()
            }

            0 -> {
                //流水
                mSpayApi.querySpayOrderWxCardOrderList(
                    spayRs,
                    json
                ).addCallBack(callback, NNS_NONE).execute()
            }

            else -> {
                //卡券
                mSpayApi.querySpayOrderWxCard(
                    spayRs,
                    json
                ).addCallBack(callback, NNS_NONE).execute()
            }
        }
    }


    fun queryRefundDetail(
        out_refund_no: String?,
        mch_id: String?,
        user_id: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        val uid =
            if (MainApplication.getInstance().isAdmin(0) && MainApplication.getInstance()
                    .isOrderAuth("0")
            ) user_id else ""
        mSpayApi.queryRefundDetail(
            out_refund_no,
            mch_id,
            uid,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun queryPreAuthDailyStatics(
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.queryPreAuthDailyStatics(spayRs)
            .addCallBack(callback, NNS_2).execute()
    }


    fun queryDailyStatics(
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.queryDailyStatics(spayRs)
            .addCallBack(callback, NNS_2).execute()
    }


    fun loadCardPaymentDate(
        spayRs: String?,
        json_str: String?,
        callback: CallBackUtil<*>
    ) {
        val json = ApiConstants.JSON_STRING_FLAG + json_str
        mSpayApi.loadCardPaymentDate(
            spayRs,
            json
        ).addCallBack(callback, NNS_NONE).execute()
    }


    fun uploadImg(
        img_type: String?,
        img_base_64: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.uploadImg(
            img_type,
            img_base_64,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun paymentLinkOrder(
        spayRs: String?,
        json_str: String?,
        callback: CallBackUtil<*>
    ) {
        val json = ApiConstants.JSON_STRING_FLAG + json_str
        mSpayApi.paymentLinkOrder(
            spayRs,
            json
        ).addCallBack(callback, NNS_NONE).execute()
    }


    fun unifiedPayReverse(
        user_id: String?,
        mch_id: String?,
        out_auth_trade_no: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {

        if (MainApplication.getInstance().userInfo.isAuthFreezeOpen == 1
            && ParamsConstants.PRE_AUTH == PreferenceUtil.getString(
                ParamsConstants.CHOOSE_PAY_OR_PRE_AUTH,
                "sale"
            )
        ) {
            mSpayApi.unifiedPayReverseAuth(
                user_id,
                mch_id,
                out_auth_trade_no,
                sign,
                spayRs
            ).addCallBack(callback, NNS_4).execute()
        } else {
            mSpayApi.unifiedPayReversePay(
                user_id,
                mch_id,
                out_auth_trade_no,
                sign,
                spayRs
            ).addCallBack(callback, NNS_4).execute()
        }
    }


    fun unifiedNativePay(
        money: String?,
        attach: String?,
        device_info: String?,
        out_trade_no: String?,
        da_money: String?,
        user_name: String?,
        u_id: String?,
        body: String?,
        mch_id: String?,
        client: String?,
        api_code: String?,
        tra_type: String?,
        service: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {

        val flag =
            MainApplication.getInstance().userInfo.isAuthFreezeOpen == 1
                    && ParamsConstants.PRE_AUTH == PreferenceUtil.getString(
                ParamsConstants.CHOOSE_PAY_OR_PRE_AUTH, "sale"
            )

        val un = if (MainApplication.getInstance().isAdmin(0) && !flag) user_name else ""
        val uid = if (MainApplication.getInstance().isAdmin(0)) u_id else ""


        var se = ""
        try {
            //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
            val payTypeMap =
                if (MainApplication.getInstance().userInfo.isAuthFreezeOpen == 1 && TextUtils.equals(
                        PreferenceUtil.getString(
                            ParamsConstants.CHOOSE_PAY_OR_PRE_AUTH,
                            "sale"
                        ),
                        ParamsConstants.PRE_AUTH
                    )
                ) {
                    SharedPreUtils.readProduct(ParamsConstants.PAY_TYPE_MAP_PRE_AUTH + BuildConfig.bankCode + mch_id)
                } else {
                    SharedPreUtils.readProduct(ParamsConstants.PAY_TYPE_MAP + BuildConfig.bankCode + mch_id)
                }
            val map = payTypeMap as Map<String, String>
            if (map.isNotEmpty()) {
                map[service]?.let {
                    se = it
                }
            }
        } catch (e: Exception) {
            SentryUtils.uploadTryCatchException(
                e, SentryUtils.getClassNameAndMethodName()
            )
            service?.let {
                se = it
            }
        }


        if (flag) {
            mSpayApi.unifiedNativePayAuth(
                money,
                attach,
                device_info,
                out_trade_no,
                da_money,
                un,
                uid,
                body,
                mch_id,
                client,
                api_code,
                tra_type,
                se,
                sign,
                spayRs
            ).addCallBack(callback, NNS_4).execute()
        } else {
            mSpayApi.unifiedNativePayPay(
                money,
                attach,
                device_info,
                out_trade_no,
                da_money,
                un,
                uid,
                body,
                mch_id,
                client,
                api_code,
                tra_type,
                se,
                sign,
                spayRs
            ).addCallBack(callback, NNS_4).execute()
        }
    }


    fun scanQueryOrderDetail(
        order_no_mch: String?,
        auth_no: String?,
        mch_id: String?,
        user_id: String?,
        spayRs: String?,
        scanType: Int,
        callback: CallBackUtil<*>
    ) {
        val onm = if (scanType == 1) order_no_mch else ""
        val atn = if (scanType == 2) auth_no else ""
        val uid =
            if (MainApplication.getInstance().isAdmin(0) && MainApplication.getInstance()
                    .isOrderAuth("0")
            ) user_id else ""


        if (scanType == 2) {
            mSpayApi.scanQueryOrderDetailAuth(
                onm,
                atn,
                mch_id,
                uid,
                spayRs
            ).addCallBack(callback, NNS_4).execute()
        } else {
            mSpayApi.scanQueryOrderDetailQuery(
                onm,
                atn,
                mch_id,
                uid,
                spayRs
            ).addCallBack(callback, NNS_4).execute()
        }
    }


    fun queryOrderDetail(
        auth_no: String?,
        out_trade_no: String?,
        order_no_mch: String?,
        mch_id: String?,
        user_id: String?,
        spayRs: String?,
        isMark: Boolean,
        callback: CallBackUtil<*>
    ) {
        val flag =
            MainApplication.getInstance().userInfo.isAuthFreezeOpen == 1
                    && ParamsConstants.PRE_AUTH == PreferenceUtil.getString(
                ParamsConstants.BILL_LIST_CHOOSE_PAY_OR_PRE_AUTH, "sale"
            )

        val atn = if (flag && isMark) auth_no else ""
        val otn = if (!flag && isMark) out_trade_no else ""
        val onm = if (!isMark) order_no_mch else ""
        val uid =
            if (MainApplication.getInstance().isAdmin(0) && MainApplication.getInstance()
                    .isOrderAuth("0")
            ) user_id else ""

        if (flag && isMark
        ) {
            mSpayApi.queryOrderDetailAuth(
                atn,
                otn,
                onm,
                mch_id,
                uid,
                spayRs
            ).addCallBack(callback, NNS_4).execute()
        } else {
            mSpayApi.queryOrderDetailQuery(
                atn,
                otn,
                onm,
                mch_id,
                uid,
                spayRs
            ).addCallBack(callback, NNS_4).execute()
        }
    }


    fun queryOrderByOrderNo(
        service: String?,
        user_id: String?,
        mch_id: String?,
        out_auth_trade_no: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        if (MainApplication.getInstance().userInfo.isAuthFreezeOpen == 1 && ParamsConstants.PRE_AUTH ==
            PreferenceUtil.getString(ParamsConstants.CHOOSE_PAY_OR_PRE_AUTH, "sale")
        ) {
            mSpayApi.queryOrderByOrderNoAuth(
                service,
                user_id,
                mch_id,
                out_auth_trade_no,
                sign,
                spayRs
            ).addCallBack(callback, NNS_4).execute()
        } else {
            mSpayApi.queryOrderByOrderNoPay(
                service,
                user_id,
                mch_id,
                out_auth_trade_no,
                sign,
                spayRs
            ).addCallBack(callback, NNS_4).execute()
        }
    }


    fun queryCardPaymentOrderDetails(
        out_trade_no: String?,
        order_no_mch: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.queryCardPaymentOrderDetails(
            out_trade_no,
            order_no_mch,
            sign,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun masterCardNativePay(
        mch_id: String?,
        service: String?,
        api_provider: String?,
        body: String?,
        client: String?,
        money: String?,
        device_info: String?,
        attach: String?,
        user_name: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        val un = if (MainApplication.getInstance().isAdmin(0)) user_name else ""
        mSpayApi.masterCardNativePay(
            mch_id,
            service,
            api_provider,
            body,
            client,
            money,
            device_info,
            attach,
            un,
            sign,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun doCoverOrder(
        service: String?,
        out_auth_trade_no: String?,
        da_money: String?,
        client: String?,
        auth_code: String?,
        money: String?,
        mch_id: String?,
        attach: String?,
        device_info: String?,
        user_name: String?,
        u_id: String?,
        body: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {

        val at = if (!BuildConfig.isOverseasPay) attach else ""
        val dei = if (!BuildConfig.isOverseasPay) device_info else ""

        val flag =
            MainApplication.getInstance().userInfo.isAuthFreezeOpen == 1 && ParamsConstants.PRE_AUTH == PreferenceUtil.getString(
                ParamsConstants.CHOOSE_PAY_OR_PRE_AUTH, "sale"
            )
        val un = if (MainApplication.getInstance().isAdmin(0) && !flag) user_name else ""

        val ui = if (MainApplication.getInstance().isAdmin(0)) u_id else ""


        if (MainApplication.getInstance().userInfo.isAuthFreezeOpen == 1 && ParamsConstants.PRE_AUTH == PreferenceUtil.getString(
                ParamsConstants.CHOOSE_PAY_OR_PRE_AUTH, "sale"
            )
        ) {
            mSpayApi.doCoverOrderAuth(
                service,
                out_auth_trade_no,
                da_money,
                client,
                auth_code,
                money,
                mch_id,
                at,
                dei,
                un,
                ui,
                body,
                sign,
                spayRs
            ).addCallBack(callback, NNS_4).execute()
        } else {
            mSpayApi.doCoverOrderPay(
                service,
                out_auth_trade_no,
                da_money,
                client,
                auth_code,
                money,
                mch_id,
                at,
                dei,
                un,
                ui,
                body,
                sign,
                spayRs
            ).addCallBack(callback, NNS_4).execute()
        }
    }


    fun queryWalletList(
        service: String?,
        sign: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.queryWalletList(
            service,
            sign,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun getCode(
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.getCode(
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun getCertificateString(
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.getCertificateString(
            spayRs
        ).addCallBack(callback, NNS_3).execute()
    }


    fun ecdhKeyExchange(
        publick_key: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.ecdhKeyExchange(
            publick_key,
            spayRs
        ).addCallBack(callback, NNS_3).execute()
    }


    fun deviceLogin(
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.deviceLogin(
            spayRs
        ).addCallBack(callback, NNS_1).execute()
    }


    fun getExchangeRate(
        mch_id: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {

        mSpayApi.getExchangeRate(
            mch_id,
            spayRs
        ).addCallBack(callback, NNS_4).execute()
    }


    fun getVersionCode(
        ver_code: String?,
        bank_code: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.getVersionCode(
            ver_code,
            bank_code,
            spayRs
        ).addCallBack(callback, NNS_2).execute()
    }


    fun loginAsync(
        user_name: String?,
        password: String?,
        client: String?,
        s_key: String?,
        bank_code: String?,
        android_trans_push: String?,
        code: String?,
        push_cid: String?,
        spayRs: String?,
        callback: CallBackUtil<*>
    ) {
        mSpayApi.loginAsync(
            user_name,
            password,
            client,
            s_key,
            bank_code,
            android_trans_push,
            code,
            push_cid,
            spayRs
        ).addCallBack(callback, NNS_1).execute()
    }
}