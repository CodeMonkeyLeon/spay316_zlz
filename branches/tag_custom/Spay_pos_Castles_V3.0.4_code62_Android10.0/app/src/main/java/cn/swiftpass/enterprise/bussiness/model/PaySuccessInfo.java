package cn.swiftpass.enterprise.bussiness.model;

/**
 * 财付通交易后的银行和姓名
 * User: Alan
 * Date: 13-8-20
 * Time: 下午3:32
 * 微信二维码
 */
public class PaySuccessInfo
{
    
    public String username;
    
    public String bank_card;
    
}
