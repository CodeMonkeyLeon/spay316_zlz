package cn.swiftpass.enterprise.ui.viewpage;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;

public class NewPage extends ViewPager
{
    private boolean scrollble = true;
    
    public NewPage(Context context)
    {
        super(context);
    }
    
    public NewPage(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
    
    @Override
    protected boolean canScroll(View v, boolean checkV, int dx, int x, int y)
    {
        
        if (v instanceof HorizontalScrollView)
        {
            return true;
        }
        return super.canScroll(v, checkV, dx, x, y);
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent ev)
    {
        if (!scrollble)
        {
            return true;
        }
        return super.onTouchEvent(ev);
    }
    
    public boolean isScrollble()
    {
        return scrollble;
    }
    
    public void setScrollble(boolean scrollble)
    {
        this.scrollble = scrollble;
    }
}
