/*
 * 文 件 名:  ShopBaseDataDB.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2014-3-20
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.io.database.access;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.ShopBankBaseInfo;
import cn.swiftpass.enterprise.io.database.table.ShopBankDataTab;

/**
 * 基本行业 数据 db操作
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2014-3-20]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class ShopBankDataDB
{
    
    private static Dao<ShopBankBaseInfo, Integer> userDao;
    
    private static ShopBankDataDB instance = null;
    
    public static ShopBankDataDB getInstance()
    {
        if (instance == null)
        {
            instance = new ShopBankDataDB();
        }
        
        return instance;
    }
    
    public ShopBankDataDB()
    {
        try
        {
            userDao = MainApplication.getContext().getHelper().getDao(ShopBankBaseInfo.class);
        }
        catch (SQLException e)
        {

        }
    }
    
    /**
     * 保存 商户行业数据
     * <功能详细描述>
     * @param userModel
     * @throws SQLException
     * @see [类、类#方法、类#成员]
     */
    public void save(ShopBankBaseInfo info)
        throws SQLException
    {
        List<ShopBankBaseInfo> result = queryByID(info.bankId);
        if (result == null || result.size() == 0)
        {
            
            userDao.create(info);
        }
        else
        {
            userDao.update(info);
        }
    }
    
    /**
     * 根据id查询
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public List<ShopBankBaseInfo> queryByID(String bankID)
    {
        String sql = "select * from t_shop_bank where bankId = '" + bankID + "'";
        List<ShopBankBaseInfo> list = new ArrayList<ShopBankBaseInfo>();
        try
        {
            
            GenericRawResults<String[]> s = userDao.queryRaw(sql);
            List<String[]> listStr = s.getResults();
            for (int i = 0; i < listStr.size(); i++)
            {
                ShopBankBaseInfo info = new ShopBankBaseInfo();
                
                String[] str = listStr.get(i);
                
                info.bankId = str[0];
                info.bankName = str[1];
                info.code = str[2];
                info.bankNumberCode = str[3];
                
                list.add(info);
            }
            
        }
        catch (SQLException e)
        {
            return list;
        }
        
        return list;
    }
    
    /**
     * 查询父类
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public List<ShopBankBaseInfo> query()
    {
        List<ShopBankBaseInfo> list = findList("-1");
        return list;
    }
    
    public List<ShopBankBaseInfo> findChildBank(String bankId)
    {
        List<ShopBankBaseInfo> list = findList(bankId);
        return list;
    }
    
    private List<ShopBankBaseInfo> findList(String bankId)
    {
        String sql = "select * from t_shop_bank where " + ShopBankDataTab.COLUMN_PARENT_BANK_ID + "=='" + bankId + "'";
        List<ShopBankBaseInfo> list = new ArrayList<ShopBankBaseInfo>();
        try
        {
            
            GenericRawResults<String[]> s = userDao.queryRaw(sql);
            
            List<String[]> listStr = s.getResults();
            for (int i = 0; i < listStr.size(); i++)
            {
                ShopBankBaseInfo info = new ShopBankBaseInfo();
                
                String[] str = listStr.get(i);
                
                info.bankId = str[0];
                info.bankName = str[1];
                info.code = str[2];
                info.bankNumberCode = str[3];
                
                list.add(info);
            }
            
        }
        catch (SQLException e)
        {
            return list;
        }
        
        return list;
    }
}
