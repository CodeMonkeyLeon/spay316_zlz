package cn.swiftpass.enterprise.ui.activity.pos;

import java.util.List;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import cn.newpost.tech.blue.socket.PosBlueSocket;
import cn.newpost.tech.blue.socket.StatusCode;
import cn.newpost.tech.blue.socket.entity.Order;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.enums.TransactionTypeEnum;
import cn.swiftpass.enterprise.bussiness.logica.bluetooth.BluetoothManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.BlueDevice;
import cn.swiftpass.enterprise.bussiness.model.PosBluetoothResult;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.adapter.MyBluetoothAdapter;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.Utils;

/**
 * 蓝牙匹配
 * User: Alan
 * Date: 14-1-20
 * Time: 下午5:32
 * To change this template use File | Settings | File Templates.
 */
public class BlueToothDialog extends BaseActivity implements View.OnClickListener, ListView.OnItemClickListener
{
    private static final String TAG = BlueToothDialog.class.getSimpleName();
    private ListView listView;
    
    private MyBluetoothAdapter myAdapter;
    
    private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    
    private List<BlueDevice> devices;
    
    private TextView tvSet, tvClose;
    
    private Context mContext;
    
    private String posName = "pos6210";
    
    private String money;
    
    private String orderNo;
    
    private boolean isPaying = false;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);
        
        if (mBluetoothAdapter == null)
        {
            showToastInfo(getResources().getString(R.string.unsupported_bluetooth));
            finish();
            return;
        }
        
        initView();
    }
    
    private void initView()
    {
        mContext = this;
        listView = getViewById(R.id.bonded_list);
        tvSet = getViewById(R.id.btn_setbluetooth);
        tvClose = getViewById(R.id.btn_cancel);
        
        tvSet.setOnClickListener(this);
        tvClose.setOnClickListener(this);
        
        myAdapter = new MyBluetoothAdapter(this);
        listView.setAdapter(myAdapter);
        
        Intent it = getIntent();
        money = it.getStringExtra("money");
        orderNo = it.getStringExtra("orderNo");
        
        initData();
    }
    
    @Override
    protected void onResume()
    {
        super.onResume();
        
    }
    
    /***
     * 初始化数据
     */
    private void initData()
    {
        UINotifyListener<List<BlueDevice>> listener = new UINotifyListener<List<BlueDevice>>()
        {
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                if (object != null)
                    showToastInfo(object.toString());
            }
            
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                showLoading(getResources().getString(R.string.wait_a_moment));
            }
            
            @Override
            public void onSucceed(List<BlueDevice> result)
            {
                super.onSucceed(result);
                dismissLoading();
                if (result.size() == 0)
                {
                    showToastInfo(getResources().getString(R.string.please_open_bluetooth));
                    return;
                }
                devices = result;
                myAdapter.setList(result);
                //自动连接
                autoContention();
                
            }
        };
        if (mBluetoothAdapter != null)
            BluetoothManager.getInstance().searchBlueDevice(mBluetoothAdapter.getBondedDevices(), listener);
    }
    
    @Override
    protected void onStart()
    {
        super.onStart();
    }
    
    @Override
    protected void onStop()
    {
        super.onStop();
    }
    
    @Override
    protected void onPause()
    {
        super.onPause();
    }
    
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    private void setBluetool()
    {
        //蓝牙未打开，打开蓝牙
        if (!mBluetoothAdapter.isEnabled())
        {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, 3);
        }
        else
        {
            showToastInfo(getResources().getString(R.string.bluetooth_is_open));
        }
    }
    
    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btn_cancel:
                finish();
                break;
            case R.id.btn_setbluetooth:
                setBluetool();
                break;
            default:
                break;
        }
    }
    
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
    {
        String address = (devices.get(position)).getAddress();
        BluetoothDevice btDev = mBluetoothAdapter.getRemoteDevice(address);
        if (btDev.getBondState() == BluetoothDevice.BOND_BONDED)
        {//已配对     是否能做到不点击直接连接
            if (!address.equals("null"))
            {
                BluetoothManager.getInstance().connectionBluetoothPos(btDev, connectionListener);
            }
        }
    }
    
    private void autoContention()
    {
        for (BlueDevice bd : devices)
        {
            if (bd.getDeviceName().equals(posName))
            {
                String address = bd.getAddress();
                BluetoothDevice btDev = mBluetoothAdapter.getRemoteDevice(address);
                if (btDev.getBondState() == BluetoothDevice.BOND_BONDED)
                {//已配对     是否能做到不点击直接连接
                    if (!address.equals("null"))
                    {
                        BluetoothManager.getInstance().connectionBluetoothPos(btDev, connectionListener);
                    }
                }
                break;
            }
        }
        
    }
    
    PosBlueSocket posBlueSocket;
    
    private UINotifyListener<PosBluetoothResult> connectionListener = new UINotifyListener<PosBluetoothResult>()
    {
        @Override
        public void onError(Object object)
        {
            super.onError(object);
            if (object != null)
                showToastInfo(object.toString());
        }
        
        @Override
        public void onPreExecute()
        {
            super.onPreExecute();
            showLoading(getStringById(R.string.wait_a_moment));
        }
        
        @Override
        public void onPostExecute()
        {
            super.onPostExecute();
            dismissLoading();
        }
        
        /***
         *  连接成功后直接支付
         * @param result
         */
        @Override
        public void onSucceed(PosBluetoothResult result)
        {
            super.onSucceed(result);
            posBlueSocket = result.posBlueSocket;
            switch (result.state)
            {
                case StatusCode.SUCCESS:// 连接成功
                    order = new Order(); //下单
                    order.setOrderId(orderNo);
                    order.setAmount(BluetoothManager.fillLeftWith0(money, 12)); //金额加密
                    order.setTime(System.currentTimeMillis() + "");
                    posTransacte(result.posBlueSocket, order);
                    break;
                case StatusCode.CONNECT_RFCOMM_ERROR:
                    posBlueSocket.close();
                    showToastInfo("连接错误，code:" + StatusCode.CONNECT_RFCOMM_ERROR);
                    break;
                case StatusCode.CONNECT_HTTP_ERROR:
                    posBlueSocket.close();
                    showToastInfo("连接错误，code:" + StatusCode.CONNECT_RFCOMM_ERROR);
                    break;
                default:
                    break;
            
            }
        }
    };
    
    private Order order;
    
    /**进行交易*/
    private void posTransacte(PosBlueSocket blueSocket, final Order order)
    {
        BluetoothManager.getInstance().posTransacte(blueSocket, order, new UINotifyListener<Order>()
        {
            @Override
            public void onError(Object object)
            {
                super.onError(object);
            }
            
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                showLoading(getStringById(R.string.wait_a_moment));
            }
            
            @Override
            public void onPostExecute()
            {
                super.onPostExecute();
                dismissLoading();
            }
            
            @Override
            public void onSucceed(Order result)
            {
                super.onSucceed(result);
                switch (result.getStatus())
                {
                    case StatusCode.ERROR://交易失败
                    {
                        showToastInfo(getStringById(R.string.trading_failure));
                        break;
                    }
                    case StatusCode.CANCEL://交易取消
                    {
                        showToastInfo(getStringById(R.string.trading_cancel));
                        break;
                    }
                    case StatusCode.SUCCESS://交易完成
                    {
                        if (result != null)
                        {
                            Log.i(TAG,"交易前：" + order.getOrderId() + " 交易后:" + result.getOrderId());
                            //if(order.getOrderId().equals(result.getOrderId())){  //位数不对
                            order.setCardNO(result.getCardNO());
                            isPaying = true;
                            showConsumeStatus();
                            // }else{
                            //showToastInfo("交易数据错误！");
                            // }
                        }
                        break;
                    }
                    default:
                        break;
                }
                posBlueSocket.close();
            }
        });
    }
    
    /**
     * 订单完成后显示对话框
     */
    public void showConsumeStatus()
    {
        
        submitOrder();
        //        StringBuffer sb = new StringBuffer();
        //        sb.append(String.format(getString(R.string.orders_id), order.getOrderId()));
        //        sb.append("\n");
        //        sb.append(String.format(getString(R.string.orders_amount), BluetoothManager.amountConvert(order.getAmount())));
        //        sb.append("\n");
        //        sb.append(String.format(getString(R.string.card_no_var), order.getCardNO()));
        //        sb.append("\n");
        long t = Utils.Long.tryParse(order.getTime(), System.currentTimeMillis());
        String s = DateUtil.formatTime(t, "yyyy-MM-dd HH:mm:ss");
        //        sb.append(String.format(getString(R.string.tran_time), s));
        
        //        CommonConfirmDialog.show(this,
        //            getStringById(R.string.trading_success),
        //            sb.toString(),
        //            new CommonConfirmDialog.ConfirmListener()
        //            {
        //                @Override
        //                public void ok()
        //                {
        //                    isPaying = false;
        //                    finish();
        //                }
        //                
        //                @Override
        //                public void cancel()
        //                {
        //                    isPaying = false;
        //                    finish();
        //                }
        //            },
        //            true);
        
    }
    
    /***
     *
     * @param money
     */
    public static void startActivity(Context mContext, String money, String orderNo)
    {
        Intent it = new Intent();
        it.setClass(mContext, BlueToothDialog.class);
        it.putExtra("money", money);
        it.putExtra("orderNo", orderNo);
        mContext.startActivity(it);
    }
    
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (posBlueSocket != null)
            posBlueSocket.close();
    }
    
    /**提交订单*/
    private void submitOrder()
    {
        cn.swiftpass.enterprise.bussiness.model.Order o = new cn.swiftpass.enterprise.bussiness.model.Order();
        // UserModel user =  LocalAccountManager.getInstance().getLoggedUser();
        o.money = Integer.parseInt(money);//单位分
        o.remark = "pos刷卡交易";
        o.orderNo = orderNo;
        o.add_time = System.currentTimeMillis();
        o.transactionType = TransactionTypeEnum.PAY_POS.getValue();
        
        OrderManager.getInstance().submitOrderData(new UINotifyListener<String[]>()
        {
            @Override
            public void onError(Object object)
            {
                super.onError(object);
            }
            
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
            }
            
            @Override
            public void onSucceed(String[] result)
            {
                super.onSucceed(result);
                showToastInfo(getStringById(R.string.trading_success));
            }
            
            @Override
            public void onPostExecute()
            {
                super.onPostExecute();
                
            }
        }, o, null);
    }
}
