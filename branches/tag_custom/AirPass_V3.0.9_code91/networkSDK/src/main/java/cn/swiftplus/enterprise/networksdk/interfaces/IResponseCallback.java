package cn.swiftplus.enterprise.networksdk.interfaces;


public interface IResponseCallback {

    //请求成功回调事件处理
    void onSuccess(Object ResultObject);

    //请求失败回调事件处理
    void onFailure(Object ErrorObject);

}
