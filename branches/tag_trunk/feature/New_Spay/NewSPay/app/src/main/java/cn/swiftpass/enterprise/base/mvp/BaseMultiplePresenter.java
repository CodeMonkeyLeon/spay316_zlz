package cn.swiftpass.enterprise.base.mvp;

import java.util.ArrayList;
import java.util.List;

/**
 * 创建人：caoxiaoya
 * 时间：2019/4/29
 * 描述：用于多个P复用
 * 备注：XXXActivity extends BaseActivity<XXXPersenter>， XXXPersenter替换成BaseMultiplePresenter
 * new BaseMultiplePresenter并调用addPresenter，把相关Persenter加进去
 */

public class BaseMultiplePresenter<V extends IView> extends BasePresenter<V> {

    private List<BasePresenter> mPresenterList = new ArrayList<>();

    /**
     * list集合装入多个P
     * 每个P其实已经单独进行相关绑定和判断，这里无需进行多次绑定，只需添加
     *
     * @param cls
     * @param <P>
     */
    @SafeVarargs
    public final <P extends BasePresenter<V>> void addPresenter(P... cls) {
        for (P multipleP : cls) {
            mPresenterList.add(multipleP);
        }
    }

    /**
     * 遍历每一个P进行解绑
     */
    @Override
    public void detachView() {
        for (BasePresenter presenter : mPresenterList) {
            presenter.detachView();
        }
    }

}
