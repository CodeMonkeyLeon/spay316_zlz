package cn.swiftpass.enterprise.utils;

import android.text.InputFilter;
import android.text.Spanned;

/**
 * Created by congwei.li on 2021/9/24.
 *
 * @Description:
 */
public class EditTextDigitsFilter implements InputFilter {

    String digits;

    public EditTextDigitsFilter(String digits) {
        this.digits = digits;
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest,
                               int dstart, int dend) {
        for (int i = start; i < end; i++) {
            if (!digits.contains(String.valueOf(source.charAt(i)))) {
                return "";
            }
        }
        return null;
    }
}
