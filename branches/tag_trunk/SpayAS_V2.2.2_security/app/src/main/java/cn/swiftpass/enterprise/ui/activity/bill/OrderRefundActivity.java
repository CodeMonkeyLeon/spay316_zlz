/*
 * 文 件 名:  ReportActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-8
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.bill;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Selection;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.bill.BillOrderManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.refund.RefundManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.PayResultActivity;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.spayMainTabActivity;
import cn.swiftpass.enterprise.ui.widget.CustomDialog;
import cn.swiftpass.enterprise.ui.widget.dialog.RefundConditonConfirmDialog;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 退款
 * 
 * @author  he_hui
 * @version  [版本号, 2016-11-8]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class OrderRefundActivity extends TemplateActivity {
    
    private Order orderModel;
    
    private TextView tx_order, tv_money, tv_refunding,tv_money_type,tv_bill_money,et_order_max_money;
    
    private EditText et_money;
    
    private Button btn_next_step;
    private CustomDialog  mCustomDialog;
    private RefundConditonConfirmDialog Dialog_Refund;
    private double minus = 1;
    private String money="";
    private LinearLayout.LayoutParams mLayoutParams;
    private String dialogTitle;
    private int activityType;
    private String allMoney;

    private Pattern mPattern = Pattern.compile("[^0-9.]");

    public static void startActivity(Context context, Order orderModel) {
        Intent it = new Intent();//默认activityType=0
        it.setClass(context, OrderRefundActivity.class);
        it.putExtra("order", orderModel);
        context.startActivity(it);
    }

    public static void startActivity(Context context, Order orderModel,int activityType) {
        Intent it = new Intent();//消费-正常退款1，预授权解冻2，预授权支付4(不需要密码)
        it.setClass(context, OrderRefundActivity.class);
        it.putExtra("order", orderModel);
        it.putExtra("activityType", activityType);
        context.startActivity(it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_order_refund);
        activityType=getIntent().getIntExtra("activityType",0);
        if (activityType==0){
            titleBar.setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
        }
        MainApplication.listActivities.add(this);
        //根据当前的小数点的位数来判断应该除以多少
        for(int i = 0 ; i < MainApplication.numFixed ; i++ ){
            minus = minus * 10;
        }
        initview();
        CharSequence text = et_money.getText();
        //Debug.asserts(text instanceof Spannable);
        if (text instanceof Spannable)
        {
            Spannable spanText = (Spannable)text;
            Selection.setSelection(spanText, text.length());
        }
        setLister();
        showSoftInputFromWindow(OrderRefundActivity.this, et_money);
        orderModel = (Order)getIntent().getSerializableExtra("order");
        if (orderModel != null) {
            if (activityType==2||activityType==4){
                tx_order.setText(orderModel.getAuthNo());
                switch (activityType){
                    case 2:
                        titleBar.setTitle(R.string.unfreezed);
                        tv_refunding.setText(R.string.remaining_amount);
                        tv_bill_money.setText(R.string.unfreezing_amount);//解冻
                        dialogTitle=getString(R.string.please_unfreeze);
                        break;
                    case 4:
                        titleBar.setTitle(R.string.capture);
                        tv_refunding.setText(R.string.remaining_amount);
                        tv_bill_money.setText(R.string.capture_amount);//支付收款
                        dialogTitle=getString(R.string.please_capture);
                        break;
                    default:
                        break;
                }
                et_money.setTextColor(getResources().getColor(R.color.bg_text_pre_auth));
                btn_next_step.setBackgroundResource(R.drawable.btn_pre_finish);
              //  double overMoney=orderModel.getTotalFreezeAmount()-(orderModel.getTotalPayAmount()+orderModel.getRestAmount());//剩余授权金额
                money=DateUtil.formatMoneyUtils(orderModel.getRestAmount());
                et_money.setText(DateUtil.formatMoneyUtils(orderModel.getRestAmount()));
                tv_money.setText(MainApplication.getFeeFh()+DateUtil.formatMoneyUtils(orderModel.getRestAmount()));
                btn_next_step.setText(R.string.bt_confirm);
                et_order_max_money.setText(R.string.platform_pre_auth_order_id);
            }else {
                et_money.setTextColor(getResources().getColor(R.color.bg_text_new));
                btn_next_step.setBackgroundResource(R.drawable.btn_finish);
                titleBar.setTitle(R.string.apply_refund);
                dialogTitle=getString(R.string.please_confirm_amount);
                if (activityType==3){
                    tx_order.setText(orderModel.getOrderNo());
                }else {
                    tx_order.setText(orderModel.getOrderNoMch());
                }
                tv_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(orderModel.getMoney()));
                et_money.setText(DateUtil.formatMoneyUtils(orderModel.getMoney()));
                tv_bill_money.setText(getString(R.string.tx_bill_stream_refund_money));
                money=DateUtil.formatMoneyUtils(orderModel.getMoney());
                if (orderModel.getRfMoneyIng() > 0 || orderModel.getRefundMoney() > 0) {//部分退款
                    tv_refunding.setText(R.string.tx_can_refund_money);
                    long refMoney = orderModel.getMoney() - orderModel.getRfMoneyIng() - orderModel.getRefundMoney();
                    tv_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(refMoney));
                    et_money.setText(DateUtil.formatMoneyUtils(refMoney));
                    money=DateUtil.formatMoneyUtils(refMoney);
                }
            }


            et_money.setSelection(et_money.length());//将光标移至文字末尾
            tv_money_type.setText(MainApplication.getFeeFh());
        }

        setPricePoint(et_money);
        allMoney=et_money.getText().toString();
    }
    
    private void setPricePoint(final EditText editText)
    {
        editText.addTextChangedListener(new TextWatcher()
        {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(MainApplication.numFixed == 0){//即没有小数点的位数，最小币种单位为1
                    //如果币种的最小单位为1，则不让输入小数点
                    if (s.toString().contains(".")){
                        s = s.toString().subSequence(0, s.toString().indexOf("."));
                        editText.setText(s);
                        editText.setSelection(s.length());//光标跟在后面
                    }
                } else {//如果有小数点位数，则根据位数判断输入的截取
                    if (s.toString().contains("."))
                    {
                        if (s.length() - 1 - s.toString().indexOf(".") > MainApplication.numFixed)
                        {
                            s = s.toString().subSequence(0, s.toString().indexOf(".") + MainApplication.numFixed+1);
                            editText.setText(s);
                            editText.setSelection(s.length());
                        }
                    }
                    if (s.toString().trim().substring(0).equals("."))
                    {
                        s = "0" + s;
                        editText.setText(s);
                        editText.setSelection(2);
                    }

                    if (s.toString().startsWith("0") && s.toString().trim().length() > 1)
                    {
                        if (!s.toString().substring(1, 2).equals("."))
                        {
                            editText.setText(s.subSequence(0, 1));
                            editText.setSelection(1);
                            return;
                        }
                    }
                }

                //限制输入的最大金额不能超过可退金额
                if(s.length() > 0 ){
                    String moneyText = et_money.getText().toString();
                    //Pattern p = Pattern.compile("[^0-9.]");//去掉当前字符串中除去0-9以及小数点的其他字符
                    Matcher m = mPattern.matcher(moneyText);
                    moneyText = m.replaceAll("");

                    if(moneyText.equals(".")){//如果输入的是小数点则不走下面的逻辑
                        return;
                    }
                    long refound = 0;
                    if(!TextUtils.isEmpty(moneyText)){
                        BigDecimal bigDecimal = new BigDecimal(moneyText).multiply(new BigDecimal(minus));
                        refound = bigDecimal.longValue();
                    }

                    long canRefound ;
                    if (activityType==2||activityType==4){
                        canRefound = orderModel.getRestAmount();//剩余授权金额
                    }else {
                        canRefound = orderModel.getMoney();
                    }

                    if (orderModel.getRfMoneyIng() > 0 || orderModel.getRefundMoney() > 0) {//部分退款
                        long refMoney = orderModel.getMoney() - orderModel.getRfMoneyIng() - orderModel.getRefundMoney();
                        canRefound = refMoney;
                    }
                    if(refound > canRefound){
                        et_money.setText(s.subSequence(0,s.length()-1));
                        et_money.setSelection(et_money.getText().length());
                    }
                }

            }
            
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            
            @Override
            public void afterTextChanged(Editable s) {
                String inputMoneyText="";
                if ( et_money.getText().toString().contains(",")){
                    inputMoneyText=et_money.getText().toString().replaceAll(",","");
                    et_money.setText(inputMoneyText);
                    et_money.setSelection(et_money.length());//将光标追踪到内容的最后
                }else {
                    inputMoneyText = et_money.getText().toString();
                }

                allMoney=et_money.getText().toString();

                //Pattern p = Pattern.compile("[^0-9.]");//去掉当前字符串中除去0-9以及小数点的其他字符
                Matcher matcher = mPattern.matcher(inputMoneyText);
                inputMoneyText = matcher.replaceAll("");
                double inputMoney = 0;
                if(!TextUtils.isEmpty(inputMoneyText)){
                    BigDecimal bigDecimal = new BigDecimal(inputMoneyText);
                    inputMoney = bigDecimal.doubleValue();
                }

                if (inputMoneyText.length()>0){
                    mLayoutParams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                }else {
                    mLayoutParams = new LinearLayout.LayoutParams(
                            50, LinearLayout.LayoutParams.WRAP_CONTENT);
                }
                if (activityType==2||activityType==4){
                    if(s.toString().length() > 0 && inputMoney != 0){
                        setButtonBg(btn_next_step, true, R.string.bt_confirm);

                    }else{
                        setButtonBg(btn_next_step, false, R.string.bt_confirm);
                    }
                }else {
                    if(s.toString().length() > 0 && inputMoney != 0){
                        setButtonBg(btn_next_step, true, R.string.tx_bill_stream_refund_cirfom);

                    }else{
                        setButtonBg(btn_next_step, false, R.string.tx_bill_stream_refund_cirfom);
                    }
                }

                et_money.setLayoutParams(mLayoutParams);
            }
            
        });
        
    }

    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b,enable,res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            if (activityType==2||activityType==4){
                b.setBackgroundResource(R.drawable.btn_nor_pre_shape);
            }else {
                b.setBackgroundResource(R.drawable.btn_nor_shape);
            }
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            if (activityType==2||activityType==4){
                b.setBackgroundResource(R.drawable.btn_press_pre_shape);
            }else {
                b.setBackgroundResource(R.drawable.btn_press_shape);
            }
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }


    private void setLister() {
        btn_next_step.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View v) {
                if (str2Doule(money)==str2Doule(et_money.getText().toString())){
                    showDialog();
                }else {
                    confirm();
                }
            }
        });
    }

    private void showDialog(){
        mCustomDialog = new CustomDialog.Builder(OrderRefundActivity.this)
                .gravity(Gravity.CENTER).widthdp(250)
                .cancelTouchout(false)
                .view(R.layout.dialog_confirm)
                .addViewOnclick(R.id.tv_cancel, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCustomDialog.cancel();
                    }
                }).addViewOnclick(R.id.tv_confirm, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirm();
                        mCustomDialog.cancel();
                    }
                })
                .build();
        mCustomDialog.show();
        TextView tv_money=(TextView)mCustomDialog.findViewById(R.id.tv_money);
        TextView tv_currency=(TextView)mCustomDialog.findViewById(R.id.tv_currency);
        TextView tv_title=(TextView)mCustomDialog.findViewById(R.id.tv_title);
        TextView tv_cancel = (TextView)mCustomDialog.findViewById(R.id.tv_cancel);
        TextView tv_confirm = (TextView)mCustomDialog.findViewById(R.id.tv_confirm);
        tv_currency.setText(MainApplication.getFeeFh());
        tv_money.setText(et_money.getText().toString());
        tv_title.setText(dialogTitle);

        if(activityType == 2 || activityType == 4){
            tv_cancel.setTextColor(getResources().getColor(R.color.bg_text_pre_auth));
            tv_confirm.setTextColor(getResources().getColor(R.color.bg_text_pre_auth));
        }else{
            tv_cancel.setTextColor(getResources().getColor(R.color.bg_text_new));
            tv_confirm.setTextColor(getResources().getColor(R.color.bg_text_new));
        }
    }

    private void confirm(){
        String input_money = et_money.getText().toString();
        if (TextUtils.isEmpty(input_money))
        {

            toastDialog(OrderRefundActivity.this, R.string.et_refund_moeny, null);//退款金额不能为空
            return;
        }
        if (input_money.contains(".") && input_money.substring(input_money.lastIndexOf('.')).equals("."))
        {
            toastDialog(OrderRefundActivity.this, R.string.et_refund_format, null);//退款金额格式错误
            et_money.setFocusable(true);
            return;
        }
        long editMoney = 0;
        if (input_money.contains(".")) {
            //                    double d = Double.parseDouble(input_money) * 100;
            editMoney = Long.parseLong(OrderManager.getInstance().getMoney(input_money));
        }
        else if (input_money.contains(",")){
               //如果包含有这个符号，则去掉
                editMoney = Long.parseLong(input_money.replaceAll(",","")) * (long) minus;

        }else {
            editMoney = Long.parseLong(input_money) * (long) minus;
        }
        if (activityType==2||activityType==4) {
            if (editMoney > orderModel.getTotalFreezeAmount()) {
                toastDialog(OrderRefundActivity.this, R.string.et_refund_less_money, null);
                et_money.setFocusable(true);
                return;
            }
        }else {
            if (editMoney > orderModel.getMoney()) {
                toastDialog(OrderRefundActivity.this, R.string.et_refund_less_money, null);
                et_money.setFocusable(true);
                return;
            }
        }
        //验证是否可以发起退款
        orderModel.setRefundFeel(editMoney);
        if (activityType==4){
            unifiedAuthPay();//预授权支付不需要密码验证
        }else {
            requestRefoundState();//预授权解冻需要密码验证，所以要走这一步
        }
    }

    private void requestRefoundState(){
        RefundManager.getInstant().checkCanRefundOrNot(orderModel.getOutTradeNo(),
                orderModel.getMoney(),
                orderModel.getRefundFeel(),
                new UINotifyListener<Boolean>(){
                    @Override
                    public void onError(Object object) {
                        super.onError(object);
                        dismissLoading();
                        if (object != null) {
                            if(object instanceof  Integer){
                                int resultCode = (Integer) object;
                                switch (resultCode) {
                                    case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                        ToastHelper.showToastUIThread( OrderRefundActivity.this ,ToastHelper.toStr(R.string.show_net_bad));
                                        break;
                                    case RequestResult.RESULT_TIMEOUT_ERROR:
                                        ToastHelper.showToastUIThread( OrderRefundActivity.this ,ToastHelper.toStr(R.string.show_net_timeout));
                                        break;
                                    case RequestResult.RESULT_READING_ERROR:
                                        ToastHelper.showToastUIThread( OrderRefundActivity.this ,ToastHelper.toStr(R.string.show_net_server_fail));
                                        break;
                                        default:
                                            break;
                                }
                            }else {
                                //如果请求失败则弹框提示当前的失败信息
                                final String message = object.toString();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        switch (activityType){
                                            case 0://默认消费需要弹窗提示
                                                Dialog_Refund=new RefundConditonConfirmDialog(OrderRefundActivity.this, new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        //如果点击全额退款的处理，此处就要更改输入金额，然后跳转到确认身份的页面
                                                        //如果点击全额退款，则要，先判断是否是部分退款的全额退款
                                                        orderModel.setRefundFeel(orderModel.getMoney());
                                                        et_money.setText(DateUtil.formatMoneyUtils(orderModel.getMoney()));
                                                        //部分退款
                                                        if (orderModel.getRfMoneyIng() > 0 || orderModel.getRefundMoney() > 0)
                                                        {
                                                            long refMoney = orderModel.getMoney() - orderModel.getRfMoneyIng() - orderModel.getRefundMoney();
                                                            orderModel.setRefundFeel(refMoney);
                                                            et_money.setText(DateUtil.formatMoneyUtils(refMoney));
                                                        }

                                                        et_money.setSelection(et_money.length());//将光标追踪到内容的最后
                                                        Dialog_Refund.dismiss();
                                                        OrderRefundLoginConfirmActivity.startActivity(OrderRefundActivity.this, orderModel);
                                                    }
                                                });
                                                if(Dialog_Refund != null && !Dialog_Refund.isShowing()){
                                                    Dialog_Refund.setMessage(message);
                                                    Dialog_Refund.show();
                                                }
                                                break;
                                            case 2:
                                                orderModel.setRefundFeel(orderModel.getMoney());
                                                if (allMoney.equals(DateUtil.formatMoneyUtils(orderModel.getMoney()))) {
                                                    et_money.setText(DateUtil.formatMoneyUtils(orderModel.getMoney()));
                                                }else {
                                                    et_money.setText(allMoney);
                                                }
                                                et_money.setSelection(et_money.length());//将光标追踪到内容的最后
                                                String input_money = et_money.getText().toString();
                                                long  money = Long.parseLong(OrderManager.getInstance().getMoney(input_money));
                                                orderModel.setMoney(money);//赋值给该字段，方便下级取值
                                                OrderRefundLoginConfirmActivity.startActivity(OrderRefundActivity.this, orderModel,2);
                                                break;
                                                default:break;
                                        }


                                    }
                                });
                            }

                        }
                    }

                    @Override
                    public void onPreExecute() {
                        super.onPreExecute();
                        loadDialog(OrderRefundActivity.this, getStringById(R.string.public_data_loading));
                    }

                    @Override
                    public void onPostExecute() {
                        super.onPostExecute();
                        dismissLoading();
                    }

                    @Override
                    public void onSucceed(Boolean result) {
                        super.onSucceed(result);
                        dismissLoading();
                        if(result){
                            //只要请求成功，就直接跳转到确认身份的页面
                            switch (activityType){
                                case 0://消费 0是默认值
                                    OrderRefundLoginConfirmActivity.startActivity(OrderRefundActivity.this, orderModel);
                                    break;
                                case 2://解冻
                                    String input_money = et_money.getText().toString();
                                    long  editMoney = Long.parseLong(OrderManager.getInstance().getMoney(input_money));
                                    orderModel.setMoney(editMoney);//赋值给该字段，方便下级取值
                                  OrderRefundLoginConfirmActivity.startActivity(OrderRefundActivity.this, orderModel,2);
                                        break;
                                case 3:
                                    OrderRefundLoginConfirmActivity.startActivity(OrderRefundActivity.this, orderModel);
                                    break;
                                    default:
                                        break;
                            }

                        }
                    }
                });
    }

    /**
     * 当支付结果未知时，5s一次，轮询2次，点击ok后，跳转到账单流水页。
     * 因此生成的未支付订单，在订单详情下方添加“同步订单状态”按钮（页面2.8），
     * 每次点击按钮，则会查询一次。
     */
    private CustomDialog  errorDialog;
    private void showErrorDialog(String msg){
            errorDialog = new CustomDialog.Builder(OrderRefundActivity.this)
                    .gravity(Gravity.CENTER).widthdp(250)
                    .cancelTouchout(true)
                    .view(R.layout.dialog_error)
                    .addViewOnclick(R.id.tv_confirm, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            errorDialog.dismiss();
                            if(finishTask){
                                finishTask = false;
                                Intent it = new Intent();
                                it.putExtra("tab_index","bill");
                                it.setClass(OrderRefundActivity.this, spayMainTabActivity.class);

                                for (Activity a : MainApplication.listActivities) {
                                    a.finish();
                                }
                            }
                        }
                    })
                    .build();
        errorDialog.show();
        if (msg!=null && !msg.isEmpty()) {
            TextView tv_title=(TextView)errorDialog.findViewById(R.id.tv_title);
            tv_title.setText(msg);
        }

    }

    /**
     * cxy
     * 预授权转支付
     */
    private void unifiedAuthPay(){
        if (orderModel!=null){
            String input_money = et_money.getText().toString();
            long   editMoney = Long.parseLong(OrderManager.getInstance().getMoney(input_money));
            BillOrderManager.getInstance().unifiedAuthPay(editMoney+"",orderModel.getAuthNo(),orderModel.getBody(),new UINotifyListener<Order>(){

                @Override
                public void onPreExecute() {
                    super.onPreExecute();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadDialog(OrderRefundActivity.this, getStringById(R.string.public_data_loading));
                        }
                    });

                }

                @Override
                public void onSucceed(Order result) {
                    super.onSucceed(result);
                    dismissLoading();
                    if (result!=null){
                        result.setTradeType(orderModel.getTradeType());
                        result.setOrderFee(result.getTotalFee());//金额
                        result.setApiCode("2");//支付宝
                        //result.setOrderNoMch(result.getTransactionId());//平台授权订单号 返回参数自带值
                       // result.setTransactionId(result.getOrderNoMch());//支付宝单号  返回参数自带值

                        result.setTradeName(result.getTradeName());//支付方式
                        PayResultActivity.startActivity(OrderRefundActivity.this,result);
                    }
                }

                @Override
                public void onError(Object object) {
                    super.onError(object);
                    String message = object.toString();
                    if (message !=null){
                    if (message.contains("SPAY")) {
                        outRequestNo =message.replaceAll("SPAY","");
                        authPayQuery(outRequestNo);//失败的时候再去调用支付查询接口，5S轮训两次
                    }else {
                        dismissLoading();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showErrorDialog(getString(R.string.payment_failed));
                            }
                        });

                    }

                    }
                }
            });
        }
    }


    private  Timer timer;
    private Task task;
    private int  timeTag=0;//记录次数、
    private boolean finishTask = false;
    private Handler mHandler=new Handler();
    private String outRequestNo;
    /**
     * 自定义TimerTask对象
     */
    public class Task extends TimerTask {
        @Override
        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    timeTag++;
                    if (timeTag<2){
                        authPayQuery(outRequestNo);
                        Log.e("cxy","Task timeTag<2");
                    }else if (timeTag>=2){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dismissLoading();
                                finishTask = true;
                                showErrorDialog(null);
                                Log.e("cxy","Task timeTag>2");
                                timeTag=0;
                                timer.cancel();
                            }
                        });

                    }
                }
            });
        }
    }

    /**
     * 预授权转支付查询
     */
    private void authPayQuery(String outRequestNo){
        if (orderModel!=null){
            BillOrderManager.getInstance().authPayQuery("",outRequestNo,new UINotifyListener<Order>(){
                @Override
                public void onSucceed(Order result) {
                    super.onSucceed(result);
                    if (result!=null){
                        if(result.getTradeState().equals(2)){
                            dismissLoading();

                            result.setTradeType(orderModel.getTradeType());
                            result.setOrderFee(result.getTotalFee());//金额
                            result.setApiCode("2");//支付宝
                            result.setOrderNoMch(result.getOrderNoMch());//平台授权订单号
                            result.setTransactionId(result.getTransactionId());//支付宝单号

                            PayResultActivity.startActivity(OrderRefundActivity.this,result);

                        }else {//如果业务请求是成功了，但是状态tradeState不是2，则还要去查询
                            if (timer==null&&task==null) {
                                timer = new Timer();
                                task = new Task();
                                timer.schedule(task, 0, 5 * 1000);
                            }
                        }
                    }
                }

                @Override
                public void onError(Object object) {
                    super.onError(object);
                    if (timer==null&&task==null) {
                        timer = new Timer();
                        task = new Task();
                        timer.schedule(task, 0, 5 * 1000);
                    }
                }
            });
        }
    }

    private void initview()
    {
        et_order_max_money=getViewById(R.id.et_order_max_money);
        tv_bill_money=getViewById(R.id.tv_bill_money);
        tv_refunding = getViewById(R.id.tv_refunding);
        btn_next_step = getViewById(R.id.btn_next_step);
        tx_order = getViewById(R.id.tx_order);
        tv_money = getViewById(R.id.tv_money);
        et_money = getViewById(R.id.et_money);
        tv_money_type=getViewById(R.id.tv_money_type);
        mLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        et_money.setLayoutParams(mLayoutParams);

        String saleOrPreauth = PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth","sale");
        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(saleOrPreauth,"pre_auth")){//授权
            btn_next_step.setBackgroundResource(R.drawable.btn_pre_finish);
            et_money.setTextColor(getResources().getColor(R.color.bg_text_pre_auth));
        }else{//消费
            btn_next_step.setBackgroundResource(R.drawable.btn_finish);
            et_money.setTextColor(getResources().getColor(R.color.bg_text_new));
        }
    }
    
    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitleChoice(false);
        titleBar.setLeftButtonVisible(true);
        titleBar.setLeftButtonIsVisible(true);
        String saleOrPreauth = PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth","sale");
        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(saleOrPreauth,"pre_auth")){//授权
            titleBar.setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
        }else{//消费
            titleBar.setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
            titleBar.setTitle(R.string.apply_refund);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (task != null) {
            task.cancel();
            task = null;
        }
    }


    public double str2Doule(String numText){
        if(TextUtils.isEmpty(numText)){
            return 0;
        }
        String num = numText;
        Matcher matcher = mPattern.matcher(num);
        num = matcher.replaceAll("");
        double inputMoney = 0;
        if(!TextUtils.isEmpty(num)){
            BigDecimal bigDecimal = new BigDecimal(num);
            inputMoney = bigDecimal.doubleValue();
        }
        return inputMoney;
    }
}
