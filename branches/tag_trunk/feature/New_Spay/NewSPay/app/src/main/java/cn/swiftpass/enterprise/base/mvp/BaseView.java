package cn.swiftpass.enterprise.base.mvp;

/**
 * 创建人：caoxiaoya
 * 时间：2019/4/16
 * 描述：用于V层调用loading
 * 备注：
 */
public interface BaseView {
    void showLoadingDialog();
    void showLoadingDialog(String text);
    void showLoadingDialog(String text,boolean isCancelOutside);
    void dismissLoadingDialog();
}
