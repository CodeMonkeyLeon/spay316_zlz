package cn.swiftpass.enterprise.ui.activity;

import android.os.Bundle;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;

/**
 * Created by aijingya on 2019/8/21.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(为云播报音箱配置网络引导页面)
 * @date 2019/8/21.15:15.
 */
public class MySpeakerConfigurationInstructionsActivity extends BaseActivity {

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speaker_configurations_instructions);
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(getStringById(R.string.terminal_speaker_bind_instruction));
        titleBar.setLeftButtonVisible(true);

    }
}
