package cn.swiftpass.enterprise.bussiness.enums;

/**
 * 交易类型
 * User: Alan
 * Date: 14-3-11
 * Time: 下午6:56
 * To change this template use File | Settings | File Templates.
 */
public enum PayType
{
    PAY_WX(0, "微信支付"), PAY_POS(1, "POS支付"), PAY_CASH(2, "现金支付"), PAY_WXSK(3, "微信小额刷卡");
    
    private final Integer value;
    
    private String displayName = "";
    
    private PayType(Integer v, String displayName)
    {
        this.value = v;
        this.displayName = displayName;
    }
    
    public String getDisplayName()
    {
        return displayName;
    }
    
    public Integer getValue()
    {
        return value;
    }
}
