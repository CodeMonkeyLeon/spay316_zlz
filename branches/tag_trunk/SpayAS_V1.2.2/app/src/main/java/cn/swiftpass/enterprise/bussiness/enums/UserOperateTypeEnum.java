package cn.swiftpass.enterprise.bussiness.enums;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-10-19
 * Time: 下午6:13
 * To change this template use File | Settings | File Templates.
 */
public enum UserOperateTypeEnum
{
    /**
     * 管理账号
     */
    manager(105, "管理账号"), approve(106, "审核账号"), ;
    
    private Integer enumId;
    
    private String displayName;
    
    private UserOperateTypeEnum(Integer enumId, String displayName)
    {
        this.displayName = displayName;
        this.enumId = enumId;
    }
    
    public String getDisplayName()
    {
        return displayName;
    }
    
    public Integer getEnumId()
    {
        // TODO Auto-generated method stub
        return this.enumId;
    }
}
