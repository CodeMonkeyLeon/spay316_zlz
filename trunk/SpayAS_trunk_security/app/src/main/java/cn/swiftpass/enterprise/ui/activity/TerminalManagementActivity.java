package cn.swiftpass.enterprise.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.common.utils.PngSetting;

import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;
import cn.swiftpass.enterprise.ui.activity.print.BluetoothSettingActivity;
import cn.swiftpass.enterprise.utils.PngUtils;

/**
 * Created by aijingya on 2019/8/13.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(设备管理的界面)
 * @date 2019/8/13.17:26.
 */
public class TerminalManagementActivity extends BaseActivity implements View.OnClickListener {

    private LinearLayout ll_Speaker, ll_Printer;

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }


    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terminal_management_layout);
        initView();

        if (PngSetting.INSTANCE.isChangePng1()) {
            intiPng();
        }
    }

    private void intiPng() {
        ImageView imgSpeaker = findViewById(R.id.id_img_speaker);
        ImageView imgPrinter = findViewById(R.id.id_img_printer);

        PngUtils.INSTANCE.setPng1(this, imgSpeaker, R.drawable.icon_terminal_management_speaker);
        PngUtils.INSTANCE.setPng1(this, imgPrinter, R.drawable.icon_terminal_management_printer);
    }


    public void initView() {
        ll_Speaker = findViewById(R.id.ll_Speaker);
        ll_Speaker.setOnClickListener(this);

        if (!BuildConfig.isShowCloudSpeaker) {
            //屏蔽云播报
            ll_Speaker.setVisibility(View.GONE);
        }

        ll_Printer = findViewById(R.id.ll_Printer);
        ll_Printer.setOnClickListener(this);
        if (BuildConfig.IS_POS_VERSION) {
            ll_Printer.setVisibility(View.GONE);
        } else {
            ll_Printer.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_Speaker:
                showPage(MySpeakerListActivity.class);
                break;
            case R.id.ll_Printer:
                showPage(BluetoothSettingActivity.class);
                break;
        }
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(getStringById(R.string.fragment_tab_setting_terminal_management));
    }


}
