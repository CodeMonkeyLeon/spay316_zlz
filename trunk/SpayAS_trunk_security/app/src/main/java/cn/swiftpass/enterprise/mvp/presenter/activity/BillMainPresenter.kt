package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import android.util.Log
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.bussiness.model.OrderList
import cn.swiftpass.enterprise.bussiness.model.WxCard
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.BillMainContract
import cn.swiftpass.enterprise.mvp.utils.ParamsUtils
import cn.swiftpass.enterprise.utils.DateUtil
import cn.swiftpass.enterprise.utils.JsonUtil
import cn.swiftpass.enterprise.utils.SignUtil
import cn.swiftpass.enterprise.utils.StringUtil
import com.example.common.entity.EmptyData
import com.example.common.sentry.SentryUtils.uploadNetInterfaceException
import okhttp3.Call
import java.text.ParseException

/**
 * @author lizheng.zhao
 * @date 2022/12/2
 *
 * @下次你路过
 * @人间已无我
 */
class BillMainPresenter : BillMainContract.Presenter {

    companion object {
        const val TAG = "BillMainPresenter"
    }

    private var mView: BillMainContract.View? = null


    override fun queryCardDetail(
        cardId: String?,
        cardCode: String?
    ) {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
            AppClient.queryCardDetail(
                MainApplication.getInstance().getMchId(),
                MainApplication.getInstance().getUserId().toString(),
                cardId,
                cardCode,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.queryCardDetailFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->

                            try {
                                val order = JsonUtil.jsonToBean(
                                    response.message,
                                    WxCard::class.java
                                ) as WxCard
                                view.queryCardDetailSuccess(order)
                            } catch (e: java.lang.Exception) {
                                uploadNetInterfaceException(
                                    e,
                                    "spay/queryCardDetail",
                                    "WxCard json解析异常",
                                    ""
                                )
                                view.queryCardDetailSuccess(WxCard())
                            }
                        }
                    }
                }
            )
        }
    }

    override fun queryOrderDetail(
        orderNo: String?,
        mchId: String?,
        isMark: Boolean
    ) {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
            AppClient.queryOrderDetail(
                orderNo,
                orderNo,
                orderNo,
                mchId,
                MainApplication.getInstance().getUserId().toString(),
                System.currentTimeMillis().toString(),
                isMark,
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {


                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.queryOrderDetailFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"",
                                "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                Order::class.java
                            ) as Order
                            order.add_time =
                                if (TextUtils.isEmpty(order.addTime)) 0 else order.addTime.toLong()
                            order.useId = order.userId.toString()
                            order.canAffim = order.canAffirm
                            order.setUplanDetailsBeans(order.discountDetail)
                            view.queryOrderDetailSuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun queryRefundDetail(
        orderNo: String?,
        mchId: String?
    ) {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
            AppClient.queryRefundDetail(
                orderNo,
                mchId,
                MainApplication.getInstance().getUserId().toString(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.queryRefundDetailFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"",
                                "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                Order::class.java
                            ) as Order
                            order.useId = order.userId.toString()
                            order.orderNoMch = order.orderNo
                            order.cashFeel =
                                if (TextUtils.isEmpty(order.cashFee)) 0 else order.cashFee.toLong()
                            view.queryRefundDetailSuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun querySpayOrder(
        listStr: ArrayList<String?>?,
        payTypeList: ArrayList<String?>?,
        isRefund: Int,
        page: Int,
        order: String?,
        userId: String?,
        startDate: String?,
        isLoadMore: Boolean
    ) {
        mView?.let { view ->
            if (isLoadMore) {
                view.showLoading(
                    R.string.public_data_loading,
                    ParamsConstants.COMMON_LOADING
                )
            }


            val mapParam: MutableMap<String?, Any?> = HashMap()
            mapParam[ParamsConstants.PAGE_SIZE] = "20"
            mapParam[ParamsConstants.PAGE] = page.toString()
            mapParam[ParamsConstants.MCH_ID] = MainApplication.getInstance().getMchId()

            //如果是收银员登录的同时收银员也没有账单权限
            if (MainApplication.getInstance().isAdmin(0) && MainApplication.getInstance()
                    .isOrderAuth("0")
            ) {
                mapParam[ParamsConstants.USER_ID] =
                    MainApplication.getInstance().getUserId().toString()
            }

            if (!StringUtil.isEmptyOrNull(userId)) {
                mapParam[ParamsConstants.USER_ID] = userId
            }

            if (!StringUtil.isEmptyOrNull(startDate)) {
                if (isRefund == 1) { //退款不用时分秒
                    mapParam[ParamsConstants.START_DATE] = startDate
                    mapParam[ParamsConstants.END_DATE] = startDate
                } else {
                    mapParam[ParamsConstants.START_DATE] = "$startDate 00:00:00"
                    mapParam[ParamsConstants.END_DATE] = "$startDate 23:59:59"
                }
            } else {
                if (isRefund == 1) { //退款不用时分秒
                    mapParam[ParamsConstants.START_DATE] =
                        DateUtil.formatYYMD(System.currentTimeMillis())
                    mapParam[ParamsConstants.END_DATE] =
                        DateUtil.formatYYMD(System.currentTimeMillis())
                } else {
                    mapParam[ParamsConstants.START_DATE] =
                        DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00"
                    mapParam[ParamsConstants.END_DATE] =
                        DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59"
                }
            }

            if (!StringUtil.isEmptyOrNull(order)) {
                mapParam[ParamsConstants.ORDER_NO_MCH] = order
            }

            val spayRs = System.currentTimeMillis()
            //加签名
            if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getNewSignKey())) {
                mapParam[ParamsConstants.SPAY_RS] = spayRs.toString()
                mapParam[ParamsConstants.NNS] = SignUtil.getInstance().createSign(
                    JsonUtil.jsonToMap(JsonUtil.objectToJson(mapParam)),
                    MainApplication.getInstance().getNewSignKey()
                )
            }


            if (payTypeList != null && payTypeList.size > 0) {
                mapParam[ParamsConstants.API_PROVIDER_LIST] = payTypeList
            }
            val tradeType = arrayListOf<Int>()
            val payType = arrayListOf<Int>()
            ParamsUtils.parseToJson(listStr, tradeType, payType)
            if (tradeType.size > 0) {
                mapParam[ParamsConstants.API_PROVIDER_LIST] = tradeType
            }
            if (payType.size > 0) {
                mapParam[ParamsConstants.API_PROVIDER_LIST] = payType
            }

            AppClient.querySpayOrder(
                spayRs.toString(),
                JsonUtil.mapToJsons(mapParam),
                isRefund,
                object : CallBackUtil.CallBackCommonResponse<OrderList>(OrderList()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.querySpayOrderFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val sentryUrl =
                                if (TextUtils.isEmpty(response.url)) "" else response.url.replace(
                                    MainApplication.getInstance().baseUrl,
                                    ""
                                )
                            val dynModels = JsonUtil.jsonToBean(
                                response.message,
                                OrderList::class.java
                            ) as OrderList
                            for (orderInfo in dynModels.data) {
                                if (isRefund == 0) {
                                    val tradeTimeNew = orderInfo.tradeTimeNew
                                    if (!StringUtil.isEmptyOrNull(tradeTimeNew)) {
                                        try {
                                            orderInfo.setFormatTimePay(
                                                DateUtil.formartDateYYMMDD(
                                                    tradeTimeNew
                                                )
                                            )
                                            orderInfo.setFormartYYMM(
                                                DateUtil.formartDateYYMMDDTo(
                                                    tradeTimeNew
                                                )
                                            )
                                            orderInfo.setTradeTimeNew(
                                                DateUtil.formartDateToHHMMSS(
                                                    tradeTimeNew
                                                )
                                            )
                                        } catch (e: ParseException) {
                                            uploadNetInterfaceException(
                                                e,
                                                sentryUrl,
                                                "",
                                                ""
                                            )
                                        }
                                    }
                                } else if (isRefund == 1) {
                                    val time = orderInfo.addTimeNew
                                    orderInfo.setMoney(orderInfo.refundFee)
                                    if (!StringUtil.isEmptyOrNull(time)) {
                                        try {
                                            orderInfo.setFormatRefund(
                                                DateUtil.formartDateYYMMDD(
                                                    time
                                                )
                                            )
                                            orderInfo.setFormartYYMM(
                                                DateUtil.formartDateYYMMDDTo(
                                                    time
                                                )
                                            )
                                            orderInfo.setAddTimeNew(
                                                DateUtil.formartDateToHHMMSS(
                                                    time
                                                )
                                            )
                                        } catch (e: ParseException) {
                                            uploadNetInterfaceException(
                                                e,
                                                sentryUrl,
                                                "",
                                                ""
                                            )
                                            Log.e(TAG, Log.getStackTraceString(e))
                                        }
                                    }
                                } else {
                                    val useTimeNew = orderInfo.useTimeNew
                                    if (!StringUtil.isEmptyOrNull(useTimeNew)) {
                                        try {
                                            orderInfo.setFromatCard(
                                                DateUtil.formartDateYYMMDD(
                                                    useTimeNew
                                                )
                                            )
                                            orderInfo.setFormartYYMM(
                                                DateUtil.formartDateYYMMDDTo(
                                                    useTimeNew
                                                )
                                            )
                                            orderInfo.setUseTimeNew(
                                                DateUtil.formartDateToHHMMSS(
                                                    useTimeNew
                                                )
                                            )
                                        } catch (e: ParseException) {
                                            uploadNetInterfaceException(
                                                e,
                                                sentryUrl,
                                                "",
                                                ""
                                            )
                                            // TODO Auto-generated catch block
                                            Log.e(TAG, Log.getStackTraceString(e))
                                        }
                                    }
                                }
                            }
                            try {
                                val reqFeqTime: Int =
                                    if (TextUtils.isEmpty(dynModels.reqFeqTime)) 0 else dynModels.reqFeqTime.toInt()
                                if (dynModels.data.size > 0 && dynModels.data[0] != null) {
                                    dynModels.data[0].setReqFeqTime(reqFeqTime)
                                } else {
                                    if (reqFeqTime > 0) {
                                        uploadNetInterfaceException(
                                            Exception("时间错误 reqFeqTime > 0"),
                                            sentryUrl,
                                            "reqFeqTime=$reqFeqTime",
                                            ""
                                        )
                                        view.querySpayOrderFailed("reqFeqTime=$reqFeqTime")
                                    }
                                }
                            } catch (e: Exception) {
                                uploadNetInterfaceException(
                                    e,
                                    sentryUrl,
                                    "",
                                    ""
                                )
                            }
                            view.querySpayOrderSuccess(dynModels.data, isLoadMore, page)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: BillMainContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}