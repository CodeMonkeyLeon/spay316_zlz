package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-31
 * Time: 下午2:29
 * To change this template use File | Settings | File Templates.
 */
public class CouponOrderInfo implements Serializable {
    public int beforeMoney; //原价
    public int discountMoney; //折扣金额
    public int afterMoney;//优惠后

    public String useId;//优惠券唯一标识  领用表ID

}
