package cn.swiftpass.enterprise.ui.activity.pos;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import cn.newpost.tech.blue.socket.PosBlueSocket;
import cn.newpost.tech.blue.socket.StatusCode;
import cn.newpost.tech.blue.socket.entity.Order;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.enums.PayType;
import cn.swiftpass.enterprise.bussiness.logica.bluetooth.BluetoothManager;
import cn.swiftpass.enterprise.bussiness.logica.coupon.CouponManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.BlueDevice;
import cn.swiftpass.enterprise.bussiness.model.PosBluetoothResult;
import cn.swiftpass.enterprise.ui.activity.OrderDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.Utils;

/**
 * pos交易
 * User: Alan
 * Date: 14-2-25
 * Time: 下午7:57
 * To change this template use File | Settings | File Templates.
 */
public class POSPayActivity extends TemplateActivity
{
    //private MyBluetoothAdapter myAdapter;
    private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    
    private ImageView imageView;
    
    private String posName = "pos6210";
    
    private List<BlueDevice> devices;
    
    PosBlueSocket posBlueSocket;
    
    private Order order;
    
    private String money;
    
    private String orderNo;
    
    private boolean isPaying = false;
    
    private Context mContext;
    
    public static final int REQUEST_CODE_BLUETOOTH = 0x1001;
    
    private String bmpFilePath = null; //bmp文件路径
    
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_pos_pay);
        
        if (mBluetoothAdapter == null)
        {
            showToastInfo("此设备不支持蓝牙。");
            finish();
            return;
        }
        
        Intent it = getIntent();
        money = it.getStringExtra("money");
        orderNo = it.getStringExtra("orderNo");
        
        setBluetool();
        
        wiateAnima();
        
        initData();
    }
    
    private void wiateAnima()
    {
        Object localObject = new TranslateAnimation(5.0F, 170.0F, 0.0F, 0.0F);
        ((Animation)localObject).setDuration(4000L);
        ((Animation)localObject).setRepeatCount(-1);
        imageView = getViewById(R.id.iv_card);
        imageView.setAnimation((Animation)localObject);
        ((Animation)localObject).startNow();
    }
    
    private void stopAnima()
    {
        Animation localAnimation = AnimationUtils.loadAnimation(POSPayActivity.this, R.anim.push_left_in);
        imageView.startAnimation(localAnimation);
    }
    
    /***
     * 初始化数据
     */
    private void initData()
    {
        
        UINotifyListener<List<BlueDevice>> listener = new UINotifyListener<List<BlueDevice>>()
        {
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                if (object != null)
                    showToastInfo(object.toString());
            }
            
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                // showLoading("请稍候...");
            }
            
            @Override
            public void onSucceed(List<BlueDevice> result)
            {
                super.onSucceed(result);
                // dismissLoading();
                if (result.size() == 0)
                {
                    showToastInfo("请先打开蓝牙并匹配蓝牙设备。");
                    return;
                }
                devices = result;
                // myAdapter.setList(result);
                //自动连接
                autoContention();
                
            }
        };
        if (mBluetoothAdapter != null)
            BluetoothManager.getInstance().searchBlueDevice(mBluetoothAdapter.getBondedDevices(), listener);
    }
    
    private void autoContention()
    {
        for (BlueDevice bd : devices)
        {
            if (bd.getDeviceName().equals(posName))
            {
                String address = bd.getAddress();
                BluetoothDevice btDev = mBluetoothAdapter.getRemoteDevice(address);
                if (btDev.getBondState() == BluetoothDevice.BOND_BONDED)
                {//已配对     是否能做到不点击直接连接
                    if (!address.equals("null"))
                    {
                        BluetoothManager.getInstance().connectionBluetoothPos(btDev, connectionListener);
                    }
                }
                break;
            }
        }
        
    }
    
    private void setBluetool()
    {
        //蓝牙未打开，打开蓝牙
        if (!mBluetoothAdapter.isEnabled())
        {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_CODE_BLUETOOTH);
        }
        else
        {
            //showToastInfo("蓝牙已经打开");
        }
    }
    
    private UINotifyListener<PosBluetoothResult> connectionListener = new UINotifyListener<PosBluetoothResult>()
    {
        @Override
        public void onError(Object object)
        {
            super.onError(object);
            if (object != null)
                showToastInfo(object.toString());
        }
        
        @Override
        public void onPreExecute()
        {
            super.onPreExecute();
            showLoading("请稍候,正在连接POS机...");
        }
        
        @Override
        public void onPostExecute()
        {
            super.onPostExecute();
            dismissLoading();
            
        }
        
        /***
         *  连接成功后直接支付
         * @param result
         */
        @Override
        public void onSucceed(PosBluetoothResult result)
        {
            super.onSucceed(result);
            posBlueSocket = result.posBlueSocket;
            switch (result.state)
            {
                case StatusCode.SUCCESS:// 建立RFCOMM socket和HTTP socket 成功
                case StatusCode.RFCOMM_CONNECTED:// 连接成功
                    order = new Order(); //下单
                    order.setFunID(Order.OBTAIN_SN_FUNC);
                    final Order o = posBlueSocket.writeToPOS(order);
                    CouponManager.getInstance().saveSNnumber(o.getTerminalID());
                    OrderManager.getInstance().getOrderSignaturekey(o.getTerminalID(),
                        null,
                        new UINotifyListener<String>()
                        {
                            
                            @Override
                            public void onSucceed(String result)
                            {
                                super.onSucceed(result);
                                if (result != null)
                                {
                                    String posSn = CouponManager.getInstance().getISNnumber();
                                    if (!o.getTerminalID().equals(posSn))
                                    {
                                        
                                    }
                                    order = new Order();
                                    order.setFunID(Order.KEY_IMPORT_FUNC);
                                    order.setKey(result);
                                    Order o = posBlueSocket.writeToPOS(order);
                                    order = new Order(); //下单
                                    order.setOrderId(orderNo);
                                    order.setAmount(BluetoothManager.fillLeftWith0(money, 12)); //金额加密
                                    order.setTime(System.currentTimeMillis() + "");
                                    order.setFunID(Order.ORDER_FUNC);
                                    // order.setType(Order.ONLINE);
                                    posTransacte(posBlueSocket, order, o.getTerminalID(), result, o.getTerminalID());
                                }
                                else
                                {
                                    showToastInfo("获取服务器密钥失败");
                                    finish();
                                }
                                
                            }
                            
                        });
                    
                    break;
                case StatusCode.CONNECT_RFCOMM_ERROR:
                    posBlueSocket.close();
                    showToastInfo("连接错误，code:" + StatusCode.CONNECT_RFCOMM_ERROR + ",请检查pos是否打开");
                    finish();
                    break;
                case StatusCode.CONNECT_HTTP_ERROR:
                    posBlueSocket.close();
                    showToastInfo("连接错误，code:" + StatusCode.CONNECT_HTTP_ERROR);
                    finish();
                    break;
            
            }
        }
    };
    
    /**
     * 订单完成后显示对话框
     */
    public void showConsumeStatus()
    {
        
        StringBuffer sb = new StringBuffer();
        sb.append(String.format(getString(R.string.orders_id), order.getOrderId()));
        sb.append("\n");
        sb.append(String.format(getString(R.string.orders_amount), BluetoothManager.amountConvert(order.getAmount())));
        sb.append("\n");
        sb.append(String.format(getString(R.string.card_no_var), order.getCardNO()));
        sb.append("\n");
        long t = Utils.Long.tryParse(order.getTime(), System.currentTimeMillis());
        String s = DateUtil.formatTime(t, "yyyy-MM-dd HH:mm:ss");
        sb.append(String.format(getString(R.string.tran_time), s));
        
        //        CommonConfirmDialog.show(this, "交易成功", sb.toString(), new CommonConfirmDialog.ConfirmListener()
        //        {
        //            @Override
        //            public void ok()
        //            {
        //                isPaying = false;
        //                finish();
        //            }
        //            
        //            @Override
        //            public void cancel()
        //            {
        //                isPaying = false;
        //                finish();
        //            }
        //        }, true);
        
    }
    
    /***
     *
     * @param money
     */
    public static void startActivity(Context mContext, String money, String orderNo)
    {
        Intent it = new Intent();
        it.setClass(mContext, POSPayActivity.class);
        it.putExtra("money", money);
        it.putExtra("orderNo", orderNo);
        mContext.startActivity(it);
    }
    
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (posBlueSocket != null)
            posBlueSocket.close();
    }
    
    cn.swiftpass.enterprise.bussiness.model.Order wftOrder;
    
    /**提交WFT订单*/
    private void submitOrder(String deCode)
    {
        wftOrder = new cn.swiftpass.enterprise.bussiness.model.Order();
        // UserModel user =  LocalAccountManager.getInstance().getLoggedUser();
        wftOrder.money = Integer.parseInt(money);//单位分
        wftOrder.remark = "pos刷卡交易";
        wftOrder.orderNo = orderNo;
        
        wftOrder.add_time = System.currentTimeMillis();
        wftOrder.transactionType = PayType.PAY_POS.getValue();
        
        OrderManager.getInstance().sendOrderData(new UINotifyListener<String[]>()
        {
            @Override
            public void onError(Object object)
            {
                super.onError(object);
            }
            
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
            }
            
            @Override
            public void onSucceed(String[] result)
            {
                super.onSucceed(result);
                // showToastInfo("交易成功！");
                if (result != null)
                {
                    
                    DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
                    //                    try
                    //                    {
                    //                        //                        wftOrder.notifyTime = format.parse(result[1]);
                    //                    }
                    //                    catch (ParseException e)
                    //                    {
                    //                        e.printStackTrace();
                    //                    }
                    wftOrder.payType = PayType.PAY_POS.getValue();
                    wftOrder.posCardNo = result[0];
                    //                    wftOrder.state = OrderStatusEnum.PAY_SUCCESS.getValue();
                    OrderDetailsActivity.startActivity(mContext, wftOrder);
                }
                else
                {
                    showToastInfo("交易失败,请重新付款");
                }
                finish();
            }
            
            @Override
            public void onPostExecute()
            {
                super.onPostExecute();
                
            }
        }, wftOrder, bmpFilePath, deCode);
    }
    
    /**进行交易*/
    private void posTransacte(final PosBlueSocket blueSocket, final Order order, final String encKey,
        final String masterKey, final String terminalID)
    {
        BluetoothManager.getInstance().posTransacte(blueSocket, order, new UINotifyListener<Order>()
        {
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                if (object != null)
                {
                    showToastInfo(object.toString());
                }
                //  if(posBlueSocket != null)posBlueSocket.close();
            }
            
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                //showLoading("请稍候...");
                titleBar.setRightLodingVisible(true);
            }
            
            @Override
            public void onPostExecute()
            {
                super.onPostExecute();
                // dismissLoading();
                titleBar.setRightLodingVisible(false, false);
            }
            
            @Override
            public void onSucceed(Order result)
            {
                super.onSucceed(result);
                stopAnima();
                switch (result.getStatus())
                {
                    case StatusCode.ERROR://交易失败
                    {
                        showToastInfo("连接POS端异常");
                        break;
                    }
                    case StatusCode.CONNECT_RFCOMM_ERROR://订单不存在
                    {
                        showToastInfo("订单不存在");
                        break;
                    }
                    case StatusCode.CANCEL://交易取消
                    {
                        showToastInfo("交易被取消");
                        break;
                    }
                    case StatusCode.SUCCESS://交易完成
                    {
                        if (result != null)
                        {
                            //System.out.println("交易前："+order.getOrderId() +" 交易后:"+result.getOrderId());
                            //if(order.getOrderId().equals(result.getOrderId())){  //位数不对
                            // order.setCardNO(result.getCardNO());
                            /***
                             * 请求签名
                             */
                            Order order2 = new Order();
                            order2.setFunID(Order.GET_SIGNATURE_DATA_FUNC);
                            order2.setOrderId(orderNo);
                            Order o = blueSocket.writeToPOS(order2);
                            switch (o.getStatus())
                            {
                                case StatusCode.ERROR://交易失败
                                {
                                    showToastInfo("连接POS端异常");
                                    finish();
                                    break;
                                }
                                case StatusCode.CONNECT_RFCOMM_ERROR://订单不存在
                                {
                                    showToastInfo("订单不存在");
                                    finish();
                                    break;
                                }
                                case StatusCode.CANCEL://交易取消
                                {
                                    showToastInfo("交易被取消");
                                    finish();
                                    break;
                                }
                                case StatusCode.SUCCESS://交易完成
                                {
                                    if (o != null)
                                    {
                                        byte[] signData = o.getSignature();
                                        // 将源数据存为jbg文件
                                        String jbgName = o.getOrderId() + ".jbg";
                                        File file;
                                        try
                                        {
                                            file = new File(AppHelper.getImgCacheDir() + jbgName);
                                            // FileOutputStream fOutputStream = openFileOutput(jbgName, MODE_PRIVATE);
                                            if (!file.exists())
                                            {
                                                file.getParentFile().mkdirs();
                                                file.createNewFile();
                                            }
                                            FileOutputStream fOutputStream = new FileOutputStream(file);
                                            try
                                            {
                                                fOutputStream.write(signData);
                                                fOutputStream.close();
                                            }
                                            catch (IOException e)
                                            {
                                                e.printStackTrace();
                                            }
                                        }
                                        catch (Exception e1)
                                        {
                                            e1.printStackTrace();
                                        }
                                        //jbg文件路径
                                        String jbgFilePath = null;
                                        try
                                        {
                                            jbgFilePath = AppHelper.getImgCacheDir() + "/" + jbgName;
                                            bmpFilePath = AppHelper.getImgCacheDir() + "/" + orderNo + ".jpg";
                                        }
                                        catch (Exception e)
                                        {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }
                                        
                                        File jbgFile = new File(jbgFilePath);
                                        if (jbgFile.exists())
                                        {// 文件存在
                                         //                                            int result2 = JbigTools.jbg2bmp(jbgFilePath, bmpFilePath);//转化结果
                                        //                                            if (result2 == 0)
                                        //                                            {
                                        //                                                
                                        //                                            }
                                        }
                                        else
                                        {
                                            
                                        }
                                        
                                    }
                                    /***
                                     * 交易成功后。将数据发送到服务器，由服务器解析
                                     */
                                    isPaying = true;
                                    submitOrder(result.getEncOrderData());
                                    break;
                                }
                                case StatusCode.NO_SIGNATURE:
                                    showToastInfo("交易没有签名");
                                    finish();
                                    break;
                                case StatusCode.CONNECT_HTTP_ERROR:
                                    showToastInfo("连接服务器端异常");
                                    finish();
                                    break;
                            }
                            
                        }
                        break;
                    }
                }
                posBlueSocket.close();
            }
        });
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_pos_pay);
        titleBar.setRightButtonVisible(false);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightLayClick()
            {
                // TODO Auto-generated method stub
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                // TODO Auto-generated method stub
                
            }
        });
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
        {
            if (requestCode == REQUEST_CODE_BLUETOOTH)
            {
                initData();
            }
        }
    }
    
    /**
     * 将String型格式化,比如想要将2011-11-11格式化成2011年11月11日,就StringPattern("2011-11-11",
     * "yyyy-MM-dd","yyyy年MM月dd日").
     * 
     * @param date
     *            String 想要格式化的日期
     * @param oldPattern
     *            String 想要格式化的日期的现有格式
     * @param newPattern
     *            String 想要格式化成什么格式
     * @return String
     */
    public final static String StringPattern(String date, String oldPattern, String newPattern)
    {
        String datetime = null;
        try
        {
            if (date == null || oldPattern == null || newPattern == null)
                return "";
            SimpleDateFormat sdf1 = new SimpleDateFormat(oldPattern); // 实例化模板对象
            SimpleDateFormat sdf2 = new SimpleDateFormat(newPattern); // 实例化模板对象
            Date d = null;
            try
            {
                d = sdf1.parse(date); // 将给定的字符串中的日期提取出来
                datetime = sdf2.format(d);
            }
            catch (Exception e)
            { // 如果提供的字符串格式有错误，则进行异常处理
                e.printStackTrace(); // 打印异常信息
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return datetime;
    }
}
