package cn.swiftpass.enterprise.ui.fmt;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.common.sp.PreferenceUtil;
import com.example.common.utils.PngSetting;

import java.util.ArrayList;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bean.QRCodeBean;
import cn.swiftpass.enterprise.bussiness.model.MerchantTempDataModel;
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo;
import cn.swiftpass.enterprise.bussiness.model.UserInfo;
import cn.swiftpass.enterprise.common.Constant;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.fragment.TabSettingContract;
import cn.swiftpass.enterprise.mvp.presenter.fragment.TabSettingPresenter;
import cn.swiftpass.enterprise.ui.activity.CashierManagerActivity;
import cn.swiftpass.enterprise.ui.activity.ChangePwdActivity;
import cn.swiftpass.enterprise.ui.activity.ContentTextActivity;
import cn.swiftpass.enterprise.ui.activity.SettingMoreActivity;
import cn.swiftpass.enterprise.ui.activity.SettlementListActivity;
import cn.swiftpass.enterprise.ui.activity.StaticCodeActivity;
import cn.swiftpass.enterprise.ui.activity.TerminalManagementActivity;
import cn.swiftpass.enterprise.ui.activity.UpgradeDailog;
import cn.swiftpass.enterprise.ui.activity.scan.CaptureActivity;
import cn.swiftpass.enterprise.ui.activity.scan.CodeListActivity;
import cn.swiftpass.enterprise.ui.activity.shop.ShopkeeperActivity;
import cn.swiftpass.enterprise.ui.activity.spayMainTabActivity;
import cn.swiftpass.enterprise.ui.activity.user.GoodsNameSettingActivity;
import cn.swiftpass.enterprise.ui.widget.CommonConfirmDialog;
import cn.swiftpass.enterprise.utils.KotlinUtils;
import cn.swiftpass.enterprise.utils.PngUtils;
import cn.swiftpass.enterprise.utils.interfaces.OnActivityDataToFragmentListener;
import cn.swiftpass.enterprise.utils.interfaces.OnOkClickListener;

/**
 * Created by aijingya on 2018/5/8.
 *
 * @Package cn.swiftpass.enterprise.ui.fmt
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/5/8.11:07.
 */

public class FragmentTabSetting extends BaseFragment<TabSettingContract.Presenter> implements OnActivityDataToFragmentListener, View.OnClickListener, TabSettingContract.View {
    private static final String TAG = FragmentTabSetting.class.getSimpleName();
    private spayMainTabActivity mActivity;

    private LinearLayout llShopInfo, ll_E_Card, ll_merachant_settlement, empManager, lay_scan;
    private View line_Settlement;
    private View ll_shopinfo, sum_id, empManager_id, ll_shopinfo_id;

    private SharedPreferences sp;
    private LinearLayout bodyLay, ll_static_code, lay_prize;
    private View marketing_id;
    private LinearLayout marketingLay, ll_setting_more, ll_terminal_management;
    private TextView tv_mch_name, tv_mch_id;
    private ImageView iv_title;

    private FrameLayout mFlBgMchInfo;
    private LinearLayout mLlUserInfo;
    private TextView mTvMchNameInfo;
    private TextView mTvMchIdInfo;

    private ArrayList<QRCodeBean> codes;
    private String bindUserId = "";
    private UpgradeInfo mApkDownInfo = null;

    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_setting_new, container, false);
        sp = mContext.getSharedPreferences("login", 0);
        initViews(view);
        return view;

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof spayMainTabActivity) {
            mActivity = (spayMainTabActivity) activity;
        }
    }

    @Override
    public void onResult(int requestCode, int resultCode, @Nullable Intent data) {
        //授予安装未知应用权限
        requestApkInstallPermission();
    }

    @Override
    public void onPermissionsResult(int requestCode, @Nullable String[] permissions, @Nullable int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            //拒绝
        } else {
            //允许
            requestApkInstallPermission();
        }
    }

    /**
     * 下载apk
     */
    private void downApk() {
        new UpgradeDailog(mActivity, mApkDownInfo, new UpgradeDailog.UpdateListener() {
            @Override
            public void cancel() {

            }
        }).show();
    }

    /**
     * 申请apk安装权限
     */
    private void requestApkInstallPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            boolean haveInstallPermission = getActivity().getPackageManager().canRequestPackageInstalls();
            if (haveInstallPermission) {
                //有apk安装权限
                downApk();
            } else {
                //没有apk安装权限, 需在设置页面手动开启
                KotlinUtils.INSTANCE.showMissingPermissionDialog(
                        getActivity(),
                        getString(R.string.setting_permission_apk_install_lack),
                        getStringById(R.string.title_setting),
                        getString(R.string.btnCancel),
                        new OnOkClickListener() {
                            @Override
                            public void onOkClick() {
                                Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES);
                                getActivity().startActivityForResult(intent, KotlinUtils.APP_INSTALL_REQUEST_PERMISSION_SETTING);
                            }
                        }
                );
            }
        } else {
            //有apk安装权限
            downApk();
        }
    }

    /**
     * 申请存储权限
     */
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(
                getActivity(),
                WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_DENIED
                ||
                ContextCompat.checkSelfPermission(
                        getActivity(),
                        READ_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_DENIED
        ) {
            //没有存储权限
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    getActivity(),
                    WRITE_EXTERNAL_STORAGE
            ) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(
                            getActivity(),
                            READ_EXTERNAL_STORAGE
                    )
            ) {
                //之前拒绝过，用户需要手动去设置里开启存储权限
                KotlinUtils.INSTANCE.showMissingPermissionDialog(
                        getActivity(),
                        getString(R.string.setting_permission_storage_lack),
                        getStringById(R.string.title_setting),
                        getString(R.string.btnCancel),
                        new OnOkClickListener() {
                            @Override
                            public void onOkClick() {
                                startAppSettings();
                            }
                        }
                );

            } else {
                //首次申请
                ActivityCompat.requestPermissions(
                        getActivity(),
                        new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE},
                        KotlinUtils.EXTERNAL_STORAGE_REQUEST_PERMISSION_SETTING
                );
            }

        } else {
            //有存储权限
            requestApkInstallPermission();
        }
    }

    /**
     * 修改密码
     */
    public void onChangePwd() {
        ChangePwdActivity.startActivity(mContext);
    }

    private void initViews(View view) {
        codes = new ArrayList<QRCodeBean>();
        iv_title = view.findViewById(R.id.iv_title);
        tv_mch_name = view.findViewById(R.id.tv_mch_name);
        tv_mch_id = view.findViewById(R.id.tv_mch_id);

        ll_setting_more = view.findViewById(R.id.ll_setting_more);
        ll_setting_more.setOnClickListener(this);

        ll_terminal_management = view.findViewById(R.id.ll_terminal_management);
        ll_terminal_management.setOnClickListener(this);

        sum_id = view.findViewById(R.id.sum_id);

        marketingLay = view.findViewById(R.id.marketingLay);
        lay_prize = view.findViewById(R.id.lay_prize);
        marketingLay.setOnClickListener(this);
        marketing_id = view.findViewById(R.id.marketing_id);
        lay_prize.setOnClickListener(this);

        ll_shopinfo_id = view.findViewById(R.id.ll_shopinfo_id);
        empManager_id = view.findViewById(R.id.empManager_id);

        ll_static_code = view.findViewById(R.id.ll_static_code);
        ll_static_code.setOnClickListener(this);
        lay_scan = view.findViewById(R.id.lay_scan);
        lay_scan.setOnClickListener(this);

        empManager = view.findViewById(R.id.empManager);
        empManager.setOnClickListener(this);


        ll_E_Card = view.findViewById(R.id.ll_E_Card);
        ll_E_Card.setOnClickListener(this);


        UserInfo userInfo = MainApplication.getInstance().getUserInfo();

        if (userInfo.isCardOpen == 1) { //开通电子卡的功能
            ll_E_Card.setVisibility(View.VISIBLE);
        } else {
            ll_E_Card.setVisibility(View.GONE);
        }

        line_Settlement = view.findViewById(R.id.line_Settlement);
        ll_merachant_settlement = view.findViewById(R.id.ll_merachant_settlement);
        ll_merachant_settlement.setOnClickListener(this);

        //结算功能权限属于商家，若登入账号为收银员，则隐藏该入口。
        if (MainApplication.getInstance().isAdmin(0)) {
            line_Settlement.setVisibility(View.GONE);
            ll_merachant_settlement.setVisibility(View.GONE);
        } else {
            marketing_id.setVisibility(View.GONE);
            line_Settlement.setVisibility(View.VISIBLE);
            ll_merachant_settlement.setVisibility(View.VISIBLE);
        }

        bodyLay = view.findViewById(R.id.bodyLay);
        bodyLay.setOnClickListener(this);
        StringBuilder title = new StringBuilder().append(getString(R.string.tx_welcome))
                .append(PreferenceUtil.getString("user_name", userInfo.username));


        ll_shopinfo = view.findViewById(R.id.ll_shopinfo);

        llShopInfo = view.findViewById(R.id.ll_shopinfo);


        mFlBgMchInfo = view.findViewById(R.id.id_bg_mch_info);
        mLlUserInfo = view.findViewById(R.id.id_ll_user_info);
        mTvMchNameInfo = view.findViewById(R.id.id_tv_mch_name);
        mTvMchIdInfo = view.findViewById(R.id.id_tv_mch_id);


        if (MainApplication.getInstance().isAdmin(0)) {
            //收银员
            tv_mch_name.setText(MainApplication.getInstance().getMchName());
            tv_mch_id.setText(MainApplication.getInstance().getMchId());
            iv_title.setVisibility(View.GONE);
            ll_shopinfo.setEnabled(false);
            showMchInfo(true, userInfo);
        } else {
            //商户
            ll_shopinfo.setEnabled(true);
            tv_mch_name.setText(MainApplication.getInstance().getMchName());
            tv_mch_id.setText(MainApplication.getInstance().getMchId());
            showMchInfo(false, userInfo);
        }
        llShopInfo.setOnClickListener(this);
        checkVersion(true);


        // 收银员 商户资料和收银员管理，退款管理 隐藏
        if (MainApplication.getInstance().isAdmin(0)) {
            sum_id.setVisibility(View.GONE);
            empManager.setVisibility(View.GONE);
            llShopInfo.setVisibility(View.GONE);
            bodyLay.setVisibility(View.GONE);
            ll_shopinfo.setVisibility(View.VISIBLE);

            empManager_id.setVisibility(View.GONE);
            ll_shopinfo_id.setVisibility(View.GONE);

            marketingLay.setVisibility(View.GONE);
            marketing_id.setVisibility(View.GONE);

        }

        if (PngSetting.INSTANCE.isChangePng1()) {
            initPng(view);
        }
    }


    private void showMchInfo(boolean isShow, UserInfo userInfo) {
        LinearLayout.LayoutParams userInfoLayoutParam = (LinearLayout.LayoutParams) mLlUserInfo.getLayoutParams();
        if (isShow) {
            //收银员, 展示商户信息
            mFlBgMchInfo.setVisibility(View.VISIBLE);
            userInfoLayoutParam.topMargin = 0;
            mLlUserInfo.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_setting_me_2));
            String realName = "Hi, " + userInfo.realname;
            String userName = getString(R.string.string_login_account) + ": " + PreferenceUtil.getString("user_name",
                    userInfo.username);
            mTvMchNameInfo.setText(realName);
            mTvMchIdInfo.setText(userName);
        } else {
            //商户, 隐藏商户信息
            mFlBgMchInfo.setVisibility(View.GONE);
            userInfoLayoutParam.topMargin = KotlinUtils.INSTANCE.dp2px(15f, getActivity());
            mLlUserInfo.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_setting_me));
        }


        mLlUserInfo.setLayoutParams(userInfoLayoutParam);
    }


    private void initPng(View v) {
        ImageView imgMerchant = v.findViewById(R.id.id_img_me_merchant);
        ImageView imgCashier = v.findViewById(R.id.id_img_me_cashier);
        ImageView imgQrCode = v.findViewById(R.id.id_img_me_qrcode);
        ImageView imgVip = v.findViewById(R.id.id_img_me_vip);
        ImageView imgSettlement = v.findViewById(R.id.id_img_me_settlement);
        ImageView imgMarketing = v.findViewById(R.id.id_img_marketing);
        ImageView imgSpeaker = v.findViewById(R.id.id_img_speaker);
        ImageView imgSetting = v.findViewById(R.id.id_img_setting);


        PngUtils.INSTANCE.setPng1(getActivity(), imgMerchant, R.drawable.icon_me_merchant);
        PngUtils.INSTANCE.setPng1(getActivity(), imgCashier, R.drawable.icon_me_cashier);
        PngUtils.INSTANCE.setPng1(getActivity(), imgQrCode, R.drawable.icon_me_qrcode);
        PngUtils.INSTANCE.setPng1(getActivity(), imgVip, R.drawable.icon_me_vip);
        PngUtils.INSTANCE.setPng1(getActivity(), imgSettlement, R.drawable.icon_me_settlement);
        PngUtils.INSTANCE.setPng1(getActivity(), imgMarketing, R.drawable.icon_my_marketing_03);
        PngUtils.INSTANCE.setPng1(getActivity(), imgSpeaker, R.drawable.icon_me_speaker);
        PngUtils.INSTANCE.setPng1(getActivity(), imgSetting, R.drawable.icon_me_setting);
    }

    @Override
    public void getVersionCodeSuccess(@Nullable UpgradeInfo result, boolean isDisplayIcon) {
        if (null != result) {
            if (isDisplayIcon) {
                //upVerInfo = result;
                //                        ivNewVersion.setVisibility(View.VISIBLE);
            } else {
                showUpgradeInfoDialog(result, new ComDialogListener(result));
            }
        } else {
            if (!isDisplayIcon) {
                showToastInfo(R.string.show_no_version);
            }
        }
    }

    @Override
    public void getVersionCodeFailed(@Nullable Object error) {

    }

    /**
     * 检查版本显示提示图标
     */
    public void checkVersion(final boolean isDisplayIcon) {
        // 不用太频繁的检测升级 所以使用时间间隔区分
        if (mPresenter != null) {
            mPresenter.getVersionCode(isDisplayIcon);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //设置
            case R.id.ll_setting_more:
                showPage(SettingMoreActivity.class);
                break;

            //固定二维码收款
            case R.id.ll_static_code:
                getData();
                break;
            // 扫一扫退款
            case R.id.lay_scan:
                CaptureActivity.startActivity(mContext, Constant.PAY_TYPE_REFUND);
                break;

            //设备管理
            case R.id.ll_terminal_management:
                showPage(TerminalManagementActivity.class);
                break;

            case R.id.bodyLay://修改商品名称
                showPage(GoodsNameSettingActivity.class);
                break;
            case R.id.ll_changePwd:
                onChangePwd();
                break;

            case R.id.ll_E_Card://点击电子卡
                //添加cookic
                String login_skey = PreferenceUtil.getString("login_skey", "");
                String login_sauthid = PreferenceUtil.getString("login_sauthid", "");
                String url = MainApplication.getInstance().getBaseUrl() + "spay/sm/memberShipCard/login";
                ContentTextActivity.startActivityWithCookie(mContext, url, R.string.E_Card, login_skey, login_sauthid);
                break;
            case R.id.ll_merachant_settlement://结算功能权限属于商家，若登入账号为收银员，则隐藏该入口。
                //进入结算页面
                showPage(SettlementListActivity.class);
                break;
            case R.id.empManager:
                showPage(CashierManagerActivity.class);
                break;
            case R.id.ll_shopinfo:
                if (mPresenter != null) {
                    mPresenter.queryMerchantDataByTel();
                }
                break;
        }
    }


    @Override
    public void queryMerchantDataByTelSuccess(@NonNull MerchantTempDataModel result) {
        if (null != result) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("merchantData", result);
            showPage(ShopkeeperActivity.class, bundle);
        }
    }

    @Override
    public void queryMerchantDataByTelFailed(@Nullable Object error) {
        if (error != null) {
            toastDialog(mActivity, String.valueOf(error), null);
        }
    }


    @Override
    public void getCodeListSuccess(@NonNull ArrayList<QRCodeBean> result) {
        if (result.size() != 0) {
            codes.addAll(result);
            /**
             * size =1 跳转到详情页
             * size >1 跳转到列表页
             */
            if (result.size() == 1) {
                go2StaticCodeActivity(result.get(0));
            } else {
                go2CodeListActivity(result);
            }
        } else {
            go2CodeListActivity(result);
        }
    }

    @Override
    public void getCodeListFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }
        if (error != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    toastDialog(getActivity(), error.toString(), null);
                }
            });
        }
    }

    private void getData() {
        codes.clear();
        if (MainApplication.getInstance().isAdmin(0)) {
            if (MainApplication.getInstance().getUserId() > 0) {
                bindUserId = MainApplication.getInstance().getUserId() + "";
            } else {
                bindUserId = MainApplication.getInstance().getUserId() + "";
            }
        }
        if (mPresenter != null) {
            mPresenter.getCodeList(1, bindUserId);
        }
    }

    /**
     * 跳转到固码页面
     *
     * @param qrCodeBean
     */
    private void go2StaticCodeActivity(QRCodeBean qrCodeBean) {
        Intent intent = new Intent(getActivity(), StaticCodeActivity.class);
        qrCodeBean.switch_type = 2;
        intent.putExtra("QRCodeBean", qrCodeBean);
        this.startActivity(intent);
    }

    /**
     * 跳转到二维码列表页面
     */
    private void go2CodeListActivity(ArrayList<QRCodeBean> data) {
        CodeListActivity.startCodeListActivity(getActivity(), data);
    }

    public ArrayList<QRCodeBean> getCodes() {
        return codes;
    }

    @Override
    protected boolean isLoginRequired() {
        return false;
    }

    @Override
    protected TabSettingContract.Presenter createPresenter() {
        return new TabSettingPresenter();
    }

    /**
     * 升级点击事件
     */
    class ComDialogListener implements CommonConfirmDialog.ConfirmListener {
        private UpgradeInfo result;

        public ComDialogListener(UpgradeInfo result) {
            this.result = result;
            mApkDownInfo = result;
        }

        @Override
        public void ok() {
            requestStoragePermission();
        }

        @Override
        public void cancel() {
            // LocalAccountManager.getInstance().saveCheckVersionTime();

        }
    }
}
