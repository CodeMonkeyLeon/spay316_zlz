package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.OrderSearchResult
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @我来到这个世界
 * @为了看看太阳和蓝色的地平线
 */
class OrderStreamContract {


    interface View : BaseView {

        fun queryOrderDataSuccess(response: OrderSearchResult?)

        fun queryOrderDataFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {

        fun queryOrderData(
            orderNo: String?,
            currentPage: Int,
            state: Int,
            time: String?,
            orderNoMch: String?,
            isLoading: Boolean
        )
    }


}