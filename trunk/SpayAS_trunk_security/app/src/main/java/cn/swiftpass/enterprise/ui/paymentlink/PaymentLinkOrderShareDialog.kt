package cn.swiftpass.enterprise.ui.paymentlink

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.ui.fmt.BaseDialogFragment
import cn.swiftpass.enterprise.ui.paymentlink.adapter.PaymentLinkOrderShareAdapter
import cn.swiftpass.enterprise.ui.paymentlink.entity.OrderShareEntity
import cn.swiftpass.enterprise.ui.paymentlink.interfaces.OnPaymentLinkShareTypeClickListener

class PaymentLinkOrderShareDialog : BaseDialogFragment() {


    override fun getLayoutID() = R.layout.dialog_payment_link_order_share


    private var mOnItemClickListener: OnPaymentLinkShareTypeClickListener? = null

    fun setOnItemClickListener(listener: OnPaymentLinkShareTypeClickListener?) {
        mOnItemClickListener = listener
    }


    companion object {
        fun getInstance() = PaymentLinkOrderShareDialog()
    }


    override fun init(view: View?, savedInstanceState: Bundle?) {
        view?.let {
            initRecyclerView(it)
            val btnCancel = it.findViewById<TextView>(R.id.tv_select_cancel)
            btnCancel.setOnClickListener {
                dismiss()
            }
        }
    }


    private fun initRecyclerView(view: View) {
        val recyclerView = view.findViewById<RecyclerView>(R.id.id_recycler_view)
        val layoutManager = GridLayoutManager(mContext, 3) // 列数为2
        recyclerView.layoutManager = layoutManager

        val adapter = PaymentLinkOrderShareAdapter(mContext) // 自定义Adapter
        adapter.setData.invoke(setUIData())
        adapter.setOnPaymentLinkShareTypeClickListener(object :
            OnPaymentLinkShareTypeClickListener {
            override fun onChoose(type: Int) {
                mOnItemClickListener?.run {
                    onChoose(type)
                }
                dismiss()
            }
        })
        recyclerView.adapter = adapter
    }


    private fun setUIData(): ArrayList<OrderShareEntity> {
        val list = ArrayList<OrderShareEntity>()
        list.add(
            OrderShareEntity(
                R.drawable.icon_link_share_email,
                getString(R.string.payment_link_send_email),
                OrderShareEntity.TYPE_EMAIL
            )
        )
        list.add(
            OrderShareEntity(
                R.drawable.icon_link_share_message,
                getString(R.string.payment_link_share_message),
                OrderShareEntity.TYPE_MESSAGE
            )
        )
        list.add(
            OrderShareEntity(
                R.drawable.icon_link_share_link,
                getString(R.string.payment_link_share_copy_payment_link),
                OrderShareEntity.TYPE_LINK
            )
        )
//        list.add(
//            OrderShareEntity(
//                R.drawable.icon_link_share_wechat,
//                getString(R.string.string_payment_share_we_chat),
//                OrderShareEntity.TYPE_WE_CHAT
//            )
//        )
//        list.add(
//            OrderShareEntity(
//                R.drawable.icon_link_share_wecome,
//                getString(R.string.string_payment_share_we_come),
//                OrderShareEntity.TYPE_WE_COME
//            )
//        )
        list.add(
            OrderShareEntity(
                R.drawable.icon_link_share_more,
                getString(R.string.payment_link_share_to_app),
                OrderShareEntity.TYPE_MORE
            )
        )



        return list
    }


}