package com.example.common.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.RectF
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import com.example.common.OnSwitchButtonChangeListener
import com.example.common.R

/**
 * 开关View
 */
class SwitchButton : View {

    private var mContext: Context? = null
    private var mAttrs: AttributeSet? = null
    private var mDef: Int = 0

    private var mOpenColor = Color.parseColor("#23A2F5")
    private var mHeight = 0
    private var mWidth = 0
    private var mHeightWidthRatio = 0.65f

    private var mIsOpened = false

    private var mCanClick = true


    constructor(context: Context) : super(context) {
        mContext = context
    }

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        mContext = context
        mAttrs = attributeSet
        initConfig.invoke()
    }

    constructor(context: Context, attributeSet: AttributeSet, def: Int) : super(
        context,
        attributeSet,
        def
    ) {
        mContext = context
        mAttrs = attributeSet
        mDef = def
        initConfig.invoke()
    }


    private var mOnSwitchButtonChangeListener: OnSwitchButtonChangeListener? = null

    fun setOnStatusChangeListener(listener: OnSwitchButtonChangeListener?) {
        mOnSwitchButtonChangeListener = listener
    }


    private val CIRCLE_MARGIN = 6

    private val COLOR_CLOSE by lazy {
        Color.parseColor("#ECECEC")
    }

    private val mBackgroundPaint by lazy {
        Paint().apply {
            //实心
            style = Paint.Style.FILL
            //抗锯齿
            isAntiAlias = true
        }
    }

    private val mCirclePaint by lazy {
        Paint().apply {
            color = Color.WHITE
            //抗锯齿
            isAntiAlias = true
            setShadowLayer(2f, -3f, 3f, Color.parseColor("#40000000"))
        }
    }

    //圆的半径
    private var mCircleR = 0f

    //总平移距离
    private var mTotalOffset = 0f

    //每次平移距离比例
    private var mPerOffsetRatio = 0f


    private val mBgRectF by lazy {
        val rect = Rect(0, 0, width, height)
        RectF(rect)
    }


    private val initConfig: () -> Unit = {
        mContext?.run {
            mAttrs?.run {
                val typedArray = obtainStyledAttributes(this, R.styleable.SwitchButton)
                mOpenColor = typedArray.getColor(
                    R.styleable.SwitchButton_openColor,
                    resources.getColor(R.color.color_app_theme)
                )
                mHeightWidthRatio = typedArray.getDimension(
                    R.styleable.SwitchButton_heightWidthRatio,
                    0.65f
                )
                mIsOpened = typedArray.getBoolean(
                    R.styleable.SwitchButton_isOpen,
                    false
                )
                mCanClick = typedArray.getBoolean(
                    R.styleable.SwitchButton_canClick,
                    true
                )
                //程序在运行时维护了一个 TypedArray的池，池中默认大小为5。
                // 程序调用时，会向该池中请求一个实例，
                // 用完之后，调用 recycle() 方法来释放该实例，从而使其可被其他模块复用。
                typedArray.recycle()
            }
        }
    }


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        mWidth = MeasureSpec.getSize(widthMeasureSpec)
        mHeight = (mHeightWidthRatio * mWidth).toInt()
        mCircleR = mHeight / 2f - CIRCLE_MARGIN
        mTotalOffset = mWidth - 2 * (CIRCLE_MARGIN + mCircleR)
        setMeasuredDimension(mWidth, mHeight)
    }


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.run {
            mBackgroundPaint.color = if (mIsOpened) mOpenColor else COLOR_CLOSE
            drawRoundRect(mBgRectF, mHeight / 2f, mHeight / 2f, mBackgroundPaint)

            //以下save和restore很重要，确保动画在中间一层
            save()
            mPerOffsetRatio = if (mPerOffsetRatio - 0.1f > 0) mPerOffsetRatio - 0.1f else 0f
            val realOffset =
                mTotalOffset * (if (mIsOpened) 1 - mPerOffsetRatio else mPerOffsetRatio)
            translate(realOffset, 0f)
            drawCircle(mHeight / 2f, mHeight / 2f, mCircleR, mCirclePaint)
            restore()


            if (mPerOffsetRatio > 0f) {
                invalidate()
            }
        }
    }


    /**
     * 设置开关状态 --- 无动画效果
     */
    fun open(isOpen: Boolean) {
        if (mIsOpened != isOpen) {
            mIsOpened = isOpen
            mPerOffsetRatio = 1f
            Log.i("SwitchButton", "isOpen set: $mIsOpened")
            invalidate()
        }
    }

    /**
     * 设置开关状态 --- 无动画效果
     */
    fun openWithoutAnim(isOpen: Boolean) {
        if (mIsOpened != isOpen) {
            mIsOpened = isOpen
            Log.i("SwitchButton", "isOpen set no anim: $mIsOpened")
            invalidate()
        }
    }


    fun isOpen() = mIsOpened


    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (mCanClick) {
            event?.run {
                when (action) {
                    MotionEvent.ACTION_DOWN -> {
                        return true
                    }

                    MotionEvent.ACTION_UP -> {
                        mPerOffsetRatio = 1f
                        mIsOpened = !mIsOpened
                        mOnSwitchButtonChangeListener?.run {
                            onChange(mIsOpened)
                        }
                        Log.i("SwitchButton", "isOpen: $mIsOpened")
                        invalidate()
                    }
                }
            }
        }
        return super.onTouchEvent(event)
    }
}