package cn.swiftpass.enterprise.ui.widget.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.KeyboardShortcutGroup;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.PreferenceUtil;

/**
 * Created by aijingya on 2018/11/30.
 *
 * @Package cn.swiftpass.enterprise.ui.widget.dialog
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/11/30.14:42.
 */

public class ChangePreauthDialog extends Dialog {
    private static final String TAG = ChangePreauthDialog.class.getSimpleName();

    private TextView tv_message;
    private LinearLayout ll_select_item1,ll_select_item2;
    private TextView tv_select_01,tv_select_02;
    private ImageView iv_select_01,iv_select_02;
    private TextView confirm,cancel;

    private Context mContext;
    private View.OnClickListener confirmButtonListener;
    private String selectType;
    private PageTypeEnum PageType; //是收款页面还是账单页面
    public enum PageTypeEnum {
        PAY,//收款页面
        BILL;//账单页面
    }

    public ChangePreauthDialog(Activity context ,PageTypeEnum showPage ,View.OnClickListener ButtonListener) {
        super(context, R.style.simpleDialog);
        confirmButtonListener = ButtonListener;
        mContext = context;
        PageType = showPage;
        init(context);
        setCancelable(true);
    }

    private void init(Context context) {
        View rootView = View.inflate(context, R.layout.dialog_choose_pre_auth_layout, null);
        setContentView(rootView);

        tv_message = (TextView)rootView.findViewById(R.id.tv_message);

        ll_select_item1 = (LinearLayout)rootView.findViewById(R.id.ll_select_item1);
        ll_select_item2 = (LinearLayout)rootView.findViewById(R.id.ll_select_item2);

        tv_select_01 = (TextView) rootView.findViewById(R.id.tv_select_01);
        tv_select_02 = (TextView) rootView.findViewById(R.id.tv_select_02);

        iv_select_01 = (ImageView) rootView.findViewById(R.id.iv_select_01);
        iv_select_02 = (ImageView) rootView.findViewById(R.id.iv_select_02);

        confirm = rootView.findViewById(R.id.confirm);
        cancel = rootView.findViewById(R.id.cancel);

        //读取上一次选择的是什么状态，然后根据状态设置title，默认是sale
        if(PageType == PageTypeEnum.PAY){ //收款页面
            selectType = PreferenceUtil.getString("choose_pay_or_pre_auth", "sale");
        }else if(PageType == PageTypeEnum.BILL){ //账单页面
            selectType = PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth", "sale");
        }

        if(TextUtils.equals(selectType,"sale")){//如果选择的是sale
            iv_select_01.setSelected(true);
            iv_select_02.setSelected(false);
        }else if(TextUtils.equals(selectType,"pre_auth")){//如果选择的是预授权pre_auth
            iv_select_01.setSelected(false);
            iv_select_02.setSelected(true);
        }

        confirm.setOnClickListener(confirmButtonListener);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        ll_select_item1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_select_01.setSelected(true);
                iv_select_02.setSelected(false);
                selectType = "sale";
            }
        });

        ll_select_item2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_select_01.setSelected(false);
                iv_select_02.setSelected(true);
                selectType = "pre_auth";
            }
        });
    }

    public void  setMessage(String message){
        if(!TextUtils.isEmpty(message)){
            tv_message.setText(message);
        }else{
            Toast.makeText(mContext,"with no message",Toast.LENGTH_SHORT).show();
        }
    }

    public String getSelectType(){
        return selectType;
    }

}
