package cn.swiftpass.enterprise.newspay.receipt.view;


import butterknife.BindView;
import cn.swiftpass.enterprise.R;
import cn.swiftpass.enterprise.base.BaseFragment;
import cn.swiftpass.enterprise.widget.Toolbar;


public class ReceiptFragment extends BaseFragment {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_receipt;
    }

    @Override
    protected void initView() {
        mToolbar.setCenterTitle(getString(R.string.title_pay), 0);
    }
}
