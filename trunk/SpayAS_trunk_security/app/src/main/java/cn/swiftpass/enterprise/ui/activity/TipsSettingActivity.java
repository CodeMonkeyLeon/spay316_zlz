package cn.swiftpass.enterprise.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.bussiness.model.TipsBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.TipsSettingContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.TipsSettingPresenter;
import cn.swiftpass.enterprise.ui.adapter.TipsRecycleviewAdapter;
import cn.swiftpass.enterprise.ui.widget.TitleBar;

/**
 * Created by aijingya on 2019/7/2.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(小费展示的界面)
 * @date 2019/7/2.15:24.
 */
public class TipsSettingActivity extends BaseActivity<TipsSettingContract.Presenter> implements TipsSettingContract.View {

    private RecyclerView RecyclerView_tips_setting;
    private List<TipsBean> TipsAdapterBeanList = new ArrayList<>();
    private TipsBean bean1, bean2, bean3;

    @Override
    protected TipsSettingContract.Presenter createPresenter() {
        return new TipsSettingPresenter();
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips_setting);

        initView();
        //网络请求获取小费配置信息
        getTipsSettings();
    }

    public void initView() {
        RecyclerView_tips_setting = findViewById(R.id.RecyclerView_tips_setting);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        RecyclerView_tips_setting.removeAllViews();
        TipsAdapterBeanList.clear();

        //网络请求获取小费配置信息
        getTipsSettings();

    }

    public void initData(List<TipsBean> TipsList) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        RecyclerView_tips_setting.setLayoutManager(layoutManager);
        TipsAdapterBeanList.addAll(TipsList);
        bean1 = TipsList.get(0);
        bean1.setLabel(getStringById(R.string.tips_option1));
        bean2 = TipsList.get(1);
        bean2.setLabel(getStringById(R.string.tips_option2));
        bean3 = TipsList.get(2);
        bean3.setLabel(getStringById(R.string.tips_option3));
        RecyclerView_tips_setting.setAdapter(new TipsRecycleviewAdapter(this, TipsAdapterBeanList));
    }


    @Override
    public void getTipsSettingSuccess(@NonNull ArrayList<TipsBean> response) {
        //此处更新UI，展示从服务端获取的小费比例配置
        initData(response);
    }

    @Override
    public void getTipsSettingFailed(@Nullable Object error) {
        if (error != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    toastDialog(TipsSettingActivity.this, error.toString(), null);
                }
            });
        }
    }

    public void getTipsSettings() {
        if (mPresenter != null) {
            mPresenter.getTipsSetting();
        }
    }


    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(getStringById(R.string.tips_setting));
        titleBar.setLeftButtonVisible(true);
        titleBar.setRightButtonVisible(false);
        titleBar.setRightButLayVisibleForTotal(true, getStringById(R.string.tips_edit));

        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {
            }

            @Override
            public void onRightButLayClick() {
                //点击Edit按钮之后，则跳转到小费编辑页面
                Intent it = new Intent();
                it.setClass(TipsSettingActivity.this, TipsEditActivity.class);
                it.putExtra("TipsBean1", bean1);
                it.putExtra("TipsBean2", bean2);
                it.putExtra("TipsBean3", bean3);
                TipsSettingActivity.this.startActivity(it);

            }
        });
    }


}
