package com.cxy.networklibrary.di.component;

import android.app.Application;


import com.cxy.networklibrary.CoreComponent;
import com.cxy.networklibrary.activity.ActivityLifeCallback;
import com.cxy.networklibrary.cache.CacheConfig;
import com.cxy.networklibrary.cache.CacheManager;
import com.cxy.networklibrary.di.module.ConfigModule;
import com.cxy.networklibrary.di.module.OtherModule;
import com.cxy.networklibrary.di.module.HttpModule;
import com.cxy.networklibrary.fragment.FragmentLifeCallback;
import com.cxy.networklibrary.http.HttpConfig;
import com.cxy.networklibrary.http.HttpManager;
import com.cxy.networklibrary.other.ActivityListManager;
import com.cxy.networklibrary.other.CrashDiary;
import com.cxy.networklibrary.other.OtherConfig;
import com.cxy.networklibrary.other.PermissionManager;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import okhttp3.OkHttpClient;


@Singleton
@Component(modules = {HttpModule.class, OtherModule.class, ConfigModule.class})
public interface AppComponent {

    Application application();//提供Application

    CacheConfig cacheConfig();//提供缓存配置

    HttpConfig httpConfig();//提供网络请求配置

    OtherConfig otherConfig();//提供其他模块的配置

    CacheManager cacheManager();//提供缓存管理者

    HttpManager httpManager();//提供网络请求的管理者

    CrashDiary crashDiary();//提供崩溃日志管理者

    ActivityListManager activityListManager();

   // PermissionManager permissionManager();//提供权限管理的管理者

    OkHttpClient okHttpClient();//提供OkHttpClient

    ActivityLifeCallback activityLifeCallback();

    FragmentLifeCallback fragmentLifeCallback();

    void inject(CoreComponent component);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}
