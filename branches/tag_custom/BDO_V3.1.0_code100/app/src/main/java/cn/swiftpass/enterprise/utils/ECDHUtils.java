package cn.swiftpass.enterprise.utils;

import android.nfc.Tag;
import android.os.Build;
import android.util.Log;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;

import java.io.UnsupportedEncodingException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.ECParameterSpec;
import java.security.spec.EllipticCurve;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.KeyAgreement;


/**
 * Created by aijingya on 2018/8/20.
 *
 * @Package cn.swiftpass.enterprise.utils
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/8/20.11:02.
 */

public class ECDHUtils {
    private static final String TAG = ECDHUtils.class.getSimpleName();
    private static ECDHUtils instance;
    private static KeyPairGenerator kpg;
    private static KeyFactory kf;

    private static final String ECDH = "ECDH";
    private static final String CHARSET = "UTF-8";

    private static  Provider pr;

    public static synchronized ECDHUtils getInstance() {
        if (instance == null) {
            instance = new ECDHUtils();
        }

        return instance;
    }

    private ECDHUtils() {
        try {
            Security.removeProvider("BC");
            Security.addProvider(new BouncyCastleProvider());
            pr = new BouncyCastleProvider();
            Security.addProvider(pr);
            kpg = KeyPairGenerator.getInstance(ECDH,pr);
            kf = KeyFactory.getInstance(ECDH, pr);
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG,Log.getStackTraceString(e));
            throw new RuntimeException(e);
        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
            throw new RuntimeException(e);
        }
    }


    public  String getAppPubKey() throws Exception {
        KeyPair keyPair = generateKeyPairParams("secp256k1");
        byte[] pubBytes = keyPair.getPublic().getEncoded();
        byte[] priBytes = keyPair.getPrivate().getEncoded();
        String pubKey = new String(Base64.encode(pubBytes), CHARSET);
        String priKey = new String(Base64.encode(priBytes), CHARSET);

        SharedPreUtile.saveObject(pubKey, "pubKey");
        SharedPreUtile.saveObject(priKey, "priKey");
        return pubKey;
    }

    public  String ecdhGetShareKey(String peerPublicKeyStr, String privateKeyStr)
            throws Exception {

        Security.addProvider(new BouncyCastleProvider());
        // 初始化ecdh keyFactory
        KeyFactory keyFactory = KeyFactory.getInstance(ECDH, pr);
        // 处理私钥
        byte[] priKeyBytes = Base64.decode(privateKeyStr);
        PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(
                priKeyBytes);
        PrivateKey ecPriKey = keyFactory.generatePrivate(pkcs8EncodedKeySpec);
        // 处理公钥
        byte[] pubKeyBytes = Base64.decode(peerPublicKeyStr);
        X509EncodedKeySpec pubX509 = new X509EncodedKeySpec(pubKeyBytes);
        PublicKey ecPubKey = keyFactory.generatePublic(pubX509);

        String aKeyAgreeString;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.P){
            KeyAgreement aKeyAgree = KeyAgreement.getInstance(ECDH);
            aKeyAgree.init(ecPriKey);
            aKeyAgree.doPhase(ecPubKey, true);
            aKeyAgreeString = new String(Base64.encode(aKeyAgree.generateSecret()), CHARSET);
        }else{
            KeyAgreement aKeyAgree = KeyAgreement.getInstance(ECDH,"BC");
            aKeyAgree.init(ecPriKey);
            aKeyAgree.doPhase(ecPubKey, true);
            aKeyAgreeString = new String(Base64.encode(aKeyAgree.generateSecret()), CHARSET);
        }
        return aKeyAgreeString;
    }


    public synchronized static KeyPair generateKeyPairParams(String string) throws Exception {
        // kpg.initialize(256);
        kpg.initialize(new ECGenParameterSpec(string), new SecureRandom());
        return kpg.generateKeyPair();
    }


}
