package cn.swiftpass.enterprise.ui.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.WxCard;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/***
 * 退款检查对话框
 */
public class GuideDialog extends Dialog implements View.OnClickListener
{
    
    //private Button wx_card_but;
    
    //private ConfirmListener btnListener;
    
    //private Activity mContext;
    
    //private ListView card_list;

    //private List<WxCard> vardOrders;
    
    private ImageView micropay_help_img;
    
    private Button bt_know;
    
    public GuideDialog(Activity context, String payType)
    {
        super(context);
        //mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.micropay_help_layout);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        this.setCanceledOnTouchOutside(false);
        initView(payType);
    }
    
    private void initView(String payType)
    {
        micropay_help_img = (ImageView)findViewById(R.id.micropay_help_img);
        if (payType.equals(MainApplication.PAY_QQ_NATIVE)
            || payType.equalsIgnoreCase(cn.swiftpass.enterprise.MainApplication.PAY_QQ_NATIVE1))
        {
            //手Q
            PreferenceUtil.commitString(MainApplication.getMchId() + "qqPay", "qqPay");
            micropay_help_img.setImageResource(R.drawable.picture_help_qq);
        }
        else if (payType.startsWith(MainApplication.PAY_ZFB_NATIVE) || payType.equals(MainApplication.PAY_ZFB_NATIVE1))
        {
            //支付宝扫码
            PreferenceUtil.commitString(MainApplication.getMchId() + "zfbPay", "zfbPay");
            micropay_help_img.setImageResource(R.drawable.picture_help_pay);
        }
        else if (payType.equalsIgnoreCase(MainApplication.PAY_JINGDONG_NATIVE))
        {
            PreferenceUtil.commitString(MainApplication.getMchId() + "jdPay", "jdPay");
            micropay_help_img.setImageResource(R.drawable.picture_help_jd);
        }
        else
        {
            PreferenceUtil.commitString(MainApplication.getMchId() + "wxPay", "wxPay");
            //微信扫码
            micropay_help_img.setImageResource(R.drawable.picture_help_wechat);
        }
        micropay_help_img.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                GuideDialog.this.cancel();
                GuideDialog.this.dismiss();
            }
        });
        
        bt_know = (Button)findViewById(R.id.bt_know);
        bt_know.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                GuideDialog.this.cancel();
                GuideDialog.this.dismiss();
            }
        });
    }
    
    public interface ConfirmListener
    {
        public void cancel();
    }
    
    @Override
    public void onClick(View v)
    {
        
    }
    
    class WxCardAdapter extends BaseAdapter
    {
        
        private Context context;
        
        private List<WxCard> list;
        
        public WxCardAdapter(Context context, List<WxCard> list)
        {
            this.context = context;
            this.list = list;
        }
        
        @Override
        public int getCount()
        {
            return list.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            return list.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            MyHolder myHolder;
            WxCard str = list.get(position);
            if (convertView == null)
            {
                convertView = View.inflate(context, R.layout.wx_card_list_item, null);
                myHolder = new MyHolder(convertView);
                convertView.setTag(myHolder);
            }
            else
            {
                myHolder = (MyHolder)convertView.getTag();
            }
            
            myHolder.card_title.setText(str.getTitle());
            
            if (null != str.getCardType() && !"".equals(str.getCardType()))
            {
                
                if (str.getCardType().equalsIgnoreCase(Order.WxCardType.CARSH_CARD.cardType)) // 代金券
                {
                    
                    myHolder.card_dealDetail.setText(Order.WxCardType.getCardNameForType(str.getCardType()) + ","
                        + ToastHelper.toStr(R.string.tx_reduce_cost) + ":" + MainApplication.getFeeFh() + " "
                        + DateUtil.formatMoneyUtils(str.getReduceCost()));
                }
                else if (str.getCardType().equalsIgnoreCase(Order.WxCardType.DISCOUNT_CARD.cardType))
                {
                    myHolder.card_dealDetail.setText(Order.WxCardType.getCardNameForType(str.getCardType()) + ","
                        + ToastHelper.toStr(R.string.et_discount) + ":" + str.getDiscount() + "%");
                }
                else if (str.getCardType().equalsIgnoreCase(Order.WxCardType.GIFT_CARD.cardType))
                {
                    myHolder.card_dealDetail.setText(Order.WxCardType.getCardNameForType(str.getCardType()) + ","
                        + str.getGift());
                    
                }
                else if (str.getCardType().equalsIgnoreCase(Order.WxCardType.GROUPON_CARD.cardType))
                {
                    myHolder.card_dealDetail.setText(Order.WxCardType.getCardNameForType(str.getCardType()) + ","
                        + str.getDealDetail());
                    
                }
                else
                {
                    myHolder.card_dealDetail.setText(Order.WxCardType.getCardNameForType(str.getCardType()));
                }
            }
            
            return convertView;
        }
        
    }
    
    private class MyHolder
    {
        private TextView card_title;
        
        private TextView card_dealDetail;
        
        public MyHolder(View v)
        {
            card_title = (TextView)v.findViewById(R.id.card_title);
            card_dealDetail = (TextView)v.findViewById(R.id.card_dealDetail);
        }
        
    }
    
}
