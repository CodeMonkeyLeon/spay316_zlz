package cn.swiftpass.enterprise.utils;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.Log;

import com.example.common.sentry.SentryUtils;
import com.example.common.utils.AppVersionUtils;

import java.io.File;
import java.util.UUID;

import cn.swiftpass.enterprise.MainApplication;

@SuppressLint("NewApi")
public class AppHelper {
    private static final String TAG = "AppHelper";

    public static boolean isSdcardExist() {
        return TextUtils.equals(Environment.getExternalStorageState(), Environment.MEDIA_MOUNTED);
    }

    /**
     * 如果SDcard存在，则返回SDcard上文件的目录 否则，抛异常
     */
    public static String getCacheDir() {
        String path = null;
        try {
            if (!isSdcardExist()) {
                throw new Exception("SD卡不存在");
            }
            path = Environment.getExternalStorageDirectory().getAbsolutePath();
            path += GlobalConstant.FILE_CACHE_ROOT;
            File dir = new File(path);
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    throw new Exception("创建文件缓存目录失败");
                }
            }
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return path;
    }

    /**
     * 创建缓存目录
     * 如果SDcard存在，则返回SDcard上文件的目录 否则，抛异常
     */
    public static String getAppCacheDir() {
        String path = null;
        try {
            if (!isSdcardExist()) {
                throw new Exception("SD卡不存在");
            }
            File root = new File(FileUtils.getRootPath());
            if (!root.exists()) {
                if (!root.mkdirs()) {
                    throw new Exception("创建文件缓存目录失败");
                }
            }
            path = FileUtils.getAppCache();
            File dir = new File(path);
            if (!dir.exists()) {
                if (!dir.mkdirs()) {
                    throw new Exception("创建文件缓存目录失败");
                }
            }
            String logPath = FileUtils.getAppLog();
            File logDir = new File(logPath);
            if (!logDir.exists()) {
                if (!logDir.mkdirs()) {
                    throw new Exception("创建log文件缓存目录失败");
                }
            }
            String appPath = MainApplication.getInstance().getDefaultDownloadPath();
            File f = new File(appPath);
            if (!f.exists()) {
                if (!f.mkdirs()) {
                    throw new Exception("创建APP文件缓存目录失败");
                }
            }
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return path;
    }

    /**
     * 获取数据库的存放路径，策略为 1.有卡，则主DB存放在应用程序的目录下
     * ，辅DB防止卡的目录下。 2.无卡，则主辅DB都放在应用程序目录下。
     *
     * @param mainAble 是否有卡
     * @return
     * @throws Exception
     */
    public static String getDBPath(boolean mainAble, String dbName) {
        //false)// //不允许 读取数据库文件
        if (isSdcardExist()) {
            // 有卡
            if (mainAble) {
                return MainApplication.getInstance().getDatabasePath(dbName).getAbsolutePath();
            } else {
                String path = getCacheDir();
                if (!TextUtils.isEmpty(path)) {
                    path = path + "databases/";
                    File dir = new File(path);
                    if (!dir.exists()) {
                        if (!dir.mkdirs()) {
                            return null;
                        }
                    }
                    return path + dbName;
                } else {
                    return null;
                }
            }
        } else {
            // 无卡
            return MainApplication.getInstance().getDatabasePath(dbName).getAbsolutePath();
        }
    }


    public static int getUpdateVersionCode(Context context) {
        return AppVersionUtils.INSTANCE.getUpdateVersionCode(context);
    }

    public static String getVerName(Context context) {
        return AppVersionUtils.INSTANCE.getVersionName(context);
    }

    public static int getVerCode(Context context) {
        return AppVersionUtils.INSTANCE.getVersionCode(context);
    }

    public static final String getNetState(Context context) {
        ConnectivityManager connectionManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkinfo = connectionManager.getActiveNetworkInfo();
        if (networkinfo != null && networkinfo.isConnected()) {
            return networkinfo.getTypeName();
        }
        return "no network";
    }

    public static int getAndroidSDKVersion() {
        return android.os.Build.VERSION.SDK_INT;
    }

    public static String getAndroidSDKVersionName() {
        return android.os.Build.VERSION.RELEASE;
    }


    public static String getUUID() {

        String serial;

        String m_szDevIDShort = "35" +
                Build.BOARD.length() % 10 + Build.BRAND.length() % 10 +

                Build.CPU_ABI.length() % 10 + Build.DEVICE.length() % 10 +

                Build.DISPLAY.length() % 10 + Build.HOST.length() % 10 +

                Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10 +

                Build.MODEL.length() % 10 + Build.PRODUCT.length() % 10 +

                Build.TAGS.length() % 10 + Build.TYPE.length() % 10 +

                Build.USER.length() % 10; //13 位

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                serial = android.os.Build.getSerial();
            } else {
                serial = Build.SERIAL;
            }
            //API>=29 使用serial号
            return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
        } catch (Exception exception) {
//            SentryUtils.INSTANCE.uploadTryCatchException(
//                    exception,
//                    SentryUtils.INSTANCE.getClassNameAndMethodName()
//            );
            //serial需要一个初始化
            serial = "serial"; // 随便一个初始化
        }
        //使用硬件信息拼凑出来的15位号码
        return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
    }


    /**
     * 手机品牌跟
     */
    public static String getBrandAndModel() {
        return Build.BRAND + " " + Build.MODEL;
    }

    /**
     * 实现文本复制功能
     *
     * @param text
     */
    public static void copy(String text, Context context) {
        // 得到剪贴板管理器
        ClipboardManager cmb = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        cmb.setText(text);
    }

    /**
     * 复制文本到剪切板
     *
     * @param context
     * @param text
     */
    public static void setClipboardText(Context context, String text) {
        try {
            ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData cd = ClipData.newPlainText("text", text);
            cm.setPrimaryClip(cd);
        } catch (Throwable e) {
            SentryUtils.INSTANCE.uploadTryCatchThrowable(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            //e.printStackTrace();
        }
    }

    /**
     * 实现粘贴功能
     *
     * @param context
     * @return
     */
    @SuppressWarnings("deprecation")
    public static String paste(Context context) {
        // 得到剪贴板管理器
        ClipboardManager cmb = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        return cmb.getText().toString().trim();
    }

    /**
     * 震动
     *
     * @param context
     */
    public static void execVibrator(Context context) {
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = {0, 10, 20, 30}; // 停止 开启 停止 开启
        vibrator.vibrate(pattern, -1); //重复两次上面的pattern 如果只想震动一次，index设为
    }


}
