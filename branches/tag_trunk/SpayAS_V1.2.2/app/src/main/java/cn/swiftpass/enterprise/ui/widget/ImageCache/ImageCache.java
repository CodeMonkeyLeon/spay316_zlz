/**
 * 
 */
package cn.swiftpass.enterprise.ui.widget.ImageCache;

import java.util.concurrent.Executor;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.support.v4.util.LruCache;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.NotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;

/**
 * 图片缓存类
 * @author huangdonghua
 *
 */
abstract public class ImageCache<T>
{
    protected Context mContext;
    
    /**图片缓存对象*/
    private LruCache<T, Bitmap> mCache;
    
    /**线程执行者*/
    
    private Executor mExecutor;
    
    public ImageCache(Context aContext)
    {
        mContext = aContext;
        final int memClass = ((ActivityManager)mContext.getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
        final int cacheSize = 1024 * 1024 * memClass / 8;//设定缓存大小为可用内存的1/8
        mCache = new LruCache<T, Bitmap>(cacheSize)
        {
            
            @Override
            protected void entryRemoved(boolean evicted, T key, Bitmap oldValue, Bitmap newValue)
            {
                // TODO Auto-generated method stub
                super.entryRemoved(evicted, key, oldValue, newValue);
                //oldValue.recycle();
            }
            
            @Override
            protected int sizeOf(T key, Bitmap value)
            {
                // TODO Auto-generated method stub
                return value.getRowBytes() * value.getHeight();
            }
            
        };
        
        // mExecutor = Executors.newSingleThreadExecutor();
    }
    
    /**
     * 创建
     * @param aKey
     * @return
     */
    abstract protected Bitmap doCreateBitmap(T aKey, int flag);
    
    /**
     * 加载视频截图
     * @param
     * @param aCallback
     */
    public void loadBitmap(final T aKey, final OnImageLoadCompleteCallback<T> aCallback, final int flag)
    {
        Bitmap bitmap = mCache.get(aKey);
        if (bitmap == null)
        {
            final Handler handler = new Handler()
            {
                @Override
                public void handleMessage(Message msg)
                {
                    // TODO Auto-generated method stub
                    mCache.put(aKey, (Bitmap)msg.obj);//添加缓存
                    aCallback.onImageLoadComplete(aKey, (Bitmap)msg.obj);
                }
                
            };
            
            /*  //缓存里没有的话.则去加载图片
              mExecutor.execute(new Runnable() {
                  
                  @Override
                  public void run() {
                      // TODO Auto-generated method stub
                      Bitmap b = doCreateBitmap(aKey);
                      handler.obtainMessage(0, b).sendToTarget();
                  }
              });*/
            ThreadHelper.executeWithCallback(new Executable()
            {
                @Override
                public Object execute()
                    throws Exception
                {
                    Bitmap b = doCreateBitmap(aKey, flag);
                    if (b != null)
                    {
                        handler.obtainMessage(0, b).sendToTarget();
                    }
                    return null;
                }
            }, new NotifyListener<Bitmap>());
        }
        else
        {
            //缓存里有的话.则直接通知回调
            aCallback.onImageLoadComplete(aKey, bitmap);
        }
    }
    
    public void release()
    {
        mContext = null;
        if (mCache != null)
        {
            mCache.evictAll();
            mCache = null;
        }
        mExecutor = null;
    }
    
    /**
     * 加载图片完成时的回调
     * @author yewei
     *
     */
    public interface OnImageLoadCompleteCallback<T>
    {
        public void onImageLoadComplete(T aKey, Bitmap aBitmap);
    }
}
