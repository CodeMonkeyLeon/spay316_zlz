package cn.swiftpass.enterprise.ui.activity.user;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.BaseFragmentActivity;
import cn.swiftpass.enterprise.ui.activity.total.OrderTotalByTerminalActivity;
import cn.swiftpass.enterprise.ui.fragment.CashierFragment;
import cn.swiftpass.enterprise.ui.fragment.DealTypeFragment;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.MyPopupWindowUtils;
import cn.swiftpass.enterprise.utils.MyPopupWindowUtils.OnPopuWindowItemClickListener;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 *  交易分布图
 * <功能详细描述>
 * 
 * @author  shuo_yan
 * @version  [版本号, 2015-4-27]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class DealScatterFragmentActivity extends BaseFragmentActivity implements OnClickListener,
    OnPopuWindowItemClickListener
{
    
    private LinearLayout deal_scatter_back_btn, deal_scatter_index_btn, deal_scatter_date_title;
    
    private TextView deal_scatter_index_tv;
    
    private LinearLayout deal_scatter_date_left_btn, deal_scatter_date_right_btn;
    
    private TextView deal_scatter_date_left_tv, deal_scatter_date_right_tv;
    
    private LinearLayout ll_deal_scatter_title;
    
    private FragmentManager fm;
    
    private FragmentTransaction ft;
    
    private List<Fragment> fragments;
    
    private DealTypeFragment dealTypeFragment; //终端类型
    
    private CashierFragment cashierFragment;//收银员
    
    private OrderTotalByTerminalActivity orderTotalByTerminalActivity; // 支付类型
    
    private MyPopupWindowUtils popWindowUtils;
    
    private PopupWindow pop;
    
    private long startDate;//开始日期
    
    private long endDate;//结束日期
    
    public static final int POPWINDOWN_CLICK = 1;//POPWINDOW点击回调
    
    private String[] data = null;
    
    private int currentPageIndex = 0;
    
    private TextView tv_back, tv_title;
    
    private Handler handler = new Handler()
    {
        public void handleMessage(Message msg)
        {
            if (msg != null)
            {
                switch (msg.what)
                {
                    case POPWINDOWN_CLICK:
                        if (currentPageIndex != msg.arg1)
                        {
                            currentPageIndex = msg.arg1;
                            deal_scatter_index_tv.setText(data[currentPageIndex]);
                            selectPage(currentPageIndex, fm.beginTransaction());
                        }
                        pop.dismiss();
                        break;
                    
                    default:
                        break;
                }
            }
        };
    };
    
    private WebView wb;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deal_scatter_fragment);
        initObject();
        initView();
        initListrner();
        initFragment();
        
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initFragment()
    {
        
        cashierFragment = new CashierFragment();
        dealTypeFragment = new DealTypeFragment();
        orderTotalByTerminalActivity = new OrderTotalByTerminalActivity();
        fragments.add(orderTotalByTerminalActivity);
        fragments.add(dealTypeFragment);
        fragments.add(cashierFragment);
        ft.add(R.id.deal_scatter_content, orderTotalByTerminalActivity);
        ft.add(R.id.deal_scatter_content, dealTypeFragment);
        ft.add(R.id.deal_scatter_content, cashierFragment);
        selectPage(currentPageIndex, ft);
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView()
    {
        deal_scatter_back_btn = getViewById(R.id.deal_scatter_back_btn);
        deal_scatter_index_btn = getViewById(R.id.deal_scatter_index_btn);
        deal_scatter_date_title = getViewById(R.id.deal_scatter_date_title);
        
        deal_scatter_index_tv = getViewById(R.id.deal_scatter_index_tv);
        deal_scatter_date_left_btn = getViewById(R.id.deal_scatter_date_left_btn);
        deal_scatter_date_right_btn = getViewById(R.id.deal_scatter_date_right_btn);
        
        deal_scatter_date_left_tv = getViewById(R.id.deal_scatter_date_left_tv);
        deal_scatter_date_right_tv = getViewById(R.id.deal_scatter_date_right_tv);
        ll_deal_scatter_title = getViewById(R.id.ll_deal_scatter_title);
        
        deal_scatter_date_left_tv.setText(DateUtil.formatYYMD(startDate));
        deal_scatter_date_right_tv.setText(DateUtil.formatYYMD(endDate));
        
        tv_back = getViewById(R.id.tv_back);
        tv_title = getViewById(R.id.tv_title);
        DynModel dynModel = (DynModel)SharedPreUtile.readProduct("dynModel" + ApiConstant.bankCode);
        if (null != dynModel)
        {
            try
            {
                if (!StringUtil.isEmptyOrNull(dynModel.getHeaderColor()))
                {
                    ll_deal_scatter_title.setBackgroundColor(Color.parseColor(dynModel.getHeaderColor()));
                }
                if (!StringUtil.isEmptyOrNull(dynModel.getHeaderFontColor()))
                {
                    deal_scatter_index_tv.setTextColor(Color.parseColor(dynModel.getHeaderFontColor()));
                    tv_back.setTextColor(Color.parseColor(dynModel.getHeaderFontColor()));
                    tv_title.setTextColor(Color.parseColor(dynModel.getHeaderFontColor()));
                }
                else
                {
                    deal_scatter_back_btn.setBackgroundResource(R.drawable.micro_pay_);
                    deal_scatter_index_btn.setBackgroundResource(R.drawable.micro_pay_);
                }
            }
            catch (Exception e)
            {
                Log.e("hehui", "TemplateActivity setContentView " + e);
            }
            
        }
        
        //        wb = (WebView)findViewById(R.id.webview);
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initListrner()
    {
        deal_scatter_back_btn.setOnClickListener(this);
        deal_scatter_index_btn.setOnClickListener(this);
        deal_scatter_date_left_btn.setOnClickListener(this);
        deal_scatter_date_right_btn.setOnClickListener(this);
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initObject()
    {
        fm = getSupportFragmentManager();
        ft = fm.beginTransaction();
        fragments = new ArrayList<Fragment>();
        
        popWindowUtils = new MyPopupWindowUtils(this, this);
        data = getResources().getStringArray(R.array.deal_scatter_type);
        endDate = System.currentTimeMillis();
        startDate = endDate - (long)(1000 * 60 * 60 * 24 * 7);
    }
    
    /** 控件Fragment的显示和隐藏
     * <功能详细描述>
     * @param i
     * @param ft2
     * @see [类、类#方法、类#成员]
     */
    private void selectPage(int i, FragmentTransaction ft)
    {
        if (i > fragments.size() - 1 || ft == null)
        {
            return;
        }
        
        if (fragments != null && fragments.size() > 0)
        {
            for (int a = 0; a < fragments.size(); a++)
            {
                if (a == i)
                {
                    ft.show(fragments.get(a));
                }
                else
                {
                    ft.hide(fragments.get(a));
                }
            }
            
            ft.commit();
        }
    }
    
    /** {@inheritDoc} */
    
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.deal_scatter_back_btn: //返回
                this.finish();
                break;
            case R.id.deal_scatter_index_btn://右边下拉选择页面
                pop = popWindowUtils.getDealPageChangeDialog(ll_deal_scatter_title.getWidth(), data, handler);
                pop.showAsDropDown(ll_deal_scatter_title);
                break;
            case R.id.deal_scatter_date_left_btn://左边开始日期选择
                showDatetimeDialog(0, startDate);
                break;
            case R.id.deal_scatter_date_right_btn://右边结束日期选择
                showDatetimeDialog(1, endDate);
                break;
            default:
                break;
        }
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private Calendar c = null;
    
    protected Dialog dialog;
    
    private void showDatetimeDialog(final int type, long date)
    {
        c = Calendar.getInstance();
        c.setTimeInMillis(date);
        dialog = new DatePickerDialog(DealScatterFragmentActivity.this, new DatePickerDialog.OnDateSetListener()
        {
            public void onDateSet(DatePicker dp, int year, int month, int dayOfMonth)
            {
                c.set(year, month, dayOfMonth);
                if (type == 0)
                {
                    //订单流水
                    startDate = c.getTimeInMillis();
                    deal_scatter_date_left_tv.setText(DateUtil.formatYYMD(startDate));
                }
                else if (type == 1)
                {
                    //退款查询 
                    endDate = c.getTimeInMillis();
                    deal_scatter_date_right_tv.setText(DateUtil.formatYYMD(endDate));
                    if (startDate < endDate)
                    {
                        //开始搜索
                    }
                }
                
            }
        }, c.get(Calendar.YEAR), // 传入年份
            c.get(Calendar.MONTH), // 传入月份
            c.get(Calendar.DAY_OF_MONTH) // 传入天数*/
            );
        dialog.show();
        
    }
    
    /** {@inheritDoc} */
    
    @Override
    public void onPopuWindowItemClick(View v)
    {
        
    }
    
}
