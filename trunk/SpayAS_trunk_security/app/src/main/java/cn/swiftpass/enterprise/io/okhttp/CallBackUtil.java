package cn.swiftpass.enterprise.io.okhttp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.widget.ImageView;

import com.example.common.entity.EmptyData;
import com.example.common.sentry.SentryUtils;
import com.google.gson.Gson;

import org.apache.http.conn.ConnectTimeoutException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;

import javax.net.ssl.SSLHandshakeException;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.Logger;
import okhttp3.Call;
import okhttp3.Response;


public abstract class CallBackUtil<T> {

    public static Handler mMainHandler = new Handler(Looper.getMainLooper());

    public void onProgress(float progress, long total) {
    }

    public void onError(final Call call, final Exception e) {
        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                onFailure(call, e);
            }
        });
    }

    public void onSuccess(Call call, Response response, String url) {
        final T obj = onParseResponse(call, response, url);
        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                onResponse(obj);
            }
        });
    }

    /**
     * 解析response，执行在子线程
     */
    public abstract T onParseResponse(Call call, Response response, String url);

    /**
     * 访问网络失败后被调用，执行在UI线程
     */
    public abstract void onFailure(Call call, Exception e);

    /**
     * 访问网络成功后被调用，执行在UI线程
     */
    public abstract void onResponse(T response);

    public void displayResponseLog(String url, String response) {
        Logger.logD("----------------------------------------------------------------------------------------------------------------------------");
        Logger.logD("| 打印响应内容");
        Logger.logD("| url : " + url);
        Logger.logD("| response : " + response);
        Logger.logD("----------------------------------------------------------------------------------------------------------------------------");
    }

    public static abstract class CallBackDefault extends CallBackUtil<Response> {
        @Override
        public Response onParseResponse(Call call, Response response, String url) {
            return response;
        }
    }

    public static abstract class CallBackString extends CallBackUtil<String> {
        @Override
        public String onParseResponse(Call call, Response response, String url) {
            try {
                return response.body().string();
            } catch (IOException e) {
                new RuntimeException("failure");
                return "";
            }
        }
    }

    public static abstract class CallBackCommonResponse<R> extends CallBackUtil<CommonResponse> {
        boolean isNeedCookie;
        R mR;

        public CallBackCommonResponse(R r) {
            isNeedCookie = true;
            mR = r;
        }

        public CallBackCommonResponse(boolean isNeedCookie, R r) {
            this.isNeedCookie = isNeedCookie;
            mR = r;
        }

        @Override
        public CommonResponse onParseResponse(Call call, Response response, String url) {
            CommonResponse requestResult;
            Gson gson = new Gson();
            try {
                if (isNeedCookie && !TextUtils.isEmpty(response.header("Set-Cookie"))) {
                    OkhttpUtil.cookieList.clear();
                    OkhttpUtil.cookieList.add(response.header("Set-Cookie"));
                }
                String result = response.body().string();
                displayResponseLog(url, result);
                requestResult = gson.fromJson(result, CommonResponse.class);
                requestResult.url = url;
                return requestResult;
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadNetInterfaceException(
                        e,
                        url,
                        "接口返回数据json解析异常",
                        ""
                );
                return null;
            }
        }

        @Override
        public void onSuccess(Call call, Response response, String url) {
            CommonResponse commonResponse = onParseResponse(call, response, url);
            if (null == commonResponse) {
                commonResponse = new CommonResponse(call.request().url().toString());
                commonResponse.result = ErrorCode.RESULT_READING_ERROR;
                commonResponse.message = ErrorCode.getErrorMsg(ErrorCode.RESULT_READING_ERROR);
                SentryUtils.INSTANCE.uploadNetInterfaceException(
                        new Exception("接口没有返回数据 或 返回数据json解析异常"),
                        url.replace(MainApplication.getInstance().getBaseUrl(), ""),
                        commonResponse.message,
                        commonResponse.result
                );
                failureUI(call, commonResponse);
                return;
            }
            if (!ErrorCode.RESULT_SUCCESS.equals(commonResponse.result)) {
                if (url.contains("checkinVersion") && TextUtils.equals(commonResponse.result, "402")) {
                    //版本更新接口不上报sentry
                } else {
                    SentryUtils.INSTANCE.uploadNetInterfaceException(
                            new Exception("接口返回错误码"),
                            url.replace(MainApplication.getInstance().getBaseUrl(), ""),
                            commonResponse.message,
                            commonResponse.result
                    );
                }
                if (ErrorCode.RESULT_NEED_LOGIN.equals(commonResponse.result)) {
                    //session过期
                    commonResponse.message = MainApplication.getInstance().getString(cn.swiftpass.enterprise.intl.R.string.msg_need_login);
                    MainApplication.getInstance().setNeedLogin(true);
                }
                failureUI(call, commonResponse);
                return;
            }


            if (mR instanceof EmptyData) {
                //无需解析json数据
                successUI(commonResponse);
            } else {
                //需要解析json
                try {
                    if (url.contains("spay/orderCount")) {
                        //如果dataList值为空，替换""为[]，因为Gson无法解析""为数组
                        commonResponse.message = commonResponse.message.replace("\"dataList\":\"\"",
                                "\"dataList\":[]");
                    } else if (url.contains("spay/card/transList")
                            || url.contains("spay/paymentLinkApp/order")
                            || url.contains("spay/paymentLinkApp/goods")
                            || url.contains("spay/paymentLinkApp/customer")) {
                        if (commonResponse.message.contains("orderRelateGoodsList")
                                && commonResponse.message.contains("orderCostList")) {
                            commonResponse.message = commonResponse.message.replace("\"orderRelateGoodsList\":\"\"",
                                    "\"orderRelateGoodsList\":[]");
                            commonResponse.message = commonResponse.message.replace("\"orderCostList\":\"\"",
                                    "\"orderCostList\":[]");
                        } else {
                            commonResponse.message = commonResponse.message.replace("\"data\":\"\"",
                                    "\"data\":[]");
                        }
                    } else if (url.contains("spay/card/orderDetail")) {
                        commonResponse.message = commonResponse.message.replace("\"data\":\"\"",
                                "\"data\":[]");
                        commonResponse.message = commonResponse.message.replace("\"discountDetail\":\"\"",
                                "\"discountDetail\":[]");
                    } else if (url.contains("spay/transactionReportDetail")) {
                        commonResponse.message = commonResponse.message.replace("\"dayStatistics\":\"\"",
                                "\"dayStatistics\":[]");
                        commonResponse.message = commonResponse.message.replace("\"payChannelStatistical\":\"\"",
                                "\"payChannelStatistical\":[]");
                        commonResponse.message = commonResponse.message.replace("\"statisticsByAmount\":\"\"",
                                "\"statisticsByAmount\":[]");
                        commonResponse.message = commonResponse.message.replace("\"statisticsByCount\":\"\"",
                                "\"statisticsByCount\":[]");
                    } else if (url.contains("spay/authDetail")
                            || url.contains("spay/queryOrderDetail")
                            || url.contains("spay/unifiedQueryAuth")
                            || url.contains("spay/payQueryOrder")
                            || url.contains("spay/invoiceIdQuery")
                            || url.contains("spay/authReverse")
                            || url.contains("spay/unifiedPayReverse")
                            || url.contains("spay/payReverse")
                            || url.contains("spay/card/query")
                            || url.contains("spay/card/inquiry")
                            || url.contains("spay/unifiedMicroAuth")
                            || url.contains("spay/unifiedMicroPay")
                            || url.contains("spay/queryRefundDetail")
                            || url.contains("spay/spayRefundV2")
                            || url.contains("spay/card/refund")) {
                        commonResponse.message = commonResponse.message.replace("\"discountDetail\":\"\"",
                                "\"discountDetail\":[]");
                    } else if (url.contains("spay/mch/get")) {
                        commonResponse.message = commonResponse.message.replace("\"centerRate\":\"\"",
                                "\"centerRate\":[]");
                    }
                    JsonUtil.jsonToBean(commonResponse.message, mR.getClass());
                    successUI(commonResponse);
                } catch (Exception e) {
                    //json解析异常
                    SentryUtils.INSTANCE.uploadNetInterfaceException(
                            e,
                            url.replace(MainApplication.getInstance().getBaseUrl(), ""),
                            "json解析异常: " + ErrorCode.getErrorMsg(ErrorCode.RESULT_INTERNAL_ERROR),
                            ErrorCode.RESULT_INTERNAL_ERROR
                    );
                    commonResponse.message = ErrorCode.getErrorMsg(ErrorCode.RESULT_INTERNAL_ERROR);
                    failureUI(call, commonResponse);
                }
            }
        }

        private void successUI(CommonResponse arg) {
            mMainHandler.post(new Runnable() {
                @Override
                public void run() {
                    onResponse(arg);
                }
            });
        }

        private void failureUI(Call arg1, CommonResponse arg2) {
            mMainHandler.post(new Runnable() {
                @Override
                public void run() {
                    onFailure(arg1, arg2);
                }
            });
        }

        @Override
        public void onError(final Call call, final Exception e) {
            final String sentryUrl = call.request().url().toString().replace(MainApplication.getInstance().getBaseUrl(), "");
            CommonResponse result = new CommonResponse(call.request().url().toString());
            try {
                throw e;
            } catch (SocketTimeoutException | ConnectTimeoutException e1) {// 超时异常
                result.result = ErrorCode.RESULT_TIMEOUT_ERROR;
                result.message = ErrorCode.getErrorMsg(ErrorCode.RESULT_TIMEOUT_ERROR);
            } catch (SSLHandshakeException e1) {
                result.result = ErrorCode.RESULT_SSL_HANDSHAKE_ERROR;
                result.message = ErrorCode.getErrorMsg(ErrorCode.RESULT_SSL_HANDSHAKE_ERROR);
            } catch (Exception e1) {// 连接服务器超时
                result.result = ErrorCode.RESULT_READING_ERROR;
                result.message = ErrorCode.getErrorMsg(ErrorCode.RESULT_READING_ERROR);
            }
            SentryUtils.INSTANCE.uploadNetworkLayerException(
                    e,
                    sentryUrl,
                    result.message
            );
            failureUI(call, result);
        }

        @Override
        public void onFailure(Call call, Exception e) {

        }

        public abstract void onFailure(Call call, CommonResponse commonResponse);
    }

    public static abstract class CallBackBitmap extends CallBackUtil<Bitmap> {
        private int mTargetWidth;
        private int mTargetHeight;

        public CallBackBitmap() {
        }

        public CallBackBitmap(int targetWidth, int targetHeight) {
            mTargetWidth = targetWidth;
            mTargetHeight = targetHeight;
        }

        ;

        public CallBackBitmap(ImageView imageView) {
            int width = imageView.getWidth();
            int height = imageView.getHeight();
            if (width <= 0 || height <= 0) {
                throw new RuntimeException("无法获取ImageView的width或height");
            }
            mTargetWidth = width;
            mTargetHeight = height;
        }

        ;

        @Override
        public Bitmap onParseResponse(Call call, Response response, String url) {
            if (mTargetWidth == 0 || mTargetHeight == 0) {
                return BitmapFactory.decodeStream(response.body().byteStream());
            } else {
                return getZoomBitmap(response);
            }
        }

        /**
         * 压缩图片，避免OOM异常
         */
        private Bitmap getZoomBitmap(Response response) {
            byte[] data = null;
            try {
                data = response.body().bytes();
            } catch (IOException e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
            }
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;

            BitmapFactory.decodeByteArray(data, 0, data.length, options);
            int picWidth = options.outWidth;
            int picHeight = options.outHeight;
            int sampleSize = 1;
            int heightRatio = (int) Math.floor((float) picWidth / (float) mTargetWidth);
            int widthRatio = (int) Math.floor((float) picHeight / (float) mTargetHeight);
            if (heightRatio > 1 || widthRatio > 1) {
                sampleSize = Math.max(heightRatio, widthRatio);
            }
            options.inSampleSize = sampleSize;
            options.inJustDecodeBounds = false;
            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);

            if (bitmap == null) {
                throw new RuntimeException("Failed to decode stream.");
            }
            return bitmap;
        }
    }

    /**
     * 下载文件时的回调类
     */
    public static abstract class CallBackFile extends CallBackUtil<File> {

        private final String mDestFileDir;
        private final String mDestFileName;

        /**
         * @param destFileDir:文件目录
         * @param destFileName：文件名
         */
        public CallBackFile(String destFileDir, String destFileName) {
            mDestFileDir = destFileDir;
            mDestFileName = destFileName;
        }

        @Override
        public File onParseResponse(Call call, Response response, String url) {
            InputStream is = null;
            byte[] buf = new byte[1024 * 8];
            int len = 0;
            FileOutputStream fos = null;
            try {
                is = response.body().byteStream();
                final long total = response.body().contentLength();

                long sum = 0;

                File dir = new File(mDestFileDir);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                File file = new File(dir, mDestFileName);
                fos = new FileOutputStream(file);
                while ((len = is.read(buf)) != -1) {
                    sum += len;
                    fos.write(buf, 0, len);
                    final long finalSum = sum;
                    mMainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            onProgress(finalSum * 100.0f / total, total);
                        }
                    });
                }
                fos.flush();

                return file;

            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadNetInterfaceException(
                        e,
                        url.replace(MainApplication.getInstance().getBaseUrl(), ""),
                        "",
                        ""
                );
            } finally {
                try {
                    response.body().close();
                    if (is != null) is.close();
                } catch (IOException e) {
                    SentryUtils.INSTANCE.uploadNetInterfaceException(
                            e,
                            url.replace(MainApplication.getInstance().getBaseUrl(), ""),
                            "",
                            ""
                    );
                }
                try {
                    if (fos != null) fos.close();
                } catch (IOException e) {
                    SentryUtils.INSTANCE.uploadNetInterfaceException(
                            e,
                            url.replace(MainApplication.getInstance().getBaseUrl(), ""),
                            "",
                            ""
                    );
                }

            }
            return null;
        }
    }
}
