package cn.swiftpass.enterprise;

import android.app.Activity;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import androidx.multidex.MultiDex;

import com.example.common.sentry.SentryUtils;
import com.example.common.sp.PreferenceUtil;
import com.example.common.utils.PngSetting;
import com.google.gson.Gson;
import com.iflytek.cloud.SpeechUtility;
import com.igexin.sdk.IUserLoggerInterface;
import com.igexin.sdk.PushManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import org.xclcharts.base.BaseApplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.swiftpass.enterprise.broadcast.NetworkStateReceiver;
import cn.swiftpass.enterprise.bussiness.logica.BaseManager;
import cn.swiftpass.enterprise.bussiness.model.CertificateBean;
import cn.swiftpass.enterprise.bussiness.model.UserInfo;
import cn.swiftpass.enterprise.bussiness.model.WalletListBean;
import cn.swiftpass.enterprise.common.Constant;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.io.database.DatabaseHelper;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkAuthority;
import cn.swiftpass.enterprise.utils.ApkUtil;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.ErrorHandler;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.permissionutils.PermissionInterceptor;
import cn.swiftpass.enterprise.utils.permissionutils.PermissionsManage;
import cn.swiftplus.enterprise.printsdk.print.PrintClient;

public class MainApplication extends BaseApplication {

    private static final String TAG = MainApplication.class.getSimpleName();


    /**
     * instance 单例
     */
    private static MainApplication instance = null;

    public MainApplication() {
        instance = this;
    }

    public static MainApplication getInstance() {
        return instance;
    }

    private UserInfo mUserInfo = new UserInfo();
    // 用户id
    private long mUserId = 0;
    // list存activity
    private List<Activity> mListActivities = new ArrayList<Activity>();
    private List<Activity> mAllActivities = new ArrayList<Activity>();
    private String mIsDefault = "0";
    private List<WalletListBean> mWalletListBeans = new ArrayList<WalletListBean>(); //已开通的小钱包列表
    private boolean mNeedLogin = false;
    private Map<String, String> mPayTypeMap = new HashMap<String, String>(); //支付类型map
    private Map<String, String> mTradeStateMap = new HashMap<String, String>(); //交易状态
    private Map<String, String> mPreAuthTradeStateMap = new HashMap<String, String>(); //预授权的交易状态，在前端默认写死
    //卡通道的交易状态的集合,----- V3.1.0版本新增2021/03/26
    private Map<String, String> mCardPaymentTradeStateMap = new HashMap<String, String>();
    public Map<String, String> refundStateMap = new HashMap<String, String>(); //退款状态
    private Map<String, String> mApiProviderMap = new HashMap<String, String>(); //支付类型
    private boolean mIsUpdateShow = false; //更新提示
    private Integer mNumFixed = 2; //获取币种的最小的小数点位数，为整数，如果获取不到默认为2，即小数点后面两位
    private boolean mHasCardPayment = false; //已开通的支付方式里，是否有卡交易通道，默认是false
    private boolean mHasQRPayment = false; //已开通的支付方式里，是否有QR交易通道，默认是false
    private String mSerPubKey = ""; //服务端公钥
    private String mSKey = ""; //密钥标识
    private String mFileMd5 = "";//更新安装包的MD5
    //请求CA证书公钥获取接口得到最新CA证书公钥以及过期时间等相关的信息----V3.0.5最新版本CA证书需求添加
    private CertificateBean mCertificateBean;
    private String mNewSignKey = "";//新的签名key
    // 不管是蓝牙连接方还是服务器方，得到socket对象后都传入
    private BluetoothSocket mBluetoothSocket = null;
    //默认下载存储路径
    private String mDefaultDownloadPath = "";
    private Context mContext;
    private double mSourceToUsdExchangeRate = 0;
    private double mAlipayPayRate = 0;
    private PaymentLinkAuthority mPlAuth;
    //转美元费率
    private double mUsdToRmbExchangeRate = 1.0;
    //附加手续费率
    private double mSurchargeRate = 0;
    //预扣税费率
    private double mTaxRate = 0;
    //增值税率
    private double mVatRate = 0;
    //是否有附加税率
    private boolean mIsSurchargeOpen;
    //是否有预扣税
    private boolean mIsTaxRateOpen;
    //是否有小费----旧版本用此字段，V2.2.4已废弃
    private boolean mIsTipOpen;
    //是否有小费 0为无，1为有----V2.2.4新字段
    private boolean mTipOpenFlag;
    private NetworkStateReceiver mNetworkStateReceiver;
    private boolean isInitOK = false;
    // ORMLite
    private volatile DatabaseHelper helper;
    private volatile boolean created = false;
    private volatile boolean destroyed = false;

    private String mBaseUrl = "https://app.wepayez.com/";


    public String getBaseUrl() {
        return mBaseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        mBaseUrl = baseUrl;
    }

    /**
     * 签名key
     */
    public String getNewSignKey() {
        if (!StringUtil.isEmptyOrNull(mNewSignKey))
            return mNewSignKey;

        if (mUserInfo == null) {
            return PreferenceUtil.getString("newSignKey" + getMchId(), "");
        } else {
            return PreferenceUtil.getString("newSignKey" + getMchId(), mUserInfo.signKey);
        }
    }

    /**
     * 签名key
     */

    public void setNewSignKey(String sign) {
        if (!StringUtil.isEmptyOrNull(sign)) {
            PreferenceUtil.commitString("newSignKey" + getMchId(), sign);
        }
    }

    public boolean hasQRPayment() {
        return mHasQRPayment;
    }

    public MainApplication setHasQRPayment(boolean hasQRPayment) {
        this.mHasQRPayment = hasQRPayment;
        return this;
    }

    public boolean hasCardPayment() {
        return mHasCardPayment;
    }

    public MainApplication setHasCardPayment(boolean hasCardPayment) {
        this.mHasCardPayment = hasCardPayment;
        return this;
    }

    public Integer getNumFixed() {
        return mNumFixed;
    }

    public MainApplication setNumFixed(Integer numFixed) {
        this.mNumFixed = numFixed;
        return this;
    }

    public boolean isUpdateShow() {
        return mIsUpdateShow;
    }

    public MainApplication setUpdateShow(boolean updateShow) {
        mIsUpdateShow = updateShow;
        return this;
    }

    public Map<String, String> getApiProviderMap() {
        return mApiProviderMap;
    }

    public MainApplication setApiProviderMap(Map<String, String> apiProviderMap) {
        this.mApiProviderMap = apiProviderMap;
        return this;
    }

    @SuppressWarnings("unchecked")
    public Map<String, String> getRefundStateMap() {
        if (refundStateMap.size() > 0) {
            return refundStateMap;
        }
        return (Map<String, String>) SharedPreUtils.readProduct("refundStateMap" + getMchId());
    }

    public void setRefundStateMap(Map<String, String> map) {
        refundStateMap = map;
        if (map != null && map.size() > 0) {
            SharedPreUtils.saveObject(map, "refundStateMap" + getMchId());
        }
    }


    //V3.1.0版本新增---卡通道交易类型的支付方式的读取和设置
    public Map<String, String> getCardPaymentTradeStateMap() {
        if (mCardPaymentTradeStateMap.size() > 0) {
            return mCardPaymentTradeStateMap;
        }
        return (Map<String, String>) SharedPreUtils.readProduct("cardPaymentTradeStateMap" + getMchId());

    }

    public void setCardPaymentTradeStateMap(Map<String, String> mCardPaymentTradeStateMap) {
        this.mCardPaymentTradeStateMap = mCardPaymentTradeStateMap;
        if (mCardPaymentTradeStateMap != null && mCardPaymentTradeStateMap.size() > 0) {
            SharedPreUtils.saveObject(mCardPaymentTradeStateMap, "cardPaymentTradeStateMap" + getMchId());
        }
    }


    public Map<String, String> getPreAuthTradeStateMap() {
        if (mPreAuthTradeStateMap.size() > 0) {
            return mPreAuthTradeStateMap;
        }
        if ((Map<String, String>) SharedPreUtils.readProduct("PreAuthtradeStateMap" + getMchId()) == null) {
            return mPreAuthTradeStateMap;
        }
        return (Map<String, String>) SharedPreUtils.readProduct("PreAuthtradeStateMap" + getMchId());
    }

    public void setPreAuthTradeStateMap(Map<String, String> preAuthtradeStateMap) {
        if (preAuthtradeStateMap != null && preAuthtradeStateMap.size() > 0) {
            SharedPreUtils.saveObject(preAuthtradeStateMap, "PreAuthtradeStateMap" + getMchId());
        }
    }


    /**
     * 支付类型名称
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> getTradeTypeMap() {
        if (mTradeStateMap.size() > 0) {
            return mTradeStateMap;
        }
        return (Map<String, String>) SharedPreUtils.readProduct("tradeStateMap" + getMchId());
    }

    /**
     * 支付状态
     */

    public void setTradeStateMap(Map<String, String> map) {
        mTradeStateMap = map;
        if (map != null && map.size() > 0) {
            SharedPreUtils.saveObject(map, "tradeStateMap" + getMchId());
        }
    }


    /**
     * 支付类型名称
     */

    @SuppressWarnings("unchecked")
    public Map<String, String> getPayTypeMap() {
        if (mPayTypeMap.size() > 0) {
            return mPayTypeMap;
        }
        return (Map<String, String>) SharedPreUtils.readProduct("payTypeMap" + getMchId());
    }

    /**
     * 支付类型名称
     */

    public void setPayTypeMap(Map<String, String> map) {
        mPayTypeMap = map;
        if (map != null && map.size() > 0) {
            SharedPreUtils.saveObject(map, "payTypeMap" + getMchId());
        }
    }

    public boolean isNeedLogin() {
        return mNeedLogin;
    }

    public void setNeedLogin(boolean needLogin) {
        this.mNeedLogin = needLogin;
    }

    public void setWalletListBeans(List<WalletListBean> walletListBeans) {
        this.mWalletListBeans.clear();
        this.mWalletListBeans.addAll(walletListBeans);
    }

    public List<WalletListBean> getWalletListBeans() {
        return mWalletListBeans;
    }

    public String isDefault() {
        return mIsDefault;
    }

    public MainApplication setIsDefault(String isDefault) {
        this.mIsDefault = isDefault;
        return this;
    }

    public List<Activity> getAllActivities() {
        return mAllActivities;
    }

    public List<Activity> getListActivities() {
        return mListActivities;
    }

    public long getUserId() {
        if (mUserId > 0) {
            return mUserId;
        }
        return PreferenceUtil.getLong("userId", mUserId);
    }

    public void setUserId(Long userId) {
        if (userId > 0) {
            PreferenceUtil.commitLong("userId", userId);
        }
    }

    public UserInfo getUserInfo() {
        return mUserInfo;
    }

    public MainApplication setUserInfo(UserInfo userInfo) {
        this.mUserInfo = userInfo;
        return this;
    }

    public boolean isRefundAuth(String auth) {
        if (mUserInfo == null) return false;
        return !TextUtils.isEmpty(mUserInfo.isRefundAuth) && mUserInfo.isRefundAuth.equals(auth);
    }

    public boolean isOrderAuth(String auth) {
        if (mUserInfo == null) return false;
        return !TextUtils.isEmpty(mUserInfo.isOrderAuth) && mUserInfo.isOrderAuth.equals(auth);
    }

    public boolean isAdmin(int admin) {
        if (mUserInfo == null) return false;
        return !TextUtils.isEmpty(String.valueOf(mUserInfo.isAdmin)) && admin == mUserInfo.isAdmin;
    }


    public String getSerPubKey() {
        return mSerPubKey;
    }

    public MainApplication setSerPubKey(String serPubKey) {
        this.mSerPubKey = serPubKey;
        return this;
    }


    public String getSKey() {
        return mSKey;
    }

    public MainApplication setSKey(String skey) {
        this.mSKey = skey;
        return this;
    }

    public String getFileMd5() {
        return mFileMd5;
    }

    public MainApplication setFileMd5(String fileMd5) {
        this.mFileMd5 = fileMd5;
        return this;
    }

    public CertificateBean getCertificateBean() {
        return mCertificateBean;
    }

    public MainApplication setCertificateBean(CertificateBean certificateBean) {
        this.mCertificateBean = certificateBean;
        return this;
    }


    public BluetoothSocket getBluetoothSocket() {
        return mBluetoothSocket;
    }

    public MainApplication setBluetoothSocket(BluetoothSocket bluetoothSocket) {
        this.mBluetoothSocket = bluetoothSocket;
        return this;
    }

    public String getDefaultDownloadPath() {
        return mDefaultDownloadPath;
    }

    public MainApplication setDefaultDownloadPath(String defaultDownloadPath) {
        this.mDefaultDownloadPath = defaultDownloadPath;
        return this;
    }

    public double getSourceToUsdExchangeRate() {
        return mSourceToUsdExchangeRate;
    }

    public void setSourceToUsdExchangeRate(double sourceToUsdExchangeRate) {
        this.mSourceToUsdExchangeRate = sourceToUsdExchangeRate;
    }

    public double getAlipayPayRate() {
        return mAlipayPayRate;
    }

    public void setAlipayPayRate(double alipayPayRate) {
        this.mAlipayPayRate = alipayPayRate;
    }

    public PaymentLinkAuthority getPaymentLinkAuthority() {
        return mPlAuth;
    }

    public void setPaymentLinkAuthority(String plAuth) {
        if (TextUtils.isEmpty(plAuth)) {
            this.mPlAuth = null;
            return;
        }
        try {
            Gson gson = new Gson();
            this.mPlAuth = gson.fromJson(plAuth, PaymentLinkAuthority.class);
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            this.mPlAuth = null;
        }

    }

    public double getUsdToRmbExchangeRate() {
        return mUsdToRmbExchangeRate;
    }

    public void setUsdToRmbExchangeRate(double usdToRmbExchangeRate) {
        this.mUsdToRmbExchangeRate = usdToRmbExchangeRate;
    }

    public double getSurchargeRate() {
        return mSurchargeRate;
    }

    public void setSurchargeRate(double surchargeRate) {
        this.mSurchargeRate = surchargeRate;
    }

    public double getTaxRate() {
        return mTaxRate;
    }

    public void setTaxRate(double taxRate) {
        this.mTaxRate = taxRate;
    }

    public double getVatRate() {
        return mVatRate;
    }

    public void setVatRate(double vatRate) {
        this.mVatRate = vatRate;
    }

    public boolean isSurchargeOpen() {
        return mIsSurchargeOpen;
    }

    public void setSurchargeOpen(boolean surchargeOpen) {
        this.mIsSurchargeOpen = surchargeOpen;
    }

    public boolean isTaxRateOpen() {
        return mIsTaxRateOpen;
    }

    public void setTaxRateOpen(boolean taxRateOpen) {
        mIsTaxRateOpen = taxRateOpen;
    }

    public boolean isTipOpen() {
        return mIsTipOpen;
    }

    public void setTipOpen(boolean tipOpen) {
        mIsTipOpen = tipOpen;
    }

    public boolean isTipOpenFlag() {
        return mTipOpenFlag;
    }

    public void setTipOpenFlag(boolean tipOpenFlag) {
        this.mTipOpenFlag = tipOpenFlag;
    }


    /******************************************************************************************************************************************/

    /**
     * 币种符号
     */
    @SuppressWarnings("unchecked")
    public String getFeeFh() {
        if (mUserInfo == null) {
            return PreferenceUtil.getString("feeFh" + getMchId(),
                    "");
        } else {
            if (StringUtil.isEmptyOrNull(mUserInfo.feeFh))
                return mUserInfo.feeFh;

            return PreferenceUtil.getString("feeFh" + getMchId(),
                    mUserInfo.feeFh);
        }
    }

    public void setFeeFh(String map) {
        if (!StringUtil.isEmptyOrNull(map)) {
            PreferenceUtil.commitString("feeFh" + getMchId(), map);
        }
    }

    /**
     * 货币类型
     */
    @SuppressWarnings("unchecked")
    public String getFeeType() {
        if (mUserInfo == null) {
            return PreferenceUtil.getString("feeTyp" + getMchId() + getUserId(),
                    "");
        } else {
            if (StringUtil.isEmptyOrNull(mUserInfo.feeType)) {
                return mUserInfo.feeType;
            }
            return PreferenceUtil.getString("feeTyp" + getMchId() + getUserId(),
                    mUserInfo.feeType);
        }
    }

    public void setFeeType(String map) {
        PreferenceUtil.commitString("feeTyp" + getMchId() + getUserId(), map);
    }

    /**
     * 签名key
     */

    public String getSignKey() {

        if (mUserInfo == null) {
            return PreferenceUtil.getString("signKey" + getMchId(), "");
        } else {

            if (!StringUtil.isEmptyOrNull(mUserInfo.signKey)) {
                return mUserInfo.signKey;
            }
            return PreferenceUtil.getString("signKey" + getMchId(), mUserInfo.signKey);
        }
    }

    /**
     * 签名key
     */

    public void setSignKey(String sign) {
        if (!StringUtil.isEmptyOrNull(sign)) {
            PreferenceUtil.commitString("signKey" + getMchId(), sign);
        }
    }


    /**
     * 商户号
     */
    public String getMchId() {
        if (mUserInfo == null) {
            return PreferenceUtil.getString("mchId", "");
        } else {
            if (!StringUtil.isEmptyOrNull(mUserInfo.mchId)) {
                return mUserInfo.mchId;
            }
            return PreferenceUtil.getString("mchId", mUserInfo.mchId);
        }
    }

    /**
     * 商户号
     */
    public void setMchId(String mchId) {
        if (!StringUtil.isEmptyOrNull(mchId)) {
            PreferenceUtil.commitString("mchId", mchId);
        }
    }

    /**
     * 蓝牙状态 <功能详细描述>
     */
    @SuppressWarnings("unchecked")
    public Boolean getBlueState() {
        return PreferenceUtil.getBoolean("blueState" + BuildConfig.bankCode, false);
    }

    /**
     * 蓝牙状态 <功能详细描述>
     */
    public void setBlueState(Boolean sign) {
        PreferenceUtil.commitBoolean("blueState" + BuildConfig.bankCode, sign);
    }

    public String getRealName() {
        if (mUserInfo == null) {
            return PreferenceUtil.getString("realName", "");
        } else {
            if (!StringUtil.isEmptyOrNull(mUserInfo.realname)) {
                return mUserInfo.realname;
            }
            return PreferenceUtil.getString("realName", mUserInfo.realname);
        }
    }

    public void setRealName(String realName) {
        if (!StringUtil.isEmptyOrNull(realName)) {
            PreferenceUtil.commitString("realName", realName);
        }
    }

    /**
     * 商户名称
     */
    public String getMchName() {

        if (mUserInfo == null) {
            return PreferenceUtil.getString("mchName", "");
        } else {
            if (!StringUtil.isEmptyOrNull(mUserInfo.mchName)) {
                return mUserInfo.mchName;
            }
            return PreferenceUtil.getString("mchName", mUserInfo.mchName);
        }
    }

    public void setMchName(String mchName) {
        if (!StringUtil.isEmptyOrNull(mchName)) {
            PreferenceUtil.commitString("mchName", mchName);
        }
    }

    public String getExchangeRate() {
        return PreferenceUtil.getString("exchangeRate" + MainApplication.getInstance().getUserId() +
                getMchId(), "");
    }

    public void setExchangeRate(String value) {
        PreferenceUtil.commitString("exchangeRate" + MainApplication.getInstance().getUserId() + getMchId(), value);
    }


    /**
     * 蓝牙打印设置自动状态 <功能详细描述> 默认是不自动打印
     */

    @SuppressWarnings("unchecked")
    public Boolean getAutoBluePrintSetting() {
        return PreferenceUtil.getBoolean("autoBluePrintSetting" + BuildConfig.bankCode, false);
    }

    public void setAutoBluePrintSetting(Boolean sign) {
        PreferenceUtil.commitBoolean("autoBluePrintSetting" + BuildConfig.bankCode, sign);
    }

    /**
     * 蓝牙打印设置状态 <功能详细描述>
     */

    @SuppressWarnings("unchecked")
    public Boolean getBluePrintSetting() {
        return PreferenceUtil.getBoolean("BluePrintSetting" + BuildConfig.bankCode, true);
    }

    /**
     * 蓝牙打印设置状态 <功能详细描述>
     */

    public void setBluePrintSetting(Boolean sign) {
        PreferenceUtil.commitBoolean("BluePrintSetting" + BuildConfig.bankCode, sign);
    }

    @SuppressWarnings("unchecked")
    public String getBlueDeviceAddress() {
        return PreferenceUtil.getString("blueDeviceAddress" + BuildConfig.bankCode, "");
    }

    public void setBlueDeviceNameAddress(String device) {
        PreferenceUtil.commitString("blueDeviceAddress" + BuildConfig.bankCode, device);
    }

    @SuppressWarnings("unchecked")
    public String getBlueDeviceName() {
        return PreferenceUtil.getString("blueDevice" + BuildConfig.bankCode, "");
    }

    public void setBlueDeviceName(String device) {
        PreferenceUtil.commitString("blueDevice" + BuildConfig.bankCode, device);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        unregisterReceiver(mNetworkStateReceiver);
        releaseHelper();
        destroyed = true;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //获取Context
        mContext = getApplicationContext();

        //apk更新包下载地址
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            mDefaultDownloadPath = getApplicationContext().getExternalCacheDir().getPath();
            Log.d(TAG, "have SD");
        } else {
            mDefaultDownloadPath = getApplicationContext().getCacheDir().getPath();
            Log.d(TAG, " not have SD");
        }

        // 设置权限申请拦截器
        PermissionsManage.setInterceptor(new PermissionInterceptor());
        PermissionsManage.setScopedStorage(true);
        // 多进程导致多次初始化Application,这里只初始化App主进程的Application
        String curProcessName = ApkUtil.getProcessName(this, android.os.Process.myPid());
        if (!curProcessName.equals(getPackageName())) {
            return;
        }

        if (!BuildConfig.IS_POS_VERSION) {
            try {
                //个推
                PushManager.getInstance().initialize(getApplicationContext());
                if (BuildConfig.isDebug) {
                    PushManager.getInstance().setDebugLogger(this, new IUserLoggerInterface() {
                        @Override
                        public void log(String s) {
                            Logger.i("TAG_ZLZ", "setDebugLogger-> " + s);
                        }
                    });
                }
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                Log.e(TAG, Log.getStackTraceString(e));
            }
        } else if (BuildConfig.IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)) {//如果是POS版本，则需要带上POS机器型号，再初始化打印SDK
            PrintClient.initClient(mContext);
            PrintClient.getInstance().initPrintSDK(BuildConfig.posTerminal);
        }

        //        FrontiaApplication.initFrontiaApplication(getApplicationContext()); // W.L
        // 初始化使用百度社交化分享控件
        // 删除1.3.2版本的日志文件
        ErrorHandler.getInstance().delete();
        // 初始异常文件
        ErrorHandler errorHandler = ErrorHandler.getInstance();
        errorHandler.init(this);

        /**
         * ORMLite
         */
        if (helper == null) {
            helper = getHelperInternal(this);
            created = true;
        }
        initConfig();

        if (!BuildConfig.IS_POS_VERSION) {
            // 定位地理城市
            // locate();
            // 获取定位位置城市
            // 科大讯飞 初始化
            try {
                SpeechUtility.createUtility(MainApplication.this, "appid=5775d91b");
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
            }
        }

        //初始化Sentry
        SentryUtils.INSTANCE.init(
                this,
                BuildConfig.bankCode,
                BuildConfig.VERSION_NAME,
                BuildConfig.Environment,
                BuildConfig.isUploadLocalTryCatchException);


        //初始化png
        initPng();
    }


    /**
     * 初始化png
     */
    private void initPng() {
        PngSetting.INSTANCE.init1(BuildConfig.isChangePng1);
    }


    private void initConfig() {
        if (isInitOK) return;
        AppHelper.getAppCacheDir();
        try {
            //从本地缓存里面取服务器地址
            PreferenceUtil.init(this);
            String serverConfig = PreferenceUtil.getString(Constant.SERVER_CONFIG, "");
            Log.i("TAG_ZLZ", "初始化的serverConfig: " + serverConfig);
            if (TextUtils.isEmpty(serverConfig)) {
                //要进行是否开启CDN的判断
                String CDN_status = PreferenceUtil.getString("CDN", "open");
                if (CDN_status.equals("open")) {
                    mBaseUrl = BuildConfig.serverAddr;
                } else {
                    mBaseUrl = BuildConfig.serverAddrBack;
                }
            } else {
                switch (serverConfig) {
                    case Constant.SERVER_ADDRESS_PRD:
                        mBaseUrl = BuildConfig.serverAddrPrd;
                        break;
                    case Constant.SERVER_ADDRESS_DEV:
                        mBaseUrl = BuildConfig.serverAddrDev;
                        break;
                    case Constant.SERVER_ADDRESS_DEV_JH:
                        mBaseUrl = BuildConfig.serverAddrDevJH;
                        break;
                    case Constant.SERVER_ADDRESS_TEST_123:
                        mBaseUrl = BuildConfig.serverAddrTest123;
                        break;
                    case Constant.SERVER_ADDRESS_TEST_61:
                        mBaseUrl = BuildConfig.serverAddrTest61;
                        break;
                    case Constant.SERVER_ADDRESS_TEST_63:
                        mBaseUrl = BuildConfig.serverAddrTest63;
                        break;
                    case Constant.SERVER_ADDRESS_TEST_UAT:
                        mBaseUrl = BuildConfig.serverAddrTestUAT;
                        break;
                    case Constant.SERVER_ADDRESS_OVERSEAS_61:
                        mBaseUrl = BuildConfig.serverAddrOverseas61;
                        break;
                    case Constant.SERVER_ADDRESS_OVERSEAS_63:
                        mBaseUrl = BuildConfig.serverAddrOverseas63;
                        break;
                    case Constant.SERVER_ADDRESS_CHECKOUT_DEV:
                        mBaseUrl = BuildConfig.serverAddrCheckoutDev;
                        break;
                    case Constant.SERVER_ADDRESS_SELF:
                        mBaseUrl = PreferenceUtil.getString(Constant.SERVER_ADDRESS_SELF, "");
                        break;
                }
                Log.i("TAG_TEST", "base url: (initConfig serverCifg not null)  " + mBaseUrl);
            }
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
        mNetworkStateReceiver = new NetworkStateReceiver();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            //android13
            registerReceiver(mNetworkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION), Context.RECEIVER_VISIBLE_TO_INSTANT_APPS);
        } else {
            registerReceiver(mNetworkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }

        isInitOK = true;

    }

    public void exit() {
        // 程序注销
        BaseManager.destoryAll();
        destory();
    }

    public SharedPreferences getApplicationPreferences() {
        return getSharedPreferences("prefs", Context.MODE_PRIVATE);
    }

    public DatabaseHelper getHelper() {
        if (helper == null) {
            if (!created) {
                throw new IllegalStateException("A call has not been made to onCreate() yet so the helper is null");
            } else if (destroyed) {
                throw new IllegalStateException("A call to onDestroy has already been made and the helper cannot be used after that point");
            } else {
                throw new IllegalStateException("Helper is null for some unknown reason");
            }
        } else {
            return helper;
        }
    }

    protected DatabaseHelper getHelperInternal(Context context) {
        DatabaseHelper newHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        return newHelper;
    }

    protected void releaseHelper() {
        OpenHelperManager.releaseHelper();
        this.helper = null;
    }

    public void destory() {
        isInitOK = false;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
