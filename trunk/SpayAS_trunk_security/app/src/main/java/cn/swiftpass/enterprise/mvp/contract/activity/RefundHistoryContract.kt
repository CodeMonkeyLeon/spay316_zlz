package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @人的一生有一个半童年
 * @一个童年在自己小时候
 * @而半个童年在自己孩子的小时候
 */
class RefundHistoryContract {


    interface View : BaseView {

        fun queryUserRefundOrderSuccess(
            response: ArrayList<Order?>?,
            refundState: String?,
            isLoadMore: Boolean
        )

        fun queryUserRefundOrderFailed(error: Any?)


        fun queryRefundDetailSuccess(response: Order?)

        fun queryRefundDetailFailed(error: Any?)

    }


    interface Presenter : BasePresenter<View> {


        fun queryRefundDetail(
            orderNo: String?,
            mchId: String?
        )


        fun queryUserRefundOrder(
            outTradeNo: String?,
            refundTime: String?,
            mchId: String?,
            refundState: String?,
            page: Int,
            isLoadMore: Boolean
        )

    }


}