package cn.swiftpass.enterprise.ui.fmt;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.common.sentry.SentryUtils;
import com.example.common.sp.PreferenceUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.DailyStatisticsBean;
import cn.swiftpass.enterprise.bussiness.model.MasterCardDailyStatisticsBean;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.PreAuthDailyStatisticsBean;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.fragment.TabBillContract;
import cn.swiftpass.enterprise.mvp.presenter.fragment.TabBillPresenter;
import cn.swiftpass.enterprise.ui.activity.MasterCardOrderDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.OrderDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.PreAuthActivity;
import cn.swiftpass.enterprise.ui.activity.PreAuthDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.bill.UserListActivity;
import cn.swiftpass.enterprise.ui.activity.list.NewPullDownListView;
import cn.swiftpass.enterprise.ui.activity.list.PullToRefreshBase;
import cn.swiftpass.enterprise.ui.activity.list.PullToRefreshListView;
import cn.swiftpass.enterprise.ui.activity.spayMainTabActivity;
import cn.swiftpass.enterprise.ui.activity.user.RefundRecordOrderDetailsActivity;
import cn.swiftpass.enterprise.ui.widget.CardPaymentSelectPopupWindow;
import cn.swiftpass.enterprise.ui.widget.PreAuthSelectPopupWindow;
import cn.swiftpass.enterprise.ui.widget.SelectDatePopupWindow;
import cn.swiftpass.enterprise.ui.widget.SelectDateTimePickerViews;
import cn.swiftpass.enterprise.ui.widget.SelectPicPopupWindow;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.KotlinUtils;
import cn.swiftpass.enterprise.utils.LocaleUtils;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.StringUtil;


/**
 * Created by aijingya on 2018/5/2.
 *
 * @Package cn.swiftpass.enterprise.ui.fmt
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/5/2.17:25.
 */

public class FragmentTabBill extends BaseFragment<TabBillContract.Presenter> implements NewPullDownListView.OnRefreshListioner, NewPullDownListView.OnLoadDateRefreshListioner, TabBillContract.View {
    private static final String TAG = FragmentTabBill.class.getSimpleName();
    private static Map<String, String> dayTatal = new HashMap<String, String>();
    public String mTimeStr;
    public long mTimeLong;
    List<Integer> tradeType = new ArrayList<Integer>();
    List<Integer> payType = new ArrayList<Integer>();
    SelectDatePopupWindow selectDatePopupWindow;
    View headview1;
    View headview2;
    private spayMainTabActivity mActivity;
    private ViewHolder holder;
    private UserModel userModel;
    private ListView listView;
    private List<Order> orderList = new ArrayList<Order>();
    private BillStreamAdapter billStreamAdapter;
    private List<String> choiceTypeList = new ArrayList<String>();
    private List<String> payTypeList = new ArrayList<String>();
    private List<String> choiceTypeOutsideList = new ArrayList<String>();
    private List<String> payTypeOutsideList = new ArrayList<String>();
    private List<Integer> ApiProviderList = new ArrayList<Integer>();
    private List<Integer> TradeTypeList = new ArrayList<Integer>();
    private List<Integer> TradeStateList = new ArrayList<Integer>();
    private List<Integer> ApiProviderOutsideList = new ArrayList<Integer>();
    private List<Integer> TradeTypeOutsideList = new ArrayList<Integer>();
    private List<Integer> TradeStateOutsideList = new ArrayList<Integer>();
    private Map<String, String> dayCountMap = new HashMap<String, String>();
    private int pageFulfil = 0;
    private TextView tv_prompt;
    private int isRef = 0;//0流水，1，退款，2 卡券
    private int isUnFreezen = 0;//0预授权流水，1，预授权解冻
    private PullToRefreshListView mPullRefreshListView;
    private TextView tv_total;
    private RelativeLayout ly_stream;
    private String streamTime, refundTime;
    //刷新反正多次刷新
    private boolean isMoerRefresh = true;
    private LinearLayout ly_but_choice, ly_choice_date, ly_but_cachier;
    private LinearLayout ll_outside_float_bar, ll_outside_choice_date, ll_outside_choice_cachier, ll_outside_choice_filter;
    private View view_cashier_line_outside, view_cashier_line;

    //    private String time;
    private LinearLayout ll_static_data;
    private TextView tv_outside_bill_title_amount, tv_outside_total_amount, tv_outside_bill_total_count_title, tv_outside_total_count;
    private TextView tv_outside_refund_amount_bill_title, tv_outside_refund_amount, tv_outside_refund_count_bill_tltle, tv_outside_refund_count;
    private TextView tv_outside_net_amount_bill_title, tv_outside_net_amount, tv_outside_daily_ring_ratio_bill_title, tv_outside_daily_ring_ratio;
    private TextView tv_cashier_name, tv_outside_cashier_name;
    private TextView tv_time, tv_outside_time;
    private boolean loadMore = true;
    private Integer reqFeqTime = 0;
    private boolean loadNext = false;
    private boolean isHadhandle;
    private boolean isPreAuth = false;//区分是否预授权
    private boolean isMasterCard = false;//区分是否是Mastercard交易
    private String bill_sale_or_Pre_auth_or_Card_payment; //订单列表页面，选择是收银还是预授权订单还是卡交易
    private LinearLayout ll_bill_summary, ll_outside_bill_sunmmary;
    private TextView tv_bill_title_amount, tv_total_amount, tv_bill_total_count_title, tv_total_count;
    private TextView tv_refund_amount_bill_title, tv_refund_amount, tv_refund_count_bill_tltle, tv_refund_count;
    private TextView tv_net_amount_bill_title, tv_net_amount, tv_daily_ring_ratio_bill_title, tv_daily_ring_ratio;
    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            //流水
            if (msg.what == HandlerManager.STREAM_BILL) {
                if (!StringUtil.isEmptyOrNull(refundTime)) {
                    String value = dayTatal.get(refundTime);
                    if (!StringUtil.isEmptyOrNull(value)) {
                        tv_total.setText(getString(R.string.tx_bill_stream_total) + DateUtil.formatMoneyUtils(Long.parseLong(value)));
                    }
                }
            }
            //退款
            else if (msg.what == HandlerManager.STREAM_BILL_REFUND) {
                if (!StringUtil.isEmptyOrNull(streamTime)) {
                    String value = dayTatal.get(streamTime);
                    if (!StringUtil.isEmptyOrNull(value)) {
                        tv_total.setText(getString(R.string.tv_refund_title_info) + DateUtil.formatMoneyUtils(Long.parseLong(value)));
                    }
                }
            } else if (msg.what == HandlerManager.PAY_FINISH_REFUND) {
                tv_total.setText("");
            } else if (msg.what == HandlerManager.PAY_SWITCH_TAB) {//刷新账单
                if (choiceTypeList.size() > 0) {
                    choiceTypeList.clear();
                }
                if (choiceTypeOutsideList.size() > 0) {
                    choiceTypeOutsideList.clear();
                }

                if (ApiProviderList.size() > 0) {
                    ApiProviderList.clear();
                }
                if (ApiProviderOutsideList.size() > 0) {
                    ApiProviderOutsideList.clear();
                }

                if (TradeTypeList.size() > 0) {
                    TradeTypeList.clear();
                }
                if (TradeTypeOutsideList.size() > 0) {
                    TradeTypeOutsideList.clear();
                }

                if (TradeStateList.size() > 0) {
                    TradeStateList.clear();
                }

                if (TradeStateOutsideList.size() > 0) {
                    TradeStateOutsideList.clear();
                }

                payTypeList.clear();
                payTypeOutsideList.clear();

                isRef = 0;
                isUnFreezen = 0;

                tradeType.clear();
                payType.clear();
                isMoerRefresh = true;

//                time = "";
                mTimeStr = "";
                if (tv_time != null) {

                    tv_time.setText(R.string.tx_today);
                    tv_time.setTextSize(16);
                } else {
                    tv_time.setTextSize(16);
                    tv_time.setText(R.string.tx_today);
                }

                if (tv_outside_time != null) {
                    tv_outside_time.setText(R.string.tx_today);
                    tv_outside_time.setTextSize(16);
                } else {
                    tv_outside_time.setTextSize(16);
                    tv_outside_time.setText(R.string.tx_today);
                }

                userModel = null;
                tv_cashier_name.setVisibility(View.VISIBLE);
                tv_cashier_name.setText(R.string.tx_bill_stream_cashier);

                tv_outside_cashier_name.setVisibility(View.VISIBLE);
                tv_outside_cashier_name.setText(R.string.tx_bill_stream_cashier);

                //此处要判断是否是card payment的交易通道,来控制收银员的展示不展示
                //判断是收银员还是商户，如果是收银员，且收银员没有账单权限，则不显示cashier这一列筛选项
                if (MainApplication.getInstance().isAdmin(0) && MainApplication.getInstance().isOrderAuth("0")) {
                    ll_outside_choice_cachier.setVisibility(View.GONE);
                    view_cashier_line_outside.setVisibility(View.GONE);

                    ly_but_cachier.setVisibility(View.GONE);
                    view_cashier_line.setVisibility(View.GONE);
                } else {
                    //如果是card payment交易通道，则不展示收银员的选择列表
                    if (isMasterCard) {
                        ll_outside_choice_cachier.setVisibility(View.GONE);
                        view_cashier_line_outside.setVisibility(View.GONE);

                        ly_but_cachier.setVisibility(View.GONE);
                        view_cashier_line.setVisibility(View.GONE);
                    } else {
                        ll_outside_choice_cachier.setVisibility(View.VISIBLE);
                        view_cashier_line_outside.setVisibility(View.VISIBLE);

                        ly_but_cachier.setVisibility(View.VISIBLE);
                        view_cashier_line.setVisibility(View.VISIBLE);
                    }
                }

                //请求对应的列表数据
                loadDateTask(1, true, null, 0);

                if (isPreAuth) {//如果当前选择的是预授权，则请求预授权接口数据
                    getPreAuthDailyStatics();
                } else if (isMasterCard) {//如果选择的是masterCard卡支付通道，则请求卡交易接口数据
                    getMasterCardDailyStatics();
                } else {//如果当前选择的是账单，则请求账单接口数据
                    getBillDailyStatics();
                }

            } else if (msg.what == HandlerManager.PAY_DETAIL_TO_REFRESH) {
                loadDateTask(1, true, getSelectedUserId(), 0);
            } else if (msg.what == HandlerManager.CHOICE_CASHIER) { //收银员---- 在选择Card payment卡交易通道，收银员列表不展示，故不会进这个流程
                try {
                    if (loadNext) { //
                        toastDialog(mActivity, R.string.tx_request_more, null);
                        return;
                    }
                    userModel = (UserModel) msg.obj;
                    if (userModel != null) {
                        tv_cashier_name.setVisibility(View.VISIBLE);
                        tv_cashier_name.setText(userModel.getRealname());

                        tv_outside_cashier_name.setVisibility(View.VISIBLE);
                        tv_outside_cashier_name.setText(userModel.getRealname());

                        if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                            loadDateTask(1, true, getSelectedUserId(), isUnFreezen);
                        } else {
                            //如果是退款
                            if (isRef == 1) {
                                orderList.clear();
                                loadDateTask(1, true, getSelectedUserId(), 0);
                            } else {
                                orderList.clear();
                                loadDateTask(1, true, getSelectedUserId(), 0);
                            }
                        }
                    } else {
                        tv_cashier_name.setVisibility(View.VISIBLE);
                        tv_cashier_name.setText(R.string.tx_bill_stream_cashier);

                        tv_outside_cashier_name.setVisibility(View.VISIBLE);
                        tv_outside_cashier_name.setText(R.string.tx_bill_stream_cashier);

                        if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                            loadDateTask(1, true, null, isUnFreezen);
                        } else {
                            if (isRef == 1) {//如果是退款
                                orderList.clear();
                                loadDateTask(1, true, null, 0);
                            } else {
                                orderList.clear();
                                loadDateTask(1, true, null, 0);
                            }
                        }
                    }
                } catch (Exception e) {
                    tv_cashier_name.setVisibility(View.VISIBLE);
                    tv_cashier_name.setText(R.string.tx_bill_stream_cashier);

                    tv_outside_cashier_name.setVisibility(View.VISIBLE);
                    tv_outside_cashier_name.setText(R.string.tx_bill_stream_cashier);

                    if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                        loadDateTask(1, true, null, isUnFreezen);
                    } else {
                        if (isRef == 1) {//如果是退款
                            orderList.clear();
                            loadDateTask(1, true, null, 0);
                        } else {
                            orderList.clear();
                            loadDateTask(1, true, null, 0);
                        }
                    }
                }
            } else if (msg.what == HandlerManager.BILL_CHOICE_SALE || msg.what == HandlerManager.BILL_CHOICE_PRE_AUTH || msg.what == HandlerManager.BILL_CHOICE_MASTER_CARD) {
                if (msg.what == HandlerManager.BILL_CHOICE_SALE) {
                    isPreAuth = false;
                    isMasterCard = false;
                } else if (msg.what == HandlerManager.BILL_CHOICE_PRE_AUTH) {
                    isPreAuth = true;
                    isMasterCard = false;
                } else if (msg.what == HandlerManager.BILL_CHOICE_MASTER_CARD) {
                    isPreAuth = false;
                    isMasterCard = true;
                }

                setOutsideText();
                setInsideText();

                if (choiceTypeList.size() > 0) {
                    choiceTypeList.clear();
                }

                if (choiceTypeOutsideList.size() > 0) {
                    choiceTypeOutsideList.clear();
                }

                if (ApiProviderList.size() > 0) {
                    ApiProviderList.clear();
                }
                if (ApiProviderOutsideList.size() > 0) {
                    ApiProviderOutsideList.clear();
                }

                if (TradeTypeList.size() > 0) {
                    TradeTypeList.clear();
                }
                if (TradeTypeOutsideList.size() > 0) {
                    TradeTypeOutsideList.clear();
                }

                if (TradeStateList.size() > 0) {
                    TradeStateList.clear();
                }

                if (TradeStateOutsideList.size() > 0) {
                    TradeStateOutsideList.clear();
                }


                payTypeList.clear();
                payTypeOutsideList.clear();

                isRef = 0;
                isUnFreezen = 0;


                tradeType.clear();
                payType.clear();
                isMoerRefresh = true;

//                time = "";
                mTimeStr = "";
                if (tv_time != null) {

                    tv_time.setText(R.string.tx_today);
                    tv_time.setTextSize(16);
                } else {
                    tv_time.setTextSize(16);
                    tv_time.setText(R.string.tx_today);
                }

                if (tv_outside_time != null) {
                    tv_outside_time.setText(R.string.tx_today);
                    tv_outside_time.setTextSize(16);
                } else {
                    tv_outside_time.setTextSize(16);
                    tv_outside_time.setText(R.string.tx_today);
                }


                userModel = null;

                tv_cashier_name.setVisibility(View.VISIBLE);
                tv_cashier_name.setText(R.string.tx_bill_stream_cashier);

                tv_outside_cashier_name.setVisibility(View.VISIBLE);
                tv_outside_cashier_name.setText(R.string.tx_bill_stream_cashier);

                //此处要判断是否是card payment的交易通道,来控制收银员的展示不展示
                //判断是收银员还是商户，如果是收银员，且收银员没有账单权限，则不显示cashier这一列筛选项
                if (MainApplication.getInstance().isAdmin(0) && MainApplication.getInstance().isOrderAuth("0")) {
                    ll_outside_choice_cachier.setVisibility(View.GONE);
                    view_cashier_line_outside.setVisibility(View.GONE);

                    ly_but_cachier.setVisibility(View.GONE);
                    view_cashier_line.setVisibility(View.GONE);
                } else {
                    //如果是card payment交易通道，则不展示收银员的选择列表
                    if (isMasterCard) {
                        ll_outside_choice_cachier.setVisibility(View.GONE);
                        view_cashier_line_outside.setVisibility(View.GONE);

                        ly_but_cachier.setVisibility(View.GONE);
                        view_cashier_line.setVisibility(View.GONE);
                    } else {
                        ll_outside_choice_cachier.setVisibility(View.VISIBLE);
                        view_cashier_line_outside.setVisibility(View.VISIBLE);

                        ly_but_cachier.setVisibility(View.VISIBLE);
                        view_cashier_line.setVisibility(View.VISIBLE);
                    }
                }

                loadDateTask(1, true, null, 0);

                if (isPreAuth) {//如果当前选择的是预授权，则请求预授权接口数据
                    getPreAuthDailyStatics();
                } else if (isMasterCard) {//如果选择的是masterCard卡支付通道，则请求卡交易接口数据
                    getMasterCardDailyStatics();
                } else {//如果当前选择的是账单，则请求账单接口数据
                    getBillDailyStatics();
                }
            }
        }

        ;
    };
    /**
     * 初始化标签
     */
    private String label_year, label_month, label_day;

    private int EndYear = Calendar.getInstance().get(Calendar.YEAR);

    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_bill_stream, container, false);
        bill_sale_or_Pre_auth_or_Card_payment = PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth", "sale");//用来区分预授权还是消费颜色标记
        if (TextUtils.equals(bill_sale_or_Pre_auth_or_Card_payment, "sale")) {
            isPreAuth = false;
            isMasterCard = false;
        } else if (TextUtils.equals(bill_sale_or_Pre_auth_or_Card_payment, "pre_auth")) {
            isPreAuth = true;
            isMasterCard = false;
        } else if (TextUtils.equals(bill_sale_or_Pre_auth_or_Card_payment, "card_payment")) {
            isPreAuth = false;
            isMasterCard = true;
        }

        mTimeLong = System.currentTimeMillis();

        initLabel();
        initView(view);

        return view;
    }

    private void initLabel() {
        label_year = getStringById(R.string.pickerview_year);
        label_month = getStringById(R.string.pickerview_month);
        label_day = getStringById(R.string.pickerview_day);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setLister();
        HandlerManager.registerHandler(HandlerManager.STREAM_BILL, handler);
        HandlerManager.registerHandler(HandlerManager.CHOICE_CASHIER, handler);
        HandlerManager.registerHandler(HandlerManager.BILL_CHOICE_SALE_OR_PRE_AUTH_OR_CARD_PAYMENT, handler);
        if (orderList.size() == 0) {
            orderList.clear();
            loadDateTask(1, true, null, isUnFreezen);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        HandlerManager.unregisterHandler(HandlerManager.STREAM_BILL, handler);
        HandlerManager.unregisterHandler(HandlerManager.CHOICE_CASHIER, handler);
        HandlerManager.unregisterHandler(HandlerManager.BILL_CHOICE_SALE_OR_PRE_AUTH_OR_CARD_PAYMENT, handler);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof spayMainTabActivity) {
            mActivity = (spayMainTabActivity) activity;
        }
    }

    void loadDateTask(int pullType, final boolean isLoadMore, String userId, int isUnFreezen) {

        if (null != selectDatePopupWindow) {
            selectDatePopupWindow.dismiss();
        }
        switch (pullType) {
            case 1:
                loadMore = true;
                pageFulfil = 1;
                if (isMasterCard) {
                    loadCardPaymentDate(isLoadMore, mTimeStr, pageFulfil);
                } else {
                    loadDate(pageFulfil, isLoadMore, isRef, mTimeStr, isUnFreezen, userId);
                }
                break;
            case 2:
                mPullRefreshListView.onRefreshComplete();
                break;
            case 3:
                pageFulfil = pageFulfil + 1;
                if (loadMore) {
                    if (isMasterCard) {
                        loadCardPaymentDate(isLoadMore, mTimeStr, pageFulfil);
                    } else {
                        loadDate(pageFulfil, isLoadMore, isRef, mTimeStr, isUnFreezen, userId);
                    }
                } else {
                    mPullRefreshListView.onRefreshComplete();
                }
                break;
            default:
                break;
        }
    }

    /**
     * 暂停 后 才去请求
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void sleep(long time) {
        try {
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadNext = false;
                        }
                    });
                }

            };
            Timer timer = new Timer();
            timer.schedule(task, time * 1000);
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            return;
        }
    }


    @Override
    public void querySpayOrderNewSuccess(@Nullable ArrayList<Order> model, int page, boolean isLoadMore) {
        isMoerRefresh = true;
        if (page == 1) {
            orderList.clear();
        }

        if (null != model && model.size() > 0) {
            tv_prompt.setVisibility(View.GONE);
            //pageCount = model.get(0).getPageCount();

            reqFeqTime = model.get(0).getReqFeqTime();

            setListData(mPullRefreshListView, orderList, billStreamAdapter, model, isLoadMore, page);
            mActivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    mPullRefreshListView.setVisibility(View.VISIBLE);
                    ly_stream.setVisibility(View.VISIBLE);
                    ll_static_data.setVisibility(View.GONE);
                    ll_outside_float_bar.setVisibility(View.GONE);

                }
            });
            //暂停
            if (reqFeqTime > 0) {
                loadNext = true;
                sleep(reqFeqTime);
            }
            // mySetListData(bill_list, orderList, billStreamAdapter, model, isLoadMore);
        } else {
            loadMore = false;
            if (orderList.size() == 0) {
                mActivity.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        tv_prompt.setVisibility(View.VISIBLE);
                        mPullRefreshListView.setVisibility(View.GONE);
                        ly_stream.setVisibility(View.GONE);
                        ll_static_data.setVisibility(View.VISIBLE);
                        ll_outside_float_bar.setVisibility(View.VISIBLE);

                    }
                });
            } else {
                mPullRefreshListView.onRefreshComplete();
            }
        }
    }


    @Override
    public void querySpayOrderNewFailed(@Nullable Object error) {
        mPullRefreshListView.onRefreshComplete();
        isMoerRefresh = true;
        if (checkSession()) {
            return;
        }
        if (null != error) {
            if (error.toString().startsWith("reqFeqTime")) { // 服务器返回有这个参数，说明服务器有压力，需要暂停reqFeqTime=对应的秒数才去请求返回
                String time = error.toString().substring(error.toString().lastIndexOf("=") + 1);
                if (!StringUtil.isEmptyOrNull(time)) {
                    Integer count = Integer.parseInt(time);
                    loadNext = true;
                    sleep(count);
                }
            } else {
                mActivity.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        if (!StringUtil.isEmptyOrNull(error.toString())) {
                            toastDialog(mActivity, error.toString(), null);
                        } else {
                            toastDialog(mActivity, R.string.tx_load_fail, null);
                        }

                        if (orderList.size() == 0) {
                            tv_prompt.setVisibility(View.VISIBLE);
                            mPullRefreshListView.setVisibility(View.GONE);
                            ly_stream.setVisibility(View.GONE);
                            ll_static_data.setVisibility(View.VISIBLE);
                            ll_outside_float_bar.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }

        }
    }


    void loadDate(final int page, final boolean isLoadMore, int isRefund, String startDate,
                  int isUnFrozen, String userId) {
        if (loadNext) {
            mPullRefreshListView.onRefreshComplete();
            toastDialog(mActivity, R.string.tx_request_more, null);
            return;
        }

        if (mPresenter != null) {
            reqFeqTime = 0;
            if (page == 1) {
                isMoerRefresh = false;
            }
            mPresenter.querySpayOrderNew(reqFeqTime,
                    choiceTypeList,
                    payTypeList,
                    isRefund,
                    page,
                    null,
                    isUnFrozen,
                    userId,
                    startDate,
                    isLoadMore
            );
        }
    }


    @Override
    public void loadCardPaymentDateSuccess(ArrayList<Order> result, int page, boolean isLoadMore) {
        isMoerRefresh = true;

        if (page == 1) {
            orderList.clear();
        }

        if (null != result && result.size() > 0) {
            tv_prompt.setVisibility(View.GONE);
            reqFeqTime = result.get(0).getReqFeqTime();

            setListData(mPullRefreshListView, orderList, billStreamAdapter, result, isLoadMore, page);

            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mPullRefreshListView.setVisibility(View.VISIBLE);
                    ly_stream.setVisibility(View.VISIBLE);
                    ll_static_data.setVisibility(View.GONE);
                    ll_outside_float_bar.setVisibility(View.GONE);
                }
            });
            //暂停
            if (reqFeqTime > 0) {
                loadNext = true;
                sleep(reqFeqTime);
            }
        } else {
            loadMore = false;
            if (orderList.size() == 0) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tv_prompt.setVisibility(View.VISIBLE);
                        mPullRefreshListView.setVisibility(View.GONE);
                        ly_stream.setVisibility(View.GONE);
                        ll_static_data.setVisibility(View.VISIBLE);
                        ll_outside_float_bar.setVisibility(View.VISIBLE);
                    }
                });
            } else {
                mPullRefreshListView.onRefreshComplete();
            }
        }
    }

    @Override
    public void loadCardPaymentDateFailed(@Nullable Object error) {
        mPullRefreshListView.onRefreshComplete();
        isMoerRefresh = true;
        if (checkSession()) {
            return;
        }
        if (null != error) {
            if (error.toString().startsWith("reqFeqTime")) { // 服务器返回有这个参数，说明服务器有压力，需要暂停reqFeqTime=对应的秒数才去请求返回
                String time = error.toString().substring(error.toString().lastIndexOf("=") + 1);
                if (!StringUtil.isEmptyOrNull(time)) {
                    Integer count = Integer.parseInt(time);
                    loadNext = true;
                    sleep(count);
                }
            } else {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!StringUtil.isEmptyOrNull(error.toString())) {
                            toastDialog(mActivity, error.toString(), null);
                        } else {
                            toastDialog(mActivity, R.string.tx_load_fail, null);
                        }

                        if (orderList.size() == 0) {
                            tv_prompt.setVisibility(View.VISIBLE);
                            mPullRefreshListView.setVisibility(View.GONE);
                            ly_stream.setVisibility(View.GONE);
                            ll_static_data.setVisibility(View.VISIBLE);
                            ll_outside_float_bar.setVisibility(View.VISIBLE);

                        }
                    }
                });
            }
        }
    }

    public void loadCardPaymentDate(final boolean isLoadMore, final String startDate, final int page) {
        if (loadNext) {
            mPullRefreshListView.onRefreshComplete();
            toastDialog(mActivity, R.string.tx_request_more, null);
            return;
        }


        if (mPresenter != null) {
            reqFeqTime = 0;
            if (page == 1) {
                isMoerRefresh = false;
            }
            mPresenter.loadCardPaymentDate(ApiProviderList, TradeTypeList,
                    TradeStateList, startDate, page, false, null, isLoadMore);
        }
    }

    /**
     * 判断之前选中的日期和当前的日期是否是同一天
     * 比较方法，分别比较年月日
     *
     * @param selecttime
     * @return
     */
    private boolean isToday(String selecttime) {
        String newDate = DateUtil.formatYYMD(System.currentTimeMillis());
        String date = selecttime;
        String[] arr1 = newDate.split("\\-");
        String[] arr2 = date.split("\\-");
        return arr1[0].equals(arr2[0]) && arr1[1].equals(arr2[1]) && arr1[2].equals(arr2[2]);
    }

    private void setTime(long time) {
        String newDate = DateUtil.formatYYMD(System.currentTimeMillis());
        String date = DateUtil.formatYYMD(time);

        String[] arr1 = newDate.split("\\-");

        String[] arr2 = date.split("\\-");

        if (arr1[0].equals(arr2[0]) && arr1[1].equals(arr2[1]) && arr1[2].equals(arr2[2])) {
            tv_time.setText(R.string.tx_today);
            tv_time.setTextSize(16);

            tv_outside_time.setText(R.string.tx_today);
            tv_outside_time.setTextSize(16);
        } else {
            tv_time.setTextSize(14);
            tv_time.setText(DateUtil.formatYYMD(time));

            tv_outside_time.setTextSize(14);
            tv_outside_time.setText(DateUtil.formatYYMD(time));
        }
    }

    private void setLister() {
        PullToRefreshBase.OnRefreshListener mOnrefreshListener = new PullToRefreshBase.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadDateTask(mPullRefreshListView.getRefreshType(), false, getSelectedUserId(), isUnFreezen);

                if (mPullRefreshListView.getRefreshType() == PullToRefreshBase.MODE_PULL_DOWN_TO_REFRESH) {
                    if (isPreAuth) {//如果当前选择的是预授权，则请求预授权接口数据
                        getPreAuthDailyStatics();
                    } else if (isMasterCard) {//如果当前选择的是卡交易，则请求卡交易接口数据
                        getMasterCardDailyStatics();
                    } else {//如果当前选择的是账单，则请求账单接口数据
                        getBillDailyStatics();
                    }
                }
            }
        };
        mPullRefreshListView.setOnRefreshListener(mOnrefreshListener);

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int lastItemIndex;//当前ListView中最后一个Item的索引
            private int oldVisibleItem;//当前ListView中滑动当前的前一个item的索引

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                lastItemIndex = firstVisibleItem + visibleItemCount - 1 - 1;

                if (firstVisibleItem >= 2) {
                    ll_static_data.setVisibility(View.GONE);
                    ll_outside_float_bar.setVisibility(View.VISIBLE);
                } else {
                    ll_static_data.setVisibility(View.GONE);
                    ll_outside_float_bar.setVisibility(View.GONE);
                }

                if (firstVisibleItem > oldVisibleItem && firstVisibleItem < 10) {
                    // 向上滑动
                    if (firstVisibleItem >= 2) {
                        ll_static_data.setVisibility(View.GONE);
                        ll_outside_float_bar.setVisibility(View.VISIBLE);
                    }
                }
                if (firstVisibleItem <= oldVisibleItem && firstVisibleItem < 10) {
                    // 向下滑动
                    if (firstVisibleItem <= 2) {
                        ll_static_data.setVisibility(View.GONE);
                        ll_outside_float_bar.setVisibility(View.GONE);
                    }
                }
                oldVisibleItem = firstVisibleItem;
            }


            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {


                if (lastItemIndex >= billStreamAdapter.getCount() - 2 - listView.getHeaderViewsCount() + 1) {
                    if (scrollState == SCROLL_STATE_IDLE) {
                        //停止滚动时, 再获取数据
                        Log.i("TAG_ZLZ", "lastItemIndex: " + lastItemIndex);
                        loadDateTask(3, true, getSelectedUserId(), isUnFreezen);
                    }
                }
            }
        });


        tv_prompt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadDateTask(1, false, getSelectedUserId(), isUnFreezen);
            }
        });

        ly_choice_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickSelectData(ly_choice_date);
            }
        });

        ll_outside_choice_date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                clickSelectData(ll_outside_choice_date);
            }
        });

        ly_but_choice.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ClickFilter(ll_outside_choice_date, choiceTypeList, payTypeList, ApiProviderList, TradeTypeList, TradeStateList);
            }
        });

        ll_outside_choice_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClickFilter(ll_outside_choice_date, choiceTypeOutsideList, payTypeOutsideList, ApiProviderOutsideList, TradeTypeOutsideList, TradeStateOutsideList);
            }
        });

        ly_but_cachier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickSelectCashier();

            }
        });
        ll_outside_choice_cachier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickSelectCashier();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                if (position == 1 || position == 2) {
                    return;
                }
                final Order order = orderList.get(position - 1 - listView.getHeaderViewsCount() + 1);
                if (null != order) {
                    if (isMasterCard) {//如果是卡通道，则根据单号去请求订单详情接口
                        if (mPresenter != null) {
                            mPresenter.queryCardPaymentOrderDetails(order.getOutTradeNo(), null);
                        }
                    } else {//维持原来的逻辑，不是卡通道，则进去判断是否是预授权，则分别走不同的详情请求接口，退款状态只有非预授权才有
                        if (isRef == 1) {
                            if (mPresenter != null) {
                                mPresenter.queryRefundDetail(order.getOutRefundNo(), MainApplication.getInstance().getMchId());
                            }
                        } else if (isRef == 0) {
                            String orderNo = "";
                            //如果有预授权权限，同时账单页面选择的是预授权
                            if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                                orderNo = order.getAuthNo();
                            } else {
                                orderNo = order.getOutTradeNo();
                            }

                            if (isUnFreezen == 1) {//如果是解冻，则把列表数据直接带过去
                                // PreAuthActivity.startActivity(mActivity,order,1);//预授权解冻详情
                                PreAuthDetailsActivity.startActivity(mContext, order, 2, 2);//跳预授权转支付详情

                            } else {
                                if (mPresenter != null) {
                                    mPresenter.queryOrderDetail(orderNo, MainApplication.getInstance().getMchId(), true);
                                }
                            }
                        }
                    }
                }
            }
        });
    }


    @Override
    public void queryOrderDetailSuccess(Order result) {
        if (result != null) {
            //如果有预授权权限，同时账单页面选择的是预授权
            if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                PreAuthActivity.startActivity(mActivity, result, 2);//预授权支付详情
            } else {
                OrderDetailsActivity.startActivity(mActivity, result);
            }
        }
    }

    @Override
    public void queryOrderDetailFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }
        if (error != null) {
            toastDialog(mActivity, error.toString(), null);
        }
    }

    @Override
    public void queryRefundDetailSuccess(Order result) {
        if (result != null) {
            RefundRecordOrderDetailsActivity.startActivity(mActivity, result);
        }
    }

    @Override
    public void queryRefundDetailFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }
        if (error != null) {
            toastDialog(mActivity, error.toString(), null);
        }
    }

    @Override
    public void queryCardPaymentOrderDetailsSuccess(Order response) {
        if (response != null) {
            //跳转到卡交易的详情页面
            MasterCardOrderDetailsActivity.startActivity(mActivity, response);
        }
    }

    @Override
    public void queryCardPaymentOrderDetailsFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }
        if (error != null) {
            toastDialog(mActivity, error.toString(), null);
        }
    }

    public void clickSelectData(View view) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(KotlinUtils.INSTANCE.longToDate(mTimeLong));
        SelectDateTimePickerViews selectDateTimePickerViews = new SelectDateTimePickerViews.Builder(mContext, new SelectDateTimePickerViews.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(String date, long time, View v) {
                Log.i("TAG_ZLZ", "选中日期 : " + date);
                setTime(time);
                mTimeStr = date;
                mTimeLong = time;
                orderList.clear();
                loadDateTask(1, true, getSelectedUserId(), isUnFreezen);
            }
        }).setResetText(getStringById(R.string.string_time_filter_reset))//取消按钮文字
                .setConfirmText(getStringById(R.string.string_time_filter_confirm))//确认按钮文字
                .setTitleText(getStringById(R.string.data_title))//标题文字
                .setLabel(label_year, label_month, label_day)
                .isCenterLabel(false)
                .setLanguage(LocaleUtils.isEnglish())
                .setRange(KotlinUtils.INSTANCE.getStartYear(-89), EndYear)
                .setRangDate(KotlinUtils.INSTANCE.getStartDate(-89), Calendar.getInstance())
                .setDate(calendar)
                .build();
        selectDateTimePickerViews.show();

//        selectDatePopupWindow = new SelectDatePopupWindow(mActivity, isPreAuth, time, new SelectDatePopupWindow.HandleBtn() {
//
//            @Override
//            public void handleOkBtn(long time) {
//                if (time == 0) {
//
//                    if (!StringUtil.isEmptyOrNull(FragmentTabBill.this.time)) {
//                        if (isToday(FragmentTabBill.this.time)) {
//                            tv_time.setText(R.string.tx_today);
//                            tv_time.setTextSize(16);
//
//                            tv_outside_time.setText(R.string.tx_today);
//                            tv_outside_time.setTextSize(16);
//                        } else {
//                            tv_time.setTextSize(14);
//                            tv_time.setText(FragmentTabBill.this.time);
//
//                            tv_outside_time.setTextSize(14);
//                            tv_outside_time.setText(FragmentTabBill.this.time);
//
//                        }
//                        orderList.clear();
//                        loadDateTask(1, true, getSelectedUserId(), isUnFreezen);
//                        return;
//                    } else {
//                        time = System.currentTimeMillis();
//                    }
//                }
//                long nowTime = System.currentTimeMillis();
//                if (time > nowTime) {
//                    toastDialog(mActivity, R.string.tx_date, null);
//                    return;
//                }
//                Calendar ca = Calendar.getInstance();//得到一个Calendar的实例  90天
//                ca.add(Calendar.DAY_OF_MONTH, -90);
//
//                if (time <= ca.getTimeInMillis()) {
//                    toastDialog(mActivity, R.string.tx_choice_date, null);
//                    return;
//                }
//                setTime(time);
//                FragmentTabBill.this.time = DateUtil.formatYYMD(time);
//                orderList.clear();
//                loadDateTask(1, true, getSelectedUserId(), isUnFreezen);
//            }
//        });
//        selectDatePopupWindow.showAtLocation(view, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0); //设置layout在PopupWindow中显示的位置

    }

    public void clickSelectCashier() {

        if (userModel != null) {
            UserListActivity.startActivity(mActivity, userModel, true, false, isPreAuth);
        } else {
            Intent intent = new Intent(mActivity, UserListActivity.class);
            intent.putExtra("isFromBillEnter", true);
            intent.putExtra("isFromCodeListEnter", false);
            intent.putExtra("isPreAuth", isPreAuth);
            startActivity(intent);
        }
    }

    public void ClickFilter(View view, List<String> mChoiceTypeList, List<String> mPayTypeList,
                            List<Integer> mApiProviderList, List<Integer> mTradeTypeList, List<Integer> mTradeStateList) {

        if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth", "sale"), "pre_auth")) {
            PreAuthSelectPopupWindow p = new PreAuthSelectPopupWindow(isPreAuth, isHadhandle, mActivity, mChoiceTypeList, mPayTypeList, new PreAuthSelectPopupWindow.HandleBtn() {

                @Override
                public void handleOkBtn(List<String> choiceType, List<String> mPayTypeList, boolean ishandled) {
                    payTypeList = mPayTypeList;
                    choiceTypeList = choiceType;

                    payTypeOutsideList = mPayTypeList;
                    choiceTypeOutsideList = choiceType;

                    orderList.clear();
                    isHadhandle = ishandled;
                    if (choiceType.contains("Unfreezed")) {//如果包含解冻
                        isUnFreezen = 1;
                        dayCountMap.clear();
                        dayTatal.clear();
                        loadDateTask(1, true, getSelectedUserId(), isUnFreezen);
                    } else {
                        dayCountMap.clear();
                        dayTatal.clear();
                        isUnFreezen = 0;

                        loadDateTask(1, true, getSelectedUserId(), isUnFreezen);
                    }
                }
            });

            /**
             * 对话框消失监听
             */
            p.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {

                }
            });
            //设置layout在PopupWindow中显示的位置
            p.showAtLocation(view, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);

        } else if (isMasterCard) {
            //展示CardPayment的弹框
            CardPaymentSelectPopupWindow window = new CardPaymentSelectPopupWindow(mActivity, mApiProviderList, mTradeTypeList, mTradeStateList, new CardPaymentSelectPopupWindow.HandleBtn() {
                @Override
                public void handleOkBtn(List<Integer> apiProviderList, List<Integer> tradeTypeList, List<Integer> tradeStatusList) {
                    ApiProviderList = apiProviderList;
                    TradeTypeList = tradeTypeList;
                    TradeStateList = tradeStatusList;

                    ApiProviderOutsideList = apiProviderList;
                    TradeTypeOutsideList = tradeTypeList;
                    TradeStateOutsideList = tradeStatusList;

                    orderList.clear();

                    dayCountMap.clear();
                    dayTatal.clear();
                    loadDateTask(1, true, null, 0);
                }
            });
            /**
             * 对话框消失监听
             */
            window.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                }
            });
            //设置layout在PopupWindow中显示的位置
            window.showAtLocation(view, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);

        } else {
            SelectPicPopupWindow p = new SelectPicPopupWindow(isHadhandle, mActivity, mChoiceTypeList, mPayTypeList, new SelectPicPopupWindow.HandleBtn() {

                @Override
                public void handleOkBtn(List<String> choiceType, List<String> mPayTypeList, boolean ishandled) {
                    payTypeList = mPayTypeList;
                    choiceTypeList = choiceType;

                    payTypeOutsideList = mPayTypeList;
                    choiceTypeOutsideList = choiceType;

                    orderList.clear();
                    isHadhandle = ishandled;
                    if (choiceType.contains("refund")) {
                        isRef = 1;
                        if (choiceTypeOutsideList.size() > 1) {
                            HandlerManager.notifyMessage(HandlerManager.STREAM_BILL, HandlerManager.PAY_FINISH_REFUND);
                        } else {
                            HandlerManager.notifyMessage(HandlerManager.STREAM_BILL, HandlerManager.STREAM_BILL_REFUND);
                        }
                        loadDateTask(1, true, getSelectedUserId(), 0);
                        dayCountMap.clear();
                        dayTatal.clear();
                    } else if (choiceType.contains("card")) {
                        isRef = 2;
                        loadDateTask(1, true, getSelectedUserId(), 0);
                        dayCountMap.clear();
                        dayTatal.clear();
                    } else {
                        dayCountMap.clear();
                        dayTatal.clear();
                        HandlerManager.notifyMessage(HandlerManager.STREAM_BILL, HandlerManager.STREAM_BILL);
                        isRef = 0;
                        tradeType.clear();
                        payType.clear();
                        KotlinUtils.INSTANCE.parseToJson(choiceType, tradeType, payType);

                        loadDateTask(1, true, getSelectedUserId(), 0);
                    }
                }
            });

            /**
             * 对话框消失监听
             */
            p.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {

                }
            });
            //设置layout在PopupWindow中显示的位置
            p.showAtLocation(view, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
        }
    }

    private void setListData(final PullToRefreshListView pull, List<Order> list,
                             BillStreamAdapter adapter, List<Order> result,
                             boolean isLoadMore, int page) {
        if (!isLoadMore) {
            list.clear();
        }
        list.addAll(result);
        adapter.notifyDataSetChanged();
        pull.onRefreshComplete();
        if (page == 1) {
            listView.setSelection(0);
        }
    }

    private void initView(View view) {
        ly_stream = view.findViewById(R.id.ly_stream);
        tv_prompt = view.findViewById(R.id.tv_prompt);
        tv_prompt.setText(R.string.tv_bill);
        tv_prompt.setTextColor(getResources().getColor(R.color.color_A3A3A3));
        tv_total = view.findViewById(R.id.tv_total);

        ll_outside_float_bar = view.findViewById(R.id.ll_outside_float_bar);
        ll_outside_choice_date = ll_outside_float_bar.findViewById(R.id.ly_choice_date);
        ll_outside_choice_cachier = ll_outside_float_bar.findViewById(R.id.ly_but_cachier);
        ll_outside_choice_filter = ll_outside_float_bar.findViewById(R.id.ly_but_choice);
        tv_outside_time = ll_outside_float_bar.findViewById(R.id.tv_time);
        tv_outside_cashier_name = ll_outside_float_bar.findViewById(R.id.tv_cashier_name);
        view_cashier_line_outside = ll_outside_float_bar.findViewById(R.id.view_cashier_line);
        ll_outside_float_bar.setVisibility(View.GONE);

        //判断是收银员还是商户，如果是收银员，且收银员没有账单权限，则不显示cashier这一列筛选项
        if (MainApplication.getInstance().isAdmin(0) && MainApplication.getInstance().isOrderAuth("0")) {
            ll_outside_choice_cachier.setVisibility(View.GONE);
            view_cashier_line_outside.setVisibility(View.GONE);
        } else {
            //如果是card payment交易通道，则不展示收银员的选择列表
            if (isMasterCard) {
                ll_outside_choice_cachier.setVisibility(View.GONE);
                view_cashier_line_outside.setVisibility(View.GONE);
            } else {
                ll_outside_choice_cachier.setVisibility(View.VISIBLE);
                view_cashier_line_outside.setVisibility(View.VISIBLE);
            }
        }

        ll_static_data = view.findViewById(R.id.ll_static_data);
        ll_static_data.setVisibility(View.GONE);

        ll_outside_bill_sunmmary = ll_static_data.findViewById(R.id.ll_bill_summary);

        tv_outside_bill_title_amount = ll_static_data.findViewById(R.id.tv_bill_title_amount);
        tv_outside_total_amount = ll_static_data.findViewById(R.id.tv_total_amount);
        tv_outside_bill_total_count_title = ll_static_data.findViewById(R.id.tv_bill_total_count_title);
        tv_outside_total_count = ll_static_data.findViewById(R.id.tv_total_count);

        tv_outside_refund_amount_bill_title = ll_static_data.findViewById(R.id.tv_refund_amount_bill_title);
        tv_outside_refund_amount = ll_static_data.findViewById(R.id.tv_refund_amount);
        tv_outside_refund_count_bill_tltle = ll_static_data.findViewById(R.id.tv_refund_count_bill_tltle);
        tv_outside_refund_count = ll_static_data.findViewById(R.id.tv_refund_count);

        tv_outside_net_amount_bill_title = ll_static_data.findViewById(R.id.tv_net_amount_bill_title);
        tv_outside_net_amount = ll_static_data.findViewById(R.id.tv_net_amount);
        tv_outside_daily_ring_ratio_bill_title = ll_static_data.findViewById(R.id.tv_daily_ring_ratio_bill_title);
        tv_outside_daily_ring_ratio = ll_static_data.findViewById(R.id.tv_daily_ring_ratio);

        setOutsideText();

        mPullRefreshListView = view.findViewById(R.id.pullrefresh);
        listView = mPullRefreshListView.getRefreshableView();
        mPullRefreshListView.setHeaderColor(R.color.app_drawer_title);
        mPullRefreshListView.setHeaderTextColor(R.color.white);
        mPullRefreshListView.setFootTextColor(R.color.white);
        mPullRefreshListView.setBackgroundResource(R.color.app_drawer_title);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        headview1 = inflater.inflate(R.layout.bill_list_static_data, null);// 得到加载view
        if (headview1 != null) {
            listView.addHeaderView(headview1);
        }

        LayoutInflater inflater2 = LayoutInflater.from(mContext);
        headview2 = inflater2.inflate(R.layout.item_bill_select, null);// 得到加载view
        if (headview2 != null) {
            listView.addHeaderView(headview2);
        }

        if (headview1 != null) {
            ll_bill_summary = headview1.findViewById(R.id.ll_bill_summary);

            tv_bill_title_amount = headview1.findViewById(R.id.tv_bill_title_amount);
            tv_total_amount = headview1.findViewById(R.id.tv_total_amount);
            tv_bill_total_count_title = headview1.findViewById(R.id.tv_bill_total_count_title);
            tv_total_count = headview1.findViewById(R.id.tv_total_count);

            tv_refund_amount_bill_title = headview1.findViewById(R.id.tv_refund_amount_bill_title);
            tv_refund_amount = headview1.findViewById(R.id.tv_refund_amount);
            tv_refund_count_bill_tltle = headview1.findViewById(R.id.tv_refund_count_bill_tltle);
            tv_refund_count = headview1.findViewById(R.id.tv_refund_count);

            tv_net_amount_bill_title = headview1.findViewById(R.id.tv_net_amount_bill_title);
            tv_net_amount = headview1.findViewById(R.id.tv_net_amount);
            tv_daily_ring_ratio_bill_title = headview1.findViewById(R.id.tv_daily_ring_ratio_bill_title);
            tv_daily_ring_ratio = headview1.findViewById(R.id.tv_daily_ring_ratio);

            setInsideText();

        }

        if (headview2 != null) {
            ly_choice_date = headview2.findViewById(R.id.ly_choice_date);
            tv_time = headview2.findViewById(R.id.tv_time);
            ly_but_cachier = headview2.findViewById(R.id.ly_but_cachier);
            tv_cashier_name = headview2.findViewById(R.id.tv_cashier_name);
            ly_but_choice = headview2.findViewById(R.id.ly_but_choice);
            view_cashier_line = headview2.findViewById(R.id.view_cashier_line);

        }

        //判断是收银员还是商户，如果是收银员，且收银员没有账单权限，则不显示cashier这一列筛选项
        if (MainApplication.getInstance().isAdmin(0) && MainApplication.getInstance().isOrderAuth("0")) {
            ly_but_cachier.setVisibility(View.GONE);
            view_cashier_line.setVisibility(View.GONE);
        } else {
            //如果是card payment交易通道，则不展示收银员的选择列表
            if (isMasterCard) {
                ly_but_cachier.setVisibility(View.GONE);
                view_cashier_line.setVisibility(View.GONE);
            } else {
                ly_but_cachier.setVisibility(View.VISIBLE);
                view_cashier_line.setVisibility(View.VISIBLE);
            }
        }

        billStreamAdapter = new BillStreamAdapter(orderList);
        listView.setAdapter(billStreamAdapter);

        if (isPreAuth) {//如果当前选择的是预授权，则请求预授权接口数据
            getPreAuthDailyStatics();
        } else if (isMasterCard) {//如果当前选择的是卡通道，则请求卡列表的接口数据
            getMasterCardDailyStatics();
        } else {//如果当前选择的是账单，则请求账单接口数据
            getBillDailyStatics();
        }
    }


    public void setOutsideText() {
        //判断是预授权还是非预授权，则当日账单今日汇总数据是显示不同颜色
        if (isPreAuth) {
            ll_outside_bill_sunmmary.setBackgroundResource(R.color.title_bg_pre_auth);
            //判断是预授权还是非预授权，则当日账单今日汇总数据设置每一项的内容不同
            tv_outside_bill_title_amount.setText(getStringById(R.string.report_pre_auth_amount));
            tv_outside_bill_total_count_title.setText(getStringById(R.string.report_pre_auth_count));
            tv_outside_refund_amount_bill_title.setText(getStringById(R.string.report_capture_amount));
            tv_outside_refund_count_bill_tltle.setText(getStringById(R.string.report_capture_count));
            tv_outside_net_amount_bill_title.setText(getStringById(R.string.report_unfreeze_amount));
            tv_outside_daily_ring_ratio_bill_title.setText(getStringById(R.string.report_unfreeze_count));

        } else if (isMasterCard) {
            ll_outside_bill_sunmmary.setBackgroundResource(R.color.app_drawer_title);
            //判断是卡交易通道，则当日账单今日汇总数据设置每一项的内容不同
            tv_outside_bill_title_amount.setText(getStringById(R.string.report_total_amount));
            tv_outside_bill_total_count_title.setText(getStringById(R.string.report_total_count));
            tv_outside_refund_amount_bill_title.setText(getStringById(R.string.report_refund_amount));
            tv_outside_refund_count_bill_tltle.setText(getStringById(R.string.report_refund_count));
            tv_outside_net_amount_bill_title.setText(getStringById(R.string.report_net_amount));
            tv_outside_daily_ring_ratio_bill_title.setText(getStringById(R.string.report_daily_ring_ratio));
        } else {
            ll_outside_bill_sunmmary.setBackgroundResource(R.color.app_drawer_title);
            //判断是预授权还是非预授权，则当日账单今日汇总数据设置每一项的内容不同
            tv_outside_bill_title_amount.setText(getStringById(R.string.report_total_amount));
            tv_outside_bill_total_count_title.setText(getStringById(R.string.report_total_count));
            tv_outside_refund_amount_bill_title.setText(getStringById(R.string.report_refund_amount));
            tv_outside_refund_count_bill_tltle.setText(getStringById(R.string.report_refund_count));
            tv_outside_net_amount_bill_title.setText(getStringById(R.string.report_net_amount));
            tv_outside_daily_ring_ratio_bill_title.setText(getStringById(R.string.report_daily_ring_ratio));
        }
    }

    public void setInsideText() {
        //判断是预授权还是非预授权，则当日账单今日汇总数据是显示不同颜色
        if (isPreAuth) {
            ll_bill_summary.setBackgroundResource(R.color.title_bg_pre_auth);
            //判断是预授权还是非预授权，则当日账单今日汇总数据设置每一项的内容不同
            tv_bill_title_amount.setText(getStringById(R.string.report_pre_auth_amount));
            tv_bill_total_count_title.setText(getStringById(R.string.report_pre_auth_count));
            tv_refund_amount_bill_title.setText(getStringById(R.string.report_capture_amount));
            tv_refund_count_bill_tltle.setText(getStringById(R.string.report_capture_count));
            tv_net_amount_bill_title.setText(getStringById(R.string.report_unfreeze_amount));
            tv_daily_ring_ratio_bill_title.setText(getStringById(R.string.report_unfreeze_count));

        } else if (isMasterCard) {
            ll_bill_summary.setBackgroundResource(R.color.app_drawer_title);
            //判断是卡交易通道，则当日账单今日汇总数据设置每一项的内容不同
            tv_bill_title_amount.setText(getStringById(R.string.report_total_amount));
            tv_bill_total_count_title.setText(getStringById(R.string.report_total_count));
            tv_refund_amount_bill_title.setText(getStringById(R.string.report_refund_amount));
            tv_refund_count_bill_tltle.setText(getStringById(R.string.report_refund_count));
            tv_net_amount_bill_title.setText(getStringById(R.string.report_net_amount));
            tv_daily_ring_ratio_bill_title.setText(getStringById(R.string.report_daily_ring_ratio));

        } else {
            ll_bill_summary.setBackgroundResource(R.color.app_drawer_title);
            //判断是预授权还是非预授权，则当日账单今日汇总数据设置每一项的内容不同
            tv_bill_title_amount.setText(getStringById(R.string.report_total_amount));
            tv_bill_total_count_title.setText(getStringById(R.string.report_total_count));
            tv_refund_amount_bill_title.setText(getStringById(R.string.report_refund_amount));
            tv_refund_count_bill_tltle.setText(getStringById(R.string.report_refund_count));
            tv_net_amount_bill_title.setText(getStringById(R.string.report_net_amount));
            tv_daily_ring_ratio_bill_title.setText(getStringById(R.string.report_daily_ring_ratio));
        }
    }

    private boolean isMove(int position) {
        // 获取当前与下一项
        //        ItemEntity currentEntity = (ItemEntity)getItem(position);
        //          ItemEntity nextEntity = (ItemEntity)getItem(position + 1);
        try {
            Order order = orderList.get(position);
            Order order1 = orderList.get(position + 1);

            if (null == order || null == order1) {
                return true;
            }

            if (isRef == 0) {
                // 获取两项header内容
                //                String currentTitle = DateUtil.formartDateYYMMDD(order.getTradeTimeNew());
                //                String nextTitle = DateUtil.formartDateYYMMDD(order1.getTradeTimeNew());

                String currentTitle = order.getFormatTimePay();
                String nextTitle = order1.getFormatTimePay();
                if (null == currentTitle || null == nextTitle) {
                    return false;
                }

                // 当前不等于下一项header，当前项需要移动了
                if (!currentTitle.equals(nextTitle)) {
                    return true;
                }

            } else if (isRef == 1) { //退款
                String currentTitle = order.getFormatRefund();
                String nextTitle = order1.getFormatRefund();
                if (currentTitle == null || nextTitle == null) {
                    return false;
                }

                if (!currentTitle.equalsIgnoreCase(nextTitle)) {
                    return true;
                }
            } else {
                String currentTitle = order.getFromatCard();
                String nextTitle = order1.getFromatCard();
                if (currentTitle == null || nextTitle == null) {
                    return false;
                }

                if (!currentTitle.equalsIgnoreCase(nextTitle)) {
                    return false;
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
            return true;
        }

        return false;
    }

    @Override
    public void onLoadMoreDate() {

    }

    @Override
    public void onRefresh() {
        pageFulfil = 1;
        if (isMoerRefresh) {
            if (isMasterCard) {
                loadCardPaymentDate(false, mTimeStr, pageFulfil);
            } else {
                loadDate(pageFulfil, false, isRef, mTimeStr, isUnFreezen, getSelectedUserId());
            }
        }
    }

    @Override
    public void onLoadMore() {
        pageFulfil = pageFulfil + 1;
        if (isMasterCard) {
            loadCardPaymentDate(true, mTimeStr, pageFulfil);
        } else {
            loadDate(pageFulfil, true, isRef, mTimeStr, isUnFreezen, getSelectedUserId());
        }
    }

    public String getSelectedUserId() {
        String userId = null;
        if (tv_cashier_name != null && userModel != null) {
            if (!StringUtil.isEmptyOrNull(tv_cashier_name.getText().toString())
                    && !tv_cashier_name.getText().equals(getStringById(R.string.tx_bill_stream_cashier))) {
                userId = String.valueOf(userModel.getId());
            }
        }

        if (tv_outside_cashier_name != null && userModel != null) {
            if (!StringUtil.isEmptyOrNull(tv_outside_cashier_name.getText().toString())
                    && !tv_outside_cashier_name.getText().equals(getStringById(R.string.tx_bill_stream_cashier))) {
                userId = String.valueOf(userModel.getId());
            }
        }

        return userId;
    }


    @Override
    public void queryDailyStaticsSuccess(DailyStatisticsBean response) {
        updateBillDailyHeaderView(response);
    }

    @Override
    public void queryDailyStaticsFailed(@Nullable Object error) {

    }

    /*
     * SPAY 今日数据消费统计
     * 订单列表页当日消费数据
     * */
    public void getBillDailyStatics() {
        if (mPresenter != null) {
            mPresenter.queryDailyStatics();
        }
    }

    public void updateBillDailyHeaderView(DailyStatisticsBean result) {
        if (tv_total_amount != null) {
            tv_total_amount.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(result.getTransactionAmount()));
        }
        if (tv_total_count != null) {
            tv_total_count.setText(result.getTransactionsCount() + "");
        }
        if (tv_refund_amount != null) {
            tv_refund_amount.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(result.getRefundAmount()));
        }
        if (tv_refund_count != null) {
            tv_refund_count.setText(result.getRefundCount() + "");
        }
        if (tv_net_amount != null) {
            tv_net_amount.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(result.getTransactionsRetainedProfits()));
        }
        if (tv_daily_ring_ratio != null) {
            tv_daily_ring_ratio.setText(DateUtil.formatPaseRMBMoney(result.getYesterdayDoD() * 100.00f) + "%");
        }

        //outside
        if (tv_outside_total_amount != null) {
            tv_outside_total_amount.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(result.getTransactionAmount()));
        }
        if (tv_outside_total_count != null) {
            tv_outside_total_count.setText(result.getTransactionsCount() + "");
        }
        if (tv_outside_refund_amount != null) {
            tv_outside_refund_amount.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(result.getRefundAmount()));
        }
        if (tv_outside_refund_count != null) {
            tv_outside_refund_count.setText(result.getRefundCount() + "");
        }
        if (tv_outside_net_amount != null) {
            tv_outside_net_amount.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(result.getTransactionsRetainedProfits()));
        }
        if (tv_outside_daily_ring_ratio != null) {
            tv_outside_daily_ring_ratio.setText(DateUtil.formatPaseRMBMoney(result.getYesterdayDoD() * 100.00f) + "%");
        }
    }

    @Override
    public void queryPreAuthDailyStaticsSuccess(PreAuthDailyStatisticsBean result) {
        if (result != null) {
            updatePreAuthView(result);
        }
    }

    @Override
    public void queryPreAuthDailyStaticsFailed(@Nullable Object error) {

    }

    /*
     * SPAY 今日数据预授权统计
     * 订单列表页当日预授权数据
     * */
    public void getPreAuthDailyStatics() {
        if (mPresenter != null) {
            mPresenter.queryPreAuthDailyStatics();
        }
    }

    public void updatePreAuthView(PreAuthDailyStatisticsBean result) {
        if (tv_total_amount != null) {
            tv_total_amount.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(result.getPreAuthorizationAmount()));
        }
        if (tv_total_count != null) {
            tv_total_count.setText(result.getPreAuthorizationCount() + "");
        }
        if (tv_refund_amount != null) {
            tv_refund_amount.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(result.getReceiptAmount()));
        }
        if (tv_refund_count != null) {
            tv_refund_count.setText(result.getReceiptCount() + "");
        }
        if (tv_net_amount != null) {
            tv_net_amount.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(result.getUnfreezeAmount()));
        }
        if (tv_daily_ring_ratio != null) {
            tv_daily_ring_ratio.setText(result.getUnfreezeCount() + "");
        }

        //outside
        if (tv_outside_total_amount != null) {
            tv_outside_total_amount.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(result.getPreAuthorizationAmount()));
        }
        if (tv_outside_total_count != null) {
            tv_outside_total_count.setText(result.getPreAuthorizationCount() + "");
        }
        if (tv_outside_refund_amount != null) {
            tv_outside_refund_amount.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(result.getReceiptAmount()));
        }
        if (tv_outside_refund_count != null) {
            tv_outside_refund_count.setText(result.getReceiptCount() + "");
        }
        if (tv_outside_net_amount != null) {
            tv_outside_net_amount.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(result.getUnfreezeAmount()));
        }
        if (tv_outside_daily_ring_ratio != null) {
            tv_outside_daily_ring_ratio.setText(result.getUnfreezeCount() + "");
        }

    }


    @Override
    public void queryMasterCardDailyStaticsSuccess(MasterCardDailyStatisticsBean result) {
        if (result != null) {
            updateMasterCardView(result);
        }
    }

    @Override
    public void queryMasterCardDailyStaticsFailed(@Nullable Object error) {

    }

    /*
     * SPAY 今日数据MasterCard通道统计
     * 订单列表页当日MasterCard通道数据
     * 当日流水统计
     * */
    public void getMasterCardDailyStatics() {
        if (mPresenter != null) {
            mPresenter.queryMasterCardDailyStatics();
        }
    }

    public void updateMasterCardView(MasterCardDailyStatisticsBean result) {
        if (tv_total_amount != null) {
            tv_total_amount.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(result.getTransactionAmount()));
        }
        if (tv_total_count != null) {
            tv_total_count.setText(result.getTransactionsCount() + "");
        }
        if (tv_refund_amount != null) {
            tv_refund_amount.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(result.getRefundAmount()));
        }
        if (tv_refund_count != null) {
            tv_refund_count.setText(result.getRefundCount() + "");
        }
        if (tv_net_amount != null) {
            tv_net_amount.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(result.getTransactionsRetainedProfits()));
        }
        if (tv_daily_ring_ratio != null) {
            tv_daily_ring_ratio.setText(DateUtil.formatPaseRMBMoney(result.getYesterdayDoD() * 100.00f) + "%");
        }

        //outside
        if (tv_outside_total_amount != null) {
            tv_outside_total_amount.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(result.getTransactionAmount()));
        }
        if (tv_outside_total_count != null) {
            tv_outside_total_count.setText(result.getTransactionsCount() + "");
        }
        if (tv_outside_refund_amount != null) {
            tv_outside_refund_amount.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(result.getRefundAmount()));
        }
        if (tv_outside_refund_count != null) {
            tv_outside_refund_count.setText(result.getRefundCount() + "");
        }
        if (tv_outside_net_amount != null) {
            tv_outside_net_amount.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(result.getTransactionsRetainedProfits()));
        }
        if (tv_outside_daily_ring_ratio != null) {
            tv_outside_daily_ring_ratio.setText(DateUtil.formatPaseRMBMoney(result.getYesterdayDoD() * 100.00f) + "%");
        }
    }

    @Override
    protected TabBillContract.Presenter createPresenter() {
        return new TabBillPresenter();
    }

    private class BillStreamAdapter extends BaseAdapter {

        private List<Order> orders;

        public BillStreamAdapter() {
        }

        public BillStreamAdapter(List<Order> orders) {
            this.orders = orders;
        }

        @Override
        public int getCount() {
            return orders.size();
        }

        @Override
        public Object getItem(int position) {
            return orders.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(mContext, R.layout.activity_bill_stream_list_item, null);
                holder = new ViewHolder();
                holder.tv_center = (TextView) convertView.findViewById(R.id.tv_center);
                holder.tv_time = (TextView) convertView.findViewById(R.id.tv_time);
                holder.tv_money = (TextView) convertView.findViewById(R.id.tv_money);
                holder.tv_type = (TextView) convertView.findViewById(R.id.tv_type);
                holder.tv_state = (TextView) convertView.findViewById(R.id.tv_state);
                holder.iv_type = (ImageView) convertView.findViewById(R.id.iv_type);
                holder.v_iv = (View) convertView.findViewById(R.id.v_iv);
                holder.iv_cover = (ImageView) convertView.findViewById(R.id.iv_cover);
                holder.iv_cover.setVisibility(View.GONE);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            Order order = orderList.get(position);
            if (null != order) {
                //如果有预授权权限，同时账单页面选择的是预授权
                if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                    if (order.getMoney() > 0) {
                        holder.tv_money.setVisibility(View.VISIBLE);
                        holder.tv_money.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(order.getMoney()));
                    }
                    if (isUnFreezen == 1) {
                        holder.tv_center.setText(StringUtil.paseStrToMarkStr(order.getOutRequestNo()));
                    } else {
                        holder.tv_center.setText(StringUtil.paseStrToMarkStr(order.getAuthNo()));
                    }

                    //解冻
                    if (isUnFreezen == 1) {

                        if (!StringUtil.isEmptyOrNull(order.getOperateTimeFormat())) {
                            try {
                                holder.tv_time.setText(order.getOperateTimeFormat());
                                if (isMove(position)) {
                                    holder.v_iv.setVisibility(View.GONE);
                                } else {
                                    holder.v_iv.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception e) {
                                SentryUtils.INSTANCE.uploadTryCatchException(
                                        e,
                                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                                );
                                Log.e(TAG, Log.getStackTraceString(e));
                            }
                        }

                        holder.tv_type.setVisibility(View.GONE);
                        switch (order.getOperationStatus()) {
                            case 1:
                                //解冻成功
                                holder.tv_state.setText(getContext().getResources().getString(R.string.freezen_success));
                                holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.pay_fail));
                                holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.black));
                                holder.iv_cover.setVisibility(View.GONE);
                                break;
                            case 0:
                            case 2:
                                //解冻失败
                                holder.tv_state.setText(getContext().getResources().getString(R.string.unfreezen_failed));
                                holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                holder.iv_cover.setVisibility(View.GONE);
                                break;
                        }
                    } else {
                        if (!StringUtil.isEmptyOrNull(order.getTradeTimeNew())) {
                            try {
                                holder.tv_time.setText(order.getTradeTimeNew());
                                if (isMove(position)) {
                                    holder.v_iv.setVisibility(View.GONE);
                                } else {
                                    holder.v_iv.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception e) {
                                SentryUtils.INSTANCE.uploadTryCatchException(
                                        e,
                                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                                );
                                Log.e(TAG, Log.getStackTraceString(e));
                            }
                        }

                        holder.tv_type.setVisibility(View.GONE);
                        holder.tv_state.setText(MainApplication.getInstance().getPreAuthTradeStateMap().get(order.getTradeState() + ""));
                        switch (order.getTradeState()) {
                            case 1:
                                //未授权
                                holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                holder.iv_cover.setVisibility(View.VISIBLE);
                                break;
                            case 2:
                                //已授权
                                holder.tv_state.setTextColor(Color.parseColor("#03C700"));
                                holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.black));
                                holder.iv_cover.setVisibility(View.GONE);
                                break;
                            case 3:
                                //已撤销
                                holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                holder.iv_cover.setVisibility(View.VISIBLE);
                                break;
                            default:
                                holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                holder.iv_cover.setVisibility(View.VISIBLE);
                                break;
                        }
                    }

                    Object object = SharedPreUtils.readProduct("payTypeIcon" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
                    if (object != null) {
                        try {
                            Map<String, String> typePicMap = (Map<String, String>) object;
                            if (typePicMap != null && typePicMap.size() > 0) {
                                String picUrl = typePicMap.get("2");
                                if (!StringUtil.isEmptyOrNull(picUrl)) {
                                    if (getContext() != null) {
                                        Glide.with(getContext())
                                                .load(picUrl)
                                                .placeholder(R.drawable.icon_general_receivables)
                                                .error(R.drawable.icon_general_receivables)
                                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                .into(holder.iv_type);
                                    }

                                } else {
                                    holder.iv_type.setImageResource(R.drawable.icon_general_receivables);
                                }
                            }
                        } catch (Exception e) {
                            SentryUtils.INSTANCE.uploadTryCatchException(
                                    e,
                                    SentryUtils.INSTANCE.getClassNameAndMethodName()
                            );
                            Log.e(TAG, Log.getStackTraceString(e));
                        }
                    } else {
                        //如果是预授权列表，则getApiProvider默认写死成2
                        //默认写死支付宝
                        holder.tv_type.setVisibility(View.GONE);
                        holder.iv_type.setImageResource(R.drawable.icon_general_pay);
                    }

                } else if (isMasterCard) {//如果是MasterCard卡交易通道的列表数据
                    if (order.getMoney() > 0) {
                        holder.tv_money.setVisibility(View.VISIBLE);
                        holder.tv_money.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(order.getMoney()));
                    }
                    holder.tv_center.setText(StringUtil.paseStrToMarkStr(order.getOrderNoMch()));

                    if (!StringUtil.isEmptyOrNull(order.getTradeTimeNew())) {
                        holder.tv_time.setText(order.getTradeTimeNew());
                        if (isMove(position)) {
                            holder.v_iv.setVisibility(View.GONE);
                        } else {
                            holder.v_iv.setVisibility(View.VISIBLE);
                        }
                    }
                    //交易状态：(1.初始化，2：成功，3：失败)
                    switch (order.getTradeState()) {
                        case 1:
                            holder.tv_type.setVisibility(View.VISIBLE);
                            holder.tv_type.setText(order.getTradeTypeText());

                            holder.tv_state.setText(order.getTradeStateText());
                            holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                            holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                            holder.iv_cover.setVisibility(View.VISIBLE);
                            break;
                        case 2:
                            holder.tv_type.setVisibility(View.VISIBLE);
                            holder.tv_type.setText(order.getTradeTypeText());

                            holder.tv_state.setText(order.getTradeStateText());
                            holder.tv_state.setTextColor(Color.parseColor("#03C700"));
                            holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.black));
                            holder.iv_cover.setVisibility(View.GONE);
                            break;
                        case 3:
                            holder.tv_type.setVisibility(View.VISIBLE);
                            holder.tv_type.setText(order.getTradeTypeText());

                            holder.tv_state.setText(order.getTradeStateText());
                            holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                            holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                            holder.iv_cover.setVisibility(View.VISIBLE);
                            break;
                        default:
                            holder.tv_type.setVisibility(View.VISIBLE);
                            holder.tv_type.setText(order.getTradeTypeText());

                            holder.tv_state.setText(order.getTradeStateText());
                            holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                            holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                            holder.iv_cover.setVisibility(View.VISIBLE);
                            break;
                    }

                    Object object = SharedPreUtils.readProduct("payTypeIcon" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
                    if (object != null) {
                        try {
                            Map<String, String> typePicMap = (Map<String, String>) object;
                            if (typePicMap != null && typePicMap.size() > 0) {
                                String picUrl = typePicMap.get(order.getApiProvider() + "");
                                if (!StringUtil.isEmptyOrNull(picUrl)) {
                                    if (getContext() != null) {
                                        Glide.with(getContext())
                                                .load(picUrl)
                                                .placeholder(R.drawable.icon_general_receivables)
                                                .error(R.drawable.icon_general_receivables)
                                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                .into(holder.iv_type);
                                    }
                                } else {
                                    holder.iv_type.setImageResource(R.drawable.icon_general_receivables);
                                }
                            }
                        } catch (Exception e) {
                            SentryUtils.INSTANCE.uploadTryCatchException(
                                    e,
                                    SentryUtils.INSTANCE.getClassNameAndMethodName()
                            );
                            Log.e(TAG, Log.getStackTraceString(e));
                        }
                    } else {
                        holder.iv_type.setImageResource(R.drawable.icon_general_receivables);
                    }
                } else { //否则走原来的逻辑
                    if (order.getMoney() > 0) {
                        holder.tv_money.setVisibility(View.VISIBLE);
                        holder.tv_money.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(order.getMoney()));
                    }

                    holder.tv_center.setText(StringUtil.paseStrToMarkStr(order.getOrderNoMch()));

                    if (isRef == 1) {
                        holder.tv_type.setVisibility(View.GONE);
                        holder.tv_center.setText(StringUtil.paseStrToMarkStr(order.getRefundNo()));

                        try {
                            holder.tv_time.setText(order.getAddTimeNew());
                            if (isMove(position)) {
                                holder.v_iv.setVisibility(View.GONE);
                            } else {
                                holder.v_iv.setVisibility(View.VISIBLE);
                            }

                            holder.tv_state.setText(MainApplication.getInstance().getRefundStateMap().get(order.getRefundState() + ""));
                            //0:审核中 1:退款成功 2:退款失败  3:退款成功 5:退款成功
                            switch (order.getRefundState()) {
                                case 0:
                                    //审核中
                                    holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.paytype_title_reset));
                                    holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.black));
                                    holder.iv_cover.setVisibility(View.GONE);
                                    break;
                                case 1:
                                case 3:
                                case 5:
                                    //退款成功
                                    holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.pay_fail));
                                    holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.black));
                                    holder.iv_cover.setVisibility(View.GONE);
                                    break;
                                case 2:
                                    //退款失败
                                    holder.tv_state.setText(R.string.refund_failure);
                                    holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                    holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                    holder.iv_cover.setVisibility(View.GONE);
                                    break;
                                default:
                                    holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                    holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                    holder.iv_cover.setVisibility(View.VISIBLE);
                                    break;
                            }
                        } catch (Exception e) {
                            SentryUtils.INSTANCE.uploadTryCatchException(
                                    e,
                                    SentryUtils.INSTANCE.getClassNameAndMethodName()
                            );
                            Log.e(TAG, Log.getStackTraceString(e));
                        }
                    } else if (isRef == 0) {
                        holder.tv_type.setVisibility(View.GONE);
                        if (!StringUtil.isEmptyOrNull(order.getTradeTimeNew())) {
                            try {
                                holder.tv_time.setText(order.getTradeTimeNew());
                                if (isMove(position)) {
                                    holder.v_iv.setVisibility(View.GONE);
                                } else {
                                    holder.v_iv.setVisibility(View.VISIBLE);
                                }

                                holder.tv_state.setText(MainApplication.getInstance().getTradeTypeMap().get(order.getTradeState() + ""));
                                switch (order.getTradeState()) {
                                    case 2:
                                        //支付成功
                                        holder.tv_state.setTextColor(Color.parseColor("#03C700"));
                                        holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.black));
                                        holder.iv_cover.setVisibility(View.GONE);
                                        break;
                                    case 1:
                                        //未支付
                                        holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                        holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                        holder.iv_cover.setVisibility(View.VISIBLE);
                                        break;
                                    case 3:
                                        //关闭
                                        holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                        holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                        holder.iv_cover.setVisibility(View.VISIBLE);
                                        break;
                                    case 4:
                                        //转入退款
                                        holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.pay_fail));
                                        holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.black));
                                        holder.iv_cover.setVisibility(View.GONE);
                                        break;
                                    case 8:
                                        //已撤销
                                        holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                        holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                        holder.iv_cover.setVisibility(View.VISIBLE);
                                        break;
                                    default:
                                        holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                        holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                        holder.iv_cover.setVisibility(View.VISIBLE);
                                        break;
                                }

                            } catch (Exception e) {
                                SentryUtils.INSTANCE.uploadTryCatchException(
                                        e,
                                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                                );
                                Log.e(TAG, Log.getStackTraceString(e));
                            }
                        }
                    } else {
                        holder.tv_time.setText(order.getUseTimeNew());
                        holder.tv_money.setVisibility(View.INVISIBLE);
                        holder.tv_type.setVisibility(View.GONE);
                        holder.iv_type.setImageResource(R.drawable.icon_pop_coupon);
                        if (isMove(position)) {
                            holder.v_iv.setVisibility(View.GONE);
                        } else {
                            holder.v_iv.setVisibility(View.VISIBLE);
                        }
                        //卡券
                        holder.tv_state.setText(R.string.tx_affirm_succ);
                        holder.tv_state.setTextColor(Color.parseColor("#666666"));
                    }

                    if (isRef == 0 || isRef == 1) {

                        Object object = SharedPreUtils.readProduct("payTypeIcon" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
                        if (object != null) {
                            try {

                                Map<String, String> typePicMap = (Map<String, String>) object;
                                if (typePicMap != null && typePicMap.size() > 0) {
                                    String picUrl = typePicMap.get(order.getApiProvider() + "");
                                    if (!StringUtil.isEmptyOrNull(picUrl)) {
                                        if (getContext() != null) {
                                            Glide.with(getContext())
                                                    .load(picUrl)
                                                    .placeholder(R.drawable.icon_general_receivables)
                                                    .error(R.drawable.icon_general_receivables)
                                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                    .into(holder.iv_type);
                                        }

                                    } else {
                                        holder.iv_type.setImageResource(R.drawable.icon_general_receivables);
                                    }
                                }
                            } catch (Exception e) {
                                SentryUtils.INSTANCE.uploadTryCatchException(
                                        e,
                                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                                );
                                Log.e(TAG, Log.getStackTraceString(e));
                            }
                        } else {

                            switch (order.getApiProvider()) {
                                case 1:
                                    //微信
                                    holder.iv_type.setImageResource(R.drawable.icon_general_wechat);
                                    break;
                                case 2:
                                    //支付宝
                                    holder.iv_type.setImageResource(R.drawable.icon_general_pay);
                                    break;
                                case 12:
                                    //京东
                                    holder.iv_type.setImageResource(R.drawable.icon_general_jingdong);
                                    break;
                                case 4:
                                    //qq
                                    holder.iv_type.setImageResource(R.drawable.icon_general_qq);
                                    break;
                                default:
                                    holder.iv_type.setImageResource(R.drawable.icon_general_receivables);
                                    break;
                            }
                        }
                    }
                }
            }
            return convertView;
        }
    }

    private class ViewHolder {
        private TextView tv_time, tv_center, tv_money, tv_type, tv_state;

        private ImageView iv_type;

        private ImageView iv_cover;

        private View v_iv;
    }
}
