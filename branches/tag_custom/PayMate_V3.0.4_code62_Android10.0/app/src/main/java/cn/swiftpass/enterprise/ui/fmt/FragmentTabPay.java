package cn.swiftpass.enterprise.ui.fmt;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.tencent.stat.StatService;

import net.tsz.afinal.FinalBitmap;

import org.xclcharts.common.DensityUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.upgrade.UpgradeManager;
import cn.swiftpass.enterprise.bussiness.model.QRcodeInfo;
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.NoteMarkActivity;
import cn.swiftpass.enterprise.ui.activity.PayActivity;
import cn.swiftpass.enterprise.ui.activity.ShowQRcodeActivity;
import cn.swiftpass.enterprise.ui.activity.UpgradeDailog;
import cn.swiftpass.enterprise.ui.activity.scan.CaptureActivity;
import cn.swiftpass.enterprise.ui.activity.spayMainTabActivity;
import cn.swiftpass.enterprise.ui.view.MyTextView;
import cn.swiftpass.enterprise.ui.widget.CommonConfirmDialog;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.ProgressInfoDialog;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DataReportUtils;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.FileUtils;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.NetworkUtils;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.Utils;
import handmark.pulltorefresh.library.PullToRefreshBase;
import handmark.pulltorefresh.library.PullToRefreshScrollView;
import handmark.pulltorefresh.library.internal.LoadingLayout;

/**
 * Created by aijingya on 2018/5/2.
 *
 * @Package cn.swiftpass.enterprise.ui.fmt
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/5/2.17:26.
 */

public class FragmentTabPay extends BaseFragment implements View.OnClickListener{
    private static final String TAG = FragmentTabPay.class.getSimpleName();
    private spayMainTabActivity mActivity;
    private MyTextView etMoney;

    private TextView tvMoneyUppercase;

    private String orderCode = "";

    //private Activity act;
    private String sum = ""; //依次输入的金额的字符串。如果碰到两个+号就把前面的第一个+号前后的字符串拼接求和之后再拼接+号
    private String two = "";


    //小费的编辑输入
    private String sumTips = "";
    private String twoTips = "";
    private String strTotalMoney;
    private String strSurchargelMoney;
    private String strTipMoney = "";
    private boolean isSubmitData = false;
    public static final int REQUEST_2CODE = 0x12;
    public static final int REQUEST_EPAYLINKS = 0x13;
    private SharedPreferences sp;
    private String userName;
    /**
     * 新增按开通的支付方式显示下拉汇率显示
     */
    private String serviceType = "";
    //优惠劵图片
    private ImageView ivPictureRotating, right_icon_ashing_jd, left_icon_ashing_jd, right_alipay, zfb_left_img;
    private View view, wxView, zfbView, qqView, jdView;
    //统计时间
    //private int isClickNumer;
    private int lastIndex;

    private TextView btn_ic_money, id_title_surcharge;

    private ImageView buttonPoint, button1, button2, button3, button4, button5, button6, button7, button8, button9, button0;

    private LinearLayout ivInputNumberDel;

    private LinearLayout onClickListener1, onClickListener2, onClickListener3,
            onClickListener4, onClickListener5, onClickListener6, onClickListener7,
            onClickListener8, onClickListener9, onClickListenerPoint, onClickListenerClear,
            onClickListener0,id_lin_add;

    private LinearLayout lay_Clear, ly_one, ly_two, ly_three;

    private List<View> views;

    private ViewPager viewPager;

    private String payType = MainApplication.PAY_WX_MICROPAY;
    private List<String> payMeths = new ArrayList<String>();
    private LinearLayout cib_lay;
    private ViewFlipper pay_left_flipper, pay_right_flipper;
    private AlertDialog dialogInfo;
    private boolean isStop = true; // 强行中断交易
    private ImageView right_icon_qq, right_icon_wechat, left_icon_qq, left_icon_wechat, iv_icon;
    private FrameLayout lay_rotating;
    private TextView prompt_txt, tx_time,tv_money_info;
    TextView tv_pase;
    private FinalBitmap finalBitmap;
    private LinearLayout ly_to_scan_pay;
    private String payNatieType = MainApplication.PAY_WX_NATIVE;
    boolean setDefPayMethod = true; //设置默认支付方式
    private PayActivity.MyAdapter myAdapter;
    private boolean isWitch = true; // 是否切换
    private FrameLayout lay_notify; //显示公告
    private TextView id_tips, id_title_addtips;
    private String noticeId = "";
    private ImageView iv_clear, button10, tv_reduce;
    LinearLayout onClickListener10;
    private RelativeLayout id_rel_surcharge, id_rel_tip;
    private View id_line_second, id_line_first;
    private LinearLayout ll_textview_view;

    private String maxNum = "999999999.";
    private double maxMoney;
    private boolean isPay = true; //默认是收款
    private boolean mLoadRateSuccess = false;//获取附加税率接口
    private  double MAX_FEE = 50000d;
    private double MAX_SALSE_VALUE = 0;//根据税额
    private int currentMode = 0;//1代表输入小费模式 0 其他
    ProgressInfoDialog dialog1;
    private String mark = "";
    private TextView mWXExchange,mAliExchange;
    private LinearLayout mWXpayLayout,mAlipayLayout;
    String message = "";
//    private PullToRefreshScrollView mScrollView;
    TextView tv_content;
    private LinearLayout mHasFeeLayout;
    private LinearLayout mNoSurchargeMarkLayout;
    private LinearLayout mHasSurchargeMarkLayout;
    private TextView tv_nosurcharge,tv_hassurcharge;
    private LinearLayout mSurchargeDetailLayout;
    private LinearLayout ly_pay;
    private LinearLayout ly_refresh;
    private double minus = 1;
    private String saleOrPreauth;          //上一次选择的是sale 还是 pre_auth,默认是sale

    @Override
    protected int getLayoutId() {
        return 0;
    }


    @Override
    protected boolean isLoginRequired() {
        return super.isLoginRequired();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_pay_new, container, false);
        if(MainApplication.isPre_authOpen == 0){//如果未开通预授权通道，则清除title
            mActivity.cleanTitleAllView();
        }
        finalBitmap = FinalBitmap.create(mContext);
        try {
            finalBitmap.configDiskCachePath(FileUtils.getAppCache());
        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }

        sp = mContext.getSharedPreferences("login", 0);
        userName = sp.getString("user_name", "");

        HandlerManager.registerHandler(HandlerManager.PAY_SET_PAY_METHOD, handler);
        HandlerManager.registerHandler(HandlerManager.PAY_FINISH, handler);
        HandlerManager.registerHandler(HandlerManager.CHOICE_SALE_OR_PRE_AUTH, handler);
//        serviceType = MainApplication.serviceType;
        serviceType = MainApplication.activateServiceType;

        maxMoney = Double.parseDouble(maxNum);
        //如果币种的最小单位为1，则不让输入小数点
        if(MainApplication.numFixed == 0){
            maxMoney = Double.parseDouble(maxNum);
        }else { //如果输入的金额带小数点
            for(int i = 0 ; i< MainApplication.numFixed ; i++){
                maxNum = maxNum+"9";
            }
            maxMoney = Double.parseDouble(maxNum);
        }

        //根据当前的小数点的位数来判断应该除以或者乘以多少
        for(int i = 0 ; i < MainApplication.numFixed ; i++ ){
            minus = minus * 10;
        }
        initView(view);
//        mScrollView.getRefreshableView();
        setLister();

        if (MainApplication.isUpdateShow) {
            if (NetworkUtils.isNetWorkValid(mContext)) {
                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                checkVersion();

                            }
                        });
                    }
                };
                Timer timer = new Timer();
                timer.schedule(task, 200);
            } else {
                DialogInfo dialogInfo = new DialogInfo(mContext, getString(R.string.no_network), getString(R.string.to_open_network), getString(R.string.to_open), DialogInfo.NETWORKSTATUE, new DialogInfo.HandleBtn() {

                    @Override
                    public void handleOkBtn() {
                        Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                        mContext.startActivity(intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                    }

                    @Override
                    public void handleCancleBtn() {
                    }
                }, null);
                DialogHelper.resize(mContext, dialogInfo);
                dialogInfo.show();

            }
        }

        return view;
    }

    public Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case 0:
                    if (etMoney != null) {
                        clearData();
                    }
                    break;
                // 设置默认支付方式
                case HandlerManager.PAY_SET_PAY_METHOD:
                    String typeStr = (String) msg.obj;
                    if (viewPager != null) {
                        setDefPayMethod = false;
                        setPayMehotd(typeStr);
                    }
                    break;
                    // 设置默认支付方式
                case HandlerManager.PAY_FINISH:
                    if (etMoney != null) {
                        clearData();
                    }
                    NoteMarkActivity.setNoteMark("");
                    mark = NoteMarkActivity.getNoteMark();
                    break;
                case HandlerManager.CHOICE_SALE:
                    //更改当前界面的颜色
                    runOnUiThreadSafety(new Runnable() {
                        @Override
                        public void run() {
                            if(ll_textview_view != null){
                                ll_textview_view.setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
                            }

//                            if(mScrollView != null){
//                                mScrollView.setBackgroundColor(getActivity().getResources().getColor(R.color.app_drawer_title));
//                            }

                            if (ly_to_scan_pay!=null){//预授权 改变扫码按钮颜色
                                ly_to_scan_pay.setBackgroundResource(R.drawable.ly_scan);
                            }

                            tv_money_info.setText(mActivity.getString(R.string.tx_pay_money));

                            if(!MainApplication.isSurchargeOpen()){
                                if(id_rel_surcharge != null){
                                    id_rel_surcharge.setVisibility(View.GONE);
                                }

                                if(mHasFeeLayout != null){
                                    mHasFeeLayout.setVisibility(View.INVISIBLE);
                                }

                                if(mNoSurchargeMarkLayout != null){
                                    mNoSurchargeMarkLayout.setVisibility(View.VISIBLE);
                                }

                                if(mHasSurchargeMarkLayout != null){
                                    mHasSurchargeMarkLayout.setVisibility(View.GONE);
                                }

                            }else{
                                if(id_rel_surcharge != null){
                                    id_rel_surcharge.setVisibility(View.VISIBLE);
                                }

                                if(mHasFeeLayout != null){
                                    mHasFeeLayout.setVisibility(View.VISIBLE);
                                }
                                if(mNoSurchargeMarkLayout != null){
                                    mNoSurchargeMarkLayout.setVisibility(View.GONE);
                                }
                                if(mHasSurchargeMarkLayout != null){
                                    mHasSurchargeMarkLayout.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    });
                    break;
                    case HandlerManager.CHOICE_PRE_AUTH:
                        runOnUiThreadSafety(new Runnable() {
                            @Override
                            public void run() {
                                if(ll_textview_view != null){
                                    ll_textview_view.setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
                                }

                            /*    if(mScrollView != null){
                                    mScrollView.setBackgroundColor(getActivity().getResources().getColor(R.color.title_bg_pre_auth));
                                }*/

                                if (ly_to_scan_pay!=null){//预授权 改变扫码按钮颜色
                                    ly_to_scan_pay.setBackgroundResource(R.drawable.ly_scan_pre_auth);
                                }


                                tv_money_info.setText(mActivity.getString(R.string.pre_auth_amount));

                                //预授权模式下，不管是否开启预授权功能，默认不展示预授权的显示
                                if(id_rel_surcharge != null){
                                    id_rel_surcharge.setVisibility(View.GONE);
                                }

                                if(mHasFeeLayout != null){
                                    mHasFeeLayout.setVisibility(View.INVISIBLE);
                                }

                                if(mNoSurchargeMarkLayout != null){
                                    mNoSurchargeMarkLayout.setVisibility(View.VISIBLE);
                                }

                                if(mHasSurchargeMarkLayout != null){
                                    mHasSurchargeMarkLayout.setVisibility(View.GONE);
                                }
                            }
                        });
                        break;
                default:
                    break;
            }
        }

    };

    @Override
    public void onDestroyView() {
        super.onDestroy();
        HandlerManager.unregisterHandler(HandlerManager.CHOICE_SALE_OR_PRE_AUTH, handler);
    }



    @Override
    public void onResume() {
        super.onResume();
        if (isSubmitData) {
            clearData();
            isSubmitData = false;
        }
        String exchange = MainApplication.getExchangeRate();
        if(!TextUtils.isEmpty(exchange)){
            setPayExchange(ALI);
            setPayExchange(WX);
        }else{
            if (!mLoadRateSuccess) {
                //获取附加税率等信息
                getExchangeRate();
            }
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof spayMainTabActivity){
            mActivity=(spayMainTabActivity) activity;
        }
    }

    private static final int WX = 0x12;
    private static final int ALI = 0x13;
    private void setPayExchange(int TYPE){
        if (MainApplication.getSurchargeRate() == -1) {
            //附加手续费率 不存在
        }
        if (MainApplication.getTaxRate() == -1) {
            //预扣税费率 不存在
        }
        if (MainApplication.getVatRate() == -1) {
            // 增值税率  不存在
        }
        //判断是否有DCC
        BigDecimal b2 = new BigDecimal(MainApplication.getExchangeRate());
        BigDecimal usdtoRmb = new BigDecimal(MainApplication.getUsdToRmbExchangeRate());
        BigDecimal usd = new BigDecimal(MainApplication.getSourceToUsdExchangeRate());
        BigDecimal ali = new BigDecimal(MainApplication.getAlipayPayRate());
        StringBuffer sb = new StringBuffer();
        sb.append("1 ")
                .append(MainApplication.feeType)
                .append(" = ");
        String exchange = "";
        //支付宝汇率判断
        if (TYPE == ALI){
            exchange = getBigDecimalDouble(ali);
            sb.append(exchange);
        }else{
            //微信汇率判断
            if (usdtoRmb.doubleValue() != 0 && usd.doubleValue() != 0) {
                //含dcc功能
//                        MAX_SALSE_VALUE = (int) (50000.0 / usdtoRmb.doubleValue() / usd.doubleValue() / (1 + b2.doubleValue())) * 100;
//                        MAX_SALSE_VALUE = (int) ((50000.00d / usdtoRmb.doubleValue() / usd.doubleValue())*100);

                BigDecimal tep = new BigDecimal(MAX_FEE).divide(usdtoRmb,12,BigDecimal.ROUND_HALF_UP).
                        divide(usd,12,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(minus));

                MAX_SALSE_VALUE = tep.doubleValue();
                //增加费率展示
                exchange = getBigDecimalDouble(usd.multiply(usdtoRmb));
                sb.append(exchange);
            } else {
                if (0.0 != b2.doubleValue()){
                    MAX_SALSE_VALUE = ((MAX_FEE / b2.doubleValue()) * minus);
                    BigDecimal tep = new BigDecimal(MAX_FEE).divide(b2,12,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(minus));
                    MAX_SALSE_VALUE = tep.doubleValue();
                    //增加费率展示
                    exchange = getBigDecimalDouble(b2);
                    sb.append(exchange);
                }else{
                    //增加费率展示
                    exchange = getBigDecimalDouble(b2);
                    sb.append(exchange);
                }
            }
        }

        sb.append(" CNY");
        /*if (TYPE == ALI){
            mAliExchange.setText(sb.toString());
        }else{
            mWXExchange.setText(sb.toString());
        }*/
        if (!MainApplication.isSurchargeOpen()) {
            id_rel_surcharge.setVisibility(View.GONE);
        } else {
            //如果开通预授权，并且选择是预授权，手动关闭surcharge
            if(MainApplication.isPre_authOpen == 1 &&  TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth","sale"),"pre_auth")){
                id_rel_surcharge.setVisibility(View.GONE);
            }else{
                id_rel_surcharge.setVisibility(View.VISIBLE);
            }
        }

        if (MAX_SALSE_VALUE <= 0) {
            MAX_SALSE_VALUE = MAX_FEE*minus;
        }

//                    if (!MainApplication.isTaxRateOpen()) {
//
//                    }
        if (!MainApplication.isTipOpen()) {
            id_rel_tip.setVisibility(View.GONE);
            id_line_second.setVisibility(View.GONE);
        } else {
            id_rel_tip.setVisibility(View.VISIBLE);
            id_line_second.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 获取实时费率
     * getExchangeRate:
     *
     * @author admin
     */
    private void getExchangeRate() {
        LocalAccountManager.getInstance().getExchangeRate(new UINotifyListener<Boolean>() {
            @Override
            public void onSucceed(Boolean result) {
                super.onSucceed(result);
                if (result) {
                    mLoadRateSuccess = true;
                    setPayExchange(ALI);
                    setPayExchange(WX);
                }

            }
        });
    }

    /**
     * 数据保留六位小数并四舍五入
     * @param doublevalues
     * @return
     */
    private String getBigDecimalDouble(BigDecimal doublevalues){
        BigDecimal values = doublevalues;
        if (0.0 != values.doubleValue()){
            String exchange = values.setScale(6,BigDecimal.ROUND_HALF_UP).toPlainString();
            return exchange;
        }
        return "0.000000";
    }

    private void initView(View view ) {
        tv_pase = view.findViewById(R.id.tv_pase);
        id_line_second = view.findViewById(R.id.id_line_second);
        id_line_first = view.findViewById(R.id.id_line_first);
        id_rel_surcharge = view.findViewById(R.id.id_rel_surcharge);
        id_rel_tip = view.findViewById(R.id.id_rel_tip);
        ll_textview_view = view.findViewById(R.id.ll_textview_view);
        tv_money_info = view.findViewById(R.id.tv_money_info);

        iv_icon = view.findViewById(R.id.iv_icon);
        id_title_addtips = view.findViewById(R.id.id_title_addtips);

        button10 = view.findViewById(R.id.button10);

        id_title_surcharge = view.findViewById(R.id.id_change_surcharge);
        onClickListener10 = view.findViewById(R.id.onClickListener10);
        id_tips = view.findViewById(R.id.id_tips);
        ly_one = view.findViewById(R.id.ly_one);

        ly_three = view.findViewById(R.id.ly_three);
        iv_clear = view.findViewById(R.id.iv_clear);

        right_icon_qq = view.findViewById(R.id.right_icon_qq);
        right_icon_wechat = view.findViewById(R.id.right_icon_wechat);
        left_icon_qq = view.findViewById(R.id.left_icon_qq);
        left_icon_wechat = view.findViewById(R.id.left_icon_wechat);
        right_alipay = view.findViewById(R.id.right_alipay);
        zfb_left_img = view.findViewById(R.id.zfb_left_img);
        right_icon_ashing_jd = view.findViewById(R.id.right_icon_ashing_jd);
        left_icon_ashing_jd = view.findViewById(R.id.left_icon_ashing_jd);
        btn_ic_money = view.findViewById(R.id.btn_ic_money);
        btn_ic_money.setText(MainApplication.feeFh);
        lay_Clear = view.findViewById(R.id.lay_Clear);
        tx_time = view.findViewById(R.id.tx_time);
        prompt_txt = view.findViewById(R.id.prompt_txt);
        lay_rotating = view.findViewById(R.id.lay_rotating);
        pay_left_flipper = view.findViewById(R.id.pay_left_flipper);
        pay_right_flipper = view.findViewById(R.id.pay_right_flipper);
        id_lin_add = view.findViewById(R.id.id_lin_add);
        etMoney = view.findViewById(R.id.et_money);
        etMoney.setText(getDefaultValue());
        etMoney.setFocusable(true);
       //设置成默认的值0
        tv_pase.setText(MainApplication.feeFh + getDefaultValue());
        //set the listener of MyTextView
        etMoney.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String txMoeny = s.toString();
                if(TextUtils.isEmpty(txMoeny) || txMoeny.equals("+") || txMoeny.equals(".") || StringToMoney(txMoeny) == 0  ){
                    ly_to_scan_pay.setEnabled(false);
                } else {
                    ly_to_scan_pay.setEnabled(true);
                }
            }
        });

        tvMoneyUppercase = view.findViewById(R.id.tv_money_uppercase);
        ivPictureRotating = view.findViewById(R.id.iv_n_picture_rotating);
        cib_lay = view.findViewById(R.id.cib_lay);
        ivInputNumberDel = view.findViewById(R.id.fht_iv_input_number_del);
        ivInputNumberDel.setOnClickListener(this);
        button1 = view.findViewById(R.id.button1);
        button2 = view.findViewById(R.id.button2);
        button3 = view.findViewById(R.id.button3);
        button4 = view.findViewById(R.id.button4);
        button5 = view.findViewById(R.id.button5);
        button6 = view.findViewById(R.id.button6);
        button7 = view.findViewById(R.id.button7);
        button8 = view.findViewById(R.id.button8);
        button9 = view.findViewById(R.id.button9);
        buttonPoint = view.findViewById(R.id.buttonPoint);
        ly_to_scan_pay = view.findViewById(R.id.ly_to_scan_pay);
        ly_to_scan_pay.setEnabled(false);

        button0 = view.findViewById(R.id.button0);
        onClickListener1 = view.findViewById(R.id.onClickListener1);
        setBgHeiht(ly_one);
        setBgHeiht(ly_three);
        onClickListener2 = view.findViewById(R.id.onClickListener2);
        onClickListener3 = view.findViewById(R.id.onClickListener3);
        onClickListener4 = view.findViewById(R.id.onClickListener4);
        onClickListener5 = view.findViewById(R.id.onClickListener5);
        onClickListener6 = view.findViewById(R.id.onClickListener6);
        onClickListener7 = view.findViewById(R.id.onClickListener7);
        onClickListener8 = view.findViewById(R.id.onClickListener8);
        onClickListener9 = view.findViewById(R.id.onClickListener9);
        onClickListener10 = view.findViewById(R.id.onClickListener10);
        onClickListenerPoint = view.findViewById(R.id.onClickListenerPoint);
        onClickListenerClear = view.findViewById(R.id.onClickListenerClear);
        onClickListener0 = view.findViewById(R.id.onClickListener0);

        mSurchargeDetailLayout = view.findViewById(R.id.surcharge_details);
        mHasFeeLayout = view.findViewById(R.id.id_rel_view);
        mNoSurchargeMarkLayout = view.findViewById(R.id.input_nosurcharge_mark);
        mHasSurchargeMarkLayout = view.findViewById(R.id.input_mark);
        tv_nosurcharge = view.findViewById(R.id.tv_mark);
        tv_hassurcharge = view.findViewById(R.id.tv_nosurcharge_mark);

        if (!MainApplication.isSurchargeOpen()) {
            mHasFeeLayout.setVisibility(View.INVISIBLE);
            mNoSurchargeMarkLayout.setVisibility(View.VISIBLE);
            mHasSurchargeMarkLayout.setVisibility(View.GONE);
        }else{
            //如果开通预授权，并且选择是预授权
            if(MainApplication.isPre_authOpen == 1 &&  TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth","sale"),"pre_auth")){
                mHasFeeLayout.setVisibility(View.INVISIBLE);
                mNoSurchargeMarkLayout.setVisibility(View.VISIBLE);
                mHasSurchargeMarkLayout.setVisibility(View.GONE);
            }else{
                mHasFeeLayout.setVisibility(View.VISIBLE);
                mNoSurchargeMarkLayout.setVisibility(View.GONE);
                mHasSurchargeMarkLayout.setVisibility(View.VISIBLE);
            }
        }
        WindowManager wm = getActivity().getWindowManager();
        int screenHeight = wm.getDefaultDisplay().getHeight();
       /* mScrollView = view.findViewById(R.id.mScrollView);
        mScrollView.setMinimumHeight(screenHeight);
        mScrollView.setTag(0x10);
        mScrollView.setBackgroundColor(getActivity().getResources().getColor(R.color.app_drawer_title));
        mScrollView.setScrollingWhileRefreshingEnabled(true);
        mScrollView.setMode(PullToRefreshBase.Mode.PULL_FROM_START);
        mScrollView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ScrollView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ScrollView> refreshView) {
                try {
                    StatService.trackCustomEvent(mContext, "kMTASPayPayShowRate", "下拉查看汇率");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                // 1.携带参数的打点
                Bundle values = new Bundle();
                values.putString("kGFASPayPayShowRate","下拉查看汇率");
                DataReportUtils.getInstance().report("kGFASPayPayShowRate",values);

                mScrollView.onRefreshComplete();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ScrollView> refreshView) {

            }
        });*/
//        LoadingLayout loadingView = mScrollView.getHeaderLayout();
      /*  if (null != loadingView){
            mWXpayLayout = loadingView.findViewById(R.id.wxpay_layout);
            mAlipayLayout = loadingView.findViewById(R.id.alipay_layout);
            mWXExchange = loadingView.findViewById(R.id.tv_wx_detail);
            mAliExchange = loadingView.findViewById(R.id.tv_ali_detail);
            ly_pay = loadingView.findViewById(R.id.layout_pay);
            ly_refresh = loadingView.findViewById(R.id.layout_refresh);
            ly_pay.setVisibility(View.VISIBLE);
            if (!serviceType.contains(MainApplication.PAY_ALIPAY_TAG)){
                mAlipayLayout.setVisibility(View.GONE);
            }else{
                mAlipayLayout.setVisibility(View.VISIBLE);
            }
            if (!serviceType.contains(MainApplication.PAY_WEIXIN_TAG)){
                mWXpayLayout.setVisibility(View.GONE);
            }else{
                mWXpayLayout.setVisibility(View.VISIBLE);
            }
            if (!serviceType.contains(MainApplication.PAY_WEIXIN_TAG)
                    && !serviceType.contains(MainApplication.PAY_ALIPAY_TAG)){
                ly_pay.setVisibility(View.GONE);
            }
        }*/

        //只有开通预授权的通道才会有更改颜色的逻辑
        if(MainApplication.isPre_authOpen == 1){
            //读取上一次选择的是什么状态，然后根据状态设置title，默认是sale
            saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth","sale");
            if(TextUtils.equals(saleOrPreauth,"sale")){
                ll_textview_view.setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
//                mScrollView.setBackgroundColor(getActivity().getResources().getColor(R.color.app_drawer_title));

                if (ly_to_scan_pay!=null){//预授权 改变扫码按钮颜色
                    ly_to_scan_pay.setBackgroundResource(R.drawable.ly_scan);
                }

            }else if(TextUtils.equals(saleOrPreauth,"pre_auth")) {
                ll_textview_view.setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
//                mScrollView.setBackgroundColor(getActivity().getResources().getColor(R.color.title_bg_pre_auth));

                if (ly_to_scan_pay!=null){//预授权 改变扫码按钮颜色
                    ly_to_scan_pay.setBackgroundResource(R.drawable.ly_scan_pre_auth);
                }

            }
        }else{
            ll_textview_view.setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
//            mScrollView.setBackgroundColor(getActivity().getResources().getColor(R.color.app_drawer_title));

            if (ly_to_scan_pay!=null){//预授权 改变扫码按钮颜色
                ly_to_scan_pay.setBackgroundResource(R.drawable.ly_scan);
            }

        }


        //收款
        ly_to_scan_pay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                AppHelper.execVibrator(mContext);
                if (isPay) {
                    try {
                        StatService.trackCustomEvent(mContext, "kMTASPayPayScan", "扫一扫收款");
                    } catch (Exception e) {
                        Log.e(TAG,Log.getStackTraceString(e));
                    }

                    // 1.携带参数的打点
                    Bundle values = new Bundle();
                    values.putString("kGFASPayPayScan","扫一扫收款");
                    DataReportUtils.getInstance().report("kGFASPayPayScan",values);

                    if (!NetworkUtils.isNetworkAvailable(mContext)) {
                        toastDialog(mActivity, R.string.network_exception, null);
                        return;
                    }
                    toCaptureActivity();
                } else {//等号操作
                    if (currentMode == 1) {
                        if (sumTips.contains("+") && !sumTips.endsWith("+")) {
                            //相加
                            String arr[] = sumTips.split("\\+");
                            sumTips = Utils.subtractSum(arr[0], arr[1]);
                            twoTips = "";
                            fullValue();
                            setToPayView();
                        } else if (sumTips.contains("+") && sumTips.endsWith("+")) {
                            fullValue();
                            setToPayView();
                        } else if (sumTips.contains("-") && !sumTips.endsWith("-") && !sumTips.startsWith("-")) {
                            String arr[] = sumTips.split("\\-");
                            sumTips = Utils.subtract(arr[0], arr[1]);
                            twoTips = "";
                            fullValue();
                            setToPayView();
                        }

                    } else {
                        if (sum.contains("+") && !sum.endsWith("+")) {
                            //相加
                            String arr[] = sum.split("\\+");
                            sum = Utils.subtractSum(arr[0], arr[1]);
                            two = "";
                            fullValue();
                            setToPayView();
                        } else if (sum.contains("+") && sum.endsWith("+")) {
                            fullValue();
                            setToPayView();
                        } else if (sum.contains("-") && !sum.endsWith("-") && !sum.startsWith("-")) {
                            String arr[] = sum.split("\\-");
                            sum = Utils.subtract(arr[0], arr[1]);
                            two = "";
                            fullValue();
                            setToPayView();
                        }
                    }


                }
            }
        });

        initViewSurcharge();
       /* view.findViewById(R.id.ly_scan).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    StatService.trackCustomEvent(mContext, "SPConstTapScanButton", "扫一扫");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
                AppHelper.execVibrator(mContext);
                if (!NetworkUtils.isNetworkAvailable(mContext)) {
                    toastDialog(mActivity, R.string.network_exception, null);
                    return;
                }

                if (!StringUtil.isEmptyOrNull(etMoney.getText().toString()) && !TextUtils.equals(etMoney.getText(),"0.")
                        || TextUtils.equals(etMoney.getText(),"0.0") || TextUtils.equals(etMoney.getText(),"0")
                        || TextUtils.equals(etMoney.getText(),"0.00")) {
                    toCaptureActivity();
                } else {
                    CaptureActivity.startActivity(mContext, "pay",mark);
                }

            }
        });*/
    }

    /**
     * 扫描二维码
     */
    private void toCaptureActivity() {
        if (!checkMoeny()) {
            return;
        }
        Intent it = new Intent();
        it.setClass(mContext, CaptureActivity.class);
        it.putExtra("money", strTotalMoney);
        it.putExtra("surcharge", strSurchargelMoney);
        it.putExtra("tips", strTipMoney);
        it.putExtra("payType", payType);
        it.putExtra("mark", mark);
        Logger.e("JAMY","mark to: "+mark);
        startActivity(it);

    }

    long intTotlaMoney = 0;
    private boolean checkMoeny() {
        if (!NetworkUtils.isNetworkAvailable(mContext)) {
            toastDialog(mActivity, R.string.network_exception, null);
            return false;
        }

        if (TextUtils.isEmpty(etMoney.getText())) {
            toastDialog(mActivity, R.string.please_input_amount, null);
            return false;
        }

        if ( StringToMoney(etMoney.getText().toString()) == 0) {
            toastDialog(mActivity, R.string.tv_money_error, null);
            return false;
        }

        if (sum.contains("+") && !sum.endsWith("+")) {
            //相加
            String arr[] = sum.split("\\+");
            sum = Utils.subtractSum(arr[0], arr[1]);
            two = "";
            fullValue();

        }

        String money = etMoney.getText().toString();

        if (money.endsWith(".")) {
            toastDialog(mActivity, R.string.tv_money_error, null);
            return false;
        }
        if (money.contains(",")) {
            strTotalMoney = OrderManager.getInstance().getMoney(money.replace(",", ""));
        } else {
            strTotalMoney = OrderManager.getInstance().getMoney(money);
        }

        String surchargeMoney = id_title_surcharge.getText().toString();
        if (!surchargeMoney.isEmpty()) {

            if (surchargeMoney.endsWith(".")) {
                toastDialog(mActivity, R.string.tv_money_error, null);
                return false;
            }

            if (surchargeMoney.contains(MainApplication.feeFh)) {
                strSurchargelMoney = surchargeMoney.replace(MainApplication.feeFh, "");
            }

            if (strSurchargelMoney.contains(",")) {
                strSurchargelMoney = OrderManager.getInstance().getMoney(strSurchargelMoney.replace(",", ""));
            } else {
                strSurchargelMoney = OrderManager.getInstance().getMoney(strSurchargelMoney);
            }
        }
        intTotlaMoney = Utils.Long.tryParse(strTotalMoney, 0);
        if (intTotlaMoney == 0) {
            //            showToastInfo(getStringById(R.string.please_input_amount));
            toastDialog(mActivity, R.string.please_input_amount, null);
            return false;
        }

        if (intTotlaMoney < 0) {
            toastDialog(mActivity, R.string.tv_pay_less_money, null);
            return false;
        }

        long surcharge = Utils.Long.tryParse(strSurchargelMoney, 0);
        long Totalmoney = intTotlaMoney+surcharge;
      /*  if (Totalmoney > MAX_SALSE_VALUE) {
            toastDialog(mActivity, R.string.tv_pay_exceed_money, null);
            return false;
        }*/
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        String note_mark = NoteMarkActivity.getNoteMark();
        if(!TextUtils.isEmpty(note_mark)){
            tv_nosurcharge.setText(note_mark);
            tv_hassurcharge.setText(note_mark);
        }else{
            tv_nosurcharge.setText(getActivity().getResources().getString(R.string.note_mark));
            tv_hassurcharge.setText(getActivity().getResources().getString(R.string.note_mark));
        }
    }

    private void initViewSurcharge() {

        id_title_surcharge.setText(MainApplication.feeFh + getDefaultValue());
    }

    private double StringToMoney(String txMoeny){
        Pattern p = Pattern.compile("[^0-9.]");//去掉当前字符串中除去0-9以及小数点的其他字符
        Matcher matcher = p.matcher(txMoeny);
        txMoeny = matcher.replaceAll("");
        BigDecimal bigDecimal = new BigDecimal(txMoeny);
        double inputMoney = bigDecimal.doubleValue();
        return  inputMoney;
    }

    private String getDefaultValue(){
        //默认设置
        String defaultStr= "0.";
        if(MainApplication.numFixed == 0){
            //如果不含小数点
            defaultStr = "0";
        }else{
            for(int i = 0 ; i < MainApplication.numFixed;i++){
                defaultStr = defaultStr+"0";
            }
        }
        return  defaultStr;
    }

    void setBgHeiht(LinearLayout ly) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) ly.getLayoutParams();
        params.height = DensityUtil.dip2px(mContext, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (null == data){
           return;
        }
        String note_mark = NoteMarkActivity.getNoteMark();
        note_mark = data.getStringExtra("note_mark");
        mark = note_mark;
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_NOTE){
            if(!TextUtils.isEmpty(note_mark)){
                tv_nosurcharge.setText(note_mark);
                tv_hassurcharge.setText(note_mark);
            }else{
                tv_nosurcharge.setText(getActivity().getResources().getString(R.string.note_mark));
                tv_hassurcharge.setText(getActivity().getResources().getString(R.string.note_mark));
            }
        }else{
            tv_nosurcharge.setText(getActivity().getResources().getString(R.string.note_mark));
            tv_hassurcharge.setText(getActivity().getResources().getString(R.string.note_mark));
        }
    }

    /**
     * 去除小数点后面的0
     * 如果小数点后面全是0则小数点也去除
     * @return
     */
   private String splitZero(String sourceStr){
       if(sourceStr.indexOf(".") > 0){
           //正则表达
           //去掉后面无用的零
           sourceStr = sourceStr.replaceAll("0+?$", "");
           //如小数点后面全是零则去掉小数点
           sourceStr = sourceStr.replaceAll("[.]$", "");
       }
       return sourceStr;
   }

    private final static int REQUEST_NOTE = 0x11;
    private void setLister() {

        mSurchargeDetailLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuffer sb = new StringBuffer();
                sb.append(getResources().getString(R.string.surcharge_tips));
                if (0.0 != MainApplication.getSurchargeRate()){

                    BigDecimal surchargeBigDec =  new BigDecimal(Double.toString(MainApplication.getSurchargeRate()));
                    double surcharge = surchargeBigDec.multiply(new BigDecimal(Double.toString(1000))).doubleValue();
                    String splitZero = splitZero(surcharge+"");
                    if (TextUtils.isEmpty(splitZero)){
                        sb.append("0");
                    }else{
                        sb.append(splitZero);
                    }
                }else{
                    sb.append("0");
                }
                sb.append(getResources().getString(R.string.surcharge_tips_last));
                toastDialog(mActivity,mActivity.getString(R.string.btnOk),sb.toString(),R.string.tx_surcharge, null);
                try {
                    StatService.trackCustomEvent(mContext, "kMTASPayPayShowSurcharge", "查看surcharge提示");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                // 1.携带参数的打点
                Bundle values = new Bundle();
                values.putString("kGFASPayPayShowSurcharge","查看surcharge提示");
                DataReportUtils.getInstance().report("kGFASPayPayShowSurcharge",values);
            }
        });

        mHasSurchargeMarkLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent();
                it.setClass(getActivity(), NoteMarkActivity.class);
                FragmentTabPay.this.startActivityForResult(it,REQUEST_NOTE);
            }
        });

        mNoSurchargeMarkLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent();
                it.setClass(getActivity(), NoteMarkActivity.class);
                FragmentTabPay.this.startActivityForResult(it,REQUEST_NOTE);
            }
        });

        iv_clear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lay_notify.setVisibility(View.GONE);

            }
        });

        lay_Clear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(mContext);
                clearData();
            }
        });

        onClickListener0.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(mContext);
                executeValueNew(button0.getTag().toString());
                fullValue();
            }
        });


        //加号
        onClickListener10.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                AppHelper.execVibrator(mContext);
                executeValueNew(button10.getTag().toString());
                fullValue();
                /*if (!(etMoney.getText().toString().equalsIgnoreCase("0.0")
                        || etMoney.getText().toString().equalsIgnoreCase("0.00"))) {
                    setToEqView();
                }*/
                if (StringToMoney(etMoney.getText().toString()) != 0) {
                    setToEqView();
                }

            }
        });
        //等号

        onClickListener1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(mContext);
                executeValueNew(button1.getTag().toString());
                fullValue();
            }
        });
        onClickListener2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(mContext);
                executeValueNew(button2.getTag().toString());
                fullValue();
            }
        });
        onClickListener3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(mContext);
                executeValueNew(button3.getTag().toString());
                fullValue();
            }
        });
        onClickListener4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(mContext);
                executeValueNew(button4.getTag().toString());
                fullValue();
            }
        });
        onClickListener5.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(mContext);
                executeValueNew(button5.getTag().toString());
                fullValue();
            }
        });
        onClickListener6.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(mContext);
                executeValueNew(button6.getTag().toString());
                fullValue();
            }
        });
        onClickListener7.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(mContext);
                executeValueNew(button7.getTag().toString());
                fullValue();
            }
        });
        onClickListener8.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(mContext);
                executeValueNew(button8.getTag().toString());
                fullValue();
            }
        });
        onClickListener9.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AppHelper.execVibrator(mContext);
                executeValueNew(button9.getTag().toString());
                //                fullValueNew();
                fullValue();
            }
        });
        onClickListenerPoint.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //只有包含小数点的位数的情况下才让输入小数点
                if(MainApplication.numFixed != 0){
                    AppHelper.execVibrator(mContext);
                    executeValueNew(buttonPoint.getTag().toString());
                    fullValue();
                }
            }
        });

        onClickListenerClear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                etNumberClear();


            }
        });

        id_tips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMode = 1;
                sum = "";
                id_line_first.setBackgroundColor(getResources().getColor(R.color.tv_no_focus));
                id_line_second.setBackgroundColor(getResources().getColor(R.color.white));
                btn_ic_money.setTextColor(getResources().getColor(R.color.tv_no_focus));
                etMoney.setTextColor(getResources().getColor(R.color.tv_no_focus));
                id_tips.setTextColor(getResources().getColor(R.color.white));
                id_title_addtips.setTextColor(getResources().getColor(R.color.white));
            }
        });

//        etMoney.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                currentMode = 0;
//                id_line_first.setBackgroundColor(getResources().getColor(R.color.white));
//                id_line_second.setBackgroundColor(getResources().getColor(R.color.tv_no_focus));
//                btn_ic_money.setTextColor(getResources().getColor(R.color.white));
//                etMoney.setTextColor(getResources().getColor(R.color.white));
//                id_tips.setTextColor(getResources().getColor(R.color.tv_no_focus));
//                id_title_addtips.setTextColor(getResources().getColor(R.color.tv_no_focus));
//            }
//        });
    }


    private void executeValueNew(String tag) {
        if (currentMode == 1) {
            if (tag.equals("")) {
                sumTips = "0";
            } else {
                if (sumTips.equals(tag)) {
                    if (sumTips.equals("0")) {
                        sumTips = tag;
                    } else {
                        sumTips = sumTips + tag;
                    }
                } else {

                    if (StringUtil.isEmptyOrNull(sumTips) && (tag.equals("00") || tag.equals("."))) {
                        return;
                    }
                    if (sumTips.contains("+")) {
                        if (tag.equals("+")) {//
                            if (sumTips.endsWith("+")) {
                                return;
                            } else {//相加
                                String arr[] = sumTips.split("\\+");
                                sumTips = Utils.subtractSum(arr[0], arr[1]) + "+";
                                twoTips = "";
                            }
                        } else {
                            if (StringUtil.isEmptyOrNull(twoTips) && tag.equals(".")) {
                                return;
                            }
                            if (twoTips.contains(".") && tag.equals(".")) {
                                return;
                            }
                            if (twoTips.contains(".")) {
                                if (twoTips.substring(twoTips.indexOf(".") + 1).length() >= 2) {
                                    return;
                                } else {
                                    if (twoTips.equals("0.0") && tag.equals("0")) {
                                        return;
                                    }
                                    if(twoTips.equals("0")){
                                        twoTips =  tag;
                                    }else{
                                        twoTips = twoTips + tag;
                                    }

                                    if (Utils.compareTo(twoTips, maxNum)) {//大于10亿
                                        twoTips = twoTips.substring(0, twoTips.length() - 1);
                                    }
                                    String pase = sumTips.substring(0, sumTips.indexOf("+") + 1);
                                    sumTips = pase + twoTips;
                                }
                            } else {
                                if (twoTips.startsWith("0") && tag.equals("0")) {
                                    return;
                                }
                                twoTips = twoTips + tag;
                                if (Utils.compareTo(twoTips, maxNum)) {//大于10亿
                                    if (twoTips.length() == 11) {
                                        twoTips = twoTips.substring(0, twoTips.length() - 2);
                                    } else {
                                        twoTips = twoTips.substring(0, twoTips.length() - 1);
                                    }
                                }
                                String pase = sumTips.substring(0, sumTips.indexOf("+") + 1);
                                sumTips = pase + twoTips;
                            }
                        }
                    } else if (sumTips.contains("-")) //
                    {
                        if (tag.equals("-")) {
                            if (sumTips.endsWith("-")) {
                                return;
                            } else {//相加
                                String arr[] = sumTips.split("\\-");
                                //                                long m = Long.parseLong(arr[0]) - Long.parseLong(arr[1]);
                                sumTips = Utils.subtract(arr[0], arr[1]) + "-";
                                twoTips = "";
                            }
                        } else if (tag.equals("+")) {
                            if (sumTips.endsWith("-")) {
                                sumTips = sumTips.substring(0, sumTips.length() - 1) + "+";
                                return;
                            }
                            if (sumTips.startsWith("-")) {//负数的时候
                                sumTips = sumTips + "+";
                            } else {
                                String arr[] = sumTips.split("\\-");
                                //                            long m = Long.parseLong(arr[0]) - Long.parseLong(arr[1]);
                                sumTips = Utils.subtract(arr[0], arr[1]) + "+";
                            }
                            twoTips = "";
                        } else {

                            if (StringUtil.isEmptyOrNull(twoTips) && tag.equals(".")) {
                                return;
                            }
                            if (twoTips.contains(".") && tag.equals(".")) {
                                return;
                            }
                            if (twoTips.contains(".")) {
                                if (twoTips.substring(twoTips.indexOf(".") + 1).length() >= 2) {
                                    return;
                                } else {
                                    if (twoTips.equals("0.0") && tag.equals("0")) {
                                        return;
                                    }
                                    twoTips = twoTips + tag;
                                    if (Double.parseDouble(twoTips) > 50000) {//大于5万
                                        twoTips = twoTips.substring(0, twoTips.length() - 1);
                                    }
                                    String pase = sumTips.substring(0, sumTips.indexOf("-") + 1);
                                    sumTips = pase + twoTips;

                                }
                            } else {
                                if (twoTips.startsWith("0") && tag.equals("0")) {
                                    return;
                                }
                                twoTips = twoTips + tag;
                                if (Double.parseDouble(twoTips) > 50000) {//大于5万
                                    twoTips = twoTips.substring(0, twoTips.length() - 1);
                                }
                                String pase = sumTips.substring(0, sumTips.indexOf("-") + 1);
                                sumTips = pase + twoTips;
                            }
                        }

                    } else {
                        if (tag.equals("+")) { //加号
                            sumTips = sumTips + tag;
                        } else if (tag.equals("-")) {
                            sumTips = sumTips + tag;
                        } else {//

                            if (sumTips.contains(".") && tag.equals(".")) {
                                return;
                            }
                            formFloat(tag);
                        }
                    }

                }

            }
        } else {//---------------没有小费的情况
            if (tag.equals("")) {
                sum = "0";
            } else {
                if (sum.equals(tag)) {
                    if (sum.equals("0")) {
                        sum = tag;
                    } else {
                        sum = sum + tag;
                    }
                } else {
                    if (StringUtil.isEmptyOrNull(sum) && (tag.equals("00"))) {
                        return;
                    }
                    if (sum.contains("+")) {
                        if (tag.equals("+")) {//如果当前输入的就是+号
                            if (sum.endsWith("+")) {
                                return;
                            } else {//相加
                                String arr[] = sum.split("\\+");
                                //                                long m = Long.parseLong(arr[0]) + Long.parseLong(arr[1]);
                                sum = Utils.subtractSum(arr[0], arr[1]) + "+";
                                two = "";
                            }
                        } else {
                            if (two.contains(".") && tag.equals(".")) {
                                return;
                            }
                            if (two.contains(".")) {
                                if (two.substring(two.indexOf(".") + 1).length() >= MainApplication.numFixed) {
                                    return;
                                } else {
                                    String str = "0.";
                                    for(int i = 1 ; i< MainApplication.numFixed ; i++){
                                        str = str+"0";
                                    }
                                    if (two.equals(str) && tag.equals("0")) {
                                        return;
                                    }
                                    two = two + tag;
                                    if (!TextUtils.isEmpty(two) && Utils.compareTo(two, maxNum)) {//大于999 999 999 .99
                                        two = two.substring(0, two.length() - 1);
                                    }
                                    String pase = sum.substring(0, sum.indexOf("+") + 1);

                                    sum = pase + two;
                                }
                            } else {
                                if (two.startsWith("0") && tag.equals("0")) {
                                    return;
                                }else if(TextUtils.isEmpty(two) && tag.equals(".")){
                                    two = "0"+tag;
                                } else {
                                    two = two + tag;
                                }

                                if (!TextUtils.isEmpty(two) && Utils.compareTo(two, maxNum)) {//大于999 999 999 .99
                                    two = two.substring(0, two.length() - 1);
                                    /*if (two.length() == 11) {
                                        two = two.substring(0, two.length() - 2);

                                    } else {
                                        two = two.substring(0, two.length() - 1);
                                    }*/
                                }
                                String pase = sum.substring(0, sum.indexOf("+") + 1);
                                sum = pase + two;
                            }

                        }
                    } else if (sum.contains("-")) {
                        if (tag.equals("-")) {//如果当前输入的是-号
                            if (sum.endsWith("-")) {
                                return;
                            } else {//相加
                                String arr[] = sum.split("\\-");
                                //                                long m = Long.parseLong(arr[0]) - Long.parseLong(arr[1]);
                                sum = Utils.subtract(arr[0], arr[1]) + "-";
                                two = "";
                            }
                        } else if (tag.equals("+")) {
                            if (sum.endsWith("-")) {
                                sum = sum.substring(0, sum.length() - 1) + "+";
                                return;
                            }
                            if (sum.startsWith("-")) {//负数的时候
                                sum = sum + "+";
                            } else {

                                String arr[] = sum.split("\\-");
                                //                            long m = Long.parseLong(arr[0]) - Long.parseLong(arr[1]);
                                sum = Utils.subtract(arr[0], arr[1]) + "+";
                            }
                            two = "";
                        } else {

                            if (StringUtil.isEmptyOrNull(two) && tag.equals(".")) {
                                return;
                            }
                            if (two.contains(".") && tag.equals(".")) {
                                return;
                            }
                            if (two.contains(".")) {
                                if (two.substring(two.indexOf(".") + 1).length() >= MainApplication.numFixed) {
                                    return;
                                } else {
                                    String str = "0.";
                                    for(int i = 1 ; i< MainApplication.numFixed ; i++){
                                        str = str+"0";
                                    }
                                    if (two.equals(str) && tag.equals("0")) {
                                        return;
                                    }
                                    two = two + tag;
                                   /* if (Double.parseDouble(two) > 50000) {//大于5万
                                        two = two.substring(0, two.length() - 1);
                                    }*/
                                    String pase = sum.substring(0, sum.indexOf("-") + 1);

                                    sum = pase + two;

                                }
                            } else {
                                if (two.startsWith("0") && tag.equals("0")) {
                                    return;
                                }

                                two = two + tag;
                               /* if (Double.parseDouble(two) > 50000) {//大于5万
                                    two = two.substring(0, two.length() - 1);
                                }*/
                                String pase = sum.substring(0, sum.indexOf("-") + 1);

                                sum = pase + two;
                            }
                        }

                    } else {
                        if (tag.equals("+")) { //加号
                            if(TextUtils.isEmpty(sum)){
                                return;
                            }else{
                                sum = sum + tag;
                            }
                        } else if (tag.equals("-")) {
                            if(TextUtils.isEmpty(sum)){
                                return;
                            }else{
                                sum = sum + tag;
                            }
                        }else if(tag.equals(".")){
                            if(TextUtils.isEmpty(sum)){
                                sum = "0"+tag;
                            }else if(sum.contains(".")){
                                return;
                            }else{
                                sum = sum + tag;
                            }
                        } else {
                            if (sum.contains(".") && tag.equals(".")) {
                                return;
                            }
                            formFloat(tag);

                        }
                    }

                }

            }

        }

    }

    void formFloat(String tag) {
        if (currentMode == 1) {
            if (sumTips.contains(".")) {
                if (sumTips.substring(sumTips.indexOf(".") + 1).length() >= 2) {
                    return;
                } else {
                    if (sumTips.equals("0.0") && (tag.equals("0") || tag.equals("00"))) {
                        return;
                    }
                    sumTips = sumTips + tag;
                }
            } else {
                sumTips = sumTips + tag;
                if (Utils.compareTo(sumTips, maxNum)) {//大于10亿

                    if (sumTips.length() == 11) {
                        sumTips = sumTips.substring(0, sumTips.length() - 2);
                    } else {
                        sumTips = sumTips.substring(0, sumTips.length() - 1);
                    }
                }
            }
        } else {

            if (sum.contains(".")) {
                if (sum.substring(sum.indexOf(".") + 1).length() >= MainApplication.numFixed) {
                    return;
                } else {
                    String str = "0.";
                    for(int i = 1 ; i < MainApplication.numFixed ; i++){
                        str = str+"0";
                    }
                    if (sum.equals(str) && (tag.equals("0"))) {
                        return;
                    }
                    sum = sum + tag;
                    if (!TextUtils.isEmpty(sum) && Utils.compareTo(sum, maxNum)) {//大于999 999 999.99
                        sum = sum.substring(0, sum.length() - 1);
                    }
                }
            } else {
                sum = sum + tag;
                if (!TextUtils.isEmpty(sum) && Utils.compareTo(sum, maxNum)) {//大于999 999 999.99
                    sum = sum.substring(0, sum.length() - 1);
                }
            }
        }


    }

    private void showConfirm(String msg, final ProgressInfoDialog dialogs) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.public_cozy_prompt);
        builder.setMessage(msg);
        builder.setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isStop = false;
                if (dialogs != null) {
                    dialogs.dismiss();
                }
            }
        });

        builder.setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialogInfo = builder.show();
    }

    public void checkVersion() {
        //不用太平凡的检测升级 所以使用时间间隔区分
        LocalAccountManager.getInstance().saveCheckVersionTime();
        UpgradeManager.getInstance().getVersonCode(new UINotifyListener<UpgradeInfo>() {
            @Override
            public void onError(Object object) {
                super.onError(object);
            }

            @Override
            public void onSucceed(UpgradeInfo result) {
                super.onSucceed(result);
                if (null != result) {
                    //                    if (result.version > PreferenceUtil.getInt("update", 0))
                    //                    {
                    // "发现新版本"
                    LocalAccountManager.getInstance().saveCheckVersionFlag(result.mustUpgrade);
                    showUpgradeInfoDialog(result, new ComDialogListener(result));
                    //                    }
                }
            }
        });
    }

    class ComDialogListener implements CommonConfirmDialog.ConfirmListener {
        private UpgradeInfo result;

        public ComDialogListener(UpgradeInfo result) {
            this.result = result;
        }

        @Override
        public void ok() {
            new UpgradeDailog(mActivity, result, new UpgradeDailog.UpdateListener() {
                @Override
                public void cancel() {

                }
            }).show();
        }

        @Override
        public void cancel() {

            PreferenceUtil.commitInt("update", result.version); // 保存更新，下次进来不更新

        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.rl_wxpay:
//                ApiConstant.isLocaltionQRcode = false;
//                checkData(true);
//                break;
            /* case R.id.bt_scan:
                 toCaptureActivity();
                 break;*/
//            case R.id.rl_posPay:
//                //                toNewpostech(v); // pos刷卡
//                checkData(false);
//                break;
            case R.id.fht_iv_input_number_del:
                clearData();
                break;
//            case R.id.pay_four_chose: // 京东扫码支付
//                toPay(MainApplication.PAY_JINGDONG_NATIVE);
//                break;
//            case R.id.pay_first_chose: // 手q支付
//                if (payMeths.contains(MainApplication.PAY_QQ_NATIVE)) {
//                    toPay(MainApplication.PAY_QQ_NATIVE);
//                } else {
//                    toPay(MainApplication.PAY_QQ_NATIVE1);
//                }
//                break;
            case R.id.pay_second_chose: // 微信支付
                //                sharPreference.edit()
                //                    .putString("payMethStr", payMeth[0] + "," + payMeth[1] + "," + payMeth[2])
                //                    .commit();
                //                sharPreference.edit().putString("payIconId", payIcon[0] + "," + payIcon[1] + "," + payIcon[2]).commit();

                toPay(payNatieType);
                break;
            case R.id.pay_three_chose: // 支付宝支付
                //                sharPreference.edit()
                //                    .putString("payMethStr", payMeth[0] + "," + payMeth[1] + "," + payMeth[2])
                //                    .commit();
                //                sharPreference.edit().putString("payIconId", payIcon[0] + "," + payIcon[1] + "," + payIcon[2]).commit();
                if (payMeths.contains(MainApplication.PAY_ZFB_NATIVE1)) {
                    toPay(MainApplication.PAY_ZFB_NATIVE1);
                } else {
                    toPay(MainApplication.PAY_ZFB_NATIVE);
                }
                break;
            case R.id.lay_rotating: // 活动详情
                lay_rotating.setVisibility(View.GONE);
//                AwardDetiActivity.startActivity(PayActivity.this, url);
                break;
        }
    }


    /*
    * 设置默认支付方式
    */
    private void setPayMehotd(String payType) {
        if (payMeths.size() == 4) {
            views.clear();
            if (payType.equalsIgnoreCase(MainApplication.PAY_QQ_NATIVE1)) {//设置qq默认支付方式
                views.add(0, qqView);
                views.add(1, wxView);
                views.add(2, zfbView);
                views.add(3, jdView);

                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(left_icon_ashing_jd);
                pay_left_flipper.addView(zfb_left_img);
                pay_left_flipper.addView(left_icon_wechat);
                pay_left_flipper.addView(left_icon_qq);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_wechat);
                pay_right_flipper.addView(right_alipay);
                pay_right_flipper.addView(right_icon_ashing_jd);
                pay_right_flipper.addView(right_icon_qq);
                setDefPayMethod = true;
            } else if (payType.equals(MainApplication.PAY_ZFB_NATIVE1)) { // 支付宝默认支付
                views.add(0, zfbView);
                views.add(1, wxView);
                views.add(2, qqView);
                views.add(3, jdView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(left_icon_ashing_jd);
                pay_left_flipper.addView(left_icon_qq);
                pay_left_flipper.addView(left_icon_wechat);
                pay_left_flipper.addView(zfb_left_img);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_wechat);
                pay_right_flipper.addView(right_icon_qq);
                pay_right_flipper.addView(right_icon_ashing_jd);
                pay_right_flipper.addView(right_alipay);
                setDefPayMethod = true;
            } else if (payType.equals(MainApplication.PAY_JINGDONG_NATIVE)) {
                views.add(0, jdView);
                views.add(1, wxView);
                views.add(2, qqView);
                views.add(3, zfbView);

                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(zfb_left_img);
                pay_left_flipper.addView(left_icon_qq);
                pay_left_flipper.addView(left_icon_wechat);
                pay_left_flipper.addView(left_icon_ashing_jd);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_wechat);
                pay_right_flipper.addView(right_icon_qq);
                pay_right_flipper.addView(right_alipay);
                pay_right_flipper.addView(right_icon_ashing_jd);
                setDefPayMethod = true;
            } else {
                views.add(0, wxView);
                views.add(1, qqView);
                views.add(2, jdView);
                views.add(3, zfbView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(zfb_left_img);
                pay_left_flipper.addView(left_icon_ashing_jd);
                pay_left_flipper.addView(left_icon_qq);
                pay_left_flipper.addView(left_icon_wechat);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_qq);
                pay_right_flipper.addView(right_icon_ashing_jd);
                pay_right_flipper.addView(right_alipay);
                pay_right_flipper.addView(right_icon_wechat);
                setDefPayMethod = true;
            }
            myAdapter.notifyDataSetChanged();
            lastIndex = (views.size()) * 100;
            viewPager.setCurrentItem(lastIndex);
        } else if (payMeths.size() == 3) {
            setPayDefMethod(payType);
        } else { //2种支付类型
            if (payMeths.contains(MainApplication.PAY_WX_NATIVE) && (payMeths.contains(MainApplication.PAY_QQ_NATIVE) || payMeths.contains(MainApplication.PAY_QQ_NATIVE1))) {//微信 qq
                if (payType.equals(MainApplication.PAY_QQ_NATIVE1)) {
                    viewPager.setCurrentItem(1);
                } else {
                    viewPager.setCurrentItem(0);
                }
            } else if (payMeths.contains(MainApplication.PAY_WX_NATIVE) && (payMeths.contains(MainApplication.PAY_ZFB_NATIVE) || payMeths.contains(MainApplication.PAY_ZFB_NATIVE1))) { // 微信，支付宝
                if (payType.equals(MainApplication.PAY_ZFB_NATIVE1)) {
                    viewPager.setCurrentItem(1);
                } else {
                    viewPager.setCurrentItem(0);
                }
            } else if (payMeths.contains(MainApplication.PAY_WX_NATIVE) && payMeths.contains(MainApplication.PAY_JINGDONG_NATIVE)) { // 微信 京东
                if (payType.equals(MainApplication.PAY_JINGDONG_NATIVE)) {
                    viewPager.setCurrentItem(1);
                } else {
                    viewPager.setCurrentItem(0);
                }
            } else if ((payMeths.contains(MainApplication.PAY_QQ_NATIVE) || payMeths.contains(MainApplication.PAY_QQ_NATIVE1)) && (payMeths.contains(MainApplication.PAY_ZFB_NATIVE) || payMeths.contains(MainApplication.PAY_ZFB_NATIVE1))) {
                //qq 支付宝
                if (payType.equals(MainApplication.PAY_ZFB_NATIVE1)) {
                    viewPager.setCurrentItem(0);
                } else {
                    viewPager.setCurrentItem(1);
                }
            } else if ((payMeths.contains(MainApplication.PAY_QQ_NATIVE) || payMeths.contains(MainApplication.PAY_QQ_NATIVE1)) && payMeths.contains(MainApplication.PAY_JINGDONG_NATIVE)) {
                if (payType.equals(MainApplication.PAY_QQ_NATIVE1)) {
                    viewPager.setCurrentItem(0);
                } else {
                    viewPager.setCurrentItem(1);
                }
            } else {
                // 支付宝 京东
                if (payType.equals(MainApplication.PAY_ZFB_NATIVE1)) {
                    viewPager.setCurrentItem(0);
                } else {
                    viewPager.setCurrentItem(1);
                }
            }
        }
    }

    private void setPayDefMethod(String payType) {

        // 有微信支付
        if ((payMeths.contains(MainApplication.PAY_WX_NATIVE) || payMeths.contains(MainApplication.PAY_WX_NATIVE_INTL)) && (payMeths.contains(MainApplication.PAY_QQ_NATIVE) || payMeths.contains(MainApplication.PAY_QQ_NATIVE1)) && (payMeths.contains(MainApplication.PAY_ZFB_NATIVE) || payMeths.contains(MainApplication.PAY_ZFB_NATIVE1))) { // 微信支付宝，手Q，支付宝){
            if (payType.equalsIgnoreCase(MainApplication.PAY_QQ_NATIVE1)) {
                views.clear();
                views.add(0, qqView);
                views.add(1, wxView);
                views.add(2, zfbView);
                myAdapter.notifyDataSetChanged();
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(zfb_left_img);
                pay_left_flipper.addView(left_icon_wechat);
                pay_left_flipper.addView(left_icon_qq);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_wechat);
                pay_right_flipper.addView(right_alipay);
                pay_right_flipper.addView(right_icon_qq);
            } else if (payType.equals(MainApplication.PAY_ZFB_NATIVE1)) {
                views.clear();
                views.add(0, zfbView);
                views.add(1, wxView);
                views.add(2, qqView);
                myAdapter.notifyDataSetChanged();
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(left_icon_qq);
                pay_left_flipper.addView(left_icon_wechat);
                pay_left_flipper.addView(zfb_left_img);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_wechat);
                pay_right_flipper.addView(right_icon_qq);
                pay_right_flipper.addView(right_alipay);
            } else {
                views.clear();
                views.add(0, wxView);
                views.add(1, qqView);
                views.add(2, zfbView);
                myAdapter.notifyDataSetChanged();

                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(zfb_left_img);
                pay_left_flipper.addView(left_icon_qq);
                pay_left_flipper.addView(left_icon_wechat);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_qq);
                pay_right_flipper.addView(right_alipay);
                pay_right_flipper.addView(right_icon_wechat);
            }

            setDefPayMethod = true;
        } else if ((payMeths.contains(MainApplication.PAY_WX_NATIVE) || payMeths.contains(MainApplication.PAY_WX_NATIVE_INTL)) && (payMeths.contains(MainApplication.PAY_QQ_NATIVE) || payMeths.contains(MainApplication.PAY_QQ_NATIVE1)) && payMeths.contains(MainApplication.PAY_JINGDONG_NATIVE)) { //  微信支付宝，手Q，jd
            views.clear();
            if (payType.equalsIgnoreCase(MainApplication.PAY_QQ_NATIVE1)) {
                views.add(0, qqView);
                views.add(1, wxView);
                views.add(2, jdView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(left_icon_ashing_jd);
                pay_left_flipper.addView(left_icon_wechat);
                pay_left_flipper.addView(left_icon_qq);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_wechat);
                pay_right_flipper.addView(right_icon_ashing_jd);
                pay_right_flipper.addView(right_icon_qq);
            } else if (payType.equals(MainApplication.PAY_JINGDONG_NATIVE)) {
                views.add(0, jdView);
                views.add(1, wxView);
                views.add(2, qqView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(left_icon_qq);
                pay_left_flipper.addView(left_icon_wechat);
                pay_left_flipper.addView(left_icon_ashing_jd);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_wechat);
                pay_right_flipper.addView(right_icon_qq);
                pay_right_flipper.addView(right_icon_ashing_jd);
            } else {
                views.add(0, wxView);
                views.add(1, qqView);
                views.add(2, jdView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(left_icon_ashing_jd);
                pay_left_flipper.addView(left_icon_qq);
                pay_left_flipper.addView(left_icon_wechat);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_qq);
                pay_right_flipper.addView(right_icon_ashing_jd);
                pay_right_flipper.addView(right_icon_wechat);
            }
            myAdapter.notifyDataSetChanged();
            setDefPayMethod = true;
        } else if ((payMeths.contains(MainApplication.PAY_WX_NATIVE) || payMeths.contains(MainApplication.PAY_WX_NATIVE_INTL)) && (payMeths.contains(MainApplication.PAY_ZFB_NATIVE) || payMeths.contains(MainApplication.PAY_ZFB_NATIVE1)) && payMeths.contains(MainApplication.PAY_JINGDONG_NATIVE)) { //  微信支付宝，支付宝，jd
            views.clear();
            if (payType.equals(MainApplication.PAY_ZFB_NATIVE1)) {
                views.add(0, zfbView);
                views.add(1, wxView);
                views.add(2, jdView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(left_icon_ashing_jd);
                pay_left_flipper.addView(left_icon_wechat);
                pay_left_flipper.addView(zfb_left_img);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_wechat);
                pay_right_flipper.addView(right_icon_ashing_jd);
                pay_right_flipper.addView(right_alipay);
            } else if (payType.equals(MainApplication.PAY_JINGDONG_NATIVE)) {
                views.add(0, jdView);
                views.add(1, wxView);
                views.add(2, zfbView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(zfb_left_img);
                pay_left_flipper.addView(left_icon_wechat);
                pay_left_flipper.addView(left_icon_ashing_jd);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_wechat);
                pay_right_flipper.addView(right_alipay);
                pay_right_flipper.addView(right_icon_ashing_jd);
            } else {
                views.add(0, wxView);
                views.add(1, jdView);
                views.add(2, zfbView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(zfb_left_img);
                pay_left_flipper.addView(left_icon_ashing_jd);
                pay_left_flipper.addView(left_icon_wechat);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_ashing_jd);
                pay_right_flipper.addView(right_alipay);
                pay_right_flipper.addView(right_icon_wechat);
            }

            myAdapter.notifyDataSetChanged();
            setDefPayMethod = true;
        } else {//手Q，支付宝，jd
            views.clear();
            if (payType.equals(MainApplication.PAY_QQ_NATIVE1)) {
                views.add(0, qqView);
                views.add(1, jdView);
                views.add(2, zfbView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(zfb_left_img);
                pay_left_flipper.addView(left_icon_ashing_jd);
                pay_left_flipper.addView(left_icon_qq);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_ashing_jd);
                pay_right_flipper.addView(right_alipay);
                pay_right_flipper.addView(right_icon_qq);
            } else if (payType.equals(MainApplication.PAY_JINGDONG_NATIVE)) {
                views.add(0, jdView);
                views.add(1, qqView);
                views.add(2, zfbView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(zfb_left_img);
                pay_left_flipper.addView(left_icon_qq);
                pay_left_flipper.addView(left_icon_ashing_jd);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_qq);
                pay_right_flipper.addView(right_alipay);
                pay_right_flipper.addView(right_icon_ashing_jd);
            } else {
                views.add(0, zfbView);
                views.add(1, qqView);
                views.add(2, jdView);
                pay_left_flipper.removeAllViews();
                pay_left_flipper.addView(left_icon_ashing_jd);
                pay_left_flipper.addView(left_icon_qq);
                pay_left_flipper.addView(zfb_left_img);

                pay_right_flipper.removeAllViews();
                pay_right_flipper.addView(right_icon_qq);
                pay_right_flipper.addView(right_icon_ashing_jd);
                pay_right_flipper.addView(right_alipay);
            }
            myAdapter.notifyDataSetChanged();
            setDefPayMethod = true;
        }

        lastIndex = (views.size()) * 100;
        viewPager.setCurrentItem(lastIndex);
    }

    /**
     * 转成RMB
     *
     * @author admin
     */
    void parserToRmb(BigDecimal bigDecimal) {
        if (currentMode == 1) {
            return;
        }
        try {
            if (!MainApplication.getFeeType().equalsIgnoreCase("CNY")) {
//                tv_pase.setVisibility(View.VISIBLE);
//                mHasFeeLayout.setVisibility(View.VISIBLE);
                if (bigDecimal != null) {
                    BigDecimal surchargeRate = new BigDecimal(MainApplication.getSurchargeRate()).setScale(6, BigDecimal.ROUND_HALF_UP);
                    BigDecimal additionalCharge = bigDecimal.multiply(surchargeRate).setScale(MainApplication.numFixed, BigDecimal.ROUND_HALF_UP);
                    strSurchargelMoney = DateUtil.formatPaseMoney(additionalCharge);
                    id_title_surcharge.setText(MainApplication.feeFh + DateUtil.formatPaseMoney(additionalCharge));
                    BigDecimal totalCharge = null;
                    if (MainApplication.getSourceToUsdExchangeRate() > 0 && MainApplication.getUsdToRmbExchangeRate() > 0) {
                        BigDecimal sourceToUsd = new BigDecimal(MainApplication.getSourceToUsdExchangeRate());
                        BigDecimal usdRate = new BigDecimal(MainApplication.getUsdToRmbExchangeRate());
                        totalCharge = additionalCharge.add(bigDecimal).multiply(sourceToUsd).setScale(MainApplication.numFixed, BigDecimal.ROUND_HALF_UP)
                                .multiply(usdRate).setScale(MainApplication.numFixed, BigDecimal.ROUND_DOWN);
                    } else {
                        BigDecimal rate = new BigDecimal(MainApplication.getExchangeRate());
                        totalCharge = additionalCharge.add(bigDecimal).multiply(rate).setScale(MainApplication.numFixed, BigDecimal.ROUND_FLOOR);
                    }
                    BigDecimal totalOriginalCharge = bigDecimal.add(additionalCharge).setScale(MainApplication.numFixed, BigDecimal.ROUND_FLOOR);
                    //tv_pase.setText(getString(R.string.tv_charge_total) + ":" + MainApplication.feeFh + DateUtil.formatPaseMoney(totalOriginalCharge) + "  " + getString(R.string.tx_about) + getString(R.string.tx_mark) + DateUtil.formatPaseMoney(totalCharge));
                    tv_pase.setText( MainApplication.feeFh + DateUtil.formatPaseMoney(totalOriginalCharge));

                }
            } else {
//                mHasFeeLayout.setVisibility(View.GONE);
//                tv_pase.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Logger.e(TAG, "parserToRmb-->" + e);
        }
    }

    private void fullValue() {
        if (currentMode == 1) {
            try {
                Logger.i("hehui", "sum-->" + sumTips);
                boolean isInt = Utils.Integer.isInteger(sumTips);
                if (isInt) {
                    BigDecimal bigDecimal = new BigDecimal(sumTips);
                    parserToRmb(bigDecimal);
                    id_tips.setText(DateUtil.formatPaseMoneyUtil(bigDecimal));
                } else {
                    if (sumTips.contains("+")) {
                        if (sumTips.endsWith("+")) {
                            String paseSum = sumTips.substring(0, sumTips.length() - 1);
                            BigDecimal bigDecimal = new BigDecimal(paseSum);
                            if (paseSum.contains(".")) {
                                if (paseSum.endsWith(".00")) {
                                    id_tips.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + ".00");
                                    parserToRmb(bigDecimal);
                                } else {
                                    id_tips.setText(DateUtil.formatPaseMoneyUtil(bigDecimal));
                                    parserToRmb(bigDecimal);
                                }
                            } else {
                                id_tips.setText(DateUtil.formatPaseMoneyUtil(bigDecimal));
                                parserToRmb(bigDecimal);
                            }
                        } else {
                            String paseSum = sumTips.substring(sumTips.indexOf("+") + 1);
                            if (paseSum.contains(".")) {
                                if (paseSum.endsWith(".")) {
                                    String one = paseSum.substring(0, paseSum.lastIndexOf("."));
                                    BigDecimal bigDecimal = new BigDecimal(one);
                                    id_tips.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + ".");
                                    parserToRmb(bigDecimal);
                                } else {
                                    String one = paseSum.substring(0, paseSum.lastIndexOf("."));
                                    String two = paseSum.substring(paseSum.lastIndexOf(".") + 1);
                                    BigDecimal bigDecimal = new BigDecimal(one);
                                    BigDecimal bigDecimalTwo = new BigDecimal(paseSum);
                                    id_tips.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + "." + two);
                                    parserToRmb(bigDecimalTwo);
                                }

                            } else {
                                BigDecimal bigDecimal = new BigDecimal(paseSum);
                                id_tips.setText(DateUtil.formatPaseMoneyUtil(bigDecimal));
                                parserToRmb(bigDecimal);
                            }
                        }
                    } else if (sumTips.contains("-") && !sumTips.startsWith("-")) {
                        if (sumTips.endsWith("-")) {
                            id_tips.setText(sumTips.substring(0, sumTips.length() - 1));
                        } else {
                            id_tips.setText(sumTips.substring(sumTips.indexOf("-") + 1));
                        }
                    } else {
                        if (StringUtil.isEmptyOrNull(sumTips)) {
                            id_tips.setText(getDefaultValue());
                        } else {
                            if (sumTips.contains(".")) {
                                if (sumTips.endsWith(".")) {
                                    String paseSum = sumTips.substring(0, sumTips.lastIndexOf("."));
                                    BigDecimal bigDecimal = new BigDecimal(paseSum);
                                    id_tips.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + ".");
                                    parserToRmb(bigDecimal);
                                } else {
                                    String one = sumTips.substring(0, sumTips.lastIndexOf("."));
                                    String two = sumTips.substring(sumTips.lastIndexOf(".") + 1);
                                    BigDecimal bigDecimal = new BigDecimal(one);
                                    BigDecimal bigDecimalTwo = new BigDecimal(sumTips);
                                    id_tips.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + "." + two);
                                    parserToRmb(bigDecimalTwo);
                                }
                            } else {
                                BigDecimal bigDecimal = new BigDecimal(sumTips);
                                id_tips.setText(DateUtil.formatPaseMoneyUtil(bigDecimal));
                                parserToRmb(bigDecimal);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }
        } else {
            try {
                Logger.i("hehui", "sum-->" + sum);
                boolean isInt = Utils.Integer.isInteger(sum);
                if (isInt && !TextUtils.isEmpty(sum)) {
                    BigDecimal bigDecimal = new BigDecimal(sum);
                    parserToRmb(bigDecimal);
                    etMoney.setText(String.valueOf(DateUtil.formatPaseMoneyUtil(bigDecimal)));
                    changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal));
                } else { //如果不是整数，则是可能是带符号的情况或者带小数的情况
                    if (sum.contains("+")) {
                        if (sum.endsWith("+") ) {
                            String paseSum = sum.substring(0, sum.length() - 1);
                            BigDecimal bigDecimal = new BigDecimal(paseSum);
                            if (paseSum.contains(".")) {
                                //获取当前数据小数点后面0的个数
                                String strDot=".";
                                for(int i = 0 ; i < MainApplication.numFixed ; i++){
                                    strDot = strDot+"0";
                                    if (paseSum.endsWith(strDot)) {
                                        etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + strDot);
                                        changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal) + strDot);
                                        parserToRmb(bigDecimal);
                                    }else {
                                        etMoney.setText(String.valueOf(DateUtil.formatPaseMoneyUtil(bigDecimal)));
                                        changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal));
                                        parserToRmb(bigDecimal);
                                    }
                                }
                                /*if (paseSum.endsWith(".00")) {
                                    etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + ".00");
                                    changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal) + ".00");
                                    parserToRmb(bigDecimal);
                                } else if(paseSum.endsWith(".0")){
                                    etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + ".0");
                                    changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal) + ".0");
                                    parserToRmb(bigDecimal);
                                } else {
                                    etMoney.setText(String.valueOf(DateUtil.formatPaseMoneyUtil(bigDecimal)));
                                    changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal));
                                    parserToRmb(bigDecimal);
                                }*/
                            } else {
                                etMoney.setText(String.valueOf(DateUtil.formatPaseMoneyUtil(bigDecimal)));
                                changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal));
                                parserToRmb(bigDecimal);
                            }
                        } else {
                            String paseSum = sum.substring(sum.indexOf("+") + 1);
                            if (paseSum.contains(".")) {
                                if (paseSum.endsWith(".")) {
                                    String one = paseSum.substring(0, paseSum.lastIndexOf("."));
                                    BigDecimal bigDecimal = new BigDecimal(one);

                                    etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + ".");
                                    changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal) + ".");
                                    parserToRmb(bigDecimal);
                                } else {
                                    String one = paseSum.substring(0, paseSum.lastIndexOf("."));
                                    String two = paseSum.substring(paseSum.lastIndexOf(".") + 1);
                                    BigDecimal bigDecimal = new BigDecimal(one);
                                    BigDecimal bigDecimalTwo = new BigDecimal(paseSum);

                                    etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + "." + two);
                                    changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal) + "." + two);
                                    parserToRmb(bigDecimalTwo);
                                }

                            } else {
                                BigDecimal bigDecimal = new BigDecimal(paseSum);

                                etMoney.setText(String.valueOf(DateUtil.formatPaseMoneyUtil(bigDecimal)));
                                changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal));
                                parserToRmb(bigDecimal);
                            }
                        }
                    } else if (sum.contains("-") && !sum.startsWith("-")) {
                        if (sum.endsWith("-")) {

                            etMoney.setText(sum.substring(0, sum.length() - 1));
                            changeTextSize(etMoney,sum.substring(0, sum.length() - 1));
                        } else {

                            etMoney.setText(sum.substring(sum.indexOf("-") + 1));
                            changeTextSize(etMoney,sum.substring(sum.indexOf("-") + 1));

                        }
                    } else {
                        if (StringUtil.isEmptyOrNull(sum)) {

                            etMoney.setText(getDefaultValue());
                            changeTextSize(etMoney,getDefaultValue());
                            initViewSurcharge();
                            tv_pase.setText(MainApplication.feeFh+getDefaultValue());
                        } else {
                            if (sum.contains(".")) {
                                if (sum.endsWith(".")) {
                                    String paseSum = sum.substring(0, sum.lastIndexOf("."));
                                    BigDecimal bigDecimal = new BigDecimal(paseSum);

                                    etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + ".");
                                    changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal) + ".");
                                    parserToRmb(bigDecimal);
                                } else {
                                    String one = sum.substring(0, sum.lastIndexOf("."));
                                    String two = sum.substring(sum.lastIndexOf(".") + 1);
                                    BigDecimal bigDecimal = new BigDecimal(one);
                                    BigDecimal bigDecimalTwo = new BigDecimal(sum);

                                    etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal) + "." + two);
                                    changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal) + "." + two);
                                    parserToRmb(bigDecimalTwo);
                                }
                            } else {
                                BigDecimal bigDecimal = new BigDecimal(sum);

                                etMoney.setText(String.valueOf(DateUtil.formatPaseMoneyUtil(bigDecimal)));
                                changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal));
                                parserToRmb(bigDecimal);
                            }
                        }
                    }

                    String txMoeny = etMoney.getText().toString();
                    if(StringToMoney(txMoeny) == 0|| txMoeny.equals(".")|| txMoeny.equals("+")){
                        ly_to_scan_pay.setEnabled(false);
                    }else{
                        ly_to_scan_pay.setEnabled(true);
                    }
                }
            } catch (Exception e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }

        }

    }

    private int deletePoint;
    private int deletePointTips;

    //删除按钮，每次删除一位
    private void etNumberClear() {
        //小费输入模式
        if (currentMode == 1) {
            try {
                AppHelper.execVibrator(mContext);
                String ret = "";
                ret = id_tips.getText().toString();
                if (sumTips.length() > 0) {
                    if (ret.length() == deletePointTips) {
                        deletePointTips = 0;
                        sumTips = ret.substring(0, ret.length() - 2);
                        BigDecimal bigDecimal = new BigDecimal(sumTips);
                        parserToRmb(bigDecimal);
                    } else {
                        if (ret.length() > 1) {
                            id_tips.setText(ret.substring(0, ret.length() - 1));
                            sumTips = sumTips.substring(0, sumTips.length() - 1);
                            if (!StringUtil.isEmptyOrNull(twoTips)) {
                                twoTips = twoTips.substring(0, twoTips.length() - 1);
                            }
                            BigDecimal bigDecimal = new BigDecimal(sumTips);
                            parserToRmb(bigDecimal);
                        } else {
                            if (sumTips.contains("+")) {
                                sumTips = sumTips.substring(0, sumTips.indexOf("+"));
                                id_tips.setText(sumTips);
                                BigDecimal bigDecimal = new BigDecimal(sumTips);
                                parserToRmb(bigDecimal);
                            } else if (sumTips.contains("-")) {
                                sumTips = sumTips.substring(0, sumTips.indexOf("-"));
                                id_tips.setText(sumTips);
                                BigDecimal bigDecimal = new BigDecimal(sumTips);
                                parserToRmb(bigDecimal);
                            } else {
                                sumTips = sumTips.substring(0, sumTips.length() - 1);
                                if (sumTips.equals("")) {
                                    id_tips.setText(getDefaultValue());

                                    twoTips = "";
                                    sumTips = "";
                                } else {
                                    BigDecimal bigDecimal = new BigDecimal(sumTips);
                                    parserToRmb(bigDecimal);
                                    id_tips.setText(sumTips);
                                }
                            }
                        }
                    }
                } else {
                    id_tips.setText(getDefaultValue());
                    sum = "";
                    if (tv_pase != null) {
                        tv_pase.setText(MainApplication.feeFh+getDefaultValue());
                    }
                }
            } catch (Exception e) {
                Logger.i(e);
            }
        } else {
            try {
                AppHelper.execVibrator(mContext);
                String ret = "";
                ret = etMoney.getText().toString();
                //去掉当前字符串中除去0-9以及小数点的其他字符
                Pattern p = Pattern.compile("[^0-9.]");
                Matcher m = p.matcher(ret);
                ret = m.replaceAll("");
                BigDecimal bigDecimalMoney = new BigDecimal(ret);
                double inputMoney = bigDecimalMoney.doubleValue();

                if (sum.length() > 0) {
                    if (ret.length() == deletePoint) {
                        deletePoint = 0;
                        sum = ret.substring(0, ret.length() - 2);
                        BigDecimal bigDecimal = new BigDecimal(sum);
                        parserToRmb(bigDecimal);
                    } else {
                        if(ret.equals(getDefaultValue())){
                            return;
                        }
                        if (ret.length() > 1) {
                            String currentStr = ret.substring(0, ret.length() - 1);
                            BigDecimal bigDecimal_text = new BigDecimal(currentStr);

                            if(currentStr.endsWith(".")){
                                etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal_text)+".");
                                changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal_text)+".");
                            }else if(currentStr.contains(".") && currentStr.endsWith("0") ){
                                String str = ".";
                                for(int i = 1; i < MainApplication.numFixed ; i++){
                                    str = str+"0";
                                    if(currentStr.endsWith(str) ){
                                        etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal_text)+str);
                                        changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal_text)+str);
                                    }
                                }
                              /*  etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal_text)+".0");
                                changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal_text)+".0");*/
                            } else {
                                etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal_text));
                                changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal_text));
                            }

                            if(sum.endsWith("+")){
                                sum = sum.substring(0, sum.length() - 2);
                            }else{
                                sum = sum.substring(0, sum.length() - 1);
                            }

                            if (!StringUtil.isEmptyOrNull(two)) {
                                two = two.substring(0, two.length() - 1);
                            }

//                            BigDecimal bigDecimal = new BigDecimal(sum);
                            parserToRmb(bigDecimal_text);
                        } else if(ret.length() == 1){
                            etMoney.setText(getDefaultValue());
                            changeTextSize(etMoney,getDefaultValue());
                            sum = sum.substring(0, sum.length() - 1);
                            two = "";
                            initViewSurcharge();
                            BigDecimal bigDecimal = new BigDecimal(getDefaultValue());
                            parserToRmb(bigDecimal);
                            if (tv_pase != null) {
                                tv_pase.setText(MainApplication.feeFh+getDefaultValue());
                            }
                        } else {
                            if (sum.contains("+")) {
                                sum = sum.substring(0, sum.indexOf("+"));
                                BigDecimal bigDecimal_text = new BigDecimal(sum);

                                etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal_text));
                                changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal_text));
//                                BigDecimal bigDecimal = new BigDecimal(sum);
                                parserToRmb(bigDecimal_text);
                            } else if (sum.contains("-")) {
                                sum = sum.substring(0, sum.indexOf("-"));
                                BigDecimal bigDecimal_text = new BigDecimal(sum);

                                etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal_text));
                                changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal_text));
//                                BigDecimal bigDecimal = new BigDecimal(sum);
                                parserToRmb(bigDecimal_text);
                            } else {
                                sum = sum.substring(0, sum.length() - 1);
                                if (sum.equals("")) {

                                    etMoney.setText(getDefaultValue());
                                    changeTextSize(etMoney,getDefaultValue());
                                    initViewSurcharge();
                                    two = "";
                                    sum = "";
                                    if (tv_pase != null) {
                                        tv_pase.setText(MainApplication.feeFh+getDefaultValue());
                                    }
                                } else {
                                    BigDecimal bigDecimal = new BigDecimal(sum);
                                    parserToRmb(bigDecimal);

                                    etMoney.setText(DateUtil.formatPaseMoneyUtil(bigDecimal));
                                    changeTextSize(etMoney,DateUtil.formatPaseMoneyUtil(bigDecimal));
                                }
                            }
                        }
                    }
                } else {

                    etMoney.setText(getDefaultValue());
                    changeTextSize(etMoney,getDefaultValue());
                    initViewSurcharge();
                    sum = "";
                    if (tv_pase != null) {
                        tv_pase.setText(MainApplication.feeFh+getDefaultValue());
                    }
                }
            } catch (Exception e) {
                Logger.i(e);
            }

        }

    }


    /**
     * 清空文本框
     */
    private void clearData() {
        if (currentMode == 1) {
            //小费模式
            id_tips.setText(getDefaultValue());
            twoTips = "";
            sumTips = "";
            initViewSurcharge();
        } else {
            two = "";
            sum = "";

            etMoney.setText(getDefaultValue());
            changeTextSize(etMoney,getDefaultValue());
            initViewSurcharge();
            tvMoneyUppercase.setText("");
            tv_pase.setText(MainApplication.feeFh+getDefaultValue());
        }
        setToPayView();
//        btnPay.setBackgroundResource(R.drawable.bottom_gray_reg);
//        btnWXPay.setOnClickListener(null);
        ivInputNumberDel.setVisibility(View.GONE);
        //isClickNumer = 1;

    }

    public void changeTextSize(TextView textView , String showString){
        int textLength = showString.length();
        if(textLength < 6){
            String txMoeny = etMoney.getText().toString();

            if(txMoeny.equals(".")|| txMoeny.equals("+") || StringToMoney(txMoeny) == 0){
                textView.setTextSize(48);
                btn_ic_money.setTextSize(48);
            }else{
                textView.setTextSize(40);
                btn_ic_money.setTextSize(40);
            }

        }else if(textLength >=6  && textLength <10){
            textView.setTextSize(35);
            btn_ic_money.setTextSize(35);
        }else{
            textView.setTextSize(30);
            btn_ic_money.setTextSize(30);
        }
    }

    void setToEqView() {
        isPay = false;
        iv_icon.setVisibility(View.GONE);
        id_lin_add.setVisibility(View.VISIBLE);
    }

    void setToPayView() {
        isPay = true;
        iv_icon.setVisibility(View.VISIBLE);
        id_lin_add.setVisibility(View.GONE);
    }


    private void toPay(final String payType) {

        onClickPay(payType);

    }


    private void onClickPay(final String payType) {

        AppHelper.execVibrator(mContext);

        if (!checkMoeny()) {
            return;
        }
        String msg = getString(R.string.spanning_pay_wechat_code);
        if (payType.equals(MainApplication.PAY_QQ_NATIVE) || payType.equalsIgnoreCase(MainApplication.PAY_QQ_NATIVE1)) {
            msg = getString(R.string.spanning_pay_qq_code);
        } else if (payType.equals(MainApplication.PAY_ZFB_NATIVE) || payType.startsWith(MainApplication.PAY_ZFB_NATIVE1)) {
            msg = getString(R.string.spanning_pay_zfb_code);
        } else if (payType.equals(MainApplication.PAY_JINGDONG_NATIVE)) {
            msg = getString(R.string.spanning_pay_jd_code);
        }

        dialog1 = new ProgressInfoDialog(mActivity, msg, new ProgressInfoDialog.HandleBtn() {

            @Override
            public void handleOkBtn() {
                showConfirm(getStringById(R.string.interrupt_trading), dialog1);
            }

        });

        OrderManager.getInstance().unifiedNativePay(strTotalMoney, payType, 0, null, null, mark,new UINotifyListener<QRcodeInfo>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                DialogHelper.resize(mActivity, dialog1);
                if (!dialog1.isShowing()) {
                    dialog1.show();
                }
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                dialog1.dismiss();
                if (dialogInfo != null) {
                    dialogInfo.dismiss();
                }

                if (checkSession()) {
                    return;
                }

                if (!isStop) {
                    return;
                }
                if (object != null) {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            DialogInfo dialogInfo = new DialogInfo(mContext, getStringById(R.string.public_cozy_prompt), object.toString(), getStringById(R.string.btnOk), DialogInfo.REGISTFLAG, null, null);
                            DialogHelper.resize(mActivity, dialogInfo);
                            dialogInfo.show();
                        }
                    });
                }

            }

            @Override
            public void onSucceed(QRcodeInfo result) {
                super.onSucceed(result);
                dialog1.dismiss();
                if (dialogInfo != null) {
                    dialogInfo.dismiss();
                }
                if (result != null && isStop) {
                    result.setPayType(payType);
                    result.setWitch(isWitch);
                    ShowQRcodeActivity.startActivity(result, mContext,mark);
                    //clearData();
                } else {
                    isStop = true;
                }
            }
        });

    }
}
