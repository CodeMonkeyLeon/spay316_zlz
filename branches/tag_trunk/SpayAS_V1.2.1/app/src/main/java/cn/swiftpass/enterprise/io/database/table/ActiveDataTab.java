/*
 * 文 件 名:  ShopBankDataTab.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2014-3-20
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.io.database.table;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2014-3-20]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class ActiveDataTab extends TableBase
{
    
    public static final String TABLE_NAME_ACTIVE = "t_active";
    
    public static final String USER_NAME = "userName";
    
    public static final String START_TIME = "startTime";
    
    //    public static final String END_TIME = "endTime";
    
    //    public static final String COLUMN_BANK_CODE = "bankNumberCode";
    //    
    //    public static final String COLUMN_PARENT_BANK_ID = "parentBankId";
    
    @Override
    public void createTable(SQLiteDatabase db)
    {
        final String SQL_CREATE_TABLE =
            "create table " + TABLE_NAME_ACTIVE + "(" + USER_NAME + " VARCHAR(50)," + START_TIME + " VARCHAR(50))";
        Log.i("hehui", "ActiveDataTab SQL_CREATE_TABLE-->" + SQL_CREATE_TABLE);
        db.execSQL(SQL_CREATE_TABLE);
    }
    
}
