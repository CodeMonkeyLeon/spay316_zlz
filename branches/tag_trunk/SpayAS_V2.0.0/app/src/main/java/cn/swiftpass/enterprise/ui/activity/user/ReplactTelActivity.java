/*
 * 文 件 名:  ReplactTelActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2015-3-12
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.user;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.VerifyUtil;

/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2015-3-12]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class ReplactTelActivity extends TemplateActivity
{
    
    private TextView replaceTitle;
    
    private EditText userPhone, password_id, code;
    
    private ImageView iv_pwd_promt, iv_clearPwd, iv_phone_promt, iv_clearPhone;
    
    private Button confirm, getCode;
    
    private TimeCount time;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.replace_phone);
        
        initView();
        
        setLister();
    }
    
    /**
     *验证码倒计时60秒
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void Countdown()
    {
        // 总共60 没间隔1秒
        time = new TimeCount(60000, 1000);
        time.start();
    }
    
    class TimeCount extends CountDownTimer
    {
        public TimeCount(long millisInFuture, long countDownInterval)
        {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }
        
        @Override
        public void onFinish()
        {//计时完毕时触发
            getCode.setEnabled(true);
            getCode.setClickable(true);
            getCode.setText("重新获取验证码");
        }
        
        @Override
        public void onTick(long millisUntilFinished)
        {//计时过程显示
            getCode.setEnabled(false);
            getCode.setClickable(false);
            getCode.setText("重新获取验证码" + millisUntilFinished / 1000 + "秒");
        }
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void setLister()
    {
        
        iv_clearPwd.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                password_id.setText("");
            }
        });
        
        iv_clearPhone.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                userPhone.setText("");
            }
        });
        confirm.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (isAbsoluteNullStr(userPhone.getText().toString()))
                {
                    showToastInfo("手机号码不能为空");
                    return;
                }
                
                if (isAbsoluteNullStr(code.getText().toString()))
                {
                    showToastInfo("验证码不能为空");
                    return;
                }
                
                if (isAbsoluteNullStr(password_id.getText().toString()))
                {
                    showToastInfo("密码不能为空");
                    return;
                }
                
                UserManager.replasePhone(MainApplication.phone, userPhone.getText().toString(), password_id.getText()
                    .toString(), code.getText().toString(), new UINotifyListener<Boolean>()
                {
                    @Override
                    public void onPreExecute()
                    {
                        super.onPreExecute();
                        showLoading(false, "确认中...");
                    }
                    
                    @Override
                    public void onError(final Object object)
                    {
                        super.onError(object);
                        dismissLoading();
                        if (object != null)
                        {
                            //                    showToastInfo(object.toString());
                            ReplactTelActivity.this.runOnUiThread(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    //                                    DialogInfoSdk dialogInfo =
                                    //                                        new DialogInfoSdk(ReplactTelActivity.this,
                                    //                                            ReplactTelActivity.this.getResources()
                                    //                                                .getString(Resourcemap.getById_title_prompt()), "" + object.toString(),
                                    //                                            "确定", DialogInfoSdk.SUBMIT_NO_CANLE, null);
                                    //                                    
                                    //                                    DialogHelper.resize(ReplactTelActivity.this, dialogInfo);
                                    //                                    dialogInfo.show();
                                }
                            });
                        }
                    }
                    
                    @Override
                    public void onSucceed(Boolean result)
                    {
                        super.onSucceed(result);
                        dismissLoading();
                        if (result)
                        {
                            showToastInfo("手机号码切换成功");
                            finish();
                        }
                        
                    }
                });
                
            }
        });
        
        getCode.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                String telNumber = userPhone.getText().toString();
                if ("".equals(telNumber) || telNumber == null)
                {
                    showToastInfo("手机号码不能为空");
                    userPhone.setFocusable(true);
                    
                    return;
                }
                else
                {
                    if (!VerifyUtil.verifyValid(userPhone.getText().toString(), GlobalConstant.MOBILEPHOE))
                    {
                        showToastInfo("输入手机号码格式不正确");
                        userPhone.setFocusable(true);
                        return;
                    }
                }
                
                Countdown();
                getCode.setEnabled(false);
                UserManager.getRegisterCode(telNumber, "phoneCode", new UINotifyListener<Integer>()
                {
                    @Override
                    public void onPreExecute()
                    {
                        super.onPreExecute();
                        titleBar.setRightLodingVisible(true);
                    }
                    
                    @Override
                    public void onError(Object object)
                    {
                        super.onError(object);
                        if (object != null)
                        {
                            showToastInfo(object.toString());
                        }
                        titleBar.setRightLodingVisible(false, false);
                        getCode.setEnabled(true);
                        
                    }
                    
                    @Override
                    public void onSucceed(Integer result)
                    {
                        super.onSucceed(result);
                        titleBar.setRightLodingVisible(false, false);
                        getCode.setEnabled(true);
                        getCode.setClickable(true);
                        switch (result)
                        {
                        // 成功
                            case 0:
                                showToastInfo("获取验证码成功,请注意查收短信!");
                                return;
                                // 手机号码以注册
                            case 2:
                                showToastInfo("服务器错误，请稍后在试!");
                                return;
                            case RequestResult.RESULT_TIMEOUT_ERROR: // 请求超时
                                showToastInfo("连接超时，请稍微再试!");
                                return;
                                
                        }
                        
                    }
                });
            }
        });
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView()
    {
        replaceTitle = getViewById(R.id.replaceTitle);
        
        replaceTitle.setText("温馨提示：您当前登陆的手机号码为" + MainApplication.phone + ",更换后，登陆时需使用新手机号码");
        
        userPhone = getViewById(R.id.userPhone);
        
        password_id = getViewById(R.id.password_id);
        
        iv_clearPwd = getViewById(R.id.iv_clearPwd);
        
        iv_pwd_promt = getViewById(R.id.iv_pwd_promt);
        
        iv_phone_promt = getViewById(R.id.iv_phone_promt);
        
        iv_clearPhone = getViewById(R.id.iv_clearPhone);
        
        confirm = getViewById(R.id.confirm);
        
        code = getViewById(R.id.code);
        
        getCode = getViewById(R.id.getCode);
        
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        {
            
            @Override
            public void onExecute(CharSequence s, int start, int before, int count)
            {
                if (userPhone.hasFocus())
                {
                    if (userPhone.getText().length() > 0)
                    {
                        iv_clearPhone.setVisibility(View.VISIBLE);
                        iv_phone_promt.setVisibility(View.GONE);
                    }
                    else
                    {
                        iv_clearPhone.setVisibility(View.GONE);
                        iv_phone_promt.setVisibility(View.VISIBLE);
                    }
                    
                }
                if (password_id.hasFocus())
                {
                    if (password_id.getText().toString().length() > 0)
                    {
                        iv_clearPwd.setVisibility(View.VISIBLE);
                        iv_pwd_promt.setVisibility(View.GONE);
                    }
                    else
                    {
                        iv_clearPwd.setVisibility(View.GONE);
                        iv_pwd_promt.setVisibility(View.VISIBLE);
                    }
                    
                }
            }
            
            @Override
            public void onAfterTextChanged(Editable s)
            {
            }
            
        });
        
        userPhone.addTextChangedListener(editTextWatcher);
        password_id.addTextChangedListener(editTextWatcher);
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle("更换手机");
    }
    
}
