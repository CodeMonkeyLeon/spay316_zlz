package cn.swiftpass.enterprise.ui.activity;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.shop.PersonalManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.City;
import cn.swiftpass.enterprise.bussiness.model.CountryAndUnitMode;
import cn.swiftpass.enterprise.bussiness.model.ShopBankBaseInfo;
import cn.swiftpass.enterprise.bussiness.model.ShopBaseDataInfo;
import cn.swiftpass.enterprise.io.database.CityDao;
import cn.swiftpass.enterprise.io.database.access.CountryAndUnitDB;
import cn.swiftpass.enterprise.io.database.access.ShopBankDataDB;
import cn.swiftpass.enterprise.io.database.access.ShopBaseDataDB;
import cn.swiftpass.enterprise.ui.activity.shop.ImagePase;
import cn.swiftpass.enterprise.ui.activity.shop.ImagePreviewActivity;
import cn.swiftpass.enterprise.ui.activity.shop.ShopkeeperNextActivity;
import cn.swiftpass.enterprise.ui.widget.SelectTowDialog;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.HelpUI;
import cn.swiftpass.enterprise.utils.HelpUI.PopModel;
import cn.swiftpass.enterprise.utils.VerifyUtil;

public class RegisterNewActivity extends TemplateActivity
{
    
    private TextView cityTv;
    
    private TextView provinceTv;
    
    private TextView industryTv, country_tv, currecy_receive_tv;
    
    private TextView bank_name;
    
    private String region_id;
    
    private CityDao dao;
    
    private List<City> shopCityArrs = new ArrayList<City>();
    
    private List<HelpUI.PopModel> countryList = null;
    
    private List<HelpUI.PopModel> bankList = null;
    
    private TextView user_phone, shop_name, is_big_seller, request_code;
    
    private TextView detail_address, mail_address, bank_card_owner_id, bank_card_no_id, id_card_id, child_bank_name;
    
    private List<City> provinces = new ArrayList<City>();
    
    private SelectTowDialog takImgDialog;
    
    private String imageUrl;
    
    private File tempFile;
    
    private Uri originalUri;
    
    private int imageID;
    
    private ImageView business_license, operator_id_card, code_book;
    
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x1001;
    
    public static final int REQUEST_CODE_CAPTURE_PICTURE = 0x1002;
    
    public static boolean cancel_perview = true;
    
    private SharedPreferences sp;
    
    private static final int REG_STATE_FIRST_STEP = 1;
    
    private static final int REG_STATE_SECOND_STEP = 2;
    
    private static final int REG_STATE_THIRD_STEP = 3;
    
    private static final int REG_STATE_FINISH = 4;
    
    private int regState;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        sp = getSharedPreferences(getClass().getName(), 0);
        dao = CityDao.getInstance(RegisterNewActivity.this);
        initViewOfStepFirst();
        
    }
    
    private void initViewOfStepFirst()
    {
        
        setContentView(R.layout.register_step_1);
        regState = REG_STATE_FIRST_STEP;
        
        industryTv = getViewById(R.id.industry_tv);
        user_phone = getViewById(R.id.userPhone);
        shop_name = getViewById(R.id.shop_name);
        is_big_seller = getViewById(R.id.is_big_seller_tv);
        country_tv = getViewById(R.id.country_tv);
        currecy_receive_tv = getViewById(R.id.currecy_receive);
        request_code = getViewById(R.id.requestcode_id);
        
        loadTextFromSp(industryTv);
        loadTextFromSp(user_phone);
        loadTextFromSp(shop_name);
        loadTextFromSp(is_big_seller);
        loadTextFromSp(country_tv);
        loadTextFromSp(currecy_receive_tv);
        loadTextFromSp(request_code);
        
        loadCountryAndUnitData();
        
    }
    
    private void initViewOfStepSecond()
    {
        
        regState = REG_STATE_SECOND_STEP;
        
        setContentView(R.layout.register_step_2);
        
        cityTv = getViewById(R.id.citys);
        provinceTv = getViewById(R.id.province_tv);
        bank_name = getViewById(R.id.bank_name);
        detail_address = getViewById(R.id.detail_address);
        mail_address = getViewById(R.id.mail_address);
        bank_card_owner_id = getViewById(R.id.bank_card_owner_id);
        bank_card_no_id = getViewById(R.id.bank_card_no_id);
        id_card_id = getViewById(R.id.id_card_id);
        child_bank_name = getViewById(R.id.child_bank_name);
        
        loadTextFromSp(cityTv);
        loadTextFromSp(provinceTv);
        loadTextFromSp(bank_name);
        loadTextFromSp(detail_address);
        loadTextFromSp(mail_address);
        loadTextFromSp(bank_card_owner_id);
        loadTextFromSp(bank_card_no_id);
        loadTextFromSp(id_card_id);
        loadTextFromSp(child_bank_name);
        
        loadBankData();
        
    }
    
    private void loadTextFromSp(TextView tv)
    {
        tv.setText(sp.getString(String.valueOf(tv.getId()), ""));
    }
    
    private void saveTextFromSp(TextView tv, Editor editor)
    {
        editor.putString(String.valueOf(tv.getId()), tv.getText().toString());
    }
    
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    /** 跳往第二步
     * <功能详细描述>
     * @param view
     * @see [类、类#方法、类#成员]
     */
    public void regToSecondStep(View view)
    {
        
        if (!isAbsoluteNullStr(user_phone.getText().toString()))
        {
            if (!VerifyUtil.verifyValid(user_phone.getText().toString(), GlobalConstant.MOBILEPHOE))
            {
                showToastInfo("输入手机号码格式不正确");
                user_phone.setText("");
                user_phone.setFocusable(true);
                return;
            }
        }
        else
        {
            showToastInfo("手机号码不能为空");
            user_phone.setFocusable(true);
            return;
        }
        
        if (isAbsoluteNullStr(shop_name.getText().toString()))
        {
            showToastInfo("商户名称不能为空");
            shop_name.setFocusable(true);
            return;
        }
        
        if (isAbsoluteNullStr(industryTv.getText().toString()))
        {
            showToastInfo("请选择所属行业");
            return;
        }
        
        if (isAbsoluteNullStr(is_big_seller.getText().toString()))
        {
            showToastInfo("请选择是否为大商户");
            return;
        }
        
        saveStepFirstData();
        
        initViewOfStepSecond();
    }
    
    /** 跳往第三步
     * <功能详细描述>
     * @param view
     * @see [类、类#方法、类#成员]
     */
    public void regToThirdStep(View view)
    {
        
        if (isAbsoluteNullStr(provinceTv.getText().toString()))
        {
            showToastInfo("请选择省份");
            return;
        }
        if (isAbsoluteNullStr(cityTv.getText().toString()))
        {
            showToastInfo("请选择市");
            return;
        }
        if (isAbsoluteNullStr(detail_address.getText().toString()))
        {
            showToastInfo(detail_address.getHint().toString());
            detail_address.requestFocus();
            return;
        }
        if (isAbsoluteNullStr(mail_address.getText().toString()))
        {
            showToastInfo(mail_address.getText().toString());
            mail_address.requestFocus();
            return;
        }
        
        if (isAbsoluteNullStr(bank_name.getText().toString()))
        {
            showToastInfo(bank_name.getHint().toString());
            return;
        }
        
        if (isAbsoluteNullStr(child_bank_name.getText().toString()))
        {
            showToastInfo(child_bank_name.getHint().toString());
            return;
        }
        
        if (isAbsoluteNullStr(bank_card_owner_id.getText().toString()))
        {
            showToastInfo(bank_card_owner_id.getHint().toString());
            bank_card_owner_id.requestFocus();
            return;
        }
        if (isAbsoluteNullStr(bank_card_no_id.getText().toString()))
        {
            showToastInfo(bank_card_no_id.getHint().toString());
            bank_card_no_id.requestFocus();
            return;
        }
        
        if (isAbsoluteNullStr(id_card_id.getText().toString()))
        {
            showToastInfo(id_card_id.getHint().toString());
            id_card_id.requestFocus();
            return;
        }
        
        saveStepSecondData();
        
        initViewOfStepThird();
        
    }
    
    private void initViewOfStepThird()
    {
        regState = REG_STATE_THIRD_STEP;
        setContentView(R.layout.register_step_3);
        business_license = getViewById(R.id.business_license);
        operator_id_card = getViewById(R.id.operator_id_card);
        code_book = getViewById(R.id.code_book);
        
        Executors.newSingleThreadExecutor().execute(new Runnable()
        {
            
            @Override
            public void run()
            {
                loadBitmapToImageView(business_license);
                loadBitmapToImageView(operator_id_card);
                loadBitmapToImageView(code_book);
            }
        });
        
    };
    
    private void loadBitmapToImageView(final ImageView imageView)
    {
        final String filePath = sp.getString(String.valueOf(imageView.getId()), null);
        
        if (filePath == null)
        {
            return;
        }
        
        Bitmap bitmap_pic = null;
        // 得到缩略图的路径
        try
        {
            
            File file = new File(filePath);
            if (file.exists() && file.length() / 1024 > 100)
            {
                bitmap_pic = ImagePase.createBitmap(filePath, ImagePase.bitmapSize_Image);
            }
            else
            {
                bitmap_pic = ImagePase.readBitmapFromStream(filePath);
            }
            
            if (bitmap_pic != null)
            {
                
                final Bitmap final_bitmap_pic = bitmap_pic;
                runOnUiThread(new Runnable()
                {
                    
                    @Override
                    public void run()
                    {
                        imageView.setImageBitmap(final_bitmap_pic);
                        hideUploadTextView(imageView);
                    }
                });
                
            }
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    private void saveStepFirstData()
    {
        Editor editor = sp.edit();
        saveTextFromSp(user_phone, editor);
        saveTextFromSp(shop_name, editor);
        saveTextFromSp(industryTv, editor);
        saveTextFromSp(is_big_seller, editor);
        saveTextFromSp(request_code, editor);
        saveTextFromSp(country_tv, editor);
        saveTextFromSp(currecy_receive_tv, editor);
        editor.commit();
    }
    
    private void saveStepSecondData()
    {
        Editor editor = sp.edit();
        saveTextFromSp(provinceTv, editor);
        saveTextFromSp(cityTv, editor);
        saveTextFromSp(detail_address, editor);
        saveTextFromSp(mail_address, editor);
        saveTextFromSp(bank_name, editor);
        saveTextFromSp(child_bank_name, editor);
        saveTextFromSp(bank_card_owner_id, editor);
        saveTextFromSp(bank_card_no_id, editor);
        saveTextFromSp(id_card_id, editor);
        editor.commit();
    }
    
    private void saveStepThirdData(ImageView iv, String imgPath)
    {
        Editor editor = sp.edit();
        editor.putString(String.valueOf(iv.getId()), imgPath);
        editor.commit();
    }
    
    @Override
    public void onBackPressed()
    {
        
        if (regState == REG_STATE_SECOND_STEP)
        {
            //当前第二步注册，返回就是回到第一步
            saveStepSecondData();
            initViewOfStepFirst();
        }
        else if (regState == REG_STATE_THIRD_STEP)
        {
            //当前第三步注册，返回就是回到第二步
            initViewOfStepSecond();
        }
        else if (regState == REG_STATE_FIRST_STEP)
        {
            //TODO 退出注册流程清空数据
            showRegExitDialog(RegisterNewActivity.this);
            
        }
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onLeftButtonClick()
            {
                onBackPressed();
            }
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                
            }
        });
        
        titleBar.setTitle(R.string.title_register);
    }
    
    /** 选择行业
     * <功能详细描述>
     * @param view
     * @see [类、类#方法、类#成员]
     */
    public void selectIndustry(View view)
    {
        List<ShopBaseDataInfo> baseInfo = ShopBaseDataDB.getInstance().query();
        List<HelpUI.PopModel> bunisArrs = new ArrayList<HelpUI.PopModel>();
        HelpUI.PopModel uiModel = null;
        for (ShopBaseDataInfo info : baseInfo)
        {
            uiModel = new HelpUI.PopModel();
            uiModel.id = info.getTypeno();
            uiModel.label = info.getName();
            
            bunisArrs.add(uiModel);
            
        }
        if (bunisArrs.size() > 0)
        {
            HelpUI.showRegPopWindow(getApplicationContext(), view.getWidth() - 200, bunisArrs, industryTv, null);
        }
        
    }
    
    /** 选择是否为大商户
     * <功能详细描述>
     * @param view
     * @see [类、类#方法、类#成员]
     */
    public void selectIsBigSeller(View view)
    {
        List<HelpUI.PopModel> popArrs = new ArrayList<HelpUI.PopModel>();
        HelpUI.PopModel seller = null;
        seller = new HelpUI.PopModel();
        seller.label = "普通商户";
        seller.id = "1";
        popArrs.add(seller);
        seller = new HelpUI.PopModel();
        seller.label = "大商户";
        seller.id = "2";
        popArrs.add(seller);
        HelpUI.showRegPopWindow(getApplicationContext(), view.getWidth() - 200, popArrs, is_big_seller, null);
    }
    
    public void selectProvince(View view)
    {
        final CityDao dao = CityDao.getInstance(getApplicationContext());
        provinces = dao.getAllProvince();
        HelpUI.PopModel uiModel = null;
        List<HelpUI.PopModel> popArrs = new ArrayList<HelpUI.PopModel>();
        for (City info : provinces)
        {
            uiModel = new HelpUI.PopModel();
            uiModel.id = info.getNumber();
            uiModel.label = info.getCity();
            
            popArrs.add(uiModel);
            
        }
        HelpUI.showRegPopWindow(getApplicationContext(),
            view.getWidth() - 100,
            popArrs,
            provinceTv,
            new HelpUI.onSelectItemListener()
            {
                
                @Override
                public void onSelectItemEvent(int selectIdex, PopModel selectModel)
                {
                    String interStr = selectModel.id.substring(0, 2);
                    List<City> shopCityArrs = dao.getAllCityByID(interStr);
                    if (shopCityArrs.size() > 0)
                    {
                        cityTv.setText(shopCityArrs.get(0).getCity());
                        cityTv.setTag(shopCityArrs.get(0).getNumber());
                        
                    }
                }
            });
        
    }
    
    public void selectCity(View view)
    {
        
        if (isAbsoluteNullStr(provinceTv.getText().toString()))
        {
            showToastInfo(provinceTv.getHint().toString());
            return;
        }
        
        region_id = (String)provinceTv.getTag();
        // 截取region_id 前两位
        String interStr = region_id.substring(0, 2);
        shopCityArrs = dao.getAllCityByID(interStr);
        HelpUI.PopModel uiModel = null;
        if (shopCityArrs.size() > 0)
        {
            List<HelpUI.PopModel> popArrs = new ArrayList<HelpUI.PopModel>();
            for (City info : shopCityArrs)
            {
                uiModel = new HelpUI.PopModel();
                uiModel.id = info.getNumber();
                uiModel.label = info.getCity();
                
                popArrs.add(uiModel);
                
            }
            HelpUI.showRegPopWindow(getApplicationContext(), view.getWidth() - 100, popArrs, cityTv, null);
        }
    }
    
    public void selectBank(View view)
    {
        if (bankList == null || bankList.size() == 0)
        {
            return;
        }
        HelpUI.showRegPopWindow(RegisterNewActivity.this, view.getWidth() - 200, bankList, bank_name, null);
    }
    
    public void selectChildBank(View view)
    {
        
        if (isAbsoluteNullStr(bank_name.getText().toString()))
        {
            showToastInfo(bank_name.getHint().toString());
            return;
        }
        
        if (bank_name.getTag() != null)
        {
            String bankId = (String)bank_name.getTag();
            List<ShopBankBaseInfo> childBankList = ShopBankDataDB.getInstance().findChildBank(bankId);
            
            if (childBankList == null && childBankList.size() == 0)
            {
                return;
            }
            
            List<HelpUI.PopModel> childBankShowList = new ArrayList<HelpUI.PopModel>();
            
            for (ShopBankBaseInfo info : childBankList)
            {
                HelpUI.PopModel popModel = new HelpUI.PopModel();
                popModel.id = info.bankId;
                popModel.label = info.bankName;
                popModel.obWf = new WeakReference<Object>(info);
                childBankShowList.add(popModel);
            }
            
            HelpUI.showRegPopWindow(RegisterNewActivity.this,
                view.getWidth() - 200,
                childBankShowList,
                child_bank_name,
                new HelpUI.onSelectItemListener()
                {
                    
                    @Override
                    public void onSelectItemEvent(int selectIdex, PopModel selectModel)
                    {
                        child_bank_name.setText(selectModel.label);
                    }
                });
            
        }
        
    }
    
    public void selectCountry(View view)
    {
        if (countryList == null || countryList.size() == 0)
        {
            return;
        }
        HelpUI.showRegPopWindow(RegisterNewActivity.this,
            view.getWidth() - 200,
            countryList,
            country_tv,
            new HelpUI.onSelectItemListener()
            {
                
                @Override
                public void onSelectItemEvent(int selectIdex, PopModel selectModel)
                {
                    country_tv.setText(selectModel.label);
                    WeakReference<Object> wf = selectModel.obWf;
                    if (wf != null && wf.get() != null)
                    {
                        CountryAndUnitMode countryAndUnitMode = (CountryAndUnitMode)wf.get();
                        
                        currecy_receive_tv.setText(countryAndUnitMode.unitName);
                    }
                }
            });
    }
    
    private void loadCountryAndUnitData()
    {
        
        if (countryList == null)
        {
            countryList = new ArrayList<HelpUI.PopModel>();
        }
        else
        {
            return;
        }
        
        List<CountryAndUnitMode> countryAndUnitList = CountryAndUnitDB.getInstance().findAll();
        
        if (countryAndUnitList == null)
        {
            PersonalManager.getCountryAndUnit(new UINotifyListener<List<CountryAndUnitMode>>()
            {
                
                @Override
                public void onSucceed(List<CountryAndUnitMode> result)
                {
                    if (result == null || result.size() == 0)
                    {
                        return;
                    }
                    for (CountryAndUnitMode info : result)
                    {
                        CountryAndUnitDB.getInstance().save(info);
                        HelpUI.PopModel popModel = new HelpUI.PopModel();
                        popModel.id = String.valueOf(info.countryId);
                        popModel.label = info.countryName;
                        popModel.obWf = new WeakReference<Object>(info);
                        countryList.add(popModel);
                    }
                    
                }
                
            });
        }
        else
        {
            for (CountryAndUnitMode info : countryAndUnitList)
            {
                HelpUI.PopModel popModel = new HelpUI.PopModel();
                popModel.id = String.valueOf(info.countryId);
                popModel.label = info.countryName;
                popModel.obWf = new WeakReference<Object>(info);
                countryList.add(popModel);
            }
        }
        
    }
    
    private void loadBankData()
    {
        
        if (bankList == null)
        {
            bankList = new ArrayList<HelpUI.PopModel>();
        }
        else
        {
            return;
        }
        
        List<ShopBankBaseInfo> dataList = ShopBankDataDB.getInstance().query();
        
        if (dataList != null)
        {
            for (ShopBankBaseInfo info : dataList)
            {
                HelpUI.PopModel popModel = new HelpUI.PopModel();
                popModel.id = info.bankId;
                popModel.label = info.bankName;
                popModel.obWf = new WeakReference<Object>(info);
                bankList.add(popModel);
            }
        }
        
    }
    
    private void showRegExitDialog(final Context context)
    {
        DialogHelper.showDialog("提示", "注册未完成，确定要返回吗？", "取消", "确定", context, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                Editor editor = sp.edit();
                editor.clear();
                editor.commit();
                
                finish();
            }
        }, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                
            }
        }).show();
    }
    
    /**
     * 上传图片 <功能详细描述>
     * 
     * @param view
     * @see [类、类#方法、类#成员]
     */
    public void onUploadImg(final View view)
    {
        view.setEnabled(false);
        // 从相册选择照片
        final String status = Environment.getExternalStorageState();
        takImgDialog =
            new SelectTowDialog(RegisterNewActivity.this, "选择照片来源", new String[] {"相册", "拍照"},
                new SelectTowDialog.OnSelectTowListener()
                {
                    @Override
                    public void onTow()
                    {
                        try
                        {
                            // 是否有SD卡
                            if (status.equals(Environment.MEDIA_MOUNTED))
                            {
                                takeImg(view);
                            }
                            else
                            {
                                Toast.makeText(RegisterNewActivity.this, "没有SD卡，请检查SD卡是否可用!", Toast.LENGTH_SHORT)
                                    .show();
                            }
                            view.setEnabled(true);
                            takImgDialog.dismiss();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                    
                    @Override
                    public void onOne()
                    {
                        try
                        {
                            if (status.equals(Environment.MEDIA_MOUNTED))
                            {
                                startCamera(view);
                            }
                            else
                            {
                                Toast.makeText(RegisterNewActivity.this, "没有SD卡，请检查SD卡是否可用!", Toast.LENGTH_SHORT)
                                    .show();
                            }
                            view.setEnabled(true);
                            takImgDialog.dismiss();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                });
        DialogHelper.resize(this, takImgDialog);
        takImgDialog.show();
        takImgDialog.setCanceledOnTouchOutside(true);
        view.setEnabled(true);
        
    }
    
    private void startCamera(View view)
        throws Exception
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String root = AppHelper.getImgCacheDir();
        imageUrl = root + String.valueOf(new Date().getTime()) + ".jpg";
        tempFile = new File(imageUrl);
        originalUri = Uri.fromFile(tempFile);
        
        ImageView imageView = (ImageView)view;
        
        imageID = imageView.getId();
        
        HandlerManager.notifyMessage(HandlerManager.SHOWPIC, HandlerManager.SHOWPIC, imageView.getId());
        intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, originalUri);
        startActivityForResult(intent, REQUEST_CODE_CAPTURE_PICTURE);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
        {
            switch (requestCode)
            {
            // 相册获取图片
                case REQUEST_CODE_TAKE_PICTURE:
                    if (ShopkeeperNextActivity.IMAGE_PREVIEW)
                    {
                        originalUri = data.getData();
                        Intent intent = new Intent();
                        intent.setData(originalUri);
                        intent.setClass(this, ImagePreviewActivity.class);
                        
                        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
                        return;
                    }
                    else
                    {
                        ShopkeeperNextActivity.IMAGE_PREVIEW = true;
                    }
                    
                    if (originalUri == null)
                    {
                        break;
                    }
                    // 取消
                    if (!cancel_perview)
                    {
                        cancel_perview = true;
                        break;
                    }
                    ImageView imageView = (ImageView)findViewById(imageID);
                    
                    String path = getPicPath(originalUri);
                    saveStepThirdData(imageView, path);
                    originalUri = null;
                    Bitmap bitmap = null;
                    
                    try
                    {
                        
                        // File file = new File(path);
                        // String newUrlStr = path.substring(0,
                        // path.lastIndexOf('/'));
                        // newPath = newUrlStr + "/" + String.valueOf(new
                        // Date().getTime()) + ".jpg";
                        // if (file.exists())
                        // {
                        // newFile = new File(newPath);
                        // file.renameTo(newFile);
                        // }
                        // fos = new FileOutputStream(newFile);
                        if (path != null)
                        {
                            File file = new File(path);
                            if (file.exists() && file.length() / 1024 > 100)
                            {
                                bitmap = ImagePase.createBitmap(path, ImagePase.bitmapSize_Image);
                            }
                            else
                            {
                                // bitmap = BitmapFactory.decodeFile(path);
                                bitmap = ImagePase.readBitmapFromStream(path);
                            }
                            // bitmap = BitmapFactory.decodeFile(path);
                        }
                    }
                    catch (Exception e1)
                    {
                        e1.printStackTrace();
                    }
                    
                    if (bitmap != null)
                    {
                        imageView.setImageBitmap(bitmap);
                        hideUploadTextView(imageView);
                    }
                    
                    takImgDialog.dismiss();
                    break;
                
                // 拍照
                case REQUEST_CODE_CAPTURE_PICTURE:
                    
                    ImageView image = (ImageView)findViewById(imageID);
                    String pathPhoto = getPicPath(originalUri);
                    saveStepThirdData(image, pathPhoto);
                    Bitmap bitmap_pic = null;
                    // 得到缩略图的路径
                    try
                    {
                        
                        File file = new File(pathPhoto);
                        if (file.exists() && file.length() / 1024 > 100)
                        {
                            bitmap_pic = ImagePase.createBitmap(pathPhoto, ImagePase.bitmapSize_Image);
                        }
                        else
                        {
                            bitmap_pic = ImagePase.readBitmapFromStream(pathPhoto);
                        }
                        
                        if (bitmap_pic != null)
                        {
                            image.setImageBitmap(bitmap_pic);
                            hideUploadTextView(image);
                        }
                        else
                        {
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    
                    takImgDialog.dismiss();
                    break;
                default:
                    break;
            }
            
        }
        else
        {
            // Toast.makeText(this, "获取照片失败,重新获取！", Toast.LENGTH_LONG).show();
            return;
        }
    }
    
    private void hideUploadTextView(ImageView imageView)
    {
        //隐藏上传的文字
        TextView textView = (TextView)((FrameLayout)imageView.getParent()).getChildAt(1);
        textView.setVisibility(View.GONE);
    }
    
    /**
     * 获取相册图片
     * 
     * @throws Exception
     */
    public void takeImg(View view)
        throws Exception
    {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setDataAndType(MediaStore.Images.Media.INTERNAL_CONTENT_URI, "image/*");
        // intent.setType("image/*");
        // String root = AppHelper.getImgCacheDir();
        // imageUrl = root + String.valueOf(new Date().getTime()) + ".jpg";
        // tempFile = new File(imageUrl);
        
        ImageView imageView = (ImageView)view;
        
        imageID = imageView.getId();
        
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
    }
    
    /**
     * 
     * 返回图片地址
     * 
     * @param originalUri
     * @return String
     */
    @SuppressLint("NewApi")
    public String getPicPath(Uri originalUri)
    {
        ContentResolver mContentResolver = RegisterNewActivity.this.getContentResolver();
        String originalPath = null;
        if ((originalUri + "").contains("/sdcard"))
        {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/sdcard"));
        }
        else if ((originalUri + "").contains("/data"))
        {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/data"));
        }
        else if ((originalUri + "").contains("/storage")) // 小米 手机 适配
        {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/storage"));
        }
        else
        {
            
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT
                && originalUri.toString().contains("documents"))
            {
                String wholeID = DocumentsContract.getDocumentId(originalUri);
                String id = wholeID.split(":")[1];
                String[] column = {MediaStore.Images.Media.DATA};
                String sel = MediaStore.Images.Media._ID + " = ?";
                Cursor cursor =
                    mContentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column,
                        sel,
                        new String[] {id},
                        null);
                int columnIndex = cursor.getColumnIndex(column[0]);
                if (cursor.moveToFirst())
                {
                    originalPath = cursor.getString(columnIndex);
                }
                cursor.close();
            }
            else
            {
                
                Cursor cursor = mContentResolver.query(originalUri, null, null, null, null);
                if (cursor != null)
                {
                    cursor.moveToFirst();
                    originalPath = cursor.getString(1);
                    cursor.close();
                }
            }
            
        }
        
        return originalPath;
    }
    
}
