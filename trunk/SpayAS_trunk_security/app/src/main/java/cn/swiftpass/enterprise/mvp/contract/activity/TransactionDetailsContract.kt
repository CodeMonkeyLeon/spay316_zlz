package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.ReportTransactionDetailsBean
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/11/21
 *
 * @世界都湿了
 * @星星亮得怕人
 * @我收起伞
 * @天收起滴水的云
 * @时针转到零点
 * @㧟了上帝的脚跟
 * @你没有来
 * @我还在等
 */
class TransactionDetailsContract {

    interface View : BaseView {

        fun queryTransactionReportDetailsSuccess(response: ReportTransactionDetailsBean?)

        fun queryTransactionReportDetailsFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {

        fun queryTransactionReportDetails(
            reportType: String?,
            startDate: String?,
            endDate: String?
        )
    }


}