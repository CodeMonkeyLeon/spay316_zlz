package cn.swiftpass.enterprise.mvp.contract.fragment

import cn.swiftpass.enterprise.bussiness.model.DailyStatisticsBean
import cn.swiftpass.enterprise.bussiness.model.MasterCardDailyStatisticsBean
import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.bussiness.model.PreAuthDailyStatisticsBean
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView


/**
 * @author lizheng.zhao
 * @date 2022/11/15
 *
 * @你应该是一场梦我应该是一阵风
 */
class TabBillContract {


    interface View : BaseView {
        fun querySpayOrderNewSuccess(
            response: ArrayList<Order>?,
            page: Int,
            isLoadMore: Boolean
        )

        fun querySpayOrderNewFailed(error: Any?)

        fun loadCardPaymentDateSuccess(
            response: ArrayList<Order>?,
            page: Int,
            isLoadMore: Boolean
        )

        fun loadCardPaymentDateFailed(error: Any?)

        fun queryCardPaymentOrderDetailsSuccess(response: Order?)

        fun queryCardPaymentOrderDetailsFailed(error: Any?)

        fun queryRefundDetailSuccess(response: Order?)

        fun queryRefundDetailFailed(error: Any?)

        fun queryOrderDetailSuccess(response: Order?)

        fun queryOrderDetailFailed(error: Any?)

        fun queryDailyStaticsSuccess(response: DailyStatisticsBean?)

        fun queryDailyStaticsFailed(error: Any?)

        fun queryPreAuthDailyStaticsSuccess(response: PreAuthDailyStatisticsBean?)

        fun queryPreAuthDailyStaticsFailed(error: Any?)

        fun queryMasterCardDailyStaticsSuccess(response: MasterCardDailyStatisticsBean?)

        fun queryMasterCardDailyStaticsFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {

        fun queryMasterCardDailyStatics()


        fun queryPreAuthDailyStatics()


        fun queryDailyStatics()


        fun queryOrderDetail(
            orderNo: String?,
            mchId: String?,
            isMark: Boolean
        )


        fun queryRefundDetail(
            orderNo: String?,
            mchId: String?
        )


        fun querySpayOrderNew(
            reqFeqTime: Int,
            listStr: List<String>?,
            payTypeList: List<String>?,
            isRefund: Int,
            page: Int,
            order: String?,
            isUnfrozen: Int,
            userId: String?,
            startDate: String?,
            isLoadMore: Boolean
        )

        fun loadCardPaymentDate(
            apiProviderList: List<Integer>?,
            tradeTypeList: List<Integer>?,
            tradeStateList: List<Integer>?,
            startTime: String?,
            page: Int,
            isSearch: Boolean,
            orderNoMch: String?,
            isLoadMore: Boolean
        )


        fun queryCardPaymentOrderDetails(
            outTradeNo: String?,
            orderNoMch: String?
        )
    }


}