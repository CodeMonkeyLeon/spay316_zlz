package cn.swiftpass.enterprise.ui.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tencent.stat.StatService;

import net.tsz.afinal.FinalBitmap;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.enums.ClientEnum;
import cn.swiftpass.enterprise.bussiness.enums.OrderStatusEnum;
import cn.swiftpass.enterprise.bussiness.logica.bill.BillOrderManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.QRcodeInfo;
import cn.swiftpass.enterprise.bussiness.model.RefundModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.print.PrinterApos;
import cn.swiftpass.enterprise.ui.activity.bill.OrderRefundActivity;
import cn.swiftpass.enterprise.ui.activity.print.BluePrintUtil;
import cn.swiftpass.enterprise.ui.activity.print.BluetoothSettingActivity;
import cn.swiftpass.enterprise.ui.activity.scan.CaptureActivity;
import cn.swiftpass.enterprise.ui.activity.user.RefundRecordOrderDetailsActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.ProgressInfoDialog;
import cn.swiftpass.enterprise.ui.widget.RefundCheckDialog;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.ui.widget.WxCardDialog;
import cn.swiftpass.enterprise.utils.CreateOneDiCodeUtil;
import cn.swiftpass.enterprise.utils.DataReportUtils;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 订单详细 跟退款详细 User: Alan Date: 13-9-29 Time: 下午2:32
 */
@SuppressLint("NewApi")
public class OrderDetailsActivity extends TemplateActivity {

    private static final String TAG = OrderDetailsActivity.class.getSimpleName();

    private TextView tvMoney, tvAddTime, tvNotifyTime, tvState, tvRefundCode, tvTitleTime, wx_tvOrderCode, wx_title_info;
    //private TextView tvRefund, tvUser, tvBankType, tvTransactionId, tvBankTypeTitle;

    //tvOrderCode
    private LinearLayout llBanktype, llNotifyTime;

    private LinearLayout llRefund, lr_wx, logo_lay, ll_pay_top, id_lin_withholding;

    //private LinearLayout llTransactionId, ly_refund_money;

    private ImageView ivPayType, iv_pay_img;

    private Order orderModel;

    private RefundModel refundModel;

    //private Context context;

    private Button btnRefund, blue_print, btn_order_syn, btn_order_reverse, order_affirm, btn_order_again;

    //private ImageView signatureView; // 获取签名图片

    //private Bitmap bmp;

    String refundNum = null; // 退款单号

    private LinearLayout cashierLay, ly_refundMoney;

    //private LinearLayout mch_lay, la_state;

    private RelativeLayout pay_top_re;

    private TextView cashierText, pay_method, tv_transNo, tx_refund_money, tx_tx_tips;
//    body_info pay_mch

    private ImageView logo_title, iv_code;

    //private View top_line;

    private FinalBitmap finalBitmap;

    private TextView tv_code;

    //private TextView refundMoney;

    private static boolean isConnected;

    public static final int CONNECT_DEVICE = 1;

    public static final int ENABLE_BT = 2;

    private int currIndex = 0;// 0--bluetooth,

    private TextView spay_pay_client, tx_receivable,  tx_surcharge, id_total, id_parser_money;

    //private TextView tv_rfmoney;
//    pay_mchId

    private LinearLayout ly_code, ll_orderAffirm, money_lay, discount_lay, card_lay, id_lin_tips, id_lin_surcharge, id_order_line;

    private LinearLayout ll_uplan_list;

   //private LinearLayout ly_refund;

    ProgressInfoDialog dialog1;

    private TextView tx_discount, tx_vcard;

    //private TextView tx_receiv, tv_marketing;

    private ImageView line_01;

    //private String payType = "";

    private boolean isStop = true; // 强行中断交易

    private AlertDialog dialogInfo;

    private LinearLayout ly_attach, ly_reverse, ly_card_len, ly_affirm;

    private TextView tv_attach, tv_refund, tx_withholding;

    //private TextView tv_reverse, tx_rmb;

    private View id_line_cashier, id_line_ly_card_len, id_lr_wx,id_line_attach,id_line_refound;

    private boolean isCashierVisiable = false;

    private PrinterApos printerSample;
    private Bitmap bitmapLogo;

    //根据当前的小数点的位数来判断应该除以多少
    private double min = 1;

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, final Intent data) {
        switch (requestCode) {
            case CONNECT_DEVICE:
                if (resultCode == Activity.RESULT_OK) {
                    if (currIndex == 0) {
                        Logger.i("hehui", "onActivityResult isConnected-->" + isConnected);

                    }
                }
                break;

            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        finalBitmap = FinalBitmap.create(OrderDetailsActivity.this);//
        try {
            finalBitmap.configDiskCachePath(cn.swiftpass.enterprise.utils.FileUtils.getAppCache());
        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }

        //新增小票logo打印
        bitmapLogo = getImageFromAssetsFile("icon_print_logo.png",this);

        //根据当前的小数点的位数来判断应该除以多少
        for(int i = 0 ; i < MainApplication.numFixed ; i++ ){
            min = min * 10;
        }

        setupInitViews();
       // new FileUtils().createFile(OrderDetailsActivity.this);
        setLinster();
        ToastHelper.getInstance(OrderDetailsActivity.this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initUIWithData();
        this.bindDeviceService();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.unbindDeviceService();
    }


    boolean isCon = false;

    RefundCheckDialog dialogRever = null;

    private LinearLayout mCheckRefundLayout;

    private void queryAuthOrderGetStatus(){

        //同步预授权的订单状态
        BillOrderManager.getInstance().authPayQuery(orderModel.getOrderNoMch(),"",new UINotifyListener<Order>(){
            @Override
            public void onSucceed(final Order result) {
                super.onSucceed(result);
                dismissLoading();
                if (result != null) {
                    if (result.getTradeState()!=null&&result.getTradeState()==2) {
                        tvState.setVisibility(View.VISIBLE);
                        tvState.setText(R.string.tx_bill_stream_chioce_succ);
                        mCheckRefundLayout.setVisibility(View.VISIBLE);
                        blue_print.setVisibility(View.VISIBLE);
                        btn_order_syn.setVisibility(View.GONE);
                        tvState.setTextColor(getResources().getColor(R.color.bill_item_succ));
                        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.title_order_refund));
                        result.setMark(true);
                        //  orderModel = result;
                        refundModel = null;
                        Intent i = new Intent();
                        i.putExtra("isRefreshList", PreAuthThawActivity.REQUEST_REFRESH);
                        setResult(PreAuthThawActivity.REQUEST_REFRESH, i);//如果同步成功，返回的上级刷新列表
                    }else {
                        toastDialog(OrderDetailsActivity.this, R.string.this_order_is_unpaid, new NewDialogInfo.HandleBtn() {

                            @Override
                            public void handleOkBtn() {
                                finish();
                            }

                        });
                    }
                  //  initUIWithData();
                }
            }

            @Override
            public void onError(Object object) {
                super.onError(object);
                dismissLoading();
                if (object != null) {
                    toastDialog(OrderDetailsActivity.this, R.string.this_order_is_unpaid, new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            finish();
                        }

                    });
                }
            }

            @Override
            public void onPreExecute() {
                showLoading(false, R.string.show_order_loading);
                super.onPreExecute();
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

        });

    }



    private void queryOrderGetStuts() {
        OrderManager.getInstance().queryOrderByOrderNo(orderModel.getOutTradeNo(), MainApplication.PAY_ZFB_QUERY, new UINotifyListener<Order>() {
            @Override
            public void onError(Object object) {
                super.onError(object);
                dismissLoading();
                if (object != null) {
                    showToastInfo(object.toString());
                }
            }

            @Override
            public void onPreExecute() {
                showLoading(false, R.string.show_order_loading);
                super.onPreExecute();
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(Order result) {
                super.onSucceed(result);
                dismissLoading();
                if (result != null) {
                    if (result.state.equals("2")) {
                        result.setMark(true);
                        orderModel = result;
                        refundModel = null;
                        initUIWithData();
//                        HandlerManager.notifyMessage(HandlerManager.PAY_TYPE, HandlerManager.PAY_TYPE, "1");
//                        OrderDetailsActivity.startActivity(OrderDetailsActivity.this, result);
//                        finish();
                    } else if (result.state.equals("3")) {
                        toastDialog(OrderDetailsActivity.this, R.string.tv_order_close, new NewDialogInfo.HandleBtn() {

                            @Override
                            public void handleOkBtn() {
                                finish();
                            }

                        });
                    } else {
                        toastDialog(OrderDetailsActivity.this, R.string.show_order_status, new NewDialogInfo.HandleBtn() {

                            @Override
                            public void handleOkBtn() {
                                finish();
                            }

                        });
                        if ((orderModel.getTradeType().equals(MainApplication.PAY_QQ_MICROPAY) || orderModel.getTradeType().startsWith(MainApplication.PAY_WX_MICROPAY) || orderModel.getTradeType().equals(MainApplication.PAY_ZFB_MICROPAY)) && !orderModel.tradeState.equals(OrderStatusEnum.REVERSE.getValue()) && !orderModel.tradeState.equals(OrderStatusEnum.PAY_CLOSE.getValue())) {
                            showWindowRever();
                        }
                    }
                } else {
                    showToastInfo(R.string.show_order_syn);
                }
        }
        });
    }

    /*
     * 冲正弹窗口
     */
    private void showWindowRever() {
        dialogRever = new RefundCheckDialog(OrderDetailsActivity.this, false,RefundCheckDialog.REVERS, getString(R.string.public_cozy_prompt), getString(R.string.tx_dialog_money), orderModel.getOutTradeNo(), String.valueOf(orderModel.money), null, new RefundCheckDialog.ConfirmListener() {
            @Override
            public void ok(String code) {

                if (null == orderModel.outTradeNo || "".equals(orderModel.outTradeNo)) {
                    return;
                }
                DialogHelper.showDialog(getString(R.string.title_dialog), getString(R.string.tx_dialog_reverse), R.string.btnCancel, R.string.btnOk, OrderDetailsActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialog1 = new ProgressInfoDialog(OrderDetailsActivity.this, getString(R.string.dialog_order_loading), new ProgressInfoDialog.HandleBtn() {

                            @Override
                            public void handleOkBtn() {
                                showConfirm(getString(R.string.interrupt_trading), dialog1);
                            }

                        });
                        OrderManager.getInstance().unifiedPayReverse(orderModel.outTradeNo, new UINotifyListener<Order>() {
                            @Override
                            public void onError(final Object object) {
                                super.onError(object);
                                dialog1.dismiss();

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (object != null) {

                                            DialogInfo dialogInfo = new DialogInfo(OrderDetailsActivity.this, getString(R.string.public_cozy_prompt), "" + object.toString(), getString(R.string.btnOk), DialogInfo.FLAG, null, null);
                                            DialogHelper.resize(OrderDetailsActivity.this, dialogInfo);
                                            dialogInfo.show();
                                        }
                                    }
                                });
                            }

                            @Override
                            public void onPreExecute() {
                                super.onPreExecute();
                                DialogHelper.resize(OrderDetailsActivity.this, dialog1);
                                dialog1.show();
                                //                showLoading("订单冲正中，请稍候..");
                            }

                            @Override
                            public void onPostExecute() {
                                super.onPostExecute();
                            }

                            @Override
                            public void onSucceed(Order result) {
                                super.onSucceed(result);
                                //                dismissLoading();
                                if (result != null) {

                                    result.setMark(true);
                                    HandlerManager.notifyMessage(HandlerManager.PAY_TYPE, HandlerManager.PAY_TYPE, "1");
                                    showToastInfo(R.string.dialog_order_reverse_succ);
                                    finish();
                                }
                            }
                        });
                    }
                }).show();
                if (dialogRever != null) {
                    dialogRever.dismiss();
                }
                //                                }
                //                            }).show();

            }

            @Override
            public void cancel() {
                //                        queryOrderGetStuts(outTradeNo, "支付确认中，请稍候...", true, false);
                queryOrderGetStuts();
            }

        });
        dialogRever.show();

        dialogRever.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                if (keycode == KeyEvent.KEYCODE_BACK) {
                    // HandlerManager.notifyMessage(HandlerManager.STREAM_BILL, HandlerManager.PAY_DETAIL_TO_REFRESH);
                    return true;
                }
                return false;
            }
        });
    }

    /**
     * 蓝牙打印
     */
    void bluePrint() {
        switchLanguage(PreferenceUtil.getString("language", ""));
        orderModel.setPay(true);
        orderModel.setPartner(getString(R.string.tv_pay_user_stub));

        printerSample = new PrinterApos(OrderDetailsActivity.this,isCashierVisiable, bitmapLogo,orderModel) {
            @Override
            protected void onDeviceServiceCrash() {
                OrderDetailsActivity.this.bindDeviceService();
            }
            @Override
            protected void displayPrinterInfo(String info) {
                Log.i("zhouwei", "打印完成" + info);
            }
        };
        printerSample.startPrint();
        //打印第二联
        TimerTask task = new TimerTask() {
            public void run() {
                orderModel.setPay(true);
                orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
                printerSample = new PrinterApos(OrderDetailsActivity.this,isCashierVisiable, bitmapLogo,orderModel) {
                    @Override
                    protected void onDeviceServiceCrash() {
                        OrderDetailsActivity.this.bindDeviceService();
                    }
                    @Override
                    protected void displayPrinterInfo(String info) {
                        Log.i("zhouwei", "打印完成" + info);
                    }
                };
                printerSample.startPrint();
            }
        };
        Timer timer = new Timer();
        timer.schedule(task, 4000);

    }
    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void showDialog() {
        dialog = new DialogInfo(OrderDetailsActivity.this, null, getString(R.string.tx_blue_set), getString(R.string.title_setting), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                showPage(BluetoothSettingActivity.class);
                dialog.cancel();
                dialog.dismiss();
            }

            @Override
            public void handleCancleBtn() {
                dialog.cancel();
            }
        }, null);

        DialogHelper.resize(OrderDetailsActivity.this, dialog);
        dialog.show();
    }

    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void setLinster() {

        order_affirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                orderAffirm(orderModel.getOrderNoMch());
            }
        });

        card_lay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                WxCardDialog wxCardDialgo = new WxCardDialog(OrderDetailsActivity.this, orderModel.getWxCardList(), new WxCardDialog.ConfirmListener() {

                    @Override
                    public void cancel() {

                    }
                });
                DialogHelper.resize(OrderDetailsActivity.this, wxCardDialgo);
                wxCardDialgo.show();
            }
        });
        blue_print.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    StatService.trackCustomEvent(OrderDetailsActivity.this, "kMTASPayBillOrderPrint", "打印按钮");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                // 1.携带参数的打点
                Bundle values = new Bundle();
                values.putString("kGFASPayBillOrderPrint","打印按钮");
                DataReportUtils.getInstance().report("kGFASPayBillOrderPrint",values);

                bluePrint();
            }

        });

        /**
         * 订单同步功能
         */
        btn_order_syn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    StatService.trackCustomEvent(OrderDetailsActivity.this, "kMTASPayBillSyncOrder", "同步订单按钮");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                // 1.携带参数的打点
                Bundle value = new Bundle();
                value.putString("kGFASPayBillSyncOrder","同步订单按钮");
                DataReportUtils.getInstance().report("kGFASPayBillSyncOrder",value);

                if(orderModel.getTradeType().equals(MainApplication.PAY_ALIPAY_AUTH_MICROPAY)
                        || orderModel.getTradeType().equals(MainApplication.PAY_ALIPAY_AUTH_NATIVEPAY)){
                    queryAuthOrderGetStatus();//预授权的订单，则会调用预授权的同步订单接口
                }else{
                    queryOrderGetStuts();
                }

            }
        });

        /**
         * 再次发起收款
         */
        btn_order_again.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (orderModel.getTradeType().equals(MainApplication.PAY_ZFB_MICROPAY) || orderModel.getTradeType().startsWith(MainApplication.PAY_WX_MICROPAY) || orderModel.getTradeType().equals(MainApplication.PAY_QQ_MICROPAY) || orderModel.getTradeType().equals(MainApplication.PAY_QQ_PROXY_MICROPAY) || orderModel.getTradeType().equals(MainApplication.PAY_JINGDONG)) {
                    if (orderModel.getWxCardList() != null && orderModel.getWxCardList().size() > 0) {
                        Intent it = new Intent();
                        it.setClass(OrderDetailsActivity.this, CaptureActivity.class);
                        it.putExtra("money", orderModel.money + "");
                        it.putExtra("discountAmount", orderModel.getDaMoney());//优惠金额
                        it.putExtra("payType", MainApplication.PAY_TYPE_MICROPAY_VCARD);
                        it.putExtra("tradeType", orderModel.getTradeType());
                        it.putExtra("outTradeNo", orderModel.getOutTradeNo());
                        it.putExtra("vardOrders", (Serializable) orderModel.getWxCardList());
                        startActivity(it);
                        finish();
                    } else {
                        CaptureActivity.startActivitys(OrderDetailsActivity.this, orderModel.getOutTradeNo(), orderModel.money + "", orderModel.getTradeType(), 1);
                        OrderDetailsActivity.this.finish();
                    }
                } else if (orderModel.getTradeType().startsWith(MainApplication.PAY_WX_NATIVE) || orderModel.getTradeType().equals(MainApplication.PAY_QQ_NATIVE1) || orderModel.getTradeType().equals(MainApplication.PAY_QQ_NATIVE) || orderModel.getTradeType().startsWith(MainApplication.PAY_ZFB_NATIVE) || orderModel.getTradeType().equals(MainApplication.PAY_JINGDONG_NATIVE)) {
                    toNative(orderModel.money + "", orderModel.getTradeType(), orderModel.getOutTradeNo());
                } else {
                    showToastInfo(R.string.show_order_again);
                }
            }
        });

        // 订单冲正
        btn_order_reverse.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });

        mCheckRefundLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    StatService.trackCustomEvent(OrderDetailsActivity.this, "kMTASPayBillOrderRefundList", "退款记录入口");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                // 1.携带参数的打点
                Bundle values = new Bundle();
                values.putString("kGFASPayBillOrderRefundList","退款记录入口");
                DataReportUtils.getInstance().report("kGFASPayBillOrderRefundList",values);

                loadDate(1,false,1,null);
            }
        });

    }

    /**
     * 进入退款记录列表
     */
    private void go2orderListActivity(int size){
        Intent intent  = new Intent();
        intent.setClass(OrderDetailsActivity.this,RefundOrderListActivity.class);
        intent.putExtra("orderNo", orderModel.getOrderNoMch());
        intent.putExtra("order_size",size+"");
        startActivity(intent);
    }

    /**
     * 进入退款记录列表
     * @param order
     */
    private final static int QUERY_REFUND = 0x111;
    private void go2OrderDetailActivity(Order order){
        Intent intent  = new Intent();
        intent.setClass(OrderDetailsActivity.this,RefundRecordOrderDetailsActivity.class);
        intent.putExtra("order",order);
        intent.putExtra("Tag",QUERY_REFUND);
        startActivity(intent);
    }

    void loadDate(final int page, final boolean isLoadMore, int isRefund, String startDate) {

        BillOrderManager.getInstance().querySpayOrder(null,0, null, null, isRefund, page, orderModel.getOrderNoMch(), startDate, new UINotifyListener<List<Order>>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                dissDialog();
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                dissDialog();

            }

            @Override
            public void onSucceed(List<Order> model) {
                dissDialog();
                Logger.i("JAMY","list: "+model.size());
                int size = model.size();
                if (size == 1){
                    Order order = model.get(0);
                    go2OrderDetailActivity(order);
                }else{
                    go2orderListActivity(size);
                }
            }
        });
    }


    private void toNative(String strTotalMoney, final String service, String outTradeNo) {
        String msg = getString(R.string.spanning_pay_wechat_code);
        if (service.equals(MainApplication.PAY_QQ_NATIVE) || service.equals(MainApplication.PAY_QQ_NATIVE1)) {
            msg = getString(R.string.spanning_pay_qq_code);
        } else if (service.startsWith(MainApplication.PAY_ZFB_NATIVE)) {
            msg = getString(R.string.spanning_pay_zfb_code);
        } else if (service.equals(MainApplication.PAY_JINGDONG_NATIVE)) {
            msg = getString(R.string.spanning_pay_jd_code);
        }

        dialog1 = new ProgressInfoDialog(OrderDetailsActivity.this, msg, new ProgressInfoDialog.HandleBtn() {

            @Override
            public void handleOkBtn() {
                showConfirm(getStringById(R.string.interrupt_trading), dialog1);
            }
        });

        OrderManager.getInstance().unifiedNativePay(strTotalMoney, service, orderModel.getDaMoney(), orderModel.getWxCardList(), outTradeNo, new UINotifyListener<QRcodeInfo>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                DialogHelper.resize(OrderDetailsActivity.this, dialog1);
                if (!dialog1.isShowing()) {
                    dialog1.show();
                }
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                dialog1.dismiss();
                if (dialogInfo != null) {
                    dialogInfo.dismiss();
                }
                if (checkSession()) {
                    return;
                }
                if (!isStop) {
                    return;
                }
                if (object != null) {
                    OrderDetailsActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //                                DialogInfo dialogInfo =
                            //                                    new DialogInfo(OrderDetailsActivity.this,
                            //                                        getStringById(R.string.public_cozy_prompt), object.toString(),
                            //                                        getStringById(R.string.btnOk), DialogInfo.REGISTFLAG, null, null);
                            //                                DialogHelper.resize(OrderDetailsActivity.this, dialogInfo);
                            //                                dialogInfo.show();
                            showToastInfo(object.toString());
                        }
                    });
                }

            }

            @Override
            public void onSucceed(QRcodeInfo result) {
                super.onSucceed(result);
                dialog1.dismiss();
                if (dialogInfo != null) {
                    dialogInfo.dismiss();
                }
                if (result != null && isStop) {
                    result.setPayType(service);
                    result.setDiscountAmount(orderModel.getDaMoney()); //优惠金额
                    result.setIsMark(1);
                    ShowQRcodeActivity.startActivity(result, OrderDetailsActivity.this);
                    OrderDetailsActivity.this.finish();
                } else {
                    isStop = true;
                }
            }
        });
    }

    private void showConfirm(String msg, final ProgressInfoDialog dialogs) {
        AlertDialog.Builder builder = new AlertDialog.Builder(OrderDetailsActivity.this);
        builder.setTitle(R.string.public_cozy_prompt);
        builder.setMessage(msg);
        builder.setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialogs != null) {
                    dialogs.dismiss();
                    finish();
                }
            }
        });

        builder.setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialogInfo = builder.show();
    }


    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_order_detail);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                if (orderModel != null && orderModel.isMark()) {
                    HandlerManager.notifyMessage(HandlerManager.PAY_TYPE, HandlerManager.PAY_TYPE, "1");
                }
                // HandlerManager.notifyMessage(HandlerManager.STREAM_BILL, HandlerManager.PAY_DETAIL_TO_REFRESH);
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButLayClick() {
                try {
                    StatService.trackCustomEvent(OrderDetailsActivity.this, "kMTASPayBillOrderToRefund", "退款入口");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                // 1.携带参数的打点
                Bundle values = new Bundle();
                values.putString("kGFASPayBillOrderToRefund","退款入口");
                DataReportUtils.getInstance().report("kGFASPayBillOrderToRefund",values);


                if (orderModel.money - (orderModel.getRefundMoney() + orderModel.getRfMoneyIng()) == 0) {
                    toastDialog(OrderDetailsActivity.this, R.string.tv_order_reunding, null);
                    return;
                }
                MainApplication.listActivities.add(OrderDetailsActivity.this);
                OrderRefundActivity.startActivity(OrderDetailsActivity.this, orderModel);
            }
        });
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    private void setupInitViews() {
        //context = this;
        setContentView(R.layout.activity_order_details);
        //tx_rmb = getViewById(R.id.tx_rmb);
        ly_affirm = getViewById(R.id.ly_affirm);
        tv_refund = getViewById(R.id.tv_refund);
        ly_card_len = getViewById(R.id.ly_card_len);
        ly_reverse = getViewById(R.id.ly_reverse);
        ly_attach = getViewById(R.id.ly_attach);
        tv_attach = getViewById(R.id.tv_attach);
        //tv_marketing = getViewById(R.id.tv_marketing);
        btn_order_again = getViewById(R.id.btn_order_again);
        tx_vcard = getViewById(R.id.tx_vcard);
        card_lay = getViewById(R.id.card_lay);
        line_01 = getViewById(R.id.line_01);
        //tx_receiv = getViewById(R.id.tx_receiv);
        money_lay = getViewById(R.id.money_lay);
        id_line_ly_card_len = getViewById(R.id.id_line_ly_card_len);
        id_line_cashier = getViewById(R.id.id_line_cashier);
        discount_lay = getViewById(R.id.discount_lay);
        tx_receivable = getViewById(R.id.tx_receivable);
        id_lr_wx = getViewById(R.id.id_lr_wx);
        tx_discount = getViewById(R.id.tx_discount);
        id_order_line = getViewById(R.id.id_order_line);
        ll_orderAffirm = getViewById(R.id.ll_orderAffirm);
        //la_state = getViewById(R.id.la_state);
        order_affirm = getViewById(R.id.order_affirm);
        //tv_rfmoney = getViewById(R.id.tv_rfmoney);
        //ly_refund = getViewById(R.id.ly_refund);
        tx_refund_money = getViewById(R.id.tx_refund_money);
        ly_refundMoney = getViewById(R.id.ly_refundMoney);
        //refundMoney = getViewById(R.id.refundMoney);
        //ly_refund_money = getViewById(R.id.ly_refund_money);
        btn_order_reverse = getViewById(R.id.btn_order_reverse);
        ly_code = getViewById(R.id.ly_code);
        btn_order_syn = getViewById(R.id.btn_order_syn);
        spay_pay_client = getViewById(R.id.spay_pay_client);
        iv_code = getViewById(R.id.iv_code);
        tv_code = getViewById(R.id.tv_code);
        blue_print = getViewById(R.id.blue_print);
        id_lin_tips = getViewById(R.id.id_lin_tips);
        //mch_lay = getViewById(R.id.mch_lay);
        tv_transNo = getViewById(R.id.tv_transNo);
        logo_lay = getViewById(R.id.logo_lay);
        ll_pay_top = getViewById(R.id.ll_pay_top);
        logo_title = getViewById(R.id.logo_title);
        id_lin_surcharge = getViewById(R.id.id_lin_surcharge);
        id_lin_withholding = getViewById(R.id.id_lin_withholding);
        pay_method = getViewById(R.id.pay_method);
        llBanktype = getViewById(R.id.ll_banktype);
        //tvBankTypeTitle = getViewById(R.id.tv_banktypeTitle);
        tvMoney = getViewById(R.id.tv_money);
        tvAddTime = getViewById(R.id.tv_addtime);
        ivPayType = getViewById(R.id.iv_payType);

        tvState = getViewById(R.id.tv_state);
        btnRefund = getViewById(R.id.btn_refund);
        iv_pay_img = getViewById(R.id.iv_pay_img);

        llRefund = getViewById(R.id.ll_refund_code);
        tvRefundCode = getViewById(R.id.tv_refund_code);

        wx_tvOrderCode = getViewById(R.id.wx_tvOrderCode);
        pay_top_re = getViewById(R.id.pay_top_re);

        //tvBankType = getViewById(R.id.tv_banktype);

        orderModel = (Order) getIntent().getSerializableExtra("order");
        refundModel = (RefundModel) getIntent().getSerializableExtra("refund");

        //llTransactionId = getViewById(R.id.ll_transactionId);
        //tvTransactionId = getViewById(R.id.tv_transactionId);

        llNotifyTime = getViewById(R.id.ll_notifytime);
        tvNotifyTime = getViewById(R.id.tv_notifyTime);

        wx_title_info = getViewById(R.id.wx_title_info);
        tx_surcharge = getViewById(R.id.tx_surcharge);
        tx_withholding = getViewById(R.id.tx_withholding);
        lr_wx = (LinearLayout) getViewById(R.id.lr_wx);

        tvTitleTime = getViewById(R.id.tv_titletime);
        //signatureView = getViewById(R.id.signature_pic);
        tx_tx_tips = getViewById(R.id.tx_tx_tips);
        id_total = getViewById(R.id.id_total);
        cashierText = getViewById(R.id.cashierText);
        cashierLay = getViewById(R.id.cashierLay);
        id_parser_money = getViewById(R.id.id_parser_money);

        id_line_attach = getViewById(R.id.line_attach);
        id_line_refound = getViewById(R.id.line_refound);

        mCheckRefundLayout = getViewById(R.id.check_refund_layout);

        ll_uplan_list = getViewById(R.id.ll_uplan_list);

        initUIWithData();
    }


    private void initUIWithData() {
        lr_wx.setVisibility(View.VISIBLE);
        id_lr_wx.setVisibility(View.VISIBLE);
        if (orderModel != null) {
            id_total.setText(MainApplication.feeFh + DateUtil.formatMoneyUtils(orderModel.getOrderFee()));

            if (!MainApplication.isTipOpen()) {
                id_lin_tips.setVisibility(View.GONE);
            } else {
                tx_tx_tips.setText(MainApplication.feeFh + DateUtil.formatMoneyUtils(orderModel.getTipFee()));
            }

            if (!MainApplication.isSurchargeOpen()) {
                id_lin_surcharge.setVisibility(View.GONE);
                id_order_line.setVisibility(View.GONE);
            } else {
                if (orderModel != null) {
                    if(!TextUtils.isEmpty(orderModel.getTradeType())&&orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") || orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")){
                        id_lin_surcharge.setVisibility(View.GONE);
                        id_order_line.setVisibility(View.GONE);
                    }else{
                        tx_surcharge.setText(MainApplication.feeFh + DateUtil.formatMoneyUtils(orderModel.getSurcharge()));
                    }
                }
            }

           /* if (!MainApplication.isTaxRateOpen()) {
                id_lin_withholding.setVisibility(View.GONE);
            } else {
                tx_withholding.setText(MainApplication.feeFh + DateUtil.formatMoneyUtils(orderModel.getWithholdingTax()));
            }*/
            id_lin_withholding.setVisibility(View.GONE);

            //人民币转换显示
            if (orderModel.getCashFeel() <= 0) {
                id_parser_money.setVisibility(View.GONE);
            } else {
                id_parser_money.setVisibility(View.VISIBLE);

                //因为当前是人民币的金额，人民币的话，币种最小单位为100
                BigDecimal paseBigDecimal = new BigDecimal(orderModel.getCashFeel() / MainApplication.RMBFIX );
                id_parser_money.setText(getString(R.string.tx_mark) + DateUtil.formatPaseRMBMoney(paseBigDecimal));
            }

            //Uplan ---V3.0.5迭代新增
            if(orderModel != null){
                //如果为预授权的支付类型，则不展示任何优惠相关的信息
                /*因目前SPAY與網關支持的預授權接口僅為‘支付寶’預授權，
                遂與銀聯Uplan不衝突，遂預授權頁面內，無需處理該優惠信息。*/
                if(!TextUtils.isEmpty(orderModel.getTradeType())&&orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") || orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")){
                    ll_uplan_list.removeAllViews();
                }else{
                    ll_uplan_list.removeAllViews();

                    // 优惠详情 --- 如果列表有数据，则展示，有多少条，展示多少条
                    if(orderModel.getUplanDetailsBeans() != null && orderModel.getUplanDetailsBeans().size() > 0){
                        for(int i =0 ; i<orderModel.getUplanDetailsBeans().size() ; i++){
                            LayoutInflater inflater = LayoutInflater.from(this);
                            View view = inflater.inflate(R.layout.item_uplan_discount, null);// 得到加载view item_uplan_discount

                            TextView tv_title = (TextView) view.findViewById(R.id.tv_uplan_discount_title);
                            TextView tv_content = (TextView) view.findViewById(R.id.tv_uplan_discount_content);

                            //如果是Uplan Discount或者Instant Discount，则采用本地词条，带不同语言的翻译

                            if( orderModel.getUplanDetailsBeans().get(i).getDiscountNote().equalsIgnoreCase("Uplan discount")){
                                tv_title.setText(getStringById(R.string.uplan_Uplan_discount));
                            }else if(orderModel.getUplanDetailsBeans().get(i).getDiscountNote().equalsIgnoreCase("Instant Discount")){
                                tv_title.setText(getStringById(R.string.uplan_Uplan_instant_discount));
                            } else{//否则，后台传什么展示什么
                                tv_title.setText(orderModel.getUplanDetailsBeans().get(i).getDiscountNote());
                            }
                            tv_content.setText(MainApplication.feeFh +" -"+  orderModel.getUplanDetailsBeans().get(i).getDiscountAmt());

                            ll_uplan_list.addView(view);
                        }
                    }

                    // 消费者实付金额 --- 如果不为0 ，就展示
                    if(orderModel.getCostFee() != 0){
                        LayoutInflater inflater = LayoutInflater.from(this);
                        View view = inflater.inflate(R.layout.item_uplan_discount, null);// 得到加载view item_uplan_discount

                        TextView tv_title = (TextView) view.findViewById(R.id.tv_uplan_discount_title);
                        TextView tv_content = (TextView) view.findViewById(R.id.tv_uplan_discount_content);

                        tv_title.setText(getStringById(R.string.uplan_Uplan_actual_paid_amount));
                        tv_content.setText(MainApplication.feeFh +DateUtil.formatMoneyUtils(orderModel.getCostFee()));

                        ll_uplan_list.addView(view);
                    }
                }
            }

            //付款备注
            if (!StringUtil.isEmptyOrNull(orderModel.getAttach())) {
                ly_attach.setVisibility(View.VISIBLE);
                id_line_attach.setVisibility(View.VISIBLE);
                tv_attach.setText(orderModel.getAttach());
            }

            if (null != orderModel.getUserName() && !"".equals(orderModel.getUserName()) && !"null".equals(orderModel.getUserName()) && !StringUtil.isEmptyOrNull(orderModel.getUseId())) {
                cashierText.setText(orderModel.getUserName());
                isCashierVisiable = true;
            } else {
                isCashierVisiable = false;
                cashierLay.setVisibility(View.GONE);
                id_line_cashier.setVisibility(View.GONE);
            }

//            pay_mchId.setText(MainApplication.getMchId());
            if (!isAbsoluteNullStr(orderModel.getClient())) {
                spay_pay_client.setText(ClientEnum.getClienName(orderModel.getClient()));
            } else {
                spay_pay_client.setText(ClientEnum.UNKNOW.clientType);
            }

            // 生成一维码
            if (!isAbsoluteNullStr(orderModel.getOrderNoMch())) {
                ly_code.setVisibility(View.VISIBLE);
                WindowManager wm = this.getWindowManager();
                int width = wm.getDefaultDisplay().getWidth();
                final int w = (int) (width * 0.85);
                try {

                    OrderDetailsActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            iv_code.setImageBitmap(CreateOneDiCodeUtil.createCode(orderModel.getOrderNoMch(), w, 180));
                        }
                    });
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
                tv_code.setText(orderModel.getOrderNoMch());
            }

            if (!MainApplication.mchLogo.equals("") && !MainApplication.mchLogo.equals("null") && MainApplication.mchLogo != null) {
                logo_lay.setVisibility(View.VISIBLE);
                ll_pay_top.setVisibility(View.GONE);
                finalBitmap.display(logo_title, MainApplication.mchLogo);
            }

            tv_transNo.setText(orderModel.getOrderNoMch());
            pay_method.setText(orderModel.tradeName);
            llRefund.setVisibility(View.GONE);
            //根据当前的小数点的位数来判断应该除以多少
            double temp = 1;
            for(int i = 0 ; i < MainApplication.numFixed ; i++ ){
                temp = temp * 10;
            }
            double money = orderModel.money / temp;
            wx_tvOrderCode.setText(orderModel.transactionId);
            tvAddTime.setText(orderModel.getTradeTimeNew());
            if (orderModel.getTradeTimeNew() == null || TextUtils.isEmpty(orderModel.getTradeTimeNew())) {
                tvAddTime.setText(orderModel.getNotifyTime());
            }
            orderModel.setAddTimeNew(orderModel.getTradeTimeNew());
            if (orderModel.getDaMoney() > 0) { //优惠金额大于零
                discount_lay.setVisibility(View.VISIBLE);
                money_lay.setVisibility(View.GONE);

                double discount = (double) orderModel.getDaMoney();

                tx_discount.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(discount));
                tx_receivable.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtil(money));
                // 实际金额 = 应收金额+优惠金额
                //                long discountM = orderModel.getDaMoney() + orderModel.money;
                //                tvMoney.setText(MainApplication.feeFh + DateUtil.formatMoneyUtils((double)discountM));
                card_lay.setVisibility(View.GONE);
                //                tv_marketing.setText(getString(R.string.tx_markteing));
                if (orderModel.getWxCardList() != null && orderModel.getWxCardList().size() > 0) {
                    ly_card_len.setVisibility(View.VISIBLE);
                    tx_vcard.setText(getString(R.string.tx_wxcard_use) + orderModel.getWxCardList().size() + getString(R.string.tx_wxcard_use_size));
                } else {
                    ly_card_len.setVisibility(View.GONE);
                    id_line_ly_card_len.setVisibility(View.GONE);
                    card_lay.setVisibility(View.GONE);
                }
            } else {
                ly_card_len.setVisibility(View.GONE);
                id_line_ly_card_len.setVisibility(View.GONE);
                discount_lay.setVisibility(View.GONE);
                money_lay.setVisibility(View.GONE);
                line_01.setVisibility(View.VISIBLE);
                card_lay.setVisibility(View.GONE);
                tx_receivable.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtil(money));
            }

            if (orderModel.getRefundMoney() > 0) {
                //根据当前的小数点的位数来判断应该除以多少
                double min = 1;
                for(int i = 0 ; i < MainApplication.numFixed ; i++ ){
                    min = min * 10;
                }
                double m = (double) orderModel.getRefundMoney()/min;
                id_line_refound.setVisibility(View.VISIBLE);
                ly_refundMoney.setVisibility(View.VISIBLE);
                tv_refund.setText(R.string.tx_bill_stream_refund_money);
                tx_refund_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtil(m));
//                tx_refund_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtil(money));
                //                tx_receivable.setText(MainApplication.feeFh + DateUtil.formatMoneyUtil(money));
//                tx_receiv.setText(R.string.tx_bill_stream_refund_money);
//                tx_receivable.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(m));
            }
            if (orderModel.tradeState.equals(OrderStatusEnum.PAY_FAILL.getValue())) // 判断状态
            {
                mCheckRefundLayout.setVisibility(View.GONE);
                btnRefund.setVisibility(View.GONE);
                lr_wx.setVisibility(View.GONE);
                id_lr_wx.setVisibility(View.GONE);
                //                mch_lay.setVisibility(View.GONE);
                tvState.setText(R.string.stream_receivable);
            } else if (orderModel.tradeState.equals(OrderStatusEnum.PAY_SUCCESS.getValue()) || orderModel.tradeState.equals(OrderStatusEnum.RETURN_PRODUCT.getValue())) {
                mCheckRefundLayout.setVisibility(View.VISIBLE);
                if (orderModel.getTradeType().equalsIgnoreCase(MainApplication.PAY_QQ_NATIVE) || orderModel.getTradeType().equalsIgnoreCase(MainApplication.PAY_QQ_WAP) || orderModel.getTradeType().contains(MainApplication.PAY_QQ_TAG)) {
                    wx_title_info.setText(R.string.tv_pay_qq_order);
                    iv_pay_img.setImageResource(R.drawable.icon_qq_color);
                    pay_method.setText(R.string.qq_scan_pay);
                    if (orderModel.getTradeType().equalsIgnoreCase(MainApplication.PAY_QQ_WAP)) {
                        if (orderModel.tradeState.equals(OrderStatusEnum.PAY_SUCCESS.getValue()) || (orderModel.money - (orderModel.getRefundMoney() + orderModel.getRfMoneyIng()) > 0)) {

                            if ((orderModel.money - (orderModel.getRefundMoney() + orderModel.getRfMoneyIng()) > 0)) {
                                if (orderModel.getAffirm() == 0) {
                                    order_affirm.setVisibility(View.VISIBLE);
                                    ll_orderAffirm.setVisibility(View.GONE);
                                    order_affirm.setOnClickListener(new View.OnClickListener() {

                                        @Override
                                        public void onClick(View v) {
                                            orderAffirm(orderModel.getOrderNoMch());
                                        }
                                    });
                                } else {
                                    ll_orderAffirm.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                        if (orderModel.getAffirm() == 1) {
                            ll_orderAffirm.setVisibility(View.VISIBLE);
                        }
                    }
                } else if (orderModel.getTradeType().startsWith(MainApplication.PAY_ZFB_NATIVE)) {
                    iv_pay_img.setImageResource(R.drawable.icon_alipay_color);
                    wx_title_info.setText(R.string.tv_pay_zfb_order);
                    pay_method.setText(R.string.zfb_scan_code_pay);
                } else if (orderModel.getTradeType().equals(MainApplication.PAY_ZFB_MICROPAY) || orderModel.getTradeType().startsWith(MainApplication.PAY_ALIPAY_WAP) || orderModel.getTradeType().contains(MainApplication.PAY_ALIPAY_TAG)) {
                    iv_pay_img.setImageResource(R.drawable.icon_alipay_color);
                    wx_title_info.setText(R.string.tv_pay_zfb_order);
                    pay_method.setText(R.string.zfb_code_pay);

                    if (orderModel.getTradeType().equals(MainApplication.PAY_ZFB_WAP) || orderModel.getTradeType().equals(MainApplication.PAY_ALIPAY_WAP)) {
                        if (orderModel.tradeState.equals(OrderStatusEnum.PAY_SUCCESS.getValue()) || (orderModel.money - (orderModel.getRefundMoney() + orderModel.getRfMoneyIng()) > 0)) {

                            if ((orderModel.money - (orderModel.getRefundMoney() + orderModel.getRfMoneyIng()) > 0)) {
                                if (orderModel.getAffirm() == 0) {
                                    order_affirm.setVisibility(View.VISIBLE);
                                    ll_orderAffirm.setVisibility(View.GONE);
                                    order_affirm.setOnClickListener(new View.OnClickListener() {

                                        @Override
                                        public void onClick(View v) {
                                            orderAffirm(orderModel.getOrderNoMch());
                                        }
                                    });
                                } else {
                                    ll_orderAffirm.setVisibility(View.VISIBLE);
                                }
                            }
                        }

                        if (orderModel.getAffirm() == 1) {
                            ll_orderAffirm.setVisibility(View.VISIBLE);
                        }
                    }
                } else if (orderModel.getTradeType().startsWith(MainApplication.PAY_WX_MICROPAY)) {
                    // ivPayType.setImageResource(R.drawable.picture_pay_barcode);
                    iv_pay_img.setImageResource(R.drawable.icon_cancel_wechat);
                    pay_method.setText(R.string.wechat_car_pay);
                } else if (orderModel.getTradeType().equals(MainApplication.PAY_QQ_MICROPAY) || orderModel.getTradeType().equals(MainApplication.PAY_QQ_PROXY_MICROPAY) || orderModel.getTradeType().equalsIgnoreCase(MainApplication.PAY_QQ_NATIVE1)) {
                    wx_title_info.setText(R.string.tv_pay_qq_order);
                    pay_method.setText(R.string.qq_code_pay);
                    iv_pay_img.setImageResource(R.drawable.icon_qq_color);
                    pay_method.setText(R.string.qq_code_pay);
                } else if (orderModel.getTradeType().equalsIgnoreCase(MainApplication.PAY_JINGDONG) || orderModel.getTradeType().equalsIgnoreCase(MainApplication.PAY_JINGDONG_NATIVE) || orderModel.getTradeType().contains(MainApplication.PAY_JD_TAG)) {
                    wx_title_info.setText(R.string.tx_pay_jd_order);
                    iv_pay_img.setImageResource(R.drawable.icon_list_jd);
                } else if (orderModel.getTradeType().equals(MainApplication.PAY_WX_SJPAY) || orderModel.getTradeType().equals(MainApplication.PAY_ZFB_WAP) || orderModel.getTradeType().equals(MainApplication.PAY_ALIPAY_WAP)) {
                    iv_pay_img.setImageResource(R.drawable.icon_cancel_wechat);
                    if (orderModel.tradeState.equals(OrderStatusEnum.PAY_SUCCESS.getValue()) || (orderModel.money - (orderModel.getRefundMoney() + orderModel.getRfMoneyIng()) > 0)) {

                        if ((orderModel.money - (orderModel.getRefundMoney() + orderModel.getRfMoneyIng()) > 0)) {
                            if (orderModel.getAffirm() == 0) {
                                order_affirm.setVisibility(View.VISIBLE);
                                ll_orderAffirm.setVisibility(View.GONE);

                            } else {
                                ll_orderAffirm.setVisibility(View.VISIBLE);
                            }
                        }
                    }

                    if (orderModel.getAffirm() == 1) {
                        ll_orderAffirm.setVisibility(View.VISIBLE);
                    }
                } else {
                    pay_method.setText(R.string.wechat_scan_code_pay);
                    iv_pay_img.setImageResource(R.drawable.icon_cancel_wechat);
                    //                    orderModel.setTradeName("微信支付");
                }

                if (!TextUtils.isEmpty(orderModel.getTradeName())) {
                    pay_method.setText(orderModel.getTradeName());
                } else {
                    pay_method.setText(Order.TradeTypetoStr.getTradeNameForType(orderModel.getTradeType()));
                }
            } else {
                mCheckRefundLayout.setVisibility(View.GONE);
                tvState.setText(OrderStatusEnum.getDisplayNameByVaue(3));
                btnRefund.setVisibility(View.GONE);
                lr_wx.setVisibility(View.GONE);
                id_lr_wx.setVisibility(View.GONE);
                tvState.setText(R.string.stream_receivable);
                ivPayType.setVisibility(View.GONE);
                llBanktype.setVisibility(View.GONE);
                // 隐藏 支付成功 的标题
                pay_top_re.setVisibility(View.GONE);
            }

            if (orderModel.tradeState.equals(OrderStatusEnum.NO_PAY.getValue()) || orderModel.tradeState.equals(OrderStatusEnum.REVERSE.getValue()) || orderModel.tradeState.equals(OrderStatusEnum.PAY_CLOSE.getValue())) {
                llNotifyTime.setVisibility(View.GONE);
                tvState.setText(OrderStatusEnum.getDisplayNameByVaue(3));
                btnRefund.setVisibility(View.GONE);
                lr_wx.setVisibility(View.GONE);
                id_lr_wx.setVisibility(View.GONE);
                //                mch_lay.setVisibility(View.GONE);
                //                la_state.setVisibility(View.GONE);
                if (orderModel.tradeState.equals(OrderStatusEnum.NO_PAY.getValue())) {
                    //                    tvState.setText("已冲正");
                    btn_order_syn.setVisibility(View.VISIBLE);
                    if (!orderModel.getTradeType().equals(MainApplication.PAY_WX_SJPAY) && orderModel.getIsAgainPay() == 1) {
                    }
                }
                tvState.setText(OrderStatusEnum.getName(orderModel.tradeState));

                ivPayType.setVisibility(View.GONE);
                llBanktype.setVisibility(View.GONE);
                // 隐藏 支付成功 的标题
                pay_top_re.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(orderModel.getTradeName())) {
                    pay_method.setText(orderModel.getTradeName());
                } else {
                    pay_method.setText(Order.TradeTypetoStr.getTradeNameForType(orderModel.getTradeType()));
                }
            }

            switch (orderModel.getTradeState()) {
                case 2:
                    //支付成功
                    tvState.setTextColor(getResources().getColor(R.color.bill_item_succ));
                    blue_print.setVisibility(View.VISIBLE);
                    btn_order_syn.setVisibility(View.GONE);
                    if (orderModel.getRufundMark() != null) {
                        if (orderModel.getRufundMark() == 2) {
                            titleBar.setRightButLayVisibleForTotal(true, getString(R.string.title_order_refund));
                        } else if (orderModel.getRufundMark() == 1) {
                            //                            btnRefund.setVisibility(View.VISIBLE);
                            if (orderModel.money - (orderModel.getRefundMoney() + orderModel.getRfMoneyIng()) <= 0) {
                                titleBar.setRightButLayVisibleForTotal(false, getString(R.string.title_order_refund));
                            } else {
                                if (MainApplication.isAdmin.equals("1") || (MainApplication.isAdmin.equals("0") && MainApplication.isRefundAuth.equals("1"))) {
                                    titleBar.setRightButLayVisibleForTotal(true, getString(R.string.title_order_refund));
                                }
                            }
                        }
                    }

                    if (orderModel.getCanAffim() != null) {

                        if (orderModel.getCanAffim() == 1) {
                            if (orderModel.getAffirm() == 1) {
                                ly_affirm.setVisibility(View.GONE);
                                order_affirm.setVisibility(View.GONE);
                                ll_orderAffirm.setVisibility(View.GONE);

                            } else {
                                order_affirm.setVisibility(View.GONE);
                                ly_affirm.setVisibility(View.GONE);
                                ll_orderAffirm.setVisibility(View.GONE);
                            }
                        } else {
                            order_affirm.setVisibility(View.GONE);
                            ll_orderAffirm.setVisibility(View.GONE);
                            ly_affirm.setVisibility(View.GONE);
                        }

                        if (orderModel.getAffirm() == 1) {
                            ly_affirm.setVisibility(View.GONE);
                            order_affirm.setVisibility(View.GONE);
                            ll_orderAffirm.setVisibility(View.GONE);
                        }
                    }


                    break;
                case 1:
                    //未支付
                    blue_print.setVisibility(View.GONE);
                    btn_order_syn.setVisibility(View.VISIBLE);
                    tvState.setTextColor(getResources().getColor(R.color.pay_other_status));
                    break;
                case 3:
                    //关闭
                    blue_print.setVisibility(View.GONE);
                    tvState.setTextColor(getResources().getColor(R.color.pay_other_status));
                    break;
                case 4:
                    //已退款  转入退款
                    //如果是转入退款则不显示打印小票的按钮
                    blue_print.setVisibility(View.GONE);
                    tvState.setTextColor(getResources().getColor(R.color.pay_fail));
                    if (orderModel.getRufundMark() == 2) {
                        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.title_order_refund));
                    } else if (orderModel.getRufundMark() == 1) {
                        if (orderModel.money - orderModel.getRefundMoney() <= 0) {
                            titleBar.setRightButLayVisibleForTotal(false, getString(R.string.title_order_refund));
                        } else {
                            if (MainApplication.isAdmin.equals("1") || (MainApplication.isAdmin.equals("0") && MainApplication.isRefundAuth.equals("1"))) {
                                titleBar.setRightButLayVisibleForTotal(true, getString(R.string.title_order_refund));
                            }
                        }
                    }
                    break;
                case 8:
                    //未支付  已撤销
                    blue_print.setVisibility(View.GONE);
                    ly_reverse.setVisibility(View.VISIBLE);
                    tvState.setTextColor(getResources().getColor(R.color.pay_other_status));
                    break;
                default:
                    blue_print.setVisibility(View.GONE);
                    break;
            }

            if(orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") ||
                    orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")){
//                titleBar.setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
              //  tvState.setText(MainApplication.getPreAuthtradeStateMap().get(orderModel.getTradeState()+""));//收款成功 解冻失败
                if (orderModel.getTradeState()==1){
                    tvState.setText(R.string.stream_receivable);//未支付

                }else   if (orderModel.getTradeState()==2){
                    tvState.setText(R.string.tx_bill_stream_chioce_succ);//收款成功
                }else   if (orderModel.getTradeState()==4){
                    tvState.setText(R.string.transfer_refund);//转入退款
                }
            }else{
                if(MainApplication.getTradeTypeMap() != null && !TextUtils.isEmpty(orderModel.getTradeState()+"")){
                    tvState.setText(MainApplication.getTradeTypeMap().get(orderModel.getTradeState()+""));
                }else{
                    tvState.setText("");
                }
            }


            if (!StringUtil.isEmptyOrNull(MainApplication.getPayTypeMap().get(orderModel.getApiCode()))) {
                String language =   PreferenceUtil.getString("language", "");;
                //zh-rHK
                Locale locale = MainApplication.getContext().getResources().getConfiguration().locale;
                String lan = locale.getCountry();
                if (!TextUtils.isEmpty(language)) {
                    if (language.equals(MainApplication.LANG_CODE_EN_US)) {

                        wx_title_info.setText(MainApplication.getPayTypeMap().get(orderModel.getApiCode()) + " " + getString(R.string.tx_orderno));
                    } else {
                        wx_title_info.setText(MainApplication.getPayTypeMap().get(orderModel.getApiCode()) + getString(R.string.tx_orderno));
                    }
                } else {
                    if (lan.equalsIgnoreCase("en")) {
                        wx_title_info.setText(MainApplication.getPayTypeMap().get(orderModel.getApiCode()) + " " + getString(R.string.tx_orderno));
                    } else {
                        wx_title_info.setText(MainApplication.getPayTypeMap().get(orderModel.getApiCode()) + getString(R.string.tx_orderno));
                    }

                }

            }
        } else if (refundModel != null) {
            tvRefundCode.setText(refundModel.refundNo);
            //根据当前的小数点的位数来判断应该除以多少
            double temps = 1;
            for(int i = 0 ; i < MainApplication.numFixed ; i++ ){
                temps = temps * 10;
            }
            double money = refundModel.totalFee / temps;
//            tvOrderCode.setText(refundModel.orderNo);
            if (refundModel.orderTime != null) {
                tvAddTime.setText(DateUtil.formatTime(refundModel.orderTime.getTime()));
            }
            tvMoney.setText(money + getStringById(R.string.pay_yuan));
            tvTitleTime.setText(R.string.apply_time);
            if (refundModel.addTime.getTime() != 0) {
                tvNotifyTime.setText(DateUtil.formatTime(refundModel.addTime.getTime()));
            }
            // tvUser.setText(refundModel.userName);
            tvState.setText(OrderStatusEnum.getDisplayNameByVaue(refundModel.orderState));
            btnRefund.setText(R.string.audit_through);
        }

        if (!StringUtil.isEmptyOrNull(MainApplication.getPayTypeMap().get(orderModel.getApiCode()))) {
            String language  = PreferenceUtil.getString("language", "");;
            //zh-rHK
            Locale locale = MainApplication.getContext().getResources().getConfiguration().locale;
            String lan = locale.getCountry();
            if (!TextUtils.isEmpty(language)) {
                if (language.equals(MainApplication.LANG_CODE_EN_US)) {

                    wx_title_info.setText(MainApplication.getPayTypeMap().get(orderModel.getApiCode()) + " " + getString(R.string.tx_orderno));
                } else {
                    wx_title_info.setText(MainApplication.getPayTypeMap().get(orderModel.getApiCode()) + getString(R.string.tx_orderno));
                }
            } else {
                if (lan.equalsIgnoreCase("en")) {
                    wx_title_info.setText(MainApplication.getPayTypeMap().get(orderModel.getApiCode()) + " " + getString(R.string.tx_orderno));
                } else {
                    wx_title_info.setText(MainApplication.getPayTypeMap().get(orderModel.getApiCode()) + getString(R.string.tx_orderno));
                }

            }
        }
    }

    public static void startActivity(Context context, Order orderModel) {
        Intent it = new Intent();
        it.setClass(context, OrderDetailsActivity.class);
        it.putExtra("order", orderModel);
        context.startActivity(it);
    }

    public static void startActivity(Context context, RefundModel refundModel) {
        Intent it = new Intent();
        it.setClass(context, OrderDetailsActivity.class);
        it.putExtra("refund", refundModel);
        context.startActivity(it);
    }

    private void orderAffirm(String outTradeNo) {
        OrderManager.getInstance().orderAffirm(outTradeNo, new UINotifyListener<Order>() {
            @Override
            public void onError(final Object object) {
                super.onError(object);
                dismissLoading();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (object != null) {
                            toastDialog(OrderDetailsActivity.this, object.toString(), null);
                        }
                    }
                });
            }

            @Override
            public void onPreExecute() {
                super.onPreExecute();
                showLoading(false, R.string.show_affirm_loading);
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
                dismissLoading();
            }

            @Override
            public void onSucceed(Order result) {
                super.onSucceed(result);
                dismissLoading();
                if (null != result) {
                    //                    showToastInfo(R.string.show_affirm_succ);
                    toastDialog(OrderDetailsActivity.this, R.string.show_affirm_succ, new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            ll_orderAffirm.setVisibility(View.VISIBLE);
                        }
                    });
                    ly_affirm.setVisibility(View.GONE);
                }
            }
        });
    }

    /**
     * 暂时不支持退款
     */
    //
    public void onRefund(View v) {
        /*
         * if (ApiConstant.ISOVERSEASY) { showToastInfo("暂不支持境外退款，谢谢合作！"); }
         * else {
         */

        MainApplication.listActivities.add(OrderDetailsActivity.this);
        OrderRefundActivity.startActivity(OrderDetailsActivity.this, orderModel);


//        if (orderModel != null) {
//
//        }

    }


    public void showBigOneCode(View view) {
        if (orderModel != null) {
//            OneDiCodeActivity.startActivity(OrderDetailsActivity.this, orderModel.getOrderNoMch());
            RotateCanvasViewActivity.startActivity(OrderDetailsActivity.this, orderModel.getOrderNoMch());
        }
    }

    /**
     * 从Asset中读取文件
     * @param fileName
     * @param context
     * @return
     */
    public Bitmap getImageFromAssetsFile(String fileName,Context context)
    {
        Bitmap image = null;
        AssetManager am = context.getResources().getAssets();
        InputStream is = null;
        try
        {
            is = am.open(fileName);
            image = BitmapFactory.decodeStream(is);

        }
        catch (IOException e)
        {
            Logger.e(TAG,Log.getStackTraceString(e));
        }finally {
            if (null != is){
                try {
                    is.close();
                } catch (IOException e) {
                    Logger.e(TAG,Log.getStackTraceString(e));
                }
            }
        }
        return image;

    }
}
