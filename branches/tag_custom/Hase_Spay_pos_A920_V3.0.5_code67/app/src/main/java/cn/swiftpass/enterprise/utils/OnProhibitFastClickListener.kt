package cn.swiftpass.enterprise.utils

import android.view.View

/**
 * @author lizheng.zhao
 * @date 2022/05/25
 * @description 防止快速点击
 */
abstract class OnProhibitFastClickListener(var diff: Long = 1000L) : View.OnClickListener {

    abstract fun onFilterClick(v: View?)

    override fun onClick(v: View?) {
        v?.let {
            if (!ButtonUtils.isFastDoubleClick(it.id, diff)) {
                onFilterClick(it)
            }
        }
    }
}