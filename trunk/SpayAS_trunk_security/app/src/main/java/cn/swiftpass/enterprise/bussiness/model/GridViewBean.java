/*
 * 文 件 名:  GridViewBean.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-13
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * 图像实体类
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-13]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class GridViewBean implements Serializable
{
    /**
     * @return 返回 imageUrl
     */
    public String getImageUrl()
    {
        return imageUrl;
    }
    
    /**
     * @param 对imageUrl进行赋值
     */
    public void setImageUrl(String imageUrl)
    {
        this.imageUrl = imageUrl;
    }
    
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    // 文字
    private String text;
    
    private String imageUrl;
    
    /**
     * @return 返回 text
     */
    public String getText()
    {
        return text;
    }
    
    /**
     * @param 对text进行赋值
     */
    public void setText(String text)
    {
        this.text = text;
    }
    
    @Override
    public String toString()
    {
        return "GridViewBean [text=" + text + ", imageUrl=" + imageUrl + "]";
    }
    
}
