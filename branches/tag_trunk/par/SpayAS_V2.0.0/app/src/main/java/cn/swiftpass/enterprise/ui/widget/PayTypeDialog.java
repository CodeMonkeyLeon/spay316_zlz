/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.widget;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 弹出提示框
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-11]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class PayTypeDialog extends Dialog
{
    private Context context;
    private ViewGroup mRootView;

    private PayTypeDialog.HandleItemBtn handleItemBtn;
    

    private LinearLayout tv_close;

    private String money;
    
    TextView tv_dia_no;
    
    private ListView lv_pay;
    
    private ViewHolder holder;
    
    private PayTypeAdape payTypeAdape;
    
    private List<DynModel> list;
    
    /**
     * title 
     * content 提示内容
     * <默认构造函数>
     */
    public PayTypeDialog(Context context, String money, PayTypeDialog.HandleItemBtn handleItemBtn)
    {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        
        mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.pay_type_dialog_info, null);
        
        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);
        this.handleItemBtn = handleItemBtn;
        this.money = money;
        this.context = context;
        initView();
        
        initValue();
        setLinster();
        
    }
    
    private void initValue()
    {
        
        Object object = SharedPreUtile.readProduct("dynPayType" + ApiConstant.bankCode + MainApplication.getMchId());
        
        if (object != null)
        {
            list = (List<DynModel>)object;
            if (null != list && list.size() > 0)
            {
                payTypeAdape = new PayTypeAdape(context, list);
                lv_pay.setAdapter(payTypeAdape);
                return;
            }
        }
    }


    /**
     * 设置监听
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void setLinster()
    {
        lv_pay.setOnItemClickListener(new OnItemClickListener()
        {
            
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
            {
                dismiss();
                if (list.size() > 0)
                {
                    DynModel dynModel = list.get(position);
                    if (dynModel != null && !StringUtil.isEmptyOrNull(money))
                    {
                        handleItemBtn.toPay(money, dynModel.getApiCode());
                    }
                }
            }
        });
    }
    
    /**
     * 初始化
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView()
    {
        tv_dia_no = (TextView)findViewById(R.id.tv_dia_no);
        tv_close = (LinearLayout)findViewById(R.id.tv_close);
        
        lv_pay = (ListView)findViewById(R.id.lv_pay);
        
        tv_close.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });
        
    }
    
    class PayTypeAdape extends BaseAdapter
    {
        
        private List<DynModel> list;
        
        private Context context;
        
        private PayTypeAdape(Context context, List<DynModel> list)
        {
            this.context = context;
            this.list = list;
        }
        
        @Override
        public int getCount()
        {
            return list.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            return list.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(context, R.layout.activity_paytype_list_item, null);
                holder = new ViewHolder();
                holder.v_line = (View)convertView.findViewById(R.id.v_line);
                holder.iv_image = (ImageView)convertView.findViewById(R.id.iv_image);
                holder.tv_pay_name = (TextView)convertView.findViewById(R.id.tv_pay_name);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            if ((position + 1) == list.size())
            {
                holder.v_line.setVisibility(View.GONE);
            }
            else
            {
                holder.v_line.setVisibility(View.VISIBLE);
            }
            
            DynModel dynModel = list.get(position);
            if (null != dynModel)
            {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType()))
                {
                    holder.tv_pay_name.setText(MainApplication.getPayTypeMap().get(dynModel.getApiCode()));
                    Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_general_receivables);
                    if (bitmap != null)
                    {
                        
                        MainApplication.finalBitmap.display(holder.iv_image, dynModel.getSmallIconUrl(), bitmap);
                    }
                    else
                    {
                        MainApplication.finalBitmap.display(holder.iv_image, dynModel.getSmallIconUrl());
                    }
                }
            }
            
            return convertView;
        }
        
    }
    
    class ViewHolder
    {
        private View v_line;
        
        private ImageView iv_image;
        
        TextView tv_pay_name;
    }
    
    public void showPage(Class clazz)
    {
        Intent intent = new Intent(context, clazz);
        context.startActivity(intent);
    }
    

    public interface HandleItemBtn
    {
        void toPay(String money, String payType);
    }


}
