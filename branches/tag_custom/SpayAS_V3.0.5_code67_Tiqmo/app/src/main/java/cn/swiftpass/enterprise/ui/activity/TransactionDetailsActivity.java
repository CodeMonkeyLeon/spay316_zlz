package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.bill.BillOrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.ReportTransactionDetailsBean;
import cn.swiftpass.enterprise.bussiness.model.TransactionReportBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.fmt.FragmentDetailsChart;
import cn.swiftpass.enterprise.ui.fmt.FragmentDetailsChartPie;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabBill;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabPay;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabReport;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabSetting;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabSummary;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.SpayTitleView;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * Created by aijingya on 2019/4/17.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(报表详情的页面)
 * @date 2019/4/17.14:21.
 */
public class TransactionDetailsActivity extends FragmentActivity{
    public List<TransactionReportBean> transactionReportBeanList = new ArrayList<TransactionReportBean>();
    public int position;//当前已选的是哪一项的详情
    public String reportType;//当前选的报表类型，日报，周报还是月报，不同类型展示不同的详情

    private ImageView iv_pre_icon,iv_next_icon;
    private TextView tv_transaction_details_date;
    private LinearLayout ll_transaction_details_report;
    private TextView tv_detail_fee_type,tv_detail_money_transaction_report,tv_details_rate_text,tv_detail_rate_value;
    private TextView tv_detail_report_total_count_value,tv_detail_refund_amount_count,tv_details_refund_count_value,tv_detail_net_amount_value;
    private FrameLayout fl_chart_transaction_data,fl_chart_payment_channel_data,fl_chart_payment_top5_cashiers;
    private SpayTitleView report_transaction_details_spay_title_view;

    private FragmentDetailsChart FragmentTransactionData;
    private FragmentDetailsChartPie FragmentPaymentChannel;
    private FragmentDetailsChart FragmentCashierTop5;

    private ReportTransactionDetailsBean reportTransactionDetailsBean = new ReportTransactionDetailsBean();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.report_transaction_details_activity);

        Intent intent = this.getIntent();
        transactionReportBeanList = (ArrayList<TransactionReportBean>) intent.getSerializableExtra("TransactionReportList");
        position = intent.getIntExtra("position",0);
        reportType = intent.getStringExtra("reportType");

        //把当前的集合反转，因为列表的数据是倒序的,现在要正序
        Collections.reverse(transactionReportBeanList);
        position = transactionReportBeanList.size() - position+1;

        initView();
        initTransactionDetailsTitle();

        getTransactionDetails(transactionReportBeanList.get(position-1).getStartDate(),
                transactionReportBeanList.get(position-1).getEndDate());

    }

    public void initView(){
        report_transaction_details_spay_title_view = findViewById(R.id.report_transaction_details_spay_title_view);

        iv_pre_icon = findViewById(R.id.iv_pre_icon);
        iv_next_icon = findViewById(R.id.iv_next_icon);
        tv_transaction_details_date = findViewById(R.id.tv_transaction_details_date);
        ll_transaction_details_report = findViewById(R.id.ll_transaction_details_report);

        tv_detail_fee_type = findViewById(R.id.tv_detail_fee_type);
        tv_detail_money_transaction_report = findViewById(R.id.tv_detail_money_transaction_report);
        tv_details_rate_text = findViewById(R.id.tv_details_rate_text);
        tv_detail_rate_value = findViewById(R.id.tv_detail_rate_value);

        tv_detail_report_total_count_value = findViewById(R.id.tv_detail_report_total_count_value);
        tv_detail_refund_amount_count = findViewById(R.id.tv_detail_refund_amount_count);
        tv_details_refund_count_value = findViewById(R.id.tv_details_refund_count_value);
        tv_detail_net_amount_value = findViewById(R.id.tv_detail_net_amount_value);

        fl_chart_transaction_data = findViewById(R.id.fl_chart_transaction_data);
        fl_chart_payment_channel_data = findViewById(R.id.fl_chart_payment_channel_data);
        fl_chart_payment_top5_cashiers = findViewById(R.id.fl_chart_payment_top5_cashiers);

        iv_pre_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position = position-1;
                setPreOrNextEnable();
                if(reportType.equalsIgnoreCase("1")|| reportType.equalsIgnoreCase("3")){//如果是日报和月报则只显示一个时间
                    tv_transaction_details_date.setText(transactionReportBeanList.get(position-1).getStartDate());
                }else if(reportType.equalsIgnoreCase("2") ){//如果是周报，则显示时间段
                    tv_transaction_details_date.setText(transactionReportBeanList.get(position-1).getStartDate() +" "
                            +getResources().getString(R.string.data_to)+" "+transactionReportBeanList.get(position-1).getEndDate());
                }

                getTransactionDetails(transactionReportBeanList.get(position-1).getStartDate(),
                        transactionReportBeanList.get(position-1).getEndDate());
            }
        });

        iv_next_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position = position+1;
                setPreOrNextEnable();
                if(reportType.equalsIgnoreCase("1")|| reportType.equalsIgnoreCase("3")){//如果是日报和月报则只显示一个时间
                    tv_transaction_details_date.setText(transactionReportBeanList.get(position-1).getStartDate());
                }else if(reportType.equalsIgnoreCase("2") ){//如果是周报，则显示时间段
                    tv_transaction_details_date.setText(transactionReportBeanList.get(position-1).getStartDate() +" "
                            +getResources().getString(R.string.data_to)+" "+transactionReportBeanList.get(position-1).getEndDate());
                }

                getTransactionDetails(transactionReportBeanList.get(position-1).getStartDate(),
                        transactionReportBeanList.get(position-1).getEndDate());

            }
        });
    }

    public void setPreOrNextEnable(){
        if(position == 1){
            iv_pre_icon.setEnabled(false);
        }else{
            iv_pre_icon.setEnabled(true);
        }
        if(position == transactionReportBeanList.size()){
            iv_next_icon.setEnabled(false);
        }else{
            iv_next_icon.setEnabled(true);
        }
    }

    public void initData(){
        setPreOrNextEnable();
        if(reportType.equalsIgnoreCase("1")|| reportType.equalsIgnoreCase("3")){//如果是日报和月报则只显示一个时间
            tv_transaction_details_date.setText(transactionReportBeanList.get(position-1).getStartDate());
        }else if(reportType.equalsIgnoreCase("2") ){//如果是周报，则显示时间段
            tv_transaction_details_date.setText(transactionReportBeanList.get(position-1).getStartDate() +" "
                    +getResources().getString(R.string.data_to)+" "+transactionReportBeanList.get(position-1).getEndDate());
        }

        if(!TextUtils.isEmpty(MainApplication.getFeeFh())){
            tv_detail_fee_type.setText(MainApplication.getFeeFh());
        }

        tv_detail_money_transaction_report.setText(DateUtil.formatMoneyUtils(reportTransactionDetailsBean.getTransactionTotalBean().getTransactionAmountTotal()));

        if(reportType.equalsIgnoreCase("1")){//如果是日报则文案为DoD
            tv_details_rate_text.setText(getString(R.string.report_day_to_day));
        }else if(reportType.equalsIgnoreCase("2") ){//如果是周报则显示为WoW
            tv_details_rate_text.setText(getString(R.string.report_week_to_week));
        }else if(reportType.equalsIgnoreCase("3")){//如果是月报则显示为MoM
            tv_details_rate_text.setText(getString(R.string.report_month_to_month));
        }

        tv_detail_rate_value.setText(DateUtil.formatPaseRMBMoney(reportTransactionDetailsBean.getTransactionTotalBean()
                        .getTransactionRelativeRatio() *100.00f) +"%");

        tv_detail_report_total_count_value.setText(reportTransactionDetailsBean.getTransactionTotalBean().getTransactionCountTotal()+"");

        tv_detail_refund_amount_count.setText(DateUtil.getMoneyWithFeefh(DateUtil.formatMoneyUtils(reportTransactionDetailsBean.getTransactionTotalBean().getRefundAmountTotal())));

        tv_details_refund_count_value.setText(reportTransactionDetailsBean.getTransactionTotalBean().getRefundCountTotal()+"");

        tv_detail_net_amount_value.setText(DateUtil.getMoneyWithFeefh(DateUtil.formatMoneyUtils(reportTransactionDetailsBean.getTransactionTotalBean().getTransactionsRetainedProfits())));

    }

    //折线图的页面
    public void initTransactionDate(){
        fl_chart_transaction_data.removeAllViews();
        fl_chart_transaction_data.setVisibility(View.VISIBLE);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction mfragmentTransaction = fm.beginTransaction();

        FragmentTransactionData = new FragmentDetailsChart();
        FragmentTransactionData.setChartTitle(getString(R.string.report_transaction_data));
        FragmentTransactionData.setTransactionDetails(reportTransactionDetailsBean);
        FragmentTransactionData.setChartType("1");
        FragmentTransactionData.setReportType(reportType);
        mfragmentTransaction.add(R.id.fl_chart_transaction_data, FragmentTransactionData);

        mfragmentTransaction.commit();
    }

    //饼状图的页面
    public void initPaymentChannel(){
        fl_chart_payment_channel_data.removeAllViews();
        fl_chart_payment_channel_data.setVisibility(View.VISIBLE);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction mfragmentTransaction = fm.beginTransaction();

        if(FragmentPaymentChannel != null){
            mfragmentTransaction.remove(FragmentPaymentChannel);
        }

        FragmentPaymentChannel = new FragmentDetailsChartPie();
        FragmentPaymentChannel.setChartTitle(getString(R.string.report_payment_channel_data));
        FragmentPaymentChannel.setTransactionDetails(reportTransactionDetailsBean);

        mfragmentTransaction.add(R.id.fl_chart_payment_channel_data, FragmentPaymentChannel);

        mfragmentTransaction.commit();
    }



    //柱状图的页面
    public void initDataCashierTop5(){
        fl_chart_payment_top5_cashiers.removeAllViews();
        fl_chart_payment_top5_cashiers.setVisibility(View.VISIBLE);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction mfragmentTransaction = fm.beginTransaction();

        FragmentCashierTop5 = new FragmentDetailsChart();
        FragmentCashierTop5.setChartTitle(getString(R.string.report_top5_cashiers));
        FragmentCashierTop5.setTransactionDetails(reportTransactionDetailsBean);
        FragmentCashierTop5.setChartType("3");
        mfragmentTransaction.add(R.id.fl_chart_payment_top5_cashiers, FragmentCashierTop5);

        mfragmentTransaction.commit();
    }


    public SpayTitleView getTitleView() {
        return report_transaction_details_spay_title_view;
    }

    public void cleanTitleAllView() {
        if (report_transaction_details_spay_title_view != null){
            report_transaction_details_spay_title_view.cleanAllView();
        }
    }

   /*   @Override
  public void onAttachFragment(Fragment fragment){
        //当前的界面的保存状态，只是从新让新的Fragment指向了原本未被销毁的fragment，它就是onAttach方法对应的Fragment对象
        if(FragmentTransactionData == null && fragment instanceof FragmentDetailsChart){
            FragmentTransactionData = (FragmentDetailsChart)fragment;
        }else if(FragmentPaymentChannel == null && fragment instanceof FragmentDetailsChartPie){
            FragmentPaymentChannel = (FragmentDetailsChartPie)fragment;
        }else if(FragmentCashierTop5 == null && fragment instanceof FragmentDetailsChart){
            FragmentCashierTop5 = (FragmentDetailsChart)fragment;
        }
    }*/

    public void initTransactionDetailsTitle(){
        cleanTitleAllView();
        String titleText = "";
        if(reportType.equalsIgnoreCase("1")){//如果是日报则文案为日报
            titleText = getString(R.string.report_daily_report_title);
        }else if(reportType.equalsIgnoreCase("2") ){//如果是周报则显示为周报
            titleText =  getString(R.string.report_weekly_report_title);
        }else if(reportType.equalsIgnoreCase("3")){//如果是月报则显示为月报
            titleText =  getString(R.string.report_monthly_report_title);
        }
        getTitleView().addMiddleTextView(titleText);
        getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));

        getTitleView().addLeftImageView(R.drawable.icon_general_top_back_default, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    protected boolean checkSession() {
        if (isLoginRequired() && MainApplication.getContext().isNeedLogin()) {
            MainApplication.getContext().setNeedLogin(false);
            //            showNeedLoginToast();
            toastDialog(this, R.string.msg_need_login, new NewDialogInfo.HandleBtn() {

                @Override
                public void handleOkBtn() {
                    PreferenceUtil.removeKey("login_skey");
                    PreferenceUtil.removeKey("login_sauthid");
                    MainApplication.isSessionOutTime = true;
                    redirectLoginActivity();
                }
            });

            return true;
        }
        return false;
    }

    protected boolean isLoginRequired() {
        return true;
    }

    public void redirectLoginActivity() {

        Intent intent = new Intent();
        intent.setClassName(this, "cn.swiftpass.enterprise.ui.activity.WelcomeActivity");
        startActivity(intent);

        finish();
        for (Activity a : MainApplication.allActivities) {
            a.finish();
        }
    }


    public void getTransactionDetails(String startDate,String endDate){
        BillOrderManager.getInstance().QueryTransactionReportDetails(reportType,startDate,endDate,new UINotifyListener<ReportTransactionDetailsBean>(){
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loadDialog(TransactionDetailsActivity.this, R.string.public_data_loading);
            }

            @Override
            protected void onPostExecute() {
                super.onPostExecute();
                dismissLoading();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                dismissLoading();
                if (checkSession()) {
                    return;
                }

                if(object != null){
                   runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!StringUtil.isEmptyOrNull(object.toString())) {
                                toastDialog(TransactionDetailsActivity.this,object.toString() ,null);
                            } else {
                                toastDialog(TransactionDetailsActivity.this, R.string.tx_load_fail, null);
                            }
                        }
                    });
                }
            }

            @Override
            public void onSucceed(ReportTransactionDetailsBean result) {
                super.onSucceed(result);
                reportTransactionDetailsBean = result;
                if(result != null){
                    initData();
                }
                //如果是日报，则不展示折线图
                if(reportType.equalsIgnoreCase("1")){
                    initPaymentChannel();
                    initDataCashierTop5();
                }else if(reportType.equalsIgnoreCase("2") ||
                        reportType.equalsIgnoreCase("3")){//如果是周报和月报，则展示三个图
                    initTransactionDate();
                    initPaymentChannel();
                    initDataCashierTop5();
                }
            }
        });

    }

    private Dialog dialogNew = null;
    public void loadDialog(Activity context, int res) {
        try {

            if (null == dialogNew) {
                dialogNew = new Dialog(context, R.style.my_dialog);
                dialogNew.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogNew.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialogNew.setCanceledOnTouchOutside(false);
                LayoutInflater inflater = LayoutInflater.from(context);
                View v = inflater.inflate(R.layout.load_progress_info, null);// 得到加载view dialog_common
                TextView tv = (TextView) v.findViewById(R.id.tv_dialog);
                tv.setText(res);
                // dialog透明
                WindowManager.LayoutParams lp = dialogNew.getWindow().getAttributes();
                lp.alpha = 0.8f;
                dialogNew.getWindow().setAttributes(lp);
                dialogNew.setContentView(v);
                dialogNew.setOnKeyListener(new DialogInterface.OnKeyListener() {

                    @Override
                    public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                        if (keycode == KeyEvent.KEYCODE_BACK) {
                            return true;
                        }
                        return false;
                    }
                });
            }
            DialogHelper.resizeNew(context, dialogNew);
            dialogNew.show();
        } catch (Exception e) {
        }
    }


    public void dismissLoading() {
        if (null != dialogNew) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        dialogNew.dismiss();
                        dialogNew = null;
                    } catch (Exception e) {
                    }
                }
            });
        }
    }

    public void toastDialog(final Activity context, final Integer content, final NewDialogInfo.HandleBtn handleBtn) {
        context.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                try {

                    NewDialogInfo info = new NewDialogInfo(context, context.getString(content), null, 2, null, handleBtn);
                    info.setOnKeyListener(new DialogInterface.OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                            if (keycode == KeyEvent.KEYCODE_BACK) {
                                return true;
                            }
                            return false;
                        }
                    });
                    DialogHelper.resize(context, info);

                    info.show();
                } catch (Exception e) {
                }
            }
        });

    }

    /**
     * 只带内容
     * @param context
     * @param content
     * @param handleBtn
     */
    public void toastDialog(final Activity context, final String content, final NewDialogInfo.HandleBtn handleBtn) {
        context.runOnUiThread(new Runnable() {

            @Override
            public void run() {

                try {
                    if (StringUtil.isEmptyOrNull(content)) {
                        return;
                    }

                    NewDialogInfo info = new NewDialogInfo(context, content, null, 2, null, handleBtn);
                    info.setOnKeyListener(new DialogInterface.OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                            if (keycode == KeyEvent.KEYCODE_BACK) {
                                return true;
                            }
                            return false;
                        }
                    });
                    DialogHelper.resize(context, info);

                    info.show();
                } catch (Exception e) {
                }
            }
        });

    }

}
