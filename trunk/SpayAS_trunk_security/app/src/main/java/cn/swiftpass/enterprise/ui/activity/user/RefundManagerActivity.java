package cn.swiftpass.enterprise.ui.activity.user;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import androidx.annotation.Nullable;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnMenuItemClickListener;
import com.example.common.sentry.SentryUtils;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.enums.OrderStatusEnum;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.OrderSearchResult;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.bussiness.model.UserModelList;
import cn.swiftpass.enterprise.common.Constant;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.RefundManagerContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.RefundManagerPresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.activity.OrderDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.scan.CaptureActivity;
import cn.swiftpass.enterprise.ui.activity.settle.SettleCashierAdapter;
import cn.swiftpass.enterprise.ui.activity.settle.SettleUserActivity;
import cn.swiftpass.enterprise.ui.adapter.RefundHistoryAdapter;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.DisplayUtil;
import cn.swiftpass.enterprise.utils.EditTextUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 退款管理
 *
 * @author he_hui
 * @version [版本号, 2015-3-16]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class RefundManagerActivity extends BaseActivity<RefundManagerContract.Presenter> implements
        OnClickListener, TextWatcher, OnItemClickListener, RefundManagerContract.View {

    private static final String TAG = RefundManagerActivity.class.getSimpleName();
    public CashierAdapter cashierAdapter;
    boolean prompt = true; // 提示线下，默认不显示
    private Button refund_history_btn;
    private LinearLayout refund_back_btn;
    private EditText refund_search_input;
    private ImageView refund_search_clear_btn;
    private SwipeMenuListView refund_search_result_list;
    private RefundHistoryAdapter adapter;
    private List<UserModel> userModels;
    private Button but_seach;
    private SettleCashierAdapter settleCashierAdapter;
    private LinearLayout but_scan;
    private LinearLayout ll_refund_search_default;
    private List<Order> list;
    private TextView refund_des;
    private LinearLayout ll_refund_history;
    private boolean isFromOrderStreamSearch, defaultTimeNow;//是否是从订单流水查询跳转过来的
    private long searchTime = -1;
    private int serchType = -1;//查询类型 （0为查询订单流水 1为查询退款订单）
    private LinearLayout refund_top;
    private TextView tx_back;
    private List<UserModel> listViewUserFirst = new ArrayList<UserModel>();
    private List<UserModel> listViewUser;
    private TextView tv_null_info;
    private DialogInfo dialogInfo;

    @Override
    protected RefundManagerContract.Presenter createPresenter() {
        return new RefundManagerPresenter();
    }

    /**
     * {@inheritDoc}
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.refund_manager);

        getIntentValue();
        initObject();
        initView();
        initListener();
        showSoftInputFromWindow(RefundManagerActivity.this, refund_search_input);
        MainApplication.getInstance().getListActivities().add(RefundManagerActivity.this);
    }

    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void getIntentValue() {
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("bundle");
        if (bundle != null) {
            //            searchTime = bundle.getLong("searchTime", System.currentTimeMillis());
            isFromOrderStreamSearch = bundle.getBoolean("isFromOrderStreamSearch", false);
            serchType = bundle.getInt("serchType", 0);
            defaultTimeNow = bundle.getBoolean("defaultTimeNow", false);
        }
    }

    private void initObject() {
        list = new ArrayList<Order>();
    }

    private void initListener() {
        this.refund_history_btn.setOnClickListener(this);
        this.refund_back_btn.setOnClickListener(this);
        this.refund_search_clear_btn.setOnClickListener(this);
        this.refund_search_input.addTextChangedListener(this);
        but_seach.setOnClickListener(this);
        but_scan.setOnClickListener(this);
        refund_search_result_list.setOnItemClickListener(this);

        refund_search_input.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(refund_search_input.getText().toString())) {
                    but_seach.setVisibility(View.VISIBLE);
                    but_scan.setVisibility(View.GONE);
                }
            }
        });

    }

    private void initView() {
        refund_top = getViewById(R.id.refund_top);
        tx_back = getViewById(R.id.tx_back);
        this.refund_history_btn = getViewById(R.id.refund_history_btn);
        this.refund_back_btn = getViewById(R.id.refund_back_btn);
        this.refund_search_input = getViewById(R.id.refund_search_input);

        this.refund_search_clear_btn = getViewById(R.id.refund_search_clear_btn);
        this.refund_search_result_list = getViewById(R.id.refund_search_result_list);
        this.ll_refund_search_default = getViewById(R.id.ll_refund_search_default);

        this.but_seach = getViewById(R.id.but_seach);
        this.but_scan = getViewById(R.id.but_scan);
        this.refund_des = getViewById(R.id.refund_des);
        this.ll_refund_history = getViewById(R.id.ll_refund_history);
        tv_null_info = getViewById(R.id.tv_null_info);
        if (isFromOrderStreamSearch) {
            but_seach.setVisibility(View.VISIBLE);
            but_scan.setVisibility(View.GONE);
            this.refund_des.setVisibility(View.GONE);
            this.ll_refund_history.setVisibility(View.GONE);
            //            // 新建一个可以添加属性的文本对象
            //            SpannableString ss = new SpannableString("请输入您要搜索的交易号");
            //            // 新建一个属性对象,设置文字的大小
            //            AbsoluteSizeSpan ass = new AbsoluteSizeSpan(12, true);
            //            // 附加属性到文本
            //            ss.setSpan(ass, 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            //            // 设置hint
            //            this.refund_search_input.setHint(new SpannedString(ss));// 一定要进行转换,否则属性会消失
        } else {
            but_seach.setVisibility(View.GONE);
            but_scan.setVisibility(View.VISIBLE);
        }
        //初始显示最近数据
        if (serchType == -1) {
            search("", prompt);
        } else if (serchType == 3) { //收银员列表
            //            refund_search_input.setHint(R.string.tv_cashier_serch_hint);
            EditTextUtil.setHintTextSize(refund_search_input, getString(R.string.tv_cashier_serch_hint), 12);
            refund_search_input.setInputType(InputType.TYPE_CLASS_TEXT);
            listViewUser = new ArrayList<UserModel>();
            settleCashierAdapter = new SettleCashierAdapter(listViewUser, RefundManagerActivity.this, 0);
            refund_search_result_list.setVisibility(View.VISIBLE);
            ll_refund_search_default.setVisibility(View.GONE);
            refund_search_result_list.setAdapter(settleCashierAdapter);

            loadCashierData(0, null, false);
        } else if (serchType == 2) {
            EditTextUtil.setHintTextSize(refund_search_input, getString(R.string.tv_cashier_serch_hint), 12);
            refund_search_input.setInputType(InputType.TYPE_CLASS_TEXT);
            refund_search_input.setOnEditorActionListener(new OnEditorActionListener() {

                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if ((actionId == 0 || actionId == 3) && event != null) {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            if (!StringUtil.isEmptyOrNull(refund_search_input.getText().toString())) {
                                loadData(0, refund_search_input.getText().toString().trim());
                                return true;
                            }
                        }
                    }
                    return false;
                }
            });
            SwipeMenuCreator creator = new SwipeMenuCreator() {

                @Override
                public void create(SwipeMenu menu) {
                    SwipeMenuItem openItem = new SwipeMenuItem(getApplicationContext());
                    openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9, 0xCE)));
                    openItem.setWidth(DisplayUtil.dip2Px(RefundManagerActivity.this, 90));
                    openItem.setTitle(R.string.bt_delete);
                    openItem.setTitleSize(18);
                    openItem.setTitleColor(Color.WHITE);
                    openItem.setBackground(new ColorDrawable(Color.RED));
                    menu.addMenuItem(openItem);
                    //                SwipeMenuItem deleteItem = new SwipeMenuItem(getApplicationContext());
                    //                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9, 0x3F, 0x25)));
                    //                deleteItem.setWidth(dp2px(90));
                    //                deleteItem.setIcon(R.drawable.ic_delete);
                    //                menu.addMenuItem(deleteItem);
                }
            };

            refund_search_result_list.setMenuCreator(creator);

            refund_search_result_list.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                @Override
                public void onMenuItemClick(int position, SwipeMenu menu, int index) {

                    switch (index) {
                        case 0:
                            UserModel userModel = userModels.get(position);
                            if (userModel != null) {
                                delete(userModel.getId(), position);
                            }
                            break;
                        //                    case 1:
                        //                        listUser.remove(position);
                        //                        mAdapter.notifyDataSetChanged();
                        //                        break;
                    }
                }
            });
        }
        //定制化版本 初始化
        DynModel dynModel = (DynModel) SharedPreUtils.readProduct("dynModel" + BuildConfig.bankCode);
        if (null != dynModel) {
            try {
                refund_top.setBackgroundColor(Color.parseColor(dynModel.getHeaderColor()));
                tx_back.setTextColor(Color.parseColor(dynModel.getHeaderFontColor()));
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonColor())) {
                    but_seach.setBackgroundColor(Color.parseColor(dynModel.getButtonColor()));
                }
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonFontColor())) {
                    but_seach.setTextColor(Color.parseColor(dynModel.getButtonFontColor()));
                }
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                Log.e(TAG, Log.getStackTraceString(e));
            }

        } else {
            //            refund_back_btn.setBackgroundResource(R.drawable.micro_pay_);
            //            but_scan.setBackgroundResource(R.drawable.micro_pay_);
        }
    }


    @Override
    public void cashierDeleteSuccess(boolean response, int position) {
        if (response) {
            toastDialog(RefundManagerActivity.this,
                    R.string.show_delete_succ,
                    new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            userModels.remove(position);
                            cashierAdapter.notifyDataSetChanged();
                        }
                    });
            //                                    showPage(CashierManager.class);
            //                                    finish();
        }
    }

    @Override
    public void cashierDeleteFailed(@Nullable Object error) {
        if (error != null) {
            toastDialog(RefundManagerActivity.this, error.toString(), null);
        }
    }

    void delete(final long userId, final int position) {

        dialogInfo =
                new DialogInfo(RefundManagerActivity.this, getString(R.string.public_cozy_prompt),
                        getString(R.string.dialog_delete), getString(R.string.btnOk), getString(R.string.btnCancel),
                        DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

                    @Override
                    public void handleOkBtn() {
                        if (mPresenter != null) {
                            mPresenter.cashierDelete(userId, position);
                        }
                    }

                    @Override
                    public void handleCancelBtn() {
                        dialogInfo.cancel();
                    }
                }, null);

        DialogHelper.resize(RefundManagerActivity.this, dialogInfo);
        dialogInfo.show();
    }


    @Override
    public void queryCashierSuccess(UserModelList response, boolean isFirst, boolean isLoadCashier) {
        if (isLoadCashier) {
            if (response.data != null && response.data.size() > 0) {

                userModels = response.data;
                tv_null_info.setVisibility(View.GONE);
                refund_search_result_list.setVisibility(View.VISIBLE);
                ll_refund_search_default.setVisibility(View.GONE);
                if (!isFirst) { //第一次
                    listViewUserFirst.addAll(response.data);
                    listViewUser.addAll(response.data);
                    settleCashierAdapter.notifyDataSetChanged();
                } else {
                    if (listViewUser.size() > 0) { //有数据
                        listViewUser.clear();
                    }
                    listViewUser.addAll(response.data);

                    settleCashierAdapter.notifyDataSetChanged();
                }
            } else {
                showToastInfo(R.string.tv_no_user);
                RefundManagerActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        refund_search_result_list.setVisibility(View.GONE);
                        ll_refund_search_default.setVisibility(View.VISIBLE);
                    }
                });
            }
        } else {

            if (response.data != null && response.data.size() > 0) {
                userModels = response.data;
                cashierAdapter = new CashierAdapter(response.data, RefundManagerActivity.this);
                refund_search_result_list.setVisibility(View.VISIBLE);
                ll_refund_search_default.setVisibility(View.GONE);
                refund_search_result_list.setAdapter(cashierAdapter);
            } else {

                showToastInfo(R.string.tv_no_user);
                RefundManagerActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        refund_search_result_list.setVisibility(View.GONE);
                        ll_refund_search_default.setVisibility(View.VISIBLE);
                    }
                });

            }

        }
    }

    @Override
    public void queryCashierFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }
        if (error != null) {
            showToastInfo(error.toString());
        }
    }

    /**
     * 加载收银员列表
     * <功能详细描述>
     *
     * @param page
     * @see [类、类#方法、类#成员]
     */
    private void loadCashierData(final int page, String serchContent, final boolean isFirst) {
        if (mPresenter != null) {
            mPresenter.queryCashier(page, 10, serchContent, isFirst, true);
        }
    }

    private void loadData(final int page, final String input) {
        if (mPresenter != null) {
            mPresenter.queryCashier(page, 10, input, false, false);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.but_scan:// 扫一扫退款
                CaptureActivity.startActivity(RefundManagerActivity.this, Constant.PAY_TYPE_REFUND);
                break;
            case R.id.refund_history_btn:
                showPage(RefundHistoryActivity.class);
                break;
            case R.id.refund_back_btn:
                this.finish();
                break;
            case R.id.refund_search_clear_btn:
                this.refund_search_input.setText("");
                tv_null_info.setVisibility(View.GONE);
                if (isFromOrderStreamSearch && serchType == 3) {
                    if (listViewUser.size() > 0) { //有数据
                        listViewUser.clear();
                    }
                    listViewUser.addAll(listViewUserFirst);

                    settleCashierAdapter.notifyDataSetChanged();
                } else {
                    this.refund_search_clear_btn.setVisibility(View.GONE);
                    this.refund_search_result_list.setVisibility(View.GONE);
                    this.ll_refund_search_default.setVisibility(View.VISIBLE);
                    notifyListView();
                }
                break;
            case R.id.but_seach:

                //                queryOrderGetStuts("75211000011440060254800");
                if (isFromOrderStreamSearch) {
                    //                    searchOrderStream();
                    if (serchType == 0) {
                        //查询流水订单
                        searchOrderStream();
                    } else if (serchType == 1) {
                        //根据单号查询退款 
                        searchRefundByDate();
                    } else if (serchType == 2) { //收银员收索
                        if (!StringUtil.isEmptyOrNull(refund_search_input.getText().toString())) {
                            loadData(0, refund_search_input.getText().toString().trim());
                        }
                    } else if (serchType == 3) {
                        //交易汇总里面的收索
                        loadCashierData(0, refund_search_input.getText().toString().trim(), true);
                    }
                    /*else if (serchType == 4)
                    {
                        try
                        {
                            if (defaultTimeNow)
                            { //实时
                                loadOrderTotal(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00",
                                    DateUtil.formatTime(System.currentTimeMillis()),
                                    "5",
                                    refund_search_input.getText().toString());
                            }
                            else
                            {//按天
                            
                                loadOrderTotal(DateTimeUtil.getMothdFirst() + " 00:00:00",
                                    DateUtil.formatTime(DateUtil.getBeforeDate().getTime()),
                                    "5",
                                    refund_search_input.getText().toString());
                            }
                        }
                        catch (Exception e)
                        {
                            Log.e(TAG,Log.getStackTraceString(e));
                        }
                    }*/

                } else {
                    //查询退款 
                    final String orderNo = refund_search_input.getText().toString().trim();
                    if (orderNo == null || orderNo.length() < 1) {
                        showToastInfo(getStringById(R.string.please_enter_number));
                        return;
                    }
                    search(orderNo, false);
                }
                break;
        }

    }


    @Override
    public void queryUserRefundOrderSuccess(ArrayList<Order> response, String orderNo) {
        mySuccessd(response, orderNo, RefundHistoryAdapter.REFUND_SEARCH);
    }

    @Override
    public void queryUserRefundOrderFailed(@Nullable Object error) {
        if (error != null) {
            showToastInfo(error.toString());
        }
    }

    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void searchRefundByDate() {

        final String orderNo = refund_search_input.getText().toString().trim();
        if (orderNo == null || orderNo.length() < 1) {
            showToastInfo(getStringById(R.string.please_enter_number));
            return;
        }

        String formtTime = null;
        if (searchTime != -1) {
            formtTime = DateUtil.formatYYMD(searchTime);
        }


        if (mPresenter != null) {
            mPresenter.queryUserRefundOrder(
                    orderNo,
                    formtTime,
                    MainApplication.getInstance().getMchId(),
                    null,
                    0
            );
        }
    }

    private void notifyListView() {
        if (this.list != null && list.size() > 0) {
            list.clear();
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void queryRefundOrderSuccess(ArrayList<Order> response, String orderNo) {
        if (response != null && response.size() > 0) {
            mySuccessd(response, orderNo, RefundHistoryAdapter.ORDER_STREAM);
        } else {
            refund_search_clear_btn.setVisibility(View.INVISIBLE);
            refund_search_result_list.setVisibility(View.GONE);
            ll_refund_search_default.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void queryRefundOrderFailed(@Nullable Object error) {
        if (null != error && !"".equals(error) && !prompt) {
            showToastInfo(error.toString());
        }
    }

    private void search(final String orderNo, final boolean prompt) {
        if (mPresenter != null) {
            mPresenter.queryRefundOrder(
                    orderNo,
                    MainApplication.getInstance().getMchId(),
                    0
            );
        }
    }

    /**
     * 退款扫一扫
     * <功能详细描述>
     *
     * @param view
     * @see [类、类#方法、类#成员]
     */
    public void onScanClick(View view) {

    }

    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @param result
     * @see [类、类#方法、类#成员]
     */
    protected void mySuccessd(List<Order> result, String orderNo, int tag) {
        if (result != null && result.size() > 0) {
            if (list.size() > 0) {
                list.clear();
            }
            for (Order o : result) {
                String sh, transactionId;
                if (o.getOutTradeNo().length() > 10) {
                    sh = o.getOutTradeNo().substring(0, 10);//商户号
                    transactionId = o.getOutTradeNo().substring(10);//订单号
                } else {
                    sh = o.getOutTradeNo();
                    transactionId = o.getOutTradeNo();
                }
                if (sh.contains(orderNo))//如果商户号包含搜索关键字，就判断订单号是否也包含搜索关键字
                {
                    if (transactionId.contains(orderNo)) {
                        list.add(o);
                    }
                } else {
                    list.add(o);
                }
            }
            //            list = result;
            refund_search_result_list.setVisibility(View.VISIBLE);
            ll_refund_search_default.setVisibility(View.GONE);
            adapter = new RefundHistoryAdapter(RefundManagerActivity.this, list, tag);
            adapter.setSeachContent(orderNo);
            refund_search_result_list.setAdapter(adapter);
        } else {
            showToastInfo(getStringById(R.string.records_not_found));
        }
    }

    @Override
    public void afterTextChanged(Editable arg0) {

    }

    @Override
    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        this.but_scan.setVisibility(View.GONE);
        this.but_seach.setVisibility(View.VISIBLE);
    }

    @Override
    public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        String searchContent = refund_search_input.getText().toString().trim();
        if (searchContent != null && searchContent.length() > 0) {
            this.refund_search_clear_btn.setVisibility(View.VISIBLE);
            if (!isFromOrderStreamSearch) {
                this.but_scan.setVisibility(View.GONE);
                this.but_seach.setVisibility(View.VISIBLE);
            }
        } else {
            if (!isFromOrderStreamSearch) {
                this.but_scan.setVisibility(View.VISIBLE);
                this.but_seach.setVisibility(View.GONE);
            }

            this.refund_search_clear_btn.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void queryOrderDetailSuccess(Order response) {
        if (response != null) {
            OrderDetailsActivity.startActivity(RefundManagerActivity.this, response);
        }
    }

    @Override
    public void queryOrderDetailFailed(@Nullable Object error) {
        if (error != null) {
            showToastInfo(error.toString());
        }
    }


    @Override
    public void queryRefundDetailSuccess(Order response) {
        if (response != null) {
            RefundRecordOrderDetailsActivity.startActivity(RefundManagerActivity.this, response);
        }
    }

    @Override
    public void queryRefundDetailFailed(@Nullable Object error) {
        if (error != null) {
            showToastInfo(error.toString());
        }
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
        Order order = null;
        if (null != list && list.size() > 0) {
            order = list.get(position);
        }
        if (isFromOrderStreamSearch) {
            if (serchType == 0) {
                //查询流水订单
                if (order != null) {
                    if (mPresenter != null) {
                        mPresenter.queryOrderDetail(
                                order.getOutTradeNo(),
                                MainApplication.getInstance().getUserInfo().mchId,
                                true
                        );
                    }
                }
            } else if (serchType == 1) {
                if (order != null) {
                    if (mPresenter != null) {
                        mPresenter.queryRefundDetail(
                                order.getOutRefundNo(),
                                MainApplication.getInstance().getMchId()
                        );
                    }
                }
            } else if (serchType == 2) {//收银员列表收索查看详情
                try {

                    UserModel userModel = userModels.get(position - 1);
                    if (userModel != null) {

                        Bundle bundle = new Bundle();
                        bundle.putSerializable("userModel", userModel);
                        showPage(CashierAddActivity.class, bundle);
                    }

                } catch (Exception e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    UserModel userModel = userModels.get(position - 1);
                    if (userModel != null) {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("userModel", userModel);
                        showPage(CashierAddActivity.class, bundle);
                    }
                }
            } else if (serchType == 3) {
                try {
                    UserModel userModel = listViewUser.get(position);
                    //                    HandlerManager.notifyMessage(HandlerManager.SETTLE_TYPE, HandlerManager.SETTLE_TYPE, userModel);
                    //                    finish();

                    SettleUserActivity.startActivity(RefundManagerActivity.this, userModel);

                } catch (Exception e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    UserModel userModel = listViewUser.get(position - 1);
                    //                    HandlerManager.notifyMessage(HandlerManager.SETTLE_TYPE, HandlerManager.SETTLE_TYPE, userModel);
                    SettleUserActivity.startActivity(RefundManagerActivity.this, userModel);
                }
            }
            //            else if (serchType == 4)
            //            {
            //                try
            //                {
            //                    UserModel userModel = listViewUser.get(position);
            //                    HandlerManager.notifyMessage(HandlerManager.PAY_TOTAL_TYPE_DISTRIBUTION_USER,
            //                        HandlerManager.PAY_TOTAL_TYPE_DISTRIBUTION_USER,
            //                        userModel);
            //                    finish();
            //                    
            //                }
            //                catch (Exception e)
            //                {
            //                    UserModel userModel = listViewUser.get(position - 1);
            //                    HandlerManager.notifyMessage(HandlerManager.PAY_TOTAL_TYPE_DISTRIBUTION_USER,
            //                        HandlerManager.PAY_TOTAL_TYPE_DISTRIBUTION_USER,
            //                        userModel);
            //                    finish();
            //                }
            //            }

        } else {
            //查询退款订单
            if (order != null) {

                if (mPresenter != null) {
                    mPresenter.queryOrderDetail(
                            order.getOutTradeNo(),
                            MainApplication.getInstance().getMchId(),
                            true
                    );
                }
            }
        }
    }


    @Override
    public void queryOrderDataSuccess(OrderSearchResult response, String orderNo) {
        if (response == null) {
            mySuccessd(null, orderNo, RefundHistoryAdapter.ORDER_STREAM);
        } else {
            mySuccessd(response.data, orderNo, RefundHistoryAdapter.ORDER_STREAM);
        }
    }

    @Override
    public void queryOrderDataFailed(@Nullable Object error) {
        if (error != null) {
            showToastInfo(error.toString());
        }
    }

    //查询订单流水
    private void searchOrderStream() {
        OrderStatusEnum orderState = OrderStatusEnum.PAY_SUCCESS;
        if (!TextUtils.isEmpty(MainApplication.getInstance().getMchId())) {
            final String orderNo = refund_search_input.getText().toString().trim();
            if (orderNo == null || orderNo.length() < 1) {
                showToastInfo(getStringById(R.string.please_enter_number));
                return;
            }

            String formtTime = null;
            if (searchTime != -1) {
                formtTime = DateUtil.formatYYMD(searchTime);
            }

            if (mPresenter != null) {
                mPresenter.queryOrderData(
                        orderNo,
                        0,
                        orderState.getValue(),
                        formtTime,
                        null
                );
            }
        }
    }
}
