/*
 * 文 件 名:  BillMainActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-8
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.bill;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.bill.BillOrderManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.refund.RefundManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.bussiness.model.WxCard;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.OrderDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.list.NewPullDownListView;
import cn.swiftpass.enterprise.ui.activity.list.PullToRefreshBase.OnRefreshListener;
import cn.swiftpass.enterprise.ui.activity.list.PullToRefreshListView;
import cn.swiftpass.enterprise.ui.activity.user.RefundRecordOrderDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.user.VerificationDetails;
import cn.swiftpass.enterprise.ui.widget.SelectDatePopupWindow;
import cn.swiftpass.enterprise.ui.widget.SelectPicPopupWindow;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 账单 主界面
 *
 * @author he_hui
 * @version [版本号, 2016-11-8]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class BillMainActivity extends TemplateActivity implements NewPullDownListView.OnRefreshListioner, NewPullDownListView.OnLoadDateRefreshListioner {
    private static final String TAG = BillMainActivity.class.getSimpleName();

    //private NewPullDownListView bill_list;

    private ViewHolder holder;

    //    private PinnedHeaderListView listView;

    private ListView listView;

    private UserModel userModel;

    private List<Order> orderList = new ArrayList<Order>();

    private Handler mHandler = new Handler();

    private BillStreamAdapter billStreamAdapter;

//    private LinearLayout ly_choice;

    private List<String> choiceTypeList = new ArrayList<String>();

    private List<String> payTypeList = new ArrayList<String>();

    private Map<String, String> dayCountMap = new HashMap<String, String>();

    private int pageFulfil = 0;

    private TextView tv_prompt;

    //private EditText refund_search_input;

    private int isRef = 0;//0流水，1，退款，2 卡券

    //private Map<Integer, String> mapPostion = new HashMap<Integer, String>();

    private PullToRefreshListView mPullRefreshListView;

    //private TextView tv_date;

    private TextView tv_total;

    private RelativeLayout ly_title;

    private static Map<String, String> dayTatal = new HashMap<String, String>();

    private List<String> titleList = new ArrayList<String>();

    private RelativeLayout ly_stream;

    //private Integer pageCount = 0;

    private String streamTime, refundTime;

    //private boolean scrollFlag = false;// 标记是否滑动

    List<Integer> tradeType = new ArrayList<Integer>();

    List<Integer> payType = new ArrayList<Integer>();

    //private boolean isRefresh = true;

    private boolean isMoerRefresh = true;//刷新反正多次刷新

    private LinearLayout ly_but_choice, ly_choice_date, ly_but_cachier;

    private TextView tv_cashier_name;

    private String time;

    private TextView tv_time;

    SelectDatePopupWindow selectDatePopupWindow;

    private boolean loadMore = true;

    private Integer reqFeqTime = 0;

    private boolean loadNext = false;

    Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == HandlerManager.STREAM_BILL) { //流水
                if (!StringUtil.isEmptyOrNull(refundTime)) {
                    String value = dayTatal.get(refundTime);
                    if (!StringUtil.isEmptyOrNull(value)) {

                        tv_total.setText(getString(R.string.tx_bill_stream_total) + DateUtil.formatMoneyUtils(Long.parseLong(value)));
                    }
                }

            } else if (msg.what == HandlerManager.STREAM_BILL_REFUND) {//退款

                if (!StringUtil.isEmptyOrNull(streamTime)) {
                    String value = dayTatal.get(streamTime);
                    if (!StringUtil.isEmptyOrNull(value)) {
                        tv_total.setText(getString(R.string.tv_refund_title_info) + DateUtil.formatMoneyUtils(Long.parseLong(value)));
                    }
                }
            } else if (msg.what == HandlerManager.PAY_FINISH_REFUND) {
                tv_total.setText("");
            } else if (msg.what == HandlerManager.PAY_SWITCH_TAB) {//刷新账单
                if (choiceTypeList.size() > 0) {
                    choiceTypeList.clear();
                }
                payTypeList.clear();
                isRef = 0;
                //                orderList.clear();
                tradeType.clear();
                payType.clear();
                isMoerRefresh = true;

                time = "";
                if (tv_time != null) {

                    tv_time.setText(R.string.tx_today);
                } else {
                    tv_time = getViewById(R.id.tv_time);
                    tv_time.setText(R.string.tx_today);
                }
                loadDateTask(1, 1, true, false, getSelectedUserId());

            } else if (msg.what == HandlerManager.PAY_DETAIL_TO_REFRESH) {
                //                orderList.clear();
                loadDateTask(1, 1, true, false, getSelectedUserId());
            } else if (msg.what == HandlerManager.BILL_CHOICE_USER) { //收银员
                try {
                    if (loadNext) { //
                        toastDialog(BillMainActivity.this, R.string.tx_request_more, null);
                        return;
                    }
                    userModel = (UserModel) msg.obj;
                    if (userModel != null) {
                        tv_cashier_name.setVisibility(View.VISIBLE);
                        tv_cashier_name.setText(userModel.getRealname());
                        //如果是退款
                        if (isRef == 1) {
                            loadDateTask(1, 1, true, true, getSelectedUserId());
                        } else {
                            loadDateTask(1, 1, true, false, getSelectedUserId());
                        }
                    } else {
                        tv_cashier_name.setVisibility(View.VISIBLE);
                        tv_cashier_name.setText(R.string.tx_bill_stream_cashier);
                        if (isRef == 1) {//如果是退款
                            loadDateTask(1, 1, true, true, null);
                        } else {
                            loadDateTask(1, 1, true, false, null);
                        }
                    }
                } catch (Exception e) {
                    tv_cashier_name.setVisibility(View.VISIBLE);
                    tv_cashier_name.setText(R.string.tx_bill_stream_cashier);
                    if (isRef == 1) {//如果是退款
                        loadDateTask(1, 1, true, true, null);
                    } else {
                        loadDateTask(1, 1, true, false, null);
                    }
                }
            }

        }

        ;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_stream);
        initView();
        setLister();
        HandlerManager.registerHandler(HandlerManager.STREAM_BILL, handler);
        HandlerManager.registerHandler(HandlerManager.BILL_CHOICE_USER, handler);
        if (orderList.size() == 0) {
            orderList.clear();
            loadDateTask(1, 1, true, false, null);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    void loadDateTask(int page, int pullType, final boolean isLoadMore, boolean isRefund, String userId) {

        if (null != selectDatePopupWindow) {
            selectDatePopupWindow.dismiss();
        }
        switch (pullType) {
            case 1:
                loadMore = true;
                pageFulfil = 1;
                titleList.clear();
                loadDate(pageFulfil, isLoadMore, isRef, time, userId);
                break;
            case 2:
                mPullRefreshListView.onRefreshComplete();
                break;
            case 3:
                pageFulfil = pageFulfil + 1;
                if (loadMore) {
                    loadDate(pageFulfil, isLoadMore, isRef, time, userId);
                } else {
                    mPullRefreshListView.onRefreshComplete();
                }
                break;
            default:
                break;
        }
    }

    /**
     * 暂停 后 才去请求
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void sleep(long time) {
        try {
            //            Thread.sleep(time * 1000);

            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Logger.i("hehui", "sleep-->");
                            loadNext = false;
                        }
                    });

                }

            };
            Timer timer = new Timer();
            timer.schedule(task, time * 1000);

        } catch (Exception e) {
            return;
        }
    }

    void loadDate(final int page, final boolean isLoadMore, int isRefund, String startDate, String userId) {
        Logger.i("hehui", "loadNext-->" + loadNext);
        if (loadNext) {
            mPullRefreshListView.onRefreshComplete();
            toastDialog(BillMainActivity.this, R.string.tx_request_more, null);
            return;
        }

        BillOrderManager.getInstance().querySpayOrder(reqFeqTime, choiceTypeList, payTypeList, isRefund, page, null,
                userId, startDate, new UINotifyListener<List<Order>>() {
                    @Override
                    public void onPreExecute() {
                        super.onPreExecute();
                        reqFeqTime = 0;
                        if (page == 1) {
                            isMoerRefresh = false;
                        }
                        if (isLoadMore) {
                            loadDialog(BillMainActivity.this, R.string.public_data_loading);
                        }
                    }

                    @Override
                    public void onPostExecute() {
                        super.onPostExecute();
                    }

                    @Override
                    public void onError(final Object object) {
                        super.onError(object);
                        dissDialog();
                        isMoerRefresh = true;
                        if (checkSession()) {
                            return;
                        }
                        if (null != object) {
                            if (object.toString().startsWith("reqFeqTime")) { // 服务器返回有这个参数，说明服务器有压力，需要暂停reqFeqTime=对应的秒数才去请求返回
                                String time = object.toString().substring(object.toString().lastIndexOf("=") + 1);
                                if (!StringUtil.isEmptyOrNull(time)) {
                                    Integer count = Integer.parseInt(time);
                                    loadNext = true;
                                    sleep(count);
                                } else {
                                    dissDialog();
                                }
                            } else {
                                dissDialog();
                                BillMainActivity.this.runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        if (!StringUtil.isEmptyOrNull(object.toString())) {
                                            toastDialog(BillMainActivity.this, object.toString(), null);
                                        } else {
                                            toastDialog(BillMainActivity.this, R.string.tx_load_fail, null);
                                        }

                                        if (orderList.size() == 0) {
                                            tv_prompt.setVisibility(View.VISIBLE);
                                            ly_title.setVisibility(View.GONE);
                                            mPullRefreshListView.setVisibility(View.GONE);
                                            ly_stream.setVisibility(View.GONE);
                                        }
                                    }
                                });
                            }

                        }
                    }

                    @Override
                    public void onSucceed(List<Order> model) {
                        dissDialog();
                        isMoerRefresh = true;
                        if (null != model && model.size() > 0) {
                            tv_prompt.setVisibility(View.GONE);
                            if (page == 1) {
                                orderList.clear();
                            }

                            //pageCount = model.get(0).getPageCount();

                            reqFeqTime = model.get(0).getReqFeqTime();

                            setListData(mPullRefreshListView, orderList, billStreamAdapter, model, isLoadMore, page);
                            BillMainActivity.this.runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    // TODO Auto-generated method stub
                                    mPullRefreshListView.setVisibility(View.VISIBLE);
                                    ly_stream.setVisibility(View.VISIBLE);

                                }
                            });
                            //暂停
                            if (reqFeqTime > 0) {
                                loadNext = true;
                                sleep(reqFeqTime);
                            }
                            // mySetListData(bill_list, orderList, billStreamAdapter, model, isLoadMore);
                        } else {
                            loadMore = false;
                            if (orderList.size() == 0) {
                                BillMainActivity.this.runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {

                                        tv_prompt.setVisibility(View.VISIBLE);
                                        ly_title.setVisibility(View.GONE);
                                        mPullRefreshListView.setVisibility(View.GONE);
                                        ly_stream.setVisibility(View.GONE);
                                    }
                                });
                                //                            bill_list.setVisibility(View.GONE);
                            } else {
                                mPullRefreshListView.onRefreshComplete();
                            }
                        }
                    }
                });
    }

    private void setTime(long time) {
        String newDate = DateUtil.formatYYMD(System.currentTimeMillis());
        String date = DateUtil.formatYYMD(time);

        String[] arr1 = newDate.split("\\-");

        String[] arr2 = date.split("\\-");

        if (arr1[0].equals(arr2[0]) && arr1[1].equals(arr2[1]) && arr1[2].equals(arr2[2])) {
            tv_time.setText(R.string.tx_today);
        } else {

            tv_time.setText(DateUtil.formatYYMD(time));
        }
    }

    private void setLister() {

        tv_prompt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                loadDateTask(1, 1, false, false, getSelectedUserId());
            }
        });

        ly_choice_date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectDatePopupWindow = new SelectDatePopupWindow(BillMainActivity.this, time, new SelectDatePopupWindow.HandleBtn() {

                    @Override
                    public void handleOkBtn(long time) {
                        if (time == 0) {

                            if (!StringUtil.isEmptyOrNull(BillMainActivity.this.time)) {
                                tv_time.setText(BillMainActivity.this.time);
                                orderList.clear();
                                loadDateTask(1, 1, true, false, getSelectedUserId());
                                return;
                            } else time = System.currentTimeMillis();

                        }
                        long nowTime = System.currentTimeMillis();
                        if (time > nowTime) {
                            toastDialog(BillMainActivity.this, R.string.tx_date, null);
                            return;
                        }
                        Calendar ca = Calendar.getInstance();//得到一个Calendar的实例  90天
                        ca.add(Calendar.DAY_OF_MONTH, -90);

                        if (time <= ca.getTimeInMillis()) {
                            toastDialog(BillMainActivity.this, R.string.tx_choice_date, null);
                            return;
                        }
                        setTime(time);
                        BillMainActivity.this.time = DateUtil.formatYYMD(time);
                        orderList.clear();
                        loadDateTask(1, 1, true, false, getSelectedUserId());
                    }

                });

                selectDatePopupWindow.showAtLocation(ly_choice_date, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0); //设置layout在PopupWindow中显示的位置
            }
        });

//        ly_but_choice.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                try {
//                    StatService.trackCustomEvent(BillMainActivity.this, "SPConstTapBillFilter", "账单筛选");
//                } catch (Exception e) {
//                    Log.e(TAG,Log.getStackTraceString(e));
//                }
//
//                SelectPicPopupWindow p = new SelectPicPopupWindow(false,BillMainActivity.this, choiceTypeList, payTypeList, new SelectPicPopupWindow.HandleBtn() {
//
//                    @Override
//                    public void handleOkBtn(List<String> choiceType, List<String> payTypeList,boolean ishandled) {
//                        BillMainActivity.this.payTypeList = payTypeList;
//                        choiceTypeList = choiceType;
//                        orderList.clear();
//                        if (choiceType.contains("refund")) {
//                            isRef = 1;
//                            //                                    loadDate(1, true, true);
//                            if (choiceTypeList.size() > 1) {
//                                HandlerManager.notifyMessage(HandlerManager.STREAM_BILL, HandlerManager.PAY_FINISH_REFUND);
//                            } else {
//                                HandlerManager.notifyMessage(HandlerManager.STREAM_BILL, HandlerManager.STREAM_BILL_REFUND);
//                            }
//                            loadDateTask(1, 1, true, true,getSelectedUserId());
//                            dayCountMap.clear();
//                            dayTatal.clear();
//                        } else if (choiceType.contains("card")) {
//                            isRef = 2;
//                            loadDateTask(1, 1, true, false,getSelectedUserId());
//                            dayCountMap.clear();
//                            dayTatal.clear();
//                        } else {
//                            dayCountMap.clear();
//                            dayTatal.clear();
//                            HandlerManager.notifyMessage(HandlerManager.STREAM_BILL, HandlerManager.STREAM_BILL);
//                            isRef = 0;
//                            //                                    loadDate(1, true, false);
//                            tradeType.clear();
//                            payType.clear();
//                            BillOrderManager.getInstance().paseToJson(choiceType, tradeType, payType);
//
//                            loadDateTask(1, 1, true, false,getSelectedUserId());
//                        }
//                    }
//                });
//                p.showAtLocation(ly_but_choice, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0); //设置layout在PopupWindow中显示的位置
//            }
//        });


        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Order order = orderList.get(position - 1);
                if (null != order) {
                    //isRefresh = false;
                    if (isRef == 1) {
                        RefundManager.getInstant().queryRefundDetail(order.getOutRefundNo(), MainApplication.getMchId(), new UINotifyListener<Order>() {

                            @Override
                            public void onError(Object object) {
                                super.onError(object);
                                dismissLoading();
                                if (checkSession()) {
                                    return;
                                }
                                if (object != null) {
                                    toastDialog(BillMainActivity.this, object.toString(), null);
                                }
                            }

                            @Override
                            public void onPreExecute() {
                                super.onPreExecute();

                                loadDialog(BillMainActivity.this, getStringById(R.string.public_data_loading));
                            }

                            @Override
                            public void onSucceed(Order result) {
                                super.onSucceed(result);
                                dismissLoading();
                                if (result != null) {
                                    RefundRecordOrderDetailsActivity.startActivity(BillMainActivity.this, result);
                                }
                            }

                        });
                    } else if (isRef == 0) {
                        OrderManager.getInstance().queryOrderDetail(order.getOutTradeNo(), MainApplication.getMchId(), true, new UINotifyListener<Order>() {

                            @Override
                            public void onError(Object object) {
                                super.onError(object);
                                dismissLoading();
                                if (checkSession()) {
                                    return;
                                }
                                if (object != null) {
                                    toastDialog(BillMainActivity.this, object.toString(), null);
                                }
                            }

                            @Override
                            public void onPreExecute() {
                                super.onPreExecute();

                                loadDialog(BillMainActivity.this, getStringById(R.string.public_data_loading));

                            }

                            @Override
                            public void onSucceed(Order result) {

                                super.onSucceed(result);
                                dismissLoading();
                                if (result != null) {
                                    OrderDetailsActivity.startActivity(BillMainActivity.this, result);
                                }
                            }

                        });
                    } else {
                        OrderManager.getInstance().queryVardDetail(order.getCardId(), order.getCardCode(), new UINotifyListener<WxCard>() {

                            @Override
                            public void onError(Object object) {
                                super.onError(object);
                                dismissLoading();
                                if (object != null) {
                                    //                                showToastInfo(object.toString());
                                    toastDialog(BillMainActivity.this, object.toString(), null);
                                }
                            }

                            @Override
                            public void onPreExecute() {
                                super.onPreExecute();

                                loadDialog(BillMainActivity.this, R.string.public_data_loading);
                            }

                            @Override
                            public void onSucceed(WxCard result) {
                                super.onSucceed(result);
                                dismissLoading();
                                if (result != null) {
                                    VerificationDetails.startActivity(BillMainActivity.this, result);
                                }
                            }

                        });
                    }

                }
            }
        });

       /* ly_choice.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    StatService.trackCustomEvent(BillMainActivity.this, "SPConstTapBillFilter", "账单筛选");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
                SelectPicPopupWindow p = new SelectPicPopupWindow(BillMainActivity.this, choiceTypeList, payTypeList, new SelectPicPopupWindow.HandleBtn() {

                    @Override
                    public void handleOkBtn(List<String> choiceType, List<String> payTypeList) {
                        BillMainActivity.this.payTypeList = payTypeList;
                        choiceTypeList = choiceType;
                        orderList.clear();
                        if (choiceType.contains("refund")) {
                            isRef = 1;
                            //                                    loadDate(1, true, true);
                            if (choiceTypeList.size() > 1) {
                                HandlerManager.notifyMessage(HandlerManager.STREAM_BILL, HandlerManager.PAY_FINISH_REFUND);
                            } else {
                                HandlerManager.notifyMessage(HandlerManager.STREAM_BILL, HandlerManager.STREAM_BILL_REFUND);
                            }
                            loadDateTask(1, 1, true, true,getSelectedUserId());
                            dayCountMap.clear();
                            dayTatal.clear();
                        } else if (choiceType.contains("card")) {
                            isRef = 2;
                            loadDateTask(1, 1, true, false,getSelectedUserId());
                            dayCountMap.clear();
                            dayTatal.clear();
                        } else {
                            dayCountMap.clear();
                            dayTatal.clear();
                            HandlerManager.notifyMessage(HandlerManager.STREAM_BILL, HandlerManager.STREAM_BILL);
                            isRef = 0;
                            //                                    loadDate(1, true, false);
                            tradeType.clear();
                            payType.clear();
                            BillOrderManager.getInstance().paseToJson(choiceType, tradeType, payType);

                            loadDateTask(1, 1, true, false,getSelectedUserId());
                        }
                    }
                });
                p.showAtLocation(ly_choice, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0); //设置layout在PopupWindow中显示的位置
            }
        });*/
    }

    private void setListData(final PullToRefreshListView pull, List<Order> list, BillStreamAdapter adapter, List<Order> result, boolean isLoadMore, int page) {
        if (!isLoadMore) {
            //            pull.onfinish(getStringById(R.string.refresh_successfully));// 刷新完成
            //pull.setRefreshingLabel(getStringById(R.string.refresh_successfully));
            //            pull.setFinish(R.string.refresh_successfully);
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // 这里表示刷新处理完成后把上面的加载刷新界面隐藏
                    //                    pull.onRefreshComplete();

                }
            }, 800);

            list.clear();
        } else {
            mHandler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    //                    pull.onLoadMoreComplete();
                    // pull.setMore(true);
                    pull.onRefreshComplete();
                }
            }, 1000);
        }
        list.addAll(result);
        pull.onRefreshComplete();
        adapter.notifyDataSetChanged();
        if (page == 1) {
            listView.setSelection(0);
        }
    }

//    private void mySetListData(final NewPullDownListView pull, List<Order> list, BillStreamAdapter adapter, List<Order> result, boolean isLoadMore) {
//        if (!isLoadMore) {
//            pull.onfinish(getStringById(R.string.refresh_successfully));// 刷新完成
//
//            mHandler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    // 这里表示刷新处理完成后把上面的加载刷新界面隐藏
//                    pull.onRefreshComplete();
//                }
//            }, 800);
//
//            list.clear();
//        } else {
//            mHandler.postDelayed(new Runnable() {
//
//                @Override
//                public void run() {
//                    pull.onLoadMoreComplete();
//                    // pull.setMore(true);
//                }
//            }, 1000);
//        }
//        list.addAll(result);
//        adapter.notifyDataSetChanged();
//
//    }

    private void initView() {

        //ly_but_choice,ly_choice_date;
        tv_time = getViewById(R.id.tv_time);
        ly_but_choice = getViewById(R.id.ly_but_choice);
        ly_choice_date = getViewById(R.id.ly_choice_date);
        ly_but_cachier = getViewById(R.id.ly_but_cachier);
        tv_cashier_name = getViewById(R.id.tv_cashier_name);
        ly_stream = getViewById(R.id.ly_stream);
        //refund_search_input = getViewById(R.id.refund_search_input);
        tv_prompt = getViewById(R.id.tv_prompt);
        tv_prompt.setText(R.string.tv_bill);
        ly_title = getViewById(R.id.ly_title);
        tv_total = getViewById(R.id.tv_total);

        mPullRefreshListView = getViewById(R.id.pullrefresh);
        listView = mPullRefreshListView.getRefreshableView();

        billStreamAdapter = new BillStreamAdapter(orderList);
        listView.setAdapter(billStreamAdapter);
//        ly_choice = getViewById(R.id.ly_choice);
        //tv_date = getViewById(R.id.tv_date);

        OnRefreshListener mOnrefreshListener = new OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadDateTask(1, mPullRefreshListView.getRefreshType(), false, false, getSelectedUserId());
            }
        };
        mPullRefreshListView.setOnRefreshListener(mOnrefreshListener);
        listView.setOnScrollListener(new OnScrollListener() {
            private int lastItemIndex;//当前ListView中最后一个Item的索引  

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                lastItemIndex = firstVisibleItem + visibleItemCount - 1 - 1;

                //                Logger.i("hehui", "lastItemIndex-->" + lastItemIndex);
                // 第一项与第二项标题不同，说明标题需要替换
                //                if (firstVisibleItem < orderList.size())
                //                {
                //                    try
                //                    {
                //                        if (orderList.size() == 0)
                //                        {
                //                            return;
                //                        }
                //                        
                //                        if (isRef == 1)
                //                        {
                //                            //                            Logger.i("hehui",
                //                            //                                "orderList.get(firstVisibleItem - 1).getFormartYYMM()-->"
                //                            //                                    + orderList.get(firstVisibleItem - 1).getFormartYYMM() + ",tv_date.getText()-->"
                //                            //                                    + tv_date.getText());
                //                            if (firstVisibleItem == 1)
                //                            {
                //                                //                                String time =
                //                                //                                    DateUtil.formartDateYYMMDDTo(orderList.get(firstVisibleItem).getAddTimeNew());
                //                                tv_date.setText(orderList.get(firstVisibleItem - 1).getFormartYYMM());
                //                                
                //                                //                                String value = dayTatal.get(orderList.get(firstVisibleItem - 1).getFormatRefund());
                //                                //                                refundTime = orderList.get(firstVisibleItem - 1).getFormatRefund();
                //                                //                                if (!StringUtil.isEmptyOrNull(value))
                //                                //                                {
                //                                //                                    if (choiceTypeList.size() > 1)
                //                                //                                    {
                //                                //                                        tv_total.setText("");
                //                                //                                        
                //                                //                                    }
                //                                //                                    else
                //                                //                                    {
                //                                //                                        tv_total.setText(getString(R.string.tv_refund_title_info)
                //                                //                                            + DateUtil.formatMoneyUtils(Long.parseLong(value)));
                //                                //                                    }
                //                                //                                }
                //                                
                //                            }
                //                            else if (!orderList.get(firstVisibleItem - 1).getFormartYYMM().equals(tv_date.getText()))
                //                            {
                //                                String time = orderList.get(firstVisibleItem - 1).getFormartYYMM();
                //                                tv_date.setText(time);
                //                                
                //                                //                                String value = dayTatal.get(orderList.get(firstVisibleItem - 1).getFormatRefund());
                //                                //                                
                //                                //                                refundTime = orderList.get(firstVisibleItem - 1).getFormatRefund();
                //                                //                                
                //                                //                                if (!StringUtil.isEmptyOrNull(value))
                //                                //                                {
                //                                //                                    if (choiceTypeList.size() > 1)
                //                                //                                    {
                //                                //                                        tv_total.setText("");
                //                                //                                    }
                //                                //                                    else
                //                                //                                    {
                //                                //                                        tv_total.setText(getString(R.string.tv_refund_title_info)
                //                                //                                            + DateUtil.formatMoneyUtils(Long.parseLong(value)));
                //                                //                                    }
                //                                //                                }
                //                                
                //                            }
                //                        }
                //                        else if (isRef == 0)
                //                        {
                //                            if (firstVisibleItem == 1)
                //                            {
                //                                //                                Logger.i("hehui",
                //                                //                                    "orderList.get(firstVisibleItem).getFormartYYMM()-->"
                //                                //                                        + orderList.get(firstVisibleItem).getFormartYYMM() + ",tv_date.getText()-->"
                //                                //                                        + tv_date.getText() + ",firstVisibleItem-->" + firstVisibleItem);
                //                                
                //                                tv_date.setText(orderList.get(firstVisibleItem - 1).getFormartYYMM());
                //                                //                                String date = orderList.get(firstVisibleItem - 1).getFormatTimePay();
                //                                //                                streamTime = date;
                //                                //                                String value = dayTatal.get(date);
                //                                //                                if (!StringUtil.isEmptyOrNull(value) && tradeType.size() == 0 && payType.size() == 0)
                //                                //                                {
                //                                //                                    tv_total.setText(getString(R.string.tx_bill_stream_total)
                //                                //                                        + DateUtil.formatMoneyUtils(Long.parseLong(value)));
                //                                //                                }
                //                                //                                else
                //                                //                                {
                //                                //                                    tv_total.setText("");
                //                                //                                }
                //                                
                //                            }
                //                            else if (!orderList.get(firstVisibleItem - 1).getFormartYYMM().equals(tv_date.getText()))
                //                            {
                //                                //                                Logger.i("hehui", "orderList.get(firstVisibleItem-1).getFormatTimePay()-->"
                //                                //                                    + orderList.get(firstVisibleItem - 1).getFormatTimePay() + ",dayTatal-->"
                //                                //                                    + dayTatal);
                //                                tv_date.setText(orderList.get(firstVisibleItem - 1).getFormartYYMM());
                //                                
                //                                //                                String value = dayTatal.get(orderList.get(firstVisibleItem - 1).getFormatTimePay());
                //                                //                                streamTime = orderList.get(firstVisibleItem - 1).getFormatTimePay();
                //                                //                                if (!StringUtil.isEmptyOrNull(value) && tradeType.size() == 0 && payType.size() == 0)
                //                                //                                {
                //                                //                                    tv_total.setText(getString(R.string.tx_bill_stream_total)
                //                                //                                        + DateUtil.formatMoneyUtils(Long.parseLong(value)));
                //                                //                                }
                //                                //                                else
                //                                //                                {
                //                                //                                    tv_total.setText("");
                //                                //                                }
                //                            }
                //                            //                            else if (!orderList.get(firstVisibleItem + 1).getFormartYYMM().equals(tv_date.getText()))
                //                            //                            {
                //                            //                                tv_date.setText(orderList.get(firstVisibleItem + 1).getFormartYYMM());
                //                            //                                
                //                            //                                String value = dayTatal.get(orderList.get(firstVisibleItem).getFormatTimePay());
                //                            //                                if (!StringUtil.isEmptyOrNull(value))
                //                            //                                {
                //                            //                                    
                //                            //                                    tv_total.setText(getString(R.string.tx_bill_stream_total)
                //                            //                                        + DateUtil.formatMoneyUtils(Long.parseLong(value)));
                //                            //                                }
                //                            //                            }
                //                        }
                //                        else
                //                        {
                //                            //卡券
                //                            if (firstVisibleItem == 1)
                //                            {
                //                                
                //                                tv_date.setText(orderList.get(firstVisibleItem - 1).getFormartYYMM());
                //                                String date = orderList.get(firstVisibleItem - 1).getFromatCard();
                //                                String value = dayTatal.get(date);
                //                                //                                if (!StringUtil.isEmptyOrNull(value))
                //                                //                                {
                //                                //                                    tv_total.setText(getString(R.string.tx_bill_stream_total)
                //                                //                                        + DateUtil.formatMoneyUtils(Long.parseLong(value)));
                //                                tv_total.setText(getString(R.string.pay_hx_tx) + "：" + value);
                //                                
                //                                //                                }
                //                                
                //                            }
                //                            else if (!orderList.get(firstVisibleItem - 1).getFormartYYMM().equals(tv_date.getText()))
                //                            {
                //                                
                //                                tv_date.setText(orderList.get(firstVisibleItem - 1).getFormartYYMM());
                //                                
                //                                String value = dayTatal.get(orderList.get(firstVisibleItem - 1).getFromatCard());
                //                                
                //                                tv_total.setText(getString(R.string.pay_hx_tx) + "：" + value);
                //                            }
                //                        }
                //                        
                //                    }
                //                    catch (Exception e)
                //                    {
                //                        Log.e(TAG,Log.getStackTraceString(e));
                //                    }
                //                    //头条不显示标题
                //                    if (firstVisibleItem == 0)
                //                        ly_title.setVisibility(View.GONE);
                //                    else
                //                        ly_title.setVisibility(View.VISIBLE);
                //                }

            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (lastItemIndex >= billStreamAdapter.getCount() - 2) {
                    loadDateTask(1, 3, true, false, getSelectedUserId());
                }

            }

        });
        //        listView.setOnScrollListener(billStreamAdapter);
    }

    @Override
    protected void setupTitleBar() {
        // TODO Auto-generated method stub
        super.setupTitleBar();

        titleBar.setTitle(R.string.tv_tab_bill);
        //        titleBar.setLeftButtonImage(R.drawable.icon_general_bill_search);

        titleBar.setLeftButtonResVisible(true, R.drawable.icon_general_bill_search);
        //        if (MainApplication.isAdmin.equals("0") && MainApplication.isTotalAuth == 0)
        //        {//没有统计查看
        //            titleBar.setRightButLayVisibleForTotal(false, getString(R.string.tx_sum));
        //        }
        //        else
        //        {
        //        }
        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.tx_sum));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                showPage(OrderSearchActivity.class);
            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButLayClick() {
                //流水
                showPage(ReportActivity.class);

            }
        });
    }

    //private String money;

    private class BillStreamAdapter extends BaseAdapter //implements OnScrollListener, PinnedHeaderAdapter
    {

        private List<Order> orders;

        public BillStreamAdapter() {
        }

        public BillStreamAdapter(List<Order> orders) {
            this.orders = orders;
        }

        @Override
        public int getCount() {
            return orders.size();
        }

        @Override
        public Object getItem(int position) {
            return orders.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(BillMainActivity.this, R.layout.activity_bill_stream_list_item, null);
                holder = new ViewHolder();
                holder.tv_pay_type = (TextView) convertView.findViewById(R.id.tv_pay_type);
                holder.tv_time = (TextView) convertView.findViewById(R.id.tv_time);
                holder.tv_money = (TextView) convertView.findViewById(R.id.tv_money);
                holder.tv_state = (TextView) convertView.findViewById(R.id.tv_state);
                holder.tv_total = (TextView) convertView.findViewById(R.id.tv_total);
                holder.tv_date = (TextView) convertView.findViewById(R.id.tv_date);
                holder.iv_type = (ImageView) convertView.findViewById(R.id.iv_type);
                holder.ly_title = (RelativeLayout) convertView.findViewById(R.id.ly_title);
                holder.v_iv = (View) convertView.findViewById(R.id.v_iv);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            Order order = orderList.get(position);
            if (null != order) {
                if (order.getMoney() > 0) {
                    holder.tv_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(order.getMoney()));
                }
                holder.tv_money.setVisibility(View.VISIBLE);
                if (isRef == 1) {
                    String langeage = PreferenceUtil.getString("language", "");
                    if (langeage.equals("en_us")) {
                        holder.tv_pay_type.setText(MainApplication.getPayTypeMap().get(String.valueOf(order.getApiProvider())) + " " + getString(R.string.title_order_refund));
                    } else {
                        holder.tv_pay_type.setText(MainApplication.getPayTypeMap().get(String.valueOf(order.getApiProvider())) + getString(R.string.title_order_refund));
                    }

                    try {
                        holder.tv_time.setText(order.getAddTimeNew());
                        if (isMove(position)) {
                            holder.v_iv.setVisibility(View.GONE);
                        } else {
                            holder.v_iv.setVisibility(View.VISIBLE);
                        }

                        holder.tv_state.setText(MainApplication.getRefundStateMap().get(order.getRefundState() + ""));
                        switch (order.getRefundState()) {
                            case 0:
                                //支付成功
                                //                                holder.tv_state.setTextColor(Color.RED);
                                holder.tv_state.setTextColor(Color.parseColor("#bfbfbf"));
                                holder.tv_money.setTextColor(Color.parseColor("#bfbfbf"));
                                break;
                            case 1: //未支付
                            case 3: //关闭
                                holder.tv_state.setTextColor(Color.RED);
                                holder.tv_money.setTextColor(Color.parseColor("#000000"));
                                // holder.tv_state.setText(R.string.refund_accepted);
                                break;
                            case 2: //退款失败
                                holder.tv_state.setText(R.string.refund_failure);
                                holder.tv_money.setTextColor(Color.parseColor("#000000"));
                                // holder.tv_state.setTextColor(getResources().getColor(R.color.bill_item_refund));
                                break;
                        }
                    } catch (Exception e) {
                        Log.e(TAG, Log.getStackTraceString(e));
                    }
                } else if (isRef == 0) {
                    holder.tv_pay_type.setText(MainApplication.getPayTypeMap().get(String.valueOf(order.getApiProvider())));

                    if (!StringUtil.isEmptyOrNull(order.getTradeTimeNew())) {
                        try {
                            holder.tv_time.setText(order.getTradeTimeNew());
                            if (isMove(position)) {
                                holder.v_iv.setVisibility(View.GONE);
                            } else {
                                holder.v_iv.setVisibility(View.VISIBLE);
                            }

                            holder.tv_state.setText(MainApplication.getTradeTypeMap().get(order.getTradeState() + ""));
                            switch (order.getTradeState()) {
                                case 2:
                                    //支付成功
                                    holder.tv_state.setTextColor(getResources().getColor(R.color.bill_item_succ));
                                    holder.tv_money.setTextColor(Color.parseColor("#000000"));
                                    break;
                                case 1: //未支付
                                    holder.tv_state.setTextColor(Color.parseColor("#bfbfbf"));
                                    holder.tv_money.setTextColor(Color.parseColor("#bfbfbf"));
                                    break;
                                case 3: //关闭
                                    //                                    holder.tv_state.setTextColor(getResources().getColor(R.color.bill_item_fail));
                                    //                                    holder.tv_money.setTextColor(Color.parseColor("#000000"));
                                    holder.tv_state.setTextColor(Color.parseColor("#bfbfbf"));
                                    holder.tv_money.setTextColor(Color.parseColor("#bfbfbf"));
                                    break;
                                case 4: //未支付
                                    holder.tv_state.setTextColor(getResources().getColor(R.color.bill_item_refund));
                                    holder.tv_money.setTextColor(Color.parseColor("#000000"));
                                    break;
                                case 8: //未支付
                                    //                                    holder.tv_state.setTextColor(getResources().getColor(R.color.bill_item_fail));
                                    //                                    holder.tv_money.setTextColor(Color.parseColor("#000000"));
                                    holder.tv_state.setTextColor(Color.parseColor("#bfbfbf"));
                                    holder.tv_money.setTextColor(Color.parseColor("#bfbfbf"));
                                    break;
                            }

                        } catch (Exception e) {
                            Log.e(TAG, Log.getStackTraceString(e));
                        }
                    }
                } else {
                    holder.tv_time.setText(order.getUseTimeNew());
                    holder.tv_pay_type.setText(order.getTitle());
                    holder.tv_money.setVisibility(View.INVISIBLE);
                    holder.iv_type.setImageResource(R.drawable.icon_pop_coupon);
                    //String value = dayCountMap.get(order.getFromatCard());
                    if (isMove(position)) {
                        holder.v_iv.setVisibility(View.GONE);
                    } else {
                        holder.v_iv.setVisibility(View.VISIBLE);
                    }
                    //卡券
                    holder.tv_state.setText(R.string.tx_affirm_succ);
                    holder.tv_state.setTextColor(Color.parseColor("#666666"));
                }

                if (isRef == 0 || isRef == 1) {

                    Object object = SharedPreUtile.readProduct("payTypeIcon" + ApiConstant.bankCode + MainApplication.getMchId());
                    if (object != null) {
                        try {

                            Map<String, String> typePicMap = (Map<String, String>) object;
                            if (typePicMap != null && typePicMap.size() > 0) {
                                String picUrl = typePicMap.get(order.getApiProvider() + "");
                                if (!StringUtil.isEmptyOrNull(picUrl)) {
                                    Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.icon_general_receivables);
                                    if (bitmap != null) {
                                        MainApplication.finalBitmap.display(holder.iv_type, picUrl, bitmap);
                                    } else {
                                        MainApplication.finalBitmap.display(holder.iv_type, picUrl);
                                    }
                                } else {
                                    holder.iv_type.setImageResource(R.drawable.icon_general_receivables);
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, Log.getStackTraceString(e));
                        }
                    } else {

                        switch (order.getApiProvider()) {
                            case 1: //微信
                                holder.iv_type.setImageResource(R.drawable.icon_general_wechat);
                                break;
                            case 2: //支付宝
                                holder.iv_type.setImageResource(R.drawable.icon_general_pay);
                                break;
                            case 12: //京东
                                holder.iv_type.setImageResource(R.drawable.icon_general_jingdong);
                                break;
                            case 4: //qq
                                holder.iv_type.setImageResource(R.drawable.icon_general_qq);
                                break;
                            default:
                                holder.iv_type.setImageResource(R.drawable.icon_general_receivables);
                                break;
                        }
                    }
                }

            }
            return convertView;
        }
    }

//    private boolean needTitle(int position) {
//        if (position == 0) {
//            return true;
//        }
//
//        if (position < 0) {
//            return false;
//        }
//
//        Order order = orderList.get(position);
//        Order order1 = orderList.get(position - 1);
//
//        if (order == null || order1 == null) {
//            return false;
//        }
//
//        try {
//            if (isRef == 0) {
//
//                String currentTitle = order.getFormatTimePay();
//                String nextTitle = order1.getFormatTimePay();
//                if (currentTitle == null || nextTitle == null) {
//                    return false;
//                }
//                if (currentTitle.equalsIgnoreCase(nextTitle))//|| titleList.contains(currentTitle)
//                {
//                    return false;
//                }
//            } else if (isRef == 1) { //退款
//
//                String currentTitle = order.getFormatRefund();
//                String nextTitle = order1.getFormatRefund();
//                if (currentTitle == null || nextTitle == null) {
//                    return false;
//                }
//
//                if (currentTitle.equalsIgnoreCase(nextTitle)) {
//                    return false;
//                }
//            } else { //卡券
//                String currentTitle = order.getFromatCard();
//                String nextTitle = order1.getFromatCard();
//                if (currentTitle == null || nextTitle == null) {
//                    return false;
//                }
//
//                if (currentTitle.equalsIgnoreCase(nextTitle)) {
//                    return false;
//                }
//            }
//        } catch (Exception e) {
//            Log.e(TAG,Log.getStackTraceString(e));
//        }
//
//        return true;
//    }

    private boolean isMove(int position) {
        // 获取当前与下一项
        //        ItemEntity currentEntity = (ItemEntity)getItem(position);
        //          ItemEntity nextEntity = (ItemEntity)getItem(position + 1);
        try {
            Order order = orderList.get(position);
            Order order1 = orderList.get(position + 1);

            if (null == order || null == order1) {
                return true;
            }

            if (isRef == 0) {
                // 获取两项header内容
                //                String currentTitle = DateUtil.formartDateYYMMDD(order.getTradeTimeNew());
                //                String nextTitle = DateUtil.formartDateYYMMDD(order1.getTradeTimeNew());

                String currentTitle = order.getFormatTimePay();
                String nextTitle = order1.getFormatTimePay();
                if (null == currentTitle || null == nextTitle) {
                    return false;
                }

                // 当前不等于下一项header，当前项需要移动了
                if (!currentTitle.equals(nextTitle)) {
                    return true;
                }

            } else if (isRef == 1) { //退款
                String currentTitle = order.getFormatRefund();
                String nextTitle = order1.getFormatRefund();
                if (currentTitle == null || nextTitle == null) {
                    return false;
                }

                if (!currentTitle.equalsIgnoreCase(nextTitle)) {
                    return true;
                }
            } else {
                String currentTitle = order.getFromatCard();
                String nextTitle = order1.getFromatCard();
                if (currentTitle == null || nextTitle == null) {
                    return false;
                }

                if (!currentTitle.equalsIgnoreCase(nextTitle)) {
                    return false;
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
            return true;
        }

        return false;
    }

    private class ViewHolder {
        private TextView tv_total, tv_time, tv_pay_type, tv_money, tv_state, tv_date;

        private ImageView iv_type;

        private RelativeLayout ly_title;

        private View v_iv;
    }

    @Override
    public void onLoadMoreDate() {

    }

    @Override
    public void onRefresh() {
        Logger.i("hehui", "onRefresh()");
        pageFulfil = 1;
        if (isMoerRefresh) {
            loadDate(pageFulfil, false, isRef, time, getSelectedUserId());
        }
    }

    @Override
    public void onLoadMore() {
        Logger.i("hehui", "onLoadMore()");
        pageFulfil = pageFulfil + 1;
        loadDate(pageFulfil, true, isRef, time, getSelectedUserId());
    }

    public String getSelectedUserId() {
        String userId = null;
        if (tv_cashier_name != null && userModel != null) {
            if (!StringUtil.isEmptyOrNull(tv_cashier_name.getText().toString())
                    && !tv_cashier_name.getText().equals(getStringById(R.string.tx_bill_stream_cashier))) {
                userId = String.valueOf(userModel.getId());
            }
        }
        return userId;
    }
}
