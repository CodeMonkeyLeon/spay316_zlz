package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by congwei.li on 2022/4/24.
 *
 * @Description:
 */
public class UserInfo implements Serializable {

    /**
     * ACCOUNT_TYPE_RECEIPT  收银员
     * ACCOUNT_TYPE_ADMIN    管理员
     */
    public static final int ACCOUNT_TYPE_RECEIPT = 0;
    public static final int ACCOUNT_TYPE_ADMIN = 1;

    //{
    //	"id": "30000662",
    //	"empId": "",
    //	"username": "100580000055",
    //	"realname": "100580000055",
    //	"deptname": "",
    //	"createdAt": "",
    //	"remark": "1",
    //	"isAdmin": 1,
    //	"enabled": true,
    //	"mchId": "100580000055",
    //	"isSysAdmin": "",
    //	"isRefundAuth": "1",
    //	"isOrderAuth": "1",
    //	"isTotalAuth": "1",
    //	"isActivityAuth": "1",
    //	"isUnfreezeAuth": "1",
    //	"showEwallet": 0,
    //	"mchName": "yinliantest01",
    //	"mchLogo": "",
    //	"tradeName": "",
    //	"serviceType": "pay.hcpay.jspay|pay.hase.fps.native|pay.weixin.wallet.native.intl|pay.weixin.micropay|pay.upi.native.intl|pay.upi.micropay.intl|pay.weixin.native.intl|pay.weixin.micropay|pay.weixin.jspay|pay.alipay.auth.native.freeze|pay.upi.native.intl|pay.upi.micropay.intl|pay.alipay.auth.micropay.freeze|pay.swallet.micropay.intl|pay.swallet.native.intl|pay.swallet.jspay.intl|pay.bochk.fps.native|pay.alipay.app.intl|pay.weixin.micropay|pay.weixin.native.intl|pay.weixin.raw.app|pay.equicom.jspay.intl|pay.equicom.micropay.intl|pay.equicom.native.intl|pay.paypay.micropay|pay.paypay.native|pay.weixin.jspay|pay.santander.jspay.intl|pay.santander.micropay.intl|pay.santander.native.intl|pay.mpgs.tokenization|pay.mpgs.3ds.check|pay.mpgs.card|pay.alipay.jspay|pay.alipay.native.intl|pay.alipay.micropay|pay.alipay.webpay.intl|pay.alipay.wappay.intl|pay.instapay.native.v2|pay.instapay.native",
    //	"activateServiceType": "pay.upi.native.intl|pay.upi.micropay.intl|pay.weixin.native.intl|pay.weixin.micropay|pay.weixin.jspay|pay.alipay.auth.native.freeze|pay.swallet.micropay.intl|pay.swallet.native.intl|pay.swallet.jspay.intl|pay.equicom.jspay.intl|pay.equicom.micropay.intl|pay.equicom.native.intl|pay.santander.jspay.intl|pay.santander.micropay.intl|pay.santander.native.intl|pay.mpgs.tokenization|pay.mpgs.3ds.check|pay.mpgs.card|pay.alipay.jspay|pay.alipay.native.intl|pay.alipay.micropay|pay.alipay.webpay.intl|pay.alipay.wappay.intl|pay.instapay.native.v2|pay.instapay.native",
    //	"activateCardServiceType": "",
    //	"cardServiceType": "",
    //	"authServiceType": "",
    //	"isAuthFreezeOpen": 1,
    //	"signKey": "bd8a83a4702eb94a2d880239b292f7bb",
    //	"cookieMap": {
    //		"SAUTHID": "30000662",
    //		"SKEY": "spay3_session_pre_1ccdc7a1-adb0-4d41-bb9b-73b767567850"
    //	},
    //	"appId": "",
    //	"machineId": "",
    //	"roleType": 8,
    //	"delete": false,
    //	"bankCode": "intl_and",
    //	"feeType": "MOP",
    //	"feeFh": "MOP$",
    //	"merchantShortName": "yinliantest01",
    //	"themeMd5": "",
    //	"apiShowListMd5": "05d6c5ef423267ef3eb2cefcb175615d",
    //	"apiProviderMap": {
    //		"1": "微信",
    //		"45": "MPGS",
    //		"2": "支付宝",
    //		"37": "Equicom",
    //		"40": "Santander",
    //		"52": "Instapay",
    //		"30": "Swallet",
    //		"20": "银联",
    //		"54": "Instapay TP2M"
    //	},
    //	"refundStateMap": {
    //		"0": "审核中",
    //		"1": "退款成功",
    //		"2": "退款失败",
    //		"3": "退款成功",
    //		"5": "退款成功"
    //	},
    //	"tradeStateMap": {
    //		"1": "未支付",
    //		"2": "收款成功",
    //		"3": "已关闭",
    //		"4": "转入退款",
    //		"7": "冲正中",
    //		"8": "已撤销",
    //		"9": "已撤销"
    //	},
    //	"payTypeMap": {
    //		"44": "NambaPay",
    //		"25": "银联二维码",
    //		"47": "XNAP & Other e-Wallets",
    //		"26": "转数快",
    //		"29": "GCash",
    //		"999": "未知",
    //		"51": "Tiqmo",
    //		"30": "Swallet",
    //		"52": "Bank Payment/Instapay",
    //		"32": "HomeCredit",
    //		"54": "QR Ph",
    //		"55": "Alipay+",
    //		"56": "WeChat Pay",
    //		"35": "微信支付",
    //		"57": "ShopeePay",
    //		"36": "轻钱包",
    //		"58": "MasterCard",
    //		"37": "Equicom",
    //		"38": "GrabPay",
    //		"39": "Kabu",
    //		"18": "微信支付香港钱包",
    //		"1": "微信支付",
    //		"2": "支付宝",
    //		"40": "Santander",
    //		"62": "Instapay",
    //		"41": "HelloMoney",
    //		"63": "GCash",
    //		"20": "银联二维码",
    //		"42": "Mpay",
    //		"65": "PayMe"
    //	},
    //	"cardTradeTypeMap": {
    //		"11": "预授权撤销冲正",
    //		"12": "预授权完成冲正",
    //		"13": "预授权完成撤销冲正",
    //		"1": "消费",
    //		"2": "消费撤销",
    //		"3": "预授权",
    //		"4": "预授权撤销",
    //		"5": "退款",
    //		"6": "预授权完成",
    //		"7": "预授权完成撤销",
    //		"8": "消费冲正",
    //		"9": "消费撤销冲正",
    //		"10": "预授权冲正"
    //	},
    //	"isSurchargeOpen": 1,
    //	"isTaxRateOpen": 1,
    //	"isTipOpen": 0,
    //	"tipOpenFlag": 1,
    //	"email": "",
    //	"timeZone": "",
    //	"rate": "0.000000000000",
    //	"isFixCode": 0,
    //	"rateTime": "",
    //	"surchargeRate": "0.005000000000",
    //	"taxRate": "0.020000000000",
    //	"vatRate": "0.000000000000",
    //	"numFixed": 2,
    //	"usdToRmbExchangeRate": "6.550559000000",
    //	"sourceToUsdExchangeRate": "0.123685837972",
    //	"isCardOpen": 0,
    //	"alipayPayRate": "0.862400000000",
    //	"token": "",
    //	"isDefault": "",
    //	"refundLimit": "",
    //	"passWord": "",
    //	"paymentLinkPermsJson": ""
    //}

    public String id;
    public String empId;
    public String username;
    public String realname;
    public String deptname;
    public String createdAt;
    public String remark;
    public String phone;
    /**
     * 0 : 收银员    1 : 管理员
     */
    public int isAdmin;
    public boolean enabled;
    public String mchId;
    public String isSysAdmin;
    public String isRefundAuth;
    public String isOrderAuth;
    public String isTotalAuth;
    public String isActivityAuth;
    public String isUnfreezeAuth;
    public String showEwallet;
    public String mchName;
    public String channelId;
    public String mchLogo;
    public String tradeName;
    public String serviceType;
    public String activateServiceType;
    public String activateCardServiceType;//已开通的卡支付类型---V3.1.0 版本新增卡交易通道
    public String cardServiceType;//V3.1.0 版本新增卡交易通道---卡支付类型
    public String authServiceType;
    public int isAuthFreezeOpen;
    public String signKey;
    public String appId;
    public String machineId;
    public int roleType;
    public boolean delete;
    public String bankCode;
    public String feeType; // 货币类型
    public String feeFh;// 货币符号
    public String merchantShortName;
    public String themeMd5;//是否有需要版本定制化
    public String apiShowListMd5;
    public HashMap<String, String> apiProviderMap;
    public HashMap<String, String> refundStateMap;
    public HashMap<String, String> tradeStateMap;
    public HashMap<String, String> payTypeMap;
    public HashMap<String, String> cardTradeTypeMap;

    public int isSurchargeOpen;
    public int isTaxRateOpen;
    public int isTipOpen;
    public int tipOpenFlag;
    public String email;
    public String timeZone;
    public String rate;
    public int isFixCode;   //固定二维码是否为聚合 0为聚合，1为不聚合
    public String rateTime;
    public String surchargeRate;
    public String taxRate;
    public String vatRate;
    public int numFixed; //获取当前币种的最小单位，即小数点的位数，获取不到默认是两位
    public String usdToRmbExchangeRate;
    public String sourceToUsdExchangeRate;
    public int isCardOpen;  //是否开启电子卡：0未开启，1开启
    public String alipayPayRate;
    public String token;
    public String isDefault;
    public String refundLimit;
    public String passWord;
    public String paymentLinkPermsJson;


    public UserInfo() {
        this.id = "";
        this.empId = "";
        this.username = "";
        this.realname = "";
        this.deptname = "";
        this.createdAt = "";
        this.remark = "";
        this.phone = "";
        this.isAdmin = 0;
        this.enabled = false;
        this.mchId = "";
        this.isSysAdmin = "";
        this.isRefundAuth = "";
        this.isOrderAuth = "";
        this.isTotalAuth = "";
        this.isActivityAuth = "";
        this.isUnfreezeAuth = "";
        this.showEwallet = "";
        this.mchName = "";
        this.channelId = "";
        this.mchLogo = "";
        this.tradeName = "";
        this.serviceType = "";
        this.activateServiceType = "";
        this.activateCardServiceType = "";
        this.cardServiceType = "";
        this.authServiceType = "";
        this.isAuthFreezeOpen = 0;
        this.signKey = "";
        this.appId = "";
        this.machineId = "";
        this.roleType = 0;
        this.delete = false;
        this.bankCode = "";
        this.feeType = "";
        this.feeFh = "";
        this.merchantShortName = "";
        this.themeMd5 = "";
        this.apiShowListMd5 = "";
        this.apiProviderMap = new HashMap<>();
        this.refundStateMap = new HashMap<>();
        this.tradeStateMap = new HashMap<>();
        this.payTypeMap = new HashMap<>();
        this.cardTradeTypeMap = new HashMap<>();
        this.isSurchargeOpen = 0;
        this.isTaxRateOpen = 0;
        this.isTipOpen = 0;
        this.tipOpenFlag = 0;
        this.email = "";
        this.timeZone = "";
        this.rate = "";
        this.isFixCode = 0;
        this.rateTime = "";
        this.surchargeRate = "";
        this.taxRate = "";
        this.vatRate = "";
        this.numFixed = 0;
        this.usdToRmbExchangeRate = "";
        this.sourceToUsdExchangeRate = "";
        this.isCardOpen = 0;
        this.alipayPayRate = "";
        this.token = "";
        this.isDefault = "";
        this.refundLimit = "";
        this.passWord = "";
        this.paymentLinkPermsJson = "";
    }
}
