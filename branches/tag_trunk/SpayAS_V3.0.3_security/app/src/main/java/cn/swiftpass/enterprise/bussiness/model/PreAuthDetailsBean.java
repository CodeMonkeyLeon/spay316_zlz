package cn.swiftpass.enterprise.bussiness.model;

public class PreAuthDetailsBean {
        private String title;
        private String title_msg;
        private String msg;


    public PreAuthDetailsBean() {
    }

    public PreAuthDetailsBean(String title, String title_msg, String msg) {
        this.title = title;
        this.title_msg = title_msg;
        this.msg = msg;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle_msg() {
        return title_msg;
    }

    public void setTitle_msg(String title_msg) {
        this.title_msg = title_msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
