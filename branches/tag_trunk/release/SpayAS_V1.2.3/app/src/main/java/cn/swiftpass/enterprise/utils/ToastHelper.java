package cn.swiftpass.enterprise.utils;

import android.app.Activity;
import android.widget.Toast;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;

public class ToastHelper
{
    
    public static String toStr(int resId)
    {
        return MainApplication.getContext().getString(resId);
    }
    
    public static void showInfo(final Activity activity, final String msg)
    {
        
        activity.runOnUiThread(new Runnable()
        {
            
            @Override
            public void run()
            {
                
                NewDialogInfo info = new NewDialogInfo(activity, msg, null, 2, null, null);
                
                DialogHelper.resize(activity, info);
                
                info.show();
            }
        });
    }

    public static void showToastUIThread(final Activity activity, final String msg){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainApplication.getContext(), msg, Toast.LENGTH_SHORT).show();
            }
        });

    }
    
    public static void showInfo(String msg)
    {
        Toast.makeText(MainApplication.getContext(), msg, Toast.LENGTH_SHORT).show();
        
        //        MainApplication.getContext().runOnUiThread(new Runnable()
        //        {
        //            
        //            @Override
        //            public void run()
        //            {
        
        //        NewDialogInfo info = new NewDialogInfo(MainApplication.getContext(), msg, null, 2, null, null);
        //                    {
        //                        
        //                        @Override
        //                        public void handleOkBtn()
        //                        {
        //                        }
        //                        
        //                    });
        
        //        DialogHelper.resize(MainApplication.getContext(), info);
        //        
        //        info.show();
        //            }
        //        });
    }
    
    public static void showError(String msg)
    {
        Toast.makeText(MainApplication.getContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
