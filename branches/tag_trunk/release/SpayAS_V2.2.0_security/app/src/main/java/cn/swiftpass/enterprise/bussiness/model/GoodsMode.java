package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-10-27
 * Time: 下午8:04
 * To change this template use File | Settings | File Templates.
 */
public class GoodsMode implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    private String body;
    
    private String status;
    
    private String tmpName;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    private String count;
    
    /**
     * @return 返回 tmpName
     */
    public String getTmpName()
    {
        return tmpName;
    }
    
    /**
     * @param 对tmpName进行赋值
     */
    public void setTmpName(String tmpName)
    {
        this.tmpName = tmpName;
    }
    
    /**
     * @return 返回 body
     */
    public String getBody()
    {
        return body;
    }
    
    /**
     * @param 对body进行赋值
     */
    public void setBody(String body)
    {
        this.body = body;
    }
    
    /**
     * @return 返回 status
     */
    public String getStatus()
    {
        return status;
    }
    
    /**
     * @param 对status进行赋值
     */
    public void setStatus(String status)
    {
        this.status = status;
    }
    
}
