/*
 * 文 件 名:  ShopBaseDataDB.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2014-3-20
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.io.database.access;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.util.Log;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.ShopBaseDataInfo;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;

/**
 * 基本行业 数据 db操作
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2014-3-20]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class ShopBaseDataDB
{
    
    private static Dao<ShopBaseDataInfo, Integer> userDao;
    
    private static ShopBaseDataDB instance = null;
    
    public static ShopBaseDataDB getInstance()
    {
        if (instance == null)
        {
            instance = new ShopBaseDataDB();
        }
        
        return instance;
    }
    
    public ShopBaseDataDB()
    {
        try
        {
            userDao = MainApplication.getContext().getHelper().getDao(ShopBaseDataInfo.class);
        }
        catch (SQLException e)
        {
            Log.e("hehui", "ShopBaseDataDB-->" + e.getMessage());
        }
    }
    
    /**
     * 保存 商户行业数据
     * <功能详细描述>
     * @param userModel
     * @throws SQLException
     * @see [类、类#方法、类#成员]
     */
    public void save(ShopBaseDataInfo info)
        throws SQLException
    {
        userDao.create(info);
    }
    
    //    /**
    //     * 查询父类下子节点
    //     * <功能详细描述>
    //     * @return
    //     * @see [类、类#方法、类#成员]
    //     */
    //    public List<ShopBaseDataInfo> querySonData(String merchantTypeId)
    //    {
    //        String sql = "select * from t_shop_baseData where parentId ='" + merchantTypeId + "'";
    //        List<ShopBaseDataInfo> list = new ArrayList<ShopBaseDataInfo>();
    //        try
    //        {
    //            
    //            GenericRawResults<String[]> s = userDao.queryRaw(sql);
    //            List<String[]> listStr = s.getResults();
    //            for (int i = 0; i < listStr.size(); i++)
    //            {
    //                ShopBaseDataInfo info = new ShopBaseDataInfo();
    //                
    //                String[] str = listStr.get(i);
    //                
    //                info.merchantTypeId = str[0];
    //                info.typeName = str[1];
    //                info.parentId = str[2];
    //                
    //                list.add(info);
    //            }
    //            
    //        }
    //        catch (SQLException e)
    //        {
    //            Log.e("hehui", "query failed " + e.getMessage());
    //            return list;
    //        }
    //        return list;
    //    }
    
    /**
     * 根据父类id查询
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public List<ShopBaseDataInfo> queryByParentID(String parentId)
    {
        String sql = "select * from t_shop_baseData where merchantTypeId ='" + parentId + "'";
        List<ShopBaseDataInfo> list = new ArrayList<ShopBaseDataInfo>();
        try
        {
            
            GenericRawResults<String[]> s = userDao.queryRaw(sql);
            List<String[]> listStr = s.getResults();
            for (int i = 0; i < listStr.size(); i++)
            {
                ShopBaseDataInfo info = new ShopBaseDataInfo();
                
                String[] str = listStr.get(i);
                
                //                info.merchantTypeId = str[0];
                //                info.typeName = str[1];
                //                info.parentId = str[2];
                
                list.add(info);
            }
            
        }
        catch (SQLException e)
        {
            Log.e("hehui", "query failed " + e.getMessage());
            return list;
        }
        
        return list;
    }
    
    /**
     * 根据子类查询父类
     */
    public ShopBaseDataInfo queryBySonId(String sonId)
    {
        
        String sql =
            "select * from t_shop_baseData where merchantTypeId=(select parentId from t_shop_baseData where merchantTypeId='"
                + sonId + "')";
        ShopBaseDataInfo info = new ShopBaseDataInfo();
        try
        {
            GenericRawResults<String[]> s = userDao.queryRaw(sql);
            List<String[]> listStr = s.getResults();
            if (listStr.size() > 0)
            {
                //                String[] str = listStr.get(0);
                //                info.merchantTypeId = str[0];
                //                info.typeName = str[1];
                //                info.parentId = str[2];
            }
            
        }
        catch (SQLException e)
        {
            Log.e("hehui", "query failed " + e.getMessage());
            return info;
        }
        
        return info;
        
    }
    
    /**
     * 查询父类
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public List<ShopBaseDataInfo> query()
    {
        String sql = "select * from t_shop_baseData";
        List<ShopBaseDataInfo> list = new ArrayList<ShopBaseDataInfo>();
        try
        {
            
            GenericRawResults<String[]> s = userDao.queryRaw(sql);
            List<String[]> listStr = s.getResults();
            for (int i = 0; i < listStr.size(); i++)
            {
                ShopBaseDataInfo info = new ShopBaseDataInfo();
                
                String[] str = listStr.get(i);
                
                info.name = str[0];
                info.typeno = str[1];
                //                info.parentId = str[2];
                
                list.add(info);
            }
            
        }
        catch (SQLException e)
        {
            return list;
        }
        
        return list;
    }
}
