package cn.swiftpass.enterprise.io.database.table;

import android.database.sqlite.SQLiteDatabase;
import cn.swiftpass.enterprise.utils.Logger;

/**
 *
 * User: Administrator
 * Date: 13-9-23
 * Time: 上午11:29
 * To change this template use File | Settings | File Templates.
 */
public class OrderTable extends TableBase {

    public static final String COLUMN_ORDER_NO="order_no";
    public static final String TABLE_NAME="tb_order";
    public static final String COLUMN_M_ID="m_id";
    public static final String COLUMN_U_ID="u_id";
    public static final String COLUMN_PRODUCT="product";
    public static final String COLUMN_MONEY="money";
    public static final String COLUMN_STATE="state";
    public static final String COLUMN_FINISH_TIME="finsh_time";
    public static final String COLUMN_ADD_TIME="add_time";
    public static final String COLUMN_NOTIFY_TIME="notify_time";
    public static final String COLUMN_USER_NAME="userName";
    public static final String COLUMN_CLIENT_TYPE="clientType";
    public static final String COLUMN_REMARK="remark";
    public static final String COLUMN_BANK_NAME="bank_type";
    public static final String COLUMN_TRANSACTIONID="transaction_Id";
    public static final String COLUMN_CREATE_ORDER_TYPE = "create_order_type";
    @Override
    public void createTable(SQLiteDatabase db) {
        final String SQL_CREATE_TABLE = "create table "+TABLE_NAME+"("
                + COLUMN_ID + " INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT  ," +
                COLUMN_ORDER_NO + " VARCHAR(32)," +
                COLUMN_M_ID + " VARCHAR(15)," +
                COLUMN_U_ID  + " long," +
                COLUMN_PRODUCT + " VARCHAR(20), " +
                COLUMN_REMARK + " VARCHAR(200), " +
                COLUMN_STATE + "  INT, " +
                COLUMN_FINISH_TIME  + "  long, " +
                COLUMN_NOTIFY_TIME  + "  long, " +
                COLUMN_MONEY  + "  int, " +
                COLUMN_CLIENT_TYPE + "  VARCHAR(30), " +
                COLUMN_USER_NAME + "  VARCHAR(30), " +
                COLUMN_BANK_NAME + "  VARCHAR(30), " +
                COLUMN_TRANSACTIONID + "  VARCHAR(35), " +
                COLUMN_ADD_TIME + " LONG,  " +
                COLUMN_CREATE_ORDER_TYPE + " int"+
                ")";
        Logger.i("OrderTable", SQL_CREATE_TABLE);
        db.execSQL(SQL_CREATE_TABLE);
    }
}
