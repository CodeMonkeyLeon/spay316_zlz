package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created by aijingya on 2018/5/28.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/5/28.15:58.
 */

public class ChannelRate  implements Serializable {

    public int payCenterId;  //1,
    public String centerName;   //微信境外通道（跨境通）
    public int payRate;  //12
    public int isClose; //0
}
