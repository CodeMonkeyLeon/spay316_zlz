package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.CustAndProdEditContract
import cn.swiftpass.enterprise.ui.paymentlink.model.ImageUrl
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkCustomer
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkProduct
import cn.swiftpass.enterprise.utils.JsonUtil
import com.example.common.entity.EmptyData
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/6
 *
 * @月光还是少年的月光
 * @九州一色还是李白的霜
 */
class CustAndProdEditPresenter : CustAndProdEditContract.Presenter {

    private var mView: CustAndProdEditContract.View? = null


    override fun uploadImg(
        imgBase64: String?,
        imgType: String?
    ) {

        mView?.let {
            it.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
        }


        AppClient.uploadImg(
            imgType,
            imgBase64,
            System.currentTimeMillis().toString(),
            object : CallBackUtil.CallBackCommonResponse<ImageUrl>(ImageUrl()) {


                override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                    mView?.let { view ->
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { error ->
                            view.uploadImgFailed(error.message)
                        }
                    }
                }


                override fun onResponse(res: CommonResponse?) {
                    mView?.let { view ->
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        res?.let { response ->
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                ImageUrl::class.java
                            ) as ImageUrl
                            view.uploadImgSuccess(order)
                        }
                    }
                }
            }
        )


    }


    override fun editCustomerDetail(c: PaymentLinkCustomer?) {
        mView?.let { view ->
            c?.let { customer ->
                view.showLoading(
                    R.string.public_data_loading,
                    ParamsConstants.COMMON_LOADING
                )

                AppClient.editCustomerDetail(
                    "3",
                    customer.custId,
                    customer.custName,
                    customer.custMobile,
                    customer.custEmail,
                    System.currentTimeMillis().toString(),
                    object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {
                        override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            commonResponse?.let {
                                view.editCustomerDetailFailed(it.message)
                            }
                        }

                        override fun onResponse(response: CommonResponse?) {
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            response?.let {
                                view.editCustomerDetailSuccess(it.message)
                            }
                        }
                    }
                )

            }
        }
    }

    override fun addNewCustomer(c: PaymentLinkCustomer?) {
        mView?.let { view ->
            c?.let { customer ->
                view.showLoading(
                    R.string.public_data_loading,
                    ParamsConstants.COMMON_LOADING
                )

                AppClient.addNewCustomer(
                    "4",
                    customer.custName,
                    customer.custMobile,
                    customer.custEmail,
                    System.currentTimeMillis().toString(),
                    object :
                        CallBackUtil.CallBackCommonResponse<PaymentLinkCustomer>(PaymentLinkCustomer()) {

                        override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            commonResponse?.let {
                                view.addNewCustomerFailed(it.message)
                            }
                        }

                        override fun onResponse(r: CommonResponse?) {
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            r?.let { response ->
                                val order = JsonUtil.jsonToBean(
                                    response.message,
                                    PaymentLinkCustomer::class.java
                                ) as PaymentLinkCustomer
                                view.addNewCustomerSuccess(order)
                            }
                        }
                    }
                )
            }
        }
    }

    override fun editProductDetail(p: PaymentLinkProduct?) {
        mView?.let { view ->
            p?.let { product ->
                view.showLoading(
                    R.string.public_data_loading,
                    ParamsConstants.COMMON_LOADING
                )

                AppClient.editProductDetail(
                    "3",
                    product.goodsId,
                    product.goodsName,
                    product.goodsCode,
                    product.goodsDesc,
                    product.goodsPrice,
                    product.goodsPic,
                    System.currentTimeMillis().toString(),
                    object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {

                        override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            commonResponse?.let {
                                view.editProductDetailFailed(it.message)
                            }
                        }


                        override fun onResponse(response: CommonResponse?) {
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            response?.let {
                                view.editProductDetailSuccess(it.message)
                            }
                        }
                    }
                )
            }
        }
    }

    override fun addNewProduct(p: PaymentLinkProduct?) {
        mView?.let { view ->
            p?.let { product ->

                view.showLoading(
                    R.string.public_data_loading,
                    ParamsConstants.COMMON_LOADING
                )
                val price = if (!TextUtils.isEmpty(product.goodsPrice)) product.goodsPrice else "0"
                AppClient.addNewProduct(
                    "4",
                    product.goodsName,
                    product.goodsCode,
                    product.goodsDesc,
                    price,
                    product.goodsPic,
                    System.currentTimeMillis().toString(),
                    object :
                        CallBackUtil.CallBackCommonResponse<PaymentLinkProduct>(PaymentLinkProduct()) {

                        override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            commonResponse?.let {
                                view.addNewProductFailed(it.message)
                            }
                        }


                        override fun onResponse(r: CommonResponse?) {
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            r?.let { response ->
                                val order = JsonUtil.jsonToBean(
                                    response.message,
                                    PaymentLinkProduct::class.java
                                ) as PaymentLinkProduct
                                view.addNewProductSuccess(order)
                            }
                        }
                    }
                )
            }
        }
    }

    override fun getCustomerDetail(c: PaymentLinkCustomer?) {
        mView?.let { view ->
            c?.let { customer ->
                view.showLoading(
                    R.string.public_data_loading,
                    ParamsConstants.COMMON_LOADING
                )

                AppClient.deleteOrQueryCustomer(
                    "2",
                    customer.custId,
                    System.currentTimeMillis().toString(),
                    object :
                        CallBackUtil.CallBackCommonResponse<PaymentLinkCustomer>(PaymentLinkCustomer()) {

                        override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            commonResponse?.let {
                                view.getCustomerDetailFailed(it.message)
                            }
                        }


                        override fun onResponse(r: CommonResponse?) {
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            r?.let { response ->
                                val order = JsonUtil.jsonToBean(
                                    response.message,
                                    PaymentLinkCustomer::class.java
                                ) as PaymentLinkCustomer
                                view.getCustomerDetailSuccess(order)
                            }
                        }
                    }
                )
            }
        }
    }

    override fun getProductDetail(p: PaymentLinkProduct?) {
        mView?.let { view ->
            p?.let { product ->
                view.showLoading(
                    R.string.public_data_loading,
                    ParamsConstants.COMMON_LOADING
                )

                AppClient.deleteOrQueryProduct(
                    "2",
                    product.goodsId,
                    System.currentTimeMillis().toString(),
                    object :
                        CallBackUtil.CallBackCommonResponse<PaymentLinkProduct>(PaymentLinkProduct()) {

                        override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            commonResponse?.let {
                                view.getProductDetailFailed(it.message)
                            }
                        }

                        override fun onResponse(r: CommonResponse?) {
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            r?.let { response ->
                                val order = JsonUtil.jsonToBean(
                                    response.message,
                                    PaymentLinkProduct::class.java
                                ) as PaymentLinkProduct
                                view.getProductDetailSuccess(order)
                            }
                        }
                    }
                )
            }
        }
    }

    override fun attachView(view: CustAndProdEditContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}