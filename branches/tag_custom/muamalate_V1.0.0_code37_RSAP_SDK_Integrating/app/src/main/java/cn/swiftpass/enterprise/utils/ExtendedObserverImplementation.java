package cn.swiftpass.enterprise.utils;
import android.content.Context;
import android.widget.Toast;
import no.promon.shield.callbacks.CallbackData;
import no.promon.shield.callbacks.CallbackType;
import no.promon.shield.callbacks.ExtendedObserver;
import no.promon.shield.callbacks.RepackagingData;
import no.promon.shield.callbacks.RootingData;

/**
 * Created by aijingya on 2019/6/19.
 *
 * @Package cn.swiftpass.enterprise.utils
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2019/6/19.14:52.
 */
public class ExtendedObserverImplementation implements ExtendedObserver {
    private Context mContext;
    public ExtendedObserverImplementation(Context context){
        super();
        this.mContext = context;
    }

    @Override
    public void handleCallback(CallbackData callbackData) {
        CallbackType type = callbackData.getCallbackType();
        switch (type)
        {
            case ROOTING:
            {
                RootingData Datas = (RootingData) callbackData;
                Toast.makeText(mContext,"Device is certainly rooted or not --->"+Datas.isDeviceCertainlyRooted()
                        +"Device rooting probabilty --->"+Datas.getRootingProbability(),Toast.LENGTH_SHORT).show();
                break;
            }
            case REPACKAGING:
            {
                RepackagingData Datas = (RepackagingData) callbackData;
                Toast.makeText(mContext,"Dates is repackaged or not --->"+Datas.isRepackaged(),Toast.LENGTH_SHORT).show();
                break;
            }

        }
    }

}
