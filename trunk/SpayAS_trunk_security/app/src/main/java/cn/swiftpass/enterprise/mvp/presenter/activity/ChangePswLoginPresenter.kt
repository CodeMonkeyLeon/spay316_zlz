package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.ChangePswLoginContract
import cn.swiftpass.enterprise.utils.*
import com.example.common.entity.EmptyData
import okhttp3.Call
import java.util.*

/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @如果大地早已冰封
 * @就让我们面对着暖流
 * @走向海
 */
class ChangePswLoginPresenter : ChangePswLoginContract.Presenter {

    private var mView: ChangePswLoginContract.View? = null


    override fun ecdhKeyExchange(publicKey: String?) {
        mView?.let { view ->
            AppClient.ecdhKeyExchange(
                publicKey,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<ECDHInfo>(ECDHInfo()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        commonResponse?.let {
                            view.ecdhKeyExchangeFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        r?.let { response ->
                            val info = JsonUtil.jsonToBean(
                                response.message,
                                ECDHInfo::class.java
                            ) as ECDHInfo
                            MainApplication.getInstance().setSerPubKey(info.serPubKey)
                            MainApplication.getInstance().setSKey(info.skey)
                            view.ecdhKeyExchangeSuccess(info)
                        }
                    }
                }
            )
        }
    }

    override fun changePwd(oldPwd: String?, phone: String?, newPwd: String?, repPassword: String?) {
        mView?.let { view ->
            view.showLoading(R.string.wait_a_moment, ParamsConstants.COMMON_LOADING)

            val priKey = SharedPreUtils.readProduct(ParamsConstants.SECRET_KEY) as String?
            val paseStr = MD5.md5s(priKey).uppercase(Locale.getDefault())

            AppClient.changePwd(
                MainApplication.getInstance().getSKey(),
                phone,
                AESHelper.aesEncrypt(oldPwd, paseStr.substring(8, 24)),
                AESHelper.aesEncrypt(newPwd, paseStr.substring(8, 24)),
                AESHelper.aesEncrypt(repPassword, paseStr.substring(8, 24)),
                KotlinUtils.isNewVersion().toString(),
                MainApplication.getInstance().getMchId(),
                MainApplication.getInstance().getUserId().toString(),
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {


                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {

                            if (TextUtils.equals("405", it.result)) {
                                view.changePwdFailed("405 ${it.message}")
                            } else {
                                view.changePwdFailed(it.message)
                            }
                        }
                    }


                    override fun onResponse(response: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        view.changePwdSuccess(true)
                    }
                }
            )

        }
    }

    override fun attachView(view: ChangePswLoginContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}