/*
 * 文 件 名:  MarketingActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-1-12
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.marketing;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.order.HistoryRankingManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.AwardModel;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.adapter.RefundViewPagerAdapter;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.PullDownListView;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 营销规则
 * 
 * @author  he_hui
 * @version  [版本号, 2016-1-12]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class MarketingReportActivity extends TemplateActivity implements PullDownListView.OnRefreshListioner,
    PullDownListView.OnLoadDateRefreshListioner
{
    private ViewPager viewpager;
    
    private List<AwardModel> scanList, fulfilList;
    
    private MyMarketingAdapter sacnAdapter;
    
    private List<View> pageList;
    
    private PullDownListView scanListView, fulfilListView;
    
    private ListView fulfillistViewAuditing;
    
    private PullDownListView scanlistViewAuditing;
    
    private RefundViewPagerAdapter viewPagerAdapter;
    
    private TextView tv_scan_minus, tv_full_send, tv_add;
    
    private RelativeLayout lay_back;
    
    private boolean isTag = false; // 默认是扫码立减
    
    private LinearLayout lay_switch, lay_start;
    
    List<AwardModel> stop = new ArrayList<AwardModel>();
    
    List<AwardModel> noStart;
    
    private TextView tv_noStart, tv_stop, tv_start_text;
    
    private LinearLayout lay_no_award;
    
    private RelativeLayout ly_no_start;
    
    private String activeType;
    
    private ListView listView;
    
    private int pageFulfil = 0;
    
    private String activeId;
    
    private TextView tv_prompt;
    
    private AwardModel awardModel;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marketing_report_list);
        //        activeId = getIntent().getStringExtra("activeId");
        
        initData();
        initView();
        awardModel = (AwardModel)getIntent().getSerializableExtra("awardModel");
        
        if (!StringUtil.isEmptyOrNull(awardModel.getActiveId()))
        {
            loadDate(1, awardModel.getActiveId(), true);
        }
    }
    
    public static void startActivity(Context context, AwardModel awardModel)
    {
        Intent it = new Intent();
        it.setClass(context, MarketingReportActivity.class);
        it.putExtra("awardModel", awardModel);
        context.startActivity(it);
    }
    
    @Override
    protected void onResume()
    {
        super.onResume();
    }
    
    private void loadDate(int page, String activeId, final boolean isLoadMore)
    {
        
        HistoryRankingManager.getInstance().queryActiveTotal(activeId, new UINotifyListener<List<AwardModel>>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                if (isLoadMore)
                {
                    showNewLoading(true, getString(R.string.public_data_loading));
                }
            }
            
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dismissLoading();
                if (checkSession())
                {
                    return;
                }
                if (object != null)
                {
                    MarketingReportActivity.this.runOnUiThread(new Runnable()
                    {
                        
                        @Override
                        public void run()
                        {
                            toastDialog(MarketingReportActivity.this, object.toString(), new NewDialogInfo.HandleBtn()
                            {
                                
                                @Override
                                public void handleOkBtn()
                                {
                                    finish();
                                }
                                
                            });
                        }
                    });
                }
            }
            
            @Override
            public void onSucceed(List<AwardModel> result)
            {
                super.onSucceed(result);
                dismissLoading();
                
                if (result != null && result.size() > 0)
                {
                    
                    MarketingReportActivity.this.runOnUiThread(new Runnable()
                    {
                        
                        @Override
                        public void run()
                        {
                            // TODO Auto-generated method stub
                            tv_prompt.setVisibility(View.GONE);
                            scanlistViewAuditing.setVisibility(View.VISIBLE);
                        }
                    });
                    mySetListData(scanlistViewAuditing, scanList, sacnAdapter, result, isLoadMore);
                }
                else
                {
                    MarketingReportActivity.this.runOnUiThread(new Runnable()
                    {
                        
                        @Override
                        public void run()
                        {
                            tv_prompt.setVisibility(View.VISIBLE);
                            scanlistViewAuditing.setVisibility(View.GONE);
                        }
                    });
                }
                
            }
        });
        
    }
    
    private class MyMarketingAdapter extends BaseAdapter
    {
        private Context context;
        
        private List<AwardModel> scanList;
        
        private ViewHolder holder;
        
        public MyMarketingAdapter(Context context, List<AwardModel> scanList)
        {
            this.context = context;
            
            this.scanList = scanList;
        }
        
        @Override
        public int getCount()
        {
            return scanList.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            return scanList.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(context, R.layout.activity_maketing_report_list_item, null);
                holder = new ViewHolder();
                holder.tv_active_name = (TextView)convertView.findViewById(R.id.tv_active_name);
                holder.tv_date = (TextView)convertView.findViewById(R.id.tv_date);
                holder.tv_total_money = (TextView)convertView.findViewById(R.id.tv_total_money);
                holder.tv_peop_num = (TextView)convertView.findViewById(R.id.tv_peop_num);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            AwardModel awardModel = scanList.get(position);
            if (awardModel != null)
            {
                holder.tv_active_name.setText(MarketingReportActivity.this.awardModel.getActiveName());
                holder.tv_date.setText(awardModel.getAddDate());
                holder.tv_total_money.setText(DateUtil.formatMoneyUtils(awardModel.getSmljTotalMoney()));
                holder.tv_peop_num.setText(awardModel.getSmljTotalNum() + "");
            }
            return convertView;
        }
        
        private class ViewHolder
        {
            private TextView tv_active_name, tv_date, tv_total_money, tv_peop_num;
        }
        
    }
    
    private void initData()
    {
        scanList = new ArrayList<AwardModel>();
        sacnAdapter = new MyMarketingAdapter(this, scanList);
    }
    
    /**  
     * 重新计算ListView的高度，解决ScrollView和ListView两个View都有滚动的效果，在嵌套使用时起冲突的问题  
     * @param listView  
     */
    public void setListViewHeight(ListView listView)
    {
        
        // 获取ListView对应的Adapter    
        
        ListAdapter listAdapter = listView.getAdapter();
        
        if (listAdapter == null)
        {
            return;
        }
        int totalHeight = 0;
        for (int i = 0, len = listAdapter.getCount(); i < len; i++)
        { // listAdapter.getCount()返回数据项的数目    
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0); // 计算子项View 的宽高    
            totalHeight += listItem.getMeasuredHeight(); // 统计所有子项的总高度    
        }
        
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tv_active_report_title);
        //        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.tx_add));
        titleBar.setRightButLayVisible(false, 0);
        titleBar.setRightButLayBackground(R.drawable.icon_add);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                //历史排行榜
                showPage(MarketingAddActivity.class);
                
            }
            
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
        });
    }
    
    private Handler mHandler = new Handler();
    
    private void mySetListData(final PullDownListView pull, List<AwardModel> list, MyMarketingAdapter adapter,
        List<AwardModel> result, boolean isLoadMore)
    {
        if (!isLoadMore)
        {
            pull.onfinish(getStringById(R.string.refresh_successfully));// 刷新完成
            
            mHandler.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    // 这里表示刷新处理完成后把上面的加载刷新界面隐藏
                    pull.onRefreshComplete();
                }
            }, 800);
            
            list.clear();
        }
        else
        {
            mHandler.postDelayed(new Runnable()
            {
                
                @Override
                public void run()
                {
                    pull.onLoadMoreComplete();
                    // pull.setMore(true);
                }
            }, 1000);
        }
        list.addAll(result);
        adapter.notifyDataSetChanged();
        
    }
    
    private void initView()
    {
        tv_prompt = getViewById(R.id.tv_prompt);
        scanlistViewAuditing = (PullDownListView)findViewById(R.id.markteing_pulldown);
        listView = scanlistViewAuditing.mListView;
        listView.setAdapter(sacnAdapter);
        scanlistViewAuditing.setAutoLoadMore(true);
        scanlistViewAuditing.setRefreshListioner(this);
        scanlistViewAuditing.setOnLoadDateRefreshListioner(this);
        
        //        fulfillistViewAuditing = (ListView)v2.findViewById(R.id.markteing_pulldown);
        
        //        scanListView.setAutoLoadMore(true);
        //        fulfilListView.setAutoLoadMore(true);
        
        //        scanlistViewAuditing = scanListView.mListView;
        //        fulfillistViewAuditing = fulfilListView.mListView;
        
        //        scanListView.setRefreshListioner(this);
        //        fulfilListView.setRefreshListioner(this);
        
        //        scanlistViewAuditing.setAdapter(sacnAdapter);
        //        fulfillistViewAuditing.setAdapter(fulfilAdapter);
        
        //        setListViewHeight(scanlistViewAuditing);
        //        setListViewHeight(fulfillistViewAuditing);
        
        //        viewPagerAdapter = new RefundViewPagerAdapter(pageList);
        //        viewpager.setAdapter(viewPagerAdapter);
        //        viewpager.setOnPageChangeListener(this);
        //        viewpager.setCurrentItem(0);
        
        //        listView.setOnItemClickListener(new OnItemClickListener()
        //        {
        //            
        //            @Override
        //            public void onItemClick(AdapterView<?> arg0, View arg1, int poiston, long arg3)
        //            {
        //                AwardModel awardModel = scanList.get(poiston - 1);
        //                if (awardModel != null)
        //                {
        //                    MarketingDetailActivity.startActivity(MarketingReportActivity.this, awardModel);
        //                    
        //                }
        //            }
        //            
        //        });
        
    }
    
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    @Override
    public void onRefresh()
    {
        pageFulfil = 1;
        loadDate(pageFulfil, activeId, false);
    }
    
    @Override
    public void onLoadMore()
    {
        pageFulfil = 1;
        loadDate(pageFulfil, activeId, true);
    }
    
    @Override
    public void onLoadMoreDate()
    {
        
    }
}
