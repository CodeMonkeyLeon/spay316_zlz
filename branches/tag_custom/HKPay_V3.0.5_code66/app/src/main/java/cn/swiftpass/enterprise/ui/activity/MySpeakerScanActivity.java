package cn.swiftpass.enterprise.ui.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;

import java.io.IOException;
import java.util.Vector;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.Camera.CameraManager;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.Decoding.CaptureActivityHandler;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.Zxing.ViewfinderView;
import cn.swiftpass.enterprise.utils.DialogHelper;

/**
 * Created by aijingya on 2019/8/28.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(扫描云音响二维码并识别二维码)
 * @date 2019/8/28.15:48.
 */
public class MySpeakerScanActivity extends BaseActivity implements SurfaceHolder.Callback {
    private static final String TAG = MySpeakerScanActivity.class.getSimpleName();
    @BindView(R.id.surface_view)
    SurfaceView surfaceView;
    @BindView(R.id.viewfinder_view)
    ViewfinderView viewfinderView;
    @BindView(R.id.ll_back)
    LinearLayout llBack;
    @BindView(R.id.ll_title_add_speaker)
    LinearLayout llTitleAddSpeaker;
    @BindView(R.id.ll_title)
    LinearLayout llTitle;
    @BindView(R.id.tv_instruction)
    TextView tvInstruction;

    private CaptureActivityHandler handler;
    private boolean hasSurface;
    private SurfaceHolder surfaceHolder;
    private Vector<BarcodeFormat> decodeFormats;
    private String characterSet;
    private boolean playBeep;
    private MediaPlayer mediaPlayer;
    private static final float BEEP_VOLUME = 0.10f;
    private static final long VIBRATE_DURATION = 200L;
    private boolean vibrate;
    private String speakerIDCode;

    /**
     * 判断是否需要检测，防止不停的弹框
     */
    private boolean isNeedCheck = true;
    static final String[] PERMISSIONS = new String[]{Manifest.permission.CAMERA};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_speaker_scan);
        ButterKnife.bind(this);
        //初始化
        CameraManager.init(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isNeedCheck && Build.VERSION.SDK_INT >= 23) {
            checkPermissions(PERMISSIONS);
        }
        hasSurface = false;
        initView();
    }


    public void initView() {
        surfaceHolder = surfaceView.getHolder();

        if (hasSurface) {
            initCamera(surfaceHolder, false);
        } else {
            surfaceHolder.addCallback(this);
            surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }
        decodeFormats = null;
        characterSet = null;

        playBeep = true;
        AudioManager audioService = (AudioManager) getSystemService(AUDIO_SERVICE);
        if (audioService.getRingerMode() != AudioManager.RINGER_MODE_NORMAL) {
            playBeep = false;
        }
        initBeepSound();
        vibrate = true;
    }


    public void submitData(final String code, boolean vibration) {
        if (vibration) {
            playBeepSoundAndVibrate();
        }
        speakerIDCode = code;

        //如果码里带中文的话，可能扫码出来会新增一个有非法字符的前缀或者后缀
        if (speakerIDCode.contains("\uFEFF")){
            speakerIDCode = speakerIDCode.replace("\uFEFF","");
        }
        //如果扫码出来的字符串长度不在6-34之间，则弹框提示长度不合法
        if(speakerIDCode.trim().toString().length() > 34 || speakerIDCode.trim().toString().length() < 6){
            toastDialog(this, getStringById(R.string.terminal_speaker_id_illegal), new NewDialogInfo.HandleBtn() {
                @Override
                public void handleOkBtn() {
                    restartCamera();
                    finish();
                }
            });
        }else{
            //带数据跳转到输入页面
            Intent intent = new Intent();
            intent.putExtra("speakerIDCode", speakerIDCode);
            intent.setClass(MySpeakerScanActivity.this, MySpeakerIdInputActivity.class);
            startActivity(intent);

            //结束掉当前界面
            finish();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
        CameraManager.get().closeDriver();
        if (!hasSurface) {
            surfaceHolder.removeCallback(this);
        }
    }
    /**
     * 申请权限结果的回调方法
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] paramArrayOfInt) {
        if (requestCode == PERMISSON_REQUESTCODE) {
            if (!verifyPermissions(paramArrayOfInt)) {
                showMissingPermissionDialog(MySpeakerScanActivity.this);
                isNeedCheck = false;
            }
        }
    }

    /**
     * 显示提示没有相机权限信息
     */
    protected void showMissingPermissionDialog(Context context) {
        DialogInfo dialogInfo = new DialogInfo(context, null, getString(R.string.setting_permisson_camera), getStringById(R.string.title_setting), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                startAppSettings();
            }

            @Override
            public void handleCancleBtn() {
                finish();
            }
        }, null);
        dialogInfo.setOnKeyListener(new DialogInterface.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                return keycode == KeyEvent.KEYCODE_BACK;
            }
        });
        DialogHelper.resize(context, dialogInfo);
        dialogInfo.show();
    }

    void restartCamera() {
        closeCamera();
        viewfinderView.setVisibility(View.VISIBLE);
        initCamera(surfaceHolder, false);
    }


    private void closeCamera() {
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
    }


    private void initCamera(SurfaceHolder surfaceHolder, boolean isFirst) {
        try {
            CameraManager.get().openDriver(surfaceHolder, surfaceView);
        } catch (IOException ioe) {
            return;
        } catch (RuntimeException e) {
            return;
        }
        if (handler == null) {
            handler = new CaptureActivityHandler(this, decodeFormats, characterSet);
        }
    }

    private void initBeepSound() {
        if (playBeep && mediaPlayer == null) {
            // The volume on STREAM_SYSTEM is not adjustable, and users found it
            // too loud,
            // so we now play on the music stream.
            setVolumeControlStream(AudioManager.STREAM_MUSIC);
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setOnCompletionListener(beepListener);

            AssetFileDescriptor file = getResources().openRawResourceFd(R.raw.beep);
            try {
                mediaPlayer.setDataSource(file.getFileDescriptor(), file.getStartOffset(), file.getLength());
                file.close();
                mediaPlayer.setVolume(BEEP_VOLUME, BEEP_VOLUME);
                mediaPlayer.prepare();
            } catch (IOException e) {
                mediaPlayer = null;
            }
        }
    }

    private void playBeepSoundAndVibrate() {
        if (playBeep && mediaPlayer != null) {
            mediaPlayer.start();
        }
        if (vibrate) {
            Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
            vibrator.vibrate(VIBRATE_DURATION);
        }
    }


    /**
     * When the beep has finished playing, rewind to queue up another one.
     */
    private final MediaPlayer.OnCompletionListener beepListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            mediaPlayer.seekTo(0);
        }
    };

    public ViewfinderView getViewfinderView() {
        return viewfinderView;
    }

    public Handler getHandler() {
        return handler;
    }

    public void drawViewfinder() {
        viewfinderView.drawViewfinder();
    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (!hasSurface) {
            hasSurface = true;
            initCamera(holder, false);
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        hasSurface = false;
    }

    @OnClick(R.id.ll_back)
    public void onViewClicked() {
        finish();
    }
}
