/*
 * 文 件 名:  MarketingAddActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-1-18
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.marketing;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.order.HistoryRankingManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.AwardModel;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.reward.HistoryActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;

import com.tencent.stat.StatService;

/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2016-1-18]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class MarketingAddActivity extends TemplateActivity
{
    private EditText et_activeName, et_order_money, et_order_max_money, et_order_min_money, et_peop_num,
        et_total_money;
    
    private TextView tv_start_time, tv_end_time, tv_title, tv_report;
    
    private LinearLayout lin_report;
    
    private Button but_add;
    
    private AwardModel awardModel;
    
    private ImageView imageView1, imageView2, iv_report;
    
    private RelativeLayout lay_report, lay_end_time, lay_start_time;
    
    boolean isTag = false;
    
    private ListView lv_report_v;
    
    private DialogInfo dialogInfo;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marketing_detais);
        initView();
        setLister();
        //        but_add.getBackground().setAlpha(102);
        showSoftInputFromWindow(MarketingAddActivity.this, et_activeName);
        awardModel = (AwardModel)getIntent().getSerializableExtra("awardModel");
        if (awardModel != null)
        {
            initData(awardModel);
            loadReportDate(awardModel.getActiveId());
        }
    }
    
    private void setPricePoint(final EditText editText)
    {
        editText.addTextChangedListener(new TextWatcher()
        {
            
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.toString().contains("."))
                {
                    if (s.length() - 1 - s.toString().indexOf(".") > 2)
                    {
                        s = s.toString().subSequence(0, s.toString().indexOf(".") + 3);
                        editText.setText(s);
                        editText.setSelection(s.length());
                    }
                }
                if (s.toString().trim().substring(0).equals("."))
                {
                    s = "0" + s;
                    editText.setText(s);
                    editText.setSelection(2);
                }
                
                if (s.toString().startsWith("0") && s.toString().trim().length() > 1)
                {
                    if (!s.toString().substring(1, 2).equals("."))
                    {
                        editText.setText(s.subSequence(0, 1));
                        editText.setSelection(1);
                        return;
                    }
                }
                
                int len = s.toString().trim().length();
                
                if (len > 5)
                {
                    if (!s.toString().trim().contains("."))
                    {
                        s = s.toString().substring(0, len - 1);
                        editText.setText(s);
                        editText.setSelection(s.length());
                    }
                    
                }
                
            }
            
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                
            }
            
            @Override
            public void afterTextChanged(Editable s)
            {
                // TODO Auto-generated method stub
                
            }
            
        });
        
    }
    
    private void loadReportDate(String activeId)
    {
        HistoryRankingManager.getInstance().queryActiveTotal(activeId, new UINotifyListener<List<AwardModel>>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                
            }
            
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dismissLoading();
                if (checkSession())
                {
                    return;
                }
                if (object != null)
                {
                    
                    //                    showToastInfo(object.toString());
                }
            }
            
            @Override
            public void onSucceed(List<AwardModel> result)
            {
                super.onSucceed(result);
                dismissLoading();
                
                if (result != null && result.size() > 0)
                {
                    MarketingReportAdapter adapter = new MarketingReportAdapter(MarketingAddActivity.this, result);
                    lv_report_v.setAdapter(adapter);
                    
                    setListViewHeight(lv_report_v);
                }
                
            }
        });
        
    }
    
    public static void startActivity(Context context, AwardModel awardModel)
    {
        Intent it = new Intent();
        it.setClass(context, MarketingAddActivity.class);
        it.putExtra("awardModel", awardModel);
        context.startActivity(it);
    }
    
    private void setLister()
    {
        but_add.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                try
                {
                    StatService.trackCustomEvent(MarketingAddActivity.this, "SPConstTapMarketCount", "营销活动添加");
                }
                catch (Exception e)
                {
                }
                
                if (awardModel != null)
                {
                    String str = getString(R.string.tx_is_activation);
                    if (awardModel.getActiveStatus() == 1)
                    {
                        str = getString(R.string.tx_is_stop);
                    }
                    dialogInfo =
                        new DialogInfo(MarketingAddActivity.this, getString(R.string.public_cozy_prompt), str,
                            getString(R.string.btnOk), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG,
                            new DialogInfo.HandleBtn()
                            {
                                
                                @Override
                                public void handleOkBtn()
                                {
                                    HistoryRankingManager.getInstance().updateActiveStatus(awardModel.getActiveId(),
                                        awardModel.getActiveStatus(),
                                        new UINotifyListener<Boolean>()
                                        {
                                            @Override
                                            public void onPreExecute()
                                            {
                                                super.onPreExecute();
                                                
                                                showNewLoading(true, getString(R.string.public_data_loading));
                                            }
                                            
                                            @Override
                                            public void onError(final Object object)
                                            {
                                                super.onError(object);
                                                dismissLoading();
                                                if (checkSession())
                                                {
                                                    return;
                                                }
                                                if (object != null)
                                                {
                                                    
                                                    showToastInfo(object.toString());
                                                }
                                            }
                                            
                                            @Override
                                            public void onSucceed(Boolean result)
                                            {
                                                super.onSucceed(result);
                                                dismissLoading();
                                                
                                                if (result)
                                                {
                                                    if (awardModel.getActiveStatus() == 0)
                                                    {
                                                        showToastInfo(getString(R.string.tx_active) + "'"
                                                            + awardModel.getActiveName() + "'"
                                                            + getString(R.string.tx_activation));
                                                    }
                                                    else if (awardModel.getActiveStatus() == 1)
                                                    {
                                                        showToastInfo(getString(R.string.tx_active) + "'"
                                                            + awardModel.getActiveName() + "'"
                                                            + getString(R.string.tx_stop));
                                                    }
                                                    
                                                    finish();
                                                }
                                                
                                            }
                                        });
                                }
                                
                                @Override
                                public void handleCancleBtn()
                                {
                                    dialogInfo.cancel();
                                }
                            }, null);
                    
                    DialogHelper.resize(MarketingAddActivity.this, dialogInfo);
                    dialogInfo.show();
                    
                }
                else
                {
                    
                    if (validate())
                    {
                        AwardModel awardModel = initDate();
                        subminDate(awardModel);
                    }
                }
            }
        });
        
        lay_end_time.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                showDatetimeDialog(tv_start_time, tv_end_time);
            }
        });
        
        lay_start_time.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                showDatetimeDialog(tv_start_time, null);
            }
        });
        
        lay_report.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (!isTag)
                {
                    isTag = true;
                    lv_report_v.setVisibility(View.GONE);
                    iv_report.setImageResource(R.drawable.icon_forward);
                }
                else
                {
                    iv_report.setImageResource(R.drawable.icon_forward_up);
                    isTag = false;
                    lv_report_v.setVisibility(View.VISIBLE);
                }
            }
        });
    }
    
    private boolean compareTime(long time, long awardTime)
    {
        
        String currentTime = DateUtil.formatYYMD(time);
        
        String compartTiemStr = DateUtil.formatYYMD(awardTime);
        
        if (currentTime != null && compartTiemStr != null)
        {
            
            String[] arr1 = currentTime.split("-");
            
            String[] arr2 = compartTiemStr.split("-");
            
            if (Integer.parseInt(arr1[0]) == Integer.parseInt(arr2[0]))
            { // 年相等
                if (Integer.parseInt(arr1[1]) == Integer.parseInt(arr2[1]))
                { //月相等
                    if (Integer.parseInt(arr1[2]) == Integer.parseInt(arr2[2]))
                    {
                        return true;
                    }
                    else if (Integer.parseInt(arr1[2]) > Integer.parseInt(arr2[2]))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (Integer.parseInt(arr1[1]) > Integer.parseInt(arr2[1]))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
        
        return false;
    }
    
    private Calendar c = null;
    
    private boolean copareTime(String start, String end)
    {
        
        String[] arr1 = start.split("-");
        
        String[] arr2 = end.split("-");
        
        if (Integer.parseInt(arr2[0]) > Integer.parseInt(arr1[0]))
        {
            return false;
        }
        else if (Integer.parseInt(arr2[0]) == Integer.parseInt(arr1[0]))
        {
            if (Integer.parseInt(arr2[1]) > Integer.parseInt(arr1[1]))
            {
                if ((Integer.parseInt(arr2[1]) - Integer.parseInt(arr1[1])) > 1)
                {
                    return false;
                }
            }
            
        }
        
        return true;
    }
    
    long start_time = 0;
    
    long end_time = 0;
    
    public void showDatetimeDialog(final TextView startView, final TextView endView)
    {
        c = Calendar.getInstance();
        
        dialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener()
        {
            public void onDateSet(DatePicker dp, int year, int month, int dayOfMonth)
            {
                // String d = DateUtil.formatTime(datetime, "yyyy-MM-dd") ;
                // c.set(Calendar.DAY_OF_YEAR,);DateUtil.formatYYMD
                c.set(year, month, dayOfMonth);
                long time = System.currentTimeMillis();
                if (!compareTime(c.getTimeInMillis(), time))
                { // 不能选择大于今天的日期
                    showToastInfo(R.string.show_time_less_now);
                    return;
                }
                if (endView == null)
                {
                    start_time = c.getTimeInMillis();
                    startView.setText(DateUtil.formatYYMD(c.getTimeInMillis()) + " 00:00:00");
                }
                else
                {
                    if (!TextUtils.isEmpty(startView.getText().toString()))
                    {
                        
                        long startTime = DateUtil.getSecondsFromDate(startView.getText().toString());
                        
                        long endTime = c.getTimeInMillis();
                        
                        //得到31天的秒数
                        long oneDay = 24 * 60 * 60 * 1000;
                        if ((start_time / oneDay) - (endTime / oneDay) >= 0)
                        {
                            showToastInfo(R.string.show_endtime_than_starttime);
                        }
                        else
                        {
                            if ((endTime / oneDay) - (start_time / oneDay) > 30)
                            { // 活动时间不能超过31天
                                showToastInfo(R.string.show_time_exceed_thirty);
                            }
                            else
                            {
                                endView.setText(DateUtil.formatYYMD(c.getTimeInMillis()) + " 00:00:00");
                            }
                        }
                        
                    }
                }
                
            }
        }, c.get(Calendar.YEAR), // 传入年份
            c.get(Calendar.MONTH), // 传入月份
            c.get(Calendar.DAY_OF_MONTH) // 传入天数*/
            );
        dialog.show();
    }
    
    private void subminDate(final AwardModel awardModel)
    {
        HistoryRankingManager.getInstance().addActive(awardModel, new UINotifyListener<AwardModel>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                
                showNewLoading(true, getString(R.string.public_data_loading));
            }
            
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dismissLoading();
                if (checkSession())
                {
                    return;
                }
                if (object != null)
                {
                    
                    toastDialog(MarketingAddActivity.this, object.toString(), null);
                }
            }
            
            @Override
            public void onSucceed(AwardModel result)
            {
                super.onSucceed(result);
                dismissLoading();
                
                if (result != null)
                {
                    //                    showToastInfo(R.string.show_add_succ);
                    awardModel.setAdd(true);
                    awardModel.setActiveStatus(0);
                    awardModel.setActiveId(result.getActiveId());
                    MarketingDetailActivity.startActivity(MarketingAddActivity.this, awardModel);
                    MarketingAddActivity.this.finish();
                }
                
            }
        });
        
    }
    
    public String getMoney(String strMoney)
    {
        double money = Double.parseDouble(strMoney);
        double t = money * 100;
        DecimalFormat df = new DecimalFormat("0");
        return df.format(t);
    }
    
    private AwardModel initDate()
    {
        AwardModel awardModel = new AwardModel();
        awardModel.setActiveName(et_activeName.getText().toString());
        if (!TextUtils.isEmpty(et_total_money.getText().toString()))
        {
            awardModel.setFullMoney(Long.parseLong(getMoney(et_total_money.getText().toString())));
        }
        if (!TextUtils.isEmpty(et_order_min_money.getText().toString()))
        {
            awardModel.setMinMoney(Long.parseLong(getMoney(et_order_min_money.getText().toString())));
        }
        
        if (!TextUtils.isEmpty(et_order_money.getText().toString()))
        {
            awardModel.setOrderFullMoney(Long.parseLong(getMoney(et_order_money.getText().toString())));
        }
        
        if (!TextUtils.isEmpty(et_order_min_money.getText().toString()))
        {
            awardModel.setMinMoney(Long.parseLong(getMoney(et_order_min_money.getText().toString())));
        }
        
        if (!TextUtils.isEmpty(et_order_max_money.getText().toString()))
        {
            awardModel.setMaxMoney(Long.parseLong(getMoney(et_order_max_money.getText().toString())));
        }
        if (!TextUtils.isEmpty(et_peop_num.getText().toString()))
        {
            awardModel.setMaxNum(Long.parseLong(et_peop_num.getText().toString()));
        }
        
        awardModel.setStartDate(tv_start_time.getText().toString());
        
        awardModel.setEndDate(tv_end_time.getText().toString());
        
        return awardModel;
    }
    
    /**  
     * 重新计算ListView的高度，解决ScrollView和ListView两个View都有滚动的效果，在嵌套使用时起冲突的问题  
     * @param listView  
     */
    public void setListViewHeight(ListView listView)
    {
        
        // 获取ListView对应的Adapter    
        
        ListAdapter listAdapter = listView.getAdapter();
        
        if (listAdapter == null)
        {
            return;
        }
        int totalHeight = 0;
        for (int i = 0, len = listAdapter.getCount(); i < len; i++)
        { // listAdapter.getCount()返回数据项的数目    
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0); // 计算子项View 的宽高    
            totalHeight += listItem.getMeasuredHeight(); // 统计所有子项的总高度    
        }
        
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
    
    private boolean validate()
    {
        if (TextUtils.isEmpty(et_activeName.getText().toString()))
        {
            showToastInfo(R.string.show_active_name_nutnull);
            et_activeName.setFocusable(true);
            return false;
        }
        if (TextUtils.isEmpty(et_order_money.getText().toString()))
        {
            showToastInfo(R.string.et_order_money_nutnull);
            et_order_money.setFocusable(true);
            return false;
        }
        else
        {
            String et_order_moneyStr = et_order_money.getText().toString();
            if (et_order_moneyStr.contains(".")
                && et_order_moneyStr.substring(et_order_moneyStr.lastIndexOf('.')).equals("."))
            {
                showToastInfo(R.string.et_order_money_format);
                et_order_money.setFocusable(true);
                return false;
            }
        }
        
        if (TextUtils.isEmpty(et_order_max_money.getText().toString()))
        {
            showToastInfo(R.string.et_order_max_money_nutnull);
            et_order_max_money.setFocusable(true);
            return false;
        }
        else
        {
            String et_order_max_money_Str = et_order_max_money.getText().toString();
            if (et_order_max_money_Str.contains(".")
                && et_order_max_money_Str.substring(et_order_max_money_Str.lastIndexOf('.')).equals("."))
            {
                showToastInfo(R.string.et_order_max_money_format);
                et_order_max_money.setFocusable(true);
                return false;
            }
        }
        //        
        if (TextUtils.isEmpty(et_order_min_money.getText().toString()))
        
        {
            showToastInfo(R.string.et_order_min_money_nutnull);
            et_order_min_money.setFocusable(true);
            return false;
        }
        else
        {
            String et_order_max_money_Str = et_order_min_money.getText().toString();
            if (et_order_max_money_Str.contains(".")
                && et_order_max_money_Str.substring(et_order_max_money_Str.lastIndexOf('.')).equals("."))
            {
                showToastInfo(R.string.et_order_min_money_format);
                et_order_min_money.setFocusable(true);
                return false;
            }
        }
        
        if (TextUtils.isEmpty(et_peop_num.getText().toString()))
        {
            showToastInfo(getString(R.string.et_peop_num));
            et_peop_num.setFocusable(true);
            return false;
        }
        else
        {
            long num = Long.parseLong(et_peop_num.getText().toString());
            if (num == 0)
            {
                showToastInfo(R.string.et_peop_len_thar_zero);
                et_peop_num.setFocusable(true);
                return false;
            }
        }
        
        if (TextUtils.isEmpty(et_total_money.getText().toString()))
        {
            showToastInfo(R.string.et_total_money);
            et_total_money.setFocusable(true);
            return false;
        }
        else
        {
            String et_order_max_money_Str = et_total_money.getText().toString();
            if (et_order_max_money_Str.contains(".")
                && et_order_max_money_Str.substring(et_order_max_money_Str.lastIndexOf('.')).equals("."))
            {
                showToastInfo(R.string.et_total_money_format);
                et_total_money.setFocusable(true);
                return false;
            }
            
        }
        //        
        if (TextUtils.isEmpty(tv_start_time.getText().toString()))
        {
            showToastInfo(R.string.tv_start_time);
            tv_start_time.setFocusable(true);
            return false;
        }
        
        if (TextUtils.isEmpty(tv_end_time.getText().toString()))
        {
            showToastInfo(R.string.tv_end_time);
            tv_end_time.setFocusable(true);
            return false;
        }
        if (!TextUtils.isEmpty(et_total_money.getText().toString()))
        {
            
            String et_order_max_money_Str = et_total_money.getText().toString();
            if (et_order_max_money_Str.contains(".")
                && et_order_max_money_Str.substring(et_order_max_money_Str.lastIndexOf('.')).equals("."))
            {
                showToastInfo(getString(R.string.et_order_min_money_format));
                et_total_money.setFocusable(true);
                return false;
            }
        }
        double max_money = Double.parseDouble(et_order_max_money.getText().toString());
        double min_money = Double.parseDouble(et_order_min_money.getText().toString());
        double total_money = Double.parseDouble(et_total_money.getText().toString());
        
        if (paseLong(max_money) < paseLong(min_money))
        {
            showToastInfo(R.string.tv_max_money_thar_min_money);
            et_order_max_money.setFocusable(true);
            return false;
        }
        double order_money = Double.parseDouble(et_order_money.getText().toString());
        if (paseLong(order_money) < paseLong(max_money))
        {
            showToastInfo(R.string.tv_order_money_thar_max_money);
            et_order_max_money.setFocusable(true);
            return false;
        }
        if (paseLong(total_money) < paseLong(max_money))
        {
            showToastInfo(R.string.tv_total_money_thar_max_money);
            et_total_money.setFocusable(true);
            return false;
        }
        return true;
    }
    
    private long paseLong(double s)
    {
        return (long)(100 * s);
    }
    
    private void initData(AwardModel awardModel)
    {
        et_activeName.setText(awardModel.getActiveName());
        if (awardModel.getOrderFullMoney() > 0)
        {
            et_order_money.setHint(DateUtil.formatMoneyUtils(awardModel.getOrderFullMoney())
                + getString(R.string.pay_yuan));
            //            et_order_money.setText(DateUtil.formatMoneyUtils(awardModel.getOrderFullMoney()) + "元");
        }
        
        if (awardModel.getFullMoney() > 0)
        {
            et_total_money.setHint(DateUtil.formatMoneyUtils(awardModel.getFullMoney()) + getString(R.string.pay_yuan));
        }
        
        if (awardModel.getMaxMoney() > 0)
        {
            et_order_max_money.setHint(DateUtil.formatMoneyUtils(awardModel.getMaxMoney())
                + getString(R.string.pay_yuan));
        }
        if (awardModel.getMinMoney() > 0)
        {
            et_order_min_money.setHint(DateUtil.formatMoneyUtils(awardModel.getMinMoney())
                + getString(R.string.pay_yuan));
        }
        tv_start_time.setText(awardModel.getStartDate());
        tv_end_time.setText(awardModel.getEndDate());
        et_peop_num.setText(awardModel.getMaxNum() + "");
        switch (awardModel.getActiveStatus())
        {
            case 0:
                but_add.setText(R.string.tv_activation);
                break;
            case 1:
                but_add.setText(R.string.tv_stop);
                tv_title.setText(R.string.tv_active_starting);
                lin_report.setVisibility(View.VISIBLE);
                tv_report.setText(DateUtil.formatMoneyUtils(awardModel.getSmljTotalMoney())
                    + getString(R.string.pay_yuan) + awardModel.getSmljTotalNum() + getString(R.string.tv_person_time));
                break;
            case 2:
                tv_title.setText(R.string.tv_active_prompt);
                but_add.setVisibility(View.GONE);
                lin_report.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
        setEnable();
        
        titleBar.setTitle(awardModel.getActiveName());
    }
    
    private void setEnable()
    {
        imageView1.setVisibility(View.GONE);
        imageView2.setVisibility(View.GONE);
        
        et_activeName.setEnabled(false);
        et_order_money.setEnabled(false);
        et_order_max_money.setEnabled(false);
        et_order_min_money.setEnabled(false);
        et_peop_num.setEnabled(false);
        et_total_money.setEnabled(false);
        lay_end_time.setEnabled(false);
        lay_start_time.setEnabled(false);
        
    }
    
    private void initView()
    {
        lin_report = getViewById(R.id.lin_report);
        lv_report_v = getViewById(R.id.lv_report_v);
        iv_report = getViewById(R.id.iv_report);
        tv_report = getViewById(R.id.tv_report);
        lay_report = getViewById(R.id.lay_report);
        tv_title = getViewById(R.id.tv_title);
        imageView1 = getViewById(R.id.imageView1);
        imageView2 = getViewById(R.id.imageView2);
        et_activeName = getViewById(R.id.et_activeName);
        et_order_money = getViewById(R.id.et_order_money);
        setPricePoint(et_order_money);
        et_order_max_money = getViewById(R.id.et_order_max_money);
        setPricePoint(et_order_max_money);
        et_order_min_money = getViewById(R.id.et_order_min_money);
        setPricePoint(et_order_min_money);
        et_peop_num = getViewById(R.id.et_peop_num);
        et_total_money = getViewById(R.id.et_total_money);
        setPricePoint(et_total_money);
        tv_start_time = getViewById(R.id.tv_start_time);
        tv_end_time = getViewById(R.id.tv_end_time);
        
        lay_end_time = getViewById(R.id.lay_end_time);
        lay_start_time = getViewById(R.id.lay_start_time);
        
        but_add = getViewById(R.id.but_add);
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tx_add_active);
        titleBar.setRightButLayVisibleForTotal(false, getString(R.string.tx_add));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                //历史排行榜
                showPage(HistoryActivity.class);
            }
            
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
        });
    }
    
    class MarketingReportAdapter extends BaseAdapter
    {
        private Context context;
        
        private List<AwardModel> scanList;
        
        private ViewHolder holder;
        
        public MarketingReportAdapter(Context context, List<AwardModel> scanList)
        {
            
            this.context = context;
            this.scanList = scanList;
        }
        
        @Override
        public int getCount()
        {
            // TODO Auto-generated method stub
            return scanList.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            // TODO Auto-generated method stub
            return scanList.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            // TODO Auto-generated method stub
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(context, R.layout.activity_maketing_report_list_item, null);
                holder = new ViewHolder();
                holder.tv_title = (TextView)convertView.findViewById(R.id.tv_title);
                holder.tv_money = (TextView)convertView.findViewById(R.id.tv_money);
                
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            
            AwardModel awardModel = scanList.get(position);
            if (awardModel != null)
            {
                holder.tv_title.setText(awardModel.getAddDate() + "(" + awardModel.getSmljTotalNum() + "人次)");
                holder.tv_money.setText(DateUtil.formatMoneyUtils(awardModel.getSmljTotalMoney()) + "元");
            }
            
            return convertView;
        }
        
        private class ViewHolder
        {
            private TextView tv_title, tv_money;
        }
        
    }
}
