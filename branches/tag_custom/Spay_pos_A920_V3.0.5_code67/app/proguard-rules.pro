##################################基本指令区，默认不用修改下面内容##########################################
-ignorewarnings                           #忽略所有警告
-optimizationpasses 5                    #指定代码的压缩级别
-dontusemixedcaseclassnames             #是否使用大小写混合
-dontskipnonpubliclibraryclasses       #是否混淆第三方jar， 指定不去忽略非公共的库的类
-dontskipnonpubliclibraryclassmembers #指定不去忽略非公共的库的类的成员
-dontpreverify                          #不做预校验的操作
-verbose                                 # 混淆时是否记录日志
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*,!code/allocation/variable # 混淆时所采用的算法
-printmapping proguardMapping.txt       #生成原类名和混淆后的类名的映射文件
-keepattributes *Annotation*            #不混淆Annotation
-keepattributes InnerClasses           #不混淆内部类
-keepattributes Signature              #不混淆泛型
-keepattributes Exceptions             #不混淆异常
-keepattributes EnclosingMethod        #不混淆反射
-keepattributes *JavascriptInterface*  #不混淆Android和JS交互
-keepattributes SourceFile,LineNumberTable #抛出异常时保留代码行号
###############################################################################################################

##############################################基本组件白名单，默认不用修改以下内容----Start#######################################
#-keep public class * extends android.app.Fragment
#-keep public class * extends android.app.Activity
#-keep public class * extends android.app.Application
#-keep public class * extends android.app.Service
#-keep public class * extends android.content.BroadcastReceiver
#-keep public class * extends android.content.ContentProvider
#-keep public class * extends android.app.backup.BackupAgentHelper
#-keep public class * extends android.preference.Preference
#-keep public class * extends android.view.View
#保留Google原生服务需要的类
#-keep public class com.android.vending.licensing.ILicensingService
#-keep public class com.google.vending.licensing.ILicensingService
#XML映射问题,如果你遇到一些控件无法Inflate，报NullPointException，比如ListView，NavigationView等等
#-keep class org.xmlpull.v1.** {*;}

#保留自定义View,如"属性动画"中的set/get方法
-keepclassmembers public class * extends android.view.View{
    *** get*();
    void set*(***);
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
}
#保留Activity中参数是View的方法，如XML中配置android:onClick=”buttonClick”属性，Activity中调用的buttonClick(View view)方法
-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
}
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}
#Parcelable实现类中的CREATOR字段是绝对不能改变的，包括大小写
-keepclassmembers class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}
#保留混淆枚举中的values()和valueOf()方法
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
#保留R下面的资源
-keep class **.R$* {*;}
#R文件中的所有记录资源id的静态字段
-keepclassmembers class **.R$* {
    public static <fields>;
}
# 保留native方法的类名和方法名
-keepclasseswithmembernames class * {
    native <methods>;
}
# -->某些构造方法不能去混淆
-keepclasseswithmembers class * {
    public <init>(android.content.Context);
}
-keepclassmembers class * {
   public <init>(org.json.JSONObject);
}
#对于带有回调函数的onXXEvent的，不能被混淆
-keepclassmembers class * {
    void *(**On*Event);
}
##############################################基本组件白名单----End#######################################

###################################### Support包规则----start ###################################################
# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version.  We know about them, and they are safe.
-dontwarn android.support.**        # 如果引用了v4或者v7包
-keep class android.support.** {*;} #保留support下的所有类及其内部类

# Understand the @Keep support annotation.
-keep class android.support.annotation.Keep
-keep @android.support.annotation.Keep class * {*;}

-keepclasseswithmembers class * {
    @android.support.annotation.Keep <methods>;
}
-keepclasseswithmembers class * {
    @android.support.annotation.Keep <fields>;
}
-keepclasseswithmembers class * {
    @android.support.annotation.Keep <init>(...);
}
###################################### Support包规则-----end ###################################################

######################################webview-----start ###########################################################
-keepclassmembers class fqcn.of.javascript.interface.for.Webview {
   public *;
}
-keepclassmembers class * extends android.webkit.WebViewClient {
    public void *(android.webkit.WebView, java.lang.String, android.graphics.Bitmap);
    public boolean *(android.webkit.WebView, java.lang.String);
}
-keepclassmembers class * extends android.webkit.WebViewClient {
    public void *(android.webkit.WebView, jav.lang.String);
}

######################################webview-----end ###########################################################


#---------------------------------1.实体类---------------------------------
-keep class cn.swiftpass.enterprise.bussiness.** { *;}
#--keep class cn.swiftpass.enterprise.io.net.** { *;}
#--keep class cn.swiftpass.enterprise.print.** { *;}
-keep class cn.swiftpass.enterprise.utils.** {*;}
-keep class cn.swiftpass.enterprise.broadcast.** {*;}
#--keep class cn.swiftpass.enterprise.ui.** {*;}
#-keep class *.** {*;}
#-------------------------------------------------------------------------

#---------------------------------2.第三方包-------------------------------
#所有实体类不混淆，切记
-keep class cn.swiftpass.enterprise.newmvp.base.** { *; }
-keep class cn.swiftpass.enterprise.newmvp.loginmodule.model.bean.** { *; }

#okhttp3
-dontwarn com.squareup.okhttp3.**
-keep class com.squareup.okhttp3.** { *;}
-dontwarn okio.**

#retrofit2
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions

# RxLifeCycle2
-keep class com.trello.rxlifecycle2.** { *; }
-keep interface com.trello.rxlifecycle2.** { *; }
-dontwarn com.trello.rxlifecycle2.**

# universal-image-loader
-dontwarn com.nostra13.universalimageloader.**
-keep class com.nostra13.universalimageloader.** {*;}

#afinal
-dontwarn com.net.tsz.afinal.**
-keep class com.net.tsz.afinal.** {*;}

#blue_pos
-dontwarn cn.newpost.tech.blue.socket.**
-keep class cn.newpost.tech.blue.socket.** {*;}

#commons-codec
-dontwarn org.apache.commons.codec.**
-keep class org.apache.commons.codec.** {*;}

#GetuiSDK
-dontwarn com.igexin.**
-keep class assets.** {*;}
-keep class com.igexin.** {*;}
-keep class org.json.** { *; }




## gson[version 2.8.0]
-keep class sun.misc.Unsafe { *; }
-dontwarn com.google.**
-keep class com.google.gson.** {*;}
-keep class com.google.gson.stream.** { *; }
-keep class com.google.gson.examples.android.model.** { *; }
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer
## gson

#httpmine
-dontwarn org.apache.http.entity.mine.**
-keep class org.apache.http.entity.mine.** {*;}

#mid-sdk
-dontwarn com.tencent.mid.**
-keep class com.tencent.mid.** {*;}

#MPChartLib
-dontwarn com.github.mikephil.charting.**
-keep class com.github.mikephil.charting.**{*;}

#Msc
-dontwarn com.iflytek.**
-keep class com.iflytek.**{*;}


#mta
-dontwarn com.tencent.stat.**
-keep class com.tencent.stat.** {*;}

#ScanerLibrary
-dontwarn com.scaner.barcode.**
-keep class com.scaner.barcode.** {*;}

#simple-xml
-dontwarn org.simpleframework.xml.**
-keep class org.simpleframework.xml.** {*;}


#xcl-charts
-dontwarn org.xclcharts.**
-keep class org.xclcharts.** {*;}

#zxingcore
-dontwarn com.google.zxing.**
-keep class com.google.zxing.** {*;}

# 友盟统计
-keepclassmembers class * {
    public <init> (org.json.JSONObject);
}

# 友盟统计5.0.0以上SDK需要
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

# 友盟统计R.java删除问题
-keep public class com.gdhbgh.activity.R$*{
    public static final int *;
}

# OkHttp
-dontwarn com.squareup.okhttp.**
-keep class com.squareup.okhttp.** {*;}
-keep interface com.squareup.okhttp.** {*;}
-dontwarn okio.**
## okhttp3[version_logging-interceptor 3.3.1]
-dontwarn com.squareup.okhttp3.**
-keep class com.squareup.okhttp3.** { *;}
-dontwarn okio.**
## okhttp3

#nineoldandroids
-dontwarn com.nineoldandroids.*
-keep class com.nineoldandroids.** {*;}

# 支付宝
-keep class com.alipay.android.app.IAlixPay{*;}
-keep class com.alipay.android.app.IAlixPay$Stub{*;}
-keep class com.alipay.android.app.IRemoteServiceCallback{*;}
-keep class com.alipay.android.app.IRemoteServiceCallback$Stub{*;}
-keep class com.alipay.sdk.app.PayTask{
    public *;
}
-keep class com.alipay.sdk.app.AuthTask{
    public *;
}

# socket.io
-keep class socket.io-client.
-keepclasseswithmembers,allowshrinking class socket.io-client.* {*;}
-keep class io.socket.
-keepclasseswithmembers,allowshrinking class io.socket.* {*;}

# OrmLite
-keep class com.j256.**
-keepclassmembers class com.j256.** { *; }
-keep enum com.j256.**
-keepclassmembers enum com.j256.** { *; }
-keep interface com.j256.**
-keepclassmembers interface com.j256.** { *; }

#EventBus
-keepattributes *Annotation*
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

# Only required if you use AsyncExecutor
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}

#retroift
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions

## butterknife[version 8.2.1]
-keep class butterknife.*
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keepclasseswithmembernames class * { @butterknife.* <methods>; }
-keepclasseswithmembernames class * { @butterknife.* <fields>; }
-keep class **$$ViewBinder { *; }
## butterknife
#rxjava
-dontwarn sun.misc.**
-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
 long producerIndex;
 long consumerIndex;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
 rx.internal.util.atomic.LinkedQueueNode producerNode;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueConsumerNodeRef {
 rx.internal.util.atomic.LinkedQueueNode consumerNode;
}
#Fresco
-keep class com.facebook.fresco.** {*;}
-keep interface com.facebook.fresco.** {*;}
-keep enum com.facebook.fresco.** {*;}
## glide[version 3.7.0]
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {  **[] $VALUES;  public *;}
## glide

-dontwarn com.fourmob.datetimepicker.**
-keep class com.fourmob.datetimepicker.** { *;}

-dontwarn com.nineoldandroids.**
-keep class com.nineoldandroids.** { *;}

-dontwarn com.sleepbot.datetimepicker.**
-keep class com.sleepbot.datetimepicker.** { *;}

-keep class com.j256.ormlite.** { *;}
-keep class com.j256.ormlite.android.AndroidLog { *;}


-dontwarn org.bouncycastle.**
-keep class org.bouncycastle.** { *;}

-dontwarn com.tencent.bugly.**
-keep public class com.tencent.bugly.**{*;}

#PAXGL
-dontwarn com.pax.gl.**
-keep class com.pax.gl.** {*;}

#PAXNEP
-dontwarn com.pax.**
-keep class com.pax.** {*;}


#-------------------------------------------------------------------------

#---------------------------------3.与js互相调用的类------------------------
-keepclassmembers class cn.swiftpass.enterprise.ui.activity {
  *;
}
-keepclassmembers class cn.swiftpass.enterprise.ui.activity$* {
  *;
}
#-------------------------------------------------------------------------

#---------------------------------4.反射相关的类和方法-----------------------


-dontwarn android.net.SSLCertificateSocketFactory


#-libraryjars libs/afinal_0.5_bin.jar
#-libraryjars libs/androidprintsdk.jar
#-libraryjars libs/android-support-v4.jar
#-libraryjars libs/blue_pos.jar
#-libraryjars libs/commons-codec-1.4.jar
#-libraryjars libs/GetuiExt-2.0.3.jar
#-libraryjars libs/GetuiSDK2.6.4.0.jar
#-libraryjars libs/gson-2.2.2.jar
#-libraryjars libs/httpmime-4.2.1.jar
#-libraryjars libs/mid-sdk-2.20.jar
#-libraryjars libs/MPChartLib.jar
#-libraryjars libs/Msc.jar
#-libraryjars libs/mta-sdk-2.2.0.jar
#-libraryjars libs/ormlite-android-4.42.jar
#-libraryjars libs/ormlite-core-4.42.jar
#-libraryjars libs/ScanerLibrary V1.2.jar
#-libraryjars libs/simple-xml-2.7.jar
#-libraryjars libs/Sunflower.jar
#-libraryjars libs/universal-image-loader-1.8.6.jar
#-libraryjars libs/xcl-charts.jar
#-libraryjars libs/zxingcore.jar
