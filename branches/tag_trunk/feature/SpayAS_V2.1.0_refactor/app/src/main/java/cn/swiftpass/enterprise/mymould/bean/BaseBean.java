package cn.swiftpass.enterprise.mymould.bean;

import android.text.TextUtils;

import com.cxy.networklibrary.util.GsonUtil;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.JsonAdapter;

import java.lang.reflect.Type;


public class BaseBean<T> {

    /**
     * result : 200
     * message : {"data":[{"id":"30000094","empId":"","username":"12345678","realname":"Olivia","deptname":"","remark":"","isAdmin":"","enabled":true,"mchId":"","isSysAdmin":"","isRefundAuth":"1","isOrderAuth":"1","isTotalAuth":"0","isActivityAuth":"0","showEwallet":"","mchName":"","mchLogo":"","tradeName":"","serviceType":"","signKey":"","cookieMap":"","roleType":"","delete":false,"bankCode":"","feeType":"","feeFh":"","merchantShortName":"","themeMd5":"","apiShowListMd5":"","apiProviderMap":{},"refundStateMap":{},"tradeStateMap":{},"payTypeMap":{},"isSurchargeOpen":0,"isTaxRateOpen":0,"isTipOpen":0,"email":"jiangyum9588@qq.com","timeZone":"","machineId":"","createdAt":"","appId":""},{"id":"30000045","empId":"","username":"cash003","realname":"Yuki","deptname":"","remark":"","isAdmin":"","enabled":true,"mchId":"","isSysAdmin":"","isRefundAuth":"0","isOrderAuth":"1","isTotalAuth":"0","isActivityAuth":"0","showEwallet":"","mchName":"","mchLogo":"","tradeName":"","serviceType":"","signKey":"","cookieMap":"","roleType":"","delete":false,"bankCode":"","feeType":"","feeFh":"","merchantShortName":"","themeMd5":"","apiShowListMd5":"","apiProviderMap":{},"refundStateMap":{},"tradeStateMap":{},"payTypeMap":{},"isSurchargeOpen":0,"isTaxRateOpen":0,"isTipOpen":0,"email":"250901445@qq.com","timeZone":"","machineId":"","createdAt":"","appId":""}],"currentPage":0,"perPage":15,"pageCount":2,"totalRows":18,"reqFeqTime":""}
     * code :
     */

    private String result;
    @JsonAdapter(Deserializer.class)
    private T message;
    private String code;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public T getMessage() {
        return message;
    }

    public void setMessage(T message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public static class Deserializer<T> implements JsonDeserializer<T> {
        @Override
        public T deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            if (json.isJsonPrimitive()) {
                String strJson = json.getAsString();
                if (!TextUtils.isEmpty(strJson)) {
                    return GsonUtil.getInstance().fromJson(strJson, typeOfT);
                }
            }
            return null;
        }

    }

}
