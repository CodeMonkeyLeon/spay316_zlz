package cn.swiftplus.enterprise.printsdk.print.A920;

import android.content.Context;
import com.pax.dal.IDAL;
import com.pax.neptunelite.api.DALProxyClient;

/**
 * Created by aijingya on 2020/8/7.
 *
 * @Package cn.swiftplus.enterprise.printsdk.print.A920
 * @Description:
 * @date 2020/8/7.10:44.
 */
public class POSA920Client {
    private static POSA920Client sA920Client;
    public static IDAL idal = null;
    private Context mContext;

    public static void initA920(Context context){
        if (sA920Client != null) {
            throw new RuntimeException("A920Client Already initialized");
        }
        sA920Client = new POSA920Client(context);
    }

    public static POSA920Client getInstance() {
        if (sA920Client == null) {
            throw new RuntimeException("A920Client is not initialized");
        }
        return sA920Client;
    }

    private POSA920Client(Context context){
        mContext = context;
        //百富pos初始化
        try {
            idal = DALProxyClient.getInstance().getDal(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
