package cn.swiftpass.enterprise.ui.activity;

import static cn.swiftpass.enterprise.intl.BuildConfig.IS_POS_VERSION;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.common.sentry.SentryUtils;
import com.example.common.sp.PreferenceUtil;

import java.math.BigDecimal;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.common.Constant;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.MasterCardOrderDetailsContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.MasterCardOrderDetailsPresenter;
import cn.swiftpass.enterprise.print.PrintOrder;
import cn.swiftpass.enterprise.ui.activity.bill.OrderRefundActivity;
import cn.swiftpass.enterprise.ui.activity.print.BluePrintUtil;
import cn.swiftpass.enterprise.ui.activity.print.BluetoothSettingActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.CreateOneDiCodeUtil;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.LocaleUtils;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;
import cn.swiftplus.enterprise.printsdk.print.PrintClient;

/**
 * Created by aijingya on 2021/3/19.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description:
 * @date 2021/3/19.15:07.
 */
public class MasterCardOrderDetailsActivity extends BaseActivity<MasterCardOrderDetailsContract.Presenter> implements MasterCardOrderDetailsContract.View {

    public static final String TAG = "MasterCardOrderDetailsActivity";

    private LinearLayout ll_order_fee, ll_surcharge, ll_refundMoneyAmount, ll_money_receivable, ll_card_payment_order_final_status, ll_cashier_layout, ll_order_num, ll_attach, ll_big_one_code;
    private TextView tx_total_receivable, id_parser_money;
    private TextView tv_amount_title, tv_order_total_fee, tv_surcharge, tv_refund_title, tv_refund_money, tv_order_type, tv_order_status, tv_receivable_money, tv_discount_amount;
    private TextView tv_addtime, tv_cashier_name, tv_payment_method, tv_order_num_title_info, tv_OrderNum, tv_plantform_order_id, tv_attach, tv_code, tv_order_final_status;
    private View id_line_cashier, view_order_num_line, line_attach, view_final_status_line;
    private View iv_line_refund;
    private ImageView iv_code;
    private Button btn_order_syn, btn_print;
    //根据当前的小数点的位数来判断应该除以多少
    private double min = 1;
    private boolean isCashierVisiable = false;//是否显示收银员
    private Order orderModel;

    public static void startActivity(Context mContext, Order order) {
        Intent it = new Intent();
        it.setClass(mContext, MasterCardOrderDetailsActivity.class);
        it.putExtra("order", order);
        mContext.startActivity(it);
    }

    @Override
    protected MasterCardOrderDetailsContract.Presenter createPresenter() {
        return new MasterCardOrderDetailsPresenter();
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainApplication.getInstance().getListActivities().add(MasterCardOrderDetailsActivity.this);
        //根据当前的小数点的位数来判断应该除以多少
        for (int i = 0; i < MainApplication.getInstance().getNumFixed(); i++) {
            min = min * 10;
        }

        orderModel = (Order) getIntent().getSerializableExtra("order");

        initView();
        initUIWithData();
        setListener();

        ToastHelper.getInstance(MasterCardOrderDetailsActivity.this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initUIWithData();
        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印的绑定操作
        if (IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)) {
            PrintClient.getInstance().bindDeviceService();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印的解绑操作
        if (IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)) {
            PrintClient.getInstance().unbindDeviceService();
        }
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_order_detail);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {
            }

            @Override
            public void onRightButLayClick() {
                if (orderModel.money - (orderModel.getRefundMoney() + orderModel.getRfMoneyIng()) == 0) {
                    toastDialog(MasterCardOrderDetailsActivity.this, R.string.tv_order_reunding, null);
                    return;
                }
                OrderRefundActivity.startActivity(MasterCardOrderDetailsActivity.this, orderModel, true);
            }
        });
    }

    public void initView() {
        setContentView(R.layout.activity_card_payment_order_details);


        ll_order_fee = findViewById(R.id.ll_order_fee);
        ll_surcharge = findViewById(R.id.ll_surcharge);
        ll_refundMoneyAmount = findViewById(R.id.ll_refundMoneyAmount);
        ll_money_receivable = findViewById(R.id.ll_money_receivable);
        ll_card_payment_order_final_status = findViewById(R.id.ll_card_payment_order_final_status);
        ll_cashier_layout = findViewById(R.id.ll_cashier_layout);
        ll_order_num = findViewById(R.id.ll_order_num);
        ll_attach = findViewById(R.id.ll_attach);
        ll_big_one_code = findViewById(R.id.ll_big_one_code);

        tx_total_receivable = findViewById(R.id.tx_total_receivable);
        id_parser_money = findViewById(R.id.id_parser_money);

        tv_amount_title = findViewById(R.id.tv_amount_title);
        tv_order_total_fee = findViewById(R.id.tv_order_total_fee);
        tv_surcharge = findViewById(R.id.tv_surcharge);

        tv_refund_title = findViewById(R.id.tv_refund_title);
        tv_refund_money = findViewById(R.id.tv_refund_money);

        tv_order_type = findViewById(R.id.tv_order_type);
        tv_order_status = findViewById(R.id.tv_order_status);
        tv_receivable_money = findViewById(R.id.tv_receivable_money);
        tv_discount_amount = findViewById(R.id.tv_discount_amount);

        tv_addtime = findViewById(R.id.tv_addtime);
        tv_cashier_name = findViewById(R.id.tv_cashier_name);
        tv_payment_method = findViewById(R.id.tv_payment_method);
        tv_order_final_status = findViewById(R.id.tv_order_final_status);
        tv_order_num_title_info = findViewById(R.id.tv_order_num_title_info);
        tv_OrderNum = findViewById(R.id.tv_OrderNum);
        tv_plantform_order_id = findViewById(R.id.tv_plantform_order_id);
        tv_attach = findViewById(R.id.tv_attach);
        tv_code = findViewById(R.id.tv_code);

        id_line_cashier = findViewById(R.id.id_line_cashier);
        view_order_num_line = findViewById(R.id.view_order_num_line);
        line_attach = findViewById(R.id.line_attach);
        view_final_status_line = findViewById(R.id.view_final_status_line);

        iv_line_refund = findViewById(R.id.iv_line_refund);
        iv_code = findViewById(R.id.iv_code);

        btn_order_syn = findViewById(R.id.btn_order_syn);
        btn_print = findViewById(R.id.btn_print);
    }

    private void initUIWithData() {
        if (orderModel != null) {
            //total
            tx_total_receivable.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(orderModel.getMoney()));

            //人民币转换显示
            if (orderModel.getCashFeel() <= 0) {
                id_parser_money.setVisibility(View.GONE);
            } else {
                id_parser_money.setVisibility(View.VISIBLE);

                //因为当前是人民币的金额，人民币的话，币种最小单位为100
                BigDecimal paseBigDecimal = new BigDecimal(orderModel.getCashFeel() / Constant.RMB_FIX);
                id_parser_money.setText(getString(R.string.tx_mark) + DateUtil.formatPaseRMBMoney(paseBigDecimal));
            }

            //判断是否开通附加费，没有附加费则不展示
            if (!MainApplication.getInstance().isSurchargeOpen()) {
                ll_order_fee.setVisibility(View.GONE);
                ll_surcharge.setVisibility(View.GONE);
            } else {
                if (orderModel != null) {
                    ll_order_fee.setVisibility(View.VISIBLE);
                    ll_surcharge.setVisibility(View.VISIBLE);
                    tv_order_total_fee.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(orderModel.getOrderFee()));
                    tv_surcharge.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(orderModel.getSurcharge()));
                }
            }
            //是否是转入退款的单
            if (orderModel.getRefundMoney() > 0) {
                double m = (double) orderModel.getRefundMoney() / min;
                iv_line_refund.setVisibility(View.VISIBLE);
                ll_refundMoneyAmount.setVisibility(View.VISIBLE);

                tv_refund_title.setText(R.string.tx_bill_stream_refund_money);
                tv_refund_money.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtil(m));
            } else {
                iv_line_refund.setVisibility(View.GONE);
                ll_refundMoneyAmount.setVisibility(View.GONE);
            }

            tv_order_type.setText(orderModel.getTradeTypeText());

            //订单状态 交易状态：(1.初始化，2：支付成功，3：支付失败)
            //根据订单状态展示不同的按钮
            switch (orderModel.getTradeState()) {
                case 1:
                    //初始化
                    btn_print.setVisibility(View.GONE);
                    btn_order_syn.setVisibility(View.VISIBLE);
                    tv_order_status.setTextColor(getResources().getColor(R.color.pay_other_status));
                    break;
                case 2:
                    //支付成功
                    //订单详情类型：1.消费、2.消费撤销、3.预授权、4.预授权撤销、5.退货、6.预授权完成、7.预授权完成撤销、8.消费冲正、9.撤销冲正、10.预授权冲正、
                    // 11.预授权撤销冲正、12.预授权完成冲正、13.预授权完成撤销冲正
                    //小票打印：当订单的交易类型为 【1.消费、2.消费撤销、3.预授权、4.预授权撤销、5.退货、6.预授权完成、7.预授权完成撤销】时，
                    // 且订单状态为 2成功时，展示‘打印小票’ button
                    if (orderModel.getTradeType().equalsIgnoreCase("1") ||
                            orderModel.getTradeType().equalsIgnoreCase("2") ||
                            orderModel.getTradeType().equalsIgnoreCase("3") ||
                            orderModel.getTradeType().equalsIgnoreCase("4") ||
                            orderModel.getTradeType().equalsIgnoreCase("5") ||
                            orderModel.getTradeType().equalsIgnoreCase("6") ||
                            orderModel.getTradeType().equalsIgnoreCase("7")) {
                        btn_print.setVisibility(View.VISIBLE);
                    } else {
                        btn_print.setVisibility(View.GONE);
                    }

                    btn_order_syn.setVisibility(View.GONE);
                    tv_order_status.setTextColor(getResources().getColor(R.color.bill_item_succ));
                    //当交易类型为 1消费 或者 6预授权完成
                    if (orderModel.getTradeType().equalsIgnoreCase("1") ||
                            orderModel.getTradeType().equalsIgnoreCase("6")) {
                        if (orderModel.getRefundMark() != null) {
                            if (orderModel.getRefundMark() == 1) {
                                if (orderModel.getRefundRemainFee() <= 0) {
                                    titleBar.setRightButLayVisibleForTotal(false, getString(R.string.title_order_refund));
                                } else {
                                    if (MainApplication.getInstance().isAdmin(1) || (MainApplication.getInstance().isAdmin(0)
                                            && MainApplication.getInstance().isRefundAuth("1"))) {
                                        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.title_order_refund));
                                    }
                                }
                            } else if (orderModel.getRefundMark() == 0) {
                                titleBar.setRightButLayVisibleForTotal(false, getString(R.string.title_order_refund));
                            }
                        }
                    } else {
                        titleBar.setRightButLayVisibleForTotal(false, getString(R.string.title_order_refund));
                    }
                    break;
                case 3:
                    //支付失败
                    btn_print.setVisibility(View.GONE);
                    btn_order_syn.setVisibility(View.GONE);
                    tv_order_status.setTextColor(getResources().getColor(R.color.pay_other_status));
                    break;
                default:
                    btn_print.setVisibility(View.GONE);
                    btn_order_syn.setVisibility(View.VISIBLE);
                    tv_order_status.setTextColor(getResources().getColor(R.color.pay_other_status));
                    break;

            }
            tv_order_status.setText(orderModel.getTradeStateText());

            //订单终态---1.已撤销 2.已冲正
            if (orderModel.getOrderfinalStatus() != null && !TextUtils.isEmpty(orderModel.getOrderfinalStatus())) {
                //只有为1 和 2 的时候才展示，除此之外，都不展示
                if (orderModel.getOrderfinalStatus().equalsIgnoreCase("1")) {
                    ll_card_payment_order_final_status.setVisibility(View.VISIBLE);
                    view_final_status_line.setVisibility(View.VISIBLE);

                    tv_order_final_status.setText(getStringById(R.string.card_payment_final_status_canceled));
                } else if (orderModel.getOrderfinalStatus().equalsIgnoreCase("2")) {
                    ll_card_payment_order_final_status.setVisibility(View.VISIBLE);
                    view_final_status_line.setVisibility(View.VISIBLE);

                    tv_order_final_status.setText(getStringById(R.string.card_payment_final_status_reversal));
                } else {
                    ll_card_payment_order_final_status.setVisibility(View.GONE);
                    view_final_status_line.setVisibility(View.GONE);
                }
            } else {
                ll_card_payment_order_final_status.setVisibility(View.GONE);
                view_final_status_line.setVisibility(View.GONE);
            }

            //交易时间
            tv_addtime.setText(orderModel.getTradeTimeNew());
            if (orderModel.getTradeTimeNew() == null || TextUtils.isEmpty(orderModel.getTradeTimeNew())) {
                tv_addtime.setText(orderModel.getNotifyTime());
            }
            //收银员展示
            if (!TextUtils.isEmpty(orderModel.getUserName()) && !StringUtil.isEmptyOrNull(orderModel.getUseId())) {
                ll_cashier_layout.setVisibility(View.VISIBLE);
                id_line_cashier.setVisibility(View.VISIBLE);
                tv_cashier_name.setText(orderModel.getUserName());
                isCashierVisiable = true;
            } else {
                isCashierVisiable = false;
                ll_cashier_layout.setVisibility(View.GONE);
                id_line_cashier.setVisibility(View.GONE);
            }
            //支付方式
            tv_payment_method.setText(orderModel.getTradeName());
            //第三方订单号
            if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getPayTypeMap().get(orderModel.getApiCode()))) {
                String language = PreferenceUtil.getString("language", "");
                ;
                //zh-rHK
                Locale locale = MainApplication.getInstance().getResources().getConfiguration().locale;
                String lan = locale.getCountry();
                if (!TextUtils.isEmpty(language)) {
                    if (language.equals(Constant.LANG_CODE_EN_US)) {
                        tv_order_num_title_info.setText(MainApplication.getInstance().getPayTypeMap().get(orderModel.getApiCode()) + " " + getString(R.string.tx_orderno));
                    } else {
                        tv_order_num_title_info.setText(MainApplication.getInstance().getPayTypeMap().get(orderModel.getApiCode()) + getString(R.string.tx_orderno));
                    }
                } else {
                    if (lan.equalsIgnoreCase("en")) {
                        tv_order_num_title_info.setText(MainApplication.getInstance().getPayTypeMap().get(orderModel.getApiCode()) + " " + getString(R.string.tx_orderno));
                    } else {
                        tv_order_num_title_info.setText(MainApplication.getInstance().getPayTypeMap().get(orderModel.getApiCode()) + getString(R.string.tx_orderno));
                    }

                }

            }
            if (!TextUtils.isEmpty(orderModel.getTransactionId())) {
                ll_order_num.setVisibility(View.VISIBLE);
                view_order_num_line.setVisibility(View.VISIBLE);
                tv_OrderNum.setText(orderModel.getTransactionId());
            } else {
                ll_order_num.setVisibility(View.GONE);
                view_order_num_line.setVisibility(View.GONE);
            }

            //平台订单号
            tv_plantform_order_id.setText(orderModel.getOrderNoMch());
            //平台订单一维码
            // 生成一维码
            if (!isAbsoluteNullStr(orderModel.getOrderNoMch())) {
                ll_big_one_code.setVisibility(View.VISIBLE);
                WindowManager wm = this.getWindowManager();
                int width = wm.getDefaultDisplay().getWidth();
                final int w = (int) (width * 0.85);
                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            iv_code.setImageBitmap(CreateOneDiCodeUtil.createCode(orderModel.getOrderNoMch(), w, 180));
                        }
                    });
                } catch (Exception e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                }
                tv_code.setText(orderModel.getOrderNoMch());
            } else {
                ll_big_one_code.setVisibility(View.GONE);
            }

            //付款备注
            if (!StringUtil.isEmptyOrNull(orderModel.getAttach())) {
                ll_attach.setVisibility(View.VISIBLE);
                line_attach.setVisibility(View.VISIBLE);
                tv_attach.setText(orderModel.getAttach());
            }
        }
    }

    public void setListener() {
        btn_order_syn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //同步订单状态，去查询卡交易的单
                queryMasterCardOrderStatus(orderModel.getOutTradeNo());
            }
        });

        btn_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bluePrint();
            }
        });
    }


    @Override
    public void syncMasterCardOrderStatusByOrderNoSuccess(Order response) {
        if (response != null) {
            //交易状态：(1.初始化，2：支付成功，3：支付失败),初始化不用更新UI
            if (response.getTradeState() == 2 || response.getTradeState() == 3) {
                orderModel = response;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initUIWithData();
                    }
                });
            } else {
                toastDialog(MasterCardOrderDetailsActivity.this, R.string.card_payment_sync_order, new NewDialogInfo.HandleBtn() {
                    @Override
                    public void handleOkBtn() {
                        finish();
                    }
                });
            }
        } else {
            showToastInfo(R.string.show_order_syn);
        }
    }

    @Override
    public void syncMasterCardOrderStatusByOrderNoFailed(Object error) {
        showToastInfo(error.toString());
    }

    //平台订单同步
    public void queryMasterCardOrderStatus(String outTradeNo) {
        if (mPresenter != null) {
            mPresenter.syncMasterCardOrderStatusByOrderNo(outTradeNo);
        }
    }

    //跳转到专门的展示一维码的界面
    public void showBigOneCode(View view) {
        if (orderModel != null) {
            RotateCanvasViewActivity.startActivity(MasterCardOrderDetailsActivity.this, orderModel.getOrderNoMch());
        }
    }

    /**
     * 打印小票
     */
    public void bluePrint() {
        LocaleUtils.switchLanguage(MasterCardOrderDetailsActivity.this);

        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印
        if (IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)) {
            PrintOrder.printMasterCardOrderDetails(MasterCardOrderDetailsActivity.this, isCashierVisiable, true, true, getString(R.string.tv_pay_user_stub), orderModel);
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    try {
                        PrintOrder.printMasterCardOrderDetails(MasterCardOrderDetailsActivity.this, isCashierVisiable, true, true, getString(R.string.tv_pay_mch_stub), orderModel);
                    } catch (Exception e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                    }
                }
            };
            Timer timer = new Timer();
            timer.schedule(task, PrintClient.getInstance().getPrintDelay());
        } else {
            //关闭蓝牙打印
            bluePrintWithBlueToothPermission();
        }
    }


    /**
     * 获得蓝牙权限后进行打印
     */
    private void bluePrintWithBlueToothPermission() {
        if (!MainApplication.getInstance().getBluePrintSetting() && !BuildConfig.IS_POS_VERSION) {
            showDialog();
            return;
        }

        if (MainApplication.getInstance().getBluetoothSocket() != null && MainApplication.getInstance().getBluetoothSocket().isConnected()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        BluePrintUtil.printMasterCardOrder(isCashierVisiable, true, true, getString(R.string.tv_pay_user_stub), orderModel);
                        //打印第二联
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                BluePrintUtil.printMasterCardOrder(isCashierVisiable, true, true, getString(R.string.tv_pay_mch_stub), orderModel);
                            }
                        }, 3000);
                    } catch (Exception e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                    }
                }
            });
        } else {
            String bluetoothDeviceAddress = MainApplication.getInstance().getBlueDeviceAddress();
            if (!StringUtil.isEmptyOrNull(bluetoothDeviceAddress)) {
                boolean isSucc = BluePrintUtil.blueConnent(bluetoothDeviceAddress, MasterCardOrderDetailsActivity.this);
                if (isSucc) {
                    try {
                        BluePrintUtil.printMasterCardOrder(isCashierVisiable, true, true, getString(R.string.tv_pay_user_stub), orderModel);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                BluePrintUtil.printMasterCardOrder(isCashierVisiable, true, true, getString(R.string.tv_pay_mch_stub), orderModel);
                            }
                        }, 3000);
                    } catch (Exception e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                    }
                }
            } else {
                showDialog();
            }
        }
    }


    //未连接蓝牙的弹框
    public void showDialog() {
        dialog = new DialogInfo(MasterCardOrderDetailsActivity.this, null,
                getString(R.string.tx_blue_set), getString(R.string.title_setting),
                getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG,
                new DialogInfo.HandleBtn() {
                    @Override
                    public void handleOkBtn() {
                        showPage(BluetoothSettingActivity.class);
                        dialog.cancel();
                        dialog.dismiss();
                    }

                    @Override
                    public void handleCancelBtn() {
                        dialog.cancel();
                    }
                }, null);
        DialogHelper.resize(MasterCardOrderDetailsActivity.this, dialog);
        dialog.show();
    }

}
