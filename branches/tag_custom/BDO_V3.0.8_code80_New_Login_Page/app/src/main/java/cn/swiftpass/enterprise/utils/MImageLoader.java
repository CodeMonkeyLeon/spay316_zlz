package cn.swiftpass.enterprise.utils;

import android.widget.ImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;


public class MImageLoader extends ImageLoader {
	private volatile static MImageLoader instance=null;
	private DisplayImageOptions options;
	private MImageLoader(){}

	public static MImageLoader getInstance(){
		if (instance == null) {
			synchronized (MImageLoader.class) {
				if (instance == null) {
					instance = new MImageLoader();
				}
			}
		}
		return instance;
	}

	public void loadImage(ImageView imageView, String url, int defaultImageResId, int errorImageResId) {
		displayImage(url, imageView, getOptions(defaultImageResId,errorImageResId));
	}

	public void loadImageRound(ImageView imageView, String url, int defaultImageResId, int errorImageResId) {
		displayImage(url, imageView, getRoundImageOption(defaultImageResId,errorImageResId));
	}

	//圆角图片options
	public DisplayImageOptions getRoundImageOption(int defaultImageResId, int errorImageResId){
		options = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(errorImageResId)
				.showImageOnFail(errorImageResId)
				.cacheInMemory(true)
				.displayer(new RoundedBitmapDisplayer(20))
				.build();
		return options;

	}

	public DisplayImageOptions getOptions(int defaultImageResId, int errorImageResId){
		options = new DisplayImageOptions.Builder()
		.showImageForEmptyUri(errorImageResId)
		.showImageOnFail(errorImageResId)
		.cacheInMemory(true)
		.build();
		return options;
	}
}
