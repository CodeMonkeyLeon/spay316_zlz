package cn.swiftpass.enterprise.ui.paymentlink.model;

import java.io.Serializable;

/**
 * Created by congwei.li on 2021/9/27.
 *
 * @Description:
 */
public class ImageUrl implements Serializable {
    public String picUrl;
}
