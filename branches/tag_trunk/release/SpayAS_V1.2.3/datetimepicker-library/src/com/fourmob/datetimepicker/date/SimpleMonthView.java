package com.fourmob.datetimepicker.date;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.fourmob.datetimepicker.R;
import com.fourmob.datetimepicker.Utils;

import java.security.InvalidParameterException;
import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Locale;

public class SimpleMonthView extends View {

    private static final String TAG = SimpleMonthView.class.getSimpleName();
    public static final String VIEW_PARAMS_HEIGHT = "height";

    public static final String VIEW_PARAMS_MONTH = "month";

    public static final String VIEW_PARAMS_YEAR = "year";

    public static final String VIEW_PARAMS_SELECTED_DAY = "selected_day";

    public static final String VIEW_PARAMS_WEEK_START = "week_start";

    public static final String VIEW_PARAMS_NUM_DAYS = "num_days";

    public static final String VIEW_PARAMS_FOCUS_MONTH = "focus_month";

    public static final String VIEW_PARAMS_SHOW_WK_NUM = "show_wk_num";

    private static final int SELECTED_CIRCLE_ALPHA = 60;

    protected static int DEFAULT_HEIGHT = 32;

    protected static final int DEFAULT_NUM_ROWS = 6;

    protected static int DAY_SELECTED_CIRCLE_SIZE;

    protected static int DAY_SEPARATOR_WIDTH = 1;

    protected static int MINI_DAY_NUMBER_TEXT_SIZE;

    protected static int MIN_HEIGHT = 10;

    protected static int MONTH_DAY_LABEL_TEXT_SIZE;

    protected static int MONTH_HEADER_SIZE;

    protected static int MONTH_LABEL_TEXT_SIZE;

    protected static float mScale = 0.0F;

    protected int mPadding = 0;

    private String mDayOfWeekTypeface;

    private String mMonthTitleTypeface;

    protected Paint mMonthDayLabelPaint;

    protected Paint mMonthNumPaint;

    protected Paint mMonthTitleBGPaint;

    protected Paint mMonthTitlePaint;

    protected Paint mSelectedCirclePaint;

    protected Paint titleBg;

    protected int mDayTextColor;

    protected int mMonthTitleBGColor;

    protected int mMonthTitleColor;

    protected int mTodayNumberColor;

    private final StringBuilder mStringBuilder;

    private final Formatter mFormatter;

    protected int mFirstJulianDay = -1;

    protected int mFirstMonth = -1;

    protected int mLastMonth = -1;

    protected boolean mHasToday = false;

    protected int mSelectedDay = -1;

    protected int mToday = -1;

    protected int mWeekStart = 1;

    protected int mNumDays = 7;

    protected int mNumCells = mNumDays;

    protected int mSelectedLeft = -1;

    protected int mSelectedRight = -1;

    private int mDayOfWeekStart = 0;

    protected int mMonth;

    protected int mRowHeight = DEFAULT_HEIGHT;

    protected int mWidth;

    protected int mYear;

    private final Calendar mCalendar;

    private final Calendar mDayLabelCalendar;

    private int mNumRows = DEFAULT_NUM_ROWS;

    private DateFormatSymbols mDateFormatSymbols = new DateFormatSymbols();

    private OnDayClickListener mOnDayClickListener;

    private int currentYear, currentMonth;

    private int currentDay;//当前天

    private final Calendar vCalendar = Calendar.getInstance();

    private Context context;

    private Calendar calendarTag;

    private String MONTH_SUB[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private String DAY_SUB[] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};

    public SimpleMonthView(Context context, int currentYear, int currentMonth, Calendar calendarTag) {
        super(context);
        this.calendarTag = calendarTag;
        this.context = context;
        this.currentMonth = currentMonth;
        this.currentYear = currentYear;
        Resources resources = context.getResources();
        mDayLabelCalendar = Calendar.getInstance();
        mCalendar = Calendar.getInstance();

        mDayOfWeekTypeface = resources.getString(R.string.day_of_week_label_typeface);
        mMonthTitleTypeface = resources.getString(R.string.sans_serif);
        mDayTextColor = resources.getColor(R.color.date_picker_text_normal);
        mTodayNumberColor = resources.getColor(R.color.blue);
        mMonthTitleColor = resources.getColor(R.color.white);
        mMonthTitleBGColor = resources.getColor(R.color.circle_background);

        mStringBuilder = new StringBuilder(50);
        mFormatter = new Formatter(mStringBuilder, Locale.getDefault());

        MINI_DAY_NUMBER_TEXT_SIZE = resources.getDimensionPixelSize(R.dimen.day_number_size);
        MONTH_LABEL_TEXT_SIZE = resources.getDimensionPixelSize(R.dimen.month_label_size);
        MONTH_DAY_LABEL_TEXT_SIZE = resources.getDimensionPixelSize(R.dimen.month_day_label_text_size);
        MONTH_HEADER_SIZE = resources.getDimensionPixelOffset(R.dimen.month_list_item_header_height);
        DAY_SELECTED_CIRCLE_SIZE = resources.getDimensionPixelSize(R.dimen.day_number_select_circle_radius);

        mRowHeight = ((resources.getDimensionPixelOffset(R.dimen.date_picker_view_animator_height) - MONTH_HEADER_SIZE) / 6) + dip2Px(context, 30);

        initView();
    }

    /**
     * 把dp单位的尺寸�?转换为px单位
     *
     * @param context
     * @param dpValue
     * @return
     */
    public int dip2Px(Context context, float dpValue) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    private int calculateNumRows() {
        int offset = findDayOffset();
        int dividend = (offset + mNumCells) / mNumDays;
        int remainder = (offset + mNumCells) % mNumDays;
        return (dividend + (remainder > 0 ? 1 : 0));
    }

    private void drawMonthDayLabels(Canvas canvas) {

        int y = (MONTH_HEADER_SIZE - MONTH_DAY_LABEL_TEXT_SIZE) / 2 + (MONTH_LABEL_TEXT_SIZE / 3);
        int dayWidthHalf = (mWidth - mPadding * 2) / (mNumDays * 2);

        for (int i = 0; i < mNumDays; i++) {
            int calendarDay = (i + mWeekStart) % mNumDays;
            int x = (2 * i + 1) * dayWidthHalf + mPadding;
            mDayLabelCalendar.set(Calendar.DAY_OF_WEEK, calendarDay);

            String week = mDateFormatSymbols.getShortWeekdays()[mDayLabelCalendar.get(Calendar.DAY_OF_WEEK)].toUpperCase(Locale.getDefault());
            if (week.length() == 2) {
                week = (String) week.subSequence(1, week.length());
            }
            int id = mDayLabelCalendar.get(Calendar.DAY_OF_WEEK);
            if (id == 1 || id == 7) {
                mMonthDayLabelPaint.setColor(Color.parseColor("#4182E2"));
            } else {
                mMonthDayLabelPaint.setColor(Color.parseColor("#999999"));
            }
//            if(i==0){
//                titleBg.setColor(Color.RED);
//            }else if(i==1){
//                titleBg.setColor(Color.GREEN);
//            }else if(i==2){
//                titleBg.setColor(Color.BLUE);
//            }else if(i==3){
//                titleBg.setColor(Color.GRAY);
//            }else{
//                titleBg.setColor(Color.BLACK);
//            }

            int rectX = 0;
            if(mWidth <=540){
                rectX = x - 30;
            }else{
                rectX = x - 85;
            }
            //矩形背景
            canvas.drawRect(rectX, y - dip2Px(context, 30), x + dip2Px(context, 50), y + dip2Px(context, 15), titleBg);
            String langeage = PreferenceManager.getDefaultSharedPreferences(context).getString("language", "");
            try {
                if (langeage.equals("en_us")) {
                    if (week.equals("一")) {
                        week = DAY_SUB[0];
                    } else if (week.equals("二")) {
                        week = DAY_SUB[1];

                    } else if (week.equals("三")) {

                        week = DAY_SUB[2];
                    } else if (week.equals("四")) {

                        week = DAY_SUB[3];
                    } else if (week.equals("五")) {
                        week = DAY_SUB[4];

                    } else if (week.equals("六")) {

                        week = DAY_SUB[5];
                    } else if (week.equals("日")) {

                        week = DAY_SUB[6];
                    }

                } else if (langeage.equals("zh_tw")) {

                } else if (langeage.equals("zh_cn")) {

                }
            } catch (NumberFormatException e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }

            float xPos = 0;
            if(mWidth <=540){
                xPos = x- dip2Px(context, 2);
            }else{
                xPos = x - dip2Px(context, 5);
            }
            canvas.drawText(week, xPos, y, mMonthDayLabelPaint);
        }
    }

    private void drawMonthTitle(Canvas canvas) {
        //        int x = (mWidth + 2 * mPadding) / 2;
        int x = (mWidth + 2 * mPadding) - dip2Px(context, 70);
        //        int y = (MONTH_HEADER_SIZE - MONTH_DAY_LABEL_TEXT_SIZE) / 2 + (MONTH_LABEL_TEXT_SIZE / 3);
        int y = MONTH_HEADER_SIZE - (MONTH_DAY_LABEL_TEXT_SIZE / 2) + dip2Px(context, 15);
        //TODO 更改日期2017年11月
        String dateStr = getMonthAndYearString();
        String langeage = PreferenceManager.getDefaultSharedPreferences(context).getString("language", "");

        try {
            if (langeage.equals("en_us")) {
                /*int indexMon = dateStr.indexOf("月");
                int indexYear = dateStr.indexOf("年");
                dateStr = MONTH_SUB[Integer.valueOf(dateStr.substring(indexYear + 1, indexMon)).intValue() - 1]  + dateStr.substring(0, indexYear);
*/
            } else if (langeage.equals("zh_tw")) {

            } else if (langeage.equals("zh_cn")) {

            }
        } catch (NumberFormatException e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }

        //zh_cn en_us zh_tw 2018年1月
        canvas.drawText(dateStr, x, y, mMonthTitlePaint);
    }

    private int findDayOffset() {
        return (mDayOfWeekStart < mWeekStart ? (mDayOfWeekStart + mNumDays) : mDayOfWeekStart) - mWeekStart;
    }

    private String getMonthAndYearString() {
        int flags = DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_NO_MONTH_DAY;
        mStringBuilder.setLength(0);
        long millis = mCalendar.getTimeInMillis();

        //        SimpleDateFormat sdf = new SimpleDateFormat("MM");
        //        try
        //        {
        //            return sdf.format(new Date(millis));
        //        }
        //        catch (Exception e)
        //        {
        //            Log.e(TAG,Log.getStackTraceString(e));
        //            return null;
        //        }


        return DateUtils.formatDateRange(getContext(), millis, millis, flags);
    }

    private void onDayClick(SimpleMonthAdapter.CalendarDay calendarDay) {
        if (mOnDayClickListener != null) {
            mOnDayClickListener.onDayClick(this, calendarDay);
        }
    }

    private boolean sameDay(int monthDay, Time time) {
        return (mYear == time.year) && (mMonth == time.month) && (monthDay == time.monthDay);
    }

    boolean isFirst = true;

    boolean isTwo = true;

    protected void drawMonthNums(Canvas canvas) {
        int y = (mRowHeight + MINI_DAY_NUMBER_TEXT_SIZE) / 2 - DAY_SEPARATOR_WIDTH + MONTH_HEADER_SIZE;
        int paddingDay = (mWidth - 2 * mPadding) / (2 * mNumDays);
        int dayOffset = findDayOffset();
        int day = 1;

        while (day <= mNumCells) {
            int x = paddingDay * (1 + dayOffset * 2) + mPadding;

            mCalendar.set(Calendar.DAY_OF_MONTH, day);
            Calendar ca = Calendar.getInstance();//得到一个Calendar的实例 
            //            ca.add(Calendar.MONTH, -2);
            ca.add(Calendar.DAY_OF_MONTH, -90);
            if (null != calendarTag) {
                if ((mCalendar.getTimeInMillis() >= calendarTag.getTimeInMillis()) && mCalendar.getTimeInMillis() <= getBeforeDate()) {
                    mMonthNumPaint.setColor(mDayTextColor);
                    //                    if (mHasToday && (mToday == day) && isFirst)
                    //                    {
                    //                        //                    mMonthNumPaint.setColor(mTodayNumberColor);
                    //                        isFirst = false;
                    //                        mMonthNumPaint.setColor(Color.WHITE);
                    //                        canvas.drawRect(x - dip2Px(context, 20), y - dip2Px(context, 10), x + dip2Px(context, 20), y
                    //                            + dip2Px(context, 25), mSelectedCirclePaint);
                    //                    }
                    if (mSelectedDay == day) {
                        mMonthNumPaint.setColor(Color.WHITE);
                        canvas.drawRect(x - dip2Px(context, 20), y - dip2Px(context, 10), x + dip2Px(context, 20), y + dip2Px(context, 25), mSelectedCirclePaint);
                    }
                } else {
                    mMonthNumPaint.setColor(Color.parseColor("#cccccc"));
                }
            } else {
                if ((mCalendar.getTimeInMillis() > ca.getTimeInMillis()) && mCalendar.getTimeInMillis() <= getBeforeDate()) {
                    mMonthNumPaint.setColor(mDayTextColor);
                    if (mSelectedDay == day) {
                        mMonthNumPaint.setColor(Color.WHITE);
                        canvas.drawRect(x - dip2Px(context, 20), y - dip2Px(context, 10), x + dip2Px(context, 20), y + dip2Px(context, 25), mSelectedCirclePaint);
                    }
                } else {
                    mMonthNumPaint.setColor(Color.parseColor("#cccccc"));
                }

            }

            canvas.drawText(String.format("%d", day), x, y + 30, mMonthNumPaint);

            dayOffset++;
            if (dayOffset == mNumDays) {
                dayOffset = 0;
                y += mRowHeight;
            }
            day++;
        }
    }

    /**
     * 得到当前的前一天
     * <功能详细描述>
     *
     * @return
     * @see [类、类#方法、类#成员]
     */
    public long getBeforeDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 0);

        return calendar.getTimeInMillis() + 10000;
    }

    public SimpleMonthAdapter.CalendarDay getDayFromLocation(float x, float y) {
        int padding = mPadding;
        if ((x < padding) || (x > mWidth - mPadding)) {
            return null;
        }

        int yDay = (int) (y - MONTH_HEADER_SIZE) / mRowHeight;
        int day = 1 + ((int) ((x - padding) * mNumDays / (mWidth - padding - mPadding)) - findDayOffset()) + yDay * mNumDays;

        return new SimpleMonthAdapter.CalendarDay(mYear, mMonth, day);
    }

    protected void initView() {
        mMonthTitlePaint = new Paint();
        mMonthTitlePaint.setFakeBoldText(false);
        mMonthTitlePaint.setAntiAlias(true);
        mMonthTitlePaint.setTextSize(MONTH_LABEL_TEXT_SIZE);
        mMonthTitlePaint.setTypeface(Typeface.create(mMonthTitleTypeface, Typeface.NORMAL));
        mMonthTitlePaint.setColor(Color.parseColor("#000000"));
        mMonthTitlePaint.setTextAlign(Align.CENTER);
        mMonthTitlePaint.setStyle(Style.FILL);

        mMonthTitleBGPaint = new Paint();
        mMonthTitleBGPaint.setFakeBoldText(true);
        mMonthTitleBGPaint.setAntiAlias(true);
        mMonthTitleBGPaint.setColor(mMonthTitleBGColor);
        mMonthTitleBGPaint.setTextAlign(Align.CENTER);
        mMonthTitleBGPaint.setStyle(Style.FILL);

        mSelectedCirclePaint = new Paint();
        mSelectedCirclePaint.setFakeBoldText(true);
        mSelectedCirclePaint.setAntiAlias(true);
        mSelectedCirclePaint.setColor(Color.parseColor("#4182E2"));
        mSelectedCirclePaint.setTextAlign(Align.CENTER);
        mSelectedCirclePaint.setStyle(Style.FILL);
        //        mSelectedCirclePaint.setAlpha(SELECTED_CIRCLE_ALPHA);
        mSelectedCirclePaint.setTypeface(Typeface.DEFAULT_BOLD);

        titleBg = new Paint();
        titleBg.setFakeBoldText(true);
        titleBg.setAntiAlias(true);
        titleBg.setColor(Color.parseColor("#f5f5f5"));
        titleBg.setTextAlign(Align.CENTER);
        titleBg.setStyle(Style.FILL);
        //        mSelectedCirclePaint.setAlpha(SELECTED_CIRCLE_ALPHA);

        mMonthDayLabelPaint = new Paint();
        mMonthDayLabelPaint.setAntiAlias(true);
        mMonthDayLabelPaint.setTextSize(MONTH_DAY_LABEL_TEXT_SIZE);
        mMonthDayLabelPaint.setColor(mDayTextColor);
        mMonthDayLabelPaint.setTypeface(Typeface.create(mDayOfWeekTypeface, Typeface.NORMAL));
        mMonthDayLabelPaint.setStyle(Style.FILL);
        mMonthDayLabelPaint.setTextAlign(Align.CENTER);
        mMonthDayLabelPaint.setFakeBoldText(false);

        mMonthNumPaint = new Paint();
        mMonthNumPaint.setAntiAlias(true);
        mMonthNumPaint.setTextSize(MINI_DAY_NUMBER_TEXT_SIZE);
        mMonthNumPaint.setStyle(Style.FILL);
        mMonthNumPaint.setTextAlign(Align.CENTER);
        mMonthDayLabelPaint.setFakeBoldText(true);
        //        mMonthNumPaint.setTypeface(Typeface.create(mDayOfWeekTypeface, Typeface.DEFAULT_BOLD));
        mMonthNumPaint.setTypeface(Typeface.DEFAULT_BOLD);
        mMonthNumPaint.setFakeBoldText(false);
    }

    protected void onDraw(Canvas canvas) {
        drawMonthDayLabels(canvas);
        drawMonthTitle(canvas);
        drawMonthNums(canvas);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(View.MeasureSpec.getSize(widthMeasureSpec), mRowHeight * mNumRows + MONTH_HEADER_SIZE);
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        mWidth = w;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            SimpleMonthAdapter.CalendarDay calendarDay = getDayFromLocation(event.getX(), event.getY());

            Calendar ca = Calendar.getInstance();//得到一个Calendar的实例 
            //            ca.add(Calendar.MONTH, -2);
            ca.add(Calendar.DAY_OF_MONTH, -90);

            Calendar ca2 = Calendar.getInstance();
            ca2.set(calendarDay.year, calendarDay.month, calendarDay.day);
            if (null != calendarTag) {
                if ((ca2.getTimeInMillis() >= calendarTag.getTimeInMillis()) && ca2.getTimeInMillis() <= getBeforeDate()) {
                    if (calendarDay != null) {
                        onDayClick(calendarDay);
                    }
                }
            } else {

                if ((ca2.getTimeInMillis() > ca.getTimeInMillis()) && ca2.getTimeInMillis() <= getBeforeDate()) {
                    if (calendarDay != null) {
                        onDayClick(calendarDay);
                    }
                }
            }

        }
        return true;
    }

    public void reuse() {
        mNumRows = DEFAULT_NUM_ROWS;
        requestLayout();
    }

    public void setMonthParams(HashMap<String, Integer> params) {
        if (!params.containsKey(VIEW_PARAMS_MONTH) && !params.containsKey(VIEW_PARAMS_YEAR)) {
            throw new InvalidParameterException("You must specify month and year for this view");
        }
        setTag(params);

        if (params.containsKey(VIEW_PARAMS_HEIGHT)) {
            mRowHeight = params.get(VIEW_PARAMS_HEIGHT);
            if (mRowHeight < MIN_HEIGHT) {
                mRowHeight = MIN_HEIGHT;
            }
        }
        if (params.containsKey(VIEW_PARAMS_SELECTED_DAY)) {
            mSelectedDay = params.get(VIEW_PARAMS_SELECTED_DAY);
        }

        mMonth = params.get(VIEW_PARAMS_MONTH);
        mYear = params.get(VIEW_PARAMS_YEAR);

        final Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();
        mHasToday = false;
        mToday = -1;

        mCalendar.set(Calendar.MONTH, mMonth);
        mCalendar.set(Calendar.YEAR, mYear);
        mCalendar.set(Calendar.DAY_OF_MONTH, 1);
        mDayOfWeekStart = mCalendar.get(Calendar.DAY_OF_WEEK);

        if (params.containsKey(VIEW_PARAMS_WEEK_START)) {
            mWeekStart = params.get(VIEW_PARAMS_WEEK_START);
        } else {
            mWeekStart = mCalendar.getFirstDayOfWeek();
        }

        mNumCells = Utils.getDaysInMonth(mMonth, mYear);
        for (int i = 0; i < mNumCells; i++) {
            final int day = i + 1;
            if (sameDay(day, today)) {
                mHasToday = true;
                mToday = day;
            }
        }

        mNumRows = calculateNumRows();
    }

    public void setOnDayClickListener(OnDayClickListener onDayClickListener) {
        mOnDayClickListener = onDayClickListener;
    }

    public static abstract interface OnDayClickListener {
        public abstract void onDayClick(SimpleMonthView simpleMonthView, SimpleMonthAdapter.CalendarDay calendarDay);
    }
}