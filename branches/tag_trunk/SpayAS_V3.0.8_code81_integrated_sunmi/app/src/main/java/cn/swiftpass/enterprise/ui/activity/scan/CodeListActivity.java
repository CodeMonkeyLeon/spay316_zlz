package cn.swiftpass.enterprise.ui.activity.scan;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.zxing.WriterException;
import com.tencent.stat.StatService;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bean.QRCodeBean;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.MaxCardManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.AddCodeActivity;
import cn.swiftpass.enterprise.ui.activity.StaticCodeActivity;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.bill.BillMainActivity;
import cn.swiftpass.enterprise.ui.activity.bill.UserListActivity;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DataReportUtils;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DisplayUtil;
import cn.swiftpass.enterprise.utils.GlideApp;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.ImageUtil;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;
import handmark.pulltorefresh.library.PullToRefreshBase;
import handmark.pulltorefresh.library.PullToRefreshGridView;
import handmark.pulltorefresh.library.internal.LoadingLayout;

import static cn.swiftpass.enterprise.MainApplication.getContext;

/**
 * 二维码列表
 *
 * @author jamy
 */
public class CodeListActivity extends TemplateActivity implements AdapterView.OnItemClickListener {
    private static final String TAG = CodeListActivity.class.getSimpleName();
    private LinearLayout mInputLayout;
    private EditText et_input;
    private PullToRefreshGridView pull_gridview;
    private CodeAdapter adapter;
    private GridView gridView;
    private LinearLayout layout_pay_foot;
    private LinearLayout layout_refresh_foot;
    private TextView tv_nomore_foot,tv_refresh_text,tv_release_text;

    private LinearLayout layout_pay_head;
    private LinearLayout layout_refresh_head;
    LoadingLayout mFootView = null;
    LoadingLayout mHeadView = null;
    private Context context;
    private List<QRCodeBean> codes;
    private int page = 1;
    /**
     * 是否是刷新操作
     */
    private boolean isRefresh = true;
    /**
     * 判断是否还有更多数据
     */
    private boolean isNomore = false;
    private TextView mEmptyView;
    private String bindUserId = "";
    UserModel userModel;
    /**
     * 没有更多数据
     */
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            if (msg.what == HandlerManager.BIND_CODE_FINISH) {
                    isRefresh = true;
                    isNomore = false;
                    page = 1;
                    codes.clear();
                    adapter.notifyDataSetChanged();
                    getData(isRefresh,page,bindUserId);
            }else if (msg.what == HandlerManager.CHOICE_CASHIER_STATIC_CODE){
                page = 1;
                userModel = (UserModel) msg.obj;
                isRefresh = true;
                isNomore = false;
                codes.clear();
                adapter.notifyDataSetChanged();
                if (null != userModel){
                    bindUserId = String.valueOf(userModel.getId());
                    getData(isRefresh,page,bindUserId);
                }else{
                    bindUserId = "";
                    getData(isRefresh,page,bindUserId);
                }
            }
        }

    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_list);
        MainApplication.listActivities.add(this);
        HandlerManager.registerHandler(HandlerManager.BIND_CODE_FINISH, handler);
        HandlerManager.registerHandler(HandlerManager.CHOICE_CASHIER_STATIC_CODE, handler);
        context = this;
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        initView();
        if (MainApplication.isAdmin.equals("0"))
        {
            if (MainApplication.userId > 0) {
                bindUserId = MainApplication.userId+"";
            } else {
                bindUserId = MainApplication.getUserId()+"";
            }
            mInputLayout.setVisibility(View.GONE);
        }else{
            mInputLayout.setVisibility(View.VISIBLE);
        }
        getData(isRefresh,page,bindUserId);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(R.string.tv_scan_prompt3);
        titleBar.setLeftButtonVisible(true);
        titleBar.setRightButtonVisible(false);
        titleBar.setRightButLayVisible(true, 0);
        titleBar.setRightButLayBackground(R.drawable.icon_add);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {

            @Override
            public void onRightLayClick()
            {

            }

            @Override
            public void onRightButtonClick()
            {

            }

            @Override
            public void onRightButLayClick()
            {
                try {
                    StatService.trackCustomEvent(CodeListActivity.this, "kMTASPayMeQRCodeBind", "绑定二维码入口");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                // 1.携带参数的打点
                Bundle value1 = new Bundle();
                value1.putString("kGFASPayMeQRCodeBind","绑定二维码入口");
                DataReportUtils.getInstance().report("kGFASPayMeQRCodeBind",value1);

                Intent intent = new Intent(CodeListActivity.this, AddCodeActivity.class);
                startActivity(intent);
            }

            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
        });
    }

    private void initView(){
        codes = new ArrayList<QRCodeBean>();
        mInputLayout = getViewById(R.id.ly_input);
        et_input = getViewById(R.id.et_input);
        mEmptyView = getViewById(R.id.ly_cashier_no);
        et_input.setFocusable(true);
        et_input.setFocusableInTouchMode(true);
        et_input.requestFocus();
        et_input.setOnTouchListener(new View.OnTouchListener()
        {

            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    try {
                        StatService.trackCustomEvent(CodeListActivity.this, "kMTASPayMeQRCodeSearchCashier", "店铺二维码搜索入口");
                    } catch (Exception e) {
                        Log.e(TAG,Log.getStackTraceString(e));
                    }

                    // 1.携带参数的打点
                    Bundle value1 = new Bundle();
                    value1.putString("kGFASPayMeQRCodeSearchCashier","店铺二维码搜索入口");
                    DataReportUtils.getInstance().report("kGFASPayMeQRCodeSearchCashier",value1);
                    showPage(UserListActivity.class);
                }
                return true;
            }
        });

        pull_gridview = getViewById(R.id.pull_gridView);
        gridView = pull_gridview.getRefreshableView();
        gridView.setEmptyView(mEmptyView);

        gridView.setOnItemClickListener(this);
        pull_gridview.setScrollingWhileRefreshingEnabled(true);
        pull_gridview.setTag(0x11);
        pull_gridview.setMode(PullToRefreshBase.Mode.BOTH);
        adapter = new CodeAdapter();
        gridView.setAdapter(adapter);
        mFootView = pull_gridview.getFooterLayout();
        mHeadView = pull_gridview.getHeaderLayout();
        if (null != mFootView){
            layout_pay_foot = mFootView.findViewById(R.id.layout_pay);
            tv_nomore_foot = mFootView.findViewById(R.id.pull_to_nomore_text);
            tv_refresh_text = mFootView.findViewById(R.id.pull_to_refresh_text);
            tv_release_text = mFootView.findViewById(R.id.pull_to_refresh_sub_text);
            layout_refresh_foot = mFootView.findViewById(R.id.layout_refresh);
            layout_refresh_foot.setVisibility(View.VISIBLE);
        }
        if (null != mHeadView){
            layout_pay_foot = mHeadView.findViewById(R.id.layout_pay);
            layout_refresh_head = mHeadView.findViewById(R.id.layout_refresh);
            layout_refresh_head.setVisibility(View.VISIBLE);
        }

        pull_gridview.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<GridView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase refreshView) {
                page = 1;
                isRefresh = true;
                isNomore = false;
                getData(isRefresh,page,bindUserId);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase refreshView) {
                isRefresh = false;
                if (!isNomore){
                    getData(isRefresh,page,bindUserId);
                }else{
                    //没有更多数据则显示到底了
                    pull_gridview.onRefreshComplete();
                }
            }
        });
    }

    @Override
    public void showPage(Class clazz) {
        Intent intent = new Intent(CodeListActivity.this, UserListActivity.class);
        if (null != userModel){
            intent.putExtra("userModel", userModel);
        }
        intent.putExtra("isFromBillEnter",false);
        intent.putExtra("isFromCodeListEnter",true);
        startActivity(intent);
    }

    private void getData(final boolean isLoadMore, int pageNo, final String bindUserId){
        OrderManager.getInstance().GetCodeList(pageNo,20,bindUserId,new UINotifyListener<List<QRCodeBean>>(){
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                loadDialog(CodeListActivity.this, R.string.public_data_loading);
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                dismissLoading();
                if (checkSession()) {
                    return;
                }
                if (object != null) {
                    CodeListActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toastDialog(CodeListActivity.this, object.toString(), null);
                        }
                    });
                }else{
                    CodeListActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toastDialog(CodeListActivity.this, getResources().getString(R.string.error_msg), null);
                        }
                    });
                }
                CodeListActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pull_gridview.onRefreshComplete();
                    }
                });
            }

            @Override
            public void onSucceed(List<QRCodeBean> result) {
                super.onSucceed(result);
                dismissLoading();
                int lenth = result.size();
               if (lenth == 0){
                  if (isRefresh){
                      codes.clear();
                  }else{
                      isNomore = true;
                  }
               }else{
                   if (isRefresh){
                       codes.clear();
                   }
                   page++;
                   codes.addAll(result);
                   removeDuplicateWithOrder(codes);
                   if (lenth < 20){
                       isNomore = true;

                   }
               }
               //如果没有更多数据则显示到底了
               if (isNomore){
                   if (null != tv_refresh_text && null != tv_nomore_foot){
                       tv_refresh_text.setVisibility(View.GONE);
                       tv_nomore_foot.setVisibility(View.VISIBLE);
                   }
               }else{
                   if (null != tv_refresh_text && null != tv_nomore_foot){
                       tv_refresh_text.setVisibility(View.VISIBLE);
                       tv_nomore_foot.setVisibility(View.GONE);
                   }
               }
                adapter.notifyDataSetChanged();
                pull_gridview.onRefreshComplete();
            }
        });
    }

    /**
     * 删除ArrayList中重复元素，保持顺序
     * @param list
     * @return
     */
    public static void removeDuplicateWithOrder(List list) {
        Set set = new HashSet();
        List newList = new ArrayList();
        for (Iterator iter = list.iterator(); iter.hasNext();) {
            Object element = iter.next();
            if (set.add(element))
            {
                newList.add(element);
            }
        }
        list.clear();
        list.addAll(newList);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (codes.size() > 0){
            QRCodeBean qrCodeBean =codes.get(position);
            go2StaticCodeActivity(qrCodeBean);
        }

    }

    private void go2StaticCodeActivity(QRCodeBean qrCodeBean){
        Intent intent = new Intent(this, StaticCodeActivity.class);
        qrCodeBean.switch_type = 1;
        intent.putExtra("QRCodeBean",qrCodeBean);
        this.startActivity(intent);
    }

    private class CodeAdapter extends BaseAdapter
    {


        public CodeAdapter() {
        }

        @Override
        public int getCount() {
            return codes.size();
        }

        @Override
        public Object getItem(int position) {
            return codes.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.code_list_item, null);
                holder = new ViewHolder();
                holder.ali_pay_layout = convertView.findViewById(R.id.ali_pay_layout);
                holder.all_pay_layout = convertView.findViewById(R.id.all_pay_layout);
                holder.ll_other_union_pay_layout = convertView.findViewById(R.id.ll_other_union_pay_layout);
                holder.icon_other_union_pay = convertView.findViewById(R.id.icon_other_union_pay);
                holder.tv_other_UnionPay = convertView.findViewById(R.id.tv_other_UnionPay);
                holder.iv_other_union_code_image = convertView.findViewById(R.id.iv_other_union_code_image);
                holder.tv_code_number = convertView.findViewById(R.id.code_number);
                holder.tv_code_id = convertView.findViewById(R.id.code_id);
                holder.view_line = convertView.findViewById(R.id.view_line);
                holder.iv_code_image = convertView.findViewById(R.id.code_image);
                holder.iv_icon_ali = convertView.findViewById(R.id.icon_ali);
                holder.tv_ali = convertView.findViewById(R.id.tv_ali);
                holder.iv_ali_code_image = convertView.findViewById(R.id.ali_code_image);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            //读取支付宝，银联二维码，Gcash码等的图标
            Object object = SharedPreUtile.readProduct("payTypeIcon" + ApiConstant.bankCode + MainApplication.merchantId);
            String code_content = ApiConstant.BASE_URL_PORT+"spay/scanQr?qrcodeId="+codes.get(position).qrCodeId;
            Bitmap logo = ImageUtil.base64ToBitmap(codes.get(position).qrLogo);
            Map<String, String> typePicMap = (Map<String, String>) object;
            //如果为其他通道二维码（银联二维码，Gcash码等）直接获取qrCodeInfo属性
            if (codes.get(position).qrType == 20){
                code_content = codes.get(position).qrCodeInfo;
            }
            Bitmap bitmap = null;
            //如果为其他通道二维码（银联二维码，Gcash码等）则动态改变二维码大小
            if(!TextUtils.isEmpty(code_content)){
                if (codes.get(position).qrType == 20){
                    bitmap = createCode(CodeListActivity.this,code_content,120,120,logo);
                }else if (1 == codes.get(position).qrType){
                    bitmap = createCode(CodeListActivity.this,code_content,105,105,logo);
                }else{
                    bitmap = createCode(CodeListActivity.this,code_content,120,120,logo);
                }
            }
            //如果为其他通道二维码（银联二维码，Gcash码等）
            if(codes.get(position).qrType == 20 ){
                holder.view_line.setVisibility(View.GONE);
                holder.tv_code_id.setVisibility(View.GONE);
            }else {
                holder.view_line.setVisibility(View.VISIBLE);
                holder.tv_code_id.setVisibility(View.VISIBLE);
                holder.tv_code_id.setText("ID:"+codes.get(position).Id+"");
            }

            String[] allpayLogos=codes.get(position).payLogos.split(",");
            /*if(!TextUtils.isEmpty(codes.get(position).cashierDesk)){
                holder.tv_code_number.setText(codes.get(position).cashierDesk);
            }else{//用apicode从登录接口去匹配
                //如果payLogos.length=1，且qrType ！= 0,则表示是非聚合码
                if(codes.get(position).qrType != 0 && allpayLogos.length == 1 ){
                    holder.tv_code_number.setText(MainApplication.getPayTypeMap().get(String.valueOf(allpayLogos[0])));
                }
            }*/
            //判断是否是预制二维码，如果是预制二维码则展示收银台名称
            //1:预制二维码；2:生成的和银联等不支持预制的
            if(codes.get(position).qrCreateType == 1){
                if(!TextUtils.isEmpty(codes.get(position).cashierDesk)){
                    holder.tv_code_number.setText(codes.get(position).cashierDesk);
                }
            }else if(codes.get(position).qrCreateType == 2){
                if(!TextUtils.isEmpty(codes.get(position).qrSignOwnerName)){
                    holder.tv_code_number.setText(codes.get(position).qrSignOwnerName);
                }
            }

            int qrtype = codes.get(position).qrType;
            if (1 == qrtype){//支付宝专属码
                holder.ali_pay_layout.setVisibility(View.VISIBLE);
                holder.all_pay_layout.setVisibility(View.GONE);
                holder.ll_other_union_pay_layout.setVisibility(View.GONE);

                //支付宝专属码的前提下在判断只有一个码
                if(allpayLogos.length == 1 ){
                    holder.tv_ali.setText(MainApplication.getPayTypeMap().get(String.valueOf(allpayLogos[0])));
                }

                if (typePicMap != null && typePicMap.size() > 0) {
                    String picUrl = typePicMap.get(allpayLogos[0] + "");
                    if (!StringUtil.isEmptyOrNull(picUrl)) {
                        if(getContext() != null){
                            GlideApp.with(getContext())
                                    .load(picUrl)
                                    .placeholder(R.drawable.icon_general_receivables)
                                    .error(R.drawable.icon_general_receivables)
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .into(holder.iv_icon_ali);
                        }

                    } else {
                        holder.iv_icon_ali.setImageResource(R.drawable.icon_general_receivables);
                    }
                }

                if (null != bitmap){
                    holder.iv_ali_code_image.setImageBitmap(bitmap);
                }
            }else if(20 == qrtype){//如果为其他通道二维码（银联二维码，Gcash码等）
                holder.ali_pay_layout.setVisibility(View.GONE);
                holder.all_pay_layout.setVisibility(View.GONE);
                holder.ll_other_union_pay_layout.setVisibility(View.VISIBLE);
                if (null != bitmap){
                    holder.iv_other_union_code_image.setImageBitmap(bitmap);
                }
                //专属码的前提下在判断只有一个码
                if(allpayLogos.length == 1 ){
                    holder.tv_other_UnionPay.setText(MainApplication.getPayTypeMap().get(String.valueOf(allpayLogos[0])));
                }

                if (typePicMap != null && typePicMap.size() > 0) {
                    String picUrl = typePicMap.get(allpayLogos[0] + "");
                    if (!StringUtil.isEmptyOrNull(picUrl)) {
                        if(getContext() != null){
                            GlideApp.with(getContext())
                                    .load(picUrl)
                                    .placeholder(R.drawable.icon_general_receivables)
                                    .error(R.drawable.icon_general_receivables)
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .into(holder.icon_other_union_pay);
                        }
                    } else {
                        holder.icon_other_union_pay.setImageResource(R.drawable.icon_general_receivables);
                    }
                }

            }else{
                holder.ali_pay_layout.setVisibility(View.GONE);
                holder.all_pay_layout.setVisibility(View.VISIBLE);
                holder.ll_other_union_pay_layout.setVisibility(View.GONE);
                if (null != bitmap){
                    holder.iv_code_image.setImageBitmap(bitmap);
                }
            }
            return convertView;
        }


        private Bitmap createCode(Activity activity, String uuId,  int width, int height,Bitmap mBitmapfinal) {
            Bitmap bitmap = null;
            int w = DisplayUtil.dip2Px(CodeListActivity.this, width);
            int h = DisplayUtil.dip2Px(CodeListActivity.this, height);
            try {
                if (null != mBitmapfinal){
                    bitmap = MaxCardManager.getInstance().create2DCode(false,true,activity,uuId, w, h,mBitmapfinal);
                }else{
                    bitmap = MaxCardManager.getInstance().create2DCode(true,uuId, w, h);
                }
            } catch (WriterException e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }
            return bitmap;
        }

    }

    ViewHolder holder = null;
    static class ViewHolder {
         TextView tv_code_number, tv_ali,tv_code_id ,tv_other_UnionPay;
         View view_line;
         ImageView iv_code_image,iv_icon_ali,iv_ali_code_image;
         ImageView icon_other_union_pay,iv_other_union_code_image;
         LinearLayout all_pay_layout;
         LinearLayout ali_pay_layout;
         LinearLayout ll_other_union_pay_layout;
    }


}
