package cn.swiftpass.enterprise.demo;

import android.support.annotation.NonNull;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by YZX on 2019年04月08日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */
public abstract class ResponseHandler<T> implements Callback<JsonResponse<T>> {

    public static final String ERROR_CODE_UNKNOWN = "Unknown";
    public static final String NETWORK_ERROR_CODE_UNKNOWN = "NetworkUnknown";
    public static final String NETWORK_ERROR_CODE_TIMEOUT = "NetworkTimeout";
    public static final String NETWORK_ERROR_CODE_UNAVAILABLE = "NetworkUnavailable";

    public abstract void onSuccess(T result);

    public abstract void onFailure(String code, String error);

    @Override
    public void onResponse(@NonNull Call<JsonResponse<T>> call, @NonNull Response<JsonResponse<T>> response) {
        if (response.isSuccessful()) {
            JsonResponse<T> jsonResp = response.body();
            if (jsonResp == null) {
                onFailure(call, new ResponseException(null, "json解析异常"));
                return;
            }
            if ("200".equals(jsonResp.getCode())) {
                T result = jsonResp.getData();
                if (result != null) {
                    onSuccess(result);
                } else {
                    onFailure(call, new ResponseException(null, "后台返回数据错误"));
                }
            } else {
                onFailure(call, new ResponseException(jsonResp.getCode(), jsonResp.getError()));
            }
        }
    }

    @Override
    public void onFailure(@NonNull Call<JsonResponse<T>> call, @NonNull Throwable t) {
        if (t instanceof SocketTimeoutException) {
            onFailure("NetworkTimeout", "网络超时");
        } else if (t instanceof ConnectException) {
            onFailure("NetworkTimeout", "无法连接服务器");
        } else if (t instanceof UnknownHostException) {
            onFailure("NetworkUnknown", "网络不可用");
        } else if (t instanceof ResponseException) {
            ResponseException exception = (ResponseException) t;
            onFailure(exception.getCode(), exception.getError());
        } else {
            onFailure("Unknown", "客户端发生未知异常，请稍后再试!");
        }
    }

}
