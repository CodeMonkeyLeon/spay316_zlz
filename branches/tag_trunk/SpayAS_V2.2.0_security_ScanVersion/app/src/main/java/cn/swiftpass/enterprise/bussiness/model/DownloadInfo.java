package cn.swiftpass.enterprise.bussiness.model;


import cn.swiftpass.enterprise.bussiness.enums.DownloadStatus;


import java.io.Serializable;

public class DownloadInfo implements Serializable
{

	/**
	  * @Fields serialVersionUID : TODO（用一句话描述这个变量表示什么）
	  */
	private static final long serialVersionUID = 11111L;

	public int id;
	//apkid

	public long downloadId;
	//下载名字

	public String mDownName = null;
	//url

	public String mDownUrl = null;
	//保存路径

	public String mDownSavePath = null;
	//下载状态

	public DownloadStatus mDownStatus ;
	//总字节数

	public long mDownTotalBytes = -1;
	//当前下载的字节数

	public long mDownCurrentBytes = 0;
	//扩展1

	public String mDownExtend1 = null;
	//扩展2

	public String mDownExtend2 = null;
	//扩展3

	public int mDownExtend3 = 0;
	//扩展4

	public int mDownExtend4 = 0;
	
	
	// ===========================================================
	// Constructors
	// ===========================================================
	public DownloadInfo(){
		
	}
	
	public DownloadInfo(String mDownName, String mDownUrl,
                        String mDownSavePath, DownloadStatus mDownStatus, int mDownTotalBytes,
                        int mDownCurrentBytes) {
		super();
		this.mDownName = mDownName;
		this.mDownUrl = mDownUrl;
		this.mDownSavePath = mDownSavePath;
		this.mDownStatus = mDownStatus;
		this.mDownTotalBytes = mDownTotalBytes;
		this.mDownCurrentBytes = mDownCurrentBytes;
	}

	
}
