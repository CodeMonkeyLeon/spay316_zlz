package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

import cn.swiftpass.enterprise.io.database.table.CountryAndUnitTable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = CountryAndUnitTable.TABLE_NAME)
public class CountryAndUnitMode implements Serializable
{
    
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    @DatabaseField(generatedId = true)
    public long id;
    
    /**
     * 国家编号
     */
    @DatabaseField(columnName = CountryAndUnitTable.COLUMN_COUNTRY_ID)
    public Integer countryId;
    
    /**
     * 国家名称
     */
    @DatabaseField(columnName = CountryAndUnitTable.COLUMN_COUNTRY_NAME)
    public String countryName;
    
    /**
     * 单位编号
     */
    @DatabaseField(columnName = CountryAndUnitTable.COLUMN__UNIT_ID)
    public Integer unitId;
    
    /**
     * 单位名称
     */
    @DatabaseField(columnName = CountryAndUnitTable.COLUMN__UNIT_NAME)
    public String unitName;
    
}
