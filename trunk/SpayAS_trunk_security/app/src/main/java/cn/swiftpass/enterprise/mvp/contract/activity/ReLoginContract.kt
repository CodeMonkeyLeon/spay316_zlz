package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.CertificateBean
import cn.swiftpass.enterprise.bussiness.model.DynModel
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/11/18
 *
 * @一切都明明白白
 * @但我们仍匆匆错过
 * @因为你相信命运
 * @因为我怀疑生活。
 */
class ReLoginContract {

    interface View : BaseView {

        fun ecdhKeyExchangeSuccess(response: ECDHInfo?)

        fun ecdhKeyExchangeFailed(error: Any?)

        fun logoutSuccess(response: Boolean)

        fun logoutFailed(error: Any?)

        fun getCertificateStringSuccess(
            response: CertificateBean?,
            isDeviceLogin: Boolean
        )

        fun getCertificateStringFailed(error: Any?, isDeviceLogin: Boolean)

        fun deviceLoginSuccess(response: Boolean)

        fun deviceLoginFailed(error: Any?)

        fun apiShowListSuccess(response: ArrayList<DynModel>?)

        fun apiShowListFailed(error: Any?)

    }


    interface Presenter : BasePresenter<View> {

        fun apiShowList()

        fun deviceLogin()

        fun getCertificateString(isDeviceLogin: Boolean)

        fun logout()

        fun ecdhKeyExchange(publicKey: String?)
    }


}