package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.PreAuthDetailsBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.print.PrintPreAuth;
import cn.swiftpass.enterprise.print.SunmiPrintHelper;
import cn.swiftpass.enterprise.ui.activity.print.BluetoothSettingActivity;
import cn.swiftpass.enterprise.ui.adapter.PreAutFinishAdapter;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.ApkUtil;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 创建人：caoxiaoya
 * 时间：2018/12/15
 * 描述：扫码/已支出列表/解冻列表  最后完成界面
 * 备注：Intent.FLAG_ACTIVITY_REORDER_TO_FRONT只允许存在一个，不能同时复用，所以新建一个
 */

public class PreAutFinishActivity extends TemplateActivity {

    private RecyclerView mRecyclerView;
    private PreAutFinishAdapter mAdapter;
    private int activityType;
    private List<PreAuthDetailsBean> mList = new ArrayList<>();
    private Order mOrder;



    public static void startActivity(Context mContext, Order order, int activityType) {
        Intent it = new Intent();
        it.setClass(mContext, PreAutFinishActivity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT); //W.l 解决弹出多个activity
        it.putExtra("order", order);
        it.putExtra("activityType",activityType);//标识哪个Activity传过来的
        mContext.startActivity(it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_aut_finish);
        MainApplication.listActivities.add(this);
        initView();
        initData();

        //初始化 sunmi pos 打印机
        SunmiPrintHelper.getInstance().initSunmiPrinterService(this);
        SunmiPrintHelper.getInstance().initPrinter();

        if(activityType == 1 ){//预授权支付完成自动打印小票
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    bluePrint(1);
                }
            });
        }
        if(activityType == 2 ){//预授权解冻完成自动打印小票
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    bluePrint(3);
                }
            });
        }
        if(activityType == 5 ){//预授权反扫完下单一次成功成自动打印小票
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    bluePrint(5);
                }
            });
        }
    }

  /*  @Override
    public void onDestroy() {
        super.onDestroy();
        SunmiPrintHelper.getInstance().deInitSunmiPrinterService(this);
    }*/

    private void initView() {
        activityType = getIntent().getIntExtra("activityType", 0);
        mRecyclerView = findViewById(R.id.rv_pre_auth);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mAdapter = new PreAutFinishAdapter(this, mList, activityType);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initData() {
        mOrder = (Order) getIntent().getSerializableExtra("order");
        if(mOrder!=null) {
            switch (activityType) {
                case 1://二维码扫码最后完成
                    type_one();
                    mAdapter.setHeaderView(createHeadView());
                    mAdapter.setFooterView(typeOneFootView());

                    break;
                case 2://预授权解冻-需要上一步验证密码
                    type_two();
                    mAdapter.setHeaderView(createHeadView());
                    mAdapter.setFooterView(typeOneFootView());
                    break;
                case 3://预授权转支付-不需要上一步验证密码
                    type_three();
                    mAdapter.setHeaderView(createHeadView());
                    mAdapter.setFooterView(typeOneFootView());
                    break;
                case 4://预授权收款
                    type_four();
                    mAdapter.setHeaderView(createHeadView());
                    mAdapter.setFooterView(typeOneFootView());
                    break;
                case 5://支付宝下单成功情况下----没有走轮询逻辑
                    type_five();
                    mAdapter.setHeaderView(createHeadView());
                    mAdapter.setFooterView(typeOneFootView());
                    break;
                default:
                    break;
            }
        }
    }

    private void type_five() {
        mList.add(new PreAuthDetailsBean(getString(R.string.order_status), null, getStringById(R.string.pre_auth_authorized)));//订单状态
        mList.add(new PreAuthDetailsBean(getString(R.string.pre_auth_time), mOrder.getTimeEnd(),null));//授权时间

        if (!isAbsoluteNullStr(mOrder.getTradeName())){//先判断大写的Name，再判断小写name
            mList.add(new PreAuthDetailsBean(getString(R.string.payment_method), mOrder.getTradeName(), null));//交易方式
        }else {
            mList.add(new PreAuthDetailsBean(getString(R.string.payment_method), mOrder.getTradename(), null));//交易方式
        }


        if(ApkUtil.isZhLanguage(PreAutFinishActivity.this)){
            mList.add(new PreAuthDetailsBean(MainApplication.getPayTypeMap().get("2") + getString(R.string.tx_orderno), mOrder.getOutTransactionId(), null));//(微信-支付宝)支付单号
        }else {
            mList.add(new PreAuthDetailsBean(MainApplication.getPayTypeMap().get("2")+" " + getString(R.string.tx_orderno), mOrder.getOutTransactionId(), null));//(微信-支付宝)支付单号
        }


        mList.add(new PreAuthDetailsBean(getString(R.string.platform_pre_auth_order_id), mOrder.getAuthNo(), null));//平台预授权订单号
        mAdapter.setData(mList);
    }

    private void type_four(){
        mList.add(new PreAuthDetailsBean(getString(R.string.unfreezed_time),mOrder.getTradeTime(),null));
        mList.add(new PreAuthDetailsBean(getString(R.string.tv_unfreezen_applicant),mOrder.getTotalFee()+"",null));
        mList.add(new PreAuthDetailsBean(getString(R.string.unfreezed_order_id),mOrder.getAuthNo(),null));
        mAdapter.setData(mList);
    }

        private void  type_two(){
            titleBar.setTitle(R.string.unfreezing);
            titleBar.setLeftButtonVisible(false);
            mList.add(new PreAuthDetailsBean(getString(R.string.unfreezed_time),mOrder.getUnFreezeTime(),null));
            mList.add(new PreAuthDetailsBean(getString(R.string.tv_unfreezen_applicant),MainApplication.mchName,null));//办理人
            mList.add(new PreAuthDetailsBean(getString(R.string.unfreezed_order_id),mOrder.getAuthNo(),null));
            mAdapter.setData(mList);
        }

        private void type_three(){//转支付查询成功
            titleBar.setTitle(R.string.title_order_detail);
            titleBar.setLeftButtonVisible(false);
            mList.add(new PreAuthDetailsBean(getString(R.string.order_status),null,getString(R.string.tx_bill_stream_chioce_succ)));//订单状态
            mList.add(new PreAuthDetailsBean(getString(R.string.pre_auth_time),mOrder.getTradeTime(),null));//交易时间
            mList.add(new PreAuthDetailsBean(getString(R.string.payment_method),mOrder.getTradeName(),null));//支付方式

            if(ApkUtil.isZhLanguage(PreAutFinishActivity.this)){
                mList.add(new PreAuthDetailsBean(MainApplication.getPayTypeMap().get("2") + getString(R.string.tx_orderno),mOrder.getTransactionId(),null));//支付宝单号
            }else {
                mList.add(new PreAuthDetailsBean(MainApplication.getPayTypeMap().get("2")+" " + getString(R.string.tx_orderno),mOrder.getTransactionId(),null));//支付宝单号
            }


            mList.add(new PreAuthDetailsBean(getString(R.string.tx_order_no),mOrder.getAuthNo(),null));//平台订单号
            mAdapter.setData(mList);
        }

        private void type_one () {
            mList.add(new PreAuthDetailsBean(getString(R.string.order_status), null, getStringById(R.string.pre_auth_authorized)));//订单状态
            mList.add(new PreAuthDetailsBean(getString(R.string.pre_auth_time), mOrder.getTradeTime(),null));//授权时间
            mList.add(new PreAuthDetailsBean(getString(R.string.payment_method), mOrder.getTradeName(), null));//交易方式

            if(ApkUtil.isZhLanguage(PreAutFinishActivity.this)){
                mList.add(new PreAuthDetailsBean(MainApplication.getPayTypeMap().get("2") + getString(R.string.tx_orderno), mOrder.getOutTransactionId(), null));//(微信-支付宝)支付单号
            }else {
                mList.add(new PreAuthDetailsBean(MainApplication.getPayTypeMap().get("2")+" " + getString(R.string.tx_orderno), mOrder.getOutTransactionId(), null));//(微信-支付宝)支付单号
            }


            mList.add(new PreAuthDetailsBean(getString(R.string.platform_pre_auth_order_id), mOrder.getAuthNo(), null));//平台预授权订单号
            mAdapter.setData(mList);
        }

        private TextView tv_type_money;
        private View createHeadView () {
            LinearLayout.LayoutParams layoutParams =
                    new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            LayoutInflater inflater = LayoutInflater.from(this);
            View viewHead = inflater.inflate(R.layout.item_preauth_head, null);
            viewHead.setLayoutParams(layoutParams);
            layoutParams.setMargins(30, 0, 30, 0);
            TextView tv_type_total= viewHead.findViewById(R.id.tv_type_total);
             tv_type_money= viewHead.findViewById(R.id.tv_type_money);
            TextView tv_money= viewHead.findViewById(R.id.tv_money);

            switch (activityType){
                case 1:
                    titleBar.setTitle(R.string.pre_auth_details);
                    tv_type_total.setText(R.string.pre_auth_amount);
                    tv_type_money.setText(MainApplication.getFeeFh()+DateUtil.formatMoneyUtils(mOrder.getRestAmount()));
                    break;
                case 2:
                    tv_type_total.setText(R.string.unfreezing_amount);
                    tv_type_money.setText(MainApplication.getFeeFh() +DateUtil.formatMoneyUtils(mOrder.getTotalFee()));
                    break;
                case 3:
                    tv_type_money.setText(MainApplication.getFeeFh() +DateUtil.formatMoneyUtils(mOrder.getTotalFee()));
                    break;
                case 4:
                    tv_type_money.setText(MainApplication.getFeeFh() +DateUtil.formatMoneyUtils(mOrder.getTotalFee()));
                    break;
                case 5:
                    titleBar.setTitle(R.string.pre_auth_details);
                    tv_type_total.setText(R.string.pre_auth_amount);
                    tv_type_money.setText(MainApplication.getFeeFh()+DateUtil.formatMoneyUtils(mOrder.getTotalFee()));
                    break;
                    default:
                        break;
            }


            return viewHead;
        }

    private View typeOneFootView(){
        LinearLayout.LayoutParams layoutParams=
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LayoutInflater inflater = LayoutInflater.from(this);
        View viewFoot=inflater.inflate(R.layout.item_preauth_foot, null);
        viewFoot.setLayoutParams(layoutParams);

        Button btn_finish= viewFoot.findViewById(R.id.btn_finish);//完成
        Button btn_blue_print= viewFoot.findViewById(R.id.btn_blue_print);//打印
        final ImageView iv_qr= viewFoot.findViewById(R.id.iv_qr);//一维码
        TextView tv_qr_numbertext= viewFoot.findViewById(R.id.tv_qr_numbertext);//一维码数字
        TextView tv_description= viewFoot.findViewById(R.id.tv_description);//说明

        switch (activityType){
            case 1:
                btn_finish.setVisibility(View.VISIBLE);
                btn_finish.setText(getString(R.string.pay_success_finishing));
                btn_finish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent it = new Intent();
                        it.setClass(PreAutFinishActivity.this, spayMainTabActivity.class);
                        startActivity(it);
                        for (Activity a : MainApplication.allActivities)
                        {
                            a.finish();
                        }

                    }
                });
                break;
            case 2:
                tv_description.setPadding(50,30,50, 30);
                tv_description.setGravity(Gravity.LEFT);
                tv_description.setTextColor(getResources().getColor(R.color.bg_color_text));
                tv_description.setVisibility(View.VISIBLE);
                tv_description.setText(getString(R.string.unfreeze));
                btn_finish.setText(getString(R.string.pay_success_finishing));
                btn_finish.setVisibility(View.VISIBLE);
                btn_finish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
                break;
            case 3:
                btn_finish.setText(getString(R.string.pay_success_finishing));
                btn_finish.setVisibility(View.VISIBLE);
                btn_finish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
                break;
            case 4:
                tv_description.setPadding(50,30,50, 30);
                tv_description.setGravity(Gravity.LEFT);
                tv_description.setTextColor(getResources().getColor(R.color.bg_color_text));
                tv_description.setVisibility(View.VISIBLE);
                tv_description.setText(getString(R.string.unfreeze));
                btn_finish.setVisibility(View.VISIBLE);
                btn_finish.setText(getString(R.string.btnOk));
                btn_finish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
                break;
            case 5:
                btn_finish.setVisibility(View.VISIBLE);
                btn_finish.setText(getString(R.string.pay_success_finishing));
                btn_finish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent it = new Intent();
                        it.setClass(PreAutFinishActivity.this, spayMainTabActivity.class);
                        startActivity(it);
                        for (Activity a : MainApplication.allActivities)
                        {
                            a.finish();
                        }

                    }
                });
                break;
            default:
                break;
        }

        return viewFoot;
    }

        @Override
        protected void setupTitleBar () {
            super.setupTitleBar();
            titleBar.setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
            titleBar.setRightButLayVisibleForTotal(true, getString(R.string.print));
            titleBar.setTitle(R.string.details);
            titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
                @Override
                public void onLeftButtonClick() {

                }

                @Override
                public void onRightButtonClick() {
                }

                @Override
                public void onRightLayClick() {

                }

                @Override
                public void onRightButLayClick() {
                    //打印方法
                    if(activityType == 1 ){//预授权支付完成打印小票
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                bluePrint(1);
                            }
                        });
                    }
                    if(activityType == 2 ){//预授权解冻完成打印小票
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                bluePrint(3);
                            }
                        });
                    }
                    if(activityType == 5 ){//预授权反扫完下单一次成功成自动打印小票
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                bluePrint(5);
                            }
                        });
                    }

                }
            });
        }

        public void bluePrint(final int printType){
            PreAutFinishActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        mOrder.setPartner(getString(R.string.tv_pay_user_stub));
                        PrintPreAuth.printPreAut(printType,mOrder);
                        //打印第二联
                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                mOrder.setPartner(getString(R.string.tv_pay_mch_stub));
                                PrintPreAuth.printPreAut(printType,mOrder);
                            }
                        }, 3000);
                    } catch (Exception e) {
                    }
                }
            });
        }


    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
        void showDialog() {
            dialog = new DialogInfo(PreAutFinishActivity.this, null, getString(R.string.tx_blue_set), getString(R.string.title_setting), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

                @Override
                public void handleOkBtn() {
                    showPage(BluetoothSettingActivity.class);
                    dialog.cancel();
                    dialog.dismiss();
                }

                @Override
                public void handleCancleBtn() {
                    dialog.cancel();
                }
            }, null);

            DialogHelper.resize(PreAutFinishActivity.this, dialog);
            dialog.show();
        }
    }

