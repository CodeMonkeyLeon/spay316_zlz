/*
 * 文 件 名:  ShopBaseDataDB.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2014-3-20
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.io.database.access;

import java.sql.SQLException;
import java.util.List;

import android.util.Log;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.AwaInfo;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;

/**
 * 参加活动
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2014-3-20]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class ActiveDataDB
{
    
    private static Dao<AwaInfo, Integer> userDao;
    
    private static ActiveDataDB instance = null;
    
    public static ActiveDataDB getInstance()
    {
        if (instance == null)
        {
            instance = new ActiveDataDB();
        }
        
        return instance;
    }
    
    public ActiveDataDB()
    {
        try
        {
            userDao = MainApplication.getContext().getHelper().getDao(AwaInfo.class);
        }
        catch (SQLException e)
        {
            Log.e("hehui", "ShopBaseDataDB-->" + e.getMessage());
        }
    }
    
    /**
     * 保存 商户行业数据
     * <功能详细描述>
     * @param userModel
     * @throws SQLException
     * @see [类、类#方法、类#成员]
     */
    public void save(AwaInfo info)
        throws SQLException
    {
        //        AwaInfo result = queryByID(info.userName);
        //        if (result == null)
        //        {
        userDao.create(info);
        //        }
        //        else
        //        {
        //            userDao.update(result);
        //        }
    }
    
    /**
     * 根据id查询
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public AwaInfo queryByID(String bankID)
    {
        String sql = "select * from t_active where userName = '" + bankID + "'";
        AwaInfo list = null;
        try
        {
            GenericRawResults<String[]> s = userDao.queryRaw(sql);
            List<String[]> listStr = s.getResults();
            for (int i = 0; i < listStr.size(); i++)
            {
                list = new AwaInfo();
                String[] str = listStr.get(i);
                list.setUserName(str[0]);
                list.setStartTime(str[1]);
            }
            
        }
        catch (SQLException e)
        {
            Log.e("hehui", "query failed " + e.getMessage());
            return null;
        }
        
        return list;
    }
    
}
