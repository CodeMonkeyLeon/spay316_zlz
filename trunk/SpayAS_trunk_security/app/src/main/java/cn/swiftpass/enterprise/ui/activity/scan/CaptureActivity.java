package cn.swiftpass.enterprise.ui.activity.scan;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.common.sentry.SentryUtils;
import com.example.common.sp.PreferenceUtil;
import com.google.zxing.BarcodeFormat;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.Vector;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.broadcast.GTPushIntentService;
import cn.swiftpass.enterprise.broadcast.PushTransmissionModel;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.Camera.CameraManager;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.Decoding.CaptureActivityHandler;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.Decoding.InactivityTimer;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.MasterCardInfo;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.QRcodeInfo;
import cn.swiftpass.enterprise.bussiness.model.WalletListBean;
import cn.swiftpass.enterprise.bussiness.model.WxCard;
import cn.swiftpass.enterprise.common.Constant;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.CaptureContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.CapturePresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.activity.InstapayInfoActivity;
import cn.swiftpass.enterprise.ui.activity.MasterCardOrderDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.NoteMarkActivity;
import cn.swiftpass.enterprise.ui.activity.OrderDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.PayResultActivity;
import cn.swiftpass.enterprise.ui.activity.PreAutFinishActivity;
import cn.swiftpass.enterprise.ui.activity.PreAuthActivity;
import cn.swiftpass.enterprise.ui.activity.ShowQrCodeActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.PayTypeDialog;
import cn.swiftpass.enterprise.ui.widget.RefundCheckDialog;
import cn.swiftpass.enterprise.ui.widget.Zxing.ViewfinderView;
import cn.swiftpass.enterprise.ui.widget.dialog.ScanImputDialog;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.DisplayUtil;
import cn.swiftpass.enterprise.utils.KotlinUtils;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;
import cn.swiftpass.enterprise.utils.Utils;
import cn.swiftpass.enterprise.utils.permissionutils.OnPermissionCallback;
import cn.swiftpass.enterprise.utils.permissionutils.Permission;
import cn.swiftpass.enterprise.utils.permissionutils.PermissionDialogUtils;
import cn.swiftpass.enterprise.utils.permissionutils.PermissionsManage;

/**
 * 扫描2code
 *
 * @author Alan
 */
public class CaptureActivity extends BaseActivity<CaptureContract.Presenter> implements Callback, View.OnClickListener, CaptureContract.View {

    public static final String FLAG = "NATIVE.SHOWQRCODE.ACTIVITY";
    private static final float BEEP_VOLUME = 0.10f;
    private static final long VIBRATE_DURATION = 200L;
    /**
     * When the beep has finished playing, rewind to queue up another one.
     */
    private final OnCompletionListener beepListener = new OnCompletionListener() {
        public void onCompletion(MediaPlayer mediaPlayer) {
            mediaPlayer.seekTo(0);
        }
    };
    protected Dialog dialog3;
    long totalFee;
    List<String> vard = new ArrayList<String>();
    List<WxCard> vardOrders = new ArrayList<WxCard>();
    long discountAmount; // 优惠金额
    SurfaceView surfaceView;
    double minus = 1;
    String title = ToastHelper.toStr(R.string.tx_pay_loading);
    boolean flag = true;
    int count = 1;
    RefundCheckDialog dialogRever = null;
    boolean tag = true;
    private CaptureActivityHandler handler;
    private ViewfinderView viewfinderView;
    private boolean hasSurface;
    private Vector<BarcodeFormat> decodeFormats;
    private String characterSet;
    private TextView txtResult, tv_money, tv_surcharge;
    private InactivityTimer inactivityTimer;
    private MediaPlayer mediaPlayer;
    private boolean playBeep;
    //    private ScanCodeCheckDialog dialog;
    private boolean vibrate;
    private String money, payType, strSurchargelMoney;
    private Context mContext;
    private Timer timer;
    private String mOrderNo;
    private String mOutTradeNo;
    private Handler mHandler;
    private DialogInfo showDialog;
    private LinearLayout money_lay, ly_back;
    private List<String> payMeths = new ArrayList<String>();
    private List<WxCard> vardList = new ArrayList<WxCard>();
    private String fTag = "";
    private String tradeType = null, payOutTradeNo;
    private Integer isMark = 0;
    private DialogInfo dialogs;
    private TextView tv_code_info;
    private LinearLayout ly_switch;
    private TextView tv_switch_code, tv_input_code, tv_pase;
    private SurfaceHolder surfaceHolder;
    private LinearLayout has_fee_layout;
    private String mark = "";
    private PushTransmissionModel pushTransmissionModel;
    private boolean isPreAuth = false;
    private long timeCount = 15;
    private long startTime = 0l;
    private long resultTime = 0l;


    private Runnable myRunnable = new Runnable() {

        @Override
        public void run() {

            if (showDialog == null) {
                return;
            }
            if (timeCount >= 0) {
                if ((resultTime - startTime < 5000) && (timeCount == 10 || timeCount == 5)) {
                    //5s内返回了结果，才发起下一次请求
                    queryOrderGetStatus(mOutTradeNo, false, false, true);
                }
//                showDialog.setMessage(getString(R.string.dialog_order_stuts) + "，(" + timeCount + getString(R.string.dialog_start) + (count - 1) + getString(R.string.dialog_end));
                String msg = "";
                String end = getString(R.string.string_dialog_scan_end);
                if (end.contains("Sekunde")) {
                    //德语, 秒前面加空格
                    msg = getString(R.string.string_dialog_scan_start) + timeCount + " " + end;
                } else {
                    msg = getString(R.string.string_dialog_scan_start) + timeCount + end;
                }
                showDialog.setMessage(msg);
                timeCount--;
                mHandler.postDelayed(this, 1000);
            } else {
                mHandler.removeCallbacks(this);
                if (!isFinishing() && showDialog != null && showDialog.isShowing()) {
                    showDialog.dismiss();
                }
                if (mIsShowWindowReverse) {
                    showWindowRever();
                    mIsShowWindowReverse = false;
                }
            }


//            if (timeCount > 0 && showDialog != null) {
//                //                dialogInfo.setBtnOkText("确定(" + timeCount + "秒后关闭)");
//                showDialog.setMessage(getString(R.string.dialog_order_stuts) + "，(" + timeCount + getString(R.string.dialog_start) + (count - 1) + getString(R.string.dialog_end));
//                timeCount -= 1;
//                mHandler.postDelayed(this, 1000);
//            } else {
//                mHandler.removeCallbacks(this);
//                if (showDialog != null && showDialog.isShowing()) {
//
//                    showDialog.dismiss();
//                }
//                queryOrderGetStatus(outTradeNo, false, false, true);
//            }

        }
    };

    public static void startActivity(Context context, String payType, String mark) {
        Intent it = new Intent();
        it.setClass(context, CaptureActivity.class);
        it.putExtra("payType", payType);
        it.putExtra("mark", mark);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(it);
    }

    public static void startActivity(Context context, String payType) {
        startActivity(context, payType, "");
    }

    public static void startActivity(Context context, String payType, String flag, String money) {
        Intent it = new Intent();
        it.setClass(context, CaptureActivity.class);
        it.putExtra("payType", payType);
        it.putExtra("flag", flag);
        it.putExtra("money", money);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(it);
    }

    @Override
    protected CaptureContract.Presenter createPresenter() {
        return new CapturePresenter();
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_capture_new);
        ly_back = getViewById(R.id.ly_back);
        MainApplication.getInstance().getListActivities().add(this);
        payType = getIntent().getStringExtra("payType");
        mark = getIntent().getStringExtra("mark");
        timer = new Timer();
        CameraManager.init(this);

        viewfinderView = findViewById(R.id.viewfinder_view);
        hasSurface = false;
        inactivityTimer = new InactivityTimer(this);

        //根据当前的小数点的位数来判断应该除以多少
        for (int i = 0; i < MainApplication.getInstance().getNumFixed(); i++) {
            minus = minus * 10;
        }

        pushTransmissionModel = new PushTransmissionModel();
        tv_pase = getViewById(R.id.tv_pase);
        ly_switch = getViewById(R.id.ly_switch);

        tv_code_info = getViewById(R.id.tv_code_info);
        tv_code_info.setPadding(0, 0, 0, DisplayUtil.dip2Px(CaptureActivity.this, 20));

        mHandler = new Handler();

        money_lay = getViewById(R.id.money_lay);
        tv_switch_code = getViewById(R.id.tv_switch_code);
        tv_input_code = getViewById(R.id.tv_input_code);
        setLister();
        tv_input_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScanImputDialog scanImputDialog = new ScanImputDialog(CaptureActivity.this, new ScanImputDialog.ConfirmListener() {

                    @Override
                    public void ok(String code) {
                        submitData(code, false);
                    }
                });
                DialogHelper.resize(CaptureActivity.this, scanImputDialog);
                scanImputDialog.show();
            }
        });

        tv_switch_code.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //判断是否开通一种支付类型
                String payType = isOpenFisrtpayType();
                if (!StringUtil.isEmptyOrNull(payType)) {
                    if (getNativePayType(payType).equalsIgnoreCase(Constant.NATIVE_LIQUID)) {//如果当前只开通了一种支付通道，同时这种通道是小钱包liquid
                        //则直接弹框，先显示第一级弹框，点击后再显示二级弹框
                        PayTypeDialog dialogPay = new PayTypeDialog(CaptureActivity.this, money, new PayTypeDialog.HandleItemBtn() {
                            @Override
                            public void toPay(String money, String payType, String liquidPayType, WalletListBean walletBean, boolean isInstapay, boolean isInstapayQRPayment, boolean isRbInstapay) {
                                toNewPay(money, payType, liquidPayType, walletBean, isInstapay, isInstapayQRPayment, isRbInstapay);
                            }
                        });
                        DialogHelper.resize(CaptureActivity.this, dialogPay);
                        dialogPay.show();
                        return;

                    } else if (getNativePayType(payType).equalsIgnoreCase(Constant.NATIVE_INSTAPAY)) {//如果当前只开通了一种支付通道，同时这种通道是instapay
                        //则走下单逻辑，下单成功直接跳转到instapay收款界面
                        toNewPay(money, payType, null, null, true, false, false);
                        return;
                    } else if (getNativePayType(payType).equalsIgnoreCase(Constant.NATIVE_INSTAPAY_V2)) {//如果当前只开通了一种支付通道，同时这种通道是instapay的QR Ph Payment
                        //则走下单逻辑，下单成功直接跳转到收款界面
                        toNewPay(money, payType, null, null, false, true, false);
                        return;
                    } else if (isMasterCardServiceType(getNativePayType(payType))) {//如果当前只开通了一种支付通道，同时这种通道是MasterCard卡通道
                        //则走masterCard下单逻辑，下单成功直接跳转到MasterCard卡收单二维码收款界面
                        toMasterCardNewPay(money, payType);
                        return;
                    } else if (!TextUtils.isEmpty(payType) && Integer.parseInt(payType) == Constant.RB_INSTAPAY_CODE) {
                        //只开通一种, rbInstapay
                        toNewPay(money, payType, null, null, false, true, true);
                        return;
                    } else {
                        //走原来的逻辑，展示二维码界面
                        toNewPay(money, payType, null, null, false, false, false);
                        return;
                    }
                }

                PayTypeDialog dialogPay = new PayTypeDialog(CaptureActivity.this, money, new PayTypeDialog.HandleItemBtn() {

                    @Override
                    public void toPay(String money, String payType, String liquidPayType, WalletListBean walletBean, boolean isInstapay, boolean isInstapayQRPayment, boolean isRbInstapay) {
                        if (isMasterCardServiceType(getNativePayType(payType))) {//如果当前选择的通道是MasterCard卡通道
                            //则走masterCard下单逻辑，下单成功直接跳转到MasterCard卡收单二维码收款界面
                            toMasterCardNewPay(money, payType);
                        } else {
                            toNewPay(money, payType, liquidPayType, walletBean, isInstapay, isInstapayQRPayment, isRbInstapay);
                        }
                    }
                });
                DialogHelper.resize(CaptureActivity.this, dialogPay);
                dialogPay.show();
            }
        });
        txtResult = findViewById(R.id.txtResult);
        txtResult.setText(MainApplication.getInstance().getFeeFh());
        tv_money = getViewById(R.id.tv_money);
        //额外费用
        tv_surcharge = getViewById(R.id.surcharge);
        has_fee_layout = getViewById(R.id.has_fee_layout);
        fTag = getIntent().getStringExtra("flag");

        payOutTradeNo = getIntent().getStringExtra("outTradeNo");
        isMark = getIntent().getIntExtra("isMark", 0);
        if (null != getIntent().getSerializableExtra("vardOrders")) {
            vardList = ((List<WxCard>) getIntent().getSerializableExtra("vardOrders"));
        }

        if (!isAbsoluteNullStr(payType) && payType.equals(Constant.PAY_TYPE_REFUND)) {
            txtResult.setVisibility(View.GONE);
            tv_money.setVisibility(View.GONE);
            //新增
            has_fee_layout.setVisibility(View.INVISIBLE);

        } else if (!isAbsoluteNullStr(payType) && payType.equals(Constant.PAY_TYPE_MICROPAY_VCARD)) { //

            vardOrders = vardList;

            money_lay.setVisibility(View.VISIBLE);
            money = getIntent().getStringExtra("money");
            double d = Double.parseDouble(money);
            money = (long) d + "";
            discountAmount = getIntent().getLongExtra("discountAmount", 0);

            tv_money.setText(getString(R.string.tx_real_money) + "：");
            //新增
            has_fee_layout.setVisibility(View.VISIBLE);
//            tv_surcharge.setText();
            ly_switch.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(money)) {
                changeTextSize(money);
            }
        } else if (!isAbsoluteNullStr(fTag) && fTag.equalsIgnoreCase(FLAG)) { // 从二维码界面调转进来
            money = getIntent().getStringExtra("money");
            tag = false;
            money_lay.setVisibility(View.VISIBLE);
            ly_switch.setVisibility(View.VISIBLE);
            changeTextSize(money);
            BigDecimal bigDecimal = new BigDecimal(Long.parseLong(money) / minus);
            paseRMB(bigDecimal, tv_pase);

        } else if (!isAbsoluteNullStr(payType) && payType.equalsIgnoreCase(Constant.PAY_TYPE_SCAN_OPNE)) { // 从固定二维码，扫码激活
            //            titleBar.setRightButLayVisible(false, R.string.tx_mic_help);
            txtResult.setVisibility(View.GONE);
            tv_money.setVisibility(View.GONE);
            //新增
            has_fee_layout.setVisibility(View.INVISIBLE);
        } else {
            money = getIntent().getStringExtra("money");
            strSurchargelMoney = getIntent().getStringExtra("surcharge");
            if (!StringUtil.isEmptyOrNull(money)) {
                if (MainApplication.getInstance().isSurchargeOpen()) {
                    if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                        tv_money.setVisibility(View.GONE);
                        has_fee_layout.setVisibility(View.INVISIBLE);

                        //如果是预授权。则手动不计算surcharge
                        strSurchargelMoney = "0";
                    } else {
                        has_fee_layout.setVisibility(View.VISIBLE);
                        tv_money.setVisibility(View.VISIBLE);
                    }
                } else {
                    tv_money.setVisibility(View.GONE);
                    has_fee_layout.setVisibility(View.INVISIBLE);
                }

                money_lay.setVisibility(View.VISIBLE);
                ly_switch.setVisibility(View.VISIBLE);
                BigDecimal bigDecimal = null;
                BigDecimal bigSurcharge = null;

                if (TextUtils.isEmpty(strSurchargelMoney)) {
                    String mon = MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtil((Long.parseLong(money)) / minus);
                    changeTextSize(mon);
//                    changeTextSize(money);
                    bigSurcharge = new BigDecimal(0);
                } else {
                    String mon = MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtil((Long.parseLong(money) + Long.parseLong(strSurchargelMoney)) / minus);
                    changeTextSize(mon);
                    bigSurcharge = new BigDecimal(Long.parseLong(strSurchargelMoney) / minus);
                }

                bigDecimal = new BigDecimal(Long.parseLong(money) / minus);
                paseRMBWithSurcharge(bigDecimal, bigSurcharge);
            } else {
                ly_switch.setVisibility(View.INVISIBLE);
                money_lay.setVisibility(View.GONE);
                tv_code_info.setText(R.string.tv_scan_prompt);
            }
        }

        if (isMark == 1) { //不能再使用卡券
            tradeType = getIntent().getStringExtra("tradeType");
        }
        if (!PermissionsManage.isGranted(this, Permission.CAMERA)) {
            PermissionDialogUtils.requestPermission(CaptureActivity.this,
                    Permission.Group.CAMERA, true,
                    new OnPermissionCallback() {
                        @Override
                        public void onGranted(List<String> permissions, boolean all) {
                            if (all) {
                                restartCamera();
                            }
                        }
                    });
        }
        initData();
    }

    private void setLister() {
        ly_back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void masterCardNativePaySuccess(MasterCardInfo response, String apiCode) {
        if (response != null) {
            response.setApiCode(apiCode);
            ShowQrCodeActivity.startActivity(null, response, CaptureActivity.this, null,
                    false, false, true, mark);
        }
    }

    @Override
    public void masterCardNativePayFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }

        if (error != null && !TextUtils.isEmpty(error.toString())) {
            CaptureActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    toastDialog(CaptureActivity.this, error.toString(), null);
                }
            });
        } else {
            toastDialog(CaptureActivity.this, getStringById(R.string.generate_code_failed), null);
        }
    }

    /* switch收银台下单*/
    public void toMasterCardNewPay(String money, String apiCode) {
        if (mPresenter != null) {
            mPresenter.masterCardNativePay(money, apiCode, mark);
        }
    }

    @Override
    public void unifiedNativePaySuccess(QRcodeInfo response, boolean isInstapay, boolean isInstapayQRPayment, boolean isRbInstapay, WalletListBean bean, String pt) {
        if (response != null) {
            payType = pt;
            response.setSurcharge(strSurchargelMoney);
            response.setPayType(payType);

            if (isInstapay) {//如果是instapay下单，则直接跳转到instapay的页面
                InstapayInfoActivity.startActivity(CaptureActivity.this, response);
            } else if (isInstapayQRPayment || isRbInstapay) {//如果是instapay的QR Ph Payment或rbInstapay下单，则直接跳转到原来的动态码页面
                //同时传值过去，标记是否是二级列表进去的固码,同时标记instapay QRPayment、rbInstapay的下单
                ShowQrCodeActivity.startActivity(response, CaptureActivity.this, bean,
                        TextUtils.isEmpty(tradeType) ? false : true, true, mark);
            } else {
                //同时传值过去，标记是否是二级列表进去的固码
                ShowQrCodeActivity.startActivity(response, CaptureActivity.this, bean,
                        TextUtils.isEmpty(tradeType) ? false : true, false, mark);
            }
        }
    }

    @Override
    public void unifiedNativePayFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }

        if (error != null && !TextUtils.isEmpty(error.toString())) {
            CaptureActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    toastDialog(CaptureActivity.this, error.toString(), null);
                }
            });
        } else {
            toastDialog(CaptureActivity.this, getStringById(R.string.generate_code_failed), null);
        }
    }

    /**
     * 生成二维码
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void toNewPay(String money, final String payType, final String tradeType, final WalletListBean bean, boolean isInstapay, boolean isInstapayQRPayment, boolean isRbInstapay) {

        if (mPresenter != null) {
            mPresenter.unifiedNativePay(
                    money,
                    payType,
                    tradeType,
                    discountAmount,
                    null,
                    mark,
                    isInstapay,
                    isInstapayQRPayment,
                    isRbInstapay,
                    bean
            );
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 从系统设置权限完成后返回app的处理
        if (requestCode == PermissionsManage.REQUEST_CODE) {
            if (PermissionsManage.isGranted(CaptureActivity.this, Permission.CAMERA)) {
//                restartCamera();
                KotlinUtils.INSTANCE.reStartApp(CaptureActivity.this);
            } else {
                PermissionDialogUtils.finishWithoutPermission(CaptureActivity.this,
                        Permission.Group.CAMERA, true);
            }
        }
    }

    private void initData() {
        surfaceView = findViewById(R.id.preview_view);
        surfaceHolder = surfaceView.getHolder();

        if (hasSurface) {
            initCamera(surfaceHolder, false);
        } else {
            surfaceHolder.addCallback(this);
            surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }
        decodeFormats = null;
        characterSet = null;

        playBeep = true;
        AudioManager audioService = (AudioManager) getSystemService(AUDIO_SERVICE);
        if (audioService.getRingerMode() != AudioManager.RINGER_MODE_NORMAL) {
            playBeep = false;
        }
        initBeepSound();
        vibrate = true;
    }

    private void paseRMBWithSurcharge(BigDecimal bigDecimal, BigDecimal bigSurcharge) {
        try {

            if (!MainApplication.getInstance().getFeeType().equalsIgnoreCase("CNY")) {
                tv_pase.setVisibility(View.GONE);
                BigDecimal totalCharge = null;
                if (MainApplication.getInstance().getSourceToUsdExchangeRate() > 0 && MainApplication.getInstance().getUsdToRmbExchangeRate() > 0) {
                    BigDecimal sourceToUsd = new BigDecimal(MainApplication.getInstance().getSourceToUsdExchangeRate());
                    BigDecimal usdRate = new BigDecimal(MainApplication.getInstance().getUsdToRmbExchangeRate());
                    totalCharge = bigSurcharge.add(bigDecimal).multiply(sourceToUsd).setScale(MainApplication.getInstance().getNumFixed(), BigDecimal.ROUND_HALF_UP).
                            multiply(usdRate).setScale(MainApplication.getInstance().getNumFixed(), BigDecimal.ROUND_DOWN);
                } else {
                    BigDecimal rate = new BigDecimal(MainApplication.getInstance().getExchangeRate());
                    totalCharge = bigSurcharge.add(bigDecimal).multiply(rate).setScale(MainApplication.getInstance().getNumFixed(), BigDecimal.ROUND_FLOOR);
                }
//                String totalStr = "(" + getString(R.string.tx_bill_stream_pay_money) + ":" + DateUtil.formatPaseMoney(bigDecimal) + "," + getString(R.string.tx_surcharge) + ":" + DateUtil.formatPaseMoney(bigSurcharge) + ")";
                tv_money.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatPaseMoney(bigDecimal));
                tv_surcharge.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatPaseMoney(bigSurcharge));
                tv_pase.setText(getString(R.string.tx_about) + getString(R.string.tx_mark) + DateUtil.formatPaseRMBMoney(totalCharge));
            } else {
                //如果是人民币
                tv_pase.setVisibility(View.GONE);

                tv_money.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatPaseMoney(bigDecimal));
                tv_surcharge.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatPaseMoney(bigSurcharge));
            }
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Logger.e("hehui", "paseRMB-->" + e);
        }
    }

    private void changeTextSize(String mt) {
        String totalvalues = mt;
        if (totalvalues.length() > 12) {
            txtResult.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
        } else {
            txtResult.setTextSize(TypedValue.COMPLEX_UNIT_SP, 40);
        }
        txtResult.setText(totalvalues);

    }

    @Override
    public boolean onKeyDown(int keycode, KeyEvent event) {
        if (keycode == KeyEvent.KEYCODE_BACK) {
            finish();
            return true;
        }
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
        CameraManager.get().closeDriver();
        if (!hasSurface) {
            SurfaceView surfaceView = findViewById(R.id.preview_view);
            SurfaceHolder surfaceHolder = surfaceView.getHolder();
            surfaceHolder.removeCallback(this);
        }
    }

    void closeCamera() {
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
    }

    @Override
    protected void onDestroy() {
        inactivityTimer.shutdown();
        super.onDestroy();
    }

    void restartCamera() {
        closeCamera();
        viewfinderView.setVisibility(View.VISIBLE);
        SurfaceView surfaceView = findViewById(R.id.preview_view);
        SurfaceHolder surfaceHolder = surfaceView.getHolder();
        initCamera(surfaceHolder, false);
    }

    private void initCamera(SurfaceHolder surfaceHolder, boolean isFirst) {
        try {
            CameraManager.get().openDriver(surfaceHolder, surfaceView);
        } catch (IOException ioe) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    ioe,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            return;
        } catch (RuntimeException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            return;
        }
        if (handler == null) {
            handler = new CaptureActivityHandler(this, decodeFormats, characterSet);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (!hasSurface) {
            hasSurface = true;
            initCamera(holder, false);
        }

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        hasSurface = false;

    }

    public ViewfinderView getViewfinderView() {
        return viewfinderView;
    }

    public Handler getHandler() {
        return handler;
    }

    public void drawViewfinder() {
        viewfinderView.drawViewfinder();
    }

    private void initBeepSound() {
        if (playBeep && mediaPlayer == null) {
            // The volume on STREAM_SYSTEM is not adjustable, and users found it
            // too loud,
            // so we now play on the music stream.
            setVolumeControlStream(AudioManager.STREAM_MUSIC);
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setOnCompletionListener(beepListener);

            AssetFileDescriptor file = getResources().openRawResourceFd(R.raw.beep);
            try {
                mediaPlayer.setDataSource(file.getFileDescriptor(), file.getStartOffset(), file.getLength());
                file.close();
                mediaPlayer.setVolume(BEEP_VOLUME, BEEP_VOLUME);
                mediaPlayer.prepare();
            } catch (IOException e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                mediaPlayer = null;
            }
        }
    }

    private void playBeepSoundAndVibrate() {
        if (playBeep && mediaPlayer != null) {
            mediaPlayer.start();
        }
        if (vibrate) {
            Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
            vibrator.vibrate(VIBRATE_DURATION);
        }
    }

    @Override
    protected boolean isLoginRequired() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public void queryOrderDetailSuccess(Order response) {
        if (response != null) {
            mOutTradeNo = response.getOutTradeNo();
            totalFee = response.money;
            OrderDetailsActivity.startActivity(CaptureActivity.this, response);
            finish();
        } else {
            toastDialog(CaptureActivity.this, R.string.show_order_no_find, new NewDialogInfo.HandleBtn() {

                @Override
                public void handleOkBtn() {
                    finish();
                }
            });
        }
    }

    @Override
    public void queryOrderDetailFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }

        if (error != null && !TextUtils.isEmpty(error.toString())) {

            toastDialog(CaptureActivity.this, error.toString(), new NewDialogInfo.HandleBtn() {

                @Override
                public void handleOkBtn() {
                    finish();
                }
            });
        } else {
            toastDialog(CaptureActivity.this, R.string.show_order_no_find, new NewDialogInfo.HandleBtn() {

                @Override
                public void handleOkBtn() {
                    finish();
                }
            });
        }
    }

    // 到spay 服务端查询订单
    private void queryFormServer(final String orderNo) {
        if (mPresenter != null) {
            mPresenter.queryOrderDetail(orderNo, MainApplication.getInstance().getMchId(), false);
        }
    }

    @Override
    public void scanQueryOrderDetailSuccess(Order response) {
        if (response != null) {
            PreAuthActivity.startActivity(CaptureActivity.this, response, 2);//预授权解冻 参数1
            finish();
        } else {
            toastDialog(CaptureActivity.this, R.string.show_order_no_find, new NewDialogInfo.HandleBtn() {

                @Override
                public void handleOkBtn() {
                    finish();
                }
            });
        }
    }

    @Override
    public void scanQueryOrderDetailFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }
        if (error != null && !TextUtils.isEmpty(error.toString())) {

            toastDialog(CaptureActivity.this, error.toString(), new NewDialogInfo.HandleBtn() {

                @Override
                public void handleOkBtn() {
                    finish();
                }
            });
        } else {
            toastDialog(CaptureActivity.this, R.string.show_order_no_find, new NewDialogInfo.HandleBtn() {

                @Override
                public void handleOkBtn() {
                    finish();
                }
            });
        }
    }

    public void scanPreAuthOrder(final String orderNo) {
        if (mPresenter != null) {
            mPresenter.scanQueryOrderDetail(orderNo, MainApplication.getInstance().getMchId(), 2);
        }
    }

    @Override
    public void queryCardPaymentOrderDetailsSuccess(Order response) {
        if (response != null) {
            //跳转到卡交易的详情页面
            MasterCardOrderDetailsActivity.startActivity(CaptureActivity.this, response);
            finish();
        } else {
            toastDialog(CaptureActivity.this, R.string.show_order_no_find, new NewDialogInfo.HandleBtn() {
                @Override
                public void handleOkBtn() {
                    finish();
                }
            });
        }
    }

    @Override
    public void queryCardPaymentOrderDetailsFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }
        if (error != null && !TextUtils.isEmpty(error.toString())) {
            toastDialog(CaptureActivity.this, error.toString(), new NewDialogInfo.HandleBtn() {
                @Override
                public void handleOkBtn() {
                    finish();
                }
            });
        } else {
            toastDialog(CaptureActivity.this, R.string.show_order_no_find, new NewDialogInfo.HandleBtn() {

                @Override
                public void handleOkBtn() {
                    finish();
                }
            });
        }
    }

    //根据单号查询卡支付订单详情
    public void scanCardPaymentOrder(final String orderNoMch) {
        if (mPresenter != null) {
            mPresenter.queryCardPaymentOrderDetails(null, orderNoMch);
        }
    }

    public void submitData(final String code, boolean vibration) {

        if (vibration) {
            playBeepSoundAndVibrate();
        }

        //扫码查单
        if (payType.equals("pay")) {
            // 查询该笔订单是否存在
            //如果开通预授权，则走新的逻辑---V3.1.0修改成如下的新逻辑
           /* if(MainApplication.isPre_authOpen == 1 ){
                queryFormServerWithPreAuth(code);
            }else{
                queryFormServer(code);
            }*/

            //左上角扫描二维码查订单需根据当前选择的收款类型查询对应数据库表，如果选择卡交易，则查询卡交易表，如果当前为‘消费’，则查询QR交易表
            //V3.1.0新修改---2021/03/24
            //订单列表页面，选择是收银还是预授权订单,还是卡交易
            String bill_sale_or_Pre_auth_or_Card_payment = PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth", "sale");
            //如果选择的是预授权则
            if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(bill_sale_or_Pre_auth_or_Card_payment, "pre_auth")) {
                scanPreAuthOrder(code);
            } else if (MainApplication.getInstance().hasCardPayment() && TextUtils.equals(bill_sale_or_Pre_auth_or_Card_payment, "card_payment")) {
                scanCardPaymentOrder(code);
            } else {
                queryFormServer(code);
            }

            return;
        } else {
            if (code.startsWith(MainApplication.getInstance().getMchId())) {
                //如果开通预授权，则走新的逻辑---V3.1.0修改成如下的新逻辑
             /*   if(MainApplication.isPre_authOpen == 1 ){
                    queryFormServerWithPreAuth(code);
                }else{
                    queryFormServer(code);
                }*/

                //左上角扫描二维码查订单需根据当前选择的收款类型查询对应数据库表，如果选择卡交易，则查询卡交易表，如果当前为‘消费’，则查询QR交易表
                //V3.1.0新修改---2021/03/24
                //订单列表页面，选择是收银还是预授权订单,还是卡交易
                String bill_sale_or_Pre_auth_or_Card_payment = PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth", "sale");
                //如果选择的是预授权则
                if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(bill_sale_or_Pre_auth_or_Card_payment, "pre_auth")) {
                    scanPreAuthOrder(code);
                } else if (MainApplication.getInstance().hasCardPayment() && TextUtils.equals(bill_sale_or_Pre_auth_or_Card_payment, "card_payment")) {
                    scanCardPaymentOrder(code);
                } else {
                    queryFormServer(code);
                }

                return;
            } else if (code.startsWith("http") && code.contains("qrId")) {
                //扫码激活
                ActivateScanOneActivity.startActivity(CaptureActivity.this, code);
                return;
            } else if (code.startsWith("spay://")) {
                //扫码登录
                String c = code.substring(code.lastIndexOf("=") + 1);
//            ScanCodeLoginActivity.startActivity(CaptureActivity.this, c);
                return;
            } else {
                if (mPresenter != null) {
                    mPresenter.doCoverOrder(code, money, tradeType, vardList, discountAmount, payOutTradeNo, mark);
                }
            }
        }

    }

    @Override
    public void doCoverOrderSuccess(QRcodeInfo response) {
        if (response != null) {
            if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                mOrderNo = response.outAuthNo;
            } else {
                mOrderNo = response.orderNo;
            }

            //查询订单获取状态 定时任务查询订单状态,状态不为2 才轮询
            if (response.order.state != null && !TextUtils.isEmpty(response.order.state) && !response.order.state.equals("2")) {
                // 查询订单获取状态 定时任务查询订单状态
                queryOrderGetStatus(mOrderNo, false, false, false);

            } else {
                //否则下单成功成功
                response.order.money = Utils.Integer.tryParse(money, 0);
                //交易成功清除Note
                NoteMarkActivity.setNoteMark("");

                if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                    PreAutFinishActivity.startActivity(CaptureActivity.this, response.order, 5);
                } else {
                    //交易成功进行语音播报
                    if (!BuildConfig.IS_POS_VERSION) {
                        voiceplay(response.order, pushTransmissionModel);
                    }
                    PayResultActivity.startActivity(mContext, response.order);
                }
                finish();
            }
        }
    }

    @Override
    public void doCoverOrderFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //String mmsg = null;
                if (error != null && error.toString().contains("SPAY")) {
                    String out_order_no = error.toString().substring(0, error.toString().length() - 4);
                    queryOrderGetStatus(out_order_no, false, false, false);
                } else if ("20".equals(error.toString())) {
                    //mmsg = "单笔限额已经超出";
                    //                                    ToastHelper.showError(getString(R.string.single_limit_exceeded));
                    toastDialog(CaptureActivity.this, R.string.single_limit_exceeded, new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            restartCamera();
                        }

                    });

                } else {
                    toastDialog(CaptureActivity.this, error.toString(), new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            finish();
                        }

                    });
                }
            }
        });
    }

    /**
     * 语音播报
     */
    private void voiceplay(Order order, PushTransmissionModel pushTransmissionModel) {
        StringBuffer sb = new StringBuffer();
//        sb.append(getResources().getString(R.string.successful_collection)).append(",");
        double totalfee = 0.0;
        if (null != order) {
            //如果是不包含小数的情况，则后台传多少的值就直接读多少的值
            if (minus == 1) {
                sb.append(order.totalFee).append(MainApplication.getInstance().getUserInfo().feeType);
            } else {
                totalfee = ((double) (order.totalFee) / minus);
                sb.append(totalfee).append(MainApplication.getInstance().getUserInfo().feeType);
            }
        }

        pushTransmissionModel.setVoiceContent(sb.toString());
        GTPushIntentService.startVoicePlay(mContext, pushTransmissionModel);
    }

    @Override
    public void unifiedPayReverseSuccess(Order response) {
        if (response != null) {
            toastDialog(CaptureActivity.this, R.string.order_cz_success_1, new NewDialogInfo.HandleBtn() {

                @Override
                public void handleOkBtn() {
                    CaptureActivity.this.finish();
                }
            });
        }
    }

    @Override
    public void unifiedPayReverseFailed(@Nullable Object error) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (error != null) {
                    toastDialog(CaptureActivity.this, error.toString(), new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            CaptureActivity.this.finish();
                        }
                    });
                }
            }
        });
    }

    /**
     * 冲正接口
     * <功能详细描述>
     *
     * @param orderNo
     * @see [类、类#方法、类#成员]
     */
    public void payReverse(final String orderNo) {
        if (mPresenter != null) {
            mPresenter.unifiedPayReverse(orderNo);
        }
    }

    /*
     * 冲正弹窗口
     */
    private void showWindowRever() {
        isPreAuth = MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth");
        dialogRever = new RefundCheckDialog(CaptureActivity.this, isPreAuth, RefundCheckDialog.REVERS, getString(R.string.public_cozy_prompt), getString(R.string.tx_dialog_money), mOutTradeNo, String.valueOf(money), null, new RefundCheckDialog.ConfirmListener() {
            @Override
            public void ok(String code) {

                dialogs = new DialogInfo(CaptureActivity.this, getString(R.string.public_cozy_prompt), getString(R.string.tx_dialog_reverse), getString(R.string.btnOk), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

                    @Override
                    public void handleOkBtn() {
                        queryOrderGetStatus(mOutTradeNo, true, true, false);
                        //   payReverse(outTradeNo);
                        if (dialogRever != null) {
                            dialogRever.dismiss();
                        }
                    }

                    @Override
                    public void handleCancelBtn() {
                        dialogs.cancel();
                    }
                }, null);

                DialogHelper.resize(CaptureActivity.this, dialogs);
                dialogs.show();

            }

            @Override
            public void cancel() {
                //继续查询
                timeCount = 15;
                queryOrderGetStatus(mOutTradeNo, false, false, false);

            }

        });
        dialogRever.show();

        dialogRever.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                return keycode == KeyEvent.KEYCODE_BACK;
            }
        });
    }

    private boolean mIsShowWindowReverse = false;

    @Override
    public void queryOrderByOrderNoSuccess(Order response, boolean isMoreQuery, boolean isRevers, String orderNo) {
        resultTime = System.currentTimeMillis();
        mIsShowWindowReverse = false;
        if (response != null) {
            //如果交易成功则将备注清空
            if (response.state.equals("2")) {
                NoteMarkActivity.setNoteMark("");
            }
            if (response.state.equals("2") && flag) {
                // 支付成功
                flag = false;
                if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                    PreAutFinishActivity.startActivity(CaptureActivity.this, response, 1);
                } else {
                    if (!BuildConfig.IS_POS_VERSION) {
                        voiceplay(response, pushTransmissionModel);
                    }
                    PayResultActivity.startActivity(mContext, response);
                }
                finish();
            } else {
                if (isRevers) {
                    if (response.state.equals("2")) {// 支付成功
                        if (isMoreQuery) {
                            showToastInfo(R.string.show_order_revers);
                        }
                        if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                            PreAutFinishActivity.startActivity(CaptureActivity.this, response, 1);
                        } else {
                            if (!BuildConfig.IS_POS_VERSION) {
                                voiceplay(response, pushTransmissionModel);
                            }
                            PayResultActivity.startActivity(mContext, response);
                        }
                        finish();
                    } else {
                        if (isMoreQuery) { // 再次查询后去冲正
                            payReverse(mOutTradeNo);
                        } else {
                            showWindowRever();
                        }
                    }
                    return;
                }

                if (timeCount >= 15) {
                    if (!(showDialog != null && showDialog.isShowing())) {
                        showDialog = new DialogInfo(CaptureActivity.this, getStringById(R.string.public_cozy_prompt), "", getStringById(R.string.btnOk), DialogInfo.SCAN_PAY, null, null);
                        DialogHelper.resize(CaptureActivity.this, showDialog);
                        showDialog.setOnKeyListener(new OnKeyListener() {

                            @Override
                            public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                                return keycode == KeyEvent.KEYCODE_BACK;
                            }
                        });
                        showDialog.show();
                        mHandler.post(myRunnable);
                    }
                    mOutTradeNo = orderNo;
                } else {
                    //1次15秒查询后，如果还未返回成功，调用冲正接口
                    if (!Constant.ORDER_STATE_SUCCESS.equals(response.getTradeState())) {
                        mOutTradeNo = orderNo;
                        mIsShowWindowReverse = true;
//                        showWindowRever();
                    }
                }


            }
        }
    }

    @Override
    public void queryOrderByOrderNoFailed(@Nullable Object error, boolean isMoreQuery, String orderNo) {
        resultTime = System.currentTimeMillis();
        mIsShowWindowReverse = false;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (error != null) {

                    if (timeCount >= 15) {
                        if (!(showDialog != null && showDialog.isShowing())) {
                            showDialog = new DialogInfo(CaptureActivity.this, getStringById(R.string.public_cozy_prompt), "", getStringById(R.string.btnOk), DialogInfo.SCAN_PAY, null, null);
                            DialogHelper.resize(CaptureActivity.this, showDialog);
                            showDialog.setOnKeyListener(new OnKeyListener() {

                                @Override
                                public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                                    return keycode == KeyEvent.KEYCODE_BACK;
                                }
                            });
                            showDialog.show();
                            mHandler.post(myRunnable);
                        }
                        mOutTradeNo = orderNo;
                    } else {
                        //1次15秒查询后，如果还未返回成功，调用冲正接口
                        mOutTradeNo = orderNo;
                        mIsShowWindowReverse = true;
//                        showWindowRever();
                    }


                    if (isMoreQuery) { // 再次查询后去冲正
                        payReverse(mOutTradeNo);
                    }
                }
            }
        });
    }

    /**
     * 查询订单获取状态
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void queryOrderGetStatus(final String orderNo, final boolean isRevers, final boolean isMoreQuery, boolean hideLoading) {
        if (mPresenter != null) {
            startTime = System.currentTimeMillis();
            mPresenter.queryOrderByOrderNo(orderNo, isMoreQuery, isRevers, hideLoading);
        }
    }

    public String isOpenFisrtpayType() {
        //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth", "sale");
        if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(saleOrPreauth, "pre_auth")) {
            Object object = SharedPreUtils.readProduct("dynPayType_pre_auth" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            if (object != null) {
                List<DynModel> list = (List<DynModel>) object;
                if (null != list && list.size() > 0 && list.size() == 1) {
                    return list.get(0).getApiCode();
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            Object object = SharedPreUtils.readProduct("ActiveDynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            Object object_CardPayment = SharedPreUtils.readProduct("cardPayment_ActiveDynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            List<DynModel> list = new ArrayList<>();
            List<DynModel> list_CardPayment = new ArrayList<>();
            if (object != null) {
                list = (List<DynModel>) object;
            }
            if (object_CardPayment != null) {
                list_CardPayment = (List<DynModel>) object_CardPayment;
            }
            //把两个数组拼接起来
            List<DynModel> listAll = new ArrayList<>();
            if (list != null && list.size() > 0) {
                listAll.addAll(list);
            }
            if (list_CardPayment != null && list_CardPayment.size() > 0) {
                listAll.addAll(list_CardPayment);
            }

            if (null != listAll && listAll.size() > 0 && listAll.size() == 1) {
                return listAll.get(0).getApiCode();
            } else {
                return null;
            }
        }
    }

    public String getNativePayType(String ApiCode) {
        //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth", "sale");
        if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(saleOrPreauth, "pre_auth")) {
            Object object = SharedPreUtils.readProduct("dynPayType_pre_auth" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            if (object != null) {
                List<DynModel> list = (List<DynModel>) object;
                if (null != list && list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getApiCode().equalsIgnoreCase(ApiCode)) {
                            return list.get(i).getNativeTradeType();
                        }
                    }
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            Object object = SharedPreUtils.readProduct("ActiveDynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            Object object_CardPayment = SharedPreUtils.readProduct("cardPayment_ActiveDynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());

            List<DynModel> list = new ArrayList<>();
            List<DynModel> list_CardPayment = new ArrayList<>();
            if (object != null) {
                list = (List<DynModel>) object;
            }
            if (object_CardPayment != null) {
                list_CardPayment = (List<DynModel>) object_CardPayment;
            }

            //把两个数组拼接起来
            List<DynModel> listAll = new ArrayList<>();
            if (list != null) {
                listAll.addAll(list);
            }
            if (list_CardPayment != null) {
                listAll.addAll(list_CardPayment);
            }

            if (null != listAll && listAll.size() > 0) {
                for (int i = 0; i < listAll.size(); i++) {
                    if (listAll.get(i).getApiCode().equalsIgnoreCase(ApiCode)) {
                        return listAll.get(i).getNativeTradeType();
                    }
                }
            } else {
                return null;
            }
        }
        return null;
    }

    @Override
    public void onClick(View v) {

    }

    //V3.1.0 版本新增MasterCard交易通道
    //判断当前的交易类型是否是卡交易类型
    //如果是MasterCard交易通道则走新的下单和查单的逻辑
    public boolean isMasterCardServiceType(String NativePayType) {
        if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getUserInfo().cardServiceType)) {
            String[] arrPays = MainApplication.getInstance().getUserInfo().cardServiceType.split("\\|");
            for (int i = 0; i < arrPays.length; i++) {
                if (NativePayType.equalsIgnoreCase(arrPays[i])) {
                    return true;
                }
            }
        }
        return false;
    }


}
