package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.*
import cn.swiftpass.enterprise.common.Constant
import cn.swiftpass.enterprise.intl.BuildConfig
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.CaptureContract
import cn.swiftpass.enterprise.mvp.utils.ParamsUtils
import cn.swiftpass.enterprise.utils.JsonUtil
import cn.swiftpass.enterprise.utils.SharedPreUtils
import cn.swiftpass.enterprise.utils.StringUtil
import com.example.common.sp.PreferenceUtil
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/11/22
 *
 * @命运不是风
 * @来回吹
 * @命运是大地
 * @走到哪你都在命运中
 */
class CapturePresenter : CaptureContract.Presenter {


    private var mView: CaptureContract.View? = null


    override fun queryOrderByOrderNo(
        orderNo: String?,
        isMoreQuery: Boolean,
        isRevers: Boolean,
        hideLoading: Boolean
    ) {
        mView?.let { view ->

            if (!hideLoading) {
                view.showLoading(R.string.confirming_waiting, ParamsConstants.COMMON_LOADING)
            }
            AppClient.queryOrderByOrderNo(
                Constant.PAY_ZFB_QUERY,
                MainApplication.getInstance().getUserId().toString(),
                MainApplication.getInstance().getMchId(),
                orderNo,
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.queryOrderByOrderNoFailed(it.message, isMoreQuery, orderNo)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"",
                                "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                Order::class.java
                            ) as Order
                            order.tradeState =
                                if (TextUtils.isEmpty(order.state)) 0 else order.state.toInt()
                            order.tradeType = order.service
                            order.openid = order.subOpenID
                            order.cashFeel =
                                if (TextUtils.isEmpty(order.cashFee)) 0 else order.cashFee.toLong()
                            order.setUplanDetailsBeans(order.discountDetail)
                            view.queryOrderByOrderNoSuccess(order, isMoreQuery, isRevers, orderNo)
                        }
                    }
                }
            )
        }
    }

    override fun unifiedPayReverse(orderNo: String?) {
        mView?.let { view ->
            view.showLoading(R.string.order_czing_waiting, ParamsConstants.COMMON_LOADING)


            AppClient.unifiedPayReverse(
                MainApplication.getInstance().getUserId().toString() + "",
                MainApplication.getInstance().getMchId() + "",
                orderNo,
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.unifiedPayReverseFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"",
                                "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                Order::class.java
                            ) as Order
                            order.state = order.tradeState.toString()
                            view.unifiedPayReverseSuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun doCoverOrder(
        code: String?,
        money: String?,
        payType: String?,
        vardOrders: List<WxCard>?,
        discountAmount: Long,
        outTradeNo: String?,
        mark: String?
    ) {
        mView?.let { view ->
            view.showLoading(R.string.tx_pay_loading, ParamsConstants.COMMON_LOADING)
            val deskName = PreferenceUtil.getString(ParamsConstants.CASHIER_DESK_NAME, "")
            val body = if (!TextUtils.isEmpty(MainApplication.getInstance().userInfo.tradeName)) {
                MainApplication.getInstance().userInfo.tradeName
            } else {
                BuildConfig.body
            }
            AppClient.doCoverOrder(
                payType,
                outTradeNo,
                discountAmount.toString(),
                Constant.CLIENT,
                code,
                money,
                MainApplication.getInstance().getMchId(),
                mark,
                deskName,
                MainApplication.getInstance().userInfo.realname,
                MainApplication.getInstance().getUserId().toString() + "",
                body,
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.doCoverOrderFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"", "\"discountDetail\":[]"
                            )
                            val qRcodeInfo = QRcodeInfo()
                            val order = JsonUtil.jsonToBean(
                                response.message, Order::class.java
                            ) as Order
                            qRcodeInfo.orderNo = order.outTradeNo
                            qRcodeInfo.isQpay = false
                            qRcodeInfo.money = order.money.toString()
                            order.state = order.tradeState.toString()
                            order.cashFeel =
                                if (TextUtils.isEmpty(order.cashFee)) 0 else order.cashFee.toLong()

                            //Uplan ---V3.0.5迭代新增
                            //Uplan 获取满减等信息
                            if (null != order.discountDetail && order.discountDetail.size > 0) {
                                order.setUplanDetailsBeans(order.discountDetail)
                            }

                            if (null != vardOrders && vardOrders.size > 0) {
                                order.setWxCardList(vardOrders)
                            }

                            qRcodeInfo.order = order
                            view.doCoverOrderSuccess(qRcodeInfo)
                        }
                    }
                }
            )
        }
    }

    override fun queryCardPaymentOrderDetails(outTradeNo: String?, orderNoMch: String?) {

        mView?.let { view ->
            view.showLoading(R.string.tx_query_loading, ParamsConstants.COMMON_LOADING)
            AppClient.queryCardPaymentOrderDetails(
                outTradeNo,
                orderNoMch,
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.queryCardPaymentOrderDetailsFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            response.message = response.message.replace(
                                "\"data\":\"\"", "\"data\":[]"
                            )
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"", "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message, Order::class.java
                            ) as Order
                            order.useId = order.userId.toString()
                            order.cashFeel =
                                if (TextUtils.isEmpty(order.cashFee)) 0 else order.cashFee.toLong()
                            view.queryCardPaymentOrderDetailsSuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun scanQueryOrderDetail(orderNo: String?, mchId: String?, scanType: Int) {
        mView?.let { view ->
            view.showLoading(R.string.tx_query_loading, ParamsConstants.COMMON_LOADING)
            AppClient.scanQueryOrderDetail(
                orderNo,
                orderNo,
                mchId,
                MainApplication.getInstance().getUserId().toString(),
                System.currentTimeMillis().toString(),
                scanType,
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.scanQueryOrderDetailFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"", "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message, Order::class.java
                            ) as Order
                            order.add_time =
                                if (TextUtils.isEmpty(order.addTime)) 0 else order.addTime.toLong()
                            order.useId = order.userId.toString()
                            order.canAffim = order.canAffirm
                            order.cashFeel =
                                if (TextUtils.isEmpty(order.cashFee)) 0 else order.cashFee.toLong()
                            view.scanQueryOrderDetailSuccess(order)
                        }
                    }
                }
            )

        }
    }

    override fun queryOrderDetail(orderNo: String?, mchId: String?, isMark: Boolean) {

        mView?.let { view ->
            view.showLoading(R.string.tx_query_loading, ParamsConstants.COMMON_LOADING)
            AppClient.queryOrderDetail(
                orderNo,
                orderNo,
                orderNo,
                mchId,
                MainApplication.getInstance().getUserId().toString(),
                System.currentTimeMillis().toString(),
                isMark,
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.queryOrderDetailFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"", "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message, Order::class.java
                            ) as Order
                            order.add_time =
                                if (TextUtils.isEmpty(order.addTime)) 0 else order.addTime.toLong()
                            order.useId = order.userId.toString()
                            order.canAffim = order.canAffirm
                            order.setUplanDetailsBeans(order.discountDetail)
                            view.queryOrderDetailSuccess(order)

                        }
                    }
                }
            )
        }
    }

    override fun unifiedNativePay(
        money: String?,
        payType: String?,
        traType: String?,
        adm: Long,
        outTradeNo: String?,
        note: String?,
        isInstapay: Boolean,
        isInstapayQRPayment: Boolean,
        isRbInstapay: Boolean,
        bean: WalletListBean?
    ) {
        mView?.let { view ->
            view.showLoading(R.string.tv_pay_prompt, ParamsConstants.COMMON_LOADING)
            val deskName = PreferenceUtil.getString(ParamsConstants.CASHIER_DESK_NAME, "")
            var pt = ParamsUtils.updateType(payType)
            val body = if (!TextUtils.isEmpty(MainApplication.getInstance().userInfo.tradeName)) {
                MainApplication.getInstance().userInfo.tradeName
            } else {
                BuildConfig.body
            }
            AppClient.unifiedNativePay(
                money,
                note,
                deskName,
                outTradeNo,
                adm.toString(),
                MainApplication.getInstance().userInfo.realname,
                MainApplication.getInstance().getUserId().toString() + "",
                body,
                MainApplication.getInstance().getMchId(),
                Constant.CLIENT,
                pt,
                traType,
                pt,
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<QRcodeInfo>(QRcodeInfo()) {


                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.unifiedNativePayFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val order = JsonUtil.jsonToBean(
                                response.message, QRcodeInfo::class.java
                            ) as QRcodeInfo
                            if (!StringUtil.isEmptyOrNull(order.service) && order.service == Constant.PAY_QQ_NATIVE1) {
                                val codeImg = order.codeImg
                                val code = codeImg.substring(0, codeImg.lastIndexOf("pay"))
                                val tokenId = codeImg.substring(codeImg.lastIndexOf("=") + 1)
                                order.uuId = code + "pay/qqpay?token_id=" + tokenId
                            } else {
                                val codeUlr = order.uuid
                                if (!TextUtils.isEmpty(codeUlr) && codeUlr != "null") {
                                    order.uuId = codeUlr
                                } else {
                                    // 截取code_img_url
                                    val code_img_url = order.codeImg
                                    if (!TextUtils.isEmpty(code_img_url)) {
                                        order.uuId =
                                            code_img_url.substring(code_img_url.lastIndexOf("=") + 1)
                                    }
                                }
                            }
                            order.totalMoney = order.money
                            order.payType = payType
                            order.orderNo = order.outTradeNo
                            order.apiCode = payType
                            view.unifiedNativePaySuccess(
                                order, isInstapay, isInstapayQRPayment, isRbInstapay, bean, pt
                            )
                        }
                    }
                }
            )
        }
    }

    override fun masterCardNativePay(money: String?, apiCode: String?, attach: String?) {
        mView?.let { view ->
            view.showLoading(R.string.tv_pay_prompt, ParamsConstants.COMMON_LOADING)
            val deskName = PreferenceUtil.getString(ParamsConstants.CASHIER_DESK_NAME, "")

            val mchId = MainApplication.getInstance().getMchId()
            val mCardPaymentPayTypeMap = SharedPreUtils.readProduct(
                ParamsConstants.CARD_PAYMENT_PAY_TYPE_MAP + BuildConfig.bankCode + mchId
            )
            val map = mCardPaymentPayTypeMap as Map<String, String>

            val body = if (!TextUtils.isEmpty(MainApplication.getInstance().userInfo.tradeName)) {
                MainApplication.getInstance().userInfo.tradeName
            } else {
                BuildConfig.body
            }

            AppClient.masterCardNativePay(
                mchId,
                map[apiCode],
                apiCode,
                body,
                Constant.CLIENT,
                money,
                deskName,
                attach,
                MainApplication.getInstance().userInfo.realname,
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<MasterCardInfo>(MasterCardInfo()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.masterCardNativePayFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val order = JsonUtil.jsonToBean(
                                response.message, MasterCardInfo::class.java
                            ) as MasterCardInfo
                            view.masterCardNativePaySuccess(order, apiCode)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: CaptureContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}