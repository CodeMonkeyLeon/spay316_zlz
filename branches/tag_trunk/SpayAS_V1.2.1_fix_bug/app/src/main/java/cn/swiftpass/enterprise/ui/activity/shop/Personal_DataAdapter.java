package cn.swiftpass.enterprise.ui.activity.shop;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.City;

public class Personal_DataAdapter extends BaseAdapter
{
    private LayoutInflater inflater;
    
    private Context context;
    
    private List<City> citys;
    
    public Personal_DataAdapter(Context context, List<City> citys)
    {
        this.context = context;
        this.citys = citys;
        this.inflater = LayoutInflater.from(context);
        
    }
    
    @Override
    public int getCount()
    {
        return citys.size();
    }
    
    @Override
    public Object getItem(int position)
    {
        return citys.get(position);
    }
    
    @Override
    public long getItemId(int position)
    {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final ViewHolder holder;
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.personal_pop_list_item, null);
            holder = new ViewHolder();
            holder.name = (TextView)convertView.findViewById(R.id.username_spinner_item);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder)convertView.getTag();
        }
        City city = citys.get(position);
        holder.name.setText(city.getCity());
        return convertView;
        
    }
    
    public class ViewHolder
    {
        public TextView name;
    }
    
}
