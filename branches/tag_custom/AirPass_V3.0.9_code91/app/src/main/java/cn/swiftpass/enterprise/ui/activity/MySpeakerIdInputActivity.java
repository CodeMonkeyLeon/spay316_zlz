package cn.swiftpass.enterprise.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.swiftpass.enterprise.intl.R;

/**
 * Created by aijingya on 2019/8/19.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: （扫码+ 输入添加云音箱界面）
 * @date 2019/8/19.14:30.
 */
public class MySpeakerIdInputActivity extends TemplateActivity {
    @BindView(R.id.et_input_speaker_id)
    EditText etInputSpeakerId;
    @BindView(R.id.iv_speaker_scan)
    ImageView ivSpeakerScan;
    @BindView(R.id.btn_add_speaker_next)
    Button btnAddSpeakerNext;
    @BindView(R.id.iv_deletContent)
    ImageView ivDeletContent;

    private String speakerIDCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_speaker);
        ButterKnife.bind(this);

        initView();
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        speakerIDCode = intent.getStringExtra("speakerIDCode");
        etInputSpeakerId.setText(speakerIDCode);
        etInputSpeakerId.setSelection(etInputSpeakerId.getText().length());
    }


    public void initView() {
        etInputSpeakerId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().trim().length() > 0 ){
                    ivDeletContent.setVisibility(View.VISIBLE);
                }else{
                    ivDeletContent.setVisibility(View.GONE);
                }

                if (s.toString().trim().length() >= 6) {
                    btnAddSpeakerNext.setEnabled(true);
                } else {
                    btnAddSpeakerNext.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }


    @OnClick({R.id.iv_speaker_scan, R.id.btn_add_speaker_next,R.id.iv_deletContent})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_speaker_scan:
                //只要点击扫码，则清空输入框的内容，然后跳转到扫码界面
                etInputSpeakerId.setText("");
                showPage(MySpeakerScanActivity.class);
                break;
            case R.id.btn_add_speaker_next:
                //带数据跳转到绑定页面
                Intent intent = new Intent();
                intent.putExtra("speakerIDCode", etInputSpeakerId.getText().toString().trim());
                intent.setClass(MySpeakerIdInputActivity.this, MySpeakerBindActivity.class);
                startActivity(intent);
                break;
            case R.id.iv_deletContent:
                etInputSpeakerId.setText("");
                break;
        }
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(getStringById(R.string.terminal_add_speaker));
        titleBar.setLeftButtonVisible(true);
    }

}
