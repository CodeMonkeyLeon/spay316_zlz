package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.CashierAddBean
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo
import cn.swiftpass.enterprise.bussiness.model.UserModel
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @所以对于我来说
 * @真实未必在现实里
 * @梦往往是最值得信赖的
 * @它不说谎
 */
class CashierAddContract {


    interface View : BaseView {


        fun cashierAddSuccess(
            response: CashierAddBean?,
            isTag: Boolean
        )

        fun cashierAddFailed(
            error: Any?,
            isTag: Boolean,
            userModel: UserModel?
        )

        fun ecdhKeyExchangeSuccess(
            response: ECDHInfo?,
            userModel: UserModel?,
            isTag: Boolean
        )

        fun ecdhKeyExchangeFailed(error: Any?)


        fun cashierDeleteSuccess(response: Boolean)

        fun cashierDeleteFailed(error: Any?)


        fun cashierResetPswSuccess(response: CashierAddBean?)

        fun cashierResetPswFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {


        fun cashierResetPsw(userId: Long)


        fun cashierDelete(userId: Long)


        fun ecdhKeyExchange(
            publicKey: String?,
            userModel: UserModel?,
            isTag: Boolean
        )


        fun cashierAdd(
            userModel: UserModel?,
            isAddCashier: Boolean,
            isTag: Boolean
        )
    }
}