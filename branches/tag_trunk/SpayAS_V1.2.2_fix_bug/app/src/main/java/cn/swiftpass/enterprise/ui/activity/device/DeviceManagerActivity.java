package cn.swiftpass.enterprise.ui.activity.device;

import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.user.CashierAddActivity;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.StringUtil;

public class DeviceManagerActivity extends TemplateActivity
{
    private ViewHolder holder;
    
    private ListView listView;
    
    private TextView tv_device_default;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        
        super.onCreate(savedInstanceState);
        setContentView(R.layout.device_manager);
        
        listView = getViewById(R.id.device_manager_id);
        tv_device_default = getViewById(R.id.tv_device_default);
        listView.setOnItemClickListener(new OnItemClickListener()
        {
            
            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3)
            {
                try
                {
                    
                }
                catch (Exception e)
                {
                }
            }
        });
        loadData(0, true);
    }
    
    /** {@inheritDoc} */
    
    @Override
    protected void onResume()
    {
        super.onResume();
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void loadData(final int page, final boolean isLoadMore)
    {
        
        UserManager.queryDeviceList(new UINotifyListener<List<String>>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                if (!isLoadMore)
                {
                    showLoading(false, R.string.public_data_loading);
                }
            }
            
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dismissLoading();
                if (checkSession())
                {
                    return;
                }
                
                if (object != null)
                {
                    showToastInfo(object.toString());
                }
                tv_device_default.setVisibility(View.GONE);
            }
            
            @Override
            public void onSucceed(List<String> result)
            {
                super.onSucceed(result);
                dismissLoading();
                
                if (result != null && result.size() > 0)
                {
                    DeviceAdapter adapter = new DeviceAdapter(result);
                    
                    listView.setAdapter(adapter);
                }
                else
                {
                    tv_device_default.setVisibility(View.GONE);
                }
                
            }
        });
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setTitle(R.string.tv_device_title);
        titleBar.setLeftButtonVisible(true);
        titleBar.setRightButtonVisible(false);
        
        //        titleBar.setRightButLayVisible(true, 0);
        //        titleBar.setRightButLayBackground(R.drawable.icon_add);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                //                finish();
                showPage(CashierAddActivity.class);
            }
            
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
        });
    }
    
    private class DeviceAdapter extends BaseAdapter
    {
        
        private List<String> deviceList;
        
        private DeviceAdapter(List<String> deviceList)
        {
            this.deviceList = deviceList;
        }
        
        @Override
        public int getCount()
        {
            return deviceList.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            return deviceList.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(DeviceManagerActivity.this, R.layout.device_list_item, null);
                holder = new ViewHolder();
                holder.tv_device_content = (TextView)convertView.findViewById(R.id.tv_device_content);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            
            String devString = deviceList.get(position);
            if (!StringUtil.isEmptyOrNull(devString))
            {
                holder.tv_device_content.setText(devString);
            }
            
            return convertView;
        }
    }
    
    private class ViewHolder
    {
        private TextView tv_device_content;
        
    }
    
}
