package cn.swiftpass.enterprise.bussiness.logica.bill;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.BaseManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalInfo;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalItemInfo;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.NetHelper;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.MD5;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 账单
 *
 * @author he_hui
 * @version [版本号, 2016-11-17]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class BillOrderManager extends BaseManager {
    private static final String TAG = BillOrderManager.class.getSimpleName();
    @Override
    public void init() {
        // TODO Auto-generated method stub

    }

    @Override
    public void destory() {
        // TODO Auto-generated method stub

    }

    public static BillOrderManager getInstance() {
        return Container.instance;
    }

    private static class Container {
        public static BillOrderManager instance = new BillOrderManager();
    }

    public void paseToJson(List<String> listStr, List<Integer> tradeType, List<Integer> payType) {
        if (listStr != null && listStr.size() > 0) {
            for (String s : listStr) {
                if (s.equals("wx")) {
                    tradeType.add(1);
                } else if (s.equals("zfb")) {
                    tradeType.add(2);
                } else if (s.equals("jd")) {
                    tradeType.add(12);
                } else if (s.equals("qq")) {
                    tradeType.add(4);
                } else if (s.equals("2")) {
                    payType.add(2);
                } else if (s.equals("1")) {
                    payType.add(1);
                } else if (s.equals("3")) {
                    payType.add(3);
                } else if (s.equals("4")) {
                    payType.add(4);
                } else if (s.equals("8")) {
                    payType.add(8);
                }
            }
        }
    }

    /**
     * 汇总 和 走势
     * <功能详细描述>
     *
     * @param money    isReund =true代表查询退款 false是流水
     * @param listener countMethod 1、走势  2、汇总 (int)
     * @see [类、类#方法、类#成员]
     */
    public void orderCount(final String startTime, final String endTime, final int countMethod, final String userId, final String countDayKind, final UINotifyListener<OrderTotalInfo> listener) {
        ThreadHelper.executeWithCallback(new Executable<OrderTotalInfo>() {
            @Override
            public OrderTotalInfo execute() throws Exception {

                JSONObject json = new JSONObject();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);// todo
                } else {
                    json.put("mchId", MainApplication.getMchId());// todo
                }
                json.put("freeQueryTime", 1);
                json.put("startTime", startTime);
                json.put("endTime", endTime);
                json.put("countMethod", countMethod);

                //如果是收银员登录的同时收银员也没有账单权限
                if (MainApplication.isAdmin.equals("0") && MainApplication.isOrderAuth.equals("0")) // 
                {
                    if (MainApplication.userId > 0) {

                        json.put("userId", MainApplication.userId);
                    } else {
                        json.put("userId", MainApplication.getUserId());
                    }
                }
                if (!StringUtil.isEmptyOrNull(userId)) {
                    json.put("userId", userId);
                }
                if (!StringUtil.isEmptyOrNull(countDayKind)) {
                    json.put("countDayKind", countDayKind);
                }

                String url = ApiConstant.BASE_URL_PORT + "spay/orderCount";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                try {
                    long spayRs = System.currentTimeMillis();
                    //                    //加签名
                    if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                        json.put("spayRs", spayRs);
                        json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                    }
                    Logger.i("hehui", "orderCount params-->" + json);

                    OrderTotalInfo info = new OrderTotalInfo();
                    RequestResult result = NetHelper.httpsPost(url, json, String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    if (!result.hasError()) {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200) {
                            Logger.i("hehui", "orderCount-->" + result.data.getString("message"));

                            JSONObject js = new JSONObject(result.data.getString("message"));
                            Integer reqTime = result.data.optInt("reqFeqTime", 0);
                            info.setReqFeqTime(reqTime);
                            info.setCountTotalCount(js.optLong("countTotalCount", 0));
                            info.setCountTotalFee(js.optLong("countTotalFee", 0));
                            info.setCountTotalRefundCount(js.optLong("countTotalRefundCount", 0));
                            info.setCountTotalRefundFee(js.optLong("countTotalRefundFee", 0));
                            info.setOrderTotalItemInfo((List<OrderTotalItemInfo>) JsonUtil.jsonToList(js.getString("dataList"), new com.google.gson.reflect.TypeToken<List<OrderTotalItemInfo>>() {
                            }.getType()));
                            return info;
                        } else {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }

                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return null;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return null;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return null;
                        }
                    }

                } catch (Exception e) {
                    Logger.e("hehui", "" + e);
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }

                return null;
            }
        }, listener);

    }

    /**
     * 交易流水 退款流水 订单搜索
     * <功能详细描述>
     *
     * @param money    isReund =true代表查询退款 false是流水
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void querySpayOrder(final Integer reqFeqTime, final List<String> listStr, final List<String> payTypeList, final int isReund,
                               final int page, final String order,
                               final String userId, final String startDate, final UINotifyListener<List<Order>> listener) {
        ThreadHelper.executeWithCallback(new Executable<List<Order>>() {
            @Override
            public List<Order> execute() throws Exception {

                List<Order> resultList = new ArrayList<Order>();

                JSONObject json = new JSONObject();

                json.put("page", page);
                json.put("pageSize", 20);
                List<Integer> tradeType = new ArrayList<Integer>();
                List<Integer> payType = new ArrayList<Integer>();
                paseToJson(listStr, tradeType, payType);
                Map<String, Object> mapParam = new HashMap<String, Object>();
                mapParam.put("pageSize", "20");
                if (!StringUtil.isEmptyOrNull(MainApplication.getMchId())) {
                    mapParam.put("mchId", MainApplication.getMchId());
                    json.put("mchId", MainApplication.getMchId());
                } else {
                    mapParam.put("mchId", MainApplication.getMchId());
                    json.put("mchId", MainApplication.getMchId());
                }

                //如果是收银员登录的同时收银员也没有账单权限
                if (MainApplication.isAdmin.equals("0") && MainApplication.isOrderAuth.equals("0"))
                {
                    if (MainApplication.userId > 0) {

                        mapParam.put("userId", MainApplication.userId);
                        json.put("userId", MainApplication.userId);
                    } else {
                        mapParam.put("userId", MainApplication.getUserId());
                        json.put("userId", MainApplication.getUserId());
                    }
                }

                if (!StringUtil.isEmptyOrNull(userId)) {
                    mapParam.put("userId", userId);
                    json.put("userId", userId);
                }


                if (!StringUtil.isEmptyOrNull(startDate)) {
                    if (isReund == 1) { //退款不用时分秒
                        mapParam.put("startDate", startDate);
                        mapParam.put("endDate", startDate);
                        json.put("startDate", startDate);
                        json.put("endDate", startDate);
                    } else {
                        mapParam.put("startDate", startDate + " 00:00:00");
                        mapParam.put("endDate", startDate + " 23:59:59");
                        json.put("startDate", startDate + " 00:00:00");
                        json.put("endDate", startDate + " 23:59:59");
                    }

                } else {
                    if (isReund == 1) { //退款不用时分秒

                        mapParam.put("startDate", DateUtil.formatYYMD(System.currentTimeMillis()));
                        mapParam.put("endDate", DateUtil.formatYYMD(System.currentTimeMillis()));

                        json.put("startDate", DateUtil.formatYYMD(System.currentTimeMillis()));
                        json.put("endDate", DateUtil.formatYYMD(System.currentTimeMillis()));

                    } else {

                        mapParam.put("startDate", DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00");
                        mapParam.put("endDate", DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59");

                        json.put("startDate", DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00");
                        json.put("endDate", DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59");
                    }

                }

                if (payTypeList != null && payTypeList.size() > 0) {
                    mapParam.put("apiProviderList", payTypeList);
                }

                if (!StringUtil.isEmptyOrNull(order)) {
                    mapParam.put("orderNoMch", order);
                    json.put("orderNoMch", order);
                }
                if (tradeType.size() > 0) {
                    mapParam.put("apiProviderList", tradeType);
                }

                if (payType.size() > 0) {
                    //                    json.put("tradeStateList", payType);
                    mapParam.put("tradeStateList", payType);
                }

                mapParam.put("page", page);
                String url = null;
                if (isReund == 1) {
                    //url = ApiConstant.BASE_URL_PORT + "spay/searchRefundOrderV2";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                    url = ApiConstant.BASE_URL_PORT + "spay/refundList";
                } else if (isReund == 0) {//流水
                    //如果有预授权权限，同时账单页面选择的是预授权
                    if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth","sale"),"pre_auth")){
                        url = ApiConstant.BASE_URL_PORT + "spay/authOrderList";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                    }else{
                        url = ApiConstant.BASE_URL_PORT + "spay/order/orderList";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                    }

                }

                long spayRs = System.currentTimeMillis();
                //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {

                    mapParam.put("spayRs", spayRs);
                    json.put("spayRs", spayRs);
                    mapParam.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                }

                Logger.i("hehui", "querySpayOrder params-->" + JsonUtil.mapToJsons(mapParam));
                try {
                    RequestResult result = NetHelper.httpsPost(url, JsonUtil.mapToJsons(mapParam), String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    if (!result.hasError()) {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200) {
                            JSONObject js = new JSONObject(result.data.getString("message"));
                            JSONArray array = new JSONArray(js.getString("data"));

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);
                                Order order = new Order();
                                order.setOutRefundNo(object.optString("outRefundNo", ""));
                                order.setTransactionId(object.optString("transactionId"));
                                order.setMchName(object.optString("mchName"));
                                order.setTradeType(object.optString("tradeType", ""));
                                order.setOutTradeNo(object.optString("outTradeNo"));
                                order.setOutAuthNo(object.optString("outAuthNo"));
                                order.setAuthNo(object.optString("authNo"));
                                order.setMoney(object.optLong("money", 0));
                                order.setTradeTime(object.optString("tradeTime"));
                                order.setTradeName(object.optString("tradeName"));
                                order.add_time = object.optLong("addTime");
                                order.setTradeState(object.optInt("tradeState", -1));
                                order.setTradeStateText(object.optString("tradeStateText"));
                                order.notifyTime = object.optString("notifyTime", "");
                                order.setBody(object.optString("body", ""));
                                order.setOrderNoMch(object.optString("orderNoMch", ""));
                                order.setRufundMark(object.optInt("rufundMark", 0));
                                order.setClient(object.optString("client", ""));
                                order.setAddTime(object.optString("addTime", ""));
                                order.setUserName(object.optString("userName", ""));
                                order.setUseId(object.optString("userId", ""));
                                order.setRefundMoney(object.optLong("refundMoney", 0));
                                order.setRfMoneyIng(object.optLong("rfMoneyIng", 0));
                                order.setAffirm(object.optInt("affirm", 0));
                                order.setDaMoney(object.optLong("daMoney", 0));
                                order.setIsAgainPay(object.optInt("isAgainPay", 0));
                                order.setAttach(object.optString("attach", ""));
                                order.setApiProvider(object.optInt("apiProvider", 0));
                                if (isReund == 0) {
                                    String tradeTimeNew = object.optString("tradeTimeNew", "");
                                    if (!StringUtil.isEmptyOrNull(tradeTimeNew)) {
                                        try {
                                            order.setFormatTimePay(DateUtil.formartDateYYMMDD(tradeTimeNew));
                                            order.setFormartYYMM(DateUtil.formartDateYYMMDDTo(tradeTimeNew));
                                            order.setTradeTimeNew(DateUtil.formartDateToHHMMSS(tradeTimeNew));
                                        } catch (ParseException e) {
                                            Log.e(TAG,Log.getStackTraceString(e));
                                        }
                                    }
                                } else if (isReund == 1) {
                                    String time = object.optString("addTimeNew", "");
                                    order.setRefundState(object.optInt("refundState", 0));
                                    order.setMoney(object.optLong("refundFee", 0));
                                    if (!StringUtil.isEmptyOrNull(time)) {
                                        try {
                                            order.setFormatRefund(DateUtil.formartDateYYMMDD(time));
                                            order.setFormartYYMM(DateUtil.formartDateYYMMDDTo(time));
                                            order.setAddTimeNew(DateUtil.formartDateToHHMMSS(time));
                                        } catch (ParseException e) {
                                            Log.e(TAG,Log.getStackTraceString(e));
                                        }
                                    }
                                } else {
                                    String useTimeNew = object.optString("useTimeNew", "");
                                    if (!StringUtil.isEmptyOrNull(useTimeNew)) {
                                        try {
                                            order.setFromatCard(DateUtil.formartDateYYMMDD(useTimeNew));

                                            order.setFormartYYMM(DateUtil.formartDateYYMMDDTo(useTimeNew));
                                            order.setUseTimeNew(DateUtil.formartDateToHHMMSS(useTimeNew));
                                        } catch (ParseException e) {
                                            // TODO Auto-generated catch block
                                            Log.e(TAG,Log.getStackTraceString(e));
                                        }
                                    }

                                    order.setBrandName(object.optString("brandName", ""));
                                    order.setCardCode(object.optString("cardCode", ""));
                                    order.setTitle(object.optString("cardTitle", ""));
                                    order.setCardId(object.optString("cardId", ""));
                                    order.setNickName(object.optString("nickName", ""));
                                    order.setUseTime(object.optLong("useTime", 0));
                                    order.setTotalRows(object.optLong("totalRows", 0));
                                    order.setDescription(object.optString("description", ""));
                                }

                                resultList.add(order);
                            }
                            try {
                                Integer reqFeqTime = result.data.optInt("reqFeqTime", 0);

                                if (resultList.size() > 0 && resultList.get(0) != null) {
                                    //resultList.get(0).setMap(JsonUtil.jsonToMap(js.getString("dayCountMap")));
//                                    resultList.get(0).setPageCount(js.optInt("pageCount", 0));
                                    resultList.get(0).setReqFeqTime(reqFeqTime);
                                } else {
                                    if (reqFeqTime > 0) {
                                        listener.onError("reqFeqTime=" + reqFeqTime);
                                    }
                                    return resultList;
                                }

                            } catch (Exception e) {
                                Log.e(TAG,Log.getStackTraceString(e));
                            }

                            return resultList;
                        } else {
                            listener.onError(result.data.getString("message"));
                            return resultList;
                        }

                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return null;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return null;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return null;
                        }
                    }

                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }

                return resultList;
            }
        }, listener);

    }

    /**
     * 交易流水 退款流水 订单搜索
     * 预授权搜索，预授权列表
     *
     * @param money    isReund =true代表查询退款 false是流水
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void querySpayOrderNew(final Integer reqFeqTime, final List<String> listStr, final List<String> payTypeList, final int isReund,
                               final int page, final String order,final int isUnfreezen,
                               final String userId, final String startDate, final UINotifyListener<List<Order>> listener) {
        ThreadHelper.executeWithCallback(new Executable<List<Order>>() {
            @Override
            public List<Order> execute() throws Exception {

                List<Order> resultList = new ArrayList<Order>();

                JSONObject json = new JSONObject();

                json.put("page", page);
                json.put("pageSize", 20);
                List<Integer> tradeType = new ArrayList<Integer>();
                List<Integer> payType = new ArrayList<Integer>();
                paseToJson(listStr, tradeType, payType);
                Map<String, Object> mapParam = new HashMap<String, Object>();
                mapParam.put("pageSize", "20");
                if (!StringUtil.isEmptyOrNull(MainApplication.getMchId())) {
                    mapParam.put("mchId", MainApplication.getMchId());
                    json.put("mchId", MainApplication.getMchId());
                } else {
                    mapParam.put("mchId", MainApplication.getMchId());
                    json.put("mchId", MainApplication.getMchId());
                }

                if(isUnfreezen == 1){
                    mapParam.put("operationType", 2);
                    json.put("operationType", 2);
                }


                //如果是收银员登录的同时收银员也没有账单权限
                if (MainApplication.isAdmin.equals("0") && MainApplication.isOrderAuth.equals("0"))
                {
                    if (MainApplication.userId > 0) {

                        mapParam.put("userId", MainApplication.userId);
                        json.put("userId", MainApplication.userId);
                    } else {
                        mapParam.put("userId", MainApplication.getUserId());
                        json.put("userId", MainApplication.getUserId());
                    }
                }

                if (!StringUtil.isEmptyOrNull(userId)) {
                    mapParam.put("userId", userId);
                    json.put("userId", userId);
                }


                if (!StringUtil.isEmptyOrNull(startDate)) {
                    if (isReund == 1) { //退款不用时分秒
                        mapParam.put("startDate", startDate);
                        mapParam.put("endDate", startDate);
                        json.put("startDate", startDate);
                        json.put("endDate", startDate);
                    } else {
                        mapParam.put("startDate", startDate + " 00:00:00");
                        mapParam.put("endDate", startDate + " 23:59:59");
                        json.put("startDate", startDate + " 00:00:00");
                        json.put("endDate", startDate + " 23:59:59");
                    }

                } else {
                    if (isReund == 1) { //退款不用时分秒

                        mapParam.put("startDate", DateUtil.formatYYMD(System.currentTimeMillis()));
                        mapParam.put("endDate", DateUtil.formatYYMD(System.currentTimeMillis()));

                        json.put("startDate", DateUtil.formatYYMD(System.currentTimeMillis()));
                        json.put("endDate", DateUtil.formatYYMD(System.currentTimeMillis()));

                    } else {

                        mapParam.put("startDate", DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00");
                        mapParam.put("endDate", DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59");

                        json.put("startDate", DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00");
                        json.put("endDate", DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59");
                    }

                }

                if (payTypeList != null && payTypeList.size() > 0) {
                    mapParam.put("apiProviderList", payTypeList);
                }

                if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth","sale"),"pre_auth") ){
                    if (!StringUtil.isEmptyOrNull(order)) {
                        mapParam.put("authNo", order);
                        json.put("authNo", order);
                    }
                }else{
                    if (!StringUtil.isEmptyOrNull(order)) {
                        mapParam.put("orderNoMch", order);
                        json.put("orderNoMch", order);
                    }
                }

                if (tradeType.size() > 0) {
                    mapParam.put("apiProviderList", tradeType);
                }

                if (payType.size() > 0) {
                    //                    json.put("tradeStateList", payType);
                    mapParam.put("tradeStateList", payType);
                }

                mapParam.put("page", page);
                String url = null;
                if (isReund == 1 && isUnfreezen == 0) {//退款列表
                    //url = ApiConstant.BASE_URL_PORT + "spay/searchRefundOrderV2";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                    url = ApiConstant.BASE_URL_PORT + "spay/refundList";
                } else if (isReund == 0 && isUnfreezen == 0) {//流水列表
                    //如果有预授权权限，同时账单页面选择的是预授权
                    if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth","sale"),"pre_auth")){
                        url = ApiConstant.BASE_URL_PORT + "spay/authOrderList";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                    }else{
                        url = ApiConstant.BASE_URL_PORT + "spay/order/orderList";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                    }
                }else if(isUnfreezen == 1){//预授权解冻列表
                    url = ApiConstant.BASE_URL_PORT + "spay/authOperateQuery";
                }

                long spayRs = System.currentTimeMillis();
                //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {

                    mapParam.put("spayRs", spayRs);
                    json.put("spayRs", spayRs);
                    mapParam.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                }

                Logger.i("hehui", "querySpayOrder params-->" + JsonUtil.mapToJsons(mapParam));
                try {
                    RequestResult result = NetHelper.httpsPost(url, JsonUtil.mapToJsons(mapParam), String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    if (!result.hasError()) {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200) {
                            JSONObject js = new JSONObject(result.data.getString("message"));
                            JSONArray array = new JSONArray(js.getString("data"));
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);
                                Order order = new Order();

                                order.setRefundNo(object.optString("refundNo",""));
                                order.setOutRefundNo(object.optString("outRefundNo", ""));
                                order.setTransactionId(object.optString("transactionId"));
                                order.setMchName(object.optString("mchName"));
                                order.setTradeType(object.optString("tradeType", ""));
                                order.setOutTradeNo(object.optString("outTradeNo"));
                                order.setOutAuthNo(object.optString("outAuthNo"));
                                order.setAuthNo(object.optString("authNo"));
                                order.setMoney(object.optLong("money", 0));
                                order.setTradeTime(object.optString("tradeTime"));
                                order.setTradeName(object.optString("tradeName",""));
                                order.add_time = object.optLong("addTime");
                                order.setTradeState(object.optInt("tradeState", -1));
                                order.setTradeStateText(object.optString("tradeStateText"));
                                order.notifyTime = object.optString("notifyTime", "");
                                order.setBody(object.optString("body", ""));
                                order.setOrderNoMch(object.optString("orderNoMch", ""));
                                order.setRufundMark(object.optInt("rufundMark", 0));
                                order.setClient(object.optString("client", ""));
                                order.setAddTime(object.optString("addTime", ""));
                                order.setUserName(object.optString("userName", ""));
                                order.setUseId(object.optString("userId", ""));
                                order.setRefundMoney(object.optLong("refundMoney", 0));
                                order.setRfMoneyIng(object.optLong("rfMoneyIng", 0));
                                order.setAffirm(object.optInt("affirm", 0));
                                order.setDaMoney(object.optLong("daMoney", 0));
                                order.setIsAgainPay(object.optInt("isAgainPay", 0));
                                order.setAttach(object.optString("attach", ""));
                                order.setApiProvider(object.optInt("apiProvider", 0));
                                //如果是解冻列表，设置解冻状态
                                order.setOperationStatus(object.optInt("operationStatus", 0));
                                order.setOperationType(object.optInt("operationType", 0));
                                order.setOperateTime(object.optString("operateTime", ""));
                                order.setFinishedTime(object.optString("finishedTime", ""));
                                order.setOutRequestNo(object.optString("outRequestNo", ""));

                                String operateTimeNew = object.optString("operateTime", "");
                                if (!StringUtil.isEmptyOrNull(operateTimeNew)) {
                                    try {
                                        order.setOperateTimeFormat(DateUtil.formartDateToHHMMSS(operateTimeNew));
                                    } catch (ParseException e) {
                                        Log.e(TAG,Log.getStackTraceString(e));
                                    }
                                }

                                if (isReund == 0) {
                                    String tradeTimeNew = object.optString("tradeTimeNew", "");
                                    if (!StringUtil.isEmptyOrNull(tradeTimeNew)) {
                                        try {
                                            order.setFormatTimePay(DateUtil.formartDateYYMMDD(tradeTimeNew));
                                            order.setFormartYYMM(DateUtil.formartDateYYMMDDTo(tradeTimeNew));
                                            order.setTradeTimeNew(DateUtil.formartDateToHHMMSS(tradeTimeNew));
                                        } catch (ParseException e) {
                                            Log.e(TAG,Log.getStackTraceString(e));
                                        }
                                    }
                                } else if (isReund == 1) {
                                    String time = object.optString("addTimeNew", "");
                                    order.setRefundState(object.optInt("refundState", 0));
                                    order.setMoney(object.optLong("refundFee", 0));
                                    if (!StringUtil.isEmptyOrNull(time)) {
                                        try {
                                            order.setFormatRefund(DateUtil.formartDateYYMMDD(time));
                                            order.setFormartYYMM(DateUtil.formartDateYYMMDDTo(time));
                                            order.setAddTimeNew(DateUtil.formartDateToHHMMSS(time));
                                        } catch (ParseException e) {
                                            Log.e(TAG,Log.getStackTraceString(e));
                                        }
                                    }
                                } else {
                                    String useTimeNew = object.optString("useTimeNew", "");
                                    if (!StringUtil.isEmptyOrNull(useTimeNew)) {
                                        try {
                                            order.setFromatCard(DateUtil.formartDateYYMMDD(useTimeNew));

                                            order.setFormartYYMM(DateUtil.formartDateYYMMDDTo(useTimeNew));
                                            order.setUseTimeNew(DateUtil.formartDateToHHMMSS(useTimeNew));
                                        } catch (ParseException e) {
                                            // TODO Auto-generated catch block
                                            Log.e(TAG,Log.getStackTraceString(e));
                                        }
                                    }

                                    order.setTitle(object.optString("cardTitle", ""));
                                    order.setCardId(object.optString("cardId", ""));
                                    order.setNickName(object.optString("nickName", ""));
                                    order.setUseTime(object.optLong("useTime", 0));
                                    order.setTotalRows(object.optLong("totalRows", 0));
                                    order.setDescription(object.optString("description", ""));
                                }

                                resultList.add(order);
                            }
                            try {
                                Integer reqFeqTime = result.data.optInt("reqFeqTime", 0);

                                if (resultList.size() > 0 && resultList.get(0) != null) {
                                    //resultList.get(0).setMap(JsonUtil.jsonToMap(js.getString("dayCountMap")));
//                                    resultList.get(0).setPageCount(js.optInt("pageCount", 0));
                                    resultList.get(0).setReqFeqTime(reqFeqTime);
                                } else {
                                    if (reqFeqTime > 0) {
                                        listener.onError("reqFeqTime=" + reqFeqTime);
                                    }
                                    return resultList;
                                }

                            } catch (Exception e) {
                                Log.e(TAG,Log.getStackTraceString(e));
                            }

                            return resultList;
                        } else {
                            listener.onError(result.data.getString("message"));
                            return resultList;
                        }

                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return null;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return null;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return null;
                        }
                    }

                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }

                return resultList;
            }
        }, listener);

    }

    /**
     * 交易流水 退款流水
     * <功能详细描述>
     *
     * @param money    isReund =true代表查询退款 false是流水
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void querySpayOrder(String tag,final Integer reqFeqTime, final List<String> listStr, final List<String> payTypeList, final int isReund, final int page, final String order, final String startDate, final UINotifyListener<List<Order>> listener) {
        ThreadHelper.executeWithCallback(new Executable<List<Order>>() {
            @Override
            public List<Order> execute() throws Exception {

                List<Order> resultList = new ArrayList<Order>();

                JSONObject json = new JSONObject();

                json.put("page", page);
                json.put("pageSize", 20);
                List<Integer> tradeType = new ArrayList<Integer>();
                List<Integer> payType = new ArrayList<Integer>();
                paseToJson(listStr, tradeType, payType);
                Map<String, Object> mapParam = new HashMap<String, Object>();
                mapParam.put("pageSize", "20");
                if (!StringUtil.isEmptyOrNull(MainApplication.getMchId())) {
                    mapParam.put("mchId", MainApplication.getMchId());
                    json.put("mchId", MainApplication.getMchId());
                } else {
                    mapParam.put("mchId", MainApplication.getMchId());
                    json.put("mchId", MainApplication.getMchId());
                }
                if (MainApplication.isAdmin.equals("0") && MainApplication.isOrderAuth.equals("0")) //
                {
                    if (MainApplication.userId > 0) {

                        mapParam.put("userId", MainApplication.userId);
                        json.put("userId", MainApplication.userId);
                    } else {
                        mapParam.put("userId", MainApplication.getUserId());
                        json.put("userId", MainApplication.userId);
                    }
                }
                if (!StringUtil.isEmptyOrNull(startDate)) {
                    if (isReund == 1) { //退款不用时分秒
                        mapParam.put("startDate", null);
                        mapParam.put("endDate", null);
                        json.put("startDate", null);
                        json.put("endDate", null);
                    } else {
                        mapParam.put("startDate",null);
                        mapParam.put("endDate", null);
                        json.put("startDate", null);
                        json.put("endDate", null);
                    }

                } else {
                    if (isReund == 1) { //退款不用时分秒

                        mapParam.put("startDate",null);
                        mapParam.put("endDate", null);

                        json.put("startDate", null);
                        json.put("endDate", null);

                    } else {

                        mapParam.put("startDate",null);
                        mapParam.put("endDate", null);

                        json.put("startDate", null);
                        json.put("endDate", null);
                    }

                }

                if (payTypeList != null && payTypeList.size() > 0) {
                    mapParam.put("apiProviderList", payTypeList);
                }
                if (!StringUtil.isEmptyOrNull(order)) {
                    mapParam.put("orderNo", order);
                    json.put("orderNo", order);
                }
                if (tradeType.size() > 0) {
                    mapParam.put("apiProviderList", tradeType);
                }

                if (payType.size() > 0) {
                    //                    json.put("tradeStateList", payType);
                    mapParam.put("tradeStateList", payType);
                }

                mapParam.put("page", page);
                String url = null;
                if (isReund == 1) {
                    //url = ApiConstant.BASE_URL_PORT + "spay/searchRefundOrderV2";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                    url =ApiConstant.BASE_URL_PORT + "spay/refundList";
                } else if (isReund == 0) {//流水
                    // ur url = ApiConstant.BASE_URL_PORT + "spay/order/querySpayOrderV2";
                    url = ApiConstant.BASE_URL_PORT + "spay/order/orderList";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                } else {
                    url = ApiConstant.BASE_URL_PORT + "spay/wxCardGetUserListV2"; //卡券

                    if (!StringUtil.isEmptyOrNull(startDate)) {
                        mapParam.put("cardTime", startDate);
                    } else {
                        mapParam.put("cardTime", DateUtil.formatYYMD(System.currentTimeMillis()));
                    }
                }

                long spayRs = System.currentTimeMillis();
                //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {

                    mapParam.put("spayRs", spayRs);
                    json.put("spayRs", spayRs);
                    mapParam.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                }

                Logger.i("hehui", "querySpayOrder params-->" + JsonUtil.mapToJsons(mapParam));
                try {
                    RequestResult result = NetHelper.httpsPost(url, JsonUtil.mapToJsons(mapParam), String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    if (!result.hasError()) {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200) {
                            JSONObject js = new JSONObject(result.data.getString("message"));
                            JSONArray array = new JSONArray(js.getString("data"));
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);
                                Order order = new Order();
                                order.setRefundNo(object.optString("refundNo", ""));
                                order.setRefundTimeNew(object.optString("refundTimeNew", ""));
                                order.setOutRefundNo(object.optString("outRefundNo", ""));
                                order.setTransactionId(object.optString("transactionId"));
                                order.setMchName(object.optString("mchName"));
                                order.setTradeType(object.optString("tradeType", ""));
                                order.setOutTradeNo(object.optString("outTradeNo"));
                                order.money = object.optLong("money", 0);
                                order.setTradeTime(object.optString("tradeTime"));
                                order.setTradeName(object.optString("tradeName"));
                                order.add_time = object.optLong("addTime");
                                order.setTradeState(object.optInt("tradeState", -1));
                                order.setTradeStateText(object.optString("tradeStateText"));
                                order.notifyTime = object.optString("notifyTime", "");
                                order.setBody(object.optString("body", ""));
                                order.setOrderNoMch(object.optString("orderNoMch", ""));
                                order.setRufundMark(object.optInt("rufundMark", 0));
                                order.setClient(object.optString("client", ""));
                                order.setAddTime(object.optString("addTime", ""));
                                order.setUserName(object.optString("userName", ""));
                                order.setUseId(object.optString("userId", ""));
                                order.setRefundMoney(object.optLong("refundMoney", 0));
                                order.setRfMoneyIng(object.optLong("rfMoneyIng", 0));
                                order.setAffirm(object.optInt("affirm", 0));
                                order.setDaMoney(object.optLong("daMoney", 0));
                                order.setIsAgainPay(object.optInt("isAgainPay", 0));
                                order.setAttach(object.optString("attach", ""));
                                order.setApiProvider(object.optInt("apiProvider", 0));
                                if (isReund == 0) {
                                    String tradeTimeNew = object.optString("tradeTimeNew", "");
                                    if (!StringUtil.isEmptyOrNull(tradeTimeNew)) {
                                        try {
                                            order.setFormatTimePay(DateUtil.formartDateYYMMDD(tradeTimeNew));
                                            order.setFormartYYMM(DateUtil.formartDateYYMMDDTo(tradeTimeNew));
                                            order.setTradeTimeNew(DateUtil.formartDateToHHMMSS(tradeTimeNew));
                                        } catch (ParseException e) {
                                            Log.e(TAG,Log.getStackTraceString(e));
                                        }
                                    }
                                } else if (isReund == 1) {
                                    String time = object.optString("addTimeNew", "");
                                    order.setRefundState(object.optInt("refundState", 0));
                                    order.setMoney(object.optLong("refundFee", 0));
                                    if (!StringUtil.isEmptyOrNull(time)) {
                                        try {
                                            order.setFormatRefund(DateUtil.formartDateYYMMDD(time));
                                            order.setFormartYYMM(DateUtil.formartDateYYMMDDTo(time));
                                            order.setAddTimeNew(DateUtil.formartDateToHHMMSS(time));
                                        } catch (ParseException e) {
                                            Log.e(TAG,Log.getStackTraceString(e));
                                        }
                                    }
                                } else {
                                    String useTimeNew = object.optString("useTimeNew", "");
                                    if (!StringUtil.isEmptyOrNull(useTimeNew)) {
                                        try {
                                            order.setFromatCard(DateUtil.formartDateYYMMDD(useTimeNew));

                                            order.setFormartYYMM(DateUtil.formartDateYYMMDDTo(useTimeNew));
                                            order.setUseTimeNew(DateUtil.formartDateToHHMMSS(useTimeNew));
                                        } catch (ParseException e) {
                                            // TODO Auto-generated catch block
                                            Log.e(TAG,Log.getStackTraceString(e));
                                        }
                                    }

                                    order.setBrandName(object.optString("brandName", ""));
                                    order.setCardCode(object.optString("cardCode", ""));
                                    order.setTitle(object.optString("cardTitle", ""));
                                    order.setCardId(object.optString("cardId", ""));
                                    order.setNickName(object.optString("nickName", ""));
                                    order.setUseTime(object.optLong("useTime", 0));
                                    order.setTotalRows(object.optLong("totalRows", 0));
                                    order.setDescription(object.optString("description", ""));
                                }

                                resultList.add(order);
                            }
                            try {
                                Integer reqFeqTime = result.data.optInt("reqFeqTime", 0);

                                if (resultList.size() > 0 && resultList.get(0) != null) {
                                    //resultList.get(0).setMap(JsonUtil.jsonToMap(js.getString("dayCountMap")));
//                                    resultList.get(0).setPageCount(js.optInt("pageCount", 0));
                                    resultList.get(0).setReqFeqTime(reqFeqTime);
                                } else {
                                    if (reqFeqTime > 0) {
                                        listener.onError("reqFeqTime=" + reqFeqTime);
                                    }
                                    return resultList;
                                }

                            } catch (Exception e) {
                                Log.e(TAG,Log.getStackTraceString(e));
                            }

                            return resultList;
                        } else {
                            listener.onError(result.data.getString("message"));
                            return resultList;
                        }

                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return null;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return null;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return null;
                        }
                    }

                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }

                return resultList;
            }
        }, listener);

    }

    /**
     * cxy
     * SPAY 预授权解冻
     */
    public void authUnfreeze(final String authNo,final String money,final String remark, final UINotifyListener<Order> listener){
        ThreadHelper.executeWithCallback(new Executable<Order>() {
            @Override
            public Order execute() throws Exception {

                JSONObject json = new JSONObject();
                Map<String, Object> mapParam = new HashMap<String, Object>();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);// todo
                    mapParam.put("mchId", MainApplication.merchantId);// todo
                } else {
                    json.put("mchId", MainApplication.getMchId());// todo
                    mapParam.put("mchId", MainApplication.getMchId());// todo
                }
                json.put("userId", MainApplication.userId);
                mapParam.put("userId", MainApplication.userId);

                json.put("authNo", authNo);
                mapParam.put("authNo", authNo);

                json.put("token", MainApplication.refundToken);
                mapParam.put("token", MainApplication.refundToken);

                if (null != MainApplication.body && !"".equals(MainApplication.body) && !MainApplication.body.equals("null")) {
                    json.put("remark", MainApplication.body);
                    mapParam.put("remark", MainApplication.body);

                } else {
                    json.put("remark", ApiConstant.body);
                    mapParam.put("remark", ApiConstant.body);
                }


                json.put("money", money);
                mapParam.put("money", money);

                String url = ApiConstant.BASE_URL_PORT + "spay/authUnfreeze";
                try {
                    long spayRs = System.currentTimeMillis();
                    //加签名
                    String priKey = (String) SharedPreUtile.readProduct("secretKey");
                    json.put("spayRs", String.valueOf(spayRs));
                    mapParam.put("spayRs", String.valueOf(spayRs));

                    if(!StringUtil.isEmptyOrNull(MainApplication.skey)){
                        mapParam.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(priKey).substring(0,16)));

                    }else{
                        mapParam.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(String.valueOf(spayRs))));
                    }

                    RequestResult result = NetHelper.httpsPost(url, JsonUtil.mapToJsons(mapParam), String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    if (!result.hasError()) {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200) {
                            Order info = new Order();
                            JSONObject js = new JSONObject(result.data.getString("message"));

                            info.setAuthNo(js.optString("authNo",""));
                            info.setRequestNo(js.optString("requestNo",""));
                            info.setOutRequestNo(js.optString("outRequestNo",""));
                            info.setTotalFee(js.optLong("totalFee",0));
                            info.setUnFreezeTime(js.optString("unFreezeTime",""));
                            return info;
                        } else {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }

                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return null;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return null;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return null;
                        }
                    }

                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }

                return null;
            }
        }, listener);
    }

    /**
     * cxy
     * SPAY 预授权转支付查询
     */
    public void authPayQuery(final String orderNo,  final String outRequestNo,
                             final UINotifyListener<Order> listener){
        ThreadHelper.executeWithCallback(new Executable<Order>() {
            @Override
            public Order execute() throws Exception {
                Map<String, Object> mapParam = new HashMap<>();
                JSONObject json = new JSONObject();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);// todo
                    mapParam.put("mchId", MainApplication.merchantId);
                } else {
                    json.put("mchId", MainApplication.getMchId());// todo
                    mapParam.put("mchId", MainApplication.getMchId());// todo
                }
                json.put("userId", MainApplication.userId);
                mapParam.put("userId", MainApplication.userId);

                json.put("outRequestNo", outRequestNo);
                mapParam.put("outRequestNo", outRequestNo);

                json.put("orderNo", orderNo);
                mapParam.put("orderNo", orderNo);


                String url = ApiConstant.BASE_URL_PORT + "spay/authPayQuery";
                try {
                    long spayRs = System.currentTimeMillis();
                    //加签名
                    String priKey = (String) SharedPreUtile.readProduct("secretKey");
                    json.put("spayRs", String.valueOf(spayRs));
                    mapParam.put("spayRs", String.valueOf(spayRs));

                    if(!StringUtil.isEmptyOrNull(MainApplication.skey)){
                        mapParam.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(priKey).substring(0,16)));

                    }else{
                        mapParam.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(String.valueOf(spayRs))));
                    }

                    RequestResult result = NetHelper.httpsPost(url, JsonUtil.mapToJsons(mapParam), String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    if (!result.hasError()) {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200) {
                            Order info = new Order();
                            JSONObject js = new JSONObject(result.data.getString("message"));

                            Log.e("cxy","message--------"+js);

                            info.setTransactionId(js.optString("transactionId",""));
                            info.setRestAmount(js.optLong("restAmount",0));
                            info.setTotalUnfreezeAmount(js.optLong("totalUnfreezeAmount",0));
                            info.setTotalFreezeAmount(js.optLong("totalFreezeAmount",0));
                            info.setTotalPayAmount(js.optLong("totalPayAmount",0));
                            info.setRequestNo(js.optString("requestNo",""));
                            info.setOrderNoMch(js.optString("orderNoMch",""));

                            info.setAuthNo(js.optString("authNo",""));
                            info.setTradeName(js.optString("tradeName",""));
                            info.setBody(js.optString("body",""));
                            info.setOutTradeNo(js.optString("outTradeNo",""));
                            info.setMchCreateIp(js.optString("mchCreateIp",""));
                            info.setTimeStart(js.optString("timeStart",""));
                            info.setCenterId(js.optString("centerId",""));
                            info.setTradeState(js.optInt("tradeState",0));
                            info.setTransactionId(js.optString("transactionId",""));
                            info.setOrderNoMch(js.optString("orderNoMch",""));
                            info.setMoney(js.optLong("money",0));
                            info.setTotalFee(js.optLong("totalFee",0));
                            info.setSubOpenID(js.optString("subOpenID",""));
                            info.setTradeTime(js.optString("tradeTime",""));
                            info.setNotifyTime(js.optString("notifyTime",""));
                            info.setDaMoney(js.optLong("daMoney",0));
                            info.setState(js.optString("state",""));
                            info.setNeed_query(js.optString("need_query",""));

                            return info;
                        } else {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }

                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return null;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return null;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return null;
                        }
                    }

                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }

                return null;
            }
        }, listener);
    }

    /**
     * cxy
     * SPAY 预授权转支付
     */
    public void unifiedAuthPay(final String money,final String authNo,  final String body,
                             final UINotifyListener<Order> listener){
        ThreadHelper.executeWithCallback(new Executable<Order>() {
            @Override
            public Order execute() throws Exception {
                Map<String, Object> mapParam = new HashMap<>();
                JSONObject json = new JSONObject();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);// todo
                    mapParam.put("mchId", MainApplication.merchantId);
                } else {
                    json.put("mchId", MainApplication.getMchId());// todo
                    mapParam.put("mchId", MainApplication.getMchId());// todo
                }
                json.put("userId", MainApplication.userId);
                mapParam.put("userId", MainApplication.userId);

                json.put("authNo", authNo);
                mapParam.put("authNo", authNo);

                json.put("body", body);
                mapParam.put("body", body);

                json.put("money", money);
                mapParam.put("money", money);

                String url = ApiConstant.BASE_URL_PORT + "spay/unifiedAuthPay";
                try {
                    long spayRs = System.currentTimeMillis();
                    //加签名
                    String priKey = (String) SharedPreUtile.readProduct("secretKey");
                    json.put("spayRs", String.valueOf(spayRs));
                    mapParam.put("spayRs", String.valueOf(spayRs));

                    if(!StringUtil.isEmptyOrNull(MainApplication.skey)){
                        mapParam.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(priKey).substring(0,16)));

                    }else{
                        mapParam.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(String.valueOf(spayRs))));
                    }

                    RequestResult result = NetHelper.httpsPost(url, JsonUtil.mapToJsons(mapParam), String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    if (!result.hasError()) {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200) {
                            Order info = new Order();
                            JSONObject js = new JSONObject(result.data.getString("message"));
                            Log.e("cxy","预授权转支付--------"+js);
                            info.setAuthNo(js.optString("authNo",""));
                            info.setRequestNo(js.optString("requestNo",""));
                            info.setTotalFee(js.optLong("totalFee",0));
                            info.setUnFreezeTime(js.optString("unFreezeTime",""));
                            info.setTransactionId(js.optString("transactionId",""));
                            info.setOrderNoMch(js.optString("orderNoMch",""));
                            info.setTradeName(js.optString("tradeName",""));
                            info.setOutTradeNo(js.optString("outTradeNo",""));
                            return info;
                        } else {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }

                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return null;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return null;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return null;
                        }
                    }

                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }

                return null;
            }
        }, listener);
    }

    /**
     * 预授权操作列表
     * @param listener
     */
    public void authOperateQuery(final String operationType,final String authNo,
                                final String startDate,
                                 final String endDate,final int page,
                                 final int pageSize,
                                 final List<String> apiProviderList,final String langCode,
                                 final  UINotifyListener<List<Order>> listener){
        ThreadHelper.executeWithCallback(new Executable<List<Order>>() {
            @Override
            public List<Order> execute() throws Exception {
                List<Order> resultList = new ArrayList<>();
                JSONObject json = new JSONObject();
                Map<String, Object> mapParam = new HashMap<>();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);// todo
                    mapParam.put("mchId", MainApplication.merchantId);// todo
                } else {
                    json.put("mchId", MainApplication.getMchId());// todo
                    mapParam.put("mchId", MainApplication.getMchId());// todo
                }
                json.put("userId", MainApplication.userId);
                mapParam.put("userId", MainApplication.userId);

                json.put("authNo", authNo);
                mapParam.put("authNo", authNo);

                json.put("operationType", operationType);
                mapParam.put("operationType", operationType);

//                mapParam.put("startDate", DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00");
//                json.put("startDate", DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00");
//
//                mapParam.put("endDate", DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59");
//                json.put("endDate", DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59");

                json.put("page", page);
                mapParam.put("page", page);

                json.put("pageSize", pageSize);
                mapParam.put("pageSize", pageSize);

                String url = ApiConstant.BASE_URL_PORT + "spay/authOperateQuery";
                try {
                    long spayRs = System.currentTimeMillis();
                    //加签名
                    String priKey = (String) SharedPreUtile.readProduct("secretKey");
                    json.put("spayRs", String.valueOf(spayRs));
                    mapParam.put("spayRs", String.valueOf(spayRs));

                    if(!StringUtil.isEmptyOrNull(MainApplication.skey)){
                        mapParam.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(priKey).substring(0,16)));

                    }else{
                        mapParam.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MD5.md5s(String.valueOf(spayRs))));
                    }

                    RequestResult result = NetHelper.httpsPost(url, JsonUtil.mapToJsons(mapParam), String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    if (!result.hasError()) {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200) {
                            JSONObject js = new JSONObject(result.data.getString("message"));
                            JSONArray array = new JSONArray(js.getString("data"));
                            int pageCount = js.getInt("pageCount");

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);
                                Order order = new Order();

                                order.setPageCount(pageCount);//临时赋值给每个list

                                order.setOutRequestNo(object.optString("outRequestNo", ""));
                                order.setOrderNo(object.optString("orderNo", ""));
                                order.setAuthNo(object.optString("authNo", ""));
                                order.setMchId(object.optString("mchId", ""));
                                order.setMchName(object.optString("mchName", ""));
                                order.setRequestNo(object.optString("requestNo", ""));
                                order.setOutAuthNo(object.optString("outAuthNo", ""));
                                order.setFeeType(object.optInt("feeType", 0));
                                order.setMoney(object.optLong("money", 0));
                                order.setOperationStatus(object.optInt("operationStatus", 0));
                                order.setOperationType(object.optInt("operationType", 0));
                                order.setClient(object.optString("client", ""));
                                order.setOperateTime(object.optString("operateTime", ""));
                                order.setFinishTime(object.optLong("finishedTime", 0));
                                resultList.add(order);
                            }
                            return resultList;
                        } else {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }

                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return null;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return null;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return null;
                        }
                    }

                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.ts_fail_info));
                }

                return null;
            }
        }, listener);
    }
}
