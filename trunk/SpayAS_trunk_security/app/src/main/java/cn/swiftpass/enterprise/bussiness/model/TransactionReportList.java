package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by aijingya on 2019/4/15.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: ${TODO}(日报，周报，月报 数据列表的数据类型)
 * @date 2019/4/15.20:31.
 */
public class TransactionReportList implements Serializable {
    public ArrayList<TransactionReportBean> data;
}
