package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;
import cn.swiftpass.enterprise.utils.CreateOneDiCodeUtil;

/**
 * Created by aijingya on 2018/12/25.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/12/25.14:38.
 */

public class RotateCanvasViewActivity extends BaseActivity {


    private ImageView iv_code;
    private TextView tv_code;
    private String orderNoMch;

    public static void startActivity(Activity activity, String orderNoMch) {
        Intent intent = new Intent(activity, RotateCanvasViewActivity.class);
        intent.putExtra("orderNoMch", orderNoMch);
        activity.startActivity(intent);
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        orderNoMch = getIntent().getStringExtra("orderNoMch");

        if (TextUtils.isEmpty(orderNoMch)) {
            finish();
            return;
        }

        setContentView(R.layout.activity_rotate_view);

        initView();
    }

    private void initView() {
        iv_code = findViewById(R.id.iv_code);
        tv_code = findViewById(R.id.tv_code);

        WindowManager wm = this.getWindowManager();
        int width = wm.getDefaultDisplay().getWidth();
        int w = (int) (width * 0.35);
        int height = wm.getDefaultDisplay().getHeight();
        int h = (int) (height * 0.75);
        Bitmap tempBitmap = CreateOneDiCodeUtil.createCode(orderNoMch, h, w);
        iv_code.setImageBitmap(tempBitmap);
        tv_code.setText(orderNoMch);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        finish();
        return super.onTouchEvent(event);
    }

    @Override
    protected boolean isLoginRequired() {
        return false;
    }

}
