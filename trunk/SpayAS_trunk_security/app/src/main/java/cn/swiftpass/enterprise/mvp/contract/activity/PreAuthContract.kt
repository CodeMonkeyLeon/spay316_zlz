package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @阳光聚散
 * @我们不多说
 */
class PreAuthContract {

    interface View : BaseView {

        fun authOperateQuerySuccess(response: ArrayList<Order>?)

        fun authOperateQueryFailed(error: Any?)

        fun queryOrderDetailSuccess(response: Order?)

        fun queryOrderDetailFailed(error: Any?)


        fun authPayQuerySuccess(response: Order?)

        fun authPayQueryFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {


        fun authPayQuery(
            orderNo: String?,
            outRequestNo: String?
        )


        fun queryOrderDetail(
            orderNo: String?,
            mchId: String?,
            isMark: Boolean
        )


        fun authOperateQuery(
            operationType: String?,
            authNo: String?,
            page: Int,
            pageSize: Int
        )
    }


}