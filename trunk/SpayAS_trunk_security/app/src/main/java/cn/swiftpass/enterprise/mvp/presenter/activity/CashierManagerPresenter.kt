package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.UserModelList
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.CashierManagerContract
import cn.swiftpass.enterprise.utils.JsonUtil
import com.example.common.entity.EmptyData
import okhttp3.Call


/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @如果大地早已冰封
 * @就让我们面对着暖流
 * @走向海
 */
class CashierManagerPresenter : CashierManagerContract.Presenter {

    private var mView: CashierManagerContract.View? = null

    override fun queryCashier(
        page: Int,
        pageSize: Int,
        input: String?,
        type: Int
    ) {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
            AppClient.queryCashier(
                MainApplication.getInstance().getMchId(),
                page.toString(),
                input,
                pageSize.toString(),
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<UserModelList>(UserModelList()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.queryCashierFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val dynModels = JsonUtil.jsonToBean(
                                response.message,
                                UserModelList::class.java
                            ) as UserModelList
                            view.queryCashierSuccess(dynModels, type, page)
                        }
                    }
                }
            )
        }
    }

    override fun cashierDelete(
        userId: Long,
        position: Int
    ) {
        mView?.let { view ->
            view.showLoading(R.string.show_delete_loading, ParamsConstants.COMMON_LOADING)
            AppClient.cashierDelete(
                userId.toString(),
                MainApplication.getInstance().getMchId(),
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.cashierDeleteFailed(it.message)
                        }
                    }

                    override fun onResponse(response: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        view.cashierDeleteSuccess(true, position)
                    }
                }
            )

        }
    }

    override fun attachView(view: CashierManagerContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}