package cn.swiftpass.enterprise.ui.activity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnLoadListener;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnMenuItemClickListener;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnRefreshListener;
import com.tencent.stat.StatService;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.user.CashierAddActivity;
import cn.swiftpass.enterprise.ui.activity.user.RefundManagerActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.DisplayUtil;
import cn.swiftpass.enterprise.utils.Logger;

public class CashierManager extends TemplateActivity implements OnRefreshListener, OnLoadListener
{
    private static final String TAG = CashierManager.class.getSimpleName();
    //private PullDownListView cashier_list;
    
    private ViewHolder holder;
    
    private SwipeMenuListView listView;
    
    //private List<UserModel> list = new ArrayList<UserModel>();
    
    private List<UserModel> listUser;
    
    private Handler mHandler = new Handler();
    
    private CashierAdapter cashierAdapter;
    
    private int pageFulfil = 0;
    
    private int pageCount = 0;
    
    private EditText et_input;
    
    private DialogInfo dialogInfo;
    
    private TextView ly_cashier_no;
    
    private Handler handler = new Handler()
    {
        public void handleMessage(Message msg)
        {
            @SuppressWarnings("unchecked")
            List<UserModel> result = (List<UserModel>)msg.obj;
            switch (msg.what)
            {
                case SwipeMenuListView.REFRESH:
                    listUser.clear();
                    listUser.addAll(result);
                    break;
                case SwipeMenuListView.LOAD:
                    listUser.addAll(result);
                    break;
            }
            listView.onRefreshComplete();
            listView.onLoadComplete();
            listView.setResultSize(result.size());
            cashierAdapter.notifyDataSetChanged();
        };
    };
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cashier_manager);
        
        //        cashier_list = (PullDownListView)findViewById(R.id.cashier_manager_id);
        listUser = new ArrayList<UserModel>();
        cashierAdapter = new CashierAdapter(listUser);
        
        //        cashier_list.setAutoLoadMore(true);
        //        cashier_list.setRefreshListioner(this);
        //        listView = cashier_list.mListView;
        listView = getViewById(R.id.listView);
        listView.setAdapter(cashierAdapter);
        
        initView();
        
        et_input = getViewById(R.id.et_input);
        et_input.setFocusable(true);
        et_input.setFocusableInTouchMode(true);
        et_input.requestFocus();
        et_input.setOnTouchListener(new OnTouchListener()
        {
            
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    try {
                        StatService.trackCustomEvent(CashierManager.this, "kMTASPayMeCashierSearch", "收银员管理搜索入口");
                    } catch (Exception e) {
                        Log.e(TAG,Log.getStackTraceString(e));
                    }
                    Bundle b = new Bundle();
                    b.putBoolean("isFromOrderStreamSearch", true);
                    b.putInt("serchType", 2); //从收银员列表查询
                    showPage(RefundManagerActivity.class, b);
                }
                return true;
            }
        });
        
        //loadData();

        
        listView.setOnItemClickListener(new OnItemClickListener()
        {
            
            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3)
            {
                try
                {
                    UserModel userModel = listUser.get(position - 1);
                    if (userModel != null)
                    {
                        
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("userModel", userModel);
                        showPage(CashierAddActivity.class, bundle);
                    }
                    
                }
                catch (Exception e)
                {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
        });
        
    }
    
    private void initView()
    {
        ly_cashier_no = getViewById(R.id.ly_cashier_no);
        
        SwipeMenuCreator creator = new SwipeMenuCreator()
        {
            
            @Override
            public void create(SwipeMenu menu)
            {
                SwipeMenuItem openItem = new SwipeMenuItem(getApplicationContext());
                openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9, 0xCE)));
                openItem.setWidth(DisplayUtil.dip2Px(CashierManager.this, 90));
                openItem.setTitle(R.string.bt_delete);
                openItem.setTitleSize(18);
                openItem.setTitleColor(Color.WHITE);
                openItem.setBackground(new ColorDrawable(Color.RED));
                menu.addMenuItem(openItem);
                //                SwipeMenuItem deleteItem = new SwipeMenuItem(getApplicationContext());
                //                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9, 0x3F, 0x25)));
                //                deleteItem.setWidth(dp2px(90));
                //                deleteItem.setIcon(R.drawable.ic_delete);
                //                menu.addMenuItem(deleteItem);
            }
        };
        
        listView.setMenuCreator(creator);
        listView.setOnRefreshListener(this);
        listView.setOnLoadListener(this);
        listView.setOnMenuItemClickListener(new OnMenuItemClickListener()
        {
            @Override
            public void onMenuItemClick(int position, SwipeMenu menu, int index)
            {
                
                switch (index)
                {
                    case 0:
                        UserModel userModel = listUser.get(position);
                        if (userModel != null)
                        {
                            delete(userModel.getId(), position);
                        }
                        break;
                //                    case 1:
                //                        listUser.remove(position);
                //                        mAdapter.notifyDataSetChanged();
                //                        break;
                }
            }
        });
    }
    
    void delete(final long userId, final int position)
    {
        
        dialogInfo =
            new DialogInfo(CashierManager.this, getString(R.string.public_cozy_prompt),
                getString(R.string.dialog_delete), getString(R.string.btnOk), getString(R.string.btnCancel),
                DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn()
                {
                    
                    @Override
                    public void handleOkBtn()
                    {
                        UserManager.cashierDelete(userId, new UINotifyListener<Boolean>()
                        {
                            @Override
                            public void onPreExecute()
                            {
                                super.onPreExecute();
                                
                                showLoading(false, getString(R.string.show_delete_loading));
                            }
                            
                            @Override
                            public void onError(final Object object)
                            {
                                super.onError(object);
                                dismissLoading();
                                if (object != null)
                                {
                                    toastDialog(CashierManager.this, object.toString(), null);
                                }
                            }
                            
                            @Override
                            public void onSucceed(Boolean result)
                            {
                                super.onSucceed(result);
                                dismissLoading();
                                
                                if (result)
                                {
                                    toastDialog(CashierManager.this,
                                        R.string.show_delete_succ,
                                        new NewDialogInfo.HandleBtn()
                                        {
                                            
                                            @Override
                                            public void handleOkBtn()
                                            {
                                                listUser.remove(position);
                                                cashierAdapter.notifyDataSetChanged();
                                            }
                                        });
                                    //                                    showPage(CashierManager.class);
                                    //                                    finish();
                                }
                                
                            }
                        });
                    }
                    
                    @Override
                    public void handleCancleBtn()
                    {
                        dialogInfo.cancel();
                    }
                }, null);
        
        DialogHelper.resize(CashierManager.this, dialogInfo);
        dialogInfo.show();
    }
    
    /** {@inheritDoc} */
    boolean isTag = true;
    
    @Override
    protected void onResume()
    {
        // TODO Auto-generated method stub
        super.onResume();
        loadData(0, false, 2);
        if (isTag)
        {
            loadData(0, false, 0);
            isTag = false;
        }
        else
        {
            loadData(0, false, 2);
        }
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void loadData(final int page, final boolean isLoadMore, final int what)
    {
        
        UserManager.queryCashier(page, 10, null, new UINotifyListener<List<UserModel>>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                if (isLoadMore)
                {
                    showLoading(false, R.string.public_data_loading);
                }
            }
            
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dismissLoading();
                if (checkSession())
                {
                    return;
                }
                if (object != null)
                {
                    //                    showToastInfo(object.toString());
                    toastDialog(CashierManager.this, object.toString(), new NewDialogInfo.HandleBtn()
                    {
                        
                        @Override
                        public void handleOkBtn()
                        {
                            if (listUser.size() == 0)
                            {
                                finish();
                            }
                        }
                        
                    });
                }
            }
            
            @Override
            public void onSucceed(List<UserModel> result)
            {
                super.onSucceed(result);
                dismissLoading();
                
                if (result != null && result.size() > 0)
                {
                    //                    list = result;
                    //                    listUser.addAll(result);
                    //                    cashierAdapter.notifyDataSetChanged();
                    listView.setVisibility(View.VISIBLE);
                    if (what == 2)
                    {
                        listUser.clear();
                        listUser.addAll(result);
                        cashierAdapter.notifyDataSetChanged();
                        //                        listView.onLoadComplete();
                    }
                    else
                    {
                        Message msg = handler.obtainMessage();
                        msg.what = what;
                        msg.obj = result;
                        handler.sendMessage(msg);
                    }
                    //                    listView.setAdapter(new CashierAdapter(result));
                    pageCount = result.get(0).getPageCount();
                    //                    mySetListData(cashier_list, listUser, cashierAdapter, result, isLoadMore);
                    //                    if (isLoadMore)
                    //                    {
                    //                        pageFulfil += 1;
                    //                    }
                    
                    //                    cashier_list.setMore(true);
                    CashierManager.this.runOnUiThread(new Runnable()
                    {
                        
                        @Override
                        public void run()
                        {
                            
                            ly_cashier_no.setVisibility(View.GONE);
                            listView.setVisibility(View.VISIBLE);
                        }
                    });
                }
                else
                {
                    CashierManager.this.runOnUiThread(new Runnable()
                    {
                        
                        @Override
                        public void run()
                        {
                            if (listUser.size() == 0)
                            {
                                ly_cashier_no.setVisibility(View.VISIBLE);
                                ly_cashier_no.setText(R.string.tv_user_list);
                                listView.setVisibility(View.GONE);
                            }
                            //                            else
                            //                            {
                            //                                listView.onLoadComplete();
                            //                                //                                listView.setResultSize(0);
                            //                            }
                        }
                    });
                }
                
            }
        });
        
    }
    
//    private void mySetListData(final PullDownListView pull, List<UserModel> list, CashierAdapter adapter,
//        List<UserModel> result, boolean isLoadMore)
//    {
//        if (!isLoadMore)
//        {
//            pull.onfinish(getStringById(R.string.refresh_successfully));// 刷新完成
//
//            mHandler.postDelayed(new Runnable()
//            {
//                @Override
//                public void run()
//                {
//                    // 这里表示刷新处理完成后把上面的加载刷新界面隐藏
//                    pull.onRefreshComplete();
//                }
//            }, 800);
//
//            list.clear();
//        }
//        else
//        {
//            mHandler.postDelayed(new Runnable()
//            {
//
//                @Override
//                public void run()
//                {
//                    pull.onLoadMoreComplete();
//                    // pull.setMore(true);
//                }
//            }, 1000);
//        }
//        list.addAll(result);
//        adapter.notifyDataSetChanged();
//
//    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setTitle(R.string.tx_user_manager);
        titleBar.setLeftButtonVisible(true);
        titleBar.setRightButtonVisible(false);
        titleBar.setRightButLayVisible(true, 0);
        titleBar.setRightButLayBackground(R.drawable.icon_add);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                //                finish();
                try {
                    StatService.trackCustomEvent(CashierManager.this, "kMTASPayMeCashierAdd", "添加收银员按钮");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
                showPage(CashierAddActivity.class);
            }
            
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
        });
    }
    
    private class CashierAdapter extends BaseAdapter
    {
        
        private List<UserModel> userModels;
        
        public CashierAdapter()
        {
        }
        
        public CashierAdapter(List<UserModel> userModels)
        {
            this.userModels = userModels;
        }
        
        @Override
        public int getCount()
        {
            return userModels.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            return userModels.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(CashierManager.this, R.layout.cashier_list, null);
                holder = new ViewHolder();
//                holder.cashier_state = (TextView)convertView.findViewById(R.id.cashier_state);
                holder.cashier_store = (TextView)convertView.findViewById(R.id.cashier_store);
                holder.user_account = (TextView)convertView.findViewById(R.id.user_account);
                holder.userId = (TextView)convertView.findViewById(R.id.userId);
                holder.userName = (TextView)convertView.findViewById(R.id.userName);
                holder.usertel = (TextView)convertView.findViewById(R.id.usertel);
                holder.v_line = convertView.findViewById(R.id.v_line);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            
            UserModel userModel = userModels.get(position);
            
            if (position == (userModels.size() - 1))
            {
                holder.v_line.setVisibility(View.GONE);
            }
            else
            {
                holder.v_line.setVisibility(View.VISIBLE);
            }
            
            if (userModel.getDeptname() != null)
            {
                holder.cashier_store.setText(userModel.getDeptname());
            }
            
            //            if (userModel.getEnabled() != null && userModel.getEnabled().equals("true"))
            //            {
            //                holder.cashier_state.setText("(" + getString(R.string.open) + ")");
            //            }
            //            else
            //            {
            //                holder.cashier_state.setText("(" + getString(R.string.tx_frozen) + ")");
            //            }
            holder.userId.setText(getResources().getString(R.string.cash_number)+": "+String.valueOf(userModel.getId()));
            if (userModel.getPhone() != null)
            {
                holder.usertel.setText(userModel.getPhone());
            }
            
            if (userModel.getRealname() != null)
            {
                holder.userName.setText(getResources().getString(R.string.cashier)+": "+userModel.getRealname());
            }
            if(userModel.getUsername() != null){
                holder.user_account.setText(getResources().getString(R.string.cashier_account)+": "+userModel.getUsername());
            }
            
            return convertView;
        }
    }
    
    private class ViewHolder
    {
        private TextView userId, userName, usertel, cashier_store ,user_account;
//        private TextView cashier_state;
        private View v_line;
        
    }
    
    @Override
    public void onLoad()
    {
        pageFulfil = pageFulfil + 1;
        Logger.i("hehui", "pageFulfil-pageCount->" + pageFulfil + ",pageCount-->" + pageCount);
        if (pageFulfil <= pageCount)
        {
            loadData(pageFulfil, true, SwipeMenuListView.LOAD);
        }
    }
    
    @Override
    public void onRefresh()
    {
        pageFulfil = 0;
        loadData(pageFulfil, false, SwipeMenuListView.REFRESH);
    }
    
    //    @Override
    //    public void onRefresh()
    //    {
    //        pageFulfil = 0;
    //        loadData(pageFulfil, false);
    //    }
    //    
    //    @Override
    //    public void onLoadMore()
    //    {
    //        pageFulfil = pageFulfil + 1;
    //        Logger.i("hehui", "pageFulfil-pageCount->" + pageFulfil + ",pageCount-->" + pageCount);
    //        if (pageFulfil <= pageCount)
    //        {
    //            loadData(pageFulfil, true);
    //        }
    //    }
    
}
