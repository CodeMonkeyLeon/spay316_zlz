package cn.swiftpass.enterprise.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.SettlementAdapterBean;
import cn.swiftpass.enterprise.bussiness.model.SettlementBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.adapter.SettlementRecycleviewAdapter;

/**
 * Created by aijingya on 2019/5/20.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(日结详情的页面)
 * @date 2019/5/20 17:06.
 */

public class SettlementDetailsActivity extends TemplateActivity {

    private RecyclerView RecyclerView_settlement_detail;
    private SettlementBean settlementBean;
    private List<SettlementAdapterBean> settlementAdapterBeanList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settlement_detail);
        settlementBean = (SettlementBean) getIntent().getSerializableExtra("SettlementBean");
        initView();
        initData();
    }

    private void initView() {
        RecyclerView_settlement_detail = findViewById(R.id.RecyclerView_settlement_detail);
    }

    private void initData(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        RecyclerView_settlement_detail.setLayoutManager(layoutManager);

        //添加头部数据
        SettlementAdapterBean bean = new SettlementAdapterBean();
        bean.setSettlementDate(settlementBean.getSettlementDate());
        bean.setSettlementAmount(settlementBean.getSettlementAmount());
        bean.setOrganizationBeginTime(settlementBean.getOrganizationBeginTime());
        bean.setOrganizationEndTime(settlementBean.getOrganizationEndTime());
        settlementAdapterBeanList.add(bean);

        //添加下面的列表数据
        SettlementAdapterBean beanTransactionAmount = new SettlementAdapterBean();
        beanTransactionAmount.setTitle(getStringById(R.string.settlent_details_transaction_amount));
        beanTransactionAmount.setAmount(settlementBean.getTransactionAmount());
        settlementAdapterBeanList.add(beanTransactionAmount);

        SettlementAdapterBean beanRefundAmount = new SettlementAdapterBean();
        beanRefundAmount.setTitle(getStringById(R.string.tx_bill_stream_refund_money));
        beanRefundAmount.setAmount(settlementBean.getRefundAmount());
        settlementAdapterBeanList.add(beanRefundAmount);

        SettlementAdapterBean MDRAmount = new SettlementAdapterBean();
        MDRAmount.setTitle(getStringById(R.string.report_MDR_amount));
        MDRAmount.setAmount(settlementBean.getMerchantCharge());
        settlementAdapterBeanList.add(MDRAmount);

        //Withholding Tax 预扣税费
        if(MainApplication.isTaxRateOpen()){
            SettlementAdapterBean PreChargeAmount = new SettlementAdapterBean();
            PreChargeAmount.setTitle(getStringById(R.string.tx_withholding));
            PreChargeAmount.setAmount(settlementBean.getPreChargeTax());
            settlementAdapterBeanList.add(PreChargeAmount);
        }

        //surcharge 附加费 开关打开
        if(MainApplication.isSurchargeOpen()){
            SettlementAdapterBean SurchargeAmount = new SettlementAdapterBean();
            SurchargeAmount.setTitle(getStringById(R.string.tx_surcharge));
            SurchargeAmount.setAmount(settlementBean.getAdditionalTax());
            settlementAdapterBeanList.add(SurchargeAmount);
        }

        //Tip 小费 开关打开
        if(MainApplication.isTipOpenFlag()){
            SettlementAdapterBean TipAmount = new SettlementAdapterBean();
            TipAmount.setTitle(getStringById(R.string.tx_tips));
            TipAmount.setAmount(settlementBean.getTip());
            settlementAdapterBeanList.add(TipAmount);
        }

        //RecyclerView设置Adapter
        RecyclerView_settlement_detail.setAdapter(new SettlementRecycleviewAdapter(this, settlementAdapterBeanList));
    }


    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(getStringById(R.string.report_settlement));
        titleBar.setLeftButtonVisible(true);
    }
}
