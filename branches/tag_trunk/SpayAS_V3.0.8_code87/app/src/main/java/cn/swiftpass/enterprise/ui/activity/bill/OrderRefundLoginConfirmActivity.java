/*
 * 文 件 名:  ReportActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-8
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.bill;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.ziyeyouhu.library.KeyboardTouchListener;
import com.ziyeyouhu.library.KeyboardUtil;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.bill.BillOrderManager;
import cn.swiftpass.enterprise.bussiness.logica.refund.RefundManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.PreAutFinishActivity;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.CustomDialog;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.ECDHUtils;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 退款登录确认验证
 *
 * @author he_hui
 * @version [版本号, 2016-11-8]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class OrderRefundLoginConfirmActivity extends TemplateActivity {
    private static final String TAG = OrderRefundLoginConfirmActivity.class.getCanonicalName();
    private Order orderModel;

    private EditText et_id;

    private Button btn_next_step;
    private int activityType;
    private int minus=1;


    public static void startActivity(Context context, Order orderModel) {
        Intent it = new Intent();
        it.setClass(context, OrderRefundLoginConfirmActivity.class);
        it.putExtra("order", orderModel);
        context.startActivity(it);
    }

    /**
     * 授权解冻跳转 参数2
     * 转支付跳转 参数3  不需要密码验证，不需要该界面
     * @param context
     * @param orderModel
     */
    public static void startActivity(Context context, Order orderModel,int activityType) {
        Intent it = new Intent();
        it.setClass(context, OrderRefundLoginConfirmActivity.class);
        it.putExtra("order", orderModel);
        it.putExtra("activityType", activityType);
        context.startActivity(it);
    }

    private LinearLayout rootView;
    private KeyboardUtil keyboardUtil;
    private ScrollView scrollView;

    //安全键盘
    private void initMoveKeyBoard() {
        rootView = (LinearLayout) findViewById(R.id.rootview);
        scrollView = (ScrollView) findViewById(R.id.scrollview);
        keyboardUtil = new KeyboardUtil(this, rootView, scrollView);

        // monitor the KeyBarod state
        keyboardUtil.setKeyBoardStateChangeListener(new OrderRefundLoginConfirmActivity.KeyBoardStateListener());
        // monitor the finish or next Key
        keyboardUtil.setInputOverListener(new OrderRefundLoginConfirmActivity.inputOverListener());
        et_id.setOnTouchListener(new KeyboardTouchListener(keyboardUtil, KeyboardUtil.INPUTTYPE_ABC, -1));
    }

    class KeyBoardStateListener implements KeyboardUtil.KeyBoardStateChangeListener {

        @Override
        public void KeyBoardStateChange(int state, EditText editText) {

            if (state == 1) {

                et_id.setFocusable(true);
                et_id.setFocusableInTouchMode(true);

                et_id.requestFocus();

                et_id.findFocus();
            }

        }
    }

    class inputOverListener implements KeyboardUtil.InputFinishListener {

        @Override
        public void inputHasOver(int onclickType, EditText editText) {

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (keyboardUtil.isShow) {
                keyboardUtil.hideSystemKeyBoard();
                keyboardUtil.hideAllKeyBoard();
                keyboardUtil.hideKeyboardLayout();
                finish();
            } else {
                return super.onKeyDown(keyCode, event);
            }

            return false;
        } else{
            return super.onKeyDown(keyCode, event);
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_refund_login);
        orderModel = (Order) getIntent().getSerializableExtra("order");
        activityType=getIntent().getIntExtra("activityType",0);
        if (activityType==0){
            titleBar.setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
        }
        initview();
        setLister();
        btn_next_step.getBackground().setAlpha(102);
        MainApplication.listActivities.add(this);
        initMoveKeyBoard();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                keyboardUtil.showKeyBoardLayout(et_id, KeyboardUtil.INPUTTYPE_ABC,-1);
            }
        }, 200);

        //根据当前的小数点的位数来判断应该除以多少
        for(int i = 0 ; i < MainApplication.numFixed ; i++ ){
            minus = minus * 10;
        }

        if (activityType==2){
            btn_next_step.setBackgroundResource(R.drawable.btn_pre_finish);
        }

    }



    @Override
    protected void onPause() {
        super.onPause();
        //onPause的时候把密码清空
        et_id.setText("");
    }

    private void setLister() {
        btn_next_step.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String pass = et_id.getText().toString();
                if (StringUtil.isEmptyOrNull(pass)) {
                    toastDialog(OrderRefundLoginConfirmActivity.this, R.string.pay_refund_pwd, null);
                    et_id.setFocusable(true);
                    return;
                }
                checkUser();
            }
        });
    }

    private void ECDHKeyExchange(){
        try {
            ECDHUtils.getInstance().getAppPubKey();
        }catch (Exception e){
            Log.e(TAG,Log.getStackTraceString(e));
        }
        final String publicKey =  SharedPreUtile.readProduct("pubKey").toString();
        final String privateKey =  SharedPreUtile.readProduct("priKey").toString();

        LocalAccountManager.getInstance().ECDHKeyExchange(publicKey,new UINotifyListener<ECDHInfo>(){
            @Override
            public void onError(Object object) {
                super.onError(object);
                dismissLoading();
                /*if(object != null){
                    toastDialog(OrderRefundLoginConfirmActivity.this, object.toString(), null);
                }*/
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(ECDHInfo result) {
                super.onSucceed(result);
                if(result != null){
                    try {
                        String secretKey = ECDHUtils.getInstance().ecdhGetShareKey(MainApplication.serPubKey,privateKey);
                        SharedPreUtile.saveObject(secretKey, "secretKey");
                        //再去请求一次登录接口
                        checkUser();
                    }catch (Exception e){
                        Log.e(TAG,Log.getStackTraceString(e));
                    }
                }
            }
        });
    }

    /**
     * 验证用户
     */
    private void checkUser() {
        LocalAccountManager.getInstance().refundLogin(null, MainApplication.getMchId(), MainApplication.userName, et_id.getText().toString(), false, new UINotifyListener<Boolean>() {
            @Override
            public void onError(final Object object) {
                super.onError(object);
                dismissMyLoading();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(object != null){
                            if (object.toString().startsWith("405")){//Require to renegotiate ECDH key
                                ECDHKeyExchange();
                            }else {
                                toastDialog(OrderRefundLoginConfirmActivity.this, object.toString(), null);
                            }
                        }
                    }
                });
            }

            @Override
            public void onSucceed(Boolean result) {
                super.onSucceed(result);
                dismissMyLoading();
                if (result) {
                    if (orderModel != null) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                switch (activityType){
                                    case 0://0 默认消费
                                        regisRefund();//消费
                                        break;
                                    case 2:
                                        authUnfreeze();//预授权解冻 spay/authUnfreeze
                                        break;
                                        default:
                                            break;
                                }
                            }
                        });

                    }
                }
            }

            @Override
            public void onPreExecute() {
                super.onPreExecute();
                loadDialog(OrderRefundLoginConfirmActivity.this, R.string.tx_bill_stream_refund_login_loading);
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
                dismissMyLoading();
            }
        });
    }

    private void regisRefund() {
        String transaction = null;
        RefundManager.getInstant().regisRefunds(orderModel.getOutTradeNo(), transaction,
                orderModel.getMoney(), orderModel.getRefundFeel(), new UINotifyListener<Order>() {
            @Override
            public void onError(final Object object) {
                dismissLoading();
                super.onError(object);
                if (object != null) {
                    toastDialog(OrderRefundLoginConfirmActivity.this, object.toString(), getString(R.string.btnOk), new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            for (Activity a : MainApplication.listActivities) {
                                a.finish();
                            }
                            OrderRefundLoginConfirmActivity.this.finish();
                        }
                    });
                }
            }

            @Override
            public void onPreExecute() {
                super.onPreExecute();

                loadDialog(OrderRefundLoginConfirmActivity.this, R.string.refunding_wait);
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
                dismissLoading();
            }

            @Override
            public void onSucceed(Order result) {
                dismissLoading();
                super.onSucceed(result);
                if (result != null) {
                    orderModel.setRefundNo(result.getRefundNo());
                    orderModel.setAddTimeNew(result.getAddTimeNew());
                    orderModel.setMoney(orderModel.getRefundFeel());

                    orderModel.setCashFeel(result.getCashFeel());
                    orderModel.setSurcharge(result.getSurcharge());
                    orderModel.setRefundFee(result.getRefundFee());
                    orderModel.setVat(result.getVat());
                    orderModel.setOrderFee(result.getOrderFee());
                    OrderRefundCompleteActivity.startActivity(OrderRefundLoginConfirmActivity.this, orderModel);

                }
            }
        });
    }

    /**
     * cxy
     * 预授权解冻  需要密码
     */
    private void authUnfreeze(){
        if (orderModel!=null){
        loadDialog(OrderRefundLoginConfirmActivity.this,getString(R.string.loading));
        BillOrderManager.getInstance().authUnfreeze(orderModel.getAuthNo(),orderModel.getMoney()+"",
                "",new UINotifyListener<Order>(){
                    @Override
                    public void onError(Object object) {
                        super.onError(object);
                        dismissLoading();
                        OrderRefundLoginConfirmActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showErrorDialog();
                            }
                        });
                    }

                    @Override
                    public void onSucceed(Order result) {
                        super.onSucceed(result);
                        dismissLoading();
                        if (result!=null){//最后跳转到完成界面

                            if (MainApplication.isAdmin.equals("1")) {
                                //此时要在打印小票的时候更新申请人
                                if (orderModel != null) {
                                    orderModel.setUserName(MainApplication.mchName);
                                }
                            } else {
                                //此时要在打印小票的时候更新申请人
                                if (orderModel != null) {
                                    orderModel.setUserName(MainApplication.realName);
                                }
                            }
                            orderModel.setUnFreezeTime(result.getUnFreezeTime());
                            orderModel.setOutRequestNo(result.getOutRequestNo());
                            orderModel.setAuthNo(result.getAuthNo());
                            orderModel.setRequestNo(result.getRequestNo());
                            orderModel.setTotalFee(result.getTotalFee());
                            orderModel.setUnFreezeTime(result.getUnFreezeTime());

                            PreAutFinishActivity.startActivity(OrderRefundLoginConfirmActivity.this,orderModel,2);//传2
                            for (Activity a : MainApplication.listActivities) {
                                a.finish();
                            }
                            finish();
                        }
                    }
                });
        }

    }

    private CustomDialog  errorDialog;
    private void showErrorDialog(){
        errorDialog= new CustomDialog.Builder(OrderRefundLoginConfirmActivity.this)
                .gravity(Gravity.CENTER).widthdp(250)
                .cancelTouchout(true)
                .view(R.layout.dialog_error)
                .addViewOnclick(R.id.tv_confirm, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        errorDialog.cancel();

                   /*     Intent it = new Intent();
                        it.putExtra("tab_index","bill");
                        it.setClass(OrderRefundLoginConfirmActivity.this, spayMainTabActivity.class);
                        startActivity(it);*/

                        for (Activity a : MainApplication.listActivities) {
                            a.finish();
                        }
                        finish();
                    }
                })
                .build();
        errorDialog.show();
        TextView tv_title=(TextView)errorDialog.findViewById(R.id.tv_title);
        tv_title.setText(getString(R.string.Pre_auth_unfreezen_failed));
    }

    private void initview() {
        et_id = getViewById(R.id.et_id);
        btn_next_step = getViewById(R.id.btn_next_step);

        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (et_id.isFocused()) {
                    if (et_id.getText().toString().length() > 0) {
                        //                        btn_next_step.setEnabled(true);
                        //                        btn_next_step.setBackgroundResource(R.drawable.btn_register);

                        setButtonBg(btn_next_step, true, R.string.submit);
                    } else {
                        //                        btn_next_step.setEnabled(false);
                        //                        btn_next_step.setBackgroundResource(R.drawable.general_button_grey_default);

                        setButtonBg(btn_next_step, false, R.string.submit);
                    }
                }
            }
        });
        et_id.addTextChangedListener(editTextWatcher);
    }

    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b,enable,res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            if (activityType==2){
                b.setBackgroundResource(R.drawable.btn_nor_pre_shape);
            }else {
                b.setBackgroundResource(R.drawable.btn_nor_shape);
            }
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            if (activityType==2){
                b.setBackgroundResource(R.drawable.btn_press_pre_shape);
            }else {
                b.setBackgroundResource(R.drawable.btn_press_shape);
            }
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(R.string.tx_bill_stream_login_cirfom);
        titleBar.setTitleChoice(false);
        titleBar.setLeftButtonVisible(true);
        titleBar.setLeftButtonIsVisible(true);
        String saleOrPreauth = PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth","sale");
        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(saleOrPreauth,"pre_auth")){
            titleBar.setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
        }else{
            titleBar.setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
        }
    }

}
