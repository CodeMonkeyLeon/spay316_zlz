package cn.swiftpass.enterprise.mvp.presenter.fragment

import cn.swiftpass.enterprise.bussiness.model.TransactionReportList
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.fragment.TransactionReportContract
import cn.swiftpass.enterprise.utils.JsonUtil
import okhttp3.Call


/**
 * @author lizheng.zhao
 * @date 2022/11/16
 *
 * @你应该是一场梦我应该是一阵风
 */
class TransactionReportPresenter : TransactionReportContract.Presenter {


    private var mView: TransactionReportContract.View? = null


    override fun queryTransactionReport(reportType: String?) {

        mView?.let {
            it.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
        }

        AppClient.queryTransactionReport(
            reportType,
            System.currentTimeMillis().toString(),
            object :
                CallBackUtil.CallBackCommonResponse<TransactionReportList>(TransactionReportList()) {


                override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                    mView?.let { view ->
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { error ->
                            view.queryTransactionReportFailed(error.message)
                        }
                    }
                }


                override fun onResponse(res: CommonResponse?) {
                    mView?.let { view ->
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        res?.let { response ->
                            val bean = JsonUtil
                                .jsonToBean(
                                    response.message,
                                    TransactionReportList::class.java
                                ) as TransactionReportList
                            view.queryTransactionReportSuccess(bean.data)
                        }
                    }
                }
            }
        )

    }

    override fun attachView(view: TransactionReportContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}