/*
 * 文 件 名:  CashierFragment.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  yan_shuo
 * 修改时间:  2015-4-27
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ListView;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.adapter.CashierDetailsAdapter;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.PreferenceUtil;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author Administrator
 * @version [版本号, 2015-4-27]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@SuppressLint("SetJavaScriptEnabled")
public class CashierFragment extends BaseFragment {
    private ListView cashier_listview;

    private CashierDetailsAdapter adapter;

    private List<String> list;

    private WebView wb;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = View.inflate(getActivity(), R.layout.activity_contenttext, null);
        v.setLayoutParams(new LayoutParams(-1, -1));
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //        initObject();
    }

    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initObject() {
        list = new ArrayList<String>();
        for (int a = 0; a < 5; a++) {
            list.add("");
        }
        //        adapter = new CashierDetailsAdapter(getActivity(), list);
    }

    final class InJavaScriptLocalObj {
        public void showSource(String html) {
            Log.d("HTML", html);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews(getView());
        //        initView(getView());
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @SuppressLint("JavascriptInterface")
    private void initViews(View v) {
        wb = (WebView) v.findViewById(R.id.webview);
        //        wb.addJavascriptInterface(new InJavaScriptLocalObj(), "local_obj");
        wb.getSettings().setJavaScriptEnabled(true);
        wb.getSettings().setAllowFileAccessFromFileURLs(false);
        wb.getSettings().setAllowUniversalAccessFromFileURLs(false);

        WebSettings settings = wb.getSettings();
        if (AppHelper.getAndroidSDKVersion() == 17) {
            settings.setDisplayZoomControls(false);
        }
        settings.setLoadWithOverviewMode(true);
        //settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        //各种分辨率适应
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int mDensity = metrics.densityDpi;
        if (mDensity == 120) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        } else if (mDensity == 160) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        } else if (mDensity == 240) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        }
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);

        wb.setWebChromeClient(new myWebChromeClien());
        wb.setWebViewClient(new MyWebViewClient());
        //        int title = getIntent().getIntExtra("title", R.string.title_context_text);
        //        titleBar.setTitle(title);
        //        String url = getIntent().getStringExtra("url");
        JSONObject params = new JSONObject();
        try {
            params.put("mchId", String.valueOf(MainApplication.merchantId));
            Map<String, String> heards = new HashMap<String, String>();

            String language = PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN);

            Locale locale = getResources().getConfiguration().locale; //zh-rHK
            String lan = locale.getCountry();

            if (!TextUtils.isEmpty(language)) {
                heards.put("fp-lang", PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN));
            } else {
                if (lan.equalsIgnoreCase("CN")) {
                    heards.put("fp-lang", MainApplication.LANG_CODE_ZH_CN);
                } else {
                    heards.put("fp-lang", MainApplication.LANG_CODE_ZH_TW);
                }

            }
            if (ApiConstant.bankCode.equals("citic_and") || ApiConstant.bankCode.equals("czb_and") || ApiConstant.bankCode.equals("czcb_and") || ApiConstant.bankCode.equals("zsy_and")) {
                wb.loadUrl(ApiConstant.BASE_URL_PORT + "spay/getTotalByTerminal?&tradeType=2&mchId=" + MainApplication.merchantId + "&bankCode=CCB", heards);
            } else if (ApiConstant.bankCode.equals("spdb_and")) {
                wb.loadUrl(ApiConstant.BASE_URL_PORT + "spay/getTotalByTerminal?&tradeType=2&mchId=" + MainApplication.merchantId + "&bankCode=spdb", heards);
            } else if (ApiConstant.bankCode.equalsIgnoreCase("fjnx_and")) {
                //福建银行
                wb.loadUrl(ApiConstant.BASE_URL_PORT + "spay/getTotalByTerminal?tradeType=2&mchId=" + MainApplication.merchantId + "&bankCode=rcc", heards);
            } else if (ApiConstant.bankCode.equals("ceb_and")) {
                wb.loadUrl(ApiConstant.BASE_URL_PORT + "spay/getTotalByTerminal?&tradeType=2&mchId=" + MainApplication.merchantId + "&bankCode=cebbank", heards);
            } else {
                wb.loadUrl(ApiConstant.BASE_URL_PORT + "spay/getTotalByTerminal?&tradeType=2&mchId=" + MainApplication.merchantId, heards);
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    class myWebChromeClien extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);

        }
    }

    final class MyWebViewClient extends WebViewClient {
        public MyWebViewClient() {
            super();
        }

        /**
         * {@inheritDoc}
         */

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            showLoading(true, "加载中...");
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.i("hehui", "shouldOverrideUrlLoading");
            showLoading(true, "加载中...");
            return super.shouldOverrideUrlLoading(view, url);

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            //            dialog.dismiss();
            dismissLoading();
            super.onPageFinished(view, url);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            dismissLoading();
            wb.loadUrl("file:///android_asset/error.html");

        }
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }
}
