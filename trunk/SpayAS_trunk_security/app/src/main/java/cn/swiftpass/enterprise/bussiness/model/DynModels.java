package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 同步各种定制版本
 */
public class DynModels implements Serializable {
   public ArrayList<DynModel> data;
   public String md5;
}
