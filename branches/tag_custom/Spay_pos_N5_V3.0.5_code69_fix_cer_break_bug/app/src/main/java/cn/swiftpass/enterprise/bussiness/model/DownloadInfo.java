package cn.swiftpass.enterprise.bussiness.model;


import cn.swiftpass.enterprise.bussiness.enums.DownloadStatus;
import cn.swiftpass.enterprise.io.database.table.DownloadTable;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = DownloadTable.TABLE_DOWNLOAD)
public class DownloadInfo implements Serializable
{

	/**
	  * @Fields serialVersionUID : TODO（用一句话描述这个变量表示什么）
	  */
	private static final long serialVersionUID = 11111L;

	@DatabaseField(columnName = DownloadTable.COLUMN_ID,generatedId=true)
	public int id;
	//apkid
	@DatabaseField(columnName = DownloadTable.COLUMN_DOWNLOAD_ID) 
	public long downloadId;
	//下载名字
	@DatabaseField(columnName = DownloadTable.COLUMN_DOWN_NAME)
	public String mDownName = null;
	//url
	@DatabaseField(columnName = DownloadTable.COLUMN_DOWN_URL)
	public String mDownUrl = null;
	//保存路径
	@DatabaseField(columnName = DownloadTable.COLUMN_DOWN_SAVE_PATH)
	public String mDownSavePath = null;
	//下载状态
	@DatabaseField(columnName = DownloadTable.COLUMN_DOWN_STATUS,dataType=DataType.ENUM_INTEGER)
	public DownloadStatus mDownStatus ;
	//总字节数
	@DatabaseField(columnName = DownloadTable.COLUMN_DOWN_TOTAL_BYTES)
	public long mDownTotalBytes = -1;
	//当前下载的字节数
	@DatabaseField(columnName = DownloadTable.COLUMN_DOWN_CURRENT_BYTES)
	public long mDownCurrentBytes = 0;
	//扩展1
	@DatabaseField(columnName = DownloadTable.COLUMN_DOWN_EXTEND1)
	public String mDownExtend1 = null;
	//扩展2
	@DatabaseField(columnName = DownloadTable.COLUMN_DOWN_EXTEND2)
	public String mDownExtend2 = null;
	//扩展3
	@DatabaseField(columnName = DownloadTable.COLUMN_DOWN_EXTEND3)
	public int mDownExtend3 = 0;
	//扩展4
	@DatabaseField(columnName = DownloadTable.COLUMN_DOWN_EXTEND4)
	public int mDownExtend4 = 0;
	
	
	// ===========================================================
	// Constructors
	// ===========================================================
	public DownloadInfo(){
		
	}
	
	public DownloadInfo(String mDownName, String mDownUrl,
                        String mDownSavePath, DownloadStatus mDownStatus, int mDownTotalBytes,
                        int mDownCurrentBytes) {
		super();
		this.mDownName = mDownName;
		this.mDownUrl = mDownUrl;
		this.mDownSavePath = mDownSavePath;
		this.mDownStatus = mDownStatus;
		this.mDownTotalBytes = mDownTotalBytes;
		this.mDownCurrentBytes = mDownCurrentBytes;
	}

	
}
