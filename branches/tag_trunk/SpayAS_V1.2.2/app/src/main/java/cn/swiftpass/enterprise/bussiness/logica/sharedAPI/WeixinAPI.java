package cn.swiftpass.enterprise.bussiness.logica.sharedAPI;

import java.io.ByteArrayOutputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-3-5
 * Time: 下午8:15
 * To change this template use File | Settings | File Templates.
 */
public class WeixinAPI
{
    
    /**************** 微信API ************/
    private static final int TIMELINE_SUPPORTED_VERSION = 0x21020001;
    
    //    private IWXAPI wxApi;
    
    private WeixinAPI()
    {
    }
    
    public static WeixinAPI instance;
    
    public static WeixinAPI getInstance()
    {
        if (null == instance)
        {
            instance = new WeixinAPI();
        }
        return instance;
    }
    
    public void regToWX(Context context)
    {
        //        wxApi = WXAPIFactory.createWXAPI(context, GlobalConstant.WX_APP_ID, true);
        //        wxApi.registerApp(GlobalConstant.WX_APP_ID);
    }
    
    //    
    //    private SendMessageToWX.Req send(Context context)
    //    {
    //        WXTextObject textobj = new WXTextObject();
    //        String s = context.getResources().getString(R.string.msg_shared);
    //        //textobj.text = s.replace( "%",LocalAccountManager.getInstance().jiami(LocalAccountManager.getInstance().getUID()));
    //        textobj.text = s;
    //        WXMediaMessage msg = new WXMediaMessage();
    //        msg.description = context.getResources().getString(R.string.msg_shared);
    //        msg.mediaObject = textobj;
    //        
    //        SendMessageToWX.Req req = new SendMessageToWX.Req();
    //        req.transaction = String.valueOf(System.currentTimeMillis());
    //        req.message = msg;
    //        return req;
    //    }
    //    
    //    /** 推荐给朋友 */
    //    public void sendFirendShareinfo(Context context)
    //    {
    //        SendMessageToWX.Req req = send(context);
    //        // int wxSdkVersion = wxApi.getWXAppSupportAPI();
    //        req.scene = SendMessageToWX.Req.WXSceneSession;
    //        wxApi.sendReq(req);
    //    }
    //    
    //    /** 推荐给朋友 */
    //    public void sendFriendCircle(Context context)
    //    {
    //        SendMessageToWX.Req req = send(context);
    //        int wxSdkVersion = wxApi.getWXAppSupportAPI();
    //        if (wxSdkVersion >= TIMELINE_SUPPORTED_VERSION)
    //        {
    //            req.scene = SendMessageToWX.Req.WXSceneTimeline;
    //        }
    //        else
    //        {
    //            req.scene = SendMessageToWX.Req.WXSceneSession;
    //        }
    //        wxApi.sendReq(req);
    //    }
    
    /**发送带文本的分享*/
    public void sendReq(Context context, String text, Bitmap bmp, boolean isSendFriednCircle)
    {
        String url;
        try
        {
            url = "http://www.";// 收到分享的好友点击信息会跳转到这个地址去
        }
        catch (Exception e)
        {
            e.printStackTrace();
            url = "http://www.swiftpass.cn";
        }
        regToWX(context);
        //        int wxSdkVersion = wxApi.getWXAppSupportAPI();
        //        WXWebpageObject localWXWebpageObject = new WXWebpageObject();
        //        localWXWebpageObject.webpageUrl = url;
        //        WXMediaMessage localWXMediaMessage = new WXMediaMessage(localWXWebpageObject);
        //        localWXMediaMessage.title = "";// 不能太长，否则微信会提示出错。不过博主没验证过具体能输入多长。
        //        localWXMediaMessage.description = text;
        //        localWXMediaMessage.thumbData = getBitmapBytes(bmp, false);
        //        SendMessageToWX.Req localReq = new SendMessageToWX.Req();
        //        if (isSendFriednCircle && wxSdkVersion >= TIMELINE_SUPPORTED_VERSION)
        //        {
        //            //朋友圈
        //            localReq.transaction = System.currentTimeMillis() + "";
        //        }
        //        else
        //        {
        //            localReq.transaction = SendMessageToWX.Req.WXSceneSession + "";
        //        }
        //        localReq.message = localWXMediaMessage;
        //        //IWXAPI api = WXAPIFactory.createWXAPI(context, GlobalConstant.WX_APP_ID, true);
        //        wxApi.sendReq(localReq);
        
    }
    
    /** 发送之前要处理图片 */
    private static byte[] getBitmapBytes(Bitmap bitmap, boolean paramBoolean)
    {
        Bitmap localBitmap = Bitmap.createBitmap(80, 80, Bitmap.Config.RGB_565);
        Canvas localCanvas = new Canvas(localBitmap);
        int i;
        int j;
        if (bitmap.getHeight() > bitmap.getWidth())
        {
            i = bitmap.getWidth();
            j = bitmap.getWidth();
            
        }
        else
        {
            i = bitmap.getHeight();
            j = bitmap.getHeight();
            
        }
        while (true)
        {
            localCanvas.drawBitmap(bitmap, new Rect(0, 0, i, j), new Rect(0, 0, 80, 80), null);
            if (paramBoolean)
                bitmap.recycle();
            ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
            localBitmap.compress(Bitmap.CompressFormat.JPEG, 100, localByteArrayOutputStream);
            localBitmap.recycle();
            byte[] arrayOfByte = localByteArrayOutputStream.toByteArray();
            try
            {
                localByteArrayOutputStream.close();
                return arrayOfByte;
                
            }
            catch (Exception e)
            {
                e.printStackTrace();
                
            }
            i = bitmap.getHeight();
            j = bitmap.getHeight();
            
        }
        
    }
    
}
