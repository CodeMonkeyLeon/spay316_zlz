/*
 * 文 件 名:  AgentInfo.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2014-8-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.model;

/**
 * 代理商
 * 
 * @author  he_hui
 * @version  [版本号, 2014-8-11]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class AgentInfo implements java.io.Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    private String agentPhone; // 代理商电话
    
    private String city; // 所在城市
    
    /**
     * @return 返回 agentPhone
     */
    public String getAgentPhone()
    {
        return agentPhone;
    }
    
    /**
     * @param 对agentPhone进行赋值
     */
    public void setAgentPhone(String agentPhone)
    {
        this.agentPhone = agentPhone;
    }
    
    /**
     * @return 返回 city
     */
    public String getCity()
    {
        return city;
    }
    
    /**
     * @param 对city进行赋值
     */
    public void setCity(String city)
    {
        this.city = city;
    }
}
