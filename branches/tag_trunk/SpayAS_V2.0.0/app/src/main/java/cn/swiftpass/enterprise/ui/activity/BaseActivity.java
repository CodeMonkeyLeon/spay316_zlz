package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.tencent.stat.MtaSDkException;
import com.tencent.stat.StatConfig;
import com.tencent.stat.StatReportStrategy;
import com.tencent.stat.StatService;
import com.tencent.stat.common.StatLogger;

import java.io.File;
import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.print.BluePrintUtil;
import cn.swiftpass.enterprise.ui.widget.CommonConfirmDialog;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.MyToast;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 基类
 * 有一个抽象方法，是否需要登录
 * User: Alan
 * Date: 13-9-21
 * Time: 上午11:52
 * To change this template use File | Settings | File Templates.
 */
public class BaseActivity extends ActivityGroup {
    private static final String TAG = BaseActivity.class.getCanonicalName();

    private static StatLogger logger = new StatLogger("MTADemon");

    protected ProgressDialog loadingDialog = null;

    protected boolean isResumed = false;

    //LoadProgressDialog myloadDialog ;
//    protected ImageCache imageCache;

    MyToast myToast;

    //public static ActivityGroup group;

    private DialogInfo dialogInfo;

    private Dialog dialogNew = null;

    /**
     * 转成RMB
     *
     * @author admin
     */
    public void paseRMB(BigDecimal bigDecimal, TextView tv_pase) {
        try {

            if (!MainApplication.getFeeType().equalsIgnoreCase("CNY")) {
                tv_pase.setVisibility(View.VISIBLE);
                BigDecimal b2 = new BigDecimal(MainApplication.getExchangeRate());
                if (bigDecimal != null && b2 != null) {
                    BigDecimal paseBigDecimal = bigDecimal.multiply(b2).setScale(2, BigDecimal.ROUND_FLOOR);
                    tv_pase.setText(getString(R.string.tx_about) + getString(R.string.tx_mark) + DateUtil.formatPaseMoney(paseBigDecimal));
                }
            } else {
                tv_pase.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Logger.e("hehui", "parserToRmb-->" + e);
        }
    }

    /**
     * 清除缓存
     * deleteSharedPre:(这里用一句话描述这个方法的作用).
     *
     * @author admin
     */
    public void deleteSharedPre() {
        try {
            SharedPreUtile.removeKey("showMpayInMySettingMD5" + MainApplication.getMchId() + MainApplication.getUserId());
            PreferenceUtil.removeKey("payTypeMd5" + ApiConstant.bankCode + MainApplication.getMchId());
            SharedPreUtile.removeKey("funcGridInfo" + MainApplication.getMchId() + MainApplication.getUserId());
            PreferenceUtil.removeKey("payTypeMd5" + ApiConstant.bankCode + MainApplication.getMchId());
            NoteMarkActivity.setNoteMark("");
        } catch (Exception e) {
            Logger.e(TAG, "deleteSharedPre-->" + e);
        }
    }

    public void setButtonBg(Button b, boolean enable, int res) {
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_register);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.general_button_main_default);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }

    protected void switchLanguage(String language) {
        //设置应用语言类型
        Resources resources = getResources();
        Configuration config = resources.getConfiguration();
        DisplayMetrics dm = resources.getDisplayMetrics();


        if (!TextUtils.isEmpty(language) && language.equals(MainApplication.LANG_CODE_ZH_TW)
                || !TextUtils.isEmpty(language) && language.equals(MainApplication.LANG_CODE_ZH_HK_HANT) ) // 繁体
        {
            config.locale = Locale.TRADITIONAL_CHINESE;
        } else if (!TextUtils.isEmpty(language) && language.equals(MainApplication.LANG_CODE_EN_US)) {
            config.locale = Locale.ENGLISH;
        } else if (!TextUtils.isEmpty(language) && language.equals(MainApplication.LANG_CODE_ZH_CN)
                || !TextUtils.isEmpty(language) && language.equals(MainApplication.LANG_CODE_ZH_CN_HANS)) {
            config.locale = Locale.SIMPLIFIED_CHINESE;
        } else { // 跟随系统
            config.locale = Locale.getDefault(); //默认英文
        }
        Logger.i("hehui", "Locale.getDefault()-->" + Locale.getDefault());
        if (!StringUtil.isEmptyOrNull(language)) {
            PreferenceUtil.commitString("language", language);
        } else {
            Locale locale = getResources().getConfiguration().locale;
            String lan = locale.getLanguage() + "-" + locale.getCountry();
//            String lan = getResources().getConfiguration().locale.getCountry();//英文 返回为空  中文 CN TW
//            String lan = Locale.getDefault().toString();
            Logger.i("hehui", "lan-->" + lan);//  zh-HK  en-HK   zh-CN   en-
            if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_NEW)|| lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_HANS)) {
                //                PreferenceUtil.commitString("language", MainApplication.LANG_CODE_ZH_CN);
                config.locale = Locale.SIMPLIFIED_CHINESE;
            } else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_NEW) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO_NEW) ||
                    lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW_NEW) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_HANT)) {
                Logger.i("hehui", "LANG_CODE_ZH_HK-->");
                config.locale = Locale.TRADITIONAL_CHINESE;
            } else {
                config.locale = Locale.ENGLISH;
            }

//            if (lan.equalsIgnoreCase("CN")) {
//                config.locale = Locale.SIMPLIFIED_CHINESE;
//            } else if(lan.equalsIgnoreCase("TW")) {
//                config.locale = Locale.TRADITIONAL_CHINESE;
//            }else{
//                config.locale = Locale.ENGLISH;
//            }
        }
        resources.updateConfiguration(config, dm);

        //保存设置语言的类型
        //        if (StringUtil.isEmptyOrNull(language))
        //        {
        //            PreferenceUtil.commitString("language", MainApplication.LANG_CODE_EN_US);
        //        }
        //        else
        //        {
        //            PreferenceUtil.commitString("language", language);
        //        }

    }

    /**
     * 判断是否需要检测，防止不停的弹框
     */
    //private boolean isNeedCheck = true;

    protected static final int PERMISSON_REQUESTCODE = 0;

    /**
     * @since 2.5.0
     * requestPermissions方法是请求某一权限，
     */
    protected void checkPermissions(String... permissions) {
        List<String> needRequestPermissonList = findDeniedPermissions(permissions);
        if (null != needRequestPermissonList && needRequestPermissonList.size() > 0) {
            ActivityCompat.requestPermissions(this, needRequestPermissonList.toArray(new String[needRequestPermissonList.size()]), PERMISSON_REQUESTCODE);
        }
    }

    public boolean isGranted(String permission) {
        return !isMarshmallow() || isGranted_(permission);
    }

    private boolean isMarshmallow() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    private boolean isGranted_(String permission) {
        int checkSelfPermission = ActivityCompat.checkSelfPermission(this, permission);
        return checkSelfPermission == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * 检测是否所有的权限都已经授权
     *
     * @param grantResults
     * @return
     * @since 2.5.0
     */
    protected boolean verifyPermissions(int[] grantResults) {
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }




    /**
     * 启动应用的设置
     *
     * @since 2.5.0
     */
    protected void startAppSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:" + getPackageName()));
        startActivity(intent);
    }


    /**
     * 获取权限集中需要申请权限的列表
     *
     * @param permissions
     * @return
     * @since 2.5.0
     * checkSelfPermission方法是在用来判断是否app已经获取到某一个权限
     * shouldShowRequestPermissionRationale方法用来判断是否
     * 显示申请权限对话框，如果同意了或者不在询问则返回false
     */
    protected List<String> findDeniedPermissions(String[] permissions) {
        List<String> needRequestPermissonList = new ArrayList<String>();
        for (String perm : permissions) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                needRequestPermissonList.add(perm);
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, perm)) {
                    needRequestPermissonList.add(perm);
                }
            }
        }
        return needRequestPermissonList;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //初始化PreferenceUtil
        PreferenceUtil.init(this);
        //根据上次的语言设置，重新设置语言
        switchLanguage(PreferenceUtil.getString("language", ""));
        //group = this;
        MainApplication.getContext().setLastActivityRef(new WeakReference<Activity>((Activity) this));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //添加
        ToastHelper.getInstance(this);
        StatConfig.setAutoExceptionCaught(true);
        // 腾讯云 统计
        initMTAConfig(false);

        MainApplication.allActivities.add(this);
        // 腾讯云 启动mta
        try {
            // 第三个参数必须为：com.tencent.stat.common.StatConstants.VERSION
            StatService.startStatService(this, ApiConstant.appKey, com.tencent.stat.common.StatConstants.VERSION);
        } catch (MtaSDkException e) {
            // MTA初始化失败
            Log.e(TAG, Log.getStackTraceString(e));
        }


    }

    boolean isOpen = true, isClose = true, isOpening = true, isCloseing = true;


    /**
     * 连接默认上一次
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void connentBlue() {
        boolean connState = PreferenceUtil.getBoolean("connState" + ApiConstant.bankCode, true);
        if (connState && !StringUtil.isEmptyOrNull(MainApplication.getBlueDeviceAddress())) {
            //异步去连接蓝牙
            new Thread(new Runnable() {
                @Override
                public void run() {
                    boolean isConnect = BluePrintUtil.blueConnent(MainApplication.getBlueDeviceAddress());
                    Logger.i(TAG, " connentBlue isConnect-->" + isConnect);
                    if (!isConnect) {
                        MainApplication.setBlueState(false);
                        //                        MainApplication.bluetoothSocket = null;
                        //                        MainApplication.setBlueDeviceName("");
                        //                        MainApplication.setBlueDeviceNameAddress("");
                        //                        PreferenceUtil.commitBoolean("connState" + MainApplication.getMchId(), false);
                        HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET, HandlerManager.BLUE_CONNET);
                    } else {
                        MainApplication.setBlueState(true);
                    }
                }
            }).start();
        }
    }

    /**
     * 根据不同的模式，建议设置的开关状态，可根据实际情况调整，仅供参考。
     *
     * @param isDebugMode 根据调试或发布条件，配置对应的MTA配置
     */
    private void initMTAConfig(boolean isDebugMode) {
        logger.d("isDebugMode:" + isDebugMode);
        if (isDebugMode) { // 调试时建议设置的开关状态
            // 查看MTA日志及上报数据内容
            StatConfig.setDebugEnable(true);
            // 禁用MTA对app未处理异常的捕获，方便开发者调试时，及时获知详细错误信息。
            StatConfig.setAutoExceptionCaught(false);
            // StatConfig.setEnableSmartReporting(false);
            // Thread.setDefaultUncaughtExceptionHandler(new
            // UncaughtExceptionHandler() {
            //
            // @Override
            // public void uncaughtException(Thread thread, Throwable ex) {
            // logger.error("setDefaultUncaughtExceptionHandler");
            // }
            // });
            // 调试时，使用实时发送
            // StatConfig.setStatSendStrategy(StatReportStrategy.BATCH);
            // // 是否按顺序上报
            // StatConfig.setReportEventsByOrder(false);
            // // 缓存在内存的buffer日志数量,达到这个数量时会被写入db
            // StatConfig.setNumEventsCachedInMemory(30);
            // // 缓存在内存的buffer定期写入的周期
            // StatConfig.setFlushDBSpaceMS(10 * 1000);
            // // 如果用户退出后台，记得调用以下接口，将buffer写入db
            // StatService.flushDataToDB(getApplicationContext());

            // StatConfig.setEnableSmartReporting(false);
            // StatConfig.setSendPeriodMinutes(1);
            // StatConfig.setStatSendStrategy(StatReportStrategy.PERIOD);
        } else { // 发布时，建议设置的开关状态，请确保以下开关是否设置合理
            // 禁止MTA打印日志
            StatConfig.setDebugEnable(false);
            // 根据情况，决定是否开启MTA对app未处理异常的捕获
            StatConfig.setAutoExceptionCaught(true);
            StatConfig.setInstallChannel(ApiConstant.Channel);
            // 选择默认的上报策略
            StatConfig.setStatSendStrategy(StatReportStrategy.APP_LAUNCH);
        }
    }

    /**
     * EditText获取焦点并显示软键盘
     */
    public void showSoftInputFromWindow(Activity activity, EditText editText) {
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    public void redirectLoginActivity() {

        Intent intent = new Intent();
        intent.setClassName(this, "cn.swiftpass.enterprise.ui.activity.WelcomeActivity");
        startActivity(intent);

        finish();
        for (Activity a : MainApplication.allActivities) {
            a.finish();
        }
        //        MainApplication.getContext().exit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isResumed = true;
        StatService.onResume(this);
        //Logger.i("Log", this.getClass().getSimpleName()+" onResume");
    }

    @Override
    protected void onPause() {
        isResumed = false;
        dismissLoading();
        super.onPause();
        StatService.onPause(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Logger.i("Log", this.getClass().getSimpleName()+" onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    protected <T extends View> T getViewById(int id) {
        View view = findViewById(id);
        return (T) view;
    }

    protected <T extends View> T inflate(int res) {
        return (T) LayoutInflater.from(this).inflate(res, null);
    }

    public void showMyLoading(String msg) {
        showLoading(true, msg);
    }

    public void showLoading(boolean cancelble, int id) {
        showLoading(cancelble, getString(id));
    }

    public void showLoading(boolean cancelble, String str) {
        if (!isResumed) return;
        //        if (loadingDialog == null)
        //        {
        //            loadingDialog = new ProgressDialog(this);
        //            loadingDialog.setCancelable(cancelble);
        //            
        //        }
        //        loadingDialog.setMessage(str);
        //        loadingDialog.show();
        loadDialog(this, str);
    }

    public void showNewLoading(boolean cancelble, String str) {
        if (!cancelble) return;
        //        if (loadingDialog == null)
        //        {
        //            loadingDialog = new ProgressDialog(this);
        //            loadingDialog.setCancelable(cancelble);
        //            
        //        }
        //        loadingDialog.setMessage(str);
        //        loadingDialog.show();

        loadDialog(this, str);
    }

    public void showNewLoading(boolean cancelble, String str, Context context) {
        try {
            if (!cancelble) return;
            //            if (loadingDialog == null)
            //            {
            //                loadingDialog = new ProgressDialog(context);
            //                loadingDialog.setCancelable(cancelble);
            //                
            //            }
            //            loadingDialog.setMessage(str);
            //            loadingDialog.show();

            loadDialog(this, str);
        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }

    }

    public void dismissMyLoading() {
        //        if (loadingDialog != null)
        //        {
        //            Logger.i("hehui", "dismissMyLoading-->");
        //            loadingDialog.dismiss();
        //            loadingDialog = null;
        //        }
        dissDialog();
    }

    public void showLoading(String msg) {
        showMyLoading(msg);
    }

    public void dismissLoading() {
        dismissMyLoading();
    }

    public void updateLoading(String msg, int progress) {
        if (loadingDialog != null) {
            loadingDialog.setMessage(msg);
            loadingDialog.setProgress(progress);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 通知释放空间
        //System.gc();
        Runtime.getRuntime().gc();

        android.os.Debug.stopMethodTracing();
        //Logger.i("Log", this.getClass().getSimpleName()+" onDestroy");
    }

    protected boolean needExitOnLogout() {
        return true;
    }

    protected boolean isLoginRequired() {
        return true;
    }

    ;

    public void exit() {
        // ActionHelper.backHome();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    /**
     * 判断字符串是否空串包括str="null"这种情况
     * <功能详细描述>
     *
     * @param str
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static boolean isAbsoluteNullStr(String str) {
        str = deleteBlank(str);
        if (str == null || str.length() == 0 || "null".equalsIgnoreCase(str)) {
            return true;
        }

        return false;
    }

    /**
     * 去前后空格和去换行符
     *
     * @param str
     * @return
     */
    public static String deleteBlank(String str) {
        if (null != str && 0 < str.trim().length()) {
            char[] array = str.toCharArray();
            int start = 0, end = array.length - 1;
            while (array[start] == ' ') start++;
            while (array[end] == ' ') end--;
            return str.substring(start, end + 1).replaceAll("\n", "");

        } else {
            return "";
        }

    }

    //private final static int RESULT_LOAD_IMAGE = 10001;

    private final static int RESULT_TAKE_PHOTO = 10002;

    protected String picturePath = null;


    protected void takePhoto() {
        picturePath = GlobalConstant.FILE_CACHE_ROOT;

        File tempFile = new File(picturePath, System.currentTimeMillis() + ".jpg");
        final Intent ci = getTakePickIntent(tempFile);
        startActivityForResult(ci, RESULT_TAKE_PHOTO);
    }

    private static Intent getTakePickIntent(File f) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE, null);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        return intent;
    }

    /**
     * 管理其它窗体
     */
    public void showDialog(Dialog dialog) {
        if (dialog != null) {
            // 先关闭之前的窗体
            hideDialog();
            if (!isResumed) return;
            this.dialog = dialog;
            // 没有显现，则显现
            if (!dialog.isShowing()) {
                dialog.show();
            }
        }
    }

    protected Dialog dialog;

    /**
     * 关闭自定义弹出窗体
     */
    private void hideDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && null != data) {
            if (requestCode != RESULT_TAKE_PHOTO) {
                picturePath = null;
            }
        } else {
            picturePath = null;
        }
    }

    public interface OnConfirmListener {
        public void onOK();

        public void onCancel();
    }

    /**
     * 显示一个确认对话框
     *
     * @param msg               　提示信息
     * @param onConfirmListener 　“确定”和“取消”按钮的回调
     */
    public void showConfirm(String msg, final OnConfirmListener onConfirmListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.public_cozy_prompt);
        builder.setMessage(msg);
        builder.setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (onConfirmListener != null) {
                    dialog.dismiss();
                    onConfirmListener.onOK();
                }
            }
        });

        builder.setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (onConfirmListener != null) {
                    dialog.dismiss();
                    onConfirmListener.onCancel();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onAttachedToWindow() {
        // getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
        super.onAttachedToWindow();
    }

    protected String avatarImgPath;

    /**
     * 任意线程都可显示toast
     */
    protected void showToastInfo(final String text) {

        showToastInfo(text, true);
    }

    /**
     * 任意线程都可显示toast
     */
    protected void showToastInfo(final int resId) {

        showToastInfo(resId, true);
    }

    /**
     * 任意线程都可显示toast
     */
    protected void showToastInfo(final String text, final boolean isLong) {
        if (checkSession()) {
            return;
        }
        runOnUiThread(new Runnable() {
            public void run() {
                /*  Toast.makeText(BaseActivity.this, text,
                          isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();*/

                myToast = new MyToast();
                myToast.showToast(BaseActivity.this, text);
            }
        });
    }

    void showNeedLoginToast() {
        runOnUiThread(new Runnable() {
            public void run() {
                /*  Toast.makeText(BaseActivity.this, text,
                          isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();*/

                myToast = new MyToast();
                myToast.showToast(BaseActivity.this, getString(R.string.msg_need_login));
            }
        });
    }

    protected boolean checkSession() {
        if (isLoginRequired() && MainApplication.getContext().isNeedLogin()) {
            MainApplication.getContext().setNeedLogin(false);
            //            showNeedLoginToast();
            toastDialog(BaseActivity.this, R.string.msg_need_login, new NewDialogInfo.HandleBtn() {

                @Override
                public void handleOkBtn() {
                    PreferenceUtil.removeKey("login_skey");
                    PreferenceUtil.removeKey("login_sauthid");
                    MainApplication.isSessionOutTime = true;
                    redirectLoginActivity();
                }
            });

            return true;
        }
        return false;
    }

    /**
     * 任意线程都可显示toast
     */
    protected void showToastInfo(final int resId, final boolean isLong) {

        if (checkSession()) {
            return;
        }

        runOnUiThread(new Runnable() {
            public void run() {
                /*Toast.makeText(BaseActivity.this,
                        getResources().getString(resId),
                        isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();*/

                myToast = new MyToast();
                myToast.showToast(BaseActivity.this, getString(resId));
            }
        });
    }

    /*private void checkNetwork() {
        // 网络状态 通知
        DataObserverManager.getInstance().registerObserver(new DataObserver(WxPayActivity.class.getName(), DataObserver.EVENT_NETWORK_CHANGER_DESTORY) {

            @Override
            public void onChange() {

                showConfirm(getString(R.string.network_exception), new OnConfirmListener() {
                    @Override
                    public void onOK() {
                        // 转至 网络 设置界面
                        Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                        startActivity(intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        System.exit(0);

                    }

                    @Override
                    public void onCancel() {
                        BaseActivity.this.finish();
                    }
                });
            }
        });
    }*/

    public void showExitDialog(final Activity context) {
        dialogInfo = new DialogInfo(context, getString(R.string.public_cozy_prompt), getString(R.string.show_sign_out), getString(R.string.btnOk), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                finish();
                MainApplication.getContext().exit();
//                                System.exit(0);
                MainApplication.isSessionOutTime = true;

            }

            @Override
            public void handleCancleBtn() {
                dialogInfo.cancel();
            }
        }, null);

        DialogHelper.resize(context, dialogInfo);
        dialogInfo.show();
    }


    public Dialog createDialog(Activity context, String str) {
        // 加载样式
        Dialog dialog = new Dialog(this, R.style.my_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.load_progress_info, null);// 得到加载view dialog_common
        TextView tv = (TextView) v.findViewById(R.id.tv_dialog);
        tv.setText(str);
        // dialog透明
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        //ProgressBar pb = (ProgressBar)v.findViewById(R.id.dialog);
        lp.alpha = 0.8f;
        dialog.getWindow().setAttributes(lp);
        dialog.setContentView(v);
        DialogHelper.resizeNew(context, dialogNew);
        dialog.show();
        return dialog;
    }

    public void loadDialog(Activity context, String str) {
        // 加载样式
        if (null == dialogNew) {
            dialogNew = new Dialog(context, R.style.my_dialog);
            dialogNew.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogNew.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogNew.setCanceledOnTouchOutside(false);
            LayoutInflater inflater = LayoutInflater.from(context);
            View v = inflater.inflate(R.layout.load_progress_info, null);// 得到加载view dialog_common
            TextView tv = (TextView) v.findViewById(R.id.tv_dialog);
            tv.setText(str);
            // dialog透明
            WindowManager.LayoutParams lp = dialogNew.getWindow().getAttributes();
            lp.alpha = 0.6f;
            dialogNew.getWindow().setAttributes(lp);
            dialogNew.setContentView(v);
            dialogNew.setOnKeyListener(new OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                    if (keycode == KeyEvent.KEYCODE_BACK) {
                        return true;
                    }
                    return false;
                }
            });
        }
        DialogHelper.resizeNew(context, dialogNew);
        dialogNew.show();
    }

    public void loadDialog(Activity context, int res) {
        // 加载样式
        try {

            if (null == dialogNew) {
                dialogNew = new Dialog(context, R.style.my_dialog);
                dialogNew.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogNew.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialogNew.setCanceledOnTouchOutside(false);
                LayoutInflater inflater = LayoutInflater.from(context);
                View v = inflater.inflate(R.layout.load_progress_info, null);// 得到加载view dialog_common
                TextView tv = (TextView) v.findViewById(R.id.tv_dialog);
                tv.setText(res);
                // dialog透明
                WindowManager.LayoutParams lp = dialogNew.getWindow().getAttributes();
                lp.alpha = 0.8f;
                dialogNew.getWindow().setAttributes(lp);
                dialogNew.setContentView(v);
                dialogNew.setOnKeyListener(new OnKeyListener() {

                    @Override
                    public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                        if (keycode == KeyEvent.KEYCODE_BACK) {
                            return true;
                        }
                        return false;
                    }
                });
            }
            DialogHelper.resizeNew(context, dialogNew);
            dialogNew.show();
        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }
    }

    public void dissDialog() {
        if (null != dialogNew) {
            BaseActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        dialogNew.dismiss();
                        dialogNew = null;
                    } catch (Exception e) {
                        Log.e(TAG,Log.getStackTraceString(e));
                    }
                }
            });
        }
    }

    public void toastDialog(final Activity context, final String content, final String btStr, final NewDialogInfo.HandleBtn handleBtn) {
        context.runOnUiThread(new Runnable() {

            @Override
            public void run() {

                try {
                    if (StringUtil.isEmptyOrNull(content)) {
                        return;
                    }

                    NewDialogInfo info = new NewDialogInfo(context, content, null, 2, null, handleBtn);
                    info.setOnKeyListener(new OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                            if (keycode == KeyEvent.KEYCODE_BACK) {
                                return true;
                            }
                            return false;
                        }
                    });
                    DialogHelper.resize(context, info);

                    info.show();
                } catch (Exception e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
        });

    }

    public void toastDialog(final Activity context, final String content, final NewDialogInfo.HandleBtn handleBtn) {
        context.runOnUiThread(new Runnable() {

            @Override
            public void run() {

                try {
                    if (StringUtil.isEmptyOrNull(content)) {
                        return;
                    }

                    NewDialogInfo info = new NewDialogInfo(context, content, null, 2, null, handleBtn);
                    info.setOnKeyListener(new OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                            if (keycode == KeyEvent.KEYCODE_BACK) {
                                return true;
                            }
                            return false;
                        }
                    });
                    DialogHelper.resize(context, info);

                    info.show();
                } catch (Exception e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
        });

    }

    public void toastDialog(final Activity context, final Integer content, final NewDialogInfo.HandleBtn handleBtn) {
        context.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                try {

                    NewDialogInfo info = new NewDialogInfo(context, context.getString(content), null, 2, null, handleBtn);
                    info.setOnKeyListener(new OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                            if (keycode == KeyEvent.KEYCODE_BACK) {
                                return true;
                            }
                            return false;
                        }
                    });
                    DialogHelper.resize(context, info);

                    info.show();
                } catch (Exception e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
        });

    }

    /**
     * 跳转页面
     *
     * @param clazz
     * @see [类、类#方法、类#成员]
     */
    @SuppressWarnings("rawtypes")
    public void showPage(Class clazz) {
        Intent intent = new Intent(this, clazz);
        this.startActivity(intent);
    }

    /**
     * 跳转页面
     *
     * @param clazz
     * @see [类、类#方法、类#成员]
     */
    @SuppressWarnings("rawtypes")
    public void showPage(Class clazz, Bundle bundle) {
        Intent intent = new Intent(this, clazz);
        intent.putExtra("bundle", bundle);
        this.startActivity(intent);
    }

    /**
     * 跳转页面
     *
     * @param clazz
     * @see [类、类#方法、类#成员]
     */
    @SuppressWarnings("rawtypes")
    public void showPage(Class clazz, String param) {
        Intent intent = new Intent(this, clazz);
        intent.putExtra("picUrl", param);
        this.startActivity(intent);
    }

    public void showUpgradeInfoDialog(UpgradeInfo result, CommonConfirmDialog.ConfirmListener listener) {
        StringBuilder sb = new StringBuilder();
        sb.append(getString(R.string.show_version) + result.versionName + "\n");
//        if (result.dateTime != 0) {
//            sb.append(getString(R.string.show_version_time) + DateUtil.formatYMD(result.dateTime) + "\n");
//        } else {
//            sb.append(getString(R.string.show_version_time_unknow) + "\n");
//        }
        if (!TextUtils.isEmpty(result.message)) {
            if (result.message.equals("null")) {
                result.message = "";
            }
            sb.append(getString(R.string.show_version_update_content) + "\n" + result.message + "\n\n");
        }
        sb.append(getString(R.string.show_is_update));

        CommonConfirmDialog.show(this, getString(R.string.show_find_new_version), sb.toString(), listener, true, result);
    }

    /**
     */
    //private int flag = 2;

    private long addTime;

    private Calendar c = null;

    public void showDatetimeDialog(final TextView mTvTime) {
        addTime = System.currentTimeMillis();
        c = Calendar.getInstance();
        c.setTimeInMillis(addTime);
        dialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker dp, int year, int month, int dayOfMonth) {
                if (year > getYear()) { //设置年大于当前年，直接设置，不用判断下面的
                    c.set(year, month, dayOfMonth);
                    addTime = c.getTimeInMillis();
                    mTvTime.setText(DateUtil.formatYYMD(addTime));
                    //flag = 1;
                } else if (year == getYear()) { //设置年等于当前年，则向下开始判断月
                    if ((month + 1) > getMonth()) { //设置月等于当前月，直接设置，不用判断下面的
                        //flag = 1;
                        c.set(year, month, dayOfMonth);
                        addTime = c.getTimeInMillis();
                        mTvTime.setText(DateUtil.formatYYMD(addTime));
                    } else if ((month + 1) == getMonth()) { //设置月等于当前月，则向下开始判断日
                        if (dayOfMonth > getDay()) { //设置日大于当前月，直接设置，不用判断下面的
                            //flag = 1;
                            c.set(year, month, dayOfMonth);
                            addTime = c.getTimeInMillis();
                            mTvTime.setText(DateUtil.formatYYMD(addTime));
                        } else if (dayOfMonth == getDay()) { //设置日等于当前日，则向下开始判断时
                            //flag = 2;
                            c.set(year, month, dayOfMonth);
                            addTime = c.getTimeInMillis();
                            mTvTime.setText(DateUtil.formatYYMD(addTime));
                        } else { //设置日小于当前日，提示重新设置
                            //flag = 3;
                            showToastInfo("当前日不能小于今日,请重新设置");
                        }
                    } else { //设置月小于当前月，提示重新设置
                        //flag = 3;
                        showToastInfo("当前月不能小于今月，请重新设置");
                    }
                } else { //设置年小于当前年，提示重新设置
                    //flag = 3;
                    showToastInfo("当前年不能小于今年，请重新设置");
                }
                //mTvTime.setText(DateUtil.formatYYMD(addTime));

            }
        }, c.get(Calendar.YEAR), // 传入年份
                c.get(Calendar.MONTH), // 传入月份
                c.get(Calendar.DAY_OF_MONTH) // 传入天数*/
        );
        dialog.show();
    }

    /**
     * 得到时间和日期一系列方法
     *
     * @return
     */
    public int getYear() {
        return c.get(Calendar.YEAR);
    }

    public int getMonth() {
        return c.get(Calendar.MONTH) + 1;//系统日期从0开始算起
    }

    public int getDay() {
        return c.get(Calendar.DAY_OF_MONTH);
    }

    public int getHonor() {
        return c.get(Calendar.HOUR_OF_DAY);
    }

    public int getMin() {
        return c.get(Calendar.MINUTE);
    }

    public String getStringById(int id) {
        try {
            return this.getResources().getString(id);
        } catch (Exception e) {
            return "";
        }

    }

    /**
     * 判断当前语言环境是否是英文环境
     * @return
     */
    public boolean isEnglish(){
        boolean isEnglish = false;
        String language = PreferenceUtil.getString("language", "");
        Log.e("JAMY","language: "+language);
        if (!TextUtils.isEmpty(language) && language.equals(MainApplication.LANG_CODE_EN_US)) {
            isEnglish = true;
        }
        return isEnglish;
    }
}
