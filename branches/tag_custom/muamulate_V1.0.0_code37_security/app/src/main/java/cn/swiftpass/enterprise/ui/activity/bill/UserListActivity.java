/*
 * 文 件 名:  UserListActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-21
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.bill;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnLoadListener;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnRefreshListener;
import java.util.ArrayList;
import java.util.List;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2016-11-21]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class UserListActivity extends BaseActivity implements OnRefreshListener, OnLoadListener {

    private static final String TAG = UserListActivity.class.getSimpleName();
    private SwipeMenuListView cashier_list;

    private SettleCashierAdapter adapter;

    private List<UserModel> list = new ArrayList<UserModel>();

    private Handler mHandler = new Handler();

    private TextView tv_prompt;

    private int pageFulfil = 0;

    private RelativeLayout lr_all;

    private ImageView iv_all_choice;

    //当前选中的收银员的model
    private UserModel userModel;

    private int pageCount = 0;

    private LinearLayout ll_cashier_search_back_btn;
    private EditText et_cashier_search_input;
    private ImageView iv_clear_btn;
    private Button but_cashier_seach;

    //从汇总页面还是从账单列表页面跳转到当前的页面
    private boolean isFromBillEnter;
    private boolean isFromCodeListEnter;

    public static void startActivity(Context context, UserModel userModel ,boolean isFromBillEnter,boolean isFromCodeListEnter) {
        Intent it = new Intent();
        it.putExtra("userModel", userModel);
        it.putExtra("isFromBillEnter",isFromBillEnter);
        it.putExtra("isFromCodeListEnter",isFromCodeListEnter);
        it.setClass(context, UserListActivity.class);
        context.startActivity(it);
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            @SuppressWarnings("unchecked")
            List<UserModel> result = (List<UserModel>) msg.obj;
            switch (msg.what) {
                case SwipeMenuListView.REFRESH:
                    list.clear();
                    list.addAll(result);
                    break;
                case SwipeMenuListView.LOAD:
                    list.addAll(result);
                    break;
            }
            cashier_list.onRefreshComplete();
            cashier_list.onLoadComplete();
            cashier_list.setResultSize(result.size());
            adapter.notifyDataSetChanged();
        }

        ;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_push_record);

        userModel = (UserModel) getIntent().getSerializableExtra("userModel");
        isFromBillEnter = getIntent().getBooleanExtra("isFromBillEnter",false);
        isFromCodeListEnter = getIntent().getBooleanExtra("isFromCodeListEnter",false);
        initView();
        if (userModel != null) {
            iv_all_choice.setVisibility(View.INVISIBLE);
        }
        loadCashierData(0, true, 2);

        lr_all.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setDefault(list);
                iv_all_choice.setVisibility(View.VISIBLE);
                adapter.notifyDataSetChanged();
                if(isFromBillEnter){
                    HandlerManager.notifyMessage(HandlerManager.CHOICE_CASHIER, HandlerManager.CHOICE_CASHIER, null);
                }else{
                    if (isFromCodeListEnter){
                        HandlerManager.notifyMessage(HandlerManager.CHOICE_CASHIER_STATIC_CODE, HandlerManager.CHOICE_CASHIER_STATIC_CODE, null);

                    }else{
                        HandlerManager.notifyMessage(HandlerManager.BILL_CHOICE_USER, HandlerManager.BILL_CHOICE_USER, null);
                    }
                }
                finish();
            }
        });
    }

    private void initView() {
        lr_all = getViewById(R.id.lr_all);
        iv_all_choice = getViewById(R.id.iv_all_choice);
        tv_prompt = getViewById(R.id.tv_prompt);
        tv_prompt.setText(R.string.tv_user_list);
        ll_cashier_search_back_btn = getViewById(R.id.ll_cashier_search_back_btn);
        iv_clear_btn = getViewById(R.id.iv_clear_btn);
        et_cashier_search_input = getViewById(R.id.et_cashier_search_input);
        et_cashier_search_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String searchContent = et_cashier_search_input.getText().toString().trim();
                if (searchContent != null && searchContent.length() > 0)
                {
                    iv_clear_btn.setVisibility(View.VISIBLE);
                }
                else
                {
                    iv_clear_btn.setVisibility(View.INVISIBLE);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        iv_clear_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!StringUtil.isEmptyOrNull(et_cashier_search_input.getText().toString())){
                    et_cashier_search_input.setText("");
                }
            }
        });

        et_cashier_search_input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    list.clear();
                    loadData(0, et_cashier_search_input.getText().toString());

                    return true;
                }
                return false;
            }
        });

        but_cashier_seach = getViewById(R.id.but_cashier_seach);
        but_cashier_seach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //收银员收索
                if (!StringUtil.isEmptyOrNull(et_cashier_search_input.getText().toString()))
                {
                    list.clear();
                    loadData(0, et_cashier_search_input.getText().toString());
                }
            }
        });

        cashier_list = (SwipeMenuListView) findViewById(R.id.cashier_manager_id);

        if (userModel != null) {
            adapter = new SettleCashierAdapter(UserListActivity.this, userModel.getId());
        } else {
            adapter = new SettleCashierAdapter(UserListActivity.this, 0);
        }
        //        cashier_list.setAutoLoadMore(true);
        cashier_list.setOnRefreshListener(this);
        cashier_list.setOnLoadListener(this);
        //        listView = cashier_list.mListView;
        cashier_list.setAdapter(adapter);

        ll_cashier_search_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        cashier_list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {
                try {
                    UserModel userModel = list.get(position - 1);
                    if (userModel != null) {
                        //选中则先回复全部不选中，然后设置对应的选中
                        setDefault(list);
                        userModel.setIsselect(true);
                        iv_all_choice.setVisibility(View.INVISIBLE);
                        adapter.notifyDataSetChanged();
                        if(isFromBillEnter){
                            HandlerManager.notifyMessage(HandlerManager.CHOICE_CASHIER,
                                    HandlerManager.CHOICE_CASHIER,
                                    userModel);
                        }else{
                            if (isFromCodeListEnter){
                                HandlerManager.notifyMessage(HandlerManager.CHOICE_CASHIER_STATIC_CODE,
                                        HandlerManager.CHOICE_CASHIER_STATIC_CODE,
                                        userModel);
                            }else{
                                HandlerManager.notifyMessage(HandlerManager.BILL_CHOICE_USER,
                                        HandlerManager.BILL_CHOICE_USER,
                                        userModel);
                            }

                        }
                        UserListActivity.this.finish();
                    }
                } catch (Exception e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
        });

    }

    /**
     * 默认全部不选中状态
     */
    private void setDefault(List<UserModel> datas){
        if (datas.size() == 0){
            return;
        }
        int lenth = datas.size();
        UserModel usermodel;
        for (int i =0;i<lenth;i++){
            datas.get(i).setIsselect(false);
        }
    }


    /**
     * 默认全部不选中状态
     */
    private void setSelect(List<UserModel> datas,UserModel model){
        if (datas.size() == 0){
            return;
        }
        int lenth = datas.size();
        for (int i =0;i<lenth;i++){
            if (0 < model.getId() && datas.get(i).getId() == model.getId()){
                datas.get(i).setIsselect(true);
            }else{
                datas.get(i).setIsselect(false);
            }
        }
    }

    private void loadData(final int page, final String input)
    {

        UserManager.queryCashier(page, 10, input, new UINotifyListener<List<UserModel>>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                showLoading(false, R.string.public_data_loading);
            }

            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dismissLoading();
                if (checkSession())
                {
                    return;
                }
                if (object != null)
                {
                    showToastInfo(object.toString());
                }
            }

            @Override
            public void onSucceed(List<UserModel> result)
            {
                super.onSucceed(result);
                dismissLoading();

                if (result != null && result.size() > 0) {
                    UserListActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                           showDateView();
                        }
                    });
                    pageCount = result.get(0).getPageCount();
                    list.clear();
                    list.addAll(result);

                    adapter.notifyDataSetChanged();
                    cashier_list.onLoadComplete();

                }else {

                    UserListActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            if (list.size() == 0) {
                                hideDateView();
                                cashier_list.setVisibility(View.GONE);
                            }
                            //                            else
                            //                            {
                            //                                cashier_list.onLoadComplete();
                            //                                cashier_list.setResultSize(0);
                            //                            }
                        }
                    });
                }
            }
        });

    }

    /**
     * 加载收银员列表
     * <功能详细描述>
     *
     * @param page
     * @param isLoadMore
     * @see [类、类#方法、类#成员]
     */
    private void loadCashierData(final int page, final boolean isLoadMore, final int what) {

        UserManager.queryCashier(page, 20, null, new UINotifyListener<List<UserModel>>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                if (isLoadMore) {
                    loadDialog(UserListActivity.this, R.string.public_data_loading);
                }
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                dissDialog();
                if (checkSession()) {
                    return;
                }
                if (object != null) {
                    toastDialog(UserListActivity.this, object.toString(), null);
                    UserListActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideDateView();
                        }
                    });
                }
            }

            @Override
            public void onSucceed(List<UserModel> result) {
                super.onSucceed(result);
                dismissLoading();

                if (result != null && result.size() > 0) {
                    UserListActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            showDateView();
                        }
                    });
                    pageCount = result.get(0).getPageCount();
                    if (what == 2) {
                        list.clear();
                        list.addAll(result);
                        if (null != userModel){
                            setSelect(list,userModel);
                        }else{
                            setDefault(list);
                        }
                        adapter.notifyDataSetChanged();
                        cashier_list.setResultSize(result.size());
                        cashier_list.onLoadComplete();
                    } else {
                        Message msg = handler.obtainMessage();
                        msg.what = what;
                        msg.obj = result;
                        handler.sendMessage(msg);
                    }
                } else {

                    UserListActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            if (list.size() == 0) {

                                hideDateView();
                            }
                        }
                    });
                }

            }
        });
    }

    public void hideDateView(){
        tv_prompt.setVisibility(View.VISIBLE);
        cashier_list.setVisibility(View.GONE);
        lr_all.setVisibility(View.GONE);
    }

    public void showDateView(){
        tv_prompt.setVisibility(View.GONE);
        cashier_list.setVisibility(View.VISIBLE);
        lr_all.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLoad() {
        pageFulfil = pageFulfil + 1;
        Logger.i("hehui", "pageFulfil-->" + pageFulfil);
        if (pageFulfil <= pageCount) {
            loadCashierData(pageFulfil, true, SwipeMenuListView.LOAD);
        }
    }

    @Override
    public void onRefresh() {
        pageFulfil = 0;
        Logger.i("hehui", "onRefresh pageFulfil-->" + pageFulfil);
        loadCashierData(pageFulfil, false, SwipeMenuListView.REFRESH);
    }


    public class SettleCashierAdapter extends BaseAdapter
    {
        private ViewHolder holder;

        private Context context;

        private long userId = 0;

        public SettleCashierAdapter()
        {
        }

        public SettleCashierAdapter(Context context, long userId)
        {

            this.context = context;

            this.userId = userId;
        }

        @Override
        public int getCount()
        {
            return list.size();
        }

        @Override
        public Object getItem(int position)
        {
            return list.get(position);
        }

        @Override
        public long getItemId(int position)
        {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(context, R.layout.settl_cashier_list_item, null);
                holder = new ViewHolder();
                holder.cashier_state = (TextView)convertView.findViewById(R.id.cashier_state);
                holder.cashier_store = (TextView)convertView.findViewById(R.id.cashier_store);
                holder.userId = (TextView)convertView.findViewById(R.id.userId);
                holder.userName = (TextView)convertView.findViewById(R.id.userName);
                holder.usertel = (TextView)convertView.findViewById(R.id.usertel);
                holder.iv_choice = (ImageView)convertView.findViewById(R.id.iv_choice);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            UserModel userModel = list.get(position);
            if (userModel.getDeptname() != null)
            {
                holder.cashier_store.setText(userModel.getDeptname());
            }
            holder.userId.setText(String.valueOf(userModel.getId()));
            if (userModel.getPhone() != null)
            {
                holder.usertel.setText(userModel.getPhone());
            }

            if (userModel.getRealname() != null)
            {
                holder.userName.setText(userModel.getRealname());
            }

            if (userModel.isIsselect()){
                holder.iv_choice.setVisibility(View.VISIBLE);
            }else{
                holder.iv_choice.setVisibility(View.GONE);
            }
//            if (userId > 0 && userModel.getId() == userId)
//            {
//                holder.iv_choice.setVisibility(View.VISIBLE);
//            }
//            else
//            {
//                holder.iv_choice.setVisibility(View.GONE);
//            }
            return convertView;
        }


    }

    private class ViewHolder
    {
        private ImageView iv_choice;

        private TextView userId, cashier_state, userName, usertel, cashier_store;

    }
}
