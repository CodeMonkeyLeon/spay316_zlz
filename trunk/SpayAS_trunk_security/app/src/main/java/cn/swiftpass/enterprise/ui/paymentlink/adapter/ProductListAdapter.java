package cn.swiftpass.enterprise.ui.paymentlink.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkProduct;
import cn.swiftpass.enterprise.ui.paymentlink.widget.SlideItemView1;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.KotlinUtils;

public class ProductListAdapter extends BaseRecyclerAdapter<PaymentLinkProduct> {

    private Context mContext;
    private OnListItemClickListener clickListener;
    private SlideItemView1 openItem;

    private boolean isDeleteAble = true;

    public void setItemClickListener(OnListItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void cleanItemClickListener() {
        this.clickListener = null;
    }


    public ProductListAdapter(Context context, ArrayList<PaymentLinkProduct> data) {
        this(context, data, true);
    }


    public ProductListAdapter(Context context, ArrayList<PaymentLinkProduct> data, boolean deleteAble) {
        super(R.layout.item_payment_link_product, data);
        mContext = context;
        isDeleteAble = deleteAble;
    }

    @Override
    public void setDataList(@Nullable List<PaymentLinkProduct> data) {
        super.setDataList(data);
        if (null != openItem) {
            openItem.reset();
            openItem = null;
        }
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder holder, PaymentLinkProduct item, int position) {
        if (item == null) {
            return;
        }

        TextView desc1 = holder.getView(R.id.tv_desc1);
        TextView desc2 = holder.getView(R.id.tv_desc2);
        TextView desc3 = holder.getView(R.id.tv_desc3);
        TextView desc4 = holder.getView(R.id.tv_desc4);
        ImageView imgIcon = holder.getView(R.id.id_img);

        Glide.with(mContext)
                .load(item.goodsPic)
                .error(R.drawable.icon_payment_link_default)
                .transform(new CenterCrop(), new RoundedCorners(KotlinUtils.INSTANCE.dp2px(4f, mContext)))
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imgIcon);

        desc1.setText(item.goodsName);
        if (!TextUtils.isEmpty(item.goodsCode)) {
            desc2.setText(mContext.getResources().getString(R.string.payment_link_add_product_phone_code)
                    + ": " + item.goodsCode);
        } else {
            desc2.setText("");
        }
        if (!TextUtils.isEmpty(item.goodsDesc)) {
            desc3.setText(mContext.getResources().getString(R.string.payment_link_add_product_description)
                    + ": " + item.goodsDesc);
        } else {
            desc3.setText("");
        }
        desc4.setText(MainApplication.getInstance().getFeeFh() + " " + DateUtil.formatMoneyUtils(item.goodsPrice));
        desc2.setEllipsize(TextUtils.TruncateAt.END);
        desc2.setMaxLines(1);
        desc3.setEllipsize(TextUtils.TruncateAt.END);
        desc3.setMaxLines(1);




        SlideItemView1 view = (SlideItemView1) holder.itemView;
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // 禁止滑动
                return !isDeleteAble;
            }
        });
        view.setOnStateChangeListener(new SlideItemView1.OnStateChangeListener() {
            @Override
            public void onFuncShow(SlideItemView1 itemView, boolean isShow) {
                if (isShow) {
                    if (null != openItem && itemView != openItem) {
                        openItem.reset();
                        openItem = itemView;
                    } else if (null == openItem) {
                        openItem = itemView;
                    }
                } else {
                    if (null != openItem && itemView == openItem) {
                        openItem = null;
                    }
                }
            }

            @Override
            public void onFuncClick() {
                if (null != openItem && openItem.isExpansion()) {
                    openItem.reset();
                    openItem = null;
                }
                if (clickListener != null) {
                    clickListener.onDeleteClick(position);
                }
            }

            @Override
            public void onContentClick() {
                if (clickListener != null) {
                    clickListener.onItemClick(position);
                }
            }
        });
    }
}
