package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.bussiness.model.OrderList
import cn.swiftpass.enterprise.bussiness.model.OrderSearchResult
import cn.swiftpass.enterprise.bussiness.model.UserModelList
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.io.okhttp.OkhttpUtil
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.RefundManagerContract
import cn.swiftpass.enterprise.utils.JsonUtil
import cn.swiftpass.enterprise.utils.SignUtil
import cn.swiftpass.enterprise.utils.StringUtil
import com.example.common.entity.EmptyData
import okhttp3.Call


/**
 * @author lizheng.zhao
 * @date 2022/11/23
 *
 * @你没有如期归来
 * @而这正是离别的意义
 */
class RefundManagerPresenter : RefundManagerContract.Presenter {

    private var mView: RefundManagerContract.View? = null


    override fun queryOrderData(
        orderNo: String?,
        currentPage: Int,
        state: Int,
        time: String?,
        orderNoMch: String?
    ) {
        mView?.let { view ->

            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)

            var noMch = orderNo
            orderNoMch?.let {
                noMch = it
            }

            AppClient.queryOrderData(
                noMch,
                MainApplication.getInstance().userInfo.mchId,
                MainApplication.getInstance().getUserId().toString(),
                time,
                currentPage.toString(),
                state.toString(),
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object :
                    CallBackUtil.CallBackCommonResponse<OrderSearchResult>(OrderSearchResult()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.queryOrderDataFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val orderList = JsonUtil.jsonToBean(
                                response.message,
                                OrderSearchResult::class.java
                            ) as OrderSearchResult
                            for (order in orderList.data) {
                                order.notify_time =
                                    if (TextUtils.isEmpty(order.notifyTime)) 0 else order.notifyTime.toLong()
                                order.add_time =
                                    if (TextUtils.isEmpty(order.addTime)) 0 else order.addTime.toLong()
                                order.useId = order.userId.toString()
                            }
                            view.queryOrderDataSuccess(orderList, orderNo)
                        }
                    }
                }
            )
        }
    }

    override fun queryRefundDetail(orderNo: String?, mchId: String?) {
        mView?.let { view ->

            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)

            AppClient.queryRefundDetail(
                orderNo,
                mchId,
                MainApplication.getInstance().getUserId().toString(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.queryRefundDetailFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"",
                                "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                Order::class.java
                            ) as Order
                            order.useId = order.userId.toString()
                            order.orderNoMch = order.orderNo
                            order.cashFeel =
                                if (TextUtils.isEmpty(order.cashFee)) 0 else order.cashFee.toLong()
                            view.queryRefundDetailSuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun queryOrderDetail(orderNo: String?, mchId: String?, isMark: Boolean) {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
            AppClient.queryOrderDetail(
                orderNo,
                orderNo,
                orderNo,
                mchId,
                MainApplication.getInstance().getUserId().toString(),
                System.currentTimeMillis().toString(),
                isMark,
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.queryOrderDetailFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"", "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message, Order::class.java
                            ) as Order
                            order.add_time =
                                if (TextUtils.isEmpty(order.addTime)) 0 else order.addTime.toLong()
                            order.useId = order.userId.toString()
                            order.canAffim = order.canAffirm
                            order.setUplanDetailsBeans(order.discountDetail)
                            view.queryOrderDetailSuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun queryRefundOrder(orderNo: String?, mchId: String?, page: Int) {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)

            AppClient.queryRefundOrder(
                mchId,
                page.toString(),
                "2",
                "1",
                orderNo,
                MainApplication.getInstance().getUserId().toString(),
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<OrderList>(OrderList()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.queryRefundOrderFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val settle = JsonUtil.jsonToBean(
                                response.message, OrderList::class.java
                            ) as OrderList
                            for (order in settle.data) {
                                order.money = order.totalFee
                                order.add_time =
                                    if (TextUtils.isEmpty(order.addTime)) 0 else order.addTime.toLong()
                                order.useId = order.userId.toString()
                            }
                            view.queryRefundOrderSuccess(settle.data, orderNo)
                        }
                    }
                }
            )


        }
    }

    override fun queryUserRefundOrder(
        outTradeNo: String?, refundTime: String?, mchId: String?, refundState: String?, page: Int
    ) {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)

            AppClient.queryUserRefundOrder(
                outTradeNo,
                refundState,
                refundTime,
                mchId,
                page.toString(),
                "10",
                MainApplication.getInstance().getUserId().toString(),
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<OrderList>(OrderList()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.queryUserRefundOrderFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val settle = JsonUtil.jsonToBean(
                                response.message, OrderList::class.java
                            ) as OrderList
                            for (order in settle.data) {
                                order.money = order.totalFee
                                order.add_time =
                                    if (TextUtils.isEmpty(order.addTime)) 0 else order.addTime.toLong()
                                order.useId = order.userId.toString()
                            }
                            view.queryUserRefundOrderSuccess(settle.data, outTradeNo)
                        }
                    }
                }
            )

        }
    }

    override fun queryCashier(
        page: Int,
        pageSize: Int,
        input: String?,
        isFirst: Boolean,
        isLoadCashier: Boolean
    ) {
        mView?.let { view ->

            if (isLoadCashier) {
                if (isFirst) {
                    view.showLoading(
                        R.string.public_data_loading, ParamsConstants.COMMON_LOADING
                    )
                }
            } else {
                view.showLoading(
                    R.string.public_data_loading, ParamsConstants.COMMON_LOADING
                )
            }


            AppClient.queryCashier(
                MainApplication.getInstance().getMchId(),
                page.toString() + "",
                input,
                pageSize.toString(),
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<UserModelList>(UserModelList()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.queryCashierFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val dynModels = JsonUtil.jsonToBean(
                                response.message, UserModelList::class.java
                            ) as UserModelList
                            view.queryCashierSuccess(dynModels, isFirst, isLoadCashier)
                        }
                    }
                }
            )

        }
    }

    override fun cashierDelete(
        userId: Long, position: Int
    ) {
        mView?.let { view ->
            view.showLoading(R.string.show_delete_loading, ParamsConstants.COMMON_LOADING)

            val params: MutableMap<String, String> = HashMap()
            params[ParamsConstants.ID] = userId.toString()
            params[ParamsConstants.MCH_ID] = MainApplication.getInstance().getMchId()

            if (!TextUtils.isEmpty(MainApplication.getInstance().getSignKey())) {
                params[ParamsConstants.SIGN] =
                    SignUtil.getInstance()
                        .createSign(params, MainApplication.getInstance().getSignKey())
            }

            val spayRs = System.currentTimeMillis()
            //加签名
            if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getNewSignKey())) {
                params[ParamsConstants.SPAY_RS] = spayRs.toString()
                params[ParamsConstants.NNS] =
                    SignUtil.getInstance()
                        .createSign(params, MainApplication.getInstance().getNewSignKey())
            }


            OkhttpUtil.okHttpPost(MainApplication.getInstance().baseUrl + "spay/user/deleteSpayUser",
                params,
                OkhttpUtil.getHeadParam(spayRs.toString()),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.cashierDeleteFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        view.cashierDeleteSuccess(true, position)
                    }
                })


        }
    }

    override fun attachView(view: RefundManagerContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}