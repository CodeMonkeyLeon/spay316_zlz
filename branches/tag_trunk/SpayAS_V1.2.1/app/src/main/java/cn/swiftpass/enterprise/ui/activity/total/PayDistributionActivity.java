/*
 * 文 件 名:  PayDistributionActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-9-19
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.total;

import java.util.ArrayList;
import java.util.List;

import org.xclcharts.test.OrderTotalModel;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.City;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.chart.RoundProgressBar;
import cn.swiftpass.enterprise.ui.activity.user.RefundManagerActivity;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateTimeUtil;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DisplayUtil;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.HelpUI;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 交易分布
 * 
 * @author  he_hui
 * @version  [版本号, 2016-9-19]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class PayDistributionActivity extends TemplateActivity
{
    private ViewHolder holder;
    
    private ListView list;
    
    private boolean defaultTimeNow = true; // 默认实时
    
    private EditText refund_search_input;
    
    private LinearLayout lay_input;
    
    private List<OrderTotalModel> orderTotalModels;
    
    private boolean defaultPayType = true; //默认类型收银员
    
    private DistributionAdapter distributionAdapter;
    
    private TextView tv_total_distribtion;
    
    private LinearLayout ly_date, ly_now;
    
    private boolean isDateBut, isNowBut;
    
    private TextView tv_title_now, tv_title_date;
    
    private View v_now, v_date;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.total_distribution);
        HandlerManager.registerHandler(HandlerManager.PAY_TOTAL_TYPE_DISTRIBUTION, handler);
        HandlerManager.registerHandler(HandlerManager.PAY_TOTAL_TYPE_DISTRIBUTION_USER, handler);
        initView();
        setLister();
        orderTotalModels = new ArrayList<OrderTotalModel>();
        distributionAdapter = new DistributionAdapter(orderTotalModels);
        list.setAdapter(distributionAdapter);
        
        loadOrderTotal(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00",
            DateUtil.formatTime(System.currentTimeMillis()),
            "5",
            null);
        
    }
    
    private void setLister()
    {
        ly_now.setOnClickListener(new View.OnClickListener()
        {
            
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v)
            {
                defaultTimeNow = true;
                if (isDateBut)
                {
                    tv_title_now.setTextColor(PayDistributionActivity.this.getResources()
                        .getColor(R.color.order_title_blue));
                    v_now.setVisibility(View.VISIBLE);
                    tv_title_date.setTextColor(PayDistributionActivity.this.getResources()
                        .getColor(R.color.login_title_text_color));
                    v_date.setVisibility(View.GONE);
                    isDateBut = false;
                    isNowBut = false;
                    if (defaultPayType)
                    { //收银员
                        loadOrderTotal(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00",
                            DateUtil.formatTime(System.currentTimeMillis()),
                            "5",
                            null);
                        titleBar.setRightButLayVisible(true, R.drawable.icon_search_white);
                    }
                    else
                    { //支付类型
                        loadOrderTotal(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00",
                            DateUtil.formatTime(System.currentTimeMillis()),
                            "3",
                            null);
                        titleBar.setRightButLayVisible(false, 0);
                    }
                    
                }
            }
        });
        
        ly_date.setOnClickListener(new View.OnClickListener()
        {
            
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v)
            {
                defaultTimeNow = false;
                if (!isNowBut)
                {
                    isDateBut = true;
                    isNowBut = true;
                    tv_title_now.setTextColor(PayDistributionActivity.this.getResources()
                        .getColor(R.color.login_title_text_color));
                    v_now.setVisibility(View.GONE);
                    tv_title_date.setTextColor(PayDistributionActivity.this.getResources()
                        .getColor(R.color.order_title_blue));
                    v_date.setVisibility(View.VISIBLE);
                    if (defaultPayType)
                    { //收银员
                        loadOrderTotal(DateTimeUtil.getMothdFirst() + " 00:00:00",
                            DateUtil.formatTime(DateUtil.getBeforeDate().getTime()),
                            "5",
                            null);
                        titleBar.setRightButLayVisible(true, R.drawable.icon_search_white);
                    }
                    else
                    { //支付类型
                        loadOrderTotal(DateTimeUtil.getMothdFirst() + " 00:00:00",
                            DateUtil.formatTime(DateUtil.getBeforeDate().getTime()),
                            "2",
                            null);
                        titleBar.setRightButLayVisible(false, 0);
                    }
                    
                }
                
            }
        });
    }
    
    /**
     * 加载统计数据
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    void loadOrderTotal(String startTime, String endTime, final String countMethod, String userName)
    {
        OrderManager.getInstance().getNewOrderTotal(startTime,
            endTime,
            countMethod,
            userName,
            new UINotifyListener<List<OrderTotalModel>>()
            {
                @Override
                public void onError(Object object)
                {
                    dismissLoading();
                    super.onError(object);
                    if (null != object)
                    {
                        ToastHelper.showInfo(object.toString());
                    }
                }
                
                @Override
                public void onPreExecute()
                {
                    super.onPreExecute();
                    
                    showNewLoading(true, ToastHelper.toStr(R.string.public_data_loading), PayDistributionActivity.this);
                }
                
                public void onSucceed(List<OrderTotalModel> result)
                {
                    dismissLoading();
                    if (result != null && result.size() > 0)
                    {
                        tv_total_distribtion.setVisibility(View.GONE);
                        orderTotalModels.clear();
                        
                        orderTotalModels.addAll(result);
                        
                        distributionAdapter.notifyDataSetChanged();
                    }
                    else
                    {
                        tv_total_distribtion.setVisibility(View.VISIBLE);
                    }
                };
            });
    }
    
    Handler handler = new Handler()
    {
        
        public void handleMessage(android.os.Message msg)
        {
            switch (msg.what)
            {
                case HandlerManager.PAY_TOTAL_TYPE_DISTRIBUTION:
                    String type = (String)msg.obj;
                    if (type.equals("0"))
                    { //收银员统计
                        titleBar.setTitle(R.string.tx_user);
                        loadOrderTotal(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00",
                            DateUtil.formatTime(System.currentTimeMillis()),
                            "5",
                            null);
                        
                        //                        lay_input.setVisibility(View.VISIBLE);
                        titleBar.setRightButLayVisible(true, R.drawable.icon_search_white);
                        defaultPayType = true;
                    }
                    else
                    { // 支付类型统计
                        lay_input.setVisibility(View.GONE);
                        titleBar.setRightButLayVisible(false, 0);
                        defaultPayType = false;
                        titleBar.setTitle(R.string.tv_total_pay_type);
                        if (!defaultTimeNow)
                        { //30天
                            loadOrderTotal(DateTimeUtil.getMothdFirst() + " 00:00:00",
                                DateUtil.formatTime(DateUtil.getBeforeDate().getTime()),
                                "2",
                                null);
                        }
                        else
                        {//实时
                            loadOrderTotal(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00",
                                DateUtil.formatTime(System.currentTimeMillis()),
                                "3",
                                null);
                        }
                    }
                    break;
                case HandlerManager.PAY_TOTAL_TYPE_DISTRIBUTION_USER: //选择收银员
                    try
                    {
                        UserModel userModel = (UserModel)msg.obj;
                        if (!StringUtil.isEmptyOrNull(userModel.getRealname()))
                        {
                            if (defaultTimeNow)
                            { //实时
                                loadOrderTotal(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00",
                                    DateUtil.formatTime(System.currentTimeMillis()),
                                    "5",
                                    userModel.getRealname());
                            }
                            else
                            {//按天
                                loadOrderTotal(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00",
                                    DateUtil.formatTime(System.currentTimeMillis()),
                                    "5",
                                    userModel.getRealname());
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Log.e("hehui", "" + e);
                    }
                    break;
                default:
                    break;
            }
        };
    };
    
    private void initView()
    {
        v_now = getViewById(R.id.v_now);
        v_date = getViewById(R.id.v_date);
        tv_title_now = getViewById(R.id.tv_title_now);
        tv_title_date = getViewById(R.id.tv_title_date);
        ly_now = getViewById(R.id.ly_now);
        ly_date = getViewById(R.id.ly_date);
        tv_total_distribtion = getViewById(R.id.tv_total_distribtion);
        list = getViewById(R.id.list);
        refund_search_input = getViewById(R.id.refund_search_input);
        lay_input = getViewById(R.id.lay_input);
        refund_search_input.setFocusable(true);
        refund_search_input.setFocusableInTouchMode(true);
        refund_search_input.requestFocus();
        refund_search_input.setOnTouchListener(new OnTouchListener()
        {
            
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    Bundle b = new Bundle();
                    b.putBoolean("isFromOrderStreamSearch", true);
                    b.putInt("serchType", 4); //从收银员列表查询
                    b.putBoolean("defaultTimeNow", defaultTimeNow);
                    showPage(RefundManagerActivity.class, b);
                }
                return true;
            }
        });
    }
    
    @Override
    protected void setOnCentreTitleBarClickListener()
    {
        super.setOnCentreTitleBarClickListener();
        titleBar.setOnCentreTitleBarClickListener(new TitleBar.OnCentreTitleBarClickListener()
        {
            @Override
            public void onCentreButtonClick(View v)
            {
                List<City> list = new ArrayList<City>();
                City city = new City();
                city.setCity(getString(R.string.tx_user));
                city.setNumber("0");
                City city1 = new City();
                city1.setCity(getString(R.string.tv_total_pay_type));
                city1.setNumber("1");
                list.add(city);
                list.add(city1);
                HelpUI.initTotalPopWindow(PayDistributionActivity.this, list, 2, v, handler);
            }
            
            @Override
            public void onTitleRepotLeftClick()
            {
                // TODO Auto-generated method stub
                
            }
            
            @Override
            public void onTitleRepotRigthClick()
            {
                // TODO Auto-generated method stub
                
            }
        });
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setTitle(R.string.tx_user);
        titleBar.setTitleImage(true);
        titleBar.setLeftButtonVisible(true);
        titleBar.setLeftText(R.string.title_search);
        titleBar.setRightButLayVisible(true, 0);
        titleBar.setRightImageBackground(R.drawable.icon_search_white);
        //        titleBar.setRightButLayTextColor(false, getResources().getColor(R.color.get_captcha_color));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                
                Bundle b = new Bundle();
                b.putBoolean("isFromOrderStreamSearch", true);
                b.putInt("serchType", 4); //从收银员列表查询
                b.putBoolean("defaultTimeNow", defaultTimeNow);
                showPage(RefundManagerActivity.class, b);
                
                //                if (defaultTimeNow)
                //                {
                //                    defaultTimeNow = false;
                //                    titleBar.setRightButLayVisibleForTotal(true, getString(R.string.tv_total_title_thirty));
                //                    if (defaultPayType)
                //                    { //收银员
                //                        loadOrderTotal(DateTimeUtil.getMothdFirst() + " 00:00:00",
                //                            DateUtil.formatTime(DateUtil.getBeforeDate().getTime()),
                //                            "5",
                //                            null);
                //                    }
                //                    else
                //                    { //支付类型
                //                        loadOrderTotal(DateTimeUtil.getMothdFirst() + " 00:00:00",
                //                            DateUtil.formatTime(DateUtil.getBeforeDate().getTime()),
                //                            "2",
                //                            null);
                //                    }
                //                }
                //                else
                //                {
                //                    defaultTimeNow = true;
                //                    
                //                    titleBar.setRightButLayVisibleForTotal(true, getString(R.string.tv_total_title_now));
                //                    //                    lay_input.setVisibility(View.VISIBLE);
                //                    
                //                    if (defaultPayType)
                //                    { //收银员
                //                        loadOrderTotal(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00",
                //                            DateUtil.formatTime(System.currentTimeMillis()),
                //                            "5",
                //                            null);
                //                    }
                //                    else
                //                    { //支付类型
                //                        loadOrderTotal(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00",
                //                            DateUtil.formatTime(System.currentTimeMillis()),
                //                            "3",
                //                            null);
                //                    }
                //                    
                //                }
            }
        });
    }
    
    public class DistributionAdapter extends BaseAdapter
    {
        
        private List<OrderTotalModel> orderTotalModels;
        
        public DistributionAdapter(List<OrderTotalModel> orderTotalModels)
        {
            this.orderTotalModels = orderTotalModels;
        }
        
        @Override
        public int getCount()
        {
            return orderTotalModels.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            return orderTotalModels.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(PayDistributionActivity.this, R.layout.total_distribution_list_item, null);
                holder = new ViewHolder();
                holder.roundProgressBar1 = (RoundProgressBar)convertView.findViewById(R.id.roundProgressBar1);
                holder.roundProgressBar1.setRoundWidth(1.0f);
                holder.roundProgressBar1.setTextSize(DisplayUtil.dip2Px(PayDistributionActivity.this, 10));
                holder.roundProgressBar1.setCricleColor(getResources().getColor(R.color.total_item_text));
                holder.roundProgressBar1.setCricleProgressColor(getResources().getColor(R.color.order_title_blue));
                holder.roundProgressBar1.setTextColor(getResources().getColor(R.color.order_title_blue));
                holder.tv_user_name = (TextView)convertView.findViewById(R.id.tv_user_name);
                holder.tv_total_money = (TextView)convertView.findViewById(R.id.tv_total_money);
                holder.tv_num = (TextView)convertView.findViewById(R.id.tv_num);
                
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            //            
            OrderTotalModel orderTotal = orderTotalModels.get(position);
            if (orderTotal != null)
            {
                if (defaultPayType)
                { //默认是收银员
                
                    holder.tv_user_name.setText(orderTotal.getUserName());
                }
                else
                {
                    holder.tv_user_name.setText(orderTotal.getPayTypeName());
                }
                holder.tv_total_money.setText(MainApplication.feeFh
                    + DateUtil.formatMoneyUtils(orderTotal.getSuccessFee()));
                holder.tv_num.setText(orderTotal.getSuccessCount() + "");
                if (!StringUtil.isEmptyOrNull(orderTotal.getTotalScale()))
                {
                    holder.roundProgressBar1.setProgress(Float.parseFloat(orderTotal.getTotalScale()));
                }
            }
            
            return convertView;
        }
    }
    
    private class ViewHolder
    {
        private TextView tv_user_name, tv_total_money, tv_num;
        
        private RoundProgressBar roundProgressBar1;
        
    }
}
