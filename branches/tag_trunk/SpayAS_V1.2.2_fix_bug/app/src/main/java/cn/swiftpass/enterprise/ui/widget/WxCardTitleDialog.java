package cn.swiftpass.enterprise.ui.widget;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.WxCard;

/***
 * 退款检查对话框
 */
public class WxCardTitleDialog extends Dialog
{
    
    private ConfirmListener btnListener;
    
    private Activity mContext;
    
    private TextView tv_title, tv_time, tv_card_id, tv_card_detail;
    
    private Button btn_go_on, btn_tp_pay;
    
    public WxCardTitleDialog(Activity context, WxCard wxCard, String payType, ConfirmListener btnListener)
    {
        super(context);
        mContext = context;
        this.btnListener = btnListener;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.wxcard_success_dialog);
        //        setContentView(R.layout.activity_wx_card);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        this.setCanceledOnTouchOutside(false);
        initView();
    }
    
    private void initView()
    {
        tv_title = (TextView)findViewById(R.id.tv_title);
        tv_time = (TextView)findViewById(R.id.tv_time);
        tv_card_id = (TextView)findViewById(R.id.tv_card_id);
        tv_card_detail = (TextView)findViewById(R.id.tv_card_detail);
        
        btn_go_on = (Button)findViewById(R.id.btn_go_on);
        btn_tp_pay = (Button)findViewById(R.id.btn_tp_pay);
        
        btn_go_on.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                btnListener.ok();//继续扫
            }
        });
        
        btn_tp_pay.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                btnListener.cancel();//去支付
            }
        });
    }
    
    public interface ConfirmListener
    {
        public void cancel();
        
        public void ok();
    }
    
}
