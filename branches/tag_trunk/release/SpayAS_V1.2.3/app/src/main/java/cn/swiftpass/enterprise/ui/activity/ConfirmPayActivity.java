package cn.swiftpass.enterprise.ui.activity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.QRcodeInfo;
import cn.swiftpass.enterprise.bussiness.model.WxCard;
import cn.swiftpass.enterprise.ui.activity.scan.CaptureActivity;
import cn.swiftpass.enterprise.ui.activity.user.VerificationDetails;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.ProgressInfoDialog;
import cn.swiftpass.enterprise.ui.widget.WxCardDialog;
import cn.swiftpass.enterprise.utils.Arith;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.StringUtil;

public class ConfirmPayActivity extends TemplateActivity {
    private LinearLayout vard_lay;

    private TextView receivable_money, favourable_money, tx_vard;

    private EditText money;

    private Button confirm;

    private String payType, m;

    ProgressInfoDialog dialog1;

    private boolean isStop = true; // 强行中断交易

    private AlertDialog dialogInfo;

    private List<WxCard> vardList = new ArrayList<WxCard>();

    String favourableMoney;

    private TextView tv_discount;

    WxCard wxCard;

    private LinearLayout ly_card;

    @Override
    protected boolean isLoginRequired() {
        return false;
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.bt_confirm_pay);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_confirm);
        payType = getIntent().getStringExtra("payType");
        //        if (null != payType && !"".equals(payType))
        //        {
        //            if (payType.equals(MainApplication.PAY_TYPE_NITINE))
        //            {
        //                titleBar.setTitle("二维码收款");
        //            }
        //        }
        //        m = getIntent().getStringExtra("money");

        vardList = ((List<WxCard>) getIntent().getSerializableExtra("vardOrders"));
        initView();
        showSoftInputFromWindow(ConfirmPayActivity.this, money);
        setLinster();
    }

    private void setLinster() {
        ly_card.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //                WxCardActivity.startActivity(ConfirmPayActivity.this, vardList);
                if (vardList.size() > 0) {
                    OrderManager.getInstance().queryVardDetail(vardList.get(0).getCardId(),
                            vardList.get(0).getCardCode(),
                            new UINotifyListener<WxCard>() {

                                @Override
                                public void onError(Object object) {
                                    super.onError(object);
                                    dismissLoading();
                                    if (object != null) {
                                        //                                showToastInfo(object.toString());
                                        toastDialog(ConfirmPayActivity.this, object.toString(), null);
                                    }
                                }

                                @Override
                                public void onPreExecute() {
                                    super.onPreExecute();

                                    loadDialog(ConfirmPayActivity.this, R.string.public_data_loading);
                                }

                                @Override
                                public void onSucceed(WxCard result) {
                                    super.onSucceed(result);
                                    dismissLoading();
                                    if (result != null) {
                                        VerificationDetails.startActivity(ConfirmPayActivity.this, result);
                                    }
                                }

                            });
                }
            }
        });
    }

    private void setPricePoint(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                double d;
                if (s.toString().contains(".")) {
                    if (s.length() - 1 - s.toString().indexOf(".") > 2) {
                        s = s.toString().subSequence(0, s.toString().indexOf(".") + 3);
                        editText.setText(s);
                        editText.setSelection(s.length());
                    }
                }
                if (s.toString().trim().substring(0).equals(".")) {
                    s = "0" + s;
                    editText.setText(s);
                    editText.setSelection(2);
                }

                if (s.toString().startsWith("0") && s.toString().trim().length() > 1) {
                    if (!s.toString().substring(1, 2).equals(".")) {
                        s = s.subSequence(0, 1);
                        editText.setText(s);
                        editText.setSelection(1);
                        return;
                    }
                }

                if (!s.toString().startsWith(".")) {
                    if (s.toString().contains(".") && s.toString().substring(s.toString().lastIndexOf('.')).equals(".")) {
                        return;
                    } else {
                        if (null != s.toString() && !"".equals(s.toString())) {
                            d = Double.parseDouble(s.toString());

                            if (wxCard != null && wxCard.getCardType().equalsIgnoreCase("cash") && d > 0) {//代金券，减少金额
                                //                                if (d < (wxCard.getReduceCost() / 100))
                                //                                {
                                //                                    toastDialog(ConfirmPayActivity.this, R.string.tv_card_sale_toal_than, null);
                                //                                    money.setFocusable(true);
                                //                                    return;
                                //                                }
                                long favmoney =
                                        Long.parseLong(OrderManager.getInstance().getMoney(s.toString()))
                                                - wxCard.getReduceCost();
                                //                                if (favmoney > 0)
                                //                                {
                                favourable_money.setText(DateUtil.formatMoneyUtils(favmoney));
                                confirm.setText(DateUtil.formatMoneyUtils(favmoney) + " "
                                        + getString(R.string.bt_confirm_pay));
                                //                                }
                                //                                else if (favmoney < 0)
                                //                                {
                                //                                    
                                //                                }
                            } else if (wxCard.getCardType().equalsIgnoreCase("discount") && d > 0) {
                                //折扣券
                                BigDecimal a = new BigDecimal((100 - wxCard.getDiscount()));
                                BigDecimal b = new BigDecimal(100);
                                double discount = a.divide(b, 2, RoundingMode.HALF_UP).doubleValue();
                                Double res = Arith.mul(d, discount);

                                tv_discount.setText(getString(R.string.tx_fav_money) + " "
                                        + DateUtil.formatMoneyUtil(Arith.sub(d, res)));
                                confirm.setText(DateUtil.formatMoneyUtil(res) + " "
                                        + getString(R.string.bt_confirm_pay));
                                favourable_money.setText(DateUtil.formatMoneyUtil(res));

                            }
                            //                            if ((d * 100) >= Double.parseDouble(m))
                            //                            {
                            //                                showToastInfo(R.string.et_fav_money);
                            //                            }
                            //                            else
                            //                            {
                            //                                favourableMoney = DateUtil.formatMoneyUtils((Double.parseDouble(m)) - (d * 100));
                            //                                favourable_money.setText(MainApplication.feeFh + favourableMoney);
                            //                                
                            //                            }
                        } else {
                            //                            favourable_money.setText(MainApplication.feeFh
                            //                                + DateUtil.formatMoneyUtils((Double.parseDouble(m))));
                        }
                    }

                    int len = s.toString().trim().length();

                    if (len > 5) {
                        if (!s.toString().trim().contains(".")) {
                            s = s.toString().substring(0, len - 1);
                            editText.setText(s);
                            editText.setSelection(s.length());
                        }

                    }

                    if (len > 0) {
                        setButtonBg(confirm, true, 0);
                    } else {
                        setButtonBg(confirm, false, 0);
                    }

                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });

    }

    private void initView() {
        ly_card = getViewById(R.id.ly_card);
        tv_discount = getViewById(R.id.tv_discount);
        vard_lay = getViewById(R.id.vard_lay);
        receivable_money = getViewById(R.id.receivable_money);
        favourable_money = getViewById(R.id.favourable_money);
        money = getViewById(R.id.money);
        confirm = getViewById(R.id.confirm);
        tx_vard = getViewById(R.id.tx_vard);
        //        receivable_money.setText("¥ " + DateUtil.formatMoneyUtil(Long.parseLong(m) / 100d));
        if (vardList.size() > 0) {
            tx_vard.setText(getString(R.string.bt_card_total) + vardList.size() + getString(R.string.tx_use_card_lens));
            wxCard = vardList.get(0);
            if (wxCard != null && wxCard.getCardType().equalsIgnoreCase("cash")) {//代金券，减少金额
                tv_discount.setText(getString(R.string.tx_fav_money) + " "
                        + DateUtil.formatMoneyUtils(wxCard.getReduceCost()));
            }

        } else {
            vard_lay.setVisibility(View.GONE);
        }
        money.setFocusable(true);
        money.setFocusableInTouchMode(true);
        money.requestFocus();
        InputMethodManager inputManager =
                (InputMethodManager) money.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.showSoftInput(money, 0);

        setPricePoint(money);

        confirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String money_str = money.getText().toString();
                if (null == money_str || "".equals(money_str)) {
                    toastDialog(ConfirmPayActivity.this, R.string.tx_discount_money_notnull, null);
                    money.setFocusable(true);
                    return;
                }
                if (money_str.contains(".") && money_str.substring(money_str.lastIndexOf('.')).equals(".")) {
                    toastDialog(ConfirmPayActivity.this, R.string.tx_discount_money_format, null);
                    money.setFocusable(true);
                    return;
                } else {
                    if (!money_str.contains(".") && money_str.startsWith("0")) {
                        toastDialog(ConfirmPayActivity.this, R.string.tx_discount_money_format, null);
                        money.setFocusable(true);
                        return;
                    }
                }

                if (wxCard != null && wxCard.getCardType().equalsIgnoreCase("cash")) {//代金券，减少金额
                    if (Double.parseDouble(money_str) < (wxCard.getReduceCost() / 100)) {
                        toastDialog(ConfirmPayActivity.this, R.string.tv_card_sale_toal_than, null);
                        money.setFocusable(true);
                        return;
                    }
                }

                //                long money;
                //                money = (long)(Double.parseDouble(favourableMoney) * 100);
                //                if (null != payType && payType.equals(MainApplication.PAY_TYPE_WRITE_OFF))
                //                { // 反扫
                //                

                if (!StringUtil.isEmptyOrNull(favourable_money.getText().toString())) {
                    String favourable = favourable_money.getText().toString();
                    if (favourable.equals("0") || favourable.equals("0.00")) {
                        toastDialog(ConfirmPayActivity.this, R.string.tv_pay_money_than, null);
                        return;
                    }
                    //                    else
                    //                    {
                    //                        
                    //                        double m = Double.parseDouble(favourable_money.getText().toString());
                    //                        if (m < 0)
                    //                        {
                    //                            toastDialog(ConfirmPayActivity.this, R.string.tv_pay_money_than, null);
                    //                            return;
                    //                        }
                    //                    }

                }

                if (Double.parseDouble(money_str) > 50000) {
                    toastDialog(ConfirmPayActivity.this, R.string.tv_pay_exceed_money, null);
                    money.setFocusable(true);
                    return;
                }

                Intent it = new Intent();
                it.setClass(ConfirmPayActivity.this, CaptureActivity.class);
                long adM = Long.parseLong(OrderManager.getInstance().getMoney(money_str));

                if (wxCard != null && wxCard.getCardType().equalsIgnoreCase("cash")) {//代金券，减少金额
                    it.putExtra("discountAmount", wxCard.getReduceCost());//优惠金额
                    String reduceCostMoney = String.valueOf(Arith.sub(adM, wxCard.getReduceCost()));
                    it.putExtra("money", reduceCostMoney);
                } else if (wxCard.getCardType().equalsIgnoreCase("discount")) {
                    //折扣券
                    BigDecimal a = new BigDecimal((100 - wxCard.getDiscount()));
                    BigDecimal b = new BigDecimal(100);
                    double discount = a.divide(b, 2, RoundingMode.HALF_UP).doubleValue();
                    Double res = Arith.mul(Double.parseDouble(money_str), discount);
                    it.putExtra("money", res * 100 + "");
                    double disMoney = Arith.sub(Double.parseDouble(money_str), res);
                    long discountMoney = Long.parseLong(OrderManager.getInstance().getMoney(disMoney + ""));// (long)(disMoney * 100);
                    it.putExtra("discountAmount", discountMoney);//优惠金额
                }
                it.putExtra("payType", MainApplication.PAY_TYPE_MICROPAY_VCARD);
                it.putExtra("vardOrders", (Serializable) vardList);
                startActivity(it);

                MainApplication.listActivities.add(ConfirmPayActivity.this);
                for (Activity a : MainApplication.listActivities) {
                    a.finish();
                }
                finish();
                //                }
                //                else
                //                {//生成二维码
                //                    toNative(String.valueOf(money));
                //                }

            }
        });

        vard_lay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (vardList.size() > 0) {
                    WxCardDialog wxCardDialgo =
                            new WxCardDialog(ConfirmPayActivity.this, vardList, new WxCardDialog.ConfirmListener() {

                                @Override
                                public void cancel() {

                                }
                            });
                    DialogHelper.resize(ConfirmPayActivity.this, wxCardDialgo);
                    wxCardDialgo.show();
                }
            }
        });

    }

    private void showConfirm(String msg, final ProgressInfoDialog dialogs) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ConfirmPayActivity.this);
        builder.setTitle(R.string.public_cozy_prompt);
        builder.setMessage(msg);
        builder.setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isStop = false;
                if (dialogs != null) {
                    dialogs.dismiss();
                }
            }
        });

        builder.setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialogInfo = builder.show();
    }

    private void toNative(String strTotalMoney) {
        String msg = getString(R.string.spanning_pay_wechat_code);
        if (payType.equals(MainApplication.PAY_QQ_NATIVE)) {
            msg = getString(R.string.spanning_pay_qq_code);
        } else if (payType.startsWith(MainApplication.PAY_ZFB_NATIVE)) {
            msg = getString(R.string.spanning_pay_zfb_code);
        }

        dialog1 = new ProgressInfoDialog(ConfirmPayActivity.this, msg, new ProgressInfoDialog.HandleBtn() {

            @Override
            public void handleOkBtn() {
                showConfirm(getStringById(R.string.interrupt_trading), dialog1);
            }

        });

        long adM = (long) ((Double.parseDouble(money.getText().toString()) * 100));

        OrderManager.getInstance().unifiedNativePay(strTotalMoney,
                payType,
                adM,
                vardList,
                null,
                new UINotifyListener<QRcodeInfo>() {
                    @Override
                    public void onPreExecute() {
                        super.onPreExecute();
                        DialogHelper.resize(ConfirmPayActivity.this, dialog1);
                        if (!dialog1.isShowing()) {
                            dialog1.show();
                        }
                    }

                    @Override
                    public void onPostExecute() {
                        super.onPostExecute();
                    }

                    @Override
                    public void onError(final Object object) {
                        super.onError(object);
                        dialog1.dismiss();
                        if (dialogInfo != null) {
                            dialogInfo.dismiss();
                        }
                        if (checkSession()) {
                            return;
                        }
                        if (!isStop) {
                            return;
                        }
                        if (object != null) {
                            ConfirmPayActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    DialogInfo dialogInfo =
                                            new DialogInfo(ConfirmPayActivity.this, getStringById(R.string.public_cozy_prompt),
                                                    object.toString(), getStringById(R.string.btnOk), DialogInfo.REGISTFLAG, null,
                                                    null);
                                    DialogHelper.resize(ConfirmPayActivity.this, dialogInfo);
                                    dialogInfo.show();
                                }
                            });
                        }

                    }

                    @Override
                    public void onSucceed(QRcodeInfo result) {
                        super.onSucceed(result);
                        dialog1.dismiss();
                        if (dialogInfo != null) {
                            dialogInfo.dismiss();
                        }
                        if (result != null && isStop) {
                            result.setPayType(payType);
                            long adM = (long) ((Double.parseDouble(money.getText().toString()) * 100));
                            result.setDiscountAmount(adM); //优惠金额
                            result.setIswxCard(true);
                            ShowQRcodeActivity.startActivity(result, ConfirmPayActivity.this);
                            MainApplication.listActivities.add(ConfirmPayActivity.this);
                            ConfirmPayActivity.this.finish();
                        } else {
                            isStop = true;
                        }
                    }
                });
    }

    public static void startActivity(Context context, String payType, String money, List<WxCard> vardOrders) {
        Intent it = new Intent();
        it.setClass(context, ConfirmPayActivity.class);
        it.putExtra("payType", payType);
        it.putExtra("money", money);
        it.putExtra("vardOrders", (Serializable) vardOrders);
        context.startActivity(it);
    }

    public static void startActivity(Context context, List<WxCard> vardOrders) {
        Intent it = new Intent();
        it.setClass(context, ConfirmPayActivity.class);
        it.putExtra("vardOrders", (Serializable) vardOrders);
        context.startActivity(it);
    }
}
