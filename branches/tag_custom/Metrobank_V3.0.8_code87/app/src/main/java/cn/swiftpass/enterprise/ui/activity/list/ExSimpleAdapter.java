package cn.swiftpass.enterprise.ui.activity.list;
 
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Checkable;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Map;
 
public class ExSimpleAdapter extends SimpleAdapter {

    private static final String TAG = ExSimpleAdapter.class.getSimpleName();
    private int[] mTo;
    private String[] mFrom;
    private ViewBinder mViewBinder;
    private List<? extends Map<String, ?>> mData;
    private int mResource;
    private LayoutInflater mInflater;
    public ExSimpleAdapter(Context context,
            List<? extends Map<String, ?>> data, int resource, String[] from,
            int[] to) {
        super(context, data, resource, from, to);
        mData = data;
        mResource = resource;
        mFrom = from;
        mTo = to;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
     /**
     * @see android.widget.Adapter#getView(int, View, ViewGroup)
     */
     @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return createViewFromResource(position, convertView, parent, mResource);
    }
    private View createViewFromResource(int position, View convertView,
            ViewGroup parent, int resource) {
        View v;
        if (convertView == null) {
            v = mInflater.inflate(resource, parent, false);
 
            final int[] to = mTo;
            final int count = to.length;
            final View[] holder = new View[count];
 
            for (int i = 0; i < count; i++) {
                holder[i] = v.findViewById(to[i]);
            }
 
            v.setTag(holder);
        } else {
            v = convertView;
        }
        bindView(position, v);
 
        return v;
    }
     
    private void bindView(int position, View view) {
        final Map<?, ?> dataSet = mData.get(position);
        if (dataSet == null) {
            return;
        }
        final ViewBinder binder = mViewBinder;
        final View[] holder = (View[]) view.getTag();
        final String[] from = mFrom;
        final int[] to = mTo;
        final int count = to.length;
 
        for (int i = 0; i < count; i++) {
            final View v = holder[i]; 
            if (v != null) {
                final Object data = dataSet.get(from[i]);
                String text = data == null ? "" : data.toString();
                if (text == null) {
                    text = "";
                }
                boolean bound = false;
                if (binder != null) {
                    bound = binder.setViewValue(v, data, text);
                }
                if (!bound) {
                    if (v instanceof Checkable) {
                        if (data instanceof Boolean) {
                            ((Checkable) v).setChecked((Boolean) data);
                        } else {
                            String e = v.getClass().getName() + " should be bound to a Boolean, not a ";
                            if (null != data) {
                                e += data.getClass();
                            } else {
                                e += "null";
                            }
                            throw new IllegalStateException(e);
                        }
                    }else if(v instanceof RatingBar){
                        if(null != data) {
                            try {
                                float score = Float.parseFloat(data.toString());  //备注2
                                ((RatingBar) v).setRating(score);
                            } catch (Exception e) {
                                Log.e(TAG, Log.getStackTraceString(e));
                            }
                        }
                    }else if (v instanceof TextView) {
                        // Note: keep the instanceof TextView check at the bottom of these
                        // ifs since a lot of views are TextViews (e.g. CheckBoxes).
                        //setViewText((TextView) v, text);
                        if (TextUtils.equals(from[i],"title")) {
                            if (position == 0) {
                                ((TextView) v).setText(text);
                                v.setVisibility(View.VISIBLE);
                            } else if (!mData.get(position).get("title").equals(mData.get(position - 1).get("title"))) {
                                ((TextView) v).setText(text);
                                v.setVisibility(View.VISIBLE);
                            } else {
                                ((TextView) v).setText(text);
                                v.setVisibility(View.GONE);
                            }
                        } else {
                            ((TextView) v).setText(text);
                        }

                    } else if (v instanceof ImageView) {                   
                        if (data instanceof Integer) {
                            setViewImage((ImageView) v, (Integer) data);                           
                        }
                        else if(data instanceof byte[]) {      //备注1
                                Bitmap bmp;
                                byte[] image = (byte[])data;
                                if(image.length!=0){
                                bmp = BitmapFactory.decodeByteArray(image, 0, image.length);
                                ((ImageView) v).setImageBitmap(bmp);
                            }
                        }
                        else if(data instanceof String) {      //图片url
                        	
                        }
//                        else if(data instanceof Float) {      //打折float
//                        	if((Float) data!=0.0)
//                        		setViewImage((ImageView) v, R.drawable.discount_5);
//                        	else
//                        		setViewImage((ImageView) v, R.drawable.transparent);
//                        }
                        else { 
                        	v.setVisibility(View.GONE);
//                        	throw new IllegalStateException(v.getClass().getName() + " is not a " +
//                                " view that can be bounds by this SimpleAdapter");
                    }
                }
            }
        }
    }
  }

//    @Override
//    public boolean equals(Object obj) {
//        return super.equals(obj);
//    }
}