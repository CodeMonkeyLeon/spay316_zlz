/*
 * 文 件 名:  NotifyManageActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-9-12
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.notify;

import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.NoticeModel;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 公告管理
 * 
 * @author  he_hui
 * @version  [版本号, 2016-9-12]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class NotifyManageActivity extends TemplateActivity
{
    private ListView notify_manager;
    
    private ViewHolder holder;
    
    private List<NoticeModel> listNoticeModels;
    
    private TextView tv_notify;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.notify_manager);
        
        notify_manager = getViewById(R.id.notify_manager);
        tv_notify = getViewById(R.id.tv_notify);
        loadDate();
        
        notify_manager.setOnItemClickListener(new OnItemClickListener()
        {
            
            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3)
            {
                NoticeModel model = listNoticeModels.get(position);
                if (null != model)
                {
                    //                    int noticeSize = PreferenceUtil.getInt("noticeSize", 0);
                    //                    PreferenceUtil.commitInt("noticeSize", noticeSize - 1);
                    //                    int noticelen = PreferenceUtil.getInt("noticeSize", 0);
                    //                    if (noticelen == 0)
                    //                    {
                    //                        HandlerManager.notifyMessage(HandlerManager.NOTICE_TYPE, HandlerManager.NOTICE_TYPE);
                    //                    }
                    NoticeWebViewActivity.startActivity(NotifyManageActivity.this,
                        model.getNoticeContentUrl(),
                        model.getTitle());
                    updatePointState(model.getNoticeId() + "");
                    ImageView image = (ImageView)v.findViewById(R.id.iv_point);
                    image.setVisibility(View.GONE);
                    
                }
            }
            
        });
    }
    
    /**
     * 修改公开状态
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void updatePointState(String noticeId)
    {
        String noticePre = PreferenceUtil.getString("noticePre", "");
        if (!StringUtil.isEmptyOrNull(noticePre) && !StringUtil.isEmptyOrNull(noticeId))
        {
            PreferenceUtil.removeKey("noticePre");
            Map<String, String> mapNotice = JsonUtil.jsonToMap(noticePre);
            for (Map.Entry<String, String> entry : mapNotice.entrySet())
            {
                String value = entry.getKey();
                if (noticeId.equals(value))
                { //代表还有公开没有被打开查看详情过
                    mapNotice.put(value, "1");
                }
            }
            HandlerManager.notifyMessage(HandlerManager.NOTICE_TYPE, HandlerManager.NOTICE_TYPE);
            PreferenceUtil.commitString("noticePre", JsonUtil.mapToJson(mapNotice));
            Log.i("hehui", "updatePointState (mapNotice)-->" + JsonUtil.mapToJson(mapNotice));
        }
        
    }
    
    private void loadDate()
    {
        LocalAccountManager.getInstance().getNotice("2", new UINotifyListener<List<NoticeModel>>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                showNewLoading(true, getString(R.string.public_loading));
            }
            
            @Override
            public void onPostExecute()
            {
                super.onPostExecute();
            }
            
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                dismissLoading();
                if (object != null)
                    showToastInfo(object.toString());
                tv_notify.setVisibility(View.VISIBLE);
            }
            
            @Override
            public void onSucceed(List<NoticeModel> list)
            {
                dismissLoading();
                if (null != list && list.size() > 0)
                {
                    listNoticeModels = list;
                    PreferenceUtil.commitInt("noticeSize", list.size());
                    notify_manager.setAdapter(new NotifyAdapter(list));
                }
                else
                {
                    tv_notify.setVisibility(View.VISIBLE);
                }
            }
        });
    }
    
    @Override
    protected void setupBottomBar()
    {
        super.setupBottomBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tv_message_title);
    }
    
    private class NotifyAdapter extends BaseAdapter
    {
        
        private List<NoticeModel> deviceList;
        
        private NotifyAdapter(List<NoticeModel> deviceList)
        {
            this.deviceList = deviceList;
        }
        
        @Override
        public int getCount()
        {
            return deviceList.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            return deviceList.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(NotifyManageActivity.this, R.layout.notify_manager_item, null);
                holder = new ViewHolder();
                holder.tv_notify_title = (TextView)convertView.findViewById(R.id.tv_notify_title);
                holder.iv_point = (ImageView)convertView.findViewById(R.id.iv_point);
                holder.tv_notify_time = (TextView)convertView.findViewById(R.id.tv_notify_time);
                
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            
            NoticeModel devString = deviceList.get(position);
            if (!StringUtil.isEmptyOrNull(devString.getTitle()))
            {
                holder.tv_notify_title.setText(devString.getTitle());
            }
            holder.tv_notify_time.setText(devString.getCreateTime());
            isShowPoint(holder.iv_point, devString.getNoticeId() + "");
            
            return convertView;
        }
    }
    
    /**
     * 公告小圆点
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void isShowPoint(ImageView iv, String id)
    {
        String noticePre = PreferenceUtil.getString("noticePre", "");
        if (!StringUtil.isEmptyOrNull(noticePre))
        {
            Map<String, String> mapNotice = JsonUtil.jsonToMap(noticePre);
            for (Map.Entry<String, String> entry : mapNotice.entrySet())
            {
                String value = entry.getValue();
                String key = entry.getKey();
                if (key.equals(id) && value.equals("1"))
                { //代表还有公开没有被打开查看详情过
                  //                    iv.setVisibility(View.VISIBLE);
                    iv.setVisibility(View.GONE);
                }
            }
        }
        
    }
    
    private class ViewHolder
    {
        private TextView tv_notify_title, tv_notify_time;
        
        private ImageView iv_point;
        
    }
    
}
