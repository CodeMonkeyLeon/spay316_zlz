package cn.swiftpass.enterprise.io.database;

import android.content.Context;

/**
 * 开关数据库 <一句话功能简述> <功能详细描述>
 * 
 * @author he_hui
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class BaseDao
{
    public static DicDatabaseHelper dbHelper = null;
    
    public final static byte[] _writeLock = new byte[0];
    
    public final static byte[] _singleLock = new byte[0];
    
    public BaseDao(Context context)
    {
        synchronized (_singleLock)
        {
            if (null == dbHelper)
            {
                dbHelper = new DicDatabaseHelper(context);
            }
            openDb();
        }
    }
    
    /**
     * 打开数据库
     */
    public void openDb()
    {
        synchronized (_writeLock)
        {
            try
            {
                dbHelper.getReadableDatabase();
            }
            catch (Exception e)
            {
                
            }
            
            try
            {
                dbHelper.getWritableDatabase();
            }
            catch (Exception e)
            {
                
            }
        }
    }
    
    /**
     * 关闭数据库
     */
    public void closeDb()
    {
        synchronized (_writeLock)
        {
            dbHelper.close();
        }
    }
}
