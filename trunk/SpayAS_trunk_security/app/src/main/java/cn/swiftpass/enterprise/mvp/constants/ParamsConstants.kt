package cn.swiftpass.enterprise.mvp.constants

/**
 * @author lizheng.zhao
 * @date 2022/11/16
 *
 * @你应该是一场梦我应该是一阵风
 */
object ParamsConstants {

    /**
     * 注解所需参数
     */
    const val JSON_STR_PARAM = "json_str_param"


    /**
     * 接口参数
     */
    const val EMAIL_CONTENT = "emailContent"

    const val EMAIL_SUBJECT = "emailSubject"

    const val GOODS_PRICE = "goodsPrice"

    const val GOODS_DESC = "goodsDesc"

    const val GOODS_PIC = "goodsPic"

    const val COMMODITY_NUM = "commodityNum"

    const val COMMODITY_NAME = "commodityName"

    const val EMAIL_CODE = "emailCode"

    const val IS_ENCRYPT_PWD = "isEncryptPwd"

    const val IS_TOTAL_AUTH = "isTotalAuth"

    const val ENABLED = "enabled"

    const val IS_UNFREEZE_AUTH = "isUnfreezeAuth"

    const val IS_ORDER_AUTH = "isOrderAuth"

    const val IS_REFUND_AUTH = "isRefundAuth"

    const val REFUND_LIMIT = "refundLimit"

    const val REAL_NAME = "realname"

    const val MOBILE = "mobile"

    const val REMARK = "remark"

    const val TOKEN = "token"

    const val TIPS_LIST = "tipsList"

    const val DEFAULT_TIP = "defaultTip"

    const val TIPS_RATE = "tipsRate"

    const val CARD_CODE = "cardCode"

    const val CARD_ID = "cardId"

    const val REFUND_MONEY = "refundMoney"

    const val CARD_TIME = "cardTime"

    const val OUT_REQUEST_NO = "outRequestNo"

    const val DEVICE_NAME = "deviceName"

    const val CLIENT_TYPE = "clientType"

    const val CONTENT = "content"

    const val EMAIL = "email"

    const val EMAIL_VERIFY_CODE = "emailVerifyCode"

    const val IS_REFUND_STREAM = "isRefundStream"

    const val ADD_TIME = "addTime"

    const val TRADE_STATE = "tradeState"

    const val ID = "id"

    const val CASHIER_DESK = "cashierDesk"

    const val QR_CODE_ID = "qrcodeId"

    const val AUTH_CODE = "authCode"

    const val OUT_AUTH_NO = "outAuthNo"

    const val PAY_TYPE_MAP = "payTypeMap"

    const val PAY_TYPE_MAP_PRE_AUTH = "payTypeMap_pre_auth"

    const val TRA_TYPE = "traType"

    const val API_CODE = "apiCode"

    const val U_ID = "uId"

    const val CHOOSE_PAY_OR_PRE_AUTH = "choose_pay_or_pre_auth"

    const val DA_MONEY = "daMoney"

    const val ATTACH = "attach"

    const val DEVICE_INFO = "deviceInfo"

    const val BODY = "body"

    const val API_PROVIDER = "apiProvider"

    const val CARD_PAYMENT_PAY_TYPE_MAP = "mCardPaymentPayTypeMap"

    const val CASHIER_DESK_NAME = "CashierDeskName"

    const val SEARCH_CONTENT = "searchContent"

    const val INVOICE_ID = "invoiceId"

    const val MONEY = "money"

    const val NEW_PWD_FLAG = "newPwdFlag"

    const val REP_PASSWORD = "repPassword"

    const val OLD_PASSWORD = "oldPassword"

    const val TELEPHONE = "telephone"

    const val SERVICE = "service"

    const val PUSH_CID = "pushCid"

    const val CODE = "code"

    const val ANDROID_TRANS_PUSH = "androidTransPush"


    const val S_KEY = "skey"

    const val CLIENT = "client"

    const val PASSWORD = "password"

    const val USER_NAME = "username"

    const val USER_NAME_N = "userName"

    const val PUBLIC_KEY = "publicKey"


    const val OUT_REFUND_NO = "outRefundNo"

    const val MCH_ID = "mchId"

    const val USER_ID = "userId"

    const val SPAY_RS = "spayRs"

    const val NNS = "nns"

    const val OUT_TRADE_NO = "outTradeNo"

    const val ORDER_NO_MCH = "orderNoMch"

    const val SIGN = "sign"


    const val START_DATE = "startDate"

    const val END_DATE = "endDate"

    const val PAGE_SIZE = "pageSize"

    const val PAGE = "page"


    const val TRADE_TYPE_LIST = "tradeTypeList"

    const val API_PROVIDER_LIST = "apiProviderList"

    const val TRADE_STATE_LIST = "tradeStateList"


    const val OPERATION_TYPE = "operationType"

    const val AUTH_NO = "authNo"

    const val PRE_AUTH = "pre_auth"

    const val APP_PRIVATE_KEY = "priKey"

    const val APP_PUBLIC_KEY = "pubKey"

    const val SECRET_KEY = "secretKey"

    const val BILL_LIST_CHOOSE_PAY_OR_PRE_AUTH = "bill_list_choose_pay_or_pre_auth"

    const val VER_CODE = "verCode"

    const val BANK_CODE = "bankCode"


    const val ERROR_CODE_400 = "400"

    const val ERROR_CODE_402 = "402"


    const val PHONE = "phone"


    const val BIND_USER_ID = "bindUserId"

    const val FREE_QUERY_TIME = "freeQueryTime"


    const val START_TIME = "startTime"

    const val END_TIME = "endTime"

    const val COUNT_METHOD = "countMethod"

    const val COUNT_DAY_KIND = "countDayKind"


    const val REPORT_TYPE = "reportType"


    const val IMG_TYPE = "imgType"

    const val IMG_BASE_64 = "imgBase64"


    const val ACTION_TYPE = "actionType"

    const val EFFECTIVE_DATE = "effectiveDate"

    const val TOTAL_FEE = "totalFee"

    const val ORDER_REMARK = "orderRemark"

    const val ORDER_STATUS = "orderStatus"

    const val CUST_ID = "custId"

    const val PIC_URL = "picUrl"

    const val ORDER_COST_LIST = "orderCostList"

    const val ORDER_RELATE_GOODS_LIST = "orderRelateGoodsList"

    const val ORDER_NO = "orderNo"

    const val CUST_NAME = "custName"

    const val CUST_MOBILE = "custMobile"

    const val CUST_EMAIL = "custEmail"

    const val GOODS_ID = "goodsId"

    const val REAL_PLAT_ORDER_NO = "realPlatOrderNo"

    const val PAGE_NUMBER = "pageNumber"

    const val GOODS_NAME = "goodsName"

    const val GOODS_CODE = "goodsCode"


    /**
     * loading 类型
     * ALL_LOADING   所有loading  ---  比如页面销毁时在onDestroy()中调用dismissLoading()传入该值
     * COMMON_LOADING  通用loading
     */
    const val ALL_LOADING = -1
    const val COMMON_LOADING = 0
    const val PAY_ACTIVITY_PAY_REVERSE_LOADING = 1

    //    const val SEARCH_ACTIVITY_LOADING = 2
    const val ORDER_DETAILS_ACTIVITY_UNIFIED_PAY_REVERSE_LOADING = 3
    const val ORDER_STREAM_ACTIVITY_QUERY_ORDER_DATA = 4
}