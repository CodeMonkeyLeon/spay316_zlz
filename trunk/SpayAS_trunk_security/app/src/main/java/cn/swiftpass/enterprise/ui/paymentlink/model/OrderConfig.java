package cn.swiftpass.enterprise.ui.paymentlink.model;

import java.io.Serializable;

import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.EMAIL_NAME;

/**
 * Created by congwei.li on 2021/9/27.
 *
 * @Description:
 */
public class OrderConfig implements Serializable {
    public String merchantName;
    public String merchantDomain;
    public String merchantLogo;
    public String orderExpireTime;
    public String expireTimeUnit;
    public String emailTitle;
    public String emailContent;

    public String getDisplayTitle(String customerName) {
        return getReplaceString(emailTitle, customerName);
    }

    public String getDisplayContent(String customerName) {
        return getReplaceString(emailContent, customerName);
    }

    public String getReplaceString(String oldStr, String customerName) {
        StringBuilder title = new StringBuilder();
        if (oldStr.contains(EMAIL_NAME)) {
            title.append(oldStr.replace(EMAIL_NAME, customerName));
        } else {
            title.append(oldStr);
        }
        return title.toString();
    }
}
