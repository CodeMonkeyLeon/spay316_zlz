package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/11/18
 *
 * @小巷又弯又长
 * @没有门没有窗
 * @我拿把旧钥匙
 * @敲着厚厚的墙
 */
class PayContract {

    interface View : BaseView {
        fun payReverseSuccess(response: Order)

        fun payReverseFailed(error: Any?)

        fun getVersionCodeSuccess(response: UpgradeInfo?)

        fun getVersionCodeFailed(error: Any?)

        fun getExchangeRateSuccess(response: Boolean)

        fun getExchangeRateFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {


        fun getExchangeRate()

        fun getVersionCode()

        /**
         * 冲正接口
         */
        fun payReverse(
            orderNo: String?,
            payType: String?
        )
    }

}