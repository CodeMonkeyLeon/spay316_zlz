package cn.swiftpass.enterprise.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by admin on 2018/5/29.
 */

public class QRCodeBean implements Serializable{
    public int Id;
    public String qrCodeId="";
    public String qrCodeUrl="";
    public String qrBatchId="";
    /**
     *  二维码类型 0:聚合码;1:支付宝专属码;20:国际银联二维码
     */
    public int qrType;

    public String qrLogo="";
    public int bgColor;

    /**
     *  二维码logo，多个时逗号隔开，1：微信，2：支付宝 20：银联国际二维码
     */
    public String payLogos="";

    public String acceptOrgId="";
    public String channelId="";
    public String channelName="";
    public String mchId="";
    public String merchantName="";
    public String createTime="";
    public String bindTime="";
    public int bindStatus;
    public String cashierDesk="";
    public String unionEmv = "";
    /**
     * 增加跳转标志
     * 1.表示从二维码列表跳入固定二维码
     * 2.表示从入口处直接跳入
     */
    public int switch_type;
}
