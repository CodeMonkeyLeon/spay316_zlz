package cn.swiftpass.enterprise.utils

object ButtonUtils {

    private var lastClickTime = 0L
    private var lastButtonId = -1

    fun isFastDoubleClick(buttonId: Int, diff: Long = 1000L): Boolean {
        val time = System.currentTimeMillis()
        val timeD = time - lastClickTime
        if (timeD < 0) {
            return false
        }
        if (lastButtonId == buttonId && lastClickTime > 0 && timeD < diff) {
            return true
        }
        lastClickTime = time
        lastButtonId = buttonId
        return false
    }

}