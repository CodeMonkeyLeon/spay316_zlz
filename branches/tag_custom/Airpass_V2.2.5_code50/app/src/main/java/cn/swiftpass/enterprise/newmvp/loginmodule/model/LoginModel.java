package cn.swiftpass.enterprise.newmvp.loginmodule.model;

import com.trello.rxlifecycle2.LifecycleTransformer;

import java.util.HashMap;

import cn.swiftpass.enterprise.newmvp.base.BaseBean;
import cn.swiftpass.enterprise.newmvp.base.mvp.ResultCallback;
import cn.swiftpass.enterprise.newmvp.config.AppConfig;
import cn.swiftpass.enterprise.newmvp.config.RequestKeyConfig;
import cn.swiftpass.enterprise.newmvp.config.RequestTypeConfig;
import cn.swiftpass.enterprise.newmvp.config.RequestValueConfig;
import cn.swiftpass.enterprise.newmvp.config.SPConfig;
import cn.swiftpass.enterprise.newmvp.http.HttpManager;
import cn.swiftpass.enterprise.newmvp.http.rx.RxObserver;
import cn.swiftpass.enterprise.newmvp.loginmodule.api.LoginApiService;
import cn.swiftpass.enterprise.newmvp.loginmodule.contract.LoginContract;
import cn.swiftpass.enterprise.newmvp.loginmodule.model.bean.ExchangeKeyBean;
import cn.swiftpass.enterprise.newmvp.loginmodule.model.bean.LoginBean;
import cn.swiftpass.enterprise.newmvp.utils.GsonUtil;
import cn.swiftpass.enterprise.newmvp.utils.SharedPreferencesUtil;
import cn.swiftpass.enterprise.utils.AESHelper;
import cn.swiftpass.enterprise.utils.ECDHUtils;
import cn.swiftpass.enterprise.utils.MD5;
import cn.swiftpass.enterprise.utils.SignUtil;


/**
 * 创建人：caoxiaoya
 * 时间：2019/4/28
 * 描述：Login-model层
 * 备注：
 */
public class LoginModel implements LoginContract.Model {

    private String publicKey;
    private long spayRs;
    /**
     * 私钥+秘钥  混合
     */
    private String secretKey;
    private LoginApiService mLoginApiService;

    /**
     * 在model构造时创建api，并返回Observable
     */
    public LoginModel() {
        mLoginApiService = HttpManager.getInstance().create(LoginApiService.class);
    }

    @Override
    public void getLogin(String userName, String pwd, final ResultCallback<LoginBean> newCall, LifecycleTransformer transformer) {
        long spayRs = System.currentTimeMillis();
        String paseStr = MD5.md5s(secretKey).toUpperCase();
        HashMap<String, String> hashMap = new HashMap<>(9);
        hashMap.put(RequestKeyConfig.USERNAME, userName);
        hashMap.put(RequestKeyConfig.PASSWORD, AESHelper.aesEncrypt(pwd, paseStr.substring(8, 24)));
        hashMap.put(RequestKeyConfig.CLIENT, RequestKeyConfig.CLIENT);
        hashMap.put(RequestKeyConfig.SKEY, RequestValueConfig.SKEY);
        hashMap.put(RequestKeyConfig.BANKCODE, AppConfig.BANKCODE);
        hashMap.put(RequestKeyConfig.ANDROIDTRANSPUSH, "1");
        hashMap.put(RequestKeyConfig.PUSHCID, "");
        hashMap.put(RequestKeyConfig.SPAYRS, String.valueOf(spayRs));
        hashMap.put(RequestKeyConfig.NNS, SignUtil.getInstance().createSign(hashMap, MD5.md5s(String.valueOf(spayRs))));

        String strJson = GsonUtil.toJson(hashMap);
        HttpManager.getInstance().httpRxRequest(mLoginApiService.login(strJson, String.valueOf(spayRs)), new RxObserver<BaseBean<LoginBean>>() {
            @Override
            public void onSuccess(BaseBean<LoginBean> data) {
                newCall.onSuccess(data.getData());
            }

            @Override
            public void onFail(String code, String errorMsg) {
                newCall.onError(RequestTypeConfig.LOGIN_TYPE, code, errorMsg);
            }
        }, transformer);

    }

    @Override
    public void getExchangeServiceKey(final ResultCallback<ExchangeKeyBean> newCall, LifecycleTransformer transformer) {
        spayRs = System.currentTimeMillis();
        try {
            publicKey = ECDHUtils.getInstance().getAppPubKey();
        } catch (Exception e) {
            e.printStackTrace();
            publicKey = "";
        }
        HashMap<String, String> hashMap = new HashMap<>(3);
        hashMap.put(RequestKeyConfig.PUBLICKEY, publicKey);
        hashMap.put(RequestKeyConfig.SPAYRS, String.valueOf(spayRs));
        hashMap.put(RequestKeyConfig.NNS, SignUtil.getInstance().createSign(hashMap, MD5.md5s(String.valueOf(spayRs))));
        String strJson = GsonUtil.toJson(hashMap);

        /**
         * exchangeServiceKey本质是一个Observable，为了简化和规范代码
         * 当然也可以这样写 HttpManager.getInstance().create(LoginApiService.class).exchangeServiceKey(strJson,currentTime)
         */
        HttpManager.getInstance().httpRxRequest(mLoginApiService.exchangeServiceKey(strJson, String.valueOf(spayRs)), new RxObserver<BaseBean<ExchangeKeyBean>>() {
            @Override
            public void onSuccess(BaseBean<ExchangeKeyBean> data) {
                //密钥标识
                RequestValueConfig.SKEY = data.getData().getSkey();
                RequestValueConfig.SERPUBKEY = data.getData().getSerPubKey();
                try {
                    secretKey = ECDHUtils.getInstance().ecdhGetShareKey(RequestValueConfig.SERPUBKEY, RequestValueConfig.PRIKEY);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                SharedPreferencesUtil.getInstance().put(SPConfig.SECRETKEY, secretKey);
                newCall.onSuccess(data.getData());
            }

            @Override
            public void onFail(String code, String errorMsg) {
                newCall.onError(RequestTypeConfig.EXCHANGE_TYPE, code, errorMsg);
            }
        }, transformer);

    }

}
