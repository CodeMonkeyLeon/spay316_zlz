package cn.swiftpass.enterprise.print;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.RemoteException;
import android.text.TextUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalInfo;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalItemInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;
import woyou.aidlservice.jiuiv5.ICallback;
import woyou.aidlservice.jiuiv5.IWoyouService;


/**
 * 打印发票 <一句话功能简述> <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2016-4-20]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class PrintBillSuccess {

    public static void pintSum(OrderTotalInfo info, IWoyouService woyouService, ICallback callback, Bitmap bitmap) throws RemoteException {

        /**
         *  打印图片
         */
        woyouService.setAlignment(1, callback);
        Bitmap btm = PrintBillSuccess.getInstance().getBitmap();
        if (null != btm){
            woyouService.printBitmap(btm,callback);
        }

        woyouService.setAlignment(1, callback);
        woyouService.setFontSize(35,callback);
        StringBuffer printBuffer = new StringBuffer();
        StringBuffer titlebuffer = new StringBuffer();
        woyouService.printText(" \n", callback);
        titlebuffer.append("\n" + ToastHelper.toStr(R.string.tx_blue_print_data_sum) + "\n");
        woyouService.printText(titlebuffer.toString() , callback);
        woyouService.setAlignment(0, callback);
        woyouService.setFontSize(24,callback);
        printBuffer.append("\n" + ToastHelper.toStr(R.string.shop_name) + "：" + "\n"+ MainApplication.getMchName() + "\n");
        printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "：" + MainApplication.getMchId() + "\n");

        if (StringUtil.isEmptyOrNull(info.getUserName())) {
            //            printBuffer.append("收银员：全部收银员\n");
        } else {
            printBuffer.append(ToastHelper.toStr(R.string.tx_user) + "：" + info.getUserName() + "\n");
        }
        printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_start_time) + "：" + info.getStartTime() + "\n");
        printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_end_time) + "：" + info.getEndTime() + "\n");
        printBuffer.append("===============================\n");
        woyouService.printText(printBuffer.toString() , callback);
        if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {

            woyouService.printText(ToastHelper.toStr(R.string.tx_bill_stream_pay_money) + "：" + DateUtil.formatRMBMoneyUtils(info.getCountTotalFee()) + ToastHelper.toStr(R.string.pay_yuan) + "\n", callback);
        } else {
            woyouService.printText(ToastHelper.toStr(R.string.tx_bill_stream_pay_money) + "：" + MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(info.getCountTotalFee()) + "\n", callback);
        }

        woyouService.printText(ToastHelper.toStr(R.string.tx_bill_stream_pay_money_num) + "：" + info.getCountTotalCount() + "\n", callback);

        if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
            woyouService.printText(ToastHelper.toStr(R.string.tv_refund_dialog_refund_money) + DateUtil.formatRMBMoneyUtils(info.getCountTotalRefundFee()) + ToastHelper.toStr(R.string.pay_yuan) + "\n", callback);
        } else {
            woyouService.printText(ToastHelper.toStr(R.string.tv_refund_dialog_refund_money) + MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(info.getCountTotalRefundFee()) + "\n", callback);
        }

        printBuffer.append(ToastHelper.toStr(R.string.tx_bill_reufun_total_num) + "：" + info.getCountTotalRefundCount() + "\n");

        woyouService.printText(ToastHelper.toStr(R.string.tx_bill_reufun_total_num) + "：" + info.getCountTotalRefundCount() + "\n", callback);
        if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
            woyouService.printText(ToastHelper.toStr(R.string.tx_blue_print_pay_total) + "：" + DateUtil.formatRMBMoneyUtils((info.getCountTotalFee() - info.getCountTotalRefundFee())) + ToastHelper.toStr(R.string.pay_yuan) + "\n", callback);

        } else {
            woyouService.printText(ToastHelper.toStr(R.string.tx_blue_print_pay_total) + "：" + MainApplication.getFeeFh() + DateUtil.formatMoneyUtils((info.getCountTotalFee() - info.getCountTotalRefundFee())) + "\n", callback);
        }
        woyouService.printText("===============================\n", callback);

        /**
         * 是否开通小费功能，打印小费金额、总计
         */
        if (MainApplication.isTipOpenFlag()){
            // 小费金额
            woyouService.printText(ToastHelper.toStr(R.string.tip_amount) + "：" + MainApplication.getFeeType() + DateUtil.formatMoneyUtils(info.getCountTotalTipFee()) + "\n",callback);
            //小费笔数
            woyouService.printText(ToastHelper.toStr(R.string.tip_count) + "：" + info.getCountTotalTipFeeCount() + "\n",callback);
            woyouService.printText("===============================\n", callback);
        }


        List<OrderTotalItemInfo> list = info.getOrderTotalItemInfo();

        Map<Integer, Object> map = new HashMap<Integer, Object>();

        for (OrderTotalItemInfo itemInfo : list) {

            switch (itemInfo.getPayTypeId()) {

                case 1:
                    map.put(0, itemInfo);
                    break;
                case 2:
                    map.put(1, itemInfo);
                    break;

                case 4:
                    map.put(2, itemInfo);
                    break;
                case 12:
                    map.put(3, itemInfo);
                    break;
                default:
                    map.put(itemInfo.getPayTypeId(), itemInfo);
                    break;
            }

        }

        for (Integer key : map.keySet()) {
            OrderTotalItemInfo itemInfo = (OrderTotalItemInfo) map.get(key);
            if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {

                woyouService.printText(MainApplication.getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId())) + ToastHelper.toStr(R.string.tx_money)+ DateUtil.formatRMBMoneyUtils(itemInfo.getSuccessFee()) + ToastHelper.toStr(R.string.pay_yuan) + "\n", callback);
            } else {
                woyouService.printText(MainApplication.getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId()))+"\n", callback);
                woyouService.printText(ToastHelper.toStr(R.string.tx_money)+ MainApplication.getFeeType() + DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "\n", callback);
            }

//            woyouService.printText( ToastHelper.toStr(R.string.tx_bill_stream_pay_money_num) + "："+ itemInfo.getSuccessCount() + "\n", callback);
            woyouService.printText(ToastHelper.toStr(R.string.tv_settle_count) + "："+ itemInfo.getSuccessCount() + "\n", callback);

          /*  if (MainApplication.feeFh.equalsIgnoreCase("¥")) {

                woyouService.printText(MainApplication.getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId())) + ToastHelper.toStr(R.string.tx_money) + DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + ToastHelper.toStr(R.string.pay_yuan) + "\n", callback);
            } else {
                woyouService.printText(MainApplication.getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId())) + ToastHelper.toStr(R.string.tx_money) + MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "\n", callback);
            }
            woyouService.printText(MainApplication.getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId()))+"\n", callback);
            woyouService.printText(MainApplication.getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId())) + ToastHelper.toStr(R.string.tv_settle_count) + "：" + itemInfo.getSuccessCount()  + "\n", callback);
*/
        }
        woyouService.printText("-------------------------------\n", callback);
        printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n");
        woyouService.printText(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n\n", callback);
        printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_sign) + "：\n\n\n\n");
        woyouService.printText(ToastHelper.toStr(R.string.tx_blue_print_sign) + "：\n\n\n\n", callback);
        woyouService.printText( "\n\n", callback);

    }

    public static void pint(boolean isCashierShow, final Order orderModel, final IWoyouService woyouService, final ICallback callback, Bitmap bitmap) throws RemoteException {
        /**
         *  打印图片
         */
        woyouService.setAlignment(1, callback);
        Bitmap btm = PrintBillSuccess.getInstance().getBitmap();
        if (null != btm){
            woyouService.printBitmap(btm,callback);
        }
        woyouService.printText("\n\n", callback);

        woyouService.setFontSize(35,callback);
        woyouService.setAlignment(1, callback);
        if(!orderModel.isPay()){
            woyouService.printText(ToastHelper.toStr(R.string.tx_blue_print_refund_note) + "\n", callback);
        }
        else{
            woyouService.printText(ToastHelper.toStr(R.string.tx_blue_print_pay_note) + "\n", callback);
        }

        woyouService.setFontSize(24,callback);
        woyouService.setAlignment(0, callback);

        woyouService.printText("\n" + orderModel.getPartner() , callback);
        woyouService.printText("\n" + ToastHelper.toStr(R.string.tv_pay_client_save), callback);
        woyouService.printText("\n===============================\n", callback);
        if (!StringUtil.isEmptyOrNull(MainApplication.getMchName())) {
            woyouService.printText(ToastHelper.toStr(R.string.shop_name) + "\n" + MainApplication.getMchName() + "\n", callback);
        }
        if (!StringUtil.isEmptyOrNull(MainApplication.getMchId())) {
            woyouService.printText(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "：" + MainApplication.getMchId()+"\n" , callback);
        }

        if(!orderModel.isPay()){//退款

        }else{
            if (MainApplication.isAdmin.equals("0") && !isCashierShow) { //收银员
                woyouService.printText(ToastHelper.toStr(R.string.tx_user) + "：" + MainApplication.realName + "\n", callback);
            }

            if (isCashierShow){
                woyouService.printText(ToastHelper.toStr(R.string.tx_user) + "："+ orderModel.getUserName() + "\n",callback);
            }
        }

        if(!orderModel.isPay()){//退款
            woyouService.printText(ToastHelper.toStr(R.string.tx_blue_print_refund_time) + "：" + "\n"+ orderModel.getAddTimeNew() + "\n", callback);
            woyouService.printText(ToastHelper.toStr(R.string.refund_odd_numbers) + "：\n", callback);
            woyouService.printText(orderModel.getRefundNo() + "\n", callback);
        }else{
            try {
                woyouService.printText(ToastHelper.toStr(R.string.tx_bill_stream_time) + "："+ "\n" + orderModel.getAddTimeNew() + "\n", callback);
            } catch (Exception e) {
                woyouService.printText(ToastHelper.toStr(R.string.tx_bill_stream_time) + "：" + "\n"+ orderModel.getTradeTimeNew() + "\n", callback);
            }
        }

        woyouService.printText(ToastHelper.toStr(R.string.tx_order_no) + ": " + "\n", callback);
        woyouService.printText(orderModel.getOrderNoMch() + "\n", callback);

        if (orderModel.isPay()) {
            if (MainApplication.getPayTypeMap() != null && MainApplication.getPayTypeMap().size() > 0) {
                woyouService.printText(MainApplication.getPayTypeMap().get(orderModel.getApiCode())+ " " + ToastHelper.toStr(R.string.tx_orderno) + ":\n", callback);
            }
            woyouService.printText(orderModel.getTransactionId() + "\n", callback);

            woyouService.printText(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：" + "\n"+ orderModel.getTradeName() + "\n", callback);
            woyouService.printText(ToastHelper.toStr(R.string.tx_bill_stream_statr) + "：" + "\n"+ MainApplication.getTradeTypeMap().get(orderModel.getTradeState() + "") + "\n", callback);

            if (!StringUtil.isEmptyOrNull(orderModel.getAttach())) {
                woyouService.printText(ToastHelper.toStr(R.string.tx_bill_stream_attach) + "：" + " " + orderModel.getAttach() + "\n", callback);
            }

            if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
                if (orderModel.getDaMoney() > 0) {
                    long actualMoney = orderModel.getDaMoney() + orderModel.getOrderFee();

                    woyouService.setAlignment(0, callback);
                    woyouService.printText(ToastHelper.toStr(R.string.tx_blue_print_money) + "："+ "\n", callback);

                    woyouService.setAlignment(2, callback);
                    woyouService.printText( DateUtil.formatRMBMoneyUtils(actualMoney) + ToastHelper.toStr(R.string.pay_yuan)+ "\n", callback);

                } else {

                    woyouService.setAlignment(0, callback);
                    woyouService.printText(ToastHelper.toStr(R.string.tx_blue_print_money) + "：" + "\n", callback);

                    woyouService.setAlignment(2, callback);
                    woyouService.printText( DateUtil.formatRMBMoneyUtils(orderModel.getOrderFee()) + ToastHelper.toStr(R.string.pay_yuan)+ "\n", callback);
                }
            } else {
                if (MainApplication.isSurchargeOpen()) {
                    if(!orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") &&  !orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")){
                        woyouService.setAlignment(0, callback);
                        woyouService.printText(ToastHelper.toStr(R.string.tx_blue_print_money) + "："+ "\n", callback);

                        woyouService.setAlignment(2, callback);
                        woyouService.printText(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getOrderFee())+ "\n", callback);

                        woyouService.setAlignment(0, callback);
                        woyouService.printText(ToastHelper.toStr(R.string.tx_surcharge) + "："+ "\n", callback);

                        woyouService.setAlignment(2, callback);
                        woyouService.printText( MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getSurcharge())+ "\n", callback);
                    }
                }

                //Uplan ---V3.0.5迭代新增

                //如果为预授权的支付类型，则不展示任何优惠相关的信息
                    /*因目前SPAY與網關支持的預授權接口僅為‘支付寶’預授權，
                    遂與銀聯Uplan不衝突，遂預授權頁面內，無需處理該優惠信息。*/
                if(!TextUtils.isEmpty(orderModel.getTradeType()) && !orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") &&  !orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")){
                    // 优惠详情 --- 如果列表有数据，则展示，有多少条，展示多少条
                    if(orderModel.getUplanDetailsBeans() != null && orderModel.getUplanDetailsBeans().size() > 0){
                        for(int i =0 ; i<orderModel.getUplanDetailsBeans().size() ; i++){
                            String string_Uplan_details;
                            //如果是Uplan Discount或者Instant Discount，则采用本地词条，带不同语言的翻译
                            if(orderModel.getUplanDetailsBeans().get(i).getDiscountNote().equalsIgnoreCase("Uplan discount")){
                                string_Uplan_details = ToastHelper.toStr(R.string.uplan_Uplan_discount) + "：";
                            }else if(orderModel.getUplanDetailsBeans().get(i).getDiscountNote().equalsIgnoreCase("Instant Discount")){
                                string_Uplan_details = ToastHelper.toStr(R.string.uplan_Uplan_instant_discount) + "：";
                            } else{//否则，后台传什么展示什么
                                string_Uplan_details = orderModel.getUplanDetailsBeans().get(i).getDiscountNote()+ "：";
                            }
                            woyouService.setAlignment(0, callback);
                            woyouService.printText(string_Uplan_details+ "\n", callback);

                            woyouService.setAlignment(2, callback);
                            woyouService.printText( MainApplication.feeType+ " "+ "-" + orderModel.getUplanDetailsBeans().get(i).getDiscountAmt()+ "\n", callback);

                        }
                    }

                    // 消费者实付金额 --- 如果不为0 ，就展示
                    if(orderModel.getCostFee() != 0){
                        woyouService.setAlignment(0, callback);
                        woyouService.printText(ToastHelper.toStr(R.string.uplan_Uplan_actual_paid_amount) + "："+ "\n", callback);

                        woyouService.setAlignment(2, callback);
                        woyouService.printText( MainApplication.feeType + " "+ DateUtil.formatMoneyUtils(orderModel.getCostFee())+ "\n", callback);

                    }
                }

                woyouService.setAlignment(0, callback);
                woyouService.printText(ToastHelper.toStr(R.string.tv_charge_total) + "："+ "\n", callback);

                woyouService.setAlignment(2, callback);
                woyouService.printText( MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee())+ "\n", callback);

                if (orderModel.getCashFeel() > 0) {
                    woyouService.setAlignment(2, callback);
                    woyouService.printText(ToastHelper.toStr(R.string.pay_yuan)+ DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel())+ "\n", callback);
                }
            }

        }else {
            woyouService.printText(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "："+ "\n" + orderModel.getTradeName() + "\n", callback);
            if (!StringUtil.isEmptyOrNull(orderModel.getUserName())) {
                woyouService.printText(ToastHelper.toStr(R.string.tv_refund_peop) + "："+ "\n" + orderModel.getUserName() + "\n", callback);

            }
            if (MainApplication.getRefundStateMap() != null && MainApplication.getRefundStateMap().size() > 0) {

                woyouService.printText(ToastHelper.toStr(R.string.tv_refund_state) + "："+ "\n" + MainApplication.getRefundStateMap().get(orderModel.getRefundState() + "") + "\n", callback);
            }
            if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {

                woyouService.setAlignment(0, callback);
                woyouService.printText(ToastHelper.toStr(R.string.tx_blue_print_money) + "："+ "\n", callback);

                woyouService.setAlignment(2, callback);
                woyouService.printText( DateUtil.formatRMBMoneyUtils(orderModel.getTotalFee()) + ToastHelper.toStr(R.string.pay_yuan)+ "\n", callback);


                woyouService.setAlignment(0, callback);
                woyouService.printText(ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "："+ "\n", callback);

                woyouService.setAlignment(2, callback);
                woyouService.printText( DateUtil.formatRMBMoneyUtils(orderModel.getRefundMoney()) + ToastHelper.toStr(R.string.pay_yuan)+ "\n", callback);


            } else {

                woyouService.setAlignment(0, callback);
                woyouService.printText(ToastHelper.toStr(R.string.tv_charge_total) + "："+ "\n", callback);

                woyouService.setAlignment(2, callback);
                woyouService.printText( MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee())+ "\n", callback);

                woyouService.setAlignment(0, callback);
                woyouService.printText(ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "："+ "\n", callback);

                woyouService.setAlignment(2, callback);
                woyouService.printText( MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getRefundMoney())+ "\n", callback);

                if(orderModel.getCashFeel() > 0){
                    woyouService.setAlignment(2, callback);
                    woyouService.printText( ToastHelper.toStr(R.string.pay_yuan)+ DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel())+ "\n", callback);

                }
            }
            if (!StringUtil.isEmptyOrNull(orderModel.getPrintInfo())) {
                woyouService.setAlignment(0, callback);
                woyouService.printText(orderModel.getPrintInfo() + "\n", callback);
            }
        }

        if(!StringUtil.isEmptyOrNull(orderModel.getOrderNoMch()) && orderModel.isPay()){
            woyouService.setAlignment(1, callback);
            woyouService.printText( "\n", callback);
            woyouService.printQRCode(orderModel.getOrderNoMch(), 7, 2, callback);
            woyouService.printText("\n"+ ToastHelper.toStr(R.string.refound_QR_code)+ "\n", callback);
        }
        woyouService.printText("-------------------------------\n", callback);
        woyouService.setAlignment(0, callback);
        woyouService.printText(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n", callback);
        if (orderModel.getPartner().equalsIgnoreCase(ToastHelper.toStr(R.string.tv_pay_mch_stub))
                || orderModel.getPartner().equalsIgnoreCase(ToastHelper.toStr(R.string.tv_pay_user_stub))) {
            woyouService.printText("\n"+ ToastHelper.toStr(R.string.tx_blue_print_sign) + "：\n\n\n\n", callback);
            //                printBuffer.append("备注：\n\n\n");
        } else {
            woyouService.printText(" \n\n\n", callback);
        }

//        if (orderModel.getTradeType().equals(MainApplication.PAY_WX_MICROPAY) || orderModel.getTradeType().equals(MainApplication.PAY_WX_NATIVE) || orderModel.getTradeType().equals(MainApplication.PAY_WX_SJPAY)) {
//            woyouService.printText(ToastHelper.toStr(R.string.tx_bill_stream_wx_order) + "：\n", callback);
//            woyouService.printText(orderModel.getTransactionId() + "\n", callback);
//        } else if (orderModel.getTradeType().equals(MainApplication.PAY_QQ_MICROPAY) || orderModel.getTradeType().equals(MainApplication.PAY_QQ_NATIVE) || orderModel.getTradeType().equals(MainApplication.PAY_QQ_NATIVE1) || orderModel.getTradeType().equals(MainApplication.PAY_QQ_WAP)) {
//            woyouService.printText(ToastHelper.toStr(R.string.tv_pay_qq_order) + "：\n", callback);
//            woyouService.printText(orderModel.getTransactionId() + "\n", callback);
//        } else if (orderModel.getTradeType().startsWith(MainApplication.PAY_ZFB_NATIVE) || orderModel.getTradeType().equals(MainApplication.PAY_ZFB_MICROPAY) || orderModel.getTradeType().equals(MainApplication.PAY_ZFB_WAP) || orderModel.getTradeType().equalsIgnoreCase("pay.alipay.micropay.intl") || orderModel.getTradeType().equals("pay.alipay.micropayv2")) {
//            woyouService.printText(ToastHelper.toStr(R.string.tv_pay_zfb_order) + "：\n", callback);
//            woyouService.printText(orderModel.getTransactionId() + "\n", callback);
//        } else if (orderModel.getTradeType().equals(MainApplication.PAY_JINGDONG) || orderModel.getTradeType().equals(MainApplication.PAY_JINGDONG_NATIVE)) {
//            woyouService.printText(ToastHelper.toStr(R.string.tx_pay_jd_order) + "：\n", callback);
//            woyouService.printText(orderModel.getTransactionId() + "\n", callback);
//        } else {
//            woyouService.printText(ToastHelper.toStr(R.string.tx_pay_other_order) + "：\n", callback);
//            woyouService.printText(orderModel.getTransactionId() + "\n", callback);
//        }

        /*if (!orderModel.isPay()) {

            woyouService.printText(ToastHelper.toStr(R.string.refund_odd_numbers) + "：\n", callback);
            woyouService.printText(orderModel.getRefundNo() + "\n", callback);

            woyouService.printText(ToastHelper.toStr(R.string.tx_refund_time) + "：" + "\n", callback);

            woyouService.printText(orderModel.getAddTime() + "\n", callback);

            if (!"".equals(orderModel.getUserName()) && null != orderModel.getUserName()) {
                woyouService.printText(ToastHelper.toStr(R.string.tx_refund_user) + "：" + "\n", callback);

                woyouService.printText(orderModel.getUserName() + "\n", callback);

            } else {
                if (MainApplication.isAdmin.equals("1")) {

                    woyouService.printText(ToastHelper.toStr(R.string.tx_refund_user) + "：" + "\n", callback);
                    woyouService.printText(orderModel.getMchName() + "\n", callback);
                }
            }

            double m = orderModel.getTotalFee();

            woyouService.printText(ToastHelper.toStr(R.string.tx_blue_print_money) + ":" + " " + MainApplication.feeFh + DateUtil.formatMoneyUtil(m / 100) + "\n", callback);
            woyouService.printText(ToastHelper.toStr(R.string.tx_surcharge) + "：" + " " + MainApplication.feeFh + DateUtil.formatMoneyUtils(orderModel.getSurcharge() ) + "\n", callback);

            //woyouService.printText(ToastHelper.toStr(R.string.tx_withholding) + "：" + " " + MainApplication.feeFh + DateUtil.formatMoneyUtils(orderModel.getWithholdingTax()) + "\n", callback);

            woyouService.printText(ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + ":" + " " + MainApplication.feeFh + DateUtil.formatMoneyUtils(orderModel.getRefundMoney()) + "\n", callback);

            if (MainApplication.getRefundStateMap() != null && MainApplication.getRefundStateMap().size() > 0) {
                woyouService.printText(ToastHelper.toStr(R.string.tv_refund_state) + "：" + MainApplication.getRefundStateMap().get(orderModel.getRefundState() + "") + "\n", callback);
            }

        } *//*else {

            *//*try {
                woyouService.printText(ToastHelper.toStr(R.string.tx_bill_stream_time) + "：" + " " + orderModel.getNotifyTime() + "\n", callback);
            } catch (Exception e) {

            }
*//*
            woyouService.printText("\n\n\n\n", callback);
            double m = orderModel.money;

            woyouService.printText(ToastHelper.toStr(R.string.tx_blue_print_money) + "：" + " " + MainApplication.feeFh + DateUtil.formatMoneyUtil(m / 100) + "\n", callback);
            woyouService.printText(ToastHelper.toStr(R.string.tx_surcharge) + "：" + " " + MainApplication.feeFh + DateUtil.formatMoneyUtils(orderModel.getSurcharge()) + "\n", callback);
            //woyouService.printText(ToastHelper.toStr(R.string.tx_withholding) + "：" + " " + MainApplication.feeFh + DateUtil.formatMoneyUtils(orderModel.getWithholdingTax() ) + "\n", callback);


            // 收银员
            if (!StringUtil.isEmptyOrNull(orderModel.getUserName())) {
                woyouService.printText(ToastHelper.toStr(R.string.tx_user) + "：" + " " + orderModel.getUserName() + "\n", callback);
            }

            woyouService.printText(ToastHelper.toStr(R.string.tx_bill_stream_statr) + "：" + " " + MainApplication.getTradeTypeMap().get(orderModel.getTradeState() + "") + "\n", callback);
        }*/


    /*    if (orderModel.getPartner().equals(ToastHelper.toStr(R.string.tv_pay_mch_stub))) {
            woyouService.printText(ToastHelper.toStr(R.string.tv_pay_client_mark) + "：\n", callback);
            woyouService.printText(ToastHelper.toStr(R.string.tv_pay_client_remark) + "：\n", callback);
        }

        woyouService.printText(" \n", callback);

      */

    }

    /**
     * printType参数1：预授权成功---预授权小票(反扫)
     *  * printType参数5：预授权成功---预授权小票(正扫)
     * printType参数4：预授权订单详情---预授权小票
     * printType参数2：预授权解冻详情小票
     * printType参数3：预授权解冻完成小票
     */
    public static void printPreAuthStr(final IWoyouService woyouService, final ICallback callback, int printType, Order orderModel) throws RemoteException {
        int minus=1;
        //根据当前的小数点的位数来判断应该除以或者乘以多少
        for(int i = 0; i < MainApplication.numFixed ; i++ ){
            minus = minus * 10;
        }


        switch (printType){
            case 1:
            case 4:
            case 5:
                woyouService.setFontSize(35,callback);
                woyouService.setAlignment(1, callback);
                woyouService.printText(ToastHelper.toStr(R.string.pre_auth_receipt) + "\n", callback);
                break;

            case 2:
            case 3:
                woyouService.setFontSize(30,callback);
                woyouService.setAlignment(1, callback);
                woyouService.printText(ToastHelper.toStr(R.string.pre_auth_unfreezed_receipt) + "\n", callback);
                break;
        }
        woyouService.setFontSize(24,callback);
        woyouService.setAlignment(0, callback);

        woyouService.printText("\n" + orderModel.getPartner() , callback);
        woyouService.printText("\n" + ToastHelper.toStr(R.string.tv_pay_client_save), callback);
        woyouService.printText("\n===============================\n", callback);
        if (!StringUtil.isEmptyOrNull(MainApplication.getMchName())) {
            woyouService.printText(ToastHelper.toStr(R.string.shop_name) + "\n" + MainApplication.getMchName() + "\n", callback);
        }
        if (!StringUtil.isEmptyOrNull(MainApplication.getMchId())) {
            woyouService.printText(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "：" + MainApplication.getMchId()+"\n" , callback);
        }

        if (printType==1) {
            try {
                woyouService.printText(ToastHelper.toStr(R.string.pre_auth_time) + "："+ "\n" + orderModel.getTradeTime() + "\n", callback);
            } catch (Exception e) {
                woyouService.printText(ToastHelper.toStr(R.string.pre_auth_time) + "：" + "\n"+ orderModel.getTradeTime() + "\n", callback);
            }
        }else if(printType==4){
            try {
                woyouService.printText(ToastHelper.toStr(R.string.pre_auth_time) + "："+ "\n" + orderModel.getTradeTimeNew() + "\n", callback);
            } catch (Exception e) {
                woyouService.printText(ToastHelper.toStr(R.string.pre_auth_time) + "：" + "\n"+ orderModel.getTradeTimeNew() + "\n", callback);
            }
        }else if(printType==5){
            try {
                woyouService.printText(ToastHelper.toStr(R.string.pre_auth_time) + "："+ "\n" + orderModel.getTimeEnd() + "\n", callback);
            } catch (Exception e) {
                woyouService.printText(ToastHelper.toStr(R.string.pre_auth_time) + "：" + "\n"+ orderModel.getTimeEnd() + "\n", callback);
            }
        } else if(printType==2){
            try {
                woyouService.printText(ToastHelper.toStr(R.string.unfreezed_time) + "："+ "\n" + orderModel.getOperateTime() + "\n", callback);
            } catch (Exception e) {
                woyouService.printText(ToastHelper.toStr(R.string.unfreezed_time) + "：" + "\n"+ orderModel.getOperateTime() + "\n", callback);
            }

        }else if(printType==3){
            try {
                woyouService.printText(ToastHelper.toStr(R.string.unfreezed_time) + "："+ "\n" + orderModel.getUnFreezeTime() + "\n", callback);
            } catch (Exception e) {
                woyouService.printText(ToastHelper.toStr(R.string.unfreezed_time) + "：" + "\n"+ orderModel.getUnFreezeTime() + "\n", callback);
            }
        }

        if (printType==1 || printType == 4 || printType == 5){
            //平台预授权订单号
            try {
                woyouService.printText(ToastHelper.toStr(R.string.platform_pre_auth_order_id) + "："+ "\n" + orderModel.getAuthNo() + "\n", callback);
            } catch (Exception e) {
            }

            String language = PreferenceUtil.getString("language","");
            Locale locale = MainApplication.getContext().getResources().getConfiguration().locale; //zh-rHK
            String lan = Locale.getDefault().toString();
            if (!TextUtils.isEmpty(language)) {
                if (language.equals(MainApplication.LANG_CODE_EN_US)) {
                    //支付宝单号
                    if(printType==1 || printType == 5){
                        woyouService.printText(MainApplication.getPayTypeMap().get("2") + " "+ ToastHelper.toStr(R.string.tx_orderno) + "："+ "\n" + orderModel.getOutTransactionId() + "\n", callback);
                    }

                    if(printType== 4){
                        woyouService.printText(MainApplication.getPayTypeMap().get("2") + " "+ ToastHelper.toStr(R.string.tx_orderno) + "："+ "\n" + orderModel.getTransactionId() + "\n", callback);

                    }

                } else {
                    //支付宝单号
                    if(printType==1 || printType == 5){
                        woyouService.printText(MainApplication.getPayTypeMap().get("2")+ ToastHelper.toStr(R.string.tx_orderno) + "："+ "\n" + orderModel.getOutTransactionId() + "\n", callback);
                    }

                    if(printType== 4){
                        woyouService.printText(MainApplication.getPayTypeMap().get("2") + ToastHelper.toStr(R.string.tx_orderno) + "："+ "\n" + orderModel.getTransactionId() + "\n", callback);
                    }
                }
            } else {
                if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_EN_US) ) {
                    //支付宝单号
                    if(printType==1 || printType == 5){
                        woyouService.printText(MainApplication.getPayTypeMap().get("2") + " "+ ToastHelper.toStr(R.string.tx_orderno) + "："+ "\n" + orderModel.getOutTransactionId() + "\n", callback);
                    }

                    if(printType== 4){
                        woyouService.printText(MainApplication.getPayTypeMap().get("2") + " "+ ToastHelper.toStr(R.string.tx_orderno) + "："+ "\n" + orderModel.getTransactionId() + "\n", callback);

                    }

                } else {
                    //支付宝单号
                    if(printType==1 || printType == 5){
                        woyouService.printText(MainApplication.getPayTypeMap().get("2")+ ToastHelper.toStr(R.string.tx_orderno) + "："+ "\n" + orderModel.getOutTransactionId() + "\n", callback);
                    }

                    if(printType== 4){
                        woyouService.printText(MainApplication.getPayTypeMap().get("2") + ToastHelper.toStr(R.string.tx_orderno) + "："+ "\n" + orderModel.getTransactionId() + "\n", callback);
                    }
                }
            }


        }else if(printType == 2 || printType== 3){
            //解冻订单号
            try {
                woyouService.printText(ToastHelper.toStr(R.string.unfreezed_order_id) + "："+ "\n" + orderModel.getOutRequestNo() + "\n", callback);
            } catch (Exception e) {
            }
            //平台预授权订单号
            try {
                woyouService.printText(ToastHelper.toStr(R.string.platform_pre_auth_order_id) + "："+ "\n" + orderModel.getAuthNo() + "\n", callback);
            } catch (Exception e) {
            }
        }

        if(printType == 5){
            String tradename;
            if (!isAbsoluteNullStr(orderModel.getTradeName())){//先判断大写的Name，再判断小写name
                tradename=orderModel.getTradeName();
            }else {
                tradename=orderModel.getTradename();
            }
            try {
                woyouService.printText(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "："+ "\n" + tradename + "\n", callback);
            } catch (Exception e) {
            }

        }else{
            try {
                woyouService.printText(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "："+ "\n" + orderModel.getTradeName() + "\n", callback);
            } catch (Exception e) {
            }
        }

        if (printType == 3) {
            try {
                woyouService.printText(ToastHelper.toStr(R.string.tv_unfreezen_applicant) + "："+ "\n" + orderModel.getUserName() + "\n", callback);
            } catch (Exception e) {
            }
        }

       //订单状态
        String operationType = "";
        if(printType == 1 || printType == 4){
            operationType = MainApplication.getPreAuthtradeStateMap().get(orderModel.getTradeState() + "");

        }else if(printType == 2){
            switch (orderModel.getOperationType()){
                case 2:
                    operationType = ToastHelper.toStr(R.string.freezen_success);
                    break;
            }
        }else if(printType == 3){
            operationType = ToastHelper.toStr(R.string.unfreezing);
        }else if (printType == 5){
            operationType = ToastHelper.toStr(R.string.pre_auth_authorized);
        }

        try {
            woyouService.printText(ToastHelper.toStr(R.string.tx_bill_stream_statr) + "："+ "\n" + operationType+ "\n", callback);
        } catch (Exception e) {
        }

        switch (printType){
            case 1:
            case 4:
            case 5:
                if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))){
                    woyouService.printText(ToastHelper.toStr(R.string.pre_auth_amount) + "："+  DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan)+ "\n", callback);
                }else{
                    long preAuthorizationMoney=0;//用来接收printType判断授权金额来自反扫、正扫
                    if (printType==1){
                        preAuthorizationMoney=orderModel.getRestAmount();//反扫
                    }else if (printType==5){
                        preAuthorizationMoney=orderModel.getTotalFee();//正扫
                    }else if(printType==4){
                        preAuthorizationMoney=orderModel.getTotalFreezeAmount();//预授权详情
                    }
                    //预授权金额
                    woyouService.setAlignment(0, callback);
                    woyouService.printText(ToastHelper.toStr(R.string.pre_auth_amount) + "："+ "\n", callback);

                    woyouService.setAlignment(2, callback);
                    woyouService.printText(MainApplication.feeType + DateUtil.formatMoneyUtils(preAuthorizationMoney)+ "\n", callback);
                }
                woyouService.printText(" \n\n", callback);
                //二维码
                //打印小票的代码
                if(!StringUtil.isEmptyOrNull(orderModel.getAuthNo())){
                    woyouService.setAlignment(1, callback);
                    woyouService.printQRCode(orderModel.getAuthNo(), 7, 2, callback);
                    woyouService.printText("\n"+ ToastHelper.toStr(R.string.scan_to_check)+ "\n", callback);//此二维码用于预授权收款、解冻操作
                }
                break;
            case 2:
                if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))){
                    woyouService.setAlignment(0, callback);
                    woyouService.printText(ToastHelper.toStr(R.string.unfreezing_amount)+":"+ "\n", callback);

                    woyouService.setAlignment(2, callback);
                    woyouService.printText(DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan), callback);

                }else{
                    woyouService.setAlignment(0, callback);
                    woyouService.printText(ToastHelper.toStr(R.string.unfreezing_amount)+":"+ "\n", callback);

                    woyouService.setAlignment(2, callback);
                    woyouService.printText(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getMoney())+ "\n", callback);
                }
                //解冻将原路返回支付账户或银行卡，到账时间已第三方为准
                woyouService.setAlignment(0, callback);
                woyouService.printText(ToastHelper.toStr(R.string.unfreeze)+ "\n", callback);
                break;
            case 3:
                //授权金额
                if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))){
                    woyouService.setAlignment(0, callback);
                    woyouService.printText(ToastHelper.toStr(R.string.pre_auth_amount)+":"+ "\n", callback);

                    woyouService.setAlignment(2, callback);
                    woyouService.printText(DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan)+ "\n", callback);
                }else{
                    woyouService.setAlignment(0, callback);
                    woyouService.printText(ToastHelper.toStr(R.string.pre_auth_amount)+":"+ "\n", callback);

                    woyouService.setAlignment(2, callback);
                    woyouService.printText(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFreezeAmount())+ "\n", callback);
                }
                //解冻金额
                if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))){
                    woyouService.setAlignment(0, callback);
                    woyouService.printText(ToastHelper.toStr(R.string.unfreezing_amount)+":"+ "\n", callback);

                    woyouService.setAlignment(2, callback);
                    woyouService.printText(DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan)+ "\n", callback);
                }else{
                    woyouService.setAlignment(0, callback);
                    woyouService.printText(ToastHelper.toStr(R.string.unfreezing_amount)+":"+ "\n", callback);

                    woyouService.setAlignment(2, callback);
                    woyouService.printText( MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee())+ "\n", callback);
                }
                //解冻将原路返回支付账户或银行卡，到账时间已第三方为准
                woyouService.setAlignment(0, callback);
                woyouService.printText(ToastHelper.toStr(R.string.unfreeze)+ "\n", callback);
                break;
            default:
                break;
        }

        woyouService.printText("-------------------------------\n", callback);
        woyouService.setAlignment(0, callback);
        woyouService.printText(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n", callback);
        if (orderModel.getPartner().equalsIgnoreCase(ToastHelper.toStr(R.string.tv_pay_mch_stub))
                || orderModel.getPartner().equalsIgnoreCase(ToastHelper.toStr(R.string.tv_pay_user_stub))) {
            woyouService.printText("\n"+ ToastHelper.toStr(R.string.tx_blue_print_sign) + "：\n\n\n\n", callback);
        } else {
            woyouService.printText(" \n\n\n", callback);
        }


    }


    /************************新增获取图片方法*******************************/
    Bitmap bitmap = null;
    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    private volatile static PrintBillSuccess pb;//多线程访问
    /**
     * 使用单例模式获得PrintCuccess对象
     * @return
     */
    public static PrintBillSuccess getInstance(){
        PrintBillSuccess instance = null;
        if (pb==null){
            synchronized (PrintBillSuccess.class){
                if (instance==null){
                    instance = new PrintBillSuccess();
                    pb = instance;
                }
            }
        }
        return pb;
    }


    /**
     * 从Asset中读取文件
     * @param fileName
     * @param context
     * @return
     */
    public Bitmap getImageFromAssetsFile(String fileName, Context context)
    {
        Bitmap image = null;
        AssetManager am = context.getResources().getAssets();
        InputStream is = null;
        try
        {
            is = am.open(fileName);
            image = BitmapFactory.decodeStream(is);

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }finally {
            if (null != is){
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return image;

    }

    public static boolean isAbsoluteNullStr(String str) {
        str = deleteBlank(str);
        if (str == null || str.length() == 0 || "null".equalsIgnoreCase(str)) {
            return true;
        }

        return false;
    }
    /**
     * 去前后空格和去换行符
     *
     * @param str
     * @return
     */
    public static String deleteBlank(String str) {
        if (null != str && 0 < str.trim().length()) {
            char[] array = str.toCharArray();
            int start = 0, end = array.length - 1;
            while (array[start] == ' ') start++;
            while (array[end] == ' ') end--;
            return str.substring(start, end + 1).replaceAll("\n", "");

        } else {
            return "";
        }

    }
}
