package cn.swiftpass.enterprise.ui.fmt;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import cn.swiftpass.enterprise.bussiness.model.ReportTransactionDetailsBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;
import cn.swiftpass.enterprise.ui.activity.TransactionDetailsActivity;

/**
 * Created by aijingya on 2019/6/5.
 *
 * @Package cn.swiftpass.enterprise.ui.fmt
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2019/6/5.18:42.
 */
public class FragmentPieChart extends BaseFragment{
    private TransactionDetailsActivity mActivity;
    public ReportTransactionDetailsBean reportTransactionDetailsBean;
    public String PageType;

    LinearLayout ll_pie_with_data_view;

    public void setPageType(String type){
        this.PageType = type;
    }

    public void setReportTransactionDetailsBean(ReportTransactionDetailsBean transactionDetailsBean){
        this.reportTransactionDetailsBean = transactionDetailsBean;
    }
    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof TransactionDetailsActivity){
            mActivity=(TransactionDetailsActivity) activity;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pie_chart_view, container, false);
        ll_pie_with_data_view = view.findViewById(R.id.ll_pie_with_data_view);

        return view;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (reportTransactionDetailsBean != null) {
            initData();
        }
    }


    public void initData() {
       /* PieViewTest pieViewTest = new PieViewTest(mActivity);
        ll_pie_with_data_view.addView(pieViewTest);*/
    }


    @Override
    protected BasePresenter createPresenter() {
        return null;
    }
}
