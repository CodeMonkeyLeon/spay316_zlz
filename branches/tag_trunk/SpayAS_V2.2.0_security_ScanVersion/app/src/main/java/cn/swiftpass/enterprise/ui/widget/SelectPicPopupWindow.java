package cn.swiftpass.enterprise.ui.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.WalletModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

public class SelectPicPopupWindow extends PopupWindow {

    //private TextView tv_ok;
    private TextView tv_cancel, tv_wx_pay, tv_zfb_pay, tv_qq_pay, tv_jd_pay;

    private View mMenuView;

    //private String type;

    private TextView tv_scan;

    private ViewHolder holder;

    private ViewPayTypeHolder payholder;

    private ListView listView;

    private TextView title;

    List<WalletModel> model;

    private String choice;

    private TextView tv_cel;

    private ImageView iv_bank;

    private Button tv_wx, tv_zfb,  tv_succ, tv_fail, tv_revers, tv_refund, tv_close, tv_tyep_refund;
    private TextView tv_reset;

    private TextView ly_ok;

    private List<Button> listBut = new ArrayList<Button>();

    //    private Map<String, String> payTypeMap = new HashMap<String, String>();

    private SelectPicPopupWindow.HandleBtn handleBtn;

    private Activity activity;

    private boolean isTag = false;

    //private SelectPicPopupWindow.HandleTv handleTv;

    private View v_wx, v_zfb, v_jd, v_qq;

    private GridView gv_pay_type;

    private PayTypeAdape payTypeAdape;

    private LinearLayout ly_pay_type;

    private List<DynModel> list;

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    //    private Map<String, String> typeMap = new HashMap<String, String>();
    //
    public interface HandleBtn {
        void handleOkBtn(List<String> choiceType, List<String> payTypeList,boolean ishandled);
    }

    public SelectPicPopupWindow(Activity context, List<String> payList, final List<String> payTypeList, SelectPicPopupWindow.HandleBtn handleBtn) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.pay_type_choice_dialog_new, null);
        this.handleBtn = handleBtn;
        this.activity = context;
        this.payTypeList = payTypeList;
        initview(mMenuView);
        setDate(payList);
        setLisert();
        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        for (Button b : listBut) {
                            resetBut(b);
                        }
                        if (choiceType.size() > 0) {
                            isTag = true;
                        }
                        tv_tyep_refund.setEnabled(true);
                        choiceType.clear();

                        if (list != null && list.size() > 0) {
                            for (DynModel d : list) {
                                d.setEnaled(0);
                            }
                            payTypeAdape.notifyDataSetChanged();
                        }

                        payTypeList.clear();
                        dismiss();
                    }
                }
                return true;
            }
        });
    }
    private List<String> choiceType = new ArrayList<String>();

    List<String> payTypeList =new ArrayList<>();

    private boolean ishadhandle;

    public SelectPicPopupWindow(boolean isHandled,Activity context, List<String> payList, final List<String> payTypeList, SelectPicPopupWindow.HandleBtn handleBtn) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.pay_type_choice_dialog_new, null);
        this.handleBtn = handleBtn;
        this.activity = context;
        this.payTypeList = payTypeList;
        ishadhandle = isHandled;
        initview(mMenuView);
        setDate(payList);
        setLisert();
        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        for (Button b : listBut) {
                            resetBut(b);
                        }
                        if (choiceType.size() > 0) {
                            isTag = true;
                        }
                        tv_tyep_refund.setEnabled(true);
                        choiceType.clear();

                        if (list != null && list.size() > 0) {
                            for (DynModel d : list) {
                                d.setEnaled(0);
                            }
                            payTypeAdape.notifyDataSetChanged();
                        }

                        payTypeList.clear();
                        dismiss();
                    }
                }
                return true;
            }
        });
    }
    private void setDate(List<String> payList) {
        if (null != payList && payList.size() > 0) {
            for (String s : payList) {
                if (s.equals(tv_wx.getTag().toString())) {
                    updatepayTypeButBg(tv_wx, "1");
                } else if (s.equals(tv_zfb.getTag().toString())) {
                    updatepayTypeButBg(tv_zfb, "1");
                }else if (s.equals(tv_succ.getTag().toString())) {
                    updatepayTypeButBg(tv_succ, "1");
                    setRefundDisabled();
                } else if (s.equals(tv_fail.getTag().toString())) {
                    updatepayTypeButBg(tv_fail, "1");
                    setRefundDisabled();
                } else if (s.equals(tv_revers.getTag().toString())) {
                    updatepayTypeButBg(tv_revers, "1");
                    setRefundDisabled();
                } else if (s.equals(tv_refund.getTag().toString())) {
                    updatepayTypeButBg(tv_refund, "1");
                    setRefundDisabled();
                } else if (s.equals(tv_close.getTag().toString())) {
                    updatepayTypeButBg(tv_close, "1");
                    setRefundDisabled();
                } else if (s.equals(tv_tyep_refund.getTag().toString())) {
                    updateRefundTypeButtonBg(tv_tyep_refund, "4");
                }
            }
        }
    }

    void updatepayTypeButBg(Button b, String type) {
        isTag = false;

        if (choiceType.contains(b.getTag().toString())) {
            if (!choiceType.contains(tv_tyep_refund.getTag().toString())) {
                setRefundEnabled();
            }
            //未选中的状态
            b.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            b.setTextColor(Color.parseColor("#535353"));
            choiceType.remove(b.getTag().toString());
        } else {
            //选中的状态
            b.setBackgroundResource(R.drawable.bg_selected);
            b.setTextColor(Color.WHITE);
            choiceType.add(b.getTag().toString());
        }
    }

    boolean isContainsPayTypeAndState() {
        if (choiceType.contains(tv_wx.getTag().toString())
                || choiceType.contains(tv_zfb.getTag().toString())
                || choiceType.contains(tv_fail.getTag().toString())
                || choiceType.contains(tv_revers.getTag().toString())
                || choiceType.contains(tv_refund.getTag().toString())
                || choiceType.contains(tv_close.getTag().toString())) {
            return true;
        }

        return false;
    }

    boolean isContainsPayState() {
        if (choiceType.contains(tv_fail.getTag().toString())
                || choiceType.contains(tv_revers.getTag().toString())
                || choiceType.contains(tv_refund.getTag().toString())
                || choiceType.contains(tv_close.getTag().toString())) {
            return true;
        }
        return false;
    }


    void updatePayTypeSetEnabled(boolean enable) {
        tv_wx.setEnabled(enable);
        tv_zfb.setEnabled(enable);
        tv_succ.setEnabled(enable);
        tv_fail.setEnabled(enable);
        tv_revers.setEnabled(enable);
        tv_refund.setEnabled(enable);
        tv_close.setEnabled(enable);
        tv_tyep_refund.setEnabled(enable);
    }

    void setPayTypeSetEnabled(boolean enable) {

        tv_succ.setEnabled(enable);
        tv_fail.setEnabled(enable);
        tv_revers.setEnabled(enable);
        tv_refund.setEnabled(enable);
        tv_close.setEnabled(enable);
        if (enable) {

            tv_succ.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            tv_succ.setTextColor(Color.parseColor("#535353"));

            tv_fail.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            tv_fail.setTextColor(Color.parseColor("#535353"));

            tv_revers.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            tv_revers.setTextColor(Color.parseColor("#535353"));

            tv_refund.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            tv_refund.setTextColor(Color.parseColor("#535353"));

            tv_close.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            tv_close.setTextColor(Color.parseColor("#535353"));

        } else {
            tv_succ.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            tv_succ.setTextColor(Color.parseColor("#E3E3E3"));

            tv_fail.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            tv_fail.setTextColor(Color.parseColor("#E3E3E3"));

            tv_revers.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            tv_revers.setTextColor(Color.parseColor("#E3E3E3"));

            tv_refund.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            tv_refund.setTextColor(Color.parseColor("#E3E3E3"));

            tv_close.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            tv_close.setTextColor(Color.parseColor("#E3E3E3"));

        }
    }


    void setRefundDisabled() {
        tv_tyep_refund.setEnabled(false);
        tv_tyep_refund.setBackgroundResource(R.drawable.general_button_thickness_white_default);
        tv_tyep_refund.setTextColor(Color.parseColor("#E3E3E3"));
    }

    void setRefundEnabled() {
        tv_tyep_refund.setEnabled(true);
        tv_tyep_refund.setBackgroundResource(R.drawable.general_button_thickness_white_default);
        tv_tyep_refund.setTextColor(Color.parseColor("#535353"));

    }


    void updatepayStateButBg(Button b) {
        isTag = false;

        if (choiceType.contains(b.getTag().toString())) {
            //未选中的状态
            b.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            choiceType.remove(b.getTag().toString());
            b.setTextColor(Color.parseColor("#535353"));
            if (!isContainsPayState()) {
                setRefundEnabled();
            }

        } else {
            //选中的状态
            b.setBackgroundResource(R.drawable.bg_selected);
            choiceType.add(b.getTag().toString());
            b.setTextColor(Color.WHITE);
        }
    }

    void updateRefundTypeButtonBg(Button b, String type) {
        if (choiceType.contains(b.getTag().toString())) {
            //设置成未选中的状态
            b.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            choiceType.remove(b.getTag().toString());
            b.setTextColor(Color.parseColor("#535353"));
            b.setEnabled(true);
            setPayTypeSetEnabled(true);
        } else {
            setPayTypeSetEnabled(false);

            //设置成选中的状态
            b.setBackgroundResource(R.drawable.bg_selected);
            choiceType.add(b.getTag().toString());
            b.setTextColor(Color.WHITE);
        }
    }

    /*void updateTypeCardButBg(Button b, String type) {
        if (choiceType.contains(b.getTag().toString())) {
            b.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            choiceType.remove(b.getTag().toString());
            b.setTextColor(Color.parseColor("#000000"));
            b.setEnabled(true);
            //            updatePayTypeSetEnabled(false);
            //            setPayTypeSetEnabled(true);
            for (Button bt : listBut) {
                if (!TextUtils.equals(bt.getTag().toString(),"card")) {
                    resetBut(bt);
                }
            }
        } else {
            //            setPayTypeSetEnabled(false);
            //            updatePayTypeSetEnabled(true);
            b.setBackgroundResource(R.drawable.bg_selected);
            b.setEnabled(true);
            choiceType.add(b.getTag().toString());
            b.setTextColor(Color.WHITE);
            for (Button bt : listBut) {
                if (!TextUtils.equals(bt.getTag().toString(),"card")) {
                    setButtonGrey(bt);
                }
            }
        }
    }*/

    //tv_wx, tv_zfb, tv_qq, tv_jd, tv_succ, tv_fail, tv_revers, tv_refund, tv_close, tv_tyep_refund,
    //    tv_card;
    private void setLisert() {
        gv_pay_type.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int postion, long arg3) {
                DynModel dynModel = list.get(postion);
                if (dynModel != null) {
                    TextView tv = (TextView) v.findViewById(R.id.tv_content);
                    if (payTypeList.contains(dynModel.getApiCode())) {
                        payTypeList.remove(dynModel.getApiCode());

                        //设置成未选中的状态
                        tv.setBackgroundResource(R.drawable.general_button_thickness_white_default);
                        tv.setTextColor(Color.parseColor("#535353"));
                        tv.setEnabled(true);

                    } else {
                        payTypeList.add(dynModel.getApiCode());
                        //变成可点击
                        tv.setBackgroundResource(R.drawable.bg_selected);
                        tv.setTextColor(Color.WHITE);
                        tv.setEnabled(false);

                    }
                }
            }
        });

        tv_wx.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                updatepayTypeButBg(tv_wx, "1");
            }
        });

        tv_zfb.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                updatepayTypeButBg(tv_zfb, "2");
            }
        });
        tv_fail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setButtonGrey(tv_tyep_refund);
                updatepayStateButBg(tv_fail);
            }
        });
        tv_succ.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setButtonGrey(tv_tyep_refund);
                updatepayStateButBg(tv_succ);
            }
        });
        tv_revers.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setButtonGrey(tv_tyep_refund);
                updatepayStateButBg(tv_revers);
            }
        });
        tv_refund.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setButtonGrey(tv_tyep_refund);
                updatepayStateButBg(tv_refund);
            }
        });
        tv_close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setButtonGrey(tv_tyep_refund);
                updatepayStateButBg(tv_close);
                //                choiceType.clear();

            }
        });// tv_tyep_refund,
        //      tv_card;
        tv_tyep_refund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateRefundTypeButtonBg(tv_tyep_refund, "4");
            }
        });


        ly_ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (choiceType.size() == 0 && isTag) {
                    ToastHelper.showInfo(activity, ToastHelper.toStr(R.string.tx_bill_stream_chioce_not_null));
                    return;
                } else {
                    ishadhandle = true;
                    handleBtn.handleOkBtn(choiceType, payTypeList,true);
                    dismiss();
                }
            }
        });

        tv_reset.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                for (Button b : listBut) {
                    resetBut(b);
                }
                if (choiceType.size() > 0) {
                    isTag = true;
                }
                tv_tyep_refund.setEnabled(true);
                choiceType.clear();

                if (list != null && list.size() > 0) {
                    for (DynModel d : list) {
                        d.setEnaled(0);
                    }
                    payTypeAdape.notifyDataSetChanged();
                }

                payTypeList.clear();
            }
        });
    }

    private void initview(View v) {
        gv_pay_type = (GridView) v.findViewById(R.id.gv_pay_type);
        ly_pay_type = (LinearLayout) v.findViewById(R.id.ly_pay_type);
        gv_pay_type.setSelector(new ColorDrawable(Color.TRANSPARENT));

        tv_wx = (Button) v.findViewById(R.id.tv_wx);
        tv_zfb = (Button) v.findViewById(R.id.tv_zfb);
        tv_succ = (Button) v.findViewById(R.id.tv_succ);
        tv_fail = (Button) v.findViewById(R.id.tv_fail);
        tv_revers = (Button) v.findViewById(R.id.tv_revers);
        tv_refund = (Button) v.findViewById(R.id.tv_refund);
        tv_close = (Button) v.findViewById(R.id.tv_close);
        tv_tyep_refund = (Button) v.findViewById(R.id.tv_tyep_refund);

        tv_reset = (TextView) v.findViewById(R.id.tv_reset);
        ly_ok = (TextView) v.findViewById(R.id.ly_ok);
        listBut.add(tv_wx);
        listBut.add(tv_zfb);

        listBut.add(tv_succ);
        listBut.add(tv_fail);
        listBut.add(tv_revers);
        listBut.add(tv_refund);
        listBut.add(tv_close);
        listBut.add(tv_tyep_refund);

        list = new ArrayList<DynModel>();
        Object object = SharedPreUtile.readProduct("filterLstStream" + ApiConstant.bankCode + MainApplication.getMchId());

        if (object != null) {
            list = (List<DynModel>) object;
            if (null != list && list.size() > 0) {
                if (payTypeList.size() > 0) {
                    for (String s : payTypeList) {
                        for (int i = 0; i < list.size(); i++) {
                            DynModel dynModel = list.get(i);

                            if (s.equals(dynModel.getApiCode())) {
                                dynModel.setEnaled(1);
                                list.set(i, dynModel);
                            }
                        }
                    }
                }
                ly_pay_type.setVisibility(View.GONE);
                gv_pay_type.setVisibility(View.VISIBLE);
                payTypeAdape = new PayTypeAdape(activity, list);
                gv_pay_type.setAdapter(payTypeAdape);

            } else {
                ly_pay_type.setVisibility(View.VISIBLE);
                gv_pay_type.setVisibility(View.GONE);
            }
        } else {
            gv_pay_type.setVisibility(View.GONE);
            ly_pay_type.setVisibility(View.VISIBLE);
        }
    }

    class PayTypeAdape extends BaseAdapter {

        private List<DynModel> list;

        private Context context;

        private PayTypeAdape(Context context, List<DynModel> list) {
            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.activity_bill_paytype_list_item, null);
                payholder = new ViewPayTypeHolder();
                payholder.tv_content = (TextView) convertView.findViewById(R.id.tv_content);
                convertView.setTag(payholder);
            } else {
                payholder = (ViewPayTypeHolder) convertView.getTag();
            }

            DynModel dynModel = list.get(position);
            if (dynModel != null && !StringUtil.isEmptyOrNull(dynModel.getProviderName())) {
                payholder.tv_content.setText(MainApplication.getPayTypeMap().get(dynModel.getApiCode()));
                switch (dynModel.getEnabled()) {
                    case 0://默认状态
                        payholder.tv_content.setBackgroundResource(R.drawable.general_button_thickness_white_default);
                        payholder.tv_content.setTextColor(Color.parseColor("#535353"));
                        payholder.tv_content.setEnabled(true);
                        break;
                    case 1://选中
                        payholder.tv_content.setBackgroundResource(R.drawable.bg_selected);
                        payholder.tv_content.setTextColor(Color.WHITE);
                        payholder.tv_content.setEnabled(true);
                        break;
                    case 2://置灰
                        payholder.tv_content.setBackgroundResource(R.drawable.general_button_thickness_white_default);
                        payholder.tv_content.setTextColor(Color.parseColor("#E3E3E3"));
                        payholder.tv_content.setEnabled(false);
                        break;

                    default:
                        break;
                }

            }

            return convertView;
        }
    }

    class ViewPayTypeHolder {
        TextView tv_content;
    }

    void resetBut(Button b) {
        //设置成未选中的状态
        b.setBackgroundResource(R.drawable.general_button_thickness_white_default);
        choiceType.remove(b.getTag().toString());
        b.setTextColor(Color.parseColor("#535353"));
        b.setEnabled(true);
    }

    void setButtonGrey(Button b) {
        //设置成灰色
        b.setBackgroundResource(R.drawable.general_button_thickness_white_default);
        choiceType.remove(b.getTag().toString());
        b.setTextColor(Color.parseColor("#E3E3E3"));
        b.setEnabled(false);
    }

    /*public SelectPicPopupWindow(final Activity context, final SelectPicPopupWindow.HandleTv handleTv) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.take_phone_dialog, null);
        this.activity = context;
        //this.handleTv = handleTv;
        TextView tv_take_pic = (TextView) mMenuView.findViewById(R.id.tv_take_pic);
        TextView tv_choice = (TextView) mMenuView.findViewById(R.id.tv_choice);

        mMenuView.findViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        tv_take_pic.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
                handleTv.takePic();
            }
        });

        tv_choice.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
                handleTv.choicePic();
            }
        });

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }*/

    /*public SelectPicPopupWindow(final Activity context, List<WalletModel> model, String choice, int restitle, OnItemClickListener itemsOnClick) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.alert_dialog, null);
        listView = (ListView) mMenuView.findViewById(R.id.list1);
        listView.setVisibility(View.VISIBLE);
        this.model = model;
        this.choice = choice;
        listView.setAdapter(new WalletBankListItemAdpter(context, model));
        title = (TextView) mMenuView.findViewById(R.id.title);
        tv_cancel = (TextView) mMenuView.findViewById(R.id.tv_cancel);
        tv_cel = (TextView) mMenuView.findViewById(R.id.tv_cel);
        tv_cancel.setVisibility(View.GONE);
        tv_cel.setVisibility(View.VISIBLE);
        iv_bank = (ImageView) mMenuView.findViewById(R.id.iv_bank);
        iv_bank.setVisibility(View.VISIBLE);
        if (restitle > 0) {
            title.setVisibility(View.VISIBLE);
            title.setText(restitle);
        }

        iv_bank.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                NewDialogInfo info = new NewDialogInfo(context, null, null, 1, null, new NewDialogInfo.HandleBtn() {

                    @Override
                    public void handleOkBtn() {
                    }

                });

                DialogHelper.resize(context, info);

                info.show();
            }
        });

        //取消按钮
        tv_cel.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                //销毁弹出框
                dismiss();
            }
        });

        listView.setOnItemClickListener(itemsOnClick);

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }
*/
    private class ViewHolder {
        private TextView tv_name;

        private ImageView iv_img;
    }

    private class WalletBankListItemAdpter extends BaseAdapter {
        private Context context;

        List<WalletModel> list;

        public WalletBankListItemAdpter(Context context, List<WalletModel> model) {
            this.list = model;
            this.context = context;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.wallet_bank_list_item, null);
                holder = new ViewHolder();
                holder.iv_img = (ImageView) convertView.findViewById(R.id.iv_img);
                holder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            WalletModel model = list.get(position);
            if (null != model) {
                if (model.getAccountCode().equals(choice)) {
                    holder.iv_img.setVisibility(View.VISIBLE);
                } else {
                    holder.iv_img.setVisibility(View.GONE);
                }
                holder.tv_name.setText(model.getBankName() + "(" + model.getSuffixIdCard() + ")");
            }
            return convertView;
        }
    }

    private ListView lv_pay_type;

    List<DynModel> listNative = new ArrayList<DynModel>();

    ViewHolderNative viewHolderNative;

    SelectPicPopupWindow.HandleNative handleNative;

    private class ViewHolderNative {
        private TextView tv_pay_name;

        private View v_line;
    }

    public interface HandleNative {
        void toPay(String type);
    }

    private class NativePayTypeAdpter extends BaseAdapter {
        private Context context;

        List<DynModel> list;

        public NativePayTypeAdpter(Context context, List<DynModel> model) {
            this.list = model;
            this.context = context;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.native_pay_list_item, null);
                viewHolderNative = new ViewHolderNative();
                viewHolderNative.tv_pay_name = (TextView) convertView.findViewById(R.id.tv_pay_name);
                convertView.setTag(viewHolderNative);
            } else {
                viewHolderNative = (ViewHolderNative) convertView.getTag();
            }

            DynModel dynModel = list.get(position);
            if (dynModel != null) {
                String language = PreferenceUtil.getString("language", "");
                if (StringUtil.isEmptyOrNull(language)) {
                    language = Locale.getDefault().toString();
                }
                if (language.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN) || language.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_HANS)) {

                    if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth","sale"),"pre_auth")){
                        viewHolderNative.tv_pay_name.setText(MainApplication.getPayTypeMap().get(dynModel.getApiCode()) + context.getString(R.string.choose_pre_auth) +context.getString(R.string.title_pay));
                    }else{
                        viewHolderNative.tv_pay_name.setText(MainApplication.getPayTypeMap().get(dynModel.getApiCode())+context.getString(R.string.title_pay));
                    }

                } else if (language.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK) || language.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO) ||
                        language.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW) || language.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_HANT)) {
                    if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth","sale"),"pre_auth")){
                        viewHolderNative.tv_pay_name.setText(MainApplication.getPayTypeMap().get(dynModel.getApiCode()) +  context.getString(R.string.choose_pre_auth) +context.getString(R.string.title_pay));
                    }else {
                        viewHolderNative.tv_pay_name.setText(MainApplication.getPayTypeMap().get(dynModel.getApiCode())  +context.getString(R.string.title_pay));
                    }
                } else {
                    if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth","sale"),"pre_auth")) {
                        viewHolderNative.tv_pay_name.setText(MainApplication.getPayTypeMap().get(dynModel.getApiCode()) + " " + context.getString(R.string.choose_pre_auth)+" " +context.getString(R.string.title_pay));
                    }else{
                        viewHolderNative.tv_pay_name.setText(MainApplication.getPayTypeMap().get(dynModel.getApiCode()) + " " +context.getString(R.string.title_pay));
                    }
                }
            }

            return convertView;
        }
    }

    public SelectPicPopupWindow(Activity context, String type, final SelectPicPopupWindow.HandleNative handleNative) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.alert_dialog, null);
        tv_cancel = (TextView) mMenuView.findViewById(R.id.tv_cel);
        lv_pay_type = (ListView) mMenuView.findViewById(R.id.lv_pay_type);
        this.handleNative = handleNative;

        Object object;
        //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth","sale");
        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(saleOrPreauth,"pre_auth")){
            object = SharedPreUtile.readProduct("dynPayType_pre_auth" + ApiConstant.bankCode + MainApplication.getMchId());
        }else{
            object = SharedPreUtile.readProduct("dynPayType" + ApiConstant.bankCode + MainApplication.getMchId());
        }
//        Object object = SharedPreUtile.readProduct("dynPayType" + ApiConstant.bankCode + MainApplication.getMchId());

        if (object != null) {
            listNative = (List<DynModel>) object;
            if (null != listNative && listNative.size() > 0) {
                lv_pay_type.setVisibility(View.VISIBLE);
                NativePayTypeAdpter nativePayTypeAdpter = new NativePayTypeAdpter(context, listNative);
                lv_pay_type.setAdapter(nativePayTypeAdpter);
            }
        } else {
            lv_pay_type.setVisibility(View.GONE);
        }

        lv_pay_type.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int postion, long arg3) {
                DynModel dynModel = listNative.get(postion);
                if (dynModel != null) {
                    dismiss();
                    handleNative.toPay(dynModel.getApiCode());
                }
            }
        });

        //取消按钮
        tv_cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //销毁弹出框
                dismiss();
            }
        });

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });

    }
}
