/*
 * 文 件 名:  EmpManagerActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2015-3-12
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;

/**
 * @author jamy
 */
public class AddCodeActivity extends BaseActivity {

    private static final String TAG = AddCodeActivity.class.getSimpleName();
    private Button mStartBtn;

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_tutorial);
        MainApplication.getInstance().getListActivities().add(this);
        initView();
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(R.string.bind_qr_code);
        titleBar.setLeftButtonVisible(true);
    }

    private void initView() {
        mStartBtn = findViewById(R.id.tv_start);
        mStartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPage(SetCashierDeskNameActivity.class);
            }
        });
    }

}
