package cn.swiftpass.enterprise.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.widget.TitleBar;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-10-9
 * Time: 下午5:36
 * To change this template use File | Settings | File Templates.
 */
public class AboutUsActivity extends TemplateActivity
{
    private TextView tvContent, tvContent2;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        initView();
    }
    
    private void initView()
    {
        setContentView(R.layout.activity_aboutus);
        tvContent = getViewById(R.id.tv_content);
        tvContent2 = getViewById(R.id.tv_content2);
        //        String content1 =
        //            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;深圳市威富通科技有限公司是腾讯财付通金融和交通行业的授权合作伙伴，专注为企业提供基于微信+财付通平台的系统开发、运营及服务。";
        //        String content2 =
        //            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;威富通同时也是腾讯财付通移动支付产品的全国合作伙伴， 为行业客户提供包括微信支付在内的财付通移动支付产品、服务和行业解决方案。";
        //        tvContent.setText(Html.fromHtml(content1));
        //        tvContent2.setText(Html.fromHtml(content2));
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_aboutus);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
            }
            
            @Override
            public void onRightLayClick()
            {
                // TODO Auto-generated method stub
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                // TODO Auto-generated method stub
                
            }
        });
    }
    
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    public static void startActivity(Context context)
    {
        Intent it = new Intent();
        it.setClass(context, AboutUsActivity.class);
        context.startActivity(it);
    }
}
