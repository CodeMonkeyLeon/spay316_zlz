package cn.swiftpass.enterprise.mvp.base

/**
 * @author lizheng.zhao
 * @date 2022/11/15
 *
 * @你应该是一场梦我应该是一阵风
 */
interface BaseView {

    /**
     * 展示loading
     *
     * type 用于区分不同接口的 差异话loading弹框
     */
    fun showLoading(id: Int, type: Int)


    /**
     * 隐藏loading
     */
    fun dismissLoading(type: Int)
}