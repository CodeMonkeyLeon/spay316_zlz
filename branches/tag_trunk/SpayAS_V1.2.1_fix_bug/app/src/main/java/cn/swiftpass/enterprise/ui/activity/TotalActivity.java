/*
 * 文 件 名:  TotalActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2015-3-14
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.user.DealScatterFragmentActivity;
import cn.swiftpass.enterprise.ui.adapter.TotalActivityAdapter;
import cn.swiftpass.enterprise.utils.DateUtil;

/**
 * 统计
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2015-3-14]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class TotalActivity extends BaseActivity implements OnClickListener
{
    private TextView ll_total_date_tv, totle_count_tv;
    
    private TextView totle_money_tv;
    
    private Button deal_scatter_btn, deal_trend_btn;
    
    private TotalActivityAdapter adapter;
    
    private ListView total_listview;
    
    private long searchDate;
    
    private LinearLayout totle_money_more_btn;
    
    private List<Map<String, String>> list;
    
    /** {@inheritDoc} */
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistic_data);
        
        initObject();
        initView();
        initListener();
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initObject()
    {
        searchDate = System.currentTimeMillis();
        list = new ArrayList<Map<String, String>>();
        for (int a = 0; a < 5; a++)
        {
            list.add(new HashMap<String, String>());
        }
        adapter = new TotalActivityAdapter(this, list);
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initListener()
    {
        deal_scatter_btn.setOnClickListener(this);
        totle_money_more_btn.setOnClickListener(this);
    }
    
    /** 加载控件
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView()
    {
        deal_scatter_btn = getViewById(R.id.deal_scatter_btn);
        ll_total_date_tv = getViewById(R.id.ll_total_date_tv);
        totle_count_tv = getViewById(R.id.totle_count_tv);
        
        totle_money_tv = getViewById(R.id.totle_money_tv);
        deal_trend_btn = getViewById(R.id.deal_trend_btn);
        total_listview = getViewById(R.id.total_listview);
        
        totle_money_more_btn = getViewById(R.id.totle_money_more_btn);
        
        ll_total_date_tv.setText("(" + DateUtil.formatYYMD(searchDate) + ")");
        total_listview.setAdapter(adapter);
        
    }
    
    /** {@inheritDoc} */
    
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.deal_scatter_btn://交易分布
                showPage(DealScatterFragmentActivity.class);
                break;
            case R.id.total_search_date://选择日期
                showDatetimeDialog();
                break;
            case R.id.totle_money_more_btn://查看退款详情
                showPage(DealStreamNew.class);
                break;
            default:
                break;
        }
    }
    
    private Calendar c = null;
    
    public void showDatetimeDialog()
    {
        c = Calendar.getInstance();
        c.setTimeInMillis(searchDate);
        
        dialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener()
        {
            public void onDateSet(DatePicker dp, int year, int month, int dayOfMonth)
            {
                c.set(year, month, dayOfMonth);
                searchDate = c.getTimeInMillis();
                ll_total_date_tv.setText("(" + DateUtil.formatYYMD(searchDate) + ")");
                
                // loadMoreDate();
                
            }
        }, c.get(Calendar.YEAR), // 传入年份
            c.get(Calendar.MONTH), // 传入月份
            c.get(Calendar.DAY_OF_MONTH) // 传入天数*/
            );
        dialog.show();
    }
}
