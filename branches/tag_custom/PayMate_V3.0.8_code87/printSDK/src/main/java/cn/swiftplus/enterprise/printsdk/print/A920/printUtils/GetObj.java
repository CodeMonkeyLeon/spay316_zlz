package cn.swiftplus.enterprise.printsdk.print.A920.printUtils;
import android.util.Log;
import com.pax.dal.IDAL;

import cn.swiftplus.enterprise.printsdk.print.A920.POSA920Client;


public class GetObj {
    private static IDAL dal;

    // 获取IDal dal对象
    public static IDAL getDal() {
        dal = POSA920Client.idal;
        if (dal == null) {
            Log.e("NeptuneLiteDemo", "dal is null");
        }
        return dal;
    }
}
