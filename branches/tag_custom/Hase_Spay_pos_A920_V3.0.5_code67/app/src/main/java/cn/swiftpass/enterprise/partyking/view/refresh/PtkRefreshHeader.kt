package cn.swiftpass.enterprise.partyking.view.refresh

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import cn.swiftpass.enterprise.intl.R
import com.scwang.smartrefresh.layout.api.RefreshHeader
import com.scwang.smartrefresh.layout.api.RefreshKernel
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.constant.RefreshState
import com.scwang.smartrefresh.layout.constant.SpinnerStyle

class PtkRefreshHeader(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyle: Int = 0
) : LinearLayout(context, attributeSet, defStyle), RefreshHeader {

    companion object {
        const val TAG = "PtkRefreshHeader"
    }

    private lateinit var mImgRefresh: ImageView
    private lateinit var mProgressBarRefresh: ProgressBar
    private lateinit var mTvContent: TextView
    private lateinit var mTvSubContent: TextView


    private var mPullDownToRefreshText = "PullDownToRefresh"
    private var mReleaseToRefreshText = "ReleaseToRefresh"
    private var mRefreshingText = "Refreshing..."


    fun setRefreshText(pullDownText: String, releaseText: String, refreshingText: String) {
        mPullDownToRefreshText = pullDownText
        mReleaseToRefreshText = releaseText
        mRefreshingText = refreshingText
    }


    constructor(context: Context) : this(context, null, 0) {

    }

    constructor(context: Context, attributeSet: AttributeSet?) : this(context, attributeSet, 0) {

    }


    init {
        initView()
    }


    private fun initView() {
        LayoutInflater.from(context).inflate(R.layout.pull_to_refresh_header, this)
        mImgRefresh = findViewById(R.id.pull_to_refresh_image)
        mProgressBarRefresh = findViewById(R.id.pull_to_refresh_progress)
        mTvContent = findViewById(R.id.pull_to_refresh_text)
        mTvSubContent = findViewById(R.id.pull_to_refresh_sub_text)
        mTvSubContent.visibility = View.GONE
    }


    override fun onStateChanged(
        refreshLayout: RefreshLayout,
        oldState: RefreshState,
        newState: RefreshState
    ) {
        when (newState) {
            RefreshState.PullDownToRefresh -> {
                //下拉刷新
                mTvContent.text = mPullDownToRefreshText
                mImgRefresh.visibility = View.VISIBLE
                mProgressBarRefresh.visibility = View.GONE
                mImgRefresh.setImageResource(R.drawable.arrow_down)
            }
            RefreshState.ReleaseToRefresh -> {
                //释放刷新
                mTvContent.text = mReleaseToRefreshText
                mImgRefresh.visibility = View.VISIBLE
                mProgressBarRefresh.visibility = View.GONE
                mImgRefresh.setImageResource(R.drawable.arrow_up)
            }
            RefreshState.Refreshing -> {
                //刷新中
                mTvContent.text = mRefreshingText
                mProgressBarRefresh.visibility = View.VISIBLE
                mImgRefresh.visibility = View.GONE
            }
        }
    }

    /**
     * 获取真实视图（必须返回，不能为null）一般就是返回当前自定义的view
     */
    override fun getView() = this

    /**
     * 获取变换方式（必须指定一个：平移、拉伸、固定、全屏）,Translate指平移，大多数都是平移
     */
    override fun getSpinnerStyle() = SpinnerStyle.Translate

    override fun isSupportHorizontalDrag() = false

    override fun onFinish(refreshLayout: RefreshLayout, success: Boolean) = 0

    override fun setPrimaryColors(vararg colors: Int) {

    }

    override fun onInitialized(kernel: RefreshKernel, height: Int, maxDragHeight: Int) {

    }

    override fun onMoving(
        isDragging: Boolean,
        percent: Float,
        offset: Int,
        height: Int,
        maxDragHeight: Int
    ) {

    }

    override fun onReleased(refreshLayout: RefreshLayout, height: Int, maxDragHeight: Int) {

    }

    override fun onStartAnimator(refreshLayout: RefreshLayout, height: Int, maxDragHeight: Int) {

    }

    override fun onHorizontalDrag(percentX: Float, offsetX: Int, offsetMax: Int) {

    }
}