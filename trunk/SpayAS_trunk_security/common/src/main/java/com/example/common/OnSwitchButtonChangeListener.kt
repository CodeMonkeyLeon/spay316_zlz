package com.example.common

interface OnSwitchButtonChangeListener {
    fun onChange(isOpen: Boolean)
}