/**
 * Project Name:Spay_wftintl
 * File Name:GTPushService.java
 * Package Name:cn.swiftpass.enterprise.broadcast
 * Date:2017-9-21上午10:08:59
 *
*/

package cn.swiftpass.enterprise.broadcast;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.igexin.sdk.GTServiceManager;

/**
 * ClassName:GTPushService
 * Function: TODO ADD FUNCTION.
 * Date:     2017-9-21 上午10:08:59 
 * @author   admin 
 */
public class GTPushService extends Service
{
    
    public static final String TAG = "hehui";
    
    @Override
    public void onCreate()
    {
        super.onCreate();
        GTServiceManager.getInstance().onCreate(this);
    }
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        super.onStartCommand(intent, flags, startId);
        return GTServiceManager.getInstance().onStartCommand(this, intent, flags, startId);
    }
    
    @Override
    public IBinder onBind(Intent intent)
    {
        return GTServiceManager.getInstance().onBind(intent);
    }
    
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        GTServiceManager.getInstance().onDestroy();
    }
    
    @Override
    public void onLowMemory()
    {
        super.onLowMemory();
        GTServiceManager.getInstance().onLowMemory();
    }
}
