package cn.swiftpass.enterprise.mvp.presenter.fragment

import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bean.QRCodeBeanList
import cn.swiftpass.enterprise.bussiness.model.MerchantTempDataModel
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.fragment.TabSettingContract
import cn.swiftpass.enterprise.utils.JsonUtil
import com.example.common.entity.EmptyData
import com.example.common.sentry.SentryUtils.uploadNetInterfaceException
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/11/16
 *
 * @你应该是一场梦我应该是一阵风
 */
class TabSettingPresenter : TabSettingContract.Presenter {


    private var mView: TabSettingContract.View? = null


    override fun getCodeList(page: Int, bindUserId: String?) {

        mView?.let {
            it.showLoading(R.string.public_loading, ParamsConstants.COMMON_LOADING)
        }

        AppClient.getCodeList(
            MainApplication.getInstance().getMchId(),
            MainApplication.getInstance().getUserId().toString(),
            bindUserId,
            page.toString(),
            20.toString(),
            System.currentTimeMillis().toString(),
            object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {
                override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                    mView?.let { view ->
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { error ->
                            view.getCodeListFailed(error.message)
                        }
                    }
                }

                override fun onResponse(res: CommonResponse?) {
                    mView?.let { view ->
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        res?.let { response ->

                            try {
                                val order = JsonUtil.jsonToBean(
                                    response.message,
                                    QRCodeBeanList::class.java
                                ) as QRCodeBeanList
                                view.getCodeListSuccess(order.data)
                            } catch (e: Exception) {
                                uploadNetInterfaceException(
                                    e,
                                    "spay/qrCodeQuery",
                                    "QRCodeBeanList json解析异常",
                                    ""
                                )
                                view.getCodeListFailed(response.message)
                            }
                        }
                    }
                }
            }
        )


    }

    override fun queryMerchantDataByTel() {

        mView?.let {
            it.showLoading(R.string.public_loading, ParamsConstants.COMMON_LOADING)
        }

        var phone = ""
        MainApplication.getInstance().userInfo?.let { u ->
            u.phone?.let { p ->
                phone = p
            }
        }

        AppClient.queryMerchantDataByTel(
            MainApplication.getInstance().getMchId(),
            phone,
            System.currentTimeMillis().toString(),
            object :
                CallBackUtil.CallBackCommonResponse<MerchantTempDataModel>(MerchantTempDataModel()) {

                override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                    mView?.let { view ->
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { error ->
                            view.queryMerchantDataByTelFailed(error.message)
                        }
                    }
                }


                override fun onResponse(res: CommonResponse?) {

                    mView?.let { view ->
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        res?.let { response ->
                            //如果centerRate值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"centerRate\":\"\"",
                                "\"centerRate\":[]"
                            )
                            val model = JsonUtil.jsonToBean(
                                response.message,
                                MerchantTempDataModel::class.java
                            ) as MerchantTempDataModel
                            model.merchantTypeName = model.merchantType.toString()
                            model.bankUserName = model.accountName
                            model.identityNo = model.accountIdEntity
                            model.bank = model.bankNumberCode
                            model.branchBankName = model.bankName
                            model.idCardJustPic = model.linencePhoto
                            model.licensePic = model.indentityPhoto
                            model.idCode = model.protocolPhoto
                            view.queryMerchantDataByTelSuccess(model)
                        }
                    }


                }
            }
        )

    }

    override fun getVersionCode(isDisplayIcon: Boolean) {
//        mView?.let {
//            it.showLoading(R.string.show_new_version_loading, ParamsConstants.COMMON_LOADING)
//        }
//        AppClient.getVersionCode(
//            AppHelper.getUpdateVersionCode(MainApplication.getInstance()).toString() + "",
//            BuildConfig.bankCode,
//            System.currentTimeMillis().toString(),
//            object : CallBackUtil.CallBackCommonResponse<UpgradeInfo>(UpgradeInfo()) {
//
//                override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
//                    mView?.let { view ->
//                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
//                        commonResponse?.let { error ->
//                            if (TextUtils.equals(
//                                    ParamsConstants.ERROR_CODE_400,
//                                    error.result
//                                ) || TextUtils.equals(
//                                    ParamsConstants.ERROR_CODE_402,
//                                    error.result
//                                )
//                            ) {
//                                view.getVersionCodeSuccess(null, isDisplayIcon)
//                            } else {
//                                view.getVersionCodeFailed(error.result)
//                            }
//                        }
//                    }
//                }
//
//                override fun onResponse(res: CommonResponse?) {
//                    mView?.let { view ->
//                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
//                        res?.let { response ->
//
//
//                            if (TextUtils.equals(response.result, "200")) {
//                                try {
//                                    val upgradeInfoStr = response.message
//                                    val info = UpgradeInfo()
//                                    val json = JSONObject(upgradeInfoStr)
//                                    info.dateTime =
//                                        Utils.Long.tryParse(json.optString("updated_at"), 0)
//                                    info.message = json.optString("subDesc", "")
//                                    info.version =
//                                        Utils.Integer.tryParse(json.optString("verCode"), 0)
//                                    info.mustUpgrade = Utils.Integer.tryParse(
//                                        json.optString("isUpdate"),
//                                        0
//                                    ) == 0
//                                    info.versionName = json.optString("verName")
//                                    info.fileMd5 = json.optString("fileMd5")
//                                    MainApplication.getInstance().setFileMd5(info.fileMd5)
//                                    info.url = json.optString("filePath", "")
//                                    MainApplication.getInstance().setFileMd5(info.fileMd5)
//                                    view.getVersionCodeSuccess(info, isDisplayIcon)
//                                } catch (e: Exception) {
//                                    SentryUtils.uploadNetInterfaceException(
//                                        e,
//                                        MainApplication.getInstance().baseUrl + "spay/upgrade/checkinVersion",
//                                        e.toString(),
//                                        ""
//                                    )
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        )
    }

    override fun attachView(view: TabSettingContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}