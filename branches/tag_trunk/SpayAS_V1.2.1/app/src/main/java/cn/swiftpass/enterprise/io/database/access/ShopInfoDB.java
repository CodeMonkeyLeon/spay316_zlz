package cn.swiftpass.enterprise.io.database.access;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.model.ShopModel;
import cn.swiftpass.enterprise.io.database.table.ShopTable;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.util.List;

/**
 * 商铺信息
 * User: Alan
 * Date: 14-2-28
 * Time: 上午10:26
 * To change this template use File | Settings | File Templates.
 */
public class ShopInfoDB {
    private static Dao<ShopModel, Integer> shopInfoDB;
    private ShopInfoDB() {
        try {
            shopInfoDB = MainApplication.getContext().getHelper().getDao(ShopModel.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static ShopInfoDB instance;
    public static ShopInfoDB getInstance() {
        if (instance == null) {
            instance = new ShopInfoDB();
        }
        return instance;
    }


    /***
     * 保存
     */
    public void save(ShopModel shopModel) throws SQLException
    {
        shopModel.uId = LocalAccountManager.getInstance().getUID();
        shopModel.addTime = System.currentTimeMillis();
        //shopModel.strStartTime =  DateUtil.formatTime(shopModel.startTime.getTime(), "HH:mm");
        //shopModel.strEndTime =   DateUtil.formatTime(shopModel.endTime.getTime(), "HH:mm");
        shopModel.strStartTime = shopModel.startTime;
        shopModel.strEndTime =shopModel.endTime;
        deleteById();
        shopInfoDB.createOrUpdate(shopModel);
    }

    public boolean deleteById() throws SQLException
    {
        long uId = LocalAccountManager.getInstance().getUID();
        DeleteBuilder<ShopModel,Integer> builder = shopInfoDB.deleteBuilder();
        Where<ShopModel,Integer> where = builder.where();
        where.eq(ShopTable.COLUMN_UID, uId);
        return shopInfoDB.delete(builder.prepare()) > 0 ;
    }

    /**
     * 查询自己的店铺信息
     * 添加时间1个之前的
     * */
    public ShopModel queryShopInfo(long uId) {
        QueryBuilder<ShopModel, Integer> builder = shopInfoDB.queryBuilder();
        try {
            Where<ShopModel, Integer> where = builder.where();
            where.eq(ShopTable.COLUMN_UID, uId);
            where.and();
            //long time = System.currentTimeMillis() - 1 * 60 * 60 * 1000;
           // where.le(ShopTable.COLUMN_ADD_TIME, time);
            builder.orderBy(ShopTable.COLUMN_ADD_TIME,false);
            List<ShopModel> list = shopInfoDB.query(builder.prepare());
            if (list != null && list.size() > 0) {
                ShopModel shopModel = list.get(0);
                long currentTime = System.currentTimeMillis();
                long  time = (( currentTime / 1000) - (shopModel.addTime /1000 )) / 60;
                if(time >= 60 ) {
                   return null;
                }
                return  shopModel;
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
        return null;
    }
}
