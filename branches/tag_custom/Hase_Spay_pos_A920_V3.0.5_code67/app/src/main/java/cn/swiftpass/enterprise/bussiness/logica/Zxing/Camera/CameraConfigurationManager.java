package cn.swiftpass.enterprise.bussiness.logica.Zxing.Camera;

import android.content.Context;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.os.Build;
import android.util.Log;
import android.view.SurfaceView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;

import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.ScreenUtils;

final class CameraConfigurationManager
{
    
    private static final String TAG = CameraConfigurationManager.class.getSimpleName();
    
    private static final int TEN_DESIRED_ZOOM = 27;
    
    //private static final int DESIRED_SHARPNESS = 30;
    
    private static final Pattern COMMA_PATTERN = Pattern.compile(",");
    
    private final Context context;
    
    private Point screenResolution;
    
    //    private Point cameraResolution;
    
    private Camera.Size cameraResolution;
    
    private int previewFormat;
    
    private String previewFormatString;
    
    CameraConfigurationManager(Context context)
    {
        this.context = context;
    }

    private Size getOptimalPreviewSize(List<Size> sizes, int w, int h)
    {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double)w / h;
        if (sizes == null)
            return null;

        Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        // Try to find an size match aspect ratio and size
        for (Size size : sizes)
        {
            double ratio = (double)size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;
            if (Math.abs(size.height - targetHeight) < minDiff)
            {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        // Cannot find the one match the aspect ratio, ignore the requirement
        if (optimalSize == null)
        {
            minDiff = Double.MAX_VALUE;
            for (Size size : sizes)
            {
                if (Math.abs(size.height - targetHeight) < minDiff)
                {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    /**
     * Reads, one time, values from the camera that are needed by the app.
     */
    //    void initFromCameraParameters(Camera camera, SurfaceView surfaceView)
    //    {
    //        Camera.Parameters parameters = camera.getParameters();
    //        previewFormat = parameters.getPreviewFormat();
    //
    //        previewFormatString = parameters.get("preview-format");
    //        Logger.d(TAG, "Default preview format: " + previewFormat + '/' + previewFormatString);
    //        List<Camera.Size> sizeList = parameters.getSupportedPreviewSizes();//获取所有支持的camera尺寸
    //        Camera.Size optionSize = getOptimalPreviewSize(sizeList, surfaceView.getWidth(), surfaceView.getHeight());//获取一个最为适配的屏幕尺寸
    //        WindowManager manager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
    //        Display display = manager.getDefaultDisplay();
    //        screenResolution = new Point(display.getWidth(), display.getHeight());
    //        //        screenResolution = new Point(optionSize.width, optionSize.height);
    //        Logger.d(TAG, "Screen resolution: " + screenResolution);
    //        cameraResolution = getCameraResolution(parameters, screenResolution);
    //        Logger.d(TAG, "Camera resolution: " + screenResolution);
    //    }

    /**
     * Reads, one time, values from the camera that are needed by the app.
     */
    void initFromCameraParametersNew(Camera camera, SurfaceView surfaceView)
    {
        Camera.Parameters parameters = camera.getParameters();
        cameraResolution =
            findCloselySize(ScreenUtils.getScreenWidth(context),
                ScreenUtils.getScreenHeight(context),
                parameters.getSupportedPreviewSizes());
    }
    
    /**
     * 通过对比得到与宽高比最接近的尺寸（如果有相同尺寸，优先选择）
     *
     * @param surfaceWidth 需要被进行对比的原宽
     * @param surfaceHeight 需要被进行对比的原高
     * @param preSizeList 需要对比的预览尺寸列表
     * @return 得到与原宽高比例最接近的尺寸
     */
    protected Camera.Size findCloselySize(int surfaceWidth, int surfaceHeight, List<Camera.Size> preSizeList)
    {
        
        Collections.sort(preSizeList, new SizeComparator(surfaceWidth, surfaceHeight));
        return preSizeList.get(0);
    }
    
    /**
     * 预览尺寸与给定的宽高尺寸比较器。首先比较宽高的比例，在宽高比相同的情况下，根据宽和高的最小差进行比较。
     */
    private static class SizeComparator implements Comparator<Camera.Size>
    {
        
        private final int width;
        
        private final int height;
        
        private final float ratio;
        
        SizeComparator(int width, int height)
        {
            if (width < height)
            {
                this.width = height;
                this.height = width;
            }
            else
            {
                this.width = width;
                this.height = height;
            }
            this.ratio = (float)this.height / this.width;
        }
        
        @Override
        public int compare(Camera.Size size1, Camera.Size size2)
        {
            int width1 = size1.width;
            int height1 = size1.height;
            int width2 = size2.width;
            int height2 = size2.height;
            
            float ratio1 = Math.abs((float)height1 / width1 - ratio);
            float ratio2 = Math.abs((float)height2 / width2 - ratio);
            int result = Float.compare(ratio1, ratio2);
            if (result != 0)
            {
                return result;
            }
            else
            {
                int minGap1 = Math.abs(width - width1) + Math.abs(height - height1);
                int minGap2 = Math.abs(width - width2) + Math.abs(height - height2);
                return minGap1 - minGap2;
            }
        }
    }
    
    /**
     * Sets the camera up to take preview images which are used for both preview and decoding.
     * We detect the preview format here so that buildLuminanceSource() can build an appropriate
     * LuminanceSource subclass. In the future we may want to force YUV420SP as it's the smallest,
     * and the planar Y can be used for barcode scanning without a copy in some cases.
     */
    void setDesiredCameraParameters(Camera camera)
    {
        Camera.Parameters parameters = camera.getParameters();

        parameters.setPreviewSize(cameraResolution.width, cameraResolution.height);
        setFlash(parameters);
        setZoom(parameters);
        camera.setDisplayOrientation(90);
        
        //setSharpness(parameters);
        camera.setParameters(parameters);
    }
    
    Camera.Size getCameraResolution()
    {
        return cameraResolution;
    }
    
    Point getScreenResolution()
    {
        return screenResolution;
    }

    int getPreviewFormat()
    {
        return previewFormat;
    }

    String getPreviewFormatString()
    {
        return previewFormatString;
    }

    private static Point getCameraResolution(Camera.Parameters parameters, Point screenResolution)
    {

        String previewSizeValueString = parameters.get("preview-size-values");
        // saw this on Xperia
        if (previewSizeValueString == null)
        {
            previewSizeValueString = parameters.get("preview-size-value");
        }

        Point cameraResolution = null;

        if (previewSizeValueString != null)
        {
            Logger.d(TAG, "preview-size-values parameter: " + previewSizeValueString);
            cameraResolution = findBestPreviewSizeValue(previewSizeValueString, screenResolution);
        }

        if (cameraResolution == null)
        {
            // Ensure that the camera resolution is a multiple of 8, as the screen may not be.
            cameraResolution = new Point((screenResolution.x >> 3) << 3, (screenResolution.y >> 3) << 3);
        }

        return cameraResolution;
    }

    private static Point findBestPreviewSizeValue(CharSequence previewSizeValueString, Point screenResolution)
    {
        int bestX = 0;
        int bestY = 0;
        int diff = Integer.MAX_VALUE;
        for (String previewSize : COMMA_PATTERN.split(previewSizeValueString))
        {

            previewSize = previewSize.trim();
            int dimPosition = previewSize.indexOf('x');
            if (dimPosition < 0)
            {
                continue;
            }

            int newX;
            int newY;
            try
            {
                newX = Integer.parseInt(previewSize.substring(0, dimPosition));
                newY = Integer.parseInt(previewSize.substring(dimPosition + 1));
            }
            catch (NumberFormatException nfe)
            {
                continue;
            }

            int newDiff = Math.abs(newX - screenResolution.x) + Math.abs(newY - screenResolution.y);
            if (newDiff == 0)
            {
                bestX = newX;
                bestY = newY;
                break;
            }
            else if (newDiff < diff)
            {
                bestX = newX;
                bestY = newY;
                diff = newDiff;
            }

        }

        if (bestX > 0 && bestY > 0)
        {
            return new Point(bestX, bestY);
        }
        return null;
    }

    private static int findBestMotZoomValue(CharSequence stringValues, int tenDesiredZoom)
    {
        int tenBestValue = 0;
        for (String stringValue : COMMA_PATTERN.split(stringValues))
        {
            stringValue = stringValue.trim();
            double value;
            try
            {
                value = Double.parseDouble(stringValue);
            }
            catch (NumberFormatException nfe)
            {
                return tenDesiredZoom;
            }
            int tenValue = (int)(10.0 * value);
            if (Math.abs(tenDesiredZoom - value) < Math.abs(tenDesiredZoom - tenBestValue))
            {
                tenBestValue = tenValue;
            }
        }
        return tenBestValue;
    }
    
    private void setFlash(Camera.Parameters parameters)
    {
        // FIXME: This is a hack to turn the flash off on the Samsung Galaxy.
        // And this is a hack-hack to work around a different value on the Behold II
        // Restrict Behold II check to Cupcake, per Samsung's advice
        //if (Build.MODEL.contains("Behold II") &&
        //    CameraManager.SDK_INT == Build.VERSION_CODES.CUPCAKE) {
        if (Build.MODEL.contains("Behold II") && CameraManager.SDK_INT == 3)
        { // 3 = Cupcake
            parameters.set("flash-value", 1);
        }
        else
        {
            parameters.set("flash-value", 2);
        }
        // This is the standard setting to turn the flash off that all devices should honor.
        parameters.set("flash-mode", "off");
    }
    
    private void setZoom(Camera.Parameters parameters) {

        String zoomSupportedString = parameters.get("zoom-supported");
        if (zoomSupportedString != null && !Boolean.parseBoolean(zoomSupportedString)) {
            return;
        }

        int tenDesiredZoom = TEN_DESIRED_ZOOM;

        String maxZoomString = parameters.get("max-zoom");
        if (maxZoomString != null) {
            try {
                int tenMaxZoom = (int) (10.0 * Double.parseDouble(maxZoomString));
                if (tenDesiredZoom > tenMaxZoom) {
                    tenDesiredZoom = tenMaxZoom;
                }
            } catch (NumberFormatException nfe) {
                Log.e(TAG, Log.getStackTraceString(nfe));
            }
        }

        String takingPictureZoomMaxString = parameters.get("taking-picture-zoom-max");
        if (takingPictureZoomMaxString != null) {
            try {
                int tenMaxZoom = Integer.parseInt(takingPictureZoomMaxString);
                if (tenDesiredZoom > tenMaxZoom) {
                    tenDesiredZoom = tenMaxZoom;
                }
            } catch (NumberFormatException nfe) {
                Log.e(TAG, Log.getStackTraceString(nfe));
            }
        }

        String motZoomValuesString = parameters.get("mot-zoom-values");
        if (motZoomValuesString != null) {
            tenDesiredZoom = findBestMotZoomValue(motZoomValuesString, tenDesiredZoom);
        }

        String motZoomStepString = parameters.get("mot-zoom-step");
        if (motZoomStepString != null) {
            try {
                double motZoomStep = Double.parseDouble(motZoomStepString.trim());
                int tenZoomStep = (int) (10.0 * motZoomStep);
                if (tenZoomStep > 1) {
                    tenDesiredZoom -= tenDesiredZoom % tenZoomStep;
                }
            } catch (NumberFormatException nfe) {
                Log.e(TAG, Log.getStackTraceString(nfe));
            }
        }

        // Set zoom. This helps encourage the user to pull back.
        // Some devices like the Behold have a zoom parameter
        if (maxZoomString != null || motZoomValuesString != null) {
            parameters.set("zoom", String.valueOf(tenDesiredZoom / 10.0));
        }

        // Most devices, like the Hero, appear to expose this zoom parameter.
        // It takes on values like "27" which appears to mean 2.7x zoom
        if (takingPictureZoomMaxString != null) {
            parameters.set("taking-picture-zoom", tenDesiredZoom);
        }
    }
    
    /*
    private void setSharpness(Camera.Parameters parameters) {

      int desiredSharpness = DESIRED_SHARPNESS;

      String maxSharpnessString = parameters.get("sharpness-max");
      if (maxSharpnessString != null) {
        try {
          int maxSharpness = Integer.parseInt(maxSharpnessString);
          if (desiredSharpness > maxSharpness) {
            desiredSharpness = maxSharpness;
          }
        } catch (NumberFormatException nfe) {
          Log.w(TAG, "Bad sharpness-max: " + maxSharpnessString);
        }
      }

      parameters.set("sharpness", desiredSharpness);
    }
     */
}
