package cn.swiftpass.enterprise.newmvp.http;


import com.trello.rxlifecycle2.LifecycleTransformer;

import java.util.concurrent.TimeUnit;

import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.newmvp.base.BaseBean;
import cn.swiftpass.enterprise.newmvp.config.AppConfig;
import cn.swiftpass.enterprise.newmvp.http.interceptor.HeaderInterceptor;
import cn.swiftpass.enterprise.newmvp.http.interceptor.ResultInterceptor;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * 创建人：caoxiaoya
 * 时间：2019/4/22
 * 描述：全局Http网络请求
 * 备注：静态单例
 */
public class HttpManager {

    private Retrofit mRetrofit;

    private HttpManager() {
        // 创建Retrofit
        mRetrofit = new Retrofit.Builder()
                .client(getOkHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(AppConfig.BASE_URL)
                .build();
    }

    private static class SingletonHolder {
        private final static HttpManager instance = new HttpManager();
    }

    /**
     * 静态单例请求
     */
    public static HttpManager getInstance() {
        return SingletonHolder.instance;
    }

    private OkHttpClient getOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder()
                //设置超时时间
                .connectTimeout(AppConfig.TIMEOUT, TimeUnit.SECONDS)
                //设置读取超时时间
                .readTimeout(AppConfig.TIMEOUT, TimeUnit.SECONDS)
                //设置写入超时时间
                .writeTimeout(AppConfig.TIMEOUT, TimeUnit.SECONDS)
                //拿到json赋值给BaseBean-data
                .addInterceptor(new ResultInterceptor());
         //debug模式显示请求信息
        if (BuildConfig.DEBUG) {
            // Log信息拦截器
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            //这里可以选择拦截级别
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            //设置 Debug Log 模式
            builder.addInterceptor(loggingInterceptor);
        }
       //请求添加Spay请求必要的头部参数
        builder.addInterceptor(new HeaderInterceptor());
        return builder.build();
    }

    public <T> T create(Class<T> service) {
        return mRetrofit.create(service);
    }

    /**
     * ransformer用来注销当前请求，防止RxJava持有View导致内存泄露
     *
     * @param observable
     * @param observer
     * @param transformer
     */
    public <T> void httpRxRequest(Observable<BaseBean<T>> observable, Observer<BaseBean<T>> observer, LifecycleTransformer transformer) {
        observable.subscribeOn(Schedulers.io()).
                compose(transformer).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(observer);
    }

    /**
     * ransformer用来注销当前请求，防止RxJava持有View导致内存泄露
     *
     * @param observable
     * @param observer
     * @param transformer
     */
    public <T> void httpRxRequest(Observable<BaseBean<T>> observable, Observer<BaseBean<T>> observer) {
        observable.subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(observer);
    }


}
