package cn.swiftpass.enterprise.ui.paymentlink.interfaces

interface OnReceiveFilterClickListener {
    fun onClick(type:Int)
}