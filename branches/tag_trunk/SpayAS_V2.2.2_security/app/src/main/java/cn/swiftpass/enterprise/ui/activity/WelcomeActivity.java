package cn.swiftpass.enterprise.ui.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.tencent.stat.StatService;
import com.ziyeyouhu.library.KeyboardTouchListener;
import com.ziyeyouhu.library.KeyboardUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.upgrade.UpgradeManager;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo;
import cn.swiftpass.enterprise.bussiness.model.ErrorMsg;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.live.ForegroundLiveService;
import cn.swiftpass.enterprise.ui.activity.live.JobSchedulerManager;
import cn.swiftpass.enterprise.ui.activity.live.PlayerMusicService;
import cn.swiftpass.enterprise.ui.activity.setting.SettingCDNActivity;
import cn.swiftpass.enterprise.ui.activity.user.CashierAddActivity;
import cn.swiftpass.enterprise.ui.activity.user.FindPassFirstActivity;
import cn.swiftpass.enterprise.ui.widget.CommonConfirmDialog;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.DialogInfo.HandleBtn;
import cn.swiftpass.enterprise.ui.widget.DialogShowInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.dialog.CashierNoticeDialog;
import cn.swiftpass.enterprise.ui.widget.dialog.RootDialog;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.ECDHUtils;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.FileUtils;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.NetworkUtils;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

import static cn.swiftpass.enterprise.MainApplication.IS_POS_VERSION;
import static cn.swiftpass.enterprise.utils.RootUtils.isDeviceRootedAndroid;
import static cn.swiftpass.enterprise.utils.RootUtils.isDeviceRootedPos;

/**
 * 欢迎界面
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-8-20
 * Time: 下午3:30
 */
public class WelcomeActivity extends BaseActivity {
    private static final String TAG = WelcomeActivity.class.getCanonicalName();

    private Context mContext;

    private Button btn_login;

    private TextView tvVersion;

    private TextView tvchangeCDN;

    private TextView register;

    private int[] imageResId; // 图片ID

    private ViewPager viewPager;

    private int currentItem = 0; // 当前图片的索引号

    private List<ImageView> imageViews; // 滑动的图片集合

    private ImageView up_image;

    private LinearLayout up_layout;

    private SharedPreferences sp;

    private EditText userPwd;

    private EditText user_name;

    private EditText shop_id;

    private String imei = "no";

    private String imsi = "no";

    private View line;

    private ImageView iv_clearMId;

    private ImageView iv_clearUser;

    private ImageView iv_clearPwd;

    private ImageView iv_clearPwd_promt, iv_user_promt, iv_clearMId_promt;

    private TextView tv_debug, tv_version;

    private RelativeLayout introduction;


    private ImageView iv_code, v_code;

    private TextView tv_load;

    private LinearLayout ly_code;

    private EditText ed_code;

    private boolean isInputCode = false;

    private LinearLayout ly_title;

    private RelativeLayout ly_first;

    private Button bt_register;

    //private String regUrl = "";

    String login_skey, login_sauthid;

    private ImageView iv_login;

    private TextView tv_server_name;

    private  boolean isNeedRequestECDHKey = false;

    private boolean isNeedDeviceLogin = false;

    private RootDialog Dialog_root;

    private CashierNoticeDialog ChangePswNoticeDialog;

    //    // 切换当前显示的图片
    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            //            if (msg.what == 1)
            //            {
            //                initPromt();
            //            }
            //            viewPager.setCurrentItem(currentItem);// 切换当前显示的图片
            if (msg.what == 2) {
                Intent it = new Intent();
                it.setClass(WelcomeActivity.this, spayMainTabActivity.class);
                startActivity(it);
//                MainActivity.startActivity(WelcomeActivity.this, "OrderDetailsActivity");
                WelcomeActivity.this.finish();
                //                initPromt();
            }
        }

    };


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

    }

    //private LinearLayout rootView;
//    private ScrollView scrollView;

 /*   private void initMoveKeyBoard() {
        //rootView = (LinearLayout) findViewById(R.id.rootView);
        scrollView = (ScrollView) findViewById(R.id.sv_main);
//        keyboardUtil = new KeyboardUtil(this, rootView, scrollView);
//        keyboardUtil.setOtherEdittext(user_name);
        // monitor the KeyBarod state
//        keyboardUtil.setKeyBoardStateChangeListener(new WelcomeActivity.KeyBoardStateListener());
        // monitor the finish or next Key
//        keyboardUtil.setInputOverListener(new WelcomeActivity.inputOverListener());
//        userPwd.setOnTouchListener(new KeyboardTouchListener(keyboardUtil, KeyboardUtil.INPUTTYPE_ABC, -1));
    }*/


//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
////            if (keyboardUtil.isShow) {
////                keyboardUtil.hideSystemKeyBoard();
////                keyboardUtil.hideAllKeyBoard();
////                keyboardUtil.hideKeyboardLayout();
////            } else {
////            }
//
//            return false;
//        } else return super.onKeyDown(keyCode, event);
//    }

    private LinearLayout rootView;
    private KeyboardUtil keyboardUtil;
    private ScrollView scrollView;

    //安全键盘
    private void initMoveKeyBoard() {
        keyboardUtil = new KeyboardUtil(this, rootView, scrollView);
        keyboardUtil.setOtherEdittext(user_name);
        keyboardUtil.setOtherEdittext(ed_code);
        // monitor the KeyBarod state
        keyboardUtil.setKeyBoardStateChangeListener(new WelcomeActivity.KeyBoardStateListener());
        // monitor the finish or next Key
        keyboardUtil.setInputOverListener(new WelcomeActivity.inputOverListener());
        userPwd.setOnTouchListener(new KeyboardTouchListener(keyboardUtil, KeyboardUtil.INPUTTYPE_ABC, -1));
    }

    class KeyBoardStateListener implements KeyboardUtil.KeyBoardStateChangeListener {

        @Override
        public void KeyBoardStateChange(int state, EditText editText) {

        }
    }

    class inputOverListener implements KeyboardUtil.InputFinishListener {

        @Override
        public void inputHasOver(int onclickType, EditText editText) {

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (keyboardUtil.isShow) {
                keyboardUtil.hideSystemKeyBoard();
                keyboardUtil.hideAllKeyBoard();
                keyboardUtil.hideKeyboardLayout();

                finish();
            } else {
                return super.onKeyDown(keyCode, event);
            }
            return false;
        } else{
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isNeedCheck && Build.VERSION.SDK_INT >= 23) {
            checkPermissions(PERMISSIONS);
        }

        //如果用户拒绝了读取手机设备权限 弹出框提示设置 重新进来 检测弹框是否依然存在
        if (isGranted(Manifest.permission.READ_PHONE_STATE)) {
            if (dialogInfo != null && dialogInfo.isShowing()) {
                dialogInfo.dismiss();
                showLoginPage();
            }

        }

        //重新把登录button恢复成可点击的状态
        btn_login.setEnabled(true);
        btn_login.setText(R.string.login);

        user_name.requestFocus();
    }


    @Override
    protected void onPause() {
        super.onPause();

        //回到后台的时候，清除界面的密码
        iv_clearPwd.setVisibility(View.GONE);
        userPwd.setText("");

        //同时隐藏键盘
        if (keyboardUtil.isShow) {
            keyboardUtil.hideSystemKeyBoard();
            keyboardUtil.hideAllKeyBoard();
            keyboardUtil.hideKeyboardLayout();
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
         setContentView(R.layout.activity_guide);
        HandlerManager.registerHandler(HandlerManager.SHOWMERCHANT, handler);

        sp = this.getSharedPreferences("login", 0);

        btn_login = getViewById(R.id.btn_login);
        /*String merchantNo = LocalAccountManager.getInstance().getMerchantNo();
        if("".equals(merchantNo)){
            btnQuick2.setText(R.string.msg_login_action_d_title);
        }*/
        tvVersion = getViewById(R.id.tv_versionName);
        tvchangeCDN = getViewById(R.id.tv_network_setting);
        // 设置下划线
        //        tvVersion.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);

        //每次启动APP都会调用公钥交换接口
      /*  if(!TextUtils.isEmpty(AppHelper.getImei(MainApplication.getContext()))){
            ECDHKeyExchange();
        }*/

        initView();

       boolean isDeviceRooted = false;
         if(IS_POS_VERSION){
            isDeviceRooted = isDeviceRootedPos();
        }else {
            isDeviceRooted = isDeviceRootedAndroid();
        }

       if(isDeviceRooted){
           runOnUiThread(new Runnable() {
               @Override
               public void run() {
                   Dialog_root = new RootDialog(WelcomeActivity.this, new OnClickListener() {
                       @Override
                       public void onClick(View v) {
                           Dialog_root.dismiss();
                           for (Activity a : MainApplication.allActivities)
                           {
                               a.finish();
                           }
                       }
                   });

                   if(Dialog_root != null && !Dialog_root.isShowing()){
                       String message = getStringById(R.string.system_root_instruction);
                       Dialog_root.setMessage(message);
                       Dialog_root.setCanceledOnTouchOutside(false);
                       Dialog_root.show();
                   }
               }
           });
        }

        login_skey = PreferenceUtil.getString("login_skey", "");
        login_sauthid = PreferenceUtil.getString("login_sauthid", "");
        if (!StringUtil.isEmptyOrNull(login_skey) && !StringUtil.isEmptyOrNull(login_sauthid)
                && !StringUtil.isEmptyOrNull(MainApplication.isDefault)
                && !MainApplication.isDefault.equalsIgnoreCase("1")) {
            if (isGranted(Manifest.permission.READ_PHONE_STATE)) {
                if(!StringUtil.isEmptyOrNull(MainApplication.skey)){
                    deviceLogin();
                }else{ //否则先去调用公钥交换接口然后再调用设备登录接口
                    if(!TextUtils.isEmpty(AppHelper.getImei(MainApplication.getContext()))){
                        isNeedDeviceLogin = true;
                        ECDHKeyExchange();
                    }
                }
            } else {
                showMissingPermissionDialog(WelcomeActivity.this);
            }
        } else {
            MainApplication.isUpdateShow = false;
            checkVersionisUpdate();
            //回话过期直接跳转到登录界面
            showLoginPage();
        }
        initVale();
        clearInstalledAPK();
        //设置监听
        setLister();
        keepAlive();
    }

    private void showLoginPage() {
        //回话过期直接跳转到登录界面
        ly_first.setVisibility(View.GONE);
        ly_title.setVisibility(View.VISIBLE);
        scrollView.setVisibility(View.VISIBLE);
    }

    private void ECDHKeyExchange(){
        try {
            ECDHUtils.getInstance().getAppPubKey();
        }catch (Exception e){
            Log.e(TAG,Log.getStackTraceString(e));
        }
        final String publicKey =  SharedPreUtile.readProduct("pubKey").toString();
        final String privateKey =  SharedPreUtile.readProduct("priKey").toString();

        LocalAccountManager.getInstance().ECDHKeyExchange(publicKey,new UINotifyListener<ECDHInfo>(){
            @Override
            public void onError(Object object) {
                super.onError(object);
                /*if(object != null){
                    toastDialog(WelcomeActivity.this, object.toString(), null);
                }*/
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if(isNeedRequestECDHKey){
                    showLoading(false, getString(R.string.show_login_loading));
                }
            }

            @Override
            protected void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(ECDHInfo result) {
                super.onSucceed(result);
                if(result != null){
                    try {
                        String secretKey = ECDHUtils.getInstance().ecdhGetShareKey(MainApplication.serPubKey,privateKey);
                        SharedPreUtile.saveObject(secretKey, "secretKey");

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //如果是登录失败报错之后再请求公钥交换接口，则此时直接调用登录
                                if(isNeedRequestECDHKey){
                                    onLogin();
                                }

                                //如果是设备登录的时候发现没有skey调用公钥交换接口，则此时直接调用设备登录接口
                                if(isNeedDeviceLogin &&  !StringUtil.isEmptyOrNull(MainApplication.isDefault)
                                    && !MainApplication.isDefault.equalsIgnoreCase("1")){
                                    deviceLogin();
                                }
                            }
                        });

                    }catch (Exception e){
                        Log.e(TAG,Log.getStackTraceString(e));
                    }
                }
            }
        });
    }


    private void deviceLogin() {
        MainApplication.isUpdateShow = true;
        LocalAccountManager.getInstance().deviceLogin(new UINotifyListener<Boolean>() {
            @Override
            public void onError(Object object) {
                // TODO Auto-generated method stub
                super.onError(object);
                dismissLoading();
                isNeedDeviceLogin = false;
                if (MainApplication.getContext().isNeedLogin()) {
                    MainApplication.setNeedLogin(false);
                    WelcomeActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            ly_first.setVisibility(View.GONE);
                            ly_title.setVisibility(View.VISIBLE);
                            scrollView.setVisibility(View.VISIBLE);
                            PreferenceUtil.removeKey("login_skey");
                            PreferenceUtil.removeKey("login_sauthid");
                            MainApplication.isUpdateShow = false;
                            checkVersionisUpdate();
                        }
                    });
                    return;
                } else {
                    if (object != null) {
                        toastDialog(WelcomeActivity.this, object.toString(), new NewDialogInfo.HandleBtn() {

                            @Override
                            public void handleOkBtn() {
                                WelcomeActivity.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        ly_first.setVisibility(View.GONE);
                                        ly_title.setVisibility(View.VISIBLE);
                                        scrollView.setVisibility(View.VISIBLE);
                                        PreferenceUtil.removeKey("login_skey");
                                        PreferenceUtil.removeKey("login_sauthid");
                                        MainApplication.isUpdateShow = false;
                                        checkVersionisUpdate();
                                    }
                                });
                            }

                        });
                    }
                }
            }

            @Override
            public void onSucceed(Boolean result) {
                super.onSucceed(result);
                if (result) {
                    loginSuccToLoad(false);
                    isNeedDeviceLogin = false;
                } else {
                    ly_first.setVisibility(View.GONE);
                    ly_title.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.VISIBLE);
                    PreferenceUtil.removeKey("login_skey");
                    PreferenceUtil.removeKey("login_sauthid");
                    MainApplication.isUpdateShow = false;
                    //                        checkVersionisUpdate();
                }
            }
        });
    }

    private void keepAlive() {
        //JobScheduler 大于5.0才可以使用
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            JobSchedulerManager mJobManager = JobSchedulerManager.getJobSchedulerInstance(this);
            mJobManager.startJobScheduler();
        }
        Intent intent = new Intent(this, ForegroundLiveService.class);
        startService(intent);
        Intent liveService = new Intent(this, PlayerMusicService.class);
        startService(liveService);
    }

    /**
     * 检查版本更新
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void checkVersionisUpdate() {
        if (NetworkUtils.isNetWorkValid(WelcomeActivity.this)) {
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            checkVersion();
                        }
                    });

                }

            };
            Timer timer = new Timer();
            timer.schedule(task, 200);
        } else {

            DialogInfo dialogInfo = new DialogInfo(mContext, getString(R.string.no_network), getString(R.string.to_open_network), getString(R.string.to_open), DialogInfo.NETWORKSTATUE, new HandleBtn() {

                @Override
                public void handleOkBtn() {
                    Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                    mContext.startActivity(intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                }

                @Override
                public void handleCancleBtn() {
                }
            }, null);
            DialogHelper.resize(this, dialogInfo);
            dialogInfo.show();
        }
    }


    public void setActivity(MainActivity activity) {
        //this.activity = activity;
    }

    public static void startActivity(Context context, String activeId, String activeName, String content) {

        Intent it = new Intent();
        //if (newTask)
        // {
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //}
        it.setClass(context, WelcomeActivity.class);
        it.putExtra("activeId", activeId);
        it.putExtra("activeName", activeName);
        it.putExtra("content", content);
        context.startActivity(it);

    }

    public static void startActivity(Context context) {

        Intent it = new Intent();
        //if (newTask)
        // {
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //}
        it.setClass(context, WelcomeActivity.class);
        //        it.putExtra("content", "OrderDetailsActivity");
        //isOrder = true;
        context.startActivity(it);

    }

    protected void initPromt() {
        //        if ("".equals(user_name.getText().toString().trim()))
        //        {
        //            iv_user_promt.setVisibility(View.VISIBLE);
        //        }
        //        if ("".equals(userPwd.getText().toString().trim()))
        //        {
        //            iv_clearPwd_promt.setVisibility(View.VISIBLE);
        //        }
        //        if ("".equals(shop_id.getText().toString().trim()))
        //        {
        //            iv_clearMId_promt.setVisibility(View.VISIBLE);
        //        }
    }

    /**
     * base64转为bitmap
     *
     * @param base64Data
     * @return
     */
    public Bitmap base64ToBitmap(String base64Data) {
        byte[] bytes = Base64.decode(base64Data, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    public void refreshServerAddress(String serverString){ //点击登录按钮的时候，要重新根据当前选择的状态轮询一下取哪个网络地址
        if(serverString== null ||TextUtils.isEmpty(serverString)|| serverString.equals("")){
            //要进行是否开启CDN的判断
            String CDN_status = PreferenceUtil.getString("CDN", "open");
            if(CDN_status.equals("open")){
                ApiConstant.BASE_URL_PORT = MainApplication.config.getServerAddr();
            }else{
                ApiConstant.BASE_URL_PORT = MainApplication.config.getServerAddrBack();
            }
//            ApiConstant.BASE_URL_PORT = MainApplication.config.getServerAddr();
            ApiConstant.pushMoneyUrl = MainApplication.config.getPushMoneyUrl();
            PreferenceUtil.removeKey("serverCifg");
        }else{
            if (serverString.equals("test")){
                ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrTest;
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest;
                PreferenceUtil.commitString("serverCifg", "test");
            }else if (serverString.equals("prd")){
                ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrPrd;
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlPrd;
                PreferenceUtil.commitString("serverCifg", "prd");
            }else if (serverString.equals("dev")){
                ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrDev;
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest;
                PreferenceUtil.commitString("serverCifg", "dev");
            }else if(serverString.equals("dev-jh")){
                ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrDev_JH;
                ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest;
                PreferenceUtil.commitString("serverCifg", "dev-jh");
            }
        }
    }

    int count = 0;

    void setServerAddress() {
        count = count + 1;
        if (count == 4) { //测试环境
            ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrTest;
            ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest;
            tv_server_name.setText("test");
            tv_server_name.setVisibility(View.VISIBLE);
            PreferenceUtil.commitString("serverCifg", "test");
        } else if (count == 5) {
            //灰度线环境
            ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrPrd;
            ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlPrd;
            tv_server_name.setText("prd");
            tv_server_name.setVisibility(View.VISIBLE);
            PreferenceUtil.commitString("serverCifg", "prd");
        }else if(count == 6){
            count = 0;
            ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrDev_JH;
            ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest;
            tv_server_name.setText("dev-jh");
            tv_server_name.setVisibility(View.VISIBLE);
            PreferenceUtil.commitString("serverCifg", "dev-jh");

        } else if (count == 3) {
            ApiConstant.BASE_URL_PORT = ApiConstant.serverAddrDev;
            ApiConstant.pushMoneyUrl = ApiConstant.pushMoneyUrlTest;
            tv_server_name.setText("dev");
            tv_server_name.setVisibility(View.VISIBLE);
            PreferenceUtil.commitString("serverCifg", "dev");
        } else {
            tv_server_name.setText("");
            tv_server_name.setVisibility(View.GONE);

            //要进行是否开启CDN的判断
            String CDN_status = PreferenceUtil.getString("CDN", "open");
            if(CDN_status.equals("open")){
                ApiConstant.BASE_URL_PORT = MainApplication.config.getServerAddr();
            }else{
                ApiConstant.BASE_URL_PORT = MainApplication.config.getServerAddrBack();
            }
//            ApiConstant.BASE_URL_PORT = MainApplication.config.getServerAddr();
            ApiConstant.pushMoneyUrl = MainApplication.config.getPushMoneyUrl();
            PreferenceUtil.removeKey("serverCifg");
        }
    }

    private void setLister() {
        iv_login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!StringUtil.isEmptyOrNull(ApiConstant.imie)) {

                    String arr[] = ApiConstant.imie.split("\\,");

                    String imie = AppHelper.getImei(WelcomeActivity.this);
                    for (int i = 0; i < arr.length; i++) {
                        if (arr[i].equalsIgnoreCase(imie)) {
                            setServerAddress();
                        }
                    }

                }


            }
        });

        bt_register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });

        iv_code.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                LocalAccountManager.getInstance().getCode(new UINotifyListener<String>() {
                    @Override
                    public void onError(Object object) {
                        // TODO Auto-generated method stub
                        super.onError(object);
                        WelcomeActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                tv_load.setText(R.string.tv_code_refresh);
                                tv_load.setVisibility(View.GONE);
                            }
                        });
                    }
                    @Override
                    public void onPreExecute() {
                        super.onPostExecute();
                        WelcomeActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                tv_load.setText(R.string.public_loading);
                                tv_load.setVisibility(View.VISIBLE);
                            }
                        });
                    }

                    @Override
                    public void onSucceed(String result) {
                        super.onSucceed(result);
                        if (null != result) {
                            tv_load.setVisibility(View.GONE);
                            iv_code.setVisibility(View.VISIBLE);
                            Bitmap bitmap = base64ToBitmap(result);
                            if (null != bitmap) {
                                iv_code.setImageBitmap(bitmap);
                            }
                        }
                    }
                });
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {//osMSkt5tgFUcWsbxFuPhUhLOQBPA
                // oCnu4uGVvVFaGrCEAY2eWVZj5ibE
                //                sendWxRedpack("osMSkt5tgFUcWsbxFuPhUhLOQBPA");

               /* if (!StringUtil.isEmptyOrNull(ApiConstant.imie)) {

                    String arr[] = ApiConstant.imie.split("\\,");

                    String imie = AppHelper.getImei(WelcomeActivity.this);
                    Log.i("hehui", "imie-->" + imie);
                    for (int i = 0; i < arr.length; i++) {
                        if (arr[i].equalsIgnoreCase(imie)) {
                            setServerAddress();
                        }
                    }

                }
                Log.i("hehui", "serverAdd-->" + ApiConstant.BASE_URL_PORT );*/
                try {
                    StatService.trackCustomEvent(WelcomeActivity.this, "kMTASPayLogin", "登录界面 登录按钮");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                //先收起键盘
                if (keyboardUtil.isShow) {
                    keyboardUtil.hideSystemKeyBoard();
                    keyboardUtil.hideAllKeyBoard();
                    keyboardUtil.hideKeyboardLayout();
                }

                refreshServerAddress(tv_server_name.getText().toString());
                //如果点击登录按钮的时候缓存中还没有skey和serPubKey,则先调用公钥接口
                if(MainApplication.skey != null && !TextUtils.isEmpty(MainApplication.skey)
                        && MainApplication.serPubKey != null && !TextUtils.isEmpty(MainApplication.serPubKey)){
                    onLogin();
                }else{
                    isNeedRequestECDHKey = true;
                    ECDHKeyExchange();
                }


            }
        });

        tvVersion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                refreshServerAddress(tv_server_name.getText().toString());
                showPage(FindPassFirstActivity.class);
                //                MainApplication.updatePwdActivities.add(WelcomeActivity.this);
            }
        });

        tvchangeCDN.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showPage(SettingCDNActivity.class);
            }
        });
    }

    /***
     * 初始化
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initVale() {
        //shopID = sp.getString("shop_id", "");
        //String pwd = sp.getString("userPwd", "");
        String userNmae = sp.getString("user_name", "");
        //TODO ShouHu 隐藏安全键盘功能
//        if (!StringUtil.isEmptyOrNull(userNmae)) {
//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    keyboardUtil.showKeyBoardLayout(userPwd, KeyboardUtil.INPUTTYPE_ABC, -1);
//
//                }
//            }, 200);
//            user_name.setCursorVisible(false);
//        }
        user_name.setText(userNmae);
    }

    public boolean flag = true;
    /**
     * 判断是否需要检测，防止不停的弹框
     */
    private boolean isNeedCheck = true;

    private static final int PERMISSON_REQUESTCODE = 0;
    // 所需的全部权限
    static final String[] PERMISSIONS = new String[]{Manifest.permission.MODIFY_AUDIO_SETTINGS, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION};
//    static final String[] PERMISSIONS = new String[]{Manifest.permission.READ_PHONE_STATE};

    /**
     * 检查是否 有权限
     *
     * @param permission
     * @return
     */
    private boolean lacksPermission(String permission) {
        return ContextCompat.checkSelfPermission(WelcomeActivity.this, permission) == PackageManager.PERMISSION_DENIED;
    }

    public boolean lacksPermissions(String... permissions) {
        for (String permission : permissions) {
            if (lacksPermission(permission)) {
                return true;
            }
        }
        return false;
    }


    /**
     * 申请权限结果的回调方法
     */
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] paramArrayOfInt) {
        if (requestCode == PERMISSON_REQUESTCODE) {
            if (!verifyPermissions(paramArrayOfInt)) {
                //showMissingPermissionDialog(WelcomeActivity.this);
                isNeedCheck = false;
            }
        }
    }

    private DialogShowInfo dialogInfo;

    /**
     * 显示提示信息
     *
     * @since 2.5.0
     */
    protected void showMissingPermissionDialog(Context context) {
        dialogInfo = new DialogShowInfo(context, null, getString(R.string.setting_permisson_phone), getStringById(R.string.title_setting), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogShowInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                startAppSettings();
            }

            @Override
            public void handleCancleBtn() {
                dialogInfo.dismiss();
                showLoginPage();
            }
        }, null);
        dialogInfo.setOnKeyListener(new DialogInterface.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                if (keycode == KeyEvent.KEYCODE_BACK) {
                    return true;
                }
                startAppSettings();
                return false;
            }
        });
        DialogHelper.resize(context, dialogInfo);
        dialogInfo.show();
    }


    /**
     * 初始化
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initView() {
        iv_login = getViewById(R.id.iv_login);
        tv_server_name = getViewById(R.id.tv_server_name);
        rootView = findViewById(R.id.rootView);
        scrollView = findViewById(R.id.sv_main);

        String serverCifg = PreferenceUtil.getString("serverCifg", "");
        if (!StringUtil.isEmptyOrNull(serverCifg)) {
            if (serverCifg.equals("test")) {
                tv_server_name.setText("test");
                tv_server_name.setVisibility(View.VISIBLE);
            } else if (serverCifg.equals("prd")) {
                tv_server_name.setText("prd");
                tv_server_name.setVisibility(View.VISIBLE);
            } else if (serverCifg.equals("dev")) {
                tv_server_name.setText("dev");
                tv_server_name.setVisibility(View.VISIBLE);
            } else {
                tv_server_name.setText("");
                tv_server_name.setVisibility(View.GONE);
            }
        }

        bt_register = getViewById(R.id.bt_register);
        ly_first = getViewById(R.id.ly_first);
        ly_title = getViewById(R.id.ly_title);
        ed_code = getViewById(R.id.ed_code);
        //定义hint的值
        SpannableString ss = new SpannableString(getResources().getString(R.string.bt_code_not_null));
        AbsoluteSizeSpan ass = new AbsoluteSizeSpan(14,true);
        ss.setSpan(ass, 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ed_code.setHint(new SpannedString(ss));
        ly_code = getViewById(R.id.ly_code);
        v_code = getViewById(R.id.v_code);
        tv_load = getViewById(R.id.tv_load);
        //        viewPager = (ViewPager)findViewById(R.id.vp);
        iv_code = getViewById(R.id.iv_code);
        tv_version = getViewById(R.id.tv_ver);
        tv_version.setText("V" + AppHelper.getVerName(getApplicationContext()));
        //login_remember = getViewById(R.id.login_remember);

        imageResId = new int[]{R.drawable.n_icon_login_01, R.drawable.n_icon_login_02};

        imageViews = new ArrayList<ImageView>();

        up_image = findViewById(R.id.up_image);

        up_layout = findViewById(R.id.up_layout);

        line = getViewById(R.id.line);

        user_name = getViewById(R.id.user_name);

        userPwd = getViewById(R.id.userPwd);
        userPwd.setTypeface(user_name.getTypeface());

        shop_id = getViewById(R.id.shop_id);

        iv_clearMId = getViewById(R.id.iv_clearMId);

        iv_clearUser = getViewById(R.id.iv_clearUser);

        iv_clearPwd = getViewById(R.id.iv_clearPwd);

        iv_clearMId.setOnClickListener(clearImage);
        iv_clearUser.setOnClickListener(clearImage);
        iv_clearPwd.setOnClickListener(clearImage);

        iv_clearPwd_promt = getViewById(R.id.iv_clearPwd_promt);
        iv_user_promt = getViewById(R.id.iv_user_promt);
        iv_clearMId_promt = getViewById(R.id.iv_clearMId_promt);

        tv_debug = getViewById(R.id.tv_debug);
        tv_debug.setText((GlobalConstant.isDebug == true ? "(" + getString(R.string.tx_try_version) + ")" : ""));
        tv_debug.setTextColor(Color.BLUE);

        introduction = getViewById(R.id.introduction);

        //        slid.open();

        introduction.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                showPage(IntroductionActivity.class);
            }
        });
       /* up_image.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (flag) {
                    up_layout.setVisibility(View.VISIBLE);
                    up_image.setImageResource(R.drawable.n_icon_an);
                    line.setVisibility(View.VISIBLE);
                    shop_id.setFocusable(true);
                    flag = false;
                } else {
                    up_layout.setVisibility(View.GONE);
                    line.setVisibility(View.GONE);
                    up_image.setImageResource(R.drawable.n_icon_up);
                    flag = true;
                }
            }
        });*/

        // 初始化图片资源
        //        for (int i = 0; i < imageResId.length; i++)
        //        {
        //            ImageView imageView = new ImageView(this);
        //            imageView.setImageResource(imageResId[i]);
        //            imageView.setScaleType(ScaleType.CENTER_INSIDE);
        //            imageViews.add(imageView);
        //        }
        //        
        //        viewPager.setAdapter(new MyAdapter());// 设置填充ViewPager页面的适配器
        //        
        //        // 设置一个监听器，当ViewPager中的页面改变时调用
        //        viewPager.setOnPageChangeListener(new MyPageChangeListener());

        register = findViewById(R.id.register);

        register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // 跳转到注册界面
                //showPage(RegisterActivity.class);
//                showPage(RegisterNewActivity.class);

                //                showPage(RegisterSuccActivity.class);
                //                showPage(ApplyActivity.class);
            }

        });

        // 兴业银行
        if (null != ApiConstant.bankType && !"".equals(ApiConstant.bankType) && ApiConstant.bankType == 1) {
            register.setVisibility(View.INVISIBLE);
        }

        user_name.setOnFocusChangeListener(listener);

        userPwd.setOnFocusChangeListener(listener);

        ed_code.setOnFocusChangeListener(listener);
        //        
        //        shop_id.setOnFocusChangeListener(listener);

        EditTextWatcher editTextWatcher = new EditTextWatcher();

        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {

                if (user_name.isFocused()) {
                    //                    userPwd.setCursorVisible(false);
                    user_name.setCursorVisible(true);
                    if (user_name.getText().toString().length() > 0) {
                        iv_clearUser.setVisibility(View.VISIBLE);
                    } else {
                        iv_clearPwd.setVisibility(View.GONE);
                        iv_clearMId.setVisibility(View.GONE);

                        iv_user_promt.setVisibility(View.GONE);
                    }
                } else if (userPwd.isFocused()) {
                    userPwd.setCursorVisible(true);
                    if (userPwd.getText().toString().length() > 0) {
                        iv_clearPwd.setVisibility(View.VISIBLE);
                    } else {
                        iv_clearMId.setVisibility(View.GONE);
                        iv_clearUser.setVisibility(View.GONE);
                        iv_clearPwd_promt.setVisibility(View.GONE);
                    }
                } else if (shop_id.isFocused()) {
                    //                        iv_clearMId.setVisibility(View.VISIBLE);
                    iv_clearUser.setVisibility(View.GONE);
                    iv_clearPwd.setVisibility(View.GONE);
                    iv_clearMId_promt.setVisibility(View.GONE);
                }
                if (isInputCode) {
                    if (!StringUtil.isEmptyOrNull(user_name.getText().toString()) && !StringUtil.isEmptyOrNull(userPwd.getText().toString()) && !StringUtil.isEmptyOrNull(ed_code.getText().toString())) {
                        setButtonBg(btn_login, true, R.string.login);
                    }

                } else {

                    if (!StringUtil.isEmptyOrNull(user_name.getText().toString()) && !StringUtil.isEmptyOrNull(userPwd.getText().toString())) {
                        setButtonBg(btn_login, true, R.string.login);
                    }
                }

            }

        });

        user_name.addTextChangedListener(editTextWatcher);
        userPwd.addTextChangedListener(editTextWatcher);
        shop_id.addTextChangedListener(editTextWatcher);
        initMoveKeyBoard();
    }

    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b, enable, res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_press_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }

    private OnClickListener clearImage = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.iv_clearMId:
                    shop_id.setText("");
                    //                    iv_clearMId_promt.setVisibility(View.VISIBLE);
                    break;
                case R.id.iv_clearUser:
                    iv_clearUser.setVisibility(View.GONE);
                    user_name.setText("");
                    //                    iv_user_promt.setVisibility(View.VISIBLE);
                    break;
                case R.id.iv_clearPwd:
                    iv_clearPwd.setVisibility(View.GONE);
                    userPwd.setText("");
                    //                    iv_clearPwd_promt.setVisibility(View.VISIBLE);
                    break;
                default:
                    break;
            }
        }
    };

    private final OnFocusChangeListener listener = new OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            switch (v.getId()) {
                case R.id.user_name:
                    if (hasFocus) {
                        //                        user_name.setCursorVisible(true);
                        if (user_name.getText().length() > 0) {
                            iv_clearUser.setVisibility(View.VISIBLE);
                        } else {
                            iv_clearUser.setVisibility(View.GONE);
                        }
                    } else {
                        iv_clearUser.setVisibility(View.GONE);
                    }
                    break;
                case R.id.userPwd:
                    //                    sp.edit().putString("userPwd", userPwd.getText().toString()).commit();

                    if (hasFocus) {
                        userPwd.setCursorVisible(true);
                        if (userPwd.getText().length() > 0) {
                            iv_clearPwd.setVisibility(View.VISIBLE);
                        } else {
                            iv_clearPwd.setVisibility(View.GONE);
                        }
                    } else {
                        iv_clearPwd.setVisibility(View.GONE);
                    }

                    break;
                case R.id.shop_id:
                    //                    sp.edit().putString("shop_id", shop_id.getText().toString()).commit();

                    if (hasFocus) {
                        if (shop_id.getText().length() >= 1) {
                            iv_clearMId.setVisibility(View.VISIBLE);
                        } else {
                            iv_clearMId.setVisibility(View.GONE);
                        }
                    } else {
                        iv_clearMId.setVisibility(View.GONE);
                    }
                    break;
            }

        }
    };

    /**
     * 当ViewPager中页面的状态发生改变时调用
     *
     * @author Administrator
     */
    private class MyPageChangeListener implements OnPageChangeListener {
        //private int oldPosition = 0;

        /**
         * This method will be invoked when a new page becomes selected.
         * position: Position index of the new selected page.
         */
        @Override
        public void onPageSelected(int position) {
            currentItem = position;
            //oldPosition = position;
        }
        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }
    }

    @Override
    protected void onStart() {
        //        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        //        // 当Activity显示出来后，每两秒钟切换一次图片显示
        //        scheduledExecutorService.scheduleAtFixedRate(new ScrollTask(), 1, 3, TimeUnit.SECONDS);
        super.onStart();
    }

    /**
     * 换行切换任务
     *
     * @author Administrator
     */
    private class ScrollTask implements Runnable {

        public void run() {
            synchronized (viewPager) {
                currentItem = (currentItem + 1) % imageViews.size();
                //                handler.obtainMessage().sendToTarget(); // 通过Handler切换图片
            }
        }

    }

    @Override
    protected void onStop() {
        // 当Activity不可见的时候停止切换
        //        scheduledExecutorService.shutdown();
        super.onStop();
    }

    /**
     * 填充ViewPager页面的适配器
     *
     * @author Administrator
     */
    private class MyAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return imageResId.length;
        }

        @Override
        public Object instantiateItem(View arg0, int arg1) {
            ((ViewPager) arg0).addView(imageViews.get(arg1));
            return imageViews.get(arg1);
        }

        @Override
        public void destroyItem(View arg0, int arg1, Object arg2) {
            ((ViewPager) arg0).removeView((View) arg2);
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {

        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public void startUpdate(View arg0) {

        }

        @Override
        public void finishUpdate(View arg0) {

        }
    }

    @Override
    protected boolean isLoginRequired() {
        return false;
    }

    private void startLogin() {
        if (LocalAccountManager.getInstance().isLoggedIn()) {
           /* Intent it = new Intent();
            it.setClass(WelcomeActivity.this, MainActivity.class);
            it.putExtra("registPromptStr", "WelcomeActivity");
            startActivity(it);*/

            Intent it = new Intent();
            it.setClass(WelcomeActivity.this, spayMainTabActivity.class);
            startActivity(it);

            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            finish();
        }
    }


    /***
     * 登录操作
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    public void onLogin() {

        //        Intent it = new Intent();
        //        it.setClass(WelcomeActivity.this, LoginActivity.class);
        //        startActivity(it);
        //        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        String sID = shop_id.getText().toString();
        String name = user_name.getText().toString().trim();
        String pwd = userPwd.getText().toString().trim();

        if ("".equals(name)) {
            //            showToastInfo(getString(R.string.show_user_name));
            toastDialog(WelcomeActivity.this, R.string.show_user_name, null);
            user_name.setFocusable(true);
            return;
        }

        if ("".equals(pwd)) {
            toastDialog(WelcomeActivity.this, R.string.pay_login_pwd, null);//(getString(R.string.pay_refund_pwd));
            userPwd.setFocusable(true);
            return;
        }

        if (isInputCode && StringUtil.isEmptyOrNull(ed_code.getText().toString())) {
            toastDialog(WelcomeActivity.this, R.string.tx_ver_code, null);
            ed_code.setFocusable(true);
            return;
        }
        if (NetworkUtils.isNetWorkValid(WelcomeActivity.this)) {
            StatService.trackCustomEvent(this, "SPConstTapLoginButton", "登录");
            login(name, pwd, sID);
        } else {
            showToastInfo(getString(R.string.show_no_network));
        }
    }

    //APP清掉本地缓存的预授权和消费的切换
    public void ClearAPPState(){
        PreferenceUtil.removeKey("choose_pay_or_pre_auth");
        PreferenceUtil.removeKey("bill_list_choose_pay_or_pre_auth");

    }

    /**
     * 登录
     * <功能详细描述>
     *
     * @param username
     * @param password
     * @param mId  商户号组件
     * @see [类、类#方法、类#成员]
     */
    private void login(String username, final String password, String mId) {
        LocalAccountManager.getInstance().loginAsync(ed_code.getText().toString(), mId, username, password, imei, imsi, false, new UINotifyListener<Boolean>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                btn_login.setEnabled(false);
                btn_login.setText(R.string.bt_login_loading);
                showLoading(false, getString(R.string.show_login_loading));
                ClearAPPState();
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (object != null) {
                            if(object.toString().startsWith("405")){//Require to renegotiate ECDH key
                                isNeedRequestECDHKey = true;
                                ECDHKeyExchange();
                            }else{
                                try {
                                    dismissLoading();
                                    btn_login.setEnabled(true);
                                    btn_login.setText(R.string.login);

                                    final ErrorMsg msg = (ErrorMsg) object;
                                    if (!StringUtil.isEmptyOrNull(msg.getMessage())) {
                                        toastDialog(WelcomeActivity.this, msg.getMessage(), new NewDialogInfo.HandleBtn() {

                                            @Override
                                            public void handleOkBtn() {
                                                if (!StringUtil.isEmptyOrNull(msg.getContent())) {
                                                    Bitmap bitmap = base64ToBitmap(msg.getContent());
                                                    if (null != bitmap) {
                                                        iv_code.setVisibility(View.VISIBLE);
                                                        tv_load.setVisibility(View.GONE);
                                                        iv_code.setImageBitmap(bitmap);
                                                    }
                                                }
                                            }

                                        });
                                    }
                                } catch (Exception e) {
                                    dismissLoading();
                                    btn_login.setEnabled(true);
                                    btn_login.setText(R.string.login);

                                    if (object.toString().startsWith("403")) {//有验证码
                                        isInputCode = true;
                                        ly_code.setVisibility(View.VISIBLE);
                                        v_code.setVisibility(View.VISIBLE);
                                        String code = object.toString().substring(3);
                                        if (!StringUtil.isEmptyOrNull(code)) {

                                            Bitmap bitmap = base64ToBitmap(code);
                                            if (null != bitmap) {
                                                iv_code.setVisibility(View.VISIBLE);
                                                tv_load.setVisibility(View.GONE);
                                                iv_code.setImageBitmap(bitmap);
                                            }
                                        }
                                    } else {
                                        toastDialog(WelcomeActivity.this, object.toString(), null);
                                    }
                                }
                            }
                        }
                    }
                });
            }

            @Override
            public void onSucceed(Boolean object) {
                super.onSucceed(object);
                if(object){
                    dismissLoading();
                    sp.edit().putString("user_name", user_name.getText().toString()).commit();
                    //如果是默认密码登录的，则弹框提示，强制要求修改密码
                    final String oldpsw = password;
                    if(!StringUtil.isEmptyOrNull(MainApplication.isDefault)
                    && MainApplication.isDefault.equalsIgnoreCase("1")){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(ChangePswNoticeDialog == null)
                                {
                                    ChangePswNoticeDialog = new CashierNoticeDialog(WelcomeActivity.this);
                                    ChangePswNoticeDialog.setContentText(R.string.change_psw_instruction);
                                    ChangePswNoticeDialog.setConfirmText(R.string.bt_dialog_ok);
                                    ChangePswNoticeDialog.setCanceledOnTouchOutside(false);
                                    //设置点击返回键不消失
                                    ChangePswNoticeDialog.setCancelable(false);
                                    ChangePswNoticeDialog.setCurrentListener(new CashierNoticeDialog.ButtonConfirmCallBack() {
                                        @Override
                                        public void onClickConfirmCallBack() {
                                            if(ChangePswNoticeDialog != null && ChangePswNoticeDialog.isShowing()){
                                                ChangePswNoticeDialog.dismiss();
                                                //跳转到修改密码的页面
                                                ChangePswLoginActivity.startActivity(mContext,oldpsw);
                                            }
                                        }
                                    });
                                }
                                if(!ChangePswNoticeDialog.isShowing())
                                {
                                    ChangePswNoticeDialog.show();
                                }
                            }
                        });
                    }else{
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                MainApplication.isSessionOutTime = false;
                                isNeedRequestECDHKey = false;
                                loginSuccToLoad(true);
                            }
                        });
                    }
                }
            }

        });

    }

    void loginSuccToLoad(boolean isLoad) {
        //        //加载保存登录用户信息
        sp.edit().putString("user_name", user_name.getText().toString()).commit();
        ////        sp.edit().putString("userPwd", "").commit();
        //        if (login_remember.isChecked())
        //        {
        //            String pwd;
        //            try
        //            {
        //                pwd = AESHelper.encrypt("SPAY", userPwd.getText().toString());
        //                sp.edit().putString("userPwd", pwd).commit();
        //            }
        //            catch (Exception e)
        //            {
        //                // TODO Auto-generated catch block
        //                sp.edit().putString("userPwd", userPwd.getText().toString()).commit();
        //                Log.e(TAG,Log.getStackTraceString(e));
        //            }
        //        }

        //sp.edit().putString("shop_id", shop_id.getText().toString()).commit();

        ApiConstant.OFFLINE = false;
        // 用户 区分 临时用户的密码标示
        MainApplication.pwd_tag = userPwd.getText().toString();
        MainApplication.userName = user_name.getText().toString();
        MainApplication.isRegisterUser = true;

        //        String dynModelMd5 = PreferenceUtil.getString("dynModelMd5", "");
        //        if (!StringUtil.isEmptyOrNull(dynModelMd5))
        //        {
        //            if (!StringUtil.isEmptyOrNull(MainApplication.themeMd5)
        //                && MainApplication.themeMd5.equalsIgnoreCase(dynModelMd5))
        //            {
        //                todo();
        //            }
        //            else
        //            {
        //                getDyn(isLoad);
        //            }
        //        }
        //        else
        //        {
        //            //加载样式
        //            getDyn(isLoad);
        //        }

        //                        getDyn();


        //主动做一次蓝牙连接
        try {
            connentBlue();
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }

        loadPayType();
        //getExchangeRate();
        todo();
    }


    /**
     * 过滤支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */

    private void filterListStream(List<DynModel> list, List<DynModel> filterLst) {

        for (DynModel dynModel : list) {
            if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                for (String key : MainApplication.apiProviderMap.keySet()) {
                    if (key.equals(dynModel.getApiCode())) {
                        filterLst.add(dynModel);
                        break;
                    }

                }
            }
        }
    }

    /**
     * 过滤支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void filterList(List<DynModel> list, List<DynModel> filterLst, Map<String, String> payTypeMap) {

        if (!StringUtil.isEmptyOrNull(MainApplication.serviceType)) {
            String[] arrPays = MainApplication.serviceType.split("\\|");

            for (DynModel dynModel : list) {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                    for (String key : MainApplication.apiProviderMap.keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if(dynModel.getNativeTradeType().contains(",")){
                                    String[] arrNativeTradeType = dynModel.getNativeTradeType().split(",");
                                    for(int  j = 0 ; j < arrNativeTradeType.length;j ++ ){
                                        if(arrNativeTradeType[j].equals(arrPays[i])){
                                            filterLst.add(dynModel);
                                            payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);
                                            break;
                                        }
                                    }
                                }else if(arrPays[i].equals(dynModel.getNativeTradeType())){
                                    filterLst.add(dynModel);
                                    payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    break;
                                }
                                /*if (arrPays[i].startsWith(dynModel.getNativeTradeType()) || dynModel.getNativeTradeType().contains(arrPays[i]) || arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    filterLst.add(dynModel);
                                    if (dynModel.getNativeTradeType().contains(",")) {
                                        payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType().substring(0, dynModel.getNativeTradeType().lastIndexOf(",")));
                                    } else {
                                        payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    }
                                    break;
                                }*/
                            }

                        }
                    }

                } else {
                    continue;
                }
            }
        }

    }

    /**
     * 过滤预授权的支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void filterList_pre_auth(List<DynModel> list, List<DynModel> filterLst_pre_auth, Map<String, String> payTypeMap_pre_auth) {

        if (!StringUtil.isEmptyOrNull(MainApplication.authServiceType)) {
            String[] arrPays = MainApplication.authServiceType.split("\\|");

            for (DynModel dynModel : list) {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                    for (String key : MainApplication.apiProviderMap.keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if(dynModel.getNativeTradeType().contains(",")){
                                    String[] arrNativeTradeType = dynModel.getNativeTradeType().split(",");
                                    for(int  j = 0 ; j < arrNativeTradeType.length;j ++ ){
                                        if(arrNativeTradeType[j].equals(arrPays[i])){
                                            filterLst_pre_auth.add(dynModel);
                                            payTypeMap_pre_auth.put(dynModel.getApiCode(), arrNativeTradeType[j]);
                                            break;
                                        }
                                    }
                                }else if(arrPays[i].equals(dynModel.getNativeTradeType())){
                                    filterLst_pre_auth.add(dynModel);
                                    payTypeMap_pre_auth.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    break;
                                }
                            }
                        }
                    }

                } else {
                    continue;
                }
            }
        }

    }

    /**
     * 获取所有的支付方式
     * <功能详细描述>
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void saveAllpayList(List<DynModel> list, List<DynModel> filterLst) {
        for (DynModel dynModel : list) {
           filterLst.add(dynModel);
        }
    }

    /**
     * 过滤固定二维码支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void filterStacticList(List<DynModel> list, List<DynModel> filterLst) {

        if (!StringUtil.isEmptyOrNull(MainApplication.serviceType)) {
            String[] arrPays = MainApplication.serviceType.split("\\|");

            for (DynModel dynModel : list) {
                if (!StringUtil.isEmptyOrNull(dynModel.getFixedCodeTradeType())) {
                    for (String key : MainApplication.apiProviderMap.keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if(dynModel.getFixedCodeTradeType().contains(",")){
                                    String[] arrFixedCodeTradeType = dynModel.getFixedCodeTradeType().split(",");
                                    for(int  j = 0 ; j < arrFixedCodeTradeType.length;j ++ ){
                                        if(arrFixedCodeTradeType[j].equals(arrPays[i])){
                                            filterLst.add(dynModel);
                                            break;
                                        }
                                    }
                                }else if(arrPays[i].equals(dynModel.getFixedCodeTradeType())){
                                    filterLst.add(dynModel);
                                    break;
                                }

                               /* if (arrPays[i].startsWith(dynModel.getFixedCodeTradeType()) || dynModel.getFixedCodeTradeType().contains(arrPays[i]) || arrPays[i].equals(dynModel.getFixedCodeTradeType())) {
                                    filterLst.add(dynModel);
                                    break;
                                }*/
                            }
                        }
                    }

                } else {
                    continue;
                }
            }
        }

    }

    /**
     * 动态加载支付类型
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void loadPayType() {
        LocalAccountManager.getInstance().apiShowList(new UINotifyListener<List<DynModel>>() {
            @Override
            public void onError(Object object) {
                super.onError(object);
                if (checkSession()) {
                    return;
                }
            }

            @Override
            public void onSucceed(List<DynModel> model) {
                if (null != model && model.size() > 0) {

                    PreferenceUtil.removeKey("payTypeMd5" + ApiConstant.bankCode + MainApplication.getMchId());

                    PreferenceUtil.commitString("payTypeMd5" + ApiConstant.bankCode + MainApplication.getMchId(), model.get(0).getMd5());
                    //显示所有的支付方式
                    List<DynModel> allLst = new ArrayList<DynModel>();
                    saveAllpayList(model,allLst);
                    SharedPreUtile.saveObject(allLst, "dynallPayType" + ApiConstant.bankCode + MainApplication.getMchId());

                    //过滤支持类型
                    List<DynModel> filterLst = new ArrayList<DynModel>();
                    Map<String, String> payTypeMap = new HashMap<String, String>();
                    filterList(model, filterLst, payTypeMap);
                    SharedPreUtile.saveObject(filterLst, "dynPayType" + ApiConstant.bankCode + MainApplication.getMchId());
                    List<DynModel> filterStaticLst = new ArrayList<DynModel>();

                    //过滤预授权支持类型
                   /* List<DynModel> filterLst_pre_auth = new ArrayList<DynModel>();
                    Map<String, String> payTypeMap_auth = new HashMap<String, String>();
                    filterList_pre_auth(model, filterLst_pre_auth, payTypeMap_auth);
                    SharedPreUtile.saveObject(filterLst_pre_auth, "dynPayType_pre_auth" + ApiConstant.bankCode + MainApplication.getMchId());*/

                    //过滤预授权支持类型---写死在前端，写死成支付宝
                    List<DynModel> filterLst_pre_auth = new ArrayList<DynModel>();
                    Map<String, String> payTypeMap_auth = new HashMap<String, String>();
                    DynModel dynModel = new DynModel();
                    dynModel.setNativeTradeType(MainApplication.authServiceType);
                    dynModel.setApiCode("2");
                    filterLst_pre_auth.add(dynModel);
                    payTypeMap_auth.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                    SharedPreUtile.saveObject(filterLst_pre_auth, "dynPayType_pre_auth" + ApiConstant.bankCode + MainApplication.getMchId());


                    //流水过滤条件筛选
                    List<DynModel> filterLstStream = new ArrayList<DynModel>();
                    filterListStream(model, filterLstStream);
                    SharedPreUtile.saveObject(filterLstStream, "filterLstStream" + ApiConstant.bankCode + MainApplication.getMchId());

                    //过滤固定二维码支付类型
                    filterStacticList(model, filterStaticLst);
                    SharedPreUtile.saveObject(filterStaticLst, "dynPayTypeStatic" + ApiConstant.bankCode + MainApplication.getMchId());

                    Map<String, String> typePicMap = new HashMap<String, String>();
                    // 颜色map集合
                    Map<String, String> colorMap = new HashMap<String, String>();

                    Map<String, DynModel> payTypeNameMap = new HashMap<String, DynModel>();

                    for (DynModel d : model) {
                        if (!StringUtil.isEmptyOrNull(d.getSmallIconUrl())) {

                            //                            for (String key : MainApplication.apiProviderMap.keySet())
                            //                            {
                            //                                if (key.equals(d.getApiCode()))
                            //                                {
                            typePicMap.put(d.getApiCode(), d.getSmallIconUrl());
                            //                                }
                            //                            }
                        }

                        if (!StringUtil.isEmptyOrNull(d.getColor())) {
                            //                            for (String key : MainApplication.apiProviderMap.keySet())
                            //                            {
                            //                                if (key.equals(d.getApiCode()))
                            //                                {
                            colorMap.put(d.getApiCode(), d.getColor());
                            //                                }
                            //                            }
                        }

                        //                        if (!StringUtil.isEmptyOrNull(d.getNativeTradeType()))
                        //                        {
                        //                            
                        //                            //遍历map中的键  
                        //                            if (MainApplication.apiProviderMap.size() > 0)
                        //                            {
                        //                                
                        //                                for (String key : MainApplication.apiProviderMap.keySet())
                        //                                {
                        //                                    if (key.equals(d.getApiCode()))
                        //                                    {
                        //                                        payTypeMap.put(d.getApiCode(), d.getNativeTradeType());
                        //                                    }
                        //                                }
                        //                            }
                        //                            else
                        //                            {
                        //                                
                        //                                payTypeMap.put(d.getApiCode(), d.getNativeTradeType());
                        //                            }
                        //                            
                        //                        }

                        if (!StringUtil.isEmptyOrNull(d.getProviderName())) {
                            payTypeNameMap.put(d.getApiCode(), d);
                        }

                    }
                    SharedPreUtile.saveObject(payTypeNameMap, "payTypeNameMap" + ApiConstant.bankCode + MainApplication.getMchId());
                    SharedPreUtile.saveObject(payTypeMap, "payTypeMap" + ApiConstant.bankCode + MainApplication.getMchId());
                    SharedPreUtile.saveObject(payTypeMap_auth, "payTypeMap_pre_auth" + ApiConstant.bankCode + MainApplication.getMchId());
                    //图片
                    SharedPreUtile.saveObject(typePicMap, "payTypeIcon" + ApiConstant.bankCode + MainApplication.getMchId());
                    //                    //颜色值
                    SharedPreUtile.saveObject(colorMap, "payTypeColor" + ApiConstant.bankCode + MainApplication.getMchId());

                }
            }
        });
    }


    private void todo() {
        //启动心跳
        //HearbateService.startService(context);
//        MainActivity.startActivity(WelcomeActivity.this, "WelcomeActivity");
        Intent it = new Intent();
        it.setClass(WelcomeActivity.this, spayMainTabActivity.class);
        startActivity(it);

        //不是操作员就是管理员
        /* if (LocalAccountManager.getInstance().getLoggedUser().role == UserRole.approve) {
             RefundActivity.startActivity(context);  //审核员
         } else  if( LocalAccountManager.getInstance().getLoggedUser().role == UserRole.general){
             PayActivity.startActivity(context); //普通用户
           }else if( LocalAccountManager.getInstance().getLoggedUser().role == UserRole.manager){
             SearchActivity.startActivity(context);  //管理员
         }*/
        finish();
    }

    //    private void checkNet()
    //    {
    //        if (!NetworkUtils.isNetworkAvailable(this))
    //        {
    //            ToastHelper.showError("sorry,网络连接异常，请检查网络后再试");
    //        }
    //        else
    //        {
    //            DataObserverManager.getInstance().registerObserver(new DataObserver(LoginActivity.class.getName(),
    //                DataObserver.EVENT_LOGIN_FAIL_MAKE_DEVICE)
    //            {
    //                @Override
    //                public void onChange()
    //                {
    //                    showMakeDeviceDialog();
    //                }
    //            });
    //        }
    //    }

    //    private void showMakeDeviceDialog()
    //    {
    //        final String name = user_name.getText().toString();
    //        final String pwd = userPwd.getText().toString();
    //        DialogHelper.showDialog("提示",
    //            "设备未激活，是否马上激活设备？",
    //            R.string.btnCancel,
    //            R.string.btnOk,
    //            WelcomeActivity.this,
    //            new DialogInterface.OnClickListener()
    //            {
    //                @Override
    //                public void onClick(DialogInterface dialogInterface, int i)
    //                {
    //                    derviceAction(name, pwd);
    //                }
    //            }).show();
    //    }


    public void onGuideLogin(View v) {
        v.setClickable(false); //W.l 只让点击一次 2014-02-20
        Intent it = new Intent();
        it.setClass(WelcomeActivity.this, WelcomeGuideActivity.class);
        startActivity(it);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        finish();
    }

    public void checkVersion() {
        //不用太平凡的检测升级 所以使用时间间隔区分
        LocalAccountManager.getInstance().saveCheckVersionTime();
        UpgradeManager.getInstance().getVersonCode(new UINotifyListener<UpgradeInfo>() {
            @Override
            public void onError(Object object) {
                super.onError(object);
                startLogin();
            }

            @Override
            public void onSucceed(UpgradeInfo result) {
                super.onSucceed(result);
                if (null != result) {

                    //                    PreferenceUtil.getInt("update", 0);

                    //                    if (result.version > PreferenceUtil.getInt("update", 0))
                    //                    {
                    // "发现新版本"
                    LocalAccountManager.getInstance().saveCheckVersionFlag(result.mustUpgrade);
                    showUpgradeInfoDialog(result, new ComDialogListener(result));
                    //                    }
                } else {
                    startLogin();
                }
            }
        });
    }

    class ComDialogListener implements CommonConfirmDialog.ConfirmListener {
        private UpgradeInfo result;

        public ComDialogListener(UpgradeInfo result) {
            this.result = result;
        }

        @Override
        public void ok() {
            new UpgradeDailog(WelcomeActivity.this, result, new UpgradeDailog.UpdateListener() {
                @Override
                public void cancel() {

                }
            }).show();
        }

        @Override
        public void cancel() {
            //LocalAccountManager.getInstance().saveCheckVersionTime();
            //            if (result.mustUpgrade)
            //            {
            //                MainApplication.getContext().exit();
            //                WelcomeActivity.this.finish();
            //            }
            //            else
            //            {
            //                startLogin();
            //            }

            //            PreferenceUtil.commitInt("update", result.version); // 保存更新，下次进来不更新

        }

    }

    /**
     * 释放已经安装的apk
     */
    public static void clearInstalledAPK() {

        ThreadHelper.executeWithCallback(new Executable() {
            @Override
            public Void execute() {
                try {
                    String appPath = FileUtils.defaultDownloadPath + ApiConstant.APK_NAME;
                    Logger.i("DownloadManager", "delete apk");
                    FileUtils.deleteFile(appPath);
                } catch (Exception e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
                return null;
            }

        }, null);

    }

}
