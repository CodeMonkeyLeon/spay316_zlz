package cn.swiftpass.enterprise.ui.paymentlink.interfaces

import android.widget.ImageView

interface OnImageDeleteViewClickListener {
    fun onImageClick(imageView: ImageView?)
    fun onDeleteClick()
}