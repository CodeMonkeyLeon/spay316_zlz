package cn.swiftpass.enterprise.demo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by YZX on 2019年04月08日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */
public interface LoginApi {

    @FormUrlEncoded
    @POST("spay/user/login")
    Call<JsonResponse<LoginEntity>> login(@Field("account") String account,
                                          @Field("password") String password);

    @FormUrlEncoded
    @POST("/spay/key/register")
    Call<JsonResponse<RegisterEntity>> register(@Field("account") String account,
                                                @Field("password") String password);
}
