package cn.swiftpass.enterprise.ui.activity;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static cn.swiftpass.enterprise.camera.CameraActivity.KEY_IMAGE_PATH;
import static cn.swiftpass.enterprise.ui.activity.MyWebViewActivity.REQUEST_CODE_CAMERA_TAKE_PICTURE;
import static cn.swiftpass.enterprise.utils.RootUtils.isDeviceRootedAndroid;
import static cn.swiftpass.enterprise.utils.RootUtils.isDeviceRootedPos;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.example.common.sentry.SentryUtils;
import com.example.common.sp.PreferenceUtil;
import com.example.common.utils.PngSetting;
import com.ziyeyouhu.library.KeyboardTouchListener;
import com.ziyeyouhu.library.KeyboardUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.model.CertificateBean;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo;
import cn.swiftpass.enterprise.bussiness.model.ErrorMsg;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo;
import cn.swiftpass.enterprise.bussiness.model.WalletListBean;
import cn.swiftpass.enterprise.common.Constant;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants;
import cn.swiftpass.enterprise.mvp.contract.activity.WelcomeContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.WelcomePresenter;
import cn.swiftpass.enterprise.ui.activity.diagnosis.view.NetworkDiagnosisActivity;
import cn.swiftpass.enterprise.ui.activity.live.KeepAliveService;
import cn.swiftpass.enterprise.ui.activity.setting.SettingCDNActivity;
import cn.swiftpass.enterprise.ui.activity.user.FindPassFirstActivity;
import cn.swiftpass.enterprise.ui.test.view.TestActivity;
import cn.swiftpass.enterprise.ui.widget.CommonConfirmDialog;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.dialog.CashierNoticeDialog;
import cn.swiftpass.enterprise.ui.widget.dialog.NoNetworkDialog;
import cn.swiftpass.enterprise.ui.widget.dialog.RootDialog;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.CertificateUtils;
import cn.swiftpass.enterprise.utils.ECDHUtils;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.FileUtils;
import cn.swiftpass.enterprise.utils.KotlinUtils;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.NetworkUtils;
import cn.swiftpass.enterprise.utils.PngUtils;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.interfaces.OnOkClickListener;
import cn.swiftpass.enterprise.utils.permissionutils.Permission;
import cn.swiftpass.enterprise.utils.permissionutils.PermissionDialogUtils;

/**
 * 欢迎界面
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-8-20
 * Time: 下午3:30
 */
public class WelcomeActivity extends BaseActivity<WelcomeContract.Presenter> implements WelcomeContract.View {
    private static final String TAG = WelcomeActivity.class.getCanonicalName();
    public LinearLayout ll_bdo_ekyc;
    public boolean flag = true;
    String login_skey, login_sauthid;
    int count = 0;
    private Button btn_login;
    private TextView tvVersion;
    private TextView tvchangeCDN;
    private int[] imageResId; // 图片ID
    private ViewPager viewPager;
    private int currentItem = 0; // 当前图片的索引号
    private List<ImageView> imageViews; // 滑动的图片集合
    private SharedPreferences sp;
    private EditText userPwd;
    private EditText user_name;
    private ImageView iv_clearUser;
    private ImageView iv_clearPwd;

    private TextView mTvNetworkDiagnosis;

    private final OnFocusChangeListener listener = new OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            switch (v.getId()) {
                case R.id.user_name:
                    if (hasFocus) {
                        //                        user_name.setCursorVisible(true);
                        if (user_name.getText().length() > 0) {
                            iv_clearUser.setVisibility(View.VISIBLE);
                        } else {
                            iv_clearUser.setVisibility(View.GONE);
                        }
                    } else {
                        iv_clearUser.setVisibility(View.GONE);
                    }
                    break;
                case R.id.userPwd:
                    //                    sp.edit().putString("userPwd", userPwd.getText().toString()).commit();

                    if (hasFocus) {
                        userPwd.setCursorVisible(true);
                        if (userPwd.getText().length() > 0) {
                            iv_clearPwd.setVisibility(View.VISIBLE);
                        } else {
                            iv_clearPwd.setVisibility(View.GONE);
                        }
                    } else {
                        iv_clearPwd.setVisibility(View.GONE);
                    }

                    break;
            }

        }
    };
    private ImageView iv_code;
    private View v_code;
    private TextView tv_load;
    private LinearLayout ly_code;
    private EditText ed_code;
    private boolean isInputCode = false;
    private LinearLayout ly_title;
    private RelativeLayout ly_first;
    private ImageView iv_login;
    private TextView tv_server_name;
    private Button btn_sign_up;
    private TextView tv_check_ekyc_state;
    private boolean isNeedRequestECDHKey = false;
    private boolean isNeedDeviceLogin = false;
    private RootDialog Dialog_root;
    private NoNetworkDialog Dialog_No_Network;
    private CashierNoticeDialog ChangePswNoticeDialog;
    private boolean cerFailedRequestAgain = false;
    private LinearLayout rootView;
    private KeyboardUtil keyboardUtil;
    private ScrollView scrollView;
    private OnClickListener clearImage = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.iv_clearUser:
                    iv_clearUser.setVisibility(View.GONE);
                    user_name.setText("");
                    break;
                case R.id.iv_clearPwd:
                    iv_clearPwd.setVisibility(View.GONE);
                    userPwd.setText("");
                    break;
                default:
                    break;
            }
        }
    };
    private UpgradeInfo mApkDownInfo = null;

    public static void startActivity(Context context, String activeId, String activeName, String content) {

        Intent it = new Intent();
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        it.setClass(context, WelcomeActivity.class);
        it.putExtra("activeId", activeId);
        it.putExtra("activeName", activeName);
        it.putExtra("content", content);
        context.startActivity(it);

    }

    public static void startActivity(Context context) {
        Intent it = new Intent();
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        it.setClass(context, WelcomeActivity.class);
        context.startActivity(it);

    }

    /**
     * 释放已经安装的apk
     */
    public static void clearInstalledAPK() {

        ThreadHelper.executeWithCallback(new Executable() {
            @Override
            public Void execute() {
                try {
                    String appPath = MainApplication.getInstance().getDefaultDownloadPath() + BuildConfig.appName;
                    Logger.d("DownloadManager delete apk");
                    FileUtils.deleteFile(appPath);
                } catch (Exception e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(e));
                }
                return null;
            }

        }, null);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    //安全键盘
    private void initMoveKeyBoard() {
        keyboardUtil = new KeyboardUtil(this, rootView, scrollView);
        keyboardUtil.setOtherEdittext(user_name);
        keyboardUtil.setOtherEdittext(ed_code);

        //测试代码
//        keyboardUtil.setOtherEdittext(et_input_url);

        // monitor the KeyBarod state
        keyboardUtil.setKeyBoardStateChangeListener(new WelcomeActivity.KeyBoardStateListener());
        // monitor the finish or next Key
        keyboardUtil.setInputOverListener(new WelcomeActivity.inputOverListener());
        userPwd.setOnTouchListener(new KeyboardTouchListener(keyboardUtil, KeyboardUtil.INPUT_TYPE_ABC, -1));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (keyboardUtil.isShow) {
                keyboardUtil.hideSystemKeyBoard();
                keyboardUtil.hideAllKeyBoard();
                keyboardUtil.hideKeyboardLayout();

                finish();
            } else {
                return super.onKeyDown(keyCode, event);
            }
            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (BuildConfig.FLAVOR.equalsIgnoreCase("u_test")) {
            initAddress();
            String domain = MainApplication.getInstance().getBaseUrl().substring(0, MainApplication.getInstance().getBaseUrl().length() - 1);
            tv_server_name.setText(domain);
            tv_server_name.setVisibility(View.VISIBLE);
        }

        PermissionDialogUtils.requestPermission(WelcomeActivity.this,
                new String[]{Permission.MODIFY_AUDIO}, false, null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            //android13  通知权限申请
            PermissionDialogUtils.requestPermission(WelcomeActivity.this,
                    new String[]{Permission.POST_NOTIFICATIONS}, false, null);
        }

        //重新把登录button恢复成可点击的状态
        btn_login.setEnabled(true);
        btn_login.setText(R.string.login);

        user_name.requestFocus();
        if (!isNeedDeviceLogin) {
            ECDHKeyExchange(true);
        }

        keepAlive();
    }

    @Override
    protected void onPause() {
        super.onPause();

        //回到后台的时候，清除界面的密码
        iv_clearPwd.setVisibility(View.GONE);
        userPwd.setText("");

        //同时隐藏键盘
        if (keyboardUtil.isShow) {
            keyboardUtil.hideSystemKeyBoard();
            keyboardUtil.hideAllKeyBoard();
            keyboardUtil.hideKeyboardLayout();
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_guide);

        sp = this.getSharedPreferences("login", 0);

        btn_login = getViewById(R.id.btn_login);

        tvVersion = getViewById(R.id.tv_versionName);
        tvchangeCDN = getViewById(R.id.tv_network_setting);

        if (!BuildConfig.isShowNetConfig) {
            //隐藏网络设置
            tvchangeCDN.setVisibility(View.INVISIBLE);
        }

        initView();

        boolean isDeviceRooted = false;
        if (BuildConfig.IS_POS_VERSION) {
            isDeviceRooted = isDeviceRootedPos();
        } else {
            isDeviceRooted = isDeviceRootedAndroid();
        }

        if (isDeviceRooted) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Dialog_root = new RootDialog(WelcomeActivity.this, new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Dialog_root.dismiss();
                            for (Activity a : MainApplication.getInstance().getAllActivities()) {
                                a.finish();
                            }
                        }
                    });

                    if (Dialog_root != null && !Dialog_root.isShowing()) {
                        String message = getStringById(R.string.system_root_instruction);
                        Dialog_root.setMessage(message);
                        Dialog_root.setCanceledOnTouchOutside(false);
                        Dialog_root.show();
                    }
                }
            });
        }

        login_skey = PreferenceUtil.getString("login_skey", "");
        login_sauthid = PreferenceUtil.getString("login_sauthid", "");

        //V3.0.5 CA 证书版本加入 --- 2020/04/02 Mia
        //加入CA证书校验的判断，是否过期
        //1：先读取本地证书
        CertificateUtils.getCertificateList();

        if (!StringUtil.isEmptyOrNull(login_skey) && !StringUtil.isEmptyOrNull(login_sauthid)
                && !StringUtil.isEmptyOrNull(MainApplication.getInstance().isDefault())
                && !MainApplication.getInstance().isDefault().equalsIgnoreCase("1")) {

            //先去调用公钥交换接口然后再调用设备登录接口,每次设备登录前都主动去调用ECDH去交换
            if (!TextUtils.isEmpty(AppHelper.getUUID())) {
                //2：判断本地证书是否过期
                if (CertificateUtils.checkCertificatesListValid()) {//本地证书中只要有一个没有过期
                    //则先走正常的验证网络请求逻辑，检测证书是否有效
                    isNeedDeviceLogin = true;
                    ECDHKeyExchange(false);
                } else {
                    //证书不在有效期内，则先请求CA证书公钥获取接口
                    requestCertificate(true);
                }
            }
        } else {
            MainApplication.getInstance().setUpdateShow(false);
            checkVersionisUpdate();
            //回话过期直接跳转到登录界面
            showLoginPage();
        }

        initVale();
        clearInstalledAPK();
        //设置监听
        setLister();

        if (PngSetting.INSTANCE.isChangePng1()) {
            initPng();
        }
    }


    private void initPng() {
        ImageView imgUser = findViewById(R.id.login_ic);
        ImageView imgPassword = findViewById(R.id.pwd_ic);

        PngUtils.INSTANCE.setPng1(this, imgUser, R.drawable.icon_login_user);
        PngUtils.INSTANCE.setPng1(this, imgPassword, R.drawable.icon_login_userpassword);
    }

    private void showLoginPage() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //回话过期直接跳转到登录界面
                ly_first.setVisibility(View.GONE);
                ly_title.setVisibility(View.VISIBLE);
                scrollView.setVisibility(View.VISIBLE);
            }
        }, 1000);
    }

    @Override
    public void ecdhKeyExchangeSuccess(ECDHInfo response, boolean isInIt, String privateKey) {
        if (response != null) {
            try {
                String secretKey = ECDHUtils.getInstance().ecdhGetShareKey(
                        MainApplication.getInstance().getSerPubKey(), privateKey);
                SharedPreUtils.saveObject(secretKey, ParamsConstants.SECRET_KEY);

                if (isInIt) return;
                //如果是登录失败报错之后再请求公钥交换接口，则此时直接调用登录
                if (isNeedRequestECDHKey) {
                    onLogin();
                }

                //如果是设备登录的时候发现没有skey调用公钥交换接口，则此时直接调用设备登录接口
                if (isNeedDeviceLogin && !StringUtil.isEmptyOrNull(MainApplication.getInstance().isDefault())
                        && !MainApplication.getInstance().isDefault().equalsIgnoreCase("1")) {
                    deviceLogin();
                }

            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                Log.e(TAG, Log.getStackTraceString(e));

                showLoginPage();
            }
        }
    }

    @Override
    public void ecdhKeyExchangeFailed(Object error, boolean isInIt) {
        if (isInIt) return;
        if (!error.toString().contains("401")) {
            //如果是设备登录请求的ECDH交换
            if (isNeedDeviceLogin) {
                //修改此处逻辑---2020/04/09
                //如果是设备登录证书校验失败,不管本地有没有公钥证书，则直接调招到一键重登界面
                //只要是设备登录ECDH交换失败，不管是什么原因，证书校验失败也好，其他情况失败也好，都跳到一键登录
                if (error.toString().contains(RequestResult.HANDSHAKE_CER_ERROR)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //则跳转到一键重登页面

                            Intent it = new Intent();
                            it.putExtra("isCerFailed", true);
                            it.setClass(WelcomeActivity.this, ReLoginActivity.class);
                            startActivity(it);
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //则跳转到一键重登页面

                            Intent it = new Intent();
                            it.setClass(WelcomeActivity.this, ReLoginActivity.class);
                            startActivity(it);
                        }
                    });
                }

            } else { //如果是登录前请求的ECDH交换失败
                //此处修改新逻辑----2020/04/09
                //如果是证书校验失败,且不管本地没有公钥证书，则弹框提示,文案：请求失败，请重试
                if (error.toString().contains(RequestResult.HANDSHAKE_CER_ERROR)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //如果是登录前请求的ECDH，然后校验证书失败了，则弹框提示
                            // 则下次点击，再登录前请求ECDH前，先请求一下公钥接口
                            cerFailedRequestAgain = true;


                            showLoginPage();
                            MainApplication.getInstance().setNeedLogin(false);
                            btn_login.setEnabled(true);
                            btn_login.setText(R.string.login);

                            toastDialog(WelcomeActivity.this, R.string.certificate_request_failed, null);
                        }
                    });
                } else {
                    //如果错误不是证书校验失败，是ECDH交换失败的其他错误，则直接跳转到登录页，需要重新走登录流程
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            showLoginPage();
                            MainApplication.getInstance().setNeedLogin(false);
                            btn_login.setEnabled(true);
                            btn_login.setText(R.string.login);
                        }
                    });
                }
            }
        } else {
            //如果是会话已过期，则必须重新登录
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    showLoginPage();
                    MainApplication.getInstance().setNeedLogin(false);
                }
            });
        }
    }

    private void ECDHKeyExchange(boolean isInit) {
        try {
            ECDHUtils.getInstance().getAppPubKey();
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
        }
        final String publicKey = SharedPreUtils.readProduct(ParamsConstants.APP_PUBLIC_KEY).toString();
        final String privateKey = SharedPreUtils.readProduct(ParamsConstants.APP_PRIVATE_KEY).toString();

        if (mPresenter != null) {
            mPresenter.ecdhKeyExchange(publicKey, isNeedRequestECDHKey, isInit, privateKey);
        }
    }

    @Override
    public void deviceLoginSuccess(boolean response) {
        if (response) {
            loginSuccToLoad(false);
            isNeedDeviceLogin = false;
        }
    }

    @Override
    public void deviceLoginFailed(Object error) {
        isNeedDeviceLogin = false;
        if (MainApplication.getInstance().isNeedLogin()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //如果是会话已过期，则必须重新登录
                    showLoginPage();
                    MainApplication.getInstance().setNeedLogin(false);
                    MainApplication.getInstance().setUpdateShow(false);
                    checkVersionisUpdate();
                }
            });
        } else {
            if (error != null) {
                if (error.toString().contains("401")) {
                    //如果是会话已过期，则必须重新登录
                    showLoginPage();
                    MainApplication.getInstance().setNeedLogin(false);
                    MainApplication.getInstance().setUpdateShow(false);
                    checkVersionisUpdate();
                } else if (error.toString().contains(RequestResult.HANDSHAKE_CER_ERROR)) {//证书校验失败
                    //理论上不会进入这个逻辑，以防万一
                    //因为只有ECDH交换成功才会进入设备登录，ECDH需要校验证书的合法，故进入此接口时，
                    //证书理论上不会校验失败，如果万一失败了，直接进入一键登录的页面
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //则跳转到一键重登页面
                            Intent it = new Intent();
                            it.putExtra("isCerFailed", true);
                            it.setClass(WelcomeActivity.this, ReLoginActivity.class);
                            startActivity(it);
                        }
                    });
                } else {
                    toastDialog(WelcomeActivity.this, error.toString(), new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            WelcomeActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //则跳转到一键重登页面
                                    if (!error.toString().contains("401")) {
                                        Intent it = new Intent();
                                        it.setClass(WelcomeActivity.this, ReLoginActivity.class);
                                        startActivity(it);
                                    }
                                }
                            });
                        }

                    });
                }

            }
        }
    }

    private void deviceLogin() {
        MainApplication.getInstance().setUpdateShow(true);
        if (mPresenter != null) {
            mPresenter.deviceLogin();
        }
    }

    /**
     * 开启保活服务
     */
    private void keepAlive() {
        startService(new Intent(getApplicationContext(), KeepAliveService.class));
    }

    /**
     * 检查版本更新
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void checkVersionisUpdate() {
        if (NetworkUtils.isNetWorkValid(WelcomeActivity.this)) {
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            checkVersion();
                        }
                    });

                }

            };
            Timer timer = new Timer();
            timer.schedule(task, 200);
        } else {
            if (Dialog_No_Network == null) {
                Dialog_No_Network = new NoNetworkDialog(WelcomeActivity.this, new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Dialog_No_Network != null && Dialog_No_Network.isShowing()) {
                            Dialog_No_Network.dismiss();

                            Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                            WelcomeActivity.this.startActivity(intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        }
                    }
                });
                String str = getStringById(R.string.to_open_network);
                Dialog_No_Network.setContentText(str);
                Dialog_No_Network.setConfirmText(R.string.to_open);
                Dialog_No_Network.setCanceledOnTouchOutside(false);
            }
            if (!Dialog_No_Network.isShowing()) {
                Dialog_No_Network.show();
            }
        }
    }

    /**
     * base64转为bitmap
     *
     * @param base64Data
     * @return
     */
    public Bitmap base64ToBitmap(String base64Data) {
        byte[] bytes = Base64.decode(base64Data, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    private void initAddress() {
        String serviceConfig = PreferenceUtil.getString(Constant.SERVER_CONFIG, "");
        Log.i("TAG_ZLZ", "welcome的serverConfig: " + serviceConfig);
        if (TextUtils.isEmpty(serviceConfig)) {
            //要进行是否开启CDN的判断
            String CDN_status = PreferenceUtil.getString("CDN", "open");
            if (CDN_status.equals("open")) {
                MainApplication.getInstance().setBaseUrl(BuildConfig.serverAddr);
            } else {
                MainApplication.getInstance().setBaseUrl(BuildConfig.serverAddrBack);
            }
            Log.i("TAG_TEST", "base url: (serviceConfig null)  " + MainApplication.getInstance().getBaseUrl());
        } else {
            switch (serviceConfig) {
                case Constant.SERVER_ADDRESS_CHECKOUT_DEV:
                    MainApplication.getInstance().setBaseUrl(BuildConfig.serverAddrCheckoutDev);

                    break;
                case Constant.SERVER_ADDRESS_OVERSEAS_61:
                    MainApplication.getInstance().setBaseUrl(BuildConfig.serverAddrOverseas61);
                    break;


                case Constant.SERVER_ADDRESS_OVERSEAS_63:
                    MainApplication.getInstance().setBaseUrl(BuildConfig.serverAddrOverseas63);
                    break;

                case Constant.SERVER_ADDRESS_DEV:
                    MainApplication.getInstance().setBaseUrl(BuildConfig.serverAddrDev);
                    break;

                case Constant.SERVER_ADDRESS_TEST_123:
                    MainApplication.getInstance().setBaseUrl(BuildConfig.serverAddrTest123);
                    break;

                case Constant.SERVER_ADDRESS_TEST_61:
                    MainApplication.getInstance().setBaseUrl(BuildConfig.serverAddrTest61);
                    break;

                case Constant.SERVER_ADDRESS_TEST_63:
                    MainApplication.getInstance().setBaseUrl(BuildConfig.serverAddrTest63);
                    break;

                case Constant.SERVER_ADDRESS_TEST_UAT:
                    MainApplication.getInstance().setBaseUrl(BuildConfig.serverAddrTestUAT);
                    break;

                case Constant.SERVER_ADDRESS_PRD:
                    MainApplication.getInstance().setBaseUrl(BuildConfig.serverAddrPrd);
                    break;

                case Constant.SERVER_ADDRESS_DEV_JH:
                    MainApplication.getInstance().setBaseUrl(BuildConfig.serverAddrDevJH);
                    break;

                case Constant.SERVER_ADDRESS_SELF:
                    MainApplication.getInstance().setBaseUrl(PreferenceUtil.getString(Constant.SERVER_ADDRESS_SELF, ""));
                    break;
            }
            Log.i("TAG_TEST", "base url: (serviceConfig not null)  " + MainApplication.getInstance().getBaseUrl());
        }
    }

    private void setLister() {
        iv_login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (BuildConfig.FLAVOR.equalsIgnoreCase("u_test")) {
//                    setServerAddress();
                    TestActivity.Companion.startTestActivity(WelcomeActivity.this);
                }
            }
        });


        iv_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPresenter != null) {
                    WelcomeActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tv_load.setText(R.string.public_loading);
                            tv_load.setVisibility(View.VISIBLE);
                        }
                    });
                    mPresenter.getCode();
                }
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //先收起键盘
                if (keyboardUtil.isShow) {
                    keyboardUtil.hideSystemKeyBoard();
                    keyboardUtil.hideAllKeyBoard();
                    keyboardUtil.hideKeyboardLayout();
                }

//                refreshServerAddress(tv_server_name.getText().toString());
                //如果点击登录按钮的时候缓存中还没有skey和serPubKey,则先调用公钥接口
                if (!TextUtils.isEmpty(MainApplication.getInstance().getSKey()) && !TextUtils.isEmpty(MainApplication.getInstance().getSerPubKey())) {
                    if (CertificateUtils.checkCertificatesListValid()) {
                        //本地证书没过期
                        //如果是之前登录过，重登陆的时候，发现证书失效，则先请求公钥接口，再走后续流程
                        if (cerFailedRequestAgain) {
                            requestCertificate(false);
                        } else {
                            onLogin();
                        }
                    } else {
                        //本地证书全部过期，则先请求CA证书公钥获取接口
                        requestCertificate(false);
                    }

                } else {

                    String name = user_name.getText().toString().trim();
                    String pwd = userPwd.getText().toString().trim();

                    if ("".equals(name)) {
                        toastDialog(WelcomeActivity.this, R.string.show_user_name, null);
                        user_name.setFocusable(true);
                        return;
                    }

                    if ("".equals(pwd)) {
                        toastDialog(WelcomeActivity.this, R.string.pay_login_pwd, null);//(getString(R.string.pay_refund_pwd));
                        userPwd.setFocusable(true);
                        return;
                    }

                    if (isInputCode && StringUtil.isEmptyOrNull(ed_code.getText().toString())) {
                        toastDialog(WelcomeActivity.this, R.string.tx_ver_code, null);
                        ed_code.setFocusable(true);
                        return;
                    }

                    //1：先读取本地证书
                    CertificateUtils.getCertificateList();
                    //2：判断本地证书是否过期
                    if (CertificateUtils.checkCertificatesListValid()) {//本地证书中只要有一个没有过期
                        //则先走正常的验证网络请求逻辑，检测证书是否有效
                        if (cerFailedRequestAgain) {
                            requestCertificate(false);
                        } else {
                            isNeedRequestECDHKey = true;
                            Logger.d("Welcome  click login ECDHKeyExchange");
                            ECDHKeyExchange(false);
                        }
                    } else {
                        //证书不在有效期内，则先请求CA证书公钥获取接口
                        requestCertificate(false);
                    }
                }
            }
        });

        tvVersion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                refreshServerAddress(tv_server_name.getText().toString());
                showPage(FindPassFirstActivity.class);
                //                MainApplication.updatePwdActivities.add(WelcomeActivity.this);
            }
        });

        tvchangeCDN.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showPage(SettingCDNActivity.class);
            }
        });
    }

    @Override
    public void getCodeSuccess(String response) {
        if (null != response) {
            tv_load.setVisibility(View.GONE);
            iv_code.setVisibility(View.VISIBLE);
            Bitmap bitmap = base64ToBitmap(response);
            if (null != bitmap) {
                iv_code.setImageBitmap(bitmap);
            }
        }
    }

    @Override
    public void getCodeFailed(Object error) {
        WelcomeActivity.this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                tv_load.setText(R.string.tv_code_refresh);
                tv_load.setVisibility(View.GONE);
            }
        });
    }

    /***
     * 初始化
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initVale() {
        String userNmae = sp.getString("user_name", "");

        user_name.setText(userNmae);
    }

    /**
     * 初始化
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initView() {
        iv_login = getViewById(R.id.iv_login);
        tv_server_name = getViewById(R.id.tv_server_name);
        rootView = findViewById(R.id.rootView);
        scrollView = findViewById(R.id.sv_main);

        ly_first = getViewById(R.id.ly_first);
        ly_title = getViewById(R.id.ly_title);
        ed_code = getViewById(R.id.ed_code);
        mTvNetworkDiagnosis = findViewById(R.id.id_btn_network_diagnosis);
        if (!BuildConfig.isShowNetworkDiagnosis) {
            mTvNetworkDiagnosis.setVisibility(View.GONE);
        }
        //定义hint的值
        SpannableString ss = new SpannableString(getResources().getString(R.string.bt_code_not_null));
        AbsoluteSizeSpan ass = new AbsoluteSizeSpan(14, true);
        ss.setSpan(ass, 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ed_code.setHint(new SpannedString(ss));
        ly_code = getViewById(R.id.ly_code);
        v_code = getViewById(R.id.v_code);
        tv_load = getViewById(R.id.tv_load);
        //        viewPager = (ViewPager)findViewById(R.id.vp);
        iv_code = getViewById(R.id.iv_code);

        //login_remember = getViewById(R.id.login_remember);

        imageResId = new int[]{R.drawable.n_icon_login_01, R.drawable.n_icon_login_02};

        imageViews = new ArrayList<ImageView>();


        user_name = getViewById(R.id.user_name);

        userPwd = getViewById(R.id.userPwd);
        userPwd.setTypeface(user_name.getTypeface());

        iv_clearUser = getViewById(R.id.iv_clearUser);

        iv_clearPwd = getViewById(R.id.iv_clearPwd);

        iv_clearUser.setOnClickListener(clearImage);
        iv_clearPwd.setOnClickListener(clearImage);

        ll_bdo_ekyc = getViewById(R.id.ll_bdo_ekyc);
        btn_sign_up = getViewById(R.id.btn_sign_up);

        tv_check_ekyc_state = getViewById(R.id.tv_check_ekyc_state);


        mTvNetworkDiagnosis.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    mTvNetworkDiagnosis.setTextColor(getResources().getColor(R.color.color_app_theme_half));
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    mTvNetworkDiagnosis.setTextColor(getResources().getColor(R.color.color_app_theme));
                    goToNetworkDiagnosisPage();
                }
                return true;
            }
        });


        //如果是BDO下来的按钮才展示，否则不展示
        if (BuildConfig.bankCode.contains("bdo")) {
            ll_bdo_ekyc.setVisibility(View.VISIBLE);
            btn_sign_up.setVisibility(View.VISIBLE);
            tv_check_ekyc_state.setVisibility(View.VISIBLE);
        } else {
            ll_bdo_ekyc.setVisibility(View.GONE);
            btn_sign_up.setVisibility(View.GONE);
            tv_check_ekyc_state.setVisibility(View.GONE);
        }

        user_name.setOnFocusChangeListener(listener);

        userPwd.setOnFocusChangeListener(listener);

        ed_code.setOnFocusChangeListener(listener);

        EditTextWatcher editTextWatcher = new EditTextWatcher();

        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {

                if (user_name.isFocused()) {
                    //                    userPwd.setCursorVisible(false);
                    user_name.setCursorVisible(true);
                    if (user_name.getText().toString().length() > 0) {
                        iv_clearUser.setVisibility(View.VISIBLE);
                    } else {
                        iv_clearPwd.setVisibility(View.GONE);

                    }
                } else if (userPwd.isFocused()) {
                    userPwd.setCursorVisible(true);
                    if (userPwd.getText().toString().length() > 0) {
                        iv_clearPwd.setVisibility(View.VISIBLE);
                    } else {

                        iv_clearUser.setVisibility(View.GONE);

                    }
                } /*else if (shop_id.isFocused()) {

                    iv_clearUser.setVisibility(View.GONE);
                    iv_clearPwd.setVisibility(View.GONE);

                }*/
                if (isInputCode) {
                    if (!StringUtil.isEmptyOrNull(user_name.getText().toString()) && !StringUtil.isEmptyOrNull(userPwd.getText().toString()) && !StringUtil.isEmptyOrNull(ed_code.getText().toString())) {
                        setButtonBg(btn_login, true, R.string.login);
                    }

                } else {

                    if (!StringUtil.isEmptyOrNull(user_name.getText().toString()) && !StringUtil.isEmptyOrNull(userPwd.getText().toString())) {
                        setButtonBg(btn_login, true, R.string.login);
                    }
                }

            }

        });

        user_name.addTextChangedListener(editTextWatcher);
        userPwd.addTextChangedListener(editTextWatcher);
        initMoveKeyBoard();
    }

    private void goToNetworkDiagnosisPage() {
        NetworkDiagnosisActivity.Companion.startNetworkDiagnosisActivity(this);
    }

    //调用自定义相机的回调代码----测试用，可以移植到任何地方
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == KotlinUtils.APP_INSTALL_REQUEST_PERMISSION) {
            //授予安装未知应用权限
            requestApkInstallPermission();
        }
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_CAMERA_TAKE_PICTURE
                && null != data) {
            String path = data.getStringExtra(KEY_IMAGE_PATH);
            Uri photoUri = Uri.parse(path);
            if (photoUri != null) {
                //若生成了uri，则表示该文件添加成功
                //使用流将该uri中的内容写入字节数组即可
                byte[] image;
                try {
                    InputStream inputStream = getContentResolver().openInputStream(photoUri);
                    if (null == inputStream || 0 == inputStream.available()) {
                        return;
                    }
                    image = new byte[inputStream.available()];
                    inputStream.read(image);
                    inputStream.close();

                } catch (IOException e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                } catch (Exception e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                }
            }
        } else if (resultCode == Activity.RESULT_FIRST_USER && requestCode == REQUEST_CODE_CAMERA_TAKE_PICTURE) {
            showToastInfo(getStringById(R.string.instapay_save_failed));
        }

    }

    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b, enable, res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_press_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected boolean isLoginRequired() {
        return false;
    }

    private void startLogin() {
        if (KotlinUtils.INSTANCE.isLoggedIn()) {
            Intent it = new Intent();
            it.setClass(WelcomeActivity.this, spayMainTabActivity.class);
            startActivity(it);

            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            finish();
        }
    }

    /***
     * 登录操作
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    public void onLogin() {
        String name = user_name.getText().toString().trim();
        String pwd = userPwd.getText().toString().trim();

        if ("".equals(name)) {
            toastDialog(WelcomeActivity.this, R.string.show_user_name, null);
            user_name.setFocusable(true);
            return;
        }

        if ("".equals(pwd)) {
            toastDialog(WelcomeActivity.this, R.string.pay_login_pwd, null);//(getString(R.string.pay_refund_pwd));
            userPwd.setFocusable(true);
            return;
        }

        if (isInputCode && StringUtil.isEmptyOrNull(ed_code.getText().toString())) {
            toastDialog(WelcomeActivity.this, R.string.tx_ver_code, null);
            ed_code.setFocusable(true);
            return;
        }
        if (NetworkUtils.isNetWorkValid(WelcomeActivity.this)) {
            login(name, pwd);
        } else {
            showToastInfo(getString(R.string.show_no_network));
        }
    }

    //APP清掉本地缓存的预授权和消费的切换
    public void clearAPPState() {
        PreferenceUtil.removeKey("choose_pay_or_pre_auth");
        PreferenceUtil.removeKey("bill_list_choose_pay_or_pre_auth");

    }

    @Override
    public void loginAsyncSuccess(boolean response, String password) {
        if (response) {

            sp.edit().putString("user_name", user_name.getText().toString()).commit();
            //如果是默认密码登录的，则弹框提示，强制要求修改密码
            final String oldpsw = password;
            if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().isDefault())
                    && MainApplication.getInstance().isDefault().equalsIgnoreCase("1")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (ChangePswNoticeDialog == null) {
                            ChangePswNoticeDialog = new CashierNoticeDialog(WelcomeActivity.this);
                            ChangePswNoticeDialog.setContentText(R.string.change_psw_instruction);
                            ChangePswNoticeDialog.setConfirmText(R.string.bt_dialog_ok);
                            ChangePswNoticeDialog.setCanceledOnTouchOutside(false);
                            //设置点击返回键不消失
                            ChangePswNoticeDialog.setCancelable(false);
                            ChangePswNoticeDialog.setCurrentListener(new CashierNoticeDialog.ButtonConfirmCallBack() {
                                @Override
                                public void onClickConfirmCallBack() {
                                    if (ChangePswNoticeDialog != null && ChangePswNoticeDialog.isShowing()) {
                                        ChangePswNoticeDialog.dismiss();
                                        //跳转到修改密码的页面
                                        ChangePswLoginActivity.startActivity(WelcomeActivity.this, oldpsw);
                                    }
                                }
                            });
                        }
                        if (!ChangePswNoticeDialog.isShowing()) {
                            ChangePswNoticeDialog.show();
                        }
                    }
                });
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        isNeedRequestECDHKey = false;
                        loginSuccToLoad(true);
                    }
                });
            }
        }
    }

    @Override
    public void loginAsyncFailed(Object error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (error != null) {
                    if (error.toString().startsWith("405")) {//Require to renegotiate ECDH key
                        isNeedRequestECDHKey = true;
                        Logger.d("Welcome  login error need request ECDHKeyExchange");
                        ECDHKeyExchange(false);
                    } else if (error.toString().contains(RequestResult.HANDSHAKE_CER_ERROR)) {//证书校验失败
                        cerFailedRequestAgain = true;
                        //登录如果证书校验失败，则直接弹框提示

                        toastDialog(WelcomeActivity.this, getStringById(R.string.certificate_request_failed), null);

                        btn_login.setEnabled(true);
                        btn_login.setText(R.string.login);

                    } else {
                        try {

                            btn_login.setEnabled(true);
                            btn_login.setText(R.string.login);

                            final ErrorMsg msg = (ErrorMsg) error;
                            if (!StringUtil.isEmptyOrNull(msg.getMessage())) {
                                toastDialog(WelcomeActivity.this, msg.getMessage(), new NewDialogInfo.HandleBtn() {

                                    @Override
                                    public void handleOkBtn() {
                                        if (!StringUtil.isEmptyOrNull(msg.getContent())) {
                                            Bitmap bitmap = base64ToBitmap(msg.getContent());
                                            if (null != bitmap) {
                                                iv_code.setVisibility(View.VISIBLE);
                                                tv_load.setVisibility(View.GONE);
                                                iv_code.setImageBitmap(bitmap);
                                            }
                                        }
                                    }

                                });
                            }
                        } catch (Exception e) {

                            btn_login.setEnabled(true);
                            btn_login.setText(R.string.login);

                            if (error.toString().startsWith("403")) {//有验证码
                                isInputCode = true;
                                ly_code.setVisibility(View.VISIBLE);
                                v_code.setVisibility(View.VISIBLE);
                                String code = error.toString().substring(3);
                                if (!StringUtil.isEmptyOrNull(code)) {

                                    Bitmap bitmap = base64ToBitmap(code);
                                    if (null != bitmap) {
                                        iv_code.setVisibility(View.VISIBLE);
                                        tv_load.setVisibility(View.GONE);
                                        iv_code.setImageBitmap(bitmap);
                                    }
                                }
                            } else {
                                toastDialog(WelcomeActivity.this, error.toString(), null);
                            }
                        }
                    }
                }
            }
        });
    }

    /**
     * 登录
     * <功能详细描述>
     *
     * @param username
     * @param password
     * @see [类、类#方法、类#成员]
     */
    private void login(String username, final String password) {

        if (mPresenter != null) {
            btn_login.setEnabled(false);
            btn_login.setText(R.string.bt_login_loading);
            clearAPPState();
            mPresenter.loginAsync(ed_code.getText().toString(), username, password);
        }
    }

    void loginSuccToLoad(boolean isLoad) {
        //        //加载保存登录用户信息
        sp.edit().putString("user_name", user_name.getText().toString()).commit();

        MainApplication.getInstance().getUserInfo().username = user_name.getText().toString();

        //主动做一次蓝牙连接
        try {
            connentBlue();
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
        }

        loadPayType();
        //getExchangeRate();
        startLogin();
    }

    /**
     * 过滤支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */

    private void filterListStream(List<DynModel> list, List<DynModel> filterLst) {

        for (DynModel dynModel : list) {
            //if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
            for (String key : MainApplication.getInstance().getApiProviderMap().keySet()) {
                if (key.equals(dynModel.getApiCode())) {
                    filterLst.add(dynModel);
                    break;
                }

            }
            // }
        }
    }

    /**
     * 过滤支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void filterList(List<DynModel> list, List<DynModel> filterLst, Map<String, String> payTypeMap) {

        if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getUserInfo().serviceType)) {
            String[] arrPays = MainApplication.getInstance().getUserInfo().serviceType.split("\\|");

            for (DynModel dynModel : list) {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                    for (String key : MainApplication.getInstance().getApiProviderMap().keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if (dynModel.getNativeTradeType().contains(",")) {
                                    String[] arrNativeTradeType = dynModel.getNativeTradeType().split(",");
                                    for (int j = 0; j < arrNativeTradeType.length; j++) {
                                        if (arrNativeTradeType[j].equals(arrPays[i])) {
                                         /*   filterLst.add(dynModel);
                                            payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);*/
                                            //如果登录接口返回的dynModel里面的nativeTradeType里有多种支付类型，但是如果是已开通的支付方式里
                                            //有两条一模一样的数据就会有问题，就会匹配到两条数据，看是app做去重还是server做去重。
                                            //如果后台没办法做到去重的逻辑，需要前端APP做的话，就用下面的一段代码
                                            if (payTypeMap.get(dynModel.getApiCode()) != null &&
                                                    payTypeMap.get(dynModel.getApiCode()).equals(arrPays[i])) {
                                            } else {
                                                filterLst.add(dynModel);
                                                payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);
                                            }
                                            break;
                                        }
                                    }
                                } else if (arrPays[i].equals(dynModel.getNativeTradeType())) {

                                    //这个地方的逻辑是有点问题，以后有空再改
                                    //如果登录接口返回的dynModel里面的nativeTradeType里只有一种支付类型，但是如果是已开通的支付方式里
                                    //有两条一模一样的数据就会有问题，只匹配到一条数据，然后就直接break跳出循环了，这个时候就做了去重处理，只展示一条
                                    filterLst.add(dynModel);
                                    payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    break;
                                }
                                /*if (arrPays[i].startsWith(dynModel.getNativeTradeType()) || dynModel.getNativeTradeType().contains(arrPays[i]) || arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    filterLst.add(dynModel);
                                    if (dynModel.getNativeTradeType().contains(",")) {
                                        payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType().substring(0, dynModel.getNativeTradeType().lastIndexOf(",")));
                                    } else {
                                        payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    }
                                    break;
                                }*/
                            }

                        }
                    }

                } else {
                    continue;
                }
            }
        }

    }

    /**
     * 过滤支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void filterList_active(List<DynModel> list, List<DynModel> filterLst, Map<String, String> payTypeMap) {

        if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getUserInfo().activateServiceType)) {
            String[] arrPays = MainApplication.getInstance().getUserInfo().activateServiceType.split("\\|");

            for (DynModel dynModel : list) {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                    for (String key : MainApplication.getInstance().getApiProviderMap().keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if (dynModel.getNativeTradeType().contains(",")) {
                                    String[] arrNativeTradeType = dynModel.getNativeTradeType().split(",");
                                    for (int j = 0; j < arrNativeTradeType.length; j++) {
                                        if (arrNativeTradeType[j].equals(arrPays[i])) {
                                         /*   filterLst.add(dynModel);
                                            payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);*/

                                            //如果登录接口返回的dynModel里面的nativeTradeType里有多种支付类型，但是如果是已开通的支付方式里
                                            //有两条一模一样的数据就会有问题，就会匹配到两条数据，看是app做去重还是server做去重。
                                            //如果后台没办法做到去重的逻辑，需要前端APP做的话，就用下面的一段代码
                                            if (payTypeMap.get(dynModel.getApiCode()) != null &&
                                                    payTypeMap.get(dynModel.getApiCode()).equals(arrPays[i])) {
                                            } else {
                                                filterLst.add(dynModel);
                                                payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);
                                            }
                                            break;
                                        }
                                    }
                                } else if (arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    //这个地方的逻辑是有点问题，以后有空再改
                                    //如果登录接口返回的dynModel里面的nativeTradeType里只有一种支付类型，但是如果是已开通的支付方式里
                                    //有两条一模一样的数据就会有问题，只匹配到一条数据，然后就直接break跳出循环了，这个时候就做了去重处理，只展示一条
                                    filterLst.add(dynModel);
                                    payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    break;
                                }
                                /*if (arrPays[i].startsWith(dynModel.getNativeTradeType()) || dynModel.getNativeTradeType().contains(arrPays[i]) || arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    filterLst.add(dynModel);
                                    if (dynModel.getNativeTradeType().contains(",")) {
                                        payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType().substring(0, dynModel.getNativeTradeType().lastIndexOf(",")));
                                    } else {
                                        payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    }
                                    break;
                                }*/
                            }

                        }
                    }

                } else {
                    continue;
                }
            }
        }

    }

    //过滤卡通道类型
    public void filterCardPaymentList(List<DynModel> modelList, List<DynModel> mFilterCardPaymentList,
                                      Map<String, String> mCardPaymentPayTypeMap) {
        //全部的卡交易通道
        if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getUserInfo().cardServiceType)) {
            String[] arrPays = MainApplication.getInstance().getUserInfo().cardServiceType.split("\\|");
            for (DynModel dynModel : modelList) {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                    for (String key : MainApplication.getInstance().getApiProviderMap().keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if (dynModel.getNativeTradeType().contains(",")) {
                                    String[] arrNativeTradeType = dynModel.getNativeTradeType().split(",");
                                    for (int j = 0; j < arrNativeTradeType.length; j++) {
                                        if (arrNativeTradeType[j].equals(arrPays[i])) {
                                         /*   filterLst.add(dynModel);
                                            payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);*/

                                            //如果登录接口返回的dynModel里面的nativeTradeType里有多种支付类型，但是如果是已开通的支付方式里
                                            //有两条一模一样的数据就会有问题，就会匹配到两条数据，看是app做去重还是server做去重。
                                            //如果后台没办法做到去重的逻辑，需要前端APP做的话，就用下面的一段代码
                                            if (mCardPaymentPayTypeMap.get(dynModel.getApiCode()) != null &&
                                                    mCardPaymentPayTypeMap.get(dynModel.getApiCode()).equals(arrPays[i])) {
                                            } else {
                                                mFilterCardPaymentList.add(dynModel);
                                                mCardPaymentPayTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);
                                            }
                                            break;
                                        }
                                    }
                                } else if (arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    //这个地方的逻辑是有点问题，以后有空再改
                                    //如果登录接口返回的dynModel里面的nativeTradeType里只有一种支付类型，但是如果是已开通的支付方式里
                                    //有两条一模一样的数据就会有问题，只匹配到一条数据，然后就直接break跳出循环了，这个时候就做了去重处理，只展示一条
                                    mFilterCardPaymentList.add(dynModel);
                                    mCardPaymentPayTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    break;
                                }
                            }
                        }
                    }

                } else {
                    continue;
                }
            }
        }
    }

    //过滤已开通的卡通道类型
    public void filterActiveCardPaymentList(List<DynModel> modelList, List<DynModel> mFilterCardPaymentList_active, Map<String, String> mCardPaymentPayTypeMap_active) {
        //全部已开通的卡交易通道
        if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getUserInfo().activateCardServiceType)) {
            String[] arrPays = MainApplication.getInstance().getUserInfo().activateCardServiceType.split("\\|");
            for (DynModel dynModel : modelList) {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                    for (String key : MainApplication.getInstance().getApiProviderMap().keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if (dynModel.getNativeTradeType().contains(",")) {
                                    String[] arrNativeTradeType = dynModel.getNativeTradeType().split(",");
                                    for (int j = 0; j < arrNativeTradeType.length; j++) {
                                        if (arrNativeTradeType[j].equals(arrPays[i])) {
                                         /*   filterLst.add(dynModel);
                                            payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);*/

                                            //如果登录接口返回的dynModel里面的nativeTradeType里有多种支付类型，但是如果是已开通的支付方式里
                                            //有两条一模一样的数据就会有问题，就会匹配到两条数据，看是app做去重还是server做去重。
                                            //如果后台没办法做到去重的逻辑，需要前端APP做的话，就用下面的一段代码
                                            if (mCardPaymentPayTypeMap_active.get(dynModel.getApiCode()) != null &&
                                                    mCardPaymentPayTypeMap_active.get(dynModel.getApiCode()).equals(arrPays[i])) {
                                            } else {
                                                mFilterCardPaymentList_active.add(dynModel);
                                                mCardPaymentPayTypeMap_active.put(dynModel.getApiCode(), arrNativeTradeType[j]);
                                            }
                                            break;
                                        }
                                    }
                                } else if (arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    //这个地方的逻辑是有点问题，以后有空再改
                                    //如果登录接口返回的dynModel里面的nativeTradeType里只有一种支付类型，但是如果是已开通的支付方式里
                                    //有两条一模一样的数据就会有问题，只匹配到一条数据，然后就直接break跳出循环了，这个时候就做了去重处理，只展示一条
                                    mFilterCardPaymentList_active.add(dynModel);
                                    mCardPaymentPayTypeMap_active.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    break;
                                }
                            }
                        }
                    }

                } else {
                    continue;
                }
            }
        }
    }

    /**
     * 过滤预授权的支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void filterList_pre_auth(List<DynModel> list, List<DynModel> filterLst_pre_auth, Map<String, String> payTypeMap_pre_auth) {

        if (!StringUtil.isEmptyOrNull(Constant.NATIVE_ALIPAY_AUTH)) {
            String[] arrPays = Constant.NATIVE_ALIPAY_AUTH.split("\\|");

            for (DynModel dynModel : list) {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                    for (String key : MainApplication.getInstance().getApiProviderMap().keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if (dynModel.getNativeTradeType().contains(",")) {
                                    String[] arrNativeTradeType = dynModel.getNativeTradeType().split(",");
                                    for (int j = 0; j < arrNativeTradeType.length; j++) {
                                        if (arrNativeTradeType[j].equals(arrPays[i])) {
                                            filterLst_pre_auth.add(dynModel);
                                            payTypeMap_pre_auth.put(dynModel.getApiCode(), arrNativeTradeType[j]);
                                            break;
                                        }
                                    }
                                } else if (arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    filterLst_pre_auth.add(dynModel);
                                    payTypeMap_pre_auth.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    break;
                                }
                            }
                        }
                    }

                } else {
                    continue;
                }
            }
        }

    }

    /**
     * 过滤固定二维码支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void filterStacticList(List<DynModel> list, List<DynModel> filterLst) {

        if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getUserInfo().serviceType)) {
            String[] arrPays = MainApplication.getInstance().getUserInfo().serviceType.split("\\|");

            for (DynModel dynModel : list) {
                if (!StringUtil.isEmptyOrNull(dynModel.getFixedCodeTradeType())) {
                    for (String key : MainApplication.getInstance().getApiProviderMap().keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if (dynModel.getFixedCodeTradeType().contains(",")) {
                                    String[] arrFixedCodeTradeType = dynModel.getFixedCodeTradeType().split(",");
                                    for (int j = 0; j < arrFixedCodeTradeType.length; j++) {
                                        if (arrFixedCodeTradeType[j].equals(arrPays[i])) {
                                            filterLst.add(dynModel);
                                            break;
                                        }
                                    }
                                } else if (arrPays[i].equals(dynModel.getFixedCodeTradeType())) {
                                    filterLst.add(dynModel);
                                    break;
                                }

                               /* if (arrPays[i].startsWith(dynModel.getFixedCodeTradeType()) || dynModel.getFixedCodeTradeType().contains(arrPays[i]) || arrPays[i].equals(dynModel.getFixedCodeTradeType())) {
                                    filterLst.add(dynModel);
                                    break;
                                }*/
                            }
                        }
                    }

                } else {
                    continue;
                }
            }
        }

    }

    @Override
    public void apiShowListSuccess(ArrayList<DynModel> response) {
        if (null != response && response.size() > 0) {

            PreferenceUtil.removeKey("payTypeMd5" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());

            PreferenceUtil.commitString("payTypeMd5" + BuildConfig.bankCode + MainApplication.getInstance().getMchId(), response.get(0).getMd5());
            //显示所有的支付方式
            List<DynModel> allLst = new ArrayList<>(response);
            SharedPreUtils.saveObject(allLst, "dynallPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());

            //过滤支持类型
            List<DynModel> filterLst = new ArrayList<DynModel>();
            Map<String, String> payTypeMap = new HashMap<String, String>();
            filterList(response, filterLst, payTypeMap);
            SharedPreUtils.saveObject(filterLst, "dynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());


            //过滤已开通的支持类型
            List<DynModel> filterList_active = new ArrayList<DynModel>();
            Map<String, String> payTypeMap_active = new HashMap<String, String>();
            filterList_active(response, filterList_active, payTypeMap_active);
            SharedPreUtils.saveObject(filterList_active, "ActiveDynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());

            //读取上面过滤出来的，已开通的支付类型，如果已开通的支付类型里，有QR交易通道，则把参数置成true-----V3.1.0版本新增
            if (filterList_active != null && filterList_active.size() > 0) {
                MainApplication.getInstance().setHasQRPayment(true);
            } else {
                MainApplication.getInstance().setHasQRPayment(false);
            }

            //过滤卡通道的类型---V3.1.0版本新增
            List<DynModel> mFilterCardPaymentList = new ArrayList<DynModel>();
            Map<String, String> mCardPaymentPayTypeMap = new HashMap<String, String>();
            filterCardPaymentList(response, mFilterCardPaymentList, mCardPaymentPayTypeMap);
            SharedPreUtils.saveObject(mFilterCardPaymentList, "cardPayment_dynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());

            //过滤已开通的卡通道类型---V3.1.0版本新增
            List<DynModel> mFilterCardPaymentList_active = new ArrayList<DynModel>();
            Map<String, String> mCardPaymentPayTypeMap_active = new HashMap<String, String>();
            filterActiveCardPaymentList(response, mFilterCardPaymentList_active, mCardPaymentPayTypeMap_active);
            SharedPreUtils.saveObject(mFilterCardPaymentList_active, "cardPayment_ActiveDynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());

            //读取上面过滤出来的，已开通的支付类型，如果已开通的支付类型里，有卡交易通道，则把参数置成true-----V3.1.0版本新增
            if (mFilterCardPaymentList_active != null && mFilterCardPaymentList_active.size() > 0) {
                MainApplication.getInstance().setHasCardPayment(true);
            } else {
                MainApplication.getInstance().setHasCardPayment(false);
            }

            //过滤预授权支持类型
                   /* List<DynModel> filterLst_pre_auth = new ArrayList<DynModel>();
                    Map<String, String> payTypeMap_auth = new HashMap<String, String>();
                    filterList_pre_auth(model, filterLst_pre_auth, payTypeMap_auth);
                    SharedPreUtils.saveObject(filterLst_pre_auth, "dynPayType_pre_auth" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());*/

            //过滤预授权支持类型---写死在前端，写死成支付宝
            List<DynModel> filterLst_pre_auth = new ArrayList<DynModel>();
            Map<String, String> payTypeMap_auth = new HashMap<String, String>();
            DynModel dynModel = new DynModel();
            dynModel.setNativeTradeType(Constant.NATIVE_ALIPAY_AUTH);
            dynModel.setApiCode("2");
            filterLst_pre_auth.add(dynModel);
            payTypeMap_auth.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
            SharedPreUtils.saveObject(filterLst_pre_auth, "dynPayType_pre_auth" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());


            //流水过滤条件筛选----用来用来筛选流水的字段，V3.1.0改了，废弃此字段，不用了
            List<DynModel> filterLstStream = new ArrayList<DynModel>();
            filterListStream(response, filterLstStream);
            SharedPreUtils.saveObject(filterLstStream, "filterLstStream" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());

            //过滤固定二维码支付类型
            List<DynModel> filterStaticLst = new ArrayList<DynModel>();
            filterStacticList(response, filterStaticLst);
            SharedPreUtils.saveObject(filterStaticLst, "dynPayTypeStatic" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());

            Map<String, String> typePicMap = new HashMap<String, String>();
            // 颜色map集合
            Map<String, String> colorMap = new HashMap<String, String>();

            Map<String, DynModel> payTypeNameMap = new HashMap<String, DynModel>();

            for (DynModel d : response) {
                if (!StringUtil.isEmptyOrNull(d.getSmallIconUrl())) {
                    typePicMap.put(d.getApiCode(), d.getSmallIconUrl());
                }

                if (!StringUtil.isEmptyOrNull(d.getColor())) {
                    colorMap.put(d.getApiCode(), d.getColor());
                }

                if (!StringUtil.isEmptyOrNull(d.getProviderName())) {
                    payTypeNameMap.put(d.getApiCode(), d);
                }

            }
            SharedPreUtils.saveObject(payTypeNameMap, "payTypeNameMap" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            SharedPreUtils.saveObject(payTypeMap, "payTypeMap" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            //V3.1.0新增，卡通道
            SharedPreUtils.saveObject(mCardPaymentPayTypeMap, "mCardPaymentPayTypeMap" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            SharedPreUtils.saveObject(payTypeMap_auth, "payTypeMap_pre_auth" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            //图片
            SharedPreUtils.saveObject(typePicMap, "payTypeIcon" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            //颜色值
            SharedPreUtils.saveObject(colorMap, "payTypeColor" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());


            //读取上面过滤出来的，已开通的支付类型，如果已开通的支付类型里有小钱包，则去请求小钱包列表接口，否则不用请求
            if (filterList_active != null) {
                for (int i = 0; i < filterList_active.size(); i++) {
                    if (filterList_active.get(i).getNativeTradeType()
                            .equalsIgnoreCase(Constant.NATIVE_LIQUID)) {
                        //只要有一个liquid pay的支付类型，则去请求小钱包接口，跳出循环
                        getWalletList(Constant.NATIVE_LIQUID);
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void apiShowListFailed(Object error) {
        if (checkSession()) {
            return;
        }
    }

    /**
     * 动态加载支付类型
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void loadPayType() {
        if (mPresenter != null) {
            mPresenter.apiShowList();
        }
    }

    @Override
    public void queryWalletListSuccess(List<? extends WalletListBean> response) {

    }

    @Override
    public void queryWalletListFailed(Object error) {

    }

    public void getWalletList(String serviceType) {
        if (mPresenter != null) {
            mPresenter.queryWalletList(serviceType);
        }
    }

    @Override
    public void getVersionCodeSuccess(UpgradeInfo response) {
        if (null != response) {

            //                    PreferenceUtil.getInt("update", 0);

            //                    if (result.version > PreferenceUtil.getInt("update", 0))
            //                    {
            // "发现新版本"
            KotlinUtils.INSTANCE.saveCheckVersionFlag(response.mustUpgrade);
            showUpgradeInfoDialog(response, new ComDialogListener(response));
            //                    }
        }
    }

    @Override
    public void getVersionCodeFailed(Object error) {

    }

    public void checkVersion() {
        //不用太平凡的检测升级 所以使用时间间隔区分
        KotlinUtils.INSTANCE.saveCheckVersionTime();
        if (mPresenter != null) {
            mPresenter.getVersionCode();
        }
    }

    @Override
    public void getCertificateStringSuccess(CertificateBean response, boolean isDeviceLogin) {
        //最新配置的公钥信息
        cerFailedRequestAgain = false;

        MainApplication.getInstance().setCertificateBean(response);
        Logger.d("Welcome requestCertificate ECDHKeyExchange");
        if (isDeviceLogin) {//如果是设备登录请求的时候，先请求证书公钥,成功之后再走正常逻辑
            //先去调用公钥交换接口然后再调用设备登录接口,每次设备登录前都主动去调用ECDH去交换
            if (response != null) {
                isNeedDeviceLogin = true;
                ECDHKeyExchange(false);
            }
        } else {//如果是点登陆按钮去请求公钥证书
            //如果请求公钥证书成功，则重新调用登录接口
            if (response != null) {
                //则重新走一遍登录前的ECDH然后再走登录
                isNeedRequestECDHKey = true;
                ECDHKeyExchange(false);
            }
        }
    }

    /**
     * 下载apk
     */
    private void downApk() {
        new UpgradeDailog(WelcomeActivity.this, mApkDownInfo, new UpgradeDailog.UpdateListener() {
            @Override
            public void cancel() {

            }
        }).show();
    }

    /**
     * 申请apk安装权限
     */
    private void requestApkInstallPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            boolean haveInstallPermission = getPackageManager().canRequestPackageInstalls();
            if (haveInstallPermission) {
                //有apk安装权限
                downApk();
            } else {
                //没有apk安装权限, 需在设置页面手动开启
                KotlinUtils.INSTANCE.showMissingPermissionDialog(
                        getApplicationContext(),
                        getString(R.string.setting_permission_apk_install_lack),
                        getStringById(R.string.title_setting),
                        getString(R.string.btnCancel),
                        new OnOkClickListener() {
                            @Override
                            public void onOkClick() {
                                Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES);
                                startActivityForResult(intent, KotlinUtils.APP_INSTALL_REQUEST_PERMISSION);
                            }
                        }
                );
            }
        } else {
            //有apk安装权限
            downApk();
        }
    }

    /**
     * 申请存储权限
     */
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_DENIED
                ||
                ContextCompat.checkSelfPermission(
                        this,
                        READ_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_DENIED
        ) {
            //没有存储权限
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    WRITE_EXTERNAL_STORAGE
            ) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(
                            this,
                            READ_EXTERNAL_STORAGE
                    )
            ) {
                //之前拒绝过，用户需要手动去设置里开启存储权限
                KotlinUtils.INSTANCE.showMissingPermissionDialog(
                        getApplicationContext(),
                        getString(R.string.setting_permission_storage_lack),
                        getStringById(R.string.title_setting),
                        getString(R.string.btnCancel),
                        new OnOkClickListener() {
                            @Override
                            public void onOkClick() {
                                startAppSettings();
                            }
                        }
                );

            } else {
                //首次申请
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE},
                        KotlinUtils.EXTERNAL_STORAGE_REQUEST_PERMISSION
                );
            }

        } else {
            //有存储权限
            requestApkInstallPermission();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case KotlinUtils.EXTERNAL_STORAGE_REQUEST_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    //拒绝
                } else {
                    //允许
                    keepAlive();
                    requestApkInstallPermission();
                }
                break;
        }
    }

    @Override
    public void getCertificateStringFailed(Object error, boolean isDeviceLogin) {
        //如果返回失败或者网络异常时，则直接跳转到一键重登界面
        if (isDeviceLogin) {//如果是设备登录请求的时候，先请求证书公钥
            //获取公钥证书失败，则直接跳转到一键重登界面
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    //则跳转到一键重登页面
                    Intent it = new Intent();
                    it.putExtra("isCerFailed", true);
                    it.setClass(WelcomeActivity.this, ReLoginActivity.class);
                    startActivity(it);
                }
            });

        } else {//如果是点登陆按钮去请求公钥证书
            //获取公钥证书失败，则弹框提示
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    showLoginPage();
                    MainApplication.getInstance().setNeedLogin(false);
                    btn_login.setEnabled(true);
                    btn_login.setText(R.string.login);

                    toastDialog(WelcomeActivity.this, R.string.certificate_request_failed, null);
                }
            });

        }
    }

    /*
     **如果证书已过期或者证书校验失败
     * 请求CA证书公钥获取接口得到最新CA证书公钥
     * */
    public void requestCertificate(boolean isDeviceLogin) {
        if (mPresenter != null) {
            mPresenter.getCertificateString(isDeviceLogin);
        }
    }

    @Override
    protected WelcomeContract.Presenter createPresenter() {
        return new WelcomePresenter();
    }

    class KeyBoardStateListener implements KeyboardUtil.KeyBoardStateChangeListener {

        @Override
        public void KeyBoardStateChange(int state, EditText editText) {

        }
    }

    class inputOverListener implements KeyboardUtil.InputFinishListener {

        @Override
        public void inputHasOver(int onclickType, EditText editText) {

        }
    }

    class ComDialogListener implements CommonConfirmDialog.ConfirmListener {
        private UpgradeInfo result;

        public ComDialogListener(UpgradeInfo result) {
            this.result = result;
            mApkDownInfo = result;
        }

        @Override
        public void ok() {
            requestStoragePermission();
        }

        @Override
        public void cancel() {
        }

    }

}
