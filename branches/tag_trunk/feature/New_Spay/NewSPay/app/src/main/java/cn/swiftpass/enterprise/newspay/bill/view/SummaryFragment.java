package cn.swiftpass.enterprise.newspay.bill.view;


import butterknife.BindView;
import cn.swiftpass.enterprise.R;
import cn.swiftpass.enterprise.base.BaseFragment;
import cn.swiftpass.enterprise.widget.Toolbar;


public class SummaryFragment extends BaseFragment {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    protected int getContentLayout() {
        return R.layout.fragment_summary;
    }

    @Override
    protected void initView() {
        mToolbar.setCenterTitle(getString(R.string.tv_report_total), 0);
        mToolbar.setRightTitle(getString(R.string.print),0);
    }
}
