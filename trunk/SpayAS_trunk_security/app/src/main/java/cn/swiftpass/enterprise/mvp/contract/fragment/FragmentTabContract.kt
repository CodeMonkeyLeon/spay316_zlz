package cn.swiftpass.enterprise.mvp.contract.fragment

import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/12/7
 *
 * @有客自远方来
 * @眉间有远方的风雨
 */
class FragmentTabContract {


    interface View : BaseView {


        fun getVersionCodeSuccess(response: UpgradeInfo?)

        fun getVersionCodeFailed(error: Any?)

        fun getExchangeRateSuccess(response: Boolean)

        fun getExchangeRateFailed(error: Any?)

    }

    interface Presenter : BasePresenter<View> {

        fun getExchangeRate()


        fun getVersionCode()
    }


}