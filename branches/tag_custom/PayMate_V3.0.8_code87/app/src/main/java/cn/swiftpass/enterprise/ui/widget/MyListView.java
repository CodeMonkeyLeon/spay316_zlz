package cn.swiftpass.enterprise.ui.widget;

import android.content.Context;
import android.widget.ListView;

/**
 * Created by aijingya on 2020/5/22.
 *
 * @Package cn.swiftpass.enterprise.ui.widget.Zxing
 * @Description:
 * @date 2020/5/22.18:09.
 */
public class MyListView extends ListView {

    public MyListView(Context context) {
        super(context);
    }
}
