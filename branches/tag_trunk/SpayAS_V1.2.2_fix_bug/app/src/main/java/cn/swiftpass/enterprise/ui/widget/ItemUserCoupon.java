package cn.swiftpass.enterprise.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.CouponUseModel;
import cn.swiftpass.enterprise.ui.widget.ImageCache.ImageLoader;
import cn.swiftpass.enterprise.ui.widget.ImageCache.ImageLoader.ImageCallback;
import cn.swiftpass.enterprise.utils.Logger;

/**
 * 适配 User: W.l Date: 14-03-18 Time: 下午13:49 To change this template use File |
 * Settings | File Templates.
 */
public class ItemUserCoupon extends LinearLayout
{
    private TextView tvUserName, tvUserPhone;
    
    public ImageView ivUserUogo;
    
    public ItemUserCoupon(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
    
    public ItemUserCoupon(Context context, View view)
    {
        super(context);
        tvUserName = (TextView)view.findViewById(R.id.tv_user_Name);
        ivUserUogo = (ImageView)view.findViewById(R.id.iv_user_logo);
        tvUserPhone = (TextView)view.findViewById(R.id.tv_user_phone);
        
    }
    
    @Override
    protected void onFinishInflate()
    {
        // initViews(v);
    }
    
    public void setData(CouponUseModel couponUseModel, final ListView mListView, ImageLoader mImageLoader)
    {
        try
        {
            if (null != couponUseModel.getHead())
            {
                Bitmap bitmap = mImageLoader.loadImage(couponUseModel.getHead(), 1,
                // 回调对象
                    new ImageCallback()
                    {
                        @Override
                        public void loadedImage(String path, Bitmap bitmap)
                        {
                            // 根据回传的路径，查找listview中的imageview
                            ImageView iv = (ImageView)mListView.findViewWithTag(path);
                            if (iv != null && bitmap != null)
                            {
                                iv.setImageBitmap(bitmap);
                            }
                            else
                            {
                                if (iv != null)
                                {
                                    iv.setImageResource(R.drawable.n_icon_portrait);
                                }
                            }
                        }
                    },
                    2);
                if (bitmap != null)
                {
                    ivUserUogo.setImageBitmap(bitmap);
                }
                else
                {
                    ivUserUogo.setImageResource(R.drawable.n_icon_portrait);
                }
            }
            else
            {
                ivUserUogo.setImageResource(R.drawable.n_icon_portrait);
            }
            if (!TextUtils.isEmpty(couponUseModel.getNickname()))
            {
                tvUserName.setText("昵称 " + couponUseModel.getNickname() + "  (" + couponUseModel.getCollerCount()
                    + ")次");
            }
            else
            {
                tvUserName.setText("用户昵称未公开(" + couponUseModel.getCollerCount() + ")次");
            }
            tvUserPhone.setText("手机号 " + couponUseModel.getCollerPhone());
        }
        catch (Exception e)
        {
            Logger.i(e);
        }
    }
    
}
