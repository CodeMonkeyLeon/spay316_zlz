package cn.swiftpass.enterprise.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.stmt.query.In;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.bill.BillOrderManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.adapter.PreAuthThawAdapter;
import cn.swiftpass.enterprise.ui.widget.TitleBar;

/**
 * 创建人：caoxiaoya
 * 时间：2018/12/24
 * 描述：解冻列表
 * 备注：
 */
public class PreAuthThawActivity extends TemplateActivity {

    private PreAuthThawAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private int itemType;//标识哪个   已支出/解冻传   过来的
    private Order mOrder;
    private  TextView nullText;
    private SmartRefreshLayout mRefreshLayout;
    private List<Order> mOrderList=new ArrayList<>();
    private boolean isRefresh;
    private boolean isLoadMore;
    private int page=1;//默认加载一页
    public static final int REQUEST_REFRESH = 1;


    public static void startActivity(Context mContext, Order order,List<Order> orderList,int itemType) {
        Intent it = new Intent();
        it.setClass(mContext, PreAuthThawActivity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT); //W.l 解决弹出多个activity
        it.putExtra("itemType",itemType);
        it.putExtra("order",order);
        it.putExtra("orderList", (Serializable)orderList);
        mContext.startActivity(it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_auth_thaw);
        initView();
        initData();
    }
    private void initView() {
        //loadDialog(this,R.string.loading);
        mRefreshLayout=(SmartRefreshLayout)findViewById(R.id.refresh_layout);
        nullText=(TextView)findViewById(R.id.tv_error_message);
        mRecyclerView=(RecyclerView)findViewById(R.id.rv_pre_auth);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        //设置 Header
        mRefreshLayout.setRefreshHeader(new ClassicsHeader(this));
        //设置 Footer
        mRefreshLayout.setRefreshFooter(new ClassicsFooter(this).setSpinnerStyle(SpinnerStyle.Scale));
    }
    private void initData() {
        itemType = getIntent().getIntExtra("itemType", 0);
        mOrder = (Order) getIntent().getSerializableExtra("order");
        mOrderList = (List<Order>) getIntent().getSerializableExtra("orderList");
        if (mOrder!=null&&mOrderList != null&&mOrderList.size()>0) {//操作类型：1已支付金额 2已解冻金额
            switch (itemType) {
                case 1://已支付金额
                    mAdapter = new PreAuthThawAdapter(PreAuthThawActivity.this, mOrderList, 1);
                    mRecyclerView.setAdapter(mAdapter);
                    mAdapter.setData(mOrderList);
                    mAdapter.setOnItemClickListener(new PreAuthThawAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position,Order order) {
                            loadDialog(PreAuthThawActivity.this,R.string.loading);
                            queryOrderDetail( mOrderList.get(position).getOrderNo());
                        }
                    });
                    break;
                case 2://已解冻金额
                    mAdapter = new PreAuthThawAdapter(PreAuthThawActivity.this, mOrderList, 2);
                    mRecyclerView.setAdapter(mAdapter);
                    mAdapter.setData(mOrderList);
                    mAdapter.setOnItemClickListener(new PreAuthThawAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position,Order order) {//mOrder.getTradeName()
                            order.setTradeName(mOrder.getTradeName());
                            Intent intent=new Intent(PreAuthThawActivity.this,PreAuthDetailsActivity.class);
                            intent.putExtra("itemType",itemType);
                            intent.putExtra("order", order);
                            intent.putExtra("isUnfreezeOrPay",itemType);
                            startActivityForResult(intent,REQUEST_REFRESH);
                        }
                    });
                    //PreAuthDetailsActivity.startActivity(mContext, mList.get(position), 2,2);//跳预授权转支付详情
                    break;
                default:
                    break;
            }
            //刷新
            mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(RefreshLayout refreshlayout) {
                    isRefresh=true;
                    page=1;
                    loadData(itemType,page);
                }
            });

            //加载更多
            mRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                    isLoadMore=true;
                    page ++;
                    loadData(itemType,page);
                }
            });


        }else {
            dismissLoading();
            showErrorText(true);
        }
    }

    private void  queryOrderDetail(String authno){
        OrderManager.getInstance().queryOrderDetail(authno, MainApplication.getMchId(), false, new UINotifyListener<Order>() {

            @Override
            public void onError(Object object) {
                super.onError(object);
                dismissLoading();
            }

            @Override
            public void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            public void onSucceed(Order result) {
                super.onSucceed(result);
                dismissLoading();
                if (result != null) {
                    Intent intent=new Intent(PreAuthThawActivity.this,OrderDetailsActivity.class);
                    intent.putExtra("order", result);
                    startActivityForResult(intent,REQUEST_REFRESH);
                }
            }

        });
    }

    private void loadData(int operationType,int page){
        BillOrderManager.getInstance().authOperateQuery(String.valueOf(operationType), mOrder.getAuthNo(),
                null, null, page, 10, null, null,
                new UINotifyListener<List<Order>>() {
                    @Override
                    public void onError(Object object) {
                        super.onError(object);
                        if (PreAuthThawActivity.this.page>1) {
                            PreAuthThawActivity.this.page--;
                        }
                        if(isRefresh){//错误的时候是触发isRefresh，所以要重置
                            isRefresh=false;
                            mRefreshLayout.finishRefresh(200);
                        }
                        if (isLoadMore){
                            isLoadMore=false;
                            mRefreshLayout.finishLoadMore(200);
                        }

                        dismissLoading();
                    }

                    @Override
                    public void onSucceed(List<Order> result) {
                        super.onSucceed(result);
                        dismissLoading();
                        //无论是刷新还是加载，都会触发isRefresh、isLoadMore其中一个
                        if (result!=null&&isRefresh){//刷新
                            if (result.size()>0){//如果size大于0的时候就刷新添加数据
                                mAdapter.refresh(result);
                                mRefreshLayout.finishRefresh(500);//设置刷新0.5秒
                                showErrorText(false);
                            }else {
                                mRefreshLayout.finishLoadMore();//立即停止刷新
                                showErrorText(true);//否则就加载错误界面
                            }
                            isRefresh=false;//重置刷新标记
                        }else if (result!=null&&isLoadMore){//加载更多
                            if (result.size()>0){
                                mAdapter.addData(result);
                                mRefreshLayout.finishLoadMore(500);
                            }else {
                                mRefreshLayout.finishLoadMore();//立即停止加载更多
                            }
                            isLoadMore=false;//重置加载更多标记
                        }
                    }
                });
    }

    /**
     * 用于无数据或者无网络展示
     */
    private void showErrorText(boolean isShowError){
        if (isShowError){//展示错误页面
            nullText.setGravity(Gravity.CENTER);
            nullText.setText(getString(R.string.tv_bill));
            mRefreshLayout.setVisibility(View.GONE);
            nullText.setVisibility(View.VISIBLE);
        }else {
            nullText.setGravity(Gravity.CENTER);
            nullText.setText(getString(R.string.tv_bill));
            mRefreshLayout.setVisibility(View.VISIBLE);
            nullText.setVisibility(View.GONE);
        }

    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setBackgroundColor(getResources().getColor(R.color.bg_change_pre_auth_color));
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.pre_auth_details);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }

            @Override
            public void onRightButtonClick() { }

            @Override
            public void onRightLayClick() {
            }

            @Override
            public void onRightButLayClick() {
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_REFRESH && resultCode ==REQUEST_REFRESH) {
            int result = data.getIntExtra("isRefreshList",0);
            if (result==1){
                isRefresh=true;
                page=1;
                loadData(itemType,page);
            }
        }

    }
}
