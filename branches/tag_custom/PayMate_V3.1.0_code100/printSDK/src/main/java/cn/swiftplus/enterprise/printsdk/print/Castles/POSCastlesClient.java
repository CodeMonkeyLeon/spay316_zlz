package cn.swiftplus.enterprise.printsdk.print.Castles;

import android.content.Context;

/**
 * Created by aijingya on 2020/10/30.
 *
 * @Package cn.swiftplus.enterprise.printsdk.print.Castles
 * @Description:
 * @date 2020/10/30.14:50.
 */
public class POSCastlesClient {
    private static POSCastlesClient sPosCastlesClient;
    private Context mContext;

    public static void initCastles(Context context){
        if(sPosCastlesClient != null){
            throw new RuntimeException("PosCastlesClient Already initialized");
        }
        sPosCastlesClient = new POSCastlesClient(context);
    }

    public static POSCastlesClient getInstance(){
        if (sPosCastlesClient == null) {
            throw new RuntimeException("PosCastlesClient is not initialized");
        }
        return sPosCastlesClient;
    }

    private POSCastlesClient(Context context){
        this.mContext = context;
        //Castles pos 打印机初始化

    }


}
