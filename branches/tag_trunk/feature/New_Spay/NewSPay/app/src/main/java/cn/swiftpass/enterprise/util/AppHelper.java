package cn.swiftpass.enterprise.util;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Environment;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;


import cn.swiftpass.enterprise.R;
import cn.swiftpass.enterprise.SpayApplication;


@SuppressLint("NewApi")
public class AppHelper {
    private static final String TAG = "AppHelper";

    public static boolean isSdcardExist() {
        return TextUtils.equals(Environment.getExternalStorageState(), Environment.MEDIA_MOUNTED);
    }

//    @Override
//    public boolean equals(Object obj) {
//        return super.equals(obj);
//    }

    /**
     * 如果SDcard存在，则返回SDcard上文件的目录 否则，抛异常
     */
    public static String getCacheDir() {
        String path = null;
        try {
            if (!isSdcardExist()) {
                throw new Exception("SD卡不存在");
            }
            path = Environment.getExternalStorageDirectory().getAbsolutePath();
            path += "/swiftpass_pay/";
            File dir = new File(path);
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    throw new Exception("创建文件缓存目录失败");
                }
            }
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return path;
    }

    public static String getImgCacheDir() {
        return FileUtils.getAppCache();
    }

    /**
     * 创建缓存目录
     * 如果SDcard存在，则返回SDcard上文件的目录 否则，抛异常
     */
    public static String getAppCacheDir() {
        String path = null;
        try {
            if (!isSdcardExist()) {
                throw new Exception("SD卡不存在");
            }
            File root = new File(FileUtils.getRootPath());
            if (!root.exists()) {
                if (!root.mkdirs()) {
                    throw new Exception("创建文件缓存目录失败");
                }
            }
            path = FileUtils.getAppCache();
            File dir = new File(path);
            if (!dir.exists()) {
                if (!dir.mkdirs()) {
                    throw new Exception("创建文件缓存目录失败");
                }
            }
            String logPath = FileUtils.getAppLog();
            File logDir = new File(logPath);
            if (!logDir.exists()) {
                if (!logDir.mkdirs()) {
                    throw new Exception("创建log文件缓存目录失败");
                }
            }
            String appPath = FileUtils.getAppPath();
            File f = new File(appPath);
            if (!f.exists()) {
                if (!f.mkdirs()) {
                    throw new Exception("创建APP文件缓存目录失败");
                }
            }
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return path;
    }

    /**
     * 获取数据库的存放路径，策略为 1.有卡，则主DB存放在应用程序的目录下
     * ，辅DB防止卡的目录下。 2.无卡，则主辅DB都放在应用程序目录下。
     *
     * @param mainAble 是否有卡
     * @return
     * @throws Exception
     */
    public static String getDBPath(boolean mainAble, String dbName) {
        //false)// //不允许 读取数据库文件
        if (isSdcardExist()) {
            // 有卡
            if (mainAble) {
                return SpayApplication.getAppContext().getDatabasePath(dbName).getAbsolutePath();
            } else {
                String path = getCacheDir();
                if (!TextUtils.isEmpty(path)) {
                    path = path + "databases/";
                    File dir = new File(path);
                    if (!dir.exists()) {
                        if (!dir.mkdirs()) {
                            return null;
                        }
                    }
                    return path + dbName;
                } else {
                    return null;
                }
            }
        } else {
            // 无卡
            return SpayApplication.getAppContext().getDatabasePath(dbName).getAbsolutePath();
        }
    }


    public static int getResouceIdByName(Context context, String type, String name) {
        return context.getResources().getIdentifier(name, type, context.getPackageName());
    }


    /**
     * 版本
     *
     * @param context
     * @return
     */
    public static int getVerCode(Context context) {
        int verCode = -1;
        try {
            verCode = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (NameNotFoundException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return verCode;
    }

    public static String getVerName(Context context) {
        String verName = "";
        try {
            verName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return verName;
    }

    public static int getAndroidSDKVersion() {
        return android.os.Build.VERSION.SDK_INT;
    }

    public static String getAndroidSDKVersionName() {
        return android.os.Build.VERSION.RELEASE;
    }

    /**
     * 程序名称
     *
     * @param context
     * @return
     */
    public static String getAppName(Context context) {
        String verName = context.getResources().getText(R.string.app_name).toString();
        return verName;
    }

    public static String getAppPackageName(Context context) {
        String pName = "";
        try {
            pName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).packageName;
        } catch (NameNotFoundException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return pName;
    }

    public static String getImei(Context context) {
        try {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            String imei = tm.getDeviceId();
            if (imei == null) {
                imei = "";
            }
            return imei;
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return "";
    }

    /**
     * 手机品牌跟
     */
    public static String[] getBrand() {
        String brand = android.os.Build.BRAND;
        String mType = android.os.Build.MODEL;
        return new String[]{brand, mType};

    }

    // ////////////////////////////////////////////////////////////////////
    //private static final String[] PHONES_PROJECTION = new String[]{Phone._ID, Phone.DISPLAY_NAME, Phone.NUMBER, Phone.CONTACT_ID, Photo.TIMES_CONTACTED, Phone.LAST_TIME_CONTACTED};

    /**
     * 实现文本复制功能
     *
     * @param text
     */
    public static void copy(String text, Context context) {
        // 得到剪贴板管理器
        ClipboardManager cmb = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        cmb.setText(text);
    }

    /**
     * 实现粘贴功能
     *
     * @param context
     * @return
     */
    @SuppressWarnings("deprecation")
    public static String paste(Context context) {
        // 得到剪贴板管理器
        ClipboardManager cmb = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        return cmb.getText().toString().trim();
    }

    /**
     * 震动
     *
     * @param context
     */
    public static void execVibrator(Context context) {
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = {0, 10, 20, 30}; // 停止 开启 停止 开启
        vibrator.vibrate(pattern, -1); //重复两次上面的pattern 如果只想震动一次，index设为
    }



    public static String getIMSI() {
        try {
            TelephonyManager tm = (TelephonyManager) SpayApplication.getAppContext().getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(SpayApplication.getAppContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return tm.getSubscriberId();
            }

        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return"";
    }
}
