package cn.swiftpass.enterprise.ui.activity;

import static cn.swiftpass.enterprise.MainApplication.getInstance;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.CREATE_ORDER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.MANAGE_CREATE_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.MANAGE_CREATE_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.MANAGE_EDIT_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.MANAGE_EDIT_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.SEARCH_TYPE_MANAGE_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.SEARCH_TYPE_MANAGE_ORDER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.SEARCH_TYPE_MANAGE_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.TAB_LIST_TYPE_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.TAB_LIST_TYPE_ORDER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.TAB_LIST_TYPE_PRODUCT;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.common.sentry.SentryUtils;
import com.example.common.sp.PreferenceUtil;
import com.example.common.utils.PngSetting;

import java.util.HashMap;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;
import cn.swiftpass.enterprise.ui.activity.bill.OrderSearchActivity;
import cn.swiftpass.enterprise.ui.activity.print.BluePrintUtil;
import cn.swiftpass.enterprise.ui.activity.scan.CaptureActivity;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabBill;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabPay;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabReport;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabSetting;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabSummary;
import cn.swiftpass.enterprise.ui.paymentlink.CustAndProdEditActivity;
import cn.swiftpass.enterprise.ui.paymentlink.DialogPaymentLinkSelect;
import cn.swiftpass.enterprise.ui.paymentlink.FragmentTabPaymentLink;
import cn.swiftpass.enterprise.ui.paymentlink.OrderCreateActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.MyRadioButton;
import cn.swiftpass.enterprise.ui.widget.MyToast;
import cn.swiftpass.enterprise.ui.widget.SpayTitleView;
import cn.swiftpass.enterprise.ui.widget.dialog.ChangePreauthDialog;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.KotlinUtils;
import cn.swiftpass.enterprise.utils.LocaleUtils;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PngUtils;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;
import cn.swiftpass.enterprise.utils.interfaces.OnActivityDataToFragmentListener;

/**
 * Created by aijingya on 2018/4/26.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(新的MainTab页面)
 * @date 2018/4/26.14:32.
 */

public class spayMainTabActivity extends BaseActivity {

    private static final String TAG = spayMainTabActivity.class.getSimpleName();
    SpayTitleView spay_title_view;
    boolean isOpen = true, isClose = true, isOpening = true, isCloseing = true;
    private Context mContext;
    private FrameLayout mContent_view;
    private RadioGroup mRadioGroup_bottom_view;
    private MyRadioButton mHomePay;
    private MyRadioButton mHomeBill;
    private MyRadioButton mPaymentLink;
    private MyRadioButton mHomeSummary;
    private MyRadioButton mHomeSetting;
    private Fragment FragmentPay;
    private Fragment FragmentBill;
    private Fragment FragmentSummary;
    private Fragment FragmentSetting;
    private Fragment FragmentReport;
    private Fragment FragmentPaymentLink;
    private ChangePreauthDialog changePreauthDialog;
    private ChangePreauthDialog changeBillPreauthDialog;
    private String saleOrPreauth;          //上一次选择的是sale 还是 pre_auth,默认是sale
    private String bill_sale_or_Pre_auth_or_Card_payment; //订单列表页面，选择是收银还是预授权订单,还是卡交易
    private int tab_index = 0;           //tab_index ,默认是第0个tab
    private int tabIds[] = new int[]{R.id.id_tab_pay, R.id.id_tab_bill, R.id.id_tab_payment_link, R.id.id_tab_summary, R.id.id_tab_setting};
    private DialogInfo dialogInfo;

    private HashMap<String, TabItemDrawableEntity> mTabDrawableMap = new HashMap<>();
    public static final String TAB_PAY = "TAB_PAY";
    public static final String TAB_BILL = "TAB_BILL";
    public static final String TAB_LINK = "TAB_LINK";
    public static final String TAB_REPORT = "TAB_REPORT";
    public static final String TAB_SUMMARY = "TAB_SUMMARY";
    public static final String TAB_SETTING = "TAB_SETTING";


    /**
     * 实时监听蓝牙广播
     */
    private final BroadcastReceiver blueReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
            switch (state) {
                case BluetoothAdapter.STATE_OFF:
                    MainApplication.getInstance().setBlueState(false);
                    if (isClose) {
                        isClose = false;
                    }
                    MainApplication.getInstance().setBluetoothSocket(null);
                    HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET_STUTS, HandlerManager.BLUE_CONNET_STUTS_CLOSED);
                    break;
                case BluetoothAdapter.STATE_TURNING_OFF:
                    MainApplication.getInstance().setBlueState(false);
                    MainApplication.getInstance().setBluetoothSocket(null);
                    if (isCloseing) {
                        isCloseing = false;

                    }
                    HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET_STUTS, HandlerManager.BLUE_CONNET_STUTS_CLOSED);
                    break;
                case BluetoothAdapter.STATE_ON:
                    MainApplication.getInstance().setBlueState(true);
                    if (isOpen) {
                        isOpen = false;
                    }
                    try {
                        connentBlue();
                    } catch (Exception e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(e, SentryUtils.INSTANCE.getClassNameAndMethodName());
                        Log.e(TAG, Log.getStackTraceString(e));
                    }
                    HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET_STUTS, HandlerManager.BLUE_CONNET_STUTS);
                    break;
                case BluetoothAdapter.STATE_TURNING_ON:
                    if (isOpening) {
                        isOpening = false;

                    }
                    break;
            }

        }

    };


    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    /**
     * 连接默认上一次
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void connentBlue() {
        boolean connState = PreferenceUtil.getBoolean("connState" + BuildConfig.bankCode, true);
        if (connState && !StringUtil.isEmptyOrNull(MainApplication.getInstance().getBlueDeviceAddress())) {
            //异步去连接蓝牙
            new Thread(new Runnable() {
                @Override
                public void run() {
                    boolean isConnect = BluePrintUtil.blueConnent(MainApplication.getInstance().getBlueDeviceAddress());
                    Logger.i(TAG, " connentBlue isConnect-->" + isConnect);
                    if (!isConnect) {
                        MainApplication.getInstance().setBlueState(false);
                        //                        MainApplication.bluetoothSocket = null;
                        //                        MainApplication.setBlueDeviceName("");
                        //                        MainApplication.setBlueDeviceNameAddress("");
                        //                        PreferenceUtil.commitBoolean("connState" + MainApplication.getMchId(), false);
                        HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET, HandlerManager.BLUE_CONNET);
                    } else {
                        MainApplication.getInstance().setBlueState(true);
                    }
                }
            }).start();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case KotlinUtils.EXTERNAL_STORAGE_REQUEST_PERMISSION_PAY:
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    //拒绝
                } else {
                    //允许
                    if (FragmentPay != null) {
                        ((OnActivityDataToFragmentListener) FragmentPay).onPermissionsResult(requestCode, permissions, grantResults);
                    }
                }
                break;
            case KotlinUtils.EXTERNAL_STORAGE_REQUEST_PERMISSION_SETTING:
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    //拒绝
                } else {
                    //允许
                    if (FragmentSetting != null) {
                        ((OnActivityDataToFragmentListener) FragmentSetting).onPermissionsResult(requestCode, permissions, grantResults);
                    }
                }
                break;
        }
    }

    /*private void initData() {
        mList.add(new DrawerContentModel(R.drawable.icon_fixedqrcode, getString(R.string.drawer_fixed_code), 1));
        mList.add(new DrawerContentModel(R.drawable.icon_management,  getString(R.string.drawer_managerment), 2));
        mList.add(new DrawerContentModel(R.drawable.icon_support,  getString(R.string.drawer_Support), 3));
        mList.add(new DrawerContentModel(R.drawable.icon_setting,  getString(R.string.drawer_setting), 4));
    }*/


    @Override
    protected boolean isWindowFeatureNoTitle() {
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        KotlinUtils.INSTANCE.isReStartApp(savedInstanceState, this);
        //初始化PreferenceUtil
        PreferenceUtil.init(this);
        //根据上次的语言设置，重新设置语言
        LocaleUtils.switchLanguage(this);
        //group = this;

        mContext = getInstance();

        MainApplication.getInstance().getAllActivities().add(this);

        setContentView(R.layout.activity_main_tab);
//        initData();
        initViews();
        initEvents();

        registerBlueReceiver();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        //从intent对象中把封装好的数据取出来
        String billTab = intent.getStringExtra("tab_index");
        if (billTab != null && billTab.equals("bill")) {//如果点击的是要进入账单页面
            setSelection(1);
            tab_index = 1;
        } else {//否则默认进入首页
            setSelection(0);
            tab_index = 0;
        }

    }

    //初始化控件
    public void initViews() {
        spay_title_view = findViewById(R.id.spay_title_view);
        mContent_view = findViewById(R.id.fl_content);
        mRadioGroup_bottom_view = findViewById(R.id.rg_bottom_view);

        mHomePay = findViewById(R.id.id_tab_pay);
        mHomeBill = findViewById(R.id.id_tab_bill);
        mPaymentLink = findViewById(R.id.id_tab_payment_link);
        mHomeSummary = findViewById(R.id.id_tab_summary);
        mHomeSetting = findViewById(R.id.id_tab_setting);


        //paymentLink测试
//        mPaymentLink.setVisibility(View.VISIBLE);
        if (null != MainApplication.getInstance().getPaymentLinkAuthority() && "1".equals(MainApplication.getInstance().getPaymentLinkAuthority().orderFlag)) {
            mPaymentLink.setVisibility(View.VISIBLE);
        } else {
            mPaymentLink.setVisibility(View.GONE);
        }

        if (MainApplication.getInstance().isAdmin(1)) {//如果是商户登录，则显示报表的tab
            mHomeSummary.setText(R.string.report_new_report);
            if (!PngSetting.INSTANCE.isChangePng1()) {
                Drawable drawable = getResources().getDrawable(R.drawable.icon_tab_report);
                float scale = getResources().getDisplayMetrics().density;// 屏幕密度
                drawable.setBounds(0, 0, (int) (30 * scale + 0.5f), (int) (30 * scale + 0.5f));//30,30为宽高
                mHomeSummary.setCompoundDrawables(null, drawable, null, null);
            }
        } else if (MainApplication.getInstance().isAdmin(0)) {//如果是收银员登录，则显示原来的汇总的tab
            mHomeSummary.setText(R.string.tab_sum_title);
            if (!PngSetting.INSTANCE.isChangePng1()) {
                Drawable drawable = getResources().getDrawable(R.drawable.icon_tab_summary);
                float scale = getResources().getDisplayMetrics().density;// 屏幕密度
                drawable.setBounds(0, 0, (int) (30 * scale + 0.5f), (int) (30 * scale + 0.5f));//30,30为宽高
                mHomeSummary.setCompoundDrawables(null, drawable, null, null);
            }
        }


        if (PngSetting.INSTANCE.isChangePng1()) {
            initPng();
        }
    }


    private class TabItemDrawableEntity {
        private int pressDrawableId;
        private int selectDrawableId;
        private int defaultDrawableId;

        public TabItemDrawableEntity(int pressDrawableId, int selectDrawableId, int defaultDrawableId) {
            this.pressDrawableId = pressDrawableId;
            this.selectDrawableId = selectDrawableId;
            this.defaultDrawableId = defaultDrawableId;
        }

        public int getPressDrawableId() {
            return pressDrawableId;
        }

        public TabItemDrawableEntity setPressDrawableId(int pressDrawableId) {
            this.pressDrawableId = pressDrawableId;
            return this;
        }

        public int getSelectDrawableId() {
            return selectDrawableId;
        }

        public TabItemDrawableEntity setSelectDrawableId(int selectDrawableId) {
            this.selectDrawableId = selectDrawableId;
            return this;
        }

        public int getDefaultDrawableId() {
            return defaultDrawableId;
        }

        public TabItemDrawableEntity setDefaultDrawableId(int defaultDrawableId) {
            this.defaultDrawableId = defaultDrawableId;
            return this;
        }
    }


    private void initPng() {

        mTabDrawableMap.clear();
        mTabDrawableMap.put(TAB_PAY,
                new TabItemDrawableEntity(
                        R.drawable.icon_tab_charge_sel,
                        R.drawable.icon_tab_charge_sel,
                        R.drawable.icon_tab_charge_nor));
        mTabDrawableMap.put(TAB_BILL,
                new TabItemDrawableEntity(
                        R.drawable.icon_tab_transaction_sel,
                        R.drawable.icon_tab_transaction_sel,
                        R.drawable.icon_tab_transaction_nor
                ));
        mTabDrawableMap.put(TAB_LINK,
                new TabItemDrawableEntity(
                        R.drawable.icon_link_on,
                        R.drawable.icon_link_on,
                        R.drawable.icon_link_default
                ));
        mTabDrawableMap.put(TAB_REPORT,
                new TabItemDrawableEntity(
                        R.drawable.icon_report,
                        R.drawable.icon_report,
                        R.drawable.icon_report_nor
                ));
        mTabDrawableMap.put(TAB_SUMMARY,
                new TabItemDrawableEntity(
                        R.drawable.icon_summary,
                        R.drawable.icon_summary,
                        R.drawable.icon_summary_nor
                ));
        mTabDrawableMap.put(TAB_SETTING,
                new TabItemDrawableEntity(
                        R.drawable.icon_tab_me_sel,
                        R.drawable.icon_tab_me_sel,
                        R.drawable.icon_tab_me_nor
                ));


        setTabDrawable(TAB_PAY, mHomePay);
        setTabDrawable(TAB_BILL, mHomeBill);
        setTabDrawable(TAB_LINK, mPaymentLink);
        setTabDrawable(TAB_SETTING, mHomeSetting);

        if (MainApplication.getInstance().isAdmin(1)) {//如果是商户登录的，则把汇总的tab换成报表的tab
            setTabDrawable(TAB_REPORT, mHomeSummary);
        } else if (MainApplication.getInstance().isAdmin(0)) {//如果是收银员登录，则不展示报表的tab，只展示旧的汇总页面
            setTabDrawable(TAB_SUMMARY, mHomeSummary);
        }
    }


    private void setTabDrawable(String tabType, MyRadioButton radioButton) {
        TabItemDrawableEntity itemDrawable = mTabDrawableMap.get(tabType);
        if (itemDrawable != null) {
            // 创建一个 Selector
            StateListDrawable selector = new StateListDrawable();

            Drawable pressDrawable = PngUtils.INSTANCE.getPngDrawable1(this, itemDrawable.pressDrawableId);
            Drawable selectDrawable = PngUtils.INSTANCE.getPngDrawable1(this, itemDrawable.selectDrawableId);
            Drawable defaultDrawable = getResources().getDrawable(itemDrawable.defaultDrawableId);


            selector.addState(new int[]{android.R.attr.state_pressed}, pressDrawable);
            selector.addState(new int[]{android.R.attr.state_selected}, selectDrawable);
            // 添加一个默认状态, 默认状态必须写在其他状态的最后面, 否则其他状态失效
            selector.addState(new int[]{}, defaultDrawable);


            float scale = getResources().getDisplayMetrics().density;// 屏幕密度
            selector.setBounds(0, 0, (int) (30 * scale + 0.5f), (int) (30 * scale + 0.5f));//30,30为宽高
            radioButton.setCompoundDrawables(null, selector, null, null);
        }
    }


    @Override
    public void onAttachFragment(Fragment fragment) {
        //当前的界面的保存状态，只是从新让新的Fragment指向了原本未被销毁的fragment，它就是onAttach方法对应的Fragment对象
        if (FragmentPay == null && fragment instanceof FragmentTabPay) {
            FragmentPay = fragment;
        } else if (FragmentBill == null && fragment instanceof FragmentTabBill) {
            FragmentBill = fragment;
        } else if (FragmentSummary == null && fragment instanceof FragmentTabSummary) {
            FragmentSummary = fragment;
        } else if (FragmentSetting == null && fragment instanceof FragmentTabSetting) {
            FragmentSetting = fragment;
        } else if (FragmentReport == null && fragment instanceof FragmentTabReport) {
            FragmentReport = fragment;
        } else if (FragmentPaymentLink == null && fragment instanceof FragmentTabPaymentLink) {
            FragmentPaymentLink = fragment;
        }
    }

    private void registerBlueReceiver() {
        IntentFilter filterBlue = new IntentFilter();
        filterBlue.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filterBlue.addAction(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                //android13
                registerReceiver(blueReceiver, filterBlue, Context.RECEIVER_VISIBLE_TO_INSTANT_APPS);
            } else {
                registerReceiver(blueReceiver, filterBlue);
            }
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(e, SentryUtils.INSTANCE.getClassNameAndMethodName());
            Logger.e("hehui", "" + e);
        }
    }

    public SpayTitleView getTitleView() {
        return spay_title_view;
    }

    public void cleanTitleAllView() {
        if (spay_title_view != null) {
            spay_title_view.cleanAllView();
        }
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        if (blueReceiver != null) {
            unregisterReceiver(blueReceiver);
        }

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction mfragmentTransaction = fm.beginTransaction();
        removeAllFragments(mfragmentTransaction);

        FragmentPay = null;
        FragmentBill = null;
        FragmentSummary = null;
        FragmentReport = null;
        FragmentPaymentLink = null;
        FragmentSetting = null;

        MainApplication.getInstance().getAllActivities().clear();

        // 通知释放空间
        //System.gc();
        Runtime.getRuntime().gc();

        android.os.Debug.stopMethodTracing();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        Log.d(TAG, "onStop");
    }

    public void initEvents() {
        mRadioGroup_bottom_view.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                for (int i = 0; i < tabIds.length; i++) {
                    if (tabIds[i] == checkedId) {
                        setSelection(i);
                        tab_index = i;
                        break;
                    }
                }
            }
        });

        Intent intent = getIntent();
        //从intent对象中把封装好的数据取出来
        String billTab = intent.getStringExtra("tab_index");
        if (billTab != null && billTab.equals("bill")) {//如果点击的是要进入账单页面
            setSelection(1);
            tab_index = 1;
        } else {
            setSelection(0);
            tab_index = 0;
        }


    }

    private void setSelection(int position) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction = fm.beginTransaction();
        hideAllFragments(mFragmentTransaction);
        getTitleView().setVisibility(View.VISIBLE);
        switch (position) {
            case 0:
                mHomePay.setSelected(true);
                mHomePay.setTextColor(getResources().getColor(R.color.bg_text_new));
                initPayTitle();

                if (FragmentPay == null) {
                    FragmentPay = new FragmentTabPay();
                    mFragmentTransaction.add(R.id.fl_content, FragmentPay);
                } else {
                    mFragmentTransaction.show(FragmentPay);
                }
                break;
            case 1:
                mHomeBill.setSelected(true);
                mHomeBill.setTextColor(getResources().getColor(R.color.bg_text_new));
                initBillTitle();

                // 如果是收银登录进来
                if (MainApplication.getInstance().isAdmin(0)) {
                    clickBillTab(mFragmentTransaction);
                } else {
                    if (!TextUtils.isEmpty(MainApplication.getInstance().getUserInfo().remark) && !MainApplication.getInstance().getUserInfo().remark.equals("1")) {
                        MyToast toast = new MyToast();
                        toast.showToast(getApplicationContext(), ToastHelper.toStr(R.string.show_mch_stream));
                    } else if (MainApplication.getInstance().getUserInfo().remark.equals("0")) {
                        if (MainApplication.getInstance().isOrderAuth("1")) {
                            clickBillTab(mFragmentTransaction);
                        }
                    } else {
                        clickBillTab(mFragmentTransaction);
                    }
                }
                break;
            case 2:
                selectTabPaymentLink(mFragmentTransaction);
                break;
            case 3:
                selectTabSummary(mFragmentTransaction);
                break;
            case 4:
                selectTabMy(mFragmentTransaction);
                break;
            default:
                break;
        }
        mFragmentTransaction.commit();
    }

    private void selectTabPaymentLink(FragmentTransaction mFragmentTransaction) {

        mPaymentLink.setSelected(true);
        mPaymentLink.setTextColor(getResources().getColor(R.color.bg_text_new));
        initPaymentLinkTitle();
        if (FragmentPaymentLink == null) {
            FragmentPaymentLink = new FragmentTabPaymentLink();
            mFragmentTransaction.add(R.id.fl_content, FragmentPaymentLink);
        } else {
            mFragmentTransaction.show(FragmentPaymentLink);
        }
    }

    private void selectTabMy(FragmentTransaction mFragmentTransaction) {

        mHomeSetting.setSelected(true);
        mHomeSetting.setTextColor(getResources().getColor(R.color.bg_text_new));
        initSettingTitle();
        if (FragmentSetting == null) {
            FragmentSetting = new FragmentTabSetting();
            mFragmentTransaction.add(R.id.fl_content, FragmentSetting);
        } else {
            mFragmentTransaction.show(FragmentSetting);
        }
    }

    private void selectTabSummary(FragmentTransaction mFragmentTransaction) {
        mHomeSummary.setSelected(true);
        mHomeSummary.setTextColor(getResources().getColor(R.color.bg_text_new));

        if (MainApplication.getInstance().isAdmin(1)) {//如果是商户登录的，则把汇总的tab换成报表的tab
            initReportTitle();
            mHomeSummary.setText(R.string.report_new_report);
            if (!PngSetting.INSTANCE.isChangePng1()) {
                Drawable drawable = getResources().getDrawable(R.drawable.icon_tab_report);
                float scale = getResources().getDisplayMetrics().density;// 屏幕密度
                drawable.setBounds(0, 0, (int) (30 * scale + 0.5f), (int) (30 * scale + 0.5f));//30,30为宽高
                mHomeSummary.setCompoundDrawables(null, drawable, null, null);
            }
            if (FragmentReport == null) {
                FragmentReport = new FragmentTabReport();
                mFragmentTransaction.add(R.id.fl_content, FragmentReport);
            } else {
                mFragmentTransaction.show(FragmentReport);
            }
        } else if (MainApplication.getInstance().isAdmin(0)) {//如果是收银员登录，则不展示报表的tab，只展示旧的汇总页面
            initSummaryTitle();
            mHomeSummary.setText(R.string.tab_sum_title);
            if (!PngSetting.INSTANCE.isChangePng1()) {
                Drawable drawable = getResources().getDrawable(R.drawable.icon_tab_summary);
                float scale = getResources().getDisplayMetrics().density;// 屏幕密度
                drawable.setBounds(0, 0, (int) (30 * scale + 0.5f), (int) (30 * scale + 0.5f));//30,30为宽高
                mHomeSummary.setCompoundDrawables(null, drawable, null, null);
            }
            if (FragmentSummary == null) {
                FragmentSummary = new FragmentTabSummary();
                mFragmentTransaction.add(R.id.fl_content, FragmentSummary);
            } else {
                mFragmentTransaction.show(FragmentSummary);
            }
            HandlerManager.notifyMessage(HandlerManager.SWITCH_TAB, HandlerManager.SUMMARY_SWITCH_TAB);
        }
    }

    public void initPayTitle() {
        cleanTitleAllView();

        if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1) { //如果商户已开通预授权才有下面的流程
            //读取上一次选择的是什么状态，然后根据状态设置title，默认是sale
            saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth", "sale");
            if (TextUtils.equals(saleOrPreauth, "sale")) {
                getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
                getTitleView().addMiddleTextView(getString(R.string.choose_sale));
                getTitleView().addMiddleImageView(R.drawable.icon_select);
                HandlerManager.notifyMessage(HandlerManager.CHOICE_SALE_OR_PRE_AUTH, HandlerManager.CHOICE_SALE);

            } else if (TextUtils.equals(saleOrPreauth, "pre_auth")) {
                getTitleView().setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
                getTitleView().addMiddleTextView(getString(R.string.choose_pre_auth));
                getTitleView().addMiddleImageView(R.drawable.icon_select);
                HandlerManager.notifyMessage(HandlerManager.CHOICE_SALE_OR_PRE_AUTH, HandlerManager.CHOICE_PRE_AUTH);
            }
            getTitleView().OnClickMiddleLayout(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tab_index == 0) { //收款页面
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                changePreauthDialog = new ChangePreauthDialog(spayMainTabActivity.this, ChangePreauthDialog.PageTypeEnum.PAY, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        changePreauthDialog.dismiss();
                                        //点击确定的时候保存当前的选择状态
                                        PreferenceUtil.commitString("choose_pay_or_pre_auth", changePreauthDialog.getSelectType());
                                        //更新tabpay的界面的颜色
                                        saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth", "sale");
                                        if (TextUtils.equals(saleOrPreauth, "sale")) {
                                            getTitleView().replaceMiddleTextView(0, getString(R.string.choose_sale));
                                            getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
                                            HandlerManager.notifyMessage(HandlerManager.CHOICE_SALE_OR_PRE_AUTH, HandlerManager.CHOICE_SALE);
                                        } else if (TextUtils.equals(saleOrPreauth, "pre_auth")) {
                                            getTitleView().replaceMiddleTextView(0, getString(R.string.choose_pre_auth));
                                            getTitleView().setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
                                            HandlerManager.notifyMessage(HandlerManager.CHOICE_SALE_OR_PRE_AUTH, HandlerManager.CHOICE_PRE_AUTH);
                                        }
                                    }
                                });

                                if (changePreauthDialog != null && !changePreauthDialog.isShowing()) {
                                    changePreauthDialog.show();
                                }
                            }
                        });
                    }
                }
            });
        } else { //如果没有开通预授权通道，则显示默认的颜色
            getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
        }
    }

    public void initBillTitle() {
        cleanTitleAllView();

        //如果当前商户开启预授权权限,或者开通卡交易通道，都弹选择框
        if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 || MainApplication.getInstance().hasCardPayment()) {
            //读取上一次选择的是什么状态，然后根据状态设置title，默认是sale
            bill_sale_or_Pre_auth_or_Card_payment = PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth", "sale");
            if (TextUtils.equals(bill_sale_or_Pre_auth_or_Card_payment, "sale")) {
                getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
                getTitleView().addMiddleTextView(getString(R.string.choose_sale));
                getTitleView().addMiddleImageView(R.drawable.icon_select);
            } else if (TextUtils.equals(bill_sale_or_Pre_auth_or_Card_payment, "pre_auth")) {
                getTitleView().setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
                getTitleView().addMiddleTextView(getString(R.string.choose_pre_auth));
                getTitleView().addMiddleImageView(R.drawable.icon_select);
            } else if (TextUtils.equals(bill_sale_or_Pre_auth_or_Card_payment, "card_payment")) {
                getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
                getTitleView().addMiddleTextView(getString(R.string.select_card_payment));
                getTitleView().addMiddleImageView(R.drawable.icon_select);
            }
            getTitleView().OnClickMiddleLayout(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tab_index == 1) {//账单页面
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                changeBillPreauthDialog = new ChangePreauthDialog(spayMainTabActivity.this, ChangePreauthDialog.PageTypeEnum.BILL, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        changeBillPreauthDialog.dismiss();
                                        //点击确定的时候保存当前的选择状态
                                        PreferenceUtil.commitString("bill_list_choose_pay_or_pre_auth", changeBillPreauthDialog.getSelectType());
                                        //更新title的界面的颜色
                                        bill_sale_or_Pre_auth_or_Card_payment = PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth", "sale");
                                        if (TextUtils.equals(bill_sale_or_Pre_auth_or_Card_payment, "sale")) {
                                            getTitleView().replaceMiddleTextView(0, getString(R.string.choose_sale));
                                            getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
                                            HandlerManager.notifyMessage(HandlerManager.BILL_CHOICE_SALE_OR_PRE_AUTH_OR_CARD_PAYMENT, HandlerManager.BILL_CHOICE_SALE);
                                        } else if (TextUtils.equals(bill_sale_or_Pre_auth_or_Card_payment, "pre_auth")) {
                                            getTitleView().replaceMiddleTextView(0, getString(R.string.choose_pre_auth));
                                            getTitleView().setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
                                            HandlerManager.notifyMessage(HandlerManager.BILL_CHOICE_SALE_OR_PRE_AUTH_OR_CARD_PAYMENT, HandlerManager.BILL_CHOICE_PRE_AUTH);
                                        } else if (TextUtils.equals(bill_sale_or_Pre_auth_or_Card_payment, "card_payment")) {
                                            getTitleView().replaceMiddleTextView(0, getString(R.string.select_card_payment));
                                            getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
                                            HandlerManager.notifyMessage(HandlerManager.BILL_CHOICE_SALE_OR_PRE_AUTH_OR_CARD_PAYMENT, HandlerManager.BILL_CHOICE_MASTER_CARD);
                                        }
                                    }
                                });

                                if (changeBillPreauthDialog != null && !changeBillPreauthDialog.isShowing()) {
                                    changeBillPreauthDialog.show();
                                }
                            }
                        });
                    }
                }
            });
        } else { //如果当前商户没有开启预授权权限，也没有开通卡交易通道，则只展示交易
            getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
            getTitleView().addMiddleTextView(getResources().getString(R.string.tab_bill_title));
        }

        getTitleView().addLeftImageView(R.drawable.icon_scan, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CaptureActivity.startActivity(mContext, "pay");
            }
        });

        getTitleView().addRightTextView(getResources().getString(R.string.search_order), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 || MainApplication.getInstance().hasCardPayment()) {
                    bill_sale_or_Pre_auth_or_Card_payment = PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth", "sale");
                    if (TextUtils.equals(bill_sale_or_Pre_auth_or_Card_payment, "sale")) {
                        OrderSearchActivity.startActivity(mContext, false);
                    } else if (TextUtils.equals(bill_sale_or_Pre_auth_or_Card_payment, "pre_auth")) {
                        OrderSearchActivity.startActivity(mContext, false);
                    } else if (TextUtils.equals(bill_sale_or_Pre_auth_or_Card_payment, "card_payment")) {
                        OrderSearchActivity.startActivity(mContext, true);
                    }
                } else {
                    OrderSearchActivity.startActivity(mContext, false);
                }

            }
        });
    }

    public void initSummaryTitle() {
        cleanTitleAllView();
        getTitleView().addMiddleTextView(getResources().getString(R.string.tab_sum_title));
        getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));

        getTitleView().addRightTextView(getResources().getString(R.string.string_view_ticket), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HandlerManager.notifyMessage(HandlerManager.CLICK_TITLE, HandlerManager.SUNMMARY_PRINT_DATA);
            }
        });
    }

    public void initReportTitle() {
        cleanTitleAllView();
        getTitleView().addMiddleTextView(getResources().getString(R.string.report_new_report));
        getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));

        getTitleView().addRightTextView(getResources().getString(R.string.tab_sum_title), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //跳转到汇总页
                Intent intent = new Intent(getInstance(), SummaryActivity.class);
                startActivity(intent);
            }
        });
    }

    public void initSettingTitle() {
        if (MainApplication.getInstance().isAdmin(0)) {
            //收银员
            getTitleView().setVisibility(View.GONE);
        } else {
            cleanTitleAllView();
            getTitleView().addMiddleTextView(getResources().getString(R.string.tab_setting_title));
            getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
        }
    }

    public void initPaymentLinkTitle() {
        cleanTitleAllView();
        if (null != FragmentPaymentLink) {
            int type = ((FragmentTabPaymentLink) FragmentPaymentLink).getListType();
            setPaymentLinkMidTitle(type);
        } else {
            setPaymentLinkMidTitle(TAB_LIST_TYPE_ORDER);
        }

        getTitleView().addRightImageView(R.drawable.icon_add, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int type = ((FragmentTabPaymentLink) FragmentPaymentLink).getListType();
                switch (type) {
                    case TAB_LIST_TYPE_ORDER:
                        OrderCreateActivity.startActivityForResult(spayMainTabActivity.this, CREATE_ORDER);
                        break;
                    case TAB_LIST_TYPE_CUSTOMER:
                        CustAndProdEditActivity.startActivityForResult(spayMainTabActivity.this, MANAGE_CREATE_CUSTOMER, null);
                        break;
                    case TAB_LIST_TYPE_PRODUCT:
                        CustAndProdEditActivity.startActivityForResult(spayMainTabActivity.this, MANAGE_CREATE_PRODUCT, null);
                        break;
                }
            }
        });
        getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));

        getTitleView().OnClickMiddleLayout(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogPaymentLinkSelect fragment = DialogPaymentLinkSelect.newInstance();
                fragment.setClickListener(new DialogPaymentLinkSelect.OnItemClickListener() {
                    @Override
                    public void onSelectOrder() {
                        ((FragmentTabPaymentLink) FragmentPaymentLink).setListType(TAB_LIST_TYPE_ORDER);
                        setPaymentLinkMidTitle(TAB_LIST_TYPE_ORDER);
                    }

                    @Override
                    public void onSelectCustomer() {
                        ((FragmentTabPaymentLink) FragmentPaymentLink).setListType(TAB_LIST_TYPE_CUSTOMER);
                        setPaymentLinkMidTitle(TAB_LIST_TYPE_CUSTOMER);
                    }

                    @Override
                    public void onSelectProduct() {
                        ((FragmentTabPaymentLink) FragmentPaymentLink).setListType(TAB_LIST_TYPE_PRODUCT);
                        setPaymentLinkMidTitle(TAB_LIST_TYPE_PRODUCT);
                    }
                });
                fragment.show(getSupportFragmentManager(), "selectList");
            }
        });
    }

    private void setPaymentLinkMidTitle(int type) {
        switch (type) {
            case TAB_LIST_TYPE_ORDER:
                getTitleView().cleanMidView();
                getTitleView().addMiddleTextView(getResources().getString(R.string.tab_payment_link_order_title));
                getTitleView().addMiddleImageView(R.drawable.icon_select);
                break;
            case TAB_LIST_TYPE_CUSTOMER:
                getTitleView().cleanMidView();
                getTitleView().addMiddleTextView(getResources().getString(R.string.tab_payment_link_customer_title));
                getTitleView().addMiddleImageView(R.drawable.icon_select);
                break;
            case TAB_LIST_TYPE_PRODUCT:
                getTitleView().cleanMidView();
                getTitleView().addMiddleTextView(getResources().getString(R.string.tab_payment_link_product_title));
                getTitleView().addMiddleImageView(R.drawable.icon_select);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == KotlinUtils.APP_INSTALL_REQUEST_PERMISSION_PAY) {
            //授予安装未知应用权限
            if (FragmentPay != null) {
                ((OnActivityDataToFragmentListener) FragmentPay).onResult(requestCode, resultCode, data);
            }
        }
        if (requestCode == KotlinUtils.APP_INSTALL_REQUEST_PERMISSION_SETTING) {
            //授予安装未知应用权限
            if (FragmentSetting != null) {
                ((OnActivityDataToFragmentListener) FragmentSetting).onResult(requestCode, resultCode, data);
            }
        }
        if (requestCode == MANAGE_CREATE_CUSTOMER || requestCode == MANAGE_CREATE_PRODUCT || requestCode == MANAGE_EDIT_CUSTOMER || requestCode == MANAGE_EDIT_PRODUCT || requestCode == CREATE_ORDER || requestCode == SEARCH_TYPE_MANAGE_ORDER || requestCode == SEARCH_TYPE_MANAGE_CUSTOMER || requestCode == SEARCH_TYPE_MANAGE_PRODUCT) {
            if (null != FragmentPaymentLink) {
                FragmentPaymentLink.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    public void showPage(Class clazz) {
        Intent intent = new Intent(mContext, clazz);
        this.startActivity(intent);
    }

    private void clickBillTab(FragmentTransaction mfragmentTransaction) {
        if (FragmentBill == null) {
            FragmentBill = new FragmentTabBill();
            mfragmentTransaction.add(R.id.fl_content, FragmentBill);
        } else {
            mfragmentTransaction.show(FragmentBill);
        }
        HandlerManager.notifyMessage(HandlerManager.STREAM_BILL, HandlerManager.PAY_SWITCH_TAB);
    }

    public void removeAllFragments(FragmentTransaction ft) {
        if (FragmentPay != null) {
            ft.remove(FragmentPay);
        }

        if (FragmentBill != null) {
            ft.remove(FragmentBill);
        }

        if (FragmentSummary != null) {
            ft.remove(FragmentSummary);
        }

        if (FragmentReport != null) {
            ft.remove(FragmentReport);
        }

        if (FragmentSetting != null) {
            ft.remove(FragmentSetting);
        }

        if (FragmentPaymentLink != null) {
            ft.remove(FragmentPaymentLink);
        }
    }

    private void hideAllFragments(FragmentTransaction ft) {
        if (FragmentPay != null) {
            ft.hide(FragmentPay);
        }
        mHomePay.setSelected(false);
        mHomePay.setTextColor(getResources().getColor(R.color.tab_text_nor));


        if (FragmentBill != null) {
            ft.hide(FragmentBill);
        }
        mHomeBill.setSelected(false);
        mHomeBill.setTextColor(getResources().getColor(R.color.tab_text_nor));


        if (FragmentSummary != null) {
            ft.hide(FragmentSummary);
        }
        mHomeSummary.setSelected(false);
        mHomeSummary.setTextColor(getResources().getColor(R.color.tab_text_nor));


        if (FragmentSetting != null) {
            ft.hide(FragmentSetting);
        }
        mHomeSetting.setSelected(false);
        mHomeSetting.setTextColor(getResources().getColor(R.color.tab_text_nor));


        if (FragmentReport != null) {
            ft.hide(FragmentReport);
        }
        mHomeSummary.setSelected(false);
        mHomeSummary.setTextColor(getResources().getColor(R.color.tab_text_nor));


        if (FragmentPaymentLink != null) {
            ft.hide(FragmentPaymentLink);
        }
        mPaymentLink.setSelected(false);
        mPaymentLink.setTextColor(getResources().getColor(R.color.tab_text_nor));
    }

    @Override
    public boolean onKeyDown(int keycode, KeyEvent event) {
        if (keycode == KeyEvent.KEYCODE_BACK) {
            showExitDialog(this);
            return true;
        }
        return super.onKeyDown(keycode, event);
    }

    public void showExitDialog(final Activity context) {
        dialogInfo = new DialogInfo(context, getString(R.string.public_cozy_prompt), getString(R.string.show_sign_out), getString(R.string.btnOk), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                finish();
                getInstance().exit();

            }

            @Override
            public void handleCancelBtn() {
                dialogInfo.cancel();
            }
        }, null);

        DialogHelper.resize(context, dialogInfo);
        dialogInfo.show();
    }


}