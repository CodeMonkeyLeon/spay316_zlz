/*
 * 文 件 名:  DealStreamNew.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  yan_shuo
 * 修改时间:  2015-5-4
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.enums.OrderStatusEnum;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.OrderSearchResult;
import cn.swiftpass.enterprise.ui.activity.user.RefundManagerActivity;
import cn.swiftpass.enterprise.ui.activity.user.RefundRecordOrderDetailsActivity;
import cn.swiftpass.enterprise.ui.adapter.RefundHistoryAdapter;
import cn.swiftpass.enterprise.ui.widget.PullDownListView;
import cn.swiftpass.enterprise.ui.widget.PullDownListView.OnRefreshListioner;
import cn.swiftpass.enterprise.utils.DateUtil;

public class DealStreamNew extends BaseActivity implements OnClickListener, OnItemClickListener, OnRefreshListioner
{
    
    private PullDownListView pullDownListView;
    
    private ListView orderListView;
    
    private LinearLayout deal_stream_new_back_btn;
    
    private TextView deal_stream_new_title_tv;
    
    private LinearLayout deal_stream_new_search_btn;
    
    private Button deal_stream_new_top_left_btn, deal_stream_new_top_right_btn;
    
    private LinearLayout ll_deal_stream_new_title;
    
    private List<Order> receivablesList;//支付
    
    private List<Order> refundList;//退款
    
    private RefundHistoryAdapter receivablesAdapter;//支付
    
    private RefundHistoryAdapter refundAdapter;//退款
    
    private boolean isFirstInitReceivables = true;//是否是第一次查询收款数据
    
    private boolean isFirstInitRefund = true;//是否是第一次查询退款数据
    
    private long searchRefundTime; //退款查询时间
    
    private long searchReceTime;//订单流水时间(已支付)
    
    private Handler mHandler = new Handler();
    
    private OrderStatusEnum orderState;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deal_stream_new_layout);
        
        initObject();
        initView();
        initListener();
        loadMoreDate();
    }
    
    private void initObject()
    {
        searchRefundTime = System.currentTimeMillis();
        searchReceTime = searchRefundTime;
        orderState = OrderStatusEnum.PAY_SUCCESS;
        
        receivablesList = new ArrayList<Order>();
        refundList = new ArrayList<Order>();
        
        receivablesAdapter = new RefundHistoryAdapter(this, receivablesList, RefundHistoryAdapter.ORDER_STREAM);
        refundAdapter = new RefundHistoryAdapter(this, refundList, RefundHistoryAdapter.ORDER_STREAM_REFUND_RECORD);
    }
    
    private void initListener()
    {
        deal_stream_new_back_btn.setOnClickListener(this);
        deal_stream_new_search_btn.setOnClickListener(this);
        ll_deal_stream_new_title.setOnClickListener(this);
        
        deal_stream_new_top_left_btn.setOnClickListener(this);
        deal_stream_new_top_right_btn.setOnClickListener(this);
        
        orderListView.setOnItemClickListener(this);
        pullDownListView.setRefreshListioner(this);
    }
    
    private void initView()
    {
        deal_stream_new_back_btn = getViewById(R.id.deal_stream_new_back_btn);
        deal_stream_new_title_tv = getViewById(R.id.deal_stream_new_title_tv);
        deal_stream_new_search_btn = getViewById(R.id.deal_stream_new_search_btn);
        
        deal_stream_new_top_left_btn = getViewById(R.id.deal_stream_new_top_left_btn);
        deal_stream_new_top_right_btn = getViewById(R.id.deal_stream_new_top_right_btn);
        ll_deal_stream_new_title = getViewById(R.id.ll_deal_stream_new_title);
        
        pullDownListView = getViewById(R.id.list);
        orderListView = pullDownListView.mListView;
        
        setTitleText();
    }
    
    private void setTitleText()
    {
        if (orderState == OrderStatusEnum.PAY_SUCCESS)
        {
            
            deal_stream_new_title_tv.setText("交易流水" + DateUtil.formatMD(searchReceTime));
        }
        else
        {
            deal_stream_new_title_tv.setText("交易流水" + DateUtil.formatMD(searchRefundTime));
        }
    }
    
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.deal_stream_new_back_btn://返回
                this.finish();
                break;
            case R.id.deal_stream_new_search_btn://搜索
                
                Bundle b = new Bundle();
                b.putBoolean("isFromOrderStreamSearch", true);
                
                if (orderState == OrderStatusEnum.PAY_SUCCESS)
                {
                    b.putLong("searchTime", searchReceTime);
                    b.putInt("serchType", 0);
                }
                else if (orderState == OrderStatusEnum.RETURN_PRODUCT)
                {
                    b.putLong("searchTime", searchRefundTime);
                    b.putInt("serchType", 1);
                }
                showPage(RefundManagerActivity.class, b);
                
                break;
            case R.id.deal_stream_new_top_left_btn://已支付
                if (orderState == OrderStatusEnum.PAY_SUCCESS)
                {
                    return;
                }
                else
                {
                    orderState = OrderStatusEnum.PAY_SUCCESS;
                }
                if (isFirstInitReceivables)
                {
                    //从服务器查询
                    loadMoreDate();
                }
                else
                {
                    //直接设置
                    if (receivablesList != null && receivablesAdapter != null && receivablesList.size() > 0)
                    {
                        pullDownListView.setVisibility(View.VISIBLE);
                        orderListView.setAdapter(receivablesAdapter);
                        refundAdapter.notifyDataSetChanged();
                        deal_stream_new_top_left_btn.setText("已支付(" + receivablesList.size() + ")");
                        
                    }
                    else
                    {
                        pullDownListView.setVisibility(View.GONE);
                        //                        order_stream_count_tv.setText("暂无收款记录");
                    }
                }
                settextColor(0);
                break;
            case R.id.deal_stream_new_top_right_btn://已退款
                if (orderState == OrderStatusEnum.RETURN_PRODUCT)
                {
                    return;
                }
                else
                {
                    orderState = OrderStatusEnum.RETURN_PRODUCT;
                }
                if (isFirstInitRefund)
                {
                    //从服务器查询
                    loadMoreDate();
                }
                else
                {
                    //直接设置
                    if (refundList != null && refundAdapter != null && refundList.size() > 0)
                    {
                        pullDownListView.setVisibility(View.VISIBLE);
                        orderListView.setAdapter(refundAdapter);
                        refundAdapter.notifyDataSetChanged();
                        deal_stream_new_top_right_btn.setText("已退款(" + refundList.size() + ")");
                        
                    }
                    else
                    {
                        pullDownListView.setVisibility(View.GONE);
                        //                        order_stream_count_tv.setText("暂无退款记录");
                    }
                }
                settextColor(1);
                break;
            case R.id.ll_deal_stream_new_title://选择日期
                showDatetimeDialog();
                break;
            default:
                break;
        }
    }
    
    private void settextColor(int i)
    {
        
        switch (i)
        {
            case 0:
                deal_stream_new_top_left_btn.setText("已支付(" + receivablesList.size() + ")");
                deal_stream_new_top_left_btn.setTextColor(Color.parseColor("#ff7f00"));
                deal_stream_new_top_right_btn.setTextColor(Color.parseColor("#000000"));
                break;
            case 1:
                deal_stream_new_top_right_btn.setText("已退款(" + refundList.size() + ")");
                deal_stream_new_top_left_btn.setTextColor(Color.parseColor("#000000"));
                deal_stream_new_top_right_btn.setTextColor(Color.parseColor("#ff7f00"));
                break;
            default:
                break;
        }
        setTitleText();
    }
    
    /***
     * 查询服务器数据
     */
    private void loadMoreDate()
    {
        if (orderState == OrderStatusEnum.PAY_SUCCESS)
        {
            //查询订单流水
            search();
        }
        else
        {
            //查询退款订单
            searchRefundRecord();
        }
        
    }
    
    private Calendar c = null;
    
    public void showDatetimeDialog()
    {
        c = Calendar.getInstance();
        if (orderState == OrderStatusEnum.PAY_SUCCESS)
        {
            //订单流水
            c.setTimeInMillis(searchReceTime);
        }
        else if (orderState == OrderStatusEnum.RETURN_PRODUCT)
        {
            //退款查询 
            c.setTimeInMillis(searchRefundTime);
        }
        
        dialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener()
        {
            public void onDateSet(DatePicker dp, int year, int month, int dayOfMonth)
            {
                // String d = DateUtil.formatTime(datetime, "yyyy-MM-dd") ;
                // c.set(Calendar.DAY_OF_YEAR,);
                c.set(year, month, dayOfMonth);
                if (orderState == OrderStatusEnum.PAY_SUCCESS)
                {
                    //订单流水
                    searchReceTime = c.getTimeInMillis();
                    deal_stream_new_title_tv.setText("交易流水" + DateUtil.formatMD(searchReceTime));
                }
                else if (orderState == OrderStatusEnum.RETURN_PRODUCT)
                {
                    //退款查询 
                    searchRefundTime = c.getTimeInMillis();
                    deal_stream_new_title_tv.setText("交易流水" + DateUtil.formatMD(searchRefundTime));
                }
                
                loadMoreDate();
                
            }
        }, c.get(Calendar.YEAR), // 传入年份
            c.get(Calendar.MONTH), // 传入月份
            c.get(Calendar.DAY_OF_MONTH) // 传入天数*/
            );
        dialog.show();
    }
    
    //查询流水订单
    private void search()
    {
        if (!TextUtils.isEmpty(MainApplication.getMchId()))
        {
            // 商户号（string）  当前页  查询状态  查询时间  查询类型
            OrderManager.getInstance().queryOrderData("",
                1,
                orderState.getValue(),
                DateUtil.formatYYMD(searchReceTime),
                3,
                null,
                new UINotifyListener<OrderSearchResult>()
                {
                    
                    @Override
                    public void onError(Object object)
                    {
                        super.onError(object);
                        dismissLoading();
                        if (object != null)
                        {
                            showToastInfo(object.toString());
                        }
                    }
                    
                    @Override
                    public void onPreExecute()
                    {
                        super.onPreExecute();
                        //                        showLoading(false, "数据加载中...");
                        
                    }
                    
                    @Override
                    public void onSucceed(OrderSearchResult result)
                    {
                        
                        super.onSucceed(result);
                        dismissLoading();
                        isFirstInitReceivables = false;
                        if (result != null && result.orders != null && result.orders.size() > 0)
                        {
                            //total = DateUtil.formatMoneyUtil(result.total / 100d);
                            //tvTotal.setText(total);
                            //order_stream_count_tv.setText("成功收款" + result.orders.size() + "笔");
                            
                            myNotifyData(result.orders,
                                pullDownListView,
                                receivablesList,
                                orderListView,
                                receivablesAdapter);
                            
                            setPullDownListViewState(pullDownListView, "刷新成功");
                            
                        }
                        else
                        {
                            pullDownListView.onfinish("刷新失败");// 刷新完成
                            mHandler.postDelayed(new Runnable()
                            {
                                
                                public void run()
                                {
                                    // 这里表示刷新处理完成后把上面的加载刷新界面隐藏
                                    pullDownListView.onRefreshComplete();
                                }
                            }, 500);
                            receivablesList.clear();
                            receivablesAdapter.notifyDataSetChanged();
                            pullDownListView.setVisibility(View.GONE);
                            
                            //                            total = "0.00";
                            //                            tvTotal.setText(total);
                            //                            order_stream_count_tv.setText("暂无收款记录");
                        }
                        deal_stream_new_top_left_btn.setText("已支付(" + receivablesList.size() + ")");
                    }
                    
                });
            
        }
        else
        {
            
        }
    }
    
    //查询退款订单
    private void searchRefundRecord()
    {
        OrderManager.getInstance().queryUserRefundOrder("",
            DateUtil.formatYYMD(searchRefundTime),
            MainApplication.getMchId(),
            "1",
            0,
            new UINotifyListener<List<Order>>()
            {
                
                @Override
                public void onError(Object object)
                {
                    super.onError(object);
                    dismissLoading();
                }
                
                @Override
                public void onPreExecute()
                {
                    super.onPreExecute();
                    //                    showLoading(false, "数据加载中...");
                }
                
                @Override
                public void onSucceed(List<Order> result)
                {
                    super.onSucceed(result);
                    dismissLoading();
                    if (result != null && refundList != null && result.size() > 0)
                    {
                        //                        refundTotal = getRefundTotal(result);
                        //                        tvTotal.setText(refundTotal);
                        //                        order_stream_count_tv.setText("退款已受理" + result.size() + "笔");
                        isFirstInitRefund = false;
                        myNotifyData(result, pullDownListView, refundList, orderListView, refundAdapter);
                        setPullDownListViewState(pullDownListView, "刷新成功");
                    }
                    else
                    {
                        refundList.clear();
                        refundAdapter.notifyDataSetChanged();
                        pullDownListView.setVisibility(View.GONE);
                        
                        //                        refundTotal = "0.00";
                        //                        tvTotal.setText(refundTotal);
                        //                        order_stream_count_tv.setText("暂无退款记录");
                    }
                    deal_stream_new_top_right_btn.setText("已退款(" + refundList.size() + ")");
                }
                
            });
        
    }
    
    protected void myNotifyData(List<Order> result, PullDownListView pull, List<Order> list, ListView listView,
        RefundHistoryAdapter adapter)
    {
        pull.setVisibility(View.VISIBLE);
        list.clear();
        list.addAll(result);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
    
    protected void setPullDownListViewState(final PullDownListView pull, String desc)
    {
        pull.onfinish(desc);// 刷新完成
        mHandler.postDelayed(new Runnable()
        {
            public void run()
            {
                // 这里表示刷新处理完成后把上面的加载刷新界面隐藏
                pull.onRefreshComplete();
            }
        }, 500);
        
    }
    
    /** {@inheritDoc} */
    
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Order order = null;
        if (orderState == OrderStatusEnum.PAY_SUCCESS)
        {
            //查看订单详情
            order = receivablesList.get(position - 1);
            OrderDetailsActivity.startActivity(this, order);
        }
        else if (orderState == OrderStatusEnum.RETURN_PRODUCT)
        {
            //查看退款详情
            order = refundList.get(position - 1);
            RefundRecordOrderDetailsActivity.startActivity(this, order);
        }
    }
    
    @Override
    public void onRefresh()
    {
        loadMoreDate();
    }
    
    @Override
    public void onLoadMore()
    {
        
    }
    
}
