package com.cxy.networklibrary.di.module;


import android.support.v4.util.SimpleArrayMap;


import com.cxy.networklibrary.activity.ActivityLife;
import com.cxy.networklibrary.activity.IActivityLife;
import com.cxy.networklibrary.fragment.FragmentLife;
import com.cxy.networklibrary.fragment.IFragmentLife;
import com.cxy.networklibrary.other.ActivityListManager;
import com.cxy.networklibrary.other.CrashDiary;
import com.cxy.networklibrary.other.PermissionManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;



@Module
public class OtherModule {

    @Singleton
    @Provides
    ActivityListManager activityListManager() {
        return new ActivityListManager();
    }

    @Singleton
    @Provides
    PermissionManager permissionManager() {
        return new PermissionManager();
    }

    @Singleton
    @Provides
    CrashDiary crashDiary() {
        return new CrashDiary();
    }

    @Singleton
    @Provides
    SimpleArrayMap<String, IActivityLife> iActivityLifes() {
        return new SimpleArrayMap<>();
    }

    @Provides
    IActivityLife iActivityLife() {
        return new ActivityLife();
    }

    @Singleton
    @Provides
    SimpleArrayMap<String, IFragmentLife> iFragmentLifes() {
        return new SimpleArrayMap<>();
    }

    @Provides
    IFragmentLife iFragmentLife() {
        return new FragmentLife();
    }


}

