/*
 * 文 件 名:  ImagePase.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-14
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.shop;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.util.Log;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.ImageUtil;

/**
 * 图片处理辅助类
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-14]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class ImagePase
{
    private final static String TAG = ImagePase.class.getCanonicalName();
    
    // 压缩的大小
    private final static int ImageSize_Zoom = 120;
    
    public static int bitmapSize_Image = 480;
    
    public static int chat_Image_min_h = 145;
    
    public static int chat_Image_min_w = 205;
    
    public static int chat_Image_mh = 208;
    
    public static int chat_Image_mw = 157;
    
    /** 文件压缩质量 */
    private static final int COMPRESS_QUALITY = 85;
    
    public static String imageCompress(String path, int requestCode, int imageSize)
    {
        if (null == path || 0 >= path.length())
        {
            Log.e(TAG, "error. the path file for compress is null.");
            return null;
        }
        
        File file = new File(path);
        String newPath = "";
        FileOutputStream fos = null;
        Bitmap bitmap = null;
        int quality = COMPRESS_QUALITY;
        
        /** 先判断大小是否超过100K 超过100K 压缩 */
        if (file.exists() && file.length() / 1024 > imageSize)
        {
            try
            {
                bitmap = createBitmap(path, bitmapSize_Image);
                if (bitmap != null)
                {
                    // 相册取图片
                    if (requestCode == ShopkeeperNextActivity.REQUEST_CODE_TAKE_PICTURE)
                    {
                        newPath = AppHelper.getImgCacheDir() + new Date() + ".jpg";
                    } // 拍照
                    else if (requestCode == ShopkeeperNextActivity.REQUEST_CODE_CAPTURE_PICTURE)
                    {
                        newPath = path;
                        file.delete();
                        new File(newPath);
                    }
                    fos = new FileOutputStream(newPath);
                    // 压缩图片
                    bitmap.compress(Bitmap.CompressFormat.JPEG, quality, fos);
                    fos.flush();
                    fos.close();
                    // 回收bitmap
                    if (!bitmap.isRecycled())
                    {
                        bitmap.recycle();
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            newPath = path;
        }
        return newPath;
    }
    
    /**
     * 返回path路径下对应图片的缩略图
     * 
     * @param path
     * @return String
     * @throws Exception 
     */
    public static String getThumbNail(String path)
        throws Exception
    {
        if (isEmpty(path, true) || !(new File(path).exists()))
        {
            // path为空，或者文件不存在
            Log.e(TAG, "getThumbNail: the param path is null or the file does not exist.");
            return null;
        }
        String thumbPath = "";
        FileOutputStream fos = null;
        Bitmap bitmap = null;
        try
        {
            bitmap = createBitmap(path, ImageSize_Zoom);
            if (null != bitmap)
            {
                File file = new File(AppHelper.getImgCacheDir() + "pic/");
                if (!file.exists())
                {
                    file.mkdirs();
                }
                thumbPath = AppHelper.getImgCacheDir() + "pic/" + new Date().getTime() + ".jpg";
                Log.i("hehui", "thumbPath = " + thumbPath);
                bitmap = drawableToBitmap(bitmap);
                fos = new FileOutputStream(thumbPath);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fos);
                
                fos = new FileOutputStream(path);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fos);
                
                fos.flush();
                fos.close();
                if (!bitmap.isRecycled())
                {
                    bitmap.recycle();
                }
            }
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (OutOfMemoryError e)
        {
            e.printStackTrace();
        }
        return thumbPath;
    }
    
    /**
     * 将特殊的缩略图裁剪
     * 
     * @param bm
     * @return
     */
    public static Bitmap drawableToBitmap(Bitmap bm)
    {
        if (bm == null)
        {
            return null;
        }
        BitmapDrawable drawable = new BitmapDrawable(bm);
        Bitmap bitmap = null;
        Bitmap bitmap1 = null;
        try
        {
            int width = drawable.getIntrinsicWidth();
            int height = drawable.getIntrinsicHeight();
            int w = chat_Image_min_w;
            int h = chat_Image_min_h;
            int begingWidth = 0;
            int begingHegiht = 0;
            // 处理图片高度>宽度的图片
            if (height > width)
            {
                h = chat_Image_mh;
                w = chat_Image_mw;
                if (width < chat_Image_mw)
                {
                    height = (int)(height * chat_Image_mw) / width;
                    width = chat_Image_mw;
                }
                if (height < chat_Image_mh)
                {
                    width = (int)(width * chat_Image_mh) / height;
                    height = chat_Image_mh;
                }
                
            }
            // 处理宽度>高度图片或正方形图片
            else
            {
                if (width == height)
                {
                    width = chat_Image_min_w;
                    height = chat_Image_min_w;
                }
                if (width < chat_Image_min_w)
                {
                    height = (int)(height * chat_Image_min_w) / width;
                    width = chat_Image_min_w;
                }
                if (height < chat_Image_min_h)
                {
                    width = (int)(width * chat_Image_min_h) / height;
                    height = chat_Image_min_h;
                }
            }
            if (width > w)
            {
                begingWidth = (width - w) / 2;
            }
            if (height > h)
            {
                begingHegiht = (height - h) / 2;
            }
            Bitmap.Config config =
                drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565;
            bitmap = Bitmap.createBitmap(width, height, config);
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, width, height);
            drawable.draw(canvas);
            bitmap1 = Bitmap.createBitmap(bitmap, begingWidth, begingHegiht, w, h);
        }
        catch (OutOfMemoryError e)
        {
            if (null != bitmap && !bitmap.isRecycled())
            {
                bitmap.recycle();
                bitmap = null;
            }
            if (null != bitmap1 && !bitmap1.isRecycled())
            {
                bitmap1.recycle();
                bitmap1 = null;
            }
            e.printStackTrace();
            return bm;
            
        }
        finally
        {
            
            if (null != bitmap && !bitmap.isRecycled())
            {
                bitmap.recycle();
                bitmap = null;
            }
        }
        return bitmap1;
    }
    
    /**
     * 方法表述 判断字符串是否为空
     * 
     * @param str 被判断的字符串
     * @param isToTrim 字符串是否需要前后去除空格
     * @return boolean true 字符串为空； false 字符串不为空
     */
    public static boolean isEmpty(String str, boolean isToTrim)
    {
        if (null == str)
        {
            return true;
        }
        if (isToTrim)
        {
            if (0 >= str.trim().length())
            {
                return true;
            }
        }
        else
        {
            if (0 >= str.length())
            {
                return true;
            }
        }
        return false;
    }
    
    /**
     * 按比例压缩图片,处理大图 防止内存溢出
     */
    public static Bitmap createBitmap(String picPath, int target)
    {
        Bitmap sourceBitmap = null;
        do
        {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(picPath, options);
            if (options.mCancel || options.outWidth == -1 || options.outHeight == -1)
            {
                return null;
            }
            options.inSampleSize = computeSampleSize(options, target);
            options.inJustDecodeBounds = false;
            options.inDither = false;
            
            // options.inPreferredConfig = Bitmap.Config.ARGB_4444;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            InputStream is = null;
            try
            {
                is = new FileInputStream(picPath);
                sourceBitmap = BitmapFactory.decodeStream(is, null, options);
                try
                {
                    String root = AppHelper.getImgCacheDir() + String.valueOf(new Date().getTime()) + ".jpg";
                    ImageUtil.saveImag2(root, sourceBitmap, CompressFormat.JPEG);
                    
                    sourceBitmap = ImagePase.readBitmapFromStream(root);
                }
                catch (Exception e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                options = null;
                if (null == sourceBitmap)
                {
                    break;
                }
                int width = sourceBitmap.getWidth();
                int height = sourceBitmap.getHeight();
                int max = Math.max(width, height);
                if (max > target)
                {
                    Matrix matrix = new Matrix();
                    float scale = ((float)target) / max;
                    matrix.postScale(scale, scale);
                    sourceBitmap = Bitmap.createBitmap(sourceBitmap, 0, 0, width, height, matrix, true);
                }
            }
            catch (OutOfMemoryError e)
            {
                try
                {
                    if (is != null)
                    {
                        is.close();
                        is = null;
                    }
                    
                }
                catch (Exception ex)
                {
                    // TODO Auto-generated catch block
                    ex.printStackTrace();
                }
                if (null != sourceBitmap && !sourceBitmap.isRecycled())
                {
                    sourceBitmap.recycle();
                    sourceBitmap = null;
                }
                e.printStackTrace();
            }
            catch (FileNotFoundException e)
            {
                try
                {
                    if (is != null)
                    {
                        is.close();
                        is = null;
                    }
                }
                catch (IOException ex)
                {
                    // TODO Auto-generated catch block
                    ex.printStackTrace();
                }
                if (null != sourceBitmap && !sourceBitmap.isRecycled())
                {
                    sourceBitmap.recycle();
                    sourceBitmap = null;
                }
                e.printStackTrace();
            }
            finally
            {
                try
                {
                    if (is != null)
                    {
                        is.close();
                        is = null;
                    }
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                
            }
        } while (false);
        return sourceBitmap;
    }
    
    /**
     * 计算缩放比
     * @param options
     * @param target
     * @return
     */
    public static int computeSampleSize(BitmapFactory.Options options, int target)
    {
        int w = options.outWidth;
        int h = options.outHeight;
        // int candidateW = (int)Math.ceil((double)w/(double)target);
        // int candidateH = (int)Math.ceil((double)h/(double)target);
        int candidateW = w / target;
        int candidateH = h / target;
        
        int candidate = Math.max(candidateW, candidateH);
        
        // /**
        // * 压缩只对2的n次方起作用
        // */
        // int i=1;
        // while(i<candidate)
        // {
        // i=i*2;
        // }
        // candidate=i;
        if (candidate == 0)
            return 1;
        return candidate;
    }
    
    /**
     * 从文件中读取图片,引导页
     * 
     * @param picPath 文件存放路径
     */
    public static Bitmap readBitmapFromStream(String picPath)
    {
        Bitmap sourceBitmap = null;
        if (TextUtils.isEmpty(picPath))
        {
            sourceBitmap = null;
        }
        do
        {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(picPath, options);
            if (options.mCancel || options.outWidth == -1 || options.outHeight == -1)
            {
                return null;
            }
            options.inJustDecodeBounds = false;
            options.inDither = false;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            InputStream is = null;
            try
            {
                is = new FileInputStream(picPath);
                sourceBitmap = BitmapFactory.decodeStream(is, null, options);
                options = null;
                if (null == sourceBitmap)
                {
                    break;
                }
            }
            catch (OutOfMemoryError e)
            {
                try
                {
                    if (is != null)
                    {
                        is.close();
                        is = null;
                    }
                }
                catch (IOException ex)
                {
                    // TODO Auto-generated catch block
                    ex.printStackTrace();
                }
                if (null != sourceBitmap && !sourceBitmap.isRecycled())
                {
                    sourceBitmap.recycle();
                    sourceBitmap = null;
                }
                e.printStackTrace();
            }
            catch (FileNotFoundException e)
            {
                if (null != sourceBitmap && !sourceBitmap.isRecycled())
                {
                    sourceBitmap.recycle();
                    sourceBitmap = null;
                }
                e.printStackTrace();
            }
            finally
            {
                try
                {
                    if (is != null)
                    {
                        is.close();
                        is = null;
                    }
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        } while (false);
        return sourceBitmap;
    }
    
}
