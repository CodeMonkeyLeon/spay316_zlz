package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bean.QRCodeBean
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView


/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @当我痛苦的站在你面前
 * @你不能说我一无所有
 * @也不能说我两手空空
 */
class CodeListContract {

    interface View : BaseView {

        fun getCodeListSuccess(response: ArrayList<QRCodeBean?>?)

        fun getCodeListFailed(error: Any?)

    }

    interface Presenter : BasePresenter<View> {
        fun getCodeList(
            page: Int,
            bindUserId: String?
        )
    }


}