package cn.swiftpass.enterprise.ui.widget;

import android.app.TimePickerDialog;
import android.content.Context;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * 时间选择框
 * User: Administrator
 * Date: 13-12-30
 * Time: 下午5:13
 */
public class TimeDialog {
    public static void showTimeDialog(Context mContext, int defaultHour, int defaultMinute ,final OnSelectDateTimeListener listener){

        final Calendar c = Calendar.getInstance();
        TimePickerDialog dialog = new TimePickerDialog(
                mContext,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                        //c.setTimeInMillis(timePicker.get);

                        c.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        c.set(Calendar.MINUTE,minute);

                        listener.onCheck(c.getTimeInMillis());
                    }
                },
                defaultHour,
                defaultMinute,

                false
        );
        dialog.show();
    }

    public interface OnSelectDateTimeListener
    {
        public void onCheck(long time);
    }
}
