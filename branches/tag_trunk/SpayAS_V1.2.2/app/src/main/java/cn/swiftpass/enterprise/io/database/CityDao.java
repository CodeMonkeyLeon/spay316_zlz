package cn.swiftpass.enterprise.io.database;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import cn.swiftpass.enterprise.bussiness.model.City;

public class CityDao extends BaseDao
{
    private final static String TAG = CityDao.class.getCanonicalName();
    
    private static CityDao cityDao;
    
    public CityDao(Context context)
    {
        super(context);
        
    }
    
    /**
     * <功能详细描述>
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static CityDao getInstance(Context context)
    {
        if (cityDao == null)
        {
            cityDao = new CityDao(context);
            if (null != context)
            {
                context = context.getApplicationContext();
            }
        }
        return cityDao;
    }
    
    /**
     * 得到所以省份
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public List<City> getAllProvince()
    {
        List<City> list = new ArrayList<City>();
        String sql = "select * from region where region_id like '__0000'";
        Cursor c = dbHelper.getReadableDatabase().rawQuery(sql, null);
        Log.i(TAG, "sql-->" + sql);
        while (c.moveToNext())
        {
            String city = c.getString(c.getColumnIndex("name"));
            String number = c.getString(c.getColumnIndex("region_id"));
            City item = new City();
            item.setCity(city + "省");
            item.setNumber(number);
            list.add(item);
        }
        return list;
    }
    
    /*表名：tb_region
            描述：定义系统位置信息，包括省份，城市，地区名称合并在一起。
            比如：查询全部省份，根据数据结构定义，进行查询
            select * from tb_region where region_id like '__0000'
            查询广东省下面的城市
            select * from tb_region where region_id like '19__00' and  region_id   not like '%0000'
            查询深圳的区域
            select * from tb_region where region_id like '1903__' and  region_id   not like '%00'*/
    
    /**
     * 根据id 得到身份下所有的城市
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public List<City> getAllCityByID(String region_id)
    {
        List<City> list = new ArrayList<City>();
        String sql = "select * from region where region_id like '" + region_id + "__00' and region_id not like '%0000'";
        Log.i(TAG, "getAllCityByID() sql-->" + sql);
        Cursor c = dbHelper.getReadableDatabase().rawQuery(sql, null);
        while (c.moveToNext())
        {
            String city = c.getString(c.getColumnIndex("name"));
            City item = new City();
            item.setCity(city + "市");
            list.add(item);
        }
        return list;
    }
}
