package cn.swiftpass.enterprise.mymould.base;

import android.app.ActivityGroup;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.cxy.networklibrary.activity.IBaseActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.swiftpass.enterprise.mymould.base.mvp.BasePresenter;

public abstract class BaseActivity <P extends BasePresenter>extends ActivityGroup implements IBaseActivity {



    @Inject
    @Nullable
    protected P mPresenter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(this.getLayoutId());
        initView();
    }

    @Override
    protected void onDestroy() {
        if (mPresenter != null) {
            mPresenter.detachView();
        }
        super.onDestroy();
    }

    /**
     * 设置布局
     *
     * @return
     */
    public abstract int getLayoutId();

    /**
     * 初始化视图
     */
    public abstract void initView();

    /**
     * 用来辅助当前Activity上面的fragment与Rx生命周期绑定
     * 没有当前fragment，返回false，有的话，重写当前方法为true
     * @return
     */
    @Override
    public boolean isUseFragment() {
        return false;
    }


}
