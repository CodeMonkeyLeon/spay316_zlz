package cn.swiftpass.enterprise.ui.activity.scan;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import java.io.IOException;
import java.util.Timer;
import java.util.Vector;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bean.QRCodeBean;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.Camera.CameraManager;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.Decoding.CaptureActivityHandler;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.Decoding.InactivityTimer;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.ScanCodeCheckDialog;
import cn.swiftpass.enterprise.ui.widget.Zxing.ViewfinderView;
import cn.swiftpass.enterprise.ui.widget.dialog.CashierNoticeDialog;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.DisplayUtil;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 *扫码绑定
 *
 * @author Jamy
 */
public class ScanbindActivity extends BaseActivity implements Callback {

    private static final String TAG = ScanbindActivity.class.getSimpleName();

    private CaptureActivityHandler handler;

    private ViewfinderView viewfinderView;

    private boolean hasSurface;

    private Vector<BarcodeFormat> decodeFormats;

    private String characterSet;

    private InactivityTimer inactivityTimer;

    private MediaPlayer mediaPlayer;

    private boolean playBeep;

    private ScanCodeCheckDialog dialog;

    private static final float BEEP_VOLUME = 0.10f;

    private boolean vibrate;

    private Context mContext;

    private LinearLayout ly_back;

    private DialogInfo dialogs;

    private TextView tv_code_info;

    private SurfaceHolder surfaceHolder;

    SurfaceView surfaceView;

    static final String[] PERMISSIONS = new String[]{Manifest.permission.CAMERA};
    //    /**
//     * 判断是否需要检测，防止不停的弹框
//     */
    private boolean isNeedCheck = true;
    /**
     * 扫描识别的qrcodeId
     */
    private String qrcodeId = "";
    private String cashierdeskName = "";
    private DialogInfo dialogInfo;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_scan_bind);
        ly_back = getViewById(R.id.ly_back);
        MainApplication.listActivities.add(this);
        CameraManager.init(this);
        viewfinderView = findViewById(R.id.viewfinder_view);
        hasSurface = false;
        inactivityTimer = new InactivityTimer(this);
        tv_code_info = getViewById(R.id.tv_code_info);
        tv_code_info.setPadding(0, 0, 0, DisplayUtil.dip2Px(ScanbindActivity.this, 20));
        setLister();
        if (null != getIntent()){
            cashierdeskName = getIntent().getStringExtra("cashierDeskName");
        }
    }

    private void setLister() {
        ly_back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    /**
     * 申请权限结果的回调方法
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] paramArrayOfInt) {
        if (requestCode == PERMISSON_REQUESTCODE) {
            if (!verifyPermissions(paramArrayOfInt)) {
                showMissingPermissionDialog(ScanbindActivity.this);
                isNeedCheck = false;
            }
        }
    }

    /**
     * 显示提示信息
     *
     * @since 2.5.0
     */
    protected void showMissingPermissionDialog(Context context) {
        DialogInfo dialogInfo = new DialogInfo(context, null, getString(R.string.setting_permisson_camera), getStringById(R.string.title_setting), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                startAppSettings();
            }

            @Override
            public void handleCancleBtn() {
                finish();
            }
        }, null);
        dialogInfo.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                return keycode == KeyEvent.KEYCODE_BACK;
            }
        });
        DialogHelper.resize(context, dialogInfo);
        dialogInfo.show();
    }


    @SuppressWarnings("unchecked")
    @Override
    protected void onResume() {
        super.onResume();
        if (isNeedCheck && Build.VERSION.SDK_INT >= 23) {
            checkPermissions(PERMISSIONS);
        }

        surfaceView = findViewById(R.id.preview_view);
        surfaceHolder = surfaceView.getHolder();

        if (hasSurface) {
            initCamera(surfaceHolder, false);
        } else {
            surfaceHolder.addCallback(this);
            surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }
        decodeFormats = null;
        characterSet = null;

        playBeep = true;
        AudioManager audioService = (AudioManager) getSystemService(AUDIO_SERVICE);
        if (audioService.getRingerMode() != AudioManager.RINGER_MODE_NORMAL) {
            playBeep = false;
        }
        initBeepSound();
        vibrate = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
        CameraManager.get().closeDriver();
        if (!hasSurface) {
            SurfaceView surfaceView = findViewById(R.id.preview_view);
            SurfaceHolder surfaceHolder = surfaceView.getHolder();
            surfaceHolder.removeCallback(this);
        }
    }

    void closeCamera() {
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
    }

    @Override
    protected void onDestroy() {
        inactivityTimer.shutdown();
        super.onDestroy();
    }

    void restartCamera() {
        closeCamera();
        viewfinderView.setVisibility(View.VISIBLE);
        SurfaceView surfaceView = findViewById(R.id.preview_view);
        SurfaceHolder surfaceHolder = surfaceView.getHolder();
        initCamera(surfaceHolder, false);
    }

    private void initCamera(SurfaceHolder surfaceHolder, boolean isFirst) {
        try {
            CameraManager.get().openDriver(surfaceHolder, surfaceView);
        } catch (IOException ioe) {
            return;
        } catch (RuntimeException e) {
            return;
        }
        if (handler == null) {
            handler = new CaptureActivityHandler(this, decodeFormats, characterSet);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (!hasSurface) {
            hasSurface = true;
            initCamera(holder, false);
        }

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        hasSurface = false;

    }

    public ViewfinderView getViewfinderView() {
        return viewfinderView;
    }

    public Handler getHandler() {
        return handler;
    }

    public void drawViewfinder() {
        viewfinderView.drawViewfinder();
    }


    public void handleDecode(Result obj, Bitmap barcode) {
        inactivityTimer.onActivity();
        playBeepSoundAndVibrate();

        AlertDialog.Builder builder = new AlertDialog.Builder(ScanbindActivity.this);
        builder.setTitle(getString(R.string.public_cozy_prompt));
        builder.setPositiveButton(getString(R.string.btnOk), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                restartCamera();
            }
        });

        builder.setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.show();
    }

    private void initBeepSound() {
        if (playBeep && mediaPlayer == null) {
            // The volume on STREAM_SYSTEM is not adjustable, and users found it
            // too loud,
            // so we now play on the music stream.
            setVolumeControlStream(AudioManager.STREAM_MUSIC);
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setOnCompletionListener(beepListener);

            AssetFileDescriptor file = getResources().openRawResourceFd(R.raw.beep);
            try {
                mediaPlayer.setDataSource(file.getFileDescriptor(), file.getStartOffset(), file.getLength());
                file.close();
                mediaPlayer.setVolume(BEEP_VOLUME, BEEP_VOLUME);
                mediaPlayer.prepare();
            } catch (IOException e) {
                mediaPlayer = null;
            }
        }
    }

    private static final long VIBRATE_DURATION = 200L;

    private void playBeepSoundAndVibrate() {
        if (playBeep && mediaPlayer != null) {
            mediaPlayer.start();
        }
        if (vibrate) {
            Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
            vibrator.vibrate(VIBRATE_DURATION);
        }
    }

    /**
     * When the beep has finished playing, rewind to queue up another one.
     */
    private final OnCompletionListener beepListener = new OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            mediaPlayer.seekTo(0);
        }
    };

    @Override
    protected boolean isLoginRequired() {
        // TODO Auto-generated method stub
        return true;
    }

    public void submitData(final String code, boolean vibration) {
        if (vibration) {
            playBeepSoundAndVibrate();
        }
        if (TextUtils.isEmpty(code)){
            return;
        }
        //扫码激活
        if (code.startsWith("http") && code.contains("qrcodeId")) {
              String qrId = getQrcodeId(code);
              if (!TextUtils.isEmpty(qrId)){
                  qrcodeId =qrId;
                  getQRDetails();
              }
        }
    }

    /**
     * 获取识别的二维码详细信息
     */
    private void getQRDetails(){
        OrderManager.getInstance().BindCodeDetails(qrcodeId,cashierdeskName,new UINotifyListener<QRCodeBean>(){
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                loadDialog(ScanbindActivity.this, R.string.public_data_loading);
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                dismissLoading();
                if (checkSession()) {
                    return;
                }
                if (object != null) {
                    ScanbindActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toastDialog(ScanbindActivity.this, object.toString(), new NewDialogInfo.HandleBtn() {
                                @Override
                                public void handleOkBtn() {
                                    restartCamera();
                                }
                            });
                        }
                    });
                }
            }

            @Override
            public void onSucceed(QRCodeBean result) {
                super.onSucceed(result);
                dismissLoading();
                String MerName =getString(R.string.pay_merchant_name)+": "+MainApplication.mchName;
                String MerId =getString(R.string.merchant_id)+": "+MainApplication.merchantId;
                String cashierdesk =getString(R.string.cashier_desk)+": "+cashierdeskName;
                String Id =getString(R.string.qrcode_Id)+": "+result.Id;
                //支付宝专属码
                if (1 == result.qrType){
                      showCodeDetails(true, ScanbindActivity.this, MerName, MerId,
                              cashierdesk, Id);
                }else{
                    showCodeDetails(false, ScanbindActivity.this, MerName, MerId,
                            cashierdesk, Id);
                }
            }
        });
    }


    /**
     * 绑定二维码
     */
    private CashierNoticeDialog NoticeDialog;
    private void BindCode(){
        OrderManager.getInstance().BindedCode(qrcodeId,cashierdeskName,new UINotifyListener<String>(){
            @Override
            public void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                dismissLoading();
                if (checkSession()) {
                    return;
                }
                if (object != null) {
                    ScanbindActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toastDialog(ScanbindActivity.this, object.toString(), new NewDialogInfo.HandleBtn() {
                                @Override
                                public void handleOkBtn() {
                                    restartCamera();
                                }
                            });
                        }
                    });
                }
            }

            @Override
            public void onSucceed(String result) {
                super.onSucceed(result);
                dismissLoading();
                if(NoticeDialog == null)
                {
                    NoticeDialog = new CashierNoticeDialog(ScanbindActivity.this);
                    NoticeDialog.setContentTextSize(16);
                    NoticeDialog.setContentText(R.string.bind_third_step_msg);
                    NoticeDialog.setConfirmText(R.string.bt_know);
                    NoticeDialog.setCanceledOnTouchOutside(false);
                    NoticeDialog.setCurrentListener(new CashierNoticeDialog.ButtonConfirmCallBack() {
                        @Override
                        public void onClickConfirmCallBack() {
                            if(NoticeDialog != null && NoticeDialog.isShowing()){
                                NoticeDialog.dismiss();
                                if (MainApplication.listActivities.size() > 0) {
                                    if (MainApplication.listActivities.contains(CodeListActivity.class.getSimpleName())){
                                        HandlerManager.notifyMessage(HandlerManager.BIND_CODE_FINISH, HandlerManager.BIND_CODE_FINISH, null);
                                        for (Activity a : MainApplication.listActivities) {
                                            if (!(a instanceof CodeListActivity)){
                                                a.finish();
                                            }
                                        }
                                    }else{
                                        for (Activity a : MainApplication.listActivities) {
                                            a.finish();
                                        }
                                        showPage(CodeListActivity.class);
                                    }
                                }

                            }
                        }
                    });
                }
                if(!NoticeDialog.isShowing())
                {
                    NoticeDialog.show();
                }
            }

        });
    }




    /**
     * 获取QrcodeId
     * @param code
     * @return
     */
    private String getQrcodeId(String code){
        String [] strs = code.split("qrcodeId=");
        int len = strs.length;
        if (!TextUtils.isEmpty(strs[len-1])){
            return strs[len-1];
        }else{
            return null;
        }
    }


    public void showCodeDetails(boolean isAlipay,final Activity context,String MerName,String MerId,
                                String deskName,String QrId) {
        dialogInfo = new DialogInfo(isAlipay,context,getString(R.string.public_cozy_prompt),MerName, MerId,
                deskName,QrId, getString(R.string.cashier_add_pwd_confirm), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                 BindCode();
            }

            @Override
            public void handleCancleBtn() {
                dialogInfo.cancel();
                restartCamera();

            }
        }, null);
        dialogInfo.setBtnOkTextColor(this.getResources().getColor(R.color.cashier_add));
        DialogHelper.resize(context, dialogInfo);
        dialogInfo.show();
    }

}
