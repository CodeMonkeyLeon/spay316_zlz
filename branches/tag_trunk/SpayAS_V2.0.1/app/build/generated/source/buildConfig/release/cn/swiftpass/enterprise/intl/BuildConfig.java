/**
 * Automatically generated file. DO NOT MODIFY
 */
package cn.swiftpass.enterprise.intl;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "cn.swiftpass.enterprise.intl";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 34;
  public static final String VERSION_NAME = "2.0.1";
  // Fields from build type: release
  public static final boolean IS_POS_VERSION = false;
}
