package cn.swiftplus.enterprise.printsdk.print.TradelinkA8.printUtils;

import android.content.Context;
import android.os.Looper;
import android.text.TextUtils;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import hk.com.tradelink.tess.android.Printable;
import hk.com.tradelink.tess.android.RemoteException;
import hk.com.tradelink.tess.android.api.CallHandle;
import hk.com.tradelink.tess.android.api.Remote;
import hk.com.tradelink.tess.android.teller.Invoke;
import hk.com.tradelink.tess.ecr.export.Printing;


/**
 * Created by aijingya on 2020/10/28.
 *
 * @Package cn.swiftplus.enterprise.printsdk.print.TradelinkA8.printUtils
 * @Description:
 * @date 2020/10/28.16:53.
 */
public class PrintTradelinkA8Text {
    private static PrintTradelinkA8Text printTradelinkA8Text;
    private Context mContext;
    private List<Printable.Step> list;

    // Service binding
    private Remote api = null;
    // Request handle
    private CallHandle handle = null;


    public static PrintTradelinkA8Text getInstance(){
        if(printTradelinkA8Text == null){
            printTradelinkA8Text = new PrintTradelinkA8Text();
        }
        return printTradelinkA8Text;
    }

    public void bindDeviceService(Context context) {
        this.mContext = context;
        api = Invoke.from(mContext);
    }

    public void unbindDeviceService() {
        if (handle != null){
            handle.detach();
        }

        //  Unbind the service to avoid memory leak
        if (api != null){
            api.unbind();
        }
    }

    public void init(Context context){
        this.mContext = context;
        list = new ArrayList<>();
    }


    public void  startPrint(){
        if (handle == null) {
            //  TODO    Start a request via API binder & wrap into CallHandle
            handle = CallHandle.run(api.print(new Printable(list.toArray(new Printable.Step[list.size()]))),
                    Looper.getMainLooper(),
                    new CallHandle.SuccessListener() {
                        @Override
                        public void onFinish() {
                            Toast.makeText(mContext, "Print success", Toast.LENGTH_SHORT).show();
                            handle = null;
                            list.clear();
                        }
                    },
                    new CallHandle.ErrorListener() {
                        @Override
                        public void onError(Throwable e) {
                            if (e instanceof RemoteException) {
                                Toast.makeText(mContext, String.format(Locale.US, "Print fail [%x]", ((RemoteException) e).code), Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(mContext, String.format(Locale.US, "Print fail [%s]", e), Toast.LENGTH_SHORT).show();

                            }
                            handle = null;
                            list.clear();
                        }
                    }
            );
        }
    }



    public void printTitle(String text){
        list.add(new Printable.Strings(new String[]{text}, Printing.AUTO_WRAP|Printing.ALIGN_CENTER |Printing.SIZE_MEDIUM));
        list.add(new Printable.Lines(1));
    }

    public void printTextLine(String text,int align){
        list.add(new Printable.Strings(new String[]{text}, Printing.AUTO_WRAP|align));
    }

    public void printKeyValue(String key,String value){
        list.add(new Printable.KeyValue(key, value));
    }

    public void printMultiLines(String text){
        list.add(new Printable.Strings(new String[]{text}, Printing.AUTO_WRAP|Printing.ALIGN_START ));
    }

    public void printSingleLine(){
        list.add(new Printable.Lines(1, '-'));
    }

    public void printDoubleLine(){
        list.add(new Printable.Lines(1, '='));
    }

    public void printQRCode(String orderNoMch){
        if(!TextUtils.isEmpty(orderNoMch)){
            list.add(new Printable.Strings(new String[]{orderNoMch}, Printing.CODE_QR | Printing.ALIGN_CENTER));
        }
    }

    public void printEmptyLine(){
        list.add(new Printable.Lines(1));
    }

}
