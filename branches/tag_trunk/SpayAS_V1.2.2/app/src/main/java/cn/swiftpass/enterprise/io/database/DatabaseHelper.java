package cn.swiftpass.enterprise.io.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import cn.swiftpass.enterprise.io.database.migrations.Migration_Init;
import cn.swiftpass.enterprise.utils.AppHelper;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper
{
    private static final int DATABASE_VERSION = 71;
    
    private static final String DATABASE_NAME = "spad.db";
    
    public DatabaseHelper(Context context)
    {
        //super(context, DATABASE_NAME, null, DATABASE_VERSION);
        super(context, AppHelper.getDBPath(false, DATABASE_NAME), null, DATABASE_VERSION);
    }
    
    /*
     * public DatabaseHelper(Context context, String name, CursorFactory
     * factory, int version) { super(context, name, factory, version); }
     */
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connSource)
    {
        try
        {
            Migration_Init.createTables(db);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            logger.error(e, "创建数据库失败");
        }
    }
    
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connSource, int oldVersion, int newVersion)
    {
        try
        {
            // TODO ...
            new Migration_Init().excute(db, newVersion, oldVersion);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            logger.error(e, "更新数据库失败");
        }
    }
}
