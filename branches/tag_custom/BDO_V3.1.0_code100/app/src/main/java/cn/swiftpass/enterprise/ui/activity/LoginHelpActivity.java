package cn.swiftpass.enterprise.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewStub;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.user.FindPassFirstActivity;

/**
 * Created by congwei.li on 2021/10/29.
 *
 * @Description: 登录 need help界面
 */
public class LoginHelpActivity extends BaseActivity implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_help);

        MainApplication.listActivities.add(this);

        TextView forgetEmail = findViewById(R.id.tv_login_help_forget_email);
        TextView forgetPwd = findViewById(R.id.tv_login_help_forget_pwd);
        ViewStub vsLeft = findViewById(R.id.vs_leftview);
        RelativeLayout leftButton = findViewById(R.id.leftButtonLay);
        TextView title = findViewById(R.id.tvTitle);

        title.setText(getString(R.string.bdo_need_help));
        vsLeft.setVisibility(View.VISIBLE);
        forgetEmail.setOnClickListener(this);
        forgetPwd.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_login_help_forget_email:
                showPage(ForgetEmailActivity.class);
                break;
            case R.id.tv_login_help_forget_pwd:
                showPage(FindPassFirstActivity.class);
                break;
        }
    }
}
