package cn.swiftpass.enterprise.ui.activity;

import java.io.File;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.download.DownloadTask;
import cn.swiftpass.enterprise.bussiness.logica.download.inteferace.DownloadTaskListener;
import cn.swiftpass.enterprise.bussiness.model.DownloadInfo;
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.utils.AbstractBus;
import cn.swiftpass.enterprise.utils.ApkUtil;
import cn.swiftpass.enterprise.utils.CleanManager;
import cn.swiftpass.enterprise.utils.FileUtils;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.MessageBus;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;
import cn.swiftpass.enterprise.utils.Utils;

public class UpgradeDailog extends Dialog {
    public Activity mContext;

    private ViewGroup mRootView;

    private UpdateListener updateListener;

    private TextView tvProgress, tvTotal, tvCurrent;

    private ProgressBar pbProgress;

    private Button btn;

    //private UpgradeInfo update;

    private DownloadInfo info;

    private DownloadTask task;

    /**
     * 下载总数消息
     */
    public static final int MSG_LENGTH = 0x8801;

    /**
     * 已下载数消息
     */
    public static final int MSG_DONE = 0x8802;

    /**
     * 下载完成
     */
    public static final int MSG_OK = 0x8803;

    /**
     * 下载失败
     */
    public static final int MSG_FAIL = 0x8804;

    private SharedPreferences sp;

    public UpgradeDailog(Activity context, UpgradeInfo update, final UpdateListener updateListener) {
        super(context);
        this.mContext = context;
        this.updateListener = updateListener;
        //this.update = update;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_upgrade, null);
        setContentView(mRootView);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        this.setCanceledOnTouchOutside(false);
        tvProgress = (TextView) mRootView.findViewById(R.id.tvProgress);
        tvTotal = (TextView) mRootView.findViewById(R.id.tvTotal);
        tvCurrent = (TextView) mRootView.findViewById(R.id.tvCurrentSzie);
        sp = MainApplication.getContext().getApplicationPreferences();
        pbProgress = (ProgressBar) mRootView.findViewById(R.id.pbProgress);
        btn = (Button) mRootView.findViewById(R.id.cancel);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        mBus = MessageBus.getBusFactory();
        //startDownload(update);
        // 任务列表添加
        info = new DownloadInfo();
        info.mDownExtend1 = "";
        info.mDownName = ApiConstant.APK_NAME;
        info.downloadId = update.download;
        if (!TextUtils.isEmpty(update.url)) {
            info.mDownUrl = update.url;
        } else {
            info.mDownUrl = ApiConstant.DOWNLOAD_APP_URL + "?bankCode=" + ApiConstant.bankCode;
        }
        // 兴业银行
        //        if (null != ApiConstant.bankType && !"".equals(ApiConstant.bankType) && ApiConstant.bankType == 1)
        //        {
        //        }
        //        else
        //        {
        //            info.mDownUrl = ApiConstant.DOWNLOAD_APP_URL;
        //        }
        task = newDownloadTask(info);
        task.execute();
        MessageBus.getBusFactory().register(MSG_LENGTH, receiver);
        MessageBus.getBusFactory().register(MSG_DONE, receiver);
        MessageBus.getBusFactory().register(MSG_OK, receiver);
        MessageBus.getBusFactory().register(MSG_FAIL, receiver);
    }

    /**
     * 消息到达接收器
     */
    AbstractBus.Receiver<MessageBus.MMessage> receiver = new MessageBus.UIReceiver() {
        private static final long serialVersionUID = 8434181217182948405L;

        @Override
        public void onReceive(MessageBus.MMessage message) {
            switch (message.what) {
                case MSG_LENGTH:
                    int totolSize = message.arg1;
                    tvTotal.setText("总大小：" + Utils.formatByte(totolSize));
                    break;
                case MSG_DONE:
                    int done = message.arg1;
                    int totle = message.arg2;
                    int nProgress = (int) (((done + 0.0f) / totle) * 100);
                    //	int nProgress = ((int) done * 100 / (int) totle);
                    tvTotal.setText("总大小：" + Utils.formatByte(totle));
                    tvCurrent.setText("当前大小：" + Utils.formatByte(done));
                    tvProgress.setText("正在更新下载进度： " + nProgress + "%");
                    pbProgress.setProgress(nProgress);
                    break;
                case MSG_OK:
                    //下载完成进行安装
                    String appPath = FileUtils.defaultDownloadPath + ApiConstant.APK_NAME;
                    ApkUtil.onInstallApk(mContext, appPath);
                    //这里记录安装版本，下次如果没有更新就删除原来的文件
                    sp.edit().putString(GlobalConstant.FIELD_UPGRADE_PATH, appPath).commit();
                    dismiss();

                    // 清除sharedPreferences缓存信息
                    CleanManager.cleanSharedPreference(mContext, "payIconId");
                    CleanManager.cleanSharedPreference(mContext, "payMethStr");
                    PreferenceUtil.removeKey("update");
                    //通知服务器下载成功
                    //                    UpgradeManager.getInstance().submitDownloadCount(update.version, new UINotifyListener<Boolean>()
                    //                    {
                    //                    });

                    break;
                case MSG_FAIL:
                    ToastHelper.showInfo("下载失败！");
                    dismiss();

                    //LoginActivity.startActivity(mContext);
                    mContext.finish();
                    break;

            }
        }
    };

    @Override
    public void dismiss() {
        task.cancel();
        if (updateListener != null) {
            updateListener.cancel();
        }
        super.dismiss();
        //        mContext.finish();
    }

    public interface UpdateListener {
        public void cancel();
    }

    /**
     * Create a new download task with default config
     */
    private DownloadTask newDownloadTask(DownloadInfo info) {
        Logger.d("newDownloadTask " + info.mDownUrl);
        DownloadTask task = new DownloadTask(mContext, info.mDownUrl, FileUtils.defaultDownloadPath, info.downloadId, info.mDownName + info.mDownExtend1, taskListener);
        return task;
    }

    private DownloadTaskListener taskListener = new DownloadTaskListener() {
        @Override
        public void updateProcess(DownloadTask task) {

            doDone((int) task.getDownloadSize(), (int) task.getTotalSize());
        }

        @Override
        public void preDownload(DownloadTask task) {
            doLength((int) task.getTotalSize());
        }

        @Override
        public void downloadSuccess(DownloadTask task) {
            doOk();
        }

        @Override
        public void downloadError(DownloadTask task, Throwable error) {

            doFail();
        }

        @Override
        public void postDownload(DownloadTask task) {
            // 下载线程真正停止了才移除
            //mDownloadTasks.remove(task.getDownloadId());

        }

    };

    private MessageBus mBus;

    /**
     * 报告失败
     */
    private void doFail() {
        MessageBus.MMessage msg = mBus.createMessage(MSG_FAIL);
        MessageBus.getBusFactory().send(msg);
        // 下载失败时，通知其它子线程终止下载， 即停止下载
        //stop();
        task.cancel();

    }

    private void doLength(int fileLen) {
        MessageBus.MMessage msg = mBus.createMessage(MSG_LENGTH);
        msg.arg1 = fileLen;
        mBus.send(msg);
        //删除上一个版本的记录文件
        String appPath = sp.getString(GlobalConstant.FIELD_UPGRADE_PATH, "");
        File file = new File(appPath);
        if (file != null && file.isFile()) {

            boolean isSuccess = file.delete();
            //TODO FIX ZHANGXINCHAO 主要是解决扫描漏洞 不处理返回值的问题
            if (isSuccess) {

            } else {

            }
        }
    }

    private void doDone(int currentSize, int total) {
        MessageBus.MMessage msg = mBus.createMessage(MSG_DONE);
        msg.arg1 = currentSize;
        msg.arg2 = total;
        mBus.send(msg);
    }

    private void doOk() {
        MessageBus.MMessage msg = mBus.createMessage(MSG_OK);
        mBus.send(msg);
    }
}
