package cn.swiftplus.enterprise.printsdk.print.N5;

import android.content.Context;
import com.nexgo.oaf.apiv3.APIProxy;
import com.nexgo.oaf.apiv3.DeviceEngine;
import com.nexgo.oaf.smartpos.jni.SmartPOSJni;

import org.scf4a.ConnSession;

/**
 * Created by aijingya on 2020/10/27.
 *
 * @Package cn.swiftplus.enterprise.printsdk.print.N5
 * @Description:
 * @date 2020/10/27.11:10.
 */
public class POSN5Client {
    private static POSN5Client sPosN5Client;
    private static DeviceEngine deviceEngine;
    private Context mContext;

    public static void initN5(Context context){
        if(sPosN5Client != null){
            throw new RuntimeException("PosN5Client Already initialized");
        }
        sPosN5Client = new POSN5Client(context);
    }

    public static POSN5Client getInstance(){
        if (sPosN5Client == null) {
            throw new RuntimeException("PosN5Client is not initialized");
        }
        return sPosN5Client;
    }

    private POSN5Client(Context context){
        mContext = context;
        //N5 pos 打印机初始化
        ConnSession.getInstance();
        SmartPOSJni.getInstance();
        deviceEngine = APIProxy.getDeviceEngine();
    }

    public static DeviceEngine getDeviceEngine() {
        return deviceEngine;
    }

    public static void setDeviceEngine(DeviceEngine deviceEngine) {
        POSN5Client.deviceEngine = deviceEngine;
    }
}
