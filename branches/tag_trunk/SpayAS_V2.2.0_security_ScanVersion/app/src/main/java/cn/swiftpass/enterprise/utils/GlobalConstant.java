package cn.swiftpass.enterprise.utils;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-9-20
 * Time: 下午7:40
 * To change this template use File | Settings | File Templates.
 */
public class GlobalConstant
{
    
    public static boolean isDebug = true;
    
    public static final String FILE_CACHE_ROOT = "/swiftpass_pay/";
    
    public static final String FILE_LOG_DIR = "/logs/";
    
    /**********************http**************************************/
    public static String cookitValue;
    
    public static String IMEIValue;
    
    public static String IMSIValue;
    
    public static String netTypeValue;
    
    public static String androidVersion;
    
    /**********************Field**************************************/
    public static int DEFAULT_TIMEOUT = 35000;
    
    /**********************Field**************************************/
    public static String FIELD_USERNAME = "f_userName";
    
    public static String FIELD_PAWESSORD = "f_password";
    
    public static String FIELD_MID = "f_mid";
    
    public static String FIELD_ID = "f_id";
    
    public static String FIELD_DERVICE_ID = "f_dervice_id";
    
    public static String FIELD_MERCHANT_ID = "f_merchant_id";
    
    /**********************Field**************************************/
    public static String FIELD_UPGRADE_PATH = "f_upgrade_path";
    
    public static String FILE_SERVER_PORT = "port";
    
    public static String FILE_IP_ADDRESS = "ip_address";
    
    /****************************************检测版本时间********************************/
    public static String FILED_T_CHECK_VERSION = "f_time_check_version";
    
    public static String FIELD_FIRST_IS_FIRST = "f_isFirst";//是否第一次启动
    
    public static String FIELD_IS_FRIST_WELCOME = "f_first_welcome";//欢迎界面第一次
    
    /****************************************必须升级********************************/
    public static String FILED_T_CHECK_VERSION_MUST_UPDATE = "f_time_check_must_update";
    
    /*********************************测试********************************/
    public static String FILED_T_USERNAME = "f_t_username";

    
    public static String FILED_T_MID = "f_t_mid";
    
    /***********************************微信分享**************************/
    public static final String WX_APP_ID = "wx14ae73ed2f71f0e4";
    
    public static String FIELD_IS_OK_CLAUSE = "f_isOk_clause";
    
    //各种验证
    public static final int MOBILEPHOE = 0;
    
    public static final int IDCARD = 1;
    
    public static final int TELPHONE = 2;
    
    public static final int BANKNUM = 3;
    
    public static final int EMAIL = 4;
    
    public static final int STR = 5;
    
    public static final int CHINA = 6;
}
