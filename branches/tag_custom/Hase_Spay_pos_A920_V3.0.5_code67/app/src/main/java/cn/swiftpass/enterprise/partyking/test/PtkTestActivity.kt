package cn.swiftpass.enterprise.partyking.test

import android.os.Bundle
import android.view.View
import android.widget.Button
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.partyking.view.PartyKingDetailsActivity
import cn.swiftpass.enterprise.partyking.view.PartyKingSummaryActivity
import cn.swiftpass.enterprise.ui.activity.TemplateActivity
import cn.swiftpass.enterprise.ui.widget.TitleBar
import cn.swiftpass.enterprise.utils.Logger
import cn.swiftpass.enterprise.utils.OnProhibitFastClickListener

class PtkTestActivity : TemplateActivity() {

    private fun getLayoutId() = R.layout.act_ptk_test

    companion object {
        const val TAG = "PtkTestActivity"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        initView()
    }

    private fun initView() {
        //核销确认
        R.id.btn_write_off_confirm.setOnClick {
            PartyKingDetailsActivity.startPtkDetailsActivity(
                activity,
                TestDataUtils.getWriteOffConfirmTestData(),
                getString(R.string.string_pk_details_title)
            )
        }

        //不可核销
        R.id.btn_can_not_write_off.setOnClick {
            PartyKingDetailsActivity.startPtkDetailsActivity(
                activity,
                TestDataUtils.getCanNotWriteOffTestData(),
                getString(R.string.string_pk_details_title)
            )
        }

        //核销成功
        R.id.btn_write_off_success.setOnClick {
            PartyKingDetailsActivity.startPtkDetailsActivity(
                activity,
                TestDataUtils.getWriteOffSuccessTestData(),
                getString(R.string.string_pk_details_title)
            )
        }

        //核销失败
        R.id.btn_write_off_fail.setOnClick {
            PartyKingDetailsActivity.startPtkDetailsActivity(
                activity,
                TestDataUtils.getWriteOffFailTestData(),
                getString(R.string.string_pk_details_title)
            )
        }

        //核销超时
        R.id.btn_write_off_timeout.setOnClick {
            PartyKingDetailsActivity.startPtkDetailsActivity(
                activity,
                TestDataUtils.getWriteOffTimeoutTestData(),
                getString(R.string.string_pk_details_title)
            )
        }

        //撤回成功
        R.id.btn_rollback_success.setOnClick {
            PartyKingDetailsActivity.startPtkDetailsActivity(
                activity,
                TestDataUtils.getRollbackSuccessTestData(),
                getString(R.string.string_pk_details_title)
            )
        }


        //PTK 汇总
        R.id.btn_ptk_summary.setOnClick {
            PartyKingSummaryActivity.startPtkSummaryActivity(
                activity,
                TestDataUtils.getPtkSummaryTestData()
            )
        }


        //核销记录成功
        R.id.btn_write_off_record_success.setOnClick {
            PartyKingDetailsActivity.startPtkDetailsActivity(
                activity,
                TestDataUtils.getWriteOffRecordSuccessTestData(),
                getString(R.string.string_pk_item_details_title),
                getString(R.string.string_pk_item_details_rollback)
            )
        }


        //核销记录失败
        R.id.btn_write_off_record_fail.setOnClick {
            PartyKingDetailsActivity.startPtkDetailsActivity(
                activity,
                TestDataUtils.getWriteOffRecordFailTestData(),
                getString(R.string.string_pk_item_details_title)
            )
        }


        //核销记录超时
        R.id.btn_write_off_record_timeout.setOnClick {
            PartyKingDetailsActivity.startPtkDetailsActivity(
                activity,
                TestDataUtils.getWriteOffRecordTimeoutTestData(),
                getString(R.string.string_pk_item_details_title)
            )
        }


        //一个按钮的Dialog
        R.id.btn_one_button_dialog.setOnClick {
            showOneButtonDialog("Unknown QR Code", false, "", "OK")
        }

        //两个按钮的Dialog
        R.id.btn_two_button_dialog.setOnClick {
            showTwoButtonDialog(
                "Rollback Redemption",
                "Confirm to rollback the redemption?",
                "Confirm",
                "Cancel"
            )
        }


    }


    override fun setupTitleBar() {
        super.setupTitleBar()
        titleBar.setLeftButtonVisible(true)
        titleBar.setRightButLayVisible(true, "right")
        titleBar.setTitle("PTK TEST")
        titleBar.setOnTitleBarClickListener(object : TitleBar.OnTitleBarClickListener {
            override fun onLeftButtonClick() {
                //返回
                finish()
            }

            override fun onRightButtonClick() {
                Logger.d(TAG, "------ Right  Button  Click")
            }

            override fun onRightLayClick() {
                Logger.d(TAG, "------ Right  Lay  Click")
            }

            override fun onRightButLayClick() {
                Logger.d(TAG, "------ Right  ButLay  Click")
            }
        })
    }


    private fun Int.setOnClick(action: (view: View?) -> Unit) {
        findViewById<Button>(this).setOnClickListener(object : OnProhibitFastClickListener() {
            override fun onFilterClick(v: View?) {
                action(v)
            }
        })
    }


}