package cn.swiftpass.enterprise.ui.paymentlink.model;

import android.text.TextUtils;

import java.io.Serializable;

public class PaymentLinkCustomer implements Serializable {
    public String custId = "";
    public String custName = "";
    public String custMobile = "";
    public String custEmail = "";
    public String merchantId = "";
    public String merchantName = "";
    public String parentMerchant = "";
    public String userType = "";

    public boolean isSelected;

    public boolean isAllFilled() {
        return !TextUtils.isEmpty(custName)
//                && !TextUtils.isEmpty(custMobile)
//                && !TextUtils.isEmpty(custEmail)
                ;
    }

    public boolean isEqual(PaymentLinkCustomer customer) {
        if (null == customer) {
            return false;
        }
        return custName.equals(customer.custName)
                && custMobile.equals(customer.custMobile)
                && custEmail.equals(customer.custEmail);
    }

    public void copyFrom(PaymentLinkCustomer customer) {
        if (null == customer) {
            return;
        }
        custId = customer.custId;
        custName = customer.custName;
        custMobile = customer.custMobile;
        custEmail = customer.custEmail;
    }

    @Override
    public String toString() {
        return "PaymentLinkCustomer{" +
                "custId='" + custId + '\'' +
                ", custName='" + custName + '\'' +
                ", custMobile='" + custMobile + '\'' +
                ", custEmail='" + custEmail + '\'' +
                ", merchantId='" + merchantId + '\'' +
                ", merchantName='" + merchantName + '\'' +
                ", parentMerchant='" + parentMerchant + '\'' +
                ", userType='" + userType + '\'' +
                ", isSelected=" + isSelected +
                '}';
    }
}
