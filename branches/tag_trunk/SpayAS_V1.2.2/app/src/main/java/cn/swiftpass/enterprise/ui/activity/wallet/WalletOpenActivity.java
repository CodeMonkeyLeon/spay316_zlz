/*
 * 文 件 名:  WalletMainActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-1
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.wallet;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.wallet.WalletManager;
import cn.swiftpass.enterprise.bussiness.model.WalletModel;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.total.PayDistributionActivity;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 开通电子钱包
 * 
 * @author  he_hui
 * @version  [版本号, 2016-11-1]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class WalletOpenActivity extends TemplateActivity
{
    
    private Button btn_next_step;
    
    private TextView tx_peop, tv_id_info;
    
    private EditText et_id;
    
    private ImageView iv_promt, iv_clean_input;
    
    private WalletModel walletModel;
    
    private String isUpdateFlag = "1";
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_apply_open_layout);
        
        initView();
        setLister();
        btn_next_step.getBackground().setAlpha(102);
        showSoftInputFromWindow(WalletOpenActivity.this, et_id);
        MainApplication.listActivities.add(this);
        
        //        loadisHaveEwallet();
        
        initDate();
        walletModel = (WalletModel)getIntent().getSerializableExtra("walletModel");
        if (null != walletModel)
        {
            MainApplication.isHasEwallet = walletModel.getIsHasEwallet();
            MainApplication.accountId = walletModel.getAccountId();
            if (!StringUtil.isEmptyOrNull(walletModel.getCardholder()))
            {
                //                        btn_next_step.setEnabled(true);
                tx_peop.setText(walletModel.getCardholder());
                MainApplication.cardholder = walletModel.getCardholder();
            }
            if (StringUtil.isEmptyOrNull(walletModel.getIdentityCard()))
            {
                et_id.setHint(R.string.ed_input_id);
                et_id.setFilters(new InputFilter[] {new InputFilter.LengthFilter(18)});
                isUpdateFlag = "2";
            }
            tv_id_info.setText(walletModel.getIdentityCard());
        }
    }
    
    public static void startActivity(Context context, WalletModel model)
    {
        Intent it = new Intent();
        it.setClass(context, WalletOpenActivity.class);
        it.putExtra("walletModel", model);
        context.startActivity(it);
    }
    
    private void initDate()
    {
        
    }
    
    @Override
    protected void onPause()
    {
        super.onPause();
        if (null != btn_next_step)
        {
            btn_next_step.setEnabled(true);
            //            btn_next_step.setText(R.string.reg_next_step);
            setButtonBg(btn_next_step, true, R.string.reg_next_step);
        }
    }
    
    @Override
    protected void onResume()
    {
        super.onResume();
    }
    
    /**
     * 验证身份证是否合法
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void checIdIsRigth(String id)
    {
        WalletManager.getInstance().ewalletIdentity(id, isUpdateFlag, new UINotifyListener<List<WalletModel>>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                btn_next_step.setText(R.string.reg_next_step_loading);
                btn_next_step.setEnabled(false);
                loadDialog(WalletOpenActivity.this, R.string.reg_next_step_loading);
            }
            
            @Override
            public void onPostExecute()
            {
                super.onPostExecute();
            }
            
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                dissDialog();
                if (checkSession())
                {
                    return;
                }
                WalletOpenActivity.this.runOnUiThread(new Runnable()
                {
                    
                    @Override
                    public void run()
                    {
                        //                        btn_next_step.setText(R.string.reg_next_step);
                        //                        btn_next_step.setEnabled(true);
                        setButtonBg(btn_next_step, true, R.string.reg_next_step);
                    }
                });
                
                if (checkSession())
                {
                    return;
                }
                if (null != object)
                {
                    toastDialog(WalletOpenActivity.this, object.toString(), null);
                }
            }
            
            @Override
            public void onSucceed(List<WalletModel> model)
            {
                dissDialog();
                //                btn_next_step.setText(R.string.reg_next_step);
                //                btn_next_step.setEnabled(true);
                setButtonBg(btn_next_step, true, R.string.reg_next_step);
                if (null != model && model.size() > 0)
                {
                    if (!StringUtil.isEmptyOrNull(isUpdateFlag))
                    {
                        //                        walletModel.setIsUpdateFlag(isUpdateFlag);
                        model.get(0).setIsUpdateFlag(isUpdateFlag);
                    }
                    model.get(0).setModifyidCards(et_id.getText().toString());
                    WalletUploadImageActivity.startActivity(WalletOpenActivity.this, model);
                }
            }
        });
    }
    
    /**
     * 加载是否开通电子钱包
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void loadisHaveEwallet()
    {
        WalletManager.getInstance().isHaveEwallet(new UINotifyListener<WalletModel>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                loadDialog(WalletOpenActivity.this, R.string.public_data_loading);
            }
            
            @Override
            public void onPostExecute()
            {
                super.onPostExecute();
            }
            
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                dissDialog();
                if (checkSession())
                {
                    return;
                }
                if (null != object)
                {
                    toastDialog(WalletOpenActivity.this, object.toString(), null);
                }
            }
            
            @Override
            public void onSucceed(WalletModel model)
            {
                dissDialog();
                if (null != model)
                {
                    walletModel = model;
                    MainApplication.isHasEwallet = model.getIsHasEwallet();
                    MainApplication.accountId = model.getAccountId();
                    if (!StringUtil.isEmptyOrNull(model.getCardholder()))
                    {
                        //                        btn_next_step.setEnabled(true);
                        tx_peop.setText(model.getCardholder());
                    }
                    if (StringUtil.isEmptyOrNull(model.getIdentityCard()))
                    {
                        et_id.setHint(R.string.ed_input_id);
                    }
                    tv_id_info.setText(model.getIdentityCard());
                }
            }
        });
    }
    
    private void setLister()
    {
        iv_clean_input.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (walletModel != null)
                {
                    //如果是身份证已经实名了，只能修改后6位
                    if (!StringUtil.isEmptyOrNull(walletModel.getCheckAuth()))
                    {
                        
                        switch (Integer.parseInt(walletModel.getCheckAuth()))
                        {
                            case 1://实名
                                et_id.setText("");
                                break;
                            case 2://没有实名
                                et_id.setText("");
                                tv_id_info.setVisibility(View.GONE);
                                et_id.setHint(R.string.ed_input_id);
                                isUpdateFlag = "2";
                                et_id.setFilters(new InputFilter[] {new InputFilter.LengthFilter(18)});
                                break;
                            default:
                                break;
                        }
                        
                    }
                    else
                    {
                        isUpdateFlag = "2";
                    }
                }
            }
        });
        
        btn_next_step.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                
                if (StringUtil.isEmptyOrNull(et_id.getText().toString()))
                {
                    toastDialog(WalletOpenActivity.this, getString(R.string.tx_personal_id), null);
                    et_id.setFocusable(true);
                    return;
                }
                
                String tel = et_id.getText().toString();
                if (walletModel != null && StringUtil.isEmptyOrNull(walletModel.getIdentityCard()))
                {
                    if (tel.length() < 15)
                    {
                        toastDialog(WalletOpenActivity.this, getString(R.string.et_tel_error), null);
                        et_id.setFocusable(true);
                        return;
                    }
                    else if (tel.length() > 15 && tel.length() < 18)
                    {
                        toastDialog(WalletOpenActivity.this, getString(R.string.et_tel_error), null);
                        et_id.setFocusable(true);
                        return;
                    }
                    
                }
                else
                {
                    
                    if (tel.length() < 6)
                    {
                        toastDialog(WalletOpenActivity.this, getString(R.string.et_id_info), null);
                        et_id.setFocusable(true);
                        return;
                    }
                }
                
                if (isUpdateFlag.equals("2"))
                {
                    if (tel.length() < 15)
                    {
                        toastDialog(WalletOpenActivity.this, getString(R.string.et_tel_error), null);
                        et_id.setFocusable(true);
                        return;
                    }
                    else if (tel.length() > 15 && tel.length() < 18)
                    {
                        toastDialog(WalletOpenActivity.this, getString(R.string.et_tel_error), null);
                        et_id.setFocusable(true);
                        return;
                    }
                }
                checIdIsRigth(tel);
                
                //                showPage(WalletOpenNextActivity.class);
                
            }
        });
        
        iv_promt.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                NewDialogInfo info =
                    new NewDialogInfo(WalletOpenActivity.this, getString(R.string.bt_dialog_promt), null, 2,
                        getString(R.string.tx_card_info), new NewDialogInfo.HandleBtn()
                        {
                            
                            @Override
                            public void handleOkBtn()
                            {
                            }
                            
                        });
                
                DialogHelper.resize(WalletOpenActivity.this, info);
                
                info.show();
            }
        });
    }
    
    private void initView()
    {
        btn_next_step = getViewById(R.id.btn_next_step);
        tx_peop = getViewById(R.id.tx_peop);
        et_id = getViewById(R.id.et_id);
        iv_promt = getViewById(R.id.iv_promt);
        iv_clean_input = getViewById(R.id.iv_clean_input);
        tv_id_info = getViewById(R.id.tv_id_info);
        setPricePoint(et_id);
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        {
            
            @Override
            public void onExecute(CharSequence s, int start, int before, int count)
            {
            }
            
            @Override
            public void onAfterTextChanged(Editable s)
            {
                if (et_id.isFocused())
                {
                    if (et_id.getText().toString().length() > 0)
                    {
                        iv_clean_input.setVisibility(View.VISIBLE);
                        btn_next_step.setEnabled(true);
                        btn_next_step.setBackgroundResource(R.drawable.btn_register);
                        btn_next_step.setTextColor(getResources().getColor(R.color.white));
                        
                    }
                    else
                    {
                        iv_clean_input.setVisibility(View.GONE);
                        btn_next_step.setEnabled(false);
                        btn_next_step.setBackgroundResource(R.drawable.general_button_main_default);
                        btn_next_step.setTextColor(getResources().getColor(R.color.bt_enable));
                        btn_next_step.getBackground().setAlpha(102);
                    }
                    
                }
            }
        });
        et_id.addTextChangedListener(editTextWatcher);
    }
    
    private void setPricePoint(final EditText editText)
    {
        editText.addTextChangedListener(new TextWatcher()
        {
            
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                //                if (s.toString().contains("."))
                //                {
                //                    if (s.length() - 1 - s.toString().indexOf(".") > 2)
                //                    {
                //                        s = s.toString().subSequence(0, s.toString().indexOf(".") + 3);
                //                        editText.setText(s);
                //                        editText.setSelection(s.length());
                //                    }
                //                }
                //                if (s.toString().trim().substring(0).equals("."))
                //                {
                //                    s = "0" + s;
                //                    editText.setText(s);
                //                    editText.setSelection(2);
                //                }
                //                
                //                if (s.toString().startsWith("0") && s.toString().trim().length() > 1)
                //                {
                //                    if (!s.toString().substring(1, 2).equals("."))
                //                    {
                //                        editText.setText(s.subSequence(0, 1));
                //                        editText.setSelection(1);
                //                        return;
                //                    }
                //                }
                
                if (s.toString().length() > 0)
                {
                    btn_next_step.setEnabled(true);
                    btn_next_step.setBackgroundResource(R.drawable.btn_register);
                    btn_next_step.setTextColor(getResources().getColor(R.color.white));
                    
                }
                else
                {
                    btn_next_step.setEnabled(false);
                    btn_next_step.setBackgroundResource(R.drawable.general_button_grey_default);
                    btn_next_step.setTextColor(getResources().getColor(R.color.bt_enable));
                }
                
                Pattern idNumPattern = Pattern.compile("[\u4E00-\u9FA5]");
                Matcher m = idNumPattern.matcher(s.toString());
                
                //                if (VerifyUtil.verifyValid(s.toString(), GlobalConstant.STR) || m.find())
                //                {
                //                    s = s.toString().subSequence(0, s.toString().length() - 1);
                //                    editText.setText(s);
                //                    //                    editText.setSelection(s.length());
                //                    return;
                //                }
                
            }
            
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                
            }
            
            @Override
            public void afterTextChanged(Editable s)
            {
                // TODO Auto-generated method stub
                
            }
            
        });
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tv_id_check);
        titleBar.setLeftButtonIsVisible(true);
        
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                showPage(PayDistributionActivity.class);
            }
        });
    }
}
