/*
 * 文 件 名:  BluePrintUtil.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2017-4-20
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.print;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;

import com.example.common.sentry.SentryUtils;
import com.example.common.sp.PreferenceUtil;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalInfo;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalItemInfo;
import cn.swiftpass.enterprise.common.Constant;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.KotlinUtils;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;


/**
 * 打印模板
 *
 * @author he_hui
 * @version [版本号, 2017-4-20]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class BluePrintUtil {
    private static final String TAG = BluePrintUtil.class.getSimpleName();
    /**
     * 打印纸一行最大的字节
     */
    private static final int LINE_BYTE_SIZE = 32;

    /**
     * 蓝牙UUID
     */
    public static final UUID SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    //private static StringBuffer sb = new StringBuffer();


    public static boolean canPrint() {
        try {
            DataOutputStream os = new DataOutputStream(MainApplication.getInstance().getBluetoothSocket().getOutputStream());
            byte[] data = byteArraysToBytes(new byte[][]{Cmd.ESCCmd.ESC_dollors_nL_nH, Cmd.ESCCmd.GS_exclamationmark_n, Cmd.ESCCmd.ESC_M_n, Cmd.ESCCmd.GS_E_n, Cmd.ESCCmd.ESC_line_n, Cmd.ESCCmd.FS_line_n, Cmd.ESCCmd.ESC_lbracket_n, Cmd.ESCCmd.GS_B_n, Cmd.ESCCmd.ESC_V_n, "".getBytes()});
            os.write(data, 0, data.length);
            os.flush();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * 打印日结小票
     * <功能详细描述>fis.skipBytes
     *
     * @see [类、类#方法、类#成员]
     */
    public static void printDateSum(OrderTotalInfo info) {
        if (BuildConfig.IS_POS_VERSION) {
            return;
        }
        if (MainApplication.getInstance().getBluetoothSocket() == null) return;
        DataOutputStream os = null;
        try {
            os = new DataOutputStream(MainApplication.getInstance().getBluetoothSocket().getOutputStream());
            if (os == null) {
                return;
            }
        } catch (IOException e) {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException io) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            io,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(e));
                }

            }
            Log.e(TAG, Log.getStackTraceString(e));
        }
        StringBuffer printBuffer = new StringBuffer();
        StringBuffer titlebuffer = new StringBuffer();
        titlebuffer.append("\n           " + ToastHelper.toStr(R.string.tx_blue_print_data_sum) + "\n");
        POS_S_TextOut(titlebuffer.toString(), "gbk", 0, 0, 1, 0, 0, os);
        printBuffer.append("\n" + ToastHelper.toStr(R.string.shop_name) + "：" + "\n" + MainApplication.getInstance().getMchName() + "\n");
        printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "：" + MainApplication.getInstance().getMchId() + "\n");

        if (StringUtil.isEmptyOrNull(info.getUserName())) {
            //            printBuffer.append("收银员：全部收银员\n");
        } else {
            printBuffer.append(ToastHelper.toStr(R.string.tx_user) + "：" + info.getUserName() + "\n");
        }
        printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_start_time) + "：" + info.getStartTime() + "\n");
        printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_end_time) + "：" + info.getEndTime() + "\n");
        printBuffer.append("===============================\n");
        POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);
        if (MainApplication.getInstance().getUserInfo().feeFh.equalsIgnoreCase("¥")
                && MainApplication.getInstance().getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {

            setingPrint(ToastHelper.toStr(R.string.tx_pay_money) + "：", DateUtil.formatMoneyUtils(info.getCountTotalFee()) + ToastHelper.toStr(R.string.pay_yuan) + "\n", os);
        } else {

            setingPrint(ToastHelper.toStr(R.string.tx_pay_money) + "：", MainApplication.getInstance().getFeeType() + DateUtil.formatMoneyUtils(info.getCountTotalFee()) + "\n", os);

        }

        setingPrint(ToastHelper.toStr(R.string.tx_bill_stream_pay_money_num) + "：", info.getCountTotalCount() + "\n", os);

        if (MainApplication.getInstance().getUserInfo().feeFh.equalsIgnoreCase("¥")
                && MainApplication.getInstance().getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {

            setingPrint(ToastHelper.toStr(R.string.tv_refund_dialog_refund_money), DateUtil.formatMoneyUtils(info.getCountTotalRefundFee()) + ToastHelper.toStr(R.string.pay_yuan) + "\n", os);
        } else {

            setingPrint(ToastHelper.toStr(R.string.tv_refund_dialog_refund_money), MainApplication.getInstance().getFeeType() + DateUtil.formatMoneyUtils(info.getCountTotalRefundFee()) + "\n", os);
        }

        printBuffer.append(ToastHelper.toStr(R.string.tx_bill_reufun_total_num) + "：" + info.getCountTotalRefundCount() + "\n");

        setingPrint(ToastHelper.toStr(R.string.tx_bill_reufun_total_num) + "：", info.getCountTotalRefundCount() + "\n", os);
        if (MainApplication.getInstance().getUserInfo().feeFh.equalsIgnoreCase("¥")
                && MainApplication.getInstance().getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {//如果是人民币

            setingPrint(ToastHelper.toStr(R.string.tx_blue_print_pay_total) + "：", DateUtil.formatMoneyUtils((info.getCountTotalFee() - info.getCountTotalRefundFee())) + ToastHelper.toStr(R.string.pay_yuan) + "\n", os);

        } else {

            setingPrint(ToastHelper.toStr(R.string.tx_blue_print_pay_total) + "：", MainApplication.getInstance().getFeeType() + DateUtil.formatMoneyUtils((info.getCountTotalFee() - info.getCountTotalRefundFee())) + "\n", os);
        }
        POS_S_TextOut("===============================\n", "gbk", 0, 0, 0, 0, 0, os);

        /**
         * 是否开通小费功能，打印小费金额、总计
         */
        if (MainApplication.getInstance().isTipOpenFlag()) {
            // 小费金额
            setingPrint(ToastHelper.toStr(R.string.tip_amount) + "：", MainApplication.getInstance().getFeeType() + DateUtil.formatMoneyUtils(info.getCountTotalTipFee()) + "\n", os);
            //小费笔数
            setingPrint(ToastHelper.toStr(R.string.tip_count) + "：", info.getCountTotalTipFeeCount() + "\n", os);
            POS_S_TextOut("===============================\n", "gbk", 0, 0, 0, 0, 0, os);
        }

        List<OrderTotalItemInfo> list = info.getOrderTotalItemInfo();

        Map<Integer, Object> map = new HashMap<Integer, Object>();

        for (OrderTotalItemInfo itemInfo : list) {

            switch (itemInfo.getPayTypeId()) {

                case 1:
                    map.put(0, itemInfo);
                    break;
                case 2:
                    map.put(1, itemInfo);
                    break;

                case 4:
                    map.put(2, itemInfo);
                    break;
                case 12:
                    map.put(3, itemInfo);
                    break;
                default:
                    map.put(itemInfo.getPayTypeId(), itemInfo);
                    break;
            }

        }

        for (Integer key : map.keySet()) {
            OrderTotalItemInfo itemInfo = (OrderTotalItemInfo) map.get(key);
            if (MainApplication.getInstance().getUserInfo().feeFh.equalsIgnoreCase("¥")
                    && MainApplication.getInstance().getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {

                setingPrint(MainApplication.getInstance().getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId())) + ToastHelper.toStr(R.string.tx_money), DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + ToastHelper.toStr(R.string.pay_yuan) + "\n", os);
            } else {
                setingPrint(MainApplication.getInstance().getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId())) + "", "\n", os);
                setingPrint(ToastHelper.toStr(R.string.tx_money), MainApplication.getInstance().getFeeType() + DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "\n", os);
            }

            setingPrint(ToastHelper.toStr(R.string.tv_settle_count) + "：", itemInfo.getSuccessCount() + "\n", os);

        }
        POS_S_TextOut("-------------------------------\n", "gbk", 0, 0, 0, 0, 0, os);
        printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n");
        POS_S_TextOut(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n", "gbk", 0, 0, 0, 0, 0, os);
        printBuffer.append("\n" + ToastHelper.toStr(R.string.tx_blue_print_sign) + "：\n\n\n\n");
        POS_S_TextOut("\n" + ToastHelper.toStr(R.string.tx_blue_print_sign) + "：\n\n\n\n", "gbk", 0, 0, 0, 0, 0, os);
    }

    /**
     * 打印两列
     *
     * @param leftText  左侧文字
     * @param rightText 右侧文字
     * @return
     */
    @SuppressLint("NewApi")
    public static String printTwoData(String leftText, String rightText) {
        StringBuilder sb = new StringBuilder();
        int leftTextLength = getBytesLength(leftText);
        int rightTextLength = getBytesLength(rightText);
        sb.append(leftText);

        // 计算两侧文字中间的空格
        int marginBetweenMiddleAndRight = LINE_BYTE_SIZE - leftTextLength - rightTextLength;

        for (int i = 0; i < marginBetweenMiddleAndRight; i++) {
            sb.append(" ");
        }
        sb.append(rightText);
        return sb.toString();
    }

    /**
     * 打印设置
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    static void setingPrint(String leftText, String rightText, DataOutputStream os) {
        POS_S_TextOut(printTwoData(leftText, rightText), "gbk", 0, 0, 0, 0, 0, os);
    }

    /**
     * 获取数据长度
     *
     * @param msg
     * @return
     */
    private static int getBytesLength(String msg) {
        return msg.getBytes(Charset.forName("GBK")).length;
    }

    /**
     * 打印设置 对齐方式
     * <功能详细描述>
     *
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static void POS_S_Align(int align, DataOutputStream os) {
        if (align < 0 || align > 2) return;
        byte[] data = Cmd.ESCCmd.ESC_a_n;
        data[2] = (byte) align;
        write(data, 0, data.length, os);
    }

    @SuppressLint("NewApi")
    public static boolean blueConnent(String bluetooth) {
        try {
            BluetoothDevice btDev = null;
            BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBtAdapter != null) {
                if (!mBtAdapter.isEnabled()) {//蓝牙没有打开
                    return false;
                }

                Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();
                if (pairedDevices.size() > 0) {
                    for (BluetoothDevice device : pairedDevices) {
                        if (device.getAddress().equalsIgnoreCase(bluetooth)) {
                            btDev = device;
                        }
                    }
                } else {
                    return false;
                }

                if (btDev != null) {
                    BluetoothSocket bluetoothSocket = btDev.createRfcommSocketToServiceRecord(SPP_UUID);
                    if (bluetoothSocket != null) {
                        MainApplication.getInstance().setBluetoothSocket(bluetoothSocket);
                        if (mBtAdapter.isDiscovering())
                            //停止搜索
                            mBtAdapter.cancelDiscovery();
                        //如果当前socket处于非连接状态则调用连接
                        if (!bluetoothSocket.isConnected()) {
                            //你应当确保在调用connect()时设备没有执行搜索设备的操作。
                            // 如果搜索设备也在同时进行，那么将会显著地降低连接速率，并很大程度上会连接失败。
                            try {
                                bluetoothSocket.connect();

                                MainApplication.getInstance().setBlueDeviceName(btDev.getName());
                                MainApplication.getInstance().setBlueDeviceNameAddress(btDev.getAddress());
                                MainApplication.getInstance().setBlueState(true);
                            } catch (Exception e) {
                                SentryUtils.INSTANCE.uploadTryCatchException(
                                        e,
                                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                                );
                                ToastHelper.showInfo(ToastHelper.toStr(R.string.tx_blue_conn_fail));
                                //Logger.e("hehui", "connet fail " + e);
                                return false;
                            }
                        }
                    } else {
                        return false;
                    }

                } else {
                    return false;
                }

            } else {
                return false;
            }
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            // TODO: handle exception
            return false;
        }

        return true;
    }

    @SuppressLint("NewApi")
    public static boolean blueConnent(String bluetoothDeviceName, final Activity context) {
        try {
            BluetoothDevice btDev = null;
            BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter();

            if (mBtAdapter != null) {//检查是否打开蓝牙
                if (!mBtAdapter.isEnabled()) {
                    DialogInfo dialog = new DialogInfo(context, null, ToastHelper.toStr(R.string.tx_blue_no_open), ToastHelper.toStr(R.string.to_open), ToastHelper.toStr(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {

                            Intent mIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            context.startActivityForResult(mIntent, 1);

                        }

                        @Override
                        public void handleCancelBtn() {
                            //                                dialog.cancel();
                        }
                    }, null);

                    DialogHelper.resize(context, dialog);
                    dialog.show();

                    return false;
                }
                Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();
                if (pairedDevices.size() > 0) {
                    for (BluetoothDevice device : pairedDevices) {
                        if (device.getAddress().equalsIgnoreCase(bluetoothDeviceName)) {
                            btDev = device;
                            break;
                        }
                    }
                } else {
                    return false;
                }
                if (btDev != null) {

                    BluetoothSocket bluetoothSocket = btDev.createRfcommSocketToServiceRecord(SPP_UUID);
                    if (bluetoothSocket != null) {
                        MainApplication.getInstance().setBluetoothSocket(bluetoothSocket);
                        if (mBtAdapter.isDiscovering())
                            //停止搜索
                            mBtAdapter.cancelDiscovery();
                        //如果当前socket处于非连接状态则调用连接
                        if (!bluetoothSocket.isConnected()) {
                            //你应当确保在调用connect()时设备没有执行搜索设备的操作。
                            // 如果搜索设备也在同时进行，那么将会显著地降低连接速率，并很大程度上会连接失败。
                            try {
                                bluetoothSocket.connect();

                                MainApplication.getInstance().setBlueDeviceName(btDev.getName());
                                MainApplication.getInstance().setBlueDeviceNameAddress(btDev.getAddress());
                                MainApplication.getInstance().setBlueState(true);
                            } catch (Exception e) {
                                SentryUtils.INSTANCE.uploadTryCatchException(
                                        e,
                                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                                );
                                ToastHelper.showInfo(ToastHelper.toStr(R.string.tx_blue_conn_fail));
                                return false;
                            }
                        }
                    } else {
                        return false;
                    }

                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (IOException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
            return false;
        }

        return true;
    }


    /**
     * <功能详细描述>
     *根据收银员的是否显示来添加收银员
     * @see [类、类#方法、类#成员]
     *//*
    public static void print(boolean isCashierShow,cn.swiftpass.enterprise.bussiness.model.Order orderModel) {

        if (BuildConfig.IS_POS_VERSION) {

            return;
        }

        if (MainApplication.getInstance().getBluetoothSocket() == null || orderModel == null) return;
        DataOutputStream os = null;
        try {
            try {
                os = new DataOutputStream(MainApplication.getInstance().getBluetoothSocket().getOutputStream());
            } catch (IOException e) {
                Log.e(TAG,Log.getStackTraceString(e));
                try {
                    if (os != null) os.close();
                }catch (Exception ex){
                    Log.e(TAG,Log.getStackTraceString(e));
                }
            }
            StringBuffer printBuffer = new StringBuffer();
            StringBuffer buffer = new StringBuffer();
            printBuffer.append("\n" + orderModel.getPartner() +"\n" +ToastHelper.toStr(R.string.tv_pay_client_save));
            printBuffer.append("\n===============================\n");
            printBuffer.append(ToastHelper.toStr(R.string.shop_name) + "："+"\n"+ MainApplication.getInstance().getMchName() + "\n");
            printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "：" + MainApplication.getInstance().getMchId() + "\n");

            if (!orderModel.isPay()) {//退款
                buffer.append("\n         " + ToastHelper.toStr(R.string.tx_blue_print_refund_note) + "\n");
                printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_refund_time) + "：" + "\n"+ orderModel.getAddTimeNew() + "\n");
                printBuffer.append(ToastHelper.toStr(R.string.refund_odd_numbers) + "：\n");
                printBuffer.append(orderModel.getRefundNo() + "\n");

            } else {
                buffer.append("\n            " + ToastHelper.toStr(R.string.tx_blue_print_pay_note) + "\n");
                //收银员
                if (MainApplication.getInstance().isAdmin.equals("0")) {
                    if (isCashierShow){
                        printBuffer.append(ToastHelper.toStr(R.string.tx_user) + "："+ "\n"+ orderModel.getUserName() + "\n");
                    }
                }else{
                    if (isCashierShow){
                        printBuffer.append(ToastHelper.toStr(R.string.tx_user) + "："+ "\n" + orderModel.getUserName() + "\n");
                    }
                }

                try {
                    printBuffer.append(ToastHelper.toStr(R.string.tx_bill_stream_time) + "："+ "\n" + orderModel.getAddTimeNew() + "\n");
                } catch (Exception e) {
                    printBuffer.append(ToastHelper.toStr(R.string.tx_bill_stream_time) + "：" + "\n"+ orderModel.getTradeTimeNew() + "\n");
                }

            }

            printBuffer.append(ToastHelper.toStr(R.string.tx_order_no) + "：\n");
            printBuffer.append(orderModel.getOrderNoMch() + "\n");

            if (orderModel.isPay()) {
                if (MainApplication.getInstance().getPayTypeMap() != null && MainApplication.getInstance().getPayTypeMap().size() > 0) {
                    String language  = PreferenceUtil.getString("language", "");;
                    //zh-rHK
                    Locale locale = MainApplication.getContext().getResources().getConfiguration().locale;
                    String lan = locale.getCountry();
                    if (!TextUtils.isEmpty(language)) {
                        if (language.equals(MainApplication.LANG_CODE_EN_US)) {

                            printBuffer.append(MainApplication.getInstance().getPayTypeMap().get(orderModel.getApiCode())+ " " + ToastHelper.toStr(R.string.tx_orderno) + ":\n");
                        } else {
                            printBuffer.append(MainApplication.getInstance().getPayTypeMap().get(orderModel.getApiCode())+  ToastHelper.toStr(R.string.tx_orderno) + ":\n");
                        }
                    } else {
                        if (lan.equalsIgnoreCase("en")) {
                            printBuffer.append(MainApplication.getInstance().getPayTypeMap().get(orderModel.getApiCode())+ " " + ToastHelper.toStr(R.string.tx_orderno) + ":\n");
                        } else {
                            printBuffer.append(MainApplication.getInstance().getPayTypeMap().get(orderModel.getApiCode())+  ToastHelper.toStr(R.string.tx_orderno) + ":\n");
                        }

                    }

                }
                printBuffer.append(orderModel.getTransactionId() + "\n");
                printBuffer.append(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：" + "\n"+ orderModel.getTradeName() + "\n");
                printBuffer.append(ToastHelper.toStr(R.string.tx_bill_stream_statr) + "：" + "\n"+ MainApplication.getTradeTypeMap().get(orderModel.getTradeState() + "") + "\n");
                if (!StringUtil.isEmptyOrNull(orderModel.getAttach())) {
                    printBuffer.append(ToastHelper.toStr(R.string.tx_bill_stream_attach) + "：" + orderModel.getAttach() + "\n");
                }

                if (MainApplication.feeFh.equalsIgnoreCase("¥")) {
                    if (orderModel.getDaMoney() > 0) {
                        long actualMoney = orderModel.getDaMoney() + orderModel.getOrderFee();
                        String string_Amount = printTwoData(ToastHelper.toStr(R.string.tx_blue_print_money) + "：",
                                DateUtil.formatMoneyUtils(actualMoney) + ToastHelper.toStr(R.string.pay_yuan));
                        printBuffer.append(string_Amount+ "\n");
//                        printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_money) + "：" + DateUtil.formatMoneyUtils(actualMoney) + ToastHelper.toStr(R.string.pay_yuan) + "\n");
                    } else {
//                        printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_money) + "：" + DateUtil.formatMoneyUtils(orderModel.getOrderFee()) + ToastHelper.toStr(R.string.pay_yuan) + "\n");
                        String string_Amount = printTwoData(ToastHelper.toStr(R.string.tx_blue_print_money) + "：" ,
                                DateUtil.formatMoneyUtils(orderModel.getOrderFee()) + ToastHelper.toStr(R.string.pay_yuan) );
                        printBuffer.append(string_Amount+ "\n");
                    }
                } else {

                    if (MainApplication.getInstance().isSurchargeOpen()) {
                        String string_Amount = printTwoData(ToastHelper.toStr(R.string.tx_blue_print_money) + "：",
                                MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getOrderFee()));
                        printBuffer.append(string_Amount+ "\n");

                        String string_Surcharge = printTwoData(ToastHelper.toStr(R.string.tx_surcharge) + "：",
                                MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getSurcharge()));
                        printBuffer.append(string_Surcharge+ "\n");

//                        printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_money) + "：" + MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getOrderFee()) + "\n");

//                        printBuffer.append(ToastHelper.toStr(R.string.tx_surcharge) + "：" + MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getSurcharge()) + "\n");
                    }

                    String string_Total = printTwoData(ToastHelper.toStr(R.string.tv_charge_total) + "：",
                            MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee()));
                    printBuffer.append(string_Total+ "\n");

//                    printBuffer.append(ToastHelper.toStr(R.string.tv_charge_total) + "：" + MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee()) + "\n");

                    if (orderModel.getCashFeel() > 0) {
                        String string_CNY = printTwoData("", ToastHelper.toStr(R.string.pay_yuan)+ DateUtil.formatMoneyUtils(orderModel.getCashFeel()));
                        printBuffer.append(string_CNY+ "\n");
                    }
                }
            } else {
                printBuffer.append(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "："+ "\n" + orderModel.getTradeName() + "\n");
                if (!StringUtil.isEmptyOrNull(orderModel.getUserName())) {
                    printBuffer.append(ToastHelper.toStr(R.string.tv_refund_peop) + "："+ "\n" + orderModel.getUserName() + "\n");
                }
                if (MainApplication.getInstance().getRefundStateMap() != null && MainApplication.getInstance().getRefundStateMap().size() > 0) {

                    printBuffer.append(ToastHelper.toStr(R.string.tv_refund_state) + "："+ "\n" + MainApplication.getInstance().getRefundStateMap().get(orderModel.getRefundState() + "") + "\n");
                }
                if (MainApplication.feeFh.equalsIgnoreCase("¥")) {
//                    printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_money) + "：" + DateUtil.formatMoneyUtils(orderModel.getTotalFee()) + ToastHelper.toStr(R.string.pay_yuan) + "\n");
                    String string_amount = printTwoData(ToastHelper.toStr(R.string.tx_blue_print_money) + "：",
                            DateUtil.formatMoneyUtils(orderModel.getTotalFee()) + ToastHelper.toStr(R.string.pay_yuan));
                    printBuffer.append(string_amount+ "\n");

//                    printBuffer.append(ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "：" + DateUtil.formatMoneyUtils(orderModel.getRefundMoney()) + ToastHelper.toStr(R.string.pay_yuan) + "\n");
                    String string_refound_amount = printTwoData(ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "：",
                            DateUtil.formatMoneyUtils(orderModel.getRefundMoney()) + ToastHelper.toStr(R.string.pay_yuan));
                    printBuffer.append(string_refound_amount+ "\n");
                } else {
//                    printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_money) + "：" + MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getOrderFee()) + "\n");


                    *//*if (MainApplication.getInstance().isSurchargeOpen()) {
                        printBuffer.append(ToastHelper.toStr(R.string.tx_surcharge) + "：" + MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getSurcharge()) + "\n");
                    }*//*
                    String string_Total = printTwoData(ToastHelper.toStr(R.string.tv_charge_total) + "：",
                            MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee()) );
                    printBuffer.append(string_Total+ "\n");
//                    printBuffer.append(ToastHelper.toStr(R.string.tv_charge_total) + "：" + MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee()) + "\n");

//                    printBuffer.append(ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "：" + MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getRefundMoney()) + "\n");
                    String string_refound_amount = printTwoData(ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "：",
                            MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getRefundMoney()));
                    printBuffer.append(string_refound_amount+ "\n");

//                    printBuffer.append(ToastHelper.toStr(R.string.pay_yuan)+ DateUtil.formatMoneyUtils(orderModel.getCashFeel()) + "\n");
                    if(orderModel.getCashFeel() > 0){
                        String string_CNY = printTwoData("",
                                ToastHelper.toStr(R.string.pay_yuan)+ DateUtil.formatMoneyUtils(orderModel.getCashFeel()));
                        printBuffer.append(string_CNY+ "\n");

                    }
                }
                if (!StringUtil.isEmptyOrNull(orderModel.getPrintInfo())) {
                    printBuffer.append(orderModel.getPrintInfo() + "\n");
                }
            }
            printBuffer.append("\n");

            POS_S_TextOut(buffer.toString(), "gbk", 0, 0, 1, 0, 0, os);
            POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);

            if(!StringUtil.isEmptyOrNull(orderModel.getOrderNoMch()) && orderModel.isPay()){
                POS_EPSON_SetQRCodeV2(orderModel.getOrderNoMch(),5,7,4);
                printBuffer = new StringBuffer();
                printBuffer.append( "\n"+ToastHelper.toStr(R.string.refound_QR_code)+ "\n");
                POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);
            }
            BluePrintUtil.POS_S_Align(0,os);

            printBuffer = new StringBuffer();
            printBuffer.append("-------------------------------\n");
            printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n");
            if (orderModel.getPartner().equalsIgnoreCase(ToastHelper.toStr(R.string.tv_pay_mch_stub))
                    || orderModel.getPartner().equalsIgnoreCase(ToastHelper.toStr(R.string.tv_pay_user_stub))) {
                printBuffer.append("\n"+ToastHelper.toStr(R.string.tx_blue_print_sign) + "：\n\n\n\n");
                //                printBuffer.append("备注：\n\n\n");
            } else {
                printBuffer.append("   \n\n\n");
            }
            POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);

        } catch (Exception e) {
            return;
            // TODO: handle exception
        }

    }
*/

    /**
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    public static void print(boolean isCashierShow, cn.swiftpass.enterprise.bussiness.model.Order orderModel) {

        if (BuildConfig.IS_POS_VERSION) {

            return;
        }

        if (MainApplication.getInstance().getBluetoothSocket() == null || orderModel == null)
            return;
        DataOutputStream os = null;
        try {
            try {
                os = new DataOutputStream(MainApplication.getInstance().getBluetoothSocket().getOutputStream());
            } catch (IOException e) {
                Log.e(TAG, Log.getStackTraceString(e));
                try {
                    if (os != null) os.close();
                } catch (Exception ex) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            ex,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
            StringBuffer printBuffer = new StringBuffer();
            StringBuffer buffer = new StringBuffer();
            printBuffer.append("\n" + orderModel.getPartner() + "\n" + ToastHelper.toStr(R.string.tv_pay_client_save));
            printBuffer.append("\n===============================\n");
            printBuffer.append(ToastHelper.toStr(R.string.shop_name) + "：" + "\n" + MainApplication.getInstance().getMchName() + "\n");
            printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "：" + MainApplication.getInstance().getMchId() + "\n");

            if (!orderModel.isPay()) {//退款
                buffer.append("\n         " + ToastHelper.toStr(R.string.tx_blue_print_refund_note) + "\n");
                printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_refund_time) + "：" + "\n" + orderModel.getAddTimeNew() + "\n");
                printBuffer.append(ToastHelper.toStr(R.string.refund_odd_numbers) + "：\n");
                printBuffer.append(orderModel.getRefundNo() + "\n");

            } else {
                buffer.append("\n            " + ToastHelper.toStr(R.string.tx_blue_print_pay_note) + "\n");
                if (MainApplication.getInstance().isAdmin(0) && !isCashierShow) { //收银员
                    printBuffer.append(ToastHelper.toStr(R.string.tx_user) + "："
                            + MainApplication.getInstance().getUserInfo().realname + "\n");
                }
                if (isCashierShow) {
                    printBuffer.append(ToastHelper.toStr(R.string.tx_user) + "："
                            + "\n" + orderModel.getUserName() + "\n");
                }

                try {
                    printBuffer.append(ToastHelper.toStr(R.string.tx_bill_stream_time) + "：" + "\n" + orderModel.getAddTimeNew() + "\n");
                } catch (Exception e) {
                    printBuffer.append(ToastHelper.toStr(R.string.tx_bill_stream_time) + "：" + "\n" + orderModel.getTradeTimeNew() + "\n");
                }

            }

            printBuffer.append(ToastHelper.toStr(R.string.tx_order_no) + "：\n");
            printBuffer.append(orderModel.getOrderNoMch() + "\n");

            if (orderModel.isPay()) {
                if (MainApplication.getInstance().getPayTypeMap() != null && MainApplication.getInstance().getPayTypeMap().size() > 0) {
                    printBuffer.append(MainApplication.getInstance().getPayTypeMap().get(orderModel.getApiCode()) + " " + ToastHelper.toStr(R.string.tx_orderno) + ":\n");
                }
                printBuffer.append(orderModel.getTransactionId() + "\n");
                printBuffer.append(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：" + "\n" + orderModel.getTradeName() + "\n");
                printBuffer.append(ToastHelper.toStr(R.string.tx_bill_stream_statr) + "：" + "\n" + MainApplication.getInstance().getTradeTypeMap().get(orderModel.getTradeState() + "") + "\n");
                if (!StringUtil.isEmptyOrNull(orderModel.getAttach())) {
                    printBuffer.append(ToastHelper.toStr(R.string.tx_bill_stream_attach) + "：" + orderModel.getAttach() + "\n");
                }

                if (MainApplication.getInstance().getUserInfo().feeFh.equalsIgnoreCase("¥")
                        && MainApplication.getInstance().getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
                    if (orderModel.getDaMoney() > 0) {
                        long actualMoney = orderModel.getDaMoney() + orderModel.getOrderFee();
                        String string_Amount_title = printTwoData(ToastHelper.toStr(R.string.tx_blue_print_money) + "：", "");
                        printBuffer.append(string_Amount_title + "\n");

                        String string_Amount_content = printTwoData("", DateUtil.formatRMBMoneyUtils(actualMoney) + ToastHelper.toStr(R.string.pay_yuan));
                        printBuffer.append(string_Amount_content + "\n");
//                        printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_money) + "：" + DateUtil.formatMoneyUtils(actualMoney) + ToastHelper.toStr(R.string.pay_yuan) + "\n");
                    } else {
//                        printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_money) + "：" + DateUtil.formatMoneyUtils(orderModel.getOrderFee()) + ToastHelper.toStr(R.string.pay_yuan) + "\n");
                        String string_Amount_title = printTwoData(ToastHelper.toStr(R.string.tx_blue_print_money) + "：", "");
                        printBuffer.append(string_Amount_title + "\n");

                        String string_Amount_content = printTwoData("", DateUtil.formatRMBMoneyUtils(orderModel.getOrderFee()) + ToastHelper.toStr(R.string.pay_yuan));
                        printBuffer.append(string_Amount_content + "\n");
                    }
                } else {

                    if (MainApplication.getInstance().isSurchargeOpen()) {
                        if (!orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") && !orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")) {
                            String string_Amount_title = printTwoData(ToastHelper.toStr(R.string.tx_blue_print_money) + "：", "");
                            printBuffer.append(string_Amount_title + "\n");

                            String string_Amount_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType
                                    + " " + DateUtil.formatMoneyUtils(orderModel.getOrderFee()));
                            printBuffer.append(string_Amount_content + "\n");


                            String string_Surcharge_title = printTwoData(ToastHelper.toStr(R.string.tx_surcharge) + "：", "");
                            printBuffer.append(string_Surcharge_title + "\n");

                            String string_Surcharge_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " "
                                    + DateUtil.formatMoneyUtils(orderModel.getSurcharge()));
                            printBuffer.append(string_Surcharge_content + "\n");
                        }

                    }

                    //Uplan ---V3.0.5迭代新增

                    //如果为预授权的支付类型，则不展示任何优惠相关的信息
                    /*因目前SPAY與網關支持的預授權接口僅為‘支付寶’預授權，
                    遂與銀聯Uplan不衝突，遂預授權頁面內，無需處理該優惠信息。*/
                    if (!TextUtils.isEmpty(orderModel.getTradeType()) && !orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") && !orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")) {
                        // 优惠详情 --- 如果列表有数据，则展示，有多少条，展示多少条
                        if (orderModel.getUplanDetailsBeans() != null && orderModel.getUplanDetailsBeans().size() > 0) {
                            for (int i = 0; i < orderModel.getUplanDetailsBeans().size(); i++) {
                                String string_Uplan_details;
                                //如果是Uplan Discount或者Instant Discount，则采用本地词条，带不同语言的翻译
                                if (orderModel.getUplanDetailsBeans().get(i).getDiscountNote().equalsIgnoreCase("Uplan discount")) {
                                    string_Uplan_details = ToastHelper.toStr(R.string.uplan_Uplan_discount) + "：";
                                } else if (orderModel.getUplanDetailsBeans().get(i).getDiscountNote().equalsIgnoreCase("Instant Discount")) {
                                    string_Uplan_details = ToastHelper.toStr(R.string.uplan_Uplan_instant_discount) + "：";
                                } else {//否则，后台传什么展示什么
                                    string_Uplan_details = orderModel.getUplanDetailsBeans().get(i).getDiscountNote() + "：";
                                }
                                String uplanTitle = printTwoData(string_Uplan_details, "");
                                printBuffer.append(uplanTitle + "\n");

                                String uplanAmt = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " " + "-" + orderModel.getUplanDetailsBeans().get(i).getDiscountAmt());
                                printBuffer.append(uplanAmt + "\n");
                            }
                        }

                        // 消费者实付金额 --- 如果不为0 ，就展示
                        if (orderModel.getCostFee() != 0) {
                            String string_CostFee_title = printTwoData(ToastHelper.toStr(R.string.uplan_Uplan_actual_paid_amount) + "：", "");
                            printBuffer.append(string_CostFee_title + "\n");

                            String string_CostFee_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " " + DateUtil.formatMoneyUtils(orderModel.getCostFee()));
                            printBuffer.append(string_CostFee_content + "\n");

                        }
                    }

                    String string_Total_title = printTwoData(ToastHelper.toStr(R.string.tv_charge_total) + "：", "");
                    printBuffer.append(string_Total_title + "\n");

                    String string_Total_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " " + DateUtil.formatMoneyUtils(orderModel.getTotalFee()));
                    printBuffer.append(string_Total_content + "\n");


                    if (orderModel.getCashFeel() > 0) {
                        String string_CNY = printTwoData("", ToastHelper.toStr(R.string.pay_yuan) + " " + DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()));
                        printBuffer.append(string_CNY + "\n");
                    }
                }
            } else {
                printBuffer.append(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：" + "\n" + orderModel.getTradeName() + "\n");
                if (!StringUtil.isEmptyOrNull(orderModel.getUserName())) {
                    printBuffer.append(ToastHelper.toStr(R.string.tv_refund_peop) + "：" + "\n" + orderModel.getUserName() + "\n");
                }
                if (MainApplication.getInstance().getRefundStateMap() != null && MainApplication.getInstance().getRefundStateMap().size() > 0) {

                    printBuffer.append(ToastHelper.toStr(R.string.tv_refund_state) + "：" + "\n" + MainApplication.getInstance().getRefundStateMap().get(orderModel.getRefundState() + "") + "\n");
                }
                if (MainApplication.getInstance().getUserInfo().feeFh.equalsIgnoreCase("¥")
                        && MainApplication.getInstance().getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
                    String string_amount_title = printTwoData(ToastHelper.toStr(R.string.tx_blue_print_money) + "：", "");
                    printBuffer.append(string_amount_title + "\n");

                    String string_amount_content = printTwoData("", DateUtil.formatRMBMoneyUtils(orderModel.getTotalFee()) + ToastHelper.toStr(R.string.pay_yuan));
                    printBuffer.append(string_amount_content + "\n");

                    String string_refound_amount_title = printTwoData(ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "：", "");
                    printBuffer.append(string_refound_amount_title + "\n");

                    String string_refound_amount_content = printTwoData("", DateUtil.formatRMBMoneyUtils(orderModel.getRefundMoney()) + ToastHelper.toStr(R.string.pay_yuan));
                    printBuffer.append(string_refound_amount_content + "\n");
                } else {
                    /*if (MainApplication.getInstance().isSurchargeOpen()) {
                        printBuffer.append(ToastHelper.toStr(R.string.tx_surcharge) + "：" + MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getSurcharge()) + "\n");
                    }*/
                    String string_Total_title = printTwoData(ToastHelper.toStr(R.string.tv_charge_total) + "：", "");
                    printBuffer.append(string_Total_title + "\n");

                    String string_Total_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " " + DateUtil.formatMoneyUtils(orderModel.getTotalFee()));
                    printBuffer.append(string_Total_content + "\n");

                    String string_refound_amount_title = printTwoData(ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "：", "");
                    printBuffer.append(string_refound_amount_title + "\n");

                    String string_refound_amount_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " " + DateUtil.formatMoneyUtils(orderModel.getRefundMoney()));
                    printBuffer.append(string_refound_amount_content + "\n");

                    if (orderModel.getCashFeel() > 0) {
                        String string_CNY = printTwoData("",
                                ToastHelper.toStr(R.string.pay_yuan) + " " + DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()));
                        printBuffer.append(string_CNY + "\n");

                    }
                }
                if (!StringUtil.isEmptyOrNull(orderModel.getPrintInfo())) {
                    printBuffer.append(orderModel.getPrintInfo() + "\n");
                }
            }
            printBuffer.append("\n");

            POS_S_TextOut(buffer.toString(), "gbk", 0, 0, 1, 0, 0, os);
            POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);

            if (!StringUtil.isEmptyOrNull(orderModel.getOrderNoMch()) && orderModel.isPay()) {
                POS_EPSON_SetQRCodeV2(orderModel.getOrderNoMch(), 5, 7, 4);
                printBuffer = new StringBuffer();
                printBuffer.append("\n" + ToastHelper.toStr(R.string.refound_QR_code) + "\n");
                POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);
            }
            BluePrintUtil.POS_S_Align(0, os);

            printBuffer = new StringBuffer();
            printBuffer.append("-------------------------------\n");
            printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n");
            if (orderModel.getPartner().equalsIgnoreCase(ToastHelper.toStr(R.string.tv_pay_mch_stub))
                    || orderModel.getPartner().equalsIgnoreCase(ToastHelper.toStr(R.string.tv_pay_user_stub))) {
                printBuffer.append("\n" + ToastHelper.toStr(R.string.tx_blue_print_sign) + "：\n\n\n\n");
                //                printBuffer.append("备注：\n\n\n");
            } else {
                printBuffer.append("   \n\n\n");
            }
            printBuffer.append("   \n\n\n\n");
            POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);

        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            return;
            // TODO: handle exception
        }

    }


    public static void printMasterCardOrder(boolean isCashierShow, boolean isPay, boolean isOrderDetails, String partner, Order orderModel) {
        if (BuildConfig.IS_POS_VERSION) {
            return;
        }
        if (MainApplication.getInstance().getBluetoothSocket() == null || orderModel == null)
            return;
        DataOutputStream os = null;
        try {
            try {
                os = new DataOutputStream(MainApplication.getInstance().getBluetoothSocket().getOutputStream());
            } catch (IOException e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                Log.e(TAG, Log.getStackTraceString(e));
                try {
                    if (os != null) os.close();
                } catch (Exception ex) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            ex,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }

            StringBuffer buffer = new StringBuffer();
            if (isPay) {
                buffer.append("\n            " + ToastHelper.toStr(R.string.card_payment_print_order_receipt) + "\n");
            } else {
                buffer.append("\n         " + ToastHelper.toStr(R.string.tx_blue_print_refund_note) + "\n");
            }

            StringBuffer printBuffer = new StringBuffer();
            printBuffer.append("\n" + partner + "\n" + ToastHelper.toStr(R.string.tv_pay_client_save));
            printBuffer.append("\n===============================\n");
            printBuffer.append(ToastHelper.toStr(R.string.shop_name) + "：" + "\n" + MainApplication.getInstance().getMchName() + "\n");
            printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "：" + MainApplication.getInstance().getMchId() + "\n");

            if (isPay) {
                if (MainApplication.getInstance().isAdmin(0) && !isCashierShow) { //收银员
                    printBuffer.append(ToastHelper.toStr(R.string.tx_user) + "："
                            + MainApplication.getInstance().getUserInfo().realname + "\n");
                }
                if (isCashierShow) {
                    printBuffer.append(ToastHelper.toStr(R.string.tx_user) + "："
                            + "\n" + orderModel.getUserName() + "\n");
                }
            }

            if (isPay) {
                if (isOrderDetails) {//订单详情
                    try {
                        printBuffer.append(ToastHelper.toStr(R.string.card_payment_print_order_transation_time) + "：" + "\n" + orderModel.getTradeTimeNew() + "\n");
                    } catch (Exception e) {
                        printBuffer.append(ToastHelper.toStr(R.string.card_payment_print_order_transation_time) + "：" + "\n" + orderModel.getTradeTimeNew() + "\n");
                    }
                } else {//支付完成
                    try {
                        printBuffer.append(ToastHelper.toStr(R.string.card_payment_print_order_transation_time) + "：" + "\n" + orderModel.getTradeTime() + "\n");
                    } catch (Exception e) {
                        printBuffer.append(ToastHelper.toStr(R.string.card_payment_print_order_transation_time) + "：" + "\n" + orderModel.getTradeTime() + "\n");
                    }
                }
            } else {//退款
                try {
                    printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_refund_time) + "：" + "\n" + orderModel.getTradeTimeNew() + "\n");
                } catch (Exception e) {
                    printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_refund_time) + "：" + "\n" + orderModel.getTradeTimeNew() + "\n");
                }

                //退款单号
                try {
                    printBuffer.append(ToastHelper.toStr(R.string.refund_odd_numbers) + "：" + "\n" + orderModel.getOutRefundNo() + "\n");
                } catch (Exception e) {
                    printBuffer.append(ToastHelper.toStr(R.string.refund_odd_numbers) + "：" + "\n" + orderModel.getOutRefundNo() + "\n");
                }
            }

            printBuffer.append(ToastHelper.toStr(R.string.tx_order_no) + "：\n");
            printBuffer.append(orderModel.getOrderNoMch() + "\n");

            if (isPay && isOrderDetails) {//订单详情才打印
                //transactionType不为空才打印
                if (!TextUtils.isEmpty(orderModel.getTransactionId())) {
                    if (MainApplication.getInstance().getPayTypeMap() != null && MainApplication.getInstance().getPayTypeMap().size() > 0) {
                        printBuffer.append(MainApplication.getInstance().getPayTypeMap().get(orderModel.getApiCode()) + " " + ToastHelper.toStr(R.string.tx_orderno) + ":\n");
                    }
                    printBuffer.append(orderModel.getTransactionId() + "\n");
                }
            }

            printBuffer.append(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：" + "\n" + orderModel.getTradeName() + "\n");

            if (!isPay) {
                String status = "";
                //订单状态 交易状态：(1.初始化，2：支付成功，3：支付失败)
                switch (orderModel.getTradeState()) {
                    case 1:
                        //初始化
                        status = ToastHelper.toStr(R.string.card_payment_status_init);
                        break;
                    case 2:
                        status = ToastHelper.toStr(R.string.card_payment_status_success);
                        break;
                    case 3:
                        status = ToastHelper.toStr(R.string.card_payment_status_failed);
                        break;
                    default:
                        status = ToastHelper.toStr(R.string.card_payment_status_init);
                        break;
                }
                printBuffer.append(ToastHelper.toStr(R.string.tv_refund_state) + "：" + "\n" + status + "\n");

                if (!TextUtils.isEmpty(orderModel.getUserName())) {
                    printBuffer.append(ToastHelper.toStr(R.string.tv_refund_peop) + "：" + "\n" + orderModel.getUserName() + "\n");
                }
            }

            if (isPay) {
                if (isOrderDetails) {//订单详情
                    printBuffer.append(ToastHelper.toStr(R.string.card_payment_print_order_status) + "：" + "\n" + orderModel.getTradeStateText() + "\n");
                } else {//支付完成
                    String status = "";
                    //订单状态 交易状态：(1.初始化，2：支付成功，3：支付失败)
                    switch (orderModel.getTradeState()) {
                        case 1:
                            //初始化
                            status = ToastHelper.toStr(R.string.card_payment_status_init);
                            break;
                        case 2:
                            status = ToastHelper.toStr(R.string.card_payment_status_success);
                            break;
                        case 3:
                            status = ToastHelper.toStr(R.string.card_payment_status_failed);
                            break;
                        default:
                            status = ToastHelper.toStr(R.string.card_payment_status_init);
                            break;
                    }
                    printBuffer.append(ToastHelper.toStr(R.string.card_payment_print_order_status) + "：" + "\n" + status + "\n");
                }
                if (!TextUtils.isEmpty(orderModel.getTradeType()) && MainApplication.getInstance().getCardPaymentTradeStateMap() != null) {
                    printBuffer.append(ToastHelper.toStr(R.string.card_payment_print_order_type) + "：" + "\n"
                            + MainApplication.getInstance().getCardPaymentTradeStateMap().get(orderModel.getTradeType()) + "\n");
                }
            }

            if (isPay && !StringUtil.isEmptyOrNull(orderModel.getAttach())) {
                printBuffer.append(ToastHelper.toStr(R.string.tx_bill_stream_attach) + "：" + orderModel.getAttach() + "\n");
            }

            if (MainApplication.getInstance().getUserInfo().feeFh.equalsIgnoreCase("¥")
                    && MainApplication.getInstance().getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
                if (isPay) {
                    String string_Amount_title = printTwoData(ToastHelper.toStr(R.string.tx_blue_print_money) + "：", "");
                    printBuffer.append(string_Amount_title + "\n");

                    String string_Amount_content = printTwoData("", DateUtil.formatRMBMoneyUtils(orderModel.getMoney()) + ToastHelper.toStr(R.string.pay_yuan));
                    printBuffer.append(string_Amount_content + "\n");
                } else {
                    String string_amount_title = printTwoData(ToastHelper.toStr(R.string.tx_blue_print_money) + "：", "");
                    printBuffer.append(string_amount_title + "\n");

                    String string_amount_content = printTwoData("", DateUtil.formatRMBMoneyUtils(orderModel.getTotalFee()) + ToastHelper.toStr(R.string.pay_yuan));
                    printBuffer.append(string_amount_content + "\n");

                    String string_refound_amount_title = printTwoData(ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "：", "");
                    printBuffer.append(string_refound_amount_title + "\n");

                    String string_refound_amount_content = printTwoData("", DateUtil.formatRMBMoneyUtils(orderModel.getRefundMoney()) + ToastHelper.toStr(R.string.pay_yuan));
                    printBuffer.append(string_refound_amount_content + "\n");
                }

            } else {
                if (MainApplication.getInstance().isSurchargeOpen()) {
                    if (isPay) {
                        if (isOrderDetails) {//订单详情
                            String string_Amount_title = printTwoData(ToastHelper.toStr(R.string.tx_blue_print_money) + "：", "");
                            printBuffer.append(string_Amount_title + "\n");

                            String string_Amount_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " " + DateUtil.formatMoneyUtils(orderModel.getOrderFee()));
                            printBuffer.append(string_Amount_content + "\n");


                            String string_Surcharge_title = printTwoData(ToastHelper.toStr(R.string.tx_surcharge) + "：", "");
                            printBuffer.append(string_Surcharge_title + "\n");

                            String string_Surcharge_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " " + DateUtil.formatMoneyUtils(orderModel.getSurcharge()));
                            printBuffer.append(string_Surcharge_content + "\n");

                            String string_Total_title = printTwoData(ToastHelper.toStr(R.string.tv_charge_total) + "：", "");
                            printBuffer.append(string_Total_title + "\n");

                            String string_Total_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " " + DateUtil.formatMoneyUtils(orderModel.getMoney()));
                            printBuffer.append(string_Total_content + "\n");


                        } else {//支付完成
                            String string_Amount_title = printTwoData(ToastHelper.toStr(R.string.tx_blue_print_money) + "：", "");
                            printBuffer.append(string_Amount_title + "\n");

                            String string_Amount_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " " + DateUtil.formatMoneyUtils(orderModel.getOrderAmount()));
                            printBuffer.append(string_Amount_content + "\n");


                            String string_Surcharge_title = printTwoData(ToastHelper.toStr(R.string.tx_surcharge) + "：", "");
                            printBuffer.append(string_Surcharge_title + "\n");

                            String string_Surcharge_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " " + DateUtil.formatMoneyUtils(orderModel.getSurcharge()));
                            printBuffer.append(string_Surcharge_content + "\n");

                            String string_Total_title = printTwoData(ToastHelper.toStr(R.string.tv_charge_total) + "：", "");
                            printBuffer.append(string_Total_title + "\n");

                            String string_Total_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " " + DateUtil.formatMoneyUtils(orderModel.getMoney()));
                            printBuffer.append(string_Total_content + "\n");
                        }
                    } else {//退款完成
                        String string_Total_title = printTwoData(ToastHelper.toStr(R.string.tv_charge_total) + "：", "");
                        printBuffer.append(string_Total_title + "\n");

                        String string_Total_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " " + DateUtil.formatMoneyUtils(orderModel.getTotalFee()));
                        printBuffer.append(string_Total_content + "\n");


                        String string_Amount_title = printTwoData(ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "：", "");
                        printBuffer.append(string_Amount_title + "\n");

                        String string_Amount_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " " + DateUtil.formatMoneyUtils(orderModel.getRefundMoney()));
                        printBuffer.append(string_Amount_content + "\n");

                        if (!StringUtil.isEmptyOrNull(orderModel.getPrintInfo())) {
                            printBuffer.append(orderModel.getPrintInfo() + "\n");
                        }
                    }
                } else {
                    if (isPay) {
                        if (isOrderDetails) {//订单详情
                            String string_Total_title = printTwoData(ToastHelper.toStr(R.string.tv_charge_total) + "：", "");
                            printBuffer.append(string_Total_title + "\n");

                            String string_Total_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " " + DateUtil.formatMoneyUtils(orderModel.getMoney()));
                            printBuffer.append(string_Total_content + "\n");

                        } else {//支付完成
                            String string_Total_title = printTwoData(ToastHelper.toStr(R.string.tv_charge_total) + "：", "");
                            printBuffer.append(string_Total_title + "\n");

                            String string_Total_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " " + DateUtil.formatMoneyUtils(orderModel.getMoney()));
                            printBuffer.append(string_Total_content + "\n");
                        }
                    } else {//退款完成
                        String string_Total_title = printTwoData(ToastHelper.toStr(R.string.tv_charge_total) + "：", "");
                        printBuffer.append(string_Total_title + "\n");

                        String string_Total_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " " + DateUtil.formatMoneyUtils(orderModel.getTotalFee()));
                        printBuffer.append(string_Total_content + "\n");


                        String string_Amount_title = printTwoData(ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "：", "");
                        printBuffer.append(string_Amount_title + "\n");

                        String string_Amount_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " " + DateUtil.formatMoneyUtils(orderModel.getRefundMoney()));
                        printBuffer.append(string_Amount_content + "\n");

                        if (!StringUtil.isEmptyOrNull(orderModel.getPrintInfo())) {
                            printBuffer.append(orderModel.getPrintInfo() + "\n");
                        }
                    }
                }
            }

            printBuffer.append("\n");

            POS_S_TextOut(buffer.toString(), "gbk", 0, 0, 1, 0, 0, os);
            POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);

            if (!StringUtil.isEmptyOrNull(orderModel.getOrderNoMch())) {
                POS_EPSON_SetQRCodeV2(orderModel.getOrderNoMch(), 5, 7, 4);
                printBuffer = new StringBuffer();
                printBuffer.append("\n" + ToastHelper.toStr(R.string.card_payment_print_order_info) + "\n");
                POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);
            }
            BluePrintUtil.POS_S_Align(0, os);

            printBuffer = new StringBuffer();
            printBuffer.append("-------------------------------\n");
            printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n");
            printBuffer.append("\n" + ToastHelper.toStr(R.string.tx_blue_print_sign) + "：\n\n\n\n");
            printBuffer.append("   \n\n\n\n");
            POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);

        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            return;
        }

    }

    public static void POS_EPSON_SetQRCodeV2(String strCodedata, int nWidthX, int nVersion,
                                             int nErrorCorrectionLevel) {

        if (MainApplication.getInstance().getBluetoothSocket() == null || StringUtil.isEmptyOrNull(strCodedata))
            return;
        if (nWidthX < 2 | nWidthX > 6 | nErrorCorrectionLevel < 1
                | nErrorCorrectionLevel > 4)
            return;

        byte[] bCodeData = null;
        DataOutputStream os = null;
        try {
            os = new DataOutputStream(MainApplication.getInstance().getBluetoothSocket().getOutputStream());
        } catch (IOException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
            try {
                if (os != null) os.close();
            } catch (Exception ex) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        ex,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                Log.e(TAG, Log.getStackTraceString(e));
            }
        }
        try {

            bCodeData = strCodedata.getBytes("GBK");
        } catch (UnsupportedEncodingException e) {
            return;
        }

        Cmd.ESCCmd.GS_w_n[2] = (byte) nWidthX;
        Cmd.ESCCmd.GS_leftbracket_k_pL_pH_cn_67_n[7] = (byte) nVersion;
        Cmd.ESCCmd.GS_leftbracket_k_pL_pH_cn_69_n[7] = (byte) (47 + nErrorCorrectionLevel);
        Cmd.ESCCmd.GS_leftbracket_k_pL_pH_cn_80_m__d1dk[3] = (byte) ((bCodeData.length + 3) & 0xff);
        Cmd.ESCCmd.GS_leftbracket_k_pL_pH_cn_80_m__d1dk[4] = (byte) (((bCodeData.length + 3) & 0xff00) >> 8);

        byte[] data = byteArraysToBytes(new byte[][]{
                Cmd.ESCCmd.GS_w_n, Cmd.ESCCmd.GS_leftbracket_k_pL_pH_cn_67_n,
                Cmd.ESCCmd.GS_leftbracket_k_pL_pH_cn_69_n,
                Cmd.ESCCmd.GS_leftbracket_k_pL_pH_cn_80_m__d1dk, bCodeData,
                Cmd.ESCCmd.GS_leftbracket_k_pL_pH_cn_fn_m});
        // POS_SetMotionUnit(100,50,os);
        BluePrintUtil.POS_S_Align(1, os);
        write(data, 0, data.length, os);

    }


    // nFontType 0 标准 1 压缩 其他不指定
    public static void POS_S_TextOut(String pszString, String encoding, int nOrgx, int nWidthTimes, int nHeightTimes, int nFontType, int nFontStyle, DataOutputStream os) {
        if (MainApplication.getInstance().getBluetoothSocket() == null) return;

        if (nOrgx > 65535 || nOrgx < 0 || nWidthTimes > 7 || nWidthTimes < 0 || nHeightTimes > 7 || nHeightTimes < 0 || nFontType < 0 || nFontType > 4 || (pszString.length() == 0))
            return;

        Cmd.ESCCmd.ESC_dollors_nL_nH[2] = (byte) (nOrgx % 0x100);
        Cmd.ESCCmd.ESC_dollors_nL_nH[3] = (byte) (nOrgx / 0x100);

        byte[] intToWidth = {0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70};
        byte[] intToHeight = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
        Cmd.ESCCmd.GS_exclamationmark_n[2] = (byte) (intToWidth[nWidthTimes] + intToHeight[nHeightTimes]);

        byte[] tmp_ESC_M_n = Cmd.ESCCmd.ESC_M_n;
        if ((nFontType == 0) || (nFontType == 1)) tmp_ESC_M_n[2] = (byte) nFontType;
        else tmp_ESC_M_n = new byte[0];

        // 字体风格
        // 暂不支持平滑处理
        Cmd.ESCCmd.GS_E_n[2] = (byte) ((nFontStyle >> 3) & 0x01);

        Cmd.ESCCmd.ESC_line_n[2] = (byte) ((nFontStyle >> 7) & 0x03);
        Cmd.ESCCmd.FS_line_n[2] = (byte) ((nFontStyle >> 7) & 0x03);

        Cmd.ESCCmd.ESC_lbracket_n[2] = (byte) ((nFontStyle >> 9) & 0x01);

        Cmd.ESCCmd.GS_B_n[2] = (byte) ((nFontStyle >> 10) & 0x01);

        Cmd.ESCCmd.ESC_V_n[2] = (byte) ((nFontStyle >> 12) & 0x01);

        byte[] pbString = null;
        try {
            pbString = pszString.getBytes(encoding);
        } catch (UnsupportedEncodingException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            return;
        }

        byte[] data = byteArraysToBytes(new byte[][]{Cmd.ESCCmd.ESC_dollors_nL_nH, Cmd.ESCCmd.GS_exclamationmark_n, tmp_ESC_M_n, Cmd.ESCCmd.GS_E_n, Cmd.ESCCmd.ESC_line_n, Cmd.ESCCmd.FS_line_n, Cmd.ESCCmd.ESC_lbracket_n, Cmd.ESCCmd.GS_B_n, Cmd.ESCCmd.ESC_V_n, pbString});

        write(data, 0, data.length, os);

    }

    /**
     * 打印发送文本消息
     *
     * @param message
     */
    @SuppressLint("NewApi")
    public static void sendMessage(String message) {
        if (MainApplication.getInstance().getBluetoothSocket() == null || TextUtils.isEmpty(message))
            return;
        try {
            message += "\n";
            OutputStream outputStream = MainApplication.getInstance().getBluetoothSocket().getOutputStream();
            outputStream.write(message.getBytes("gbk"));
            outputStream.flush();
        } catch (IOException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
        }
    }

    /**
     * 把Bitmap转Byte
     */
    public static byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    /**
     * 打印二维码图片
     */
    @SuppressLint("NewApi")
    public static void sendQrcodImage(String qrcode) {
        if (MainApplication.getInstance().getBluetoothSocket() == null || StringUtil.isEmptyOrNull(qrcode))
            return;
        try {
            DataOutputStream os = new DataOutputStream(MainApplication.getInstance().getBluetoothSocket().getOutputStream());

            byte[] bCodeData = null;
            try {
                bCodeData = qrcode.getBytes("GBK");
            } catch (UnsupportedEncodingException e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                return;
            }

            Cmd.ESCCmd.GS_w_n[2] = (byte) 6;
            Cmd.ESCCmd.GS_k_m_v_r_nL_nH[3] = (byte) 10;
            Cmd.ESCCmd.GS_k_m_v_r_nL_nH[4] = (byte) 4;
            Cmd.ESCCmd.GS_k_m_v_r_nL_nH[5] = (byte) (bCodeData.length & 0xff);
            Cmd.ESCCmd.GS_k_m_v_r_nL_nH[6] = (byte) ((bCodeData.length & 0xff00) >> 8);

            byte[] data = byteArraysToBytes(new byte[][]{Cmd.ESCCmd.GS_w_n, Cmd.ESCCmd.GS_k_m_v_r_nL_nH, bCodeData});

            write(data, 0, data.length, os);
            os.close();
        } catch (IOException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
        }
    }

    /**
     * 将多个字节数组按顺序合并
     *
     * @param data
     * @return
     */
    public static byte[] byteArraysToBytes(byte[][] data) {

        int length = 0;
        for (int i = 0; i < data.length; i++)
            length += data[i].length;
        byte[] send = new byte[length];
        int k = 0;
        for (int i = 0; i < data.length; i++)
            for (int j = 0; j < data[i].length; j++)
                send[k++] = data[i][j];
        return send;
    }

    private static int write(byte[] buffer, int offset, int count, DataOutputStream os) {
        int cnt = 0;
        if (null != os) {
            try {
                os.write(buffer, offset, count);
                os.flush();
                cnt = count;
            } catch (IOException e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                Log.e(TAG, Log.getStackTraceString(e));
                KotlinUtils.INSTANCE.closeBlueSetting();
            } finally {
                //                if (os != null)
                //                {
                //                    try
                //                    {
                //                        os.close();
                //                        os = null;
                //                    }
                //                    catch (IOException e1)
                //                    {
                //                        Log.e(TAG,Log.getStackTraceString(e1));
                //                    }
                //                }
            }

        }

        return cnt;
    }

    /*    */
    /**
     * 打印二维码图片
     *
     * @param message
     * @throws IOException
     *//*
    @SuppressLint("NewApi")
    public static void sendImage(Bitmap bitmap) throws IOException {
        if (MainApplication.getInstance().getBluetoothSocket() == null || bitmap == null) return;
        int idx = 0;
        int curcount = 0;
        byte ch;
        DataOutputStream outputStream = null;
        DataInputStream is = null;
        boolean canSend = true;

        try {

            Bitmap rszBitmap = ImageProcessing.resizeImage(bitmap, 100, 100);
            Bitmap grayBitmap = ImageProcessing.toGrayscale(rszBitmap);
            byte[] dithered = bitmapToBWPix(grayBitmap);

            //            byte[] b = bitmapToBWPix(bitmap);

            byte[] b = eachLinePixToCmd(dithered, 128, 0);
            int count = b.length;
            outputStream = new DataOutputStream(MainApplication.getInstance().getBluetoothSocket().getOutputStream());
            is = new DataInputStream(MainApplication.getInstance().getBluetoothSocket().getInputStream());
            if (null != is) {
                is.skipBytes(is.available());
            }

            while (idx < b.length) {

                if ((null == is) || (null == outputStream)) break;

                if (is.available() > 0) {

                    ch = is.readByte();

                    if (0x13 == ch) canSend = false;
                    else if (0x11 == ch) canSend = true;
                    else continue;
                }
                if (canSend) // 如果没有收到任何字节，继续发送
                {
                    if (count - idx > 128) curcount = 128;
                    else curcount = count - idx;

                    outputStream.write(b, 0 + idx, curcount);
                    idx += curcount;
                }
            }
            //            outputStream.write(b, 0, b.length);
            //            outputStream.flush();
        } catch (Exception e) {
            Logger.e("hehui", "sendMessage -->" + e);
        } finally {
            if (outputStream != null) {
                try {

                    outputStream.close();
                    is.close();
                } catch (Exception e2) {
                    Log.e(TAG,Log.getStackTraceString(e2));
                }
            }
        }
    }*/

    private static int[] p0 = {0, 0x80};

    private static int[] p1 = {0, 0x40};

    private static int[] p2 = {0, 0x20};

    private static int[] p3 = {0, 0x10};

    private static int[] p4 = {0, 0x08};

    private static int[] p5 = {0, 0x04};

    private static int[] p6 = {0, 0x02};

    private static byte[] eachLinePixToCmd(byte[] src, int nWidth, int nMode) {
        int nHeight = src.length / nWidth;
        int nBytesPerLine = nWidth / 8;
        byte[] data = new byte[nHeight * (8 + nBytesPerLine)];
        int offset = 0;
        int k = 0;
        for (int i = 0; i < nHeight; i++) {
            offset = i * (8 + nBytesPerLine);
            data[offset + 0] = 0x1d;
            data[offset + 1] = 0x76;
            data[offset + 2] = 0x30;
            data[offset + 3] = (byte) (nMode & 0x01);
            data[offset + 4] = (byte) (nBytesPerLine % 0x100);
            data[offset + 5] = (byte) (nBytesPerLine / 0x100);
            data[offset + 6] = 0x01;
            data[offset + 7] = 0x00;
            for (int j = 0; j < nBytesPerLine; j++) {
                data[offset + 8 + j] = (byte) (p0[src[k]] + p1[src[k + 1]] + p2[src[k + 2]] + p3[src[k + 3]] + p4[src[k + 4]] + p5[src[k + 5]] + p6[src[k + 6]] + src[k + 7]);
                k = k + 8;
            }
        }

        return data;
    }

    /**
     * 将ARGB图转换为二值图，0代表黑，1代表白
     *
     * @param mBitmap
     * @return
     */
    public static byte[] bitmapToBWPix(Bitmap mBitmap) {

        int[] pixels = new int[mBitmap.getWidth() * mBitmap.getHeight()];
        byte[] data = new byte[mBitmap.getWidth() * mBitmap.getHeight()];

        mBitmap.getPixels(pixels, 0, mBitmap.getWidth(), 0, 0, mBitmap.getWidth(), mBitmap.getHeight());

        // for the toGrayscale, we need to select a red or green or blue color
        ImageProcessing.format_K_dither16x16(pixels, mBitmap.getWidth(), mBitmap.getHeight(), data);

        return data;
    }

    /**
     * printType参数1：预授权成功---预授权小票
     * printType参数4：预授权订单详情---预授权小票
     * printType参数2：预授权解冻详情小票
     * printType参数3：预授权解冻完成小票
     * printType参数5：预授权成功---预授权小票---预授权反扫完下单一次成功
     */
    public static void printPreAut(int printType, Order orderModel) {
        if (BuildConfig.IS_POS_VERSION) {
            return;
        }
        if (MainApplication.getInstance().getBluetoothSocket() == null || orderModel == null)
            return;

        DataOutputStream os = null;
        int minus = 1;
        //根据当前的小数点的位数来判断应该除以或者乘以多少
        for (int i = 0; i < MainApplication.getInstance().getNumFixed(); i++) {
            minus = minus * 10;
        }

        try {
            try {
                os = new DataOutputStream(MainApplication.getInstance().getBluetoothSocket().getOutputStream());
            } catch (IOException e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                Log.e(TAG, Log.getStackTraceString(e));
                try {
                    if (os != null) os.close();
                } catch (Exception ex) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            ex,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
            StringBuffer printBuffer = new StringBuffer();
            StringBuffer buffer = new StringBuffer();

            switch (printType) {
                case 1:
                case 4:
                case 5:
                    BluePrintUtil.POS_S_Align(1, os);
                    buffer.append(" \n" + ToastHelper.toStr(R.string.pre_auth_receipt) + " \n");//此二维码用于预授权收款、解冻操作
                    POS_S_TextOut(buffer.toString(), "gbk", 0, 0, 1, 0, 0, os);
                    BluePrintUtil.POS_S_Align(0, os);
                    buffer = new StringBuffer();
                    printBuffer.append("\n" + orderModel.getPartner() + "\n" + ToastHelper.toStr(R.string.tv_pay_client_save));
                    break;

                case 2:
                case 3:
                    BluePrintUtil.POS_S_Align(1, os);
                    buffer.append(" \n" + ToastHelper.toStr(R.string.pre_auth_unfreezed_receipt) + " \n");//此二维码用于预授权收款、解冻操作
                    POS_S_TextOut(buffer.toString(), "gbk", 0, 0, 1, 0, 0, os);
                    BluePrintUtil.POS_S_Align(0, os);
                    buffer = new StringBuffer();
                    printBuffer.append("\n" + orderModel.getPartner() + "\n" + ToastHelper.toStr(R.string.tv_pay_client_save));
                    break;
            }

            printBuffer.append("\n===============================\n");
            printBuffer.append(ToastHelper.toStr(R.string.shop_name) + "：" + "\n" + MainApplication.getInstance().getMchName() + "\n");
            printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "：" + MainApplication.getInstance().getMchId() + "\n");//商户编号

            if (printType == 1) {
                printBuffer.append(ToastHelper.toStr(R.string.pre_auth_time) + "：\n");//交易时间
                printBuffer.append(orderModel.getTradeTime() + "\n");
            } else if (printType == 4) {
                printBuffer.append(ToastHelper.toStr(R.string.pre_auth_time) + "：\n");//交易时间
                printBuffer.append(orderModel.getTradeTimeNew() + "\n");
            } else if (printType == 5) {
                printBuffer.append(ToastHelper.toStr(R.string.pre_auth_time) + "：\n");//交易时间
                printBuffer.append(orderModel.getTimeEnd() + "\n");
            } else if (printType == 2) {
                printBuffer.append(ToastHelper.toStr(R.string.unfreezed_time) + "：\n");//解冻时间
                printBuffer.append(orderModel.getOperateTime() + "\n");
            } else if (printType == 3) {
                printBuffer.append(ToastHelper.toStr(R.string.unfreezed_time) + "：\n");//解冻时间
                printBuffer.append(orderModel.getUnFreezeTime() + "\n");
            }

            if (printType == 1 || printType == 4 || printType == 5) {
                printBuffer.append(ToastHelper.toStr(R.string.platform_pre_auth_order_id) + "：\n");//平台预授权订单号
                printBuffer.append(orderModel.getAuthNo() + "\n");

                String language = PreferenceUtil.getString("language", "");
                Locale locale = MainApplication.getInstance().getResources().getConfiguration().locale; //zh-rHK
                String lan = locale.getCountry();
                if (!TextUtils.isEmpty(language)) {
                    if (language.equals(Constant.LANG_CODE_EN_US)) {
                        printBuffer.append(MainApplication.getInstance().getPayTypeMap().get("2") + " " + ToastHelper.toStr(R.string.tx_orderno) + "：\n");//支付宝单号
                    } else {
                        printBuffer.append(MainApplication.getInstance().getPayTypeMap().get("2") + ToastHelper.toStr(R.string.tx_orderno) + "：\n");//支付宝单号
                    }
                } else {
                    if (lan.equalsIgnoreCase("en")) {
                        printBuffer.append(MainApplication.getInstance().getPayTypeMap().get("2") + " " + ToastHelper.toStr(R.string.tx_orderno) + "：\n");//支付宝单号
                    } else {
                        printBuffer.append(MainApplication.getInstance().getPayTypeMap().get("2") + ToastHelper.toStr(R.string.tx_orderno) + "：\n");//支付宝单号
                    }
                }

                if (printType == 1 || printType == 5) {
                    printBuffer.append(orderModel.getOutTransactionId() + "\n");
                }

                if (printType == 4) {
                    printBuffer.append(orderModel.getTransactionId() + "\n");
                }

            } else if (printType == 2 || printType == 3) {
                printBuffer.append(ToastHelper.toStr(R.string.unfreezed_order_id) + "：\n");//解冻订单号
                printBuffer.append(orderModel.getOutRequestNo() + "\n");
                printBuffer.append(ToastHelper.toStr(R.string.platform_pre_auth_order_id) + "：\n");//平台预授权订单号
                printBuffer.append(orderModel.getAuthNo() + "\n");
            }

            if (printType == 5) {
                String tradename;
                if (!isAbsoluteNullStr(orderModel.getTradeName())) {//先判断大写的Name，再判断小写name
                    tradename = orderModel.getTradeName();
                } else {
                    tradename = orderModel.getTradename();
                }

                printBuffer.append(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：" + "\n" + tradename + "\n");//支付方式
            } else {
                printBuffer.append(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：" + "\n" + orderModel.getTradeName() + "\n");//支付方式
            }


            if (printType == 3) {
                printBuffer.append(ToastHelper.toStr(R.string.tv_unfreezen_applicant) + "：\n");//办理人
                printBuffer.append(orderModel.getUserName() + "\n");
            }

            printBuffer.append(ToastHelper.toStr(R.string.tx_bill_stream_statr) + "：\n");//订单状态
            String operationType = "";
            if (printType == 1 || printType == 4) {
                operationType = MainApplication.getInstance().getPreAuthTradeStateMap().get(orderModel.getTradeState() + "");

            } else if (printType == 2) {
                switch (orderModel.getOperationType()) {
                    case 2:
                        operationType = ToastHelper.toStr(R.string.freezen_success);
                        break;
                }
            } else if (printType == 3) {
                operationType = ToastHelper.toStr(R.string.unfreezing);
            } else if (printType == 5) {
                operationType = ToastHelper.toStr(R.string.pre_auth_authorized);
            }

            printBuffer.append(operationType + "\n");

            switch (printType) {
                case 1:
                case 4:
                case 5:
                    if (MainApplication.getInstance().getUserInfo().feeFh.equalsIgnoreCase("¥")
                            && MainApplication.getInstance().getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
                        String string_amount_title = printTwoData(ToastHelper.toStr(R.string.pre_auth_amount) + ":", "");
                        printBuffer.append(string_amount_title + "\n");

                        String string_amount_content = printTwoData("", DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan));
                        printBuffer.append(string_amount_content + "\n");

                    } else {
                        long preAuthorizationMoney = 0;//用来接收printType判断授权金额来自反扫、正扫
                        if (printType == 1) {
                            preAuthorizationMoney = orderModel.getRestAmount();//反扫
                        } else if (printType == 5) {
                            preAuthorizationMoney = orderModel.getTotalFee();//正扫
                        } else if (printType == 4) {
                            preAuthorizationMoney = orderModel.getTotalFreezeAmount();//预授权详情
                        }

                        String string_refound_amount_title = printTwoData(ToastHelper.toStr(R.string.pre_auth_amount) + ":", "");//预授权金额
                        printBuffer.append(string_refound_amount_title + "\n");

                        String string_refound_amount_content = printTwoData("",
                                MainApplication.getInstance().getUserInfo().feeType + " " + DateUtil.formatMoneyUtils(preAuthorizationMoney));//预授权金额
                        printBuffer.append(string_refound_amount_content + "\n");
                    }

                    printBuffer.append(" " + "\n");

                    POS_S_TextOut(buffer.toString(), "gbk", 0, 0, 1, 0, 0, os);
                    POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);
                    printBuffer.append(" " + "\n" + "  \n");
                    //二维码
                    POS_EPSON_SetQRCodeV2(orderModel.getAuthNo(), 5, 7, 4);
                    printBuffer = new StringBuffer();
                    printBuffer.append(" \n" + ToastHelper.toStr(R.string.scan_to_check) + " \n");//此二维码用于预授权收款、解冻操作
                    POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);
                    BluePrintUtil.POS_S_Align(0, os);

                    printBuffer = new StringBuffer();
                    printBuffer.append("-------------------------------\n");
                    printBuffer.append("\n" + ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + " \n");//打印时间
                    printBuffer.append("\n" + ToastHelper.toStr(R.string.tx_blue_print_sign) + "：\n\n\n\n");
                    POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);
                    break;
                case 2:
                    if (MainApplication.getInstance().getUserInfo().feeFh.equalsIgnoreCase("¥")
                            && MainApplication.getInstance().getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
                        String string_unfreezen_amount_title = printTwoData(ToastHelper.toStr(R.string.unfreezing_amount) + ":", "");
                        printBuffer.append(string_unfreezen_amount_title + "\n");

                        String string_unfreezen_amount_content = printTwoData("", DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan));
                        printBuffer.append(string_unfreezen_amount_content + "\n");

                    } else {
                        String string_unfreezen_amount2_title = printTwoData(ToastHelper.toStr(R.string.unfreezing_amount) + ":", "");
                        printBuffer.append(string_unfreezen_amount2_title + "\n");

                        String string_unfreezen_amount2_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " " + DateUtil.formatMoneyUtils(orderModel.getMoney()));
                        printBuffer.append(string_unfreezen_amount2_content + "\n");
                    }
                    printBuffer.append(ToastHelper.toStr(R.string.unfreeze) + "\n");//解冻将原路返回支付账户或银行卡，到账时间已第三方为准

                    POS_S_TextOut(buffer.toString(), "gbk", 0, 0, 1, 0, 0, os);
                    POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);
                    printBuffer.append(" " + "\n" + "  \n");

                    printBuffer = new StringBuffer();
                    printBuffer.append("-------------------------------\n");
                    printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + " \n");//打印时间
                    printBuffer.append("\n" + ToastHelper.toStr(R.string.tx_blue_print_sign) + "：\n\n\n\n");
                    POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);
                    break;
                case 3:
                    //授权金额
                    if (MainApplication.getInstance().getUserInfo().feeFh.equalsIgnoreCase("¥")
                            && MainApplication.getInstance().getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
                        String string_unfreezen_amount_title = printTwoData(ToastHelper.toStr(R.string.pre_auth_amount) + ":", "");
                        printBuffer.append(string_unfreezen_amount_title + "\n");

                        String string_unfreezen_amount_content = printTwoData("", DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan));
                        printBuffer.append(string_unfreezen_amount_content + "\n");

                    } else {
                        String string_unfreezen_amount2_title = printTwoData(ToastHelper.toStr(R.string.pre_auth_amount) + ":", "");
                        printBuffer.append(string_unfreezen_amount2_title + "\n");

                        String string_unfreezen_amount2_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " " + DateUtil.formatMoneyUtils(orderModel.getTotalFreezeAmount()));
                        printBuffer.append(string_unfreezen_amount2_content + "\n");
                    }
                    //解冻金额
                    if (MainApplication.getInstance().getUserInfo().feeFh.equalsIgnoreCase("¥")
                            && MainApplication.getInstance().getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
                        String string_unfreezen_amount_title = printTwoData(ToastHelper.toStr(R.string.unfreezing_amount) + ":", "");
                        printBuffer.append(string_unfreezen_amount_title + "\n");

                        String string_unfreezen_amount_content = printTwoData("", DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan));
                        printBuffer.append(string_unfreezen_amount_content + "\n");

                    } else {
                        String string_unfreezen_amount2_title = printTwoData(ToastHelper.toStr(R.string.unfreezing_amount) + ":", "");
                        printBuffer.append(string_unfreezen_amount2_title + "\n");

                        String string_unfreezen_amount2_content = printTwoData("", MainApplication.getInstance().getUserInfo().feeType + " " + DateUtil.formatMoneyUtils(orderModel.getTotalFee()));
                        printBuffer.append(string_unfreezen_amount2_content + "\n");
                    }
                    printBuffer.append(ToastHelper.toStr(R.string.unfreeze) + "\n");//解冻将原路返回支付账户或银行卡，到账时间已第三方为准

                    POS_S_TextOut(buffer.toString(), "gbk", 0, 0, 1, 0, 0, os);
                    POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);
                    printBuffer.append(" " + "\n" + "  \n");

                    printBuffer = new StringBuffer();
                    printBuffer.append("-------------------------------\n");
                    printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + " \n");//打印时间
                    printBuffer.append("\n" + ToastHelper.toStr(R.string.tx_blue_print_sign) + "：\n\n\n\n");
                    printBuffer.append("   \n\n\n\n");
                    POS_S_TextOut(printBuffer.toString(), "gbk", 0, 0, 0, 0, 0, os);
                    break;
                default:
                    break;
            }

        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
    }

    public static boolean isAbsoluteNullStr(String str) {
        str = deleteBlank(str);
        return str == null || str.length() == 0 || "null".equalsIgnoreCase(str);

    }

    /**
     * 去前后空格和去换行符
     *
     * @param str
     * @return
     */
    public static String deleteBlank(String str) {
        if (null != str && 0 < str.trim().length()) {
            char[] array = str.toCharArray();
            int start = 0, end = array.length - 1;
            while (array[start] == ' ') start++;
            while (array[end] == ' ') end--;
            return str.substring(start, end + 1).replaceAll("\n", "");

        } else {
            return "";
        }

    }
}
