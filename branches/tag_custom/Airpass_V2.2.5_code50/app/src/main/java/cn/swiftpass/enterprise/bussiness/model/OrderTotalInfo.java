/*
 * 文 件 名:  OrderTotalInfo.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-19
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;
import java.util.List;

/**
 * 汇总 和 走势
 * 
 * @author  he_hui
 * @version  [版本号, 2016-11-19]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class OrderTotalInfo implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    //    总交易金额：countTotalFee         (int)
    //    总交易笔数：countTotalCount            (int)
    //    总退款金额：countTotalRefundFee    (int)
    //    总退款笔数：countTotalRefundCount    (int)
    //        
    //    交易日期：checkTime yyyy-MM-dd HH:mm:ss
    //    成功金额：successFee 
    //    成功笔数：successCount
    //    退款金额：refundFee
    //    退款笔数：refundCount
    //    收银员Id：userId
    //    收银员名称：userName
    //    支付类型名称：payTypeName
    
    private Long countTotalFee;
    
    private Long countTotalCount;
    
    private Long countTotalRefundFee;
    
    private Long countTotalRefundCount;
    
    private Integer reqFeqTime = 0;
    
    private String startTime;
    
    private String endTime;

    private long countTotalTipFee;
    private long countTotalTipFeeCount;

    public long getCountTotalTipFee() {
        return countTotalTipFee;
    }

    public void setCountTotalTipFee(long countTotalTipFee) {
        this.countTotalTipFee = countTotalTipFee;
    }

    public long getCountTotalTipFeeCount() {
        return countTotalTipFeeCount;
    }

    public void setCountTotalTipFeeCount(long countTotalTipFeeCount) {
        this.countTotalTipFeeCount = countTotalTipFeeCount;
    }

    /**
     * @return 返回 startTime
     */
    public String getStartTime()
    {
        return startTime;
    }
    
    /**
     * @param 对startTime进行赋值
     */
    public void setStartTime(String startTime)
    {
        this.startTime = startTime;
    }
    
    /**
     * @return 返回 endTime
     */
    public String getEndTime()
    {
        return endTime;
    }
    
    /**
     * @param 对endTime进行赋值
     */
    public void setEndTime(String endTime)
    {
        this.endTime = endTime;
    }
    
    private String userName;
    
    /**
     * @return 返回 userName
     */
    public String getUserName()
    {
        return userName;
    }
    
    /**
     * @param 对userName进行赋值
     */
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    /**
     * @return 返回 reqFeqTime
     */
    public Integer getReqFeqTime()
    {
        return reqFeqTime;
    }
    
    /**
     * @param 对reqFeqTime进行赋值
     */
    public void setReqFeqTime(Integer reqFeqTime)
    {
        this.reqFeqTime = reqFeqTime;
    }
    
    private List<OrderTotalItemInfo> orderTotalItemInfo;
    
    /**
     * @return 返回 orderTotalItemInfo
     */
    public List<OrderTotalItemInfo> getOrderTotalItemInfo()
    {
        return orderTotalItemInfo;
    }
    
    /**
     * @param 对orderTotalItemInfo进行赋值
     */
    public void setOrderTotalItemInfo(List<OrderTotalItemInfo> orderTotalItemInfo)
    {
        this.orderTotalItemInfo = orderTotalItemInfo;
    }
    
    /**
     * @return 返回 countTotalFee
     */
    public Long getCountTotalFee()
    {
        return countTotalFee;
    }
    
    /**
     * @param 对countTotalFee进行赋值
     */
    public void setCountTotalFee(Long countTotalFee)
    {
        this.countTotalFee = countTotalFee;
    }
    
    /**
     * @return 返回 countTotalCount
     */
    public Long getCountTotalCount()
    {
        return countTotalCount;
    }
    
    /**
     * @param 对countTotalCount进行赋值
     */
    public void setCountTotalCount(Long countTotalCount)
    {
        this.countTotalCount = countTotalCount;
    }
    
    /**
     * @return 返回 countTotalRefundFee
     */
    public Long getCountTotalRefundFee()
    {
        return countTotalRefundFee;
    }
    
    /**
     * @param 对countTotalRefundFee进行赋值
     */
    public void setCountTotalRefundFee(Long countTotalRefundFee)
    {
        this.countTotalRefundFee = countTotalRefundFee;
    }
    
    /**
     * @return 返回 countTotalRefundCount
     */
    public Long getCountTotalRefundCount()
    {
        return countTotalRefundCount;
    }
    
    /**
     * @param 对countTotalRefundCount进行赋值
     */
    public void setCountTotalRefundCount(Long countTotalRefundCount)
    {
        this.countTotalRefundCount = countTotalRefundCount;
    }
    
}
