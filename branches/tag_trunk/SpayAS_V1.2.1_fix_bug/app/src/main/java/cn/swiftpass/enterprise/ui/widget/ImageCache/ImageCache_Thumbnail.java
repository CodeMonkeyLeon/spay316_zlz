/**
 * 
 */
package cn.swiftpass.enterprise.ui.widget.ImageCache;

import java.io.File;

import android.content.Context;
import android.graphics.Bitmap;
import cn.swiftpass.enterprise.bussiness.logica.common.FileManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.ApiResult;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.ImageUtil;

/**
 * 缓存
 * @author  huangdonghua
 *
 */
public class ImageCache_Thumbnail extends ImageCache<String>
{
    private static ImageCache_Thumbnail instance;
    
    public static final ImageCache_Thumbnail getInstance(Context aContext)
    {
        if (instance == null)
        {
            instance = new ImageCache_Thumbnail(aContext);
        }
        return instance;
    }
    
    private ImageCache_Thumbnail(Context aContext)
    {
        super(aContext);
        // TODO Auto-generated constructor stub
    }
    
    String avatarImgPath = null;
    
    @Override
    protected Bitmap doCreateBitmap(String aKey, int flag)
    {
        try
        {
            avatarImgPath = AppHelper.getImgCacheDir() + aKey; //w.l去掉.jpg传过来就有.jpg  avatarImgPath = AppHelper.getImgCacheDir() + aKey + ".jpg";
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if (aKey.length() > 0)
        {
            File file = new File(avatarImgPath);
            if (file.exists())
            {
                return ImageUtil.decodeFile(file, false, 0);
            }
            else
            {
                final Object lock = new Object();
                final ApiResult apiResult = new ApiResult();
                FileManager.getInstance().downloadImg(aKey, new UINotifyListener<Bitmap>()
                {
                    @Override
                    public void onSucceed(Bitmap result)
                    {
                        super.onSucceed(result);
                        if (result != null)
                        {
                            //Bitmap bitmap = ImageUtil.getRoundedCornerBitmap(result, 5);
                            
                            ImageUtil.saveImag2(avatarImgPath, result, Bitmap.CompressFormat.JPEG);
                            apiResult.data = result;
                            synchronized (lock)
                            {
                                lock.notify();
                            }
                        }
                        
                    }
                    
                }, flag);
                try
                {
                    synchronized (lock)
                    {
                        lock.wait();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                
                return (Bitmap)apiResult.data;
            }
        }
        return null;
    }
    
    @Override
    public void release()
    {
        super.release();
        instance = null;
    }
}
