package cn.swiftplus.enterprise.printsdk.print.KS8223;

import android.content.Context;
import android.text.TextUtils;
import com.szzt.sdk.device.Device;
import com.szzt.sdk.device.DeviceManager;
import com.szzt.sdk.device.printer.Printer;

/**
 * Created by aijingya on 2020/11/4.
 *
 * @Package cn.swiftplus.enterprise.printsdk.print.KS8223
 * @Description:
 * @date 2020/11/4.14:33.
 */
public class POSKS8223Client {

    private static POSKS8223Client sPosKS8223Client;
    private Context mContext;

    //证通pos
    private static DeviceManager mDeviceManager;
    private static boolean isConnect = false;
    private static String xgTokenId = "";
    private static String ZT_POS_SN = "";

    public static void initKS8223(Context context){
        if(sPosKS8223Client != null){
            throw new RuntimeException("PosKS8223Client Already initialized");
        }
        sPosKS8223Client = new POSKS8223Client(context);
    }

    public static POSKS8223Client getInstance(){
        if (sPosKS8223Client == null) {
            throw new RuntimeException("PosKS8223Client is not initialized");
        }
        return sPosKS8223Client;
    }

    private POSKS8223Client(Context context){
        mContext = context;
        //KS8223 pos 打印机初始化

    }

    //证通智能pos
    public void initztpos(){
        try {
            mDeviceManager = DeviceManager.createInstance(mContext);
            mDeviceManager.start(deviceManagerListener);
            mDeviceManager.getSystemManager();
            // mLocalBroadcastManager = LocalBroadcastManager.getInstance(MainApplication.getContext());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //证通智能pos
    public Printer getPrinter() {
        try {
            Device[] printers = mDeviceManager.getDeviceByType(Device.TYPE_PRINTER);
            String sn = mDeviceManager.getSystemManager().getDeviceInfo().getSn();

            if(!TextUtils.isEmpty(sn)) {
                ZT_POS_SN=sn;
            }
            if (printers != null){
                return (Printer) printers[0];
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public DeviceManager.DeviceManagerListener deviceManagerListener = new DeviceManager.DeviceManagerListener() {

        @Override
        public int serviceEventNotify(
                int event) {
            if (event == EVENT_SERVICE_CONNECTED) {
                isConnect = true;
                //mLocalBroadcastManager.sendBroadcast(mServiceConnectedIntent);
            } else if (event == EVENT_SERVICE_VERSION_NOT_COMPATABLE) {

            } else if (event == EVENT_SERVICE_DISCONNECTED) {
                isConnect = false;
                mDeviceManager.start(deviceManagerListener);
                // mLocalBroadcastManager.sendBroadcast(mServiceDisConnectedIntent);
            }
            return 0;
        }

        @Override
        public int deviceEventNotify(
                Device device,
                int event) {
            return 0;
        }
    };
    public boolean isDeviceManagerConnetcted() {
        return isConnect;
    }

}
