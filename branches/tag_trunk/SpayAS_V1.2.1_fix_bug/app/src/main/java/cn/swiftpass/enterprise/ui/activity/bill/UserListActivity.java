/*
 * 文 件 名:  UserListActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-21
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.bill;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.settle.SettleCashierAdapter;
import cn.swiftpass.enterprise.ui.widget.PullDownListView;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.Logger;

import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnLoadListener;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnRefreshListener;

/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2016-11-21]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class UserListActivity extends TemplateActivity implements OnRefreshListener, OnLoadListener
{
    private SwipeMenuListView cashier_list;
    
    private ListView listView;
    
    private SettleCashierAdapter adapter;
    
    private List<UserModel> list = new ArrayList<UserModel>();
    
    private Handler mHandler = new Handler();
    
    private TextView tv_prompt;
    
    private int pageFulfil = 0;
    
    private RelativeLayout lr_all;
    
    private ImageView iv_all_choice;
    
    private UserModel userModel;
    
    private int pageCount = 0;
    
    public static void startActivity(Context context, UserModel userModel)
    {
        Intent it = new Intent();
        it.putExtra("userModel", userModel);
        it.setClass(context, UserListActivity.class);
        context.startActivity(it);
    }
    
    private Handler handler = new Handler()
    {
        public void handleMessage(Message msg)
        {
            @SuppressWarnings("unchecked")
            List<UserModel> result = (List<UserModel>)msg.obj;
            switch (msg.what)
            {
                case SwipeMenuListView.REFRESH:
                    list.clear();
                    list.addAll(result);
                    break;
                case SwipeMenuListView.LOAD:
                    list.addAll(result);
                    break;
            }
            cashier_list.onRefreshComplete();
            cashier_list.onLoadComplete();
            cashier_list.setResultSize(result.size());
            adapter.notifyDataSetChanged();
        };
    };
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_push_record);
        
        userModel = (UserModel)getIntent().getSerializableExtra("userModel");
        initView();
        
        if (userModel != null)
        {
            iv_all_choice.setVisibility(View.INVISIBLE);
        }
        loadCashierData(0, true, 2);
        
        lr_all.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                iv_all_choice.setVisibility(View.VISIBLE);
                HandlerManager.notifyMessage(HandlerManager.BILL_CHOICE_USER, HandlerManager.BILL_CHOICE_USER, null);
                finish();
            }
        });
    }
    
    private void initView()
    {
        lr_all = getViewById(R.id.lr_all);
        iv_all_choice = getViewById(R.id.iv_all_choice);
        tv_prompt = getViewById(R.id.tv_prompt);
        tv_prompt.setText(R.string.tv_user_list);
        cashier_list = (SwipeMenuListView)findViewById(R.id.cashier_manager_id);
        if (userModel != null)
        {
            adapter = new SettleCashierAdapter(list, UserListActivity.this, userModel.getId());
        }
        else
        {
            adapter = new SettleCashierAdapter(list, UserListActivity.this, 0);
        }
        //        cashier_list.setAutoLoadMore(true);
        cashier_list.setOnRefreshListener(this);
        cashier_list.setOnLoadListener(this);
        //        listView = cashier_list.mListView;
        cashier_list.setAdapter(adapter);
        
        cashier_list.setOnItemClickListener(new OnItemClickListener()
        {
            
            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3)
            {
                try
                {
                    
                    UserModel userModel = list.get(position - 1);
                    if (userModel != null)
                    {
                        ImageView iView = (ImageView)v.findViewById(R.id.iv_choice);
                        iView.setVisibility(View.VISIBLE);
                        
                        HandlerManager.notifyMessage(HandlerManager.BILL_CHOICE_USER,
                            HandlerManager.BILL_CHOICE_USER,
                            userModel);
                        
                        UserListActivity.this.finish();
                    }
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                }
            }
        });
        
    }
    
    /**
     * 加载收银员列表
     * <功能详细描述>
     * @param page
     * @param isLoadMore
     * @see [类、类#方法、类#成员]
     */
    private void loadCashierData(final int page, final boolean isLoadMore, final int what)
    {
        
        UserManager.queryCashier(page, 20, null, new UINotifyListener<List<UserModel>>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                if (isLoadMore)
                {
                    loadDialog(UserListActivity.this, R.string.public_data_loading);
                }
            }
            
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dissDialog();
                if (checkSession())
                {
                    return;
                }
                if (object != null)
                {
                    toastDialog(UserListActivity.this, object.toString(), null);
                    UserListActivity.this.runOnUiThread(new Runnable()
                    {
                        
                        @Override
                        public void run()
                        {
                            tv_prompt.setVisibility(View.VISIBLE);
                            cashier_list.setVisibility(View.GONE);
                            lr_all.setVisibility(View.GONE);
                        }
                    });
                }
            }
            
            @Override
            public void onSucceed(List<UserModel> result)
            {
                super.onSucceed(result);
                dismissLoading();
                
                if (result != null && result.size() > 0)
                {
                    
                    UserListActivity.this.runOnUiThread(new Runnable()
                    {
                        
                        @Override
                        public void run()
                        {
                            lr_all.setVisibility(View.VISIBLE);
                            tv_prompt.setVisibility(View.GONE);
                            cashier_list.setVisibility(View.VISIBLE);
                        }
                    });
                    pageCount = result.get(0).getPageCount();
                    if (what == 2)
                    {
                        list.clear();
                        list.addAll(result);
                        adapter.notifyDataSetChanged();
                        //                        cashier_list.setResultSize(result.size());
                        cashier_list.onLoadComplete();
                    }
                    else
                    {
                        Message msg = handler.obtainMessage();
                        msg.what = what;
                        msg.obj = result;
                        handler.sendMessage(msg);
                    }
                    //                    mySetListData(cashier_list, list, adapter, result, isLoadMore);
                }
                else
                {
                    
                    UserListActivity.this.runOnUiThread(new Runnable()
                    {
                        
                        @Override
                        public void run()
                        {
                            if (list.size() == 0)
                            {
                                
                                tv_prompt.setVisibility(View.VISIBLE);
                                lr_all.setVisibility(View.GONE);
                                cashier_list.setVisibility(View.GONE);
                            }
                            //                            else
                            //                            {
                            //                                cashier_list.onLoadComplete();
                            //                                cashier_list.setResultSize(0);
                            //                            }
                        }
                    });
                }
                
            }
        });
        
    }
    
    private void mySetListData(final PullDownListView pull, List<UserModel> list, SettleCashierAdapter adapter,
        List<UserModel> result, boolean isLoadMore)
    {
        if (!isLoadMore)
        {
            pull.onfinish(getStringById(R.string.refresh_successfully));// 刷新完成
            
            mHandler.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    // 这里表示刷新处理完成后把上面的加载刷新界面隐藏
                    pull.onRefreshComplete();
                }
            }, 800);
            
            list.clear();
        }
        else
        {
            mHandler.postDelayed(new Runnable()
            {
                
                @Override
                public void run()
                {
                    pull.onLoadMoreComplete();
                    // pull.setMore(true);
                }
            }, 1000);
        }
        list.addAll(result);
        adapter.notifyDataSetChanged();
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setTitle(R.string.tx_user);
        titleBar.setLeftButtonIsVisible(true);
        titleBar.setLeftButtonVisible(true);
        titleBar.setRightButLayVisibleForTotal(false, getString(R.string.tx_apply_balance_list));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
                
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightLayClick()
            {
            }
            
            @Override
            public void onRightButLayClick()
            {
            }
        });
    }
    
    @Override
    public void onLoad()
    {
        pageFulfil = pageFulfil + 1;
        Logger.i("hehui", "pageFulfil-->" + pageFulfil);
        if (pageFulfil <= pageCount)
        {
            loadCashierData(pageFulfil, true, SwipeMenuListView.LOAD);
        }
    }
    
    @Override
    public void onRefresh()
    {
        pageFulfil = 0;
        Logger.i("hehui", "onRefresh pageFulfil-->" + pageFulfil);
        loadCashierData(pageFulfil, false, SwipeMenuListView.REFRESH);
    }
    
    //    @Override
    //    public void onRefresh()
    //    {
    //        pageFulfil = 0;
    //        loadCashierData(pageFulfil, false);
    //    }
    //    
    //    @Override
    //    public void onLoadMore()
    //    {
    //        pageFulfil = pageFulfil + 1;
    //        if (pageFulfil <= pageCount)
    //        {
    //            loadCashierData(pageFulfil, true);
    //        }
    //    }
}
