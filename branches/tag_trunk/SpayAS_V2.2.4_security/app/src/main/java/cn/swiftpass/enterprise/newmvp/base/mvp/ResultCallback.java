package cn.swiftpass.enterprise.newmvp.base.mvp;

import java.lang.ref.WeakReference;

/**
 * Created by YZX on 2019年04月18日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */
public class ResultCallback<T> implements Call<T> {

    private WeakReference<BaseView> mLifeCycleDependence;
    private Call<T> mCall;
    private boolean isEnableLoading;


    public ResultCallback(BaseView lifeCycleDependence, Call<T> call) {
        this(lifeCycleDependence, call, false);
    }

    public ResultCallback(BaseView lifeCycleDependence, Call<T> call, boolean isEnableLoading) {
        mLifeCycleDependence = new WeakReference<>(lifeCycleDependence);
        mCall = call;
        this.isEnableLoading = isEnableLoading;
        if (isEnableLoading) {
            //在此可以写入Spay样式的loading，该loading是UI层的loading调用
           // lifeCycleDependence.showLoadingDialog();
        }
    }

    @Override
    public void onSuccess(T data) {
        BaseView baseView = mLifeCycleDependence.get();
        if (baseView == null) {
            return;
        }
        if (isEnableLoading) {
          //  baseView.dismissLoadingDialog();
        }
        if (mCall != null) {
            mCall.onSuccess(data);
        }
    }

    @Override
    public void onError(int fromPresenter, String code, String errorMsg) {
        BaseView baseView = mLifeCycleDependence.get();
        if (baseView == null) {
            return;
        }
        if (isEnableLoading) {
           // baseView.dismissLoadingDialog();
        }
      //  baseView.showLoadingDialog();
    }
}
