/*
 * 文 件 名:  ActivateScanOneActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-7-13
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.scan;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.common.Constant;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;

/**
 * 固定二维码激活
 *
 * @author he_hui
 * @version [版本号, 2016-7-13]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class ActivateScanTwoActivity extends BaseActivity {


    Button but_activate;

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_code_introduction_two);
        MainApplication.getInstance().getListActivities().add(this);
        but_activate = findViewById(R.id.but_activate);

        but_activate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CaptureActivity.startActivity(ActivateScanTwoActivity.this, Constant.PAY_TYPE_SCAN_OPNE);
            }
        });

    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tv_active_new_sacn);
    }


}
