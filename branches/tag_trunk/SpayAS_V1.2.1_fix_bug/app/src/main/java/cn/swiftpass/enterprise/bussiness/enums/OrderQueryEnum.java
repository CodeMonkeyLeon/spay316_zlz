package cn.swiftpass.enterprise.bussiness.enums;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-11-21
 * Time: 下午4:20
 * To change this template use File | Settings | File Templates.
 */
public enum OrderQueryEnum {

      THIS_WEEK(0,"本周"),
      LAST_WEEK(1,"上周"),
      THIS_MONTH(2,"本月"),
      LAST_MONTH(3,"上月");

    private final Integer value;
    private String displayName = "";
    private OrderQueryEnum(Integer v ,String displayName)
    {
        this.value = v;
        this.displayName = displayName;

    }
    public String getDisplayName() {
        return displayName;
    }

    public Integer getValue() {
        return value;
    }

}
