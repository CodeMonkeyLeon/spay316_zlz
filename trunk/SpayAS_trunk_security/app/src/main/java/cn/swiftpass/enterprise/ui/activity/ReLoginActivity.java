package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.common.sentry.SentryUtils;
import com.example.common.sp.PreferenceUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.CertificateBean;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.common.Constant;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants;
import cn.swiftpass.enterprise.mvp.contract.activity.ReLoginContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.ReLoginPresenter;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.ECDHUtils;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * Created by aijingya on 2019/7/4.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(免密登录异常之后跳转的页面)
 * @date 2019/7/4.18:48.
 */
public class ReLoginActivity extends BaseActivity<ReLoginContract.Presenter> implements View.OnClickListener, ReLoginContract.View {

    private TextView tv_show_login_account;
    private Button btn_Relogin, btn_logout;
    private SharedPreferences sp;
    //    private boolean isNeedDeviceLogin = false;
    private boolean isCerFailed = false;

    @Override
    protected ReLoginContract.Presenter createPresenter() {
        return new ReLoginPresenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relogin);

        sp = this.getSharedPreferences("login", 0);
        isCerFailed = getIntent().getBooleanExtra("isCerFailed", false);

        initView();
        initData();
        initListener();
    }


    public void initView() {
        tv_show_login_account = findViewById(R.id.tv_show_login_account);
        btn_Relogin = findViewById(R.id.btn_Relogin);
        btn_logout = findViewById(R.id.btn_logout);

    }

    public void initListener() {
        btn_Relogin.setOnClickListener(this);
        btn_logout.setOnClickListener(this);
    }


    public void initData() {
        String Mch_id = sp.getString("user_name", "");
        tv_show_login_account.setText(Mch_id);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_Relogin:
                //不管怎么样先去调用ECDH交换接口，然后再调用设备登录接口
                if (!TextUtils.isEmpty(AppHelper.getUUID())) {
                    if (isCerFailed) {
                        //如果跳转到一键重登界面的时候isCerFailed == true
                        //则点击登录的时候，重新去请求公钥
                        requestCertificate(true);
                    } else {
                        //则先走正常的验证网络请求逻辑，检测证书是否有效
                        ECDHKeyExchange();
                    }
                }

                break;
            case R.id.btn_logout:
                logout();
                break;
        }
    }


    @Override
    public void deviceLoginSuccess(boolean response) {
        if (response) {
            loginSuccToLoad();
        }
    }

    @Override
    public void deviceLoginFailed(@Nullable Object error) {
        if (MainApplication.getInstance().isNeedLogin()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MainApplication.getInstance().setNeedLogin(false);

                    PreferenceUtil.removeKey("login_skey");
                    PreferenceUtil.removeKey("login_sauthid");
                    MainApplication.getInstance().setUserInfo(null);

                    Intent it = new Intent();
                    it.setClass(getApplicationContext(), WelcomeActivity.class);
                    it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(it);

                    finish();
                }
            });
            return;
        } else {
            if (error != null) {
                if (error.toString().contains("401")) {
                    //如果是会话已过期，则必须重新登录
                    PreferenceUtil.removeKey("login_skey");
                    PreferenceUtil.removeKey("login_sauthid");
                    MainApplication.getInstance().setUserInfo(null);

                    Intent it = new Intent();
                    it.setClass(getApplicationContext(), WelcomeActivity.class);
                    it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(it);

                    finish();
                } else if (error.toString().contains(RequestResult.HANDSHAKE_CER_ERROR)) {//证书校验失败
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toastDialog(ReLoginActivity.this, getStringById(R.string.certificate_request_failed), null);
                        }
                    });
                } else {
                    toastDialog(ReLoginActivity.this, error.toString(), new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    Intent it = new Intent();
                                    it.setClass(getApplicationContext(), WelcomeActivity.class);
                                    it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(it);

                                    finish();
                                }
                            });
                        }

                    });
                }
            }
        }
    }

    private void deviceLogin() {
        if (mPresenter != null) {
            mPresenter.deviceLogin();
        }
    }


    void loginSuccToLoad() {
        //加载保存登录用户信息
        sp.edit().putString("user_name", MainApplication.getInstance().getUserInfo().username).commit();

        //主动做一次蓝牙连接
        try {
            connentBlue();
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }

        loadPayType();

        for (Activity a : MainApplication.getInstance().getAllActivities()) {
            a.finish();
        }

        Intent it = new Intent();
        it.setClass(ReLoginActivity.this, spayMainTabActivity.class);
        startActivity(it);

        finish();
    }


    @Override
    public void apiShowListSuccess(ArrayList<DynModel> model) {
        apiListSuccess(model);
    }

    @Override
    public void apiShowListFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }
    }

    /**
     * 动态加载支付类型
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void loadPayType() {
        if (mPresenter != null) {
            mPresenter.apiShowList();
        }
    }


    private void apiListSuccess(List<DynModel> model) {
        if (null != model && model.size() > 0) {

            PreferenceUtil.removeKey("payTypeMd5" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());

            PreferenceUtil.commitString("payTypeMd5" + BuildConfig.bankCode + MainApplication.getInstance().getMchId(), model.get(0).getMd5());
            //显示所有的支付方式
            List<DynModel> allLst = new ArrayList<DynModel>();
            saveAllpayList(model, allLst);
            SharedPreUtils.saveObject(allLst, "dynallPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());

            //过滤支持类型
            List<DynModel> filterLst = new ArrayList<DynModel>();
            Map<String, String> payTypeMap = new HashMap<String, String>();
            filterList(model, filterLst, payTypeMap);
            SharedPreUtils.saveObject(filterLst, "dynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            List<DynModel> filterStaticLst = new ArrayList<DynModel>();

            //过滤已开通的支持类型
            List<DynModel> filterList_active = new ArrayList<DynModel>();
            Map<String, String> payTypeMap_active = new HashMap<String, String>();
            filterList_active(model, filterList_active, payTypeMap_active);
            SharedPreUtils.saveObject(filterList_active, "ActiveDynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());

            //读取上面过滤出来的，已开通的支付类型，如果已开通的支付类型里，有QR交易通道，则把参数置成true-----V3.1.0版本新增
            if (filterList_active != null && filterList_active.size() > 0) {
                MainApplication.getInstance().setHasQRPayment(true);
            } else {
                MainApplication.getInstance().setHasQRPayment(false);
            }

            //过滤卡通道的类型---V3.1.0版本新增
            List<DynModel> mFilterCardPaymentList = new ArrayList<DynModel>();
            Map<String, String> mCardPaymentPayTypeMap = new HashMap<String, String>();
            filterCardPaymentList(model, mFilterCardPaymentList, mCardPaymentPayTypeMap);
            SharedPreUtils.saveObject(mFilterCardPaymentList, "cardPayment_dynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());

            //过滤已开通的卡通道类型---V3.1.0版本新增
            List<DynModel> mFilterCardPaymentList_active = new ArrayList<DynModel>();
            Map<String, String> mCardPaymentPayTypeMap_active = new HashMap<String, String>();
            filterActiveCardPaymentList(model, mFilterCardPaymentList_active, mCardPaymentPayTypeMap_active);
            SharedPreUtils.saveObject(mFilterCardPaymentList_active, "cardPayment_ActiveDynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());

            //读取上面过滤出来的，已开通的支付类型，如果已开通的支付类型里，有卡交易通道，则把参数置成true-----V3.1.0版本新增
            if (mFilterCardPaymentList_active != null && mFilterCardPaymentList_active.size() > 0) {
                MainApplication.getInstance().setHasCardPayment(true);
            } else {
                MainApplication.getInstance().setHasCardPayment(false);
            }

            //过滤预授权支持类型
                   /* List<DynModel> filterLst_pre_auth = new ArrayList<DynModel>();
                    Map<String, String> payTypeMap_auth = new HashMap<String, String>();
                    filterList_pre_auth(model, filterLst_pre_auth, payTypeMap_auth);
                    SharedPreUtils.saveObject(filterLst_pre_auth, "dynPayType_pre_auth" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());*/

            //过滤预授权支持类型---写死在前端，写死成支付宝
            List<DynModel> filterLst_pre_auth = new ArrayList<DynModel>();
            Map<String, String> payTypeMap_auth = new HashMap<String, String>();
            DynModel dynModel = new DynModel();
            dynModel.setNativeTradeType(Constant.NATIVE_ALIPAY_AUTH);
            dynModel.setApiCode("2");
            filterLst_pre_auth.add(dynModel);
            payTypeMap_auth.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
            SharedPreUtils.saveObject(filterLst_pre_auth, "dynPayType_pre_auth" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());


            //流水过滤条件筛选----用来用来筛选流水的字段，V3.1.0改了，废弃此字段，不用了
            List<DynModel> filterLstStream = new ArrayList<DynModel>();
            filterListStream(model, filterLstStream);
            SharedPreUtils.saveObject(filterLstStream, "filterLstStream" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());

            //过滤固定二维码支付类型
            filterStacticList(model, filterStaticLst);
            SharedPreUtils.saveObject(filterStaticLst, "dynPayTypeStatic" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());

            Map<String, String> typePicMap = new HashMap<String, String>();
            // 颜色map集合
            Map<String, String> colorMap = new HashMap<String, String>();

            Map<String, DynModel> payTypeNameMap = new HashMap<String, DynModel>();

            for (DynModel d : model) {
                if (!StringUtil.isEmptyOrNull(d.getSmallIconUrl())) {
                    typePicMap.put(d.getApiCode(), d.getSmallIconUrl());
                }

                if (!StringUtil.isEmptyOrNull(d.getColor())) {

                    colorMap.put(d.getApiCode(), d.getColor());

                }
                if (!StringUtil.isEmptyOrNull(d.getProviderName())) {
                    payTypeNameMap.put(d.getApiCode(), d);
                }

            }
            SharedPreUtils.saveObject(payTypeNameMap, "payTypeNameMap" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            SharedPreUtils.saveObject(payTypeMap, "payTypeMap" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            //V3.1.0新增，卡通道
            SharedPreUtils.saveObject(mCardPaymentPayTypeMap, "mCardPaymentPayTypeMap" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            SharedPreUtils.saveObject(payTypeMap_auth, "payTypeMap_pre_auth" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            //图片
            SharedPreUtils.saveObject(typePicMap, "payTypeIcon" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            //                    //颜色值
            SharedPreUtils.saveObject(colorMap, "payTypeColor" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());

        }
    }


    /**
     * 获取所有的支付方式
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void saveAllpayList(List<DynModel> list, List<DynModel> filterLst) {
        for (DynModel dynModel : list) {
            filterLst.add(dynModel);
        }
    }

    /**
     * 过滤支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */

    private void filterListStream(List<DynModel> list, List<DynModel> filterLst) {

        for (DynModel dynModel : list) {
//            if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
            for (String key : MainApplication.getInstance().getApiProviderMap().keySet()) {
                if (key.equals(dynModel.getApiCode())) {
                    filterLst.add(dynModel);
                    break;
                }

            }
//            }
        }
    }

    /**
     * 过滤固定二维码支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void filterStacticList(List<DynModel> list, List<DynModel> filterLst) {

        if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getUserInfo().serviceType)) {
            String[] arrPays = MainApplication.getInstance().getUserInfo().serviceType.split("\\|");

            for (DynModel dynModel : list) {
                if (!StringUtil.isEmptyOrNull(dynModel.getFixedCodeTradeType())) {
                    for (String key : MainApplication.getInstance().getApiProviderMap().keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if (dynModel.getFixedCodeTradeType().contains(",")) {
                                    String[] arrFixedCodeTradeType = dynModel.getFixedCodeTradeType().split(",");
                                    for (int j = 0; j < arrFixedCodeTradeType.length; j++) {
                                        if (arrFixedCodeTradeType[j].equals(arrPays[i])) {
                                            filterLst.add(dynModel);
                                            break;
                                        }
                                    }
                                } else if (arrPays[i].equals(dynModel.getFixedCodeTradeType())) {
                                    filterLst.add(dynModel);
                                    break;
                                }

                               /* if (arrPays[i].startsWith(dynModel.getFixedCodeTradeType()) || dynModel.getFixedCodeTradeType().contains(arrPays[i]) || arrPays[i].equals(dynModel.getFixedCodeTradeType())) {
                                    filterLst.add(dynModel);
                                    break;
                                }*/
                            }
                        }
                    }

                } else {
                    continue;
                }
            }
        }

    }

    /**
     * 过滤支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void filterList(List<DynModel> list, List<DynModel> filterLst, Map<String, String> payTypeMap) {

        if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getUserInfo().serviceType)) {
            String[] arrPays = MainApplication.getInstance().getUserInfo().serviceType.split("\\|");

            for (DynModel dynModel : list) {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                    for (String key : MainApplication.getInstance().getApiProviderMap().keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if (dynModel.getNativeTradeType().contains(",")) {
                                    String[] arrNativeTradeType = dynModel.getNativeTradeType().split(",");
                                    for (int j = 0; j < arrNativeTradeType.length; j++) {
                                        if (arrNativeTradeType[j].equals(arrPays[i])) {
                                          /*  filterLst.add(dynModel);
                                            payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);*/
                                            //如果登录接口返回的dynModel里面的nativeTradeType里有多种支付类型，但是如果是已开通的支付方式里
                                            //有两条一模一样的数据就会有问题，就会匹配到两条数据，看是app做去重还是server做去重。
                                            //如果后台没办法做到去重的逻辑，需要前端APP做的话，就用下面的一段代码
                                            if (payTypeMap.get(dynModel.getApiCode()) != null &&
                                                    payTypeMap.get(dynModel.getApiCode()).equals(arrPays[i])) {
                                            } else {
                                                filterLst.add(dynModel);
                                                payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);
                                            }
                                            break;
                                        }
                                    }
                                } else if (arrPays[i].equals(dynModel.getNativeTradeType())) {

                                    //这个地方的逻辑是有点问题，以后有空再改
                                    //如果登录接口返回的dynModel里面的nativeTradeType里只有一种支付类型，但是如果是已开通的支付方式里
                                    //有两条一模一样的数据就会有问题，只匹配到一条数据，然后就直接break跳出循环了，这个时候就做了去重处理，只展示一条
                                    filterLst.add(dynModel);
                                    payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    break;
                                }
                                /*if (arrPays[i].startsWith(dynModel.getNativeTradeType()) || dynModel.getNativeTradeType().contains(arrPays[i]) || arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    filterLst.add(dynModel);
                                    if (dynModel.getNativeTradeType().contains(",")) {
                                        payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType().substring(0, dynModel.getNativeTradeType().lastIndexOf(",")));
                                    } else {
                                        payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    }
                                    break;
                                }*/
                            }

                        }
                    }

                } else {
                    continue;
                }
            }
        }

    }


    /**
     * 过滤支付类型
     * <功能详细描述>
     *
     * @param list
     * @see [类、类#方法、类#成员]
     */
    private void filterList_active(List<DynModel> list, List<DynModel> filterLst, Map<String, String> payTypeMap) {

        if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getUserInfo().activateServiceType)) {
            String[] arrPays = MainApplication.getInstance().getUserInfo().activateServiceType.split("\\|");

            for (DynModel dynModel : list) {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                    for (String key : MainApplication.getInstance().getApiProviderMap().keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if (dynModel.getNativeTradeType().contains(",")) {
                                    String[] arrNativeTradeType = dynModel.getNativeTradeType().split(",");
                                    for (int j = 0; j < arrNativeTradeType.length; j++) {
                                        if (arrNativeTradeType[j].equals(arrPays[i])) {
                                          /*  filterLst.add(dynModel);
                                            payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);*/

                                            //如果登录接口返回的dynModel里面的nativeTradeType里有多种支付类型，但是如果是已开通的支付方式里
                                            //有两条一模一样的数据就会有问题，就会匹配到两条数据，看是app做去重还是server做去重。
                                            //如果后台没办法做到去重的逻辑，需要前端APP做的话，就用下面的一段代码
                                            if (payTypeMap.get(dynModel.getApiCode()) != null &&
                                                    payTypeMap.get(dynModel.getApiCode()).equals(arrPays[i])) {
                                            } else {
                                                filterLst.add(dynModel);
                                                payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);
                                            }
                                            break;
                                        }
                                    }
                                } else if (arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    //这个地方的逻辑是有点问题，以后有空再改
                                    //如果登录接口返回的dynModel里面的nativeTradeType里只有一种支付类型，但是如果是已开通的支付方式里
                                    //有两条一模一样的数据就会有问题，只匹配到一条数据，然后就直接break跳出循环了，这个时候就做了去重处理，只展示一条
                                    filterLst.add(dynModel);
                                    payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    break;
                                }
                                /*if (arrPays[i].startsWith(dynModel.getNativeTradeType()) || dynModel.getNativeTradeType().contains(arrPays[i]) || arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    filterLst.add(dynModel);
                                    if (dynModel.getNativeTradeType().contains(",")) {
                                        payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType().substring(0, dynModel.getNativeTradeType().lastIndexOf(",")));
                                    } else {
                                        payTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    }
                                    break;
                                }*/
                            }

                        }
                    }

                } else {
                    continue;
                }
            }
        }

    }


    @Override
    public void ecdhKeyExchangeSuccess(ECDHInfo response) {
        if (response != null) {
            try {
                final String privateKey = SharedPreUtils.readProduct(ParamsConstants.APP_PRIVATE_KEY).toString();
                String secretKey = ECDHUtils.getInstance().ecdhGetShareKey(MainApplication.getInstance().getSerPubKey(), privateKey);
                SharedPreUtils.saveObject(secretKey, ParamsConstants.SECRET_KEY);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //此时直接调用设备登录接口
                        deviceLogin();
                    }
                });

            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
            }
        }
    }

    @Override
    public void ecdhKeyExchangeFailed(@Nullable Object error) {
        if (error != null) {
            if (error.toString().contains("401")) {
                //如果是会话过期，维持原逻辑，什么都不做
            } else if (error.toString().contains(RequestResult.HANDSHAKE_CER_ERROR)) {
                //如果一键重登过程中发现证书失效，则先去调用获取公钥接口
                if (isCerFailed) {//已经请求过证书
                    //直接弹框报错
                    toastDialog(ReLoginActivity.this, getStringById(R.string.certificate_request_failed), null);
                } else {
                    requestCertificate(true);
                }

            } else {
                //如果是公钥交换的其他错误，否则弹框提示
                toastDialog(ReLoginActivity.this, error.toString(), null);
            }
        }
    }

    private void ECDHKeyExchange() {
        try {
            ECDHUtils.getInstance().getAppPubKey();
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
        final String publicKey = SharedPreUtils.readProduct(ParamsConstants.APP_PUBLIC_KEY).toString();

        if (mPresenter != null) {
            mPresenter.ecdhKeyExchange(publicKey);
        }
    }


    @Override
    public void logoutSuccess(boolean response) {
        clearAPPState();
    }

    @Override
    public void logoutFailed(@Nullable Object error) {
        clearAPPState();
    }

    public void logout() {
        if (mPresenter != null) {
            mPresenter.logout();
        }
    }


    //APP的本地退出操作，清掉本地缓存
    public void clearAPPState() {
        //删掉本地缓存，清除登录状态
        deleteSharedPre();

        for (Activity a : MainApplication.getInstance().getAllActivities()) {
            a.finish();
        }

        Intent it = new Intent();
        it.setClass(getApplicationContext(), WelcomeActivity.class);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(it);

        PreferenceUtil.removeKey("login_skey");
        PreferenceUtil.removeKey("login_sauthid");
        PreferenceUtil.removeKey("choose_pay_or_pre_auth");
        PreferenceUtil.removeKey("bill_list_choose_pay_or_pre_auth");
        PreferenceUtil.removeKey("userPwd");
        MainApplication.getInstance().setUserInfo(null);

        finish();
    }


    @Override
    public void getCertificateStringSuccess(CertificateBean response, boolean isDeviceLogin) {
        //最新配置的公钥信息
        MainApplication.getInstance().setCertificateBean(response);
        if (isDeviceLogin) {//如果是设备登录请求的时候，先请求证书公钥,成功之后再走正常逻辑
            //先去调用公钥交换接口然后再调用设备登录接口,每次设备登录前都主动去调用ECDH去交换
            if (response != null) {
                ECDHKeyExchange();
            }
        }
    }

    @Override
    public void getCertificateStringFailed(@Nullable Object error, boolean isDeviceLogin) {
        if (isDeviceLogin) {//如果是设备登录请求的时候，先请求证书公钥
            //获取公钥证书失败，则弹框提示
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    toastDialog(ReLoginActivity.this, getStringById(R.string.certificate_request_failed), null);
                }
            });
        }
    }

    /*
     **如果证书已过期或者证书校验失败
     * 请求CA证书公钥获取接口得到最新CA证书公钥
     * */
    public void requestCertificate(boolean isDeviceLogin) {
        if (mPresenter != null) {
            mPresenter.getCertificateString(isDeviceLogin);
        }
    }

    //过滤卡通道类型
    public void filterCardPaymentList(List<DynModel> modelList, List<DynModel> mFilterCardPaymentList, Map<String, String> mCardPaymentPayTypeMap) {
        //全部的卡交易通道
        if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getUserInfo().cardServiceType)) {
            String[] arrPays = MainApplication.getInstance().getUserInfo().cardServiceType.split("\\|");
            for (DynModel dynModel : modelList) {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                    for (String key : MainApplication.getInstance().getApiProviderMap().keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if (dynModel.getNativeTradeType().contains(",")) {
                                    String[] arrNativeTradeType = dynModel.getNativeTradeType().split(",");
                                    for (int j = 0; j < arrNativeTradeType.length; j++) {
                                        if (arrNativeTradeType[j].equals(arrPays[i])) {
                                         /*   filterLst.add(dynModel);
                                            payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);*/

                                            //如果登录接口返回的dynModel里面的nativeTradeType里有多种支付类型，但是如果是已开通的支付方式里
                                            //有两条一模一样的数据就会有问题，就会匹配到两条数据，看是app做去重还是server做去重。
                                            //如果后台没办法做到去重的逻辑，需要前端APP做的话，就用下面的一段代码
                                            if (mCardPaymentPayTypeMap.get(dynModel.getApiCode()) != null &&
                                                    mCardPaymentPayTypeMap.get(dynModel.getApiCode()).equals(arrPays[i])) {
                                            } else {
                                                mFilterCardPaymentList.add(dynModel);
                                                mCardPaymentPayTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);
                                            }
                                            break;
                                        }
                                    }
                                } else if (arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    //这个地方的逻辑是有点问题，以后有空再改
                                    //如果登录接口返回的dynModel里面的nativeTradeType里只有一种支付类型，但是如果是已开通的支付方式里
                                    //有两条一模一样的数据就会有问题，只匹配到一条数据，然后就直接break跳出循环了，这个时候就做了去重处理，只展示一条
                                    mFilterCardPaymentList.add(dynModel);
                                    mCardPaymentPayTypeMap.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    break;
                                }
                            }
                        }
                    }

                } else {
                    continue;
                }
            }
        }
    }


    //过滤已开通的卡通道类型
    public void filterActiveCardPaymentList(List<DynModel> modelList, List<DynModel> mFilterCardPaymentList_active, Map<String, String> mCardPaymentPayTypeMap_active) {
        //全部已开通的卡交易通道
        if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getUserInfo().activateCardServiceType)) {
            String[] arrPays = MainApplication.getInstance().getUserInfo().activateCardServiceType.split("\\|");
            for (DynModel dynModel : modelList) {
                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
                    for (String key : MainApplication.getInstance().getApiProviderMap().keySet()) {
                        if (key.equals(dynModel.getApiCode())) {
                            for (int i = 0; i < arrPays.length; i++) {
                                if (dynModel.getNativeTradeType().contains(",")) {
                                    String[] arrNativeTradeType = dynModel.getNativeTradeType().split(",");
                                    for (int j = 0; j < arrNativeTradeType.length; j++) {
                                        if (arrNativeTradeType[j].equals(arrPays[i])) {
                                         /*   filterLst.add(dynModel);
                                            payTypeMap.put(dynModel.getApiCode(), arrNativeTradeType[j]);*/

                                            //如果登录接口返回的dynModel里面的nativeTradeType里有多种支付类型，但是如果是已开通的支付方式里
                                            //有两条一模一样的数据就会有问题，就会匹配到两条数据，看是app做去重还是server做去重。
                                            //如果后台没办法做到去重的逻辑，需要前端APP做的话，就用下面的一段代码
                                            if (mCardPaymentPayTypeMap_active.get(dynModel.getApiCode()) != null &&
                                                    mCardPaymentPayTypeMap_active.get(dynModel.getApiCode()).equals(arrPays[i])) {
                                            } else {
                                                mFilterCardPaymentList_active.add(dynModel);
                                                mCardPaymentPayTypeMap_active.put(dynModel.getApiCode(), arrNativeTradeType[j]);
                                            }
                                            break;
                                        }
                                    }
                                } else if (arrPays[i].equals(dynModel.getNativeTradeType())) {
                                    //这个地方的逻辑是有点问题，以后有空再改
                                    //如果登录接口返回的dynModel里面的nativeTradeType里只有一种支付类型，但是如果是已开通的支付方式里
                                    //有两条一模一样的数据就会有问题，只匹配到一条数据，然后就直接break跳出循环了，这个时候就做了去重处理，只展示一条
                                    mFilterCardPaymentList_active.add(dynModel);
                                    mCardPaymentPayTypeMap_active.put(dynModel.getApiCode(), dynModel.getNativeTradeType());
                                    break;
                                }
                            }
                        }
                    }

                } else {
                    continue;
                }
            }
        }
    }


}
