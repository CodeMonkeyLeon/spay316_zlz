package cn.swiftpass.enterprise.ui.accountcancel.entity

import java.io.Serializable

data class AccountCancelVerifyResultEntity(val verifyFlag: String = "") : Serializable
