package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tencent.stat.StatService;

import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.print.PrintOrder;
import cn.swiftpass.enterprise.ui.activity.print.BluePrintUtil;
import cn.swiftpass.enterprise.ui.activity.print.BluetoothSettingActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DataReportUtils;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftplus.enterprise.printsdk.print.PrintClient;

import static cn.swiftpass.enterprise.intl.BuildConfig.IS_POS_VERSION;

/**
 * Created by aijingya on 2021/3/10.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description:
 * @date 2021/3/10.15:48.
 */
public class MasterCardPayResultActivity extends TemplateActivity{
    private static final String TAG = MasterCardPayResultActivity.class.getSimpleName();
    private TextView tv_total_money_fee;
    private LinearLayout ll_total_amount,ll_surcharge,ll_cashier,ll_trasaction_id,ll_attach,ll_order_type;
    private View view_surcharge_line,view_cashier_line;
    private TextView tv_total;
    private TextView tv_surcharge;
    private TextView tv_order_status;
    private TextView tv_order_time;
    private TextView tv_pay_order;
    private TextView tv_transaction_method;
    private TextView tv_trasaction_id_title;
    private TextView tv_transactionId;
    private TextView tv_plantform_order_id;
    private TextView tv_order_type;
    private TextView tv_attach;
    private View view_line_attach,view_order_type_line;
    private Button btn_finish;

    private Order orderInfo;
    private boolean isCashierVisiable = false;

    public static void startActivity(Context mContext, Order order) {
        Intent it = new Intent();
        it.setClass(mContext, MasterCardPayResultActivity.class);
        it.putExtra("order", order);
        mContext.startActivity(it);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_payment_pay_result);
        MainApplication.listActivities.add(this);

        orderInfo = (Order) getIntent().getSerializableExtra("order");

        initView();

        setLister();

        initUIDate();

        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印
        if(IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)){
            //设置自动打印
            if(orderInfo != null){
                print(orderInfo);
            }
        }else{
            if (MainApplication.getAutoBluePrintSetting()) {//设置自动打印
                MasterCardPayResultActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        print(orderInfo);
                    }
                });
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印的绑定操作
        if(IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)){
            PrintClient.getInstance().bindDeviceService();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印的解绑操作
        if(IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)){
            PrintClient.getInstance().unbindDeviceService();
        }
    }

    private void print(final Order orderModel) {
        //调用打印机打印
        //如果是POS版本，同时有打印机型号，则走相应POS的SDK打印
        if(IS_POS_VERSION && !TextUtils.isEmpty(BuildConfig.posTerminal)){
            PrintOrder.printMasterCardOrderDetails(MasterCardPayResultActivity.this, isCashierVisiable,true,false,getString(R.string.tv_pay_user_stub),orderModel);
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    try {
                        PrintOrder.printMasterCardOrderDetails(MasterCardPayResultActivity.this, isCashierVisiable,true,false,getString(R.string.tv_pay_mch_stub),orderModel);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            Timer timer = new Timer();
            timer.schedule(task, PrintClient.getInstance().getPrintDelay());

        }else{
            if (!MainApplication.getBluePrintSetting() && !MainApplication.IS_POS_VERSION) {//关闭蓝牙打印
                showDialog();
                return;
            }

            if (MainApplication.bluetoothSocket != null && MainApplication.bluetoothSocket.isConnected()) {
                MasterCardPayResultActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            BluePrintUtil.printMasterCardOrder(isCashierVisiable,true,false,getString(R.string.tv_pay_user_stub),orderModel);
                            //打印第二联
                            try {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        BluePrintUtil.printMasterCardOrder(isCashierVisiable,true,false,getString(R.string.tv_pay_mch_stub),orderModel);
                                    }
                                }, 3000);
                            } catch (Exception e) {
                                Log.e(TAG, Log.getStackTraceString(e));
                            }
                        } catch (Exception e) {
                            Log.e(TAG,Log.getStackTraceString(e));
                        }
                    }
                });
            } else {
                String bluetoothDeviceAddress = MainApplication.getBlueDeviceAddress();
                if (!StringUtil.isEmptyOrNull(bluetoothDeviceAddress)) {
                    boolean isSucc = BluePrintUtil.blueConnent(bluetoothDeviceAddress, MasterCardPayResultActivity.this);
                    if (isSucc) {
                        try {
                            BluePrintUtil.printMasterCardOrder(isCashierVisiable,true,false,getString(R.string.tv_pay_user_stub),orderModel);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    BluePrintUtil.printMasterCardOrder(isCashierVisiable,true,false,getString(R.string.tv_pay_mch_stub),orderModel);
                                }
                            }, 3000);
                        } catch (Exception e) {
                            Log.e(TAG,Log.getStackTraceString(e));
                        }
                    }
                } else {
                    showDialog();
                }
            }
        }
    }

    public void showDialog() {
        dialog = new DialogInfo(MasterCardPayResultActivity.this, null, getString(R.string.tx_blue_set), getString(R.string.title_setting), getString(R.string.btnCancel),
                DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {
            @Override
            public void handleOkBtn() {
                dialog.cancel();
                dialog.dismiss();
                showPage(BluetoothSettingActivity.class);
            }

            @Override
            public void handleCancleBtn() {
                dialog.cancel();
            }
        }, null);
        DialogHelper.resize(MasterCardPayResultActivity.this, dialog);
        dialog.show();
    }


    private void initView(){
        tv_total_money_fee  = findViewById(R.id.tv_total_money_fee);

        ll_total_amount  = findViewById(R.id.ll_total_amount);
        ll_surcharge  = findViewById(R.id.ll_surcharge);
        view_surcharge_line = findViewById(R.id.view_surcharge_line);
        ll_cashier  = findViewById(R.id.ll_cashier);
        view_cashier_line = findViewById(R.id.view_cashier_line);
        ll_trasaction_id  = findViewById(R.id.ll_trasaction_id);
        ll_order_type = findViewById(R.id.ll_order_type);
        ll_attach = findViewById(R.id.ll_attach);
        tv_total  = findViewById(R.id.tv_total);
        tv_surcharge  = findViewById(R.id.tv_surcharge);
        tv_order_status  = findViewById(R.id.tv_order_status);
        tv_order_time  = findViewById(R.id.tv_order_time);
        tv_pay_order  = findViewById(R.id.tv_pay_order);

        tv_transaction_method  = findViewById(R.id.tv_transaction_method);
        tv_trasaction_id_title  = findViewById(R.id.tv_trasaction_id_title);
        tv_transactionId  = findViewById(R.id.tv_transactionId);
        tv_plantform_order_id  = findViewById(R.id.tv_plantform_order_id);
        tv_order_type = findViewById(R.id.tv_order_type);
        tv_attach = findViewById(R.id.tv_attach);
        view_line_attach = findViewById(R.id.view_line_attach);
        view_order_type_line = findViewById(R.id.view_order_type_line);
        btn_finish  = findViewById(R.id.btn_finish);
    }


    private void initUIDate(){
        if(orderInfo != null){
            tv_total_money_fee.setText(MainApplication.getFeeFh() + " " + DateUtil.formatMoneyUtils(orderInfo.getMoney()));

            //如果开通surcharge，则展示
            if(MainApplication.isSurchargeOpen()){
                ll_total_amount.setVisibility(View.VISIBLE);
                ll_surcharge.setVisibility(View.VISIBLE);
                view_surcharge_line.setVisibility(View.VISIBLE);

                tv_total.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(orderInfo.getOrderAmount()));
                tv_surcharge.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(orderInfo.getSurcharge()));
            }else{
                ll_total_amount.setVisibility(View.GONE);
                ll_surcharge.setVisibility(View.GONE);
                view_surcharge_line.setVisibility(View.GONE);
            }

            //订单状态 交易状态：(1.初始化，2：支付成功，3：支付失败)
            switch (orderInfo.getTradeState()){
                case 1:
                    //初始化
                    tv_order_status.setTextColor(getResources().getColor(R.color.pay_other_status));
                    tv_order_status.setText(getStringById(R.string.card_payment_status_init));
                    break;
                case 2:
                    tv_order_status.setTextColor(getResources().getColor(R.color.bill_item_succ));
                    tv_order_status.setText(getStringById(R.string.card_payment_status_success));
                    break;
                case 3:
                    tv_order_status.setTextColor(getResources().getColor(R.color.pay_other_status));
                    tv_order_status.setText(getStringById(R.string.card_payment_status_failed));
                    break;
                default:
                    tv_order_status.setTextColor(getResources().getColor(R.color.pay_other_status));
                    tv_order_status.setText("");
                    break;
            }

            if(!TextUtils.isEmpty(orderInfo.getTradeType()) && MainApplication.getCardPaymentTradeStateMap() != null){
                ll_order_type.setVisibility(View.VISIBLE);
                view_order_type_line.setVisibility(View.VISIBLE);
                tv_order_type.setText(MainApplication.getCardPaymentTradeStateMap().get(orderInfo.getTradeType()));
            }else{
                ll_order_type.setVisibility(View.GONE);
                view_order_type_line.setVisibility(View.GONE);
            }

            if (!StringUtil.isEmptyOrNull(orderInfo.getTradeTime())) {
                tv_order_time.setText(orderInfo.getTradeTime());
            } else {
                tv_order_time.setText(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
            }

            if (!TextUtils.isEmpty(orderInfo.getUserName())) {
                isCashierVisiable = true;
            } else {
                isCashierVisiable = false;
            }
            if (MainApplication.isAdmin.equals("0")) {
                ll_cashier.setVisibility(View.VISIBLE);
                view_cashier_line.setVisibility(View.VISIBLE);
                tv_pay_order.setText(orderInfo.getUserName());
            } else {
                ll_cashier.setVisibility(View.GONE);
                view_cashier_line.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(orderInfo.getTradeName())) {
                tv_transaction_method.setText(orderInfo.getTradeName());
            }

          /*  String language = PreferenceUtil.getString("language","");
            Locale locale = MainApplication.getContext().getResources().getConfiguration().locale; //zh-rHK
            String lan = locale.getCountry();
            if (!StringUtil.isEmptyOrNull(MainApplication.getPayTypeMap().get(orderInfo.getApiCode()))) {
                if (!TextUtils.isEmpty(language)) {
                    if (language.equals(MainApplication.LANG_CODE_EN_US)) {
                        tv_trasaction_id_title.setText(MainApplication.getPayTypeMap().get(orderInfo.getApiCode()) + " " + getString(R.string.tx_orderno));
                    } else {
                        tv_trasaction_id_title.setText(MainApplication.getPayTypeMap().get(orderInfo.getApiCode()) + getString(R.string.tx_orderno));
                    }
                } else {
                    if (lan.equalsIgnoreCase("en")) {
                        tv_trasaction_id_title.setText(MainApplication.getPayTypeMap().get(orderInfo.getApiCode()) + " " + getString(R.string.tx_orderno));
                    } else {
                        tv_trasaction_id_title.setText(MainApplication.getPayTypeMap().get(orderInfo.getApiCode()) + getString(R.string.tx_orderno));
                    }
                }
            }

            tv_transactionId.setText(orderInfo.getTransactionId());*/
            tv_plantform_order_id.setText(orderInfo.getOrderNoMch());

            //付款备注
            if (!StringUtil.isEmptyOrNull(orderInfo.getAttach())) {
                ll_attach.setVisibility(View.VISIBLE);
                view_line_attach.setVisibility(View.VISIBLE);
                tv_attach.setText(orderInfo.getAttach());
            }else{
                ll_attach.setVisibility(View.GONE);
                view_line_attach.setVisibility(View.GONE);
            }
        }
    }

    private void setLister(){
        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainApplication.listActivities.size() > 0) {
                    for (Activity a : MainApplication.listActivities) {
                        if(!(a instanceof spayMainTabActivity)){
                            a.finish();
                        }
                    }
                }
                HandlerManager.notifyMessage(HandlerManager.PAY_FINISH, HandlerManager.PAY_FINISH,null);
            }
        });
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(getStringById(R.string.title_order_detail));
        titleBar.setLeftButtonVisible(true);
        titleBar.setRightButtonVisible(false);
        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.print));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                if (MainApplication.listActivities.size() > 0) {
                    for (Activity a : MainApplication.listActivities) {
                        if(!(a instanceof spayMainTabActivity)){
                            a.finish();
                        }
                    }
                }
            }

            @Override
            public void onRightButtonClick() {
                try {
                    StatService.trackCustomEvent(MasterCardPayResultActivity.this, "kMTASPayPayOrderPrint", "收款成功打印");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                // 1.携带参数的打点
                Bundle values = new Bundle();
                values.putString("kGFASPayPayOrderPrint","收款成功“打印”按钮");
                DataReportUtils.getInstance().report("kGFASPayPayOrderPrint",values);
                //打印
                print(orderInfo);
            }

            @Override
            public void onRightLayClick() {
            }

            @Override
            public void onRightButLayClick() {
                //打印
                print(orderInfo);
            }
        });
    }

    @Override
    public boolean onKeyDown(int keycode, KeyEvent event) {
        if (keycode == KeyEvent.KEYCODE_BACK) {
            if (MainApplication.listActivities.size() > 0) {
                for (Activity a : MainApplication.listActivities) {
                    if(a instanceof spayMainTabActivity){

                    }else{
                        a.finish();
                    }
                }
            }
        }
        return super.onKeyDown(keycode, event);
    }

}
