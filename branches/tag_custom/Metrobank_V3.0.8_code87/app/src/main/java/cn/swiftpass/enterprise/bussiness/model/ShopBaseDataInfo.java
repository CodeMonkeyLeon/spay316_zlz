/*
 * 文 件 名:  ShopBaseDataInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2014-3-20
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

import cn.swiftpass.enterprise.io.database.table.ShopBaseDataTable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2014-3-20]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
@DatabaseTable(tableName = ShopBaseDataTable.TABLE_NAME)
public class ShopBaseDataInfo implements Serializable
{
    
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    @DatabaseField(generatedId = true)
    public long id;
    
    //    @DatabaseField(columnName = ShopBaseDataTable.COLUMN_ID)
    //    public long uId;
    
    //    @DatabaseField(columnName = ShopBaseDataTable.COLUMN_MERCHANT_TYPEID)
    //    public String merchantTypeId;
    
    @DatabaseField(columnName = ShopBaseDataTable.COLUMN_TYPE_NAME)
    public String name;
    
    @DatabaseField(columnName = ShopBaseDataTable.COLUMN_PARENT_ID)
    public String typeno;
    
    /**
     * @return 返回 name
     */
    public String getName()
    {
        return name;
    }
    
    /**
     * @param 对name进行赋值
     */
    public void setName(String name)
    {
        this.name = name;
    }
    
    /**
     * @return 返回 typeno
     */
    public String getTypeno()
    {
        return typeno;
    }
    
    /**
     * @param 对typeno进行赋值
     */
    public void setTypeno(String typeno)
    {
        this.typeno = typeno;
    }
    
    //    /**
    //     * @return 返回 merchantTypeId
    //     */
    //    public String getMerchantTypeId()
    //    {
    //        return merchantTypeId;
    //    }
    //    
    //    /**
    //     * @param 对merchantTypeId进行赋值
    //     */
    //    public void setMerchantTypeId(String merchantTypeId)
    //    {
    //        this.merchantTypeId = merchantTypeId;
    //    }
    
}
