package cn.swiftpass.enterprise.bussiness.logica.threading;

public class NotifyListener<T> {
    public NotifyListener() {
        onPreExecute();
    }

    protected void onPreExecute() {

    }

    protected void onPostExecute() {

    }

    public void onSucceed(T result) {
        onPostExecute();
    }

    public void onError(Object result) {
        onPostExecute();
    }

    public void onProgress(int progress) {

    }
}
