package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.ShareOrderContract
import cn.swiftpass.enterprise.ui.paymentlink.model.OrderConfig
import cn.swiftpass.enterprise.ui.paymentlink.model.ShareInfo
import cn.swiftpass.enterprise.utils.JsonUtil
import com.example.common.entity.EmptyData
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/6
 *
 * @在此地
 * @在国际的鸡尾酒里
 * @我仍是一块拒绝溶化的冰
 * @常保持零下的冷和固体的硬度
 */
class ShareOrderPresenter : ShareOrderContract.Presenter {

    private var mView: ShareOrderContract.View? = null


    override fun getOrderConfig() {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
            AppClient.getOrderConfig(
                "2",
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<OrderConfig>(OrderConfig()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.getOrderConfigFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                OrderConfig::class.java
                            ) as OrderConfig
                            view.getOrderConfigSuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun sendEmail(s: ShareInfo?) {
        mView?.let { view ->
            s?.let { shareInfo ->

                view.showLoading(
                    R.string.public_data_loading,
                    ParamsConstants.COMMON_LOADING
                )
                AppClient.sendEmail(
                    "7",
                    shareInfo.orderNo,
                    shareInfo.custEmail,
                    shareInfo.emailSubject,
                    shareInfo.emailContent,
                    System.currentTimeMillis().toString(),
                    object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {

                        override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            commonResponse?.let {
                                view.sendEmailFailed(it.message)
                            }
                        }

                        override fun onResponse(response: CommonResponse?) {
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            response?.let {
                                view.sendEmailSuccess(it.message)
                            }
                        }
                    }
                )
            }
        }
    }

    override fun sendMessage(s: ShareInfo?) {
        mView?.let { view ->
            s?.let { shareInfo ->
                view.showLoading(
                    R.string.public_data_loading,
                    ParamsConstants.COMMON_LOADING
                )
                AppClient.sendMessage(
                    "8",
                    shareInfo.orderNo,
                    shareInfo.custMobile,
                    System.currentTimeMillis().toString(),
                    object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {
                        override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            commonResponse?.let {
                                view.sendMessageFailed(it.message)
                            }
                        }

                        override fun onResponse(response: CommonResponse?) {
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            response?.let {
                                view.sendMessageSuccess(it.message)
                            }
                        }
                    }
                )
            }
        }
    }

    override fun attachView(view: ShareOrderContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}