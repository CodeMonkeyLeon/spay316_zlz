package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @烟囱犹如平地耸立起来的巨人
 * @望着布满灯火的大地
 * @不断地吸着烟卷
 * @思索着一种谁也不知道的事情
 */
class PreAuthThawContract {


    interface View : BaseView {

        fun queryOrderDetailSuccess(response: Order?)

        fun queryOrderDetailFailed(error: Any?)

        fun authOperateQuerySuccess(response: ArrayList<Order>?)

        fun authOperateQueryFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {

        fun authOperateQuery(
            operationType: String?,
            authNo: String?,
            page: Int,
            pageSize: Int,
        )

        fun queryOrderDetail(
            orderNo: String?,
            mchId: String?,
            isMark: Boolean
        )

    }


}