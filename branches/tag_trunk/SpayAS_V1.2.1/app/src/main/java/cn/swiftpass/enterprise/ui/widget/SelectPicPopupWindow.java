package cn.swiftpass.enterprise.ui.widget;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.WalletModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

public class SelectPicPopupWindow extends PopupWindow {
    private TextView tv_cancel, tv_ok, tv_wx_pay, tv_zfb_pay, tv_qq_pay, tv_jd_pay;

    private View mMenuView;

    private String type;

    private TextView tv_scan;

    private ViewHolder holder;

    private ViewPayTypeHolder payholder;

    private ListView listView;

    private TextView title;

    List<WalletModel> model;

    private String choice;

    private TextView tv_cel;

    private ImageView iv_bank;

    private Button tv_wx, tv_zfb, tv_qq, tv_jd, tv_succ, tv_fail, tv_revers, tv_refund, tv_close, tv_tyep_refund, tv_card;

    private LinearLayout ly_reset;

    private TextView ly_ok;

    private List<String> choiceType = new ArrayList<String>();

    private List<Button> listBut = new ArrayList<Button>();

    //    private Map<String, String> payTypeMap = new HashMap<String, String>();
    //    
    //    private Map<String, String> typeMap = new HashMap<String, String>();

    private SelectPicPopupWindow.HandleBtn handleBtn;

    private Activity activity;

    private boolean isTag = false;

    private SelectPicPopupWindow.HandleTv handleTv;

    private View v_wx, v_zfb, v_jd, v_qq;

    private GridView gv_pay_type;

    private PayTypeAdape payTypeAdape;

    private LinearLayout ly_pay_type;

    private List<DynModel> list;

    private List<String> payTypeList;

    public interface HandleTv {
        void takePic();

        void choicePic();
    }

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    public interface HandleBtn {
        void handleOkBtn(List<String> choiceType, List<String> payTypeList);
    }

    public SelectPicPopupWindow(Activity context, List<String> payList, List<String> payTypeList, SelectPicPopupWindow.HandleBtn handleBtn) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.pay_type_choice_dialog, null);
        this.handleBtn = handleBtn;
        this.activity = context;
        this.payTypeList = payTypeList;
        initview(mMenuView);
        setDate(payList);
        setLisert();
        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }

    private void setDate(List<String> payList) {
        if (null != payList && payList.size() > 0) {
            for (String s : payList) {
                if (s.equals(tv_wx.getTag().toString())) {
                    updatepayTypeButBg(tv_wx, "1");
                } else if (s.equals(tv_zfb.getTag().toString())) {
                    updatepayTypeButBg(tv_zfb, "1");
                } else if (s.equals(tv_qq.getTag().toString())) {
                    updatepayTypeButBg(tv_qq, "1");
                } else if (s.equals(tv_jd.getTag().toString())) {
                    updatepayTypeButBg(tv_jd, "1");
                } else if (s.equals(tv_succ.getTag().toString())) {
                    updatepayTypeButBg(tv_succ, "1");
                    setRefundEnabled();
                } else if (s.equals(tv_fail.getTag().toString())) {
                    updatepayTypeButBg(tv_fail, "1");
                    setRefundEnabled();
                } else if (s.equals(tv_revers.getTag().toString())) {
                    updatepayTypeButBg(tv_revers, "1");
                    setRefundEnabled();
                } else if (s.equals(tv_refund.getTag().toString())) {
                    updatepayTypeButBg(tv_refund, "1");
                    setRefundEnabled();
                } else if (s.equals(tv_close.getTag().toString())) {
                    updatepayTypeButBg(tv_close, "1");
                    setRefundEnabled();
                } else if (s.equals(tv_tyep_refund.getTag().toString())) {
                    updateTypeButBg(tv_tyep_refund, "4");
                } else if (s.equals(tv_card.getTag().toString())) {
                    updatePayTypeSetEnabled(false);
                    updateTypeButBg(tv_card, "4");
                    updateTypeSetCardEnabled();
                    for (Button b : listBut) {
                        if (!b.getTag().toString().equals("card")) {
                            setAllBut(b);
                        }
                    }
                }
            }
        }
    }

    void updatepayTypeButBg(Button b, String type) {
        isTag = false;

        if (choiceType.contains(b.getTag().toString())) {
            if (!choiceType.contains(tv_tyep_refund.getTag().toString())) {
                updateTypeOpen();
            }
            b.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            b.setTextColor(Color.parseColor("#000000"));
            choiceType.remove(b.getTag().toString());
            if (!isConPayTypeAndState()) {
                updateCardView();
            }
        } else {
            updateTypeSetEnabled();
            b.setBackgroundResource(R.drawable.general_button_main_default);
            b.setTextColor(Color.WHITE);
            choiceType.add(b.getTag().toString());
        }
    }

    boolean isConPayTypeAndState() {
        if (choiceType.contains(tv_wx.getTag().toString()) || choiceType.contains(tv_zfb.getTag().toString()) || choiceType.contains(tv_qq.getTag().toString()) || choiceType.contains(tv_jd.getTag().toString()) || choiceType.contains(tv_fail.getTag().toString()) || choiceType.contains(tv_revers.getTag().toString()) || choiceType.contains(tv_refund.getTag().toString()) || choiceType.contains(tv_close.getTag().toString())) {
            return true;
        }

        return false;
    }

    boolean isConPayState() {
        if (choiceType.contains(tv_fail.getTag().toString()) || choiceType.contains(tv_revers.getTag().toString()) || choiceType.contains(tv_refund.getTag().toString()) || choiceType.contains(tv_close.getTag().toString())) {
            return true;
        }

        return false;
    }

    //tv_wx, tv_zfb, tv_qq, tv_jd, tv_succ, tv_fail, tv_revers, tv_refund, tv_close, tv_tyep_refund,
    //tv_card;
    void updatePayTypeSetEnabled(boolean enable) {
        tv_wx.setEnabled(enable);
        tv_zfb.setEnabled(enable);
        tv_qq.setEnabled(enable);
        tv_jd.setEnabled(enable);
        tv_succ.setEnabled(enable);
        tv_fail.setEnabled(enable);
        tv_revers.setEnabled(enable);
        tv_refund.setEnabled(enable);
        tv_close.setEnabled(enable);
        tv_tyep_refund.setEnabled(enable);
        //        tv_card.setEnabled(enable);
    }

    void setPayTypeSetEnabled(boolean enable) {

        tv_succ.setEnabled(enable);
        tv_fail.setEnabled(enable);
        tv_revers.setEnabled(enable);
        tv_refund.setEnabled(enable);
        tv_close.setEnabled(enable);
        if (enable) {

            tv_succ.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            tv_succ.setTextColor(Color.parseColor("#000000"));

            tv_fail.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            tv_fail.setTextColor(Color.parseColor("#000000"));

            tv_revers.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            tv_revers.setTextColor(Color.parseColor("#000000"));

            tv_refund.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            tv_refund.setTextColor(Color.parseColor("#000000"));

            tv_close.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            tv_close.setTextColor(Color.parseColor("#000000"));

        } else {
            tv_succ.setBackgroundResource(R.drawable.general_button_grey_default);
            tv_succ.setTextColor(Color.parseColor("#bbbbbb"));

            tv_fail.setBackgroundResource(R.drawable.general_button_grey_default);
            tv_fail.setTextColor(Color.parseColor("#bbbbbb"));

            tv_revers.setBackgroundResource(R.drawable.general_button_grey_default);
            tv_revers.setTextColor(Color.parseColor("#bbbbbb"));

            tv_refund.setBackgroundResource(R.drawable.general_button_grey_default);
            tv_refund.setTextColor(Color.parseColor("#bbbbbb"));

            tv_close.setBackgroundResource(R.drawable.general_button_grey_default);
            tv_close.setTextColor(Color.parseColor("#bbbbbb"));

        }
    }

    void updateTypeSetCardEnabled() {

        //        tv_card.setEnabled(true);
        //        tv_tyep_refund.setEnabled(false);

        //        tv_tyep_refund.setBackgroundResource(R.drawable.general_button_grey_default);
        //        tv_tyep_refund.setTextColor(Color.parseColor("#bbbbbb"));

        //        tv_card.setBackgroundResource(R.drawable.general_button_main_default);
        //        tv_card.setTextColor(Color.WHITE);
    }

    void updateTypeSetEnabled() {

        tv_card.setEnabled(false);
        //        tv_tyep_refund.setEnabled(false);

        //        tv_tyep_refund.setBackgroundResource(R.drawable.general_button_grey_default);
        //        tv_tyep_refund.setTextColor(Color.parseColor("#bbbbbb"));

        tv_card.setBackgroundResource(R.drawable.general_button_grey_default);
        tv_card.setTextColor(Color.parseColor("#bbbbbb"));
    }

    void setRefundEnabled() {
        tv_tyep_refund.setEnabled(false);

        tv_tyep_refund.setBackgroundResource(R.drawable.general_button_grey_default);
        tv_tyep_refund.setTextColor(Color.parseColor("#bbbbbb"));
    }

    void updateTypeOpen() {
        tv_tyep_refund.setEnabled(true);
        tv_tyep_refund.setBackgroundResource(R.drawable.general_button_thickness_white_default);
        tv_tyep_refund.setTextColor(Color.parseColor("#000000"));

    }

    void updateCardView() {
        //        tv_card.setBackgroundResource(R.drawable.general_button_thickness_white_default);
        //        tv_card.setTextColor(Color.parseColor("#000000"));
        //        tv_card.setEnabled(true);
    }

    void updatepayStateButBg(Button b) {
        isTag = false;
        updateTypeSetEnabled();
        if (choiceType.contains(b.getTag().toString())) {
            b.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            choiceType.remove(b.getTag().toString());
            b.setTextColor(Color.parseColor("#000000"));
            if (!isConPayState()) {
                updateTypeOpen();
            }
            if (!isConPayTypeAndState()) {
                updateCardView();
            }
        } else {
            b.setBackgroundResource(R.drawable.general_button_main_default);
            choiceType.add(b.getTag().toString());
            b.setTextColor(Color.WHITE);
        }
    }

    void updateTypeButBg(Button b, String type) {
        if (choiceType.contains(b.getTag().toString())) {
            b.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            choiceType.remove(b.getTag().toString());
            b.setTextColor(Color.parseColor("#000000"));
            b.setEnabled(true);
            //            updatePayTypeSetEnabled(false);

            setPayTypeSetEnabled(true);
        } else {
            setPayTypeSetEnabled(false);
            //            updatePayTypeSetEnabled(true);
            b.setBackgroundResource(R.drawable.general_button_main_default);
            choiceType.add(b.getTag().toString());
            b.setTextColor(Color.WHITE);
        }
    }

    void updateTypeCardButBg(Button b, String type) {
        if (choiceType.contains(b.getTag().toString())) {
            b.setBackgroundResource(R.drawable.general_button_thickness_white_default);
            choiceType.remove(b.getTag().toString());
            b.setTextColor(Color.parseColor("#000000"));
            b.setEnabled(true);
            //            updatePayTypeSetEnabled(false);
            //            setPayTypeSetEnabled(true);
            for (Button bt : listBut) {
                if (!bt.getTag().toString().equals("card")) {
                    resetBut(bt);
                }
            }
        } else {
            //            setPayTypeSetEnabled(false);
            //            updatePayTypeSetEnabled(true);
            b.setBackgroundResource(R.drawable.general_button_main_default);
            b.setEnabled(true);
            choiceType.add(b.getTag().toString());
            b.setTextColor(Color.WHITE);
            for (Button bt : listBut) {
                if (!bt.getTag().toString().equals("card")) {
                    setAllBut(bt);
                }
            }
        }
    }

    //tv_wx, tv_zfb, tv_qq, tv_jd, tv_succ, tv_fail, tv_revers, tv_refund, tv_close, tv_tyep_refund,
    //    tv_card;
    private void setLisert() {

        gv_pay_type.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int postion, long arg3) {
                DynModel dynModel = list.get(postion);
                if (dynModel != null) {
                    TextView tv = (TextView) v.findViewById(R.id.tv_content);
                    if (payTypeList.contains(dynModel.getApiCode())) {
                        payTypeList.remove(dynModel.getApiCode());

                        tv.setBackgroundResource(R.drawable.general_button_thickness_white_default);
                        tv.setTextColor(Color.parseColor("#000000"));
                        tv.setEnabled(true);
                        if (payTypeList.size() > 0) {
                            updateTypeSetEnabled();
                        } else {
                            updateCardView();
                        }
                    } else {
                        payTypeList.add(dynModel.getApiCode());
                        //变成可点击
                        tv.setBackgroundResource(R.drawable.general_button_main_default);
                        tv.setTextColor(Color.WHITE);
                        tv.setEnabled(false);
                        updateTypeSetEnabled();
                    }
                }
            }
        });

        tv_wx.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                updatepayTypeButBg(tv_wx, "1");
            }
        });

        tv_zfb.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                updatepayTypeButBg(tv_zfb, "2");
            }
        });
        tv_qq.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                updatepayTypeButBg(tv_qq, "4");
            }
        });
        tv_jd.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                updatepayTypeButBg(tv_jd, "5");
            }
        });
        tv_fail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setAllBut(tv_tyep_refund);
                updatepayStateButBg(tv_fail);
            }
        });
        tv_succ.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setAllBut(tv_tyep_refund);
                updatepayStateButBg(tv_succ);
            }
        });
        tv_revers.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setAllBut(tv_tyep_refund);
                updatepayStateButBg(tv_revers);
            }
        });
        tv_refund.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setAllBut(tv_tyep_refund);
                updatepayStateButBg(tv_refund);
            }
        });
        tv_close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setAllBut(tv_tyep_refund);
                updatepayStateButBg(tv_close);
                //                choiceType.clear();

            }
        });// tv_tyep_refund,
        //      tv_card;
        tv_tyep_refund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTypeButBg(tv_tyep_refund, "4");
                if (!isConPayTypeAndState() && !choiceType.contains(tv_tyep_refund.getTag().toString())) {
                    updateCardView();
                } else {
                    tv_card.setEnabled(false);
                    tv_card.setBackgroundResource(R.drawable.general_button_grey_default);
                    tv_card.setTextColor(Color.parseColor("#bbbbbb"));
                }

            }
        });
        tv_card.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                updateTypeCardButBg(tv_card, "5");

                if (list != null && list.size() > 0) {
                    for (DynModel d : list) {
                        d.setEnaled(2);
                    }
                    payTypeAdape.notifyDataSetChanged();
                }

                payTypeList.clear();

            }
        });

        ly_ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (choiceType.size() == 0 && isTag) {
                    ToastHelper.showInfo(activity, ToastHelper.toStr(R.string.tx_bill_stream_chioce_not_null));
                    return;
                } else {
                    for (String s : choiceType) {
                        Log.i("hehui", "choiceType-->" + s);
                    }
                    handleBtn.handleOkBtn(choiceType, payTypeList);
                    dismiss();
                }
            }
        });

        ly_reset.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                for (Button b : listBut) {
                    resetBut(b);
                }
                if (choiceType.size() > 0) {
                    isTag = true;
                }
                // updatePayTypeSetEnabled(true);
                tv_tyep_refund.setEnabled(true);
                tv_card.setEnabled(true);
                choiceType.clear();

                if (list != null && list.size() > 0) {
                    for (DynModel d : list) {
                        d.setEnaled(0);
                    }
                    payTypeAdape.notifyDataSetChanged();
                }

                payTypeList.clear();

            }
        });
    }

    private void initview(View v) {
        gv_pay_type = (GridView) v.findViewById(R.id.gv_pay_type);
        ly_pay_type = (LinearLayout) v.findViewById(R.id.ly_pay_type);

        tv_wx = (Button) v.findViewById(R.id.tv_wx);
        tv_zfb = (Button) v.findViewById(R.id.tv_zfb);
        tv_qq = (Button) v.findViewById(R.id.tv_qq);
        tv_jd = (Button) v.findViewById(R.id.tv_jd);
        tv_succ = (Button) v.findViewById(R.id.tv_succ);
        tv_fail = (Button) v.findViewById(R.id.tv_fail);
        tv_revers = (Button) v.findViewById(R.id.tv_revers);
        tv_refund = (Button) v.findViewById(R.id.tv_refund);
        tv_close = (Button) v.findViewById(R.id.tv_close);
        tv_tyep_refund = (Button) v.findViewById(R.id.tv_tyep_refund);
        tv_card = (Button) v.findViewById(R.id.tv_card);

        ly_reset = (LinearLayout) v.findViewById(R.id.ly_reset);
        ly_ok = (TextView) v.findViewById(R.id.ly_ok);
        listBut.add(tv_wx);
        listBut.add(tv_zfb);
        listBut.add(tv_qq);
        listBut.add(tv_jd);
        listBut.add(tv_succ);
        listBut.add(tv_fail);
        listBut.add(tv_revers);
        listBut.add(tv_refund);
        listBut.add(tv_close);
        listBut.add(tv_tyep_refund);
        listBut.add(tv_card);

        list = new ArrayList<DynModel>();
        Object object = SharedPreUtile.readProduct("filterLstStream" + ApiConstant.bankCode + MainApplication.getMchId());

        if (object != null) {
            list = (List<DynModel>) object;
            if (null != list && list.size() > 0) {
                if (payTypeList.size() > 0) {
                    for (String s : payTypeList) {
                        for (int i = 0; i < list.size(); i++) {
                            DynModel dynModel = list.get(i);

                            if (s.equals(dynModel.getApiCode())) {
                                dynModel.setEnaled(1);
                                list.set(i, dynModel);
                            }
                        }
                    }
                }

                payTypeAdape = new PayTypeAdape(activity, list);
                gv_pay_type.setAdapter(payTypeAdape);

                if (payTypeList.size() > 0) {
                    updateTypeSetEnabled();
                } else {
                    updateCardView();
                }

            } else {
                ly_pay_type.setVisibility(View.VISIBLE);
                gv_pay_type.setVisibility(View.GONE);
            }
        } else {
            gv_pay_type.setVisibility(View.GONE);
            ly_pay_type.setVisibility(View.VISIBLE);
        }
    }

    class PayTypeAdape extends BaseAdapter {

        private List<DynModel> list;

        private Context context;

        private PayTypeAdape(Context context, List<DynModel> list) {
            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.activity_bill_paytype_list_item, null);
                payholder = new ViewPayTypeHolder();
                payholder.tv_content = (TextView) convertView.findViewById(R.id.tv_content);
                convertView.setTag(payholder);
            } else {
                payholder = (ViewPayTypeHolder) convertView.getTag();
            }

            DynModel dynModel = list.get(position);
            if (dynModel != null && !StringUtil.isEmptyOrNull(dynModel.getProviderName())) {
                payholder.tv_content.setText(MainApplication.getPayTypeMap().get(dynModel.getApiCode()));
                switch (dynModel.isEnaled()) {
                    case 0://默认状态
                        payholder.tv_content.setBackgroundResource(R.drawable.general_button_thickness_white_default);
                        payholder.tv_content.setTextColor(Color.parseColor("#000000"));
                        payholder.tv_content.setEnabled(true);
                        break;
                    case 1://选中
                        payholder.tv_content.setBackgroundResource(R.drawable.general_button_main_default);
                        payholder.tv_content.setTextColor(Color.WHITE);
                        payholder.tv_content.setEnabled(true);
                        break;
                    case 2://置灰
                        payholder.tv_content.setBackgroundResource(R.drawable.general_button_grey_default);
                        payholder.tv_content.setTextColor(Color.parseColor("#bbbbbb"));
                        payholder.tv_content.setEnabled(false);
                        break;

                    default:
                        break;
                }

            }

            return convertView;
        }
    }

    class ViewPayTypeHolder {
        TextView tv_content;
    }

    void resetBut(Button b) {
        b.setBackgroundResource(R.drawable.general_button_thickness_white_default);
        choiceType.remove(b.getTag().toString());
        b.setTextColor(Color.parseColor("#000000"));
        b.setEnabled(true);
    }

    void setAllBut(Button b) {
        b.setBackgroundResource(R.drawable.general_button_grey_default);
        choiceType.remove(b.getTag().toString());
        b.setTextColor(Color.parseColor("#bbbbbb"));
        b.setEnabled(false);
    }

    public SelectPicPopupWindow(final Activity context, final SelectPicPopupWindow.HandleTv handleTv) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.take_phone_dialog, null);
        this.activity = context;
        this.handleTv = handleTv;
        TextView tv_take_pic = (TextView) mMenuView.findViewById(R.id.tv_take_pic);
        TextView tv_choice = (TextView) mMenuView.findViewById(R.id.tv_choice);

        mMenuView.findViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        tv_take_pic.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
                handleTv.takePic();
            }
        });

        tv_choice.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
                handleTv.choicePic();
            }
        });

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }

    public SelectPicPopupWindow(final Activity context, List<WalletModel> model, String choice, int restitle, OnItemClickListener itemsOnClick) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.alert_dialog, null);
        listView = (ListView) mMenuView.findViewById(R.id.list1);
        listView.setVisibility(View.VISIBLE);
        this.model = model;
        this.choice = choice;
        listView.setAdapter(new WalletBankListItemAdpter(context, model));
        title = (TextView) mMenuView.findViewById(R.id.title);
        tv_cancel = (TextView) mMenuView.findViewById(R.id.tv_cancel);
        tv_cel = (TextView) mMenuView.findViewById(R.id.tv_cel);
        tv_cancel.setVisibility(View.GONE);
        tv_cel.setVisibility(View.VISIBLE);
        iv_bank = (ImageView) mMenuView.findViewById(R.id.iv_bank);
        iv_bank.setVisibility(View.VISIBLE);
        if (restitle > 0) {
            title.setVisibility(View.VISIBLE);
            title.setText(restitle);
        }

        iv_bank.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                NewDialogInfo info = new NewDialogInfo(context, null, null, 1, null, new NewDialogInfo.HandleBtn() {

                    @Override
                    public void handleOkBtn() {
                    }

                });

                DialogHelper.resize(context, info);

                info.show();
            }
        });

        //取消按钮
        tv_cel.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                //销毁弹出框
                dismiss();
            }
        });

        listView.setOnItemClickListener(itemsOnClick);

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }

    private class ViewHolder {
        private TextView tv_name;

        private ImageView iv_img;
    }

    private class WalletBankListItemAdpter extends BaseAdapter {
        private Context context;

        List<WalletModel> list;

        public WalletBankListItemAdpter(Context context, List<WalletModel> model) {
            this.list = model;
            this.context = context;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.wallet_bank_list_item, null);
                holder = new ViewHolder();
                holder.iv_img = (ImageView) convertView.findViewById(R.id.iv_img);
                holder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            WalletModel model = list.get(position);
            if (null != model) {
                if (model.getAccountCode().equals(choice)) {
                    holder.iv_img.setVisibility(View.VISIBLE);
                } else {
                    holder.iv_img.setVisibility(View.GONE);
                }
                holder.tv_name.setText(model.getBankName() + "(" + model.getSuffixIdCard() + ")");
            }
            return convertView;
        }
    }

    private ListView lv_pay_type;

    List<DynModel> listNative = new ArrayList<DynModel>();

    ViewHolderNative viewHolderNative;

    SelectPicPopupWindow.HandleNative handleNative;

    private class ViewHolderNative {
        private TextView tv_pay_name;

        private View v_line;
    }

    public interface HandleNative {
        void toPay(String type);
    }

    private class NativePayTypeAdpter extends BaseAdapter {
        private Context context;

        List<DynModel> list;

        public NativePayTypeAdpter(Context context, List<DynModel> model) {
            this.list = model;
            this.context = context;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.native_pay_list_item, null);
                viewHolderNative = new ViewHolderNative();
                viewHolderNative.tv_pay_name = (TextView) convertView.findViewById(R.id.tv_pay_name);
                convertView.setTag(viewHolderNative);
            } else {
                viewHolderNative = (ViewHolderNative) convertView.getTag();
            }

            DynModel dynModel = list.get(position);
            if (dynModel != null) {
                viewHolderNative.tv_pay_name.setText(MainApplication.getPayTypeMap().get(dynModel.getApiCode()) + context.getString(R.string.title_pay));
            }

            return convertView;
        }
    }

    public SelectPicPopupWindow(Activity context, String type, OnClickListener itemsOnClick, final SelectPicPopupWindow.HandleNative handleNative) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.alert_dialog, null);
        tv_cancel = (TextView) mMenuView.findViewById(R.id.tv_cel);
        tv_ok = (TextView) mMenuView.findViewById(R.id.tv_ok);
        tv_wx_pay = (TextView) mMenuView.findViewById(R.id.tv_wx_pay);
        tv_zfb_pay = (TextView) mMenuView.findViewById(R.id.tv_zfb_pay);
        tv_qq_pay = (TextView) mMenuView.findViewById(R.id.tv_qq_pay);
        tv_jd_pay = (TextView) mMenuView.findViewById(R.id.tv_jd_pay);
        tv_scan = (TextView) mMenuView.findViewById(R.id.tv_scan);
        v_wx = (View) mMenuView.findViewById(R.id.v_wx);
        v_zfb = (View) mMenuView.findViewById(R.id.v_zfb);
        v_jd = (View) mMenuView.findViewById(R.id.v_jd);
        v_qq = (View) mMenuView.findViewById(R.id.v_qq);
        lv_pay_type = (ListView) mMenuView.findViewById(R.id.lv_pay_type);
        this.handleNative = handleNative;
        this.type = type;

        Object object = SharedPreUtile.readProduct("dynPayType" + ApiConstant.bankCode + MainApplication.getMchId());

        if (object != null) {
            listNative = (List<DynModel>) object;
            if (null != listNative && listNative.size() > 0) {
                lv_pay_type.setVisibility(View.VISIBLE);
                NativePayTypeAdpter nativePayTypeAdpter = new NativePayTypeAdpter(context, listNative);
                lv_pay_type.setAdapter(nativePayTypeAdpter);
            }
        } else {

            if (!StringUtil.isEmptyOrNull(MainApplication.serviceType)) {
                if (MainApplication.serviceType.contains(MainApplication.PAY_QQ_NATIVE) || MainApplication.serviceType.contains(MainApplication.PAY_QQ_NATIVE1)) {
                    tv_qq_pay.setVisibility(View.VISIBLE);
                    v_qq.setVisibility(View.VISIBLE);
                }

                if (MainApplication.serviceType.contains(MainApplication.PAY_ZFB_NATIVE) || MainApplication.serviceType.contains(MainApplication.PAY_ZFB_NATIVE1)) {
                    tv_zfb_pay.setVisibility(View.VISIBLE);
                    v_zfb.setVisibility(View.VISIBLE);
                }

                if (MainApplication.serviceType.contains(MainApplication.PAY_JINGDONG_NATIVE)) {
                    tv_jd_pay.setVisibility(View.VISIBLE);
                    v_jd.setVisibility(View.VISIBLE);
                }

                if (MainApplication.serviceType.contains(MainApplication.PAY_WX_NATIVE)) {
                    tv_wx_pay.setVisibility(View.VISIBLE);
                    v_wx.setVisibility(View.VISIBLE);
                }

                //            if ((MainApplication.serviceType.contains(MainApplication.PAY_QQ_MICROPAY)
                //                || MainApplication.serviceType.contains(MainApplication.PAY_WX_MICROPAY)
                //                || MainApplication.serviceType.contains(MainApplication.PAY_ZFB_MICROPAY) || MainApplication.serviceType.contains(MainApplication.PAY_JINGDONG)))
                //            {
                //            }
                //            tv_scan.setVisibility(View.VISIBLE);
            }
        }

        //        if (type.equals(MainApplication.PAY_QQ_NATIVE) || type.equals(MainApplication.PAY_QQ_NATIVE1))
        //        {
        //            tv_qq_pay.setVisibility(View.GONE);
        //            v_qq.setVisibility(View.GONE);
        //        }
        //        else if (type.contains(MainApplication.PAY_ZFB_NATIVE))
        //        {
        //            tv_zfb_pay.setVisibility(View.GONE);
        //            v_zfb.setVisibility(View.GONE);
        //        }
        //        else if (type.equalsIgnoreCase(MainApplication.PAY_JINGDONG_NATIVE))
        //        {
        //            tv_jd_pay.setVisibility(View.GONE);
        //            v_jd.setVisibility(View.GONE);
        //        }
        //        else if (type.equalsIgnoreCase(MainApplication.PAY_WX_NATIVE))
        //        {
        //            tv_wx_pay.setVisibility(View.GONE);
        //            v_wx.setVisibility(View.GONE);
        //        }

        lv_pay_type.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int postion, long arg3) {
                DynModel dynModel = listNative.get(postion);
                if (dynModel != null) {
                    dismiss();
                    handleNative.toPay(dynModel.getApiCode());
                }
            }
        });

        //取消按钮
        tv_cancel.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                //销毁弹出框
                dismiss();
            }
        });
        //设置按钮监听
        //        tv_ok.setOnClickListener(itemsOnClick);
        tv_wx_pay.setOnClickListener(itemsOnClick);
        tv_zfb_pay.setOnClickListener(itemsOnClick);
        tv_qq_pay.setOnClickListener(itemsOnClick);
        tv_jd_pay.setOnClickListener(itemsOnClick);
        tv_scan.setOnClickListener(itemsOnClick);
        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });

    }
}
