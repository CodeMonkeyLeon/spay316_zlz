package cn.swiftpass.enterprise.broadcast;

import com.igexin.sdk.PushService;

/**
 * Created by aijingya on 2019/7/1.
 *
 * @Package cn.swiftpass.enterprise.broadcast
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2019/7/1.16:34.
 */
// 仅2.13.1.0及以上版本才能直接extends PushService，低于此版本请延⽤之前实现⽅式
public class DemoPushService extends PushService {
}