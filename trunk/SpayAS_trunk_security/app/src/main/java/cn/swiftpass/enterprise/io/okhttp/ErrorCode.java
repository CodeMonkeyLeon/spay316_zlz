package cn.swiftpass.enterprise.io.okhttp;

import java.util.HashMap;
import java.util.Map;

import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.ToastHelper;

public class ErrorCode {

    /**
     * 没有错误
     */
    public static final String RESULT_SUCCESS = "200";
    /**
     * 没有错误
     */
    public static final String RESULT_NO_ERROR = "0";

    /**
     * 网络不可用
     */
    public static final String RESULT_BAD_NETWORK = "-1";

    /**
     * 内部错误
     */
    public static final String RESULT_INTERNAL_ERROR = "-2";

    /**
     * 无法链接服务器，服务器内部错误！
     */
    public static final String RESULT_READING_ERROR = "-3";

    /**
     * 读取数据超时
     */
    public static final String RESULT_TIMEOUT_ERROR = "-4";

    /**
     * 网络证书校验失败
     */
    public static final String RESULT_SSL_HANDSHAKE_ERROR = "-7";


    /**
     * 会话已过期
     */
    public static final String RESULT_NEED_LOGIN = "401";

    /**
     * 网络证书校验失败异常
     */
    public static final String HANDSHAKE_CER_ERROR = "60003";

    public static Map<String, String> errorMap;

    static {
        errorMap = new HashMap();

        // 系统错误
        errorMap.put(RESULT_BAD_NETWORK, ToastHelper.toStr(R.string.show_net_bad));
        errorMap.put(RESULT_INTERNAL_ERROR, ToastHelper.toStr(R.string.ts_fail_info));
        errorMap.put(RESULT_READING_ERROR, ToastHelper.toStr(R.string.show_net_server_fail));
        errorMap.put(RESULT_TIMEOUT_ERROR, ToastHelper.toStr(R.string.show_net_timeout));
        errorMap.put(RESULT_SSL_HANDSHAKE_ERROR, HANDSHAKE_CER_ERROR);
        errorMap.put(RESULT_NEED_LOGIN, RESULT_NEED_LOGIN);
    }

    public static String getErrorMsg(String resultCode) {
        return errorMap.get(resultCode);
    }
}
