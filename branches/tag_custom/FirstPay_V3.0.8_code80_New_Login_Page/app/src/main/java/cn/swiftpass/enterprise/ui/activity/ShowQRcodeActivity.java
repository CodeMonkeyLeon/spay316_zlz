package cn.swiftpass.enterprise.ui.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.zxing.WriterException;
import com.tencent.stat.StatService;

import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.MaxCardManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.QRcodeInfo;
import cn.swiftpass.enterprise.bussiness.model.WalletListBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.scan.CaptureActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.ProgressInfoDialog;
import cn.swiftpass.enterprise.ui.widget.SelectPicPopupWindow;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DataReportUtils;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.DisplayUtil;
import cn.swiftpass.enterprise.utils.GlideApp;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

import static cn.swiftpass.enterprise.MainApplication.getContext;

/**
 * 显示二维码
 * User: he_hui
 * Date: 15-6-25
 * Time: 下午5:43
 */
public class ShowQRcodeActivity extends TemplateActivity {
    private static final String TAG = ShowQRcodeActivity.class.getSimpleName();
    private ImageView img;

    //private TextView tvState;
    private ScrollView sv_native_pay_static_code;

    private TextView tvMoney, pay_tv_info, pay_wx_str, tv_switch;
    //private TextView pay_title, tv_pay_info, tv_pay_tel;

    private ImageView iv_payment_way_small_icon;

    //private ImageView ivImgstate;

    private Context mContext;

    //private RelativeLayout llState;

    private QRcodeInfo qrcodeInfo;

    private boolean isLiquidType;

    private WalletListBean walletBean;

    Dialog dialog;

    //long startTime;

    //private long money;

    // 是否交易完成
    private boolean isbussFinsh;

    private TimeCount time;

    //private int state; // 状态

    //private boolean isMove = false;

    //private PopupWindow pop;

    //private PayMyPopupWindowUtils popUtils;

    //private LinearLayout preview_top_view;

    private DialogInfo dialogInfo;

    private Handler mHandler;

    private boolean isMark = true;

    private Button confirm;

    private LinearLayout money_lay;

    private LinearLayout vcard_lay;

    //double favourable_money = 0;

    //private TextView receivableMoney, favourableMoney, relMoney;
    private TextView id_tv_amount;

    //private DialogInfo dialogs;

    ProgressInfoDialog dialog1;

    private TextView tv_payMoney;

    private boolean isStop = true; // 强行中断交易

    private TextView tv_scan, tv_pase;

    //自定义的弹出框类
    cn.swiftpass.enterprise.ui.widget.SelectPicPopupWindow menuWindow,menuLiquidWindow;

    //List<View> viewList = new ArrayList<View>();

    //private AlertDialog dialogInfos;


    LinearLayout mHasFeelayout;
    private TextView mSurcharge;
    private String mark = "";
    double minus = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //根据当前的小数点的位数来判断应该除以多少
        for(int i = 0 ; i < MainApplication.numFixed ; i++ ){
            minus = minus * 10;
        }
        initViews();
        setLister();
        MainApplication.listActivities.add(this);
    }

    private void setLister() {
        tv_scan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent it = new Intent();
                it.setClass(ShowQRcodeActivity.this, CaptureActivity.class);
                it.putExtra("money", qrcodeInfo.totalMoney);
                it.putExtra("payType", MainApplication.PAY_WX_MICROPAY);
                startActivity(it);
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CaptureActivity.startActivity(ShowQRcodeActivity.this, qrcodeInfo.payType, CaptureActivity.FLAG, qrcodeInfo.totalMoney);

                MainApplication.listActivities.add(ShowQRcodeActivity.this);
            }

        });
    }

    private void initViews() {
        mContext = this;
        setContentView(R.layout.activity_show_qrcode_new);
        tv_pase = getViewById(R.id.tv_pase);
        tv_pase.setVisibility(View.GONE);
        tv_scan = getViewById(R.id.tv_scan);
        tv_payMoney = getViewById(R.id.tv_payMoney);
        tv_payMoney.setText(MainApplication.getFeeFh());
        iv_payment_way_small_icon = getViewById(R.id.iv_payment_way_small_icon);
        tv_switch = getViewById(R.id.tv_switch);
        sv_native_pay_static_code = getViewById(R.id.sv_native_pay_static_code);

        //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth","sale");
        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(saleOrPreauth,"pre_auth")){
            sv_native_pay_static_code.setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
            tv_switch.setTextColor(getResources().getColor(R.color.bg_text_pre_auth));
        }else{
            sv_native_pay_static_code.setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
            tv_switch.setTextColor(getResources().getColor(R.color.title_bg_new));
        }

        //favourableMoney = getViewById(R.id.favourable_money);
        //receivableMoney = getViewById(R.id.receivable_money);
        //relMoney = getViewById(R.id.rel_money);
        id_tv_amount = getViewById(R.id.id_tv_amount);
        money_lay = getViewById(R.id.money_lay);
        vcard_lay = getViewById(R.id.vcard_lay);
        confirm = getViewById(R.id.confirm);
        setButBgAndFont(confirm);
        //pay_title = getViewById(R.id.pay_title);
        pay_tv_info = getViewById(R.id.pay_tv_info);
        //popUtils = new PayMyPopupWindowUtils(this, null);
        mHandler = new Handler();
        pay_wx_str = getViewById(R.id.pay_wx_str);
        //preview_top_view = getViewById(R.id.preview_top_view);
        //ivImgstate = getViewById(R.id.iv_imgstate);
        //llState = getViewById(R.id.ll_state);
        qrcodeInfo = (QRcodeInfo) getIntent().getSerializableExtra("qrcodeInfo");

        walletBean = (WalletListBean) getIntent().getSerializableExtra("walletBean");
        isLiquidType = getIntent().getBooleanExtra("isLiquidType",false);

        mark = getIntent().getStringExtra("mark");


        //tv_pay_info = getViewById(R.id.tv_pay_info);
        //tv_pay_tel = getViewById(R.id.tv_pay_tel);
        tvMoney = getViewById(R.id.tv_money);

        //新增
        mHasFeelayout  = getViewById(R.id.has_fee_layout);
        mSurcharge = getViewById(R.id.surcharge);
        if (qrcodeInfo.isIswxCard()) {
            tv_switch.setVisibility(View.GONE);
        }

        money_lay.setVisibility(View.GONE);
        vcard_lay.setVisibility(View.GONE);
        BigDecimal bigDecimal = new BigDecimal(Long.parseLong(qrcodeInfo.totalMoney) / minus);
        String strSurchargelMoney = qrcodeInfo.getSurcharge();
        BigDecimal bigSurcharge = null;
        //如果开通预授权，则手动把surcharge写死不显示。
        if(MainApplication.isSurchargeOpen()){
            if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth","sale"),"pre_auth")){
                strSurchargelMoney = "0";
            }
        }
        if (TextUtils.isEmpty(strSurchargelMoney)) {
            tvMoney.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(Long.parseLong(qrcodeInfo.totalMoney)));
            bigSurcharge = new BigDecimal(0);
            mHasFeelayout.setVisibility(View.GONE);
        } else {
            tvMoney.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(Long.parseLong(qrcodeInfo.totalMoney) + Long.parseLong(strSurchargelMoney)));
            bigSurcharge = new BigDecimal(Long.parseLong(strSurchargelMoney) / minus);
            mHasFeelayout.setVisibility(View.VISIBLE);
        }
        paseRMBWithSurcharge(bigDecimal, bigSurcharge);

        if (qrcodeInfo.getIsMark() == 1) {
            confirm.setVisibility(View.GONE);
        }


        img = getViewById(R.id.img);
        //tvState = getViewById(R.id.tv_state);

        connection = new HttpURLConnection[1];

        createMaxCard(qrcodeInfo.uuId,isLiquidType,walletBean);

        if (MainApplication.isSurchargeOpen()) {
            if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth","sale"),"pre_auth")){
                id_tv_amount.setVisibility(View.GONE);
                mHasFeelayout.setVisibility(View.GONE);
            }else{
                id_tv_amount.setVisibility(View.VISIBLE);
                mHasFeelayout.setVisibility(View.VISIBLE);
            }
        } else {
            id_tv_amount.setVisibility(View.GONE);
            mHasFeelayout.setVisibility(View.GONE);
        }

        /**
         * 切换扫码
         */
        tv_switch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //点击扫码，不管是否是liquid pay 通道，先展示一级列表
                menuWindow = new SelectPicPopupWindow(ShowQRcodeActivity.this, qrcodeInfo.payType, new SelectPicPopupWindow.HandleNative() {

                    @Override
                    public void toPay(String type,String secondType ,boolean isSecondPay,WalletListBean bean) {
                        if(getNativePayType(type).equalsIgnoreCase(MainApplication.liquidServiceType)){//如果是点击的小钱包通道

                            menuLiquidWindow = new SelectPicPopupWindow(ShowQRcodeActivity.this, type, true, new SelectPicPopupWindow.HandleNative() {
                                @Override
                                public void toPay(String type, String secondType, boolean isSecondPay,WalletListBean bean) {
                                    //生成新的二维码之前，停止查询旧的单
                                    if(time != null){
                                        time.cancel();
                                    }

                                    String msg = getString(R.string.tv_pay_prompt);
                                    toNativePay(msg, qrcodeInfo.totalMoney, type,secondType,mark,isSecondPay,false,bean);
                                }
                            });
                            //显示窗口
                            menuLiquidWindow.showAtLocation(tv_switch, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0); //设置layout在PopupWindow中显示的位置

                        }else if(getNativePayType(type).equalsIgnoreCase(MainApplication.instapayServiceType)){//如果是点击的instapay的通道
                            //生成新的二维码之前，停止查询旧的单
                            if(time != null){
                                time.cancel();
                            }

                            //是instapay通道，下单成功，则直接跳转到instapay页面
                            String msg = getString(R.string.tv_pay_prompt);
                            toNativePay(msg, qrcodeInfo.totalMoney, type,secondType,mark,false,true,bean);

                        } else{
                            //生成新的二维码之前，停止查询旧的单
                            if(time != null){
                                time.cancel();
                            }

                            //不是小钱包通道，则直接生成二维码
                            String msg = getString(R.string.tv_pay_prompt);
                            toNativePay(msg, qrcodeInfo.totalMoney, type,secondType,mark,false,false,bean);
                        }
                    }
                });
                try {
                    StatService.trackCustomEvent(mContext, "kMTASPayPayQRCode", "切换支付方式");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                // 1.携带参数的打点
                Bundle values = new Bundle();
                values.putString("kGFASPayPayQRCode","切换支付方式“切换”按钮");
                DataReportUtils.getInstance().report("kGFASPayPayQRCode",values);

                //显示窗口
                menuWindow.showAtLocation(tv_switch, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0); //设置layout在PopupWindow中显示的位置
            }
        });
    }

    private void paseRMBWithSurcharge(BigDecimal bigDecimal, BigDecimal bigSurcharge) {
        try {
            if (!MainApplication.getFeeType().equalsIgnoreCase("CNY")) {
                //新增
                tv_pase.setVisibility(View.GONE);
                BigDecimal paseBigDecimal = null;
                if (bigDecimal != null) {
                    if (MainApplication.getSourceToUsdExchangeRate() > 0 && MainApplication.getUsdToRmbExchangeRate() > 0) {
                        BigDecimal sourceToUsd = new BigDecimal(MainApplication.getSourceToUsdExchangeRate());
                        BigDecimal usdRate = new BigDecimal(MainApplication.getUsdToRmbExchangeRate());
//                        paseBigDecimal = bigSurcharge.add(bigDecimal).multiply(usdRate).multiply(sourceToUsd).setScale(2, BigDecimal.ROUND_FLOOR);
                        paseBigDecimal = bigSurcharge.add(bigDecimal).multiply(sourceToUsd).setScale(MainApplication.numFixed, BigDecimal.ROUND_HALF_UP)
                                .multiply(usdRate).setScale(MainApplication.numFixed, BigDecimal.ROUND_DOWN);


                    } else {
                        BigDecimal rate = new BigDecimal(MainApplication.getExchangeRate());
                        paseBigDecimal = bigSurcharge.add(bigDecimal).multiply(rate).setScale(MainApplication.numFixed, BigDecimal.ROUND_FLOOR);
                    }
//                    String totalStr = "(" + getString(R.string.tv_charge_total) + ":" + DateUtil.formatPaseMoney(bigDecimal) + "," + getString(R.string.tx_surcharge) + ":" + DateUtil.formatPaseMoney(bigSurcharge) + ")";
                    String totalStr = MainApplication.getFeeFh()+ DateUtil.formatPaseMoney(bigDecimal);
                    String surchargeFee = MainApplication.getFeeFh()+ DateUtil.formatPaseMoney(bigSurcharge);
                    id_tv_amount.setText(totalStr);
                    mSurcharge.setText(surchargeFee);
                    tv_pase.setText(getString(R.string.tx_about) + getString(R.string.tx_mark) + DateUtil.formatPaseRMBMoney(paseBigDecimal));
                }
            } else {
                tv_pase.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Logger.e("hehui", "parserToRmb-->" + e);
        }
    }

    private void toNativePay(final String msg, final String money, final String type,final String tradeType,final String note,final boolean isSecond,boolean isInstapay,final WalletListBean walletBean) {

        OrderManager.getInstance().unifiedNativePay(money, type,tradeType ,0, null, null, note,new UINotifyListener<QRcodeInfo>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                loadDialog(ShowQRcodeActivity.this, R.string.tv_pay_prompt);
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                dismissLoading();

                if (checkSession()) {
                    return;
                }
                if (!isStop) {
                    return;
                }
                if (object != null) {
                    ShowQRcodeActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toastDialog(ShowQRcodeActivity.this, object.toString(), null);
                        }
                    });
                }

                ShowQRcodeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //如果生成码的过程失败了，则还是重新开始查单，查的还是上一次的单
                        Countdown();
                    }
                });
            }

            @Override
            public void onSucceed(QRcodeInfo result) {
                super.onSucceed(result);
                dismissLoading();
                if (result != null && isStop) {
                    qrcodeInfo.outAuthNo = result.outAuthNo;
                    qrcodeInfo.orderNo = result.orderNo;
                    qrcodeInfo.payType = type;
                    qrcodeInfo.setInvoiceId(result.getInvoiceId());
                    qrcodeInfo.setExpirationDate(result.getExpirationDate());

                    if(isInstapay){//如果是instapay下单，则直接跳转到instapay的页面

                        InstapayInfoActivity.startActivity(ShowQRcodeActivity.this,qrcodeInfo);

                        //停止查单，结束当前界面
                        if(time != null){
                            time.cancel();
                        }
                        finish();

                    }else{//否则还是按照以前的逻辑，生成固码
                        createMaxCard(result.uuId,isSecond,walletBean);
                    }

                } else {
                    isStop = true;
                }
            }
        });
    }

    private void createMaxCard(final String uuId, boolean isLiquidType, WalletListBean walletBean) {
        // 生成二维码
        try {
            titleBar.setRightButLayVisible(true, null);
            titleBar.setTitle(getString(R.string.tv_code_wx));
            pay_tv_info.setVisibility(View.VISIBLE);

            Object object = SharedPreUtile.readProduct("payTypeNameMap" + ApiConstant.bankCode + MainApplication.getMchId());
            if (object != null) {
                Map<String, DynModel> payTypeNameMap = (Map<String, DynModel>) object;

                DynModel dynModel = payTypeNameMap.get(qrcodeInfo.payType);

                if (null != dynModel && !StringUtil.isEmptyOrNull(dynModel.getProviderName())) {
                    try {
                        String language = PreferenceUtil.getString("language", "");
                        if (StringUtil.isEmptyOrNull(language)) {
                            language = Locale.getDefault().toString();
                        }
                        if (language.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN) || language.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_HANS)) {

                            if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth","sale"),"pre_auth")){

                                if(isLiquidType && walletBean != null){
                                    titleBar.setTitle(walletBean.getName() + getString(R.string.choose_pre_auth));
                                    pay_wx_str.setText(walletBean.getName()+getString(R.string.choose_pre_auth)  + ",");
                                }else{
                                    titleBar.setTitle(MainApplication.getPayTypeMap().get(qrcodeInfo.payType) + getString(R.string.choose_pre_auth));
                                    pay_wx_str.setText(MainApplication.getPayTypeMap().get(qrcodeInfo.payType)+getString(R.string.choose_pre_auth) + ",");
                                }


                            }else{
                                if(isLiquidType && walletBean != null){
                                    titleBar.setTitle(walletBean.getName());
                                    pay_wx_str.setText(walletBean.getName()+ ",");
                                }else {
                                    titleBar.setTitle(MainApplication.getPayTypeMap().get(qrcodeInfo.payType) );
                                    pay_wx_str.setText(MainApplication.getPayTypeMap().get(qrcodeInfo.payType)+ ",");
                                }

                            }

                        } else if (language.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK) || language.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO) ||
                                language.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW) || language.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_HANT)) {
                            if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth","sale"),"pre_auth")){
                                if(isLiquidType && walletBean != null){
                                    titleBar.setTitle(walletBean.getName()+getString(R.string.choose_pre_auth));
                                    pay_wx_str.setText(walletBean.getName()+getString(R.string.choose_pre_auth) + ",");
                                }else{
                                    titleBar.setTitle(MainApplication.getPayTypeMap().get(qrcodeInfo.payType)+getString(R.string.choose_pre_auth));
                                    pay_wx_str.setText(MainApplication.getPayTypeMap().get(qrcodeInfo.payType)+getString(R.string.choose_pre_auth) + ",");
                                }

                            }else {
                                if(isLiquidType && walletBean != null){
                                    titleBar.setTitle(walletBean.getName() );
                                    pay_wx_str.setText(walletBean.getName() + ",");
                                }else{
                                    titleBar.setTitle(MainApplication.getPayTypeMap().get(qrcodeInfo.payType));
                                    pay_wx_str.setText(MainApplication.getPayTypeMap().get(qrcodeInfo.payType)  + ",");
                                }
                            }
                        } else {
                            if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth","sale"),"pre_auth")) {

                                if(isLiquidType && walletBean != null){
                                    titleBar.setTitle(walletBean.getName()+ " "+getString(R.string.choose_pre_auth));
                                    pay_wx_str.setText(walletBean.getName()+ " "+getString(R.string.choose_pre_auth)+ ",");
                                }else{
                                    titleBar.setTitle(MainApplication.getPayTypeMap().get(qrcodeInfo.payType) + " "+getString(R.string.choose_pre_auth));
                                    pay_wx_str.setText(MainApplication.getPayTypeMap().get(qrcodeInfo.payType) + " "+getString(R.string.choose_pre_auth) + ",");
                                }
                            }else{
                                if(isLiquidType && walletBean != null ){
                                    titleBar.setTitle(walletBean.getName());
                                    pay_wx_str.setText(walletBean.getName() + ",");
                                }else {
                                    titleBar.setTitle(MainApplication.getPayTypeMap().get(qrcodeInfo.payType));
                                    pay_wx_str.setText(MainApplication.getPayTypeMap().get(qrcodeInfo.payType)+ ",");
                                }
                            }
                        }

                    } catch (Exception e) {
                        Log.e(TAG,Log.getStackTraceString(e));
                    }
                }
            }

            if(isLiquidType && walletBean != null ){
                if(!TextUtils.isEmpty(walletBean.getImageUrl())){
                    if(getContext() != null){
                        GlideApp.with(getContext())
                                .load(walletBean.getImageUrl())
                                .placeholder(R.drawable.icon_general_receivables)
                                .error(R.drawable.icon_general_receivables)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .into(iv_payment_way_small_icon);
                    }
                }else{
                    iv_payment_way_small_icon.setImageResource(R.drawable.icon_general_receivables);
                }
            }else{
                Object object_icon = SharedPreUtile.readProduct("payTypeIcon" + ApiConstant.bankCode + MainApplication.getMchId());
                if (object_icon != null) {
                    try {
                        Map<String, String> typePicMap = (Map<String, String>) object_icon;
                        if (typePicMap != null && typePicMap.size() > 0) {
                            String picUrl = typePicMap.get(qrcodeInfo.payType);
                            if (!StringUtil.isEmptyOrNull(picUrl)) {
                                if(getContext() != null){
                                    GlideApp.with(getContext())
                                            .load(picUrl)
                                            .placeholder(R.drawable.icon_general_receivables)
                                            .error(R.drawable.icon_general_receivables)
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .into(iv_payment_way_small_icon);
                                }
                            } else {
                                iv_payment_way_small_icon.setImageResource(R.drawable.icon_general_receivables);
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG,Log.getStackTraceString(e));
                    }
                }
            }

            ShowQRcodeActivity.this.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    int w = DisplayUtil.dip2Px(ShowQRcodeActivity.this, 280);
                    int h = DisplayUtil.dip2Px(ShowQRcodeActivity.this, 280);
                    float scale = ShowQRcodeActivity.this.getResources().getDisplayMetrics().density;
                    if (scale == 1.0f) {
                        w = DisplayUtil.dip2Px(ShowQRcodeActivity.this, 380);
                        h = DisplayUtil.dip2Px(ShowQRcodeActivity.this, 380);
                    }
                    // TODO Auto-generated method stub
                    Bitmap bitmap;
                    try {
                        bitmap = MaxCardManager.getInstance().create2DCode(uuId, w, h);
                        img.setImageBitmap(bitmap);
                    } catch (WriterException e) {
                        Log.e(TAG,Log.getStackTraceString(e));
                    }

                }
            });

            Countdown();

        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }
    }

    /**
     * 支付一分钟倒计时
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void Countdown() {
        // 总共2分钟 间隔2秒
        time = new TimeCount(1000 * 60 * 2, 2000);
        time.start();
    }

    private long timeCount = 5;

    private Runnable myRunnable = new Runnable() {

        @Override
        public void run() {
            if (timeCount > 0 && dialogInfo != null) {
                dialogInfo.setBtnOkText(getString(R.string.btnOk) + "(" + getString(R.string.show_close) + timeCount + getString(R.string.tv_second) + ")");
                timeCount -= 1;
                mHandler.postDelayed(this, 1000);
            } else {
                mHandler.removeCallbacks(this);
                if (dialogInfo != null && dialogInfo.isShowing()) {

                    dialogInfo.dismiss();
                }
                dialogInfo.setBtnOkText(getString(R.string.dialog_close));
                ShowQRcodeActivity.this.finish();
            }

        }
    };

    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }

        @Override
        public void onFinish() {//计时完毕时触发

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String mmsg = getString(R.string.show_request_outtime);
                    try {
                        dialogInfo = new DialogInfo(ShowQRcodeActivity.this, getStringById(R.string.public_cozy_prompt), mmsg, getStringById(R.string.btnOk), DialogInfo.REGISTFLAG, null, null);

                        DialogHelper.resize(ShowQRcodeActivity.this, dialogInfo);
                        dialogInfo.setOnKeyListener(new OnKeyListener() {

                            @Override
                            public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                                if (keycode == KeyEvent.KEYCODE_BACK) {
                                    return true;
                                }
                                return false;
                            }
                        });
                        if (dialogInfo != null) {

                            dialogInfo.show();
                        }
                        mHandler.post(myRunnable);
                    } catch (Exception e) {
                        Log.e(TAG,Log.getStackTraceString(e));
                    }
                }
            });
            //            time.cancel();
        }

        @Override
        public void onTick(long millisUntilFinished) {//计时过程显示
            String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth","sale");
            if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(saleOrPreauth,"pre_auth")){
                if (null != qrcodeInfo.outAuthNo && !"".equals(qrcodeInfo.outAuthNo)) {
                    queryOrderGetStuts();
                }
            }else {
                if (null != qrcodeInfo.orderNo && !"".equals(qrcodeInfo.orderNo)) {
                    queryOrderGetStuts();
                }
            }
        }
    }


    /**
     * 查询订单获取状态
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void queryOrderGetStuts() {
        String orderNoStr;
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth","sale");
        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(saleOrPreauth,"pre_auth")){
            orderNoStr = qrcodeInfo.outAuthNo;
        }else{
            orderNoStr = qrcodeInfo.orderNo;
        }

        OrderManager.getInstance().queryOrderByOrderNo(orderNoStr, MainApplication.PAY_ZFB_QUERY, new UINotifyListener<Order>() {
            @Override
            public void onError(Object object) {
                super.onError(object);
            }

            @Override
            public void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(Order result) {
                super.onSucceed(result);
                if (result != null) {
                    //                        result.notifyTime = result.notify_time + "";
                    if (result.state.equals("2") && isMark) {
                        if(time != null){
                            time.cancel();
                        }
                        qrcodeInfo.orderNo = null;
                        qrcodeInfo.outAuthNo =null;
                        isMark = false;
                        NoteMarkActivity.setNoteMark("");
                        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth","sale"),"pre_auth")){
                            PreAutFinishActivity.startActivity(ShowQRcodeActivity.this, result,1);
                        }else{
                            PayResultActivity.startActivity(ShowQRcodeActivity.this, result);
                        }

                        finish();

                    }

                }
            }
        });
    }


    private HttpURLConnection[] connection;

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(R.string.title_wx_pay);
        //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth","sale");
        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(saleOrPreauth,"pre_auth")){
            titleBar.setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
        }else{
            titleBar.setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
        }
        titleBar.setRightButLayVisible(false, null);
        titleBar.setLeftButtonVisible(true);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                if (!isbussFinsh) {
                    finish();
                } else {
                    finish();
                }
            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onRightLayClick() {
                // TODO Auto-generated method stub

            }

            @Override
            public void onRightButLayClick() {

            }
        });
    }


    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        OrderManager.getInstance().destory();
        if (null != connection && connection.length > 0) {
            try {
                if (null != connection[0])
                {
                    connection[0].disconnect();
                }
            } catch (Exception e) {
                Logger.i(e.getMessage());
            }
        }

        if (time != null) {
            time.cancel();
        }

        if(dialogInfo != null) {
            dialogInfo.dismiss();
        }
    }



    public static void startActivity(QRcodeInfo qrcodeInfo, Context context) {
        Intent it = new Intent();
        it.putExtra("qrcodeInfo", qrcodeInfo);
        it.setClass(context, ShowQRcodeActivity.class);
        context.startActivity(it);
    }

    public static void startActivity(QRcodeInfo qrcodeInfo, Context context,WalletListBean walletBean,boolean isLiquidType) {
        Intent it = new Intent();
        it.putExtra("qrcodeInfo", qrcodeInfo);
        it.putExtra("walletBean", walletBean);
        it.putExtra("isLiquidType", isLiquidType);
        it.setClass(context, ShowQRcodeActivity.class);
        context.startActivity(it);
    }

    @Override
    public boolean onKeyDown(int keycode, KeyEvent event) {
        if (keycode == KeyEvent.KEYCODE_BACK) {
            if (!isbussFinsh) {
                finish();
            }
            return true;
        }
        return super.onKeyDown(keycode, event);
    }

    //通过ApiCode得到当前支付类型的NativePayType
    public String getNativePayType(String ApiCode){
        //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth","sale");
        Object object;
        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(saleOrPreauth,"pre_auth")){
            object = SharedPreUtile.readProduct("dynPayType_pre_auth" + ApiConstant.bankCode + MainApplication.getMchId());
        }else{
            object = SharedPreUtile.readProduct("ActiveDynPayType" + ApiConstant.bankCode + MainApplication.getMchId());
        }

        if (object != null) {
            List<DynModel> list = (List<DynModel>) object;

            if (null != list && list.size() > 0) {
                for(int i = 0 ; i < list.size();i++){
                    if(list.get(i).getApiCode().equalsIgnoreCase(ApiCode)){
                        return list.get(i).getNativeTradeType();
                    }
                }
            } else {
                return null;
            }
        }else {
            return null;
        }
        return null;
    }

}
