package cn.swiftpass.enterprise.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.utils.AppHelper;

/**
 *
 * User: 快速体验
 * Date: 13-11-16
 * Time: 下午1:40
 * To change this template use File | Settings | File Templates.
 */
public class QuickActivity extends BaseActivity
{
    
    private Context mContext;
    
    private Button btnQuick2;
    
    private TextView tvVersion;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        initViews();
    }
    
    private void initViews()
    {
        mContext = this;
        setContentView(R.layout.activity_guide);
        btnQuick2 = getViewById(R.id.btn_login);
        String merchantNo = LocalAccountManager.getInstance().getMerchantNo();
        if ("".equals(merchantNo))
        {
            //            btnQuick2.setText(R.string.msg_login_action_d_title);
        }
        tvVersion = getViewById(R.id.tv_versionName);
        tvVersion.setText("v:" + AppHelper.getVerName(mContext));
        //(ApiConstant.pad == 0 ? "SPad ":"SPay " )+AppHelper.getVerName(mContext)
    }
    
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    public static void startActivity(Context context)
    {
        Intent it = new Intent();
        it.setClass(context, QuickActivity.class);
        context.startActivity(it);
    }
    
    public void onLogin(View v)
    {
        LoginActivity.startActivity(mContext);
        finish();
    }
    
    public void onGuideLogin(View v)
    {
        WelcomeGuideActivity.startActivity(mContext);
        finish();
    }
}
