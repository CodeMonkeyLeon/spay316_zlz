/*
 * 文 件 名:  PushMoneyActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-8-12
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.wallet;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.wallet.WalletManager;
import cn.swiftpass.enterprise.bussiness.model.Blance;
import cn.swiftpass.enterprise.bussiness.model.WalletModel;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.Utils;

import com.tencent.stat.StatService;

/**
 * 提现
 * 
 * @author  he_hui
 * @version  [版本号, 2016-8-12]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class PushMoneyActivity extends TemplateActivity
{
    private TextView tv_use_blance, tv_bank_name, tv_bank_num, tv_user_blance;
    
    private EditText et_money;
    
    private String availBlance;
    
    private Button but_blance;
    
    private String cardno; //银行卡后四位
    
    private TextView tv_update, tv_prompt;
    
    private DialogInfo dialogInfo;
    
    WalletModel walletModel = new WalletModel();
    
    private RelativeLayout ly_pormpt;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push_money);
        
        inintView();
        but_blance.getBackground().setAlpha(102);
        
        //        Blance blance = (Blance)getIntent().getSerializableExtra("blance");
        //        if (blance != null)
        //        {
        //            tv_bank_name.setText(blance.getwDBankName());
        //            tv_bank_num.setText(getString(R.string.tv_last) + blance.getwDAcNo() + getString(R.string.tv_card));
        //        }
        
        MainApplication.listActivities.add(this);
    }
    
    @Override
    protected void onResume()
    {
        super.onResume();
        loadeWalletQueryBalanc();//加载可用余额
    }
    
    private void inintView()
    {
        ly_pormpt = getViewById(R.id.ly_pormpt);
        tv_update = getViewById(R.id.tv_update);
        tv_prompt = getViewById(R.id.tv_prompt);
        tv_update.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                showPage(WalletOpenActivity.class);
                PushMoneyActivity.this.finish();
            }
        });
        
        tv_use_blance = getViewById(R.id.tv_use_blance);
        tv_bank_name = getViewById(R.id.tv_bank_name);
        tv_bank_num = getViewById(R.id.tv_bank_num);
        et_money = getViewById(R.id.et_money);
        tv_user_blance = getViewById(R.id.tv_user_blance);
        but_blance = getViewById(R.id.but_blance);
        //        et_money.setFocusableInTouchMode(true);
        //        et_money.requestFocus();
        showSoftInputFromWindow(PushMoneyActivity.this, et_money);
        tv_user_blance.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                String tipCrash = PreferenceUtil.getString("tipCrash", "");
                if (!StringUtil.isEmptyOrNull(tipCrash))
                {
                    toastDialog(PushMoneyActivity.this, tipCrash, null);
                }
                
                if (walletModel.getBalance() > 0)
                {
                    //                    et_money.setText(walletModel.getBalance() / 100 + "");
                    et_money.setText(DateUtil.formatMoneyUtils(walletModel.getBalance()));
                    et_money.setSelection(et_money.getText().toString().length());
                }
            }
        });
        
        setPricePoint(et_money);
        
        but_blance.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (StringUtil.isEmptyOrNull(et_money.getText().toString()))
                {
                    toastDialog(PushMoneyActivity.this, R.string.tv_push_isnot_null, null);
                    et_money.setFocusable(true);
                    return;
                }
                final String money = et_money.getText().toString();
                if (money.contains(".") && money.substring(money.lastIndexOf('.')).equals("."))
                {
                    toastDialog(PushMoneyActivity.this, R.string.tv_moeny_format, null);
                    et_money.setFocusable(true);
                    return;
                }
                
                final String strTotalMoney;
                if (money.contains(","))
                {
                    strTotalMoney = OrderManager.getInstance().getMoney(money.replace(",", ""));
                }
                else
                {
                    strTotalMoney = OrderManager.getInstance().getMoney(money);
                }
                
                double balance = walletModel.getBalance();
                
                if (Long.parseLong(strTotalMoney) > balance)
                {
                    toastDialog(PushMoneyActivity.this, R.string.tv_wallet_prompt_moeny_errpr, null);
                    et_money.setFocusable(true);
                    return;
                }
                if (Long.parseLong(strTotalMoney) > 5000000)
                {//
                    toastDialog(PushMoneyActivity.this, R.string.tx_wallet_push_max, null);
                    et_money.setFocusable(true);
                    return;
                }
                
                dialogInfo =
                    new DialogInfo(PushMoneyActivity.this, getString(R.string.public_cozy_prompt),
                        getString(R.string.tx_wallet_push_promt1) + money + getString(R.string.tx_wallet_push_promt2)
                            + tv_bank_name.getText().toString() + tv_bank_num.getText().toString()
                            + getString(R.string.tx_wallet_push_promt3), getString(R.string.btnOk),
                        getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn()
                        {
                            
                            @Override
                            public void handleOkBtn()
                            {
                                try
                                {
                                    StatService.trackCustomEvent(PushMoneyActivity.this,
                                        "SPConstTapWithDrawButton",
                                        "提现");
                                }
                                catch (Exception e)
                                {
                                }
                                pushMoeny(strTotalMoney, cardno);
                            }
                            
                            @Override
                            public void handleCancleBtn()
                            {
                                dialogInfo.cancel();
                            }
                        }, null);
                
                DialogHelper.resize(PushMoneyActivity.this, dialogInfo);
                dialogInfo.show();
                
                //                PushMoneyManager.getInstance().accountWithdrawals(new UINotifyListener<Blance>()
                //                {
                //                    @Override
                //                    public void onPreExecute()
                //                    {
                //                        super.onPreExecute();
                //                        showNewLoading(true, getString(R.string.public_data_loading));
                //                    }
                //                    
                //                    @Override
                //                    public void onError(Object object)
                //                    {
                //                        super.onError(object);
                //                        dismissLoading();
                //                        if (checkSession())
                //                        {
                //                            return;
                //                        }
                //                        
                //                        if (object != null)
                //                        {
                //                            showToastInfo(object.toString());
                //                        }
                //                    }
                //                    
                //                    @Override
                //                    public void onSucceed(Blance result)
                //                    {
                //                        super.onSucceed(result);
                //                        dismissLoading();
                //                        if (null != result)
                //                        {
                //                            PushMoneySuccActivity.startActivity(PushMoneyActivity.this, result);
                //                            HandlerManager.notifyMessage(HandlerManager.PAY_ACTIVE_SETTING_SCAN,
                //                                HandlerManager.PAY_PUSH_MONEY);
                //                        }
                //                    }
                //                },
                //                    m);
            }
        });
    }
    
    private void setPricePoint(final EditText editText)
    {
        editText.addTextChangedListener(new TextWatcher()
        {
            
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.toString().contains("."))
                {
                    if (s.length() - 1 - s.toString().indexOf(".") > 2)
                    {
                        s = s.toString().subSequence(0, s.toString().indexOf(".") + 3);
                        editText.setText(s);
                        editText.setSelection(s.length());
                    }
                }
                if (s.toString().trim().substring(0).equals("."))
                {
                    s = "0" + s;
                    editText.setText(s);
                    editText.setSelection(2);
                }
                
                if (s.toString().startsWith("0") && s.toString().trim().length() > 1)
                {
                    if (!s.toString().substring(1, 2).equals("."))
                    {
                        editText.setText(s.subSequence(0, 1));
                        editText.setSelection(1);
                        return;
                    }
                }
                
                int len = s.toString().trim().length();
                
                if (len > 0)
                {
                    //                    but_blance.setEnabled(true);
                    //                    but_blance.setBackgroundResource(R.drawable.btn_register);
                    //                    but_blance.setTextColor(Color.parseColor(getString(R.color.white)));
                    
                    setButtonBg(but_blance, true, R.string.tx_apply_out);
                    long m1 = 0;
                    long balance = walletModel.getBalance();
                    if (s.toString().contains("."))
                    {
                        double d;
                        if (s.toString().contains(","))
                        {
                            d = Double.parseDouble(OrderManager.getInstance().getMoney(s.toString().replace(",", "")));
                            
                        }
                        else
                        {
                            //                            d = Double.parseDouble(s.toString()) * 100;
                            
                            d = Double.parseDouble(OrderManager.getInstance().getMoney(s.toString()));
                        }
                        
                        if (d > balance)
                        {//
                         //输入的金额不能大于可提现金额
                            setButtonBg(but_blance, false, R.string.tx_apply_out);
                            tv_prompt.setText(R.string.tv_wallet_money_than);
                            ly_pormpt.setVisibility(View.GONE);
                            tv_prompt.setVisibility(View.VISIBLE);
                        }
                        else if (d > 5000000)
                        {
                            setButtonBg(but_blance, false, R.string.tx_apply_out);
                            //                        toastDialog(PushMoneyActivity.this, R.string.tx_wallet_push_max, null);
                            tv_prompt.setText(R.string.tv_wallet_money_prompt);
                            ly_pormpt.setVisibility(View.GONE);
                            tv_prompt.setVisibility(View.VISIBLE);
                        }
                        else if (d == 0.0)
                        {
                            setButtonBg(but_blance, false, R.string.tx_apply_out);
                        }
                        else
                        {
                            tv_prompt.setVisibility(View.GONE);
                            ly_pormpt.setVisibility(View.VISIBLE);
                        }
                        
                        long max = 1000000000;
                        if (d > max)
                        {
                            if (!s.toString().trim().contains("."))
                            {
                                s = s.toString().substring(0, len - 1);
                                editText.setText(s);
                                editText.setSelection(s.length());
                            }
                            
                        }
                    }
                    else
                    {
                        
                        if (s.toString().contains(","))
                        {
                            m1 = Long.parseLong(OrderManager.getInstance().getMoney(s.toString().replace(",", "")));
                        }
                        else
                        {
                            m1 = Long.parseLong(s.toString()) * 100;
                        }
                        
                        if (m1 > balance)
                        {//
                         //输入的金额不能大于可提现金额
                            setButtonBg(but_blance, false, R.string.tx_apply_out);
                            tv_prompt.setText(R.string.tv_wallet_money_than);
                            ly_pormpt.setVisibility(View.GONE);
                            tv_prompt.setVisibility(View.VISIBLE);
                        }
                        else if (m1 > 5000000)
                        {
                            setButtonBg(but_blance, false, R.string.tx_apply_out);
                            //                        toastDialog(PushMoneyActivity.this, R.string.tx_wallet_push_max, null);
                            tv_prompt.setText(R.string.tv_wallet_money_prompt);
                            ly_pormpt.setVisibility(View.GONE);
                            tv_prompt.setVisibility(View.VISIBLE);
                        }
                        else if (m1 == 0)
                        {
                            setButtonBg(but_blance, false, R.string.tx_apply_out);
                        }
                        else
                        {
                            tv_prompt.setVisibility(View.GONE);
                            ly_pormpt.setVisibility(View.VISIBLE);
                        }
                        if (Utils.compareTo(m1 + "", "100000000000"))
                        {
                            if (!s.toString().trim().contains("."))
                            {
                                s = s.toString().substring(0, len - 1);
                                editText.setText(s);
                                editText.setSelection(s.length());
                            }
                            
                        }
                    }
                }
                else
                {
                    //                    but_blance.setEnabled(false);
                    //                    but_blance.setBackgroundResource(R.drawable.general_button_grey_default);
                    //                    but_blance.setTextColor(Color.parseColor(getString(R.color.bt_bg_def)));
                    
                    setButtonBg(but_blance, false, R.string.tx_apply_out);
                }
                
            }
            
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                
            }
            
            @Override
            public void afterTextChanged(Editable s)
            {
                // TODO Auto-generated method stub
                
            }
            
        });
        
    }
    
    int count = 0;
    
    private void loadDate(String order)
    {
        WalletManager.getInstance().ewalletQueryOrder(order, new UINotifyListener<String>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                //                showNewLoading(true, getString(R.string.public_data_loading));
            }
            
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                dismissLoading();
                if (checkSession())
                {
                    return;
                }
                
                if (object != null)
                {
                    toastDialog(PushMoneyActivity.this, object.toString(), null);
                }
            }
            
            @Override
            public void onSucceed(String result)
            {
                super.onSucceed(result);
                //                dismissLoading();
                if (null != result)
                {
                    
                    if (result.equals("1"))
                    {
                        dismissLoading();
                        WalletPushSuccActivity.startActivity(PushMoneyActivity.this, walletModel, null);
                    }
                    else
                    {
                        Log.i("hehui", "count-->" + count);
                        if (count < 3)
                        {
                            count++;
                            try
                            {
                                Thread.sleep(3000);
                                loadDate(walletModel.getOrder_no());
                            }
                            catch (InterruptedException e)
                            {
                                return;
                            }
                            
                        }
                        else
                        {
                            dismissLoading();
                            WalletPushSuccActivity.startActivity(PushMoneyActivity.this, walletModel, "timeout");
                        }
                    }
                    
                }
            }
        });
        
    }
    
    void pushMoeny(final String money, final String cardno)
    {
        WalletManager.getInstance().ewalletWithdrawals(money, cardno, new UINotifyListener<String>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                loadDialog(PushMoneyActivity.this, R.string.tx_apply_outing);
                but_blance.setEnabled(false);
                but_blance.setText(R.string.tx_apply_outing);
                but_blance.setBackgroundResource(R.drawable.general_button_grey_default);
                but_blance.setTextColor(Color.parseColor(getString(R.color.bt_bg_def)));
                
            }
            
            @Override
            public void onPostExecute()
            {
                super.onPostExecute();
            }
            
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                
                if (checkSession())
                {
                    return;
                }
                if (null != object)
                {
                    PushMoneyActivity.this.runOnUiThread(new Runnable()
                    {
                        
                        @Override
                        public void run()
                        {
                            
                            et_money.setText("");
                            but_blance.setEnabled(true);
                            but_blance.setText(R.string.tx_apply_out);
                            but_blance.setBackgroundResource(R.drawable.btn_register);
                            but_blance.setTextColor(Color.parseColor(getString(R.color.white)));
                            //                            WalletModel walletModel = new WalletModel();
                            String tv_bank = tv_bank_name.getText().toString() + "(" + cardno + ")";
                            walletModel.setBankName(tv_bank);
                            walletModel.setMoney(money);
                            if (object.toString().contains("spay"))
                            {
                                String model = object.toString();
                                if (object.toString().length() > 4)
                                {
                                    String order = model.substring(4, model.length());
                                    walletModel.setOrder_no(order);
                                    //轮询3次共9秒
                                    try
                                    {
                                        Thread.sleep(2000);
                                    }
                                    catch (InterruptedException e)
                                    {
                                        return;
                                    }
                                    loadDate(order);
                                }
                                else
                                {
                                    WalletPushSuccActivity.startActivity(PushMoneyActivity.this, walletModel, 2 + "");
                                }
                            }
                            else
                            {
                                dissDialog();
                                if (object.toString().contains("fail"))
                                { //转出失败
                                    if (object.toString().length() > 4)
                                    {
                                        String order = object.toString().substring(3, object.toString().length());
                                        walletModel.setOrder_no(order);
                                    }
                                    WalletPushSuccActivity.startActivity(PushMoneyActivity.this, walletModel, 1 + "");
                                }
                                else
                                {
                                    if (object.toString().equalsIgnoreCase("timeout"))
                                    {//超时
                                        WalletPushSuccActivity.startActivity(PushMoneyActivity.this,
                                            walletModel,
                                            "timeout");
                                    }
                                    else
                                    {
                                        
                                        toastDialog(PushMoneyActivity.this,
                                            object.toString(),
                                            new NewDialogInfo.HandleBtn()
                                            {
                                                
                                                @Override
                                                public void handleOkBtn()
                                                {
                                                    PushMoneyActivity.this.finish();
                                                }
                                                
                                            });
                                    }
                                }
                            }
                        }
                    });
                    
                }
            }
            
            @Override
            public void onSucceed(String model)
            {
                but_blance.setEnabled(true);
                but_blance.setText(R.string.tx_apply_out);
                but_blance.setBackgroundResource(R.drawable.btn_register);
                but_blance.setTextColor(Color.parseColor(getString(R.color.white)));
                dissDialog();
                if (null != model)
                {
                    
                    String tv_bank = tv_bank_name.getText().toString() + "(" + cardno + ")";
                    WalletModel walletModel = new WalletModel();
                    walletModel.setBankName(tv_bank);
                    walletModel.setMoney(money);
                    if (model.contains("spay") && model.length() > 4)
                    {
                        String order = model.substring(3, model.length());
                        walletModel.setOrder_no(order);
                    }
                    et_money.setText("");
                    WalletPushSuccActivity.startActivity(PushMoneyActivity.this, walletModel, null);
                }
            }
        });
    }
    
    /**
     * 加载余额
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void loadeWalletQueryBalanc()
    {
        WalletManager.getInstance().ewalletQueryBalanc(new UINotifyListener<WalletModel>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                loadDialog(PushMoneyActivity.this, R.string.public_data_loading);
            }
            
            @Override
            public void onPostExecute()
            {
                super.onPostExecute();
            }
            
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                dissDialog();
                if (checkSession())
                {
                    return;
                }
                if (null != object)
                {
                    toastDialog(PushMoneyActivity.this, object.toString(), new NewDialogInfo.HandleBtn()
                    {
                        
                        @Override
                        public void handleOkBtn()
                        {
                            finish();
                        }
                        
                    });
                }
            }
            
            @Override
            public void onSucceed(WalletModel model)
            {
                dissDialog();
                if (null != model)
                {
                    walletModel = model;
                    tv_bank_name.setText(model.getBankName());
                    tv_bank_num.setText("(" + model.getSuffixIdCard() + ")");
                    cardno = model.getSuffixIdCard();
                    if (model.getBalance() > 0)
                    {
                        ly_pormpt.setVisibility(View.VISIBLE);
                        tv_use_blance.setText(getString(R.string.tv_wallet_moeny)
                            + DateUtil.formatMoneyUtils(model.getBalance()));
                        //                        but_blance.setEnabled(true);
                    }
                    if (model.getBalance() > 5000000)
                    { //大于5万
                        ly_pormpt.setVisibility(View.VISIBLE);
                        tv_user_blance.setVisibility(View.GONE);
                    }
                    
                    et_money.setText("");
                }
            }
        });
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tx_apply_out);
        titleBar.setRightButLayVisibleForTotal(false, getString(R.string.tv_push_record));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                showPage(WalletPushStreamActivity.class);
            }
            
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
        });
    }
    
    public static void startActivity(Context context, Blance blance)
    {
        Intent it = new Intent();
        it.setClass(context, PushMoneyActivity.class);
        it.putExtra("blance", blance);
        context.startActivity(it);
    }
}
