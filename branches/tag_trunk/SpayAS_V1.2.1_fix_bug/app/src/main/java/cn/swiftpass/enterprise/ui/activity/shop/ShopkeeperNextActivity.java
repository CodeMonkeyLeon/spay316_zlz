/*
 * 文 件 名:  ShopkeeperNextActivity.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.shop;

import java.io.File;
import java.util.Date;

import net.tsz.afinal.FinalBitmap;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.MerchantTempDataModel;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.SelectTowDialog;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 资料完善2
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-11]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class ShopkeeperNextActivity extends TemplateActivity
{
    private final static String TAG = ShopkeeperNextActivity.class.getCanonicalName();
    
    private Button previous;
    
    private Button next;
    
    // 正面省份照片
    private ImageView id_up_photo;
    
    //反面身份证照片
    private ImageView id_down_photo;
    
    // 手持省份证
    private ImageView id_people_photo;
    
    // 税务局证照片
    private ImageView id_ird;
    
    // 机构代码照片
    private ImageView id_code;
    
    private SharedPreferences sharedPreferences;
    
    private SelectTowDialog takImgDialog;
    
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x1001;
    
    public static final int REQUEST_CODE_CAPTURE_PICTURE = 0x1002;
    
    public static final int REQUEST_CODE_CAPTURE_PICTURE_CROP = 0x1003;
    
    public static final int ID_UP_PHOTO_TAG = 0;
    
    public static final int ID_DOWN_PHOTO_TAG = 1;
    
    public static final int ID_PEOPLE_PHOTO_TAG = 2;
    
    public static final int ID_IRD_TAG = 3;
    
    public static final int ID_CODE_TAG = 4;
    
    private File tempFile;
    
    public static boolean IMAGE_PREVIEW = true;
    
    public static boolean cancel_perview = true;
    
    /** 拍照保存照片的uri */
    private Uri originalUri = null;
    
    // 正面照查看
    private TextView id_up_photo_look;
    
    private TextView id_down_photo_look;
    
    private TextView id_people_photo_look;
    
    private TextView id_ird_look;
    
    private TextView id_code_look;
    
    private Integer imageID;
    
    private static final int SCALE = 5;//照片缩小比例
    
    private MerchantTempDataModel dataModel;
    
    private FinalBitmap finalBitmap;
    
    Handler handler = new Handler()
    {
        public void handleMessage(android.os.Message msg)
        {
            switch (msg.what)
            {
                case HandlerManager.SHOWPIC:
                    imageID = (Integer)msg.obj;
                    break;
                
                default:
                    break;
            }
        };
    };
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.personal_data_id_info);
        
        sharedPreferences = this.getSharedPreferences("dataTwo", 0);
        HandlerManager.registerHandler(HandlerManager.SHOWPIC, handler);
        initView();
        
        setLister();
        
        finalBitmap = FinalBitmap.create(ShopkeeperNextActivity.this);//
        try
        {
            finalBitmap.configDiskCachePath(AppHelper.getImgCacheDir());
        }
        catch (Exception e)
        {
            Log.e(TAG, "configDiskCachePath failed ");
        }
        
        Bundle bundle = this.getIntent().getBundleExtra("bundle");
        
        // 从商户信息 跳转到资料完善
        if (bundle != null)
        {
            dataModel = (MerchantTempDataModel)bundle.get("merchantDataModel");
            
            loadData(dataModel);
        }
        else
        {
            initValue();
        }
        
    }
    
    /** 把图片数据加载
     * <功能详细描述>
     * @param dataModel2
     * @see [类、类#方法、类#成员]
     */
    private void loadData(MerchantTempDataModel dataModels)
    {
        //        id_up_photo_Ulr = dataModel.getIdCardJustPic();
        String path = ApiConstant.BASE_URL_PORT + ApiConstant.DONLOAD_IMAGE;
        if (!"".equals(dataModel.getIdentityPic()) && dataModels.getIdentityPic() != null)
        {
            sharedPreferences.edit().putString("id_up_photo", dataModels.getIdentityPic()).commit();
            finalBitmap.display(id_up_photo, path + dataModel.getIdentityPic());
        }
        else
        { // 为空，提示上传失败，重新上传照片
        
        }
        
        if (!"".equals(dataModel.getIdCardTrunmPic()) && dataModel.getIdCardTrunmPic() != null)
        {
            sharedPreferences.edit().putString("id_down_photo", dataModel.getIdCardTrunmPic()).commit();
            finalBitmap.display(id_down_photo, path + dataModel.getIdCardTrunmPic());
        }
        else
        { // 为空，提示上传失败，重新上传照片
        
        }
        
        if (!"".equals(dataModel.getLicensePic()) && dataModel.getLicensePic() != null)
        {
            sharedPreferences.edit().putString("id_people_photo", dataModel.getLicensePic()).commit();
            finalBitmap.display(id_people_photo, path + dataModel.getLicensePic());
        }
        else
        { // 为空，提示上传失败，重新上传照片
        
        }
        if (!"".equals(dataModel.getTaxRegCertPic()) && dataModel.getTaxRegCertPic() != null)
        {
            sharedPreferences.edit().putString("id_ird", dataModel.getTaxRegCertPic()).commit();
            finalBitmap.display(id_ird, path + dataModel.getTaxRegCertPic());
        }
        else
        { // 为空，提示上传失败，重新上传照片
        
        }
        if (!"".equals(dataModel.getOrgaCodeCertPic()) && dataModel.getOrgaCodeCertPic() != null)
        {
            sharedPreferences.edit().putString("id_code", dataModel.getOrgaCodeCertPic()).commit();
            finalBitmap.display(id_code, path + dataModel.getOrgaCodeCertPic());
        }
        else
        { // 为空，提示上传失败，重新上传照片
        
        }
        
    }
    
    String root;
    
    // 从SharedPreferences读取数据
    private void initValue()
    {
        String id_up_photo_Ulr = sharedPreferences.getString("id_up_photo", "");
        String id_down_photo_Ulr = sharedPreferences.getString("id_down_photo", "");
        String id_people_photo_Ulr = sharedPreferences.getString("id_people_photo", "");
        String id_ird_Ulr = sharedPreferences.getString("id_ird", "");
        String id_code_Ulr = sharedPreferences.getString("id_code", "");
        Bitmap bitmap = null;
        try
        {
            root = AppHelper.getImgCacheDir();
            
            if (!"".equals(id_up_photo_Ulr) && null != id_up_photo_Ulr)
            {
                bitmap = ImagePase.createBitmap(id_up_photo_Ulr, 100);
                if (bitmap != null)
                {
                    id_up_photo.setImageBitmap(bitmap);
                }
            }
            
            if (!"".equals(id_down_photo_Ulr) && null != id_down_photo_Ulr)
            {
                bitmap = ImagePase.createBitmap(id_down_photo_Ulr, 100);
                if (bitmap != null)
                {
                    id_down_photo.setImageBitmap(bitmap);
                }
                
            }
            if (!"".equals(id_people_photo_Ulr) && null != id_people_photo_Ulr)
            {
                bitmap = ImagePase.createBitmap(id_people_photo_Ulr, 100);
                if (bitmap != null)
                {
                    id_people_photo.setImageBitmap(bitmap);
                }
            }
            if (!"".equals(id_ird_Ulr) && null != id_ird_Ulr)
            {
                
                bitmap = ImagePase.createBitmap(id_ird_Ulr, 100);
                if (bitmap != null)
                {
                    id_ird.setImageBitmap(bitmap);
                }
            }
            if (!"".equals(id_code_Ulr) && null != id_code_Ulr)
            {
                bitmap = ImagePase.createBitmap(id_code_Ulr, 100);
                
                if (bitmap != null)
                {
                    id_code.setImageBitmap(bitmap);
                }
            }
            
        }
        catch (Exception e)
        {
            Log.e(TAG, "get root cacher dir falied " + e.getMessage());
        }
    }
    
    /**
     * 设置监听
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void setLister()
    {
        // 上一步
        previous.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                //                showPage(ShopkeeperActivity.class);
                finish();
            }
            
        });
        
        // 下一步
        next.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (dataModel != null)
                {
                    if (!"".equals(dataModel.getIdentityPic()) && dataModel.getIdentityPic() != null)
                    {
                        // 如果这时候 同事也 修改图片，优先 用sharedPreferences里面的
                        if ("".equals(sharedPreferences.getString("id_up_photo", ""))
                            || sharedPreferences.getString("id_up_photo", "") == null)
                        {
                            sharedPreferences.edit().putString("id_up_photo", dataModel.getIdentityPic()).commit();
                        }
                    }
                    // 缩略图
                    if (!"".equals(dataModel.getIdCardTrunmPic()) && dataModel.getIdCardTrunmPic() != null)
                    {
                        if ("".equals(sharedPreferences.getString("id_down_photo", ""))
                            || sharedPreferences.getString("id_down_photo", "") == null)
                        {
                            sharedPreferences.edit().putString("id_down_photo", dataModel.getIdCardTrunmPic()).commit();
                        }
                    }
                    if (!"".equals(dataModel.getLicensePic()) && dataModel.getLicensePic() != null)
                    {
                        if ("".equals(sharedPreferences.getString("id_people_photo", ""))
                            || sharedPreferences.getString("id_people_photo", "") == null)
                        {
                            sharedPreferences.edit().putString("id_people_photo", dataModel.getLicensePic()).commit();
                        }
                    }
                    
                    if (!"".equals(dataModel.getTaxRegCertPic()) && dataModel.getTaxRegCertPic() != null)
                    {
                        if ("".equals(sharedPreferences.getString("id_ird", ""))
                            || sharedPreferences.getString("id_ird", "") == null)
                        {
                            sharedPreferences.edit().putString("id_ird", dataModel.getTaxRegCertPic()).commit();
                        }
                    }
                    
                    if (!"".equals(dataModel.getOrgaCodeCertPic()) && dataModel.getOrgaCodeCertPic() != null)
                    {
                        if ("".equals(sharedPreferences.getString("id_code", ""))
                            || sharedPreferences.getString("id_code", "") == null)
                        {
                            sharedPreferences.edit().putString("id_code", dataModel.getOrgaCodeCertPic()).commit();
                        }
                    }
                }
                
                // 验证 正面身份证 和 反面身份证 和 手持身份证
                
                if ("".equals(sharedPreferences.getString("id_up_photo", ""))
                    || sharedPreferences.getString("id_up_photo", "") == null)
                {
                    ToastHelper.showInfo("申请人手持身份证照必须上传!");
                    
                    return;
                }
                
                if ("".equals(sharedPreferences.getString("id_down_photo", ""))
                    || sharedPreferences.getString("id_down_photo", "") == null)
                {
                    ToastHelper.showInfo("身份证反面照必须上传!");
                    
                    return;
                }
                // 身份证正面照该为营业执照副本
                //                if ("".equals(sharedPreferences.getString("id_people_photo", ""))
                //                    || sharedPreferences.getString("id_people_photo", "") == null)
                //                {
                //                    ToastHelper.showInfo("营业执照必须上传!");
                //                    
                //                    return;
                //                }
                
                showPage(ShopKeeperDetialActivity.class);
                
                MainApplication.listActivities.add(ShopkeeperNextActivity.this);
            }
        });
        
        // 上传正面省份照片
        id_up_photo.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                onUploadImg(v);
            }
        });
        
        // 手持身份证
        id_up_photo_look.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                String id_up_photo_Ulr = sharedPreferences.getString("id_up_photo", "");
                String lastUrl = "";
                if (dataModel != null)
                {
                    if (!"".equals(dataModel.getIdentityPic()) && dataModel.getIdentityPic() != null)
                    {
                        lastUrl = dataModel.getIdentityPic();
                    }
                }
                // 优先从sharedPreferences取值
                if (!"".equals(id_up_photo_Ulr) && id_up_photo_Ulr != null)
                {
                    showPage(ShowPciActivity.class, id_up_photo_Ulr);
                }
                else
                {
                    if (!"".equals(lastUrl) && lastUrl != null)
                    {
                        showPage(ShowPciActivity.class, lastUrl);
                    }
                }
            }
        });
        
        // 上传反面身份照片
        id_down_photo.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                onUploadImg(v);
            }
        });
        
        // 反面照查看
        id_down_photo_look.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                String id_up_photo_Ulr = sharedPreferences.getString("id_down_photo", "");
                
                String lastUrl = "";
                if (dataModel != null)
                {
                    if (!"".equals(dataModel.getIdCardTrunmPic()) && dataModel.getIdCardTrunmPic() != null)
                    {
                        lastUrl = dataModel.getIdCardTrunmPic();
                    }
                }
                
                if (!"".equals(id_up_photo_Ulr) && id_up_photo_Ulr != null)
                {
                    showPage(ShowPciActivity.class, id_up_photo_Ulr);
                }
                else
                {
                    if (!"".equals(lastUrl) && lastUrl != null)
                    {
                        showPage(ShowPciActivity.class, lastUrl);
                    }
                }
            }
        });
        
        // 上传手持省份照片
        id_people_photo.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                onUploadImg(v);
            }
        });
        
        // 营业执照
        id_people_photo_look.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                String id_up_photo_Ulr = sharedPreferences.getString("id_people_photo", "");
                
                String lastUrl = "";
                if (dataModel != null)
                {
                    if (!"".equals(dataModel.getLicensePic()) && dataModel.getLicensePic() != null)
                    {
                        lastUrl = dataModel.getLicensePic();
                    }
                }
                
                if (!"".equals(id_up_photo_Ulr) && id_up_photo_Ulr != null)
                {
                    showPage(ShowPciActivity.class, id_up_photo_Ulr);
                }
                else
                {
                    if (!"".equals(lastUrl) && lastUrl != null)
                    {
                        showPage(ShowPciActivity.class, lastUrl);
                    }
                }
            }
        });
        
        // 上传税务照片
        id_ird.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                onUploadImg(v);
            }
        });
        
        // 税务照查看
        id_ird_look.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                String id_up_photo_Ulr = sharedPreferences.getString("id_ird", "");
                
                String lastUrl = "";
                if (dataModel != null)
                {
                    if (!"".equals(dataModel.getTaxRegCertPic()) && dataModel.getTaxRegCertPic() != null)
                    {
                        lastUrl = dataModel.getTaxRegCertPic();
                    }
                }
                
                if (!"".equals(id_up_photo_Ulr) && id_up_photo_Ulr != null)
                {
                    showPage(ShowPciActivity.class, id_up_photo_Ulr);
                }
                else
                {
                    if (!"".equals(lastUrl) && lastUrl != null)
                    {
                        showPage(ShowPciActivity.class, lastUrl);
                    }
                }
            }
        });
        
        // 上传结构照片
        id_code.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                onUploadImg(v);
            }
        });
        
        // 结构照查看
        id_code_look.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                String id_up_photo_Ulr = sharedPreferences.getString("id_code", "");
                String lastUrl = "";
                if (dataModel != null)
                {
                    if (!"".equals(dataModel.getOrgaCodeCertPic()) && dataModel.getOrgaCodeCertPic() != null)
                    {
                        lastUrl = dataModel.getOrgaCodeCertPic();
                    }
                }
                if (!"".equals(id_up_photo_Ulr) && id_up_photo_Ulr != null)
                {
                    showPage(ShowPciActivity.class, id_up_photo_Ulr);
                }
                else
                {
                    if (!"".equals(lastUrl) && lastUrl != null)
                    {
                        showPage(ShowPciActivity.class, lastUrl);
                    }
                }
            }
        });
    }
    
    /**
     * 上传图片
     * <功能详细描述>
     * @param view
     * @see [类、类#方法、类#成员]
     */
    public void onUploadImg(final View view)
    {
        view.setEnabled(false);
        // 从相册选择照片
        final String status = Environment.getExternalStorageState();
        takImgDialog =
            new SelectTowDialog(ShopkeeperNextActivity.this, "选择照片来源", new String[] {"相册", "拍照"},
                new SelectTowDialog.OnSelectTowListener()
                {
                    @Override
                    public void onTow()
                    {
                        try
                        {
                            // 是否有SD卡
                            if (status.equals(Environment.MEDIA_MOUNTED))
                            {
                                
                                takeImg(view);
                            }
                            else
                            {
                                Toast.makeText(ShopkeeperNextActivity.this, "没有SD卡，请检查SD卡是否可用!", Toast.LENGTH_SHORT)
                                    .show();
                            }
                            view.setEnabled(true);
                            takImgDialog.dismiss();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                    
                    @Override
                    public void onOne()
                    {
                        try
                        {
                            if (status.equals(Environment.MEDIA_MOUNTED))
                            {
                                startCamrae(view);
                            }
                            else
                            {
                                Toast.makeText(ShopkeeperNextActivity.this, "没有SD卡，请检查SD卡是否可用!", Toast.LENGTH_SHORT)
                                    .show();
                            }
                            view.setEnabled(true);
                            takImgDialog.dismiss();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                });
        DialogHelper.resize(this, takImgDialog);
        takImgDialog.show();
        takImgDialog.setCanceledOnTouchOutside(true);
        view.setEnabled(true);
        
    }
    
    /**
     * 拍照
     * <功能详细描述>
     * @throws Exception
     * @see [类、类#方法、类#成员]
     */
    private void startCamrae(View view)
        throws Exception
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String root = AppHelper.getImgCacheDir();
        imageUrl = root + String.valueOf(new Date().getTime()) + ".jpg";
        tempFile = new File(imageUrl);
        originalUri = Uri.fromFile(tempFile);
        
        ImageView imageView = (ImageView)view;
        
        imageID = imageView.getId();
        
        HandlerManager.notifyMessage(HandlerManager.SHOWPIC, HandlerManager.SHOWPIC, imageView.getId());
        intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, originalUri);
        startActivityForResult(intent, REQUEST_CODE_CAPTURE_PICTURE);
    }
    
    String imageUrl;
    
    /**
     * 获取相册图片
     * @throws Exception
     */
    public void takeImg(View view)
        throws Exception
    {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setDataAndType(MediaStore.Images.Media.INTERNAL_CONTENT_URI, "image/*");
        //        intent.setType("image/*");
        //        String root = AppHelper.getImgCacheDir();
        //        imageUrl = root + String.valueOf(new Date().getTime()) + ".jpg";
        //        tempFile = new File(imageUrl);
        
        ImageView imageView = (ImageView)view;
        
        imageID = imageView.getId();
        HandlerManager.notifyMessage(HandlerManager.SHOWPIC, HandlerManager.SHOWPIC, imageView.getId());
        
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
    }
    
    int w, h;
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
        {
            switch (requestCode)
            {
            // 相册获取图片
                case REQUEST_CODE_TAKE_PICTURE:
                    if (ShopkeeperNextActivity.IMAGE_PREVIEW)
                    {
                        originalUri = data.getData();
                        Intent intent = new Intent();
                        intent.setData(originalUri);
                        intent.setClass(this, ImagePreviewActivity.class);
                        
                        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
                        return;
                    }
                    else
                    {
                        ShopkeeperNextActivity.IMAGE_PREVIEW = true;
                    }
                    
                    if (originalUri == null)
                    {
                        break;
                    }
                    // 取消
                    if (!cancel_perview)
                    {
                        cancel_perview = true;
                        break;
                    }
                    ImageView imageView = (ImageView)findViewById(imageID);
                    
                    String path = getPicPath(originalUri);
                    Bitmap bitmap = null;
                    
                    try
                    {
                        
                        //                        File file = new File(path);
                        //                        String newUrlStr = path.substring(0, path.lastIndexOf('/'));
                        //                        newPath = newUrlStr + "/" + String.valueOf(new Date().getTime()) + ".jpg";
                        //                        if (file.exists())
                        //                        {
                        //                            newFile = new File(newPath);
                        //                            file.renameTo(newFile);
                        //                        }
                        //                        fos = new FileOutputStream(newFile);
                        if (path != null)
                        {
                            File file = new File(path);
                            if (file.exists() && file.length() / 1024 > 100)
                            {
                                bitmap = ImagePase.createBitmap(path, ImagePase.bitmapSize_Image);
                            }
                            else
                            {
                                //            bitmap = BitmapFactory.decodeFile(path);
                                bitmap = ImagePase.readBitmapFromStream(path);
                            }
                            //                            bitmap = BitmapFactory.decodeFile(path);
                        }
                    }
                    catch (Exception e1)
                    {
                        e1.printStackTrace();
                    }
                    
                    // 如果图片大于100k，则进行压缩
                    //                    String path = ImagePase.imageCompress(beforePath, REQUEST_CODE_TAKE_PICTURE, 100);
                    String thumbPath = "";
                    try
                    {
                        // 得到缩略图的路径
                        //                        thumbPath = ImagePase.getThumbNail(path);
                    }
                    catch (Exception e)
                    {
                        Log.e(TAG, " get thumb failed " + e.getMessage());
                    }
                    
                    if (bitmap != null)
                    {
                        imageView.setImageBitmap(bitmap);
                    }
                    switch (imageID)
                    {
                        case R.id.id_up_photo://Thumb
                            sharedPreferences.edit().putString("id_up_photo", path).commit();
                            // 缩略图
                            break;
                        case R.id.id_down_photo:
                            sharedPreferences.edit().putString("id_down_photo", path).commit();
                            break;
                        case R.id.id_people_photo:
                            sharedPreferences.edit().putString("id_people_photo", path).commit();
                            break;
                        case R.id.id_ird:
                            sharedPreferences.edit().putString("id_ird", path).commit();
                            break;
                        case R.id.id_code:
                            sharedPreferences.edit().putString("id_code", path).commit();
                            break;
                    }
                    takImgDialog.dismiss();
                    break;
                
                // 拍照
                case REQUEST_CODE_CAPTURE_PICTURE:
                    
                    ImageView image = (ImageView)findViewById(imageID);
                    String pathPhoto = getPicPath(originalUri);
                    Bitmap bitmap_pci = null;
                    // 得到缩略图的路径
                    try
                    {
                        
                        File file = new File(pathPhoto);
                        if (file.exists() && file.length() / 1024 > 100)
                        {
                            bitmap_pci = ImagePase.createBitmap(pathPhoto, ImagePase.bitmapSize_Image);
                        }
                        else
                        {
                            bitmap_pci = ImagePase.readBitmapFromStream(pathPhoto);
                        }
                        
                        if (bitmap_pci != null)
                        {
                            image.setImageBitmap(bitmap_pci);
                        }
                        else
                        {
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    
                    switch (imageID)
                    {
                        case R.id.id_up_photo:
                            sharedPreferences.edit().putString("id_up_photo", pathPhoto).commit();
                            break;
                        
                        case R.id.id_down_photo:
                            sharedPreferences.edit().putString("id_down_photo", pathPhoto).commit();
                            break;
                        case R.id.id_people_photo:
                            sharedPreferences.edit().putString("id_people_photo", pathPhoto).commit();
                            break;
                        case R.id.id_ird:
                            sharedPreferences.edit().putString("id_ird", pathPhoto).commit();
                            break;
                        case R.id.id_code:
                            sharedPreferences.edit().putString("id_code", pathPhoto).commit();
                            break;
                    }
                    takImgDialog.dismiss();
                    break;
                default:
                    break;
            }
            
        }
        else
        {
            //Toast.makeText(this, "获取照片失败,重新获取！", Toast.LENGTH_LONG).show();
            return;
        }
    }
    
    //    public String 
    
    /**
     * Constructs an intent for image cropping. 调用图片剪辑程序
     */
    protected static Intent getCropImageIntent(Uri photoUri)
    {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(photoUri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 100);
        intent.putExtra("outputY", 100);
        intent.putExtra("return-data", true);
        return intent;
    }
    
    /**
     * 页面参数初始化
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView()
    {
        previous = (Button)findViewById(R.id.previous);
        
        next = (Button)findViewById(R.id.next);
        
        id_up_photo = (ImageView)findViewById(R.id.id_up_photo);
        id_down_photo = (ImageView)findViewById(R.id.id_down_photo);
        id_people_photo = (ImageView)findViewById(R.id.id_people_photo);
        id_ird = (ImageView)findViewById(R.id.id_ird);
        id_code = (ImageView)findViewById(R.id.id_code);
        
        id_up_photo_look = (TextView)findViewById(R.id.id_up_photo_look);
        id_down_photo_look = (TextView)findViewById(R.id.id_down_photo_look);
        id_ird_look = (TextView)findViewById(R.id.id_ird_look);
        id_code_look = (TextView)findViewById(R.id.id_code_look);
        id_people_photo_look = getViewById(R.id.id_people_photo_look);
        
        id_up_photo.setTag(ID_UP_PHOTO_TAG);
        id_down_photo.setTag(ID_DOWN_PHOTO_TAG);
        id_people_photo.setTag(ID_PEOPLE_PHOTO_TAG);
        id_ird.setTag(ID_IRD_TAG);
        id_up_photo_look.setTag(ID_CODE_TAG);
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.btn_finish);
    }
    
    /**
     * 
     * 返回图片地址
     * @param originalUri
     * @return String
     */
    public String getPicPath(Uri originalUri)
    {
        ContentResolver mContentResolver = ShopkeeperNextActivity.this.getContentResolver();
        String originalPath = null;
        if ((originalUri + "").contains("/sdcard"))
        {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/sdcard"));
        }
        else if ((originalUri + "").contains("/data"))
        {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/data"));
        }
        else if ((originalUri + "").contains("/storage")) // 小米 手机 适配
        {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/storage"));
        }
        else
        {
            Cursor cursor = mContentResolver.query(originalUri, null, null, null, null);
            if (cursor != null)
            {
                cursor.moveToFirst();
                originalPath = cursor.getString(1);
                cursor.close();
            }
        }
        
        return originalPath;
    }
}
