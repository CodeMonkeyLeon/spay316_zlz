package com.cxy.networklibrary.fragment;

import android.os.Bundle;

/**
 * 创建人：caoxiaoya
 * 时间：2018/6/13
 * 描述：LifeCycleCallback实现相关的基类功能，那么你的Fragment需实现此接口
 * 备注：
 */

public interface IBaseFragment {

    /**
     * 需要保存数据时，将数据写进bundleToSave
     */
    void onSaveState(Bundle bundleToSave);

    /**
     * 从bundleToRestore中获取你保存金曲的数据
     */
    void onRestoreState(Bundle bundleToRestore);


}
