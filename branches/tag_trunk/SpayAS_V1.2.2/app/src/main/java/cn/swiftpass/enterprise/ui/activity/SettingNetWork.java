package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 网络设置对话框
 * 暂无用
 */
public class SettingNetWork
{
    private Context mContext;
    
    private SharedPreferences sp;
    
    private LayoutInflater inflater;
    
    private SettingNetWork()
    {
    }
    
    private static SettingNetWork settingNetWork;
    
    public static SettingNetWork getInstance()
    {
        if (settingNetWork == null)
        {
            settingNetWork = new SettingNetWork();
        }
        return settingNetWork;
    }
    
    public Dialog showNetWorkSet(Activity activity)
    {
        this.mContext = activity;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sp = PreferenceManager.getDefaultSharedPreferences(mContext);
        View v = inflater.inflate(R.layout.view_input_3, null);
        final EditText networkIP = (EditText)v.findViewById(R.id.et_text1);
        //		networkIP.setText(sp.getString( Constant.KEY_IP_ADDRESS,ProtocolProxy.getInstance().serverAddress.getAddress().getHostAddress()));
        networkIP.setText(sp.getString(GlobalConstant.FILE_IP_ADDRESS, ApiConstant.IP));
        final EditText networkPort = (EditText)v.findViewById(R.id.et_text2);
        
        networkPort.setText(sp.getString(GlobalConstant.FILE_SERVER_PORT, ApiConstant.PORT));
        
        Dialog dialog =
            new AlertDialog.Builder(activity).setTitle("网络设置")
                .setView(v)
                .setPositiveButton("确定", new DialogInterface.OnClickListener()
                {
                    
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        String ip = networkIP.getText().toString().trim();
                        String port = networkPort.getText().toString().trim();
                        
                        if ("".equals(ip))
                        {
                            ToastHelper.showInfo("IP地址不能为空！");
                            DialogHelper.keepDialog(dialog);
                            return;
                        }
                        if ("".equals(port))
                        {
                            ToastHelper.showInfo("端口号不能为空！");
                            DialogHelper.keepDialog(dialog);
                            return;
                        }
                        String regex =
                            "^(25[0-5]|2[0-4]\\d|1\\d{2}|[1-9]\\d|[1-9])\\.(25[0-5]|2[0-4]\\d|1\\d{2}|[1-9]\\d|\\d)\\"
                                + ".(25[0-5]|2[0-4]\\d|1\\d{2}|[1-9]\\d|\\d)\\.(25[0-5]|2[0-4]\\d|1\\d{2}|[1-9]\\d|[1-9])$";
                        
                        /*if(!ip.matches(regex)){
                            ToastHelper.showInfo("输入无效的IP地址，请输入如xxx.xxx.xxx.xxx格式（xxx为0-255）！");
                        	DialogHelper.keepDialog(dialog);
                        	return;
                        }*/
                        if (port.length() > 4)
                        {
                            ToastHelper.showInfo("端口号有误！");
                            DialogHelper.keepDialog(dialog);
                            return;
                        }
                        
                        sp.edit().putString(GlobalConstant.FILE_IP_ADDRESS, ip).commit();
                        sp.edit().putString(GlobalConstant.FILE_SERVER_PORT, port).commit();
                        // 重置服务端
                        ApiConstant.PORT = port;
                        ApiConstant.IP = ip;
                        DialogHelper.distoryDialog(dialog);
                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener()
                {
                    
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        DialogHelper.distoryDialog(dialog);
                    }
                })
                .show();
        return dialog;
    }
}
