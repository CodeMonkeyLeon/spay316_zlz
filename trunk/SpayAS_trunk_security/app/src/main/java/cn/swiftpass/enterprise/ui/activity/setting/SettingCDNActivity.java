package cn.swiftpass.enterprise.ui.activity.setting;

import android.os.Bundle;

import com.example.common.OnSwitchButtonChangeListener;
import com.example.common.sp.PreferenceUtil;
import com.example.common.view.SwitchButton;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;

/**
 * Created by aijingya on 2018/2/7.
 *
 * @Package cn.swiftpass.enterprise.ui.activity.setting
 * @Description: ${TODO}(从登陆页面进入的网络连接优化功能的页面)
 * @date 2018/2/7.16:44.
 */

public class SettingCDNActivity extends BaseActivity {

    private SwitchButton mSwitchCDN;

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected boolean isLoginRequired() {
        return false;
    }


    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_setting);
        initView();
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.setting_network);
    }


    public void initView() {
        mSwitchCDN = findViewById(R.id.is_switch_cdn);
        //默认是开启的状态，但是可控制开启或者关闭
        String CDN_status = PreferenceUtil.getString("CDN", "open");
        if (CDN_status.equals("open")) {
            mSwitchCDN.openWithoutAnim(true);
        } else {
            mSwitchCDN.openWithoutAnim(false);
        }


        mSwitchCDN.setOnStatusChangeListener(new OnSwitchButtonChangeListener() {
            @Override
            public void onChange(boolean isOpen) {
                if (isOpen) {
                    PreferenceUtil.commitString("CDN", "open");
                } else {
                    PreferenceUtil.commitString("CDN", "close");
                }
            }
        });
    }
}
