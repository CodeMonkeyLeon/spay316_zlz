package cn.swiftpass.enterprise.utils.interfaces

interface OnPermissionGrantedListener {
    fun onPermissionGranted()
}