package cn.swiftplus.enterprise.printsdk.print.KS8223.printUtils;


import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.RemoteException;
import android.text.TextUtils;

import com.szzt.sdk.device.aidl.IPrinterListener;
import com.szzt.sdk.device.printer.Printer;

import cn.swiftplus.enterprise.printsdk.print.KS8223.POSKS8223Client;
import hk.com.tradelink.tess.android.teller.Invoke;


/**
 * Created by aijingya on 2020/11/4.
 *
 * @Package cn.swiftplus.enterprise.printsdk.print.KS8223.printUtils
 * @Description:
 * @date 2020/11/4.14:34.
 */
public class PrintKS8223Text {
    private static PrintKS8223Text mPrintKS8223Text;
    private Printer mPrint;
    private Context mContext;

    private PrintKS8223Text(){
        bindDeviceService();
    }

    public static PrintKS8223Text getInstance(){
        if(mPrintKS8223Text == null){
            mPrintKS8223Text = new PrintKS8223Text();
        }
        return mPrintKS8223Text;
    }

    public void init(Context context){
        this.mContext = context;
        mPrint.open();
    }


    public void printTitle(String text){
        mPrint.addStr(text + "\n", Printer.Font.FONT_3, true, Printer.Align.CENTER);
        mPrint.addStr("\n", Printer.Font.FONT_3, true, Printer.Align.CENTER);
    }

    public void printTextLine(String text,int align){
        mPrint.addStr(text+ "\n", Printer.Font.FONT_2, false,align);
    }

    public void printMultiLines(String text,int align){
        mPrint.addStr(text+ "\n", Printer.Font.FONT_2, false,align);
    }

    public void printEmptyLine(){
        mPrint.addStr("\n", Printer.Font.FONT_2, false,Printer.Align.CENTER);
    }

    public void printDoubleLine(){
        mPrint.addStr("================================", Printer.Font.FONT_2, false,Printer.Align.CENTER);
    }

    public void printSingleLine(){
        mPrint.addStr("--------------------------------", Printer.Font.FONT_2, false,Printer.Align.CENTER);
    }

    /**
     *  打印小票的代码
     */
    public void printQRCode(String orderNoMch){
        if(!TextUtils.isEmpty(orderNoMch)){
            mPrint.addQrCode(orderNoMch);
            mPrint.addStr("\n", Printer.Font.FONT_2, false,Printer.Align.CENTER);
        }
    }

    /**
     *  打印图片
     */
    public void printBitmap(Bitmap logoBitmap){
        try {
            if (logoBitmap != null) {
                mPrint.addImg(logoBitmap);
                mPrint.addStr("\n", Printer.Font.FONT_2, false,Printer.Align.CENTER);
            }
        }
        catch (Exception e) {
        }
    }

    public void startPrint(){
        mPrint.start(new IPrinterListener.Stub() {
            @Override
            public void PrinterNotify(final int retCode) throws RemoteException {

                if (retCode == 0) {

                }
            }
        });
    }

    public void bindDeviceService() {
        //证通pos
        try {
            POSKS8223Client.getInstance().initztpos();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mPrint = POSKS8223Client.getInstance().getPrinter();
                }
            },500);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void unbindDeviceService(){
        /*if(mPrint!=null){
            try {
                mPrint.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }*/
    }


}
