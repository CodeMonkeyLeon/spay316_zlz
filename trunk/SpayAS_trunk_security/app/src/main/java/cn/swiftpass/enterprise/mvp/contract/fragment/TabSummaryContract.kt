package cn.swiftpass.enterprise.mvp.contract.fragment

import cn.swiftpass.enterprise.bussiness.model.OrderTotalInfo
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/11/16
 *
 * @你应该是一场梦我应该是一阵风
 */
class TabSummaryContract {


    interface View : BaseView {

        fun orderCountSuccess(
            response: OrderTotalInfo,
            countMethod: Int
        )

        fun orderCountFailed(
            error: Any?,
            startTime: String?,
            endTime: String?,
            countMethod: Int,
            userId: String?,
            countDayKind: String?,
            isShowLoading: Boolean
        )
    }

    interface Presenter : BasePresenter<View> {
        fun orderCount(
            startTime: String?,
            endTime: String?,
            countMethod: Int,
            userId: String?,
            countDayKind: String?,
            isShowLoading: Boolean
        )
    }


}