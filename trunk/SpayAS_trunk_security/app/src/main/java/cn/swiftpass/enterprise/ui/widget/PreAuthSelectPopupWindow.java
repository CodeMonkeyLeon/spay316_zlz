package cn.swiftpass.enterprise.ui.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * Created by aijingya on 2018/12/13.
 *
 * @Package cn.swiftpass.enterprise.ui.widget
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/12/13.22:06.
 */

public class PreAuthSelectPopupWindow extends PopupWindow {
    //当前界面已选择的状态---支付状态的类型
    private List<String> choiceType = new ArrayList<String>();
    //选择的支付方式----微信支付宝等
    List<String> payTypeList = new ArrayList<>();
    private List<Button> listButton = new ArrayList<Button>();
    private PreAuthSelectPopupWindow.HandleBtn handleBtn;
    private Button tv_unauthorized, tv_authorized, tv_close, tv_unfreezed;
    private TextView tv_reset, tv_confirm;
    private GridViewForScrollView gv_pre_auth_type;
    private PreAuthTypeAdape PreAuthAdaper;
    private ViewPreAuthTypeHolder PreAuthHolder;
    private List<DynModel> list;
    private Activity activity;
    private View mMenuView;
    private boolean ishadhandle;
    private boolean isTag = false;
    private boolean isPreAuth;//区分是否预授权
    private Context mContext;

    public interface HandleBtn {
        void handleOkBtn(List<String> choiceType, List<String> payTypeList, boolean ishandled);
    }

    public PreAuthSelectPopupWindow(boolean isPreAuth, boolean isHandled, Activity context, List<String> ChoiceList, final List<String> payTypeList, PreAuthSelectPopupWindow.HandleBtn handleBtn) {
        super(context);
        this.isPreAuth = isPreAuth;
        mContext = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.pre_auth_pay_type_choice_dialog, null);
        this.handleBtn = handleBtn;
        this.activity = context;
        this.payTypeList = payTypeList;
        ishadhandle = isHandled;
        initview(mMenuView);
        setDate(ChoiceList);
        setListener();
        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(ViewGroup.LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int height = mMenuView.findViewById(R.id.pop_layout_pre_auth).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        for (Button b : listButton) {
                            resetBut(b);
                        }
                        if (choiceType.size() > 0) {
                            isTag = true;
                        }
                        tv_unfreezed.setEnabled(true);
                        choiceType.clear();

                        if (list != null && list.size() > 0) {
                            for (DynModel d : list) {
                                //0，默认状态 ，1，已经选中，2，置灰状态
                                d.setEnaled(0);
                            }
                            PreAuthAdaper.notifyDataSetChanged();
                        }

                        payTypeList.clear();
                        dismiss();
                    }
                }
                return true;
            }
        });
    }

    void resetBut(Button b) {
        //设置成未选中的状态
        b.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
        choiceType.remove(b.getTag().toString());
        b.setTextColor(Color.parseColor("#535353"));
        b.setEnabled(true);
    }

    void setButtonGrey(Button b) {
        //设置成灰色
        b.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
        choiceType.remove(b.getTag().toString());
        b.setTextColor(Color.parseColor("#E3E3E3"));
        b.setEnabled(false);
    }

    private void initview(View v) {
        tv_reset = (TextView) v.findViewById(R.id.tv_reset);
        gv_pre_auth_type = (GridViewForScrollView) v.findViewById(R.id.gv_pre_auth_type);
        gv_pre_auth_type.setSelector(new ColorDrawable(Color.TRANSPARENT));
        gv_pre_auth_type.setFocusable(true);
        gv_pre_auth_type.setFocusableInTouchMode(true);
        gv_pre_auth_type.requestFocus();

        tv_unauthorized = (Button) v.findViewById(R.id.tv_unauthorized);
        tv_authorized = (Button) v.findViewById(R.id.tv_authorized);
        tv_close = (Button) v.findViewById(R.id.tv_close);
        tv_unfreezed = (Button) v.findViewById(R.id.tv_unfreezed);

        tv_confirm = (TextView) v.findViewById(R.id.tv_confirm);

        if (isPreAuth) {
            tv_confirm.setBackgroundResource(R.drawable.btn_pre_finish);
            tv_reset.setTextColor(mContext.getResources().getColor(R.color.bg_text_pre_auth));
        } else {
            tv_confirm.setBackgroundResource(R.drawable.btn_finish);
            tv_reset.setTextColor(mContext.getResources().getColor(R.color.bg_text_new));
        }

        listButton.add(tv_unauthorized);
        listButton.add(tv_authorized);
        listButton.add(tv_close);
        listButton.add(tv_unfreezed);

        list = new ArrayList<DynModel>();

        List<DynModel> listTemp = new ArrayList<DynModel>();
        //默认写死只有支付宝
        Object object = SharedPreUtils.readProduct("dynPayType_pre_auth" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
        if (object != null) {
            listTemp = (List<DynModel>) object;
            //手动剔除数据，只留下支付宝的支付方式
            if (null != listTemp && listTemp.size() > 0) {
                for (int i = 0; i < listTemp.size(); i++) {
                    DynModel dynModel = listTemp.get(i);
                    if ("2".equals(dynModel.getApiCode())) {//支付宝
                        list.add(listTemp.get(i));
                    }
                }
            }

            if (null != list && list.size() > 0) {
                if (payTypeList.size() > 0) {
                    for (String s : payTypeList) {
                        for (int i = 0; i < list.size(); i++) {
                            DynModel dynModel = list.get(i);

                            if (s.equals(dynModel.getApiCode())) {
                                //0，默认状态 ，1，已经选中，2，置灰状态
                                dynModel.setEnaled(1);
                                list.set(i, dynModel);
                            }
                        }
                    }
                }
                gv_pre_auth_type.setVisibility(View.VISIBLE);
                PreAuthAdaper = new PreAuthSelectPopupWindow.PreAuthTypeAdape(activity, list);
                gv_pre_auth_type.setAdapter(PreAuthAdaper);
            } else {
                gv_pre_auth_type.setVisibility(View.GONE);
            }
        } else {
            gv_pre_auth_type.setVisibility(View.GONE);
        }
    }

    private void setDate(List<String> payList) {
        if (null != payList && payList.size() > 0) {
            for (String s : payList) {
                if (s.equals(tv_unauthorized.getTag().toString())) {
                    updatePreAuthTypeBtnBg(tv_unauthorized);
                    setUnFreezenDisabled();
                } else if (s.equals(tv_authorized.getTag().toString())) {
                    updatePreAuthTypeBtnBg(tv_authorized);
                    setUnFreezenDisabled();
                } else if (s.equals(tv_close.getTag().toString())) {
                    updatePreAuthTypeBtnBg(tv_close);
                    setUnFreezenDisabled();
                } else if (s.equals(tv_unfreezed.getTag().toString())) {
                    updateUnFreezenTypeButtonBg(tv_unfreezed);
                }
            }
        }
    }

    void setUnFreezenDisabled() {
        tv_unfreezed.setEnabled(false);
        tv_unfreezed.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
        tv_unfreezed.setTextColor(Color.parseColor("#E3E3E3"));
    }

    void setUnFreezenEnabled() {
        tv_unfreezed.setEnabled(true);
        tv_unfreezed.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
        tv_unfreezed.setTextColor(Color.parseColor("#535353"));

    }


    void updatePreAuthTypeBtnBg(Button b) {
        isTag = false;

        if (choiceType.contains(b.getTag().toString())) {
            if (!choiceType.contains(tv_unfreezed.getTag().toString())) {
                setUnFreezenEnabled();
            }
            //未选中的状态
            b.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            b.setTextColor(Color.parseColor("#535353"));
            choiceType.remove(b.getTag().toString());
        } else {
            //选中的状态
            b.setBackgroundResource(R.drawable.button_shape);
            b.setTextColor(Color.WHITE);
            choiceType.add(b.getTag().toString());
        }
    }

    public void ClickUpdatePreAuthStateBg(Button b) {
        isTag = false;
        if (choiceType.contains(b.getTag().toString())) {
            //未选中的状态
            b.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            choiceType.remove(b.getTag().toString());
            b.setTextColor(Color.parseColor("#535353"));
            if (!isContainsPayState()) {
                setUnFreezenEnabled();
            }
        } else {
            //选中的状态
            if (isPreAuth) {
                b.setBackgroundResource(R.drawable.button_pre_shape);
            } else {
                b.setBackgroundResource(R.drawable.button_shape);
            }
            choiceType.add(b.getTag().toString());
            b.setTextColor(Color.WHITE);
            b.setEnabled(true);
        }
    }

    void updateUnFreezenTypeButtonBg(Button b) {
        if (choiceType.contains(b.getTag().toString())) {
            //设置成未选中的状态
            b.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            choiceType.remove(b.getTag().toString());
            b.setTextColor(Color.parseColor("#535353"));
            b.setEnabled(true);
            setPayTypeSetEnabled(true);
        } else {
            setPayTypeSetEnabled(false);

            //设置成选中的状态
            if (isPreAuth) {
                b.setBackgroundResource(R.drawable.button_pre_shape);
            } else {
                b.setBackgroundResource(R.drawable.button_shape);
            }
            choiceType.add(b.getTag().toString());
            b.setTextColor(Color.WHITE);
            tv_unfreezed.setEnabled(true);
        }
    }

    void setPayTypeSetEnabled(boolean enable) {
        tv_unauthorized.setEnabled(enable);
        tv_authorized.setEnabled(enable);
        tv_close.setEnabled(enable);
        tv_unfreezed.setEnabled(enable);


        if (enable) {
            tv_unauthorized.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            tv_unauthorized.setTextColor(Color.parseColor("#535353"));

            tv_authorized.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            tv_authorized.setTextColor(Color.parseColor("#535353"));

            tv_close.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            tv_close.setTextColor(Color.parseColor("#535353"));

            tv_unfreezed.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            tv_unfreezed.setTextColor(Color.parseColor("#535353"));

        } else {
            tv_unauthorized.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            tv_unauthorized.setTextColor(Color.parseColor("#E3E3E3"));

            tv_authorized.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            tv_authorized.setTextColor(Color.parseColor("#E3E3E3"));

            tv_close.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            tv_close.setTextColor(Color.parseColor("#E3E3E3"));

            tv_unfreezed.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
            tv_unfreezed.setTextColor(Color.parseColor("#E3E3E3"));
        }
    }

    boolean isContainsPayState() {
        if (choiceType.contains(tv_unauthorized.getTag().toString())
                || choiceType.contains(tv_authorized.getTag().toString())
                || choiceType.contains(tv_close.getTag().toString())) {
            return true;
        }
        return false;
    }

    private void setListener() {
        gv_pre_auth_type.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int postion, long arg3) {
                DynModel dynModel = list.get(postion);
                if (dynModel != null) {
                    TextView tv = (TextView) v.findViewById(R.id.tv_content);
                    if (payTypeList.contains(dynModel.getApiCode())) {
                        payTypeList.remove(dynModel.getApiCode());

                        //设置成未选中的状态
                        tv.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
                        tv.setTextColor(Color.parseColor("#535353"));
                        tv.setEnabled(true);

                    } else {
                        payTypeList.add(dynModel.getApiCode());
                        //变成可点击
                        if (isPreAuth) {
                            tv.setBackgroundResource(R.drawable.button_pre_shape);
                        } else {
                            tv.setBackgroundResource(R.drawable.button_shape);
                        }
                        tv.setTextColor(Color.WHITE);
                        tv.setEnabled(false);

                    }
                }
            }
        });


        tv_unauthorized.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setButtonGrey(tv_unfreezed);
                ClickUpdatePreAuthStateBg(tv_unauthorized);
            }
        });

        tv_authorized.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setButtonGrey(tv_unfreezed);
                ClickUpdatePreAuthStateBg(tv_authorized);
            }
        });
        tv_close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setButtonGrey(tv_unfreezed);
                ClickUpdatePreAuthStateBg(tv_close);
            }
        });

        tv_unfreezed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUnFreezenTypeButtonBg(tv_unfreezed);
            }
        });


        tv_confirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (choiceType.size() == 0 && isTag) {
                    ToastHelper.showInfo(activity, ToastHelper.toStr(R.string.tx_bill_stream_chioce_not_null));
                    return;
                } else {
                    ishadhandle = true;
                    handleBtn.handleOkBtn(choiceType, payTypeList, true);
                    dismiss();
                }
            }
        });

        tv_reset.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                for (Button b : listButton) {
                    resetBut(b);
                }
                if (choiceType.size() > 0) {
                    isTag = true;
                }
                tv_unfreezed.setEnabled(true);
                choiceType.clear();

                if (list != null && list.size() > 0) {
                    for (DynModel d : list) {
                        d.setEnaled(0);
                    }
                    PreAuthAdaper.notifyDataSetChanged();
                }
                payTypeList.clear();
            }
        });
    }


    class PreAuthTypeAdape extends BaseAdapter {
        private List<DynModel> list;
        private Context context;

        private PreAuthTypeAdape(Context context, List<DynModel> list) {
            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.activity_bill_paytype_list_item, null);
                PreAuthHolder = new ViewPreAuthTypeHolder();
                PreAuthHolder.tv_content = (TextView) convertView.findViewById(R.id.tv_content);
                convertView.setTag(PreAuthHolder);
            } else {
                PreAuthHolder = (ViewPreAuthTypeHolder) convertView.getTag();
            }

            DynModel dynModel = list.get(position);
            if (dynModel != null && !StringUtil.isEmptyOrNull(dynModel.getProviderName())) {
                PreAuthHolder.tv_content.setText(MainApplication.getInstance().getPayTypeMap().get(dynModel.getApiCode()));
                switch (dynModel.getEnabled()) {
                    case 0://默认状态
                        PreAuthHolder.tv_content.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
                        PreAuthHolder.tv_content.setTextColor(Color.parseColor("#535353"));
                        PreAuthHolder.tv_content.setEnabled(true);
                        break;
                    case 1://选中
                        if (isPreAuth) {
                            PreAuthHolder.tv_content.setBackgroundResource(R.drawable.button_pre_shape);
                        } else {
                            PreAuthHolder.tv_content.setBackgroundResource(R.drawable.button_shape);
                        }
                        PreAuthHolder.tv_content.setTextColor(Color.WHITE);
                        PreAuthHolder.tv_content.setEnabled(true);
                        break;
                    case 2://置灰
                        PreAuthHolder.tv_content.setBackgroundResource(R.drawable.bg_general_button_secondary_chose_shape);
                        PreAuthHolder.tv_content.setTextColor(Color.parseColor("#E3E3E3"));
                        PreAuthHolder.tv_content.setEnabled(false);
                        break;

                    default:
                        break;
                }

            }
            return convertView;
        }
    }

    class ViewPreAuthTypeHolder {
        TextView tv_content;
    }

}
