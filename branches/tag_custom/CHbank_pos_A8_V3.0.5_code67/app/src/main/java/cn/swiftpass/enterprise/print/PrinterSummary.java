package cn.swiftpass.enterprise.print;

import android.content.Context;

import com.landicorp.android.eptapi.device.Printer;
import com.landicorp.android.eptapi.device.Printer.Format;
import com.landicorp.android.eptapi.exception.RequestException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalInfo;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalItemInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 新大陆 N900打印 <一句话功能简述> <功能详细描述>
 * 
 * @author he_hui
 * @version [版本号, 2016-2-4]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public abstract class PrinterSummary {

	private Context context;

	private Printer.Progress progress = new Printer.Progress() {

		@Override
		public void doPrint(Printer arg0) throws Exception {

		}

		@Override
		public void onFinish(int code) {
			if (code == Printer.ERROR_NONE) {
				displayPrinterInfo("PRINT SUCCESS END ");
			}
			/**
			 * Has some error. Here is display it, but you may want to hanle the
			 * error such as ERROR_OVERHEAT、ERROR_BUSY、ERROR_PAPERENDED to start
			 * again in the right time later.
			 */
			else {
				displayPrinterInfo("PRINT ERR - " + (code));
			}
		}

		@Override
		public void onCrash() {
			onDeviceServiceCrash();
		}

	};

	/**
	 * Search card and show all track info
	 */
	public void startPrint() {
		try {
			progress.start();
			// DeviceService.logout();
		} catch (RequestException e) {
			onDeviceServiceCrash();
		}
	}

	protected abstract void displayPrinterInfo(String info);

	protected abstract void onDeviceServiceCrash();

	public PrinterSummary(Context context, final OrderTotalInfo info) {
		this.context = context;

		progress.addStep(new Printer.Step() {

			@Override
			public void doPrint(Printer printer) throws Exception {
				printer.setAutoTrunc(true);

				printer.setMode(Printer.MODE_VIRTUAL);
				Format format = new Format();
				format.setAscSize(Format.ASC_DOT24x12);
				format.setAscScale(Format.ASC_SC1x2);
				format.setHzSize(Format.HZ_DOT24x24);
				format.setHzScale(Format.HZ_SC1x2);
				printer.setFormat(format);

				// 标题
				printer.printText(Printer.Alignment.CENTER, ToastHelper.toStr(R.string.tx_blue_print_data_sum) + "\n");


				format.setAscSize(Format.ASC_DOT24x12);
				format.setAscScale(Format.ASC_SC1x1);
				format.setHzSize(Format.HZ_DOT24x24);
				format.setHzScale(Format.HZ_SC1x1);
				printer.setFormat(format);


				//商户名称
				printer.printText("\n" + ToastHelper.toStr(R.string.shop_name) + "：" + "\n"+ MainApplication.getMchName() + "\n");

				// 商户号
				if (!StringUtil.isEmptyOrNull(MainApplication.getMchId())) {
					printer.printText(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "：" + MainApplication.getMchId() + "\n");
				}

				if (StringUtil.isEmptyOrNull(info.getUserName())) {
					//            printBuffer.append("收银员：全部收银员\n");
				} else {
					printer.printText("\n" + ToastHelper.toStr(R.string.tx_user) + "：" + info.getUserName() + "\n");
				}

				//开始时间
				printer.printText(ToastHelper.toStr(R.string.tx_blue_print_start_time) + "：" + info.getStartTime() + "\n");
				// 结束时间
				printer.printText(ToastHelper.toStr(R.string.tx_blue_print_end_time) + "：" + info.getEndTime() + "\n");
				printer.printText("===============================\n");

				if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
					printer.printText(ToastHelper.toStr(R.string.tx_bill_stream_pay_money) + "："+ DateUtil.formatRMBMoneyUtils(info.getCountTotalFee()) + ToastHelper.toStr(R.string.pay_yuan) + "\n");
				} else {
					printer.printText(ToastHelper.toStr(R.string.tx_bill_stream_pay_money) + "："+ MainApplication.getFeeType() + DateUtil.formatMoneyUtils(info.getCountTotalFee()) + "\n");
				}
				printer.printText(ToastHelper.toStr(R.string.tx_bill_stream_pay_money_num) + "："+ info.getCountTotalCount() + "\n");

				if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
					printer.printText(ToastHelper.toStr(R.string.tv_refund_dialog_refund_money)+ DateUtil.formatRMBMoneyUtils(info.getCountTotalRefundFee()) + ToastHelper.toStr(R.string.pay_yuan) + "\n");
				} else {
					printer.printText(ToastHelper.toStr(R.string.tv_refund_dialog_refund_money)+ MainApplication.getFeeType() + DateUtil.formatMoneyUtils(info.getCountTotalRefundFee()) + "\n");
				}
				printer.printText(ToastHelper.toStr(R.string.tx_bill_reufun_total_num) + "：" + info.getCountTotalRefundCount() + "\n");

				if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
					printer.printText(ToastHelper.toStr(R.string.tx_blue_print_pay_total) + "："+ DateUtil.formatRMBMoneyUtils((info.getCountTotalFee() - info.getCountTotalRefundFee())) + ToastHelper.toStr(R.string.pay_yuan) + "\n");

				} else {
					printer.printText(ToastHelper.toStr(R.string.tx_blue_print_pay_total) + "："+ MainApplication.getFeeType() + DateUtil.formatMoneyUtils((info.getCountTotalFee() - info.getCountTotalRefundFee())) + "\n");
				}
				printer.printText("===============================\n");

				/**
				 * 是否开通小费功能，打印小费金额、总计
				 */
				if (MainApplication.isTipOpenFlag()){
					// 小费金额
					printer.printText(ToastHelper.toStr(R.string.tip_amount) + "："+ MainApplication.getFeeType() + DateUtil.formatMoneyUtils(info.getCountTotalTipFee()) + "\n");
					//小费笔数
					printer.printText(ToastHelper.toStr(R.string.tip_count) + "："+info.getCountTotalTipFeeCount()+ "\n");
					printer.printText("===============================\n");
				}


				List<OrderTotalItemInfo> list = info.getOrderTotalItemInfo();

				Map<Integer, Object> map = new HashMap<Integer, Object>();

				for (OrderTotalItemInfo itemInfo : list) {

					switch (itemInfo.getPayTypeId()) {

						case 1:
							map.put(0, itemInfo);
							break;
						case 2:
							map.put(1, itemInfo);
							break;

						case 4:
							map.put(2, itemInfo);
							break;
						case 12:
							map.put(3, itemInfo);
							break;
						default:
							map.put(itemInfo.getPayTypeId(), itemInfo);
							break;
					}

				}

				for (Integer key : map.keySet()) {
					OrderTotalItemInfo itemInfo = (OrderTotalItemInfo) map.get(key);
					if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan) )) {
						printer.printText(MainApplication.getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId())) +
								ToastHelper.toStr(R.string.tx_money)+ DateUtil.formatRMBMoneyUtils(itemInfo.getSuccessFee())+
								ToastHelper.toStr(R.string.pay_yuan) + "\n");

					} else {

						printer.printText(MainApplication.getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId()))+""+"\n");
						printer.printText(ToastHelper.toStr(R.string.tx_money)+ MainApplication.getFeeType() +
								DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + "\n");
					}
					printer.printText(ToastHelper.toStr(R.string.tv_settle_count) + "："+ itemInfo.getSuccessCount() + "\n");
				}
				printer.printText("-------------------------------\n");
				printer.printText(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n");
//				printer.printText(""+ "\n\n\n\n");
				printer.printText("\n"+ ToastHelper.toStr(R.string.tx_blue_print_sign) + "：\n\n\n\n");
				printer.printText("\r\n\n\n");


			}
		});
	}
}
