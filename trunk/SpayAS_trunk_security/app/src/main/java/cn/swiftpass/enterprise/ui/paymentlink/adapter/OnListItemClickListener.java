package cn.swiftpass.enterprise.ui.paymentlink.adapter;

public interface OnListItemClickListener {
    void onDeleteClick(int position);

    void onItemClick(int position);

    void onDeleteShow(int position, boolean isShow);
}
