package org.xclcharts.event.click;

import android.graphics.PointF;

/**
 * @InterfaceName ChartArcListener
 * @Description  用于针对arc的点击操作响应接口
 * @author XiongChuanLiang<br/>(xcl_168@aliyun.com)
 *  
 */

public interface ChartArcListener {
	public void onClick(PointF point,ArcPosition arcRecord);

}
