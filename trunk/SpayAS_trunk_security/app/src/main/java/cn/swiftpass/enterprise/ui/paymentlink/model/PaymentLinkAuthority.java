package cn.swiftpass.enterprise.ui.paymentlink.model;

import java.io.Serializable;

/**
 * Created by congwei.li on 2021/9/27.
 *
 * @Description:
 */
public class PaymentLinkAuthority implements Serializable {
    public String emailFlag;
    public String orderFlag;
    public String msgFlag;
}
