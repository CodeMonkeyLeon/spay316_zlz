package cn.swiftpass.enterprise.bussiness.enums;

/**
 *优惠券状态
 * User: Alan
 * Date: 14-2-19
 * Time: 上午11:35
 * To change this template use File | Settings | File Templates.
 */
public enum CouponStatusEnum {
    WAIT(1, "待审核"),
    NOPASS(2, "未通过"),  //已发布，还未被领取
    ISSUE(3, "正常"),  //通过 已发布，还未被领取

    /**4,5统称为无效*/
    OVERDUE(4, "已过期"), //已过使用期限
    DELETE(5, "已删除") //记录已删除
    ;

    private Integer value;
    private String displayName;

    private CouponStatusEnum(Integer value, String displayName){
        this.displayName = displayName;
        this.value = value;
    }
    public String getDisplayName() {
        return displayName;
    }

    public Integer getValue() {
        return value;
    }
}
