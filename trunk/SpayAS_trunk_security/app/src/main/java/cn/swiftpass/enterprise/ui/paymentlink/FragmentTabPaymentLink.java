package cn.swiftpass.enterprise.ui.paymentlink;

import static cn.swiftpass.enterprise.ui.paymentlink.PaymentLinkOrderFilterDialog.TYPE_ALL;
import static cn.swiftpass.enterprise.ui.paymentlink.PaymentLinkOrderFilterDialog.TYPE_CANCEL_PAY;
import static cn.swiftpass.enterprise.ui.paymentlink.PaymentLinkOrderFilterDialog.TYPE_EXCEED_TIME;
import static cn.swiftpass.enterprise.ui.paymentlink.PaymentLinkOrderFilterDialog.TYPE_PAID;
import static cn.swiftpass.enterprise.ui.paymentlink.PaymentLinkOrderFilterDialog.TYPE_PAYING;
import static cn.swiftpass.enterprise.ui.paymentlink.PaymentLinkOrderFilterDialog.TYPE_REFUNDING;
import static cn.swiftpass.enterprise.ui.paymentlink.PaymentLinkOrderFilterDialog.TYPE_UNPAID;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.CREATE_ORDER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.CREATE_ORDER_SUCCESS_TAG;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.DEFAULT_PAGE_SIZE;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.MANAGE_CREATE_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.MANAGE_CREATE_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.MANAGE_EDIT_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.MANAGE_EDIT_ORDER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.MANAGE_EDIT_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_CREATE_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_CREATE_ORDER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_CREATE_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_SEARCH_CHANGE;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.SEARCH_TYPE_MANAGE_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.SEARCH_TYPE_MANAGE_ORDER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.SEARCH_TYPE_MANAGE_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.SUCCESS_TAG;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.TAB_LIST_TYPE_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.TAB_LIST_TYPE_ORDER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.TAB_LIST_TYPE_PRODUCT;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.fragment.TabPaymentLinkContract;
import cn.swiftpass.enterprise.mvp.presenter.fragment.TabPaymentLinkPresenter;
import cn.swiftpass.enterprise.ui.fmt.BaseFragment;
import cn.swiftpass.enterprise.ui.paymentlink.adapter.BaseRecyclerAdapter;
import cn.swiftpass.enterprise.ui.paymentlink.adapter.CustomerListAdapter;
import cn.swiftpass.enterprise.ui.paymentlink.adapter.OnListItemClickListener;
import cn.swiftpass.enterprise.ui.paymentlink.adapter.OrderListAdapter;
import cn.swiftpass.enterprise.ui.paymentlink.adapter.ProductListAdapter;
import cn.swiftpass.enterprise.ui.paymentlink.interfaces.OnOrderFilterClickListener;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkCustomer;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkCustomerModel;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkOrder;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkOrderModel;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkProduct;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkProductModel;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.SharedPreUtils;

/**
 * Created by congwei.li on 2021/9/2.
 *
 * @Description: payment link界面
 */
public class FragmentTabPaymentLink extends BaseFragment<TabPaymentLinkContract.Presenter> implements TabPaymentLinkContract.View {

    private final ArrayList<PaymentLinkOrder> orders = new ArrayList<>();
    private final ArrayList<PaymentLinkCustomer> customers = new ArrayList<>();
    private final ArrayList<PaymentLinkProduct> products = new ArrayList<>();
    Activity mActivity;
    private int listType; // 0:order 默认 1:customer 2:product
    private RecyclerView rvPaymentLinkOrder;
    private RecyclerView rvPaymentLinkProduct;
    private RecyclerView rvPaymentLinkCustomer;
    private TextView tvSearch;
    private SwipeRefreshLayout swPaymentLinkList;
    private TextView tvNoData;

    private TextView mTvOrderFilter;
    private ConstraintLayout mClOrderFilter;
    private OrderListAdapter orderListAdapter;
    private CustomerListAdapter customerListAdapter;
    private ProductListAdapter productListAdapter;
    private int orderPage = 1;
    private int customerPage = 1;
    private int productPage = 1;

    private String mOrderStatus = "";

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_payment_link;
    }


    @Override
    public void deleteProductSuccess(@NonNull String response, int position) {
        products.remove(products.get(position));
        productListAdapter.notifyDataSetChanged();
    }

    @Override
    public void deleteProductFailed(@Nullable Object error) {
        if (null != getActivity()) {
            toastDialog(getActivity(), error.toString(), null);
        }
    }


    @Override
    public void deleteCustomerSuccess(@NonNull String response, int position) {
        customers.remove(customers.get(position));
        customerListAdapter.notifyDataSetChanged();
    }

    @Override
    public void deleteCustomerFailed(@Nullable Object error) {
        if (null != getActivity()) {
            toastDialog(getActivity(), error.toString(), null);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    private void setOrderFilterText() {
        switch (mOrderStatus) {
            case TYPE_ALL:
                mTvOrderFilter.setText(getString(R.string.string_payment_link_order_filter));
                break;
            case TYPE_UNPAID:
                mTvOrderFilter.setText(getString(R.string.string_payment_link_order_filter_unpaid));
                break;
            case TYPE_EXCEED_TIME:
                mTvOrderFilter.setText(getString(R.string.string_payment_link_order_filter_exceed));
                break;
            case TYPE_PAYING:
                mTvOrderFilter.setText(getString(R.string.string_payment_link_order_filter_paying));
                break;
            case TYPE_PAID:
                mTvOrderFilter.setText(getString(R.string.string_payment_link_order_filter_paid));
                break;
            case TYPE_REFUNDING:
                mTvOrderFilter.setText(getString(R.string.string_payment_link_order_filter_refunding));
                break;
            case TYPE_CANCEL_PAY:
                mTvOrderFilter.setText(getString(R.string.string_payment_link_order_filter_cancel_pay));
                break;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        rvPaymentLinkOrder = view.findViewById(R.id.rv_payment_link_order);
        rvPaymentLinkProduct = view.findViewById(R.id.rv_payment_link_product);
        rvPaymentLinkCustomer = view.findViewById(R.id.rv_payment_link_customer);
        LinearLayout llSearch = view.findViewById(R.id.ll_search_payment_link);
        tvSearch = view.findViewById(R.id.tv_search_payment_link);
        swPaymentLinkList = view.findViewById(R.id.sw_payment_link_list);
        tvNoData = view.findViewById(R.id.tv_no_data);
        mClOrderFilter = view.findViewById(R.id.id_cl_order_filter);
        mTvOrderFilter = view.findViewById(R.id.id_tv_order_filter);

        setListType(TAB_LIST_TYPE_ORDER);

        mClOrderFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaymentLinkOrderFilterDialog orderFilterDialog = PaymentLinkOrderFilterDialog.Companion.getInstance();
                orderFilterDialog.setOnClickListener(new OnOrderFilterClickListener() {
                    @Override
                    public void onClick(String type) {
                        Log.i("TAG_ZLZ", "点击类型order: " + type);
                        mOrderStatus = type;
                        orderPage = 1;
                        setOrderFilterText();
                        requestOrderList(String.valueOf(orderPage));
                    }
                });
                orderFilterDialog.show(getChildFragmentManager(), "OrderFilterDialog");
            }
        });


        llSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (listType) {
                    case TAB_LIST_TYPE_ORDER:
                        SearchActivity.startActivityForResult(mActivity, SEARCH_TYPE_MANAGE_ORDER, getStringById(R.string.payment_link_search_order_edit_hint));
                        break;
                    case TAB_LIST_TYPE_CUSTOMER:
                        SearchActivity.startActivityForResult(mActivity, SEARCH_TYPE_MANAGE_CUSTOMER, getStringById(R.string.payment_link_manage_search_customer));
                        break;
                    case TAB_LIST_TYPE_PRODUCT:
                        SearchActivity.startActivityForResult(mActivity, SEARCH_TYPE_MANAGE_PRODUCT, getStringById(R.string.payment_link_manage_search_product));
                        break;
                }
            }
        });

        rvPaymentLinkOrder.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvPaymentLinkProduct.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvPaymentLinkCustomer.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        orderListAdapter = new OrderListAdapter(getContext(), orders);
        orderListAdapter.setOnLoadMoreListener(new BaseRecyclerAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                requestOrderList(String.valueOf(++orderPage));
            }
        }, rvPaymentLinkOrder);
        rvPaymentLinkOrder.setAdapter(orderListAdapter);
        orderListAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                OrderDetailsActivity.startActivity(mContext, orders.get(position), MANAGE_EDIT_ORDER);
            }
        });

        productListAdapter = new ProductListAdapter(getContext(), products);
        productListAdapter.setOnLoadMoreListener(new BaseRecyclerAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                requestProductList(String.valueOf(++productPage));
            }
        }, rvPaymentLinkProduct);
        rvPaymentLinkProduct.setAdapter(productListAdapter);
        productListAdapter.setItemClickListener(new OnListItemClickListener() {
            @Override
            public void onDeleteClick(int position) {
                showDialog(new DialogClick() {
                    @Override
                    public void onOkClick() {
                        if (mPresenter != null) {
                            mPresenter.deleteProduct(products.get(position), position);
                        }
                    }

                    @Override
                    public void onCancelClick() {

                    }
                });
            }

            @Override
            public void onItemClick(int position) {
                CustAndProdEditActivity.startActivityForResult(getActivity(), MANAGE_EDIT_PRODUCT, products.get(position));
            }

            @Override
            public void onDeleteShow(int position, boolean isShow) {
            }
        });

        customerListAdapter = new CustomerListAdapter(getContext(), customers);
        customerListAdapter.setOnLoadMoreListener(new BaseRecyclerAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                requestCustomerList(String.valueOf(++customerPage));
            }
        }, rvPaymentLinkCustomer);
        rvPaymentLinkCustomer.setAdapter(customerListAdapter);
        customerListAdapter.setItemClickListener(new OnListItemClickListener() {
            @Override
            public void onDeleteClick(int position) {
                showDialog(new DialogClick() {
                    @Override
                    public void onOkClick() {
                        if (mPresenter != null) {
                            mPresenter.deleteCustomer(customers.get(position), position);
                        }
                    }

                    @Override
                    public void onCancelClick() {

                    }
                });
            }

            @Override
            public void onItemClick(int position) {
                CustAndProdEditActivity.startActivityForResult(getActivity(), MANAGE_EDIT_CUSTOMER, customers.get(position));
            }

            @Override
            public void onDeleteShow(int position, boolean isShow) {
            }
        });

        swPaymentLinkList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                switch (listType) {
                    case TAB_LIST_TYPE_ORDER:
                        orderPage = 1;
                        requestOrderList(String.valueOf(orderPage));
                        break;
                    case TAB_LIST_TYPE_CUSTOMER:
                        customerPage = 1;
                        requestCustomerList(String.valueOf(customerPage));
                        break;
                    case TAB_LIST_TYPE_PRODUCT:
                        productPage = 1;
                        requestProductList(String.valueOf(productPage));
                        break;
                }

            }
        });
        return view;
    }

    public int getListType() {
        return listType;
    }

    public void setListType(int listType) {
        this.listType = listType;
        switch (this.listType) {
            case TAB_LIST_TYPE_ORDER:
                orderPage = 1;
                requestOrderList(String.valueOf(orderPage));
                tvSearch.setText(getStringById(R.string.payment_link_search_order_edit_hint));
                mClOrderFilter.setVisibility(View.VISIBLE);
                break;
            case TAB_LIST_TYPE_CUSTOMER:
                customerPage = 1;
                requestCustomerList(String.valueOf(customerPage));
                tvSearch.setText(getStringById(R.string.payment_link_manage_search_customer));
                mClOrderFilter.setVisibility(View.GONE);
                break;
            case TAB_LIST_TYPE_PRODUCT:
                productPage = 1;
                requestProductList(String.valueOf(productPage));
                tvSearch.setText(getStringById(R.string.payment_link_manage_search_product));
                mClOrderFilter.setVisibility(View.GONE);
                break;
        }
    }


    @Override
    public void getOrderListSuccess(@NonNull PaymentLinkOrderModel response) {
        showListData(response);
    }

    @Override
    public void getOrderListFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }
        showErrorMsgDialog(error);
    }

    private void requestOrderList(String pageNum) {
        if (mPresenter != null) {
            swPaymentLinkList.setRefreshing(false);
            mPresenter.getOrderList("", "", "", "", pageNum, mOrderStatus);
        }
    }

    public void requestOrderListSuccess(final ArrayList<PaymentLinkOrder> data) {
        if (isDataEmpty(orderPage, data)) {
            return;
        }
        if (orderPage != 1) {
            Iterator<PaymentLinkOrder> it = data.iterator();
            while (it.hasNext()) {
                PaymentLinkOrder order = it.next();
                for (int i = 0; i < orders.size(); i++) {
                    if (order.orderNo.equals(orders.get(i).orderNo)) {
                        it.remove();
                    }
                }
            }
        }
        if (orderPage == 1) {
            orders.clear();
            orderListAdapter.setDataList(orders);
        }
        orders.addAll(data);
        if (data.size() == 0 || data.size() < DEFAULT_PAGE_SIZE) {
            orderListAdapter.loadMoreEnd(false);
        } else {
            orderListAdapter.loadMoreComplete();
        }
        orderListAdapter.notifyDataSetChanged();
    }


    @Override
    public void getCustomerListSuccess(@NonNull PaymentLinkCustomerModel response) {
        showListData(response);
    }

    @Override
    public void getCustomerListFailed(@Nullable Object error) {
        showErrorMsgDialog(error);
    }

    private void requestCustomerList(String pageNum) {
        if (mPresenter != null) {
            swPaymentLinkList.setRefreshing(false);
            mPresenter.getCustomerList(pageNum, "", "", "");
        }
    }

    private void showErrorMsgDialog(Object object) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (null != getActivity()) {
                    toastDialog(getActivity(), object.toString(), null);
                }
                switch (listType) {
                    case TAB_LIST_TYPE_ORDER:
                        requestOrderListSuccess(new ArrayList<>());
                        break;
                    case TAB_LIST_TYPE_CUSTOMER:
                        requestCustomerListSuccess(new ArrayList<>());
                        break;
                    case TAB_LIST_TYPE_PRODUCT:
                        requestProductListSuccess(new ArrayList<>());
                        break;
                }
            }
        });
    }

    private void showListData(Serializable serializable) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (serializable instanceof PaymentLinkOrderModel) {
                    requestOrderListSuccess(((PaymentLinkOrderModel) serializable).data);
                }
                if (serializable instanceof PaymentLinkCustomerModel) {
                    requestCustomerListSuccess(((PaymentLinkCustomerModel) serializable).data);
                }
                if (serializable instanceof PaymentLinkProductModel) {
                    requestProductListSuccess(((PaymentLinkProductModel) serializable).data);
                }
            }
        });
    }

    public void requestCustomerListSuccess(ArrayList<PaymentLinkCustomer> data) {
        if (isDataEmpty(customerPage, data)) {
            return;
        }
        if (customerPage != 1) {
            Iterator<PaymentLinkCustomer> it = data.iterator();
            while (it.hasNext()) {
                PaymentLinkCustomer customer = it.next();
                for (int i = 0; i < customers.size(); i++) {
                    if (customer.custId.equals(customers.get(i).custId)) {
                        it.remove();
                    }
                }
            }
        }
        if (customerPage == 1) {
            customers.clear();
            customerListAdapter.setDataList(customers);
        }
        customers.addAll(data);
        if (data.size() == 0 || data.size() < DEFAULT_PAGE_SIZE) {
            customerListAdapter.loadMoreEnd(false);
        } else {
            customerListAdapter.loadMoreComplete();
        }
        customerListAdapter.notifyDataSetChanged();
    }


    @Override
    public void getProductListSuccess(@NonNull PaymentLinkProductModel response) {
        showListData(response);
    }

    @Override
    public void getProductListFailed(@Nullable Object error) {
        showErrorMsgDialog(error);
    }

    private void requestProductList(String pageNum) {
        if (mPresenter != null) {
            swPaymentLinkList.setRefreshing(false);
            mPresenter.getProductList(pageNum, "", "", "");
        }
    }

    public void requestProductListSuccess(ArrayList<PaymentLinkProduct> data) {
        if (isDataEmpty(productPage, data)) {
            return;
        }
        if (productPage != 1) {
            Iterator<PaymentLinkProduct> it = data.iterator();
            while (it.hasNext()) {
                PaymentLinkProduct product = it.next();
                for (int i = 0; i < products.size(); i++) {
                    if (product.goodsId.equals(products.get(i).goodsId)) {
                        it.remove();
                    }
                }
            }
        }
        if (productPage == 1) {
            products.clear();
            productListAdapter.setDataList(products);
        }
        products.addAll(data);
        if (data.size() == 0 || data.size() < DEFAULT_PAGE_SIZE) {
            productListAdapter.loadMoreEnd(false);
        } else {
            productListAdapter.loadMoreComplete();
        }
        productListAdapter.notifyDataSetChanged();
    }

    public void showDialog(DialogClick dialogClick) {
        if (getActivity() != null) {
            dialog = new DialogInfo(getContext(), null, getString(R.string.payment_link_delete_confirm), getString(R.string.bt_confirm), getString(R.string.payment_link_add_order_cancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {
                @Override
                public void handleOkBtn() {
                    if (dialogClick != null) {
                        dialogClick.onOkClick();
                    }
                    dialog.cancel();
                    dialog.dismiss();
                }

                @Override
                public void handleCancelBtn() {
                    if (dialogClick != null) {
                        dialogClick.onCancelClick();
                    }
                    dialog.cancel();
                }
            }, null);
            DialogHelper.resize(getActivity(), dialog);
            dialog.show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CODE_CREATE_CUSTOMER) {
            if (requestCode == MANAGE_CREATE_CUSTOMER) {
                customerPage = 1;
                requestCustomerList(String.valueOf(customerPage));
            } else if (requestCode == MANAGE_EDIT_CUSTOMER && null != data) {
                PaymentLinkCustomer customerSelect = (PaymentLinkCustomer) data.getSerializableExtra("result");
                if (null == customerSelect) {
                    return;
                }
                for (int i = 0; i < customers.size(); i++) {
                    if (customers.get(i).custId.equals(customerSelect.custId)) {
                        customers.get(i).custEmail = customerSelect.custEmail;
                        customers.get(i).custMobile = customerSelect.custMobile;
                        customers.get(i).custName = customerSelect.custName;
                        customerListAdapter.notifyItemChanged(i);
                        break;
                    }
                }
            }
        }

        if (resultCode == RESULT_CODE_CREATE_PRODUCT) {
            if (requestCode == MANAGE_CREATE_PRODUCT) {
                productPage = 1;
                requestProductList(String.valueOf(productPage));
            } else if (requestCode == MANAGE_EDIT_PRODUCT) {
                PaymentLinkProduct product = (PaymentLinkProduct) data.getSerializableExtra("result");
                if (null == product) {
                    return;
                }
                for (int i = 0; i < products.size(); i++) {
                    if (products.get(i).goodsId.equals(product.goodsId)) {
                        products.get(i).goodsName = product.goodsName;
                        products.get(i).goodsCode = product.goodsCode;
                        products.get(i).goodsPrice = product.goodsPrice;
                        products.get(i).goodsDesc = product.goodsDesc;
                        products.get(i).goodsPic = product.goodsPic;
                        productListAdapter.notifyItemChanged(i);
                        break;
                    }
                }
            }
        }

        if (requestCode == SEARCH_TYPE_MANAGE_ORDER || requestCode == SEARCH_TYPE_MANAGE_CUSTOMER || requestCode == SEARCH_TYPE_MANAGE_PRODUCT) {
            if (resultCode == RESULT_CODE_SEARCH_CHANGE) {
                setListType(this.listType);
            }
        }

        if (requestCode == CREATE_ORDER) {
            if (resultCode == RESULT_CODE_CREATE_ORDER) {
                orderPage = 1;
                requestOrderList(String.valueOf(orderPage));
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SUCCESS_TAG.equals((String) SharedPreUtils.readProduct(CREATE_ORDER_SUCCESS_TAG)) && listType == TAB_LIST_TYPE_ORDER) {
            SharedPreUtils.saveObject("0", CREATE_ORDER_SUCCESS_TAG);
            orderPage = 1;
            requestOrderList(String.valueOf(orderPage));
        }
    }

    public boolean isDataEmpty(int page, List data) {
        if ((page == 1) && (data == null || data.size() <= 0)) {
            rvPaymentLinkOrder.setVisibility(View.GONE);
            rvPaymentLinkProduct.setVisibility(View.GONE);
            rvPaymentLinkCustomer.setVisibility(View.GONE);
            tvNoData.setVisibility(View.VISIBLE);
            return true;
        } else {
            rvPaymentLinkOrder.setVisibility(listType == TAB_LIST_TYPE_ORDER ? View.VISIBLE : View.GONE);
            rvPaymentLinkProduct.setVisibility(listType == TAB_LIST_TYPE_PRODUCT ? View.VISIBLE : View.GONE);
            rvPaymentLinkCustomer.setVisibility(listType == TAB_LIST_TYPE_CUSTOMER ? View.VISIBLE : View.GONE);
            tvNoData.setVisibility(View.GONE);
            return false;
        }
    }

    @Override
    protected TabPaymentLinkContract.Presenter createPresenter() {
        return new TabPaymentLinkPresenter();
    }


    public interface DialogClick {
        void onOkClick();

        void onCancelClick();
    }
}
