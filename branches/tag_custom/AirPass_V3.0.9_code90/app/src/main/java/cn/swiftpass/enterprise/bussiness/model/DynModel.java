package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * 
 * 同步各种定制版本
 * 
 * @author  he_hui
 * @version  [版本号, 2016-9-2]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class DynModel implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    private String company; //公司名称
    
    private String serviceTel; //电话号码
    
    private String androidLogo; //固定二维码logo
    
    private String headerColor; //导航栏颜色
    
    private String headerFontColor;//导航栏颜色字体
    
    private String themeMd5;
    
    private String buttonColor; //按钮背景
    
    private String buttonFontColor; //按钮字体颜色
    
    private String questionLink; //常见问题网页地址
    
    private String eleAccountLink;// 电子钱包协议网页
    
    private String apiCode; //支付方式
    
    private String nativeTradeType;//正扫支付方式 (没有正扫则为空)
    
    private String color;//颜色值
    
    private String bigIconUrl;
    
    private String smallIconUrl;
    
    private String providerNameZhCn;//支付名称 简体
    
    private String providerName;
    
    private String fixedCodeTradeType; //固定二维码支付方式
    
    /**
     * @return 返回 fixedCodeTradeType
     */
    public String getFixedCodeTradeType()
    {
        return fixedCodeTradeType;
    }
    
    /**
     * @param 对fixedCodeTradeType进行赋值
     */
    public void setFixedCodeTradeType(String fixedCodeTradeType)
    {
        this.fixedCodeTradeType = fixedCodeTradeType;
    }
    
    /**
     * @return 返回 providerName
     */
    public String getProviderName()
    {
        return providerName;
    }
    
    /**
     * @param 对providerName进行赋值
     */
    public void setProviderName(String providerName)
    {
        this.providerName = providerName;
    }
    
    /**
     * @return 返回 isEnaled
     */
    public int getEnabled()
    {
        return isEnaled;
    }
    
    /**
     * @param 对isEnaled进行赋值
     */
    public void setEnaled(int isEnaled)
    {
        this.isEnaled = isEnaled;
    }
    
    private String providerNameZhTw;//繁体
    
    private String providerNameEnUs;
    
    private String md5; //更新标示
    
    private int isEnaled = 0; //0，默认状态 ，1，已经选中，2，置灰状态
    
    /**
     * @return 返回 md5
     */
    public String getMd5()
    {
        return md5;
    }
    
    /**
     * @param 对md5进行赋值
     */
    public void setMd5(String md5)
    {
        this.md5 = md5;
    }
    
    /**
     * @return 返回 bigIconUrl
     */
    public String getBigIconUrl()
    {
        return bigIconUrl;
    }
    
    /**
     * @param 对bigIconUrl进行赋值
     */
    public void setBigIconUrl(String bigIconUrl)
    {
        this.bigIconUrl = bigIconUrl;
    }
    
    /**
     * @return 返回 smallIconUrl
     */
    public String getSmallIconUrl()
    {
        return smallIconUrl;
    }
    
    /**
     * @param 对smallIconUrl进行赋值
     */
    public void setSmallIconUrl(String smallIconUrl)
    {
        this.smallIconUrl = smallIconUrl;
    }
    
    /**
     * @return 返回 providerNameZhCn
     */
    public String getProviderNameZhCn()
    {
        return providerNameZhCn;
    }
    
    /**
     * @param 对providerNameZhCn进行赋值
     */
    public void setProviderNameZhCn(String providerNameZhCn)
    {
        this.providerNameZhCn = providerNameZhCn;
    }
    
    /**
     * @return 返回 providerNameZhTw
     */
    public String getProviderNameZhTw()
    {
        return providerNameZhTw;
    }
    
    /**
     * @param 对providerNameZhTw进行赋值
     */
    public void setProviderNameZhTw(String providerNameZhTw)
    {
        this.providerNameZhTw = providerNameZhTw;
    }
    
    /**
     * @return 返回 providerNameEnUs
     */
    public String getProviderNameEnUs()
    {
        return providerNameEnUs;
    }
    
    /**
     * @param 对providerNameEnUs进行赋值
     */
    public void setProviderNameEnUs(String providerNameEnUs)
    {
        this.providerNameEnUs = providerNameEnUs;
    }
    
    /**
     * @return 返回 apiCode
     */
    public String getApiCode()
    {
        return apiCode;
    }
    
    /**
     * @param 对apiCode进行赋值
     */
    public void setApiCode(String apiCode)
    {
        this.apiCode = apiCode;
    }
    
    /**
     * @return 返回 nativeTradeType
     */
    public String getNativeTradeType()
    {
        return nativeTradeType;
    }
    
    /**
     * @param 对nativeTradeType进行赋值
     */
    public void setNativeTradeType(String nativeTradeType)
    {
        this.nativeTradeType = nativeTradeType;
    }
    
    /**
     * @return 返回 color
     */
    public String getColor()
    {
        return color;
    }
    
    /**
     * @param 对color进行赋值
     */
    public void setColor(String color)
    {
        this.color = color;
    }
    
    /**
     * @return 返回 questionLink
     */
    public String getQuestionLink()
    {
        return questionLink;
    }
    
    /**
     * @param 对questionLink进行赋值
     */
    public void setQuestionLink(String questionLink)
    {
        this.questionLink = questionLink;
    }
    
    /**
     * @return 返回 eleAccountLink
     */
    public String getEleAccountLink()
    {
        return eleAccountLink;
    }
    
    /**
     * @param 对eleAccountLink进行赋值
     */
    public void setEleAccountLink(String eleAccountLink)
    {
        this.eleAccountLink = eleAccountLink;
    }
    
    /**
     * @return 返回 buttonColor
     */
    public String getButtonColor()
    {
        return buttonColor.trim();
    }
    
    /**
     * @param 对buttonColor进行赋值
     */
    public void setButtonColor(String buttonColor)
    {
        this.buttonColor = buttonColor;
    }
    
    /**
     * @return 返回 buttonFontColor
     */
    public String getButtonFontColor()
    {
        return buttonFontColor.trim();
    }
    
    /**
     * @param 对buttonFontColor进行赋值
     */
    public void setButtonFontColor(String buttonFontColor)
    {
        this.buttonFontColor = buttonFontColor;
    }
    
    /**
     * @return 返回 company
     */
    public String getCompany()
    {
        return company;
    }
    
    /**
     * @param 对company进行赋值
     */
    public void setCompany(String company)
    {
        this.company = company;
    }
    
    /**
     * @return 返回 serviceTel
     */
    public String getServiceTel()
    {
        return serviceTel;
    }
    
    /**
     * @param 对serviceTel进行赋值
     */
    public void setServiceTel(String serviceTel)
    {
        this.serviceTel = serviceTel;
    }
    
    /**
     * @return 返回 androidLogo
     */
    public String getAndroidLogo()
    {
        return androidLogo;
    }
    
    /**
     * @param 对androidLogo进行赋值
     */
    public void setAndroidLogo(String androidLogo)
    {
        this.androidLogo = androidLogo;
    }
    
    /**
     * @return 返回 headerColor
     */
    public String getHeaderColor()
    {
        return headerColor;
    }
    
    /**
     * @param 对headerColor进行赋值
     */
    public void setHeaderColor(String headerColor)
    {
        this.headerColor = headerColor;
    }
    
    /**
     * @return 返回 headerFontColor
     */
    public String getHeaderFontColor()
    {
        return headerFontColor;
    }
    
    /**
     * @param 对headerFontColor进行赋值
     */
    public void setHeaderFontColor(String headerFontColor)
    {
        this.headerFontColor = headerFontColor;
    }
    
    /**
     * @return 返回 themeMd5
     */
    public String getThemeMd5()
    {
        return themeMd5;
    }
    
    /**
     * @param 对themeMd5进行赋值
     */
    public void setThemeMd5(String themeMd5)
    {
        this.themeMd5 = themeMd5;
    }
    
}
