package com.example.common.utils

import android.content.Context
import android.content.pm.PackageManager
import com.example.common.sentry.SentryUtils

object AppVersionUtils {


    fun getVersionCode(context: Context): Int {
        var verCode = -1
        try {
            verCode = context.packageManager.getPackageInfo(context.packageName, 0).versionCode
        } catch (e: PackageManager.NameNotFoundException) {
            SentryUtils.uploadTryCatchException(
                e,
                SentryUtils.getClassNameAndMethodName()
            )
        }
        return verCode
    }


    fun getVersionName(context: Context): String {
        var verName = ""
        try {
            verName = context.packageManager.getPackageInfo(context.packageName, 0).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            SentryUtils.uploadTryCatchException(
                e,
                SentryUtils.getClassNameAndMethodName()
            )
        }
        return verName
    }


    fun getUpdateVersionCode(context: Context): Int {
        val versionName = getVersionName(context)
        val versions = versionName.split("\\.".toRegex()).toTypedArray()
        var updateVersionString = ""
        for (i in versions.indices) {
            val subString = versions[i]
            if (i == 0) {
                updateVersionString += subString
                continue
            } else if (i >= 3) {
                break
            }
            val subNumber = subString.toInt()
            updateVersionString += "%02d".format(subNumber)
        }
        return updateVersionString.toInt()
    }


}