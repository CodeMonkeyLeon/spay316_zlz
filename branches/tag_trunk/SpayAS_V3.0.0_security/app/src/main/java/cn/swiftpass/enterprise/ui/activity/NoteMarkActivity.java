package cn.swiftpass.enterprise.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.tencent.stat.StatService;

import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.PreferenceUtil;

/**
 *
 * User: jamy
 * Date: 18-5-2
 */
public class NoteMarkActivity extends TemplateActivity{
    private static final String TAG = NoteMarkActivity.class.getSimpleName();
   private EditText mEt_mark;
   private String mark ="";
   private Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_mark);
        mContext = NoteMarkActivity.this;
        initView();
    }



    private void initView(){
        mEt_mark = getViewById(R.id.et_mark);
        mark = getNoteMark();
        if (!TextUtils.isEmpty(mark)){
            mEt_mark.setText(mark);
        }
        mEt_mark.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mark = s.toString();
                if (!TextUtils.isEmpty(mark) && 60 == mark.length()){
                   closeKeyboard(mEt_mark);
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        showKeyboard(mEt_mark);
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle("");
        titleBar.setLeftButtonVisible(false);
        titleBar.setRightButtonVisible(false);
        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.data_select_done));

        //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth","sale");
        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(saleOrPreauth,"pre_auth")){
            titleBar.setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
        }else{
            titleBar.setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
        }

        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
               finish();
            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButLayClick() {
                mark = mEt_mark.getText().toString();
                setNoteMark(mark);
                Log.e("JAMY","mark:  "+mark);
                String note_mark = NoteMarkActivity.getNoteMark();
                Log.e("JAMY","note_mark:  "+note_mark);
                try {
                    StatService.trackCustomEvent(mContext, "kMTASPayPaySaveNote", "保存输入的note");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
                closeKeyboard(mEt_mark);
                Intent it =new Intent();
                it.putExtra("note_mark", mark);
                setResult(RESULT_OK,it);
                finish();
            }
        });
    }

    /**
     * <功能详细描述>
     *
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static String getNoteMark() {
        return PreferenceUtil.getString("note_mark", "");
    }

    public static void setNoteMark(String note_mark) {
        PreferenceUtil.commitString("note_mark", note_mark);
    }


    /**
     * auto show InputKeyboard
     */
    private void showKeyboard(final EditText editText){
        mEt_mark.setFocusable(true);
        mEt_mark.setFocusableInTouchMode(true);
        mEt_mark.requestFocus();
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.showSoftInput(editText, 0);
            }
        }, 500);

    }


    /**
     * auto close InputKeyboard
     */
    private void closeKeyboard(EditText editText){
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }



}
