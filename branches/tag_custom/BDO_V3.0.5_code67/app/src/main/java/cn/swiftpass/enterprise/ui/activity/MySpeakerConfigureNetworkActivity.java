package cn.swiftpass.enterprise.ui.activity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.utils.AirKissEncoder;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.Logger;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static cn.swiftpass.enterprise.MainApplication.getContext;

/**
 * Created by aijingya on 2019/8/20.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(为云音箱配置网络界面)
 * @date 2019/8/20.17:18.
 */
public class MySpeakerConfigureNetworkActivity extends TemplateActivity {

    @BindView(R.id.tv_network_setting_instruction)
    TextView tvNetworkSettingInstruction;
    @BindView(R.id.tv_wifi_ssid)
    TextView tvWifiSsid;
    @BindView(R.id.et_input_wifi_password)
    EditText etInputWifiPassword;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.tv_network_setting_view_configuration)
    TextView tvNetworkSettingViewConfiguration;
    @BindView(R.id.btn_network_setting_ok)
    Button btnNetworkSettingOk;
    @BindView(R.id.iv_deletPsw)
    ImageView ivDeletPsw;

    private Subscription sendSubscribe;
    private Subscription receiveSubscribe;
    private static final int REPLY_BYTE_CONFIRM_TIMES = 5;
    private AirKissEncoder airKissEncoder;
    private boolean isConnect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speaker_configure_network);
        ButterKnife.bind(this);

        initView();
    }

    public void initView() {
        tvWifiSsid.setText(getSSID());

        etInputWifiPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().trim().length() > 0 ){
                    ivDeletPsw.setVisibility(View.VISIBLE);
                }else{
                    ivDeletPsw.setVisibility(View.GONE);
                }

                if (s.toString().trim().length() >= 8) {
                    btnNetworkSettingOk.setEnabled(true);
                } else {
                    btnNetworkSettingOk.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    /**
     * 获取当前连接WIFI的SSID
     */
    public String getSSID() {
        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        if (wm != null) {
            WifiInfo winfo = wm.getConnectionInfo();
            if (winfo != null) {
                String s = winfo.getSSID();
                if (s.length() > 2 && s.charAt(0) == '"' && s.charAt(s.length() - 1) == '"') {
                    return s.substring(1, s.length() - 1);
                }
            }
        }
        return "";
    }


    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(getStringById(R.string.terminal_speaker_network_configuration));
        titleBar.setLeftButtonVisible(true);
    }

    @OnClick({R.id.tv_network_setting_view_configuration, R.id.btn_network_setting_ok,R.id.iv_deletPsw})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_network_setting_view_configuration:
                showPage(MySpeakerConfigurationInstructionsActivity.class);
                break;
            case R.id.btn_network_setting_ok:
              if(!TextUtils.isEmpty(tvWifiSsid.getText().toString().trim()) &&
              !TextUtils.isEmpty(etInputWifiPassword.getText().toString().trim())){
                  //此时再检查一遍WiFi状态，如果此时网络再断开，则弹框提示
                  //在此界面中点击“连接”时若当前手机网络出现网络异常时，提示“手机WIFI网络异常，请检查”，点击“OK”，弹窗消失，
                  // 输入框清空，由用户自行检查手机网络，网络正常后可重新配置设备网络
                  if(!isWifiEnabled()){
                      toastDialog(MySpeakerConfigureNetworkActivity.this, getStringById(R.string.terminal_speaker_wifi_disconnect), new NewDialogInfo.HandleBtn() {
                          @Override
                          public void handleOkBtn() {
                              etInputWifiPassword.setText("");
                          }
                      });
                  }else{
                      connectCloud(tvWifiSsid.getText().toString().trim(),etInputWifiPassword.getText().toString().trim());
                  }
              }
              break;
            case R.id.iv_deletPsw:
                etInputWifiPassword.setText("");
                break;
        }
    }


    public static boolean isWifiEnabled() {
        Context myContext = getContext();
        if (myContext == null) {
            throw new NullPointerException("Global context is null");
        }
        WifiManager wifiMgr = (WifiManager) myContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wifiMgr.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
            ConnectivityManager connManager = (ConnectivityManager) myContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo wifiInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            return wifiInfo.isConnected();
        } else {
            return false;
        }
    }

    void connectCloud(final String ssid,final String password){
        //发送AirKiss
        sendSubscribe = Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                byte DUMMY_DATA[] = new byte[1500];
                airKissEncoder = new AirKissEncoder(ssid, password);
                DatagramSocket sendSocket = null;
                try {
                    sendSocket = new DatagramSocket();
                    sendSocket.setBroadcast(true);
                    int encoded_data[] = airKissEncoder.getEncodedData();
                    for (int i = 0; i < encoded_data.length; ++i) {
                        DatagramPacket pkg = new DatagramPacket(DUMMY_DATA,
                                encoded_data[i],
                                InetAddress.getByName("255.255.255.255"),
                                10000);
                        sendSocket.send(pkg);
                        Thread.sleep(4);
                    }
                    isConnect = false;
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                    e.printStackTrace();
                } finally {
                    sendSocket.close();
                    sendSocket.disconnect();
                }
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<String>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        dismissLoading();
                        toastDialog(MySpeakerConfigureNetworkActivity.this, getStringById(R.string.terminal_network_configure_request_failed), new NewDialogInfo.HandleBtn() {
                            @Override
                            public void handleOkBtn() {
                                etInputWifiPassword.setText("");
                            }
                        });
                    }

                    @Override
                    public void onNext(String string) {
                    }
                });

        //接收udp包
        receiveSubscribe = Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                byte[] buffer = new byte[15000];
                DatagramSocket udpServerSocket = null;
                try {
                    int replyByteCounter = 0;
                    udpServerSocket = new DatagramSocket(10000);
                    udpServerSocket.setSoTimeout(1000 * 60);
                    DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                    while (true) {
                        udpServerSocket.receive(packet);
                        byte receivedData[] = packet.getData();
                        for (byte b : receivedData) {
                            if (b == airKissEncoder.getRandomChar()){
                                replyByteCounter= replyByteCounter+1;
                            }
                        }

                        if (replyByteCounter > REPLY_BYTE_CONFIRM_TIMES) {
                            subscriber.onCompleted();
                            isConnect =true;
                            break;
                        }
                    }
                } catch (SocketException e) {
                    subscriber.onError(e);
                    e.printStackTrace();
                } catch (IOException e) {
                    subscriber.onError(e);
                    e.printStackTrace();
                } finally {
                    udpServerSocket.close();
                    udpServerSocket.disconnect();
                }
            }
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onStart() {
                        super.onStart();
                        loadDialog(MySpeakerConfigureNetworkActivity.this,getStringById(R.string.terminal_network_configure_connecting));
                    }

                    @Override
                    public void onCompleted() {
                        dismissLoading();
                        if(isConnect){
                            toastDialog(MySpeakerConfigureNetworkActivity.this,getStringById(R.string.terminal_network_configure_success),new NewDialogInfo.HandleBtn(){
                                @Override
                                public void handleOkBtn() {
                                    //网络连接成功，结束当前页面，跳转到列表页面
                                    finish();
                                    showPage(MySpeakerListActivity.class);
                                }
                            });
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        dismissLoading();
                        toastDialog(MySpeakerConfigureNetworkActivity.this, getStringById(R.string.terminal_network_configure_failed),
                                getStringById(R.string.terminal_speaker_reconfigure), new NewDialogInfo.HandleBtn() {
                            @Override
                            public void handleOkBtn() {
                                etInputWifiPassword.setText("");
                            }
                        });
                    }

                    @Override
                    public void onNext(String s) {
                    }
                });
    }

    @Override
    protected void onDestroy() {
        if (sendSubscribe != null && sendSubscribe.isUnsubscribed()) {
            sendSubscribe.unsubscribe();
        }
        if (receiveSubscribe != null && receiveSubscribe.isUnsubscribed()) {
            receiveSubscribe.unsubscribe();
        }
        super.onDestroy();
    }
}
