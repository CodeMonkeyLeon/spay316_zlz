package cn.swiftpass.enterprise.partyking.adapter

import android.content.Context
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.partyking.entity.VoucherInfoItemEntity
import cn.swiftpass.enterprise.partyking.interfaces.OnPktBottomLayoutListener
import cn.swiftpass.enterprise.utils.DisplayUtil
import cn.swiftpass.enterprise.utils.Logger
import cn.swiftpass.enterprise.utils.OnProhibitFastClickListener
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder

/**
 * @author lizheng.zhao
 * @date 2022/05/25
 */
class PartyKingDetailsAdapter(val mContent: Context, val mList: List<VoucherInfoItemEntity>) :
    BaseMultiItemQuickAdapter<VoucherInfoItemEntity, BaseViewHolder>(mList) {

    companion object {
        const val TAG = "PartyKingDetailsAdapter"
    }

    private var mOnPktBottomLayoutListener: OnPktBottomLayoutListener? = null

    fun setOnPktButtonLayoutListener(listener: OnPktBottomLayoutListener) {
        mOnPktBottomLayoutListener = listener
    }

    init {
        //添加顶部样式
        addItemType(VoucherInfoItemEntity.ITEM_TYPE_TOP, R.layout.item_pk_top)
        //添加横向内容样式
        addItemType(VoucherInfoItemEntity.ITEM_TYPE_MID_HORIZONTAL, R.layout.item_pk_mid_horizontal)
        //添加纵向内容样式
        addItemType(VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL, R.layout.item_pk_mid_vertical)
        //添加底部样式
        addItemType(VoucherInfoItemEntity.ITEM_TYPE_BOTTOM, R.layout.item_pk_bottom)
    }


    override fun convert(helper: BaseViewHolder?, item: VoucherInfoItemEntity?) {
        helper?.let { h ->
            Logger.d(
                TAG,
                "--- itemViewType : ${h.itemViewType}    position : ${h.position}    adapterPosition: ${h.adapterPosition}"
            )
            when (h.itemViewType) {
                VoucherInfoItemEntity.ITEM_TYPE_TOP -> {
                    //顶部样式
                    setTopLayout(h.itemView, item)
                }
                VoucherInfoItemEntity.ITEM_TYPE_MID_HORIZONTAL,
                VoucherInfoItemEntity.ITEM_TYPE_MID_VERTICAL -> {
                    //内容样式
                    setMidLayout(h.itemViewType, h, item)
                }
                VoucherInfoItemEntity.ITEM_TYPE_BOTTOM -> {
                    //底部样式
                    setBottomLayout(h.itemView, item)
                }
            }
        }
    }


    /**
     * 设置顶部布局
     */
    private fun setTopLayout(layout: View, item: VoucherInfoItemEntity?) {
        item?.let { data ->
            val iconImageView = layout.findViewById<ImageView>(R.id.id_img)
            val titleTextView = layout.findViewById<TextView>(R.id.id_tv_title)
            //图标
            when (data.topIconType) {
                VoucherInfoItemEntity.TOP_ICON_SUCCESS -> iconImageView.setImageResource(R.drawable.icon_prompt_successful_ptk)
                VoucherInfoItemEntity.TOP_ICON_TIMEOUT -> iconImageView.setImageResource(R.drawable.icon_prompt_timeout_ptk)
                VoucherInfoItemEntity.TOP_ICON_FAIL -> iconImageView.setImageResource(R.drawable.icon_prompt_failed_ptk)
                VoucherInfoItemEntity.TOP_ICON_ROLLBACK -> iconImageView.setImageResource(R.drawable.icon_prompt_rollback_ptk)
            }
            //文字
            titleTextView.text = data.topTitle
        }
    }

    /**
     * 设置内容布局
     */
    private fun setMidLayout(
        layoutType: Int,
        helper: BaseViewHolder,
        item: VoucherInfoItemEntity?
    ) {
        item?.let { data ->
            helper.setText(R.id.id_tv_title, data.midTitle)
            helper.setText(R.id.id_tv_content, data.midContent)

            if (layoutType == VoucherInfoItemEntity.ITEM_TYPE_MID_HORIZONTAL) {
                //横向布局, 需要设置内容字体颜色
                val tvContent = helper.itemView.findViewById<TextView>(R.id.id_tv_content)
                when (data.midContentHorizontalColor) {
                    VoucherInfoItemEntity.COLOR_MID_CONTENT_SUCCESS -> tvContent.setTextColor(
                        mContent.resources.getColor(
                            R.color.pk_item_horizontal_content_success
                        )
                    )
                    VoucherInfoItemEntity.COLOR_MID_CONTENT_FAIL -> tvContent.setTextColor(
                        mContent.resources.getColor(
                            R.color.pk_item_horizontal_content_fail
                        )
                    )
                    VoucherInfoItemEntity.COLOR_MID_CONTENT_TIMEOUT -> tvContent.setTextColor(
                        mContent.resources.getColor(
                            R.color.pk_item_horizontal_content_timeout
                        )
                    )
                }
            }
        }
    }


    /**
     * 设置底部布局
     */
    private fun setBottomLayout(layout: View, item: VoucherInfoItemEntity?) {
        item?.let { data ->
            val buttonLinearLayout = layout.findViewById<LinearLayout>(R.id.id_ll_pk_item_bottom)
            val guideTextView = layout.findViewById<TextView>(R.id.id_tv_title)
            val blueButton = layout.findViewById<Button>(R.id.id_btn_type_blue)
            val whiteButton = layout.findViewById<Button>(R.id.id_btn_type_white)

            //设置布局
            guideTextView.text = data.bottomGuideText
            when (data.bottomType) {
                VoucherInfoItemEntity.BOTTOM_NO_BUTTON -> {
                    //底部没有按钮
                    blueButton.visibility = View.GONE
                    whiteButton.visibility = View.GONE
//                    setLayoutMargin(buttonLinearLayout, 143, 74)
                }
                VoucherInfoItemEntity.BOTTOM_BLUE_BUTTON_AND_GUIDE -> {
                    //蓝色按钮 + tips
                    whiteButton.visibility = View.GONE
                    blueButton.visibility = View.VISIBLE
                    blueButton.text = data.bottomButtonText
//                    setLayoutMargin(buttonLinearLayout, 143, 74)
                }
                VoucherInfoItemEntity.BOTTOM_WHITE_BUTTON_AND_GUIDE -> {
                    //白色按钮 + tips
                    blueButton.visibility = View.GONE
                    whiteButton.visibility = View.VISIBLE
                    whiteButton.text = data.bottomButtonText
//                    setLayoutMargin(buttonLinearLayout, 143, 74)
                }
                VoucherInfoItemEntity.BOTTOM_BLUE_BUTTON -> {
                    //只有蓝色按钮
                    guideTextView.visibility = View.GONE
                    whiteButton.visibility = View.GONE
                    blueButton.visibility = View.VISIBLE
                    blueButton.text = data.bottomButtonText
//                    setLayoutMargin(buttonLinearLayout, 200, 74)
                }
                VoucherInfoItemEntity.BOTTOM_WHITE_BUTTON -> {
                    //只有白色按钮
                    guideTextView.visibility = View.GONE
                    blueButton.visibility = View.GONE
                    whiteButton.visibility = View.VISIBLE
                    whiteButton.text = data.bottomButtonText
//                    setLayoutMargin(buttonLinearLayout, 200, 74)
                }

            }


            //设置布局margin
            when (data.bottomPageType) {
                VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_CONFIRM -> {
                    //核销确认
                    setLayoutMargin(buttonLinearLayout, 205, 74)
                }
                VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_CAN_NOT_WRITE_OFF -> {
                    //不可核销
                    setLayoutMargin(buttonLinearLayout, 193, 86)
                }
                VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_WRITE_OFF_SUCCESS -> {
                    //核销成功
                    setLayoutMargin(buttonLinearLayout, 143, 74)
                }
                VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_WRITE_OFF_FAIL -> {
                    //核销失败
                    setLayoutMargin(buttonLinearLayout, 77, 74)
                }
                VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_WRITE_OFF_TIMEOUT -> {
                    //核销超时
                    setLayoutMargin(buttonLinearLayout, 210, 74)

                }
                VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_ROLLBACK_SUCCESS -> {
                    //撤回成功
                    setLayoutMargin(buttonLinearLayout, 143, 74)

                }
                VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_RECORD_SUCCESS,
                VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_RECORD_FAIL -> {
                    //核销记录成功
                    //核销记录失败
                }
                VoucherInfoItemEntity.BOTTOM_PAGE_TYPE_WRITE_OFF_RECORD_TIMEOUT -> {
                    //核销记录超时
                    setLayoutMargin(buttonLinearLayout, 405, 74)
                }
            }


//            //用于监听底部布局的位置尺寸
//            buttonLinearLayout.viewTreeObserver.addOnGlobalLayoutListener(object :
//                ViewTreeObserver.OnGlobalLayoutListener {
//                override fun onGlobalLayout() {
//                    mOnPktBottomLayoutListener?.let { listener ->
//                        listener.onPktBottomLayoutSize(buttonLinearLayout)
//                    }
//                    buttonLinearLayout.viewTreeObserver.removeOnGlobalLayoutListener(this)
//                }
//            })

            //点击引导文字
            guideTextView.setOnClickListener(object : OnProhibitFastClickListener() {
                override fun onFilterClick(v: View?) {
                    mOnPktBottomLayoutListener?.let { listener ->
                        listener.onPktBottomGuideTextClick(data)
                    }
                }
            })

            //点击 蓝色按钮
            blueButton.setOnClickListener(object : OnProhibitFastClickListener() {
                override fun onFilterClick(v: View?) {
                    mOnPktBottomLayoutListener?.let { listener ->
                        listener.onPktBottomBlueButtonClick(data)
                    }
                }
            })

            //点击 白色按钮
            whiteButton.setOnClickListener(object : OnProhibitFastClickListener() {
                override fun onFilterClick(v: View?) {
                    mOnPktBottomLayoutListener?.let { listener ->
                        listener.onPktBottomWhiteButtonClick(data)
                    }
                }
            })
        }
    }


    /**
     * 设置底部布局的 Margin
     */
    private fun setLayoutMargin(
        linearLayout: LinearLayout?,
        topMarginDp: Int,
        bottomMarginDp: Int
    ) {
        linearLayout?.let { layout ->
            val params = layout.layoutParams as LinearLayout.LayoutParams
            params.topMargin = DisplayUtil.dip2Px(mContent, topMarginDp.toFloat())
            params.bottomMargin = DisplayUtil.dip2Px(mContent, bottomMarginDp.toFloat())
            layout.layoutParams = params
        }

    }


}