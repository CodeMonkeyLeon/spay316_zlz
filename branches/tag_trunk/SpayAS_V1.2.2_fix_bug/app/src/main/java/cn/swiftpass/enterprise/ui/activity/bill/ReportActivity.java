/*
 * 文 件 名:  ReportActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-8
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.bill;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.xclcharts.common.DensityUtil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.bill.BillOrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalInfo;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalItemInfo;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.draw.BarChart02View;
import cn.swiftpass.enterprise.ui.activity.draw.DountChart01View;
import cn.swiftpass.enterprise.ui.activity.draw.MyMarkerView;
import cn.swiftpass.enterprise.ui.activity.print.BluePrintUtil;
import cn.swiftpass.enterprise.ui.activity.print.BluetoothSettingActivity;
import cn.swiftpass.enterprise.ui.viewpage.NewPage;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateTimeUtil;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.XAxisValueFormatter;
import com.github.mikephil.charting.formatter.YAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.tencent.stat.StatService;

/**
 * 报表
 *
 * @author he_hui
 * @version [版本号, 2016-11-8]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class ReportActivity extends TemplateActivity {
    private NewPage page;

    private View firstView, secondView;

    private View[] views = new View[2];

    private LinearLayout ly_date, ly_user;

    private TextView tv_date, tv_total, tv_total_pay, tv_order_pay_num, tv_refund_total, tv_refund_total_num;

    private LinearLayout pie1, pie2, ly_pie2, ly_pie1;

    private OrderTotalInfo info;

    private LinearLayout ly_bar1, ly_bar2, bar1, bar2, lay_line1, lay_line2;

    private TextView tv_months, tv_moeny, tv_thirty_months, tv_num;

    private LinearLayout ly_order_summary, ly_order_trend;

    private TextView tv_one_line_moeny, tv_line_one_months, tv_line_two_months, tv_two_line_moeny;

    List<String> dateList, thirtyDays;

    private LinearLayout ly_no_two, ly_no_one;

    private List<OrderTotalItemInfo> barList = new ArrayList<OrderTotalItemInfo>();

    private List<OrderTotalItemInfo> linList = new ArrayList<OrderTotalItemInfo>();

    private ImageView iv_switch;

    private TextView tv_all_user;

    private UserModel userModel;

    private TextView tv_prompt, tv_bill, tv_wx_type, tv_wx_money;

    private LinearLayout ly_wx, ly_zfb, ly_qq, ly_jd;

    private TextView tv_zfb_type, tv_zfb_money, tv_qq_type, tv_qq_money;

    private TextView tv_jd_type, tv_jd_money, tv_other_type, tv_other_money, tv_wx_type_count, tv_wx_money_count;

    private View v_wx, v_zfb, v_qq, v_jd, v_other, v_wx_count, v_zfb_count, v_qq_count, v_jd_count, v_other_count;

    private LinearLayout ly_other, ly_wx_count, ly_zfb_count, ly_qq_count, ly_jd_count, ly_other_count;

    private TextView tv_zfb_type_count, tv_zfb_money_count, tv_qq_type_count, tv_qq_money_count;

    private TextView tv_jd_type_count, tv_jd_money_count, tv_other_type_count, tv_other_money_count;

    private String startTime, endTime, startPrint, endPrint;

    private ListView lv_one_list, lv_two_list;

    private ViewHolder holder;

    private ReportAdapter reportAdapter;

    private ReportAdapter reportAdapterTwo;

    private Map<String, String> colorMap = new HashMap<String, String>();

    private boolean loadNext = false;

    private Integer reqFeqTime = 0;

    private List<OrderTotalItemInfo> orderTotalItemInfo = new ArrayList<OrderTotalItemInfo>();

    private List<OrderTotalItemInfo> orderTotalItemInfoTwo = new ArrayList<OrderTotalItemInfo>();

    private Button btn_next_step;

    class ReportAdapter extends BaseAdapter {

        private boolean isNum;

        private List<OrderTotalItemInfo> list;

        private Context context;

        private ReportAdapter(Context context, List<OrderTotalItemInfo> list, boolean isNum) {
            this.context = context;
            this.list = list;
            this.isNum = isNum;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(context, R.layout.activity_report_circle_list_item, null);
                holder = new ViewHolder();
                holder.v_line = (View) convertView.findViewById(R.id.v_line);
                holder.tv_pay_type = (TextView) convertView.findViewById(R.id.tv_pay_type);
                holder.tv_money = (TextView) convertView.findViewById(R.id.tv_money);
                holder.tv_num = (TextView) convertView.findViewById(R.id.tv_num);
                holder.iv_type = (ImageView) convertView.findViewById(R.id.iv_type);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            if ((position + 1) == list.size()) {
                holder.v_line.setVisibility(View.GONE);
            } else {
                holder.v_line.setVisibility(View.VISIBLE);
            }

            OrderTotalItemInfo orderTotalItemInfo = list.get(position);
            if (orderTotalItemInfo != null) {

                if (MainApplication.getPayTypeMap() != null && MainApplication.getPayTypeMap().size() > 0) {

                    String typeName = MainApplication.getPayTypeMap().get(String.valueOf(orderTotalItemInfo.getPayTypeId()));

                    holder.tv_pay_type.setText(typeName);
                }

                @SuppressWarnings("unchecked") Map<String, String> typePicMap = (Map<String, String>) SharedPreUtile.readProduct("payTypeIcon" + ApiConstant.bankCode + MainApplication.getMchId());
                if (typePicMap != null && typePicMap.size() > 0) {
                    String url = typePicMap.get(String.valueOf(orderTotalItemInfo.getPayTypeId()));
                    if (!StringUtil.isEmptyOrNull(url)) {
                        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.icon_general_receivables);
                        if(MainApplication.finalBitmap != null){
                            if (bitmap != null) {
                                MainApplication.finalBitmap.display(holder.iv_type, url, bitmap);
                            } else {
                                MainApplication.finalBitmap.display(holder.iv_type, url);
                            }
                        }

                    }
                }
                holder.tv_money.setText(getString(R.string.tx_money) + DateUtil.formatMoneyUtils(orderTotalItemInfo.getSuccessFee()));
                holder.tv_num.setText(getString(R.string.tx_bill_stream_pay_money_num) + "：" + orderTotalItemInfo.getSuccessCount() );
            }

            return convertView;
        }
    }

    class ViewHolder {
        private View v_line;

        private ImageView iv_type;

        TextView tv_pay_type, tv_money, tv_num;
    }

    /**
     * 暂停 后 才去请求
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void sleep(long time) {
        try {
            //            Thread.sleep(time * 1000);

            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.i("hehui", "sleep-->");
                            loadNext = false;
                        }
                    });

                }

            };
            Timer timer = new Timer();
            timer.schedule(task, time * 1000);

        } catch (Exception e) {
            return;
        }
    }

    boolean isOpen = true;

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == HandlerManager.BILL_CHOICE_USER) { //收银员
                try {
                    if (loadNext) { //
                        toastDialog(ReportActivity.this, R.string.tx_request_more, null);
                        return;
                    }

                    userModel = (UserModel) msg.obj;
                    if (userModel != null) {
                        iv_switch.setVisibility(View.VISIBLE);
                        tv_all_user.setVisibility(View.VISIBLE);
                        tv_all_user.setText(userModel.getRealname());
                        if (!StringUtil.isEmptyOrNull(startTime) && !StringUtil.isEmptyOrNull(endTime)) {

                            loadDate(startTime, endTime, 2, userModel.getId() + "", true, null);
                        } else {

                            loadDate(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00", DateUtil.formatTime(System.currentTimeMillis()), 2, userModel.getId() + "", true, null);
                        }
                    } else {
                        iv_switch.setVisibility(View.VISIBLE);
                        tv_all_user.setVisibility(View.VISIBLE);
                        tv_all_user.setText(R.string.tv_all_user);
                        if (!StringUtil.isEmptyOrNull(startTime) && !StringUtil.isEmptyOrNull(endTime)) {
                            loadDate(startTime, endTime, 2, null, true, null);
                        } else {
                            loadDate(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00", DateUtil.formatTime(System.currentTimeMillis()), 2, null, true, null);
                        }
                    }
                } catch (Exception e) {
                    iv_switch.setVisibility(View.VISIBLE);
                    tv_all_user.setVisibility(View.VISIBLE);
                    tv_all_user.setText(R.string.tv_all_user);
                    if (!StringUtil.isEmptyOrNull(startTime) && !StringUtil.isEmptyOrNull(endTime)) {
                        loadDate(startTime, endTime, 2, null, true, null);
                    } else {
                        loadDate(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00", DateUtil.formatTime(System.currentTimeMillis()), 2, null, true, null);
                    }
                }
            } else if (msg.what == HandlerManager.BILL_CHOICE_TIME) {//选择时间
                if (loadNext) { //
                    toastDialog(ReportActivity.this, R.string.tx_request_more, null);
                    return;
                }
                String time = (String) msg.obj;
                if (!StringUtil.isEmptyOrNull(time)) {
                    String t[] = time.split("\\|");

                    try {
                        tv_date.setText(DateUtil.formartDateToMMDD(t[0]) + " " + getString(R.string.tv_least) + " " + DateUtil.formartDateToMMDD(t[1]));

                        startPrint = DateUtil.formartDateToYYMMDD(t[0]);
                        endPrint = DateUtil.formartDateToYYMMDD(t[1]);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (userModel != null) {
                        startTime = t[0];
                        endTime = t[1];
                        loadDate(startTime, endTime, 2, userModel.getId() + "", true, null);
                    } else {
                        startTime = t[0];
                        endTime = t[1];
                        loadDate(startTime, endTime, 2, null, true, null);
                    }
                }
            } else if (msg.what == HandlerManager.BLUE_CONNET_STUTS) {
                btn_next_step.setEnabled(false);
                if (isOpen) {
                    isOpen = false;
                    //                    bluePrint();
                }
                btn_next_step.setEnabled(true);
            }
        }

        ;
    };

    /**
     * 动态设置ListView的高度
     *
     * @param listView
     */
    public void setListViewHeightBasedOnChildren(ListView listView) {
        if (listView == null) return;
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_main_view);
        //        page = getViewById(R.id.pager);
        //        page.setScrollble(false);
        initview();

        setLister();

        HandlerManager.registerHandler(HandlerManager.BILL_CHOICE_USER, handler);

        dateList = DateTimeUtil.getAllThirtyDays();
        thirtyDays = DateTimeUtil.getThirtyDays();

        if (reportAdapter == null) {
            reportAdapter = new ReportAdapter(ReportActivity.this, orderTotalItemInfo, false);
            lv_one_list.setAdapter(reportAdapter);
        }

        //        if (reportAdapterTwo == null)
        //        {
        //            reportAdapterTwo = new ReportAdapter(ReportActivity.this, orderTotalItemInfoTwo, true);
        //            lv_two_list.setAdapter(reportAdapterTwo);
        //        }

        //注册handler 检查蓝牙打开和关闭的操作
        HandlerManager.registerHandler(HandlerManager.BLUE_CONNET_STUTS, handler);

    }

    /**
     * 蓝牙打印
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    @SuppressLint("NewApi")
    void bluePrint() {
        if (info != null) {
            if (!StringUtil.isEmptyOrNull(startTime) && !StringUtil.isEmptyOrNull(endTime)) {
                try {
                    info.setStartTime(startPrint + " 00:00:00");
                    info.setEndTime(endPrint + " 23:59:59");
                } catch (Exception e) {
                    Log.e("hehui", "" + e);
                }
            } else {
                info.setStartTime(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00");
                info.setEndTime(DateUtil.formatTime(System.currentTimeMillis()));
            }

            if (userModel != null) {
                info.setUserName(userModel.getRealname());
            } else {
                if (MainApplication.isAdmin.equals("0")) {
                    info.setUserName(MainApplication.realName);
                }

            }
            if (!MainApplication.getBluePrintSetting()&&!MainApplication.IS_POS_VERSION) {//关闭蓝牙打印
                showDialog();
                return;
            }
            if (MainApplication.bluetoothSocket != null && MainApplication.bluetoothSocket.isConnected()) {
                BluePrintUtil.printDateSum(info);
            } else {
                String bluetoothDeviceAddress = MainApplication.getBlueDeviceAddress();
                if (!StringUtil.isEmptyOrNull(bluetoothDeviceAddress)) {
                    boolean isSucc = BluePrintUtil.blueConnent(bluetoothDeviceAddress, ReportActivity.this);
                    if (isSucc) {
                        BluePrintUtil.printDateSum(info);
                    }
                } else {
                    Log.i("hehui", "showDialog()");
                    showDialog();
                }
            }

        }
    }

    void showDialog() {
        dialog = new DialogInfo(ReportActivity.this, null, getString(R.string.tx_blue_set), getString(R.string.title_setting), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                dialog.cancel();
                dialog.dismiss();
                showPage(BluetoothSettingActivity.class);
            }

            @Override
            public void handleCancleBtn() {
                dialog.cancel();
            }
        }, null);

        DialogHelper.resize(ReportActivity.this, dialog);
        dialog.show();
    }

    private void setLister() {
        //打印日结
        btn_next_step.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                bluePrint();
            }
        });

        ly_user.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (userModel != null) {
                    UserListActivity.startActivity(ReportActivity.this, userModel);
                } else {
                    showPage(UserListActivity.class);
                }
            }
        });

        ly_date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showPage(DateChoiceActivity.class);
            }
        });

        //        page.setOnPageChangeListener(new OnPageChangeListener()
        //        {
        //            
        //            @Override
        //            public void onPageSelected(int position)
        //            {
        //                switch (position)
        //                {
        //                    case 0: //汇总
        //                        titleBar.setTitleLeftView();
        //                        break;
        //                    case 1://走势
        //                        titleBar.setTitleRightView();
        //                        try
        //                        {
        //                            loadDate(DateUtil.getSixMonthodDate() + " 00:00:00",
        //                                DateUtil.formatYYMD(DateUtil.getBeforeDate().getTime()) + " 23:59:59",
        //                                1,
        //                                null,
        //                                true,
        //                                "2");//6个月countDayKind    1最近30天，2最近6个月  (int)   只当方法类型为1时有效
        //                        }
        //                        catch (ParseException e)
        //                        {
        //                            // TODO Auto-generated catch block
        //                            e.printStackTrace();
        //                        } //走势 首先加载6个月的
        //                        break;
        //                    default:
        //                        break;
        //                }
        //            }
        //            
        //            @Override
        //            public void onPageScrolled(int arg0, float arg1, int arg2)
        //            {
        //                
        //            }
        //            
        //            @Override
        //            public void onPageScrollStateChanged(int arg0)
        //            {
        //                
        //            }
        //        });
        //        
    }

    void setValue(OrderTotalInfo info) {
        long moeny = info.getCountTotalFee() - info.getCountTotalRefundFee();
        //        if (moeny > 0)
        //        {
        tv_total.setText(DateUtil.formatMoneyUtils(moeny));
        //        }
        //        else
        //        {
        //            tv_total.setText("0.00");
        //        }
        tv_total_pay.setText(DateUtil.formatMoneyUtils(info.getCountTotalFee()));
        tv_order_pay_num.setText(info.getCountTotalCount() + "");
        tv_refund_total.setText(DateUtil.formatMoneyUtils(info.getCountTotalRefundFee()));
        tv_refund_total_num.setText(info.getCountTotalRefundCount() + "");

    }

    void setTypeVale(OrderTotalInfo info) {
        if (info != null && info.getOrderTotalItemInfo().size() > 0) {
            // colorMap
            try {
                Object colorObject = SharedPreUtile.readProduct("payTypeColor" + ApiConstant.bankCode + MainApplication.getMchId());
                colorMap.putAll((Map<String, String>) colorObject);

                if (colorMap.size() > 0) {
                    setListViewHeightBasedOnChildren(lv_one_list);
                    lv_one_list.setVisibility(View.VISIBLE);
                    ly_no_one.setVisibility(View.GONE);
                    return;
                }
            } catch (Exception e) {
                Log.e("hehui", "" + e);
            }

        } else {
            lv_one_list.setVisibility(View.GONE);
            ly_no_one.setVisibility(View.VISIBLE);
        }
        ly_wx.setVisibility(View.GONE);
        v_wx.setVisibility(View.GONE);
        ly_zfb.setVisibility(View.GONE);
        v_zfb.setVisibility(View.GONE);

        v_qq.setVisibility(View.GONE);
        ly_qq.setVisibility(View.GONE);

        v_jd.setVisibility(View.GONE);
        ly_jd.setVisibility(View.GONE);

        v_other.setVisibility(View.GONE);
        v_other.setVisibility(View.GONE);
        for (OrderTotalItemInfo itemInfo : info.getOrderTotalItemInfo()) {
            String money = null, ratio = "0";
            money = DateUtil.formatMoneyUtils(itemInfo.getSuccessFee());

            ratio = DateTimeUtil.formatMonetToStr(itemInfo.getSuccessFee() * 100, info.getCountTotalFee());

            switch (itemInfo.getPayTypeId()) {
                case 1: //微信支付
                    ly_wx.setVisibility(View.VISIBLE);
                    v_wx.setVisibility(View.VISIBLE);
                    tv_wx_type = getViewById(R.id.tv_wx_type);
                    tv_wx_money = getViewById(R.id.tv_wx_money);
                    tv_wx_type.setText(ToastHelper.toStr(R.string.title_wx_pay) + "-" + ratio + "%");
                    tv_wx_money.setText(money + getString(R.string.pay_yuan));
                    break;
                case 2: //支付宝支付
                    tv_zfb_type = getViewById(R.id.tv_zfb_type);
                    tv_zfb_money = getViewById(R.id.tv_zfb_money);
                    ly_zfb.setVisibility(View.VISIBLE);
                    v_zfb.setVisibility(View.VISIBLE);
                    tv_zfb_type.setText(ToastHelper.toStr(R.string.tx_bill_stream_choice_alipay) + "-" + ratio + "%");
                    tv_zfb_money.setText(money + getString(R.string.pay_yuan));
                    break;
                case 4: //京东钱包支付
                    tv_qq_type.setText(ToastHelper.toStr(R.string.tx_bill_stream_choice_qq) + "-" + ratio + "%");
                    tv_qq_money.setText(money + getString(R.string.pay_yuan));
                    v_qq.setVisibility(View.VISIBLE);
                    ly_qq.setVisibility(View.VISIBLE);
                    break;
                case 12: //京东钱包支付
                    v_jd.setVisibility(View.VISIBLE);
                    ly_jd.setVisibility(View.VISIBLE);
                    tv_jd_type.setText(ToastHelper.toStr(R.string.tx_bill_stream_choice_jd) + "-" + ratio + "%");
                    tv_jd_money.setText(money + getString(R.string.pay_yuan));
                    break;
                default:
                    v_other.setVisibility(View.VISIBLE);
                    v_other.setVisibility(View.VISIBLE);
                    tv_other_type.setText(ToastHelper.toStr(R.string.tv_list_type_other) + "-" + ratio + "%");
                    tv_other_money.setText(money + getString(R.string.pay_yuan));
                    break;
            }
        }
    }

    void setTypeCountVale(OrderTotalInfo info) {

        if (info != null && info.getOrderTotalItemInfo().size() > 0) {
            // colorMap
            try {
                Object colorObject = SharedPreUtile.readProduct("payTypeColor" + ApiConstant.bankCode + MainApplication.getMchId());
                colorMap.putAll((Map<String, String>) colorObject);

                if (colorMap.size() > 0) {
                    setListViewHeightBasedOnChildren(lv_two_list);

                    lv_two_list.setVisibility(View.VISIBLE);
                    ly_no_one.setVisibility(View.GONE);
                    return;
                }

                for (OrderTotalItemInfo itemInfo : info.getOrderTotalItemInfo()) {
                    String money = null, ratio = "0";
                    money = itemInfo.getSuccessCount() + "";

                    ratio = DateTimeUtil.formatMonetToStr(itemInfo.getSuccessCount() * 100, info.getCountTotalCount());
                    switch (itemInfo.getPayTypeId()) {
                        case 1: //微信支付
                            ly_wx_count.setVisibility(View.VISIBLE);
                            v_wx_count.setVisibility(View.VISIBLE);
                            tv_wx_type_count.setText(ToastHelper.toStr(R.string.title_wx_pay) + "-" + ratio + "%");
                            tv_wx_money_count.setText(money + getString(R.string.stream_cases));
                            break;
                        case 2: //支付宝支付
                            ly_zfb_count.setVisibility(View.VISIBLE);
                            v_zfb_count.setVisibility(View.VISIBLE);
                            tv_zfb_type_count.setText(ToastHelper.toStr(R.string.tx_bill_stream_choice_alipay) + "-" + ratio + "%");
                            tv_zfb_money_count.setText(money + getString(R.string.stream_cases));

                            break;
                        case 4: //qq钱包支付
                            tv_qq_type_count.setText(ToastHelper.toStr(R.string.tx_bill_stream_choice_qq) + "-" + ratio + "%");
                            tv_qq_money_count.setText(money + getString(R.string.stream_cases));
                            v_qq_count.setVisibility(View.VISIBLE);
                            ly_qq_count.setVisibility(View.VISIBLE);

                            break;
                        case 12: //京东钱包支付
                            v_jd_count.setVisibility(View.VISIBLE);
                            ly_jd_count.setVisibility(View.VISIBLE);
                            tv_jd_type_count.setText(ToastHelper.toStr(R.string.tx_bill_stream_choice_jd) + "-" + ratio + "%");
                            tv_jd_money_count.setText(money + getString(R.string.stream_cases));

                            break;
                        default:
                            v_other_count.setVisibility(View.VISIBLE);
                            ly_other_count.setVisibility(View.VISIBLE);
                            tv_other_type_count.setText(ToastHelper.toStr(R.string.tv_list_type_other) + "-" + ratio + "%");
                            tv_other_money_count.setText(money + getString(R.string.stream_cases));

                            break;
                    }
                }
            } catch (Exception e) {
                Log.e("hehui", "" + e);
            }

        } else {
            lv_two_list.setVisibility(View.GONE);
            ly_no_one.setVisibility(View.VISIBLE);
        }

        ly_wx_count.setVisibility(View.GONE);
        v_wx_count.setVisibility(View.GONE);
        ly_zfb_count.setVisibility(View.GONE);
        v_zfb_count.setVisibility(View.GONE);

        v_qq_count.setVisibility(View.GONE);
        ly_qq_count.setVisibility(View.GONE);

        v_jd_count.setVisibility(View.GONE);
        ly_jd_count.setVisibility(View.GONE);

        v_other_count.setVisibility(View.GONE);
        ly_other_count.setVisibility(View.GONE);

    }

    void createPie(final OrderTotalInfo info) {
        DountChart01View pieChart02View1 = new DountChart01View(ReportActivity.this, info, false);
        if (info.getOrderTotalItemInfo().size() > 0 && info.getCountTotalFee() > 0) {
            pie1.removeAllViews();
            pie1.addView(pieChart02View1);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) pieChart02View1.getLayoutParams();
            params.width = DensityUtil.dip2px(ReportActivity.this, 500);
            params.height = DensityUtil.dip2px(ReportActivity.this, 280);
            ly_no_one.setVisibility(View.VISIBLE);
            pie1.setVisibility(View.VISIBLE);
            tv_prompt.setVisibility(View.GONE);

            ReportActivity.this.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    setTypeVale(info);
                }
            });
        } else {
            tv_prompt.setVisibility(View.VISIBLE);
            pie1.setVisibility(View.GONE);
            ly_no_one.setVisibility(View.GONE);
        }

        DountChart01View pieChart02View2 = new DountChart01View(ReportActivity.this, info, true);
        if (info.getOrderTotalItemInfo().size() > 0 && info.getCountTotalCount() > 0) {
            pie2.removeAllViews();
            pie2.addView(pieChart02View2);
            LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) pieChart02View2.getLayoutParams();
            params1.width = DensityUtil.dip2px(ReportActivity.this, 500);
            params1.height = DensityUtil.dip2px(ReportActivity.this, 280);
            ly_no_two.setVisibility(View.VISIBLE);
            pie2.setVisibility(View.VISIBLE);
            tv_bill.setVisibility(View.GONE);
            ReportActivity.this.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    setTypeCountVale(info);
                }
            });
        } else {
            tv_bill.setVisibility(View.VISIBLE);
            ly_no_two.setVisibility(View.GONE);
            pie2.setVisibility(View.GONE);
        }
    }

    void createBarOne(List<OrderTotalItemInfo> infoList) {
        if (info != null) {
            barList.addAll(infoList);
            //tv_months = (TextView) secondView.findViewById(R.id.tv_months);
            //            tv_moeny
            //            Collections.reverse(infoList);
            BarChart02View barChart02View1 = new BarChart02View(ReportActivity.this, DateTimeUtil.getMonthSixMonths(), infoList, tv_months, tv_moeny, false);
            bar1.removeAllViews();
            bar1.addView(barChart02View1);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) barChart02View1.getLayoutParams();
            params.width = DensityUtil.dip2px(ReportActivity.this, 380);
            params.height = DensityUtil.dip2px(ReportActivity.this, 280);
            tv_months.setText(DateTimeUtil.getMonthSixMonths().get(DateTimeUtil.getMonthSixMonths().size() - 1).toString());
            tv_moeny.setText(DateUtil.formatMoneyUtils(infoList.get(infoList.size() - 1).getSuccessFee()) + getString(R.string.pay_yuan));
            ly_bar1.setVisibility(View.VISIBLE);
        } else {
            ly_bar1.setVisibility(View.GONE);
        }
    }

    void createBarTwo(List<OrderTotalItemInfo> infoList) {
        if (info != null) {
            //tv_months = (TextView) secondView.findViewById(R.id.tv_months);
            //            tv_moeny tv_thirty_months,tv_num
            //            Collections.reverse(infoList);
            BarChart02View barChart02View1 = new BarChart02View(ReportActivity.this, DateTimeUtil.getMonthSixMonths(), infoList, tv_thirty_months, tv_num, true);
            bar2.removeAllViews();
            bar2.addView(barChart02View1);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) barChart02View1.getLayoutParams();
            params.width = DensityUtil.dip2px(ReportActivity.this, 380);
            params.height = DensityUtil.dip2px(ReportActivity.this, 280);
            tv_thirty_months.setText(DateTimeUtil.getMonthSixMonths().get(DateTimeUtil.getMonthSixMonths().size() - 1).toString());
            //            tv_thirty_months, tv_num
            tv_num.setText(infoList.get(infoList.size() - 1).getSuccessCount() + getString(R.string.stream_cases));
            ly_bar2.setVisibility(View.VISIBLE);
        } else {
            ly_bar2.setVisibility(View.GONE);
        }
    }

    void createLineChartOne(List<OrderTotalItemInfo> infoList) {
        String money = DateUtil.formatMoneyUtils(infoList.get(infoList.size() - 1).getSuccessFee());
        tv_one_line_moeny.setText(money + getString(R.string.pay_yuan));
        tv_line_one_months.setText(dateList.get(dateList.size() - 1));

        lay_line1.removeAllViews();
        LineChart lineChart = new LineChart(ReportActivity.this);
        lineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListenerImp());
        setLineChart(lineChart, true, infoList);
        lay_line1.addView(lineChart);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) lineChart.getLayoutParams();
        params.width = DensityUtil.dip2px(ReportActivity.this, 350);
        params.height = DensityUtil.dip2px(ReportActivity.this, 280);
    }

    void createLineChartTwo(List<OrderTotalItemInfo> infoList) {
        linList.addAll(infoList);
        tv_two_line_moeny.setText(infoList.get(infoList.size() - 1).getSuccessCount() + getString(R.string.stream_cases));
        tv_line_two_months.setText(dateList.get(dateList.size() - 1));

        lay_line2.removeAllViews();
        LineChart lineChart = new LineChart(ReportActivity.this);
        lineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListenerTwoImp());
        setLineChart(lineChart, false, infoList);
        lay_line2.addView(lineChart);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) lineChart.getLayoutParams();
        params.width = DensityUtil.dip2px(ReportActivity.this, 350);
        params.height = DensityUtil.dip2px(ReportActivity.this, 280);
    }

    private void initview() {
        btn_next_step = getViewById(R.id.btn_next_step);

        ly_other_count = getViewById(R.id.ly_other_count);
        tv_other_type_count = getViewById(R.id.tv_other_type_count);
        tv_other_money_count = getViewById(R.id.tv_other_money_count);
        v_other_count = getViewById(R.id.v_other_count);

        ly_jd_count = getViewById(R.id.ly_jd_count);
        tv_jd_type_count = getViewById(R.id.tv_jd_type_count);
        tv_jd_money_count = getViewById(R.id.tv_jd_money_count);
        v_jd_count = getViewById(R.id.v_jd_count);

        ly_qq_count = getViewById(R.id.ly_qq_count);
        tv_qq_type_count = getViewById(R.id.tv_qq_type_count);
        tv_qq_money_count = getViewById(R.id.tv_qq_money_count);
        v_qq_count = getViewById(R.id.v_qq_count);

        ly_zfb_count = getViewById(R.id.ly_zfb_count);
        tv_zfb_type_count = getViewById(R.id.tv_zfb_type_count);
        tv_zfb_money_count = getViewById(R.id.tv_zfb_money_count);
        v_zfb_count = getViewById(R.id.v_zfb_count);

        ly_wx_count = getViewById(R.id.ly_wx_count);
        tv_wx_type_count = getViewById(R.id.tv_wx_type_count);
        tv_wx_money_count = getViewById(R.id.tv_wx_money_count);
        v_wx_count = getViewById(R.id.v_wx_count);

        ly_other = getViewById(R.id.ly_other);
        v_other = getViewById(R.id.v_other);
        tv_other_money = getViewById(R.id.tv_other_money);
        tv_other_type = getViewById(R.id.tv_other_type);

        v_jd = getViewById(R.id.v_jd);
        ly_jd = getViewById(R.id.ly_jd);
        tv_jd_type = getViewById(R.id.tv_jd_type);
        tv_jd_money = getViewById(R.id.tv_jd_money);

        ly_qq = getViewById(R.id.ly_qq);
        tv_qq_type = getViewById(R.id.tv_qq_type);
        tv_qq_money = getViewById(R.id.tv_qq_money);
        v_qq = getViewById(R.id.v_qq);

        tv_zfb_type = getViewById(R.id.tv_zfb_type);
        tv_zfb_money = getViewById(R.id.tv_zfb_money);
        v_zfb = getViewById(R.id.v_zfb);
        ly_zfb = getViewById(R.id.ly_zfb);

        //        tv_one_line_moeny,tv_line_one_months;tv_line_two_months,tv_two_line_moeny
        tv_all_user = getViewById(R.id.tv_all_user);
        iv_switch = getViewById(R.id.iv_switch);

        tv_bill = getViewById(R.id.tv_bill);
        tv_prompt = getViewById(R.id.tv_prompt);

        ly_no_one = getViewById(R.id.ly_no_one);
        ly_no_two = getViewById(R.id.ly_no_two);
        lay_line2 = getViewById(R.id.lay_line2);
        tv_line_two_months = getViewById(R.id.tv_line_two_months);
        tv_two_line_moeny = getViewById(R.id.tv_two_line_moeny);
        tv_one_line_moeny = getViewById(R.id.tv_one_line_moeny);
        tv_line_one_months = getViewById(R.id.tv_line_one_months);
        ly_order_summary = getViewById(R.id.ly_order_summary);
        ly_order_trend = getViewById(R.id.ly_order_trend);

        tv_wx_type = getViewById(R.id.tv_wx_type);
        tv_wx_money = getViewById(R.id.tv_wx_money);
        ly_wx = getViewById(R.id.ly_wx);
        v_wx = getViewById(R.id.v_wx);
        //        LayoutInflater inflater = LayoutInflater.from(this);
        //        firstView = inflater.inflate(R.layout.activity_order_summary, null);
        //        views[0] = firstView;

        pie1 = (LinearLayout) findViewById(R.id.pie1);
        pie2 = (LinearLayout) findViewById(R.id.pie2);
        tv_total = (TextView) findViewById(R.id.tv_total);
        ly_pie2 = (LinearLayout) findViewById(R.id.ly_pie2);
        ly_pie1 = (LinearLayout) findViewById(R.id.ly_pie1);

        tv_total_pay = (TextView) findViewById(R.id.tv_total_pay);
        tv_order_pay_num = (TextView) findViewById(R.id.tv_order_pay_num);
        tv_refund_total = (TextView) findViewById(R.id.tv_refund_total);
        tv_refund_total_num = (TextView) findViewById(R.id.tv_refund_total_num);

        ly_date = (LinearLayout) findViewById(R.id.ly_date);
        tv_date = (TextView) findViewById(R.id.tv_date);
        ly_user = (LinearLayout) findViewById(R.id.ly_user);
//        if (MainApplication.isAdmin.equals("0")) {//收银员
            if ( MainApplication.isOrderAuth.equals("0")) {
                iv_switch.setVisibility(View.GONE);
                tv_all_user.setText(MainApplication.realName);
                ly_user.setEnabled(false);
            }else{
               /* if (MainApplication.isAdmin.equals("0")){
                    tv_all_user.setText(MainApplication.realName);
                }*/
                tv_all_user.setText(R.string.tv_all_user);
                iv_switch.setVisibility(View.VISIBLE);
                ly_user.setEnabled(true);
            }
//        }
        tv_date.setText(DateUtil.formatMD(System.currentTimeMillis()) + "(" + getString(R.string.public_today) + ")");

        //        secondView = inflater.inflate(R.layout.activity_order_trend, null);
        //        views[1] = secondView;

        //        private LinearLayout ly_bar1,ly_bar2,bar1,bar2;
        ly_bar1 = (LinearLayout) findViewById(R.id.ly_bar1);
        ly_bar2 = (LinearLayout) findViewById(R.id.ly_bar2);
        bar1 = (LinearLayout) findViewById(R.id.bar1);
        bar2 = (LinearLayout) findViewById(R.id.bar2);
        tv_months = (TextView) findViewById(R.id.tv_months);
        tv_moeny = (TextView) findViewById(R.id.tv_moeny);
        tv_thirty_months = (TextView) findViewById(R.id.tv_thirty_months);
        tv_num = (TextView) findViewById(R.id.tv_num);
        lay_line1 = getViewById(R.id.lay_line1);

        //        page.setAdapter(new MyAdapter());
        //        page.setCurrentItem(0);

        loadDate(DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00", DateUtil.formatTime(System.currentTimeMillis()), 2, null, true, null);

        lv_one_list = getViewById(R.id.lv_one_list);
        lv_two_list = getViewById(R.id.lv_two_list);

    }

    private void setLineChart(LineChart mChart, final boolean isMoney, List<OrderTotalItemInfo> infoList) {

        mChart.setDrawGridBackground(false);
        mChart.setHighlightEnabled(false);
        mChart.setTouchEnabled(true);
        mChart.setDragEnabled(false);
        mChart.setScaleEnabled(true);
        mChart.setPinchZoom(false);
        mChart.setBackgroundColor(Color.WHITE);
        mChart.setClickable(false);
        //设置边框
        mChart.setDrawBorders(false);
        mChart.getXAxis().setPosition(XAxisPosition.BOTTOM);
        MyMarkerView mv = new MyMarkerView(this, R.layout.custom_view);
        mChart.setMarkerView(mv);
        mChart.setDescription("");
        XAxis xAxis = mChart.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setDrawGridLines(false);
        xAxis.setSpaceBetweenLabels(1);
        xAxis.setAxisLineColor(Color.parseColor("#cccccc"));
        xAxis.setAxisLineWidth(1);
        xAxis.setTextColor(Color.parseColor("#000000"));
        xAxis.setValueFormatter(new XAxisValueFormatter() {

            @Override
            public String getXValue(String original, int index, ViewPortHandler viewPortHandler) {

                switch (Integer.parseInt(original)) {
                    case 1://
                        original = thirtyDays.get(0);
                        break;
                    case 5://
                        original = thirtyDays.get(1);
                        break;
                    case 10://
                        original = thirtyDays.get(2);
                        break;
                    case 15://
                        original = thirtyDays.get(3);
                        break;
                    case 20://
                        original = thirtyDays.get(4);
                        break;
                    case 25://
                        original = thirtyDays.get(5);
                        break;
                    case 30://
                        original = thirtyDays.get(6);
                        break;
                    default:
                        original = "";
                        break;

                }
                return original;
            }
        });

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.removeAllLimitLines();

        leftAxis.setAxisMinValue(0f);
        leftAxis.setTextColor(Color.parseColor("#000000"));
        leftAxis.setLabelCount(6, true);
        leftAxis.setGridColor(Color.parseColor("#cccccc"));
        //        leftAxis.setGridLineWidth(1);
        if (isMoney) {

            leftAxis.setValueFormatter(new YAxisValueFormatter() {

                @Override
                public String getFormattedValue(float value, YAxis yAxis) {
                    //                    String marValue = DateUtil.formatMoneyUtils(value);
                    if (value == 0f) {
                        return "";
                    }
                    double farVale = value / 100;
                    return formatDouble1(farVale) + "";
                }
            });
        }
        leftAxis.setStartAtZero(false);
        leftAxis.setDrawAxisLine(true);
        leftAxis.setAxisLineColor(Color.parseColor("#cccccc"));
        leftAxis.setAxisLineWidth(1);
        mChart.getLegend().setEnabled(false);
        mChart.getAxisRight().setEnabled(false);

        setData(30, infoList, mChart, leftAxis, isMoney);

        mChart.animateX(2500, Easing.EasingOption.EaseInOutQuart);
    }

    /**
     * @param d
     * @return
     */
    public double formatDouble1(double d) {
        return (double) Math.round(d * 100) / 100;
    }

    private void setData(int count, List<OrderTotalItemInfo> infoList, LineChart mChart, YAxis leftAxis, boolean isMoney) {
        List<Long> maxVal = new ArrayList<Long>();
        ArrayList<String> xVals = new ArrayList<String>();
        for (int i = 1; i <= count; i++) {
            xVals.add((i) + "");
        }

        ArrayList<Entry> yVals = new ArrayList<Entry>();

        for (int i = 0; i < count; i++) {
            if (isMoney) {
                maxVal.add(infoList.get(i).getSuccessFee());
                yVals.add(new Entry((infoList.get(i).getSuccessFee()), i));
            } else {
                maxVal.add(infoList.get(i).getSuccessCount());
                yVals.add(new Entry((infoList.get(i).getSuccessCount()), i));
            }
        }
        long max = Collections.max(maxVal);
        if (max > 0) {
            //            if(max >0 && max < 50){
            //                leftAxis.setAxisMaxValue(50);  
            //            }else if (max >=50 && max <100) {
            //                leftAxis.setAxisMaxValue(100);  
            //            }else if (max >=100 && && max <100) {
            //                
            //            }
            if (max > 1000) {
                leftAxis.setAxisMaxValue(max + 1000);
            } else {
                leftAxis.setAxisMaxValue(max);
            }
        } else {
            leftAxis.setAxisMaxValue(0);
        }
        // create a dataset and give it a type
        LineDataSet set1 = new LineDataSet(yVals, "str");
        // set1.setFillAlpha(110);
        // set1.setFillColor(Color.RED);

        // set the line to be drawn like this "- - - - - -"
        //        set1.enableDashedLine(10f, 5f, 0f);
        //        set1.enableDashedHighlightLine(10f, 5f, 0f);
        set1.setColor(Color.parseColor("#4182E2"));
        set1.setCircleColor(Color.RED);
        set1.setLineWidth(1f);
        set1.setCircleSize(3f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(9f);
        set1.setFillAlpha(45);
        set1.setFillColor(Color.parseColor("#4182E2"));
        set1.setDrawFilled(true);
        set1.setDrawCubic(true);
        set1.setDrawCircles(false);
        set1.setHighLightColor(Color.parseColor("#ff9742"));
        set1.setHighlightLineWidth(1.5f);

        //        set1.setDrawFilled(true);
        // set1.setShader(new LinearGradient(0, 0, 0, mChart.getHeight(),
        // Color.BLACK, Color.WHITE, Shader.TileMode.MIRROR));

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(set1); // add the datasets

        // create a data object with the datasets
        LineData data = new LineData(xVals, dataSets);
        data.setDrawValues(false);
        // set data
        mChart.setData(data);
    }

    private void loadDate(String startTime, String endTime, final int countMethod, String userId, final boolean isTrade, final String countDayKind) {
        BillOrderManager.getInstance().orderCount(startTime, endTime, countMethod, userId, countDayKind, new UINotifyListener<OrderTotalInfo>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                if (isTrade) {
                    loadDialog(ReportActivity.this, R.string.public_data_loading);
                }
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(Object object) {
                super.onError(object);
                dissDialog();
                if (checkSession()) {
                    return;
                }
                if (null != object) {

                    if (!StringUtil.isEmptyOrNull(object.toString())) {

                        toastDialog(ReportActivity.this, object.toString(), new NewDialogInfo.HandleBtn() {

                            @Override
                            public void handleOkBtn() {
                                ReportActivity.this.finish();
                            }

                        });
                    } else {
                        toastDialog(ReportActivity.this, R.string.tx_load_fail, null);
                    }
                }
            }

            @Override
            public void onSucceed(OrderTotalInfo model) {
                dissDialog();
                if (null != model) {
                    //暂停
                    if (model.getReqFeqTime() > 0) {
                        loadNext = true;
                        sleep(model.getReqFeqTime());
                    }
                    if (countMethod == 2) {
                        orderTotalItemInfo.clear();
                        orderTotalItemInfo.addAll(model.getOrderTotalItemInfo());

                        orderTotalItemInfoTwo.clear();
                        orderTotalItemInfoTwo.addAll(model.getOrderTotalItemInfo());
                        if (reportAdapter != null) {
                            reportAdapter.notifyDataSetChanged();
                            //                                reportAdapterTwo.notifyDataSetChanged();
                        }
                        info = model;

                        setValue(model);
                        if (model.getOrderTotalItemInfo().size() > 0) {
                            ly_pie1.setVisibility(View.VISIBLE);
                            setListViewHeightBasedOnChildren(lv_one_list);
                            lv_one_list.setVisibility(View.VISIBLE);
                            btn_next_step.setVisibility(View.VISIBLE);
                        } else {
                            ly_pie1.setVisibility(View.GONE);
                            lv_one_list.setVisibility(View.GONE);
                        }
                        //                            createPie(model);
                    } else {
                        //走势图
                        Collections.reverse(model.getOrderTotalItemInfo());

                        if (countDayKind.equals("2")) {//走势图-柱状图
                            createBarOne(model.getOrderTotalItemInfo());
                            createBarTwo(model.getOrderTotalItemInfo());

                            List<String> dateList = DateTimeUtil.getThirtyDaysForYYYYDDMM();
                            loadDate(dateList.get(0) + " 00:00:00", DateUtil.formatYYMD(DateUtil.getBeforeDate().getTime()) + " 23:59:59", 1, null, true, "1");
                        } else {
                            if (model != null && model.getOrderTotalItemInfo().size() > 0) {

                                createLineChartOne(model.getOrderTotalItemInfo());
                                createLineChartTwo(model.getOrderTotalItemInfo());
                            } else {
                                List<String> dateList = DateTimeUtil.getThirtyDaysForYYYYDDMM();
                                loadDate(dateList.get(0) + " 00:00:00", DateUtil.formatYYMD(DateUtil.getBeforeDate().getTime()) + " 23:59:59", 1, null, true, "1");
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();

        titleBar.setTitleChoice(false);
        titleBar.setLeftButtonVisible(true);
        titleBar.setLeftButtonIsVisible(true);
        titleBar.setTitle(R.string.tx_sum);
    }

    @Override
    protected void setOnCentreTitleBarClickListener() {
        super.setOnCentreTitleBarClickListener();
        titleBar.setOnCentreTitleBarClickListener(new TitleBar.OnCentreTitleBarClickListener() {
            @Override
            public void onCentreButtonClick(View v) {

            }

            @Override
            public void onTitleRepotLeftClick() {
                //                page.setCurrentItem(0);
                //                ly_order_summary, ly_order_trend
                ly_order_summary.setVisibility(View.VISIBLE);
                ly_order_trend.setVisibility(View.GONE);

                try {
                    StatService.trackCustomEvent(ReportActivity.this, "SPConstTapReportTotalButton", "汇总");
                } catch (Exception e) {
                }

            }

            @Override
            public void onTitleRepotRigthClick() {
                try {
                    StatService.trackCustomEvent(ReportActivity.this, "SPConstTapReportTrendButton", "走势");
                } catch (Exception e) {
                }
                //                page.setCurrentItem(1);
                ly_order_summary.setVisibility(View.GONE);
                ly_order_trend.setVisibility(View.VISIBLE);
                try {
                    if (barList.size() == 0 || linList.size() == 0) {
                        //                        if (userModel != null)
                        //                        {
                        //                            loadDate(DateUtil.getSixMonthodDate() + " 00:00:00",
                        //                                DateUtil.formatYYMD(DateUtil.getBeforeDate().getTime()) + " 23:59:59",
                        //                                1,
                        //                                String.valueOf(userModel.getId()),
                        //                                true,
                        //                                "2");//6个月countDayKind    1最近30天，2最近6个月  (int)   只当方法类型为1时有效
                        //                            
                        //                        }
                        //                        else
                        //                        {

                        loadDate(DateUtil.getSixMonthodDate() + " 00:00:00", DateUtil.formatYYMD(DateUtil.getBeforeDate().getTime()) + " 23:59:59", 1, null, true, "2");//6个月countDayKind    1最近30天，2最近6个月  (int)   只当方法类型为1时有效
                        //                        }
                    } else {

                    }
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } //走势 首先加载6个月的
            }
        });
    }

    class OnChartValueSelectedListenerImp implements OnChartValueSelectedListener {
        @Override
        public void onNothingSelected() {
            // TODO Auto-generated method stub

        }

        @Override
        public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
            float vale = e.getVal();
            //        tv_one_line_moeny, tv_line_one_months
            tv_one_line_moeny.setText(DateUtil.formatMoneyUtils(vale) + getString(R.string.pay_yuan));
            tv_line_one_months.setText(dateList.get(e.getXIndex()));
        }
    }

    class OnChartValueSelectedListenerTwoImp implements OnChartValueSelectedListener {
        @Override
        public void onNothingSelected() {
            // TODO Auto-generated method stub

        }

        @Override
        public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
            float vale = e.getVal();
            //        tv_one_line_moeny, tv_line_one_months tv_line_two_months, tv_two_line_moeny
            long v = (long) vale;
            tv_two_line_moeny.setText(v + getString(R.string.stream_cases));
            tv_line_two_months.setText(dateList.get(e.getXIndex()));
        }
    }

}
