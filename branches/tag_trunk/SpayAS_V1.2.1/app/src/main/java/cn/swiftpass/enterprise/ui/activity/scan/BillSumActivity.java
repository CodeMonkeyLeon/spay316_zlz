/*
 * 文 件 名:  BillSumActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-7-13
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.scan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.bill.BillSumManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.BillItemModel;
import cn.swiftpass.enterprise.bussiness.model.BillMordel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.DateTimePickDialogInfo;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;

/**
 * 固定二维码 结算汇总
 * 
 * @author  he_hui
 * @version  [版本号, 2016-7-13]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class BillSumActivity extends TemplateActivity
{
    private ViewHolder holder;
    
    private TextView tv_startTime, tv_endTime, tv_payNetFeeTotal, tv_mchFeeTotal, tv_bill, tv_payRemitFeeTotal;
    
    private ListView bill_list;
    
    private LinearLayout lay_end, lay_start;
    
    List<BillItemModel> userModelLst;
    
    BillSumAdapter adapter;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settle_total);
        initView();
        setLister();
        userModelLst = new ArrayList<BillItemModel>();
        adapter = new BillSumAdapter(userModelLst);
        bill_list.setAdapter(adapter);
        
        load(tv_startTime.getText().toString().replaceAll("/", "-"),
            tv_endTime.getText().toString().replaceAll("/", "-"),
            true);
    }
    
    private void setLister()
    {
        lay_start.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                DateTimePickDialogInfo dateTimeDialog =
                    new DateTimePickDialogInfo(BillSumActivity.this, DateUtil.paseTime(tv_startTime.getText()
                        .toString()), null, R.string.tv_settle_choice_start_time,
                        new DateTimePickDialogInfo.HandleBtn()
                        {
                            
                            @Override
                            public void handleOkBtn(String date)
                            {
                                tv_startTime.setText(date);
                            }
                            
                        }, null, true);
                
                DialogHelper.resize(BillSumActivity.this, dateTimeDialog);
                dateTimeDialog.show();
            }
        });
        
        lay_end.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                DateTimePickDialogInfo dateTimeDialog =
                    new DateTimePickDialogInfo(BillSumActivity.this,
                        DateUtil.paseTime(tv_endTime.getText().toString()), tv_startTime.getText().toString(),
                        R.string.tv_settle_choice_end_time, new DateTimePickDialogInfo.HandleBtn()
                        {
                            
                            @Override
                            public void handleOkBtn(String date)
                            {
                                tv_endTime.setText(date);
                                load(tv_startTime.getText().toString().replaceAll("/", "-"), tv_endTime.getText()
                                    .toString()
                                    .replaceAll("/", "-"), false);
                            }
                            
                        }, null, true);
                
                DialogHelper.resize(BillSumActivity.this, dateTimeDialog);
                dateTimeDialog.show();
            }
        });
    }
    
    private void initView()
    {
        tv_bill = getViewById(R.id.tv_bill);
        
        lay_start = getViewById(R.id.lay_start);
        lay_end = getViewById(R.id.lay_end);
        tv_startTime = getViewById(R.id.tv_startTime);
        tv_endTime = getViewById(R.id.tv_endTime);
        
        tv_startTime.setText(DateUtil.formatYYYYMD(DateUtil.getBeforeDate().getTime()));
        tv_endTime.setText(DateUtil.formatYYYYMD(DateUtil.getBeforeDate().getTime()));
        
        bill_list = getViewById(R.id.bill_list);
        
        tv_payNetFeeTotal = getViewById(R.id.tv_payNetFeeTotal);
        
        tv_payRemitFeeTotal = getViewById(R.id.tv_payRemitFeeTotal);
        
        tv_mchFeeTotal = getViewById(R.id.tv_mchFeeTotal);
    }
    
    private void load(String startTime, String endTime, final boolean isFirst)
    {
        
        BillSumManager.getInstance().getCheckBillMerchant(startTime, endTime, new UINotifyListener<BillMordel>()
        {
            @Override
            public void onPreExecute()
            {
                showNewLoading(true, getString(R.string.public_data_loading));
                super.onPreExecute();
            }
            
            @Override
            public void onError(Object object)
            {
                dismissLoading();
                if (checkSession())
                {
                    return;
                }
                super.onError(object);
            }
            
            @Override
            public void onSucceed(BillMordel result)
            {
                dismissLoading();
                super.onSucceed(result);
                
                if (result != null)
                {
                    tv_payRemitFeeTotal.setText(MainApplication.getFeeFh()
                        + DateUtil.formatMoneyUtils(result.getPayRemitFeeTotal()));
                    
                    tv_payNetFeeTotal.setText(MainApplication.getFeeFh()
                        + DateUtil.formatMoneyUtils(result.getPayNetFeeTotal()));
                    
                    tv_mchFeeTotal.setText(MainApplication.getFeeFh()
                        + DateUtil.formatMoneyUtils(result.getMchFeeTotal()));
                    if (null != result.getList() && result.getList().size() > 0)
                    {
                        bill_list.setVisibility(View.VISIBLE);
                        tv_bill.setVisibility(View.GONE);
                        
                        if (userModelLst.size() > 0)
                        {
                            userModelLst.clear();
                        }
                        userModelLst.addAll(result.getList());
                        
                        isChecked = new HashMap<Integer, Boolean>();
                        for (int i = 0; i < userModelLst.size(); i++)
                        {
                            isChecked.put(i, false);
                        }
                        
                        if (adapter == null)
                        {
                            adapter = new BillSumAdapter(result.getList());
                            bill_list.setAdapter(adapter);
                        }
                        else
                        {
                            adapter.notifyDataSetChanged();
                        }
                    }
                    else
                    {
                        bill_list.setVisibility(View.GONE);
                        tv_bill.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    bill_list.setVisibility(View.GONE);
                    tv_bill.setVisibility(View.VISIBLE);
                }
                
            }
        });
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tv_bill_sum);
    }
    
    boolean isTag = false;
    
    public Map<Integer, Boolean> isChecked;
    
    public class BillSumAdapter extends BaseAdapter
    {
        private List<BillItemModel> userModels;
        
        private BillSumAdapter(List<BillItemModel> userModels)
        {
            this.userModels = userModels;
        }
        
        @Override
        public int getCount()
        {
            
            return userModels.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            return userModels.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(BillSumActivity.this, R.layout.activity_settle_total_item, null);
                holder = new ViewHolder();
                holder.tv_time = (TextView)convertView.findViewById(R.id.tv_time);
                holder.tv_pay_total = (TextView)convertView.findViewById(R.id.tv_pay_total);
                holder.tv_receivable = (TextView)convertView.findViewById(R.id.tv_receivable);
                holder.tv_counter_fee = (TextView)convertView.findViewById(R.id.tv_counter_fee);
                holder.tv_sett_money = (TextView)convertView.findViewById(R.id.tv_sett_money);
                holder.iv_img = (ImageView)convertView.findViewById(R.id.iv_img);
                holder.lay_img = (LinearLayout)convertView.findViewById(R.id.lay_img);
                holder.lay_t = (LinearLayout)convertView.findViewById(R.id.lay_t);
                
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            
            BillItemModel billMordel = userModels.get(position);
            holder.lay_img.setOnClickListener(new btClick(position, holder.lay_t, holder.iv_img));
            if (billMordel != null)
            {
                holder.tv_time.setText(DateUtil.formatYYYYMD(billMordel.getPayTradeTime()));
                holder.tv_pay_total.setText(MainApplication.getFeeFh()
                    + DateUtil.formatMoneyUtils(billMordel.getPayRemitFee()));
                //                holder.tv_receivable.setText(getString(R.string.tv_settle_total_clean_money) + MainApplication.feeFh
                //                    + DateUtil.formatMoneyUtils(billMordel.getPayRemitFee()));
                if (billMordel.getPayTypaName().contains("`"))
                {
                    holder.tv_receivable.setText((billMordel.getPayTypaName().replaceAll("`", "")));
                }
                else
                {
                    holder.tv_receivable.setText(billMordel.getPayTypaName());
                }
                holder.tv_sett_money.setText(getString(R.string.tv_settle_total_clean_money)
                    + MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(billMordel.getPayNetFee()));
                if (billMordel.getMchFee() == 0)
                {
                    holder.tv_counter_fee.setText(getString(R.string.tv_settle_total_counter_fee)
                        + MainApplication.getFeeFh() + "0.00");
                }
                else
                {
                    holder.tv_counter_fee.setText(getString(R.string.tv_settle_total_counter_fee)
                        + MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(billMordel.getMchFee()));
                }
            }
            
            return convertView;
        }
        
        private class btClick implements OnClickListener
        {
            
            private int position;
            
            private LinearLayout lay;
            
            private ImageView iv;
            
            public btClick(int pos, LinearLayout lay, ImageView iv)
            { // 在构造时将position传给它这样就知道点击的是哪个条目的按钮
                this.position = pos;
                this.lay = lay;
                this.iv = iv;
            }
            
            @Override
            public void onClick(View v)
            {
                int vid = v.getId();
                if (vid == holder.lay_img.getId())
                {
                    if (isChecked.get(position) == false)
                    {
                        isChecked.put(position, true); // 根据点击的情况来将其位置和相应的状态存入    
                        //                        if (!isTag)
                        //                        {
                        //                            isTag = true;
                        lay.setVisibility(View.VISIBLE);
                        iv.setImageResource(R.drawable.icon_arrow_down_settle);
                        //                        }
                        //                        else
                        //                        {
                        //                            isTag = false;
                        //                            lay.setVisibility(View.GONE);
                        //                            iv.setImageResource(R.drawable.icon_arrow_right);
                        //                        }
                    }
                    else if (isChecked.get(position) == true)
                    {
                        isChecked.put(position, false); // 根据点击的情况来将其位置和相应的状态存入  
                        //                        if (!isTag)
                        //                        {
                        //                            isTag = true;
                        //                            lay.setVisibility(View.VISIBLE);
                        //                            iv.setImageResource(R.drawable.icon_arrow_down_settle);
                        //                        }
                        //                        else
                        //                        {
                        //                            isTag = false;
                        lay.setVisibility(View.GONE);
                        iv.setImageResource(R.drawable.icon_arrow_right);
                        //                        }
                    }
                }
            }
            
        }
    }
    
    private class ViewHolder
    {
        private TextView tv_time, tv_pay_total, tv_receivable, tv_counter_fee, tv_sett_money;
        
        private ImageView iv_img;
        
        private LinearLayout lay_img, lay_t;
        
    }
}
