/*
 * 文 件 名:  SignUtil.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2015-6-10
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.utils;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 签名秘钥 帮助
 * 
 * @author  he_hui
 * @version  [版本号, 2015-6-10]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class SignUtil
{
    private static SignUtil signUtil = null;
    
    public static SignUtil getInstance()
    {
        if (signUtil == null)
        {
            signUtil = new SignUtil();
        }
        return signUtil;
    }
    
    // 微信红包sign
   /* public String createRedpackSign(Map<String, String> params)
    {
        paraFilter(params);

        StringBuilder buf = new StringBuilder((params.size() + 1) * 10);
        buildPayParams(buf, params, false);
        String preStr = buf.toString();
        return MD5Util.MD5Encode(preStr, "utf-8").toUpperCase();
        //return cn.swiftpass.enterprise.utils.MD5.md5s(preStr).toUpperCase();
        //        return MD5.md5s(preStr).toUpperCase();
    }*/
    
    public String createSign(Map<String, String> params, String signKey)
    {
        params = paraFilter(params);
        StringBuilder buf = new StringBuilder((params.size() + 1) * 10);
        buildPayParams(buf, params, false);
        buf.append("&key=" + signKey);
        String preStr = buf.toString();
        return MD5.md5s(preStr).toUpperCase();
    }
    
    private void buildPayParams(StringBuilder sb, Map<String, String> payParams, boolean encoding)
    {
        List<String> keys = new ArrayList<String>(payParams.keySet());
        Collections.sort(keys);
        for (String key : keys)
        {
            sb.append(key).append("=");
            if (encoding)
            {
                sb.append(urlEncode(payParams.get(key)));
            }
            else
            {
                sb.append(payParams.get(key));
            }
            sb.append("&");
        }
        sb.setLength(sb.length() - 1);
    }
    
    /**
     * 过滤参数
     * @param sArray
     * @return
     */
    public static Map<String, String> paraFilter(Map<String, String> sArray)
    {
        Map<String, String> result = new HashMap<String, String>(sArray.size());
        if (sArray == null || sArray.size() <= 0)
        {
            return result;
        }
        for (String key : sArray.keySet())
        {
            String value = sArray.get(key);//password
            if (value == null || value.equals("") || key.equalsIgnoreCase("sign") || key.equalsIgnoreCase("nns")
                || key.equalsIgnoreCase("apiProviderList") || key.equalsIgnoreCase("tradeStateList"))
            {
                continue;
            }
            result.put(key, value);
        }
        return result;
    }
    
    private String urlEncode(String str)
    {
        try
        {
            return URLEncoder.encode(str, "UTF-8");
        }
        catch (Throwable e)
        {
            return str;
        }
    }
}
