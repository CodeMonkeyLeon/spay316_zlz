package cn.swiftpass.enterprise.common;

/**
 * Created by congwei.li on 2021/12/21.
 *
 * @Description:
 */
public class Constant {

    public static final int OLD_VERSION = 99;

    public static final String ORDER_STATE_SUCCESS = "2";//成功


    public static final String MY_PERMISSION = "cn.swiftpass.enterprise.my_permission";

    public static final String PAY_WX_MICROPAY = "pay.weixin.micropay"; // 微信扫一扫支付

    public static final String PAY_ZFB_MICROPAY = "pay.alipay.micropay"; // 支付宝扫一扫

    public static final String PAY_QQ_MICROPAY = "pay.qq.micropay"; // 手Q扫一扫

    public static final String PAY_WX_NATIVE = "pay.weixin.native"; // 微信扫码支付

    public static final String PAY_WX_NATIVE1 = "pay.weixin.native1"; // 切换支付方式用的标示

    public static final String PAY_ZFB_NATIVE = "pay.alipay.native"; // 支付宝扫码

    public static final String PAY_ZFB_NATIVE1 = "pay.alipay.nativev2"; // 支付宝扫码

    public static final String PAY_QQ_NATIVE = "pay.tenpay.native"; // 手Q扫码

    public static final String PAY_QQ_NATIVE1 = "pay.qq.jspay"; // 手Q扫码

    public static final String PAY_WX_SJPAY = "pay.weixin.jspay"; // 手Q扫码

    public static final String PAY_ZFB_QUERY = "unified.trade.query";

    public static final String PAY_WX_QUERY = "trade.single.query";

    public static final String PAY_QQ_PROXY_MICROPAY = "pay.qq.proxy.micropay"; // 手Q反扫受理机构模式

    public static final String PAY_ALIPAY_WAP = "pay.alipay.wappay";//支付宝wap支付

    public static final String PAY_ALIPAY_TAG = "alipay";

    public static final String PAY_WEIXIN_TAG = "weixin";

    public static final String PAY_QQ_TAG = "qq";

    public static final String PAY_JD_TAG = "jdpay";

    public static final String PAY_WX_MIC_INTL = "pay.weixin.micropay.intl"; // 微信境外支付

    public static final String PAY_WX_NATIVE_INTL = "pay.weixin.native.intl"; // 微信扫码支付支付

    public static final String PAY_TYPE_REFUND = "pay.scan.refund"; // 扫码退款

    public static final String PAY_TYPE_WRITE_OFF = "pay.weixin.writeoff"; // 微信卡券核销

    public static final String PAY_TYPE_SCAN_OPNE = "pay.weixin.scan.open"; // 二维码激活开通

    public static final String PAY_TYPE_NITINE = "pay.weixin.vard"; // 微信卡券核销

    public static final String PAY_TYPE_MICROPAY_VCARD = "pay.weixin.micropay.vard"; // 微信卡券核销

    public static final String PAY_JINGDONG = "pay.jdpay.micropay"; // 京东钱包反扫

    public static final String PAY_JINGDONG_NATIVE = "pay.jdpay.native"; // 京东钱包正扫

    public static final String PAY_ZFB_WAP = "pay.alipay.wappayv2"; // 支付宝wap支付

    public static final String PAY_ALIPAY_AUTH_MICROPAY = "pay.alipay.auth.micropay.freeze"; // 支付宝预授权支付

    public static final String PAY_ALIPAY_AUTH_NATIVEPAY = "pay.alipay.auth.native.freeze"; // 支付宝预授权正扫

    public static final String CLIENT = "SPAY_AND"; //android终端类型

    public static final String PAY_QQ_WAP = "pay.tenpay.wappay";// QQwap支付

    public static final String DEF_PAY_METHOD = "pay.weixin.native";//默认微信支付方式
    //获取liquid小钱包的type,默认写死在前端
    public static final String NATIVE_LIQUID = "pay.liquid.native.wallet";

    //获取AUB---instapay的type,默认写死在前端
    public static final String NATIVE_INSTAPAY = "pay.instapay.native";

    //获取AUB---instapay的type,默认写死在前端
    public static final String NATIVE_INSTAPAY_V2 = "pay.instapay.native.v2";

    public static final int RB_INSTAPAY_CODE = 69;

    //获取预授权的type,默认写死在前端
    public static final String NATIVE_ALIPAY_AUTH = "pay.alipay.auth.native.freeze";

    //转换成人民币之后，人民币的币种按照最小单位的换算
    public static final double RMB_FIX = 100.0;

    public static final String LANG_CODE_ZH_CN_NEW = "zh-CN";

    public static final String LANG_CODE_ZH_TW_NEW = "zh-tw";

    public static final String LANG_CODE_ZH_MO_NEW = "zh-MO";

    public static final String LANG_CODE_ZH_HK_NEW = "zh-HK";

    public static final String LANG_CODE_JA_JP_NEW = "ja-JP";

    /**
     * 简体中文
     */
    public static final String LANG_CODE_ZH_CN = "zh_cn";

    /**
     * 繁体中文
     */
    public static final String LANG_CODE_ZH_TW = "zh_tw";

    public static final String LANG_CODE_ZH_MO = "zh_MO";

    public static final String LANG_CODE_ZH_HK = "zh_HK";

    public static final String LANG_CODE_JA_JP = "ja_JP";

    /**
     * 英语
     */
    public static final String LANG_CODE_EN_US = "en_us";

    public static final String LANG_CODE_ZH_CN_HANS = "zh_CN_#Hans";
    public static final String LANG_CODE_ZH_HK_HANT = "zh_HK_#Hant";


    public static final String SERVER_CONFIG = "serverConfig";


    public static final String SERVER_ADDRESS_DEFAULT = "default";
    public static final String SERVER_ADDRESS_SELF = "self";
    public static final String SERVER_ADDRESS_PRD = "prd";
    public static final String SERVER_ADDRESS_DEV = "dev";
    public static final String SERVER_ADDRESS_DEV_JH = "dev-jh";
    public static final String SERVER_ADDRESS_TEST_123 = "test123";
    public static final String SERVER_ADDRESS_TEST_61 = "test61";
    public static final String SERVER_ADDRESS_TEST_63 = "test63";
    public static final String SERVER_ADDRESS_TEST_UAT = "testUAT";
    public static final String SERVER_ADDRESS_OVERSEAS_61 = "overseas61";
    public static final String SERVER_ADDRESS_OVERSEAS_63 = "overseas63";
    public static final String SERVER_ADDRESS_CHECKOUT_DEV = "checkoutDev";
}
