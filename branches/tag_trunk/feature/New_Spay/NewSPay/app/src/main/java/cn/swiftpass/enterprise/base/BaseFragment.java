package cn.swiftpass.enterprise.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.leakcanary.RefWatcher;
import com.trello.rxlifecycle2.components.support.RxFragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.swiftpass.enterprise.BuildConfig;
import cn.swiftpass.enterprise.SpayApplication;
import cn.swiftpass.enterprise.base.mvp.BasePresenter;
import cn.swiftpass.enterprise.util.Preconditions;

public abstract class BaseFragment<P extends BasePresenter> extends RxFragment {

    protected Activity mActivity;
    /**
     * 根布局视图
     */
    private View mContentView;
    private Unbinder mUnbinder;
    protected P mPresenter;

    /**
     * 返回页面布局id
     * @return
     */
    protected abstract int getContentLayout();

    /**
     * 做视图相关的初始化工作
     */
    protected abstract void initView();

    private final String errorMessage = "根布局的id非法导致根布局为空,请检查后重试!";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (Activity) context;
    }

    @SuppressLint("RestrictedApi")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mContentView == null) {
            try {
                mContentView = inflater.inflate(getContentLayout(), container, false);
                mUnbinder = ButterKnife.bind(this, mContentView);
                initView();
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }

            Preconditions.checkNotNull(mContentView, errorMessage);
        }
        return mContentView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //ButterKnife解绑
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.detachView();
            mPresenter = null;
        }
        //debug检查fragment是否有内存泄漏
        if(BuildConfig.DEBUG) {
            RefWatcher refWatcher = SpayApplication.getRefWatcher();
            refWatcher.watch(this);
        }
    }
}
