package cn.swiftpass.enterprise.mvp.presenter.dialog

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.ErrorMsg
import cn.swiftpass.enterprise.bussiness.model.UserInfo
import cn.swiftpass.enterprise.common.Constant
import cn.swiftpass.enterprise.intl.BuildConfig
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.dialog.ScanCodeCheckContract
import cn.swiftpass.enterprise.mvp.utils.ParamsUtils
import cn.swiftpass.enterprise.utils.AESHelper
import cn.swiftpass.enterprise.utils.JsonUtil
import cn.swiftpass.enterprise.utils.MD5
import cn.swiftpass.enterprise.utils.SharedPreUtils
import cn.swiftpass.enterprise.utils.StringUtil
import cn.swiftpass.enterprise.utils.ToastHelper
import cn.swiftpass.enterprise.utils.Utils
import com.igexin.sdk.PushManager
import okhttp3.Call
import java.util.Locale

/**
 * @author lizheng.zhao
 * @date 2022/12/7
 *
 * @所谓最难忘的
 * @就是从来不曾想起
 * @却永远也不会忘记
 */
class ScanCodeCheckPresenter : ScanCodeCheckContract.Presenter {

    private var mView: ScanCodeCheckContract.View? = null


    override fun loginAsync(
        code: String?,
        username: String?,
        password: String?
    ) {
        mView?.let { view ->

            view.showLoading(R.string.wait_a_moment, ParamsConstants.COMMON_LOADING)

            var psd = ""
            val priKey = SharedPreUtils.readProduct(ParamsConstants.SECRET_KEY) as String?
            val paseStr = MD5.md5s(priKey).uppercase(Locale.getDefault())
            password?.let {
                psd = AESHelper.aesEncrypt(it, paseStr.substring(8, 24))
            }

            AppClient.loginAsync(
                username,
                psd,
                Constant.CLIENT,
                MainApplication.getInstance().sKey,
                BuildConfig.bankCode,
                "1",
                code,
                PushManager.getInstance().getClientid(MainApplication.getInstance()),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<UserInfo>(UserInfo()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { error ->
                            val ret = Utils.Integer.tryParse(error.result, -1)
                            if (ret == 405) { //Require to renegotiate ECDH key
                                view.loginAsyncFailed("405${error.message}")
                            } else if (ret == 403) { //需要输入验证
                                val c = error.code
                                if (!StringUtil.isEmptyOrNull(code)) {
                                    val msg = ErrorMsg()
                                    msg.message = error.message
                                    msg.content = error.code
                                    view.loginAsyncFailed(msg)
                                } else {
                                    view.loginAsyncFailed("403$c")
                                }
                            } else if (ret in 500..599) {
                                view.loginAsyncFailed(ToastHelper.toStr(R.string.failed_to_connect_server))
                            } else if (TextUtils.isEmpty(error.message)) {
                                view.loginAsyncFailed(ToastHelper.toStr(R.string.failed_to_login_try_later))
                            } else {
                                view.loginAsyncFailed(error.message)
                            }
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val u = JsonUtil.jsonToBean(
                                response.message,
                                UserInfo::class.java
                            ) as UserInfo
                            MainApplication.getInstance().userInfo = u
                            ParamsUtils.parseJson(MainApplication.getInstance().userInfo)
                            ParamsUtils.saveLoginCookieToSp(response.message)
                            view.loginAsyncSuccess(true)
                        }
                    }
                }
            )

        }
    }


    override fun attachView(view: ScanCodeCheckContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}