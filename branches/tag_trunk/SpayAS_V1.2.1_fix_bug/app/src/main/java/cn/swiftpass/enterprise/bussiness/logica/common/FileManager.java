package cn.swiftpass.enterprise.bussiness.logica.common;

import android.graphics.Bitmap;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.NetHelper;

/**
 * 上传下载图片
 * User: Alan
 * Date: 13-12-24
 * Time: 下午2:23
 * To change this template use File | Settings | File Templates.
 */
public class FileManager
{
    
    private static FileManager instance;
    
    public static FileManager getInstance()
    {
        if (instance == null)
        {
            instance = new FileManager();
        }
        return instance;
    }
    
    public void uploadFile(Bitmap bitmap)
    {
        
    }
    
    public void downloadImg(final String id, final UINotifyListener<Bitmap> listener, final int flag)
    {
        ThreadHelper.executeWithCallback(new Executable()
        {
            @Override
            public Bitmap execute()
                throws Exception
            {
                if (flag == 1)
                {
                    return NetHelper.getBitmap(ApiConstant.BASE_URL_PORT + ApiConstant.DONLOAD_IMAGE + id);
                }
                else
                {
                    return NetHelper.getBitmap(ApiConstant.BASE_URL_PORT + ApiConstant.DONLOAD_SIGNATURE_IMAGE + id);
                }
            }
        }, listener);
    }
    
    public void loadImg(final String url, final UINotifyListener<Bitmap> listener)
    {
        ThreadHelper.executeWithCallback(new Executable()
        {
            @Override
            public Bitmap execute()
                throws Exception
            {
                return NetHelper.getBitmap(url);
            }
        }, listener);
    }
}
