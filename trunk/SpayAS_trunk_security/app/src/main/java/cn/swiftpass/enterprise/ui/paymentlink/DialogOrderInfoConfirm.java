package cn.swiftpass.enterprise.ui.paymentlink;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.common.sentry.SentryUtils;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.paymentlink.adapter.OtherFeeConfirmAdapter;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkOrder;
import cn.swiftpass.enterprise.utils.DateUtil;

/**
 * Created by congwei.li on 2021/9/16.
 *
 * @Description: 确认订单信息
 */
public class DialogOrderInfoConfirm extends Dialog {
    private static final String TAG = "DialogOrderInfoConfirm";
    Context mContext;
    TextView tvTotalItemNum, tvTotalItemAmount, tvOrderTotalAmount, tvCancel, tvConfirm;
    LinearLayout productTotalFee;
    RecyclerView rvOtherFee;
    PaymentLinkOrder order;

    private DialogOrderInfoConfirm.DialogClickListener listener;

    public DialogOrderInfoConfirm(@NonNull Context context, PaymentLinkOrder order) {
        super(context, R.style.simpleDialog);
        mContext = context;
        this.order = order;
        init();
    }

    public void init() {
        View rootView = View.inflate(mContext, R.layout.dialog_order_info_confirm, null);
        setContentView(rootView);

        tvTotalItemNum = (TextView) rootView.findViewById(R.id.tv_total_item_num);
        tvTotalItemAmount = (TextView) rootView.findViewById(R.id.tv_total_item_amount);
        tvOrderTotalAmount = (TextView) rootView.findViewById(R.id.tv_order_total_amount);
        tvCancel = (TextView) rootView.findViewById(R.id.tv_cancel);
        tvConfirm = (TextView) rootView.findViewById(R.id.tv_confirm);
        rvOtherFee = (RecyclerView) rootView.findViewById(R.id.rv_other_fee);
        productTotalFee = (LinearLayout) rootView.findViewById(R.id.ll_product_total_fee);

        if (null != order.orderRelateGoodsList && order.orderRelateGoodsList.size() > 0) {
            tvTotalItemNum.setText(String.format(
                    mContext.getResources().getString(R.string.payment_link_add_order_total_item),
                    order.getTotalProductNum()));

            int totalProductAmount = 0;
            for (int i = 0; i < order.orderRelateGoodsList.size(); i++) {
                try {
                    totalProductAmount += Double.parseDouble(order.orderRelateGoodsList.get(i).goodsPrice) *
                            Double.parseDouble(order.orderRelateGoodsList.get(i).goodsNum);
                } catch (Exception e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                }
            }
            tvTotalItemAmount.setText(MainApplication.getInstance().getFeeFh() +
                    DateUtil.formatMoneyUtils(totalProductAmount));
            productTotalFee.setVisibility(View.VISIBLE);

        } else {
            productTotalFee.setVisibility(View.GONE);
        }
        if (order.orderCostList != null && order.orderCostList.size() > 0) {
            rvOtherFee.setVisibility(View.VISIBLE);
            OtherFeeConfirmAdapter adapter = new OtherFeeConfirmAdapter(mContext, order.orderCostList);
            rvOtherFee.setLayoutManager(new LinearLayoutManager(getContext(),
                    LinearLayoutManager.VERTICAL, false));
            rvOtherFee.setAdapter(adapter);
        } else {
            rvOtherFee.setVisibility(View.GONE);
        }

        tvOrderTotalAmount.setText(MainApplication.getInstance().getFeeFh() +
                DateUtil.formatMoneyUtils(order.totalFee));

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (listener != null) {
                    listener.onClickConfirm();
                }
            }
        });
    }

    /**
     * 定义接口用来响应button的监听
     */
    public interface DialogClickListener {
        void onClickConfirm();
    }

    public void setCurrentListener(DialogOrderInfoConfirm.DialogClickListener listener) {
        this.listener = listener;
    }


}