/**
 * Copyright 2014  XCL-Charts
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @Project XCL-Charts
 * @Description Android图表基类库
 * @author XiongChuanLiang<br/>(xcl_168@aliyun.com)
 * @Copyright Copyright (c) 2014 XCL-Charts (www.xclcharts.com)
 * @license http://www.apache.org/licenses/  Apache v2 License
 * @version 1.0
 */
package cn.swiftpass.enterprise.ui.activity.draw;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import org.xclcharts.chart.BarChart;
import org.xclcharts.chart.BarData;
import org.xclcharts.common.DensityUtil;
import org.xclcharts.common.IFormatterTextCallBack;
import org.xclcharts.event.click.BarPosition;
import org.xclcharts.renderer.XEnum;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.CashierTop5Bean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateTimeUtil;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * @author XiongChuanLiang<br/>(xcl_168@aliyun.com)
 * @ClassName BarChart02View
 * @Description 柱形图例子(横向)
 */
public class BarChart02ViewNew extends DemoView {
    private static final String TAG = "BarChart02View";
    private Context context;
    private BarChart chart;

    //标签轴
    private List<String> chartLabels = new LinkedList<String>();
    private List<BarData> chartData = new LinkedList<BarData>();

    private List<CashierTop5Bean> cashierTopInfoList;

    private List<Double> MoneyList = new ArrayList<>();
    private List<Double> CountList = new ArrayList<>();

    private String mPageType; //是金额还是笔数

    public BarChart02ViewNew(Context context, List<CashierTop5Bean> cashierTopInfoList,String PageType) {
        super(context);
        this.context = context;
        this.cashierTopInfoList = cashierTopInfoList;
        this.mPageType = PageType;
        chart = new BarChart(context);
        chart.setmContent(context);

        MoneyList.clear();
        CountList.clear();

        for (CashierTop5Bean info : cashierTopInfoList) {
            if(mPageType.equalsIgnoreCase("1")){
                if (info.getTransactionAmount() > 0) {
                    double money = DateUtil.StringToMoney(DateUtil.formatMoneyUtils(info.getTransactionAmount()));
                    MoneyList.add(money);
                }
            }else if(mPageType.equalsIgnoreCase("2")){
                if (info.getTransactionCount() > 0) {
                    CountList.add((double)info.getTransactionCount());
                }
            }

        }
        initView();
    }

    public BarChart02ViewNew(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public BarChart02ViewNew(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
    }

    private void initView() {
        chartLabels();
        chartDataSet();
        chartRender();

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        //图所占范围大小
        if (null != chart){
            chart.setChartRange(w, h);
        }

    }

    private static float BARMARGIN = 0;
    /*
     * 计算如果有不同的数量的柱形图的时候，离底部标签的距离
     * */
    public void CaculateBarMarginBottom(int count){
        switch (count){
            case 1:
                BARMARGIN = getResources().getDimension(R.dimen.dp_75);
                break;
            case 2:
                BARMARGIN = getResources().getDimension(R.dimen.dp_55);
                break;
            case 3:
                BARMARGIN = getResources().getDimension(R.dimen.dp_40);
                break;
            case 4:
                BARMARGIN = getResources().getDimension(R.dimen.dp_25);
                break;
            case 5:
                BARMARGIN = 0;
                break;
        }

    }
    private void chartRender() {
        try {
            //设置绘图区默认缩进px值,留置空间显示Axis,Axistitle....	
            int[] ltrb = getBarLnDefaultSpadding();

            float textSize = getResources().getDimension(R.dimen.sp_10);

            int maxTextWidth = DensityUtil.getMaxTextWidth(chartLabels, textSize, MainApplication.getFeeFh());
            chart.setPadding(getResources().getDimension(R.dimen.dp_20) + maxTextWidth, ltrb[1], ltrb[2], ltrb[3]);

            chart.setTitleVerticalAlign(XEnum.VerticalAlign.MIDDLE);
            chart.setTitleAlign(XEnum.HorizontalAlign.LEFT);

            if(mPageType.equalsIgnoreCase("1")){
                CaculateBarMarginBottom(MoneyList.size());
            }else if(mPageType.equalsIgnoreCase("2")){
                CaculateBarMarginBottom(CountList.size());
            }
            chart.setBarPadding(BARMARGIN);

            //数据源
            chart.setDataSource(chartData);
            chart.setCategories(chartLabels);
            chart.getDataAxis().isPaintLine = false;

            double max = 0;
            //数据轴
            chart.getDataAxis().setAxisMin(0);
            if(mPageType.equalsIgnoreCase("1")){
                max = Collections.max(MoneyList);
            }else if(mPageType.equalsIgnoreCase("2")){
                max = Collections.max(CountList);
            }

            if(max <4){
                max = 4;
                chart.getDataAxis().setAxisMax(max);
            }else{
                chart.getDataAxis().setAxisMax(max);
            }

            chart.getDataAxis().setAxisSteps(max / 4d + 0.00249d);

            chart.getBar().setBarStyle(XEnum.BarStyle.ROUNDBAR);
            chart.getBar().setBarRoundRadius(0);


            chart.getDataAxis().getTickLabelPaint().setColor(Color.parseColor("#666666"));
            chart.getDataAxis().hideFirstTick();

            chart.getDataAxis().setLabelFormatter(new IFormatterTextCallBack() {

                @Override
                public String textFormatter(String value) {
                    // TODO Auto-generated method stub
                    if(mPageType.equalsIgnoreCase("1")){
                        double valueDouble = DateUtil.convertToDouble(value,0);
                        BigDecimal BigValue = new BigDecimal(valueDouble);
                        BigDecimal res = BigValue.divide(new BigDecimal(1),2,BigDecimal.ROUND_HALF_UP);
                        try {
                            String temp = formatMoneyToString(res.doubleValue());
                            return temp;
                        } catch (Exception e) {
                            e.printStackTrace();
                            return value;
                        }
                    }else if(mPageType.equalsIgnoreCase("2")){
                        double valueDouble = DateUtil.convertToDouble(value,0);
                        int count = (int)valueDouble;
                        try {
                            String temp = formatCountToString(count);
                            return temp;
                        } catch (Exception e) {
                            e.printStackTrace();
                            return value;
                        }
                    }
                    return null;

                }

            });
            //网格
            chart.getPlotGrid().hideHorizontalLines();
            chart.getPlotGrid().showVerticalLines();
            chart.getPlotGrid().getVerticalLinePaint().setColor(Color.parseColor("#999999"));
            //            chart.getPlotGrid().getHorizontalLinePaint().setAlpha(150);
            chart.getPlotGrid().getVerticalLinePaint().setStrokeWidth(DensityUtil.dip2px(getContext(), 0.1f));
            //            chart.getPlotGrid().showEvenRowBgColor();
            //横向显示柱形
            chart.setChartDirection(XEnum.Direction.HORIZONTAL);
            //在柱形顶部显示值
            chart.getBar().setItemLabelVisible(false);
            chart.getBar().getItemLabelPaint().setTextSize(textSize);
            chart.setPlotPanMode(XEnum.PanMode.HORIZONTAL);
            chart.getCategoryAxis().hideTickMarks();
            chart.getDataAxis().hideTickMarks();
            chart.getDataAxis().show();
            chart.getDataAxis().getAxisPaint().setColor(Color.parseColor("#999999"));
            chart.getDataAxis().getAxisPaint().setStrokeWidth(DensityUtil.dip2px(getContext(), 0.1f));
            chart.disableScale();
            chart.getDataAxis().setTickLabelMargin(DensityUtil.dip2px(getContext(), 10));
            chart.getDataAxis().getTickLabelPaint().setTextSize(textSize);
            chart.getCategoryAxis().setTickLabelMargin(maxTextWidth);


            chart.getCategoryAxis().getAxisPaint().setColor(Color.parseColor("#999999"));
            chart.getCategoryAxis().getAxisPaint().setStrokeWidth(DensityUtil.dip2px(getContext(), 0.1f));
            chart.getCategoryAxis().getTickLabelPaint().setColor(Color.parseColor("#999999"));
            chart.getCategoryAxis().getTickLabelPaint().setTextSize(textSize);

            chart.getCategoryAxis().setLabelFormatter(new IFormatterTextCallBack() {
                @Override
                public String textFormatter(String value) {
                    if(mPageType.equalsIgnoreCase("1")){
                        if(!StringUtil.isEmptyOrNull(value)){
                            String arrStr[] = value.split("\\|");
                            /*if(arrStr[0].length() > 10){
                                value = arrStr[0].substring(0,10) +".."+"|"+"("+ MainApplication.getFeeFh() + " " +arrStr[1]+")";
                            }else{
                                value = arrStr[0]+"|"+"("+ MainApplication.getFeeFh() + " " +arrStr[1]+")";
                            }*/
                            value = arrStr[0]+"|"+ DateUtil.getMoneyWithFeefh(arrStr[1]);
                        }
                    }else if(mPageType.equalsIgnoreCase("2")){
                        if(!StringUtil.isEmptyOrNull(value)){
                            String arrStr[] = value.split("\\|");
                            /*if(arrStr[0].length() > 10){
                                value = arrStr[0].substring(0,10) +".."+"|" +"("+arrStr[1]+")";
                            }else{
                                value = arrStr[0]+"|" +"("+arrStr[1]+")";
                            }*/
                            value = arrStr[0]+"|" + arrStr[1];

                        }
                    }
                    return value;
                }
            });

            chart.disablePanMode();
            chart.disableScale();
            chart.disabledCtlPanRange();
            /*
            chart.setDataAxisPosition(XEnum.DataAxisPosition.BOTTOM);
            chart.getDataAxis().setVerticalTickPosition(XEnum.VerticalAlign.BOTTOM);
            
            chart.setCategoryAxisPosition(XEnum.CategoryAxisPosition.LEFT);
            chart.getCategoryAxis().setHorizontalTickAlign(Align.LEFT);
            */

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        }
    }

    private void chartDataSet() {
        //标签对应的柱形数据集
        List<Double> dataSeriesA = new LinkedList<Double>();
        dataSeriesA.addAll(MoneyList);
        List<Double> dataSeriesB = new LinkedList<Double>();
        dataSeriesB.addAll(CountList);

        BarData BarDataA = new BarData("", dataSeriesA, Color.parseColor("#11A0F8"));
        BarData BarDataB = new BarData("", dataSeriesB, Color.parseColor("#11A0F8"));

        if(mPageType.equalsIgnoreCase("1")){
            chartData.add(BarDataA);
        }else if(mPageType.equalsIgnoreCase("2")){
            chartData.add(BarDataB);
        }
    }

    private void chartLabels() {
        for (int i = 0; i < cashierTopInfoList.size(); i++) {
            CashierTop5Bean info = cashierTopInfoList.get(i);
            if (info != null ) {
                String name= info.getCashierName();
                if(mPageType.equalsIgnoreCase("1")){
                    chartLabels.add(name + "|" + DateUtil.formatMoneyUtils(info.getTransactionAmount()));
                }else if(mPageType.equalsIgnoreCase("2")){
                    chartLabels.add(name + "|" +info.getTransactionCount());
                }
            }
        }

        if (chartLabels.size() < 5) {
            int len = chartLabels.size() % 5;
            for (int i = 0; i < (5 - len); i++) {
                chartLabels.add("");
            }
        }
    }

    @Override
    public void render(Canvas canvas) {
        try {
            chart.render(canvas);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub		
        super.onTouchEvent(event);
        if (event.getAction() == MotionEvent.ACTION_UP) {
//            triggerClick(event.getX(), event.getY());
        }
        return true;
    }

    //触发监听
    private void triggerClick(float x, float y) {
        BarPosition record = chart.getPositionRecord(x, y);
        if (null == record){
            return;
        }


        BarData bData = chartData.get(record.getDataID());
        Double bValue = bData.getDataSet().get(record.getDataChildID());

        Toast.makeText(this.getContext(),
                "info:" + record.getRectInfo() + " Key:" + bData.getKey() + " Current Value:" + Double.toString(bValue),
                Toast.LENGTH_SHORT).show();

        chart.showFocusRectF(record.getRectF());
        chart.getFocusPaint().setStyle(Style.STROKE);
        chart.getFocusPaint().setStrokeWidth(3);
        chart.getFocusPaint().setColor(Color.GREEN);
        this.invalidate();
    }


    //對數據進行處理---按照以下算法和格式
//    交易数据图表纵坐标逻辑：
//    若a为实际数据，b为展示数据
//    if  0<=a<1000:
//    b=a
//    elif  1000<=a<1,000,000:
//    b= “a/1000” + “K”
//    elif  1,000,000<=a<1,000,000,000:
//    b= “a/1,000,000 ” + “M”
//            else:
//    b= “a/1,000,000,000” + “B”
//
//    此处b保留一位小数

    public String formatMoneyToString(Double Money){
        String formatMoney;
        double divideNum = 1.0f;
        if(0 <= Money && Money < 1000 ){
            divideNum = 1.0f;
        }else if(Money >= 1000 && Money < 1000000){
            divideNum = 1000.0f;
        }else if(Money >= 1000000 && Money < 1000000000){
            divideNum = 1000000.0f;
        }else{
            divideNum = 1000000000.0f;
        }
        BigDecimal BigDecimalDivideNum = new BigDecimal(divideNum);
        BigDecimal BigDecimalMoney = new BigDecimal(Money).divide(BigDecimalDivideNum,1,BigDecimal.ROUND_HALF_UP);

        if(0 <= Money && Money < 1000 ){
            formatMoney = Money+"";
        }else if(Money >= 1000 && Money < 1000000){
            formatMoney = BigDecimalMoney.toString()+"K";
        }else if(Money >= 1000000 && Money < 1000000000){
            formatMoney = BigDecimalMoney.toString()+"M";
        }else{
            formatMoney = BigDecimalMoney.toString()+"B";
        }
        return formatMoney;
    }

    public String formatCountToString(int count){
        String formatCount;
        int divideNum = 1;
        if(0 <= count && count < 1000 ){
            divideNum = 1;
        }else if(count >= 1000 && count < 1000000){
            divideNum = 1000;
        }else if(count >= 1000000 && count < 1000000000){
            divideNum = 1000000;
        }else{
            divideNum = 1000000000;
        }
        BigDecimal BigDecimalDivideNum = new BigDecimal(divideNum);
        BigDecimal BigDecimalCount = new BigDecimal(count).divide(BigDecimalDivideNum,0,BigDecimal.ROUND_HALF_UP);

        if(0 <= count && count < 1000 ){
            formatCount = count+"";
        }else if(count >= 1000 && count < 1000000){
            formatCount = BigDecimalCount.toString()+"K";
        }else if(count >= 1000000 && count < 1000000000){
            formatCount = BigDecimalCount.toString()+"M";
        }else{
            formatCount = BigDecimalCount.toString()+"B";
        }
        return formatCount;
    }

}
