package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tencent.stat.StatService;

import java.util.Locale;
import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.upgrade.UpgradeManager;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.GoodsMode;
import cn.swiftpass.enterprise.bussiness.model.MerchantTempDataModel;
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.print.BluetoothSettingActivity;
import cn.swiftpass.enterprise.ui.activity.scan.CaptureActivity;
import cn.swiftpass.enterprise.ui.activity.setting.SettingSwitchLanActivity;
import cn.swiftpass.enterprise.ui.activity.shop.ShopkeeperActivity;
import cn.swiftpass.enterprise.ui.activity.user.GoodsNameSetting;
import cn.swiftpass.enterprise.ui.activity.user.ReplactTelActivity;
import cn.swiftpass.enterprise.ui.activity.user.TuiKuanPwdSettingActivity;
import cn.swiftpass.enterprise.ui.widget.CommonConfirmDialog;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.LanguageDialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.NetworkUtils;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 设置 会自动检查版本 User: ALAN Date: 13-10-28 Time: 下午5:11 To change this template use
 * File | Settings | File Templates.
 */
public class SettingMoreActivity extends TemplateActivity implements View.OnClickListener {
    private static final String TAG = SettingMoreActivity.class.getSimpleName();

    public static final String ACTION_BLUETOOTH_CONNECT_IS_SUCCESS = "action_bluetooth_connect_is_success";
    public static final String ACTION_BLUETOOTH_CONNECT_IS_CANCEL = "action_bluetooth_connect_is_cancel";

    private Context mContext;

    private TextView tvVersion;

    // private TextView tvUser;

    //private LinearLayout llCoupon;

    private LinearLayout llChangePwd, empManager, replaceTel, lay_scan,cashier_layout;

    //private View lineChangePwd;

    private ImageView ivNewVersion;

    private TextView tv_debug, textView1, textView2;

    //private LayoutInflater inflater;

    //private View view, sum_id, bodyLay_id, empManager_id, refundLay_id, ll_shopinfo_id, view_manager, view_last;


    private Button ll_logout;

    private SharedPreferences sp;

    private LinearLayout bodyLay, write_off, ll_static_code, lay_prize;

    //private View marketing_id;

    //private LinearLayout sumLay;

    private LinearLayout marketingLay, lay_choise, ly_pay_method;

    private ImageView iv_voice, iv_point;

    //private View vi_pay_method;

    private LinearLayout device_lay, lay_notify, lay_choise_lanu;
    //private LinearLayout ly_bottom;

    private View device_view;

    private TextView tv_body, tv_device, tv_bluetool_device;

    private LinearLayout bluetool_lay;

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_setting);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButLayClick() {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        initLanuage();
    }

    private void initLanuage(){
        String voice = PreferenceUtil.getString("language", "");
        if (!StringUtil.isEmptyOrNull(voice))
        {
            if (voice.equals(MainApplication.LANG_CODE_ZH_TW)
                    || voice.equals(MainApplication.LANG_CODE_ZH_HK)
                    || voice.equals(MainApplication.LANG_CODE_ZH_MO)
                    || voice.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_HANT)) {
                tv_device.setText("中文(繁體)");

            } else if (voice.equals(MainApplication.LANG_CODE_ZH_CN) || voice.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_HANS))
            {
                tv_device.setText("中文(简体)");
            } else {
                tv_device.setText("English");
            }
        }
        else
        {
            String lan = Locale.getDefault().toString();
            if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK)
                    || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO)
                    ||lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW)
                    || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_HANT))
            {
                tv_device.setText("中文(繁體)");
            }
            else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_HANS))
            {
                tv_device.setText("中文(简体)");
            } else
            {
                tv_device.setText("English");
            }

        }
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                //蓝牙关闭
                case HandlerManager.BLUE_CONNET_STUTS_CLOSED:
                    tv_bluetool_device.setText(R.string.tx_print_no_conn);
                    break;
                //蓝牙已连接上后
                case HandlerManager.BLUE_CONNET_STUTS:
                    String name = MainApplication.getBlueDeviceName();
                    if (!StringUtil.isEmptyOrNull(name)) {
                        tv_bluetool_device.setText(name);
                    } else {
                        tv_bluetool_device.setText(R.string.tx_print_no_conn);
                    }
                    break;
                case HandlerManager.BLUE_CONNET:
                    String deviceName = MainApplication.getBlueDeviceName();
                    if (!StringUtil.isEmptyOrNull(deviceName)) {
                        tv_bluetool_device.setText(deviceName);
                    } else {
                        tv_bluetool_device.setText(R.string.tx_print_no_conn);
                    }
                    break;

                case HandlerManager.NOTICE_TYPE:
                    isHidePoint(iv_point);
                    break;
                case HandlerManager.VICE_SWITCH:
                    //                    String voice = PreferenceUtil.getString("voice", "close");
                    //                    if (voice.equals("open"))
                    //                    {
                    //                        iv_voice.setImageResource(R.drawable.button_configure_switch_default);
                    //                    }
                    //                    else
                    //                    {
                    //                        iv_voice.setImageResource(R.drawable.button_configure_switch_close);
                    //                    }
                    break;
                case HandlerManager.BODY_SWITCH:
                    String body = (String) msg.obj;
                    if (body != null) {
                        tv_body.setText(body);
                    }
                    break;
                case HandlerManager.DEVICE_SWITCH:
                    String device = (String) msg.obj;
                    if (device != null) {
                        tv_device.setText(device);
                    }
                    break;
                default:
                    MerchantTempDataModel dataModel = (MerchantTempDataModel) msg.obj;
                    Integer merchantType = dataModel.getMerchantType();
                    Intent intent = new Intent(SettingMoreActivity.this, ShopkeeperActivity.class);
                    intent.putExtra("merchantType", merchantType);
                    SettingMoreActivity.this.startActivity(intent);
                    break;
            }
        }

        ;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sp = this.getSharedPreferences("login", 0);

        initViews();
        HandlerManager.registerHandler(HandlerManager.NOTICE_TYPE, handler);

        HandlerManager.registerHandler(HandlerManager.VICE_SWITCH, handler);

        SettingMoreActivity.this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                isShowPoint(iv_point);
            }
        });

        IntentFilter Filter_Success = new IntentFilter();
        Filter_Success.addAction("action_bluetooth_connect_is_success");
        registerReceiver(updateBluetoothReceiver, Filter_Success,MainApplication.permission,null);

        IntentFilter Filter_Cancel = new IntentFilter();
        Filter_Cancel.addAction("action_bluetooth_connect_is_cancel");
        registerReceiver(closeBluetoothReceiver, Filter_Cancel,MainApplication.permission,null);


        loadBody();
        loadDevice();

        setLister();
        initValue();
    }

    private void initValue() {
        String deviceName = MainApplication.getBlueDeviceName();
        if (!StringUtil.isEmptyOrNull(deviceName) && MainApplication.getBlueState()) {
            tv_bluetool_device.setText(deviceName);
        }
    }

    private void setLister() {
        bluetool_lay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showPage(BluetoothSettingActivity.class);
            }
        });
    }

    private void loadDevice() {
        UserManager.queryMchDevice(new UINotifyListener<String>() {
            @Override
            public void onPreExecute() {
                //                showNewLoading(true, getStringById(R.string.public_submitting));
                super.onPreExecute();
            }

            public void onError(Object object) {
                dismissLoading();
                //                if (object != null)
                //                    showToastInfo(object.toString());
            }

            ;

            @Override
            public void onSucceed(String result) {
                dismissLoading();
                super.onSucceed(result);

                //                if (!StringUtil.isEmptyOrNull(result))
                //                {
                //                    tv_device.setText(result);
                //                }
            }
        });
    }

    void loadBody() {
        UserManager.queryBody(new UINotifyListener<GoodsMode>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                //                    showLoading(false, R.string.public_loading);

            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
                dismissLoading();
            }

            @Override
            public void onError(Object object) {
                super.onError(object);
                //                    dismissLoading();
                //                    if (object != null)
                //                        toastDialog(SettingMoreActivity.this, object.toString(), null);
            }

            @Override
            public void onSucceed(GoodsMode object) {
                super.onSucceed(object);
                //                    dismissLoading();

                if (object != null) {
                    tv_body.setText(object.getBody());
                }
            }
        });
    }

    /**
     * 公告小圆点
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void isShowPoint(ImageView iv) {
        String noticePre = PreferenceUtil.getString("noticePre", "");
        Logger.i("hehui", "isShowPoint-->" + noticePre);
        if (!StringUtil.isEmptyOrNull(noticePre)) {
            Map<String, String> mapNotice = JsonUtil.jsonToMap(noticePre);
            for (Map.Entry<String, String> entry : mapNotice.entrySet()) {
                String value = entry.getValue();
                if (value.equals("0")) { //代表还有公开没有被打开查看详情过
                    iv.setVisibility(View.VISIBLE);
                }
            }
        }

    }

    /**
     * 公告小圆点
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void isHidePoint(ImageView iv) {
        boolean tag = true;
        String noticePre = PreferenceUtil.getString("noticePre", "");
        if (!StringUtil.isEmptyOrNull(noticePre)) {
            Map<String, String> mapNotice = JsonUtil.jsonToMap(noticePre);
            for (Map.Entry<String, String> entry : mapNotice.entrySet()) {
                String value = entry.getValue();
                if (value.equals("0")) { //代表还有公开没有被打开查看详情过
                    iv.setVisibility(View.VISIBLE);
                    tag = false;
                }
            }
            if (tag) {
                iv.setVisibility(View.GONE);
            }
        }

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();


    }

    @Override
    protected void onStop() {
        super.onStop();
        HandlerManager.unregisterHandler(HandlerManager.GETSHOPINFODONE, handler);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(updateBluetoothReceiver);
        unregisterReceiver(closeBluetoothReceiver);
    }

    BroadcastReceiver updateBluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Handle the received local broadcast
            String action = intent.getAction();
            if ("action_bluetooth_connect_is_success".equals(action)) {
                String name = MainApplication.getBlueDeviceName();
                if (!StringUtil.isEmptyOrNull(name)) {
                    tv_bluetool_device.setText(name);
                } else {
                    tv_bluetool_device.setText(R.string.tx_print_no_conn);
                }
            }
        }
    };

    BroadcastReceiver closeBluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Handle the received local broadcast
            String action = intent.getAction();
            if ("action_bluetooth_connect_is_cancel".equals(action)) {
                tv_bluetool_device.setText(R.string.tx_print_no_conn);
            }
        }
    };

    private void initViews() {
        setContentView(R.layout.activity_setting_more);
        mContext = this;
        tv_bluetool_device = getViewById(R.id.tv_bluetool_device);
        tv_bluetool_device = getViewById(R.id.tv_bluetool_device);
        bluetool_lay = getViewById(R.id.bluetool_lay);
        tv_device = getViewById(R.id.tv_device);
        //ly_bottom = getViewById(R.id.ly_bottom);
        tv_body = getViewById(R.id.tv_body);
        lay_choise_lanu = getViewById(R.id.lay_choise_lanu);
        lay_choise_lanu.setOnClickListener(this);
        device_lay = getViewById(R.id.device_lay);
        device_view = getViewById(R.id.device_view);

        device_lay.setOnClickListener(this);

        lay_notify = getViewById(R.id.lay_notify);
        lay_notify.setOnClickListener(this);
        iv_point = getViewById(R.id.iv_point);
        //        int noticeSize = PreferenceUtil.getInt("noticeSize", 0);
        //        if (noticeSize == 0)
        //        {
        //            iv_point.setVisibility(View.GONE);
        //        }
        //vi_pay_method = getViewById(R.id.vi_pay_method);
        ly_pay_method = getViewById(R.id.ly_pay_method);
        ly_pay_method.setOnClickListener(this);

        //        if (MainApplication.isAdmin.equals("0") && StringUtil.isEmptyOrNull(MainApplication.device))
        //        {
        //            device_lay.setVisibility(View.GONE);
        //            device_view.setVisibility(View.GONE);
        //        }

        iv_voice = getViewById(R.id.iv_voice);
        iv_voice.setVisibility(View.GONE);
        lay_choise = getViewById(R.id.lay_choise);
        lay_choise.setOnClickListener(this);
        marketingLay = getViewById(R.id.marketingLay);
        lay_prize = getViewById(R.id.lay_prize);
        marketingLay.setOnClickListener(this);
        //marketing_id = getViewById(R.id.marketing_id);
        lay_prize.setOnClickListener(this);
        //view_last = getViewById(R.id.view_last);
        //view_manager = getViewById(R.id.view_manager);
        //ll_shopinfo_id = getViewById(R.id.ll_shopinfo_id);
        //empManager_id = getViewById(R.id.empManager_id);
        //refundLay_id = getViewById(R.id.refundLay_id);
        ll_static_code = getViewById(R.id.ll_static_code);
        ll_static_code.setOnClickListener(this);
        lay_scan = getViewById(R.id.lay_scan);
        lay_scan.setOnClickListener(this);
        tvVersion = getViewById(R.id.tv_version);
        tvVersion.setText("V" + AppHelper.getVerName(mContext));
        //bodyLay_id = getViewById(R.id.bodyLay_id);
        // tvUser = getViewById(R.id.tv_user);

        tv_debug = getViewById(R.id.tv_debug);
        tv_debug.setText((GlobalConstant.isDebug == true ? "(" + getString(R.string.tx_try_version) + ")" : ""));
        tv_debug.setTextColor(Color.BLUE);

        ll_logout = getViewById(R.id.ll_logout);
        ll_logout.setOnClickListener(this);

        empManager = getViewById(R.id.empManager);
        empManager.setOnClickListener(this);

        replaceTel = getViewById(R.id.replaceTel);
        replaceTel.setOnClickListener(this);

        cashier_layout = getViewById(R.id.cashier_layout);
        cashier_layout.setOnClickListener(this);

        textView1 = getViewById(R.id.textView1);
        bodyLay = getViewById(R.id.bodyLay);
        bodyLay.setOnClickListener(this);
        write_off = getViewById(R.id.write_off);
        write_off.setOnClickListener(this);

        String userNmae = sp.getString("user_name", "");
        textView1.setText(getString(R.string.tx_welcome) + userNmae);

        textView2 = getViewById(R.id.textView2);
        if (MainApplication.mchName != null && !MainApplication.mchName.equals("null")) {
            textView2.setText(MainApplication.mchName);
        }

        llChangePwd = getViewById(R.id.ll_changePwd);
        //lineChangePwd = getViewById(R.id.line_changePwd);
        ivNewVersion = getViewById(R.id.iv_newVersion);
        llChangePwd.setOnClickListener(this);

        // llCoupon = getViewById(R.id.ll_coupon);    llShopInfo = getViewById(R.id.ll_shopinfo);
        //        if ("".equals(MainApplication.pwd_tag))
        //        {
        //            llChangePwd.setVisibility(View.GONE);
        //            lineChangePwd.setVisibility(View.GONE);
        //            // llCoupon.setVisibility(View.GONE);
        //            // getViewById(R.id.line_shopinfo).setVisibility(View.GONE);
        //            // llShopInfo.setVisibility(View.GONE);
        //        }

        // llCoupon.setOnClickListener(this);

        checkVersion(true);

        // if (!MainApplication.isRegisterUser)
        // {
        // llChangePwd.setVisibility(View.GONE);
        // // }

        // 收银员 商户资料和收银员管理，退款管理 隐藏
        //        if (MainApplication.isAdmin.equals("0"))
        //        { //收银员隐藏
        //          //            device_lay.setVisibility(View.GONE);
        //          //            device_view.setVisibility(View.GONE);
        //            vi_pay_method.setVisibility(View.GONE);
        //            ly_pay_method.setVisibility(View.GONE);
        //            //            ly_bottom.setVisibility(View.GONE);
        //            bodyLay.setVisibility(View.GONE);
        //            device_view.setVisibility(View.GONE);
        //        }
        if (MainApplication.isAdmin.equals("0")) {
            //            empManager.setVisibility(View.GONE);
            //            llShopInfo.setVisibility(View.GONE);
            //            ll_shopinfo.setVisibility(View.GONE);
            //            bodyLay_id.setVisibility(View.GONE);
            //            device_lay.setVisibility(View.VISIBLE);
            device_view.setVisibility(View.GONE);
            bodyLay.setVisibility(View.GONE);

            //            empManager_id.setVisibility(View.GONE);
            //            ll_shopinfo_id.setVisibility(View.GONE);
            //            if (!MainApplication.isRefundAuth.equals("") && MainApplication.isRefundAuth.equals("1"))
            //            {
            //                //                refundLay.setVisibility(View.VISIBLE);
            //                //                refundLay_id.setVisibility(View.GONE);
            //            }
            //            else
            //            {
            //                refundLay_id.setVisibility(View.GONE);
            //                refundLay.setVisibility(View.GONE);
            //                view_manager.setVisibility(View.GONE);
            //                view_last.setVisibility(View.GONE);
            //            }
            //            //如果有活动权限则显示
            //            if (MainApplication.isActivityAuth == 0)
            //            {
            //                marketingLay.setVisibility(View.GONE);
            //                marketing_id.setVisibility(View.GONE);
            //            }
        }

        String voice = PreferenceUtil.getString("voice", "close");
        if (voice.equals("open")) {
            iv_voice.setImageResource(R.drawable.button_configure_switch_default);
        } else {
            iv_voice.setImageResource(R.drawable.button_configure_switch_close);
        }
        //        if (MainApplication.payMethodSize == 0 || MainApplication.payMethodSize == 1)
        //        {
        //            vi_pay_method.setVisibility(View.GONE);
        //            ly_pay_method.setVisibility(View.GONE);
        //        }

        /**
         * 可配置设置
         */
        DynModel dynModel = (DynModel) SharedPreUtile.readProduct("dynModel" + ApiConstant.bankCode);
        if (null != dynModel) {
            try {
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonColor())) {
                    ll_logout.setBackgroundColor(Color.parseColor(dynModel.getButtonColor()));
                }

                if (!StringUtil.isEmptyOrNull(dynModel.getButtonFontColor())) {
                    ll_logout.setTextColor(Color.parseColor(dynModel.getButtonFontColor()));
                }
            } catch (Exception e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }
        }

        if (MainApplication.IS_POS_VERSION) {
            bluetool_lay.setVisibility(View.GONE);
            lay_choise.setVisibility(View.GONE);
        }
    }

    /**
     * 退出
     */
    public void onExit(View v) {
        showExitDialog(SettingMoreActivity.this);
    }


    /**
     * 修改密码
     */
    public void onChangePwd() {
        ChanagePwdActivity.startActivity(mContext);
    }

    /**
     * 检查版本
     */
    public void onCheckVersion(View v) {

        // 进行网络判断
        if (!NetworkUtils.isNetworkAvailable(this)) {
            showToastInfo(R.string.network_exception);
            return;
        }
        // 检查过了 有数据了直接跳出询问用户是否需要升级 暂时屏蔽掉
        if (upVerInfo != null) {
            upVerInfo.isMoreSetting = true;
            showUpgradeInfoDialog(upVerInfo, new ComDialogListener(upVerInfo));
        } else {
            checkVersion(false);
        }
    }

    /**
     * 进入帮助页
     */
    public void onHelpPage(View v) {
        ContentTextActivity.startActivity(mContext, ApiConstant.BASE_URL_PORT + "spay/question?tel=" + getString(R.string.qustion_company_phone), R.string.title_common_question);

        //        String language = PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN);
        //        
        //        Locale locale = getResources().getConfiguration().locale; //zh-rHK
        //        String lan = locale.getCountry();
        //        
        //        if (!TextUtils.isEmpty(language))
        //        {
        //            
        //            if (language.equals(MainApplication.LANG_CODE_ZH_TW))
        //            {
        //                ContentTextActivity.startActivity(mContext, ApiConstant.BASE_URL_PORT
        //                    + "spay/public/question_zh_tw.html", R.string.title_common_question);
        //            }
        //            else
        //            {
        //                ContentTextActivity.startActivity(mContext, ApiConstant.BASE_URL_PORT
        //                    + "spay/public/question_zh_cn.html", R.string.title_common_question);
        //            }
        //            
        //        }
        //        else
        //        {
        //            if (lan.equalsIgnoreCase("CN"))
        //            {
        //                ContentTextActivity.startActivity(mContext, ApiConstant.BASE_URL_PORT
        //                    + "spay/public/question_zh_cn.html", R.string.title_common_question);
        //            }
        //            else
        //            {
        //                ContentTextActivity.startActivity(mContext, ApiConstant.BASE_URL_PORT
        //                    + "spay/public/question_zh_tw.html", R.string.title_common_question);
        //            }
        //        }

    }

    private UpgradeInfo upVerInfo;

    /**
     * 检查版本显示提示图标
     */
    public void checkVersion(final boolean isDisplayIcon) {
        // 不用太平凡的检测升级 所以使用时间间隔区分
        UpgradeManager.getInstance().getVersonCode(new UINotifyListener<UpgradeInfo>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                // titleBar.setRightLodingVisible(true);
                showLoading(false, R.string.show_new_version_loading);
            }

            @Override
            public void onError(Object object) {
                super.onError(object);
                dismissLoading();
            }

            @Override
            public void onSucceed(UpgradeInfo result) {
                super.onSucceed(result);
                dismissLoading();
                if (null != result) {
                    if (isDisplayIcon) {
                        upVerInfo = result;
                        ivNewVersion.setVisibility(View.VISIBLE);
                    } else {
                        result.isMoreSetting = true;
                        showUpgradeInfoDialog(result, new ComDialogListener(result));
                    }
                } else {
                    if (!isDisplayIcon) {
                        showToastInfo(R.string.show_no_version);
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lay_choise_lanu:
                //语音切换
                LanguageDialogInfo dialog = new LanguageDialogInfo(SettingMoreActivity.this, new LanguageDialogInfo.HandleBtn() {

                    @Override
                    public void handleOkBtn(Boolean lang) {
                        if (lang) { // 中文
                            switchLanguage(MainApplication.LANG_CODE_ZH_CN); // 中文
                        } else {//繁体
                            switchLanguage(MainApplication.LANG_CODE_ZH_TW);
                        }
                        finish();

                        Intent it = new Intent();
                        it.setClass(SettingMoreActivity.this, spayMainTabActivity.class);
                        startActivity(it);
//                        MainActivity.startActivity(SettingMoreActivity.this, "SettingActivity");
                        LocalAccountManager.getInstance().onDestory();
                    }

                }, new LanguageDialogInfo.HandleBtnCancle() {

                    @Override
                    public void handleCancleBtn() {
                        // 繁体字
                        //                            switchLanguage(MainApplication.LANG_CODE_ZH_TW);
                        //                            finish();
                        //                            //                                LoginActivity.startActivity(mContext);
                        //                            MainActivity.startActivity(SettingActivity.this, "SettingActivity");
                        //                            LocalAccountManager.getInstance().onDestory();

                    }

                });
                DialogHelper.resize(SettingMoreActivity.this, dialog);
                dialog.show();
                break;

            // 公告
            case R.id.lay_notify:

                break;

            case R.id.device_lay: //语言切换

                //                showPage(DeviceAddActivity.class);
                showPage(SettingSwitchLanActivity.class);
                break;

            //设置默认支付方式
            //            case R.id.ly_pay_method:
            //                showPage(SetPayMethodActivity.class);
            //                
            //                break;

            //语音播报
            case R.id.lay_choise:
                String voice = PreferenceUtil.getString("voice", "close");
                if (voice.equals("open")) {
                    PreferenceUtil.commitString("voice", "close");
                    iv_voice.setImageResource(R.drawable.button_configure_switch_close);
                } else {
                    iv_voice.setImageResource(R.drawable.button_configure_switch_default);
                    PreferenceUtil.commitString("voice", "open");
                }

                //                showPage(SettingVoiceActivity.class);

                break;
            //营销规则
            case R.id.marketingLay:

                break;

            // 历史排名榜
            case R.id.lay_prize:

                break;
            //固定二维码收款
            case R.id.ll_static_code:
                if (null != MainApplication.serviceType && MainApplication.serviceType.contains(MainApplication.PAY_WX_SJPAY)) {
                    showPage(StaticCodeActivity.class);
                } else {
                    showToastInfo(R.string.show_static_code);
                }
                break;

            // 扫一扫退款
            case R.id.lay_scan:
                CaptureActivity.startActivity(SettingMoreActivity.this, MainApplication.PAY_TYPE_REFUND);
                break;
            //卡券核销
            case R.id.write_off:
                CaptureActivity.startActivity(SettingMoreActivity.this, MainApplication.PAY_TYPE_WRITE_OFF);
                break;

            case R.id.bodyLay://修改商品名称
                if (MainApplication.isAdmin.equals("1")) {
                    if (null == MainApplication.body) {
                        showPage(GoodsNameSetting.class);
                        return;
                    }

                    UserManager.queryBody(new UINotifyListener<GoodsMode>() {
                        @Override
                        public void onPreExecute() {
                            super.onPreExecute();
                            showLoading(false, R.string.public_loading);

                        }

                        @Override
                        public void onPostExecute() {
                            super.onPostExecute();
                            dismissLoading();
                        }

                        @Override
                        public void onError(Object object) {
                            super.onError(object);
                            dismissLoading();
                            if (object != null) showPage(GoodsNameSetting.class);
                            //                                toastDialog(SettingMoreActivity.this, object.toString(), null);
                        }

                        @Override
                        public void onSucceed(GoodsMode object) {
                            super.onSucceed(object);
                            dismissLoading();

                            if (object != null) {
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("body", object);
                                showPage(GoodsNameSetting.class, bundle);
                            } else {
                                showPage(GoodsNameSetting.class);
                            }
                        }
                    });
                }
                //                else
                //                {
                //                    showToastInfo(R.string.show_mch_body);
                //                }

                break;
            case R.id.ll_logout:
                try {
                    StatService.trackCustomEvent(mContext, "kMTASPayMeLogout", "安全退出入口");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                DialogInfo dialogInfo = new DialogInfo(SettingMoreActivity.this, getString(R.string.public_cozy_prompt), getString(R.string.show_sign_out), getString(R.string.btnOk), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

                    @Override
                    public void handleOkBtn() {
                        //                                finish();
                        //                                MainApplication.getContext().exit();
                        //                                System.exit(0);

                        deleteSharedPre();

                        Intent it = new Intent();
                        it.setClass(getApplicationContext(), WelcomeActivity.class);
                        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(it);

                    /*    LoginActivity.startActivity(mContext);*/
                        LocalAccountManager.getInstance().onDestory();

                        //                            CleanManager.cleanSharedPreference(SettingMoreActivity.this, "login");

                        Editor editor = sp.edit().remove("userPwd");

                        //                            editor.clear();
                        editor.commit();
                        for (Activity a : MainApplication.allActivities) {
                            a.finish();
                        }

                        PreferenceUtil.removeKey("login_skey");
                        PreferenceUtil.removeKey("login_sauthid");

                        ApiConstant.BASE_URL_PORT = MainApplication.config.getServerAddr();
                        ApiConstant.pushMoneyUrl = MainApplication.config.getPushMoneyUrl();
                        PreferenceUtil.removeKey("serverCifg");


                        finish();
                    }

                    @Override
                    public void handleCancleBtn() {
                        //                                dialogInfo.cancel();
                    }
                }, null);

                DialogHelper.resize(SettingMoreActivity.this, dialogInfo);
                dialogInfo.show();

                break;
            case R.id.ll_changePwd:

                onChangePwd();

                break;
            case R.id.replaceTel:

                showPage(ReplactTelActivity.class);

                break;

            case R.id.cashier_layout:

                showPage(CashierDeskSettingActivity.class);

                break;

            case R.id.empManager:

                if (MainApplication.isAdmin.equals("1") && MainApplication.remark.equals("1")) {
                    showPage(CashierManager.class);
                } else {
                    showToastInfo(R.string.show_mch_user);
                }

                break;

        }
    }


    /**
     * 升级点击事件
     */
    class ComDialogListener implements CommonConfirmDialog.ConfirmListener {
        private UpgradeInfo result;

        public ComDialogListener(UpgradeInfo result) {
            this.result = result;
        }

        @Override
        public void ok() {
            new UpgradeDailog(SettingMoreActivity.this, result, new UpgradeDailog.UpdateListener() {
                @Override
                public void cancel() {

                }
            }).show();
        }

        @Override
        public void cancel() {
            PreferenceUtil.commitInt("update", result.version); // 保存更新，下次进来不更新
        }

    }

    MerchantTempDataModel merchantInfo = null;

    boolean isAuth = false;

    public void existAuth() {
        UserManager.existAuth(MainApplication.userId, 2, new UINotifyListener<Integer>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            public void onError(Object object) {
                super.onError(object);
                if (object != null) {
                    showToastInfo(object.toString());
                }
            }

            @Override
            public void onSucceed(Integer result) {
                super.onSucceed(result);
                dismissMyLoading();

                switch (result) {
                    // 资料未审核
                    case 4:
                        isAuth = false;
                        break;
                    // 资料已审核
                    case 5:
                        isAuth = true;
                        break;
                }

            }
        });
    }


    public void changeRefundPwd(View view) {
        TuiKuanPwdSettingActivity.startActivity(mContext);
    }

    @Override
    protected boolean isLoginRequired() {
        return false;
    }
}