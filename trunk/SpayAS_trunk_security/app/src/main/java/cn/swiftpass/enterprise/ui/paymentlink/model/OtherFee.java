package cn.swiftpass.enterprise.ui.paymentlink.model;

import android.text.TextUtils;

import java.io.Serializable;

/**
 * Created by congwei.li on 2021/9/17.
 *
 * @Description: 附加费用
 */
public class OtherFee implements Serializable {
    public String orderCostKeyId;
    public String feeType;
    public String extraCostDesc;
    public String createTime;
    public String updateTime;
    public String extraCost;
    private boolean isDeleteShow;

    public boolean isDeleteShow() {
        return isDeleteShow;
    }

    public void setDeleteShow(boolean deleteShow) {
        isDeleteShow = deleteShow;
    }

    public boolean isAllFull() {
        return !TextUtils.isEmpty(extraCostDesc) && !TextUtils.isEmpty(extraCost);
    }

    @Override
    public String toString() {
        return "OtherFee{" +
                "orderCostKeyId='" + orderCostKeyId + '\'' +
                ", feeType='" + feeType + '\'' +
                ", extraCostDesc='" + extraCostDesc + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", extraCost='" + extraCost + '\'' +
                ", isDeleteShow=" + isDeleteShow +
                '}';
    }
}
