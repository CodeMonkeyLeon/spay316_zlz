package cn.swiftpass.enterprise;


import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.FrameLayout;

import butterknife.BindView;
import cn.swiftpass.enterprise.base.BaseActivity;
import cn.swiftpass.enterprise.newspay.bill.view.BillFragment;
import cn.swiftpass.enterprise.newspay.bill.view.SummaryFragment;
import cn.swiftpass.enterprise.newspay.myinfo.view.MyInfoFragment;
import cn.swiftpass.enterprise.newspay.receipt.view.ReceiptFragment;
import cn.swiftpass.enterprise.widget.BottomBar;
import cn.swiftpass.enterprise.widget.BottomBarTab;

public class MainActivity extends BaseActivity {

    @BindView(R.id.bottombar)
    BottomBar mBottomBar;
    @BindView(R.id.fl_fragmengt)
    FrameLayout fl_fragmengt;
    private ReceiptFragment mReceiptFragment;
    private MyInfoFragment mMyInfoFragment;
    private SummaryFragment mSummaryFragment;
    private BillFragment mBillFragment;
    /**
     * 标记当前显示的Fragment
     */
    private int fragmentId = 0;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //通过onSaveInstanceState方法保存当前显示的fragment
        outState.putInt("fragment_id",fragmentId);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        FragmentManager mFragmentManager = getSupportFragmentManager();
        //通过FragmentManager获取保存在FragmentTransaction中的Fragment实例
        mReceiptFragment = (ReceiptFragment) mFragmentManager
                .findFragmentByTag("ReceiptFragment");
        mBillFragment = (BillFragment) mFragmentManager
                .findFragmentByTag("BillFragment");
        mSummaryFragment = (SummaryFragment)mFragmentManager
                .findFragmentByTag("SummaryFragment");
        mMyInfoFragment = (MyInfoFragment)mFragmentManager
                .findFragmentByTag("MyInfoFragment");
        //恢复销毁前显示的Fragment
        setFragment(savedInstanceState.getInt("fragment_id"));
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        //根据传入的Bundle对象判断Activity是正常启动还是销毁重建
        if(savedInstanceState == null){
            //设置第一个Fragment默认选中
            setFragment(0);
        }
        mBottomBar
                .addItem(new BottomBarTab(this, R.mipmap.icon_tab_charge_nor, getString(R.string.tab_pay_title)))
                .addItem(new BottomBarTab(this, R.mipmap.icon_tab_transaction_nor, getString(R.string.tab_bill_title)))
                .addItem(new BottomBarTab(this, R.mipmap.icon_summary_nor, getString(R.string.tab_sum_title)))
                .addItem(new BottomBarTab(this, R.mipmap.icon_tab_me_nor, getString(R.string.tab_setting_title)));
        initData();
    }

    private void initData() {

        mBottomBar.setOnTabSelectedListener(new BottomBar.OnTabSelectedListener() {
            @Override
            public void onTabSelected(int position, int prePosition) {
                setFragment(position);
            }

            @Override
            public void onTabUnselected(int position) {

            }

            @Override
            public void onTabReselected(int position) {

            }
        });
    }

    private void setFragment(int index){
        //获取Fragment管理器
        FragmentManager mFragmentManager = getSupportFragmentManager();
        //开启事务
        FragmentTransaction mTransaction = mFragmentManager.beginTransaction();
        //隐藏所有Fragment
        hideFragments(mTransaction);
        switch (index){
            default:
                break;
            case 0:
                //显示对应Fragment
                if(mReceiptFragment == null){
                    mReceiptFragment = new ReceiptFragment();
                    mTransaction.add(R.id.fl_fragmengt, mReceiptFragment,
                            "ReceiptFragment");
                }else {
                    mTransaction.show(mReceiptFragment);
                }
                break;
            case 1:
                if(mBillFragment == null){
                    mBillFragment = new BillFragment();
                    mTransaction.add(R.id.fl_fragmengt, mBillFragment,
                            "BillFragment");
                }else {
                    mTransaction.show(mBillFragment);
                }
                break;
            case 2:
                if(mSummaryFragment == null){
                    mSummaryFragment = new SummaryFragment();
                    mTransaction.add(R.id.fl_fragmengt, mSummaryFragment,
                            "SummaryFragment");
                }else {
                    mTransaction.show(mSummaryFragment);
                }
                break;
            case 3:
                if(mMyInfoFragment == null){
                    mMyInfoFragment = new MyInfoFragment();
                    mTransaction.add(R.id.fl_fragmengt, mMyInfoFragment,
                            "MyInfoFragment");
                }else {
                    mTransaction.show(mMyInfoFragment);
                }
                break;
        }
        //提交事务
        mTransaction.commit();
    }

    private void hideFragments(FragmentTransaction transaction){
        if(mReceiptFragment != null){
            //隐藏Fragment
            transaction.hide(mReceiptFragment);
        }
        if(mBillFragment != null){
            transaction.hide(mBillFragment);
        }
        if(mSummaryFragment != null){
            transaction.hide(mSummaryFragment);
        }
        if(mMyInfoFragment != null){
            transaction.hide(mMyInfoFragment);
        }
    }

}
