package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.MasterCardInfo
import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.bussiness.model.QRcodeInfo
import cn.swiftpass.enterprise.bussiness.model.WalletListBean
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @世界小得像一条街的布景
 * @我们相遇了
 * @你点点头
 * @省略了所有的往事
 * @省略了问候。
 */
class MasterCardQrCodeContract {


    interface View : BaseView {

        fun unifiedNativePaySuccess(
            response: QRcodeInfo?,
            type: String?,
            isInstapay: Boolean,
            isSecond: Boolean,
            isInstapayQRPayment: Boolean,
            walletBean: WalletListBean?
        )


        fun unifiedNativePayFailed(error: Any?)


        fun masterCardNativePaySuccess(
            response: MasterCardInfo?,
            apiCode: String?
        )

        fun masterCardNativePayFailed(error: Any?)


        fun queryMasterCardOrderByOrderNoSuccess(response: Order?)


        fun queryMasterCardOrderByOrderNoFailed(error: Any?)

        fun queryOrderByOrderNoSuccess(response: Order?)

        fun queryOrderByOrderNoFailed(error: Any?)

    }


    interface Presenter : BasePresenter<View> {


        fun queryOrderByOrderNo(orderNo: String?)


        fun queryMasterCardOrderByOrderNo(
            service: String?,
            outTradeNo: String?
        )


        fun masterCardNativePay(
            money: String?,
            apiCode: String?,
            attach: String?
        )


        fun unifiedNativePay(
            money: String?,
            payType: String?,
            traType: String?,
            adm: Long,
            outTradeNo: String?,
            note: String?,
            isInstapay: Boolean,
            isSecond: Boolean,
            isInstapayQRPayment: Boolean,
            walletBean: WalletListBean?
        )
    }


}