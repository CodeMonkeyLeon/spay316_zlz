package cn.swiftpass.enterprise.utils.permissionutils;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Build;
import android.view.KeyEvent;

import java.util.Arrays;
import java.util.List;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * Created by congwei.li on 2022/5/20.
 *
 * @Description:
 */
public class PermissionDialogUtils {

    /**
     * 请求系统授权权限
     *
     * @param activity 当前activity
     * @param callback 授权结果回调
     */
    public static void requestPermission(Activity activity, String[] permissionGroup,
                                         boolean isNeedFinish, OnPermissionCallback callback) {
        if (null == activity) {
            return;
        }
        PermissionsManage.with(activity)
                .permission(permissionGroup)
                .request(new OnPermissionCallback() {
                    @Override
                    public void onGranted(List<String> permissions, boolean all) {
                        if (null == callback) {
                            return;
                        }
                        if (all) {
                            callback.onGranted(permissions, all);
                        } else {
                            noticePermission(activity, permissionGroup, isNeedFinish);
                        }
                    }

                    @Override
                    public void onDenied(List<String> permissions, boolean never) {
                        if (null == callback) {
                            return;
                        }
                        noticePermission(activity, permissionGroup, isNeedFinish);
                    }
                });
    }

    /**
     * 弹窗提示需要获取权限
     */
    public static void noticePermission(Activity activity, String[] permissionGroup,
                                        boolean isNeedFinish) {
        if (null == activity) {
            return;
        }

        String str = String.format(activity.getString(R.string.setting_permission_notice_content),
                getPermissionText(activity, permissionGroup));
        DialogInfo dialogInfo = new DialogInfo(activity,
                activity.getString(R.string.setting_permission),
                str, activity.getString(R.string.setting_permission_to_sys),
                activity.getString(R.string.setting_permission_cancel),
                DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                PermissionsManage.startPermissionActivity(activity, permissionGroup);
            }

            @Override
            public void handleCancelBtn() {
                if (!activity.isFinishing()) {
                    finishWithoutPermission(activity, permissionGroup, isNeedFinish);
                }
            }
        }, null);
        dialogInfo.setOnKeyListener(new DialogInterface.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {
                return keycode == KeyEvent.KEYCODE_BACK;
            }
        });
        DialogHelper.resize(activity, dialogInfo);
        dialogInfo.setContentColor(activity.getResources().getColor(R.color.black));
        dialogInfo.setBtnOkTextColor(activity.getResources().getColor(R.color.bg_text_new));
        dialogInfo.show();
    }

    /**
     * 权限未授权时退出当前页面
     */
    public static void finishWithoutPermission(Activity activity, String[] permissionGroup,
                                               boolean isNeedFinish) {
        if (null == activity) {
            return;
        }
        String str = String.format(activity.getString(R.string.setting_permission_not_auth),
                getPermissionText(activity, permissionGroup));
        ToastHelper.showInfo(str);
        if (isNeedFinish && !activity.isFinishing()) {
            activity.finish();
        }
    }

    /**
     * 获取当前授权文案
     */
    public static String getPermissionText(Activity activity, String[] permissionGroup) {
        String temp = "";


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            //android 13
            if (equalityOfArrays(permissionGroup, Permission.Group.STORAGE_API_33)) {
                temp = activity.getString(R.string.setting_permission_storage);
            }
            if (equalityOfArrays(permissionGroup, Permission.Group.ACCESS_API_33)) {
                temp = activity.getString(R.string.setting_permission_location);
            }
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            //android 12
            if (equalityOfArrays(permissionGroup, Permission.Group.BLUETOOTH_API_31)) {
                temp = activity.getString(R.string.setting_permission_bluetooth);
            }
            if (equalityOfArrays(permissionGroup, Permission.Group.ACCESS_API_31)) {
                temp = activity.getString(R.string.setting_permission_location);
            }
        } else {
            if (equalityOfArrays(permissionGroup, Permission.Group.STORAGE)) {
                temp = activity.getString(R.string.setting_permission_storage);
            }
            if (equalityOfArrays(permissionGroup, Permission.Group.BLUETOOTH)) {
                temp = activity.getString(R.string.setting_permission_bluetooth);
            }
            if (equalityOfArrays(permissionGroup, Permission.Group.ACCESS)) {
                temp = activity.getString(R.string.setting_permission_location);
            }
        }


        if (equalityOfArrays(permissionGroup, Permission.Group.CAMERA)) {
            temp = activity.getString(R.string.setting_permission_camera);
        }

        return temp;
    }

    public static boolean equalityOfArrays(String[] arr1, String[] arr2) {
        if (arr1.length != arr2.length) {
            return false;
        }
        Arrays.sort(arr1);
        Arrays.sort(arr2);
        return Arrays.equals(arr1, arr2);
    }
}
