package com.example.common.sentry

import android.text.TextUtils
import com.example.common.sp.PreferenceUtil
import com.google.gson.JsonObject
//import cn.swiftpass.enterprise.MainApplication
//import cn.swiftpass.enterprise.utils.PreferenceUtil
//import com.google.gson.JsonObject
import java.io.Serializable

/**
 * 需要上传给Sentry平台的自定义参数
 *
 * apiName  接口名
 * infoNumber  脱敏商户号/收银员登录账号
 * infoName  脱敏商户名称/收银员用户名
 * errorMsg  错误信息
 * errorCode  错误码 --- 业务报错code（400、401、402、403、405等）网络报错（HTTP400等）
 * exception  系统的错误类信息
 */
class SentryInfoData : Serializable {

    var apiName: String? = ""
    var infoNumber: String? = ""
    var infoName: String? = ""
    var errorMsg: String? = ""
    var errorCode: String? = ""
    var exception: String? = ""

    companion object {
        const val PARAM_API_NAME = "apiName"
        const val PARAM_INFO_NUMBER = "infoNumber"
        const val PARAM_INFO_NAME = "infoName"
        const val PARAM_ERROR_MSG = "errorMsg"
        const val PARAM_ERROR_CODE = "errorCode"
        const val PARAM_EXCEPTION = "exception"
    }


    constructor(
        apiName: String?,
        infoNumber: String?,
        infoName: String?,
        errorMsg: String?,
        errorCode: String?,
        exception: String?
    ) {
        this.apiName = apiName
        this.infoNumber = infoNumber
        this.infoName = infoName
        this.errorMsg = errorMsg
        this.errorCode = errorCode
        this.exception = exception
    }


    /**
     * 接口异常
     */
    constructor(
        apiName: String?,
        errorMsg: String?,
        errorCode: String?,
        exception: String?
    ) {
        this.apiName = apiName
        if (PreferenceUtil.isSPNull()) {
            this.infoNumber = ""
            this.infoName = ""
        } else {
            this.infoNumber = PreferenceUtil.getString("mchId", "")
            this.infoName = PreferenceUtil.getString("mchName", "")
        }
        this.errorMsg = errorMsg
        this.errorCode = errorCode
        this.exception = exception
    }


    /**
     * try catch 异常信息
     */
    constructor(
        apiName: String?,
        exception: String?,
        errorMsg: String?
    ) {
        this.apiName = apiName
        if (PreferenceUtil.isSPNull()) {
            this.infoNumber = ""
            this.infoName = ""
        } else {
            this.infoNumber = PreferenceUtil.getString("mchId", "")
            this.infoName = PreferenceUtil.getString("mchName", "")
        }
        this.errorMsg = errorMsg
        this.errorCode = ""
        this.exception = exception
    }


    fun toJson(): JsonObject {
        val json = JsonObject()
        //接口名
        json.addProperty(PARAM_API_NAME, apiName)
        //商户号
        if (TextUtils.isEmpty(infoNumber)) {
            json.addProperty(PARAM_INFO_NUMBER, "")
        } else {
            json.addProperty(PARAM_INFO_NUMBER, merchIdNameDesensitization(infoNumber))
        }
        //商户名称
        if (TextUtils.isEmpty(infoName)) {
            json.addProperty(PARAM_INFO_NAME, "")
        } else {
            json.addProperty(PARAM_INFO_NAME, merchIdNameDesensitization(infoName))
        }
        //错误信息
        json.addProperty(PARAM_ERROR_MSG, errorMsg)
        //错误码
        json.addProperty(PARAM_ERROR_CODE, errorCode)
        //系统的错误类信息
        json.addProperty(PARAM_EXCEPTION, exception)

        return json
    }


    /**
     * 商户号商户名脱敏
     * 1位：明文传
     * 2位：脱敏第一位
     * 3位及以上： 1/3（四舍五入）取中间值脱敏，  如果number % 3 > 0   则脱敏位数是：(int)（number/3）+1
     */
    private fun merchIdNameDesensitization(merchIdName: String?): String? {
        merchIdName?.let {
            when (merchIdName.length) {
                1 -> {
                    return merchIdName
                }
                2 -> {
                    return StringBuilder(merchIdName).replace(0, 1, "*").toString()
                }
                else -> {
                    val desensitizationSize =
                        if (merchIdName.length % 3 == 0) merchIdName.length / 3 else merchIdName.length / 3 + 1
                    val plaintextSize = merchIdName.length - desensitizationSize
                    val startPos =
                        if (plaintextSize % 2 == 0) plaintextSize / 2 else plaintextSize / 2 + 1
                    val endPos = startPos + desensitizationSize

                    var ciphertext = ""
                    for (i in 0 until desensitizationSize) {
                        ciphertext += "*"
                    }
                    return StringBuilder(merchIdName).replace(startPos, endPos, ciphertext)
                        .toString()
                }
            }
        }
        return merchIdName
    }

}
