package com.bigkoo.pickerview.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by jinfangmei on 2015/12/24.
 */
public class ShapreUtils {
    private static final String PREFS_NAME = "time";
    // =============================SharedPreferences=======================================
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    public ShapreUtils(Context context){
         settings = context.getApplicationContext().getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
         editor = settings.edit();
    }

    public void setSettings(SharedPreferences settings) {
        this.settings = settings;
    }

    /**
     * 提交String类型的值
     * @param key
     * @param value
     * @return
     */
    private boolean putCommint(String key, String value) {
        if (null != editor) {
            editor.putString(key, value);
            return editor.commit();
        }
        return false;
    }

    /**
     * 提交long类型的值
     * @param key
     * @param value
     * @return
     */
    private boolean putLongCommit(String key, long value) {
        if (null != editor) {
            editor.putLong(key, value);
            return editor.commit();
        }
        return false;
    }

    /**
     * 提交布尔类型数据
     * @param key
     * @param value
     * @return
     */
    private boolean putBooleanCommit(String key, boolean value) {
        if (null != editor) {
            editor.putBoolean(key, false);
            return editor.commit();
        }
        return false;
    }

    /**
     * 获取String类型的值
     * @param key
     * @return
     */
    private String getString(String key) {
        if (null != settings) {
            return settings.getString(key, "");
        }
        return "";
    }

    /**
     * 获取布尔类型值
     * @param key
     * @return
     */
    private boolean getBoolean(String key) {
        if (null != settings) {
            return settings.getBoolean(key, false);
        }
        return false;
    }

    /**
     * 获取布尔类型值
     * @param key
     * @return
     */
    private long getLong(String key) {
        if (null != settings) {
            return settings.getLong(key, 0l);
        }
        return 0l;
    }

    public void clear() {
        editor.clear().commit();
    }

    private String year="";
    private String month="";
    private String day="";
    private String hours="";
    private String min="";
    private String seconds="";
    private boolean isSelect = false;
    private long fromtime = 0l;
    private long totime = 0l;

    public void setFromtime(long fromtime) {
        this.fromtime = fromtime;
        putLongCommit("fromtime",fromtime);
    }

    public void setTotime(long totime) {
        this.totime = totime;
        putLongCommit("totime",totime);
    }

    public long getFromtime() {
        this.fromtime = getLong("fromtime");
        return fromtime;
    }

    public long getTotime() {
        this.totime = getLong("totime");
        return totime;
    }

    public void setSelect(boolean select) {
        this.isSelect = select;
        putBooleanCommit("select", isSelect);
    }

    public boolean isSelect() {
        this.isSelect = getBoolean("select");
        return isSelect;
    }
    public void setYear(String year) {
        this.year = year;
        putCommint("year", year);
    }
    public void setMonth(String month) {
        this.month = month;
        putCommint("month", month);
    }

    public void setDay(String day) {
        this.day = day;
        putCommint("day", day);
    }

    public void setHours(String hours) {
        this.hours = hours;
        putCommint("hours", hours);
    }

    public void setMin(String min) {
        this.min = min;
        putCommint("min", min);
    }


    public void setSeconds(String current_second) {
        this.seconds = current_second;
        putCommint("seconds", seconds);
    }

    public String getYear() {
        this.year = getString("year");
        return year;
    }
    public String getMonth() {
        this.month = getString("month");
        return month;
    }

    public String getDay() {
        this.day = getString("day");
        return day;
    }

    public String getHours() {
        this.hours = getString("hours");
        return hours;
    }

    public String getMin() {
        this.min = getString("min");
        return min;
    }

    public String getSeconds() {
        this.seconds = getString("seconds");
        return seconds;
    }
}
