package cn.swiftpass.enterprise.mvp.annotation


/**
 * @author lizheng.zhao
 * @date 2023/01/09
 *
 * @一眨眼
 * @算不算少年
 * @一辈子
 * @算不算永远
 */
@MustBeDocumented
@Target(AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.FUNCTION)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class IsNeedSpayId(val value: Boolean)
