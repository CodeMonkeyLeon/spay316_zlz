package cn.swiftpass.enterprise.utils;

import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

import cn.swiftpass.enterprise.MainApplication;

/**
 * Created by aijingya on 2019/7/11.
 *
 * @Package cn.swiftpass.enterprise.utils
 * @Description: ${TODO}(Google firebase analysis 统计打点工具类)
 * @date 2019/7/11.11:07.
 */
public class DataReportUtils {
    private static volatile DataReportUtils instance;
    private static FirebaseAnalytics mFirebaseAnalytics;

    public static DataReportUtils getInstance() {
        if (instance == null) {
            instance = new DataReportUtils();
        }
        return instance;
    }

    private DataReportUtils() {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(MainApplication.getContext());
    }

    /**
     * 埋点
     *
     * @param key
     * @param value
     */
    public void report(String key, Bundle value) {
        if (value == null) {
            value = new Bundle();
        }
        mFirebaseAnalytics.logEvent(key, value);
    }

    /**
     * 埋点
     *
     * @param key
     */
    public void report(String key) {
        report(key, null);
    }

}
