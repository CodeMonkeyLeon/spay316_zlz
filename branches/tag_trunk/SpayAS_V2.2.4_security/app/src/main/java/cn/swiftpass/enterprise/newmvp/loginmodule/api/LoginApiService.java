package cn.swiftpass.enterprise.newmvp.loginmodule.api;


import cn.swiftpass.enterprise.newmvp.base.BaseBean;
import cn.swiftpass.enterprise.newmvp.loginmodule.model.bean.ExchangeKeyBean;
import cn.swiftpass.enterprise.newmvp.loginmodule.model.bean.LoginBean;
import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface LoginApiService {

    /**
     * 密码登录
     * @param strJson
     * @param currentTime
     * @return
     */
    @FormUrlEncoded
    @POST("spay/user/spayLoginWithCodeV2")
    Observable<BaseBean<LoginBean>> login(@Field("data") String strJson,
                                          @Header("spayRs") String currentTime);


    /**
     * 跟服务器交换秘钥
     *
     * @param strJson
     * @param currentTime
     * @return
     */
    @FormUrlEncoded
    @POST("/spay/key/pKeyExchange")
    Observable<BaseBean<ExchangeKeyBean>> exchangeServiceKey(@Field("data") String strJson,
                                                             @Header("spayRs") String currentTime);

}
