/*
 * 文 件 名:  WalletMainActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-1
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.wallet;

import java.io.Serializable;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.wallet.WalletManager;
import cn.swiftpass.enterprise.bussiness.model.WalletModel;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.SelectPicPopupWindow;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 开通电子钱包 下一步
 * 
 * @author  he_hui
 * @version  [版本号, 2016-11-1]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class WalletOpenNextActivity extends TemplateActivity
{
    
    private Button btn_next_step;
    
    private TextView tx_peop, tv_bank_name;
    
    private EditText et_tel;
    
    private ImageView iv_phone, iv_choice_bank, iv_clean_input;
    
    private List<WalletModel> model;
    
    private String bankNum; //银行卡号
    
    private SelectPicPopupWindow pop = null;
    
    private RelativeLayout ly_chioce_bank;
    
    private WalletModel walletModel;
    
    private TextView tv_bank_list;
    
    public static void startActivity(Context context, List<WalletModel> model)
    {
        Intent it = new Intent();
        it.setClass(context, WalletOpenNextActivity.class);
        it.putExtra("walletModel", (Serializable)model);
        context.startActivity(it);
    }
    
    public static void startActivity(Context context, WalletModel model)
    {
        Intent it = new Intent();
        it.setClass(context, WalletOpenNextActivity.class);
        it.putExtra("walletModel", model);
        context.startActivity(it);
    }
    
    @Override
    protected void onPause()
    {
        super.onPause();
        if (null != btn_next_step)
        {
            //            btn_next_step.setText(R.string.reg_next_step);
            //            btn_next_step.setEnabled(true);
            setButtonBg(btn_next_step, true, R.string.reg_next_step);
        }
    }
    
    void loadDate()
    {
        WalletManager.getInstance().ewalletCardslist(walletModel.getSuffixIdCard(),
            walletModel.getIsUpdateFlag(),
            new UINotifyListener<List<WalletModel>>()
            {
                
                @Override
                public void onPreExecute()
                {
                    // TODO Auto-generated method stub
                    super.onPreExecute();
                    loadDialog(WalletOpenNextActivity.this, R.string.tx_wallet_uploading);
                }
                
                @Override
                public void onPostExecute()
                {
                    // TODO Auto-generated method stub
                    super.onPostExecute();
                    
                }
                
                @Override
                public void onError(final Object object)
                {
                    super.onError(object);
                    dissDialog();
                    if (checkSession())
                    {
                        return;
                    }
                    if (null != object)
                    {
                        WalletOpenNextActivity.this.runOnUiThread(new Runnable()
                        {
                            
                            @Override
                            public void run()
                            {
                                //                            btn_next_step.setText(R.string.reg_next_step);
                                toastDialog(WalletOpenNextActivity.this,
                                    object.toString(),
                                    new NewDialogInfo.HandleBtn()
                                    {
                                        
                                        @Override
                                        public void handleOkBtn()
                                        {
                                            finish();
                                        }
                                        
                                    });
                            }
                        });
                    }
                    
                }
                
                @Override
                public void onSucceed(List<WalletModel> model)
                {
                    dissDialog();
                    //                btn_next_step.setEnabled(true);
                    //                btn_next_step.setText(R.string.reg_next_step);
                    //                    WalletOpenNextActivity.startActivity(WalletOpenNextActivity.this, walletModel);
                    if (null != model && model.size() > 0)
                    {
                        bankNum = model.get(0).getAccountCode();
                        tx_peop.setText(model.get(0).getAccountCode());
                        tv_bank_name.setText(model.get(0).getBankName());
                        et_tel.setText(model.get(0).getPhone());
                        MainApplication.accountId = model.get(0).getAccountId();
                        //            btn_next_step.setBackgroundResource(R.drawable.btn_register);
                        //            btn_next_step.setEnabled(true);
                        
                        setButtonBg(btn_next_step, true, R.string.reg_next_step);
                        
                        if (model.size() > 1)
                        {
                            iv_choice_bank.setVisibility(View.VISIBLE);
                            iv_choice_bank.setImageResource(R.drawable.icon_general_back_default);
                        }
                        else
                        {
                            iv_choice_bank.setImageResource(R.drawable.icon_general_doubt_default);
                        }
                    }
                }
                
            });
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_apply_open_next_layout);
        
        initView();
        setLister();
        btn_next_step.getBackground().setAlpha(102);
        MainApplication.listActivities.add(this);
        
        //        walletModel = (WalletModel)getIntent().getSerializableExtra("walletModel");
        //        if (walletModel != null)
        //        {
        //            
        //            loadDate();
        //        }
        model = (List<WalletModel>)getIntent().getSerializableExtra("walletModel");
        if (null != model && model.size() > 0)
        {
            
            bankNum = model.get(0).getAccountCode();
            tx_peop.setText(model.get(0).getAccountCode());
            tv_bank_name.setText(model.get(0).getBankName());
            et_tel.setText(model.get(0).getPhone());
            if (StringUtil.isEmptyOrNull(model.get(0).getPhone()))
            {
                
                iv_clean_input.setVisibility(View.GONE);
            }
            MainApplication.accountId = model.get(0).getAccountId();
            //            btn_next_step.setBackgroundResource(R.drawable.btn_register);
            //            btn_next_step.setEnabled(true);
            
            setButtonBg(btn_next_step, true, R.string.reg_next_step);
            
            if (model.size() > 1)
            {
                iv_choice_bank.setVisibility(View.VISIBLE);
                iv_choice_bank.setImageResource(R.drawable.icon_general_back_default);
            }
            else
            {
                iv_choice_bank.setImageResource(R.drawable.icon_general_doubt_default);
            }
        }
    }
    
    /**
     * 验证4要素
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void certification(final String phone, final String isUpdateFlag, final String modifyidCards)
    {
        WalletManager.getInstance().certification(phone, isUpdateFlag, modifyidCards, new UINotifyListener<Boolean>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                //                btn_next_step.setText(R.string.reg_next_step_loading);
                //                btn_next_step.setEnabled(false);
                setButtonBg(btn_next_step, false, R.string.reg_next_step_loading);
                loadDialog(WalletOpenNextActivity.this, R.string.reg_next_step_loading);
            }
            
            @Override
            public void onPostExecute()
            {
                super.onPostExecute();
            }
            
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dissDialog();
                if (checkSession())
                {
                    return;
                }
                if (null != object)
                {
                    WalletOpenNextActivity.this.runOnUiThread(new Runnable()
                    {
                        
                        @Override
                        public void run()
                        {
                            //                            btn_next_step.setText(R.string.reg_next_step);
                            setButtonBg(btn_next_step, true, R.string.reg_next_step);
                            toastDialog(WalletOpenNextActivity.this, object.toString(), null);
                        }
                    });
                }
            }
            
            @Override
            public void onSucceed(Boolean model)
            {
                dissDialog();
                //                btn_next_step.setEnabled(true);
                //                btn_next_step.setText(R.string.reg_next_step);
                setButtonBg(btn_next_step, true, R.string.reg_next_step);
                if (model)
                {
                    WalletSubmitOpenActivity.startActivity(WalletOpenNextActivity.this, phone);
                }
            }
        });
    }
    
    private void setLister()
    {
        iv_clean_input.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                et_tel.setText("");
            }
        });
        
        tv_bank_list.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                showPage(BankListActivity.class);
                
            }
        });
        
        btn_next_step.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (StringUtil.isEmptyOrNull(et_tel.getText().toString()))
                {
                    toastDialog(WalletOpenNextActivity.this, getString(R.string.et_tel_info), null);
                    et_tel.setFocusable(true);
                    return;
                }
                if (model != null && model.size() > 0)
                {
                    if (!StringUtil.isEmptyOrNull(model.get(0).getModifyidCards()))
                    {
                        certification(et_tel.getText().toString(), model.get(0).getIsUpdateFlag(), model.get(0)
                            .getModifyidCards());
                    }
                    else
                    {
                        certification(et_tel.getText().toString(), "1", null);
                    }
                }
                else
                {
                    certification(et_tel.getText().toString(), "1", null);
                }
                
            }
        });
        
        iv_phone.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                NewDialogInfo info =
                    new NewDialogInfo(WalletOpenNextActivity.this, null, null, NewDialogInfo.WALLRT_PHONE_PROMT, null,
                        new NewDialogInfo.HandleBtn()
                        {
                            
                            @Override
                            public void handleOkBtn()
                            {
                            }
                            
                        });
                
                DialogHelper.resize(WalletOpenNextActivity.this, info);
                
                info.show();
            }
        });
        
        ly_chioce_bank.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (null != model && model.size() > 1 && !StringUtil.isEmptyOrNull(bankNum))
                {
                    pop =
                        new SelectPicPopupWindow(WalletOpenNextActivity.this, model, bankNum,
                            R.string.bt_dialog_bind_bank, new OnItemClickListener()
                            {
                                
                                @Override
                                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
                                {
                                    WalletModel walletModel = model.get(position);
                                    if (walletModel != null)
                                    {
                                        if (!walletModel.getAccountCode().equals(bankNum))
                                        {
                                            pop.dismiss();
                                            bankNum = walletModel.getAccountCode();
                                            
                                            tx_peop.setText(walletModel.getAccountCode());
                                            tv_bank_name.setText(walletModel.getBankName());
                                            et_tel.setText(walletModel.getPhone());
                                            
                                            MainApplication.accountId = walletModel.getAccountId();
                                        }
                                    }
                                }
                            });
                    pop.showAtLocation(iv_choice_bank, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0); //设置layout在PopupWindow中显示的位置
                }
                else
                {
                    NewDialogInfo info =
                        new NewDialogInfo(WalletOpenNextActivity.this, null, null, 1, null,
                            new NewDialogInfo.HandleBtn()
                            {
                                
                                @Override
                                public void handleOkBtn()
                                {
                                }
                                
                            });
                    
                    DialogHelper.resize(WalletOpenNextActivity.this, info);
                    
                    info.show();
                }
            }
        });
        
        iv_choice_bank.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (null != model && model.size() > 1 && !StringUtil.isEmptyOrNull(bankNum))
                {
                    
                    pop =
                        new SelectPicPopupWindow(WalletOpenNextActivity.this, model, bankNum,
                            R.string.bt_dialog_bind_bank, new OnItemClickListener()
                            {
                                
                                @Override
                                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
                                {
                                    WalletModel walletModel = model.get(position);
                                    if (walletModel != null)
                                    {
                                        if (!walletModel.getAccountCode().equals(bankNum))
                                        {
                                            pop.dismiss();
                                            bankNum = walletModel.getAccountCode();
                                            
                                            tx_peop.setText(walletModel.getAccountCode());
                                            tv_bank_name.setText(walletModel.getBankName());
                                            et_tel.setText(walletModel.getPhone());
                                            
                                            MainApplication.accountId = walletModel.getAccountId();
                                        }
                                    }
                                }
                            });
                    pop.showAtLocation(iv_choice_bank, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0); //设置layout在PopupWindow中显示的位置
                }
                else
                {
                    NewDialogInfo info =
                        new NewDialogInfo(WalletOpenNextActivity.this, null, null, 1, null,
                            new NewDialogInfo.HandleBtn()
                            {
                                
                                @Override
                                public void handleOkBtn()
                                {
                                }
                                
                            });
                    
                    DialogHelper.resize(WalletOpenNextActivity.this, info);
                    
                    info.show();
                }
            }
        });
    }
    
    private void initView()
    {
        iv_clean_input = getViewById(R.id.iv_clean_input);
        ly_chioce_bank = getViewById(R.id.ly_chioce_bank);
        btn_next_step = getViewById(R.id.btn_next_step);
        tx_peop = getViewById(R.id.tx_peop);
        et_tel = getViewById(R.id.et_tel);
        iv_phone = getViewById(R.id.iv_phone);
        tv_bank_name = getViewById(R.id.tv_bank_name);
        iv_choice_bank = getViewById(R.id.iv_choice_bank);
        tv_bank_list = getViewById(R.id.tv_bank_list);
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        {
            
            @Override
            public void onExecute(CharSequence s, int start, int before, int count)
            {
            }
            
            @Override
            public void onAfterTextChanged(Editable s)
            {
                if (et_tel.isFocused())
                {
                    if (et_tel.getText().toString().length() == 11)
                    {
                        //                        btn_next_step.setBackgroundResource(R.drawable.btn_register);
                        //                        btn_next_step.setEnabled(true);
                        
                        setButtonBg(btn_next_step, true, R.string.reg_next_step);
                    }
                    else
                    {
                        //                        btn_next_step.setBackgroundResource(R.drawable.general_button_grey_default);
                        //                        btn_next_step.setEnabled(false);
                        
                        setButtonBg(btn_next_step, false, R.string.reg_next_step);
                    }
                    
                    if (et_tel.getText().toString().length() > 0)
                    {
                        iv_clean_input.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        iv_clean_input.setVisibility(View.GONE);
                    }
                }
            }
        });
        
        et_tel.addTextChangedListener(editTextWatcher);
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        
        titleBar.setTitle(R.string.tv_bind_card);
        titleBar.setLeftButtonIsVisible(true);
        titleBar.setLeftButtonVisible(true);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
            }
        });
    }
}
