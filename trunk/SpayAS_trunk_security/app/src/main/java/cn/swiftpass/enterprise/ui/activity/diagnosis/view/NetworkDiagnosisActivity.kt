package cn.swiftpass.enterprise.ui.activity.diagnosis.view

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.ui.activity.BaseActivity
import cn.swiftpass.enterprise.ui.activity.diagnosis.adapter.NetworkDiagnosisAdapter
import cn.swiftpass.enterprise.ui.activity.diagnosis.entity.NetworkDiagnosisEntity
import cn.swiftpass.enterprise.ui.activity.diagnosis.entity.PingEntity
import cn.swiftpass.enterprise.ui.activity.diagnosis.interfaces.OnNetworkDiagnosisItemClickListener
import cn.swiftpass.enterprise.ui.widget.TitleBar
import cn.swiftpass.enterprise.utils.AppHelper
import cn.swiftpass.enterprise.utils.CustomOSUtils
import cn.swiftpass.enterprise.utils.KotlinUtils
import cn.swiftpass.enterprise.utils.NetworkUtils
import com.example.common.sentry.SentryUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.InetAddress
import java.net.NetworkInterface
import java.net.URI
import java.text.SimpleDateFormat
import java.util.Collections
import java.util.Date
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.thread
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


/**
 * @author lizheng.zhao
 * @date 20230421
 * @description 体验一下协程的魅力
 */
class NetworkDiagnosisActivity : BaseActivity<BasePresenter<*>>() {

    private fun getLayoutId() = R.layout.act_network_diagnosis


    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mAdapter: NetworkDiagnosisAdapter
    private val mList = ArrayList<NetworkDiagnosisEntity>()

    private val DOMAIN by lazy {
        MainApplication.getInstance().baseUrl.substring(
            0,
            MainApplication.getInstance().baseUrl.length - 1
        )
    }


    private val mSeqList = ArrayList<Int>()

    //不可重入锁
    private val mUuReentrantLock by lazy {
        ReentrantLock(false)
    }

    private val mRotateAnimation by lazy {
        AnimationUtils.loadAnimation(
            this@NetworkDiagnosisActivity,
            R.anim.rotate360
        )
    }

    private var mIp = ""

    private var mReceiveCnt = 0


    private val mPingData = PingEntity()


    companion object {
        const val POSITION_DNS = 0
        const val POSITION_NET = 1
        const val POSITION_DEVICE = 2
        const val POSITION_PING = 3

        const val REFRESH = "refresh"


        fun startNetworkDiagnosisActivity(fromActivity: Activity?) {
            fromActivity?.run {
                startActivity(Intent(this, NetworkDiagnosisActivity::class.java))
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        initView()
    }


    val initView = {
        initRecyclerView()
        refresh()
    }


    private val initRecyclerView = {
        mRecyclerView = findViewById(R.id.id_recycler_view)

        val manager = LinearLayoutManager(this)
        manager.orientation = LinearLayoutManager.VERTICAL
        mRecyclerView.layoutManager = manager


        mAdapter = NetworkDiagnosisAdapter(this, getData())
        mRecyclerView.adapter = mAdapter
        mAdapter.notifyDataSetChanged()

        mAdapter.setOnNetworkDiagnosisItemClickListener(object :
            OnNetworkDiagnosisItemClickListener {
            override fun onItemClick(position: Int) {
                if (position == POSITION_PING) {
                    PingActivity.startPingActivity(
                        this@NetworkDiagnosisActivity,
                        DOMAIN,
                        mIp
                    )
                }
            }
        })
    }


    private fun refresh() {
        refreshDns()
        refreshNet()
        refreshDevice()
    }


    /**
     * 刷新设备信息
     */
    private val refreshDevice = {
        mList[POSITION_DEVICE].content = strDevice()
        mAdapter.notifyItemChanged(
            POSITION_DEVICE,
            REFRESH
        )
    }


    /**
     * 刷新网络网络状态
     */
    private val refreshNet = {
        mList[POSITION_NET].content = strNet()
        mAdapter.notifyItemChanged(
            POSITION_NET,
            REFRESH
        )
    }


    /**
     * 刷新dns
     */
    private val refreshDns = {
        mList[POSITION_DNS].content = strDns()
        mAdapter.notifyItemChanged(
            POSITION_DNS,
            REFRESH
        )
        //Dispatchers.IO	        子线程, 适合执行磁盘或网络 I/O操作
        //launch是异步，不会阻塞主线程
        //async是同步，会阻塞主线程
        CoroutineScope(Dispatchers.IO).launch {
            mList[POSITION_DNS].content = strDns(getDeferredResult(::analysisDns))
            ping()
            //主线程中才可修改UI
            withContext(Dispatchers.Main) {
                mAdapter.notifyItemChanged(
                    POSITION_DNS,
                    REFRESH
                )
            }
        }
    }


    private val pingThread = {
        thread {
            mSeqList.clear()
            mUuReentrantLock.lock()
            mReceiveCnt = 0
            val command = "ping -c 10 $mIp"
            val process = Runtime.getRuntime().exec(command)
            val input = BufferedReader(InputStreamReader(process.inputStream))
            var line: String?
            val pingOutput = StringBuilder()
            while (input.readLine().also { line = it } != null) {
                pingOutput.append("$line\n".formatPingMsg())
            }

            val exitCode = process.waitFor()
            if (exitCode != 0) {
                mPingData.notReachable(mIp)
                updatePingUi(true)
            }
            mUuReentrantLock.unlock()
        }
    }

    private val ping = {
        if (TextUtils.isEmpty(mIp)) {
            mPingData.dnsAnalysisFailed()
            updatePingUi(true)
        } else {
            if (!mUuReentrantLock.isLocked) {
                // lock 对象当前可用，可以执行其他操作
                pingThread()
            }
        }
    }


    private fun String.formatPingMsg() {
        try {
            if (!TextUtils.isEmpty(this)) {
                if (this.contains("from $mIp")) {
                    //64 bytes from 119.29.126.90: icmp_seq=1 ttl=52 time=7.96 ms
                    val icmpSeqEntity = KotlinUtils.analysisIcmp(this)
                    icmpSeqEntity?.run {
                        if (mSeqList.contains(seq.toInt())) {
                        } else {
                            mSeqList.add(seq.toInt())
                            mPingData.sendPackage = "${seq}/10"
                            mPingData.receivePackage = "${++mReceiveCnt}/10"
                            val receive =
                                "%.2f".format((seq.toFloat() - mReceiveCnt) / seq.toFloat() * 100)
                            mPingData.lostRate = "${receive}%"
                            mPingData.minRtt = ""
                            mPingData.maxRtt = ""
                            mPingData.aveRtt = ""
                            updatePingUi()
                        }
                    }
                } else if (this.contains("transmitted")) {
                    //10 packets transmitted, 10 received, +27 duplicates, 0% packet loss, time 9013ms
                    val statisticsEntity = KotlinUtils.analysisStatistics(this)
                    statisticsEntity?.run {
                        mPingData.sendPackage = sent
                        mPingData.receivePackage = receive
                        val lossRate = (10 - receive.toInt()) * 10f
                        val rate = String.format("%.2f", lossRate)
                        mPingData.lostRate = "${rate}%"
                    }
                } else if (this.contains("max")) {
                    //rtt min/avg/max/mdev = 7.441/13.580/31.051/6.413 ms
                    val statisticsAvgEntity = KotlinUtils.analysisStatisticsAvg(this)
                    statisticsAvgEntity?.run {
                        mPingData.minRtt = min
                        mPingData.maxRtt = max
                        mPingData.aveRtt = avg
                        runOnUiThread {
                            dismissLoading(ParamsConstants.COMMON_LOADING)
                        }
                        updatePingUi(true)
                    }
                }
            }
        } catch (e: Exception) {
            SentryUtils.uploadTryCatchException(
                e,
                SentryUtils.getClassNameAndMethodName()
            )
            mPingData.error()
            updatePingUi(true)
        }
    }


    private fun getSentryUploadData(): String {
        val str = StringBuffer()
        mList.forEach {
            str.append("${it.content}\n\n")
        }
        return str.toString()
    }


    private fun updatePingUi(isNeedUploadSentry: Boolean = false) {
        mList[POSITION_PING].content = mPingData.display()
        runOnUiThread {
            if (isNeedUploadSentry) {
                //需要上传Sentry User FeedBack
                SentryUtils.sendUserFeedBack(AppHelper.getUUID(), getSentryUploadData())
            }
            mAdapter.notifyItemChanged(POSITION_PING, REFRESH)
        }
    }


    /**
     * 获取deferred
     */
    private suspend fun <T> getDeferredResult(susFun: suspend () -> T): T {
        //Dispatchers.IO	        子线程, 适合执行磁盘或网络 I/O操作
        val deferred = CoroutineScope(Dispatchers.IO).async {
            susFun()
        }
        return deferred.await()
    }


    private suspend fun analysisDns() = suspendCoroutine<String> {
        thread {
            try {
                runOnUiThread {
                    showLoading(R.string.string_one_minute, ParamsConstants.COMMON_LOADING)
                }
                val uri = URI(DOMAIN)
                val host = uri.host
                val ipAddress = InetAddress.getByName(host)
                mIp = ipAddress.hostAddress
                it.resume(ipAddress.hostAddress)
            } catch (e: Exception) {
                runOnUiThread {
                    dismissLoading(ParamsConstants.COMMON_LOADING)
                }
                mIp = ""
                it.resume("$e")
            }
        }
    }


    private fun getData(): ArrayList<NetworkDiagnosisEntity> {
        mList.clear()

        mList.add(
            NetworkDiagnosisEntity(
                DOMAIN,
                strDns()
            )
        )

        mList.add(
            NetworkDiagnosisEntity(
                "Net",
                strNet()
            )
        )

        mList.add(
            NetworkDiagnosisEntity(
                "Device",
                strDevice()
            )
        )


        mList.add(
            NetworkDiagnosisEntity(
                "Ping",
                mPingData.display(),
                true
            )
        )


        return mList
    }


    private fun isUseProxy(): String {
        val proxyAddress = System.getProperty("http.proxyHost")
        val proxyStr = System.getProperty("http.proxyPort")
        val proxyPort = proxyStr.takeIf { it != null } ?: "-1"
        val f = (!TextUtils.isEmpty(proxyAddress)) && proxyPort.toInt() != -1
        return if (f) {
            getString(R.string.string_connected)
        } else {
            getString(R.string.string_not_enable)
        }
    }


    private fun isUseVPN(): String {
        try {
            Collections.list(NetworkInterface.getNetworkInterfaces()).forEach {
                if (TextUtils.equals(it.name, "tun0") || TextUtils.equals(it.name, "ppp0")) {
                    return getString(R.string.string_connected)
                }
            }
        } catch (e: Exception) {
        }

        return getString(R.string.string_not_enable)
    }


    private fun strDns(dns: String = "${getString(R.string.string_dns_resolving)}") =
        "${getString(R.string.string_dns_resolution)}:\n    $DOMAIN\n    $dns"

    private fun strNet(): String {
        val netStatus =
            if (NetworkUtils.isNetworkAvailable(this)) getString(R.string.string_available) else getString(
                R.string.string_unavailable
            )
        val netType = AppHelper.getNetState(MainApplication.getInstance())
        val proxy = isUseProxy()
        val vpn = isUseVPN()
        return "${getString(R.string.string_network_status)}: $netStatus\n${getString(R.string.string_network_type)}: $netType\n${
            getString(
                R.string.string_proxy_status
            )
        }: $proxy\n${getString(R.string.string_vpn_status)}: $vpn"
    }


    private fun androidVersion() = when (android.os.Build.VERSION.SDK_INT) {
        19 -> {
            "4.4"
        }

        20 -> {
            "4.4W"
        }

        21 -> {
            "5"
        }

        22 -> {
            "5.1"
        }

        23 -> {
            "6"
        }

        24 -> {
            "7"
        }

        25 -> {
            "7.1.1"
        }

        26 -> {
            "8"
        }

        27 -> {
            "8.1"
        }

        28 -> {
            "9"
        }

        29 -> {
            "10"
        }

        30 -> {
            "11"
        }

        31 -> {
            "12"
        }

        32 -> {
            "12L"
        }

        33 -> {
            "13"
        }

        else -> {
            ""
        }
    }


    private fun strDevice() = "${getString(R.string.string_device_brand)}: ${
        Build.BRAND
    }\n${getString(R.string.string_device_model)}: ${
        Build.MODEL
    }\n${getString(R.string.string_device_system)}: ${
        androidVersion()
    }\n${getString(R.string.string_rom_system_type)}: ${
        CustomOSUtils.getCustomOS(
            Build.BRAND
        )
    }\n${getString(R.string.string_rom_system_version)}: ${
        CustomOSUtils.getCustomOSVersion(Build.BRAND)
    }\n${getString(R.string.string_current_time)}: ${
        SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Date())
    }\n${getString(R.string.string_app_version)}: ${
        AppHelper.getVerName(MainApplication.getInstance())
    }\nAppHash: ${AppHelper.getUUID()}"


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PingActivity.REQUEST_CODE
            && resultCode == PingActivity.RESULT_CODE
        ) {
            data?.run {
                val res = getSerializableExtra(PingActivity.RESULT_DATA)
                res?.run {
                    mPingData.copy(this as PingEntity)
                    updatePingUi()
                }
            }
        }
    }


    override fun setupTitleBar() {
        super.setupTitleBar()
        titleBar.setTitle(getString(R.string.string_network_diagnosis))
        titleBar.setLeftButtonVisible(true)
        titleBar.setRightButtonVisible(true)
        titleBar.setRightButton(R.drawable.process_loading)
        titleBar.setOnTitleBarClickListener(object : TitleBar.OnTitleBarClickListener {
            override fun onLeftButtonClick() {
                finish()
            }

            override fun onRightButtonClick() {
                Log.i("TAG_ZLZ", "点击刷新")
                titleBar.imgRight?.run {
                    startAnimation(mRotateAnimation)
                }
                refresh()
            }

            override fun onRightLayClick() {
            }

            override fun onRightButLayClick() {
            }
        })
    }


    override fun createPresenter() = null

    override fun useToolBar() = true
}