package cn.swiftpass.enterprise.ui.adapter;

import android.content.Context;
import android.widget.ArrayAdapter;
import cn.swiftpass.enterprise.utils.Utils;

public class KeyValueArrayAdapter extends ArrayAdapter<String>
{
    public KeyValueArrayAdapter(Context context, int textViewResourceId, String[] objects)
    {
        super(context, textViewResourceId, objects);
        
        //setDropDownViewResource(android.R.layout.simple_list_item_1);
        //setDropDownViewResource(R.layout.listitem_textview);
    }
    
    @Override
    public long getItemId(int position)
    {
        String str = super.getItem(position);
        
        String[] strs = str.split("\\|");
        return Utils.Integer.tryParse(strs[0], 0);
    }
    
    public int getPositionById(long id)
    {
        for (int i = 0; i < getCount(); i++)
        {
            if (getItemId(i) == id)
            {
                return i;
            }
        }
        
        return 0;
    }
    
    @Override
    public String getItem(int position)
    {
        String str = super.getItem(position);
        
        String[] strs = str.split("\\|");
        return strs[1];
    }
}
