package cn.swiftpass.enterprise.mymould.api;

import cn.swiftpass.enterprise.mymould.bean.BaseBean;
import cn.swiftpass.enterprise.mymould.bean.QueryMsgBean;
import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface MyMouldApi {

     String BASE_URL = "https://app.wepayez.com/";

    //收银管理列表分页查询接口
    @FormUrlEncoded
    @POST("spay/user/queryPageCashier")
    Observable<BaseBean<QueryMsgBean>> queryCashier(@Field("data")
     String data, @Header("spayRs") String spayRs);


}
