package cn.swiftpass.enterprise;

import android.app.Activity;
import android.app.Application;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.cxy.networklibrary.CoreComponent;
import com.cxy.networklibrary.di.module.HttpModule;
import com.cxy.networklibrary.util.FileUtil;
import com.iflytek.cloud.SpeechUtility;
import com.igexin.sdk.PushManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.nostra13.universalimageloader.core.ImageLoader;

import net.tsz.afinal.FinalBitmap;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cn.swiftpass.enterprise.broadcast.GTPushIntentService;
import cn.swiftpass.enterprise.broadcast.GTPushService;
import cn.swiftpass.enterprise.broadcast.NetworkStateReceiver;
import cn.swiftpass.enterprise.bussiness.logica.BaseManager;
import cn.swiftpass.enterprise.bussiness.logica.shop.PersonalManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.City;
import cn.swiftpass.enterprise.bussiness.model.Config;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.io.database.DatabaseHelper;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.mymould.api.MyMouldApi;
import cn.swiftpass.enterprise.utils.ApkUtil;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.ErrorHandler;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.TimeZoneUtil;


public class MainApplication extends Application {

    private static final String TAG = MainApplication.class.getSimpleName();

    public static MainApplication instance;

    private static Context context;

    public static final String permission = "cn.swiftpass.enterprise.my_permission";

    NetworkStateReceiver networkStateReceiver;

    public static Config config;

    //    private NewConfig config;

    private boolean isInitOK = false;

    private ImageLoader imageLoader;

    // 商户id
    public static String merchantId = "";

    public static String remark = "";

    // 用户id
    public static long userId = 0;

    // 用户名
    public static String userName;

    public static String realName = "";

    public static String mchName = "";

    public static String mchLogo = "";

    // 密码（区分临时用户）
    public static String pwd_tag;

    // 邀请码
    public static String channelId = "";

    // list存activity
    public static List<Activity> listActivities = new ArrayList<Activity>();

    private WeakReference<Activity> lastActivityRef = null;

    public static List<Activity> updatePwdActivities = new ArrayList<Activity>();

    public static List<Activity> allActivities = new ArrayList<Activity>();


    public static String cityStr = null;

    // 默认是体验用户
    public static boolean isRegisterUser = false;

    public static String shopID = "";

    public static int roleID = 0;

    public static boolean IsNative = false;

    public static boolean isRefund = false;

    public static String phone = "";

    public static String isAdmin = "";

    public static String isOrderAuth = "";

    public static String isRefundAuth = "";

    public static Integer isTotalAuth = 0;

    public static String body = "";

    public static Integer isActivityAuth = 0; // 活动权限

    public static final String PAY_WX_MICROPAY = "pay.weixin.micropay"; // 微信扫一扫支付

    public static final String PAY_ZFB_MICROPAY = "pay.alipay.micropay"; // 支付宝扫一扫

    public static final String PAY_QQ_MICROPAY = "pay.qq.micropay"; // 手Q扫一扫

    public static final String PAY_WX_NATIVE = "pay.weixin.native"; // 微信扫码支付

    public static final String PAY_WX_NATIVE1 = "pay.weixin.native1"; // 切换支付方式用的标示

    public static final String PAY_ZFB_NATIVE = "pay.alipay.native"; // 支付宝扫码

    public static final String PAY_ZFB_NATIVE1 = "pay.alipay.nativev2"; // 支付宝扫码

    public static final String PAY_QQ_NATIVE = "pay.tenpay.native"; // 手Q扫码

    public static final String PAY_QQ_NATIVE1 = "pay.qq.jspay"; // 手Q扫码

    public static final String PAY_WX_SJPAY = "pay.weixin.jspay"; // 手Q扫码

    public static final String PAY_ZFB_QUERY = "unified.trade.query";

    public static final String PAY_WX_QUERY = "trade.single.query";

    public static final String PAY_QQ_PROXY_MICROPAY = "pay.qq.proxy.micropay"; // 手Q反扫受理机构模式

    public static final String PAY_ALIPAY_WAP = "pay.alipay.wappay";//支付宝wap支付

    public static final String PAY_ALIPAY_TAG = "alipay";

    public static final String PAY_WEIXIN_TAG = "weixin";

    public static final String PAY_QQ_TAG = "qq";

    public static final String PAY_JD_TAG = "jdpay";

    public static final String PAY_WX_MIC_INTL = "pay.weixin.micropay.intl"; // 微信境外支付

    public static final String PAY_WX_NATIVE_INTL = "pay.weixin.native.intl"; // 微信扫码支付支付

    public static String serviceType = "";

    public static final String PAY_TYPE_REFUND = "pay.scan.refund"; // 扫码退款

    public static final String PAY_TYPE_WRITE_OFF = "pay.weixin.writeoff"; // 微信卡券核销

    public static final String PAY_TYPE_SCAN_OPNE = "pay.weixin.scan.open"; // 二维码激活开通

    public static final String PAY_TYPE_NITINE = "pay.weixin.vard"; // 微信卡券核销

    public static final String PAY_TYPE_MICROPAY_VCARD = "pay.weixin.micropay.vard"; // 微信卡券核销

    public static final String PAY_JINGDONG = "pay.jdpay.micropay"; // 京东钱包反扫

    public static final String PAY_JINGDONG_NATIVE = "pay.jdpay.native"; // 京东钱包正扫

    public static final String PAY_ZFB_WAP = "pay.alipay.wappayv2"; // 支付宝wap支付

    public static String signKey = ""; // 数字签名秘钥

    public static final String CLIENT = "SPAY_AND"; //android终端类型

    public static final String PAY_QQ_WAP = "pay.tenpay.wappay";// QQwap支付

    public static boolean needLogin = false;

    public static String feeType = ""; //货比类型

    public static String feeFh = "";// 货比符号

    public static final String DEF_PAY_METHOD = "pay.weixin.native";//默认微信支付方式

    public static String pushMoneyUri = ""; //提现url

    public static String themeMd5 = ""; //是否有需要版本定制化

    public static String Md5 = ""; //动态支付类型配置

    public static String device = ""; //设备号

    public static Integer isHasEwallet = -1; //是否开通电子钱包

    public static String accountId = "";//结算账号

    public static String cookie_key = "";//登录返回cookie

    public static String SAUTHID = "";

    public static String cardholder = "";//持卡人

    public static Map<String, String> payTypeMap = new HashMap<String, String>(); //支付类型map

    public static Map<String, String> tradeStateMap = new HashMap<String, String>(); //交易状态

    public static Map<String, String> refundStateMap = new HashMap<String, String>(); //退款状态

    public static Map<String, String> apiProviderMap = new HashMap<String, String>(); //支付类型

    public static Integer showEwallet = 0; //是否有开通电子钱包权限

    public static boolean isSessionOutTime = false;

    public static boolean isUpdateShow = false; //更新提示

    public static Integer isFixCode = 0; //固定二维码是否为聚合 0为聚合，1为不聚合

    public static Integer isCardOpen = 0; //是否开启电子卡：0未开启，1开启

    public static Integer numFixed = 2; //获取币种的最小的小数点位数，为整数，如果获取不到默认为2，即小数点后面两位

    public static double RMBFIX = 100.0; //转换成人民币之后，人民币的币种按照最小单位的换算

    public static String refundToken = ""; //退款验证身份返回的token

    public static String serPubKey = ""; //服务端公钥

    public static String skey = ""; //密钥标识

    public static String fileMd5 = "";//更新安装包的MD%

    public static final String LANG_CODE_ZH_CN_NEW = "zh-CN";

    public static final String LANG_CODE_ZH_TW_NEW = "zh-tw";

    public static final String LANG_CODE_ZH_MO_NEW = "zh-MO";

    public static final String LANG_CODE_ZH_HK_NEW = "zh-HK";


    /**
     * 简体中文
     */
    public static final String LANG_CODE_ZH_CN = "zh_cn";

    /**
     * 繁体中文
     */
    public static final String LANG_CODE_ZH_TW = "zh_tw";

    public static final String LANG_CODE_ZH_MO = "zh_MO";

    public static final String LANG_CODE_ZH_HK = "zh_HK";

    public static final boolean IS_POS_VERSION = BuildConfig.IS_POS_VERSION;
    /**
     * 英语
     */
    public static final String LANG_CODE_EN_US = "en_us";

    public static final String LANG_CODE_ZH_CN_HANS = "zh_CN_#Hans";
    public static final String LANG_CODE_ZH_HK_HANT = "zh_HK_#Hant";

    public static boolean isActive = false;

    public static Integer payMethodSize = 0;

    public static FinalBitmap finalBitmap;

    public static String newSignKey = "";//新的签名key

    public static double getSurchargeRate() {
        return surchargeRate;
    }

    public static void setSurchargeRate(double surchargeRate) {
        MainApplication.surchargeRate = surchargeRate;
    }
    public static void setAlipayPayRate(double alipayPayRate) {
        MainApplication.alipayPayRate = alipayPayRate;
    }

    public static double getAlipayPayRate() {
        return alipayPayRate;
    }
    public static double getTaxRate() {
        return taxRate;
    }

    public static void setTaxRate(double taxRate) {
        MainApplication.taxRate = taxRate;
    }

    public static double getVatRate() {
        return vatRate;
    }

    public static void setVatRate(double vatRate) {
        MainApplication.vatRate = vatRate;
    }

    public static double getUsdToRmbExchangeRate() {
        return usdToRmbExchangeRate;
    }

    public static void setUsdToRmbExchangeRate(double usdToRmbExchangeRate) {
        MainApplication.usdToRmbExchangeRate = usdToRmbExchangeRate;
    }

    public static double getSourceToUsdExchangeRate() {
        return sourceToUsdExchangeRate;
    }

    public static void setSourceToUsdExchangeRate(double sourceToUsdExchangeRate) {
        MainApplication.sourceToUsdExchangeRate = sourceToUsdExchangeRate;
    }

    private static double sourceToUsdExchangeRate = 0;

    private static double alipayPayRate = 0;
    /**
     * 转美元费率
     */
    private static double usdToRmbExchangeRate = 1.0;
    /**
     * 附加手续费率
     */
    private static double surchargeRate = 0;
    /**
     * 预扣税费率
     */
    private static double taxRate = 0;
    /**
     * 增值税率
     */
    private static double vatRate = 0;

    /**
     * 是否有附加税率
     */
    private static boolean isSurchargeOpen;

    /**
     * 是否有预扣税
     */
    private static boolean isTaxRateOpen;

    /**
     * 是否有小费
     */
    private static boolean isTipOpen;

    public static boolean isTaxRateOpen() {
        return isTaxRateOpen;
    }

    public static void setTaxRateOpen(boolean taxRateOpen) {
        isTaxRateOpen = taxRateOpen;
    }

    public static boolean isTipOpen() {
        return isTipOpen;
    }

    public static void setTipOpen(boolean tipOpen) {
        isTipOpen = tipOpen;
    }

    public static boolean isSurchargeOpen() {
        return isSurchargeOpen;
    }

    public static void setSurchargeOpen(boolean surchargeOpen) {
        isSurchargeOpen = surchargeOpen;
    }

    // 不管是蓝牙连接方还是服务器方，得到socket对象后都传入
    public static BluetoothSocket bluetoothSocket = null;

    public static MainApplication getContext() {
        return instance;
    }

    public MainApplication() {
        instance = this;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        unregisterReceiver(networkStateReceiver);
        releaseHelper();
        destroyed = true;
    }

    /**
     * 退款状态
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static void setFeeFh(String map) {
        if (!StringUtil.isEmptyOrNull(map)) {
            PreferenceUtil.commitString("feeFh" + getMchId(), map);
        }
    }

    /**
     * 退款状态
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    @SuppressWarnings("unchecked")
    public static String getFeeFh() {
        if (StringUtil.isEmptyOrNull(MainApplication.feeFh)) return MainApplication.feeFh;

        return PreferenceUtil.getString("feeFh" + getMchId(), MainApplication.feeFh);
    }

    /**
     * 退款状态
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static void setFeeType(String map) {
        PreferenceUtil.commitString("feeTyp" + getMchId() + getUserId(), map);
    }

    /**
     * 退款状态
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    @SuppressWarnings("unchecked")
    public static String getFeeType() {
        if (StringUtil.isEmptyOrNull(MainApplication.feeType)) return MainApplication.feeType;

        return PreferenceUtil.getString("feeTyp" + getMchId() + getUserId(), MainApplication.feeType);
    }

    /**
     * 退款状态
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static void setRefundStateMap(Map<String, String> map) {
        if (map != null && map.size() > 0) {
            SharedPreUtile.saveObject(map, "refundStateMap" + getMchId());
        }
    }

    /**
     * 退款状态
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    @SuppressWarnings("unchecked")
    public static Map<String, String> getRefundStateMap() {
        if (MainApplication.refundStateMap.size() > 0) return MainApplication.refundStateMap;

        return (Map<String, String>) SharedPreUtile.readProduct("refundStateMap" + getMchId());
    }

    /**
     * 支付状态
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static void setTradeStateMap(Map<String, String> map) {
        if (map != null && map.size() > 0) {
            SharedPreUtile.saveObject(map, "tradeStateMap" + getMchId());
        }
    }

    /**
     * 签名key
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static void setNewSignKey(String sign) {
        if (!StringUtil.isEmptyOrNull(sign)) {
            PreferenceUtil.commitString("newSignKey" + getMchId(), sign);
        }
    }

    /**
     * 签名key
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static String getNewSignKey() {
        if (!StringUtil.isEmptyOrNull(MainApplication.newSignKey))
            return MainApplication.newSignKey;

        return PreferenceUtil.getString("newSignKey" + getMchId(), MainApplication.signKey);
    }

    /**
     * 签名key
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static void setSignKey(String sign) {
        if (!StringUtil.isEmptyOrNull(sign)) {
            PreferenceUtil.commitString("signKey" + getMchId(), sign);
        }
    }

    /**
     * 签名key
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static String getSignKey() {
        if (!StringUtil.isEmptyOrNull(MainApplication.signKey)) return MainApplication.signKey;

        return PreferenceUtil.getString("signKey" + getMchId(), MainApplication.signKey);
    }

    /**
     * 支付类型名称
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    @SuppressWarnings("unchecked")
    public static Map<String, String> getTradeTypeMap() {
        if (MainApplication.tradeStateMap.size() > 0) return MainApplication.tradeStateMap;

        return (Map<String, String>) SharedPreUtile.readProduct("tradeStateMap" + getMchId());
    }

    /**
     * 支付类型名称
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static void setPayTypeMap(Map<String, String> map) {
        if (map != null && map.size() > 0) {
            SharedPreUtile.saveObject(map, "payTypeMap" + getMchId());
        }
    }

    /**
     * 支付类型名称
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    @SuppressWarnings("unchecked")
    public static Map<String, String> getPayTypeMap() {
        if (MainApplication.payTypeMap.size() > 0) return MainApplication.payTypeMap;

        return (Map<String, String>) SharedPreUtile.readProduct("payTypeMap" + getMchId());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //获取Context
        context = getApplicationContext();

        // 多进程导致多次初始化Application,这里只初始化App主进程的Application
        String curProcessName = ApkUtil.getProcessName(this, android.os.Process.myPid());
        if (!curProcessName.equals(getPackageName())) {
            return;
        }

        if (!IS_POS_VERSION) {
            try {
                PushManager.getInstance().initialize(getApplicationContext(), GTPushService.class);
                PushManager.getInstance().registerPushIntentService(getApplicationContext(), GTPushIntentService.class);
            } catch (Exception e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }
        }

        //        FrontiaApplication.initFrontiaApplication(getApplicationContext()); // W.L
        // 初始化使用百度社交化分享控件
        // 删除1.3.2版本的日志文件
        ErrorHandler.getInstance().delete();
        // 初始异常文件
        ErrorHandler errorHandler = ErrorHandler.getInstance();
        errorHandler.init(this);

        /**
         * ORMLite
         */
        if (helper == null) {
            helper = getHelperInternal(this);
            created = true;
        }
        initConfig();

        if (!IS_POS_VERSION) {
            // 定位地理城市
            // locate();
            // 获取定位位置城市
            // 科大讯飞 初始化
            SpeechUtility.createUtility(MainApplication.this, "appid=5775d91b");

        }
        try {
            finalBitmap = FinalBitmap.create(this);
            finalBitmap.configDiskCachePath(cn.swiftpass.enterprise.utils.FileUtils.getAppCache());
        } catch (Exception e) {
            Logger.e("hehui", "configDiskCachePath failed ");
        }

        initNetWork(this);
    }


    //返回
    public  static Context getApplicationContextObject(){
        return context;
    }


    public boolean isNeedLogin() {
        return needLogin;
    }

    public static void setNeedLogin(boolean mNeedLogin) {
        needLogin = mNeedLogin;
    }


    private void initConfig() {
        if (isInitOK) return;
        config = Config.readConfig(this);

        //        config = NewConfig.readConfig(this);
        AppHelper.getAppCacheDir();
        ApiConstant.bankType = config.getBankType();
        ApiConstant.body = config.getBody();
        ApiConstant.APK_NAME = config.getAppName();
        ApiConstant.IP = config.getServerAddr();
        ApiConstant.IP_WITHOUT_CDN = config.getServerAddrBack();
        ApiConstant.PORT = config.getServerPort();
        ApiConstant.PAYGATEWAY = config.getPayGateway();
        ApiConstant.bankCode = config.getBankCode();
        ApiConstant.bankName = config.getBankName();
        ApiConstant.wxCardUrl = config.getWxCardUrl();
        ApiConstant.redPack = config.getRedPack();
        ApiConstant.appKey = config.getAppKey();
        ApiConstant.Channel = config.getChannel();
        ApiConstant.pushMoneyUrl = config.getPushMoneyUrl();
        // if ("8080".equals(ApiConstant.PORT))
        // {
//        ApiConstant.BASE_URL_PORT = ApiConstant.IP;
        ApiConstant.serverAddrTest = config.getServerAddrTest();
        ApiConstant.serverAddrPrd = config.getServerAddrPrd();
        ApiConstant.pushMoneyUrlTest = config.getPushMoneyUrlTest();
        ApiConstant.pushMoneyUrlPrd = config.getPushMoneyUrlPrd();
        ApiConstant.serverAddrDev = config.getServerAddrDev();
        ApiConstant.imie = config.getImie();
        try {
            //从本地缓存里面取服务器地址
            PreferenceUtil.init(this);

            //要进行是否开启CDN的判断
            String CDN_status = PreferenceUtil.getString("CDN", "open");
            if(CDN_status.equals("open")){
                ApiConstant.BASE_URL_PORT = ApiConstant.IP;
            }else{
                ApiConstant.BASE_URL_PORT = ApiConstant.IP_WITHOUT_CDN;
            }

            String serverCifg = PreferenceUtil.getString("serverCifg", "");
            //            Log.i("hehui", "serverCifg-->" + serverCifg);
            if (!StringUtil.isEmptyOrNull(serverCifg)) {
                if (serverCifg.equals("test")) {
                    ApiConstant.BASE_URL_PORT = config.getServerAddrTest();
                    ApiConstant.pushMoneyUrl = config.getPushMoneyUrlTest();
                } else if (serverCifg.equals("prd")) {
                    ApiConstant.BASE_URL_PORT = config.getServerAddrPrd();
                    ApiConstant.pushMoneyUrl = config.getPushMoneyUrlPrd();
                } else if (serverCifg.equals("dev")) {
                    ApiConstant.BASE_URL_PORT = config.getServerAddrDev();
                    ApiConstant.pushMoneyUrl = config.getPushMoneyUrlTest();
                }

            }
        } catch (Exception e) {
            Logger.e("hehui", "" + e);
        }

        ApiConstant.NOTIFY_URL = ApiConstant.BASE_URL_PORT + "order/notify";
        ApiConstant.RETURN_URL = ApiConstant.BASE_URL_PORT + "order/returnUrl?orderNo=";
        ApiConstant.DOWNLOAD_APP_URL = ApiConstant.PAYGATEWAY;

        ApiConstant.DO_CFT_GET_UUID_URL = ApiConstant.BASE_URL_PORT + "/swiftUuidPro/uuid/findUuid.action?url=";

        ApiConstant.isLocaltionQRcode = config.isCreateLocaltionQRcode();
        ApiConstant.pad = config.getClientType();
        GlobalConstant.isDebug = config.isDebug();
        ApiConstant.ISOVERSEASY = config.isOverseasPay(); // W.l添加境外支付开关
        // init networkBoracast
        networkStateReceiver = new NetworkStateReceiver();
        registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        isInitOK = true;

    }

    public void exit() {
        // 程序注销
        BaseManager.destoryAll();
        destory();
    }

    public void filterResponse(RequestResult result) {

    }

    public SharedPreferences getApplicationPreferences() {
        return getSharedPreferences("prefs", Context.MODE_PRIVATE);
    }

    // ORMLite
    private volatile DatabaseHelper helper;

    private volatile boolean created = false;

    private volatile boolean destroyed = false;

    public DatabaseHelper getHelper() {
        if (helper == null) {
            if (!created) {
                throw new IllegalStateException("A call has not been made to onCreate() yet so the helper is null");
            } else if (destroyed) {
                throw new IllegalStateException("A call to onDestroy has already been made and the helper cannot be used after that point");
            } else {
                throw new IllegalStateException("Helper is null for some unknown reason");
            }
        } else {
            return helper;
        }
    }

    /**
     * 商户号
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static String getMchId() {
        if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
            return MainApplication.merchantId;
        }
        return PreferenceUtil.getString("mchId", MainApplication.merchantId);
    }

    /**
     * 蓝牙状态 <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static void setBlueState(Boolean sign) {
        PreferenceUtil.commitBoolean("blueState" + ApiConstant.bankCode, sign);
    }

    /**
     * 蓝牙状态 <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    @SuppressWarnings("unchecked")
    public static Boolean getBlueState() {

        return PreferenceUtil.getBoolean("blueState" + ApiConstant.bankCode, false);
    }

    /**
     * 商户名称
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static String getMchName() {

        if (!StringUtil.isEmptyOrNull(MainApplication.mchName)) {
            return MainApplication.mchName;
        }

        return PreferenceUtil.getString("mchName", MainApplication.mchName);
    }

    /**
     * 商户号
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static void setMchId(String mchId) {
        if (!StringUtil.isEmptyOrNull(mchId)) {
            PreferenceUtil.commitString("mchId", mchId);
        }
    }

    /**
     * 用户id
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static void setExchangeRate(String value) {
        PreferenceUtil.commitString("exchangeRate" + MainApplication.getUserId() + MainApplication.getMchId(), value);
    }

    /**
     * 用户id
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static String getExchangeRate() {
        return PreferenceUtil.getString("exchangeRate" + MainApplication.getUserId() + MainApplication.getMchId(), "");
    }

    /**
     * 用户id
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static void setUserId(Long userId) {
        if (userId > 0) {
            PreferenceUtil.commitLong("userId", userId);
        }
    }

    /**
     * 用户id
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static long getUserId() {
        if (MainApplication.userId > 0) {
            return MainApplication.userId;
        }

        return PreferenceUtil.getLong("userId", MainApplication.userId);
    }

    public static void setAutoBluePrintSetting(Boolean sign) {
        PreferenceUtil.commitBoolean("autoBluePrintSetting" + ApiConstant.bankCode, sign);
    }

    /**
     * 蓝牙打印设置自动状态 <功能详细描述> 默认是不自动打印
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    @SuppressWarnings("unchecked")
    public static Boolean getAutoBluePrintSetting() {
        return PreferenceUtil.getBoolean("autoBluePrintSetting" + ApiConstant.bankCode, false);
    }

    /**
     * 蓝牙打印设置状态 <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static void setBluePrintSetting(Boolean sign) {
        PreferenceUtil.commitBoolean("BluePrintSetting" + ApiConstant.bankCode, sign);
    }

    /**
     * 蓝牙打印设置状态 <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    @SuppressWarnings("unchecked")
    public static Boolean getBluePrintSetting() {
        return PreferenceUtil.getBoolean("BluePrintSetting" + ApiConstant.bankCode, true);
    }

    /**
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static void setBlueDeviceName(String device) {
        PreferenceUtil.commitString("blueDevice" + ApiConstant.bankCode, device);
    }

    /**
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    @SuppressWarnings("unchecked")
    public static String getBlueDeviceAddress() {
        return PreferenceUtil.getString("blueDeviceAddress" + ApiConstant.bankCode, "");
    }

    public static void setBlueDeviceNameAddress(String device) {
        PreferenceUtil.commitString("blueDeviceAddress" + ApiConstant.bankCode, device);
    }

    /**
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    @SuppressWarnings("unchecked")
    public static String getBlueDeviceName() {
        return PreferenceUtil.getString("blueDevice" + ApiConstant.bankCode, "");
    }

    /**
     * 商户名称
     * <功能详细描述>
     *
     * @param context
     * @return
     * @see [类、类#方法、类#成员]
     */

    public static void setMchName(String mchName) {
        if (!StringUtil.isEmptyOrNull(mchName)) {
            PreferenceUtil.commitString("mchName", mchName);
        }
    }

    protected DatabaseHelper getHelperInternal(Context context) {
        DatabaseHelper newHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        return newHelper;
    }

    protected void releaseHelper() {
        OpenHelperManager.releaseHelper();
        this.helper = null;
    }

    public void destory() {
        isInitOK = false;
    }


    public void setLastActivityRef(WeakReference<Activity> lastActivityRef) {
        this.lastActivityRef = lastActivityRef;
    }

    private void initNetWork(MainApplication context) {
        HashMap mapHeader=new HashMap();
        mapHeader.put("User-Agent", String.format("%s/%s (Linux; Android %s; %s Build/%s)",
                ApiConstant.APK_NAME, "3.5", Build.VERSION.RELEASE, Build.MANUFACTURER, Build.ID));

        String language = PreferenceUtil.getString("language", "");

        String langu = "en_us";
        if (!TextUtils.isEmpty(language)) {
            mapHeader.put("fp-lang", language);
            langu = language;
        } else {
            String lan = Locale.getDefault().toString();

            if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_HANS)) {
                mapHeader.put("fp-lang", MainApplication.LANG_CODE_ZH_CN);
                langu = MainApplication.LANG_CODE_ZH_CN;
            } else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO) ||
                    lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_HANT)) {
                mapHeader.put("fp-lang", MainApplication.LANG_CODE_ZH_TW);
                langu = MainApplication.LANG_CODE_ZH_TW;
            } else {
                mapHeader.put("fp-lang", MainApplication.LANG_CODE_EN_US);
                langu = MainApplication.LANG_CODE_EN_US;
            }
        }
        try {
            String key = "sp-" + "Android" + "-" + AppHelper.getImei(MainApplication.getContext());
            String value = AppHelper.getVerCode(MainApplication.getContext()) + "," + ApiConstant.bankCode + "," + langu + "," + AppHelper.getAndroidSDKVersionName() + "," + DateUtil.formatTime(System.currentTimeMillis()) + "," + "Android";
            mapHeader.put(key, value);
        } catch (Exception e5) {

        }

        mapHeader.put("Accept-Encoding", "gzip");
        //增加版本号
        mapHeader.put("interface-version", "2");
        //带上手机默认时区
        mapHeader.put("timezone", TimeZoneUtil.getCurrentTimeZone());

        CoreComponent.init(context);
        CoreComponent.configureHttp()//配置retrofit
                .setBaseUrl(MyMouldApi.BASE_URL)//设置BaseUrl
                .setConnectTimeout(20)//设置请求超时时长，单位秒
                .setMapHeader(mapHeader)//设置全局的header信息
//                .setIsUseCache(true)//设置是否启用缓存，默认不启用
//                .setCacheFolder(file)//设置缓存地址，传入的file需为文件夹，默认保存在/storage/emulated/0/Android/data/com.xxx.xxx/cache/retrofit_http_cache下
//                .setCacheSize(size)//设置缓存大小，单位byte，默认20M
//                .setCacheTimeWithNet(time)//设置有网络时缓存保留时长，单位秒，默认60秒
//                .setCacheTimeWithoutNet(time)//设置无网络时缓存保留时长，单位秒，默认一周
                .setIsUseRetryWhenError(false)//设置是否开启失败重试功能，目前仅支持普通的网络请求，上传下载不支持。默认不开启
                .setMaxRetryCount(1)//设置失败后重试的最大次数，默认3次
                .setTimeRetryDelay(2)//设置失败后重试的延迟时长，单位秒，默认3秒
                .setIsUseLog(true);//设置是否开启Log，默认不开启

        CoreComponent.configureOther().setIsShowLog(true);

        //缓存模块
        CoreComponent.configureCache()//配置缓存
//                .setDiskCacheMaxSize(50*1024*1024)//设置磁盘缓存最大缓存大小，单位为byte，默认无上限
//                .setDiskCacheMaxCount(10)//设置磁盘缓存的文件夹数量上限，默认无上限
                //配置磁盘缓存的地址，传入的File需为文件夹，默认保存在/data/user/0/com.xxx.xxx/cache下
                .setDiskCacheFolder(FileUtil.getDirectory(
                        FileUtil.getExternalCacheDir(this), "subscribe_cache"));
        CoreComponent.create();//开始构建

    }
}
