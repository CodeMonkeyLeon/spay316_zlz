package com.fourmob.datetimepicker.test;

import java.util.Calendar;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.fourmob.datetimepicker.R;
import com.fourmob.datetimepicker.date.AccessibleDateAnimator;
import com.fourmob.datetimepicker.date.DatePickerController;
import com.fourmob.datetimepicker.date.DayPickerView;
import com.fourmob.datetimepicker.date.SimpleMonthAdapter;
import com.fourmob.datetimepicker.date.SimpleMonthAdapter.CalendarDay;

public class MainTest extends Activity implements DatePickerController
{
    
    private AccessibleDateAnimator animator;
    
    private DayPickerView mDayPickerView;
    
    private final Calendar mCalendar = Calendar.getInstance();
    
    private int mWeekStart = mCalendar.getFirstDayOfWeek();
    
    private int mMaxYear = 2017;
    
    private int mMinYear = 2016;
    
    private int currentYear = mCalendar.get(Calendar.YEAR);
    
    private int currentMonth = mCalendar.get(Calendar.MONTH);
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        animator = (AccessibleDateAnimator)findViewById(R.id.animator);
        Calendar calendar = Calendar.getInstance();
        //        calendar.set(2016, 9, 14);
        
        mDayPickerView = new DayPickerView(MainTest.this, this, calendar);
        
        mCalendar.set(Calendar.YEAR, Calendar.YEAR);
        mCalendar.set(Calendar.MONTH, Calendar.MONTH);
        mCalendar.set(Calendar.DAY_OF_MONTH, Calendar.DAY_OF_MONTH);
        
        animator.addView(mDayPickerView);
        animator.setDateMillis(mCalendar.getTimeInMillis());
    }
    
    @Override
    public int getFirstDayOfWeek()
    {
        // TODO Auto-generated method stub
        return mWeekStart;
    }
    
    @Override
    public int getMaxYear()
    {
        // TODO Auto-generated method stub
        return mMaxYear;
    }
    
    @Override
    public int getMinYear()
    {
        // TODO Auto-generated method stub
        return mMinYear;
    }
    
    @Override
    public CalendarDay getSelectedDay()
    {
        // TODO Auto-generated method stub
        return new SimpleMonthAdapter.CalendarDay(mCalendar);
    }
    
    @Override
    public void onDayOfMonthSelected(int year, int month, int day)
    {
        Log.i("hehui", "onDayOfMonthSelected");
    }
    
    @Override
    public void onYearSelected(int year)
    {
        Log.i("hehui", "onYearSelected");
    }
    
    @Override
    public void registerOnDateChangedListener(OnDateChangedListener onDateChangedListener)
    {
        // TODO Auto-generated method stub
        Log.i("hehui", "registerOnDateChangedListener");
        
    }
    
    public static abstract interface OnDateChangedListener
    {
        public abstract void onDateChanged();
    }
    
    //    
    //    public static abstract interface OnDateSetListener
    //    {
    //        public abstract void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day);
    //    }
    
    @Override
    public void tryVibrate()
    {
        
    }
    
    @Override
    public void registerOnDateChangedListener(
        com.fourmob.datetimepicker.date.DatePickerDialog.OnDateChangedListener onDateChangedListener)
    {
        
    }
    
    @Override
    public int getCurrentYear()
    {
        return currentYear;
    }
    
    @Override
    public int getCurrentMonth()
    {
        return currentMonth;
    }
}
