package cn.swiftpass.enterprise.base;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Window;

import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;


import butterknife.ButterKnife;
import cn.swiftpass.enterprise.R;
import cn.swiftpass.enterprise.SpayApplication;
import cn.swiftpass.enterprise.base.mvp.BasePresenter;
import cn.swiftpass.enterprise.base.mvp.BaseView;
import cn.swiftpass.enterprise.util.FinishActivityManager;
import cn.swiftpass.enterprise.util.StatusBarUtils;
import cn.swiftpass.enterprise.widget.LoadingDialog;


public abstract class BaseActivity<P extends BasePresenter> extends RxAppCompatActivity implements BaseView {

    protected P mPresenter;
    public Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //去除title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        StatusBarUtils.getInstance().setWindowStatusBarColor(this, R.color.toolbar);
        //强制竖屏
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        SpayApplication.getRefWatcher().watch(this);
        if (getLayoutId() != 0) {
            setContentView(getLayoutId());
            ButterKnife.bind(this);
            FinishActivityManager.getManager().addActivity(this);
        }
        initView(savedInstanceState);
        mContext=this;
    }

    @Override
    protected void onDestroy() {
        if (mPresenter != null) {
            mPresenter.detachView();
        }
        super.onDestroy();
        dismissLoadingDialog();
    }

    /**
     * 设置布局
     *
     * @return
     */
    protected abstract int getLayoutId();

    /**
     * 初始化视图
     */
    protected abstract void initView(Bundle savedInstanceState);

    private  LoadingDialog.Builder mDialogBuilder;
    private  LoadingDialog mLoadingDialog;
    @Override
    public void showLoadingDialog(String text){
        if (mLoadingDialog==null){
            mDialogBuilder=new LoadingDialog.Builder(this)
                    .setMessage(text)
                    .setCancelable(true).setCancelOutside(true);
            mLoadingDialog=mDialogBuilder.create();
        }
        mLoadingDialog.show();
    }
    @Override
    public void showLoadingDialog(String text,boolean isCancelOutside){
        if (mDialogBuilder==null){
            mDialogBuilder=new LoadingDialog.Builder(this)
                    .setMessage(text)
                    .setCancelable(true).setCancelOutside(isCancelOutside);
            mLoadingDialog=mDialogBuilder.create();
        }
        mLoadingDialog.show();
    }
    @Override
    public void showLoadingDialog(){
        if (mDialogBuilder==null){
            mDialogBuilder=new LoadingDialog.Builder(this)
                    .setMessage(getString(R.string.loading))
                    .setCancelable(true).setCancelOutside(true);
            mLoadingDialog=mDialogBuilder.create();
        }
        mLoadingDialog.show();
    }
    @Override
    public void dismissLoadingDialog(){
        if (mLoadingDialog!=null){
            mLoadingDialog.dismiss();
        }
    }
}
