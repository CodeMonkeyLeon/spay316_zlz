/*
 * 文 件 名:  TtsVoice.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-7-5
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.utils;

import java.util.Locale;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import cn.swiftpass.enterprise.MainApplication;

import com.iflytek.cloud.ErrorCode;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechSynthesizer;
import com.iflytek.cloud.SynthesizerListener;
import com.iflytek.sunflower.FlowerCollector;

/**
 * 科大讯飞 文字合成语言
 *
 * @author he_hui
 * @version [版本号, 2016-7-5]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class TtsVoice {
    private static String TAG = TtsVoice.class.getSimpleName();

    private Context context;

    // 语音合成对象
    private SpeechSynthesizer mTts;

    private String mEngineType = SpeechConstant.TYPE_CLOUD;

    // 默认发音人
    private String voicer = "xiaoyan";

    private String text;

    public TtsVoice(Context context, String text) {
        this.text = text;
        this.context = context;
        // 初始化合成对象
        mTts = SpeechSynthesizer.createSynthesizer(context, mTtsInitListener);

        // 移动数据分析，收集开始合成事件
        FlowerCollector.onEvent(context, "tts_play");

        // 设置参数
        setParam();
    }

    /**
     * 初始化监听。
     */
    private InitListener mTtsInitListener = new InitListener() {
        @Override
        public void onInit(int code) {
            Log.i(TAG, "InitListener init() code = " + code);
            if (code != ErrorCode.SUCCESS) {
                Log.e(TAG, "初始化失败,错误码：" + code);
            } else {
                // 初始化成功，之后可以调用startSpeaking方法
                // 注：有的开发者在onCreate方法中创建完合成对象之后马上就调用startSpeaking进行合成，
                // 正确的做法是将onCreate中的startSpeaking调用移至这里
                startVoice();
            }
        }
    };

    /**
     * 合成回调监听。
     */
    private SynthesizerListener mTtsListener = new SynthesizerListener() {

        @Override
        public void onSpeakBegin() {
            Log.i(TAG, "开始播放");
        }

        @Override
        public void onSpeakPaused() {
            Log.i(TAG, "暂停播放");
        }

        @Override
        public void onSpeakResumed() {
            Log.i(TAG, "继续播放");
        }

        @Override
        public void onBufferProgress(int percent, int beginPos, int endPos, String info) {
            // 合成进度
        }

        @Override
        public void onSpeakProgress(int percent, int beginPos, int endPos) {
            // 播放进度
        }

        @Override
        public void onCompleted(SpeechError error) {
        }

        @Override
        public void onEvent(int eventType, int arg1, int arg2, Bundle obj) {
            // 以下代码用于获取与云端的会话id，当业务出错时将会话id提供给技术支持人员，可用于查询会话日志，定位出错原因
            // 若使用本地能力，会话id为null
            //  if (SpeechEvent.EVENT_SESSION_ID == eventType) {
            //      String sid = obj.getString(SpeechEvent.KEY_EVENT_SESSION_ID);
            //      Log.d(TAG, "session id =" + sid);
            //  }
        }
    };

    /**
     * 停止播放
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    public void stopVoice() {
        if (mTts != null) {
            mTts.pauseSpeaking();
            // 退出时释放连接
            mTts.destroy();
        }
    }

    public void startVoice() {
        if (!StringUtil.isEmptyOrNull(text)) {
            int code = ErrorCode.MSP_ERROR_FAIL;
            if (mTts != null) {
                code = mTts.startSpeaking(text, mTtsListener);
            }

            if (code != ErrorCode.SUCCESS) {
                if (code == ErrorCode.ERROR_COMPONENT_NOT_INSTALLED) {
                    //未安装则跳转到提示安装页面
                    //                mInstaller.install();
                } else {
                    Log.e(TAG, "语音合成播放失败,错误码: " + code);
                }
            }
        }
    }

    /**
     * 参数设置
     *
     * @param param
     * @return
     */
    private void setParam() {
        // 清空参数
        mTts.setParameter(SpeechConstant.PARAMS, null);
        // 根据合成引擎设置相应参数
        if (mEngineType.equals(SpeechConstant.TYPE_CLOUD)) {
            mTts.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD);
            // 设置在线合成发音人
            String language = PreferenceUtil.getString("language", "");

            Locale locale = context.getResources().getConfiguration().locale; //zh-rHK
//            String lan = locale.getCountry();//英文 返回为空  中文 CN TW
            String lan = locale.getLanguage() + "-" + locale.getCountry();
            Log.i("hehui", "TtsVoice language-->" + language + ",lan-->" + lan);
            if (!TextUtils.isEmpty(language)) {
                //                    heards.put("fp-lang", PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN));
                if (!TextUtils.isEmpty(language) && language.equals(MainApplication.LANG_CODE_ZH_TW)) // 繁体
                {
                    mTts.setParameter(SpeechConstant.VOICE_NAME, "xiaomei");
                } else if (!TextUtils.isEmpty(language) && language.equals(MainApplication.LANG_CODE_ZH_CN)) {
                    //                    mTts.setParameter(SpeechConstant.VOICE_NAME, "xiaomei");
                    mTts.setParameter(SpeechConstant.VOICE_NAME, voicer);
                } else { // 跟随系统
                    mTts.setParameter(SpeechConstant.VOICE_NAME, "catherine");
                }
            } else {

                if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_NEW)) {
                    mTts.setParameter(SpeechConstant.VOICE_NAME, voicer);
                } else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_NEW) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO_NEW) | lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW_NEW)) {
                    mTts.setParameter(SpeechConstant.VOICE_NAME, "xiaomei");
                } else {
                    mTts.setParameter(SpeechConstant.VOICE_NAME, "catherine");
                }
            }
            //设置合成语速
            mTts.setParameter(SpeechConstant.SPEED, "50");
            //设置合成音调
            mTts.setParameter(SpeechConstant.PITCH, "50");
            //设置合成音量
            mTts.setParameter(SpeechConstant.VOLUME, "50");
        } else {
            mTts.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_LOCAL);
            // 设置本地合成发音人 voicer为空，默认通过语记界面指定发音人。
            mTts.setParameter(SpeechConstant.VOICE_NAME, "");
            /**
             * TODO 本地合成不设置语速、音调、音量，默认使用语记设置
             * 开发者如需自定义参数，请参考在线合成参数设置
             */
        }
        //设置播放器音频流类型
        mTts.setParameter(SpeechConstant.STREAM_TYPE, "3");
        // 设置播放合成音频打断音乐播放，默认为true
        mTts.setParameter(SpeechConstant.KEY_REQUEST_FOCUS, "true");

        // 设置音频保存路径，保存音频格式支持pcm、wav，设置路径为sd卡请注意WRITE_EXTERNAL_STORAGE权限
        // 注：AUDIO_FORMAT参数语记需要更新版本才能生效
        mTts.setParameter(SpeechConstant.AUDIO_FORMAT, "wav");
        mTts.setParameter(SpeechConstant.TTS_AUDIO_PATH, Environment.getExternalStorageDirectory() + "/msc/tts.wav");
    }
}
