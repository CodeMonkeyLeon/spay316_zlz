package cn.swiftpass.enterprise.print;

import android.content.Context;
import android.text.TextUtils;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.MasterCardOrderInfo;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;
import cn.swiftplus.enterprise.printsdk.print.PrintClient;

/**
 * Created by aijingya on 2020/8/24.
 *
 * @Package cn.swiftpass.enterprise.print
 * @Description:
 * @date 2020/8/24.19:23.
 */
public class PrintOrder {

    public static void printOrderDetails(Context context, boolean isCashierShow , Order orderModel){

        //打印开始前，先bindService
        PrintClient.getInstance().bindDeviceService();

        if (orderModel.isPay()) {
            PrintClient.getInstance().setReceipt_type(PrintClient.RECEIPT_TYPE.order_receipt);
            PrintClient.getInstance().printTitle(context.getString(R.string.tx_blue_print_pay_note));
        }else {
            PrintClient.getInstance().setReceipt_type(PrintClient.RECEIPT_TYPE.refund_receipt);
            PrintClient.getInstance().printTitle(context.getString(R.string.tx_blue_print_refund_note));
        }

        PrintClient.getInstance().printTextLeft(orderModel.getPartner());
        PrintClient.getInstance().printTextLeft(context.getString(R.string.tv_pay_client_save));
        PrintClient.getInstance().printDoubleLine();

        if (!TextUtils.isEmpty(MainApplication.getMchName())) {
            PrintClient.getInstance().printTextLeft(context.getString(R.string.shop_name) + "：");
            if (!StringUtil.isEmptyOrNull(MainApplication.getMchName())) {
                PrintClient.getInstance().printTextLeft(MainApplication.getMchName());
            }
        }
        if (!TextUtils.isEmpty(MainApplication.getMchId())) {
            PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "："+ MainApplication.getMchId());
        }

        if(orderModel.isPay()){//收款
            if (MainApplication.isAdmin.equals("0") && !isCashierShow) { //收银员
                PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_user) + "：" + MainApplication.realName);
            }
            if (isCashierShow){
                PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_user) + "：" + orderModel.getUserName());
            }
        }

        if(orderModel.isPay()){//收款
            PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_bill_stream_time) + "：");
            PrintClient.getInstance().printTextLeft(orderModel.getAddTimeNew());

        }else{
            PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_blue_print_refund_time) + "：");
            PrintClient.getInstance().printTextLeft(orderModel.getAddTimeNew());
            PrintClient.getInstance().printTextLeft(context.getString(R.string.refund_odd_numbers)+ "：");
            PrintClient.getInstance().printTextLeft(orderModel.getRefundNo());
        }
        PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_order_no) + ": ");
        PrintClient.getInstance().printTextLeft(orderModel.getOrderNoMch());

        if (orderModel.isPay()){
            if (MainApplication.getPayTypeMap() != null && MainApplication.getPayTypeMap().size() > 0) {
                PrintClient.getInstance().printTextLeft(MainApplication.getPayTypeMap().get(orderModel.getApiCode())+ " " + context.getString(R.string.tx_orderno));
            }
            PrintClient.getInstance().printTextLeft(orderModel.getTransactionId());

            PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_bill_stream_choice_title) + "：");
            PrintClient.getInstance().printTextLeft(orderModel.getTradeName());

            PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_bill_stream_statr) + "：");
            PrintClient.getInstance().printTextLeft(MainApplication.getTradeTypeMap().get(orderModel.getTradeState() + ""));

            if (!TextUtils.isEmpty(orderModel.getAttach())) {
                PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_bill_stream_attach) + "：" + " " + orderModel.getAttach());
            }

            if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(context.getString(R.string.pay_yuan))) {
                if (orderModel.getDaMoney() > 0) {
                    long actualMoney = orderModel.getDaMoney() + orderModel.getOrderFee();
                    PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_blue_print_money) + "：");
                    PrintClient.getInstance().printTextRight(DateUtil.formatRMBMoneyUtils(actualMoney) + context.getString(R.string.pay_yuan));
                } else {
                    PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_blue_print_money) + "：" );
                    PrintClient.getInstance().printTextRight(DateUtil.formatRMBMoneyUtils(orderModel.getOrderFee()) + context.getString(R.string.pay_yuan));
                }
            }else{
                if (MainApplication.isSurchargeOpen()) {
                    if(!orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") &&  !orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")){
                        PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_blue_print_money) + "：");
                        PrintClient.getInstance().printTextRight(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getOrderFee()));

                        PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_surcharge) + "：");
                        PrintClient.getInstance().printTextRight(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getSurcharge()));
                    }
                }

                //Uplan ---V3.0.5迭代新增

                //如果为预授权的支付类型，则不展示任何优惠相关的信息
               /* 因目前SPAY與網關支持的預授權接口僅為‘支付寶’預授權，
                遂與銀聯Uplan不衝突，遂預授權頁面內，無需處理該優惠信息。*/

                if(!TextUtils.isEmpty(orderModel.getTradeType()) && !orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") &&  !orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")){
                    // 优惠详情 --- 如果列表有数据，则展示，有多少条，展示多少条
                    if(orderModel.getUplanDetailsBeans() != null && orderModel.getUplanDetailsBeans().size() > 0){
                        for(int i =0 ; i<orderModel.getUplanDetailsBeans().size() ; i++){
                            String string_Uplan_details;
                            //如果是Uplan Discount或者Instant Discount，则采用本地词条，带不同语言的翻译
                            if(orderModel.getUplanDetailsBeans().get(i).getDiscountNote().equalsIgnoreCase("Uplan discount")){
                                string_Uplan_details = context.getString(R.string.uplan_Uplan_discount) + "：";
                            }else if(orderModel.getUplanDetailsBeans().get(i).getDiscountNote().equalsIgnoreCase("Instant Discount")){
                                string_Uplan_details = context.getString(R.string.uplan_Uplan_instant_discount) + "：";
                            } else{//否则，后台传什么展示什么
                                string_Uplan_details = orderModel.getUplanDetailsBeans().get(i).getDiscountNote()+ "：";
                            }
                            PrintClient.getInstance().printTextLeft(string_Uplan_details);
                            PrintClient.getInstance().printTextRight(MainApplication.feeType+ " "+ "-" + orderModel.getUplanDetailsBeans().get(i).getDiscountAmt());
                        }
                    }

                    // 消费者实付金额 --- 如果不为0 ，就展示
                    if(orderModel.getCostFee() != 0){
                        PrintClient.getInstance().printTextLeft(context.getString(R.string.uplan_Uplan_actual_paid_amount) + "：");
                        PrintClient.getInstance().printTextRight(MainApplication.feeType + " "+ DateUtil.formatMoneyUtils(orderModel.getCostFee()));
                    }
                }
                PrintClient.getInstance().printTextLeft(context.getString(R.string.tv_charge_total) + "：");
                PrintClient.getInstance().printTextRight(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee()));

                if (orderModel.getCashFeel() > 0) {
                    PrintClient.getInstance().printTextRight(context.getString(R.string.pay_yuan)+ DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()));
                }
            }
        }else{
            PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_bill_stream_choice_title) + "：");
            PrintClient.getInstance().printTextLeft(orderModel.getTradeName());

            if (!TextUtils.isEmpty(orderModel.getUserName())) {
                PrintClient.getInstance().printTextLeft(context.getString(R.string.tv_refund_peop) + "：");
                PrintClient.getInstance().printTextLeft(orderModel.getUserName());
            }
            if (MainApplication.getRefundStateMap() != null && MainApplication.getRefundStateMap().size() > 0) {
                PrintClient.getInstance().printTextLeft(context.getString(R.string.tv_refund_state) + "：");
                PrintClient.getInstance().printTextLeft(MainApplication.getRefundStateMap().get(orderModel.getRefundState() + ""));
            }

            if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(context.getString(R.string.pay_yuan))) {
                PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_blue_print_money) + "：");
                PrintClient.getInstance().printTextRight(DateUtil.formatRMBMoneyUtils(orderModel.getTotalFee()) + context.getString(R.string.pay_yuan));

                PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_bill_stream_refund_money) + "：");
                PrintClient.getInstance().printTextRight(DateUtil.formatRMBMoneyUtils(orderModel.getRefundMoney()) + context.getString(R.string.pay_yuan));

            } else {
                PrintClient.getInstance().printTextLeft(context.getString(R.string.tv_charge_total) + "：");
                PrintClient.getInstance().printTextRight(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee()));

                PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_bill_stream_refund_money) + "：");
                PrintClient.getInstance().printTextRight(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getRefundMoney()));

                if(orderModel.getCashFeel() > 0){
                    PrintClient.getInstance().printTextRight(context.getString(R.string.pay_yuan)+ DateUtil.formatMoneyUtils(orderModel.getCashFeel()));
                }
            }
            if (!TextUtils.isEmpty(orderModel.getPrintInfo())) {
                PrintClient.getInstance().printMultiLines(orderModel.getPrintInfo());
            }
        }
        if(!TextUtils.isEmpty(orderModel.getOrderNoMch()) && orderModel.isPay()){
            PrintClient.getInstance().printQRCode(orderModel.getOrderNoMch());
            PrintClient.getInstance().printTextCenter(context.getString(R.string.refound_QR_code));
        }
        PrintClient.getInstance().pintSingleLine();

        PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()));
        PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_sign) + "：");

        PrintClient.getInstance().printEmptyLine();
        PrintClient.getInstance().printEmptyLine();
        PrintClient.getInstance().printEmptyLine();

        PrintClient.getInstance().startPrint();

    }

    public static void printMasterCardOrderDetails(Context context, boolean isCashierShow , boolean isPay,boolean isOrderDetails, String partner,Order orderModel){

        //打印开始前，先bindService
        PrintClient.getInstance().bindDeviceService();

        if (isPay) {
            PrintClient.getInstance().setReceipt_type(PrintClient.RECEIPT_TYPE.order_receipt);
            PrintClient.getInstance().printTitle(context.getString(R.string.card_payment_print_order_receipt));
        }else {
            //此位置因为退款小票也打印二维码，所以，和订单小票一样，设置成订单小票的类型
            PrintClient.getInstance().setReceipt_type(PrintClient.RECEIPT_TYPE.order_receipt);
            PrintClient.getInstance().printTitle(context.getString(R.string.tx_blue_print_refund_note));
        }

        PrintClient.getInstance().printTextLeft(partner);
        PrintClient.getInstance().printTextLeft(context.getString(R.string.tv_pay_client_save));
        PrintClient.getInstance().printDoubleLine();

        if (!TextUtils.isEmpty(MainApplication.getMchName())) {
            PrintClient.getInstance().printTextLeft(context.getString(R.string.shop_name) + "：");
            if (!StringUtil.isEmptyOrNull(MainApplication.getMchName())) {
                PrintClient.getInstance().printTextLeft(MainApplication.getMchName());
            }
        }
        if (!TextUtils.isEmpty(MainApplication.getMchId())) {
            PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "："+ MainApplication.getMchId());
        }

        if(isPay){
            if (MainApplication.isAdmin.equals("0") && !isCashierShow) { //收银员
                PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_user) + "：" + MainApplication.realName);
            }
            if (isCashierShow){
                PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_user) + "：" + orderModel.getUserName());
            }
        }

        if(isPay){
            if(isOrderDetails){//订单详情
                PrintClient.getInstance().printTextLeft(context.getString(R.string.card_payment_print_order_transation_time) + "：");
                PrintClient.getInstance().printTextLeft(orderModel.getTradeTimeNew());

            }else{//支付完成
                PrintClient.getInstance().printTextLeft(context.getString(R.string.card_payment_print_order_transation_time) + "：");
                PrintClient.getInstance().printTextLeft(orderModel.getTradeTime());
            }
        }else{//退款
            PrintClient.getInstance().printTextLeft(context.getString(R.string.card_payment_print_order_transation_time) + "：");
            PrintClient.getInstance().printTextLeft(orderModel.getTradeTimeNew());

            //退款单号
            PrintClient.getInstance().printTextLeft(context.getString(R.string.refund_odd_numbers) + "：");
            PrintClient.getInstance().printTextLeft(orderModel.getOutRefundNo());
        }

        PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_order_no) + ": ");
        PrintClient.getInstance().printTextLeft(orderModel.getOrderNoMch());

        if(isPay && isOrderDetails) {//订单详情才打印
            //transactionId不为空才打印
            if(!TextUtils.isEmpty(orderModel.getTransactionId())){
                if (MainApplication.getPayTypeMap() != null && MainApplication.getPayTypeMap().size() > 0) {
                    PrintClient.getInstance().printTextLeft(MainApplication.getPayTypeMap().get(orderModel.getApiCode())+ " " + context.getString(R.string.tx_orderno));
                }
                PrintClient.getInstance().printTextLeft(orderModel.getTransactionId());
            }
        }

        PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_bill_stream_choice_title) + "：");
        PrintClient.getInstance().printTextLeft(orderModel.getTradeName());

      if(!isPay){
          String status = "";
          //订单状态 交易状态：(1.初始化，2：支付成功，3：支付失败)
          switch (orderModel.getTradeState()){
              case 1:
                  //初始化
                  status = ToastHelper.toStr(R.string.card_payment_status_init);
                  break;
              case 2:
                  status = ToastHelper.toStr(R.string.card_payment_status_success);
                  break;
              case 3:
                  status = ToastHelper.toStr(R.string.card_payment_status_failed);
                  break;
              default:
                  status = ToastHelper.toStr(R.string.card_payment_status_init);
                  break;
          }

          PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tv_refund_state) + "：");
          PrintClient.getInstance().printTextLeft(status);


          if (!TextUtils.isEmpty(orderModel.getUserName())) {
              PrintClient.getInstance().printTextLeft(context.getString(R.string.tv_refund_peop) + "：");
              PrintClient.getInstance().printTextLeft(orderModel.getUserName());
          }
      }

        if(isPay){
            if(isOrderDetails){//订单详情
                PrintClient.getInstance().printTextLeft(context.getString(R.string.card_payment_print_order_status) + "：");
                PrintClient.getInstance().printTextLeft(orderModel.getTradeStateText());
            }else{//支付完成
                String status = ToastHelper.toStr(R.string.card_payment_status_init);
                //订单状态 交易状态：(1.初始化，2：支付成功，3：支付失败)
                switch (orderModel.getTradeState()){
                    case 1:
                        //初始化
                        status = ToastHelper.toStr(R.string.card_payment_status_init);
                        break;
                    case 2:
                        status = ToastHelper.toStr(R.string.card_payment_status_success);
                        break;
                    case 3:
                        status = ToastHelper.toStr(R.string.card_payment_status_failed);
                        break;
                    default:
                        status = ToastHelper.toStr(R.string.card_payment_status_init);
                        break;
                }
                PrintClient.getInstance().printTextLeft(context.getString(R.string.card_payment_print_order_status) + "：");
                PrintClient.getInstance().printTextLeft(status);
            }
            PrintClient.getInstance().printTextLeft(context.getString(R.string.card_payment_print_order_type) + "：");
            if(!TextUtils.isEmpty(orderModel.getTradeType()) && MainApplication.getCardPaymentTradeStateMap() != null){
                PrintClient.getInstance().printTextLeft(MainApplication.getCardPaymentTradeStateMap().get(orderModel.getTradeType()));
            }
        }

        if (isPay &&!TextUtils.isEmpty(orderModel.getAttach())) {
            PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_bill_stream_attach) + "：" + " " + orderModel.getAttach());
        }

        if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(context.getString(R.string.pay_yuan))) {
            if(isPay){
                PrintClient.getInstance().printTextLeft(context.getString(R.string.card_payment_print_order_amount) + "：" );
                PrintClient.getInstance().printTextRight(DateUtil.formatRMBMoneyUtils(orderModel.getMoney()) + context.getString(R.string.pay_yuan));
            }else{
                PrintClient.getInstance().printTextLeft(context.getString(R.string.card_payment_print_order_amount) + "：" );
                PrintClient.getInstance().printTextRight(DateUtil.formatRMBMoneyUtils(orderModel.getTotalFee()) + ToastHelper.toStr(R.string.pay_yuan));

                PrintClient.getInstance().printTextLeft(context.getString(R.string.card_payment_print_order_amount) + "：" );
                PrintClient.getInstance().printTextRight(DateUtil.formatRMBMoneyUtils(orderModel.getRefundMoney())+ context.getString(R.string.pay_yuan));
            }
        }else{
            if (MainApplication.isSurchargeOpen()) {
                if(isPay){
                    if(isOrderDetails){//订单详情
                        PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_blue_print_money) + "：");
                        PrintClient.getInstance().printTextRight(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getOrderFee()));

                        PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_surcharge) + "：");
                        PrintClient.getInstance().printTextRight(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getSurcharge()));

                        PrintClient.getInstance().printTextLeft(context.getString(R.string.tv_charge_total) + "：");
                        PrintClient.getInstance().printTextRight(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getMoney()));
                    }else{//支付完成
                        PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_blue_print_money) + "：");
                        PrintClient.getInstance().printTextRight(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getOrderAmount()));

                        PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_surcharge) + "：");
                        PrintClient.getInstance().printTextRight(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getSurcharge()));

                        PrintClient.getInstance().printTextLeft(context.getString(R.string.tv_charge_total) + "：");
                        PrintClient.getInstance().printTextRight(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getMoney()));
                    }
                }else{//退款完成
                    PrintClient.getInstance().printTextLeft(context.getString(R.string.tv_charge_total) + "：");
                    PrintClient.getInstance().printTextRight(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee()));

                    PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_bill_stream_refund_money) + "：");
                    PrintClient.getInstance().printTextRight(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getRefundMoney()));

                    if (!StringUtil.isEmptyOrNull(orderModel.getPrintInfo())) {
                        PrintClient.getInstance().printTextLeft(orderModel.getPrintInfo());
                    }
                }
            }else{
                if(isPay){
                    if(isOrderDetails){//订单详情
                        PrintClient.getInstance().printTextLeft(context.getString(R.string.tv_charge_total) + "：");
                        PrintClient.getInstance().printTextRight(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getMoney()));

                    }else{//支付完成
                        PrintClient.getInstance().printTextLeft(context.getString(R.string.tv_charge_total) + "：");
                        PrintClient.getInstance().printTextRight(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getMoney()));
                    }
                }else{//退款完成
                    PrintClient.getInstance().printTextLeft(context.getString(R.string.tv_charge_total) + "：");
                    PrintClient.getInstance().printTextRight(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee()));

                    PrintClient.getInstance().printTextLeft(context.getString(R.string.tx_bill_stream_refund_money) + "：");
                    PrintClient.getInstance().printTextRight(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getRefundMoney()));

                    if (!StringUtil.isEmptyOrNull(orderModel.getPrintInfo())) {
                        PrintClient.getInstance().printTextLeft(orderModel.getPrintInfo());
                    }
                }
            }
        }

        if(!TextUtils.isEmpty(orderModel.getOrderNoMch())){
            PrintClient.getInstance().printQRCode(orderModel.getOrderNoMch());
            PrintClient.getInstance().printTextCenter(context.getString(R.string.card_payment_print_order_info));
        }
        PrintClient.getInstance().pintSingleLine();

        PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()));
        PrintClient.getInstance().printTextLeft(ToastHelper.toStr(R.string.tx_blue_print_sign) + "：");

        PrintClient.getInstance().printEmptyLine();
        PrintClient.getInstance().printEmptyLine();
        PrintClient.getInstance().printEmptyLine();

        PrintClient.getInstance().startPrint();
    }


}
