package cn.swiftpass.enterprise.ui.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.CashierReport;
import cn.swiftpass.enterprise.utils.DateUtil;

public class OrderReportsumDialog extends Dialog implements View.OnClickListener
{
    private ViewGroup mRootView;
    
    private TextView btnOk;
    
    private ConfirmListener btnListener;
    
    private CashierReport c;
    
    public OrderReportsumDialog(Context context, String title, ConfirmListener btnListener, CashierReport c)
    {
        super(context);
        this.c = c;
        this.btnListener = btnListener;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        Window window = getWindow();
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(0));
        this.setCanceledOnTouchOutside(false);
        
        mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.dialog_order_report, null);
        setContentView(mRootView);
        
        TextView tvRefundNum = (TextView)mRootView.findViewById(R.id.tv_d_refundNum);
        TextView tvRefundFee = (TextView)mRootView.findViewById(R.id.tv_d_refundfee);
        if (c.refundNum != null)
        {
            tvRefundNum.setText(c.refundNum + "");
        }
        else
        {
            tvRefundNum.setText("0");
        }
        if (c.refundFee != null)
        {
            tvRefundFee.setText("¥ " + DateUtil.formatMoney(c.refundFee / 100d));
        }
        else
        {
            tvRefundFee.setText("¥ 0.00");
        }
        
        btnOk = (TextView)mRootView.findViewById(R.id.ok);
        
        btnOk.setOnClickListener(this);
    }
    
    public static void show(Context context, String title, ConfirmListener btnListener, CashierReport c)
    {
        OrderReportsumDialog dia = new OrderReportsumDialog(context, title, btnListener, c);
        dia.show();
    }
    
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.ok:
                if (btnListener != null)
                {
                    btnListener.ok();
                }
                break;
            
            default:
                break;
        }
        dismiss();
    }
    
    public interface ConfirmListener
    {
        public void ok();
        
        public void cancel();
    }
}
