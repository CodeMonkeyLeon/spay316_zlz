package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.bussiness.model.OrderList
import cn.swiftpass.enterprise.common.Constant
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.PreAuthDetailsContract
import cn.swiftpass.enterprise.mvp.utils.ParamsUtils
import cn.swiftpass.enterprise.utils.DateUtil
import cn.swiftpass.enterprise.utils.JsonUtil
import cn.swiftpass.enterprise.utils.SignUtil
import cn.swiftpass.enterprise.utils.StringUtil
import com.example.common.sentry.SentryUtils.uploadNetInterfaceException
import okhttp3.Call
import java.text.ParseException

/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @烟囱犹如平地耸立起来的巨人
 * @望着布满灯火的大地
 * @不断地吸着烟卷
 * @思索着一种谁也不知道的事情
 */
class PreAuthDetailsPresenter : PreAuthDetailsContract.Presenter {

    private var mView: PreAuthDetailsContract.View? = null


    override fun querySpayOrder(
        listStr: ArrayList<String?>?,
        payTypeList: ArrayList<String?>?,
        isRefund: Int,
        page: Int,
        order: String?,
        startDate: String?
    ) {
        mView?.let { view ->

            val mapParam: MutableMap<String?, Any?> = HashMap()

            mapParam[ParamsConstants.PAGE_SIZE] = "20"
            mapParam[ParamsConstants.MCH_ID] = MainApplication.getInstance().getMchId()
            mapParam[ParamsConstants.PAGE] = page.toString()
            if (MainApplication.getInstance().isAdmin(0) && MainApplication.getInstance()
                    .isOrderAuth("0")
            ) {
                mapParam[ParamsConstants.USER_ID] =
                    MainApplication.getInstance().getUserId().toString()
            }
            //退款不用时分秒
            mapParam[ParamsConstants.START_DATE] = null
            mapParam[ParamsConstants.END_DATE] = null

            if (!StringUtil.isEmptyOrNull(order)) {
                mapParam[ParamsConstants.ORDER_NO] = order
            }

            val spayRs = System.currentTimeMillis()
            //加签名
            if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getNewSignKey())) {
                mapParam[ParamsConstants.SPAY_RS] = spayRs.toString()
                mapParam[ParamsConstants.NNS] = SignUtil.getInstance().createSign(
                    JsonUtil.jsonToMap(JsonUtil.objectToJson(mapParam)),
                    MainApplication.getInstance().getNewSignKey()
                )
            }

            if (payTypeList != null && payTypeList.size > 0) {
                mapParam[ParamsConstants.API_PROVIDER_LIST] = payTypeList
            }

            val tradeType = arrayListOf<Int>()
            val payType = arrayListOf<Int>()
            ParamsUtils.parseToJson(listStr, tradeType, payType)
            if (tradeType.size > 0) {
                mapParam[ParamsConstants.API_PROVIDER_LIST] = tradeType
            }
            if (payType.size > 0) {
                mapParam[ParamsConstants.TRADE_STATE_LIST] = payType
            }

            var url: String? = null
            if (isRefund == 1) {
                url = MainApplication.getInstance().baseUrl + "spay/refundList"
            } else if (isRefund == 0) { //流水
                url = MainApplication.getInstance().baseUrl + "spay/order/orderList"
            } else {
                url = MainApplication.getInstance().baseUrl + "spay/wxCardGetUserListV2" //卡券
                if (!StringUtil.isEmptyOrNull(startDate)) {
                    mapParam[ParamsConstants.CARD_TIME] = startDate
                } else {
                    mapParam[ParamsConstants.CARD_TIME] =
                        DateUtil.formatYYMD(System.currentTimeMillis())
                }
            }



            AppClient.querySpayOrderWxCard(
                spayRs.toString(),
                JsonUtil.mapToJsons(mapParam),
                isRefund,
                object : CallBackUtil.CallBackCommonResponse<OrderList>(OrderList()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        commonResponse?.let {
                            view.querySpayOrderFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        r?.let { response ->
                            val sentryUrl =
                                if (TextUtils.isEmpty(response.url)) "" else response.url.replace(
                                    MainApplication.getInstance().baseUrl,
                                    ""
                                )
                            val dynModels = JsonUtil.jsonToBean(
                                response.message,
                                OrderList::class.java
                            ) as OrderList
                            for (orderInfo in dynModels.data) {
                                if (isRefund == 0) {
                                    val tradeTimeNew = orderInfo.tradeTimeNew
                                    if (!StringUtil.isEmptyOrNull(tradeTimeNew)) {
                                        try {
                                            orderInfo.setFormatTimePay(
                                                DateUtil.formartDateYYMMDD(
                                                    tradeTimeNew
                                                )
                                            )
                                            orderInfo.setFormartYYMM(
                                                DateUtil.formartDateYYMMDDTo(
                                                    tradeTimeNew
                                                )
                                            )
                                            orderInfo.setTradeTimeNew(
                                                DateUtil.formartDateToHHMMSS(
                                                    tradeTimeNew
                                                )
                                            )
                                        } catch (e: ParseException) {
                                            uploadNetInterfaceException(
                                                e,
                                                sentryUrl,
                                                "",
                                                ""
                                            )
                                        }
                                    }
                                } else if (isRefund == 1) {
                                    val time = orderInfo.addTimeNew
                                    orderInfo.setMoney(orderInfo.refundFee)
                                    if (!StringUtil.isEmptyOrNull(time)) {
                                        try {
                                            orderInfo.setFormatRefund(
                                                DateUtil.formartDateYYMMDD(
                                                    time
                                                )
                                            )
                                            orderInfo.setFormartYYMM(
                                                DateUtil.formartDateYYMMDDTo(
                                                    time
                                                )
                                            )
                                            orderInfo.setAddTimeNew(
                                                DateUtil.formartDateToHHMMSS(
                                                    time
                                                )
                                            )
                                        } catch (e: ParseException) {
                                            uploadNetInterfaceException(
                                                e,
                                                sentryUrl,
                                                "",
                                                ""
                                            )
                                        }
                                    }
                                } else {
                                    val useTimeNew = orderInfo.useTimeNew
                                    if (!StringUtil.isEmptyOrNull(useTimeNew)) {
                                        try {
                                            orderInfo.setFromatCard(
                                                DateUtil.formartDateYYMMDD(
                                                    useTimeNew
                                                )
                                            )
                                            orderInfo.setFormartYYMM(
                                                DateUtil.formartDateYYMMDDTo(
                                                    useTimeNew
                                                )
                                            )
                                            orderInfo.setUseTimeNew(
                                                DateUtil.formartDateToHHMMSS(
                                                    useTimeNew
                                                )
                                            )
                                        } catch (e: ParseException) {
                                            uploadNetInterfaceException(
                                                e,
                                                sentryUrl,
                                                "",
                                                ""
                                            )
                                        }
                                    }
                                }
                            }
                            try {
                                val reqFeqTime: Int =
                                    if (TextUtils.isEmpty(dynModels.reqFeqTime)) 0 else dynModels.reqFeqTime.toInt()
                                if (dynModels.data.size > 0 && dynModels.data[0] != null) {
                                    dynModels.data[0].setReqFeqTime(reqFeqTime)
                                } else {
                                    if (reqFeqTime > 0) {
                                        uploadNetInterfaceException(
                                            Exception("时间错误 reqFeqTime > 0"),
                                            sentryUrl,
                                            "reqFeqTime=$reqFeqTime",
                                            ""
                                        )
                                        view.querySpayOrderFailed("reqFeqTime=$reqFeqTime")
                                    }
                                }
                            } catch (e: Exception) {
                                uploadNetInterfaceException(
                                    e,
                                    sentryUrl,
                                    "",
                                    ""
                                )
                            }
                            view.querySpayOrderSuccess(dynModels.data)
                        }
                    }
                }
            )
        }
    }

    override fun synchronizeOrder(authNo: String?, outRequestNo: String?) {
        mView?.let { view ->
            view.showLoading(R.string.loading, ParamsConstants.COMMON_LOADING)
            AppClient.synchronizeOrder(
                Constant.PAY_ZFB_QUERY,
                MainApplication.getInstance().getUserId().toString(),
                authNo,
                outRequestNo,
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {


                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.synchronizeOrderFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"", "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message, Order::class.java
                            ) as Order
                            order.tradeState =
                                if (TextUtils.isEmpty(order.state)) 0 else order.state.toInt()
                            order.tradeType = order.service
                            order.openid = order.subOpenID
                            order.cashFeel =
                                if (TextUtils.isEmpty(order.cashFee)) 0 else order.cashFee.toLong()
                            view.synchronizeOrderSuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: PreAuthDetailsContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}