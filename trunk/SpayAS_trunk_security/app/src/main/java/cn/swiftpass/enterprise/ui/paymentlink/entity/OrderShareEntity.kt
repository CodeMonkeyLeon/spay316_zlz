package cn.swiftpass.enterprise.ui.paymentlink.entity

import java.io.Serializable

data class OrderShareEntity(
    var imgId: Int = 0,
    var text: String = "",
    val type: Int = 0
) : Serializable {
    companion object {

        const val TYPE_EMAIL = 0
        const val TYPE_MESSAGE = 1
        const val TYPE_LINK = 2
//        const val TYPE_WE_CHAT = 3
//        const val TYPE_WE_COME = 4
        const val TYPE_MORE = 5


    }
}
