package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created by aijingya on 2019/4/9.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: ${TODO}(预授权订单列表页当天数据统计)
 * @date 2019/4/9.15:35.
 */
public class PreAuthDailyStatisticsBean implements Serializable {
    private   Long preAuthorizationAmount;    //预授权金额
    private   int preAuthorizationCount;    //	预授权笔数
    private   Long receiptAmount;//	收款金额
    private   int receiptCount;//	收款笔数
    private   Long unfreezeAmount;//解冻金额
    private   int unfreezeCount; //	解冻笔数


    public Long getPreAuthorizationAmount() {
        return preAuthorizationAmount;
    }

    public void setPreAuthorizationAmount(Long preAuthorizationAmount) {
        this.preAuthorizationAmount = preAuthorizationAmount;
    }

    public int getPreAuthorizationCount() {
        return preAuthorizationCount;
    }

    public void setPreAuthorizationCount(int preAuthorizationCount) {
        this.preAuthorizationCount = preAuthorizationCount;
    }

    public Long getReceiptAmount() {
        return receiptAmount;
    }

    public void setReceiptAmount(Long receiptAmount) {
        this.receiptAmount = receiptAmount;
    }

    public int getReceiptCount() {
        return receiptCount;
    }

    public void setReceiptCount(int receiptCount) {
        this.receiptCount = receiptCount;
    }

    public Long getUnfreezeAmount() {
        return unfreezeAmount;
    }

    public void setUnfreezeAmount(Long unfreezeAmount) {
        this.unfreezeAmount = unfreezeAmount;
    }

    public int getUnfreezeCount() {
        return unfreezeCount;
    }

    public void setUnfreezeCount(int unfreezeCount) {
        this.unfreezeCount = unfreezeCount;
    }

}
