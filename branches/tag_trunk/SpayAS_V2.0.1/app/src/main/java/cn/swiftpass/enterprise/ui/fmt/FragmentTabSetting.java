package cn.swiftpass.enterprise.ui.fmt;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tencent.stat.StatService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bean.QRCodeBean;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.shop.PersonalManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.upgrade.UpgradeManager;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.MerchantTempDataModel;
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.CashierManager;
import cn.swiftpass.enterprise.ui.activity.ChanagePwdActivity;
import cn.swiftpass.enterprise.ui.activity.ContentTextActivity;
import cn.swiftpass.enterprise.ui.activity.SetPayMethodActivity;
import cn.swiftpass.enterprise.ui.activity.SettingActivity;
import cn.swiftpass.enterprise.ui.activity.SettingMoreActivity;
import cn.swiftpass.enterprise.ui.activity.StaticCodeActivity;
import cn.swiftpass.enterprise.ui.activity.UpgradeDailog;
import cn.swiftpass.enterprise.ui.activity.scan.CaptureActivity;
import cn.swiftpass.enterprise.ui.activity.scan.CodeListActivity;
import cn.swiftpass.enterprise.ui.activity.scan.ScanbindActivity;
import cn.swiftpass.enterprise.ui.activity.setting.SettingAboutActivity;
import cn.swiftpass.enterprise.ui.activity.shop.ShopkeeperActivity;
import cn.swiftpass.enterprise.ui.activity.spayMainTabActivity;
import cn.swiftpass.enterprise.ui.activity.user.GoodsNameSetting;
import cn.swiftpass.enterprise.ui.activity.user.ReplactTelActivity;
import cn.swiftpass.enterprise.ui.activity.user.TuiKuanPwdSettingActivity;
import cn.swiftpass.enterprise.ui.widget.CommonConfirmDialog;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * Created by aijingya on 2018/5/8.
 *
 * @Package cn.swiftpass.enterprise.ui.fmt
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/5/8.11:07.
 */

public class FragmentTabSetting extends BaseFragment implements View.OnClickListener{
    private static final String TAG = FragmentTabSetting.class.getSimpleName();
    private static final String FAQPORT= "https://app.wepayez.com/web/faq/index.html";
    private spayMainTabActivity mActivity;

    private LinearLayout llChangePwd, llShopInfo, ll_E_Card, empManager, replaceTel, lay_scan;
    private View lineChangePwd;
    private TextView tv_debug, textView1, textView2;
    private View ll_shopinfo, sum_id, empManager_id, ll_shopinfo_id;

    private SharedPreferences sp;
    private LinearLayout bodyLay, write_off, ll_static_code, lay_prize;
    private View marketing_id;
    private LinearLayout marketingLay, lay_choise, ly_pay_method,ll_setting_more,ll_FAQ;
    private TextView tv_mch_name, tv_mch_id;
    private ImageView iv_title;

    DynModel dynModel;

    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_setting_new, container, false);

        sp = mContext.getSharedPreferences("login", 0);

        initViews(view);

        dynModel = (DynModel) SharedPreUtile.readProduct("dynModel" + ApiConstant.bankCode);

        return view;

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof spayMainTabActivity){
            mActivity=(spayMainTabActivity) activity;
        }
    }

    /**
     * 修改密码
     */
    public void onChangePwd() {
        ChanagePwdActivity.startActivity(mContext);
    }


    private void initViews(View view ) {
        codes = new ArrayList<QRCodeBean>();
        iv_title = view.findViewById(R.id.iv_title);
        tv_mch_name = view.findViewById(R.id.tv_mch_name);
        tv_mch_id = view.findViewById(R.id.tv_mch_id);

        ll_setting_more = view.findViewById(R.id.ll_setting_more);
        ll_setting_more.setOnClickListener(this);

        ll_FAQ = view.findViewById(R.id.ll_FAQ);
        ll_FAQ.setOnClickListener(this);

        ly_pay_method = view.findViewById(R.id.ly_pay_method);
        ly_pay_method.setOnClickListener(this);

        sum_id = view.findViewById(R.id.sum_id);
        lay_choise = view.findViewById(R.id.lay_choise);
        lay_choise.setOnClickListener(this);
        marketingLay = view.findViewById(R.id.marketingLay);
        lay_prize = view.findViewById(R.id.lay_prize);
        marketingLay.setOnClickListener(this);
        marketing_id = view.findViewById(R.id.marketing_id);
        lay_prize.setOnClickListener(this);

        ll_shopinfo_id = view.findViewById(R.id.ll_shopinfo_id);
        empManager_id = view.findViewById(R.id.empManager_id);

        ll_static_code = view.findViewById(R.id.ll_static_code);
        ll_static_code.setOnClickListener(this);
        lay_scan = view.findViewById(R.id.lay_scan);
        lay_scan.setOnClickListener(this);

    /*    tv_debug = view.findViewById(R.id.tv_debug);
        tv_debug.setText((GlobalConstant.isDebug == true ? "(" + getString(R.string.tx_try_version) + ")" : ""));
        tv_debug.setTextColor(Color.BLUE);
*/
        empManager = view.findViewById(R.id.empManager);
        empManager.setOnClickListener(this);

        ll_E_Card = view.findViewById(R.id.ll_E_Card);
        ll_E_Card.setOnClickListener(this);

        if(MainApplication.isCardOpen == 1){ //开通电子卡的功能
            ll_E_Card.setVisibility(View.VISIBLE);
        }else{
            ll_E_Card.setVisibility(View.GONE);
        }

        replaceTel = view.findViewById(R.id.replaceTel);
        replaceTel.setOnClickListener(this);


        bodyLay = view.findViewById(R.id.bodyLay);
        bodyLay.setOnClickListener(this);
        write_off = view.findViewById(R.id.write_off);
        write_off.setOnClickListener(this);


//        textView2 = view.findViewById(R.id.textView2);
       /* if (MainApplication.isAdmin.equals("1")) {

            if (MainApplication.mchName != null && !MainApplication.mchName.equals("null")) {
                textView2.setText(MainApplication.mchName);
            }
        } else {
            if (!TextUtils.isEmpty(MainApplication.realName)) {
                textView2.setText(MainApplication.realName);
            }
        }*/

        llChangePwd = view.findViewById(R.id.ll_changePwd);
        lineChangePwd = view.findViewById(R.id.line_changePwd);

        llChangePwd.setOnClickListener(this);
        ll_shopinfo = view.findViewById(R.id.ll_shopinfo);

        llShopInfo = view.findViewById(R.id.ll_shopinfo);
        if ("".equals(MainApplication.pwd_tag)) {
            llChangePwd.setVisibility(View.GONE);
            lineChangePwd.setVisibility(View.GONE);

        }
        if (MainApplication.isAdmin.equals("0")) {
            tv_mch_name.setText(MainApplication.realName);
            tv_mch_id.setText(PreferenceUtil.getString("user_name", MainApplication.userName));
            iv_title.setVisibility(View.GONE);
            ll_shopinfo.setEnabled(false);

        } else {
            ll_shopinfo.setEnabled(true);
            tv_mch_name.setText(MainApplication.getMchName());
            tv_mch_id.setText(MainApplication.getMchId());
        }
        llShopInfo.setOnClickListener(this);
        checkVersion(true);


        // 收银员 商户资料和收银员管理，退款管理 隐藏
        if (MainApplication.isAdmin.equals("0")) {
            sum_id.setVisibility(View.GONE);
            empManager.setVisibility(View.GONE);
            llShopInfo.setVisibility(View.GONE);
            bodyLay.setVisibility(View.GONE);
            ll_shopinfo.setVisibility(View.VISIBLE);

            empManager_id.setVisibility(View.GONE);
            ll_shopinfo_id.setVisibility(View.GONE);

            marketingLay.setVisibility(View.GONE);
            marketing_id.setVisibility(View.GONE);

        }
    }

    /**
     * 进入帮助页
     */
    public void onHelpPage() {
        try {
            StatService.trackCustomEvent(mContext, "SPConstTapFQAManager", "常见问题");
        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }

        if (null != dynModel && !StringUtil.isEmptyOrNull(dynModel.getQuestionLink())) {
            ContentTextActivity.startActivity(mContext, dynModel.getQuestionLink(), R.string.title_common_question);
        } else {
            try {
                String language = PreferenceUtil.getString("language", "");
                if (!TextUtils.isEmpty(language)) {
//                    ContentTextActivity.startActivity(mContext, "https://app.wepayez.com/web/faq/index.html?language=" + language + "&bank="+ApiConstant.bankName, R.string.title_common_question);
                    ContentTextActivity.startActivity(mContext, FAQPORT+"?language=" + language + "&bank="+ApiConstant.bankName, R.string.title_common_question);

                } else {
                    String lan = Locale.getDefault().toString();
                    if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_HANS)) {
//                        ContentTextActivity.startActivity(mContext, "https://app.wepayez.com/web/faq/index.html?language=zh_cn&bank="+ApiConstant.bankName, R.string.title_common_question);
                        ContentTextActivity.startActivity(mContext, FAQPORT+"?language=zh_cn&bank="+ApiConstant.bankName, R.string.title_common_question);

                    } else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO) ||
                            lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_HANT)) {
//                        ContentTextActivity.startActivity(mContext, "https://app.wepayez.com/web/faq/index.html?language=zh_tw&bank="+ApiConstant.bankName, R.string.title_common_question);
                        ContentTextActivity.startActivity(mContext, FAQPORT+"?language=zh_tw&bank="+ApiConstant.bankName, R.string.title_common_question);

                    } else {
//                        ContentTextActivity.startActivity(mContext, "https://app.wepayez.com/web/faq/index.html?language=en_us&bank="+ApiConstant.bankName, R.string.title_common_question);
                        ContentTextActivity.startActivity(mContext, FAQPORT+"?language=en_us&bank="+ApiConstant.bankName, R.string.title_common_question);

                    }

                }

            } catch (Exception e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }
        }

        //        String language = PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN);
        //
        //        Locale locale = getResources().getConfiguration().locale; //zh-rHK
        //        String lan = locale.getCountry();
        //
        //        if (!TextUtils.isEmpty(language))
        //        {
        //
        //            if (language.equals(MainApplication.LANG_CODE_ZH_TW))
        //            {
        //            }
        //            else
        //            {
        //                ContentTextActivity.startActivity(mContext, ApiConstant.BASE_URL_PORT
        //                    + "spay/public/question_zh_cn.html?bankCode=" + ApiConstant.bankCode + "&tel="
        //                    + R.string.qustion_company_phone, R.string.title_common_question);
        //            }
        //
        //        }
        //        else
        //        {
        //            if (lan.equalsIgnoreCase("CN"))
        //            {
        //                ContentTextActivity.startActivity(mContext, ApiConstant.BASE_URL_PORT
        //                    + "spay/public/question_zh_cn.html?bankCode=" + ApiConstant.bankCode + "&tel="
        //                    + R.string.qustion_company_phone, R.string.title_common_question);
        //            }
        //            else
        //            {
        //                ContentTextActivity.startActivity(mContext, ApiConstant.BASE_URL_PORT
        //                    + "spay/public/question_zh_tw.html?bankCode=" + ApiConstant.bankCode + "&tel="
        //                    + R.string.qustion_company_phone, R.string.title_common_question);
        //            }
        //        }

    }

    /**
     * 检查版本显示提示图标
     */
    public void checkVersion(final boolean isDisplayIcon) {
        // 不用太平凡的检测升级 所以使用时间间隔区分
        UpgradeManager.getInstance().getVersonCode(new UINotifyListener<UpgradeInfo>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                // titleBar.setRightLodingVisible(true);
                showLoading(false, R.string.show_new_version_loading);
            }

            @Override
            public void onError(Object object) {
                super.onError(object);
                dismissLoading();
            }

            @Override
            public void onSucceed(UpgradeInfo result) {
                super.onSucceed(result);
                dismissLoading();
                if (null != result) {
                    if (isDisplayIcon) {
                        //upVerInfo = result;
                        //                        ivNewVersion.setVisibility(View.VISIBLE);
                    } else {
                        showUpgradeInfoDialog(result, new ComDialogListener(result));
                    }
                } else {
                    if (!isDisplayIcon) {
                        showToastInfo(R.string.show_no_version);
                    }
                }
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //设置默认支付方式
            case R.id.ly_pay_method:
                showPage(SetPayMethodActivity.class);
                break;
            //设置
            case R.id.ll_setting_more:
                showPage(SettingMoreActivity.class);
                break;

            //关于我们
            case R.id.lay_choise:
                showPage(SettingAboutActivity.class);
                break;
            //关于我们
            case R.id.ll_FAQ:
                try {
                    StatService.trackCustomEvent(mContext, "kMTASPayMeFAQ", "常见问题的入口");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
                onHelpPage();
                break;

            //固定二维码收款
            case R.id.ll_static_code:
                 getData();

                break;
            // 扫一扫退款
            case R.id.lay_scan:
                CaptureActivity.startActivity(mContext, MainApplication.PAY_TYPE_REFUND);
                break;

            case R.id.bodyLay://修改商品名称
                //                if (MainApplication.isAdmin.equals("1") && MainApplication.remark.equals("1"))
                //                {

                //                if (null == MainApplication.body)
                //                {
                //                    showPage(GoodsNameSetting.class);
                //                    return;
                //                }

                showPage(GoodsNameSetting.class);
                break;
            case R.id.ll_changePwd:
                onChangePwd();
                break;
            case R.id.replaceTel:
                showPage(ReplactTelActivity.class);
                break;

            case R.id.ll_E_Card://点击电子卡
                //添加cookic
                String login_skey = PreferenceUtil.getString("login_skey", "");
                String login_sauthid = PreferenceUtil.getString("login_sauthid", "");
                String url = ApiConstant.BASE_URL_PORT+"spay/sm/memberShipCard/login";
                ContentTextActivity.startActivityWithCookie(mContext, url, R.string.E_Card,login_skey,login_sauthid);
                break;
            case R.id.empManager:
                try {
                    StatService.trackCustomEvent(mContext, "SPConstTapCaisherManager", "收银员管理");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
                showPage(CashierManager.class);
                break;
            case R.id.ll_shopinfo:
                try {
                    StatService.trackCustomEvent(mContext, "kMTASPayMeMerchantInfo", "商户详情");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
                PersonalManager.getInstance().queryMerchantDataByTel(MainApplication.phone, new UINotifyListener<MerchantTempDataModel>() {
                    @Override
                    public void onPreExecute() {
                        super.onPreExecute();
                        // titleBar.setRightLodingVisible(true);
                        showLoading(false, R.string.public_loading);
                    }

                    @Override
                    public void onError(Object object) {
                        super.onError(object);
                        dismissLoading();
                        if (object != null) {
                            toastDialog(mActivity, String.valueOf(object), null);
                        }
                    }

                    @Override
                    public void onSucceed(MerchantTempDataModel result) {
                        super.onSucceed(result);
                        dismissLoading();
                        if (null != result) {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("merchantData", result);
                            showPage(ShopkeeperActivity.class, bundle);
                        }

                    }
                });

                break;
        }
    }

    private ArrayList<QRCodeBean> codes;
    private String bindUserId = "";
    private void getData(){
        codes.clear();
        if (MainApplication.isAdmin.equals("0"))
        {
            if (MainApplication.userId > 0) {
                bindUserId = MainApplication.userId+"";
            } else {
                bindUserId = MainApplication.getUserId()+"";
            }
        }
        OrderManager.getInstance().GetCodeList(1,20,bindUserId,new UINotifyListener<List<QRCodeBean>>(){
            @Override
            public void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                dismissLoading();
                if (checkSession()) {
                    return;
                }
                if (object != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toastDialog(getActivity(), object.toString(), null);
                        }
                    });
                }
            }

            @Override
            public void onSucceed(List<QRCodeBean> result) {
                super.onSucceed(result);
                dismissLoading();
                if (result.size() != 0){
                    codes.addAll(result);
                    /**
                     * size =1 跳转到详情页
                     * size >1 跳转到列表页
                     */
                    if (result.size() == 1){
                        go2StaticCodeActivity(result.get(0));
                    }else{
                        go2CodeListActivity();
                    }
                }else{
                    go2CodeListActivity();
                }
            }
        });
    }

    /**
     * 跳转到固码页面
     * @param qrCodeBean
     */
    private void go2StaticCodeActivity(QRCodeBean qrCodeBean){
        Intent intent = new Intent(getActivity(), StaticCodeActivity.class);
        qrCodeBean.switch_type = 2;
        intent.putExtra("QRCodeBean",qrCodeBean);
        this.startActivity(intent);
    }

    /**
     * 跳转到二维码列表页面
     */
    private void go2CodeListActivity(){
        Intent intent = new Intent(getActivity(), CodeListActivity.class);
        this.startActivity(intent);
    }

    public ArrayList<QRCodeBean> getCodes(){
        return codes;
    }

    /**
     * 升级点击事件
     */
    class ComDialogListener implements CommonConfirmDialog.ConfirmListener {
        private UpgradeInfo result;

        public ComDialogListener(UpgradeInfo result) {
            this.result = result;
        }

        @Override
        public void ok() {
            new UpgradeDailog(mActivity, result, new UpgradeDailog.UpdateListener() {
                @Override
                public void cancel() {

                }
            }).show();
        }

        @Override
        public void cancel() {
            // LocalAccountManager.getInstance().saveCheckVersionTime();

        }

    }

    public void changeRefundPwd(View view) {
        TuiKuanPwdSettingActivity.startActivity(mContext);
    }

    @Override
    protected boolean isLoginRequired() {
        return false;
    }
}
