package cn.swiftpass.enterprise.ui.paymentlink.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.paymentlink.SimpleTextWatcher;
import cn.swiftpass.enterprise.ui.paymentlink.model.OtherFee;
import cn.swiftpass.enterprise.ui.paymentlink.widget.SlideItemView;
import cn.swiftpass.enterprise.ui.widget.BaseRecycleAdapter;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.EditTextDigitsFilter;
import cn.swiftpass.enterprise.utils.OtherFeeInputFilter;

/**
 * Created by congwei.li on 2021/9/16.
 *
 * @Description: 创建订单界面 新增其他费用adapter
 */
public class OtherFeeListAdapter extends BaseRecycleAdapter<OtherFee> {

    private Context mContext;
    private OnListItemClickListener clickListener;
    private TextChange textChange;
    private InputFilter[] nameInputFilter;
    private InputFilter[] amountInputFilter;
    private SlideItemView openItem;

    public void setItemClickListener(OnListItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void setTextChange(TextChange textChange) {
        this.textChange = textChange;
    }

    public void cleanItemClickListener() {
        this.clickListener = null;
    }

    public OtherFeeListAdapter(Context context, List<OtherFee> data) {
        super(data);
        mContext = context;
        nameInputFilter = new InputFilter[]{new InputFilter.LengthFilter(20)};
        amountInputFilter = new InputFilter[]{new EditTextDigitsFilter("0123456789-."),
                new OtherFeeInputFilter(), new InputFilter.LengthFilter(20)};
    }

    @Override
    protected void bindData(BaseViewHolder holder, int position) {
        OtherFee data = datas.get(position);

        EditText etName = (EditText) holder.getView(R.id.et_other_fee_name);
        EditText etAmount = (EditText) holder.getView(R.id.et_other_fee_amount);
        TextView feeFh = (TextView) holder.getView(R.id.tv_fee_type);

        feeFh.setText(MainApplication.getInstance().getFeeFh() + " ");
        if (!TextUtils.isEmpty(data.extraCostDesc)) {
            etName.setText(data.extraCostDesc);
        } else {
            etName.setText("");
        }
        if (!TextUtils.isEmpty(data.extraCost)) {
            etAmount.setText(DateUtil.formatMoney(new BigDecimal(data.extraCost)));
        } else {
            etAmount.setText("");
        }
        etName.setFilters(nameInputFilter);
        etAmount.setFilters(amountInputFilter);

        if (!TextUtils.isEmpty(data.extraCost)) {
            feeFh.setTextColor(mContext.getResources().getColor(R.color.black));
        }

        SimpleTextWatcher watcherName = new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (textChange != null) {
                    textChange.onNameEdit(position, etName.getText().toString().trim());
                }
            }
        };
        SimpleTextWatcher watcherAmount = new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (textChange != null) {
                    textChange.onAmountEdit(position, etAmount.getText().toString().trim());
                }
            }
        };
        etName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etName.addTextChangedListener(watcherName);
                } else {
                    etName.removeTextChangedListener(watcherName);
                }
            }
        });
        etAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    etAmount.addTextChangedListener(watcherAmount);
                    feeFh.setTextColor(mContext.getResources().getColor(R.color.black));
                } else {
                    etAmount.removeTextChangedListener(watcherAmount);
                    if (TextUtils.isEmpty(data.extraCost)) {
                        feeFh.setTextColor(mContext.getResources().getColor(R.color.color_A3A3A3));
                    } else {
                        feeFh.setTextColor(mContext.getResources().getColor(R.color.black));
                    }
                }
            }
        });
        SlideItemView view = (SlideItemView) holder.itemView;

        view.setOnStateChangeListener(new SlideItemView.OnStateChangeListener() {
            @Override
            public void onFuncShow(SlideItemView itemView, boolean isShow) {
                if (isShow) {
                    if (null != openItem && itemView != openItem) {
                        openItem.reset();
                        openItem = itemView;
                    } else if (null == openItem) {
                        openItem = itemView;
                    }
                } else {
                    if (null != openItem && itemView == openItem) {
                        openItem = null;
                    }
                }
            }

            @Override
            public void onFuncClick() {
                if (null != openItem && openItem.isExpansion()) {
                    openItem.reset();
                    openItem = null;
                }
                if (clickListener != null) {
                    clickListener.onDeleteClick(position);
                }
            }

            @Override
            public void onContentClick() {
                if (clickListener != null) {
                    clickListener.onItemClick(position);
                }
            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_add_order_other_fee;
    }

    public interface TextChange {
        void onNameEdit(int position, String name);

        void onAmountEdit(int position, String amount);
    }
}
