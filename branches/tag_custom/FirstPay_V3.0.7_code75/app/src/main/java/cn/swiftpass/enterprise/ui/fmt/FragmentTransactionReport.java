package cn.swiftpass.enterprise.ui.fmt;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.bill.BillOrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.TransactionReportBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TransactionDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.list.NewPullDownListView;
import cn.swiftpass.enterprise.ui.activity.list.PullToRefreshBase;
import cn.swiftpass.enterprise.ui.activity.list.PullToRefreshListView;
import cn.swiftpass.enterprise.ui.activity.spayMainTabActivity;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * Created by aijingya on 2019/4/12.
 *
 * @Package cn.swiftpass.enterprise.ui.fmt
 * @Description: ${TODO}(日报，周报，月报的fragment)
 * @date 2019/4/12.16:01.
 */
public class FragmentTransactionReport extends BaseFragment {
    private static final String TAG = FragmentTransactionReport.class.getSimpleName();
    private spayMainTabActivity mActivity;

    private RelativeLayout rl_transaction_report;
    private PullToRefreshListView mPullRefreshListView;
    private TextView tv_no_transaction_data;
    private ViewHolder holder;
    private ListView listView;
    private TransactionReportAdapter transactionReportAdapter;
    private List<TransactionReportBean> transactionReportBeanList = new ArrayList<TransactionReportBean>();

    //报表类型 1-日报表 2-周报表 3-月报表
    public String Enum_Daily = "1";
    public String Enum_Weekly = "2";
    public String Enum_Monthly = "3";

    public String reportType = "0";

    public void setFragmentReportType(String Type){
        this.reportType = Type;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof spayMainTabActivity){
            mActivity=(spayMainTabActivity) activity;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_transaction_report, container, false);
        initViews(view);
        return view;

    }

    public void  initViews(View view){
        rl_transaction_report = view.findViewById(R.id.rl_transaction_report);
        mPullRefreshListView = view.findViewById(R.id.pull_to_refresh);
        tv_no_transaction_data = view.findViewById(R.id.tv_no_transaction_data);
        listView = mPullRefreshListView.getRefreshableView();
        transactionReportAdapter = new TransactionReportAdapter(transactionReportBeanList);
        listView.setAdapter(transactionReportAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //带数据跳转到详情页
                Intent intent = new Intent(getContext(),TransactionDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("TransactionReportList",(Serializable)transactionReportBeanList);
                bundle.putInt("position", position);
                bundle.putString("reportType",reportType);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        PullToRefreshBase.OnRefreshListener mOnrefreshListener = new PullToRefreshBase.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(mPullRefreshListView.getRefreshType() == PullToRefreshBase.MODE_PULL_DOWN_TO_REFRESH){
                    transactionReportBeanList.clear();
                    loadDate(reportType);
                }else if(mPullRefreshListView.getRefreshType() == PullToRefreshBase.MODE_PULL_UP_TO_REFRESH){
                    mPullRefreshListView.onRefreshComplete();
                }

            }
        };
        mPullRefreshListView.setOnRefreshListener(mOnrefreshListener);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (transactionReportBeanList.size() == 0) {
            transactionReportBeanList.clear();
            loadDate(reportType);
        }
    }

    @Override
    protected int getLayoutId() {
        return 0;
    }


    private class TransactionReportAdapter extends BaseAdapter{
        private List<TransactionReportBean> transactionReportBeanList;

        public TransactionReportAdapter(){

        }

        public TransactionReportAdapter(List<TransactionReportBean> transactionReportBeanList) {
            this.transactionReportBeanList = transactionReportBeanList;
        }

        @Override
        public int getCount() {
            return transactionReportBeanList.size();
        }

        @Override
        public Object getItem(int position) {
            return transactionReportBeanList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(mContext, R.layout.item_listview_report, null);
                holder = new ViewHolder();
                holder.tv_report_data = convertView.findViewById(R.id.tv_report_data);
                holder.tv_total_amount = convertView.findViewById(R.id.tv_total_amount);
                holder.tv_fee_type = convertView.findViewById(R.id.tv_fee_type);
                holder.tv_money_transaction_report = convertView.findViewById(R.id.tv_money_transaction_report);
                holder.tv_rate_text = convertView.findViewById(R.id.tv_rate_text);
                holder.tv_rate_value = convertView.findViewById(R.id.tv_rate_value);
                holder.report_total_count = convertView.findViewById(R.id.report_total_count);
                holder.report_total_count_value = convertView.findViewById(R.id.report_total_count_value);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            TransactionReportBean reportBean = transactionReportBeanList.get(position);
            if(reportBean != null){
                if(reportType.equalsIgnoreCase(Enum_Daily) || reportType.equalsIgnoreCase(Enum_Monthly)){//如果是日报和月报则只显示一个时间
                    holder.tv_report_data.setText(reportBean.getStartDate());
                }else if(reportType.equalsIgnoreCase(Enum_Weekly) ){//如果是周报，则显示时间段
                    holder.tv_report_data.setText(reportBean.getStartDate() +" "
                            +getResources().getString(R.string.data_to)+" "+reportBean.getEndDate());
                }

                if(!TextUtils.isEmpty(MainApplication.getFeeFh())){
                    holder.tv_fee_type.setText(MainApplication.getFeeFh());
                }

                holder.tv_money_transaction_report.setText(DateUtil.formatMoneyUtils(reportBean.getTransactionAmount()));

                if(reportType.equalsIgnoreCase(Enum_Daily)){//如果是日报则文案为DoD
                    holder.tv_rate_text.setText(getString(R.string.report_day_to_day));
                }else if(reportType.equalsIgnoreCase(Enum_Weekly) ){//如果是周报则显示为WoW
                    holder.tv_rate_text.setText(getString(R.string.report_week_to_week));
                }else if(reportType.equalsIgnoreCase(Enum_Monthly)){//如果是月报则显示为MoM
                    holder.tv_rate_text.setText(getString(R.string.report_month_to_month));
                }

                holder.tv_rate_value.setText(DateUtil.formatPaseRMBMoney(reportBean.getTransactionRelativeRatio()*100.00f) +"%");

                holder.report_total_count_value.setText(reportBean.getTransactionCount()+"");
            }

            return convertView;
        }
    }

    private class ViewHolder {
        private TextView  tv_report_data, tv_total_amount, tv_fee_type,tv_money_transaction_report;
        private TextView tv_rate_text,tv_rate_value,report_total_count,report_total_count_value;
    }

    /*
     * SPAY 今日数据消费统计
     * 报表列表页去请求日报，周报，月报列表的接口
     * 报表类型reportType  1-日报表 2-周报表 3-月报表
     * */
    public void loadDate(String reportType){
        BillOrderManager.getInstance().QueryTransactionReport(reportType,new UINotifyListener<List<TransactionReportBean>>(){

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loadDialog(mActivity, R.string.public_data_loading);
            }

            @Override
            protected void onPostExecute() {
                super.onPostExecute();
                dismissLoading();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                mPullRefreshListView.onRefreshComplete();
                dismissLoading();
                if (checkSession()) {
                    return;
                }

                if(object != null){
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!StringUtil.isEmptyOrNull(object.toString())) {
                                toastDialog(mActivity, object.toString(), null);
                            } else {
                                toastDialog(mActivity, R.string.tx_load_fail, null);
                            }

                            if (transactionReportBeanList.size() == 0) {
                                tv_no_transaction_data.setVisibility(View.VISIBLE);
                                mPullRefreshListView.setVisibility(View.GONE);
                                rl_transaction_report.setVisibility(View.GONE);
                            }
                        }
                    });
                }

            }

            @Override
            public void onSucceed(final List<TransactionReportBean> result) {
                super.onSucceed(result);
                dismissLoading();

                if(transactionReportBeanList != null){
                    transactionReportBeanList.clear();
                }

                if(result != null && result.size() > 0){
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tv_no_transaction_data.setVisibility(View.GONE);
                            mPullRefreshListView.setVisibility(View.VISIBLE);
                            rl_transaction_report.setVisibility(View.VISIBLE);

                            transactionReportBeanList.addAll(result);
                            transactionReportAdapter.notifyDataSetChanged();
                            mPullRefreshListView.onRefreshComplete();
                            listView.setSelection(0);
                        }
                    });
                }else{
                    mActivity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            tv_no_transaction_data.setVisibility(View.VISIBLE);
                            mPullRefreshListView.setVisibility(View.GONE);
                            rl_transaction_report.setVisibility(View.GONE);
                            mPullRefreshListView.onRefreshComplete();
                        }
                    });
                }
            }
        });
    }

}
