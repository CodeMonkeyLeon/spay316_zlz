package cn.swiftpass.enterprise.mvp.contract.fragment

import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView
import cn.swiftpass.enterprise.ui.paymentlink.model.*


/**
 * @author lizheng.zhao
 * @date 2022/11/17
 *
 * @你一会看我会看云我觉得你看我时很远你看云时很近
 */
class TabPaymentLinkContract {


    interface View : BaseView {

        fun deleteProductSuccess(response: String, position: Int)


        fun deleteProductFailed(error: Any?)


        fun deleteCustomerSuccess(response: String, position: Int)

        fun deleteCustomerFailed(error: Any?)


        fun getOrderListSuccess(response: PaymentLinkOrderModel)

        fun getOrderListFailed(error: Any?)


        fun getCustomerListSuccess(response: PaymentLinkCustomerModel)

        fun getCustomerListFailed(error: Any?)

        fun getProductListSuccess(response: PaymentLinkProductModel)

        fun getProductListFailed(error: Any?)

    }

    interface Presenter : BasePresenter<View> {


        fun getProductList(
            pageNumber: String?,
            goodsId: String?,
            goodsName: String?,
            goodsCode: String?
        )


        fun getCustomerList(
            pageNumber: String?,
            custName: String?,
            custMobile: String?,
            custEmail: String?
        )


        fun getOrderList(
            orderNo: String?,
            realPlatOrderNo: String?,
            custName: String?,
            orderRemark: String?,
            pageNumber: String?,
            order_status: String?
        )


        fun deleteCustomer(
            customer: PaymentLinkCustomer?,
            position: Int
        )


        fun deleteProduct(
            product: PaymentLinkProduct?,
            position: Int
        )
    }


}