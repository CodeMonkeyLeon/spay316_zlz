package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created by aijingya on 2019/4/22.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: ${TODO}(报表统计收银员top5 基本数据类型，按金额，笔数)
 * @date 2019/4/22.17:48.
 */
public class CashierTop5Bean implements Serializable {
    public String getCashierName() {
        return cashierName;
    }

    public void setCashierName(String cashierName) {
        this.cashierName = cashierName;
    }

    public Long getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Long transactionAmountTotal) {
        this.transactionAmount = transactionAmountTotal;
    }

    public int getTransactionCount() {
        return transactionCount;
    }

    public void setTransactionCount(int transactionCountTotal) {
        this.transactionCount = transactionCountTotal;
    }

    public String cashierName; //收银员名字
    public Long transactionAmount;//收银员金额
    public int transactionCount;//收银员笔数
}
