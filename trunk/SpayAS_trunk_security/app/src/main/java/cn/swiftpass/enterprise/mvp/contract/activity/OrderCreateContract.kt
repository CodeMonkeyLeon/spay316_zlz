package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView
import cn.swiftpass.enterprise.ui.paymentlink.model.OrderConfig

/**
 * @author lizheng.zhao
 * @date 2022/11/23
 *
 * @一生中
 * @我多次撒谎
 * @却始终诚实地遵守着
 * @一个儿时的诺言
 * @因此
 * @那与孩子的心
 * @不能相容的世界
 * @再也没有饶恕过我
 */
class OrderCreateContract {

    interface View : BaseView {

        fun getOrderConfigSuccess(response: OrderConfig)

        fun getOrderConfigFailed(error: Any?)
    }

    interface Presenter : BasePresenter<View> {
        fun getOrderConfig()
    }
}