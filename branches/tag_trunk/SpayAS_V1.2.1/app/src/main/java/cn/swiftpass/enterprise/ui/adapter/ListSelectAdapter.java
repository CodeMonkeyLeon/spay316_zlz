package cn.swiftpass.enterprise.ui.adapter;

import java.util.HashSet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.SelectListModel;
import cn.swiftpass.enterprise.ui.widget.SelectItemCheckView;

//选择 带checkbox
public class ListSelectAdapter extends ArrayListAdapter<SelectListModel>
{
    public HashSet<SelectListModel> checkData;
    
    public ListSelectAdapter(Context context, HashSet<SelectListModel> data)
    {
        super(context);
        if (data != null)
        { //还是判断一下好。。传来传去，模糊
            this.checkData = data;
        }
        else
        {
            this.checkData = new HashSet<SelectListModel>();
        }
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        SelectItemCheckView view = null;
        if (convertView != null && convertView instanceof SelectItemCheckView)
        {
            view = (SelectItemCheckView)convertView;
        }
        if (convertView == null)
        {
            view = (SelectItemCheckView)LayoutInflater.from(mContext).inflate(R.layout.listitem_select, null);
        }
        view.bindData((SelectListModel)getItem(position), checkData);
        return view;
    }
    
}
