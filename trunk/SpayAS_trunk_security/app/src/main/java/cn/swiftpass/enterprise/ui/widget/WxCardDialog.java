package cn.swiftpass.enterprise.ui.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.WxCard;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/***
 * 退款检查对话框
 */
public class WxCardDialog extends Dialog implements View.OnClickListener
{
    
    private Button wx_card_but;
    
    private ConfirmListener btnListener;
    
    private Activity mContext;
    
    private ListView card_list;
    
    private List<WxCard> vardOrders;
    
    public WxCardDialog(Activity context, List<WxCard> vardOrders, ConfirmListener btnListener)
    {
        super(context);
        mContext = context;
        this.btnListener = btnListener;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_wx_card);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        this.setCanceledOnTouchOutside(false);
        this.vardOrders = vardOrders;
        initView();
    }
    
    private void initView()
    {
        wx_card_but = (Button)findViewById(R.id.wx_card_but);
        card_list = (ListView)findViewById(R.id.card_list);
        //        List<String> list = new ArrayList<String>();
        //        list.add("华侨城入住满100打八折");
        //        list.add("世界之窗万圣节优惠活动八折优惠");
        //        list.add("华侨城入住满100打八折");
        
        if (vardOrders != null && vardOrders.size() > 4)
        {
            LayoutParams lp;
            lp = card_list.getLayoutParams();
            lp.height = 470;
            
            card_list.setLayoutParams(lp);
        }
        
        WxCardAdapter wxCardAdapter = new WxCardAdapter(mContext, vardOrders);
        
        card_list.setAdapter(wxCardAdapter);
        
        wx_card_but.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                dismiss();
                btnListener.cancel();
            }
        });
        
    }
    
    public interface ConfirmListener
    {
        public void cancel();
    }
    
    @Override
    public void onClick(View v)
    {
        
    }
    
    class WxCardAdapter extends BaseAdapter
    {
        
        private Context context;
        
        private List<WxCard> list;
        
        public WxCardAdapter(Context context, List<WxCard> list)
        {
            this.context = context;
            this.list = list;
        }
        
        @Override
        public int getCount()
        {
            return list.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            return list.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            MyHolder myHolder;
            WxCard str = list.get(position);
            if (convertView == null)
            {
                convertView = View.inflate(context, R.layout.wx_card_list_item, null);
                myHolder = new MyHolder(convertView);
                convertView.setTag(myHolder);
            }
            else
            {
                myHolder = (MyHolder)convertView.getTag();
            }
            
            myHolder.card_title.setText(str.getTitle());
            
            if (null != str.getCardType() && !"".equals(str.getCardType()))
            {
                
                if (str.getCardType().equalsIgnoreCase(Order.WxCardType.CARSH_CARD.cardType)) // 代金券
                {
                    
                    myHolder.card_dealDetail.setText(Order.WxCardType.getCardNameForType(str.getCardType()) + ","
                            + ToastHelper.toStr(R.string.tx_reduce_cost) + ":" + MainApplication.getInstance().getFeeFh() + " "
                            + DateUtil.formatMoneyUtils(str.getReduceCost()));
                }
                else if (str.getCardType().equalsIgnoreCase(Order.WxCardType.DISCOUNT_CARD.cardType))
                {
                    myHolder.card_dealDetail.setText(Order.WxCardType.getCardNameForType(str.getCardType()) + ","
                        + ToastHelper.toStr(R.string.et_discount) + ":" + str.getDiscount() + "%");
                }
                else if (str.getCardType().equalsIgnoreCase(Order.WxCardType.GIFT_CARD.cardType))
                {
                    myHolder.card_dealDetail.setText(Order.WxCardType.getCardNameForType(str.getCardType()) + ","
                        + str.getGift());
                    
                }
                else if (str.getCardType().equalsIgnoreCase(Order.WxCardType.GROUPON_CARD.cardType))
                {
                    myHolder.card_dealDetail.setText(Order.WxCardType.getCardNameForType(str.getCardType()) + ","
                        + str.getDealDetail());
                    
                }
                else
                {
                    myHolder.card_dealDetail.setText(Order.WxCardType.getCardNameForType(str.getCardType()));
                }
            }
            
            return convertView;
        }
        
    }
    
    private class MyHolder
    {
        private TextView card_title;
        
        private TextView card_dealDetail;
        
        public MyHolder(View v)
        {
            card_title = (TextView)v.findViewById(R.id.card_title);
            card_dealDetail = (TextView)v.findViewById(R.id.card_dealDetail);
        }
        
    }
    
}
