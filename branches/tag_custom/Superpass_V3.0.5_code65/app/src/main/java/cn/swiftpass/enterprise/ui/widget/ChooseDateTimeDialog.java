package cn.swiftpass.enterprise.ui.widget;

import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 选择开始日期 和 截止日期
 * User: Alan
 * Date: 14-1-8
 * Time: 下午8:29
 */
public class ChooseDateTimeDialog extends Dialog implements View.OnClickListener
{
    private ViewGroup mRootView;
    
    private LinearLayout btnStartDate, btnEndDate;
    
    private ConfirmListener btnListener;
    
    private TextView tvStartDate, tvEndDate;
    
    private Context mContext;
    
    private TextView btnCancel, btnOk;
    
    public ChooseDateTimeDialog(Context context, String title, ConfirmListener btnListener)
    {
        super(context);
        mContext = context;
        this.btnListener = btnListener;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.dialog_choose_datetime, null);
        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);
        
        TextView tvTitle = (TextView)findViewById(R.id.title);
        if (title != null)
        {
            tvTitle.setText(title);
        }
        
        tvStartDate = (TextView)findViewById(R.id.tv_startDate);
        tvEndDate = (TextView)findViewById(R.id.tv_endDate);
        
        tvStartDate.setText(DateUtil.formatYMD(System.currentTimeMillis()));
        tvEndDate.setText(DateUtil.formatYMD(System.currentTimeMillis()));
        
        btnStartDate = (LinearLayout)findViewById(R.id.ll_startDate);
        btnEndDate = (LinearLayout)findViewById(R.id.ll_endDate);
        
        btnOk = (TextView)findViewById(R.id.ok);
        btnCancel = (TextView)findViewById(R.id.cancel);
        
        btnStartDate.setOnClickListener(this);
        btnEndDate.setOnClickListener(this);
        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }
    
    public static void show(Activity context, String title, ConfirmListener btnListener)
    {
        ChooseDateTimeDialog dia = new ChooseDateTimeDialog(context, title, btnListener);
        dia.show();
    }
    
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.ll_startDate:
                showDatetimeDialog(true);
                break;
            case R.id.ll_endDate:
                showDatetimeDialog(false);
                break;
            case R.id.ok:
                if (btnListener != null)
                {
                    if (endDateTime < searchTime)
                    {
                        ToastHelper.showInfo("对不起，结束时间必须大于开始时间");
                    }
                    else
                    {
                        if (strTime == null && endTime != null)
                        {
                            btnListener.ok(DateUtil.formatYYMD(searchTime), endTime);
                        }
                        else if (endTime == null && strTime != null)
                        {
                            btnListener.ok(strTime, DateUtil.formatYYMD(endDateTime));
                        }
                        else if (strTime == null && endTime == null)
                        {
                            btnListener.ok(DateUtil.formatYYMD(searchTime), DateUtil.formatYYMD(endDateTime));
                        }
                        else
                        {
                            btnListener.ok(strTime, endTime);
                        }
                        dismiss();
                    }
                }
                
                break;
            case R.id.cancel:
                dismiss();
                break;
            default:
                break;
        }
        
    }
    
    public interface ConfirmListener
    {
        public void ok(String strStartDate, String strEndDate);
        
        public void cancel();
    }
    
    private Calendar c = null;
    
    private long searchTime = System.currentTimeMillis();
    
    private long endDateTime = System.currentTimeMillis();
    
    private DatePickerDialog dialog;
    
    private String strTime;
    
    private String endTime;
    
    public void showDatetimeDialog(final boolean flag)
    {
        c = Calendar.getInstance();
        if (flag)
        {
            c.setTimeInMillis(searchTime);
        }
        else
        {
            c.setTimeInMillis(endDateTime);
        }
        if (dialog != null && dialog.isShowing())
        {
            dialog.dismiss();
        }
        dialog = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener()
        {
            public void onDateSet(DatePicker dp, int year, int month, int dayOfMonth)
            {
                c.set(year, month, dayOfMonth);
                if (flag)
                {
                    searchTime = c.getTimeInMillis();
                }
                else
                {
                    endDateTime = c.getTimeInMillis();
                }
                String strTimeDate = "";
                if (flag)
                {
                    DateUtil.formatTime(searchTime);
                    if (DateUtil.isSameDay(System.currentTimeMillis(), searchTime))
                    {
                        strTimeDate = "今天";
                    }
                    else
                    {
                        c.set(year, month, dayOfMonth);
                        
                        strTimeDate = DateUtil.formatYMD(c.getTimeInMillis());
                        
                        // strTimeDate = year + "年" + (month + 1) + "月" + dayOfMonth + "日";
                    }
                }
                else
                {
                    if (DateUtil.isSameDay(System.currentTimeMillis(), endDateTime))
                    {
                        strTimeDate = "今天";
                    }
                    else
                    {
                        c.set(year, month, dayOfMonth);
                        strTimeDate = DateUtil.formatYMD(c.getTimeInMillis());
                        // strTimeDate = year + "年" + (month + 1) + "月" + dayOfMonth + "日";
                    }
                }
                
                if (flag)
                {
                    strTime = year + "-" + (month + 1) + "-" + dayOfMonth;
                    tvStartDate.setText(strTimeDate);
                }
                else
                {
                    
                    endTime = year + "-" + (month + 1) + "-" + dayOfMonth;
                    tvEndDate.setText(strTimeDate);
                }
            }
        }, c.get(Calendar.YEAR), // 传入年份
            c.get(Calendar.MONTH), // 传入月份
            c.get(Calendar.DAY_OF_MONTH) // 传入天数*/
            );
        dialog.show();
    }
}
