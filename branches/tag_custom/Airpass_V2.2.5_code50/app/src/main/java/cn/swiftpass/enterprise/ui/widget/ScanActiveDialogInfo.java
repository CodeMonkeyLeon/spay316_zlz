/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.widget;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;

/**
 * 弹出提示框
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-11]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class ScanActiveDialogInfo extends Dialog
{
    
    private Context context;
    
    private TextView content, tv_mch_name, tv_mch_id;
    
    private Button btnOk, btCancel;
    
    private ViewGroup mRootView;
    
    private ScanActiveDialogInfo.HandleBtn handleBtn;
    
    private ScanActiveDialogInfo.HandleBtnCancle cancleBtn;
    
    //private ScanActiveDialogInfo.HandleBtnrtw rTWBtn;
    
    // 更多
    public static final int FLAG = 0;
    
    // 最终提交
    public static final int SUBMIT = 1;
    
    // 注册成功
    public static final int REGISTFLAG = 2;
    
    // 微信支付 是否授权激活
    public static final int EXITAUTH = 3;
    
    // 微信支付 授权激活 重新登录
    public static final int EXITAUTHLOGIN = 4;
    
    //提交商户信息
    public static final int SUBMITSHOPINFO = 5;
    
    //判断网络连接状态
    public static final int NETWORKSTATUE = 8;
    
    //优惠卷列表，长按提示删除提示框
    public static final int SUBMIT_DEL_COUPON_INFO = 6;
    
    //优惠卷发布
    public static final int SUBMIT_COUPON_INFO = 7;
    
    public static final int SUBMIT_SHOP = 9;
    
    public static final int SCAN_PAY = 10;
    
    public static final int VARD = 11;
    
    public static final int UNIFED_DIALOG = 12;
    
    //private OnItemLongDelListener mDelListener;
    
    //private int position;
    
    //private OnSubmitCouponListener mOnSubmitCouponListener;
    
    /**
     * title 
     * content 提示内容
     * <默认构造函数>
     */
    public ScanActiveDialogInfo(Context context, ScanActiveDialogInfo.HandleBtn handleBtn,
        ScanActiveDialogInfo.HandleBtnCancle cancleBtn)
    {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.dialog_scan_active, null);
        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);
        
        this.context = context;
        this.handleBtn = handleBtn;
        this.cancleBtn = cancleBtn;
        
        initView();
        
        setLinster();
        
    }
    
    public void setBtnOkText(String string)
    {
        if (btnOk != null)
        {
            btnOk.setText(string);
        }
    }
    
    public void setMessage(String msg)
    {
        this.content.setText(msg);
    }
    
    public void setmDelListener(OnItemLongDelListener mDelListener, int position)
    {
//        this.mDelListener = mDelListener;
//        this.position = position;
    }
    
    public void setmOnSubmitCouponListener(OnSubmitCouponListener mOnSubmitCouponListener)
    {
        //this.mOnSubmitCouponListener = mOnSubmitCouponListener;
    }
    
    /**
     * 设置监听
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void setLinster()
    {
        
        btCancel.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                dismiss();
                cancleBtn.handleCancleBtn();
                cancel(); //Intl
            }
            
        });
        btnOk.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                
                dismiss();
                cancel();
                handleBtn.handleOkBtn();
            }
        });
    }
    
    /**
     * 初始化
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView()
    {
        
        btnOk = (Button)findViewById(R.id.btnOk);
        btCancel = (Button)findViewById(R.id.btCancel);
        
        tv_mch_name = (TextView)findViewById(R.id.tv_mch_name);
        tv_mch_id = (TextView)findViewById(R.id.tv_mch_id);
        
        tv_mch_name.setText(MainApplication.getMchName());
        tv_mch_id.setText(MainApplication.getMchId());
    }
    
    public void showPage(Class clazz)
    {
        Intent intent = new Intent(context, clazz);
        context.startActivity(intent);
    }
    
    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作  
     * @author Administrator  
     *
     */
    public interface HandleBtn
    {
        void handleOkBtn();
    }
    
    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作  
     * @author Administrator  
     *
     */
    public interface HandleBtnCancle
    {
        void handleCancleBtn();
    }
    
    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作  
     * @author Administrator  
     *
     */
    public interface HandleBtnrtw
    {
        void handleCancleBtn();
    }
    
    /***
     * 
     * 长按删除优惠
     * 
     * @author  wang_lin
     * @version  [2.1.0, 2014-4-23]
     */
    public interface OnItemLongDelListener
    {
        public void onItemLongDelMessage(int position);
    }
    
    /***
     * 
     * 发布和修改优惠价提示
     * @author  wang_lin
     * @version  [2.1.0, 2014-4-23]
     */
    public interface OnSubmitCouponListener
    {
        public void onSubmitCouponListenerOk();
        
        public void onSubmitCouponListenerCancel();
    }
}
