package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.zxing.WriterException;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bean.QRCodeBean;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.MaxCardManager;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DisplayUtil;
import cn.swiftpass.enterprise.utils.GlideApp;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.ImageUtil;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

import static cn.swiftpass.enterprise.MainApplication.getContext;

/**
 * 固定二维码收款
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2015-10-14]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class StaticCodeActivity extends TemplateActivity implements View.OnClickListener{
    private static final String TAG = StaticCodeActivity.class.getSimpleName();
    private final static int REQUEST_STATIC_CODE_LABEL = 0x11;
    /**
     *固定二维码地址
     */
    private String codeStrM = ApiConstant.BASE_URL_PORT + "spay/scanQr?qrcodeId=";
    /**
     *二维码图片
     */
    private ImageView img;
    private LinearLayout tv_content;

    private TextView tx_company, tx_money, tx_reMark,tv_static_code_label,tv_name;
    private LinearLayout ll_common_btn;
    private TextView tv_save_image;
    private Button confirm, setAmountBtn,btn_setLabel;
    private Context context;
    private String qrcodeId="";
    private int qrtype = -1;
    Bitmap bitmap;
    private View view_setAmount_line;
    /**
     *  默认登陆时的二维码
     */
    private Bitmap firstBitmap;

    private GridView gv_pay_type;

    private ViewPayTypeHolder payholder;

    private PayTypeAdapter payTypeAdapter;

    private String codeShowMoney = null;
    private LinearLayout mBgColorLayout;

    private QRCodeBean qrCodeBean;
    PayTypeAdapter adapter;
    private final static int BG_RED = 1;
    private final static int BG_BLUE =2 ;
    private final static int BG_GREEN = 3;

    private int bg_color = 2;

    private String lastLabel = "";
    private String currentLabel = "";

    private Handler handler = new Handler()

    {
        @Override
        public void handleMessage(android.os.Message msg) {
            if (msg.what == HandlerManager.SETCODEMONEY) {
                if (msg.obj != null) {
                    setAmountBtn.setText(getString(R.string.tv_code_clean_moeny));
                    String coneten = (String) msg.obj;
                    if (coneten.contains("|")) {
                        tx_reMark.setVisibility(View.VISIBLE);
                        tx_money.setVisibility(View.VISIBLE);
                        String str[] = coneten.split("\\|");

                        double money = Double.parseDouble(str[0]);
                        //下面的代码是为了做到根据输入金额的长度修改字体的大小-----by aijingya on 2018/02/23
                        int length = str[0].length();
                        if(length >= 8){
                            tx_money.setTextSize(15);
                        }else{
                            tx_money.setTextSize(22);
                        }
                        tx_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtil(money));
                        tx_reMark.setText(str[1]);
                        createCode(str[0]);
                        codeShowMoney = str[0];
                    } else {
                        tx_money.setVisibility(View.VISIBLE);
                        double money = Double.parseDouble(coneten);
                        //下面的代码是为了做到根据输入金额的长度修改字体的大小-----by aijingya on 2018/02/23
                        int length = coneten.length();
                        if(length >= 8){
                            tx_money.setTextSize(15);
                        }else{
                            tx_money.setTextSize(22);
                        }
                        tx_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtil(money));
                        createCode(coneten);
                        codeShowMoney = coneten;
                    }

                }
            }
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_qrcode);
        context = StaticCodeActivity.this;
        MainApplication.listActivities.add(this);

        HandlerManager.registerHandler(HandlerManager.SETCODEMONEY, handler);
        HandlerManager.registerHandler(HandlerManager.PAY_ACTIVE_SCAN, handler);
        confirm = getViewById(R.id.confirm);
        setAmountBtn = getViewById(R.id.setamount);
        btn_setLabel = getViewById(R.id.btn_setLabel);
        btn_setLabel.setOnClickListener(this);

        initView();

        //当前只有一张码，从设置进入到固码页面
        if(qrCodeBean.switch_type == 2){
            titleBar.setRightButtonVisible(false);
            titleBar.setRightButLayVisible(true, 0);
            titleBar.setRightButLayBackground(R.drawable.icon_add);
            titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
            {

                @Override
                public void onRightLayClick()
                {

                }

                @Override
                public void onRightButtonClick()
                {

                }

                @Override
                public void onRightButLayClick()
                {
                    Intent intent = new Intent(StaticCodeActivity.this, AddCodeActivity.class);
                    startActivity(intent);
                }

                @Override
                public void onLeftButtonClick()
                {
                    finish();
                }
            });
        }else{
            titleBar.setRightButLayVisible(false,null);
        }




    }



    boolean isMark = false;

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();

    }

    /**
     *   默认是收款二维码
     */
    private void createCode(String money) {
        long mon = 0;
        if (money.contains(".")) {
            double m = Double.parseDouble(money);
            mon = Long.parseLong(DateUtil.formatMoneyInt(m));
        } else {
            long m = Long.parseLong(money);
            mon = m;
            for(int index = 0 ; index < MainApplication.numFixed ; index++){
                mon = (mon * 10);
            }
//            mon = (m * 100);
        }
        Bitmap logo=null;
        if (null != qrCodeBean){
            logo = ImageUtil.base64ToBitmap(qrCodeBean.qrLogo);
        }

        //进行instapay的逻辑判断---V3.0.10
       /* 先判断bankcode是aub
        再判断是否有instapay通道
        pay.instapay.native
        pay.instapay.native.v2
        才会执行定制逻辑*/

        if(ApiConstant.bankCode.contains("aub") &&  containsInstapayQRPayment()){
            //如果后台并没有获取到logo才用本地的logo
            if(TextUtils.isEmpty(qrCodeBean.qrLogo)){
                logo  = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.logo_instapay_white);
            }
        }

        try {
            //根据聚合非聚合的情况生成不同的固定二维码的string
            String codeMsg = creatCodeMsg(true,mon);
            firstBitmap = createCodeBitmap(StaticCodeActivity.this,codeMsg,280,280,logo);
            img.setImageBitmap(firstBitmap);
        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }

    }

    public String creatCodeMsg(boolean hasMoney,long money){
        String codeString = null;
        Map<String, String> params = new HashMap<String, String>();
        params.put("mchId", MainApplication.merchantId);
        params.put("userId", MainApplication.userId + "");
        params.put("client", MainApplication.CLIENT);
        if (null == MainApplication.body || "".equals(MainApplication.body)) {
            MainApplication.body = ToastHelper.toStr(R.string.tx_mobile_pay);
        }
        params.put("body", MainApplication.body);
        //是刚进来固定二维码页面，还是输入金额后回来固定二维码页面
        //输入过金额
        if(hasMoney){
            codeString = codeStrM +qrcodeId+ "&fixedMoney=" + money;
            params.put("fixedMoney", String.valueOf(money));
        }else {
            //没输入金额的固码页面
            codeString = codeStrM +qrcodeId;
        }
        if(!TextUtils.isEmpty(currentLabel)){
            params.put("currentLabel",currentLabel);
        }

         params.put("qrcodeId", qrCodeBean.Id + "");

        sign = SignUtil.getInstance().createSign(params, MainApplication.getSignKey());
        return codeString;
    }


    private void initView() {
        pays = new ArrayList<DynModel>();
        gv_pay_type = getViewById(R.id.gv_pay_type);
        tx_money = getViewById(R.id.tx_money);
        tx_reMark = getViewById(R.id.tx_reMark);
        tv_static_code_label = getViewById(R.id.tv_static_code_label);
        tv_content = getViewById(R.id.tv_content);
        ll_common_btn = getViewById(R.id.ll_common_btn);
        view_setAmount_line = findViewById(R.id.view_setAmount_line);
//        tv_save_image = getViewById(R.id.tv_save_image);
        img = getViewById(R.id.img);
        tx_company = getViewById(R.id.tx_company);
        tv_name = getViewById(R.id.tv_name);
        mBgColorLayout = getViewById(R.id.bg_color_layout);
        if (null != getIntent()) {
            qrCodeBean = (QRCodeBean) getIntent().getSerializableExtra("QRCodeBean");
            if (null != qrCodeBean) {
                qrcodeId = qrCodeBean.qrCodeId;
                bg_color = qrCodeBean.bgColor;
                qrtype = qrCodeBean.qrType;
                if (BG_RED == bg_color) {
                    mBgColorLayout.setBackground(getResources().getDrawable(R.drawable.bottom_code_red_shape));
                } else if (BG_GREEN == bg_color) {
                    mBgColorLayout.setBackground(getResources().getDrawable(R.drawable.bottom_code_green_shape));
                } else {
                    mBgColorLayout.setBackground(getResources().getDrawable(R.drawable.bottom_code_shape));
                }
                if (qrCodeBean.qrType == 20) {//如果是银联二维码
                    tx_company.setVisibility(View.GONE);
                } else {
                    tx_company.setVisibility(View.VISIBLE);
                    tx_company.setText("ID:" + qrCodeBean.Id);
                }

                //判断是否是预制二维码，如果是预制二维码则展示收银台名称
                //1:预制二维码；2:生成的和银联等不支持预制的
                if(qrCodeBean.qrCreateType == 1){
                    if(!TextUtils.isEmpty(qrCodeBean.cashierDesk)){
                        tv_name.setVisibility(View.VISIBLE);
                        tv_name.setText(qrCodeBean.cashierDesk);
                    }else{
                        tv_name.setVisibility(View.GONE);
                    }

                }else if(qrCodeBean.qrCreateType == 2){
                    if(!TextUtils.isEmpty(qrCodeBean.qrSignOwnerName)){
                        tv_name.setVisibility(View.VISIBLE);
                        tv_name.setText(qrCodeBean.qrSignOwnerName);
                    }else{
                        tv_name.setVisibility(View.GONE);
                    }
                }
            }else {
                tx_company.setVisibility(View.VISIBLE);
                tx_company.setText("ID:" + "null");

                tv_name.setVisibility(View.GONE);
            }
        }
        adapter = new PayTypeAdapter();
        gv_pay_type.setAdapter(adapter);
        Object dynPayTypeStatic = SharedPreUtile.readProduct("dynallPayType" + ApiConstant.bankCode + MainApplication.merchantId);
        if (null != dynPayTypeStatic) {
            getPayData(dynPayTypeStatic, qrCodeBean);
        }
        initCode();

        setViewButton();
    }

    private List<DynModel> pays;
    private void getPayData(Object dynPayTypeStatic,QRCodeBean qrCodeBean){
        pays.clear();
        int paylogoLemth = 0;
        if (!qrCodeBean.payLogos.contains(",")){
            paylogoLemth = 1;
            addpays(dynPayTypeStatic,paylogoLemth,null);
            gv_pay_type.setNumColumns(paylogoLemth);
        }else{
            String [] paylogs= qrCodeBean.payLogos.split(",");
            paylogoLemth= paylogs.length;
            addpays(dynPayTypeStatic,paylogoLemth,paylogs);
        }
        Logger.e("JAMY","pays: "+pays.toString());
        adapter.notifyDataSetChanged();

    }

    private void addpays(Object dynPayTypeStatic,int paylogoLenth,String [] paylogs){
        List<DynModel> list = (List<DynModel>) dynPayTypeStatic;
        int allpaylenth = list.size();
        if (0 == paylogoLenth || 0 == allpaylenth){
            return ;
        }
        DynModel dynModel;
        for (int i=0;i <paylogoLenth ;i++){
            for (int j=0;j<allpaylenth;j++){
                dynModel = list.get(j);
                if (1 == paylogoLenth){
                    if (qrCodeBean.payLogos.equals(dynModel.getApiCode())){
                        pays.add(dynModel);
                    }
                }else{
                    if (paylogs[i].equals(dynModel.getApiCode())){
                        pays.add(dynModel);
                    }
                }
            }
        }

        if (pays.size()>0){
            gv_pay_type.setNumColumns(pays.size());
        }
    }

    //根据当前的详情页的数据判断是银联二维码还是支付宝微信的支付类型的二维码
    // 然后银联二维码只显示保存图片的按钮
    //V3.0.4  2019/11/28 修改
    //银联二维码也要展示标签的按钮
    public void setViewButton(){
         if(qrCodeBean != null && qrCodeBean.qrType == 20){
//             tv_save_image.setVisibility(View.VISIBLE);
             ll_common_btn.setVisibility(View.VISIBLE);
             view_setAmount_line.setVisibility(View.GONE);
             setAmountBtn.setVisibility(View.GONE);

         }else{
//             tv_save_image.setVisibility(View.GONE);
             ll_common_btn.setVisibility(View.VISIBLE);
             view_setAmount_line.setVisibility(View.VISIBLE);
             setAmountBtn.setVisibility(View.VISIBLE);
         }
    }


    /**
     * 默认生成无金额二维码
     */
    private void initCode(){
        Bitmap logo = null;
        if (null != qrCodeBean){
            logo = ImageUtil.base64ToBitmap(qrCodeBean.qrLogo);

            //进行instapay的逻辑判断---V3.0.10
           /* 先判断bankcode是aub
            再判断是否有instapay通道
            pay.instapay.native
            pay.instapay.native.v2
            才会执行定制逻辑*/

            if(ApiConstant.bankCode.contains("aub") &&  containsInstapayQRPayment()){
                //如果后台并没有获取到logo才用本地的logo
                if(TextUtils.isEmpty(qrCodeBean.qrLogo)){
                    logo  = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.logo_instapay_white);
                }
            }

            try {
                String codeMsg = creatCodeMsg(false,0);
                if (20 == qrtype){
                    if (null != qrCodeBean){
                        codeMsg = qrCodeBean.qrCodeInfo;
                    }
                }
                firstBitmap = createCodeBitmap(StaticCodeActivity.this,codeMsg,280,280,logo);
                img.setImageBitmap(firstBitmap);
            } catch (Exception e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }
        }
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(R.string.tv_scan_prompt3);
        titleBar.setLeftButtonVisible(true);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.setamount:
                Intent intent = new Intent(StaticCodeActivity.this, AddCodeActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_setLabel:
                lastLabel = tv_static_code_label.getText().toString().trim();
                Intent it = new Intent();
                it.putExtra("staticCodeLabel", tv_static_code_label.getText().toString().trim());
                it.setClass(this, StaticCodeLabelActivity.class);
                startActivityForResult(it,REQUEST_STATIC_CODE_LABEL);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //如果是设置标签成功，则更新界面的文案
        if (resultCode == RESULT_OK && requestCode == REQUEST_STATIC_CODE_LABEL) {
            String note_mark = data.getStringExtra("static_code_label");
            currentLabel = note_mark;
            if(!TextUtils.isEmpty(note_mark)){
                tv_static_code_label.setVisibility(View.VISIBLE);
                tv_static_code_label.setText(note_mark);
            }else{
                tv_static_code_label.setVisibility(View.GONE);
            }
        }else{
            tv_static_code_label.setVisibility(View.GONE);
        }
    }

    class PayTypeAdapter extends BaseAdapter {

        private PayTypeAdapter() {

        }

        @Override
        public int getCount() {
            return pays.size();
        }

        @Override
        public Object getItem(int position) {
            return pays.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = View.inflate(context, R.layout.activity_static_paytype_list_item, null);
                payholder = new ViewPayTypeHolder();
                payholder.iv_icon = convertView.findViewById(R.id.iv_icon);
                payholder.tv_paymethod =  convertView.findViewById(R.id.tv_paymethod);
                convertView.setTag(payholder);
            } else {
                payholder = (ViewPayTypeHolder) convertView.getTag();
            }

            if (!StringUtil.isEmptyOrNull(pays.get(position).getSmallIconUrl())) {
                if(getContext() != null){
                    GlideApp.with(getContext())
                            .load(pays.get(position).getSmallIconUrl())
                            .placeholder(R.drawable.icon_general_receivables)
                            .error(R.drawable.icon_general_receivables)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .into(payholder.iv_icon);
                }
            }
            payholder.tv_paymethod.setText(MainApplication.getPayTypeMap().get(String.valueOf(pays.get(position).getApiCode())));
            return convertView;
        }
    }

    static class ViewPayTypeHolder {
        ImageView iv_icon;
        TextView tv_paymethod;
    }


    /**
     *将二维码保存到本地
     */
    String sign = null;
    String viewPicSavePath;
    public void saveToSdcard(View v) {
        if (TextUtils.isEmpty(sign)) {
            return;
        }
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
            // 适配android 10
            if(ImageUtil.isContainsCurrentImageFile(context,sign) && currentLabel.equals(lastLabel)){
                showToastInfo(ToastHelper.toStr(R.string.save_qrcode_success));
                return;
            }
        }else{
            // 适配android 9.0以及以下
            if (!TextUtils.isEmpty(viewPicSavePath)) {
                if (new File(viewPicSavePath).exists() && viewPicSavePath.contains(sign) && currentLabel.equals(lastLabel)) {
                    showToastInfo(ToastHelper.toStr(R.string.save_qrcode_success));
                    return;
                }
            }
        }

        ScrollView scrollView = getViewById(R.id.static_qrcode_sv);
        scrollView.scrollTo(0, 0);
        final View view = getViewById(R.id.template_layout);
        final View leftView = view.findViewById(R.id.leftButtonLay);
        final LinearLayout mSaveLayout = getViewById(R.id.savelayout);
        final View confirmView = getViewById(R.id.confirm);
        confirmView.setVisibility(View.GONE);
        showLoading(false, ToastHelper.toStr(R.string.show_login_loading));
        leftView.setVisibility(View.GONE);
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            @Override
            public void run() {

                viewPicSavePath = ImageUtil.saveViewBitmapFile(sign, mSaveLayout,context);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dismissMyLoading();
                        if (qrCodeBean.qrType == 20) {//如果是银联二维码
                            tx_company.setVisibility(View.GONE);
                        } else {
                            tx_company.setVisibility(View.VISIBLE);
                        }
                        tv_name.setVisibility(View.VISIBLE);
                        tv_content.setVisibility(View.VISIBLE);
                        leftView.setVisibility(View.VISIBLE);
                        confirmView.setVisibility(View.VISIBLE);

                    }
                });
                if (!TextUtils.isEmpty(viewPicSavePath)) {
                    lastLabel = currentLabel;
                    showToastInfo(ToastHelper.toStr(R.string.save_qrcode_success) + ":" + viewPicSavePath);
                } else {
                    showToastInfo(ToastHelper.toStr(R.string.save_qrcode_fail));
                }

            }
        });
    }

    /**
     *设置金额
     */
    public void setAmount(View v) {
        if (!TextUtils.isEmpty(tx_money.getText().toString())) {
            tx_money.setVisibility(View.GONE);
            tx_reMark.setVisibility(View.GONE);
            tx_money.setText("");
            setAmountBtn.setText(getString(R.string.tv_code_set_moeny));
            if (firstBitmap != null) {
                img.setImageBitmap(firstBitmap);
            }
            initCode();
        } else {
            showPage(SetCodeMoneyActivity.class);
        }
    }

    /**
     * 生成对应的二维码
     * @param activity
     * @param uuId
     * @param width
     * @param height
     * @param mBitmapfinal
     * @return
     */
    private Bitmap createCodeBitmap(Activity activity, String uuId, int width, int height, Bitmap mBitmapfinal) {
        Bitmap bitmap = null;
        int w = DisplayUtil.dip2Px(StaticCodeActivity.this, width);
        int h = DisplayUtil.dip2Px(StaticCodeActivity.this, height);
        try {
            if (null != mBitmapfinal){
                bitmap = MaxCardManager.getInstance().create2DCode(true,false,activity,uuId, w, h,mBitmapfinal);
            }else{
                bitmap = MaxCardManager.getInstance().create2DCode(uuId, w, h);
            }
        } catch (WriterException e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }

        return bitmap;
    }


    //判断当前的支付列表中是否含有 instapay的QR Ph Payment支付通道
    public boolean containsInstapayQRPayment(){
        if(!TextUtils.isEmpty(qrCodeBean.payLogos)){
            if (!qrCodeBean.payLogos.contains(",")){
                if(getNativePayType(qrCodeBean.payLogos) !=null && getNativePayType(qrCodeBean.payLogos).equalsIgnoreCase(MainApplication.instapayQRPaymentServiceType)){
                    return  true;
                }
            }else {
                String [] logoList= qrCodeBean.payLogos.split(",");
                for(int i = 0 ; i < logoList.length ; i++){
                    if(getNativePayType(logoList[i]) != null && getNativePayType(logoList[i]).equalsIgnoreCase(MainApplication.instapayQRPaymentServiceType)){
                        return  true;
                    }
                }
            }
        }
        return false;
    }


    //通过ApiCode得到当前支付类型的NativePayType
    public String getNativePayType(String ApiCode){
        Object object = SharedPreUtile.readProduct("ActiveDynPayType" + ApiConstant.bankCode + MainApplication.getMchId());
        Object object_CardPayment =  SharedPreUtile.readProduct("cardPayment_ActiveDynPayType" + ApiConstant.bankCode + MainApplication.getMchId());

        List<DynModel> list = new ArrayList<>();
        List<DynModel> list_CardPayment = new ArrayList<>();
        if(object != null){
            list = (List<DynModel>) object;
        }
        if(object_CardPayment != null){
            list_CardPayment = (List<DynModel>) object_CardPayment;
        }

        //把两个数组拼接起来
        List<DynModel> listAll = new ArrayList<>();
        if(list != null){
            listAll.addAll(list);
        }
        if(list_CardPayment != null){
            listAll.addAll(list_CardPayment);
        }

        if (null != listAll && listAll.size() > 0) {
            for(int i = 0 ; i < listAll.size();i++){
                if(listAll.get(i).getApiCode().equalsIgnoreCase(ApiCode)){
                    return listAll.get(i).getNativeTradeType();
                }
            }
        } else {
            return null;
        }
        return null;
    }

}
