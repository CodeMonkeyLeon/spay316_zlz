package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.TipsBean
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView


/**
 * @author lizheng.zhao
 * @date 2022/12/2
 *
 * @这次我离开你
 * @是风
 * @是雨
 * @是夜晚
 * @你笑了笑
 * @我摆一摆手
 * @一条寂寞的路便展向两头了
 */
class TipsSettingContract {

    interface View : BaseView {

        fun getTipsSettingSuccess(response: ArrayList<TipsBean>)

        fun getTipsSettingFailed(error: Any?)

    }


    interface Presenter : BasePresenter<View> {

        fun getTipsSetting()
    }


}