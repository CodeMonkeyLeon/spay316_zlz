package cn.swiftpass.enterprise.newmvp.config;

/**
 * 创建人：caoxiaoya
 * 时间：2019/4/15
 * 描述：用于请求标识
 * 备注：2开头/四位数
 */
public final class RequestTypeConfig {

    /**
     * 交换秘钥
     */
    public final static int EXCHANGE_TYPE=2001;
    /**
     * 登录
     */
    public final static int LOGIN_TYPE=2002;
    /**
     * 登录--第一次登录本地缓存下来的secretKey
     */
    public final static int DEVICELOGIN_TYPE=2003;

}
