package cn.swiftpass.enterprise.ui.activity;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.common.OnSwitchButtonChangeListener;
import com.example.common.sentry.SentryUtils;
import com.example.common.sp.PreferenceUtil;
import com.example.common.view.SwitchButton;

import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.GoodsMode;
import cn.swiftpass.enterprise.bussiness.model.MerchantTempDataModel;
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo;
import cn.swiftpass.enterprise.bussiness.model.UserInfo;
import cn.swiftpass.enterprise.common.Constant;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.SettingMoreContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.SettingMorePresenter;
import cn.swiftpass.enterprise.ui.accountcancel.view.AccountCancelWarningActivity;
import cn.swiftpass.enterprise.ui.activity.setting.SettingAboutActivity;
import cn.swiftpass.enterprise.ui.activity.setting.SettingSwitchLanActivity;
import cn.swiftpass.enterprise.ui.activity.shop.ShopkeeperActivity;
import cn.swiftpass.enterprise.ui.activity.user.GoodsNameSettingActivity;
import cn.swiftpass.enterprise.ui.widget.CommonConfirmDialog;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.KotlinUtils;
import cn.swiftpass.enterprise.utils.LocaleUtils;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.interfaces.OnOkClickListener;

/**
 * 设置 会自动检查版本 User: ALAN Date: 13-10-28 Time: 下午5:11 To change this template use
 * File | Settings | File Templates.
 */
public class SettingMoreActivity extends BaseActivity<SettingMoreContract.Presenter> implements View.OnClickListener, SettingMoreContract.View {


    private static final String TAG = SettingMoreActivity.class.getSimpleName();
    BroadcastReceiver updateBluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Handle the received local broadcast
            String action = intent.getAction();
            if ("action_bluetooth_connect_is_success".equals(action)) {
                String name = MainApplication.getInstance().getBlueDeviceName();
                if (!StringUtil.isEmptyOrNull(name)) {
//                    tv_bluetool_device.setText(name);
                } else {
//                    tv_bluetool_device.setText(R.string.tx_print_no_conn);
                }
            }
        }
    };
    BroadcastReceiver closeBluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Handle the received local broadcast
            String action = intent.getAction();
            if ("action_bluetooth_connect_is_cancel".equals(action)) {
//                tv_bluetool_device.setText(R.string.tx_print_no_conn);
            }
        }
    };
    MerchantTempDataModel merchantInfo = null;
    boolean isAuth = false;
    private Context mContext;
    private LinearLayout llChangePwd, llAccountCancel, ll_tip_settings, cashier_layout;
    private Button ll_logout;
    private LinearLayout bodyLay;
    private LinearLayout lay_choise;
    private ImageView iv_point;

    private SwitchButton mSwitchVoice;
    private LinearLayout device_lay;
    private LinearLayout ll_FAQ, ll_about_us, ll_permission_setting;
    private TextView tv_body, tv_device;
    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                //蓝牙关闭
                case HandlerManager.BLUE_CONNET_STUTS_CLOSED:
//                    tv_bluetool_device.setText(R.string.tx_print_no_conn);
                    break;
                //蓝牙已连接上后
                case HandlerManager.BLUE_CONNET_STUTS:
                    String name = MainApplication.getInstance().getBlueDeviceName();
                    if (!StringUtil.isEmptyOrNull(name)) {
//                        tv_bluetool_device.setText(name);
                    } else {
//                        tv_bluetool_device.setText(R.string.tx_print_no_conn);
                    }
                    break;
                case HandlerManager.BLUE_CONNET:
                    String deviceName = MainApplication.getInstance().getBlueDeviceName();
                    if (!StringUtil.isEmptyOrNull(deviceName)) {
//                        tv_bluetool_device.setText(deviceName);
                    } else {
//                        tv_bluetool_device.setText(R.string.tx_print_no_conn);
                    }
                    break;

                case HandlerManager.NOTICE_TYPE:
                    isHidePoint(iv_point);
                    break;
                case HandlerManager.BODY_SWITCH:
                    String body = (String) msg.obj;
                    if (body != null) {
                        tv_body.setText(body);
                    }
                    break;
                case HandlerManager.DEVICE_SWITCH:
                    String device = (String) msg.obj;
                    if (device != null) {
                        tv_device.setText(device);
                    }
                    break;
                default:
                    MerchantTempDataModel dataModel = (MerchantTempDataModel) msg.obj;
                    Integer merchantType = dataModel.getMerchantType();
                    Intent intent = new Intent(SettingMoreActivity.this, ShopkeeperActivity.class);
                    intent.putExtra("merchantType", merchantType);
                    SettingMoreActivity.this.startActivity(intent);
                    break;
            }
        }

    };
    private DynModel dynModel;
    private UpgradeInfo upVerInfo;
    private UpgradeInfo mApkDownInfo = null;

    @Override
    protected SettingMoreContract.Presenter createPresenter() {
        return new SettingMorePresenter();
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_setting);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButLayClick() {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        tv_device.setText(LocaleUtils.getShowLanguage(this));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dynModel = (DynModel) SharedPreUtils.readProduct("dynModel" + BuildConfig.bankCode);
        initViews();
        HandlerManager.registerHandler(HandlerManager.NOTICE_TYPE, handler);

        HandlerManager.registerHandler(HandlerManager.VICE_SWITCH, handler);

        SettingMoreActivity.this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                isShowPoint(iv_point);
            }
        });

        IntentFilter Filter_Success = new IntentFilter();
        Filter_Success.addAction("action_bluetooth_connect_is_success");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            //android13
            registerReceiver(updateBluetoothReceiver, Filter_Success, Constant.MY_PERMISSION, null, Context.RECEIVER_VISIBLE_TO_INSTANT_APPS);
        } else {
            registerReceiver(updateBluetoothReceiver, Filter_Success, Constant.MY_PERMISSION, null);
        }


        IntentFilter Filter_Cancel = new IntentFilter();
        Filter_Cancel.addAction("action_bluetooth_connect_is_cancel");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            //android13
            registerReceiver(closeBluetoothReceiver, Filter_Cancel, Constant.MY_PERMISSION, null, Context.RECEIVER_VISIBLE_TO_INSTANT_APPS);
        } else {
            registerReceiver(closeBluetoothReceiver, Filter_Cancel, Constant.MY_PERMISSION, null);
        }


        loadBody();
        initValue();
    }

    private void initValue() {
        String deviceName = MainApplication.getInstance().getBlueDeviceName();
        if (!StringUtil.isEmptyOrNull(deviceName) && MainApplication.getInstance().getBlueState()) {
//            tv_bluetool_device.setText(deviceName);
        }
    }

    @Override
    public void queryBodySuccess(@NonNull GoodsMode response) {
        tv_body.setText(response.commodityName);
    }

    @Override
    public void queryBodyFailed(@Nullable Object error) {

    }

    private void loadBody() {
        if (mPresenter != null) {
            mPresenter.queryBody();
        }
    }

    /**
     * 公告小圆点
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void isShowPoint(ImageView iv) {
        String noticePre = PreferenceUtil.getString("noticePre", "");
        Logger.i("hehui", "isShowPoint-->" + noticePre);
        if (!StringUtil.isEmptyOrNull(noticePre)) {
            Map<String, String> mapNotice = JsonUtil.jsonToMap(noticePre);
            for (Map.Entry<String, String> entry : mapNotice.entrySet()) {
                String value = entry.getValue();
                if (value.equals("0")) { //代表还有公开没有被打开查看详情过
                    iv.setVisibility(View.VISIBLE);
                }
            }
        }

    }

    /**
     * 公告小圆点
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void isHidePoint(ImageView iv) {
        boolean tag = true;
        String noticePre = PreferenceUtil.getString("noticePre", "");
        if (!StringUtil.isEmptyOrNull(noticePre)) {
            Map<String, String> mapNotice = JsonUtil.jsonToMap(noticePre);
            for (Map.Entry<String, String> entry : mapNotice.entrySet()) {
                String value = entry.getValue();
                if (value.equals("0")) { //代表还有公开没有被打开查看详情过
                    iv.setVisibility(View.VISIBLE);
                    tag = false;
                }
            }
            if (tag) {
                iv.setVisibility(View.GONE);
            }
        }

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();


    }

    @Override
    protected void onStop() {
        super.onStop();
        HandlerManager.unregisterHandler(HandlerManager.GETSHOPINFODONE, handler);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(updateBluetoothReceiver);
        unregisterReceiver(closeBluetoothReceiver);
    }


    @Override
    protected boolean useToolBar() {
        return true;
    }

    private void initViews() {
        setContentView(R.layout.activity_setting_more);
        mContext = this;
        tv_device = findViewById(R.id.tv_device);

        tv_body = findViewById(R.id.tv_body);
        device_lay = findViewById(R.id.device_lay);

        device_lay.setOnClickListener(this);

        mSwitchVoice = findViewById(R.id.id_switch_voice);
        lay_choise = findViewById(R.id.lay_choise);
        lay_choise.setOnClickListener(this);

        ll_logout = findViewById(R.id.ll_logout);
        ll_logout.setOnClickListener(this);

        cashier_layout = findViewById(R.id.cashier_layout);
        cashier_layout.setOnClickListener(this);

        ll_FAQ = findViewById(R.id.ll_FAQ);
        ll_FAQ.setOnClickListener(this);

        ll_about_us = findViewById(R.id.ll_about_us);
        ll_about_us.setOnClickListener(this);

        ll_permission_setting = findViewById(R.id.ll_permission_setting);
        ll_permission_setting.setOnClickListener(this);

        bodyLay = findViewById(R.id.bodyLay);
        bodyLay.setOnClickListener(this);

        llChangePwd = findViewById(R.id.ll_changePwd);
        llChangePwd.setOnClickListener(this);

        llAccountCancel = findViewById(R.id.ll_account_cancel);
        llAccountCancel.setOnClickListener(this);

        ll_tip_settings = findViewById(R.id.ll_tip_settings);
        ll_tip_settings.setOnClickListener(this);

        //该入口的展示需根据平台的小费开关状态判断，若小费开关打开，则展示，反之隐藏
        if (MainApplication.getInstance().isTipOpenFlag()) {
            ll_tip_settings.setVisibility(View.VISIBLE);
        } else {
            ll_tip_settings.setVisibility(View.GONE);
        }
//        ll_tip_settings.setVisibility(View.VISIBLE);

        checkVersion(true);

        if (MainApplication.getInstance().isAdmin(0)) {
            bodyLay.setVisibility(View.GONE);
        }

        String voice = PreferenceUtil.getString("voice", "open");
        Log.i("VOICE", "voice: " + voice);
        if (voice.equals("open")) {
            mSwitchVoice.openWithoutAnim(true);
        } else {
            mSwitchVoice.openWithoutAnim(false);
        }

        /**
         * 可配置设置
         */
        if (null != dynModel) {
            try {
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonColor())) {
                    ll_logout.setBackgroundColor(Color.parseColor(dynModel.getButtonColor()));
                }

                if (!StringUtil.isEmptyOrNull(dynModel.getButtonFontColor())) {
                    ll_logout.setTextColor(Color.parseColor(dynModel.getButtonFontColor()));
                }
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(e, SentryUtils.INSTANCE.getClassNameAndMethodName());
            }
        }

        if (BuildConfig.IS_POS_VERSION) {
            lay_choise.setVisibility(View.GONE);
        }


        if (MainApplication.getInstance().isAdmin(UserInfo.ACCOUNT_TYPE_ADMIN)) {
            //商户
            llAccountCancel.setVisibility(View.VISIBLE);
        } else {
            //收银员, 不可注销账户
            llAccountCancel.setVisibility(View.GONE);
        }


        mSwitchVoice.setOnStatusChangeListener(new OnSwitchButtonChangeListener() {
            @Override
            public void onChange(boolean isOpen) {
                if (isOpen) {
                    PreferenceUtil.commitString("voice", "open");
                    Log.i("VOICE", "voice: open");
                } else {
                    PreferenceUtil.commitString("voice", "close");
                    Log.i("VOICE", "voice: close");
                }
            }
        });


//        test();
    }


//    private void test(){
//        TextView tvOpen = findViewById(R.id.id_tv_s_open);
//        SwitchButton switchOpen = findViewById(R.id.id_switch_open);
//        TextView tvClose= findViewById(R.id.id_tv_s_close);
//        SwitchButton switchClose = findViewById(R.id.id_switch_close);
//
//
//        tvOpen.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                switchOpen.open(true);
//            }
//        });
//
//
//        tvClose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                switchClose.open(false);
//            }
//        });
//
//
//        switchOpen.open(false);
//        switchClose.openWithoutAnim(true);
//
//
//    }

    /**
     * 修改密码
     */
    public void onChangePwd() {
        ChangePwdActivity.startActivity(mContext);
    }

    /**
     * 账号注销
     */
    public void onAccountCancel() {
        AccountCancelWarningActivity.Companion.startAccountCancelWarningActivity(this);
    }

    @Override
    public void getVersionCodeSuccess(@Nullable UpgradeInfo response, boolean isDisplayIcon) {
        if (null != response) {
            if (isDisplayIcon) {
                upVerInfo = response;
            } else {
                response.isMoreSetting = true;
                showUpgradeInfoDialog(response, new ComDialogListener(response));
            }
        } else {
            if (!isDisplayIcon) {
                showToastInfo(R.string.show_no_version);
            }
        }

//        response = new UpgradeInfo();
//        response.version = 202;
//        response.versionName = "202Name";
//        response.message = "更新信息";
//        response.mustUpgrade = true;
//        response.isMoreSetting = true;
//        showUpgradeInfoDialog(response, new ComDialogListener(response));
    }

    @Override
    public void getVersionCodeFailed(@Nullable Object error) {

    }

    /**
     * 检查版本显示提示图标
     */
    public void checkVersion(final boolean isDisplayIcon) {
        // 不用太平凡的检测升级 所以使用时间间隔区分
        if (mPresenter != null) {
            mPresenter.getVersionCode(isDisplayIcon);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.device_lay: //语言切换
                showPage(SettingSwitchLanActivity.class);
                break;
            //FAQ
            case R.id.ll_FAQ:
                onHelpPage();
                break;
            //关于我们
            case R.id.ll_about_us:
                showPage(SettingAboutActivity.class);
                break;
            //关于我们
            case R.id.ll_permission_setting:
                showPage(PermissionSettingActivity.class);
                break;
            //语音播报
//            case R.id.lay_choise:
//                String voice = PreferenceUtil.getString("voice", "open");
//                if (voice.equals("open")) {
//                    PreferenceUtil.commitString("voice", "close");
//                    iv_voice.setImageResource(R.drawable.button_configure_switch_close);
//                } else {
//                    setSwitchOpenView(iv_voice);
//                    PreferenceUtil.commitString("voice", "open");
//                }
//                break;

            case R.id.bodyLay://修改商品名称
                if (MainApplication.getInstance().isAdmin(1)) {
                    if (!TextUtils.isEmpty(MainApplication.getInstance().getUserInfo().tradeName)) {
                        showPage(GoodsNameSettingActivity.class);
                        return;
                    }
                    showPage(GoodsNameSettingActivity.class);
                }
                break;
            case R.id.ll_logout:
                DialogInfo dialogInfo = new DialogInfo(
                        SettingMoreActivity.this,
                        getString(R.string.public_cozy_prompt),
                        getString(R.string.show_sign_out),
                        getString(R.string.btnOk),
                        getString(R.string.btnCancel),
                        DialogInfo.UNIFED_DIALOG,
                        new DialogInfo.HandleBtn() {

                            @Override
                            public void handleOkBtn() {
                                //调用退出登录接口
                                logout();
                            }

                            @Override
                            public void handleCancelBtn() {
                            }
                        }, null);

                DialogHelper.resize(SettingMoreActivity.this, dialogInfo);
                dialogInfo.show();

                break;
            case R.id.ll_changePwd:
                onChangePwd();
                break;

            case R.id.ll_account_cancel:
                onAccountCancel();
                break;
            case R.id.ll_tip_settings:
                showPage(TipsSettingActivity.class);
                break;

            case R.id.cashier_layout:
                showPage(CashierDeskSettingActivity.class);
                break;
        }
    }

    @Override
    public void logoutSuccess(boolean response) {
        clearAPPState();
    }

    @Override
    public void logoutFailed(@Nullable Object error) {
        toastDialog(SettingMoreActivity.this, error.toString(), null);
        clearAPPState();
    }

    public void logout() {
        if (mPresenter != null) {
            mPresenter.logout();
        }
    }

    @Override
    protected boolean isLoginRequired() {
        return false;
    }

    /**
     * 进入帮助页
     */
    public void onHelpPage() {
        if (null != dynModel && !StringUtil.isEmptyOrNull(dynModel.getQuestionLink())) {
            ContentTextActivity.startActivity(mContext, dynModel.getQuestionLink(), R.string.title_common_question);
        } else {
            try {
                String language = LocaleUtils.getLocaleLanguage();
                if (language.equalsIgnoreCase(Constant.LANG_CODE_JA_JP)) {
                    language = "ja_jp";
                }
                ContentTextActivity.startActivity(mContext, "https://app.wepayez.com/web/faq/index.html?language=" + language + "&bank=" + BuildConfig.bankName, R.string.title_common_question);
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(e, SentryUtils.INSTANCE.getClassNameAndMethodName());
                Log.e(TAG, Log.getStackTraceString(e));
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == KotlinUtils.APP_INSTALL_REQUEST_PERMISSION) {
            //授予安装未知应用权限
            requestApkInstallPermission();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case KotlinUtils.EXTERNAL_STORAGE_REQUEST_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    //拒绝
                } else {
                    //允许
                    requestApkInstallPermission();
                }
                break;
        }
    }

    /**
     * 下载apk
     */
    private void downApk() {
        new UpgradeDailog(SettingMoreActivity.this, mApkDownInfo, new UpgradeDailog.UpdateListener() {
            @Override
            public void cancel() {

            }
        }).show();
    }

    /**
     * 申请apk安装权限
     */
    private void requestApkInstallPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            boolean haveInstallPermission = getPackageManager().canRequestPackageInstalls();
            if (haveInstallPermission) {
                //有apk安装权限
                downApk();
            } else {
                //没有apk安装权限, 需在设置页面手动开启
                KotlinUtils.INSTANCE.showMissingPermissionDialog(
                        getActivity(),
                        getString(R.string.setting_permission_apk_install_lack),
                        getStringById(R.string.title_setting),
                        getString(R.string.btnCancel),
                        new OnOkClickListener() {
                            @Override
                            public void onOkClick() {
                                Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES);
                                startActivityForResult(intent, KotlinUtils.APP_INSTALL_REQUEST_PERMISSION);
                            }
                        }
                );
            }
        } else {
            //有apk安装权限
            downApk();
        }
    }

    /**
     * 申请存储权限
     */
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_DENIED
                ||
                ContextCompat.checkSelfPermission(
                        this,
                        READ_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_DENIED
        ) {
            //没有存储权限
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    WRITE_EXTERNAL_STORAGE
            ) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(
                            this,
                            READ_EXTERNAL_STORAGE
                    )
            ) {
                //之前拒绝过，用户需要手动去设置里开启存储权限
                KotlinUtils.INSTANCE.showMissingPermissionDialog(
                        getActivity(),
                        getString(R.string.setting_permission_storage_lack),
                        getStringById(R.string.title_setting),
                        getString(R.string.btnCancel),
                        new OnOkClickListener() {
                            @Override
                            public void onOkClick() {
                                startAppSettings();
                            }
                        }
                );

            } else {
                //首次申请
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE},
                        KotlinUtils.EXTERNAL_STORAGE_REQUEST_PERMISSION
                );
            }

        } else {
            //有存储权限
            requestApkInstallPermission();
        }
    }

    /**
     * 升级点击事件
     */
    class ComDialogListener implements CommonConfirmDialog.ConfirmListener {
        private UpgradeInfo result;

        public ComDialogListener(UpgradeInfo result) {
            this.result = result;
            mApkDownInfo = result;
        }

        @Override
        public void ok() {
            requestStoragePermission();
        }

        @Override
        public void cancel() {
            PreferenceUtil.commitInt("update", result.version); // 保存更新，下次进来不更新
        }

    }


}