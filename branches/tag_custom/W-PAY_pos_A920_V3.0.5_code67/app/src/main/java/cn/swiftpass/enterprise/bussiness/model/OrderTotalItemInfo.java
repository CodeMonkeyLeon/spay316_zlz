/*
 * 文 件 名:  OrderTotalInfo.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-19
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * 汇总 和 走势
 * 
 * @author  he_hui
 * @version  [版本号, 2016-11-19]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class OrderTotalItemInfo implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    //        
    //    交易日期：checkTime yyyy-MM-dd HH:mm:ss
    //    成功金额：successFee 
    //    成功笔数：successCount
    //    退款金额：refundFee
    //    退款笔数：refundCount
    //    收银员Id：userId
    //    收银员名称：userName
    //    支付类型名称：payTypeName
    
    private String checkTime;
    
    private String userId;
    
    private String userName;
    
    private String payTypeName;
    
    private long successFee;
    
    private long successCount;
    
    private long refundFee;
    
    private long refundCount;
    
    private Integer payTypeId; //1 微信微信；2：支付宝；3：财付通；4：qq钱包；5：银联 6：实名认证(腾付通)
    
    //7:实名认证(银商)；8：支付宝(受理版)；9：实名认证(银盛)；10：新QQ钱包受理；
    //11：银通；12:京东(线上) 999：未知)(选传)(int)
    
    /**
     * @return 返回 payTypeId
     */
    public Integer getPayTypeId()
    {
        return payTypeId;
    }
    
    /**
     * @param 对payTypeId进行赋值
     */
    public void setPayTypeId(Integer payTypeId)
    {
        this.payTypeId = payTypeId;
    }
    
    /**
     * @return 返回 successFee
     */
    public long getSuccessFee()
    {
        return successFee;
    }
    
    /**
     * @param 对successFee进行赋值
     */
    public void setSuccessFee(long successFee)
    {
        this.successFee = successFee;
    }
    
    /**
     * @return 返回 successCount
     */
    public long getSuccessCount()
    {
        return successCount;
    }
    
    /**
     * @param 对successCount进行赋值
     */
    public void setSuccessCount(long successCount)
    {
        this.successCount = successCount;
    }
    
    /**
     * @return 返回 refundFee
     */
    public long getRefundFee()
    {
        return refundFee;
    }
    
    /**
     * @param 对refundFee进行赋值
     */
    public void setRefundFee(long refundFee)
    {
        this.refundFee = refundFee;
    }
    
    /**
     * @return 返回 refundCount
     */
    public long getRefundCount()
    {
        return refundCount;
    }
    
    /**
     * @param 对refundCount进行赋值
     */
    public void setRefundCount(long refundCount)
    {
        this.refundCount = refundCount;
    }
    
    /**
     * @return 返回 checkTime
     */
    public String getCheckTime()
    {
        return checkTime;
    }
    
    /**
     * @param 对checkTime进行赋值
     */
    public void setCheckTime(String checkTime)
    {
        this.checkTime = checkTime;
    }
    
    /**
     * @return 返回 userId
     */
    public String getUserId()
    {
        return userId;
    }
    
    /**
     * @param 对userId进行赋值
     */
    public void setUserId(String userId)
    {
        this.userId = userId;
    }
    
    /**
     * @return 返回 userName
     */
    public String getUserName()
    {
        return userName;
    }
    
    /**
     * @param 对userName进行赋值
     */
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    /**
     * @return 返回 payTypeName
     */
    public String getPayTypeName()
    {
        return payTypeName;
    }
    
    /**
     * @param 对payTypeName进行赋值
     */
    public void setPayTypeName(String payTypeName)
    {
        this.payTypeName = payTypeName;
    }
    
}
