package cn.swiftpass.enterprise.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;

/**
 * Created by aijingya on 2018/3/29.
 *
 * @Package cn.swiftpass.enterprise.utils
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/3/29.17:04.
 */

public class SecureObjectInputStream extends ObjectInputStream {
    public SecureObjectInputStream() throws SecurityException, IOException {

        super();

    }

    public SecureObjectInputStream(InputStream in) throws IOException {

        super(in);

    }



    protected Class<?> resolveClass(ObjectStreamClass desc)

            throws IOException, ClassNotFoundException {

        if(desc.getName().equals("test_abcd")){  //白名单校验

            throw new ClassNotFoundException(desc.getName()+" not find");

        }

        return super.resolveClass(desc);

    }
}
