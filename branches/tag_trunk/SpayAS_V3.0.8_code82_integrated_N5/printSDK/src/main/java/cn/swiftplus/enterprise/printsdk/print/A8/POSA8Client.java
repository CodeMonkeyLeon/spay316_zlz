package cn.swiftplus.enterprise.printsdk.print.A8;

import android.content.Context;

/**
 * Created by aijingya on 2020/8/25.
 *
 * @Package cn.swiftplus.enterprise.printsdk.print.A8
 * @Description:
 * @date 2020/8/25.11:11.
 */
public class POSA8Client {
    private static POSA8Client sPosA8Client;
    private Context mContext;

    public static void initA8(Context context){
        if (sPosA8Client != null) {
            throw new RuntimeException("A8Client Already initialized");
        }
        sPosA8Client = new POSA8Client(context);
    }

    public static POSA8Client getInstance() {
        if (sPosA8Client == null) {
            throw new RuntimeException("A8Client is not initialized");
        }
        return sPosA8Client;
    }

    private POSA8Client(Context context){
        mContext = context;
        //A8 pos初始化

    }

}
