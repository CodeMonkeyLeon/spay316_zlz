package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.UserModelList
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView


/**
 * @author lizheng.zhao
 * @date 2022/11/24
 *
 * @这普普通通的愿
 * @如今成了做人的全部代价。
 */
class CashierManagerContract {


    interface View : BaseView {


        fun cashierDeleteSuccess(
            response: Boolean,
            position: Int
        )

        fun cashierDeleteFailed(error: Any?)


        fun queryCashierSuccess(
            response: UserModelList,
            type: Int,
            page: Int
        )

        fun queryCashierFailed(error: Any?)

    }

    interface Presenter : BasePresenter<View> {


        fun queryCashier(
            page: Int,
            pageSize: Int,
            input: String?,
            type: Int
        )


        fun cashierDelete(
            userId: Long,
            position: Int
        )
    }


}