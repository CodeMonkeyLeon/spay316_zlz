package cn.swiftpass.enterprise.ui.paymentlink.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.example.common.sentry.SentryUtils;

import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkProduct;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.KotlinUtils;

public class ProductSelectAdapter extends BaseRecyclerAdapter<PaymentLinkProduct> {

    private Context mContext;
    private OnProductNumChange onProductNumChange;

    public void setOnProductNumChange(OnProductNumChange onProductNumChange) {
        this.onProductNumChange = onProductNumChange;
    }

    public ProductSelectAdapter(Context context, List<PaymentLinkProduct> data) {
        super(R.layout.item_product_select, data);
        mContext = context;
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder holder, PaymentLinkProduct item, int position) {
        if (item == null) {
            return;
        }

        TextView desc1 = (TextView) holder.getView(R.id.tv_desc1);
        TextView desc2 = (TextView) holder.getView(R.id.tv_desc2);
        TextView desc3 = (TextView) holder.getView(R.id.tv_desc3);
        TextView desc4 = (TextView) holder.getView(R.id.tv_desc4);
        TextView desc5 = (TextView) holder.getView(R.id.tv_desc5);
        View line = (View) holder.getView(R.id.view_line);
        ImageView subNum = (ImageView) holder.getView(R.id.iv_sub_num);
        ImageView addNum = (ImageView) holder.getView(R.id.iv_add_num);
        ImageView imgIcon = holder.getView(R.id.id_img);


        Glide.with(mContext)
                .load(item.goodsPic)
                .error(R.drawable.icon_payment_link_default)
                .transform(new CenterCrop(), new RoundedCorners(KotlinUtils.INSTANCE.dp2px(4f, mContext)))
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imgIcon);


        desc1.setText(item.goodsName);
        if (!TextUtils.isEmpty(item.goodsCode)) {
            desc2.setText(mContext.getResources().getString(R.string.payment_link_add_product_phone_code)
                    + ": " + item.goodsCode);
        } else {
            desc2.setText("");
        }
        if (!TextUtils.isEmpty(item.goodsDesc)) {
            desc3.setText(mContext.getResources().getString(R.string.payment_link_add_product_description)
                    + ": " + item.goodsDesc);
        } else {
            desc3.setText("");
        }
        desc4.setText(MainApplication.getInstance().getFeeFh() + " " + DateUtil.formatMoneyUtils(item.goodsPrice));
        desc5.setText(item.goodsNum);
        if (position == getItemCount() - 1) {
            line.setVisibility(View.GONE);
        }

        checkNum(item, desc5, subNum, addNum);

        subNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    int num = Integer.parseInt(item.goodsNum);
                    if (num > 0) {
                        item.goodsNum = String.valueOf(num - 1);
                        desc5.setText(item.goodsNum);
                        checkNum(item, desc5, subNum, addNum);
                        if (onProductNumChange != null) {
                            onProductNumChange.onProductChange(item);
                        }
                    }
                } catch (Exception e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                }

            }
        });

        addNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    int num = Integer.parseInt(item.goodsNum);
                    if (num < 99) {
                        item.goodsNum = String.valueOf(num + 1);
                        desc5.setText(item.goodsNum);
                        checkNum(item, desc5, subNum, addNum);
                        if (onProductNumChange != null) {
                            onProductNumChange.onProductChange(item);
                        }
                    }
                } catch (Exception e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                }
            }
        });
    }

    private void checkNum(PaymentLinkProduct data, TextView desc5, ImageView subNum, ImageView addNum) {
        try {
            if (Integer.parseInt(data.goodsNum) <= 0) {
                subNum.setImageResource(R.drawable.icon_sub_gray);
                desc5.setTextColor(mContext.getResources().getColor(R.color.color_A3A3A3));
                addNum.setImageResource(R.drawable.icon_add_black);
            } else if (Integer.parseInt(data.goodsNum) < 99) {
                subNum.setImageResource(R.drawable.icon_sub_black);
                desc5.setTextColor(mContext.getResources().getColor(R.color.bg_text_new));
                addNum.setImageResource(R.drawable.icon_add_black);
            } else {
                subNum.setImageResource(R.drawable.icon_sub_black);
                desc5.setTextColor(mContext.getResources().getColor(R.color.bg_text_new));
                addNum.setImageResource(R.drawable.icon_add_gray);
            }
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }

    }

    public interface OnProductNumChange {
        void onProductChange(PaymentLinkProduct data);
    }
}
