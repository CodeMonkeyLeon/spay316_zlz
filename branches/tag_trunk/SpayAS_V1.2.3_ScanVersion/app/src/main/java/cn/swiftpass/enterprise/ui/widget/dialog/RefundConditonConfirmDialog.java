package cn.swiftpass.enterprise.ui.widget.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import cn.swiftpass.enterprise.intl.R;

/**
 * Created by aijingya on 2018/1/11.
 *
 * @Package cn.swiftpass.enterprise.ui.widget.dialog
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/1/11.16:04.
 */

public class RefundConditonConfirmDialog extends Dialog{
    //private static final String TAG = "RefundConditonConfirmDialog";
    private TextView tv_message;
    private TextView bt_left;
    private TextView bt_right;
    private Context mContext;
    private View.OnClickListener LeftButtonListener;

    public RefundConditonConfirmDialog(Activity context , View.OnClickListener ButtonListener) {
        super(context, R.style.simpleDialog);
        LeftButtonListener = ButtonListener;
        mContext = context;
        init(context);
        setCancelable(true);

    }

    private void init(Context context) {
        View rootView = View.inflate(context, R.layout.refund_toast_dialog, null);
        setContentView(rootView);

        tv_message = (TextView)rootView.findViewById(R.id.tv_message);
        bt_left = (TextView) rootView.findViewById(R.id.bt_left);
        bt_right = (TextView) rootView.findViewById(R.id.bt_right);
        bt_left.setOnClickListener(LeftButtonListener);
        bt_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    public void  setMessage(String message){
        if(!TextUtils.isEmpty(message)){
            tv_message.setText(message);
        }else{
            Toast.makeText(mContext,"with no message",Toast.LENGTH_SHORT).show();
        }
    }

}
