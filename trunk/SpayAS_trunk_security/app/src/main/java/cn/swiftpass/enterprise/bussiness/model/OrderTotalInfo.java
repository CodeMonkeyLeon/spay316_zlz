package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;
import java.util.List;

/**
 * 汇总 和 走势
 *
 * @author he_hui
 * @version [版本号, 2016-11-19]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class OrderTotalInfo implements Serializable {
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;

    //    总交易金额：countTotalFee         (int)
    //    总交易笔数：countTotalCount            (int)
    //    总退款金额：countTotalRefundFee    (int)
    //    总退款笔数：countTotalRefundCount    (int)
    //        
    //    交易日期：checkTime yyyy-MM-dd HH:mm:ss
    //    成功金额：successFee 
    //    成功笔数：successCount
    //    退款金额：refundFee
    //    退款笔数：refundCount
    //    收银员Id：userId
    //    收银员名称：userName
    //    支付类型名称：payTypeName

    private Long countTotalFee;

    private Long countTotalCount;

    private Long countTotalRefundFee;

    private Long countTotalRefundCount;

    private Integer reqFeqTime = 0;

    private String startTime;

    private String endTime;

    private long countTotalTipFee;
    private long countTotalTipFeeCount;

    public long getCountTotalTipFee() {
        return countTotalTipFee;
    }

    public void setCountTotalTipFee(long countTotalTipFee) {
        this.countTotalTipFee = countTotalTipFee;
    }

    public long getCountTotalTipFeeCount() {
        return countTotalTipFeeCount;
    }

    public void setCountTotalTipFeeCount(long countTotalTipFeeCount) {
        this.countTotalTipFeeCount = countTotalTipFeeCount;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getReqFeqTime() {
        return reqFeqTime;
    }

    public void setReqFeqTime(Integer reqFeqTime) {
        this.reqFeqTime = reqFeqTime;
    }

    private List<OrderTotalItemInfo> orderTotalItemInfo;
    public List<OrderTotalItemInfo> dataList;

    /**
     * @return 返回 orderTotalItemInfo
     */
    public List<OrderTotalItemInfo> getOrderTotalItemInfo() {
        return orderTotalItemInfo;
    }

    public void setOrderTotalItemInfo(List<OrderTotalItemInfo> orderTotalItemInfo) {
        this.orderTotalItemInfo = orderTotalItemInfo;
    }

    public Long getCountTotalFee() {
        return countTotalFee;
    }

    public void setCountTotalFee(Long countTotalFee) {
        this.countTotalFee = countTotalFee;
    }

    public Long getCountTotalCount() {
        return countTotalCount;
    }

    public void setCountTotalCount(Long countTotalCount) {
        this.countTotalCount = countTotalCount;
    }

    public Long getCountTotalRefundFee() {
        return countTotalRefundFee;
    }

    public void setCountTotalRefundFee(Long countTotalRefundFee) {
        this.countTotalRefundFee = countTotalRefundFee;
    }

    public Long getCountTotalRefundCount() {
        return countTotalRefundCount;
    }

    public void setCountTotalRefundCount(Long countTotalRefundCount) {
        this.countTotalRefundCount = countTotalRefundCount;
    }

}
