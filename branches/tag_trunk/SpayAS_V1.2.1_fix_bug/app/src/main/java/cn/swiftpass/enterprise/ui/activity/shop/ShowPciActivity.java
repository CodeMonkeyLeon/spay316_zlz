/*
 * 文 件 名:  ShowPciActivity.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-12
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.shop;

import java.io.File;

import net.tsz.afinal.FinalBitmap;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.utils.AppHelper;

/**
 * 显示图片
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-12]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class ShowPciActivity extends BaseActivity
{
    private final static String TAG = ShowPciActivity.class.getCanonicalName();
    
    private ImageView imageView;
    
    private File tempFile;
    
    private FinalBitmap finalBitmap;
    
    private LinearLayout ly_showPic;
    
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_pic);
        
        finalBitmap = FinalBitmap.create(ShowPciActivity.this);//
        try
        {
            finalBitmap.configDiskCachePath(AppHelper.getImgCacheDir());
        }
        catch (Exception e)
        {
            Log.e(TAG, "configDiskCachePath failed ");
        }
        
        imageView = (ImageView)findViewById(R.id.showPic);
        
        ly_showPic = getViewById(R.id.ly_showPic);
        
        String path = getIntent().getStringExtra("picUrl");
        
        if (!"".equals(path) && null != path && !path.equals("null"))
        {
            File file = new File(path);
            
            Bitmap bitmap = null;
            
            if (file.exists() && file.length() / 1024 > 100)
            {
                bitmap = ImagePase.createBitmap(path, ImagePase.bitmapSize_Image);
            }
            else
            {
                //            bitmap = BitmapFactory.decodeFile(path);
                bitmap = ImagePase.readBitmapFromStream(path);
            }
            if (bitmap == null)
            {
                String url = ApiConstant.BASE_URL_PORT + ApiConstant.DONLOAD_IMAGE + path;
                finalBitmap.display(imageView, url, 200, 200, null, null);
            }
            
            if (bitmap != null)
            {
                imageView.setImageBitmap(bitmap);
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), "加载图片失败!", Toast.LENGTH_SHORT).show();
        }
        
        imageView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
            
        });
        
        ly_showPic.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
        
    }
    
}
