package cn.swiftpass.enterprise.io.database.access;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.CashierReportSum;
import cn.swiftpass.enterprise.io.database.table.CashierReportSumTable;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.util.List;

/**
 * 订单统计总和
 * User: Alan
 * Date: 13-12-6
 * Time: 下午3:53
 * To change this template use File | Settings | File Templates.
 */
public class ReportSumDB {
    private static Dao<CashierReportSum, Integer> cashierReportSumDao;
    private ReportSumDB() {
        try {
            cashierReportSumDao = MainApplication.getContext().getHelper()
                    .getDao(CashierReportSum.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    private static ReportSumDB instance;
    public static ReportSumDB getInstance() {
        if (instance == null) {
            instance = new ReportSumDB();
        }
        return instance;
    }


    public void save(CashierReportSum c) throws SQLException {
        cashierReportSumDao.create(c);

    }
    public int delete(int flag,long uid) throws SQLException
    {
        DeleteBuilder<CashierReportSum,Integer> builder = cashierReportSumDao.deleteBuilder();
        Where<CashierReportSum,Integer> where = builder.where();
        where.eq(CashierReportSumTable.COLUMN_UID, uid);
        where.and();
        where.eq(CashierReportSumTable.COLUMN_FLAG, flag);
        return cashierReportSumDao.delete(builder.prepare());
    }
    public CashierReportSum queryByUId(int flag,long uid) throws SQLException
    {
        QueryBuilder<CashierReportSum,Integer> builder = cashierReportSumDao.queryBuilder();
        Where<CashierReportSum,Integer> where = builder.where();
        where.eq(CashierReportSumTable.COLUMN_UID, uid);
        where.and();
        where.eq(CashierReportSumTable.COLUMN_FLAG, flag);
        List<CashierReportSum> list = cashierReportSumDao.query(builder.prepare());
        if(list.size() > 0 )
        {
            return list.get(0);
        }
        return null;
    }

}
