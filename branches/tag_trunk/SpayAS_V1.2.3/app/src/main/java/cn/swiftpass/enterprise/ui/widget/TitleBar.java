package cn.swiftpass.enterprise.ui.widget;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 标题头部
 * */
public class TitleBar extends RelativeLayout implements OnClickListener
{
    private ImageView imgLeft;
    
    private ImageView imgRight, imgRightShare;
    
    private TextView tvTitle;
    
    public ProgressBar progressBar;
    
    private ViewStub leftViewStub;
    
    private ViewStub rightViewStub;
    
    private View leftView;
    
    private boolean leftIsInflate;
    
    private boolean IsInflateRight;
    
    private View rightView;
    
    // private Button btnRight;
    private TextView tvRightText, butText;
    
    private ImageView rtImg;
    
    private View rightViewLayout;
    
    private ImageView ivPayCentreStream;
    
    private LinearLayout mLayout;
    
    private LinearLayout rightLay, rightButLay;
    
    private RelativeLayout layBack, lay_btn_back;
    
    private TextView back_text, tv_back;
    
    private ImageView image_back, bg_image;
    
    private LinearLayout ly_title_choice;
    
    private TextView tv_title_left, tv_title_right;
    
    private ImageView image_view;
    
    public interface OnTitleBarClickListener
    {
        public void onLeftButtonClick();
        
        public void onRightButtonClick();
        
        public void onRightLayClick();
        
        public void onRightButLayClick();
    }
    
    private OnTitleBarClickListener onTitleBarClickListener;
    
    public void setOnTitleBarClickListener(OnTitleBarClickListener onTitleBarClickListener)
    {
        this.onTitleBarClickListener = onTitleBarClickListener;
    }
    
    public interface OnCentreTitleBarClickListener
    {
        public void onCentreButtonClick(View v);
        
        public void onTitleRepotLeftClick();
        
        public void onTitleRepotRigthClick();
    }
    
    private OnCentreTitleBarClickListener onCentreTitleBarClickListener;
    
    public void setOnCentreTitleBarClickListener(OnCentreTitleBarClickListener onCentreTitleBarClickListener)
    {
        this.onCentreTitleBarClickListener = onCentreTitleBarClickListener;
    }
    
    public TitleBar(Context context)
    {
        super(context);
    }
    
    public TitleBar(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
    
    @Override
    protected void onFinishInflate()
    {
        super.onFinishInflate();
        leftViewStub = (ViewStub)findViewById(R.id.vs_leftview);
        rightViewStub = (ViewStub)findViewById(R.id.vs_rightview);
        tvTitle = (TextView)findViewById(R.id.tvTitle);
        ly_title_choice = (LinearLayout)findViewById(R.id.ly_title_choice);
        ivPayCentreStream = (ImageView)findViewById(R.id.iv_pay_centre_stream);
        mLayout = (LinearLayout)findViewById(R.id.centerLay);
        mLayout.setOnClickListener(this);
        
        tv_title_left = (TextView)findViewById(R.id.tv_title_left);
        tv_title_right = (TextView)findViewById(R.id.tv_title_right);
        tv_title_left.setOnClickListener(this);
        tv_title_right.setOnClickListener(this);
    }
    
    /**
     * 设置导航栏的背景
     * <功能详细描述>
     * @param visible
     * @see [类、类#方法、类#成员]
     */
    public void setTiteBarBackgroundColor(int res)
    {
        this.setBackgroundResource(R.drawable.bg);
    }
    
    /**
     * 设置导航栏的背景颜色
     * <功能详细描述>
     * @param visible
     * @see [类、类#方法、类#成员]
     */
    public void setTiteBackgroundColor(String res)
    {
        if (!StringUtil.isEmptyOrNull(res))
            this.setBackgroundColor(Color.parseColor(res));
    }
    
    /**
     * 设置导航栏的字体颜色
     * <功能详细描述>
     * @param visible
     * @see [类、类#方法、类#成员]
     */
    public void setTiteBarForntColor(String res)
    {
        if (!leftIsInflate)
        {
            leftViewInit();
        }
        if (!IsInflateRight)
            initRightLayout();
        
        if (null != tv_back && !StringUtil.isEmptyOrNull(res))
        {
            tv_back.setTextColor(Color.parseColor(res));
        }
        else
        {
            layBack.setBackgroundResource(R.drawable.micro_pay_);
        }
        
        if (null != butText && !StringUtil.isEmptyOrNull(res))
        {
            butText.setTextColor(Color.parseColor(res));
        }
        
        if (null != tvTitle && !StringUtil.isEmptyOrNull(res))
        {
            tvTitle.setTextColor(Color.parseColor(res));
        }
        
    }
    
    public void setLeftButtonVisible(boolean visible)
    {
        if (visible)
        {
            leftViewInit();
        }
        else
        {
            if (leftView != null)
                leftView.setVisibility(GONE);
        }
    }
    
    public void setLeftButtonResVisible(boolean visible, int resId)
    {
        if (visible)
        {
            leftViewInit();
            lay_btn_back.setVisibility(View.GONE);
            layBack.setVisibility(View.VISIBLE);
            image_view.setImageResource(resId);
        }
        else
        {
            if (leftView != null)
                leftView.setVisibility(GONE);
        }
    }
    
    public void setLeftButtonIsVisible(boolean visible)
    {
        if (!visible)
        {
            leftViewInit();
            
            layBack.setVisibility(View.GONE);
        }
        
    }
    
    /**
     * <一句话功能简述>
     * <功能详细描述>
     * @param visible
     * @see [类、类#方法、类#成员]
     */
    public void setLeftButtonImage(int res)
    {
        if (!leftIsInflate)
        {
            leftViewInit();
            
            layBack.setVisibility(View.GONE);
            lay_btn_back.setVisibility(View.VISIBLE);
            //            image_back.setImageResource(res);
            back_text.setVisibility(View.GONE);
        }
        else
        {
            lay_btn_back.setVisibility(View.VISIBLE);
            //            image_back.setImageResource(res);
        }
        
    }
    
    /**
     * 设置左边图标和文字
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    public void setLeftView(int res, int img)
    {
        if (!leftIsInflate)
        {
            leftViewInit();
        }
        else
        {
            if (leftView != null)
            {
                layBack.setVisibility(View.GONE);
                lay_btn_back.setVisibility(View.VISIBLE);
                //                back_text.setText(res);
                image_back.setImageResource(img);
            }
        }
    }
    
    private void leftViewInit()
    {
        // viewsub 只能inflate一次
        leftView = leftViewStub.inflate();
        imgLeft = (ImageView)findViewById(R.id.imgLeft);
        imgLeft.setOnClickListener(this);
        leftIsInflate = true;
        
        lay_btn_back = (RelativeLayout)findViewById(R.id.lay_btn_back);
        lay_btn_back.setOnClickListener(this);
        image_view = (ImageView)findViewById(R.id.image_view);
        layBack = (RelativeLayout)findViewById(R.id.layBack);
        layBack.setOnClickListener(this);
        
        image_back = (ImageView)findViewById(R.id.image_back);
        
        back_text = (TextView)findViewById(R.id.back_text);
        
        tv_back = (TextView)findViewById(R.id.tv_back);
        
    }
    
    public void setLeftText(int res)
    {
        if (!leftIsInflate)
        {
            leftViewInit();
        }
        if (tv_back != null)
        {
            tv_back.setText(res);
        }
    }
    
    public void setLeftButton(int res)
    {
        if (!leftIsInflate)
        {
            leftViewInit();
        }
        if (res > 0)
        {
            imgLeft.setBackgroundResource(res);
        }
    }
    
    public void setRightButton(int res)
    {
        if (!IsInflateRight)
        {
            initRightLayout();
        }
        imgRight.setVisibility(VISIBLE);
        if (res > 0)
        {
            imgRight.setBackgroundResource(res);
        }
    }
    
    public void setRightButtonVisible(boolean visible, String text)
    {
        if (!IsInflateRight)
        {
            initRightLayout();
        }
        //imgRight.setVisibility(GONE);
        //btnRight.setVisibility(VISIBLE);
        //btnRight.setText(text);
        if (visible)
        {
            imgRight.setVisibility(VISIBLE);
            tvRightText.setVisibility(VISIBLE);
            tvRightText.setText(text);
            if (text.endsWith(ToastHelper.toStr(R.string.tx_svae)))
            {
                tvRightText.setTextSize(12);
            }
        }
        else
        {
            tvRightText.setVisibility(GONE);
            imgRight.setVisibility(GONE);
        }
    }
    
    public void setRightButtonShare(int gone)
    {
        if (!IsInflateRight)
        {
            initRightLayout();
        }
        imgRight.setVisibility(View.GONE);
        tvRightText.setVisibility(View.GONE);
        imgRightShare.setVisibility(View.VISIBLE);
        imgRightShare.setOnClickListener(this);
        
    }
    
    private void initRightLayout()
    {
        rightView = rightViewStub.inflate();
        rightViewLayout = findViewById(R.id.rlRight);
        imgRight = (ImageView)findViewById(R.id.imgRight);
        imgRight.setOnClickListener(this);
        //btnRight = (Button) findViewById(R.id.btnRight);
        tvRightText = (TextView)findViewById(R.id.imgright_title);
        // btnRight.setOnClickListener(this);
        progressBar = (ProgressBar)findViewById(R.id.pr_loading);
        imgRightShare = (ImageView)findViewById(R.id.imgRightShare);
        IsInflateRight = true;
        
        rightLay = (LinearLayout)findViewById(R.id.rightLay);
        rightLay.setOnClickListener(this);
        
        rightButLay = (LinearLayout)findViewById(R.id.rightButLay);
        rightButLay.setOnClickListener(this);
        
        butText = (TextView)findViewById(R.id.butText);
        rtImg = (ImageView)findViewById(R.id.rtImg);
        
        bg_image = (ImageView)findViewById(R.id.bg_image);
    }
    
    public void setRightLayVisible(boolean visible, int resId)
    {
        
        if (!IsInflateRight)
        {
            initRightLayout();
        }
        if (visible)
        {
            rightView.setVisibility(View.VISIBLE);
            rightLay.setVisibility(View.VISIBLE);
            //            bg_image.setBackgroundResource(resId);
        }
        else
        {
            rightLay.setVisibility(View.GONE);
            if (rightView != null)
                rightView.setVisibility(GONE);
        }
    }
    
    public void setRightButLayVisible(boolean visible, String title)
    {
        
        if (!IsInflateRight)
        {
            initRightLayout();
        }
        if (visible)
        {
            rightView.setVisibility(View.VISIBLE);
            rightButLay.setVisibility(View.VISIBLE);
        }
        else
        {
            rightButLay.setVisibility(View.GONE);
            if (rightView != null)
                rightView.setVisibility(GONE);
        }
        
        butText.setText(title);
    }
    
    public void setRightButLayVisibleForTotal(boolean visible, String title)
    {
        
        if (!IsInflateRight)
        {
            initRightLayout();
        }
        if (visible)
        {
            rightView.setVisibility(View.VISIBLE);
            rightButLay.setVisibility(View.VISIBLE);
        }
        else
        {
            rightButLay.setVisibility(View.GONE);
            if (rightView != null)
                rightView.setVisibility(GONE);
        }
        rtImg.setVisibility(View.GONE);
        butText.setVisibility(View.VISIBLE);
        butText.setText(title);
    }
    
    public void setRightButLayVisible(boolean visible, int resId)
    {
        if (!IsInflateRight)
        {
            initRightLayout();
        }
        if (visible)
        {
            butText.setVisibility(View.GONE);
            rtImg.setImageResource(resId);
            rtImg.setVisibility(View.VISIBLE);
            rightView.setVisibility(View.VISIBLE);
            rightButLay.setVisibility(View.VISIBLE);
        }
        else
        {
            rightButLay.setVisibility(View.GONE);
            if (rightView != null)
                rightView.setVisibility(GONE);
        }
        
    }
    
    public void setRightButLayTextColor(boolean visible, int resId)
    {
        
        if (!IsInflateRight)
        {
            initRightLayout();
        }
        if (visible)
        {
            rightView.setVisibility(View.VISIBLE);
            rightButLay.setVisibility(View.VISIBLE);
            butText.setVisibility(View.VISIBLE);
            butText.setTextColor(resId);
        }
        else
        {
            rightButLay.setVisibility(View.GONE);
            if (rightView != null)
                rightView.setVisibility(GONE);
            butText.setVisibility(View.GONE);
        }
        
    }
    
    public void setRightButLayBackground(int resid)
    {
        butText.setVisibility(View.GONE);
        butText.setBackgroundResource(resid);
        rtImg.setVisibility(View.VISIBLE);
        rtImg.setImageResource(resid);
    }
    
    public void setRightImageBackground(int resid)
    {
        rtImg.setVisibility(View.VISIBLE);
        butText.setVisibility(View.GONE);
        rtImg.setImageResource(resid);
    }
    
    /** 右边 */
    public void setRightButtonVisible(boolean visible)
    {
        if (visible)
        {
            initRightLayout();
        }
        else
        {
            if (rightView != null)
                rightView.setVisibility(GONE);
        }
    }
    
    /** 右边按钮 */
    public void setRightButton(String text, int res)
    {
        if (!IsInflateRight)
        {
            initRightLayout();
        }
        if (res > 0)
        {
            imgRight.setBackgroundResource(res);
        }
        else
        {
            // imgRight.setBackgroundResource(R.drawable.btn_save);// 默认保存图标
        }
    }
    
    /** 显示右边进度条 */
    public void setRightLodingVisible(boolean visible)
    {
        if (!IsInflateRight)
        {
            initRightLayout();
        }
        if (visible)
        {
            progressBar.setVisibility(VISIBLE);
            rightViewLayout.setVisibility(GONE);
            //imgRight.setVisibility(GONE);
        }
        else
        {
            progressBar.setVisibility(INVISIBLE);
            //imgRight.setVisibility(VISIBLE);
            rightViewLayout.setVisibility(VISIBLE);
            
        }
    }
    
    /** 显示右边进度条 */
    public void setRightLodingVisible(boolean visible, boolean buttonVisible)
    {
        if (!IsInflateRight)
        {
            initRightLayout();
        }
        if (visible)
        {
            progressBar.setVisibility(VISIBLE);
            imgRight.setVisibility(GONE);
        }
        else
        {
            progressBar.setVisibility(View.GONE);
            rightViewLayout.setVisibility(buttonVisible ? VISIBLE : GONE);
            imgRight.setVisibility(buttonVisible ? VISIBLE : GONE);
            tvRightText.setVisibility(buttonVisible ? VISIBLE : GONE);
            
        }
    }
    
    public void setRightButtonSelected(boolean selected)
    {
        if (!IsInflateRight)
        {
            initRightLayout();
        }
        imgRight.setSelected(selected);
    }
    
    public void setTitle(int res)
    {
        /*if (MainApplication.getContext().getString(res).endsWith("流水"))
        {
            ivPayCentreStream.setVisibility(View.VISIBLE);
            mLayout.setOnClickListener(this);
        }*///有pos在打开
        tvTitle.setText(MainApplication.getContext().getString(res));
    }
    
    public void setTitleChoice(boolean res)
    {
        if (res)
        {
            tvTitle.setVisibility(View.GONE);
            ly_title_choice.setVisibility(View.VISIBLE);
        }
    }
    
    public void setTitleImage(boolean res)
    {
        if (res)
        {
            ivPayCentreStream.setVisibility(View.VISIBLE);
        }
    }
    
    public void setTitle(String res)
    {
        tvTitle.setText(res);
        tvTitle.setVisibility(View.VISIBLE);
    }
    
    public void setTitleText(int res)
    {
        tvTitle.setText(getResources().getString(res));
        tvTitle.setVisibility(View.VISIBLE);
    }
    
    public void setTitle(String text, int res)
    {
        if (res > 0)
        {
            // Drawable d = getContext().getResources().getDrawable(res);
            tvTitle.setVisibility(View.GONE);
        }
        tvTitle.setText(text);
    }
    
    /**
     * 汇总
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    public void setTitleLeftView()
    {
        tv_title_left.setTextColor(getResources().getColor(R.color.title_bg_new));
        tv_title_right.setTextColor(getResources().getColor(R.color.tempt_title_color));
        ly_title_choice.setBackgroundResource(R.drawable.general_top_left_choose);
    }
    
    /**
     * 走势
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    public void setTitleRightView()
    {
        tv_title_left.setTextColor(getResources().getColor(R.color.tempt_title_color));
        tv_title_right.setTextColor(getResources().getColor(R.color.title_bg_new));
        ly_title_choice.setBackgroundResource(R.drawable.general_top_right_choose);
    }
    
    @Override
    public void onClick(View v)
    {
        if (onTitleBarClickListener != null)
        {
            switch (v.getId())
            {
                case R.id.lay_btn_back:
                    onTitleBarClickListener.onLeftButtonClick();
                    break;
                case R.id.layBack:
                    onTitleBarClickListener.onLeftButtonClick();
                    break;
                case R.id.imgRightShare:
                case R.id.imgRight:
                    onTitleBarClickListener.onRightButtonClick();
                    break;
                
                case R.id.centerLay:
                    onCentreTitleBarClickListener.onCentreButtonClick(tvTitle);
                    break;
                case R.id.tv_title_left:
                    setTitleLeftView();
                    onCentreTitleBarClickListener.onTitleRepotLeftClick();
                    break;
                case R.id.tv_title_right:
                    setTitleRightView();
                    onCentreTitleBarClickListener.onTitleRepotRigthClick();
                    break;
                case R.id.rightLay:
                    onTitleBarClickListener.onRightLayClick();
                    break;
                case R.id.rightButLay:
                    onTitleBarClickListener.onRightButLayClick();
                    break;
            
            }
        }
    }
}
