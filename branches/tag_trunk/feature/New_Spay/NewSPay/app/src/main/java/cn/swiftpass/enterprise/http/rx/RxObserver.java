package cn.swiftpass.enterprise.http.rx;

import android.text.TextUtils;

import org.apache.http.conn.ConnectTimeoutException;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import cn.swiftpass.enterprise.R;
import cn.swiftpass.enterprise.SpayApplication;
import cn.swiftpass.enterprise.base.BaseBean;
import cn.swiftpass.enterprise.http.HttpException;
import cn.swiftpass.enterprise.widget.ToastUtil;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * 创建人：caoxiaoya
 * 时间：2019/4/22
 * 描述：Rxjava-Observer封装
 * 备注：
 */
public abstract class RxObserver<T> implements Observer<T> {

    @Override
    public void onSubscribe(Disposable d) {
    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onError(Throwable throwable) {
        if (throwable instanceof Exception) {
            onFail(HttpException.handleException(throwable).getCode(), HttpException.handleException(throwable).getMessage());
            //仅限无网络的情况下弹出
            if (throwable instanceof SocketTimeoutException||
                    throwable instanceof ConnectException||
                    throwable instanceof ConnectTimeoutException ||
                    throwable instanceof UnknownHostException){
                ToastUtil.show(SpayApplication.getAppContext().getResources().getString(R.string.show_no_network));
            }
        } else {
            //errormsg暂时均写no data
            onFail(HttpException.ERROR.UNKNOWN, "No data!");
        }
    }

    @Override
    public void onNext(T t) {
        if (t != null) {
            BaseBean bean = (BaseBean) t;
            //判空
            if (bean.getData() != null) {
                if (!TextUtils.isEmpty(bean.getCode()) && bean.getCode().equals("200")) {
                    //只返回请求成功200的时候
                    onSuccess(t);
                } else {
                    //400或者其他值，返回code和msg
                    String errorCode;
                    String errorMsg = "";
                    try {
                        //由于服务器Code返回String可能会是空指针或者空字符，加一层判断
                        if (!TextUtils.isEmpty(bean.getCode())) {
                            errorCode = bean.getCode();
                        } else {
                            //上述情况出现，赋值表明空
                            errorCode = HttpException.ERROR.NULL_POINTER_EXCEPTION;
                        }
                        //同上述
                        if (!TextUtils.isEmpty(bean.getError())) {
                            errorMsg = bean.getError();
                        } else {
                            errorMsg = "No data!";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        //如果出现异常赋值给errorCode
                        errorCode = HttpException.ERROR.NULL_POINTER_EXCEPTION;
                    }
                    onFail(errorCode, errorMsg);
                }
            } else {
                if (!TextUtils.isEmpty(bean.getCode())) {
                    onFail(bean.getCode(), bean.getError());
                } else {
                    onFail(HttpException.ERROR.NULL_POINTER_EXCEPTION, "No data!");
                }
            }
        } else {//否则假如失败状态异常，自定义网络（网络连通的情况）异常
            onFail(HttpException.ERROR.NULL_POINTER_EXCEPTION, "No data!");
        }

    }

    public abstract void onSuccess(T data);

    public abstract void onFail(String code, String errorMsg);
}
