package cn.swiftpass.enterprise.ui.activity.shop;

import java.io.File;
import java.util.List;

import net.tsz.afinal.FinalBitmap;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.GridViewBean;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.utils.AppHelper;

public class Personal_GridViewAdapter extends BaseAdapter
{
    private final static String TAG = Personal_GridViewAdapter.class.getCanonicalName();
    
    private LayoutInflater inflater;
    
    private Context context;
    
    private List<GridViewBean> beans;
    
    private FinalBitmap finalBitmap;//AppHelper.getImgCacheDir()
    
    public Personal_GridViewAdapter(Context context, List<GridViewBean> beans)
    {
        this.context = context;
        this.beans = beans;
        this.inflater = LayoutInflater.from(context);
        
        finalBitmap = FinalBitmap.create(context);//
        try
        {
            finalBitmap.configDiskCachePath(AppHelper.getImgCacheDir());
            finalBitmap.configBitmapLoadThreadSize(5);
        }
        catch (Exception e)
        {
            Log.e(TAG, "configDiskCachePath failed ");
        }
        
    }
    
    @Override
    public int getCount()
    {
        return beans.size();
    }
    
    @Override
    public Object getItem(int position)
    {
        return beans.get(position);
    }
    
    @Override
    public long getItemId(int position)
    {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final ViewHolder holder;
        Bitmap bitmap = null;
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.personal_gridview_item, null);
            holder = new ViewHolder();
            holder.name = (TextView)convertView.findViewById(R.id.name);
            holder.imageView = (ImageView)convertView.findViewById(R.id.id_photo);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder)convertView.getTag();
        }
        final GridViewBean bean = beans.get(position);
        
        try
        {
            holder.name.setText(bean.getText());
            
            //            holder.imageView.setImageBitmap(bitmap);
            
            if (bean.getImageUrl() != null && !"".equals(bean.getImageUrl()))
            {
                // 1,第一种图片是从ShopkeeperNextActivity加载，2、是从网上下载，3、从本地获取
                // 优先从本地读取图片
                File file = new File(bean.getImageUrl());
                
                if (file.exists()) // 本地存在就从本地取图片
                {
                    bitmap = ImagePase.createBitmap(bean.getImageUrl(), 100);
                    if (bitmap != null)
                    {
                        holder.imageView.setImageBitmap(bitmap);
                    }
                }
                else
                {
                    //                    String path = AppHelper.getImgCacheDir() + bean.getImageUrl();
                    //                    file = new File(path);
                    //                    if (file.exists())
                    //                    {
                    //                        bitmap = ImagePase.createBitmap(path, 100);
                    //                    }
                    //                    else
                    //                    {
                    // 从服务器下载 
                    String url = ApiConstant.BASE_URL_PORT + ApiConstant.DONLOAD_IMAGE + bean.getImageUrl();
                    finalBitmap.display(holder.imageView, url);
                    //                    }
                }
                
            }
            
        }
        catch (Exception e)
        {
            Log.e(TAG, "get root path failed " + e.getMessage());
        }
        
        return convertView;
        
    }
    
    public class ViewHolder
    {
        public TextView name;
        
        public ImageView imageView;
    }
    
}
