/*
package cn.swiftpass.enterprise.utils;

import android.app.Activity;
import android.content.ClipboardManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.hardware.Camera;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentActivity;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


*/
/**
 * Created by admin on 2017/12/18.
 *
 * @Package cn.swiftpass.wallet.intl.utils
 * @Description: ${TODO}(Android相关的基础工具类)
 * @date 2017/12/18.14:51.
 *//*


public class AndroidUtils {
    private static final String TAG = "AndroidUtils";

    */
/**
     * 将dip或dp值转换为px值，保证尺寸大小不变
     *
     * @param context
     * @param dpValue （DisplayMetrics类中属性density）
     * @return
     *//*

    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    //同上反向转换
    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    public static int getScreenWidth(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return dm.widthPixels;
    }


    public static void showKeyboard(Activity activity, EditText editText) {
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }


    public static String subZeroAndDot(String s) {
        if (TextUtils.isEmpty(s)) {
            return "0";
        }
        if (s.indexOf(".") > 0) {
            s = s.replaceAll("0+?$", "");//去掉多余的0
            s = s.replaceAll("[.]$", "");//如最后一位是.则去掉
        }
        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.HALF_DOWN);
        String result = df.format(Double.parseDouble(s));
        return result;
    }

    public static int getActionBarHeight(Context context) {
        // Calculate ActionBar height
        TypedValue tv = new TypedValue();
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            return TypedValue.complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics());
        }
        return 0;
    }

    public static void showKeyboardView(EditText editText) {
        InputMethodManager inputManager = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.showSoftInput(editText, 0);
    }

    public static void showKeyboardDelay(final EditText editText) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager inputManager = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.showSoftInput(editText, 0);
            }
        }, 300);
    }

    */
/**
     * 判断点击的位置是否在view范围内
     *
     * @param view
     * @param ev
     * @return
     *//*

    public static boolean inRangeOfView(View view, MotionEvent ev) {
        if (view != null) {
            int[] location = new int[2];
            view.getLocationOnScreen(location);
            int x = location[0];
            int y = location[1];
            if (ev.getX() < x || ev.getX() > (x + view.getWidth()) || ev.getY() < y || ev.getY() > (y + view.getHeight())) {
                return false;
            }
            return true;
        }
        return true;
    }

    public static void hideKeyboard(View view) {
        if (view != null && view.getContext() != null) {
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }


    public static int getScreenHeight(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        int screenHeight = dm.heightPixels;
        return screenHeight;
    }


    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


    public static Bitmap loadBitmapFromView(View v, int width, int height, int color) {
        return loadBitmapFromView(v, width, height, color, Bitmap.Config.RGB_565);
    }

    public static Bitmap loadBitmapFromView(View v, int width, int height, int color, Bitmap.Config config) {
        Bitmap bmp = Bitmap.createBitmap(width, height, config);
        Canvas c = new Canvas(bmp);
        c.drawColor(color);
        */
/** 如果不设置canvas画布为白色，则生成透明 *//*

        v.layout(0, 0, width, height);
        v.draw(c);
        return bmp;
    }


    public static void shareImage(Context mcontext, String title, String imgPath) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        File f = new File(imgPath);
        if (f != null && f.exists() && f.isFile()) {
            intent.setType("image/jpeg");
            Uri u = Uri.fromFile(f);
            intent.putExtra(Intent.EXTRA_STREAM, u);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        intent.putExtra(Intent.EXTRA_SUBJECT, title);
        mcontext.startActivity(Intent.createChooser(intent, title));
    }

    public static void shareImage(Context mcontext, String title, Uri imgPath) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_STREAM, imgPath);
        intent.setType("image/jpeg");
        intent.putExtra(Intent.EXTRA_SUBJECT, title);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        mcontext.startActivity(Intent.createChooser(intent, title));

    }


    public static void layoutView(View v, int width, int height) {
        v.layout(0, 0, width, height);
        int measuredWidth = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY);
        int measuredHeight = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.AT_MOST);
        v.measure(measuredWidth, measuredHeight);
        v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
    }


    */
/**
     * 根据作用域保存图片 Android 10后需要用这个方案
     * 暂时通过 AndroidMainfest 里面
     * android:requestLegacyExternalStorage="true"
     * 来进行兼容
     *
     * @param context
     * @param bmp
     * @return
     *//*

    public static Uri saveImageToSdCardByScopedStorage(Context context, Bitmap bmp) {
        // 首先保存图片 必须带文件后缀
        String fileName = "shareImage.jpg";
        FileOutputStream fos = null;

        File appDir = null;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                appDir = context.getExternalCacheDir();
            } else {
                String storePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + context.getString(R.string.app_name);
                appDir = new File(storePath);
            }


            if (!appDir.exists()) {
                appDir.mkdir();
            } else {
            }
            File file = new File(appDir, fileName);

            fos = new FileOutputStream(file);
            boolean isSuccess = bmp.compress(Bitmap.CompressFormat.JPEG, 60, fos);
            fos.flush();
            //兼容Android Q和以下版本
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                Uri uriForFile = FileProvider.getUriForFile(
                        context,
                        BuildConfig.PROVIDER_AUTHORITIES,
                        file
                );
                return uriForFile;
            } else {
                return Uri.fromFile(file);
            }


        } catch (Exception e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        } finally {
            FileUtils.closeIO(fos);
        }
        return null;

    }

    public static Uri saveImageToGalleryUriByScopedStorage(Context context, Bitmap bmp, String name) {
        // 首先保存图片 必须带文件后缀
        String fileName = name + ".jpg";
        OutputStream outputStream = null;
        FileOutputStream fos = null;
        try {
            //设置保存参数到ContentValues中
            ContentValues contentValues = new ContentValues();
            //设置文件名
            contentValues.put(MediaStore.Images.Media.DISPLAY_NAME, fileName);
            //设置文件类型
            contentValues.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
            //兼容Android Q和以下版本
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                //android Q中不再使用DATA字段，而用RELATIVE_PATH代替
                //RELATIVE_PATH是相对路径不是绝对路径
                //DCIM是系统文件夹，关于系统文件夹可以到系统自带的文件管理器中查看，不可以写没存在的名字
                contentValues.put(MediaStore.Images.Media.RELATIVE_PATH, Environment.DIRECTORY_DCIM);
                //因为insert 如果uri存在的话，会返回null 所以需要先delete
                Cursor query = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        new String[]{MediaStore.Images.Media.DISPLAY_NAME},
                        MediaStore.Images.Media.DISPLAY_NAME + "=?",
                        new String[]{fileName},
                        null
                );
                if (query != null && query.getCount() > 0) {
                    context.getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            MediaStore.Images.Media.DISPLAY_NAME + "=?",
                            new String[]{fileName});

                }
                //执行insert操作，向系统文件夹中添加文件
                //EXTERNAL_CONTENT_URI代表外部存储器，该值不变
                Uri uri = context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
                if (uri != null) {
                    //若生成了uri，则表示该文件添加成功
                    //使用流将内容写入该uri中即可
                    outputStream = context.getContentResolver().openOutputStream(uri);
                    if (outputStream != null) {
                        boolean isSuccess = bmp.compress(Bitmap.CompressFormat.JPEG, 60, outputStream);
                        outputStream.flush();
                        outputStream.close();
                        return uri;
                    }
                }
            } else {
                String storePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + context.getString(R.string.app_name);
                File appDir = new File(storePath);
                if (!appDir.exists()) {
                    appDir.mkdir();
                }
                File file = new File(appDir, fileName);

                fos = new FileOutputStream(file);
                //通过io流的方式来压缩保存图片
                boolean isSuccess = bmp.compress(Bitmap.CompressFormat.JPEG, 60, fos);
                fos.flush();
                //保存图片后发送广播通知更新数据库
                Uri uri = Uri.fromFile(file);
                context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
                if (isSuccess) {
                    return uri;
                } else {
                    return null;
                }
            }

        } catch (IOException e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        } finally {
            FileUtils.closeIO(outputStream);
            FileUtils.closeIO(fos);
        }
        return null;
    }

    public static boolean saveImageToGalleryByScopedStorage(Context context, Bitmap bmp, String name) {
        // 首先保存图片 必须带文件后缀
        String fileName = name + ".jpg";
        OutputStream outputStream = null;
        FileOutputStream fos = null;
        try {
            //设置保存参数到ContentValues中
            ContentValues contentValues = new ContentValues();
            //设置文件名
            contentValues.put(MediaStore.Images.Media.DISPLAY_NAME, fileName);
            //设置文件类型
            contentValues.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
            //兼容Android Q和以下版本
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                //android Q中不再使用DATA字段，而用RELATIVE_PATH代替
                //RELATIVE_PATH是相对路径不是绝对路径
                //DCIM是系统文件夹，关于系统文件夹可以到系统自带的文件管理器中查看，不可以写没存在的名字
                contentValues.put(MediaStore.Images.Media.RELATIVE_PATH, Environment.DIRECTORY_DCIM);
                //因为insert 如果uri存在的话，会返回null 所以需要先delete
                context.getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        MediaStore.Images.Media.DISPLAY_NAME + "=?",
                        new String[]{fileName});
                //执行insert操作，向系统文件夹中添加文件
                //EXTERNAL_CONTENT_URI代表外部存储器，该值不变
                Uri uri = context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
                if (uri != null) {
                    //若生成了uri，则表示该文件添加成功
                    //使用流将内容写入该uri中即可
                    outputStream = context.getContentResolver().openOutputStream(uri);
                    if (outputStream != null) {
                        boolean isSuccess = bmp.compress(Bitmap.CompressFormat.JPEG, 60, outputStream);
                        outputStream.flush();
                        outputStream.close();

                        return isSuccess;
                    }
                }
            } else {
                String storePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + context.getString(R.string.app_name);
                File appDir = new File(storePath);
                if (!appDir.exists()) {
                    appDir.mkdir();
                }
                File file = new File(appDir, fileName);
                fos = new FileOutputStream(file);
                //通过io流的方式来压缩保存图片
                boolean isSuccess = bmp.compress(Bitmap.CompressFormat.JPEG, 60, fos);
                fos.flush();
                //保存图片后发送广播通知更新数据库
                Uri uri = Uri.fromFile(file);
                context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
                return isSuccess;
            }

        } catch (IOException e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        } finally {
            FileUtils.closeIO(outputStream);
            FileUtils.closeIO(fos);
        }
        return false;
    }

    public static boolean saveImageToGalleryByScopedStorage(Context context, Bitmap bmp) {
        // 首先保存图片 必须带文件后缀
        String fileName = "transfer_" + localImageStr(System.currentTimeMillis());
        return saveImageToGalleryByScopedStorage(context, bmp, fileName);
    }


    */
/**
     * 获取当前app包信息对象.
     *
     * @param context
     * @return
     * @throws PackageManager.NameNotFoundException
     *//*

    private static PackageInfo getCurrentAppPackageInfo(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            String packageName = context.getPackageName();
            PackageInfo info = manager.getPackageInfo(packageName, 0);
            return info;
        } catch (PackageManager.NameNotFoundException e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
            throw new RuntimeException(e);
        }
    }


    */
/**
     * 获取当前app的版本名字.
     *
     * @param context
     * @return
     *//*

    public static String getCurrentAppVersionName(Context context) {
        PackageInfo info = getCurrentAppPackageInfo(context);
        String version = info.versionName;
        return version;
    }


    */
/**
     * 获取手机显示数据.
     *
     * @param activity 活动对象
     * @return 手机手机显示数据
     *//*

    public static DisplayMetrics getDisplayMetrics(Activity activity) {
        if (activity != null) {
            DisplayMetrics metric = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(metric);
            return metric;
        } else {
            throw new RuntimeException("Activity must not be null.");
        }
    }


    public static boolean needRequestApplyPermissien() {

        return Build.VERSION.SDK_INT >= 23;
    }


    public static boolean isValidLocationNetwork(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean providerEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        return providerEnabled;
    }


    public static boolean isValidLocationGps(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean providerEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return providerEnabled;
    }

    public static boolean isPhoneNetworkValid(Context context, int type) {
        ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo.State mobile = conMan.getNetworkInfo(type).getState();
        return mobile == NetworkInfo.State.CONNECTED || mobile == NetworkInfo.State.CONNECTING;
    }


    public static void copyToClipboard(Context context, String text) {
        ClipboardManager clip = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        //clip.getText(); // 粘贴
        clip.setText(text); // 复制
    }


    public static String getFromAssets(Context context, String fileName) {
        InputStreamReader inputReader = null;
        BufferedReader bufReader = null;
        try {
            inputReader = new InputStreamReader(context.getResources().getAssets().open(fileName));
            bufReader = new BufferedReader(inputReader);
            String line = "";
            String Result = "";
            while ((line = bufReader.readLine()) != null) {
                Result += line;
            }
            return Result;
        } catch (Exception e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        } finally {
            FileUtils.closeIO(inputReader, bufReader);
        }
        return null;
    }


    public static String localImageStr(long time) {
        return long2str(time, "yyyyMMdd_HHmmss");
    }

    public static String long2str(long time, String format) {
        if (TextUtils.isEmpty(format)) {
            format = "yyyy-MM-dd HH:mm:ss";
        }
        return new SimpleDateFormat(format).format(long2calendar(time).getTime());
    }

    public static Calendar long2calendar(long time) {
        Date date = new Date(time);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c;
    }


    //根据当前输入的内容每四位添加一个空格，返回值为拼接之后的字符串
    public static String addSpaceByInputContent(String content, int Totalcount, String spaceString) {
        if (TextUtils.isEmpty(content)) {
            return "";
        }
        //去空格
        content = content.replaceAll(" +", ""); //去掉所有空格
        if (TextUtils.isEmpty(content)) {
            return "";
        }
        //用户名限制为8位的电话Totalcount=8
        if (content.length() > Totalcount) {
            content = content.substring(0, Totalcount);
        }
        StringBuilder newString = new StringBuilder();
        for (int i = 1; i <= content.length(); i++) {
            //当为第4位时，并且不是最后一个第4位时
            //拼接字符的同时，拼接一个空格spaceString
            //如果在最后一个第四位也拼接，会产生空格无法删除的问题
            //因为一删除，马上触发输入框改变监听，又重新生成了空格
            if (i % 4 == 0 && i != content.length()) {
                newString.append(content.charAt(i - 1) + spaceString);
            } else {
                //如果不是4位的倍数，则直接拼接字符即可
                newString.append(content.charAt(i - 1));
            }
        }
        return newString.toString();
    }


    */
/**
     * 验证六位支付密码是否连续  123456
     *
     * @param pwdStr
     * @return
     *//*

    private static boolean checkPwdLegal(String pwdStr) {
        if (pwdStr.length() != 6) return false;
        int index = 0;
        int lastValue = Integer.valueOf(pwdStr.charAt(index) + "").intValue() + 1;
        int nextValue = Integer.valueOf(pwdStr.charAt(index + 1) + "").intValue();
        while (lastValue == nextValue && index <= pwdStr.length() - 3) {
            index += 1;
            lastValue = Integer.valueOf(pwdStr.charAt(index) + "").intValue() + 1;
            nextValue = Integer.valueOf(pwdStr.charAt(index + 1) + "").intValue();
        }
        if (lastValue == nextValue) {
            return index != pwdStr.length() - 2;
        } else {
            return true;
        }
    }

    */
/**
     * 返回true 表示可以使用  返回false表示不可以使用
     *//*

    public static boolean cameraIsCanUse() {
        boolean isCanUse = true;
        Camera mCamera = null;
        try {
            mCamera = Camera.open();
            Camera.Parameters mParameters = mCamera.getParameters(); //针对魅族手机
            mCamera.setParameters(mParameters);
        } catch (Exception e) {
            isCanUse = false;
        }

        if (mCamera != null) {
            try {
                mCamera.release();
            } catch (Exception e) {
                LogUtils.e(TAG, Log.getStackTraceString(e));
                return isCanUse;
            }
        }
        return isCanUse;
    }


    public static void startAppSetting(Context mContext) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:" + mContext.getPackageName()));
        mContext.startActivity(intent);
    }


    public static boolean checkPwdIsLegal(String pwdStr) {
        return !checkPwdIsSameLegal(pwdStr) && !checkPwdIsBackLegal(pwdStr) && checkPwdLegal(pwdStr);
    }

    */
/**
     * 密码复杂度 倒叙 654321
     *
     * @param pwdStr
     * @return 123321
     *//*

    private static boolean checkPwdIsBackLegal(String pwdStr) {
        if ("098765".equals(pwdStr) || "567890".equals(pwdStr)) {
            //需求如此
            return true;
        }
        if (pwdStr.length() != 6) return false;
        int index = pwdStr.length() - 1;
        int lastValue = Integer.valueOf(pwdStr.charAt(index) + "").intValue() + 1;
        int nextValue = Integer.valueOf(pwdStr.charAt(index - 1) + "").intValue();
        while (lastValue == nextValue && index > 1) {
            index -= 1;
            lastValue = Integer.valueOf(pwdStr.charAt(index) + "").intValue() + 1;
            nextValue = Integer.valueOf(pwdStr.charAt(index - 1) + "").intValue();
        }
        if (lastValue != nextValue) return false;
        return index != pwdStr.length() - 1;
    }


    private static boolean checkPwdIsSameLegal(String pwdStr) {
        if (pwdStr.length() != 6) return false;
        for (int i = 0; i < pwdStr.length() - 1; i++) {
            int lastValue = Integer.valueOf(pwdStr.charAt(i) + "").intValue();
            int nextValue = Integer.valueOf(pwdStr.charAt(i + 1) + "").intValue();
            if (lastValue != nextValue) {
                return false;
            }
        }
        return true;
    }


    */
/**
     * 登出操作
     *//*

    public static void clearMemoryCache() {
        //退出
        Activity context = MyActivityManager.getInstance().getCurrentActivity();
        if (context == null) {
            return;
        }
        HttpCoreKeyManager.getInstance().clearMemoryCache();
        //等出之后内存中 卡列表数据也要清空
        ProjectApp.setCardEntities(null);
        FileUtils.clearFile(context, "", Constants.CACHE_CARDLIST_FILENAME);
        MyActivityManager.removeAllTaskExcludeMainStack();
        ActivitySkipUtil.startAnotherActivity(context, LoginActivity.class);
        CacheManagerInstance.getInstance().saveLogoutStatus();
        CacheManagerInstance.getInstance().setCardEntities(null);
        //清除缓存之后 引导页的显示 不应该清除
        CacheManagerInstance.getInstance().setShowGuidePage(true);
    }


    public static void forbidCopyForEditText(EditText editText) {
        editText.setLongClickable(false);
        editText.setCustomSelectionActionModeCallback(new ActionModeCallbackInterceptor());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // call that method
            editText.setCustomInsertionActionModeCallback(new ActionModeCallbackInterceptor());
        }
    }


    public static void clearMemoryCacheOnly() {
        //退出
        Activity context = MyActivityManager.getInstance().getCurrentActivity();
        if (context == null) {
            return;
        }
        HttpCoreKeyManager.getInstance().clearMemoryCache();
        //等出之后内存中 卡列表数据也要清空
        ProjectApp.setCardEntities(null);
        FileUtils.clearFile(context, "", Constants.CACHE_CARDLIST_FILENAME);
        //ProjectApp.removeAllTaskExcludeMainStack();
        CacheManagerInstance.getInstance().saveLogoutStatus();
        CacheManagerInstance.getInstance().setCardEntities(null);
        //清除缓存之后 引导页的显示 不应该清除
        CacheManagerInstance.getInstance().setShowGuidePage(true);
    }

    public static void goLogin() {
        //退出
        Activity context = MyActivityManager.getInstance().getCurrentActivity();
        if (context == null) {
            return;
        }
        ActivitySkipUtil.startAnotherActivity(context, PreLoginActivity.class);
    }

    public static String formatPrice(double price, boolean halfUp) {
        DecimalFormat formater = new DecimalFormat();
        formater.setMaximumFractionDigits(2);
        formater.setGroupingSize(3);
        formater.setRoundingMode(halfUp ? RoundingMode.HALF_UP : RoundingMode.FLOOR);
        return formater.format(price);
    }

    public static String getCardType(String payeeCardType, Context mConext) {

        if (TextUtils.isEmpty(payeeCardType)) {
            return "";
        }
        if (TextUtils.equals(payeeCardType, "P")) {
            return mConext.getString(R.string.CT1_2b_1);
        } else if (TextUtils.equals(payeeCardType, "D")) {
            return mConext.getString(R.string.CT1_2b_3);
        } else if (TextUtils.equals(payeeCardType, "C")) {
            return mConext.getString(R.string.CT1_2b_2);
        } else {
            return payeeCardType;
        }
    }

    public static String formatPriceWithPoint(double price, boolean halfUp) {
        DecimalFormat formater = new DecimalFormat();
        formater.setMaximumFractionDigits(2);
        formater.setGroupingSize(3);
        formater.setRoundingMode(halfUp ? RoundingMode.HALF_UP : RoundingMode.FLOOR);
        String values = formater.format(price);
        //两位小数的要求
        try {
            if (values.contains(".")) {
                String item = values.substring(values.indexOf(".") + 1, values.length());
                if (item.length() == 1) {
                    values += "0";
                }
            } else {
                values += ".00";
            }
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        return values;
    }


    public static float getScreenDensity(Activity activity) {
        DisplayMetrics mDisplayMetrics = new DisplayMetrics();//屏幕分辨率容器
        activity.getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
        int width = mDisplayMetrics.widthPixels;
        int height = mDisplayMetrics.heightPixels;
        float density = mDisplayMetrics.density;
        int densityDpi = mDisplayMetrics.densityDpi;
        return density;
    }


    public static void showErrorMsgDialog(Context mContext, String msg, final OnMsgClickCallBack onMsgClickCallBack) {
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setPositiveButton(mContext.getString(R.string.dialog_right_btn_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onMsgClickCallBack.onBtnClickListener();
            }
        });
        try {
            Activity mActivity = (Activity) mContext;
            if (mActivity != null && !mActivity.isFinishing()) {
                CustomMsgDialog mDealDialog = builder.create();
                if (!mActivity.isFinishing()) {
                    mDealDialog.show();
                }

            }
        } catch (Exception e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        }
    }


    public static String getFormatBankCardNumber(String originalCardNumber) {
        if (TextUtils.isEmpty(originalCardNumber)) {
            return "";
        }
        if (originalCardNumber.length() < 4) {
            return originalCardNumber;
            //return "";
        }
        int lenth = originalCardNumber.length();
        return originalCardNumber.substring(lenth - 4, lenth);
    }


    public static String getSubMasCardNumber(String originalCardNumber) {
        if (TextUtils.isEmpty(originalCardNumber)) {
            return "";
        }
        if (originalCardNumber.length() < 4) {
            return originalCardNumber;
            //return "";
        }
        String cardNumber = originalCardNumber.replace("-", "");
        int lenth = cardNumber.length();
        return cardNumber.substring(lenth - 4, lenth);
    }

    public static String getSubNumberBrackets(String originalCardNumber) {
        if (TextUtils.isEmpty(originalCardNumber)) {
            return "";
        }
        if (originalCardNumber.length() < 4) {
            return originalCardNumber;
            //return "";
        }
        String cardNumber = originalCardNumber.replace("-", "");
        int lenth = cardNumber.length();
        return "(" + cardNumber.substring(lenth - 4, lenth) + ")";
    }

    */
/**
     * 区分智能账户 支付账户
     *
     * @param mcontext
     * @param originalCardNumber
     * @param cardType
     * @return
     *//*

    public static String getSubMasCardNumberTitleByLevel(Context mcontext, String originalCardNumber, String cardType) {
        String str_cardType = mcontext.getString(R.string.card_info_debit_name);
        if (TextUtils.equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT, cardType)) {
            str_cardType = mcontext.getString(R.string.payment_account);
        } else if (TextUtils.equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT_THREE, cardType)) {
            str_cardType = mcontext.getString(R.string.smart_account_old);
        } else {
            str_cardType = mcontext.getString(R.string.smart_account);
        }
        return str_cardType + "(" + getSubMasCardNumber(originalCardNumber) + ")";
    }

    public static String getSubMasCardNumberTitle(Context mcontext, String originalCardNumber, String cardType) {

        String str_cardType = mcontext.getString(R.string.card_info_debit_name);
        if (TextUtils.isEmpty(cardType)) {
            return str_cardType + "(" + "" + ")";
        }
        if (TextUtils.equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT, cardType)) {
            str_cardType = mcontext.getString(R.string.smart_account);
        }
        return str_cardType + "(" + getSubMasCardNumber(originalCardNumber) + ")";
    }

    public static String getSubMasCardNumberTitle(String originalCardNumber, Context mcontext) {
        return mcontext.getString(R.string.card_info_debit_name) + "(" + getSubMasCardNumber(originalCardNumber) + ")";
    }

    public static String getSubSmartCardNumberTitle(String originalCardNumber, Context mcontext) {
        return mcontext.getString(R.string.home_smartaccount) + "(" + originalCardNumber + ")";
    }

    public static String getSubSmartCardNumberTitleNew(String originalCardNumber, Context mcontext) {
        return mcontext.getString(R.string.smart_account_old) + "(" + originalCardNumber + ")";
    }

    public static String getPaymentTitleNew(String originalCardNumber, Context mcontext) {
        return mcontext.getString(R.string.payment_account) + "(" + originalCardNumber + ")";
    }

    public static String getMasCardNumberTitle(String originalCardNumber, Context mcontext) {
        return mcontext.getString(R.string.card_info_debit_name) + "(" + getSubMasCardNumber(originalCardNumber) + ")";
    }

    public static String formatCardNumberStr(String cardNumber) {
        if (cardNumber.length() == 16) {
            return cardNumber.subSequence(0, 4) + "    " + cardNumber.subSequence(4, 8) + "     " + cardNumber.subSequence(8, 12) + "     " + cardNumber.subSequence(12, 16);
        }
        return cardNumber;
    }

    public static String formatCardNumberStrShort(String cardNumber) {
        if (cardNumber.length() == 16) {
            return cardNumber.subSequence(0, 4) + " " + cardNumber.subSequence(4, 8) + " " + cardNumber.subSequence(8, 12) + " " + cardNumber.subSequence(12, 16);
        }
        return cardNumber;
    }

    public static String formatCardMastNumberStr(String cardNumber) {
        if (TextUtils.isEmpty(cardNumber)) {
            return null;
        }
        if (cardNumber.length() > 4) {
            return "· · · · " + cardNumber.subSequence(cardNumber.length() - 4, cardNumber.length());
        }
        return cardNumber;
    }

    public static String formatCardMastNumber(String cardNumber) {
        if (TextUtils.isEmpty(cardNumber)) {
            return null;
        }
        if (cardNumber.length() > 4) {
            return "· · · · " + cardNumber.subSequence(cardNumber.length() - 4, cardNumber.length());
        }
        return cardNumber;
    }

    public static String formatSpecialCardMastNumberStr(String cardNumber) {
        if (TextUtils.isEmpty(cardNumber)) {
            return null;
        }
        if (cardNumber.length() > 4) {
            return "* * * * " + cardNumber.subSequence(cardNumber.length() - 4, cardNumber.length());
        }
        return cardNumber;
    }

    public static String formatCardMastLongNumberStr(String cardNumber) {
        if (cardNumber.length() == 16) {
            return "****" + "     " + "****" + "     " + "****" + "     " + cardNumber.subSequence(12, 16);
        }
        return cardNumber;
    }


    public static String formatCenterPhoneNumberStr(String cardNumber) {
        if (TextUtils.isEmpty(cardNumber)) {
            return "";
        }
        if (cardNumber.length() > 4) {
            int endIndex = cardNumber.indexOf("-") + 1;
            int otherLength = cardNumber.length() - endIndex - 4;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < otherLength; i++) {
                sb.append("*");
            }
            return cardNumber.subSequence(0, endIndex) + sb.toString() + cardNumber.subSequence(cardNumber.length() - 4, cardNumber.length());
        }
        return cardNumber;
    }


    public static String getTrxCurrency(String numStr) {
        if (!TextUtils.isEmpty(numStr)) {
            for (int i = 0; i < ProjectApp.getCurrencyList().size(); i++) {
                CurrencyCodeEntity.DataBean item = ProjectApp.getCurrencyList().get(i);
                if (numStr.equals(item.getNum() + "")) {
                    return item.getCode();

                }
            }
        }
        return "HKD";
    }

    public static Bitmap stringtoBitmap(String string) {
        Bitmap bitmap = null;
        try {
            byte[] bitmapArray;
            bitmapArray = android.util.Base64.decode(string, android.util.Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0, bitmapArray.length);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    //sdk方法，穷举情况多
    public static boolean isRooted() {
        boolean flag = false;
        try {
            if (!BuildConfig.SKIP_ROOT) {
                RootBeer rootBeer = new RootBeer(ProjectApp.getContext());
                return (rootBeer.isRootedWithoutBusyBoxCheck() || rootBeer.detectRootCloakingApps());
            }
        } catch (Exception e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        }
        return flag;
    }


    private static boolean isGoogleSdk() {
        String str = Settings.Secure.getString(ProjectApp.getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        return ("sdk".equals(Build.PRODUCT)) || ("google_sdk".equals(Build.PRODUCT)) || (str == null);

    }


    public static String formatTimeString(String timeStr) {
        //1230235959
        if (timeStr.length() == 10) {
            String year = timeStr.substring(0, 2);
            String month = timeStr.substring(2, 4);
            String hour = timeStr.substring(4, 6);
            String minute = timeStr.substring(6, 8);
            String second = timeStr.substring(8, 10);
            return hour + ":" + minute + ":" + second + " " + month + "/" + year;
        } else if (timeStr.length() == 14) {
            // 20190429144824
            String year = timeStr.substring(0, 4);
            String month = timeStr.substring(4, 6);
            String day = timeStr.substring(6, 8);
            String hour = timeStr.substring(8, 10);
            String minute = timeStr.substring(10, 12);
            String second = timeStr.substring(12, 14);
            return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
        }
        return timeStr;
    }


    public static String formatDateStr(String expire) {

        if (expire == null) {
            return "";
        }
        if (expire.length() < 6) {
            return expire;
        }
        return expire.substring(0, 4) + "/" + expire.substring(4, 6) + "/" + expire.substring(6, 8);
    }


    public static String getCurrencyAmount(Object amount) {
        return ProjectApp.CURRENCY_SYMBOL + " " + String.valueOf(amount);
    }


    public static String getPrimaryAccountDisplay(String accountType, String accountNo) {
        String display = "";
        if (null != accountType) {
            display += accountType;
        }
        if (null != accountNo) {
            String account = getSubMasCardNumber(accountNo);
            display = display + "(" + account + ")";
        }
        return display;
    }


    public static String getPrimaryAccountDisplay(SmartAccountCheckAccountEntity entity) {
        String display = "";
        String accountType = null;
        String accountNo = null;
        if (null != entity) {
            accountType = entity.getAccType();
            accountNo = entity.getAccountNo();
        }
        if (null != accountType) {
            display += accountType;
        }
        if (null != accountNo) {
            accountNo = getSubMasCardNumber(accountNo);
            display = display + "(" + accountNo + ")";
        }
        return display;
    }

    public static String getTopUpMethodValue(Context context, String method) {
        if (TextUtils.equals(method, SmartAccountConst.TOPUP_METHOD_DIRECT)) {
            return context.getString(R.string.top_up_direct);
        } else if (TextUtils.equals(method, SmartAccountConst.TOPUP_METHOD_MANUAL)) {
            return context.getString(R.string.top_up_manual);
        } else if (TextUtils.equals(method, SmartAccountConst.TOPUP_METHOD_AUTO)) {
            return context.getString(R.string.top_up_auto);
        }
        return ProjectApp.getContext().getString(R.string.top_up_auto);
    }

    public static void showTipDialog(Activity activity, int resId) {
        showTipDialog(activity, activity.getResources().getString(resId), null);
    }

    public static void showTipDialog(Activity activity, String msg) {
        showTipDialog(activity, msg, null);
    }

    public static void showTipDialog(Activity activity, String msg, String btnText) {
        showTipDialog(activity, null, msg, btnText);
    }

    public static void showTipDialogWithCusSize(Activity activity, String title, String msg, int fontSize, String btnText) {
        if (msg == null) {
            return;
        }
        if (TextUtils.isEmpty(msg)) {
            msg = "";
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(activity);
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }
        builder.setMessage(msg);
        builder.setMessageTextSize(fontSize);
        String btn = btnText;
        if (TextUtils.isEmpty(btn)) {
            btn = activity.getString(R.string.POPUP1_1);
        }
        builder.setPositiveButton(btn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        if (activity != null && !activity.isFinishing()) {
            CustomMsgDialog mDealDialog = builder.create();
            mDealDialog.show();
        }
    }

    public static void showTipDialogWithIdv(Activity activity, String message, final ConfirmErrorCodeClickListener onClickListener) {
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(activity);
        builder.setMessage(message);
        String btn = activity.getString(R.string.POPUP1_1);
        builder.setPositiveButton(btn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (onClickListener != null) {
                    onClickListener.onConfirmBtnClick();
                }

            }
        });
        if (activity != null && !activity.isFinishing()) {
            CustomMsgDialog mDealDialog = builder.create();
            mDealDialog.show();
        }
    }

    public static void showTipDialog(Activity activity, String title, String msg, String btnText) {
        if (msg == null) {
            return;
        }
        if (TextUtils.isEmpty(msg)) {
            msg = "";
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(activity);
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }
        builder.setMessage(msg);
        String btn = btnText;
        if (TextUtils.isEmpty(btn)) {
            btn = activity.getString(R.string.POPUP1_1);
        }
        builder.setPositiveButton(btn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        if (activity != null && !activity.isFinishing()) {
            CustomMsgDialog mDealDialog = builder.create();
            mDealDialog.show();
        }
    }

    public static void showBindSmartAccountAndCancelDialogTwo(final Activity activity, String titleMsg, String rightBtnStr, DialogInterface.OnClickListener rightListener) {
        String msg = titleMsg;
        String left = activity.getString(R.string.string_cancel);
        String right = rightBtnStr;
        DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (activity != null && !(activity instanceof MainHomeActivity) && !(activity instanceof TransferSelTypeActivity)) {
                    activity.finish();
                }
            }
        };
        AndroidUtils.showTipDialog(activity, activity.getString(R.string.KJ209_1_1), msg, right, left, rightListener, cancelListener);
    }

    public static void showBindSmartAccount(final Activity activity, String titleMsg, String contentMsg, String rightBtnStr, DialogInterface.OnClickListener rightListener, DialogInterface.OnClickListener cancelListener) {
        String title = titleMsg;
        String msg = contentMsg;
        String left = activity.getString(R.string.string_cancel);
        String right = rightBtnStr;
        AndroidUtils.showTipDialog(activity, title, msg, right, left, rightListener, cancelListener);
    }


    public static void showBindSmartAccountAndCancelDialog(final Activity activity, String titleMsg, String contentMsg, String rightBtnStr, DialogInterface.OnClickListener rightListener) {
        String title = titleMsg;
        String msg = contentMsg;
        String left = activity.getString(R.string.string_cancel);
        String right = rightBtnStr;
        DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (activity != null && !(activity instanceof MainHomeActivity) && !(activity instanceof TransferSelTypeActivity) && !(activity instanceof ScanActivity)) {
                    activity.finish();
                }
            }
        };
        AndroidUtils.showTipDialog(activity, title, msg, right, left, rightListener, cancelListener);
    }


    public static void showBindSmartAccountAndCancelDialog(final Activity activity, String titleMsg, String rightBtnStr) {
        String title = activity.getString(R.string.SMB1_1_11);
        String msg = titleMsg;
        String left = activity.getString(R.string.string_cancel);
        String right = rightBtnStr;
        DialogInterface.OnClickListener rightListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                HashMap<String, Object> maps = new HashMap<>();
                maps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
                RegisterSmartAccountDescActivity.startActivity(activity, maps);
            }
        };
        DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (activity != null && !(activity instanceof MainHomeActivity) && !(activity instanceof TransferSelTypeActivity)) {
                    activity.finish();
                }
            }
        };
        AndroidUtils.showTipDialog(activity, title, msg, right, left, rightListener, cancelListener);
    }

    //提示用户去绑定我的账户
    public static void showBindSmartAccountAndCancelDialog(final Activity activity) {
        showBindSmartAccountAndCancelDialog(activity, activity.getString(R.string.SMB1_1_12), activity.getString(R.string.title_string_bind));
    }


//    //提示用户去绑定我的账户 结果回调
//    public static void showBindSmartAccountDialog(final Activity activity, final DialogInterface.OnClickListener negtiveClickListener) {
//        String title = activity.getString(R.string.link_smart_account_title);
//        String msg = activity.getString(R.string.SMB1_1_12);
//        String left = activity.getString(R.string.dialog_left_btn_cancel);
//        String right = activity.getString(R.string.title_string_bind);
//        DialogInterface.OnClickListener rightListener = new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                HashMap<String, Object> maps = new HashMap<>();
//                maps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
//                RegisterSmartAccountDescActivity.startActivity(activity, maps);
//            }
//        };
//        DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                negtiveClickListener.onClick(dialog, which);
//            }
//        };
//        AndroidUtils.showTipDialog(activity, title, msg, right, left, rightListener, cancelListener);
//    }


    //提示用户去绑定我的账户
    public static void showBindSmartAccountDialog(final Activity activity) {
        String title = activity.getString(R.string.SMB1_1_11);
        String msg = activity.getString(R.string.SMB1_1_12);
        String left = activity.getString(R.string.dialog_left_btn_cancel);
        String right = activity.getString(R.string.title_string_bind);
        DialogInterface.OnClickListener rightListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                HashMap<String, Object> maps = new HashMap<>();
                maps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
                RegisterSmartAccountDescActivity.startActivity(activity, maps);
            }
        };

        AndroidUtils.showTipDialog(activity, title, msg, right, left, rightListener);
    }

    public static void showTipDialog(Activity activity, String title, String msg, String posText, String negText, final DialogInterface.OnClickListener posClickListener) {
        showTipDialog(activity, title, msg, posText, negText, posClickListener, null);
    }


    public static void showTipDialog(Activity activity, String title, String msg, String posText, String negText, final DialogInterface.OnClickListener posClickListener, final DialogInterface.OnClickListener negClickListener) {
        CustomDialog.Builder builder = new CustomDialog.Builder(activity);
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }
        if (!TextUtils.isEmpty(msg)) {
            builder.setMessage(msg);
        }
        String strPos = TextUtils.isEmpty(posText) ? activity.getString(R.string.button_ok) : posText;
        builder.setPositiveButton(strPos, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (null != posClickListener) {
                    posClickListener.onClick(dialog, which);
                }
            }
        });
        String strNeg = TextUtils.isEmpty(negText) ? activity.getString(R.string.dialog_left_btn_cancel) : negText;
        builder.setNegativeButton(strNeg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (null != negClickListener) {
                    negClickListener.onClick(dialog, which);
                }
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        if (!activity.isFinishing()) {
            CustomDialog dialog = builder.create();
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    public static boolean isEmail(String email) {
        if (null == email || "".equals(email)) return false;
        String emailPattern = "\\b[A-Z0-9._%-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b";
        Pattern p = Pattern.compile(emailPattern, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(email);
        return m.matches();
    }


    public static void showFingerprintGuideDialog(Activity activity, View.OnClickListener posClickListener, View.OnClickListener negClickListener) {
        FingerPrintGuideDialog.Builder builder = new FingerPrintGuideDialog.Builder(activity);
        builder.setOnClickListener(posClickListener, negClickListener);
        if (!activity.isFinishing()) {
            FingerPrintGuideDialog dialog = builder.create();
            dialog.show();
        }
    }

    public static void showAmountSetDialog(Activity activity, String title, String msg, String posText, String negText, final DialogInterface.OnClickListener posClickListener) {
        CustomDialog.Builder builder = new CustomDialog.Builder(activity);
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }
        if (!TextUtils.isEmpty(msg)) {
            builder.setMessage(msg);
        }
        String strPos = TextUtils.isEmpty(posText) ? activity.getString(R.string.button_ok) : posText;
        builder.setPositiveButton(strPos, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (null != posClickListener) {
                    posClickListener.onClick(dialog, which);
                }
            }
        });
        String strNeg = TextUtils.isEmpty(negText) ? activity.getString(R.string.dialog_left_btn_cancel) : negText;
        builder.setNegativeButton(strNeg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        if (!activity.isFinishing()) {
            CustomDialog dialog = builder.create();
            dialog.show();
        }
    }

    public static void showTipDialog(Activity activity, String title, String msg, String posText, String negText) {
        showTipDialog(activity, title, msg, posText, negText, null);
    }

    public static String getFlowFromType(int type) {
        String flow = "";
        if (type == Constants.PAGE_FLOW_BIND_CARD || type == Constants.DATA_CARD_TYPE_SHOW_INVITE) {
            flow = ApiConstant.FLOW_BINDING;
        } else if (type == Constants.DATA_CARD_TYPE_LOGIN) {
            flow = ApiConstant.FLOW_LOGIN;
        } else if (type == Constants.PAGE_FLOW_FORGETPASSWORD) {
            flow = ApiConstant.FLOW_FORGET_PASSWORD;
        } else if (type == Constants.PAGE_FLOW_REGISTER) {
            flow = ApiConstant.FLOW_REGISTER;
        } else if (type == Constants.DATA_TYPE_SET_FIO) {
            flow = ApiConstant.FLOW_FIO;
        } else if (type == Constants.SMART_ACCOUNT_UPDATE) {
            flow = ApiConstant.UPDATE_SMARTACCOUNT;
        } else if (type == Constants.PAGE_FLOW_VITUALCARD_REGISTER) {
            flow = ApiConstant.VIRTUALCARD_REGISTER;
        } else if (type == Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD) {
            flow = ApiConstant.VIRTUALCARD_FORGETPWD;
        }
        return flow;
    }

    */
/**
     * base64转为bitmap
     *
     * @param base64Data
     * @return
     *//*

    public static Bitmap base64ToBitmap(String base64Data) {
        byte[] bytes = android.util.Base64.decode(base64Data, android.util.Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }


    */
/**
     * 1、区号不是852的，电话号码长度不能小于7位
     * 2、区号是852的，电话号码长度必须是8位，且首位不能是0 、1、2、3
     *
     * @param countryCode
     * @return
     *//*

    public static InputFilter[] getPhoneInputFilter(String countryCode) {
        if (TextUtils.isEmpty(countryCode)) {
            return new InputFilter[]{new InputFilter.LengthFilter(10000)};
        }
        if (countryCode.equals(Constants.DATA_COUNTRY_CODES[0])) {
            return new InputFilter[]{new InputFilter.LengthFilter(11)};
        } else if (countryCode.equals(Constants.DATA_COUNTRY_CODES[1])) {
            return new InputFilter[]{new InputFilter.LengthFilter(8)};
        } else if (countryCode.equals(Constants.DATA_COUNTRY_CODES[2])) {
            return new InputFilter[]{new InputFilter.LengthFilter(8)};
        } else {
//            return new InputFilter[]{new InputFilter.LengthFilter(11)};
            return new InputFilter[]{new InputFilter.LengthFilter(10000)};
        }
    }

    public static InputFilter[] getPhoneInputFilterForRegister(String countryCode) {
        if (countryCode.equals(Constants.DATA_COUNTRY_CODES[0])) {
            return new InputFilter[]{new InputFilter.LengthFilter(11)};
        } else if (countryCode.equals(Constants.DATA_COUNTRY_CODES[1])) {
            return new InputFilter[]{new InputFilter.LengthFilter(8)};
        } else if (countryCode.equals(Constants.DATA_COUNTRY_CODES[2])) {
            return new InputFilter[]{new InputFilter.LengthFilter(8)};
        } else {
            return new InputFilter[]{new InputFilter.LengthFilter(19)};
        }
    }


    public static void showConfrimExitOtherDialog(Activity mContext, final ConfirmClickListener confirmClickListener) {
        CustomDialog.Builder builder = new CustomDialog.Builder(mContext);
        builder.setMessage(mContext.getString(R.string.register_otp_title));
        builder.setTitle("           ");
        builder.setPositiveButton(mContext.getString(R.string.confirm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                confirmClickListener.onConfirmBtnClick();
            }
        });
        builder.setNegativeButton(mContext.getString(R.string.dialog_left_btn_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomDialog mDealDialog = builder.create();
            mDealDialog.show();
        }
    }

    public static void openUrl(Context mContext, String genInfo) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        Uri content_url = Uri.parse(genInfo);
        intent.setData(content_url);
        mContext.startActivity(intent);
    }


    public static void showIdvCodeEvent(String errorCode, Activity context, final ConfirmErrorCodeClickListener onClickListener) {
//        if (AndroidUtils.tempsErrorCode(errorCode)) {
//            //vss问题
//            AndroidUtils.showTipDialogWithIdv(context, context.getString(R.string.DJ011) + "(" + errorCode + ")", onClickListener);
//            return;
//        }
        if (!AndroidUtils.showErrorCode(errorCode)) {
            if (onClickListener != null) {
                onClickListener.onConfirmBtnIngoreErrorCodeClick();
            }
            return;
        }

        AndroidUtils.showIdvErrorCode(errorCode, context, onClickListener);
    }

    public static boolean isSameNumber(String reMobile, String contactNumber) {
        if (TextUtils.isEmpty(reMobile) || TextUtils.isEmpty(contactNumber)) {
            return false;
        }
        if (TextUtils.equals(reMobile, contactNumber)) {
            return true;
        }

        contactNumber = contactNumber.replace("-", "");
        reMobile = reMobile.replace("-", "");
        String items[] = {"+86", "86", "852", "+852", "853", "+853"};
        for (int i = 0; i < items.length; i++) {
            if (contactNumber.startsWith(items[i])) {
                contactNumber = contactNumber.substring(items[i].length());
                break;
            }
        }
        for (int i = 0; i < items.length; i++) {
            if (reMobile.startsWith(items[i])) {
                reMobile = reMobile.substring(items[i].length());
                break;
            }
        }
        return (reMobile.equals(contactNumber));
    }

    public static String getIdvLanguage(Context mContext) {
        String language = "S";
        String lan = SpUtils.getInstance(mContext).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            language = "S";
        } else if (AndroidUtils.isHKLanguage(lan)) {
            language = "T";
        } else {
            language = "E";
        }
        return language;
    }

    public static void hideKeyBoardInput(Activity mActivity) {
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    public static String apkVerifyWithInfo(Context context) {
        String apkPath = context.getPackageCodePath();
        try {
            MessageDigest dexDigest = MessageDigest.getInstance("SHA-1");
            byte[] bytes = new byte[1024];
            int byteCount;
            FileInputStream fis = new FileInputStream(new File(apkPath));
            while ((byteCount = fis.read(bytes)) != -1) {
                dexDigest.update(bytes, 0, byteCount);
            }
            BigInteger bigInteger = new BigInteger(1, dexDigest.digest());
            String sha = bigInteger.toString(16);
            fis.close();
            return sha;
        } catch (NoSuchAlgorithmException e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public interface ConfirmClickListener {

        void onConfirmBtnClick();
    }

    public interface ConfirmErrorCodeClickListener {

        void onConfirmBtnClick();

        void onConfirmBtnIngoreErrorCodeClick();
    }


    public static void showConfrimExitDialog(Activity mContext, final ConfirmClickListener confirmClickListener) {
        CustomDialog.Builder builder = new CustomDialog.Builder(mContext);
        builder.setTitle(mContext.getString(R.string.register_otp_title));
        builder.setMessage(mContext.getString(R.string.register_otp_disable_message));
        builder.setPositiveButton(mContext.getString(R.string.confirm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                confirmClickListener.onConfirmBtnClick();
            }
        });
        builder.setNegativeButton(mContext.getString(R.string.dialog_left_btn_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomDialog mDealDialog = builder.create();
            mDealDialog.show();
        }
    }


    public static void showConfrimExitDialog(Activity mContext, String title, final ConfirmClickListener confirmClickListener) {
        CustomDialog.Builder builder = new CustomDialog.Builder(mContext);
        builder.setTitle(mContext.getString(R.string.IDV_28_1));
        builder.setMessage(title);
        builder.setPositiveButton(mContext.getString(R.string.IDV_28_3), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                confirmClickListener.onConfirmBtnClick();
            }
        });
        builder.setNegativeButton(mContext.getString(R.string.IDV_28_4), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomDialog mDealDialog = builder.create();
            mDealDialog.show();
        }
    }


    public static boolean isHKLanguage(String lan) {
        if (TextUtils.isEmpty(lan)) {
            return false;
        }
        return (lan.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_ZH_TW) ||
                lan.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_ZH_MO) ||
                lan.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_ZH_HK) ||
                lan.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_ZH_TW_NORMAL) ||
                lan.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_ZH_MO_NEW_NORMAL) ||
                lan.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_ZH_HK_NORMAL) ||
                lan.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_ZH_MO_NEW_NORMAL));
    }


    public static boolean isEGLanguage(String lan) {
        if (TextUtils.isEmpty(lan)) {
            return false;
        }
        return (lan.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_EN_US) ||
                lan.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_EN_US_NORMAL));
    }

    public static boolean isZHLanguage(String lan) {
        if (TextUtils.isEmpty(lan)) {
            return false;
        }
        return (lan.equalsIgnoreCase(Constant.LANG_CODE_ZH_CN) ||
                (lan.equalsIgnoreCase(Constant.LANG_CODE_ZH_CN_NORMAL)));
    }


    public static void showOpenFioFailed(Activity activity, DialogInterface.OnClickListener posListener, DialogInterface.OnClickListener negListener) {
        String str_title = " ";
        String str_msg = activity.getString(R.string.open_touch_fail_msg);
        String str_cancel = activity.getString(R.string.open_touch_not);
        String str_confirm = activity.getString(R.string.open_touch_try);
        AndroidUtils.showTipDialog(activity, str_title, str_msg, str_confirm, str_cancel, posListener, negListener);
    }

    public static String subPaymentMethod(String paymentMethod) {
        if (TextUtils.isEmpty(paymentMethod)) {
            return "";
        }
        if (paymentMethod.length() >= 4) {
            return paymentMethod.substring(paymentMethod.length() - 4, paymentMethod.length());
        }
        return "";
    }

    public static String getCardFaceUrl(String cardType) {
        if (TextUtils.equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT, cardType)) {
            return ApiConstant.SMART_CARD_ART_URL;
        } else {
            return ApiConstant.CARD_ART_URL;
        }
    }


    //resid
    public static int getDefaultBankFlag(String defaultBank) {
        return TextUtils.equals(defaultBank, "Y") ? R.string.dialog_left_btn_yes : R.string.dialog_left_btn_no;
    }

    public static String fpsEmailFormat(String emailAddress) {
        if (TextUtils.isEmpty(emailAddress) || !emailAddress.contains("@")) {
            return emailAddress;
        }
        int index = emailAddress.indexOf("@");
        String preStr = emailAddress.substring(0, index);
        if (preStr.length() > 3) {
            String preStrSub = preStr.substring(0, preStr.length() - 3);
            return preStrSub + "***" + emailAddress.substring(index);
        } else if (preStr.length() > 2) {
            String preStrSub = preStr.substring(0, preStr.length() - 2);
            return preStrSub + "**" + emailAddress.substring(index);
        } else if (preStr.length() > 1) {
            String preStrSub = preStr.substring(0, preStr.length() - 1);
            return preStrSub + "*" + emailAddress.substring(index);
        } else {
            return "*" + emailAddress.substring(index);
        }
    }

    public static String fpsPhoneFormat(String phoneNum) {
        if (!TextUtils.isEmpty(phoneNum)) {
            if (phoneNum.length() < 8) {
                String regex = "[0-9]";
                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(phoneNum);
                return matcher.replaceAll("*");
            } else {
                return phoneNum.substring(0, phoneNum.length() - 6) + "***" + phoneNum.substring(phoneNum.length() - 3);
            }
        }
        return "";
    }

    public static String genateJson(ArrayList<TaxInfoEntity> taxInfoEntities) {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        JSONObject tmpObj = null;
        int count = taxInfoEntities.size();
        for (int i = 0; i < count; i++) {
            try {
                tmpObj = new JSONObject();
                tmpObj.put("residenceJurisdiction", taxInfoEntities.get(i).getResidenceJurisdiction());
                tmpObj.put("taxNumber", taxInfoEntities.get(i).getTaxNumber());
                jsonArray.put(tmpObj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        String personInfos = jsonArray.toString();
        return personInfos;
    }


    public static InputFilter getEmjoFilter() {
        //表情过滤器
        InputFilter emojiFilter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                Pattern emoji = Pattern.compile("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]", Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);
                Matcher emojiMatcher = emoji.matcher(source);
                if (emojiMatcher.find()) {
                    return "";
                }
                return null;
            }
        };
        return emojiFilter;
    }


    public static boolean isContainChinese(String str) {
        Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
        Matcher m = p.matcher(str);
        if (m.find()) {
            return true;
        }
        return false;
    }

    */
/**
     * 附言 输入框 可以换行 不允许输入换行符
     *
     * @param mId_transfer_remark
     * @param mId_remark_size
     *//*

    public static void setLimit(final EditText mId_transfer_remark, final TextView mId_remark_size) {

        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (TextUtils.equals("\n", source)) {
                    return "";
                }
                //防止复制粘贴
//                source = String.valueOf(source).replace("\n","");
                NormalInputFilter normalInputFilter = new NormalInputFilter(NormalInputFilter.CHARSEQUENCE_CHINESE_SPECIAL_SPACE);
                Pattern p = normalInputFilter.getmPattern();
                Matcher m = p.matcher(source.toString());
                if (m.find()) {
                    return null;
                } else {
                    return "";
                }
            }
        };
        InputFilter[] filters = {new InputFilter.LengthFilter(50), filter, AndroidUtils.getEmjoFilter()};
        mId_transfer_remark.setFilters(filters);
        mId_transfer_remark.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        mId_transfer_remark.setHorizontallyScrolling(false);
        mId_transfer_remark.setSingleLine(false);
        mId_transfer_remark.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                mId_remark_size.setText(mId_transfer_remark.getText().length() + "/50");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mId_transfer_remark.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    return true;
                } else {
                    return false;
                }
            }
        });

    }

    public static void setLimitWithPailiShi(final EditText mId_transfer_remark, final TextView mId_remark_size) {

        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (TextUtils.equals("\n", source)) {
                    return "";
                }
                Pattern p = Pattern.compile("^[a-zA-Z\\u4e00-\\u9fa5\\d\\/\\(\\)\\.\\,\\'\\+\\-\\?\\: ]*$");
                Matcher m = p.matcher(source.toString());
                if (m.find()) {
                    return null;
                } else {
                    return "";
                }
            }
        };
        InputFilter[] filters = {new InputFilter.LengthFilter(40), filter, AndroidUtils.getEmjoFilter()};
        mId_transfer_remark.setFilters(filters);
        mId_transfer_remark.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        mId_transfer_remark.setSingleLine(false);
        mId_transfer_remark.setHorizontallyScrolling(false);
        mId_transfer_remark.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                mId_remark_size.setText(mId_transfer_remark.getText().length() + "/40");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mId_transfer_remark.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    return true;
                } else {
                    return false;
                }
            }
        });

    }


    public static String getIdvErrorCodeMsg(String errorCode, Activity mActivity) {
        String message = null;
        try {
            message = mActivity.getString(mActivity.getResources().getIdentifier(errorCode, "string", mActivity.getPackageName()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!TextUtils.isEmpty(message)) {
            message = message + "(" + errorCode + ")";
        } else {
            message = errorCode + "(" + errorCode + ")";
        }
        return message;
    }

    public static boolean isEmptyOrNull(String str) {
        return "".equals(str) || null == str || "null".equals(str);
    }

    */
/**
     * 全局根据idv errorcode 返回 errormessage
     *
     * @param errorCode
     * @param mActivity
     * @return
     *//*

    public static String getIdvErrorMessage(String errorCode, Activity mActivity) {
        String message = null;
        //server端返回errorcode
        if (ProjectApp.getErrorCodeList() != null) {
            try {
                message = ProjectApp.getErrorCodeList().getString(errorCode);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONObject jsonObject = null;
            if (!TextUtils.isEmpty(message)) {
                try {
                    jsonObject = new JSONObject(message);
                    String lan = SpUtils.getInstance(mActivity).getAppLanguage();
                    if (AndroidUtils.isZHLanguage(lan)) {
                        message = jsonObject.getString("SC");
                    } else if (AndroidUtils.isHKLanguage(lan)) {
                        message = jsonObject.getString("TC");
                    } else {
                        message = jsonObject.getString("EN");
                    }
                } catch (JSONException e) {
                    if (BuildConfig.isLogDebug) {
                        e.printStackTrace();
                    }
                }
                if (errorCode.startsWith("PJ")) {
                    errorCode = errorCode.replace("PJ", "RJ");
                }
                return message + "(" + errorCode + ")";
            }
        }
        message = getIdvErrorCodeMsg(errorCode, mActivity);
        return message;
    }


    public static void showIdvErrorCode(String errorCode, Activity mActivity, final ConfirmErrorCodeClickListener onClickListener) {
        String message = null;
        //server端返回errorcode
        if (ProjectApp.getErrorCodeList() != null) {
            try {
                message = ProjectApp.getErrorCodeList().getString(errorCode);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONObject jsonObject = null;
            if (!TextUtils.isEmpty(message)) {
                try {
                    jsonObject = new JSONObject(message);
                    String lan = SpUtils.getInstance(mActivity).getAppLanguage();
                    if (AndroidUtils.isZHLanguage(lan)) {
                        message = jsonObject.getString("SC");
                    } else if (AndroidUtils.isHKLanguage(lan)) {
                        message = jsonObject.getString("TC");
                    } else {
                        message = jsonObject.getString("EN");
                    }
                } catch (JSONException e) {
                    if (BuildConfig.isLogDebug) {
                        e.printStackTrace();
                    }

                }
                AndroidUtils.showTipDialog(mActivity, message + "(" + errorCode + ")");
                return;
            }
        }
        message = getIdvErrorCodeMsg(errorCode, mActivity);
        AndroidUtils.showTipDialogWithIdv(mActivity, message, onClickListener);
    }


    public static boolean showErrorCode(String errorCode) {
        if (TextUtils.isEmpty(errorCode)) {
        }
//        String errorCodeList = "PJ252&&" + "PJ253&&" + "PJ301&&" + "PJ601&&" + "PJ999&&" + "DJ101&&" + "DJ103&&" + "DJ201&&" + "DJ202&&" + "DJ203&&" + "DJ301&&" + "DJ303&&" + "DJ304&&" + "DJ310" + "DJ311&&" + "DJ321&&" + "DJ002&&" + "DJ003&&" + "DJ005&&" + "DJ006&&" + "DJ007&&" + "DJ008&&" + "DJ009&&" + "DJ010&&" + "DJ011&&" + "DJ012&&" + "DJ602&&" + "DJ013&&" + "DJ014&&" + "PJ002&&" + "PJ003&&" + "PJ004&&" + "PJ005&&" + "PJ006&&" + "PJ007&&" + "PJ009&&" + "PJ010";
        String errorCodeList = "PJ000&&" + "PJ001&&" + "DJ001";
        boolean shouldShow = true;
        String errorList[] = errorCodeList.split("&&");
        if (!TextUtils.isEmpty(errorCode)) {
            for (int i = 0; i < errorList.length; i++) {
                if (errorCode.equals(errorList[i])) {
                    shouldShow = false;
                    break;
                }
            }
        }
        return shouldShow;
    }

    public static String formatIdNumber(String idNumber) {
        if (TextUtils.isEmpty(idNumber)) {
            return "";
        }
        if (idNumber.length() < 6) {
            return idNumber;
        }
        String finalStr = "";
        try {
            finalStr = idNumber.substring(0, 1) + "****" + idNumber.substring(6, idNumber.length());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return finalStr;
    }

    public static boolean isNumeric(String str) {
        //Pattern pattern = Pattern.compile("[0-9]*");
        Pattern pattern = Pattern.compile("^[+0-9][0-9]{0,}$");
        Matcher isNum = pattern.matcher(str);
        if (!isNum.matches()) {
            return false;
        }
        return true;
    }


    public static Date timeStrToLongValue(String timeValue) {
        try {
            return new SimpleDateFormat("yyyyMMdd HH:mm").parse(timeValue);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    */
/**
     * 判断活动列表是否过期 过期客户端不应该显示
     *
     * @param promotionBeans
     * @param systemTime
     * @return
     *//*

    public static List<PromotionEntity.PromotionBean> filterTimeExpire(List<PromotionEntity.PromotionBean> promotionBeans, String systemTime) {
        List<PromotionEntity.PromotionBean> promotionNewBeans = new ArrayList<>();
        if (TextUtils.isEmpty(systemTime)) {
            //系统时间为空 默认全部显示
            return promotionBeans;
        }
        for (int i = 0; i < promotionBeans.size(); i++) {
            //是否过期判断
            Date systemTimeValue = AndroidUtils.timeStrToLongValue((systemTime));
            if (!TextUtils.isEmpty(promotionBeans.get(i).getExpire()) && !TextUtils.isEmpty(promotionBeans.get(i).getEffective())) {
                //生效时间跟失效时间都不为空 判断系统时间 是否在之间
                Date expireTimeValue = AndroidUtils.timeStrToLongValue(promotionBeans.get(i).getExpire());
                Date effectiveTimeValue = AndroidUtils.timeStrToLongValue(promotionBeans.get(i).getEffective());
                if (systemTimeValue.compareTo(expireTimeValue) == -1 && systemTimeValue.compareTo(effectiveTimeValue) >= 0) {
                    promotionNewBeans.add(promotionBeans.get(i));
                }
            } else {
                //生效时间  失效时间 一个为null
                if (!TextUtils.isEmpty(promotionBeans.get(i).getEffective())) {
                    //getExpire 为null
                    Date effectiveTimeValue = AndroidUtils.timeStrToLongValue(promotionBeans.get(i).getEffective());
                    if (systemTimeValue.compareTo(effectiveTimeValue) >= 0) {
                        promotionNewBeans.add(promotionBeans.get(i));
                    }
                } else if (!TextUtils.isEmpty(promotionBeans.get(i).getExpire())) {
                    //getEffective 为null
                    Date expireTimeValue = AndroidUtils.timeStrToLongValue(promotionBeans.get(i).getExpire());
                    if (systemTimeValue.compareTo(expireTimeValue) == -1) {
                        promotionNewBeans.add(promotionBeans.get(i));
                    }
                } else {
                    promotionNewBeans.add(promotionBeans.get(i));
                }
            }
        }
        return promotionNewBeans;
    }

    */
/**
     * url码不参与积分功能 收银台本地创建
     *
     * @param response
     * @return
     *//*

    public static ActionTrxGpInfoEntity convertActionTrxGpInfoEntity(MpQrUrlInfoResult response) {
        ActionTrxGpInfoEntity actionTrxGpInfoEntity = new ActionTrxGpInfoEntity();
        actionTrxGpInfoEntity.setTranAmt(response.getTranAmt());
        actionTrxGpInfoEntity.setPanFour(response.getPanFour());
        actionTrxGpInfoEntity.setCardType(response.getCardType());
        actionTrxGpInfoEntity.setCardId(response.getCardId());
        actionTrxGpInfoEntity.setMerchantName(response.getName());
        actionTrxGpInfoEntity.setTranCur(response.getTranCur());
        actionTrxGpInfoEntity.setTranID(response.getTranID());
        //不使用积分
        actionTrxGpInfoEntity.setRedeemFlag("0");
        actionTrxGpInfoEntity.setUrlCode(true);
        return actionTrxGpInfoEntity;
    }


    */
/**
     * 通用功能加载idv证书
     *
     * @param mActivity
     *//*

    public static void loadIdvCerts(Activity mActivity) {
        //通用功能sdk初始化 包括版本检测功能 回调成功之后 返回证书列表
        BocLaunchSDK.checkBocLaunchSDK(mActivity, new GoToPageCallback() {
            @Override
            public void beforeToHome(Activity activity) {

            }

            @Override
            public void beforeToProvision(Activity activity) {
                if (BuildConfig.DOWNLOAD_CER_SERVER) {
                    Map<String, X509Certificate> certsMap = BOCHKLaunchFlow.getInstance().getCertsMap();
                    if (certsMap != null) {
                        //动态证书列表
                        List<Certificate> x509Certificates = new ArrayList<>();
                        for (Map.Entry<String, X509Certificate> entry : certsMap.entrySet()) {
                            x509Certificates.add(entry.getValue());
                        }
                        Certificate[] array = new Certificate[x509Certificates.size()];
                        Certificate[] certificates = x509Certificates.toArray(array);
                        ProjectApp.setSdkCertifacates(certificates);
                        LogUtils.i(TAG, "Certificates success " + x509Certificates.size());

                    }
                }
            }
        });
    }

    public static void openUrl(Activity mActivity, String url) {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        mActivity.startActivity(intent);
    }

    public static void setStatusBar(FragmentActivity context, boolean isWhite) {
        View decor = context.getWindow().getDecorView();
        int ui = decor.getSystemUiVisibility();
        if (isWhite) {
            ui &= ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR; //设置状态栏中字体颜色为白色
        } else {
            ui |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR; //设置状态栏中字体的颜色为黑色
        }
        decor.setSystemUiVisibility(ui);
    }


    public static String splitQrCode(String url, String key) {
        if (url.contains("?")) {
            if (url.indexOf("?") + 1 < url.length()) {
                url = url.substring(url.indexOf("?") + 1);
            } else {
                return "";
            }
        }
        HashMap<String, String> map = new HashMap<String, String>();
        String[] fSplit = url.split("&");
        for (int i = 0; i < fSplit.length; i++) {
            if (fSplit[i] == null || fSplit[i].length() == 0) {
                continue;
            }
            String[] sSplit = fSplit[i].split("=");
            String value = fSplit[i].substring(fSplit[i].indexOf('=') + 1);
            map.put(sSplit[0], value);
        }

        return map.getOrDefault(key, "");
    }


}
*/
