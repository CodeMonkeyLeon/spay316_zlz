/*
 * 文 件 名:  DistributionAdapter.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-10-12
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.total;

import java.util.List;

import org.xclcharts.test.OrderTotalModel;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.chart.RoundProgressBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * <一句话功能简述>
 * 
 * @author  he_hui
 * @version  [版本号, 2016-10-12]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class DistributionNewAdapter extends BaseAdapter
{
    private ViewHolder holder;
    
    private List<OrderTotalModel> orderTotalModels;
    
    private Context context;
    
    public DistributionNewAdapter(List<OrderTotalModel> orderTotalModels, Context context)
    {
        this.context = context;
        this.orderTotalModels = orderTotalModels;
    }
    
    @Override
    public int getCount()
    {
        return orderTotalModels.size();
    }
    
    @Override
    public Object getItem(int position)
    {
        return orderTotalModels.get(position);
    }
    
    @Override
    public long getItemId(int position)
    {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            convertView = View.inflate(context, R.layout.total_distribution_list_item, null);
            holder = new ViewHolder();
            holder.roundProgressBar1 = (RoundProgressBar)convertView.findViewById(R.id.roundProgressBar1);
            holder.roundProgressBar1.setRoundWidth(1.0f);
            holder.roundProgressBar1.setCricleColor(context.getResources().getColor(R.color.total_item_text));
            holder.roundProgressBar1.setCricleProgressColor(context.getResources().getColor(R.color.order_title_blue));
            holder.roundProgressBar1.setTextColor(context.getResources().getColor(R.color.order_title_blue));
            holder.tv_user_name = (TextView)convertView.findViewById(R.id.tv_user_name);
            holder.tv_total_money = (TextView)convertView.findViewById(R.id.tv_total_money);
            holder.tv_num = (TextView)convertView.findViewById(R.id.tv_num);
            
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder)convertView.getTag();
        }
        //            
        OrderTotalModel orderTotal = orderTotalModels.get(position);
        if (orderTotal != null)
        {
            holder.tv_user_name.setText(orderTotal.getUserName());
            holder.tv_total_money.setText(DateUtil.formatMoneyUtils(orderTotal.getSuccessFee()));
            holder.tv_num.setText(orderTotal.getSuccessCount() + "");
            if (!StringUtil.isEmptyOrNull(orderTotal.getTotalScale()))
            {
                holder.roundProgressBar1.setProgress(Float.parseFloat(orderTotal.getTotalScale()));
            }
        }
        
        return convertView;
    }
    
    private class ViewHolder
    {
        private TextView tv_user_name, tv_total_money, tv_num;
        
        private RoundProgressBar roundProgressBar1;
        
    }
}
