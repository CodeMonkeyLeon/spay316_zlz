package cn.swiftpass.enterprise.bussiness.model;

/**
 * Created by aijingya on 2018/4/28.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: ${TODO}(侧边栏的Model)
 * @date 2018/4/28.11:44.
 */

public class DrawerContentModel {
    private int imageView;
    private String text;
    private int id;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DrawerContentModel() {
    }

    public DrawerContentModel(int imageView, String text, int id) {
        this.imageView = imageView;
        this.text = text;
        this.id = id;
    }

    public int getImageView() {
        return imageView;
    }

    public void setImageView(int imageView) {
        this.imageView = imageView;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
