package cn.swiftpass.enterprise.ui.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.AgentInfo;

/**
 * 代理商
 * Date: 13-12-23
 * Time: 下午4:29
 */
public class AgentAdapter extends BaseAdapter
{
    @SuppressWarnings("unused")
    private Context mContext;
    
    private List<AgentInfo> agentInfos;
    
    private LayoutInflater inflater;
    
    public AgentAdapter(Context context, List<AgentInfo> agentInfos)
    {
        this.mContext = context;
        
        this.agentInfos = agentInfos;
        
        this.inflater = LayoutInflater.from(context);
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder = null;
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.agent_item, null);
            holder = new ViewHolder();
            holder.agentPhone = (TextView)convertView.findViewById(R.id.tv_2);
            holder.city = (TextView)convertView.findViewById(R.id.tv_1);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder)convertView.getTag();
        }
        AgentInfo info = agentInfos.get(position);
        if (info != null)
        {
            holder.agentPhone.setText(info.getAgentPhone());
            holder.city.setText(info.getCity());
        }
        return convertView;
    }
    
    @Override
    public int getCount()
    {
        return agentInfos.size();
    }
    
    @Override
    public Object getItem(int position)
    {
        return agentInfos.get(position);
    }
    
    @Override
    public long getItemId(int position)
    {
        return position;
    }
    
    public class ViewHolder
    {
        public TextView agentPhone;
        
        public TextView city;
        
    }
}
