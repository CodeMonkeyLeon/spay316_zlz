package cn.swiftpass.enterprise.mvp.api

import cn.swiftpass.enterprise.io.okhttp.OkhttpUtil
import cn.swiftpass.enterprise.io.okhttp.RequestUtil
import cn.swiftpass.enterprise.mvp.annotation.Address
import cn.swiftpass.enterprise.mvp.annotation.IsNeedCook
import cn.swiftpass.enterprise.mvp.annotation.IsNeedSpayId
import cn.swiftpass.enterprise.mvp.annotation.IsNeedVerifyCerts
import cn.swiftpass.enterprise.mvp.annotation.MethodType
import cn.swiftpass.enterprise.mvp.annotation.Param
import cn.swiftpass.enterprise.mvp.constants.ApiConstants
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants

/**
 * @author lizheng.zhao
 * @date 2022/12/14
 *
 * @一眨眼
 * @算不算少年
 * @一辈子
 * @算不算永远
 */
interface SpayApi {

    @Address(ApiConstants.URL_GET_EXCHANGE_RATE)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun getExchangeRate(
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_GET_TIPS_SETTING)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun getTipsSetting(
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_SUBMIT_TIPS_SETTING)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun submitTipsSetting(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?,
        @Param(ParamsConstants.JSON_STR_PARAM) json_str: String?
    ): RequestUtil


    @Address(ApiConstants.URL_PAYMENT_LINK_ORDER)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun sendMessage(
        @Param(ParamsConstants.ACTION_TYPE) action_type: String?,
        @Param(ParamsConstants.ORDER_NO) order_no: String?,
        @Param(ParamsConstants.CUST_MOBILE) cust_mobile: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_PAYMENT_LINK_ORDER)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun sendEmail(
        @Param(ParamsConstants.ACTION_TYPE) action_type: String?,
        @Param(ParamsConstants.ORDER_NO) order_no: String?,
        @Param(ParamsConstants.CUST_EMAIL) cust_email: String?,
        @Param(ParamsConstants.EMAIL_SUBJECT) email_subject: String?,
        @Param(ParamsConstants.EMAIL_CONTENT) email_content: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_GET_ORDER_TOTAL)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun getOrderTotal(
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.START_TIME) start_time: String?,
        @Param(ParamsConstants.END_TIME) end_time: String?,
        @Param(ParamsConstants.MOBILE) mobile: String,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_DAILY_SETTLEMENT_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryDailySettlementList(
        @Param(ParamsConstants.START_DATE) start_date: String?,
        @Param(ParamsConstants.END_DATE) end_date: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_GET_BASE)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun getBase(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_LOGOUT)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun logout(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_REGIS_REFUND)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun regisRefund(
        @Param(ParamsConstants.OUT_TRADE_NO) out_trade_no: String?,
        @Param(ParamsConstants.CLIENT) client: String?,
        @Param(ParamsConstants.REFUND_MONEY) refund_money: String?,
        @Param(ParamsConstants.TOTAL_FEE) total_fee: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.USER_NAME_N) user_name: String?,
        @Param(ParamsConstants.BODY) body: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_SPAY_ORDER_NEW_AUTH_OPERATE_QUERY)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun authOperateQuery(
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.AUTH_NO) auth_no: String?,
        @Param(ParamsConstants.OPERATION_TYPE) operation_type: String?,
        @Param(ParamsConstants.PAGE) page: String?,
        @Param(ParamsConstants.PAGE_SIZE) page_size: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_ORDER_BY_ORDER_NO_AUTH)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun synchronizeOrder(
        @Param(ParamsConstants.SERVICE) service: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.AUTH_NO) auth_no: String?,
        @Param(ParamsConstants.OUT_REQUEST_NO) out_request_no: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_UNIFIED_AUTH_PAY)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun unifiedAuthPay(
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.AUTH_NO) auth_no: String?,
        @Param(ParamsConstants.BODY) body: String?,
        @Param(ParamsConstants.MONEY) money: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_CHECK_CAN_REFUND_OR_NOT)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun checkCanRefundOrNot(
        @Param(ParamsConstants.OUT_TRADE_NO) out_trade_no: String?,
        @Param(ParamsConstants.CLIENT) client: String?,
        @Param(ParamsConstants.REFUND_MONEY) refund_money: String?,
        @Param(ParamsConstants.TOTAL_FEE) total_fee: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.USER_NAME_N) user_name: String?,
        @Param(ParamsConstants.BODY) body: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_REGIS_REFUNDS)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun regisRefunds(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?,
        @Param(ParamsConstants.JSON_STR_PARAM) json_str: String?
    ): RequestUtil


    @Address(ApiConstants.URL_CARD_PAYMENT_REFUND)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun cardPaymentRefund(
        @Param(ParamsConstants.BODY) body: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.USER_NAME_N) user_name: String?,
        @Param(ParamsConstants.REFUND_MONEY) refund_money: String?,
        @Param(ParamsConstants.TOTAL_FEE) total_fee: String?,
        @Param(ParamsConstants.CLIENT) client: String?,
        @Param(ParamsConstants.OUT_TRADE_NO) out_trade_no: String?,
        @Param(ParamsConstants.ORDER_NO_MCH) order_no_mch: String?,
        @Param(ParamsConstants.TOKEN) token: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_AUTH_UNFREEZE)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun authUnfreeze(
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.AUTH_NO) auth_no: String?,
        @Param(ParamsConstants.TOKEN) token: String?,
        @Param(ParamsConstants.MONEY) money: String?,
        @Param(ParamsConstants.REMARK) remark: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_AUTH_PAY_QUERY)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun authPayQuery(
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.OUT_REQUEST_NO) out_request_no: String?,
        @Param(ParamsConstants.ORDER_NO) order_no: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_GET_SPEAKER_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun getSpeakerList(
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_UNBIND_SPEAKER)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun unbindSpeaker(
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.DEVICE_NAME) device_name: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_BIND_SPEAKER)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun bindSpeaker(
        @Param(ParamsConstants.DEVICE_NAME) device_name: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_MASTER_CARD_ORDER_BY_ORDER_NO)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryMasterCardOrderByOrderNo(
        @Param(ParamsConstants.OUT_TRADE_NO) out_trade_no: String?,
        @Param(ParamsConstants.SERVICE) service: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_SYNC_MASTER_CARD_ORDER_STATUS_BY_ORDER_NO)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun syncMasterCardOrderStatusByOrderNo(
        @Param(ParamsConstants.OUT_TRADE_NO) out_trade_no: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil

    @Address(ApiConstants.URL_UPDATE_OR_ADD_BODY)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun updateOrAddBody(
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.COMMODITY_NAME) commodity_name: String?,
        @Param(ParamsConstants.COMMODITY_NUM) commodity_num: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_BODY)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryBody(
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil

    @Address(ApiConstants.URL_CHECK_DATA)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun checkData(
        @Param(ParamsConstants.TOKEN) token: String?,
        @Param(ParamsConstants.EMAIL_CODE) email_code: String?,
        @Param(ParamsConstants.S_KEY) s_key: String?,
        @Param(ParamsConstants.PASSWORD) password: String?,
        @Param(ParamsConstants.REP_PASSWORD) rep_password: String?,
        @Param(ParamsConstants.NEW_PWD_FLAG) new_pwd_flag: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_CHECK_PHONE_CODE)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun checkPhoneCode(
        @Param(ParamsConstants.TOKEN) token: String?,
        @Param(ParamsConstants.EMAIL_CODE) email_code: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_CHECK_FORGET_PWD_INPUT)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun checkForgetPwdInputAndGetCode(
        @Param(ParamsConstants.EMAIL) email: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_MERCHANT_DATA_BY_TEL)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun sendFeedBack(
        @Param(ParamsConstants.U_ID) u_id: String?,
        @Param(ParamsConstants.CONTENT) content: String?,
        @Param(ParamsConstants.CLIENT_TYPE) client_type: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_PRODUCT_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun editProductDetail(
        @Param(ParamsConstants.ACTION_TYPE) action_type: String?,
        @Param(ParamsConstants.GOODS_ID) goods_id: String?,
        @Param(ParamsConstants.GOODS_NAME) goods_name: String?,
        @Param(ParamsConstants.GOODS_CODE) goods_code: String?,
        @Param(ParamsConstants.GOODS_DESC) goods_desc: String?,
        @Param(ParamsConstants.GOODS_PRICE) goods_price: String?,
        @Param(ParamsConstants.GOODS_PIC) goods_pic: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_PRODUCT_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun addNewProduct(
        @Param(ParamsConstants.ACTION_TYPE) action_type: String?,
        @Param(ParamsConstants.GOODS_NAME) goods_name: String?,
        @Param(ParamsConstants.GOODS_CODE) goods_code: String?,
        @Param(ParamsConstants.GOODS_DESC) goods_desc: String,
        @Param(ParamsConstants.GOODS_PRICE) goods_price: String?,
        @Param(ParamsConstants.GOODS_PIC) goods_pic: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_CUSTOMER_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun addNewCustomer(
        @Param(ParamsConstants.ACTION_TYPE) action_type: String?,
        @Param(ParamsConstants.CUST_NAME) cust_name: String?,
        @Param(ParamsConstants.CUST_MOBILE) cust_mobile: String?,
        @Param(ParamsConstants.CUST_EMAIL) cust_email: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_CUSTOMER_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun editCustomerDetail(
        @Param(ParamsConstants.ACTION_TYPE) action_type: String?,
        @Param(ParamsConstants.CUST_ID) cust_id: String?,
        @Param(ParamsConstants.CUST_NAME) cust_name: String?,
        @Param(ParamsConstants.CUST_MOBILE) cust_mobile: String?,
        @Param(ParamsConstants.CUST_EMAIL) cust_email: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_CASHIER_DELETE)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun cashierDelete(
        @Param(ParamsConstants.ID) id: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_CASHIER_ADD_SAVE)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun cashierAddSave(
        @Param(ParamsConstants.REAL_NAME) real_name: String?,
        @Param(ParamsConstants.USER_NAME) user_name: String?,
        @Param(ParamsConstants.REFUND_LIMIT) refund_limit: String?,
        @Param(ParamsConstants.IS_REFUND_AUTH) is_refund_auth: String?,
        @Param(ParamsConstants.IS_ORDER_AUTH) is_order_auth: String?,
        @Param(ParamsConstants.IS_UNFREEZE_AUTH) is_unfreeze_auth: String?,
        @Param(ParamsConstants.ENABLED) enabled: String?,
        @Param(ParamsConstants.IS_TOTAL_AUTH) is_total_auth: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.ID) id: String?,
        @Param(ParamsConstants.IS_ENCRYPT_PWD) is_encrypt_pwd: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_CASHIER_ADD_UPDATE)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun cashierAddUpdate(
        @Param(ParamsConstants.REAL_NAME) real_name: String?,
        @Param(ParamsConstants.USER_NAME) user_name: String?,
        @Param(ParamsConstants.REFUND_LIMIT) refund_limit: String?,
        @Param(ParamsConstants.IS_REFUND_AUTH) is_refund_auth: String?,
        @Param(ParamsConstants.IS_ORDER_AUTH) is_order_auth: String?,
        @Param(ParamsConstants.IS_UNFREEZE_AUTH) is_unfreeze_auth: String?,
        @Param(ParamsConstants.ENABLED) enabled: String?,
        @Param(ParamsConstants.IS_TOTAL_AUTH) is_total_auth: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.ID) id: String?,
        @Param(ParamsConstants.IS_ENCRYPT_PWD) is_encrypt_pwd: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_CASHIER_RESET_PSW)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun cashierResetPsw(
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.IS_ENCRYPT_PWD) is_encrypt_pwd: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_CARD_DETAIL)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryCardDetail(
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.U_ID) u_id: String?,
        @Param(ParamsConstants.CARD_ID) card_id: String?,
        @Param(ParamsConstants.CARD_CODE) card_code: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_SEND_EMAIL_FOR_ACCOUNT_CANCEL)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun sendEmailForAccountCancel(
        @Param(ParamsConstants.EMAIL) email: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_CHECK_VERIFY_CODE_AND_PASSWORD)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun checkVerifyCodeAndPassword(
        @Param(ParamsConstants.EMAIL_VERIFY_CODE) email_verify_code: String?,
        @Param(ParamsConstants.PASSWORD) password: String?,
        @Param(ParamsConstants.S_KEY) s_key: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_TRANSACTION_REPORT_DETAILS)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryTransactionReportDetails(
        @Param(ParamsConstants.REPORT_TYPE) report_type: String?,
        @Param(ParamsConstants.START_DATE) start_date: String?,
        @Param(ParamsConstants.END_DATE) end_date: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_BIND_CODE_DETAILS)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun bindCodeDetails(
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.QR_CODE_ID) qr_code_id: String?,
        @Param(ParamsConstants.CASHIER_DESK) cashier_desk: String?,
        @Param(ParamsConstants.CLIENT) client: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_BIND_CODE)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun bindCode(
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.QR_CODE_ID) qr_code_id: String?,
        @Param(ParamsConstants.CASHIER_DESK) cashier_desk: String?,
        @Param(ParamsConstants.CLIENT) client: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_REFUND_LOGIN)
    @MethodType(OkhttpUtil.METHOD_POST)
    @IsNeedCook(ApiConstants.FALSE)
    fun refundLogin(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?,
        @Param(ParamsConstants.JSON_STR_PARAM) json_str: String?
    ): RequestUtil


    @Address(ApiConstants.URL_API_SHOW_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun apiShowList(
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_USER_REFUND_ORDER)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryUserRefundOrder(
        @Param(ParamsConstants.OUT_REFUND_NO) out_refund_no: String?,
        @Param(ParamsConstants.TRADE_STATE) trade_state: String?,
        @Param(ParamsConstants.ADD_TIME) add_time: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.PAGE) page: String?,
        @Param(ParamsConstants.PAGE_SIZE) page_size: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_ORDER_DATA)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryRefundOrder(
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.PAGE) page: String?,
        @Param(ParamsConstants.TRADE_STATE) trade_state: String?,
        @Param(ParamsConstants.IS_REFUND_STREAM) is_refund_stream: String?,
        @Param(ParamsConstants.ORDER_NO_MCH) order_no_mch: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_ORDER_DATA)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryOrderData(
        @Param(ParamsConstants.ORDER_NO_MCH) order_no_mch: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.ADD_TIME) add_time: String?,
        @Param(ParamsConstants.PAGE) page: String?,
        @Param(ParamsConstants.TRADE_STATE) trade_state: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_CASHIER)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryCashier(
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.PAGE) page: String?,
        @Param(ParamsConstants.SEARCH_CONTENT) search_content: String?,
        @Param(ParamsConstants.PAGE_SIZE) page_size: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_PAY_REVERSE)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun payReverse(
        @Param(ParamsConstants.OUT_TRADE_NO) out_trade_no: String?,
        @Param(ParamsConstants.MONEY) money: String?,
        @Param(ParamsConstants.SERVICE) service: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_ORDER_BY_INVOICE_ID)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryOrderByInvoiceId(
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.INVOICE_ID) invoice_id: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_PAYMENT_LINK_ORDER)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun getUpdateOrderDetail(
        @Param(ParamsConstants.ACTION_TYPE) action_type: String?,
        @Param(ParamsConstants.ORDER_NO) order_no: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_GET_ORDER_CONFIG)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun getOrderConfig(
        @Param(ParamsConstants.ACTION_TYPE) action_type: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_CHANGE_PWD)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun changePwd(
        @Param(ParamsConstants.S_KEY) s_key: String?,
        @Param(ParamsConstants.TELEPHONE) telephone: String?,
        @Param(ParamsConstants.OLD_PASSWORD) old_password: String?,
        @Param(ParamsConstants.PASSWORD) password: String?,
        @Param(ParamsConstants.REP_PASSWORD) rep_password: String?,
        @Param(ParamsConstants.NEW_PWD_FLAG) new_pwd_flag: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_TRANSACTION_REPORT)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryTransactionReport(
        @Param(ParamsConstants.REPORT_TYPE) report_type: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_ORDER_COUNT)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun orderCount(
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.FREE_QUERY_TIME) free_query_time: String?,
        @Param(ParamsConstants.START_TIME) start_time: String?,
        @Param(ParamsConstants.END_TIME) end_time: String?,
        @Param(ParamsConstants.COUNT_METHOD) count_method: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.COUNT_DAY_KIND) count_day_kind: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_MERCHANT_DATA_BY_TEL)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryMerchantDataByTel(
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.PHONE) phone: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_GET_CODE_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun getCodeList(
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.BIND_USER_ID) bind_user_id: String?,
        @Param(ParamsConstants.PAGE) page: String?,
        @Param(ParamsConstants.PAGE_SIZE) page_size: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_PRODUCT_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun getProductList(
        @Param(ParamsConstants.ACTION_TYPE) action_type: String?,
        @Param(ParamsConstants.GOODS_ID) goods_id: String?,
        @Param(ParamsConstants.GOODS_NAME) goods_name: String?,
        @Param(ParamsConstants.GOODS_CODE) goods_code: String?,
        @Param(ParamsConstants.PAGE_NUMBER) page_number: String?,
        @Param(ParamsConstants.PAGE_SIZE) page_size: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_PAYMENT_LINK_ORDER)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun getOrderList(
        @Param(ParamsConstants.ACTION_TYPE) action_type: String?,
        @Param(ParamsConstants.ORDER_NO) order_no: String?,
        @Param(ParamsConstants.REAL_PLAT_ORDER_NO) real_plat_order_no: String?,
        @Param(ParamsConstants.CUST_NAME) cust_name: String?,
        @Param(ParamsConstants.ORDER_REMARK) order_remark: String?,
        @Param(ParamsConstants.PAGE_NUMBER) page_number: String?,
        @Param(ParamsConstants.PAGE_SIZE) page_size: String?,
        @Param(ParamsConstants.ORDER_STATUS) order_status: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_CUSTOMER_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun getCustomerList(
        @Param(ParamsConstants.ACTION_TYPE) action_type: String?,
        @Param(ParamsConstants.CUST_NAME) cust_name: String?,
        @Param(ParamsConstants.CUST_MOBILE) cust_mobile: String?,
        @Param(ParamsConstants.CUST_EMAIL) cust_email: String?,
        @Param(ParamsConstants.PAGE_NUMBER) page_number: String?,
        @Param(ParamsConstants.PAGE_SIZE) page_size: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_PRODUCT_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun deleteOrQueryProduct(
        @Param(ParamsConstants.ACTION_TYPE) action_type: String?,
        @Param(ParamsConstants.GOODS_ID) goods_id: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_CUSTOMER_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun deleteOrQueryCustomer(
        @Param(ParamsConstants.ACTION_TYPE) action_type: String?,
        @Param(ParamsConstants.CUST_ID) cust_id: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_SPAY_ORDER_NEW_REFUND_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun querySpayOrderRefundList(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?,
        @Param(ParamsConstants.JSON_STR_PARAM) json_str: String?
    ): RequestUtil

    @Address(ApiConstants.URL_QUERY_SPAY_ORDER_NEW_AUTH_ORDER_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun querySpayOrderAuthOrderList(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?,
        @Param(ParamsConstants.JSON_STR_PARAM) json_str: String?
    ): RequestUtil

    @Address(ApiConstants.URL_QUERY_SPAY_ORDER_NEW_ORDER_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun querySpayOrderOrderList(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?,
        @Param(ParamsConstants.JSON_STR_PARAM) json_str: String?
    ): RequestUtil

    @Address(ApiConstants.URL_QUERY_SPAY_ORDER_NEW_REFUND_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun querySpayOrderNewRefundList(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?,
        @Param(ParamsConstants.JSON_STR_PARAM) json_str: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_SPAY_ORDER_NEW_AUTH_ORDER_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun querySpayOrderNewAuthOrderList(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?,
        @Param(ParamsConstants.JSON_STR_PARAM) json_str: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_SPAY_ORDER_NEW_ORDER_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun querySpayOrderNewOrderList(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?,
        @Param(ParamsConstants.JSON_STR_PARAM) json_str: String?
    ): RequestUtil

    @Address(ApiConstants.URL_QUERY_SPAY_ORDER_NEW_AUTH_OPERATE_QUERY)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun querySpayOrderNewAuthOperateQuery(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?,
        @Param(ParamsConstants.JSON_STR_PARAM) json_str: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_SPAY_ORDER_NEW_REFUND_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun querySpayOrderWxCardRefundList(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?,
        @Param(ParamsConstants.JSON_STR_PARAM) json_str: String?
    ): RequestUtil

    @Address(ApiConstants.URL_QUERY_SPAY_ORDER_NEW_ORDER_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun querySpayOrderWxCardOrderList(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?,
        @Param(ParamsConstants.JSON_STR_PARAM) json_str: String?
    ): RequestUtil

    @Address(ApiConstants.URL_QUERY_SPAY_ORDER_WX_CARD)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun querySpayOrderWxCard(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?,
        @Param(ParamsConstants.JSON_STR_PARAM) json_str: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_REFUND_DETAIL)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryRefundDetail(
        @Param(ParamsConstants.OUT_REFUND_NO) out_refund_no: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_PRE_AUTH_DAILY_STATICS)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryPreAuthDailyStatics(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_DAILY_STATICS)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryDailyStatics(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_LOAD_CARD_PAYMENT_DATE)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun loadCardPaymentDate(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?,
        @Param(ParamsConstants.JSON_STR_PARAM) json_str: String?
    ): RequestUtil


    @Address(ApiConstants.URL_UPLOAD_IMAGE)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun uploadImg(
        @Param(ParamsConstants.IMG_TYPE) img_type: String?,
        @Param(ParamsConstants.IMG_BASE_64) img_base_64: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_PAYMENT_LINK_ORDER)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun paymentLinkOrder(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?,
        @Param(ParamsConstants.JSON_STR_PARAM) json_str: String?
    ): RequestUtil


    @Address(ApiConstants.URL_UNIFIED_PAY_REVERSE_AUTH)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun unifiedPayReverseAuth(
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.OUT_AUTH_NO) out_auth_no: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_UNIFIED_PAY_REVERSE_PAY)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun unifiedPayReversePay(
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.OUT_TRADE_NO) out_trade_no: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_UNIFIED_NATIVE_PAY_AUTH)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun unifiedNativePayAuth(
        @Param(ParamsConstants.MONEY) money: String?,
        @Param(ParamsConstants.ATTACH) attach: String?,
        @Param(ParamsConstants.DEVICE_INFO) device_info: String?,
        @Param(ParamsConstants.OUT_TRADE_NO) out_trade_no: String?,
        @Param(ParamsConstants.DA_MONEY) da_money: String?,
        @Param(ParamsConstants.USER_NAME_N) user_name: String?,
        @Param(ParamsConstants.U_ID) u_id: String?,
        @Param(ParamsConstants.BODY) body: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.CLIENT) client: String?,
        @Param(ParamsConstants.API_CODE) api_code: String?,
        @Param(ParamsConstants.TRA_TYPE) tra_type: String?,
        @Param(ParamsConstants.SERVICE) service: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_UNIFIED_NATIVE_PAY_PAY)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun unifiedNativePayPay(
        @Param(ParamsConstants.MONEY) money: String?,
        @Param(ParamsConstants.ATTACH) attach: String?,
        @Param(ParamsConstants.DEVICE_INFO) device_info: String?,
        @Param(ParamsConstants.OUT_TRADE_NO) out_trade_no: String?,
        @Param(ParamsConstants.DA_MONEY) da_money: String?,
        @Param(ParamsConstants.USER_NAME_N) user_name: String?,
        @Param(ParamsConstants.U_ID) u_id: String?,
        @Param(ParamsConstants.BODY) body: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.CLIENT) client: String?,
        @Param(ParamsConstants.API_CODE) api_code: String?,
        @Param(ParamsConstants.TRA_TYPE) tra_type: String?,
        @Param(ParamsConstants.SERVICE) service: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_ORDER_DETAIL_AUTH)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun scanQueryOrderDetailAuth(
        @Param(ParamsConstants.ORDER_NO_MCH) order_no_mch: String?,
        @Param(ParamsConstants.AUTH_NO) auth_no: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.SPAY_RS) api_provider: String?
    ): RequestUtil

    @Address(ApiConstants.URL_ORDER_DETAIL_QUERY)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun scanQueryOrderDetailQuery(
        @Param(ParamsConstants.ORDER_NO_MCH) order_no_mch: String?,
        @Param(ParamsConstants.AUTH_NO) auth_no: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.SPAY_RS) api_provider: String?
    ): RequestUtil


    @Address(ApiConstants.URL_ORDER_DETAIL_AUTH)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryOrderDetailAuth(
        @Param(ParamsConstants.AUTH_NO) auth_no: String?,
        @Param(ParamsConstants.OUT_TRADE_NO) out_trade_no: String?,
        @Param(ParamsConstants.ORDER_NO_MCH) order_no_mch: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil

    @Address(ApiConstants.URL_ORDER_DETAIL_QUERY)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryOrderDetailQuery(
        @Param(ParamsConstants.AUTH_NO) auth_no: String?,
        @Param(ParamsConstants.OUT_TRADE_NO) out_trade_no: String?,
        @Param(ParamsConstants.ORDER_NO_MCH) order_no_mch: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil

    @Address(ApiConstants.URL_QUERY_ORDER_BY_ORDER_NO_AUTH)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryOrderByOrderNoAuth(
        @Param(ParamsConstants.SERVICE) service: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.OUT_AUTH_NO) out_auth_no: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_ORDER_BY_ORDER_NO_PAY)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryOrderByOrderNoPay(
        @Param(ParamsConstants.SERVICE) service: String?,
        @Param(ParamsConstants.USER_ID) user_id: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.OUT_TRADE_NO) out_trade_no: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_CARD_PAYMENT_ORDER_DETAILS)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryCardPaymentOrderDetails(
        @Param(ParamsConstants.OUT_TRADE_NO) out_trade_no: String?,
        @Param(ParamsConstants.ORDER_NO_MCH) order_no_mch: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_MASTER_CARD_NATIVE_PAY)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun masterCardNativePay(
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.SERVICE) service: String?,
        @Param(ParamsConstants.API_PROVIDER) api_provider: String?,
        @Param(ParamsConstants.BODY) body: String?,
        @Param(ParamsConstants.CLIENT) client: String?,
        @Param(ParamsConstants.MONEY) money: String?,
        @Param(ParamsConstants.DEVICE_INFO) device_info: String?,
        @Param(ParamsConstants.ATTACH) attach: String?,
        @Param(ParamsConstants.USER_NAME_N) user_name: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


//    @Address(ApiConstants.URL_DO_COVER_ORDER)
//    @MethodType(OkhttpUtil.METHOD_POST)
//    fun doCoverOrder(
//        @Param(ParamsConstants.SERVICE) service: String?,
//        @Param(ParamsConstants.OUT_AUTH_TRADE_NO) out_auth_trade_no: String?,
//        @Param(ParamsConstants.DA_MONEY) da_money: String?,
//        @Param(ParamsConstants.CLIENT) client: String?,
//        @Param(ParamsConstants.AUTH_CODE) auth_code: String?,
//        @Param(ParamsConstants.MONEY) money: String?,
//        @Param(ParamsConstants.MCH_ID) mch_id: String?,
//        @Param(ParamsConstants.ATTACH) attach: String?,
//        @Param(ParamsConstants.DEVICE_INFO) device_info: String?,
//        @Param(ParamsConstants.USER_NAME_N) user_name: String?,
//        @Param(ParamsConstants.U_ID) u_id: String?,
//        @Param(ParamsConstants.BODY) body: String?,
//        @Param(ParamsConstants.SIGN) sign: String?,
//        @Param(ParamsConstants.SPAY_RS) spayRs: String?
//    ): RequestUtil

    @Address(ApiConstants.URL_DO_COVER_ORDER_AUTH)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun doCoverOrderAuth(
        @Param(ParamsConstants.SERVICE) service: String?,
        @Param(ParamsConstants.OUT_AUTH_NO) out_auth_no: String?,
        @Param(ParamsConstants.DA_MONEY) da_money: String?,
        @Param(ParamsConstants.CLIENT) client: String?,
        @Param(ParamsConstants.AUTH_CODE) auth_code: String?,
        @Param(ParamsConstants.MONEY) money: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.ATTACH) attach: String?,
        @Param(ParamsConstants.DEVICE_INFO) device_info: String?,
        @Param(ParamsConstants.USER_NAME_N) user_name: String?,
        @Param(ParamsConstants.U_ID) u_id: String?,
        @Param(ParamsConstants.BODY) body: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil

    @Address(ApiConstants.URL_DO_COVER_ORDER_PAY)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun doCoverOrderPay(
        @Param(ParamsConstants.SERVICE) service: String?,
        @Param(ParamsConstants.OUT_TRADE_NO) out_trade_no: String?,
        @Param(ParamsConstants.DA_MONEY) da_money: String?,
        @Param(ParamsConstants.CLIENT) client: String?,
        @Param(ParamsConstants.AUTH_CODE) auth_code: String?,
        @Param(ParamsConstants.MONEY) money: String?,
        @Param(ParamsConstants.MCH_ID) mch_id: String?,
        @Param(ParamsConstants.ATTACH) attach: String?,
        @Param(ParamsConstants.DEVICE_INFO) device_info: String?,
        @Param(ParamsConstants.USER_NAME_N) user_name: String?,
        @Param(ParamsConstants.U_ID) u_id: String?,
        @Param(ParamsConstants.BODY) body: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_QUERY_WALLET_LIST)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun queryWalletList(
        @Param(ParamsConstants.SERVICE) service: String?,
        @Param(ParamsConstants.SIGN) sign: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_GET_CODE)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun getCode(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_GET_CERTIFICATE_STRING)
    @MethodType(OkhttpUtil.METHOD_POST)
    @IsNeedSpayId(ApiConstants.FALSE)
    @IsNeedVerifyCerts(ApiConstants.FALSE)
    fun getCertificateString(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_ECDH_KEY_EXCHANGE)
    @MethodType(OkhttpUtil.METHOD_POST)
    @IsNeedSpayId(ApiConstants.FALSE)
    fun ecdhKeyExchange(
        @Param(ParamsConstants.PUBLIC_KEY) publick_key: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_DEVICE_LOGIN)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun deviceLogin(
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_GET_VERSION_CODE)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun getVersionCode(
        @Param(ParamsConstants.VER_CODE) ver_code: String?,
        @Param(ParamsConstants.BANK_CODE) bank_code: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil


    @Address(ApiConstants.URL_LOGIN_ASYNC)
    @MethodType(OkhttpUtil.METHOD_POST)
    fun loginAsync(
        @Param(ParamsConstants.USER_NAME) user_name: String?,
        @Param(ParamsConstants.PASSWORD) password: String?,
        @Param(ParamsConstants.CLIENT) client: String?,
        @Param(ParamsConstants.S_KEY) s_key: String?,
        @Param(ParamsConstants.BANK_CODE) bank_code: String?,
        @Param(ParamsConstants.ANDROID_TRANS_PUSH) android_trans_push: String?,
        @Param(ParamsConstants.CODE) code: String?,
        @Param(ParamsConstants.PUSH_CID) push_cid: String?,
        @Param(ParamsConstants.SPAY_RS) spayRs: String?
    ): RequestUtil
}