package cn.swiftpass.enterprise.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;

import cn.swiftpass.enterprise.intl.R;

/**
 * @author Liz_Miller
 * @Title: SlidingTabLayout
 * @Package com.mobile.margaret.margaretapplication.widget
 * @Description: ${todo}(自定义控件实现切换tab的时候的指示条的效果)
 * @date 2016/5/31 11:56
 */
public class SlidingTabLayout extends LinearLayout {

    /*默认title的字体的颜色*/
    private final int DEFAULT_TITLE_TEXT_COLOR = 0x999999;

    /*选中title的字体的颜色*/
    private final int SELECTED_TITLE_TEXT_COLOR = 0x000000;

    /*默认的页卡颜色*/
    private final int DEFAULT_INDICATOR_COLOR = 0x11A0F8;
    /*默认分割线的颜色*/
    private final int DEFAULT_DIVIDER_COLOR = 0xff000000;
    /*底部线条的颜色默认值*/
    private final int DEFAULT_BOTTOM_LINE_COLOR = 0xdcdcdc;
    /*分割线距离上下边缘的距离默认为8*/
    private final int DEFAULT_DIVIDER_MARGIN = 5;
    /*indicator 的高度*/
    private final int DEFAULT_INDICATOR_HEIGHT = 3;
    /*底部线条的高度默认值*/
    private final int DEFAULT_BOTTOM_LINE_HEIGHT = 1;
    /*divider默认的宽度*/
    private final int DEFAULT_DIVIDER_WIDTH = 1;

    /*默认title的字体的颜色*/
    private int itemTextDefaultColor = DEFAULT_TITLE_TEXT_COLOR;

    /*选中title的字体的颜色*/
    private int itemTextSelectedColor = SELECTED_TITLE_TEXT_COLOR;

    /*页卡的颜色*/
    private int mIndicatorColor = DEFAULT_INDICATOR_COLOR;
    /*分割线的颜色*/
    private int mDividerColor = DEFAULT_DIVIDER_COLOR;
    /*底部线条的颜色*/
    private int mBottomLineColor = DEFAULT_BOTTOM_LINE_COLOR;
    /*分割线距离上线边距的距离*/
    private int mDividerMargin = DEFAULT_DIVIDER_MARGIN;
    /*滑动指示器的高度*/
    private int mIndicatorHeight = DEFAULT_INDICATOR_HEIGHT ;
    /*底部线条的高度*/
    private int mBottomLineHeight = DEFAULT_BOTTOM_LINE_HEIGHT;
    /*分割线的宽度*/
    private int mDividerWidth = DEFAULT_DIVIDER_WIDTH;

    /*默认title字体的大小*/
    private final int DEFAULT_TEXT_SIZE = 14;
    /*默认padding*/
    private final int DEFAULT_TEXT_PADDING = 14;


    /*页卡画笔*/
    private Paint mIndicatorPaint;
    /*分割线画笔*/
    private Paint mDividerPaint;
    /*底部线条的画笔*/
    private Paint mBottomPaint ;

    /*当前选中的页面位置*/
    private int mSelectedPosition;
    /*页面的偏移量*/
    private float mSelectionOffset;

    public ArrayList<String> titleList = new ArrayList<String>();

    /*页面*/
    private ViewPager mViewPager;
    /*页面切换监听事件*/
    private ViewPager.OnPageChangeListener mListener;


    public SlidingTabLayout(Context context) {
        this(context,null);
    }

    public SlidingTabLayout(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public SlidingTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        // 因为ViewGroup默认是不走onDraw方法的，因为ViewGroup是不需要绘制的，
        // 需要绘制的是ViewGroup的子item，这里我们设置一下背景颜色，
        // ViewGroup就会走onDraw方法去绘制它自己的背景，那么我们需要onDraw吗？
        // 当然需要，我们要在onDraw中绘制指示符。
//        setBackgroundColor(Color.TRANSPARENT);  // 必须设置背景，否则onDraw不执行

         /*获取TypedArray*/
        TypedArray typedArray = getResources().obtainAttributes(attrs, R.styleable.SlidingTabLayout);
          /*获取自定义属性的个数*/
        int TypeCount = typedArray.getIndexCount();
        for (int i = 0; i < TypeCount; i++) {
            int attr = typedArray.getIndex(i);
            switch (attr) {
                case R.styleable.SlidingTabLayout_itemTextDefaultColor:
                    /*获取标题title的的默认颜色值*/
                    itemTextDefaultColor = typedArray.getColor(attr, DEFAULT_TITLE_TEXT_COLOR);
                    break;

                case R.styleable.SlidingTabLayout_itemTextSelectedColor:
                    itemTextSelectedColor = typedArray.getColor(attr, SELECTED_TITLE_TEXT_COLOR);
                    break;
                case R.styleable.SlidingTabLayout_indicatorColor:
                    /*获取页卡颜色值*/
                    mIndicatorColor = typedArray.getColor(attr, DEFAULT_INDICATOR_COLOR);
                    break;
                case R.styleable.SlidingTabLayout_dividerColor:
                    /*获取分割线颜色的值*/
                    mDividerColor = typedArray.getColor(attr, DEFAULT_DIVIDER_COLOR);
                    break;
                case R.styleable.SlidingTabLayout_bottomLineColor:
                    /*获取底部线条颜色的值*/
                    mBottomLineColor = typedArray.getColor(attr, DEFAULT_BOTTOM_LINE_COLOR);
                    break;
                case R.styleable.SlidingTabLayout_dividerMargin:
                    /*获取分割线的距离上线边距的距离*/
                    mDividerMargin = (int) typedArray.getDimension(attr, DEFAULT_DIVIDER_MARGIN * getResources().getDisplayMetrics().density);
                    break;
                case R.styleable.SlidingTabLayout_indicatorHeight:
                    /*获取页卡的高度*/
                    mIndicatorHeight = (int) typedArray.getDimension(attr, DEFAULT_INDICATOR_HEIGHT * getResources().getDisplayMetrics().density);
                    break;
                case R.styleable.SlidingTabLayout_bottomLineHeight:
                    /*获取底部线条的高度*/
                    mBottomLineHeight = (int) typedArray.getDimension(attr, DEFAULT_BOTTOM_LINE_HEIGHT * getResources().getDisplayMetrics().density);
                    break;
                case R.styleable.SlidingTabLayout_dividerWidth:
                    /*获取分割线的宽度*/
                    mDividerWidth = (int) typedArray.getDimension(attr, DEFAULT_DIVIDER_WIDTH * getResources().getDisplayMetrics().density);
                    break;
            }
        }
        /*释放TypedArray*/
        typedArray.recycle();

        initView();
    }

    private void initView() {
        setWillNotDraw(false);  // 设置view是否更改，如果开发者用自定义的view，重写ondraw（）应该将调用此方法设置为false，这样程序会调用自定义的布局。

        mDividerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mDividerPaint.setColor(mDividerColor);
        mDividerPaint.setStrokeWidth(mDividerWidth);

        mIndicatorPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mIndicatorPaint.setColor(mIndicatorColor);

        mBottomPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mBottomPaint.setColor(mBottomLineColor);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        int mChildCount = mViewPager.getAdapter().getCount();  // 获取子item的个数
        if (mChildCount == 0) {
            return;
        }
        final int height = getHeight();
        /*当前页面的View tab*/
        View selectView = getChildAt(mSelectedPosition);
        /*计算开始绘制的位置*/
        int left = selectView.getLeft();
        /*计算结束绘制的位置*/
        int right = selectView.getRight();
        if (mSelectionOffset > 0) {
            View nextView = getChildAt(mSelectedPosition + 1);
            /*如果有偏移量，重新计算开始绘制的位置*/
            left = (int) (mSelectionOffset * nextView.getLeft() + (1.0f - mSelectionOffset) * left);
            /*如果有偏移量，重新计算结束绘制的位置*/
            right = (int) (mSelectionOffset * nextView.getRight() + (1.0f - mSelectionOffset) * right);
        }
        /*绘制滑动的页卡*/
        canvas.drawRect(left, height - mIndicatorHeight, right, height, mIndicatorPaint);
        canvas.drawRect(0, height - mBottomLineHeight, getWidth(), height, mBottomPaint);
        for (int i = 0; i < mChildCount - 1; i++) {
            View child = getChildAt(i);
            canvas.drawLine(child.getRight(), mDividerMargin,
                    child.getRight(), height - mDividerMargin,
                    mDividerPaint);
        }
    }

    @Override
    protected void onFinishInflate() {  //当View中所有的子控件均被映射成xml后触发
        super.onFinishInflate();
    }

    /**
     * 设置viewPager，初始化SlidingTab，
     * 在这个方法中为SlidingLayout设置
     * 内容，
     *
     * @param viewPager
     */
    public void setViewPager(ViewPager viewPager) {
        /*先移除所以已经填充的内容*/
        removeAllViews();
        /* viewPager 不能为空*/
        if (viewPager == null) {
            throw new RuntimeException("ViewPager不能为空");
        }
        mViewPager = viewPager;
        mViewPager.addOnPageChangeListener(new InternalViewPagerChange());
        populateTabLayout();
    }

    public void setViewPagerOnChangeListener(ViewPager.OnPageChangeListener pagerOnChangeListener) {
        mListener = pagerOnChangeListener;
    }

    public void setTitleList(ArrayList<String> titles){
        this.titleList = titles;
    }

    /**
     * 填充layout，设置其内容
     */
    private void populateTabLayout() {
        final PagerAdapter adapter = mViewPager.getAdapter();
        final OnClickListener tabOnClickListener = new TabOnClickListener();
        for (int i = 0; i < adapter.getCount(); i++) {
            TextView textView = createDefaultTabView(getContext());
            textView.setOnClickListener(tabOnClickListener);
            if(titleList != null && titleList.size() > 0){
                if(titleList.size() > i){
                    textView.setText(titleList.get(i));
                }
            }
            addView(textView);
        }
    }

    /**
     * 创建默认的TabItem
     *
     * @param context
     * @return
     */
    private TextView createDefaultTabView(Context context) {
        TextView textView = new TextView(context);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, DEFAULT_TEXT_SIZE);
        textView.setTextColor(itemTextDefaultColor);
        textView.setGravity(Gravity.CENTER);
        LayoutParams layoutParams = new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1);
        textView.setLayoutParams(layoutParams);
        int padding = (int) (DEFAULT_TEXT_PADDING * getResources().getDisplayMetrics().density);
        textView.setPadding(padding, padding, padding, padding);
        return textView;
    }

    private class TabOnClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            for (int i = 0; i < getChildCount(); i++) {
                if (v == getChildAt(i)) {
                    mViewPager.setCurrentItem(i);
                    return;
                }
            }
        }
    }


    /**
     * @param position
     * @param positionOffset
     */
    private void viewPagerChange(int position, float positionOffset) {
        mSelectedPosition = position;
        mSelectionOffset = positionOffset;
        invalidate();
    }

    private class InternalViewPagerChange implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

//            Log.v("Liz_Miller", "=============position=" + position + ",====positionOffset=" + positionOffset);
            /*
            * 规律：
            * 当positionOffset为0时，position就是当前view的位置
            * 当positionOffset不为0时，position为左边页面的位置
            *                         position + 1为右边页面的位置
            * */

            viewPagerChange(position, positionOffset);
            if (mListener != null) {
                mListener.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            TextView selectedTitle =(TextView) getChildAt(position);//需改为TextView
            selectedTitle.setTextColor(itemTextSelectedColor);
            for(int i = 0;i<getChildCount();i++){
                if(i==position){
                    continue;
                }else {
                    TextView otherView =(TextView) getChildAt(i);
                    otherView.setTextColor(itemTextDefaultColor);
                }
            }
        }

        @Override
        public void onPageSelected(int position) {

            if (mListener != null) {
                mListener.onPageSelected(position);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

            if (mListener != null) {
                mListener.onPageScrollStateChanged(state);
            }
        }
    }

}
