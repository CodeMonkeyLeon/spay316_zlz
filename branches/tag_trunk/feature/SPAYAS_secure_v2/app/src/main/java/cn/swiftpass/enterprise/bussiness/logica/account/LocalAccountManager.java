package cn.swiftpass.enterprise.bussiness.logica.account;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.igexin.sdk.PushManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.enums.UserRole;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.ErrorMsg;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.bussiness.model.ShopModel;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.database.access.UserInfoDB;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.NetHelper;
import cn.swiftpass.enterprise.utils.AESHelper;
import cn.swiftpass.enterprise.utils.GlobalConstant;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.MD5;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;
import cn.swiftpass.enterprise.utils.Utils;

/**
 * User: Alan
 * Date: 13-9-21
 * Time: 上午11:35
 */
public class LocalAccountManager {
    private SharedPreferences sp;

    public static final String TAG = "LocalAccountManager";

    private LocalAccountManager() {
        sp = MainApplication.getContext().getApplicationPreferences();
    }

    public static LocalAccountManager getInstance() {
        return Container.instance;
    }

    private static class Container {
        public static LocalAccountManager instance = new LocalAccountManager();
    }

    private UserModel user;

    public UserModel getLoggedUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public long getUID() {
        long id = user == null ? 0 : user.uId;
        if (id == 0) {
            id = getLoggedUser() != null ? getLoggedUser().uId : 0;
        }
        return id;
    }

    public String getUserName() {
        if (user != null) {
            //            String name = user.realName;
            //            return name == null ? user.name : name + "";
        }
        return "";
    }

    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    public void getCode(final UINotifyListener<String> listener) {

        ThreadHelper.executeWithCallback(new Executable<String>() {
            @Override
            public String execute() throws Exception {
                JSONObject param = null;
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/refreshLoginCode", param, null, null);
                try {
                    if (!result.hasError()) {

                        if (result.data == null) {
                            listener.onError("连接服务器失败，请稍候再试");
                            return null;
                        }
                        String res = result.data.getString("result");
                        int ret = Utils.Integer.tryParse(res, -1);
                        if (ret == 200) {

                            return result.data.getString("message");
                        } else {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                        }
                        return null;
                    }
                } catch (Exception e) {
                    listener.onError("连接服务器失败，请稍候再试");
                    return null;
                }

            }
        }, listener);
    }


    /**
     * pc 扫码登录
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    public void scanLogin(final String code, final UINotifyListener<Boolean> listener) {

        ThreadHelper.executeWithCallback(new Executable<Boolean>() {
            @Override
            public Boolean execute() throws Exception {
                JSONObject param = new JSONObject();
                param.put("loginId", code);
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/qrLogin", param, null, null);
                try {
                    if (!result.hasError()) {

                        if (result.data == null) {
                            listener.onError("连接服务器失败，请稍候再试");
                            return false;
                        }
                        String res = result.data.getString("result");
                        int ret = Utils.Integer.tryParse(res, -1);
                        if (ret == 200) {
                            return true;
                        } else {
                            listener.onError(result.data.getString("message"));
                            return false;
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                        }
                        return false;
                    }
                } catch (Exception e) {
                    listener.onError("连接服务器失败，请稍候再试");
                    return false;
                }

            }
        }, listener);
    }

    /**
     * <一句话功能简述>
     * 预埋 是否登录界面开发注册按钮
     *
     * @see [类、类#方法、类#成员]
     */
    public void showReg(final UINotifyListener<String> listener) {
        ThreadHelper.executeWithCallback(new Executable<String>() {
            @Override
            public String execute() throws Exception {
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/showReg", "", null, null);
                result.setNotifyListener(listener);
                try {
                    if (!result.hasError()) {

                        if (result.data == null) {
                            return null;
                        }

                        String res = result.data.getString("result");
                        Logger.i("hehui", "res-->" + result.data);
                        int ret = Utils.Integer.tryParse(res, -1);
                        if (ret == 200) {
                            JSONObject jsonObject = new JSONObject(result.data.getString("message"));
                            Integer show = jsonObject.getInt("show");
                            if (show == 1) {//可以注册
                                return jsonObject.optString("redirectUrl", "");
                            } else {
                                return null;
                            }
                        } else {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                        }
                        return null;
                    }
                } catch (Exception e) {
                    listener.onError("请求失败，请稍候再试");
                    return null;
                }

            }
        }, listener);
    }

    /**
     * <一句话功能简述>
     * 切换语言 得到不同语言配置信息
     *
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void getBase(final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable<Boolean>() {
            @Override
            public Boolean execute() throws Exception {
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/config/getBase", "", null, null);
                result.setNotifyListener(listener);
                try {
                    if (!result.hasError()) {

                        if (result.data == null) {
                            return false;
                        }

                        String res = result.data.getString("result");
                        Logger.i("hehui", "getBase-->" + result.data);
                        int ret = Utils.Integer.tryParse(res, -1);
                        if (ret == 200) {
                            //                            parseJson(result, logUser);
                            try {
                                JSONObject jsonObject = new JSONObject(result.data.getString("message"));
                                MainApplication.payTypeMap = JsonUtil.jsonToMap(jsonObject.optString("pay_type", ""));
                                MainApplication.tradeStateMap = JsonUtil.jsonToMap(jsonObject.optString("trade_state", ""));
                                MainApplication.refundStateMap = JsonUtil.jsonToMap(jsonObject.optString("refund_state", ""));
                                MainApplication.setRefundStateMap(MainApplication.refundStateMap);
                                MainApplication.setPayTypeMap(MainApplication.payTypeMap);
                                MainApplication.setTradeStateMap(MainApplication.tradeStateMap);
                            } catch (Exception e) {
                                Log.e(TAG, Log.getStackTraceString(e));
                            }

                            return true;
                        } else {
                            listener.onError(result.data.getString("message"));
                            return false;
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return false;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return false;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return false;
                        }
                        return false;
                    }
                } catch (Exception e) {
                    listener.onError("登录失败，请稍候再试");
                    return false;
                }

            }
        }, listener);
    }

    //安全检查，封装函数
    public JSONObject getLocalParam(String username,String pwd){
        JSONObject param = null;
        try{
            param = new JSONObject();
            param.put("username", username);
            param.put("password", pwd);
            param.put("client", MainApplication.CLIENT);
            param.put("bankCode", ApiConstant.bankCode);
            param.put("androidTransPush", "1");
            String cid = PushManager.getInstance().getClientid(MainApplication.getContext());
            param.put("pushCid", cid);
        }catch (Exception e){
            Log.e(TAG,Log.getStackTraceString(e));
        }
        return param;
    }
    /**
     * <一句话功能简述>
     * 自动登录
     *
     * @see [类、类#方法、类#成员]
     */
    public void deviceLogin(final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable<Boolean>() {
            @Override
            public Boolean execute() throws Exception {
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.USER_BAST + "/deviceLogin", "", null, null);
                result.setNotifyListener(listener);
                try {
                    if (!result.hasError()) {

                        if (result.data == null) {
                            listener.onError("登录失败,请稍后在试!");
                            return false;
                        }

                        String res = result.data.getString("result");
                        Logger.i("hehui", "res-->" + result.data);
                        int ret = Utils.Integer.tryParse(res, -1);
                        UserModel logUser = new UserModel();
                        if (ret == 200) {
                            parseJson(result, logUser);
                            return true;
                        } else {
                            listener.onError(result.data.getString("message"));
                            return false;
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return false;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return false;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return false;
                        }
                        //                        //登录失败，是否离线交易
                        //                        if (result.resultCode >= 500 && result.resultCode <= 599)
                        //                        {
                        //                            listener.onError("登录失败，连接服务器失败");
                        //                        }
                        //                        else if (result.getMessage() == null)
                        //                        {
                        //                            listener.onError("登录失败，请稍候再试");
                        //                        }
                        return false;
                    }
                } catch (Exception e) {
                    listener.onError("登录失败，请稍候再试");
                    return false;
                }

            }
        }, listener);
    }

    public void refundLogin(final String code, final String mId, final String username, final String password, final String imei, final String imsi, final boolean isNormalUser, final UINotifyListener<Boolean> listener) {

        ThreadHelper.executeWithCallback(new Executable<Boolean>() {
            @Override
            public Boolean execute() throws Exception {

                JSONObject param = new JSONObject();
                param.put("username", username);
                long spayRs = System.currentTimeMillis();
                String paseStr = MD5.md5s(spayRs + "2017SWIFTpassSPAYCCC").toUpperCase();
                param.put("password", AESHelper.aesEncrypt(password, paseStr.substring(8, 24)));
                param.put("client", MainApplication.CLIENT);
                param.put("bankCode", ApiConstant.bankCode);
                param.put("androidTransPush", "1");
                if (!StringUtil.isEmptyOrNull(code)) {
                    param.put("code", code);
                }
                String cid = PushManager.getInstance().getClientid(MainApplication.getContext());
                param.put("pushCid", cid);

                param.put("spayRs", String.valueOf(spayRs));
                param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MD5.md5s(String.valueOf(spayRs))));
                Logger.i("hehui", "loginAsync param->>" + param.toString());
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.USER_BAST + "/spayLogin", param, String.valueOf(spayRs), null);

                try {
                    if (!result.hasError()) {

                        if (result.data == null) {
                            listener.onError("登录失败,请稍后在试!");
                            return false;
                        }

                        String res = result.data.getString("result");
                        Logger.i(TAG, "res-->" + result.data);
                        int ret = Utils.Integer.tryParse(res, -1);
                        UserModel logUser = new UserModel();
                        if (ret == 200) {
                            MainApplication.userName = username;
                            parseJson(result, logUser);
                            return true;
                        } else if (ret == 403) {//需要输入验证
                            String c = result.data.getString("code");
                            if (!StringUtil.isEmptyOrNull(code)) {
                                ErrorMsg msg = new ErrorMsg();
                                msg.setMessage(result.data.getString("message"));
                                msg.setContent(result.data.getString("code"));
                                listener.onError(msg);
                            } else {
                                listener.onError("403" + c);
                            }
                            return false;
                        } else {
                            listener.onError(result.data.getString("message"));
                            return false;
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return false;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return false;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return false;
                        }
                        //登录失败，是否离线交易
                        if (result.resultCode >= 500 && result.resultCode <= 599) {
                            listener.onError("登录失败，连接服务器失败");
                        } else if (result.getMessage() == null) {
                            listener.onError("登录失败，请稍候再试");
                        }
                        return false;
                    }
                } catch (Exception e) {
                    listener.onError("登录失败，请稍候再试");
                    return false;
                }

            }
        }, listener);
    }

    /**
     * 员工登录
     *
     * @param username
     * @param pwd
     * @param listener
     *                 0.登录成功
     *                 1.商户待审核
     *                 2.商户初审通过
     *                 3.商户初审不通过
     *                 5.商户终审不通过
     *                 6.商户失效或过期
     *                 7.商户不存在
     *                 8.密码错误
     */
    public void loginAsync(final String code, final String mId, final String username, final String password, final String imei, final String imsi, final boolean isNormalUser, final UINotifyListener<Boolean> listener) {

        ThreadHelper.executeWithCallback(new Executable<Boolean>() {
            @Override
            public Boolean execute() throws Exception {
                JSONObject param = new JSONObject();
                param.put("username", username);
                long spayRs = System.currentTimeMillis();
                String paseStr = MD5.md5s(spayRs + "2017SWIFTpassSPAYCCC").toUpperCase();
                param.put("password", AESHelper.aesEncrypt(password, paseStr.substring(8, 24)));
                param.put("client", MainApplication.CLIENT);
                param.put("bankCode", ApiConstant.bankCode);
                param.put("androidTransPush", "1");
                if (!StringUtil.isEmptyOrNull(code)) {
                    param.put("code", code);
                }
                String cid = PushManager.getInstance().getClientid(MainApplication.getContext());
                param.put("pushCid", cid);
                param.put("spayRs", String.valueOf(spayRs));
                param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MD5.md5s(String.valueOf(spayRs))));
                Logger.i("hehui", "loginAsync param->>" + param.toString());
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.USER_BAST + "/spayLoginWithCode", param, String.valueOf(spayRs), null);
                try {
                    if (!result.hasError()) {

                        if (result.data == null) {
                            listener.onError("登录失败,请稍后在试!");
                            return false;
                        }

                        String res = result.data.getString("result");
                        Logger.i("hehui", "res-->" + result.data);
                        int ret = Utils.Integer.tryParse(res, -1);
                        UserModel logUser = new UserModel();
                        if (ret == 200) {
                            MainApplication.userName = username;
                            parseJson(result, logUser);
                            return true;
                        } else if (ret == 403) {//需要输入验证
                            String c = result.data.getString("code");
                            if (!StringUtil.isEmptyOrNull(code)) {
                                ErrorMsg msg = new ErrorMsg();
                                msg.setMessage(result.data.getString("message"));
                                msg.setContent(result.data.getString("code"));
                                listener.onError(msg);
                            } else {
                                listener.onError("403" + c);
                            }
                            return false;
                        } else {
                            listener.onError(result.data.getString("message"));
                            return false;
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                                return false;
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                                return false;
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                                return false;
                        }
                        //登录失败，是否离线交易
                        if (result.resultCode >= 500 && result.resultCode <= 599) {
                            listener.onError("登录失败，连接服务器失败");
                        } else if (result.getMessage() == null) {
                            listener.onError("登录失败，请稍候再试");
                        }
                        return false;
                    }
                } catch (Exception e) {
                    listener.onError("登录失败，请稍候再试");
                    return false;
                }

            }
        }, listener);
    }

    /**
     * 退款的时候验证用户
     *
     * @param mId      商户ID
     * @param username 用户名
     * @param pwd 密码
     * @param imei
     * @param imsi
     */
    public void checkUser(final String mId, final String username, final String password, final String imei, final String imsi, final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable<Boolean>() {
            @Override
            public Boolean execute() throws Exception {
                JSONObject param = new JSONObject();
                param.put("name", username);
                param.put("password", password);
                param.put("imei", imei); //imei
                param.put("imsi", imsi);
                param.put("clientType", ApiConstant.pad + "");
                param.put("merchantId", mId);
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.USER_BAST + "/login", param, null, null);
                if (!result.hasError()) {
                    String res = result.data.getString("result");
                    int ret = Utils.Integer.tryParse(res, -1);
                    switch (ret) {
                        case 0:
                            return true;

                        case 1:
                            listener.onError("登录失败，商户待审核。");
                            break;
                        case 2:
                            listener.onError("退款失败，商户初审通过,终审未通过，请联系代理商!");
                            break;
                        case 3:
                            listener.onError("登录失败，商户初审不通过");
                            break;
                        case 4:
                            listener.onError("登录失败。设备未激活，请审核员登录后进行激活");
                            break;
                        case 5:
                            listener.onError("登录失败，商户终审不通过。");
                            break;
                        case 6:
                            listener.onError("登录失败，商户失效或过期。");
                            break;
                        case 7:
                            listener.onError("登录失败，商户不存在。");
                            break;
                        case 8:
                            listener.onError("密码错误，请重新输入");
                            break;
                        case 9:
                            listener.onError("登录无效，设备无效，检查设备IMEI号");
                            break;
                        case 11:
                            listener.onError("登录失败，内部异常，请稍候再试试？");
                            //DataObserverManager.getInstance().notifyChange(DataObserver.EVENT_LOGIN_FAIL_OFFLINE_PAY);
                            break;
                        case 12:
                            listener.onError("登录失败，内部异常，请稍候再试试？");
                            //  parseJson(result,logUser);
                            //DataObserverManager.getInstance().notifyChange(DataObserver.EVENT_LOGIN_FAIL_MAKE_DEVICE);
                            break;
                    }

                } else {
                    if (result.getMessage() == null) {
                        listener.onError("登录失败，请稍候再试！");
                    } else {
                        Logger.i(result.getMessage());
                        listener.onError("登录失败，" + result.getMessage());
                    }
                    return false;
                }
                return false;
            }
        }, listener);
    }

    @SuppressWarnings("unused")
    private void parseJson(RequestResult result, UserModel logUser) throws JSONException {
        //        try
        //        {
        String o = result.data.getString("message");

        JSONObject jsonObject = new JSONObject(o);
        logUser.uId = Long.parseLong(jsonObject.getString("id"));
        MainApplication.userId = Long.parseLong(jsonObject.getString("id"));
        //        logUser.name = jsonObject.getString("userName");
        MainApplication.userName = jsonObject.optString("username", "");
        //        logUser.pwd = jsonObject.getString("pwd");
        //        logUser.realName = jsonObject.getString("realName");
        //        String name = jsonObject.optString("username", "");
        //        String str = name.substring(name.lastIndexOf("@") + 1);

        MainApplication.isActivityAuth = jsonObject.optInt("isActivityAuth", 0);
        MainApplication.themeMd5 = jsonObject.optString("themeMd5", "");
        MainApplication.Md5 = jsonObject.optString("apiShowListMd5", "");
        MainApplication.merchantId = jsonObject.optString("mchId", "");
        MainApplication.remark = jsonObject.optString("remark", "");
        MainApplication.phone = jsonObject.optString("phone", "");
        MainApplication.isAdmin = jsonObject.optString("isAdmin", "0");
        MainApplication.isOrderAuth = jsonObject.optString("isOrderAuth", "0");
        MainApplication.realName = jsonObject.optString("realname", "");
        MainApplication.mchName = jsonObject.optString("mchName", "");
        MainApplication.channelId = jsonObject.optString("channelId", "");
        MainApplication.isRefundAuth = jsonObject.optString("isRefundAuth", "0");
        String url = ApiConstant.BASE_URL_PORT + ApiConstant.DONLOAD_IMAGE;
        MainApplication.mchLogo = jsonObject.optString("mchLogo", "");
        MainApplication.body = jsonObject.optString("tradeName", "");
        MainApplication.serviceType = jsonObject.optString("serviceType", "");
        MainApplication.signKey = jsonObject.optString("signKey", "");
        MainApplication.isTotalAuth = jsonObject.optInt("isTotalAuth", 0);
        MainApplication.realName = jsonObject.optString("realname", "");
        MainApplication.feeType = jsonObject.optString("feeType", "CNY");
        MainApplication.feeFh = jsonObject.optString("feeFh", ToastHelper.toStr(R.string.tx_mark));
        MainApplication.showEwallet = jsonObject.optInt("showEwallet", 0);
        MainApplication.isFixCode = jsonObject.optInt("isFixCode", 0); //固定二维码的聚合和非聚合的判断

        MainApplication.setSurchargeOpen(((String)jsonObject.optString("isSurchargeOpen")).equals((String)"1"));
        MainApplication.setTipOpen(((String)jsonObject.optString("isTipOpen")).equals((String)"1"));
        //屏蔽预扣税功能
//        MainApplication.setTaxRateOpen(jsonObject.optString("isTaxRateOpen").equals("1"));

        MainApplication.setMchId(MainApplication.merchantId);
        MainApplication.setMchName(MainApplication.mchName);
        MainApplication.setUserId(MainApplication.userId);

        MainApplication.setSignKey(MainApplication.signKey);
        MainApplication.setFeeType(MainApplication.feeType);
        if (MainApplication.feeFh == null || MainApplication.feeFh.equals("null")) {
            MainApplication.feeFh = ToastHelper.toStr(R.string.tx_mark);
        }
        if (!MainApplication.mchLogo.equals("") && !MainApplication.mchLogo.equals("null") && MainApplication.mchLogo != null) {
            MainApplication.mchLogo = url + MainApplication.mchLogo;
        }
        try {
            MainApplication.setFeeFh(MainApplication.feeFh);
            MainApplication.payTypeMap = JsonUtil.jsonToMap(jsonObject.optString("payTypeMap", ""));
            MainApplication.tradeStateMap = JsonUtil.jsonToMap(jsonObject.optString("tradeStateMap", ""));
            MainApplication.refundStateMap = JsonUtil.jsonToMap(jsonObject.optString("refundStateMap", ""));
            MainApplication.apiProviderMap = JsonUtil.jsonToMap(jsonObject.optString("apiProviderMap", ""));
            MainApplication.setPayTypeMap(MainApplication.payTypeMap);
            MainApplication.setTradeStateMap(MainApplication.tradeStateMap);
            MainApplication.setRefundStateMap(MainApplication.refundStateMap);

        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }

        //取登录cookie 
        try {

            JSONObject cObject = new JSONObject(jsonObject.optString("cookieMap", ""));
            if (null != cObject) {
                //            MainApplication.cookie_key =
                //            MainApplication.SAUTHID = cObject.optString("SAUTHID", "");
                PreferenceUtil.commitString("login_skey", cObject.optString("SKEY", ""));
                PreferenceUtil.commitString("login_sauthid", cObject.optString("SAUTHID", ""));

                MainApplication.newSignKey = MD5.md5s(MainApplication.signKey + cObject.optString("SKEY", ""));
            } else {
                MainApplication.newSignKey = MD5.md5s(MainApplication.signKey + PreferenceUtil.getString("login_skey", ""));
            }
        } catch (Exception e) {
            MainApplication.newSignKey = MD5.md5s(MainApplication.signKey + PreferenceUtil.getString("login_skey", ""));
        }

        MainApplication.setNewSignKey(MainApplication.newSignKey);
    }

    //新增
//    @Override
//    public boolean equals(Object obj) {
//        return super.equals(obj);
//    }

    /**
     * 清除用户名
     */
    public void clearUserName() {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(GlobalConstant.FIELD_USERNAME, "");
        editor.commit();
    }

   /* *//**
     * 清除密码
     *//*
    public void clearPwd() {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(GlobalConstant.FIELD_PAWESSORD, "").commit();
    }*/

    /**
     * 清除密码
     */
    public void clearMId() {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(GlobalConstant.FIELD_MERCHANT_ID, "").commit();
    }

/*    *//**
     * 本地校验
     *//*
    public UserModel checkInUser(String username, String pwd) {
        try {

            UserModel userModel = UserInfoDB.getInstance().queryUser(0, username, pwd, null);
            this.user = userModel;
            return userModel;
        } catch (SQLException e) {
            Logger.i(e);
            return null;
        }
    }*/

    /**
     * 获取分配的设备号
     */
    public String getDerviceNo() {
        sp = MainApplication.getContext().getApplicationPreferences();
        return sp.getString(GlobalConstant.FIELD_DERVICE_ID, "");

    }

    /**
     * 获取分配的商户kye
     */
    public String getPatentKey() {
        sp = MainApplication.getContext().getApplicationPreferences();
        return sp.getString(ApiConstant.P_KEY, "");
    }

    /**
     * 获取财付通的商户
     */
    public String getPatent() {
        sp = MainApplication.getContext().getApplicationPreferences();
        return sp.getString(ApiConstant.P_PARTNER, "");
    }

    /***/
    public String getLocalMEI() {
        sp = MainApplication.getContext().getApplicationPreferences();
        return sp.getString(ApiConstant.P_IMEI, System.currentTimeMillis() + "");
    }

    /**
     * e
     */
    public void setLocalMEI(String imei) {
        sp = MainApplication.getContext().getApplicationPreferences();
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(ApiConstant.P_IMEI, imei);
        editor.commit();
    }

    /**
     * 获取商户编号
     */
    public String getMerchantNo() {
        MainApplication.getContext().getApplicationPreferences();
        SharedPreferences sp = MainApplication.getContext().getApplicationPreferences();
        return sp.getString(GlobalConstant.FIELD_MERCHANT_ID, "");
    }

    public void saveDerviceNoAndMId(String mId, String derviceNo) {
        sp = MainApplication.getContext().getApplicationPreferences();
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(GlobalConstant.FIELD_DERVICE_ID, derviceNo);
        editor.putString(GlobalConstant.FIELD_MERCHANT_ID, mId);
        editor.commit();
    }

    /**
     * 设备激活
     */
    /*public void activateDevice(final String name, final String pwd, final String imei, final String imsi, final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable<Boolean>() {
            @Override
            public Boolean execute() throws Exception {

                JSONObject param = new JSONObject();
                param.put("imei", imei);
                param.put("imsi", imsi);
                param.put("macAdd", NetworkUtils.getLocalMacAddress());
                param.put("name", name); //每个商户的管理员账号 是唯一的
                param.put("clientType", ApiConstant.pad + "");
                param.put("pwd", pwd);
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.USER_BAST + "/activateDevice", param, null, null);
                if (!result.hasError()) {
                    String res = result.data.getString("result");
                    int ret = Utils.Integer.tryParse(res, -1);
                    switch (ret) {
                        case 0:
                            listener.onError("sorry，设备激活失败，设备无效。");
                            return false;
                        case 1:
                            //listener.onError("激活成功。");
                            String mId = result.data.getString("mId");
                            String derviceNo = result.data.getString("derviceNo");
                            saveDerviceNoAndMId(mId, derviceNo);
                            setLocalMEI(imei);
                            return true;
                        case 2:
                            listener.onError("设备激活失败,已经超过使用激活设备数量,请确认。");
                            return false;
                        case -1:
                            listener.onError("sorry，设备激活失败。");
                            return false;
                    }

                } else {
                    return false;
                }
                return false;
            }
        }, listener);
    }
*/
    /**
     * 修改密码
     */
    public void changePwd(final String oldPwd, final String phone, final String newPwd, final String repPassword, final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable<Boolean>() {
            @Override
            public Boolean execute() throws Exception {
                long spayRs = System.currentTimeMillis();
                JSONObject param = new JSONObject();
                Map<String, String> map = new HashMap<String, String>();
                String paseStr = MD5.md5s(spayRs + "2017SWIFTpassSPAYCCC").toUpperCase();
                param.put("telephone", phone);
                param.put("oldPassword", AESHelper.aesEncrypt(oldPwd, paseStr.substring(8, 24)));
                param.put("password", AESHelper.aesEncrypt(newPwd, paseStr.substring(8, 24)));
                param.put("repPassword", AESHelper.aesEncrypt(repPassword, paseStr.substring(8, 24)));
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    param.put("mchId", MainApplication.merchantId);
                    map.put("mchId", MainApplication.merchantId);
                } else {
                    param.put("mchId", MainApplication.getMchId());
                    map.put("mchId", MainApplication.getMchId());
                }

                if (MainApplication.userId > 0) {

                    param.put("userId", MainApplication.userId);
                    map.put("userId", MainApplication.userId + "");
                } else {
                    param.put("userId", MainApplication.getUserId());
                    map.put("userId", MainApplication.getUserId() + "");
                }

                map.put("telephone", phone);
                map.put("oldPassword", oldPwd);
                map.put("password", newPwd);
                map.put("repPassword", repPassword);

                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey())) {
                    param.put("sign", SignUtil.getInstance().createSign(map, MainApplication.getSignKey()));

                    Logger.i("hehui", "sign-->" + SignUtil.getInstance().createSign(map, MainApplication.getSignKey()));
                }

                //                    //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                    param.put("spayRs", spayRs);
                    param.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(param.toString()), MainApplication.getNewSignKey()));
                }

                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/user/resetPwd", param, String.valueOf(spayRs), null);
                if (!result.hasError()) {
                    int res = Integer.parseInt(result.data.getString("result"));
                    if (res == 200) {
                        //修改密码后进行清空之前的密码
                       /* SharedPreferences.Editor editor = sp.edit();
                        editor.putString(GlobalConstant.FIELD_PAWESSORD, "").commit();*/
                        return true;
                    } else {
                        listener.onError(result.data.getString("message"));
                        return false;
                    }
                } else {
                    if (result.getMessage() == null) {
                        listener.onError("修改密码失败，请稍候再试");
                    } else {
                        Logger.i(result.getMessage());
                        listener.onError(result.getMessage());
                    }
                    return false;
                }
                //                return false;
            }

        }, listener);
    }

    public boolean buildHearbate() {
        try {
            //获取离线订单进行上传
            List<Order> orders = OrderManager.getInstance().getOfflineOrder();
            JSONObject param = new JSONObject();
            param.put("uId", getUID() + "");
            param.put("sessionId", ApiConstant.SESSION_ID);
            Gson g = new Gson();
            if (orders != null && orders.size() > 0) {
                param.put("offlineOrder", g.toJson(orders));//离线订单信息传送给服务器
            }
            RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.HEARBEAT_BAST, param, null, null);
            if (!result.hasError()) {
                boolean res = Boolean.parseBoolean(result.data.getString("result"));
                //修改订单状态为已经上传
                if (orders != null && orders.size() > 0) {
                    OrderManager.getInstance().updateOfflineOrder(orders);
                }
                return res;
            }

        } catch (Exception e) {
            Logger.i(e);
        }
        return false;
    }

    /**
     * 检测版本时间 发现如果太快了 在3分钟内 就不检测升级
     */
    public boolean mathCheckVersionTime() {
        long currentTime = System.currentTimeMillis();
        long time = sp.getLong(GlobalConstant.FILED_T_CHECK_VERSION, 0);

        if (time == 0) {
            return false;
        }
        if (currentTime - time <= 1000 * 60 * 3) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 统一冲正接口 <功能详细描述>
     *
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void getDyn(final UINotifyListener<DynModel> listener) {
        ThreadHelper.executeWithCallback(new Executable<DynModel>() {
            @Override
            public DynModel execute() throws Exception {

                JSONObject jsonObject = new JSONObject();
                try {
                    if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                        jsonObject.put("mchId", MainApplication.merchantId);// todo
                    } else {
                        jsonObject.put("mchId", MainApplication.getMchId());// todo
                    }

                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/getDyn", jsonObject, null, null);
                    result.setNotifyListener(listener);
                    if (!result.hasError()) {

                        if (result.data != null) {
                            Logger.i("hehui", "getDyn-->" + result.data);
                            int code = Integer.parseInt(result.data.getString("result"));
                            if (code == 200) {
                                return (DynModel) JsonUtil.jsonToBean(result.data.getString("message"), DynModel.class);

                            } else {
                                listener.onError(result.data.getString("message"));
                            }
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                        }
                    }

                    return null;
                } catch (Exception e) {

                    listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                    return null;
                }
            }
        }, listener);

    }

    /**
     * 得到实时费率
     * <功能详细描述>
     *
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void getExchangeRate(final UINotifyListener<Boolean> listener) {
        ThreadHelper.executeWithCallback(new Executable<Boolean>() {
            @Override
            public Boolean execute() throws Exception {
                JSONObject jsonObject = new JSONObject();
                try {
                    if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                        jsonObject.put("mchId", MainApplication.merchantId);// todo
                    } else {
                        jsonObject.put("mchId", MainApplication.getMchId());// todo
                    }
//                    jsonObject.put("exchangeCurrency","USD");
                    long spayRs = System.currentTimeMillis();
                    //加签名
                    if (!StringUtil.isEmptyOrNull(MainApplication.newSignKey)) {
                        jsonObject.put("spayRs", spayRs);
                        jsonObject.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(jsonObject.toString()), MainApplication.newSignKey));
                    }
                    Logger.i("hehui", "getExchangeRate  param-->" + jsonObject.toString());
                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/mch/getExchangeRate", jsonObject, String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    if (!result.hasError()) {
                        if (result.data != null) {
                            Logger.i("hehui", "getExchangeRate-->" + result.data);
                            int code = Integer.parseInt(result.data.getString("result"));
                            if (code == 200) {
                                JSONObject json = new JSONObject(result.data.getString("message"));
                                if (json.has("rate")) {
                                    MainApplication.setExchangeRate(json.optString("rate", ""));
                                }
                                if (json.has("vatRate")) {
                                    MainApplication.setVatRate(json.getDouble("vatRate"));
                                }
                                if (json.has("surchargeRate")) {
                                    MainApplication.setSurchargeRate(json.getDouble("surchargeRate"));
                                }
                                if (json.has("taxRate ")) {
                                    MainApplication.setTaxRate(json.getDouble("taxRate"));
                                }
                                if (json.has("usdToRmbExchangeRate")) {
                                    MainApplication.setUsdToRmbExchangeRate(json.getDouble("usdToRmbExchangeRate"));
                                }
                                if (json.has("sourceToUsdExchangeRate")) {
                                    MainApplication.setSourceToUsdExchangeRate(json.getDouble("sourceToUsdExchangeRate"));
                                }
                                return true;
                            } else {
                                MainApplication.setExchangeRate("1");
                                listener.onError(result.data.getString("message"));
                            }
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                        }
                    }
                    return null;
                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                    return false;
                }
            }

        }, listener);

    }

    /**
     * 动态加载支付类型以及对于的图片
     * <功能详细描述>
     *
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void apiShowList(final UINotifyListener<List<DynModel>> listener) {
        ThreadHelper.executeWithCallback(new Executable<List<DynModel>>() {
            @Override
            public List<DynModel> execute() throws Exception {

                JSONObject jsonObject = new JSONObject();
                try {
                    if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                        jsonObject.put("mchId", MainApplication.merchantId);// todo
                    } else {
                        jsonObject.put("mchId", MainApplication.getMchId());// todo
                    }
                    long spayRs = System.currentTimeMillis();
                    //加签名
                    if (!StringUtil.isEmptyOrNull(MainApplication.newSignKey)) {
                        jsonObject.put("spayRs", spayRs);
                        jsonObject.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(jsonObject.toString()), MainApplication.newSignKey));
                    }
                    Logger.i("hehui", "apiShowList  param-->" + jsonObject.toString());
                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/apiShowList", jsonObject, String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    if (!result.hasError()) {

                        if (result.data != null) {
                            Logger.i("hehui", "apiShowList-->" + result.data);
                            int code = Integer.parseInt(result.data.getString("result"));
                            if (code == 200) {
                                JSONObject json = new JSONObject(result.data.getString("message"));
                                List<DynModel> list = new ArrayList<DynModel>();
                                list.addAll(((List<DynModel>) JsonUtil.jsonToList(json.getString("data"), new com.google.gson.reflect.TypeToken<List<DynModel>>() {
                                }.getType())));

                                //过滤
                                //List<DynModel> filterLst = new ArrayList<DynModel>();
                                if (list.size() > 0) {
                                    list.get(0).setMd5(json.optString("md5", ""));
                                }

                                return list;

                            } else {
                                listener.onError(result.data.getString("message"));
                            }
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                        }
                    }

                    return null;
                } catch (Exception e) {

                    listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                    return null;
                }
            }

        }, listener);

    }

//    /**
//     * 过滤支付类型
//     * <功能详细描述>
//     *
//     * @param list
//     * @see [类、类#方法、类#成员]
//     */
//    private void filterList(List<DynModel> list, List<DynModel> filterLst) {
//        //        if (MainApplication.apiProviderMap != null && MainApplication.apiProviderMap.size() > 0)
//        //        {
//        //            for (DynModel dynModel : list)
//        //            {
//        //                for (String key : MainApplication.apiProviderMap.keySet())
//        //                {
//        //                    if (key.equals(dynModel.getApiCode()) && !StringUtil.isEmptyOrNull(dynModel.getNativeTradeType()))
//        //                    {
//        //                        filterLst.add(dynModel);
//        //                    }
//        //                }
//        //            }
//        //        }
//
//        if (!StringUtil.isEmptyOrNull(MainApplication.serviceType)) {
//            String[] arrPays = MainApplication.serviceType.split("\\|");
//
//            for (DynModel dynModel : list) {
//                if (!StringUtil.isEmptyOrNull(dynModel.getNativeTradeType())) {
//                    for (int i = 0; i < arrPays.length; i++) {
//                        if (arrPays[i].startsWith(dynModel.getNativeTradeType()) || dynModel.getNativeTradeType().contains(arrPays[i])) {
//                            filterLst.add(dynModel);
//                            break;
//                        }
//                    }
//                } else {
//                    continue;
//                }
//            }
//        }
//
//    }

    /**
     * 保存检测版本时间
     */
    public void saveCheckVersionTime() {
        SharedPreferences.Editor edit = sp.edit();
        edit.putLong(GlobalConstant.FILED_T_CHECK_VERSION, System.currentTimeMillis());
        edit.commit();
    }

    /**
     * 是否第一次打开程序
     */
    public boolean getFirstLaunch() {
        return sp.getBoolean(GlobalConstant.FIELD_FIRST_IS_FIRST, false);
    }

    /**
     * 设置第一次
     */
    public void setFirstLaunch() {
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(GlobalConstant.FIELD_FIRST_IS_FIRST, true).commit();
    }

    /**
     * 必须升级标识
     */
    public boolean getcheckVersionFlag() {
        return sp.getBoolean(GlobalConstant.FILED_T_CHECK_VERSION_MUST_UPDATE, false);
    }

    /**
     * 必须升级标识
     */
    public void saveCheckVersionFlag(boolean mustUpdate) {
        SharedPreferences.Editor edit = sp.edit();
        edit.putBoolean(GlobalConstant.FILED_T_CHECK_VERSION_MUST_UPDATE, mustUpdate);
        edit.commit();
    }

    /**
     * 测试账号信息
     */
    public void getTestUserInfo() {
        ApiConstant.QUITE_USER_NAME = sp.getString(GlobalConstant.FILED_T_USERNAME, "");
        ApiConstant.QUITE_USER_PWD = sp.getString(GlobalConstant.FILED_T_PWD, "");
        ApiConstant.QUITE_MERCHANT_ID = sp.getString(GlobalConstant.FILED_T_MID, "");
    }

    /**
     * 是否已经登陆
     *
     * @return
     */
    public synchronized boolean isLoggedIn() {
        return user != null && user.name != null;
    }

    public String getMId() {
        SharedPreferences sp = MainApplication.getContext().getApplicationPreferences();
        return sp.getString(GlobalConstant.FIELD_MERCHANT_ID, null);
    }

    /**
     * 最近登录的用户
     */
    public void query(UINotifyListener<List<UserModel>> listener) {
        ThreadHelper.executeWithCallback(new Executable<List<UserModel>>() {
            @Override
            public List<UserModel> execute() throws Exception {
                String mId = getMId();
                return UserInfoDB.getInstance().query(mId);
            }
        }, listener);

    }


    public void onDestory() {
       /* SharedPreferences.Editor editor = sp.edit();
        editor.putString(GlobalConstant.FIELD_PAWESSORD, "").commit();*/
        this.user = null;
    }

    public ShopModel shopModel;



    /**
     * 是否管理员
     */
    public boolean isManager() {
        return this.user.role == UserRole.approve ? true : false;
    }
}
