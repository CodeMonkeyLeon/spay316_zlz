package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bean.QRCodeBean
import cn.swiftpass.enterprise.common.Constant
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.ScanBindContract
import cn.swiftpass.enterprise.utils.JsonUtil
import com.example.common.entity.EmptyData
import com.example.common.sentry.SentryUtils.uploadNetInterfaceException
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/11/23
 *
 * @大家走进了夜海
 * @去打捞遗失的繁星
 */
class ScanBindPresenter : ScanBindContract.Presenter {

    private var mView: ScanBindContract.View? = null


    override fun bindCode(qrId: String?, deskName: String?) {
        mView?.let { view ->
            AppClient.bindCode(
                MainApplication.getInstance().getMchId(),
                MainApplication.getInstance().getUserId().toString(),
                qrId,
                deskName,
                Constant.CLIENT,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        commonResponse?.let {
                            view.bindCodeFailed(it.message)
                        }
                    }

                    override fun onResponse(response: CommonResponse?) {
                        response?.let {
                            view.bindCodeSuccess(it.message)
                        }
                    }
                }
            )

        }
    }

    override fun bindCodeDetails(qrId: String?, deskName: String?) {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)

            AppClient.bindCodeDetails(
                MainApplication.getInstance().getMchId(),
                MainApplication.getInstance().getUserId().toString(),
                qrId,
                deskName,
                Constant.CLIENT,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.bindCodeDetailsFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            try {
                                val order = JsonUtil.jsonToBean(
                                    response.message,
                                    QRCodeBean::class.java
                                ) as QRCodeBean
                                view.bindCodeDetailsSuccess(order)
                            } catch (e: Exception) {
                                uploadNetInterfaceException(
                                    e,
                                    "spay/qrCodeInfo",
                                    "QRCodeBean json解析异常",
                                    ""
                                )
                                view.bindCodeDetailsFailed(response.message)
                            }
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: ScanBindContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}