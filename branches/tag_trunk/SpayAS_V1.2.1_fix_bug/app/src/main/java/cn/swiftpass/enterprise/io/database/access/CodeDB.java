package cn.swiftpass.enterprise.io.database.access;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.CodeModel;
import cn.swiftpass.enterprise.io.database.table.CodeTable;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-10-31
 * Time: 下午12:17
 * To change this template use File | Settings | File Templates.
 */
public class CodeDB   {

    private static Dao<CodeModel,Integer> codeDao;
    private CodeDB()
    {
        try
        {
            codeDao  = MainApplication.getContext().getHelper().getDao(CodeModel.class);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    private static CodeDB instance;
    public static CodeDB getInstance()
    {
        if(instance == null)
        {
            instance = new CodeDB();
        }
        return instance;
    }
    public void save(CodeModel codeModel)
    {
        try {
            codeDao.create(codeModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public CodeModel getCodeModel()
    {
        try {
            QueryBuilder<CodeModel,Integer> builder = codeDao.queryBuilder();
            builder.orderBy(CodeTable.COLUMN_ID,false);
            List<CodeModel> codes = codeDao.query(builder.prepare());
            if(codes != null && codes.size() > 0 ){
                return codes.get(0);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }
}
