package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkCustomerModel
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkProductModel

/**
 * @author lizheng.zhao
 * @date 2022/12/6
 *
 * @前尘隔海
 * @古屋不再
 */
class CustAndProdSelectContract {


    interface View : BaseView {

        fun getCustomerListSuccess(response: PaymentLinkCustomerModel?)

        fun getCustomerListFailed(error: Any?)


        fun getProductListSuccess(response: PaymentLinkProductModel?)

        fun getProductListFailed(error: Any?)

    }

    interface Presenter : BasePresenter<View> {

        fun getProductList(
            pageNumber: String?,
            goodsId: String?,
            goodsName: String?,
            goodsCode: String?
        )

        fun getCustomerList(
            pageNumber: String?,
            custName: String?,
            custMobile: String?,
            custEmail: String?
        )

    }


}