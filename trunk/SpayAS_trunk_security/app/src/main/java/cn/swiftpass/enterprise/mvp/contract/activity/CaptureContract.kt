package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.*
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/11/22
 *
 * @命运不是风
 * @来回吹
 * @命运是大地
 * @走到哪你都在命运中
 */
class CaptureContract {

    interface View : BaseView {

        fun masterCardNativePaySuccess(
            response: MasterCardInfo?,
            apiCode: String?
        )

        fun masterCardNativePayFailed(error: Any?)


        fun unifiedNativePaySuccess(
            response: QRcodeInfo?,
            isInstapay: Boolean,
            isInstapayQRPayment: Boolean,
            isRbInstapay: Boolean,
            bean: WalletListBean?,
            payType: String?
        )

        fun unifiedNativePayFailed(error: Any?)


        fun queryOrderDetailSuccess(response: Order)

        fun queryOrderDetailFailed(error: Any?)


        fun scanQueryOrderDetailSuccess(response: Order?)

        fun scanQueryOrderDetailFailed(error: Any?)


        fun queryCardPaymentOrderDetailsSuccess(response: Order?)

        fun queryCardPaymentOrderDetailsFailed(error: Any?)


        fun doCoverOrderSuccess(response: QRcodeInfo?)

        fun doCoverOrderFailed(error: Any?)

        fun unifiedPayReverseSuccess(response: Order?)

        fun unifiedPayReverseFailed(error: Any?)

        fun queryOrderByOrderNoSuccess(
            response: Order?,
            isMoreQuery: Boolean,
            isRevers: Boolean,
            orderNo: String?
        )

        fun queryOrderByOrderNoFailed(
            error: Any?,
            isMoreQuery: Boolean,
            orderNo: String?
        )
    }

    interface Presenter : BasePresenter<View> {

        fun queryOrderByOrderNo(
            orderNo: String?,
            isMoreQuery: Boolean,
            isRevers: Boolean,
            hideLoading: Boolean
        )


        fun unifiedPayReverse(orderNo: String?)


        fun doCoverOrder(
            code: String?,
            money: String?,
            payType: String?,
            vardOrders: List<WxCard>?,
            discountAmount: Long,
            outTradeNo: String?,
            mark: String?

        )


        fun queryCardPaymentOrderDetails(
            outTradeNo: String?,
            orderNoMch: String?
        )


        fun scanQueryOrderDetail(
            orderNo: String?,
            mchId: String?,
            scanType: Int
        )


        fun queryOrderDetail(
            orderNo: String?,
            mchId: String?,
            isMark: Boolean
        )

        fun unifiedNativePay(
            money: String?,
            payType: String?,
            traType: String?,
            adm: Long,
            outTradeNo: String?,
            note: String?,
            isInstapay: Boolean,
            isInstapayQRPayment: Boolean,
            isRbInstapay: Boolean,
            bean: WalletListBean?
        )

        fun masterCardNativePay(
            money: String?,
            apiCode: String?,
            attach: String?
        )


    }


}