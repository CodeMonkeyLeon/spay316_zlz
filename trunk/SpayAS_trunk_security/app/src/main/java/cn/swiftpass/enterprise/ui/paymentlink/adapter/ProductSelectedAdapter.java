package cn.swiftpass.enterprise.ui.paymentlink.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.example.common.sentry.SentryUtils;

import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkProduct;
import cn.swiftpass.enterprise.ui.paymentlink.widget.SlideItemView;
import cn.swiftpass.enterprise.ui.widget.BaseRecycleAdapter;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.KotlinUtils;

/**
 * Created by congwei.li on 2021/9/15.
 *
 * @Description: 创建订单页面，已选择的商品列表adapter
 */
public class ProductSelectedAdapter extends BaseRecycleAdapter<PaymentLinkProduct> {

    private Context mContext;
    private OnListItemClickListener onItemClickListener;
    private OnProductNumChange onProductNumChange;
    private SlideItemView openItem;

    public void setOnItemClickListener(OnListItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnProductNumChange(OnProductNumChange onProductNumChange) {
        this.onProductNumChange = onProductNumChange;
    }

    public ProductSelectedAdapter(Context context, List<PaymentLinkProduct> data) {
        super(data);
        mContext = context;
    }

    @Override
    protected void bindData(BaseViewHolder holder, int position) {
        PaymentLinkProduct data = datas.get(position);

        TextView desc1 = (TextView) holder.getView(R.id.tv_desc1);
        TextView desc2 = (TextView) holder.getView(R.id.tv_desc2);
        TextView desc3 = (TextView) holder.getView(R.id.tv_desc3);
        TextView desc4 = (TextView) holder.getView(R.id.tv_desc4);
        TextView desc5 = (TextView) holder.getView(R.id.tv_desc5);
        View line = (View) holder.getView(R.id.view_line);
        ImageView subNum = (ImageView) holder.getView(R.id.iv_sub_num);
        ImageView addNum = (ImageView) holder.getView(R.id.iv_add_num);

        ImageView imgIcon = (ImageView)holder.getView(R.id.id_img);


        Glide.with(mContext)
                .load(data.goodsPic)
                .error(R.drawable.icon_payment_link_default)
                .transform(new CenterCrop(), new RoundedCorners(KotlinUtils.INSTANCE.dp2px(4f, mContext)))
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imgIcon);

        desc1.setText(data.goodsName);
        if (!TextUtils.isEmpty(data.goodsCode)) {
            desc2.setText(mContext.getResources().getString(R.string.payment_link_add_product_phone_code)
                    + ": " + data.goodsCode);
        } else {
            desc2.setText("");
        }
        if (!TextUtils.isEmpty(data.goodsDesc)) {
            desc3.setText(mContext.getResources().getString(R.string.payment_link_add_product_description)
                    + ": " + data.goodsDesc);
        } else {
            desc3.setText("");
        }
        desc4.setText(MainApplication.getInstance().getFeeFh() + " " + DateUtil.formatMoneyUtils(data.goodsPrice));
        desc5.setText(data.goodsNum);
        if (position == getItemCount() - 1) {
            line.setVisibility(View.GONE);
        } else {
            line.setVisibility(View.VISIBLE);
        }

        checkNum(data, desc5, subNum, addNum);

        subNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    int num = Integer.parseInt(data.goodsNum);
                    if (num > 1) {
                        data.goodsNum = String.valueOf(num - 1);
                        desc5.setText(data.goodsNum);
                        checkNum(data, desc5, subNum, addNum);
                        if (onProductNumChange != null) {
                            onProductNumChange.onProductChange(data);
                        }
                    }
                } catch (Exception e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                }
            }
        });

        addNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    int num = Integer.parseInt(data.goodsNum);
                    if (num < 99) {
                        data.goodsNum = String.valueOf(num + 1);
                        desc5.setText(data.goodsNum);
                        checkNum(data, desc5, subNum, addNum);
                        if (onProductNumChange != null) {
                            onProductNumChange.onProductChange(data);
                        }
                    }
                } catch (Exception e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                }
            }
        });

        SlideItemView view = (SlideItemView) holder.itemView;

        view.setOnStateChangeListener(new SlideItemView.OnStateChangeListener() {
            @Override
            public void onFuncShow(SlideItemView itemView, boolean isShow) {
                if (isShow) {
                    if (null != openItem && itemView != openItem) {
                        openItem.reset();
                        openItem = itemView;
                    } else if (null == openItem) {
                        openItem = itemView;
                    }
                } else {
                    if (null != openItem && itemView == openItem) {
                        openItem = null;
                    }
                }
            }

            @Override
            public void onFuncClick() {
                if (null != openItem && openItem.isExpansion()) {
                    openItem.reset();
                    openItem = null;
                }
                if (onItemClickListener != null) {
                    onItemClickListener.onDeleteClick(position);
                }
            }

            @Override
            public void onContentClick() {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(position);
                }
            }
        });
    }

    private void checkNum(PaymentLinkProduct data, TextView desc5, ImageView subNum, ImageView addNum) {
        try {
            if (Integer.parseInt(data.goodsNum) <= 1) {
                subNum.setImageResource(R.drawable.icon_sub_gray);
                desc5.setTextColor(mContext.getResources().getColor(R.color.bg_text_new));
                addNum.setImageResource(R.drawable.icon_add_black);
            } else if (Integer.parseInt(data.goodsNum) < 99) {
                subNum.setImageResource(R.drawable.icon_sub_black);
                desc5.setTextColor(mContext.getResources().getColor(R.color.bg_text_new));
                addNum.setImageResource(R.drawable.icon_add_black);
            } else {
                subNum.setImageResource(R.drawable.icon_sub_black);
                desc5.setTextColor(mContext.getResources().getColor(R.color.bg_text_new));
                addNum.setImageResource(R.drawable.icon_add_gray);
            }
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }

    }

    @Override
    public int getLayoutId() {
        return R.layout.item_add_order_product_selected;
    }

    public interface OnProductNumChange {
        void onProductChange(PaymentLinkProduct data);
    }
}
