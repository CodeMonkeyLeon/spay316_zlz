package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.SearchContract
import cn.swiftpass.enterprise.ui.paymentlink.PaymentLinkOrderFilterDialog.Companion.TYPE_ALL
import cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkCustomer
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkCustomerModel
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkOrderModel
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkProduct
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkProductModel
import cn.swiftpass.enterprise.utils.JsonUtil
import com.example.common.entity.EmptyData
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/11/23
 *
 * @有人像家雀儿
 * @不愿意挪窝
 * @有人像候鸟
 * @永远在路上
 */
class SearchPresenter : SearchContract.Presenter {


    private var mView: SearchContract.View? = null


    override fun getCustomerList(
        pageNumber: String?,
        custName: String?,
        custMobile: String?,
        custEmail: String?
    ) {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
            AppClient.getCustomerList(
                "1",
                custName,
                custMobile,
                custEmail,
                pageNumber,
                PaymentLinkConstant.DEFAULT_PAGE_SIZE.toString(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<PaymentLinkCustomerModel>(
                    PaymentLinkCustomerModel()
                ) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.getCustomerListFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果orderRelateGoodsList值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"data\":\"\"",
                                "\"data\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                PaymentLinkCustomerModel::class.java
                            ) as PaymentLinkCustomerModel
                            view.getCustomerListSuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun getProductList(
        pageNumber: String?, goodsId: String?, goodsName: String?, goodsCode: String?
    ) {
        mView?.let { view ->
            view.showLoading(
                R.string.public_data_loading, ParamsConstants.COMMON_LOADING
            )
            AppClient.getProductList(
                "1",
                goodsId,
                goodsName,
                goodsCode,
                pageNumber,
                PaymentLinkConstant.DEFAULT_PAGE_SIZE.toString(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<PaymentLinkProductModel>(
                    PaymentLinkProductModel()
                ) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.getProductListFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果orderRelateGoodsList值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"data\":\"\"",
                                "\"data\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                PaymentLinkProductModel::class.java
                            ) as PaymentLinkProductModel
                            view.getProductListSuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun getOrderList(
        orderNo: String?,
        realPlatOrderNo: String?,
        custName: String?,
        orderRemark: String?,
        pageNumber: String?
    ) {
        mView?.let { view ->
            view.showLoading(
                R.string.public_data_loading, ParamsConstants.COMMON_LOADING
            )
            AppClient.getOrderList(
                "1",
                orderNo,
                realPlatOrderNo,
                custName,
                orderRemark,
                pageNumber,
                PaymentLinkConstant.DEFAULT_PAGE_SIZE.toString(),
                TYPE_ALL,
                System.currentTimeMillis().toString(),
                object :
                    CallBackUtil.CallBackCommonResponse<PaymentLinkOrderModel>(PaymentLinkOrderModel()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.getOrderListFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果dataList值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"data\":\"\"", "\"data\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message, PaymentLinkOrderModel::class.java
                            ) as PaymentLinkOrderModel
                            view.getOrderListSuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun deleteCustomer(
        customer: PaymentLinkCustomer?, position: Int
    ) {
        mView?.let { view ->
            view.showLoading(
                R.string.public_data_loading, ParamsConstants.COMMON_LOADING
            )
            var cId = ""
            customer?.let {
                cId = it.custId
            }
            AppClient.deleteOrQueryCustomer(
                "5",
                cId,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.deleteCustomerFailed(it.message)
                        }
                    }

                    override fun onResponse(response: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        response?.let {
                            view.deleteCustomerSuccess(it.message, position)
                        }
                    }
                }
            )

        }
    }

    override fun deleteProduct(
        product: PaymentLinkProduct?, position: Int
    ) {
        mView?.let { view ->
            view.showLoading(
                R.string.public_data_loading, ParamsConstants.COMMON_LOADING
            )
            var gId = ""
            product?.let {
                gId = it.goodsId
            }
            AppClient.deleteOrQueryProduct(
                "5",
                gId,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.deleteProductFailed(it.message)
                        }
                    }

                    override fun onResponse(response: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        response?.let {
                            view.deleteProductSuccess(it.message, position)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: SearchContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}