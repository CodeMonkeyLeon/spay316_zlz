package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.BindSpeakerList
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.MySpeakerListContract
import cn.swiftpass.enterprise.utils.JsonUtil
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @如果你是条船
 * @漂泊就是你的命运
 * @可别靠岸
 */
class MySpeakerListPresenter : MySpeakerListContract.Presenter {

    private var mView: MySpeakerListContract.View? = null


    override fun getSpeakerList() {
        mView?.let { view ->
            view.showLoading(R.string.public_loading, ParamsConstants.COMMON_LOADING)
            AppClient.getSpeakerList(
                MainApplication.getInstance().getMchId(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<BindSpeakerList>(BindSpeakerList()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.getSpeakerListFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val list = JsonUtil.jsonToBean(
                                response.message,
                                BindSpeakerList::class.java
                            ) as BindSpeakerList
                            view.getSpeakerListSuccess(list.data)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: MySpeakerListContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}