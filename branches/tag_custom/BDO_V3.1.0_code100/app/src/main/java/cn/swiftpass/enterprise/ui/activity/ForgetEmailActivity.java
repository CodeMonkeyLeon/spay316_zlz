package cn.swiftpass.enterprise.ui.activity;

import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.TextView;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;

/**
 * Created by congwei.li on 2021/10/29.
 *
 * @Description:
 */
public class ForgetEmailActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_email);

        MainApplication.listActivities.add(this);

        String org = getStringById(R.string.bdo_login_help_cc_desc1);
        String phoneNum = getStringById(R.string.bdo_login_help_phone_num);
        SpannableString spString = new SpannableString(org);
        spString.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),
                org.indexOf(phoneNum), org.indexOf(phoneNum) + phoneNum.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE); //粗体

        TextView desc = findViewById(R.id.tv_cc_desc1);
        desc.setText(spString);
        TextView back = findViewById(R.id.tv_back_to_login);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                redirectLoginActivity();
            }
        });
    }
}
