package cn.swiftpass.enterprise.ui.paymentlink.model;

import android.text.TextUtils;

import java.io.Serializable;

public class PaymentLinkProduct implements Serializable {
    public String goodsId = "";
    public String goodsName = "";
    public String goodsCode = "";
    public String goodsDesc = "";
    public String goodsPrice = ""; // 订单列表中的商品信息的价格
    public String goodsPic = "";
    public String feeType = "";
    public String merchantId = "";
    public String parentMerchant = "";
    public String userType = "";
    public String goodsNum = "0";


    public boolean isAllFilled() {
        return !TextUtils.isEmpty(goodsName)
//                && !TextUtils.isEmpty(goodsCode)
//                && !TextUtils.isEmpty(goodsDesc)
//                && !TextUtils.isEmpty(goodsPrice)
                ;
    }

    public boolean isEqual(PaymentLinkProduct product) {
        if (null == product) {
            return false;
        }
        return goodsName.equals(product.goodsName)
                && goodsCode.equals(product.goodsCode)
                && goodsPrice.equals(product.goodsPrice)
                && goodsDesc.equals(product.goodsDesc)
                && TextUtils.equals(goodsPic, product.goodsPic);
    }

    public void copyFrom(PaymentLinkProduct product) {
        if (null == product) {
            return;
        }
        goodsId = product.goodsId;
        goodsName = product.goodsName;
        goodsCode = product.goodsCode;
        goodsPrice = product.goodsPrice;
        goodsDesc = product.goodsDesc;
        goodsPic = product.goodsPic;
    }

    @Override
    public String toString() {
        return "PaymentLinkProduct{" +
                ", goodsId='" + goodsId + '\'' +
                ", goodsName='" + goodsName + '\'' +
                ", goodsCode='" + goodsCode + '\'' +
                ", goodsDesc='" + goodsDesc + '\'' +
                ", goodsPrice='" + goodsPrice + '\'' +
                ", feeType='" + feeType + '\'' +
                ", merchantId='" + merchantId + '\'' +
                ", parentMerchant='" + parentMerchant + '\'' +
                ", userType='" + userType + '\'' +
                ", goodsNum='" + goodsNum + '\'' +
                ", goodsPic='" + goodsPic + '\'' +
                '}';
    }
}
