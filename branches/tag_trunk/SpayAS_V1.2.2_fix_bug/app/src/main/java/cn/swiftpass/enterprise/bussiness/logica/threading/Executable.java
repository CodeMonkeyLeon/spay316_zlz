package cn.swiftpass.enterprise.bussiness.logica.threading;

public abstract class Executable<T>
{
	public static interface ProgressChangedListener
	{
		void onProgressChanged(int progress); 
	}
	private ProgressChangedListener onProgressChangedListener;
	public abstract T execute() throws Exception;
	
	public void updateProgress(int progress)
	{
		if(onProgressChangedListener!=null)
		{
			onProgressChangedListener.onProgressChanged(progress);
		}
	}
	
	public void setOnProgressChangedListener(
			ProgressChangedListener onProgressChangedListener)
	{
		this.onProgressChangedListener = onProgressChangedListener;
	}
}
