package cn.swiftpass.enterprise.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VerifyUtil {

    //    public static boolean verifyMobilePhone(String phoneNum)
    //    {
    //        Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0-9])|(147))\\d{8}$");
    //        Matcher m = p.matcher(phoneNum);
    //        boolean flag = m.matches();
    //        return flag;
    //    }
    //    
    //    public static boolean verifyIdCard(String idCard)
    //    {
    //        // 验证省份证有效性
    //        Pattern idNumPattern = Pattern.compile("(\\d{14}[0-9a-zA-Z])|(\\d{17}[0-9a-zA-Z])");
    //        Matcher m = idNumPattern.matcher(idCard);
    //        boolean flag = m.matches();
    //        return flag;
    //    }

    public static boolean verifyValid(String num, int flag) {

        String pattern = "";
        switch (flag) {
            case GlobalConstant.STR:
                pattern = "[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]";
                break;
            case GlobalConstant.IDCARD: //身份证验证
                pattern = "(\\d{14}[0-9a-zA-Z])|(\\d{17}[0-9a-zA-Z])";
                break;
            case GlobalConstant.MOBILEPHOE: //移动电话
                pattern = "^((13[0-9])|(15[^4,\\D])|(18[0-9])|(147)|(17[0-9]))\\d{8}$";
                break;
            case GlobalConstant.TELPHONE: //固定电话
                pattern = "(^(0\\d{2}\\d{8})$)|(^(0\\d{3}\\d{7})$)|(^(0\\d{2}\\d{8}\\d+)$)|(^(0\\d{3}\\d{7}\\d+)$)";
                break;
            case GlobalConstant.BANKNUM: //银行卡号
                pattern = "\\d{19}";
                break;
            case GlobalConstant.EMAIL: //银行卡号
                pattern = "^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$";
                break;
            case GlobalConstant.CHINA:
                pattern = "^[u4E00-u9FA5]+$";
                break;
        }
        Pattern idNumPattern = Pattern.compile(pattern);
        Matcher m = idNumPattern.matcher(num);
        return m.matches();

    }

    /**
     * 是否包含数字
     *
     * @param str
     * @return
     */
    public static boolean isContainNum(String str) {
        Pattern p = Pattern.compile(".*\\d+.*");
        Matcher m = p.matcher(str);
        return m.matches();
    }

    /**
     * 是否包含大写字母
     *
     * @param str
     * @return
     */
    public static boolean isContainUppercase(String str) {
        String regex = ".*[A-Z]+.*";
        Matcher m = Pattern.compile(regex).matcher(str);
        return m.matches();
    }

    /**
     * 是否包含小写字母
     *
     * @param str
     * @return
     */
    public static boolean isContainLowercase(String str) {
        String regex = ".*[a-z]+.*";
        Matcher m = Pattern.compile(regex).matcher(str);
        return m.matches();
    }

    /**
     * 是否包含特殊字符
     *
     * @param str
     * @return
     */
    public static boolean isContainSpecialChar(String str) {
        String regEx = "[ _`~!@#$%^&*()+=|{}':;',.<>/?~！@#￥%……&*——+|{}‘’]";
        Matcher m = Pattern.compile(regEx).matcher(str);
        return m.find();
    }

    /**
     * 是否包含特殊字符
     *
     * @param str
     * @return
     */
    public static boolean isContainAll(String str) {
        return isContainNum(str) && isContainUppercase(str) && isContainLowercase(str)
                && isContainSpecialChar(str);
    }
}
