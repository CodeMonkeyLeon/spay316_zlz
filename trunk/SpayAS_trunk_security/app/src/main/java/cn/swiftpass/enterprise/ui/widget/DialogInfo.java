/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.WelcomeActivity;

/**
 * 弹出提示框
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class DialogInfo extends Dialog {

    private Context context;

    private TextView title;

    private TextView content;

    private TextView btnOk;

    private TextView btnCancel;

    private TextView tv_notice;
    private TextView tv_mername;
    private TextView tv_merId;
    private TextView tv_deskname;
    private TextView tv_qrid;

    private ViewGroup mRootView;

    private DialogInfo.HandleBtn handleBtn;

    private DialogInfo.HandleBtnCancle cancleBtn;

    private View line_img, line_img_bt;

    // 更多
    public static final int FLAG = 0;

    // 最终提交
    public static final int SUBMIT = 1;

    // 注册成功
    public static final int REGISTFLAG = 2;

    // 微信支付 是否授权激活
    public static final int EXITAUTH = 3;

    // 微信支付 授权激活 重新登录
    public static final int EXITAUTHLOGIN = 4;

    //提交商户信息
    public static final int SUBMITSHOPINFO = 5;

    //判断网络连接状态
    public static final int NETWORKSTATUE = 8;

    //优惠卷列表，长按提示删除提示框
    public static final int SUBMIT_DEL_COUPON_INFO = 6;

    //优惠卷发布
    public static final int SUBMIT_COUPON_INFO = 7;

    public static final int SUBMIT_SHOP = 9;

    public static final int SCAN_PAY = 10;

    public static final int VARD = 11;

    public static final int UNIFED_DIALOG = 12;

    private OnItemLongDelListener mDelListener;

    private int position;

    private OnSubmitCouponListener mOnSubmitCouponListener;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public DialogInfo(Context context, String titleStr, String contentStr, String btnOkStr,
                      String btnCancel, int flagMore, DialogInfo.HandleBtn handleBtn,
                      DialogInfo.HandleBtnCancle cancelBtn) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_info, null);
        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);
        this.context = context;
        this.handleBtn = handleBtn;
        this.cancleBtn = cancelBtn;
        initView(titleStr, contentStr, btnOkStr, btnCancel, flagMore);
        setLinster(flagMore);
    }

    public DialogInfo(boolean isAliPay, Context context, String titleStr, String MerName, String MerId,
                      String deskName, String QrId, String btnOkStr, int flagMore,
                      DialogInfo.HandleBtn handleBtn, DialogInfo.HandleBtnCancle cancleBtn) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_code_info, null);
        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);
        this.handleBtn = handleBtn;
        this.cancleBtn = cancleBtn;
        initView(isAliPay, titleStr, MerName, MerId, deskName, QrId, btnOkStr, null, flagMore);
        setLinster(flagMore);
    }

    public DialogInfo(Context context, String titleStr, String contentStr, String btnOkStr,
                      int flagMore, DialogInfo.HandleBtn handleBtn,
                      DialogInfo.HandleBtnCancle cancleBtn) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_info, null);
        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);

        this.context = context;
        this.handleBtn = handleBtn;
        this.cancleBtn = cancleBtn;

        initView(titleStr, contentStr, btnOkStr, null, flagMore);

        setLinster(flagMore);

    }

    public void setBtnOkText(String string) {
        if (btnOk != null) {
            btnOk.setText(string);
        }
    }

    public void setBtnOkTextColor(int values) {
        if (btnOk != null) {
            btnOk.setTextColor(values);
        }
    }

    public void setBtnCancelTextColor(int values) {
        if (btnCancel != null) {
            btnCancel.setTextColor(values);
        }
    }

    public void setBtnCancelTextBold(){
        if (btnCancel!=null){
            btnCancel.setTypeface(null, Typeface.BOLD);
        }
    }

    public void setContentGravity(int ver) {
        if (null != content) {
            content.setGravity(ver);
        }
    }

    public void setContentColor(int color) {
        if (null != content) {
            content.setTextColor(color);
        }
    }

    public void setMessage(String msg) {
        this.content.setText(msg);
    }

    public void setmDelListener(OnItemLongDelListener mDelListener, int position) {
        this.mDelListener = mDelListener;
        this.position = position;
    }

    public void setmOnSubmitCouponListener(OnSubmitCouponListener mOnSubmitCouponListener) {
        this.mOnSubmitCouponListener = mOnSubmitCouponListener;
    }

    /**
     * 设置监听
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void setLinster(final int flagMore) {

        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
                switch (flagMore) {
                    case SUBMIT_COUPON_INFO:
                        mOnSubmitCouponListener.onSubmitCouponListenerCancel();
                        break;
                    case EXITAUTH:
                        cancleBtn.handleCancleBtn();
                        break;
                    case UNIFED_DIALOG:
                        if (handleBtn != null) {
                            handleBtn.handleCancelBtn();
                        }
                        break;
                }
            }

        });
        btnOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
                switch (flagMore) {

                    case UNIFED_DIALOG:
                        handleBtn.handleOkBtn();
                        cancel();
                        break;
                    // 更多 资料未完善
                    //                    case FLAG:
                    //                        //                        showPage(ShopkeeperActivity.class);
                    //                        
                    //                        break;
                    // 注册欢迎提示
                    case REGISTFLAG:
                        handleBtn.handleOkBtn();
                        cancel();
                        break;

                    case SUBMIT:
                        Toast.makeText(getContext(), "提交成功!", Toast.LENGTH_SHORT).show();
                        break;
                    // 是否授权弹出框
                    case EXITAUTH: // 调转到服务热线
                        handleBtn.handleOkBtn();
                        cancel();
                        break;

                    // 是否授权弹出框
                    case SUBMIT_SHOP: // 调转到服务热线
                        handleBtn.handleOkBtn();
                        cancel();
                        break;

                    case SUBMITSHOPINFO:
                    case NETWORKSTATUE:
                    case FLAG:
                        cancel();
                        ((Activity) context).finish();
                        break;

                    case EXITAUTHLOGIN: // 激活从新登录
                        Intent login_intent = new Intent(context, WelcomeActivity.class);
                        context.startActivity(login_intent);
                        Activity activity = (Activity) context;
                        activity.finish();
                        break;
                    case SUBMIT_DEL_COUPON_INFO:
                        mDelListener.onItemLongDelMessage(position);
                        break;
                    case SUBMIT_COUPON_INFO:
                        mOnSubmitCouponListener.onSubmitCouponListenerOk();
                        break;
                    case VARD:
                        handleBtn.handleOkBtn();
                        break;
                }
            }
        });
    }

    /**
     * 初始化
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initView(boolean isAlipay, String titleStr, String MerName, String MerId,
                          String deskName, String QrId, String btnOkStr, String btnCancelStr, int flag) {
        title = (TextView) findViewById(R.id.title);

        btnOk = (TextView) findViewById(R.id.btnOk);

        btnCancel = (TextView) findViewById(R.id.btnCancel);

        line_img = (View) findViewById(R.id.line_img);

        line_img_bt = (View) findViewById(R.id.line_img_bt);
        tv_notice = findViewById(R.id.tv_notice);
        tv_mername = findViewById(R.id.tv_mername);
        tv_merId = findViewById(R.id.tv_merid);
        tv_deskname = findViewById(R.id.tv_deskname);
        tv_qrid = findViewById(R.id.tv_qrid);
        if (!isAlipay) {
            tv_notice.setVisibility(View.GONE);
        } else {
            tv_notice.setVisibility(View.VISIBLE);
        }
        tv_mername.setText(MerName);
        tv_merId.setText(MerId);
        tv_deskname.setText(deskName);
        tv_qrid.setText(QrId);
        switch (flag) {
            case UNIFED_DIALOG:
                line_img.setVisibility(View.VISIBLE);
                btnCancel.setVisibility(View.VISIBLE);
                break;

            case REGISTFLAG: // 注册成功
                btnCancel.setVisibility(View.GONE);

                title.setTextColor(context.getResources().getColor(R.color.prompt_title));

                line_img.setVisibility(View.GONE);

                break;
            case EXITAUTHLOGIN: // 激活从新登录
                btnOk.setTextColor(Color.BLUE);
                break;

            case EXITAUTH:
                if (TextUtils.isEmpty(btnCancelStr)) {
                    btnCancel.setText(R.string.tx_go_on);
                } else {
                    btnCancel.setText(btnCancelStr);
                }
                btnCancel.setVisibility(View.VISIBLE);
                line_img.setVisibility(View.VISIBLE);
                //                btnOk.setTextColor(Color.BLUE);
                break;
            case FLAG:
                btnCancel.setVisibility(View.GONE);
                line_img.setVisibility(View.GONE);
                break;
            default:
                break;
        }

        title.setText(titleStr);
        btnOk.setText(btnOkStr);
        if (flag == SCAN_PAY) {
            btnOk.setVisibility(View.GONE);
            line_img.setVisibility(View.GONE);
            line_img_bt.setVisibility(View.GONE);
        }
    }

    /**
     * 初始化
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initView(String titleStr, String contentStr, String btnOkStr, String btnCancelStr, int flag) {
        title = (TextView) findViewById(R.id.title);

        content = (TextView) findViewById(R.id.content);

        btnOk = (TextView) findViewById(R.id.btnOk);

        btnCancel = (TextView) findViewById(R.id.btnCancel);

        line_img = (View) findViewById(R.id.line_img);

        line_img_bt = (View) findViewById(R.id.line_img_bt);
        switch (flag) {
            case UNIFED_DIALOG:
                line_img.setVisibility(View.VISIBLE);
                btnCancel.setVisibility(View.VISIBLE);
                break;

            case REGISTFLAG: // 注册成功
                btnCancel.setVisibility(View.GONE);

                title.setTextColor(context.getResources().getColor(R.color.prompt_title));

                content.setTextColor(context.getResources().getColor(R.color.content_title));
                line_img.setVisibility(View.GONE);

                break;
            case EXITAUTHLOGIN: // 激活从新登录
                btnOk.setTextColor(Color.BLUE);
                break;

            case EXITAUTH:
                if (TextUtils.isEmpty(btnCancelStr)) {
                    btnCancel.setText(R.string.tx_go_on);
                } else {
                    btnCancel.setText(btnCancelStr);
                }
                btnCancel.setVisibility(View.VISIBLE);
                line_img.setVisibility(View.VISIBLE);
                //                btnOk.setTextColor(Color.BLUE);
                break;
            case FLAG:
                btnCancel.setVisibility(View.GONE);
                line_img.setVisibility(View.GONE);
                break;
            default:
                break;
        }

        if (!TextUtils.isEmpty(titleStr)) {
            title.setText(titleStr);
            findViewById(R.id.ll_title).setVisibility(View.VISIBLE);
        }
        content.setText(contentStr);
        btnOk.setText(btnOkStr);
        if (flag == SCAN_PAY) {
            btnOk.setVisibility(View.GONE);
            line_img.setVisibility(View.GONE);
            line_img_bt.setVisibility(View.GONE);
        }
    }

    public void showPage(Class clazz) {
        Intent intent = new Intent(context, clazz);
        context.startActivity(intent);
    }

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    public interface HandleBtn {
        void handleOkBtn();

        void handleCancelBtn();
    }

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    public interface HandleBtnCancle {
        void handleCancleBtn();
    }

    /***
     *
     * 长按删除优惠
     *
     * @author wang_lin
     * @version [2.1.0, 2014-4-23]
     */
    public interface OnItemLongDelListener {
        public void onItemLongDelMessage(int position);
    }

    /***
     *
     * 发布和修改优惠价提示
     * @author wang_lin
     * @version [2.1.0, 2014-4-23]
     */
    public interface OnSubmitCouponListener {
        public void onSubmitCouponListenerOk();

        public void onSubmitCouponListenerCancel();
    }
}
