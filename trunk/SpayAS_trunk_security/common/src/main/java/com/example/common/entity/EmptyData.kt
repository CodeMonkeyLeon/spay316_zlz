package com.example.common.entity

import java.io.Serializable

class EmptyData : Serializable {

    var message: String? = ""

    constructor() {
        message = "空数据"
    }

}