package cn.swiftpass.enterprise.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.util.Timer;
import java.util.TimerTask;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.widget.TitleBar;

/**
 * Created by aijingya on 2019/11/15.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: 固码输入标签的Activity(用一句话描述该文件做什么)
 * @date 2019/11/15.17:54.
 */
public class StaticCodeLabelActivity extends TemplateActivity{
    private static final String TAG = StaticCodeLabelActivity.class.getSimpleName();
    private EditText et_static_code_mark;
    private String mark ="";
    private Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_code_label);
        mContext = StaticCodeLabelActivity.this;
        initView();
    }


    private void initView(){
        et_static_code_mark = getViewById(R.id.et_static_code_mark);
        mark = getIntent().getStringExtra("staticCodeLabel");
        if (!TextUtils.isEmpty(mark)){
            et_static_code_mark.setText(mark);
        }else{
            et_static_code_mark.setText("");
        }
        et_static_code_mark.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mark = s.toString();
                if (!TextUtils.isEmpty(mark) && 20 == mark.length()){
                    closeKeyboard(et_static_code_mark);
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        showKeyboard(et_static_code_mark);
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle("");
        titleBar.setLeftButtonVisible(false);
        titleBar.setRightButtonVisible(false);
        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.data_select_done));
        titleBar.setBackgroundColor(getResources().getColor(R.color.app_drawer_title));

        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButLayClick() {
                mark = et_static_code_mark.getText().toString();
                closeKeyboard(et_static_code_mark);

                Intent it =new Intent();
                it.putExtra("static_code_label", mark);
                setResult(RESULT_OK,it);
                finish();
            }
        });
    }


    /**
     * auto show InputKeyboard
     */
    private void showKeyboard(final EditText editText){
        et_static_code_mark.setFocusable(true);
        et_static_code_mark.setFocusableInTouchMode(true);
        et_static_code_mark.requestFocus();
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.showSoftInput(editText, 0);
            }
        }, 500);

    }


    /**
     * auto close InputKeyboard
     */
    private void closeKeyboard(EditText editText){
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }



}
