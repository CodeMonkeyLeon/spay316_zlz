package com.cxy.networklibrary.http.support.interceptor;

import android.content.Context;
import android.text.TextUtils;

import com.cxy.networklibrary.util.SharedPreferencesUtil;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 创建人：caoxiaoya
 * 时间：2018/6/26
 * 描述：非首次请求：
 * 备注：
 */
public class AddCookiesInterceptor implements Interceptor {


    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();

        String login_skey = SharedPreferencesUtil.getInstance().getString("login_skey", "");
        String login_sauthid = SharedPreferencesUtil.getInstance().getString("login_sauthid", "");

        if (!TextUtils.isEmpty(login_skey)&&!TextUtils.isEmpty(login_sauthid)){
            builder.addHeader("SKEY", login_skey);
            builder.addHeader("SAUTHID", login_sauthid);
            builder.addHeader("ELETYPE", "terminal");
        }

        return chain.proceed(builder.build());
    }
}
