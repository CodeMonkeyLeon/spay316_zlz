package cn.swiftpass.enterprise.ui.paymentlink;

import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.ADD_ORDER_SHARE_EMAIL;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.ADD_ORDER_SHARE_SMS;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.CREATE_ORDER_SUCCESS;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.DATA_TAG;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.MANAGE_EDIT_ORDER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_CREATE_ORDER_SUCCESS;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.TYPE_TAG;
import static cn.swiftpass.enterprise.ui.paymentlink.entity.OrderShareEntity.TYPE_EMAIL;
import static cn.swiftpass.enterprise.ui.paymentlink.entity.OrderShareEntity.TYPE_LINK;
import static cn.swiftpass.enterprise.ui.paymentlink.entity.OrderShareEntity.TYPE_MESSAGE;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.OrderDetailsBaseContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.OrderDetailsBasePresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.paymentlink.adapter.OrderDetailOtherFeeListAdapter;
import cn.swiftpass.enterprise.ui.paymentlink.adapter.OrderDetailProductListAdapter;
import cn.swiftpass.enterprise.ui.paymentlink.interfaces.OnPaymentLinkShareTypeClickListener;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkOrder;
import cn.swiftpass.enterprise.ui.paymentlink.view.BreakTextView;
import cn.swiftpass.enterprise.ui.widget.SpayTitleView;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogUtils;
import cn.swiftpass.enterprise.utils.KotlinUtils;
import cn.swiftpass.enterprise.utils.ToastDialogUtils;

/**
 * Created by congwei.li on 2021/9/16.
 *
 * @Description: payment link详情页面，创建订单成功页面
 */
public class OrderDetailsActivity extends BaseActivity<OrderDetailsBaseContract.Presenter> implements OrderDetailsBaseContract.View {

    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.tv_pl_order_total_amount)
    TextView tvTotalAmount;
    @BindView(R.id.spay_title_view)
    SpayTitleView titleView;
    @BindView(R.id.tv_pl_order_status)
    TextView tvOrderStatus;
    @BindView(R.id.tv_pl_order_id)
    TextView tvOrderId;
    @BindView(R.id.ll_pl_order_id)
    LinearLayout llOrderId;
    @BindView(R.id.tv_pl_order_customer_name)
    TextView tvCustomerName;
    @BindView(R.id.ll_pl_order_customer_name)
    LinearLayout llCustomerName;

    @BindView(R.id.ll_pl_customer_name_line)
    View mLineCustomerName;

    @BindView(R.id.tv_pl_order_create_time)
    TextView tvCreateTime;
    @BindView(R.id.ll_pl_order_create_time)
    LinearLayout llOrderCreateTime;
    @BindView(R.id.tv_pl_order_remark)
    TextView tvOrderRemark;
    @BindView(R.id.ll_pl_order_remark)
    LinearLayout llOrderRemark;
    @BindView(R.id.ll_details_optional)
    LinearLayout llDetailsOptional;
    @BindView(R.id.iv_show_detail)
    ImageView ivShowDetail;
    @BindView(R.id.ll_details_content)
    LinearLayout llDetailsContent;
    @BindView(R.id.ll_pl_order_merchant_id)
    LinearLayout llMerchantId;
    @BindView(R.id.tv_pl_order_merchant_id)
    TextView tvMerchantId;
    @BindView(R.id.ll_pl_order_merchant_id_line)
    View vMerchantIdLine;
    @BindView(R.id.ll_pl_order_merchant_name)
    LinearLayout llMerchantName;
    @BindView(R.id.tv_pl_order_merchant_name)
    TextView tvMerchantName;
    @BindView(R.id.ll_pl_order_merchant_name_line)
    View vMerchantNameLine;
    @BindView(R.id.ll_pl_order_platform_id)
    LinearLayout llPlatformId;
    @BindView(R.id.tv_pl_order_platform_id)
    TextView tvPlatformId;
    @BindView(R.id.ll_pl_order_platform_id_line)
    View vPlatformIdLine;
    @BindView(R.id.ll_pl_order_link)
    ConstraintLayout llLink;
    @BindView(R.id.tv_pl_order_link)
    BreakTextView tvLink;

    @BindView(R.id.id_ll_link_copy)
    LinearLayout mLlCopyLink;

    @BindView(R.id.ll_pl_order_link_line)
    View vLink;
    @BindView(R.id.ll_pl_order_photo)
    LinearLayout llViewPhoto;
    @BindView(R.id.iv_pl_order_view_photo)
    ImageView tvViewPhoto;
    @BindView(R.id.ll_pl_order_photo_line)
    View vViewPhoto;
    @BindView(R.id.ll_pl_order_payment_time)
    LinearLayout llPaymentTime;
    @BindView(R.id.tv_pl_order_payment_time)
    TextView tvPaymentTime;
    @BindView(R.id.ll_pl_order_payment_time_line)
    View vPaymentTime;
    @BindView(R.id.ll_pl_order_customer_phone)
    LinearLayout llCustomerPhone;
    @BindView(R.id.tv_pl_order_customer_phone)
    TextView tvCustomerPhone;
    @BindView(R.id.ll_pl_order_customer_phone_line)
    View vCustomerPhone;
    @BindView(R.id.ll_pl_order_customer_email)
    LinearLayout llCustomerEmail;
    @BindView(R.id.tv_pl_order_customer_email)
    TextView tvCustomerEmail;
    @BindView(R.id.ll_pl_order_customer_email_line)
    View vCustomerEmail;
    @BindView(R.id.rv_pl_order_product)
    RecyclerView rvProduct;
    @BindView(R.id.rv_pl_order_other_fee)
    RecyclerView rvOtherFee;
    @BindView(R.id.tv_pl_order_finish)
    TextView tvFinish;
    @BindView(R.id.tv_pl_order_refresh)
    TextView tvRefresh;
    @BindView(R.id.tv_edit_order)
    TextView tvEditOrder;
    PaymentLinkOrder order;
    int type;

    public static void startActivity(Context mContext, PaymentLinkOrder order, int type) {
        Intent intent = new Intent(mContext, OrderDetailsActivity.class);
        intent.putExtra(TYPE_TAG, type);
        intent.putExtra(DATA_TAG, order);
        mContext.startActivity(intent);
    }

    public static void startActivityForResult(Activity mContext, PaymentLinkOrder order, int type) {
        Intent intent = new Intent(mContext, OrderDetailsActivity.class);
        intent.putExtra(TYPE_TAG, type);
        intent.putExtra(DATA_TAG, order);
        mContext.startActivityForResult(intent, type);
    }

    @Override
    protected OrderDetailsBaseContract.Presenter createPresenter() {
        return new OrderDetailsBasePresenter();
    }

    @Override
    protected boolean isWindowFeatureNoTitle() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        KotlinUtils.INSTANCE.isReStartApp(savedInstanceState, this);
        setContentView(R.layout.activity_payment_link_order_details);
        ButterKnife.bind(this);

        order = (PaymentLinkOrder) getIntent().getSerializableExtra(DATA_TAG);
        type = getIntent().getIntExtra(TYPE_TAG, MANAGE_EDIT_ORDER);

        titleView.addLeftImageView(R.drawable.icon_general_top_back_default, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                OrderDetailsActivity.this.setResult(RESULT_CODE_CREATE_ORDER_SUCCESS, i);
                if (!OrderDetailsActivity.this.isFinishing()) {
                    OrderDetailsActivity.this.finish();
                }
            }
        });
        titleView.addMiddleTextView(getResources().getString(type == CREATE_ORDER_SUCCESS ? R.string.payment_link_add_order : R.string.pl_order_detail_title));
        titleView.addRightTextView(getString(R.string.payment_link_add_order_share), new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                PaymentLinkOrderShareDialog linkOrderShareDialog = PaymentLinkOrderShareDialog.Companion.getInstance();

                linkOrderShareDialog.setOnItemClickListener(new OnPaymentLinkShareTypeClickListener() {
                    @Override
                    public void onChoose(int type) {
                        if (type == TYPE_EMAIL) {
                            //发送邮件
                            ShareOrderActivity.startActivity(OrderDetailsActivity.this, order, ADD_ORDER_SHARE_EMAIL);
                        } else if (type == TYPE_MESSAGE) {
                            //发送短信
                            ShareOrderActivity.startActivity(OrderDetailsActivity.this, order, ADD_ORDER_SHARE_SMS);
                        } else if (type == TYPE_LINK) {
                            //复制链接
                            copyLink();
                        } else {
                            //分享至其他app
                            Intent textIntent = new Intent(Intent.ACTION_SEND);
                            textIntent.setType("text/plain");
                            textIntent.putExtra(Intent.EXTRA_TEXT, order.payUrl);
                            startActivity(Intent.createChooser(textIntent, getString(R.string.payment_link_add_order_share)));
                        }
                    }
                });
                linkOrderShareDialog.show(getSupportFragmentManager(), "PaymentLinkOrderShareDialog");
            }
        });
        llDetailsContent.setVisibility(View.GONE);
        initData();
    }


    private void copyLink() {
        AppHelper.setClipboardText(OrderDetailsActivity.this, order.payUrl);
        ToastDialogUtils.toastDialog(OrderDetailsActivity.this, getString(R.string.payment_link_share_copied), null);
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        initData();
    }

    private void initView() {
        if (order == null) {
            return;
        }
        tvTotalAmount.setText(MainApplication.getInstance().getFeeFh() + " " + DateUtil.formatMoneyUtils(order.totalFee));
        tvOrderStatus.setText(order.getOrderStatusText(this));
        tvOrderStatus.setTextColor(order.getOrderStatusTextColor(this));
        tvFinish.setVisibility(type == CREATE_ORDER_SUCCESS ? View.VISIBLE : View.GONE);
        tvFinish.setEnabled(true);
        tvFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                OrderDetailsActivity.this.setResult(RESULT_CODE_CREATE_ORDER_SUCCESS, i);
                if (!OrderDetailsActivity.this.isFinishing()) {
                    OrderDetailsActivity.this.finish();
                }
            }
        });
        tvRefresh.setVisibility((type != CREATE_ORDER_SUCCESS && order.isUpdateAble()) ? View.VISIBLE : View.GONE);
        tvRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateOrderStatus();
            }
        });

        tvEditOrder.setVisibility(order.isEditAble() ? View.VISIBLE : View.GONE);
        tvEditOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OrderCreateActivity.startActivity(OrderDetailsActivity.this, order, false);
            }
        });

        llOrderId.setVisibility(TextUtils.isEmpty(order.orderNo) ? View.GONE : View.VISIBLE);
        if (!TextUtils.isEmpty(order.orderNo)) {
            tvOrderId.setText(order.orderNo);
        }
        llCustomerName.setVisibility(TextUtils.isEmpty(order.custName) ? View.GONE : View.VISIBLE);
        mLineCustomerName.setVisibility(TextUtils.isEmpty(order.custName) ? View.GONE : View.VISIBLE);
        if (!TextUtils.isEmpty(order.custName)) {
            tvCustomerName.setText(order.custName);
        }
        llOrderCreateTime.setVisibility(TextUtils.isEmpty(order.createTime) ? View.GONE : View.VISIBLE);
        if (!TextUtils.isEmpty(order.createTime)) {
            tvCreateTime.setText(order.createTime);
        }
        llOrderRemark.setVisibility(TextUtils.isEmpty(order.orderRemark) ? View.GONE : View.VISIBLE);
        if (!TextUtils.isEmpty(order.orderRemark)) {
            tvOrderRemark.setText(order.orderRemark);
        }

        ivShowDetail.setRotation(llDetailsContent.getVisibility() == View.VISIBLE ? 270f : 90f);
        llDetailsOptional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llDetailsContent.setVisibility(llDetailsContent.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                ivShowDetail.setRotation(llDetailsContent.getVisibility() == View.VISIBLE ? 270f : 90f);
            }
        });

        llMerchantId.setVisibility(TextUtils.isEmpty(order.merchantId) ? View.GONE : View.VISIBLE);
        vMerchantIdLine.setVisibility(TextUtils.isEmpty(order.merchantId) ? View.GONE : View.VISIBLE);
        if (!TextUtils.isEmpty(order.merchantId)) {
            tvMerchantId.setText(order.merchantId);
        }

        llMerchantName.setVisibility(TextUtils.isEmpty(order.merchantName) ? View.GONE : View.VISIBLE);
        vMerchantNameLine.setVisibility(TextUtils.isEmpty(order.merchantName) ? View.GONE : View.VISIBLE);
        if (!TextUtils.isEmpty(order.merchantName)) {
            tvMerchantName.setText(order.merchantName);
        }

        llPlatformId.setVisibility(TextUtils.isEmpty(order.realPlatOrderNo) ? View.GONE : View.VISIBLE);
        vPlatformIdLine.setVisibility(TextUtils.isEmpty(order.realPlatOrderNo) ? View.GONE : View.VISIBLE);
        if (!TextUtils.isEmpty(order.realPlatOrderNo)) {
            tvPlatformId.setText(order.realPlatOrderNo);
        }


        LinearLayout.LayoutParams customerNameLayoutParams = (LinearLayout.LayoutParams) llCustomerName.getLayoutParams();
        if (!TextUtils.isEmpty(order.realPlatOrderNo)) {
            //展示platform order id时, customerName距离顶部16dp
            customerNameLayoutParams.topMargin = KotlinUtils.INSTANCE.dp2px(16f, this);
        } else {
            //不展示platform order id时, customerName距离顶部0dp
            customerNameLayoutParams.topMargin = 0;
        }
        llCustomerName.setLayoutParams(customerNameLayoutParams);


        llLink.setVisibility(TextUtils.isEmpty(order.payUrl) ? View.GONE : View.VISIBLE);
        vLink.setVisibility(TextUtils.isEmpty(order.payUrl) ? View.GONE : View.VISIBLE);
        if (!TextUtils.isEmpty(order.payUrl)) {
            tvLink.setText(order.payUrl);
        }

        mLlCopyLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyLink();
            }
        });

        llViewPhoto.setVisibility(TextUtils.isEmpty(order.picUrl) ? View.GONE : View.VISIBLE);
        vViewPhoto.setVisibility(TextUtils.isEmpty(order.picUrl) ? View.GONE : View.VISIBLE);
        if (!TextUtils.isEmpty(order.picUrl)) {
            Glide.with(this).load(order.picUrl).diskCacheStrategy(DiskCacheStrategy.NONE).into(tvViewPhoto);
        }

        llPaymentTime.setVisibility(TextUtils.isEmpty(order.tradeTime) ? View.GONE : View.VISIBLE);
        vPaymentTime.setVisibility(TextUtils.isEmpty(order.tradeTime) ? View.GONE : View.VISIBLE);
        if (!TextUtils.isEmpty(order.tradeTime)) {
            tvPaymentTime.setText(order.tradeTime);
        }

        llCustomerPhone.setVisibility(TextUtils.isEmpty(order.custMobile) ? View.GONE : View.VISIBLE);
        vCustomerPhone.setVisibility(TextUtils.isEmpty(order.custMobile) ? View.GONE : View.VISIBLE);
        if (!TextUtils.isEmpty(order.custMobile)) {
            tvCustomerPhone.setText(order.custMobile);
        }

        llCustomerEmail.setVisibility(TextUtils.isEmpty(order.custEmail) ? View.GONE : View.VISIBLE);
        vCustomerEmail.setVisibility(TextUtils.isEmpty(order.custEmail) ? View.GONE : View.VISIBLE);
        if (!TextUtils.isEmpty(order.custEmail)) {
            tvCustomerEmail.setText(order.custEmail);
        }
        rvProduct.setVisibility(View.GONE);
        if (null != order.orderRelateGoodsList && order.orderRelateGoodsList.size() > 0) {
            rvProduct.setLayoutManager(new LinearLayoutManager(OrderDetailsActivity.this, LinearLayoutManager.VERTICAL, false));
            OrderDetailProductListAdapter adapter = new OrderDetailProductListAdapter(this, order.orderRelateGoodsList);
            rvProduct.setAdapter(adapter);
            rvProduct.setVisibility(View.VISIBLE);
        }
        rvOtherFee.setVisibility(View.GONE);
        if (null != order.orderCostList && order.orderCostList.size() > 0) {
            rvOtherFee.setLayoutManager(new LinearLayoutManager(OrderDetailsActivity.this, LinearLayoutManager.VERTICAL, false));
            OrderDetailOtherFeeListAdapter adapter = new OrderDetailOtherFeeListAdapter(this, order.orderCostList);
            rvOtherFee.setAdapter(adapter);
            rvOtherFee.setVisibility(View.VISIBLE);
        }

        llContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void updateOrderStatusSuccess(@NonNull PaymentLinkOrder response) {
        DialogUtils.dismissDialog(OrderDetailsActivity.this);
        order = response;
        if (order.isUpdateAble()) {
            DialogUtils.toastDialog(OrderDetailsActivity.this, getString(R.string.pl_order_status_notice), null);
        }
        initView();
    }

    @Override
    public void updateOrderStatusFailed(@Nullable Object error) {
        DialogUtils.dismissDialog(OrderDetailsActivity.this);
        DialogUtils.toastDialog(OrderDetailsActivity.this, error.toString(), null);
    }

    private void updateOrderStatus() {
        if (mPresenter != null) {
            mPresenter.updateOrderStatus(order);
        }
    }


    @Override
    public void getOrderDetailSuccess(@NonNull PaymentLinkOrder response) {
        DialogUtils.dismissDialog(OrderDetailsActivity.this);
        order = response;
        initView();
    }

    @Override
    public void getOrderDetailFailed(@Nullable Object error) {
        DialogUtils.dismissDialog(OrderDetailsActivity.this);
        DialogUtils.toastDialog(OrderDetailsActivity.this, error.toString(), null);
    }

    private void initData() {
        if (order == null) {
            return;
        }
        if (mPresenter != null) {
            mPresenter.getOrderDetail(order);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            Intent i = new Intent();
            OrderDetailsActivity.this.setResult(RESULT_CODE_CREATE_ORDER_SUCCESS, i);
            if (!OrderDetailsActivity.this.isFinishing()) {
                OrderDetailsActivity.this.finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
