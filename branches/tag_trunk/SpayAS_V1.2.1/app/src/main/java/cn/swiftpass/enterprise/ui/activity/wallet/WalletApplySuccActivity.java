/*
 * 文 件 名:  WalletMainActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-1
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.wallet;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.total.PayDistributionActivity;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.StringUtil;

import com.tencent.stat.StatService;

/**
 * 开通电子钱包 提交
 * 
 * @author  he_hui
 * @version  [版本号, 2016-11-1]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class WalletApplySuccActivity extends TemplateActivity
{
    
    private Button btn_next_step;
    
    private TextView tx_peop;
    
    private EditText et_id;
    
    private TextView tv_mch_name;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_apply_succeed);
        try
        {
            StatService.trackCustomEvent(WalletApplySuccActivity.this, "SPConstWalletApplyCount", "电子钱包开户成功数量统计");
        }
        catch (Exception e)
        {
        }
        initView();
        setLister();
    }
    
    private void setLister()
    {
        btn_next_step.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                HandlerManager.notifyMessage(HandlerManager.WALLET_OPEN_SUCCESS, HandlerManager.WALLET_OPEN_SUCCESS);
                
                for (Activity activity : MainApplication.listActivities)
                {
                    activity.finish();
                }
                WalletApplySuccActivity.this.finish();
                
            }
        });
    }
    
    private void initView()
    {
        btn_next_step = getViewById(R.id.btn_next_step);
        tx_peop = getViewById(R.id.tx_peop);
        et_id = getViewById(R.id.et_id);
        tv_mch_name = getViewById(R.id.tv_mch_name);
        if (!StringUtil.isEmptyOrNull(MainApplication.cardholder))
        {
            tv_mch_name.setText(MainApplication.cardholder);
        }
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        
        titleBar.setTitle(R.string.tx_apply_detail);
        titleBar.setLeftButtonIsVisible(true);
        titleBar.setLeftButtonVisible(true);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
                
                HandlerManager.notifyMessage(HandlerManager.WALLET_OPEN_SUCCESS, HandlerManager.WALLET_OPEN_SUCCESS);
                if (MainApplication.listActivities.size() > 0)
                {
                    for (Activity activity : MainApplication.listActivities)
                    {
                        activity.finish();
                    }
                }
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                showPage(PayDistributionActivity.class);
            }
        });
    }
}
