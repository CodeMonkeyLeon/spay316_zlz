package cn.swiftpass.enterprise.ui.widget.ImageCache;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.os.Handler;
import android.os.Message;
import cn.swiftpass.enterprise.utils.AppHelper;

public class ImageLoader
{
    public static final int GET = 1;
    
    public static final int POST = 2;
    
    public static int flag;
    
    //�?��map，用于缓存下载过的图�?
    private HashMap<String, SoftReference<Bitmap>> caches;
    
    //任务队列
    private ArrayList<Task> taskQueue;
    
    private Handler handler = new Handler()
    {
        
        @Override
        public void handleMessage(Message msg)
        {
            // TODO Auto-generated method stub
            //子线程中返回的下载完成的任务
            Task task = (Task)msg.obj;
            //调用callback对象的loadedImage方法，将图片路径和图片回传给adapter
            task.callback.loadedImage(task.path, task.bitmap);
        }
        
    };
    
    //任务下载线程
    private Thread thread = new Thread()
    {
        
        @Override
        public void run()
        {
            // TODO Auto-generated method stub
            //任务轮询
            while (true)
            {
                //当任务队列中还有未处理任务时，执行下载任�?
                while (taskQueue.size() > 0)
                {
                    //获取第一条任务，并将之从任务队列移除
                    Task task = taskQueue.remove(0);
                    //					Log.i("msg", "in thread: " + taskQueue.size() + ":" + task.path);
                    try
                    {
                        //下载图片
                        InputStream in = getStream(task.path, null, null, GET);
                        task.bitmap = getBitmap(in, 1);
                        try
                        {
                            save(task.path, task.bitmap);
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                        //将下载的图片添加到缓�?
                        caches.put(task.path, new SoftReference<Bitmap>(task.bitmap));
                        //如果handler对象不为null
                        if (handler != null)
                        {
                            //创建消息对象，并将完成的任务添加到消息对象中
                            Message msg = handler.obtainMessage();
                            msg.obj = task;
                            //发�?消息回主线程
                            handler.sendMessage(msg);
                        }
                    }
                    catch (IOException e)
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                
                //如果任务队列为空，则令线程等�?
                synchronized (this)
                {
                    try
                    {
                        this.wait();
                    }
                    catch (InterruptedException e)
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }
        
    };
    
    //创建对象时，初始化map
    public ImageLoader()
    {
        caches = new HashMap<String, SoftReference<Bitmap>>();
        taskQueue = new ArrayList<ImageLoader.Task>();
        //启动下载任务线程
        thread.start();
    }
    
    //图片加载方法
    public Bitmap loadImage(final String path, int scale, final ImageCallback callback, int flag)
    {
        this.flag = flag;
        //如果图片路径，在缓存中存在则不再下载
        Bitmap bitmap = null;
        if (caches.containsKey(path))
        {
            //取出软引�?
            SoftReference<Bitmap> rf = caches.get(path);
            //通过软引用，获取图片
            bitmap = rf.get();
            //如果该图片已经被释放，则将该path对应的键值对从map中移�?
            if (bitmap == null)
            {
                caches.remove(path);
            }
            else
            {
                //如果该图片未被释放，则返回该图片
                return bitmap;
            }
        }
        else
        {
            bitmap = getBitmap(path);
            if (bitmap != null)
            {
                caches.put(path, new SoftReference<Bitmap>(bitmap));
                return bitmap;
            }
        }
        if (!caches.containsKey(path))
        {
            //如果该路径的图片未在缓存�?
            //则创建新任务，添加到任务队列
            Task task = new Task();
            task.path = path;
            task.callback = callback;
            if (!taskQueue.contains(task))
            {
                taskQueue.add(task);
                //唤醒任务下载线程
                synchronized (thread)
                {
                    thread.notify();
                }
            }
        }
        //如果缓存中没有图片则返回null
        return null;
    }
    
    private Bitmap getBitmap(String path)
    {
        try
        {
            if (flag == 1)
            {
                return BitmapFactory.decodeFile(AppHelper.getImgCacheDir() + path.substring(path.lastIndexOf("=") + 1));
            }
            else
            {
                return BitmapFactory.decodeFile(AppHelper.getImgCacheDir()
                    + path.replaceAll("http://wx.qlogo.cn/mmopen/", "").replaceAll("/", ""));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public interface ImageCallback
    {
        void loadedImage(String path, Bitmap bitmap);
    }
    
    //任务�?
    class Task
    {
        String path;//下载任务的下载路�?
        
        Bitmap bitmap;//下载的图�?
        
        ImageCallback callback;//回调对象
        
        @Override
        public boolean equals(Object o)
        {
            return ((Task)o).path.equals(path);
        }
    }
    
    public static InputStream getStream(String path, ArrayList<BasicNameValuePair> headers,
        ArrayList<BasicNameValuePair> params, int method)
        throws IOException
    {
        InputStream in = null;
        HttpEntity entity = getEntity(path, headers, params, method);
        if (entity != null)
        {
            in = entity.getContent();
        }
        return in;
    }
    
    @SuppressWarnings("deprecation")
    private static HttpEntity getEntity(String path, ArrayList<BasicNameValuePair> headers,
        ArrayList<BasicNameValuePair> params, int method)
        throws IOException
    {
        HttpEntity entity = null;
        // 创建client
        DefaultHttpClient client = new DefaultHttpClient();
        // 创建request对象
        HttpUriRequest request = null;
        switch (method)
        {
            case GET:
                StringBuilder sb = new StringBuilder(path);
                // 如果有参数，则将参数名和参数值连接到path之后
                if (params != null && !params.isEmpty())
                {
                    sb.append('?');
                    for (BasicNameValuePair pair : params)
                    {
                        sb.append(pair.getName()).append('=').append(URLEncoder.encode(pair.getValue())).append('&');
                    }
                    sb.deleteCharAt(sb.length() - 1);
                }
                request = new HttpGet(sb.toString());
                
                break;
            case POST:
                request = new HttpPost(path);
                
                if (params != null && !params.isEmpty())
                {
                    // 设置post请求的请求实�?
                    UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params);
                    ((HttpPost)request).setEntity(ent);
                }
                break;
        }
        // 如果消息头参数不为空，则循环设置�?��的消息头
        if (headers != null && !headers.isEmpty())
        {
            for (BasicNameValuePair pair : headers)
            {
                request.setHeader(pair.getName(), pair.getValue());
            }
        }
        // 执行请求，获得response
        HttpResponse response = client.execute(request);
        // 判断response状�?�?
        int code = response.getStatusLine().getStatusCode();
        if (code == HttpStatus.SC_OK)
        {
            // 如果请求成功，获得响应实�?
            entity = response.getEntity();
        }
        return entity;
    }
    
    public static void save(String path, Bitmap bitmap)
        throws Exception
    {
        String name;
        if (flag == 1)
        {
            name = path.substring(path.lastIndexOf("=") + 1);
        }
        else
        {
            name = path.replaceAll("http://wx.qlogo.cn/mmopen/", "").replaceAll("/", "");
        }
        File file = new File(AppHelper.getImgCacheDir() + name);
        try
        {
            if (!file.exists())
            {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            FileOutputStream out = new FileOutputStream(file);
            if (bitmap != null)
            {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            }
            out.close();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        
    }
    
    public static Bitmap getBitmap(InputStream in, int scale)
    {
        Bitmap bitmap = null;
        Options ops = new Options();
        ops.inSampleSize = scale;
        bitmap = BitmapFactory.decodeStream(in, null, ops);
        return bitmap;
    }
    
    public static Bitmap readBitMap(Context context, int resId)
    {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = Bitmap.Config.RGB_565;
        opt.inPurgeable = true;
        opt.inInputShareable = true;
        // 获取资源图片
        InputStream is = context.getResources().openRawResource(resId);
        return BitmapFactory.decodeStream(is, null, opt);
    }
}
