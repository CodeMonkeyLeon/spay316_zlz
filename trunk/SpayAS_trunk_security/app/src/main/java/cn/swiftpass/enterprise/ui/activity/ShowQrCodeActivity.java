package cn.swiftpass.enterprise.ui.activity;

import static cn.swiftpass.enterprise.MainApplication.getInstance;

import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.common.sentry.SentryUtils;
import com.example.common.sp.PreferenceUtil;
import com.google.zxing.WriterException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.MaxCardManager;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.MasterCardInfo;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.QRcodeInfo;
import cn.swiftpass.enterprise.bussiness.model.WalletListBean;
import cn.swiftpass.enterprise.common.Constant;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.ShowQrCodeContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.ShowQrCodePresenter;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.SelectPicPopupWindow;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.DisplayUtil;
import cn.swiftpass.enterprise.utils.KotlinUtils;
import cn.swiftpass.enterprise.utils.LocaleUtils;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.Utils;
import cn.swiftpass.enterprise.utils.interfaces.OnOkClickListener;

/**
 * 显示二维码
 * User: he_hui
 * Date: 15-6-25
 * Time: 下午5:43
 */
public class ShowQrCodeActivity extends BaseActivity<ShowQrCodeContract.Presenter> implements ShowQrCodeContract.View {


    private static final String TAG = ShowQrCodeActivity.class.getSimpleName();
    //自定义的弹出框类
    SelectPicPopupWindow menuWindow, menuLiquidWindow;
    LinearLayout mHasFeelayout;
    double minus = 1;
    private ImageView img;
    private ScrollView sv_native_pay_static_code;
    private TextView tvMoney, pay_tv_info, pay_wx_str, tv_switch;
    private ImageView iv_payment_way_small_icon;
    private Context mContext;
    private QRcodeInfo qrcodeInfo;
    private MasterCardInfo masterCardInfo;//MasterCard下单的对象
    private boolean isLiquidType;
    private boolean isInstapayQRPayment;
    private boolean isCardPayment;
    private WalletListBean walletBean;
    private TimeCount time;
    private DialogInfo dialogInfo;
    private Handler mHandler;
    private boolean isMark = true;
    private LinearLayout money_lay;
    private LinearLayout vcard_lay;
    private TextView id_tv_amount;
    private TextView tv_payMoney;
    private boolean isStop = true; // 强行中断交易
    private TextView tv_pase;
    private TextView mSurcharge;
    private String mark = "";
    private String OrderTotalMoney; //下单金额
    private long timeCount = 5;
    private Runnable myRunnable = new Runnable() {
        @Override
        public void run() {
            if (timeCount > 0 && dialogInfo != null) {
                dialogInfo.setBtnOkText(getString(R.string.btnOk) + "(" + getString(R.string.show_close) + timeCount + getString(R.string.tv_second) + ")");
                timeCount -= 1;
                mHandler.postDelayed(this, 1000);
            } else {
                mHandler.removeCallbacks(this);
                if (dialogInfo != null && dialogInfo.isShowing()) {
                    dialogInfo.dismiss();
                }
                dialogInfo.setBtnOkText(getString(R.string.dialog_close));
                ShowQrCodeActivity.this.finish();
            }
        }
    };

    public static void startActivity(QRcodeInfo qrcodeInfo, Context context, String mark) {
        Intent it = new Intent();
        it.putExtra("qrcodeInfo", qrcodeInfo);
        it.putExtra("mark", mark);
        it.setClass(context, ShowQrCodeActivity.class);
        context.startActivity(it);
    }

    public static void startActivity(QRcodeInfo qrcodeInfo, Context context, WalletListBean walletBean, boolean isLiquidType, String mark) {
        Intent it = new Intent();
        it.putExtra("qrcodeInfo", qrcodeInfo);
        it.putExtra("walletBean", walletBean);
        it.putExtra("isLiquidType", isLiquidType);
        it.putExtra("mark", mark);
        it.setClass(context, ShowQrCodeActivity.class);
        context.startActivity(it);
    }

    public static void startActivity(QRcodeInfo qrcodeInfo, Context context, WalletListBean walletBean, boolean isLiquidType, boolean isInstapayQRPayment, String mark) {
        Intent it = new Intent();
        it.putExtra("qrcodeInfo", qrcodeInfo);
        it.putExtra("walletBean", walletBean);
        it.putExtra("isLiquidType", isLiquidType);
        it.putExtra("isQRPayment", isInstapayQRPayment);
        it.putExtra("mark", mark);
        it.setClass(context, ShowQrCodeActivity.class);
        context.startActivity(it);
    }

    public static void startActivity(QRcodeInfo qrcodeInfo, MasterCardInfo masterCardInfo, Context context, WalletListBean walletBean, boolean isLiquidType,
                                     boolean isInstapayQRPayment, boolean isCardPayment, String mark) {
        Intent it = new Intent();
        it.putExtra("qrcodeInfo", qrcodeInfo);
        it.putExtra("MasterCardInfo", masterCardInfo);
        it.putExtra("walletBean", walletBean);
        it.putExtra("isLiquidType", isLiquidType);
        it.putExtra("isQRPayment", isInstapayQRPayment);
        it.putExtra("isCardPayment", isCardPayment);
        it.putExtra("mark", mark);
        it.setClass(context, ShowQrCodeActivity.class);
        context.startActivity(it);
    }

    @Override
    protected ShowQrCodeContract.Presenter createPresenter() {
        return new ShowQrCodePresenter();
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainApplication.getInstance().getListActivities().add(this);
        //根据当前的小数点的位数来判断应该除以多少
        for (int i = 0; i < MainApplication.getInstance().getNumFixed(); i++) {
            minus = minus * 10;
        }

        initViews();
    }

    private void initViews() {
        mContext = this;
        setContentView(R.layout.activity_show_qrcode_new);
        tv_pase = getViewById(R.id.tv_pase);
        tv_pase.setVisibility(View.GONE);
        tv_payMoney = getViewById(R.id.tv_payMoney);
        tv_payMoney.setText(MainApplication.getInstance().getFeeFh());
        iv_payment_way_small_icon = getViewById(R.id.iv_payment_way_small_icon);
        tv_switch = getViewById(R.id.tv_switch);
        sv_native_pay_static_code = getViewById(R.id.sv_native_pay_static_code);

        //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth", "sale");
        if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(saleOrPreauth, "pre_auth")) {
            sv_native_pay_static_code.setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
            tv_switch.setTextColor(getResources().getColor(R.color.bg_text_pre_auth));
        } else {
            sv_native_pay_static_code.setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
            tv_switch.setTextColor(getResources().getColor(R.color.title_bg_new));
        }

        id_tv_amount = getViewById(R.id.id_tv_amount);
        money_lay = getViewById(R.id.money_lay);
        vcard_lay = getViewById(R.id.vcard_lay);

        pay_tv_info = getViewById(R.id.pay_tv_info);
        mHandler = new Handler();
        pay_wx_str = getViewById(R.id.pay_wx_str);

        qrcodeInfo = (QRcodeInfo) getIntent().getSerializableExtra("qrcodeInfo");
        masterCardInfo = (MasterCardInfo) getIntent().getSerializableExtra("MasterCardInfo");
        walletBean = (WalletListBean) getIntent().getSerializableExtra("walletBean");
        isLiquidType = getIntent().getBooleanExtra("isLiquidType", false);
        isInstapayQRPayment = getIntent().getBooleanExtra("isQRPayment", false);
        isCardPayment = getIntent().getBooleanExtra("isCardPayment", false);

        mark = getIntent().getStringExtra("mark");

        tvMoney = getViewById(R.id.tv_money);
        img = getViewById(R.id.img);
        //新增
        mHasFeelayout = getViewById(R.id.has_fee_layout);
        mSurcharge = getViewById(R.id.surcharge);

        money_lay.setVisibility(View.GONE);
        vcard_lay.setVisibility(View.GONE);

        //根据选的不同通道初始化不同的值
        if (!isCardPayment && qrcodeInfo != null) {
            OrderTotalMoney = qrcodeInfo.totalMoney;
            BigDecimal bigDecimal = new BigDecimal(Long.parseLong(qrcodeInfo.totalMoney) / minus);
            String strSurchargelMoney = qrcodeInfo.getSurcharge();
            BigDecimal bigSurcharge = null;
            //如果开通预授权，则手动把surcharge写死不显示。
            if (MainApplication.getInstance().isSurchargeOpen()) {
                if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                    strSurchargelMoney = "0";
                }
            }
            if (TextUtils.isEmpty(strSurchargelMoney)) {
                tvMoney.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(Long.parseLong(qrcodeInfo.totalMoney)));
                bigSurcharge = new BigDecimal(0);
                mHasFeelayout.setVisibility(View.GONE);
            } else {
                tvMoney.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(Long.parseLong(qrcodeInfo.totalMoney) + Long.parseLong(strSurchargelMoney)));
                bigSurcharge = new BigDecimal(Long.parseLong(strSurchargelMoney) / minus);
                mHasFeelayout.setVisibility(View.VISIBLE);
            }
            paseRMBWithSurcharge(bigDecimal, bigSurcharge);

        } else if (isCardPayment && masterCardInfo != null) {
            OrderTotalMoney = masterCardInfo.getOrderAmount() + "";//订单金额（不包含附加费的金额）---- 用来下单的金额
            //计算金额的展示
            BigDecimal orderMoneyDecimal = new BigDecimal(masterCardInfo.getOrderAmount() / minus);//订单金额（不包含附加费的金额）
            BigDecimal SurchargeMoneyDecimal = new BigDecimal(masterCardInfo.getSurcharge() / minus);//订单附加费

            //修改bug，不能用下面的这两个方法，因为金额如果大于1000，formatMoneyUtils后的金额有千分位逗号分隔，new BigDecimal里面参数传入带千分位的会报错
         /*   BigDecimal orderMoneyDecimal = new BigDecimal(DateUtil.formatMoneyUtils(masterCardInfo.getOrderAmount()));//订单金额（不包含附加费的金额）
            BigDecimal SurchargeMoneyDecimal =  new BigDecimal(DateUtil.formatMoneyUtils(masterCardInfo.getSurcharge()));//订单附加费*/

            //如果没有开通附加费
            if (!MainApplication.getInstance().isSurchargeOpen()) {
                tvMoney.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(masterCardInfo.getMoney())); //订单总额就是订单金额
                mHasFeelayout.setVisibility(View.GONE);
            } else {
                tvMoney.setText(MainApplication.getInstance().getFeeFh() + DateUtil.formatMoneyUtils(masterCardInfo.getMoney()));
                mHasFeelayout.setVisibility(View.VISIBLE);
            }
            paseRMBWithSurcharge(orderMoneyDecimal, SurchargeMoneyDecimal);
        }

        if (MainApplication.getInstance().isSurchargeOpen()) {
            if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                id_tv_amount.setVisibility(View.GONE);
                mHasFeelayout.setVisibility(View.GONE);
            } else {
                id_tv_amount.setVisibility(View.VISIBLE);
                mHasFeelayout.setVisibility(View.VISIBLE);
            }
        } else {
            id_tv_amount.setVisibility(View.GONE);
            mHasFeelayout.setVisibility(View.GONE);
        }

        //生成二维码
        if (isCardPayment) {
            if (masterCardInfo != null) {
                GenerateQRCodeView(null, masterCardInfo.getCashierUrl(), isLiquidType, isInstapayQRPayment, isCardPayment, walletBean);
            }
        } else {
            if (qrcodeInfo != null) {
                GenerateQRCodeView(qrcodeInfo.uuId, null, isLiquidType, isInstapayQRPayment, isCardPayment, walletBean);
            }
        }

        /**
         * 切换扫码
         */
        tv_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //点击扫码，不管是否是liquid pay 通道，先展示一级列表
                menuWindow = new SelectPicPopupWindow(ShowQrCodeActivity.this, new SelectPicPopupWindow.HandleNative() {
                    @Override
                    public void toPay(String type, String secondType, boolean isSecondPay, WalletListBean bean) {
                        if (getNativePayType(type).equalsIgnoreCase(Constant.NATIVE_LIQUID)) {//如果是点击的小钱包通道
                            menuLiquidWindow = new SelectPicPopupWindow(ShowQrCodeActivity.this, type, true, new SelectPicPopupWindow.HandleNative() {
                                @Override
                                public void toPay(String type, String secondType, boolean isSecondPay, WalletListBean bean) {
                                    //生成新的二维码之前，停止查询旧的单
                                    if (time != null) {
                                        time.cancel();
                                    }

                                    String msg = getString(R.string.tv_pay_prompt);
                                    toNativePay(msg, OrderTotalMoney, type, secondType, mark, isSecondPay, false, false, bean);
                                }
                            });
                            //显示窗口
                            menuLiquidWindow.showAtLocation(tv_switch, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0); //设置layout在PopupWindow中显示的位置

                        } else if (getNativePayType(type).equalsIgnoreCase(Constant.NATIVE_INSTAPAY)) {
                            //如果是点击的instapay的通道
                            //生成新的二维码之前，停止查询旧的单
                            if (time != null) {
                                time.cancel();
                            }

                            //是instapay通道，下单成功，则直接跳转到instapay页面
                            String msg = getString(R.string.tv_pay_prompt);
                            toNativePay(msg, OrderTotalMoney, type, secondType, mark, false, true, false, bean);

                        } else if (getNativePayType(type).equalsIgnoreCase(Constant.NATIVE_INSTAPAY_V2)) {//如果是点击的instapay的QRPayment通道
                            //生成新的二维码之前，停止查询旧的单
                            if (time != null) {
                                time.cancel();
                            }

                            //是instapay通道，下单成功，则直接跳转到instapay页面
                            String msg = getString(R.string.tv_pay_prompt);
                            toNativePay(msg, OrderTotalMoney, type, secondType, mark, false, false, true, bean);

                        } else if (isMasterCardServiceType(getNativePayType(type))) {//如果当前选择的通道是MasterCard卡通道
                            //生成新的二维码之前，停止查询旧的单
                            if (time != null) {
                                time.cancel();
                            }

                            //则走masterCard下单逻辑，下单成功直接跳转到MasterCard卡收单二维码收款界面
                            toMasterCardNewPay(OrderTotalMoney, type);
                        } else {
                            //生成新的二维码之前，停止查询旧的单
                            if (time != null) {
                                time.cancel();
                            }

                            //不是小钱包通道，则直接生成二维码
                            String msg = getString(R.string.tv_pay_prompt);
                            toNativePay(msg, OrderTotalMoney, type, secondType, mark, false, false, false, bean);
                        }
                    }
                });
                //显示窗口
                menuWindow.showAtLocation(tv_switch, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0); //设置layout在PopupWindow中显示的位置
            }
        });
    }

    private void paseRMBWithSurcharge(BigDecimal bigDecimal, BigDecimal bigSurcharge) {
        try {
            if (!MainApplication.getInstance().getFeeType().equalsIgnoreCase("CNY")) {
                //新增
                tv_pase.setVisibility(View.GONE);
                BigDecimal paseBigDecimal = null;
                if (bigDecimal != null) {
                    if (MainApplication.getInstance().getSourceToUsdExchangeRate() > 0 && MainApplication.getInstance().getUsdToRmbExchangeRate() > 0) {
                        BigDecimal sourceToUsd = new BigDecimal(MainApplication.getInstance().getSourceToUsdExchangeRate());
                        BigDecimal usdRate = new BigDecimal(MainApplication.getInstance().getUsdToRmbExchangeRate());
//                        paseBigDecimal = bigSurcharge.add(bigDecimal).multiply(usdRate).multiply(sourceToUsd).setScale(2, BigDecimal.ROUND_FLOOR);
                        paseBigDecimal = bigSurcharge.add(bigDecimal).multiply(sourceToUsd).setScale(MainApplication.getInstance().getNumFixed(), BigDecimal.ROUND_HALF_UP)
                                .multiply(usdRate).setScale(MainApplication.getInstance().getNumFixed(), BigDecimal.ROUND_DOWN);


                    } else {
                        BigDecimal rate = new BigDecimal(MainApplication.getInstance().getExchangeRate());
                        paseBigDecimal = bigSurcharge.add(bigDecimal).multiply(rate).setScale(MainApplication.getInstance().getNumFixed(), BigDecimal.ROUND_FLOOR);
                    }
//                    String totalStr = "(" + getString(R.string.tv_charge_total) + ":" + DateUtil.formatPaseMoney(bigDecimal) + "," + getString(R.string.tx_surcharge) + ":" + DateUtil.formatPaseMoney(bigSurcharge) + ")";
                    String totalStr = MainApplication.getInstance().getFeeFh() + DateUtil.formatPaseMoney(bigDecimal);
                    String surchargeFee = MainApplication.getInstance().getFeeFh() + DateUtil.formatPaseMoney(bigSurcharge);
                    id_tv_amount.setText(totalStr);
                    mSurcharge.setText(surchargeFee);
                    tv_pase.setText(getString(R.string.tx_about) + getString(R.string.tx_mark) + DateUtil.formatPaseRMBMoney(paseBigDecimal));
                }
            } else {
                tv_pase.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
    }


    @Override
    public void unifiedNativePaySuccess(QRcodeInfo response, @Nullable String payType, boolean isInstapay, boolean isSecond) {
        if (response != null && isStop) {
            qrcodeInfo = new QRcodeInfo();
            qrcodeInfo.outAuthNo = response.outAuthNo;
            qrcodeInfo.orderNo = response.orderNo;
            qrcodeInfo.payType = payType;
            qrcodeInfo.setInvoiceId(response.getInvoiceId());
            qrcodeInfo.setExpirationDate(response.getExpirationDate());

            //之前下单的masterCardInfo不保存
            masterCardInfo = null;

            if (isInstapay) {//如果是instapay下单，则直接跳转到instapay的页面

                InstapayInfoActivity.startActivity(ShowQrCodeActivity.this, qrcodeInfo);

                //停止查单，结束当前界面
                if (time != null) {
                    time.cancel();
                }
                finish();

            } else {//否则还是按照以前的逻辑，生成固码----instapayQRPayment也是按照原来的逻辑生成固码，只是中间要加一个logo
                GenerateQRCodeView(response.uuId, null, isSecond, isInstapayQRPayment, false, walletBean);
            }
        } else {
            isStop = true;
        }
    }

    @Override
    public void unifiedNativePayFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }
        if (!isStop) {
            return;
        }
        if (error != null) {
            ShowQrCodeActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    toastDialog(ShowQrCodeActivity.this, error.toString(), null);
                }
            });
        }

        ShowQrCodeActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //如果生成码的过程失败了，则还是重新开始查单，查的还是上一次的单
                Countdown();
            }
        });
    }

    private void toNativePay(final String msg, final String money, final String type, final String tradeType, final String note, final boolean isSecond,
                             boolean isInstapay, boolean isInstapayQRPayment,
                             final WalletListBean walletBean) {
        if (mPresenter != null) {
            mPresenter.unifiedNativePay(money, type, tradeType, 0, null, note, isInstapay, isSecond);
        }
    }


    @Override
    public void masterCardNativePaySuccess(MasterCardInfo response, String apiCode) {
        //masterCard下单完成，之前下单的qrcodeInfo不保存
        masterCardInfo = response;
        masterCardInfo.setApiCode(apiCode);
        qrcodeInfo = null;

        //下单成功，直接生成二维码
        GenerateQRCodeView(null, masterCardInfo.getCashierUrl(), false, false, true, null);

    }

    @Override
    public void masterCardNativePayFailed(@Nullable Object error) {
        if (checkSession()) {
            return;
        }
        if (!isStop) {
            return;
        }

        if (error != null && !TextUtils.isEmpty(error.toString())) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    toastDialog(ShowQrCodeActivity.this, error.toString(), null);
                }
            });
        } else {
            toastDialog(ShowQrCodeActivity.this, getStringById(R.string.generate_code_failed), null);
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //如果生成码的过程失败了，则还是重新开始查单，查的还是上一次的单
                Countdown();
            }
        });
    }

    /* switch收银台下单*/
    public void toMasterCardNewPay(String money, String apiCode) {
        if (mPresenter != null) {
            mPresenter.masterCardNativePay(money, apiCode, mark);
        }
    }

    //生成二维码的view
    public void GenerateQRCodeView(final String uuId, final String cashierUrl, boolean isLiquidType, boolean isInstapayQRPayment, boolean isMasterCardQR, WalletListBean walletBean) {
        // 生成二维码
        try {
            Object object = SharedPreUtils.readProduct("payTypeNameMap" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            if (object != null) {
                Map<String, DynModel> payTypeNameMap = (Map<String, DynModel>) object;
                //根据当前是否是masterCard去获取model
                DynModel dynModel = isMasterCardQR ? payTypeNameMap.get(masterCardInfo.getApiCode()) : payTypeNameMap.get(qrcodeInfo.payType);

                if (null != dynModel && !StringUtil.isEmptyOrNull(dynModel.getProviderName())) {
                    try {
                        String language = LocaleUtils.getLocaleLanguage();
                        if (language.equalsIgnoreCase(Constant.LANG_CODE_ZH_CN)) {
                            if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                                if (isLiquidType && walletBean != null) {
                                    titleBar.setTitle(walletBean.getName() + getString(R.string.choose_pre_auth));
                                    pay_wx_str.setText(walletBean.getName() + getString(R.string.choose_pre_auth) + ",");
                                } else {
                                    if (isMasterCardQR) {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()) + getString(R.string.choose_pre_auth));
                                        pay_wx_str.setText(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()) + getString(R.string.choose_pre_auth) + ",");
                                    } else {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType) + getString(R.string.choose_pre_auth));
                                        pay_wx_str.setText(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType) + getString(R.string.choose_pre_auth) + ",");
                                    }

                                }
                            } else {
                                if (isLiquidType && walletBean != null) {
                                    titleBar.setTitle(walletBean.getName());
                                    pay_wx_str.setText(walletBean.getName() + ",");
                                } else {
                                    if (isMasterCardQR) {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()));
                                        pay_wx_str.setText(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()) + ",");
                                    } else {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType));
                                        pay_wx_str.setText(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType) + ",");
                                    }
                                }
                            }

                        } else if (language.equalsIgnoreCase(Constant.LANG_CODE_ZH_TW)) {
                            if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                                if (isLiquidType && walletBean != null) {
                                    titleBar.setTitle(walletBean.getName() + getString(R.string.choose_pre_auth));
                                    pay_wx_str.setText(walletBean.getName() + getString(R.string.choose_pre_auth) + ",");
                                } else {
                                    if (isMasterCardQR) {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()) + getString(R.string.choose_pre_auth));
                                        pay_wx_str.setText(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()) + getString(R.string.choose_pre_auth) + ",");
                                    } else {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType) + getString(R.string.choose_pre_auth));
                                        pay_wx_str.setText(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType) + getString(R.string.choose_pre_auth) + ",");
                                    }
                                }

                            } else {
                                if (isLiquidType && walletBean != null) {
                                    titleBar.setTitle(walletBean.getName());
                                    pay_wx_str.setText(walletBean.getName() + ",");
                                } else {
                                    if (isMasterCardQR) {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()));
                                        pay_wx_str.setText(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()) + ",");
                                    } else {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType));
                                        pay_wx_str.setText(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType) + ",");
                                    }
                                }
                            }
                        } else {
                            if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {

                                if (isLiquidType && walletBean != null) {
                                    titleBar.setTitle(walletBean.getName() + " " + getString(R.string.choose_pre_auth));
                                    pay_wx_str.setText(walletBean.getName() + " " + getString(R.string.choose_pre_auth) + ",");
                                } else {
                                    if (isMasterCardQR) {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()) + " " + getString(R.string.choose_pre_auth));
                                        pay_wx_str.setText(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()) + " " + getString(R.string.choose_pre_auth) + ",");
                                    } else {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType) + " " + getString(R.string.choose_pre_auth));
                                        pay_wx_str.setText(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType) + " " + getString(R.string.choose_pre_auth) + ",");
                                    }
                                }
                            } else {
                                if (isLiquidType && walletBean != null) {
                                    titleBar.setTitle(walletBean.getName());
                                    pay_wx_str.setText(walletBean.getName() + ",");
                                } else {
                                    if (isMasterCardQR) {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()));
                                        pay_wx_str.setText(MainApplication.getInstance().getPayTypeMap().get(masterCardInfo.getApiCode()) + ",");
                                    } else {
                                        titleBar.setTitle(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType));
                                        pay_wx_str.setText(MainApplication.getInstance().getPayTypeMap().get(qrcodeInfo.payType) + ",");
                                    }
                                }
                            }
                        }

                    } catch (Exception e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                    }
                }
            }

            if (isLiquidType && walletBean != null) {
                if (!TextUtils.isEmpty(walletBean.getImageUrl())) {
                    if (getInstance() != null) {
                        Glide.with(getInstance())
                                .load(walletBean.getImageUrl())
                                .placeholder(R.drawable.icon_general_receivables)
                                .error(R.drawable.icon_general_receivables)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .into(iv_payment_way_small_icon);
                    }
                } else {
                    iv_payment_way_small_icon.setImageResource(R.drawable.icon_general_receivables);
                }
            } else {
                Object object_icon = SharedPreUtils.readProduct("payTypeIcon" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
                if (object_icon != null) {
                    try {
                        Map<String, String> typePicMap = (Map<String, String>) object_icon;
                        if (typePicMap != null && typePicMap.size() > 0) {
                            String picUrl = isMasterCardQR ? typePicMap.get(masterCardInfo.getApiCode()) : typePicMap.get(qrcodeInfo.payType);
                            if (!StringUtil.isEmptyOrNull(picUrl)) {
                                if (getInstance() != null) {
                                    Glide.with(getInstance())
                                            .load(picUrl)
                                            .placeholder(R.drawable.icon_general_receivables)
                                            .error(R.drawable.icon_general_receivables)
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .into(iv_payment_way_small_icon);
                                }
                            } else {
                                iv_payment_way_small_icon.setImageResource(R.drawable.icon_general_receivables);
                            }
                        }
                    } catch (Exception e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                    }
                }
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int w = DisplayUtil.dip2Px(ShowQrCodeActivity.this, 280);
                    int h = DisplayUtil.dip2Px(ShowQrCodeActivity.this, 280);
                    float scale = ShowQrCodeActivity.this.getResources().getDisplayMetrics().density;
                    if (scale == 1.0f) {
                        w = DisplayUtil.dip2Px(ShowQrCodeActivity.this, 380);
                        h = DisplayUtil.dip2Px(ShowQrCodeActivity.this, 380);
                    }

                    if (isInstapayQRPayment && (BuildConfig.bankCode.contains("aub") || (qrcodeInfo != null && !TextUtils.isEmpty(qrcodeInfo.payType) && Integer.parseInt(qrcodeInfo.payType) == Constant.RB_INSTAPAY_CODE))) {//如果是instapay的QRPayment的通道，则在中间添加一个logo
                        try {
                            Bitmap firstBitmap;
                            Bitmap bitmap_logo = BitmapFactory.decodeResource(getInstance().getResources(), R.drawable.logo_instapay_white);
                            firstBitmap = MaxCardManager.getInstance().createQRCodeWithLogo(true, false, ShowQrCodeActivity.this, uuId, w, h, bitmap_logo);
                            img.setImageBitmap(firstBitmap);
                        } catch (WriterException e) {
                            SentryUtils.INSTANCE.uploadTryCatchException(
                                    e,
                                    SentryUtils.INSTANCE.getClassNameAndMethodName()
                            );

                        }
                    } else if (isMasterCardQR) {//如果是MasterCard的支付通道
                        Bitmap bitmap;
                        try {
                            bitmap = MaxCardManager.getInstance().create2DCode(cashierUrl, w, h);
                            img.setImageBitmap(bitmap);
                        } catch (WriterException e) {
                            SentryUtils.INSTANCE.uploadTryCatchException(
                                    e,
                                    SentryUtils.INSTANCE.getClassNameAndMethodName()
                            );

                        }
                    } else {
                        Bitmap bitmap;
                        try {
                            bitmap = MaxCardManager.getInstance().create2DCode(uuId, w, h);
                            img.setImageBitmap(bitmap);
                        } catch (WriterException e) {
                            SentryUtils.INSTANCE.uploadTryCatchException(
                                    e,
                                    SentryUtils.INSTANCE.getClassNameAndMethodName()
                            );

                        }
                    }
                }
            });

            Countdown();

        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
    }

    /**
     * 支付一分钟倒计时
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void Countdown() {
        // 总共2分钟 间隔2秒
        time = new TimeCount(1000 * 60 * 2, 2000);
        time.start();
    }


    @Override
    public void queryMasterCardOrderByOrderNoSuccess(Order response) {
        if (response == null) {
            return;
        }
        //2：支付成功
        if (response.getTradeState() == 2) {
            //停止查询
            if (time != null) {
                time.cancel();
            }
            //停止查询的时候把这个字段手动置空，不查询
            masterCardInfo.setOutTradeNo(null);
            //清掉备注
            NoteMarkActivity.setNoteMark("");
            //跳转到成功页面
            MasterCardPayResultActivity.startActivity(ShowQrCodeActivity.this, response);
            //finish当前页面
            finish();
        }
    }

    @Override
    public void queryMasterCardOrderByOrderNoFailed(@Nullable Object error) {

    }

    public void queryMasterCardOrderStatus(String service, String outTradeNo) {
        if (mPresenter != null) {
            mPresenter.queryMasterCardOrderByOrderNo(service, outTradeNo);
        }
    }


    @Override
    public void queryOrderByOrderNoSuccess(Order response) {
        if (response == null) {
            return;
        }
        if (response.state.equals("2") && isMark) {
            if (time != null) {
                time.cancel();
            }
            qrcodeInfo.orderNo = null;
            qrcodeInfo.outAuthNo = null;
            isMark = false;
            NoteMarkActivity.setNoteMark("");
            if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth", "sale"), "pre_auth")) {
                PreAutFinishActivity.startActivity(ShowQrCodeActivity.this, response, 1);
            } else {
                PayResultActivity.startActivity(ShowQrCodeActivity.this, response);
            }
            finish();
        }
    }

    @Override
    public void queryOrderByOrderNoFailed(@Nullable Object error) {

    }

    /**
     * 查询订单获取状态
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void queryOrderGetStuts() {
        String orderNoStr;
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth", "sale");
        if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(saleOrPreauth, "pre_auth")) {
            orderNoStr = qrcodeInfo.outAuthNo;
        } else {
            orderNoStr = qrcodeInfo.orderNo;
        }


        if (mPresenter != null) {
            mPresenter.queryOrderByOrderNo(orderNoStr);
        }
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth", "sale");
        if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(saleOrPreauth, "pre_auth")) {
            titleBar.setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
        } else {
            titleBar.setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
        }
        titleBar.setRightButLayVisible(false, null);
        titleBar.setLeftButtonVisible(true);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onRightLayClick() {
                // TODO Auto-generated method stub

            }

            @Override
            public void onRightButLayClick() {

            }
        });
    }


    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (time != null) {
            time.cancel();
        }

        if (dialogInfo != null) {
            dialogInfo.dismiss();
        }
    }

    //通过ApiCode得到当前支付类型的NativePayType
    public String getNativePayType(String ApiCode) {
        //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth", "sale");
        if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(saleOrPreauth, "pre_auth")) {
            Object object = SharedPreUtils.readProduct("dynPayType_pre_auth" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            if (object != null) {
                List<DynModel> list = (List<DynModel>) object;
                if (null != list && list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getApiCode().equalsIgnoreCase(ApiCode)) {
                            return list.get(i).getNativeTradeType();
                        }
                    }
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            Object object = SharedPreUtils.readProduct("ActiveDynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());
            Object object_CardPayment = SharedPreUtils.readProduct("cardPayment_ActiveDynPayType" + BuildConfig.bankCode + MainApplication.getInstance().getMchId());

            List<DynModel> list = new ArrayList<>();
            List<DynModel> list_CardPayment = new ArrayList<>();
            if (object != null) {
                list = (List<DynModel>) object;
            }
            if (object_CardPayment != null) {
                list_CardPayment = (List<DynModel>) object_CardPayment;
            }

            //把两个数组拼接起来
            List<DynModel> listAll = new ArrayList<>();
            if (list != null) {
                listAll.addAll(list);
            }
            if (list_CardPayment != null) {
                listAll.addAll(list_CardPayment);
            }

            if (null != listAll && listAll.size() > 0) {
                for (int i = 0; i < listAll.size(); i++) {
                    if (listAll.get(i).getApiCode().equalsIgnoreCase(ApiCode)) {
                        return listAll.get(i).getNativeTradeType();
                    }
                }
            } else {
                return null;
            }
        }
        return null;
    }

    //V3.1.0 版本新增MasterCard交易通道
    //判断当前的交易类型是否是卡交易类型
    //如果是MasterCard交易通道则走新的下单和查单的逻辑
    public boolean isMasterCardServiceType(String NativePayType) {
        if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getUserInfo().cardServiceType)) {
            String[] arrPays = MainApplication.getInstance().getUserInfo().cardServiceType.split("\\|");
            for (int i = 0; i < arrPays.length; i++) {
                if (NativePayType.equalsIgnoreCase(arrPays[i])) {
                    return true;
                }
            }
        }
        return false;
    }


    @Override
    protected void onResume() {
        super.onResume();
        checkNotificationPermission();
    }


    private void checkNotificationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            //android13
            NotificationManager manager = getSystemService(NotificationManager.class);
            boolean notificationsEnabled = manager.areNotificationsEnabled();
            if (notificationsEnabled) {
                Log.i("TAG_ZLZ", "有通知权限");
            } else {
                Log.i("TAG_ZLZ", "没有通知权限");
                String str = String.format(getString(R.string.setting_permission_notice_content),
                        getString(R.string.setting_permission_notice));
                KotlinUtils.INSTANCE.showMissingPermissionDialog(
                        this,
                        str,
                        getString(R.string.setting_permission),
                        getString(R.string.setting_permission_cancel),
                        new OnOkClickListener() {
                            @Override
                            public void onOkClick() {
                                Utils.tryJumpNotifyPage(mContext);
                            }
                        }
                );
            }
        }
    }

    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }

        @Override
        public void onFinish() {//计时完毕时触发
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String mmsg = getString(R.string.show_request_outtime);
                    try {
                        dialogInfo = new DialogInfo(ShowQrCodeActivity.this, getStringById(R.string.public_cozy_prompt), mmsg, getStringById(R.string.btnOk), DialogInfo.REGISTFLAG, null, null);

                        DialogHelper.resize(ShowQrCodeActivity.this, dialogInfo);
                        dialogInfo.setOnKeyListener(new OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                                if (keycode == KeyEvent.KEYCODE_BACK) {
                                    return true;
                                }
                                return false;
                            }
                        });
                        if (dialogInfo != null) {

                            dialogInfo.show();
                        }
                        mHandler.post(myRunnable);
                    } catch (Exception e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                        Log.e(TAG, Log.getStackTraceString(e));
                    }
                }
            });
        }

        @Override
        public void onTick(long millisUntilFinished) {//计时过程显示
            //每次2S 查询一次订单状态，查询前判断是否有值
            //要做个判断，是查询masterCard的单还是非masterCard的单
            //如果是MasterCard下单,则去查MasterCard的单
            if (masterCardInfo != null && qrcodeInfo == null) {
                if (!TextUtils.isEmpty(masterCardInfo.getOutTradeNo())) {
                    queryMasterCardOrderStatus(masterCardInfo.getService(), masterCardInfo.getOutTradeNo());
                }
            } else if (qrcodeInfo != null) {//只有普通的下单成功，这个qrcodeInfo才有值，则轮询查单
                String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth", "sale");
                if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1 && TextUtils.equals(saleOrPreauth, "pre_auth")) {
                    if (null != qrcodeInfo.outAuthNo && !"".equals(qrcodeInfo.outAuthNo)) {
                        queryOrderGetStuts();
                    }
                } else {
                    if (null != qrcodeInfo.orderNo && !"".equals(qrcodeInfo.orderNo)) {
                        queryOrderGetStuts();
                    }
                }
            }
        }
    }


}
