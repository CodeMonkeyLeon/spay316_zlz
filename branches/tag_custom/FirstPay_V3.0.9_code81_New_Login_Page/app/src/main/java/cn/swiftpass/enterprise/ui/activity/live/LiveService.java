package cn.swiftpass.enterprise.ui.activity.live;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

/**
 * Created by admin on 2017/10/16.
 */

public class LiveService extends Service {
    public static final String TAG = LiveService.class.getSimpleName();

    public static void toLiveService(Context pContext) {
        Intent intent = new Intent(pContext, LiveService.class);
        pContext.startService(intent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        final ScreenManager screenManager = ScreenManager.getInstance(LiveService.this);
        ScreenBroadcastListener listener = new ScreenBroadcastListener(this);
        listener.registerListener(new ScreenBroadcastListener.ScreenStateListener() {
            @Override
            public void onScreenOn() {

                screenManager.finishActivity();
            }

            @Override
            public void onScreenOff() {

                screenManager.startActivity();

            }
        });
        return START_REDELIVER_INTENT;
    }
}

