package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created by aijingya on 2021/3/16.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: MasterCard订单列表页当天数据统计
 * @date 2021/3/16.16:08.
 */
public class MasterCardDailyStatisticsBean implements Serializable {
    private   int transactionAmount;    //交易金额
    private   int transactionsCount;    //交易笔数
    private   int refundAmount;//	退款金额
    private   int refundCount;//	退款笔数
    private   int transactionsRetainedProfits;//交易净额
    private   double yesterdayDoD; //	昨日环比

    public int getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(int transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public int getTransactionsCount() {
        return transactionsCount;
    }

    public void setTransactionsCount(int transactionsCount) {
        this.transactionsCount = transactionsCount;
    }

    public int getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(int refundAmount) {
        this.refundAmount = refundAmount;
    }

    public int getRefundCount() {
        return refundCount;
    }

    public void setRefundCount(int refundCount) {
        this.refundCount = refundCount;
    }

    public int getTransactionsRetainedProfits() {
        return transactionsRetainedProfits;
    }

    public void setTransactionsRetainedProfits(int transactionsRetainedProfits) {
        this.transactionsRetainedProfits = transactionsRetainedProfits;
    }

    public double getYesterdayDoD() {
        return yesterdayDoD;
    }

    public void setYesterdayDoD(double yesterdayDoD) {
        this.yesterdayDoD = yesterdayDoD;
    }
}
