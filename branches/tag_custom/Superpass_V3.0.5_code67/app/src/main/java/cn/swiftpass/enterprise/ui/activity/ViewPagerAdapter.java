/*
 * 文 件 名:  ViewPagerAdapter.java
 * 版    权:  copyright: 2013 Pactera. All rights reserved.
 * 描    述:  <描述>
 * 修 改 人:  jiaohongyun
 * 修改时间:  2013年11月22日
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity;

import java.util.ArrayList;

import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * ViewPager适配器 用来绑定数据和view
 * 
 * @author he_hui
 * @version [版本号, 2013年11月22日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class ViewPagerAdapter extends PagerAdapter
{
    
    // 界面列表
    private ArrayList<View> views;
    
    public ViewPagerAdapter(ArrayList<View> views)
    {
        this.views = views;
    }
    
    /**
     * 获得当前界面数
     */
    @Override
    public int getCount()
    {
        if (views != null)
        {
            return views.size();
        }
        return 0;
    }
    
    /**
     * 初始化position位置的界面
     */
    @Override
    public Object instantiateItem(View view, int position)
    {
        
        ((ViewPager)view).addView(views.get(position), 0);
        View temp = views.get(position);
        temp.setVisibility(View.VISIBLE);
        return temp;
    }
    
    /**
     * 判断是否由对象生成界面
     */
    @Override
    public boolean isViewFromObject(View view, Object arg1)
    {
        return (view == arg1);
    }
    
    /**
     * 销毁position位置的界面
     */
    @Override
    public void destroyItem(View view, int position, Object arg2)
    {
        ((ViewPager)view).removeView(views.get(position));
    }
    
    @Override
    public void finishUpdate(View arg0)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void restoreState(Parcelable arg0, ClassLoader arg1)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public Parcelable saveState()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void startUpdate(View arg0)
    {
        // TODO Auto-generated method stub
        
    }
    
}
