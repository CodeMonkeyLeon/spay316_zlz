package cn.swiftpass.enterprise.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * ascii 排序
 * @author ALAN
 *
 */
public class AsciiOrderUtils {


	/**用于参数 ascii 排序 主要是财付通规则需要使用 这里直接传需要的参数，输出
	 * attach&fee_type&out_trade_no&spbill 已经排好序的url 
	 * 参数部分 只适合get 请求方式
	 * */
	public static String getUrlAscciParameter(Map<String,Object> map)
	{
		
		 Object[] objs = map.keySet().toArray();
	     Arrays.sort(objs);
		StringBuilder sb = new StringBuilder();
		for (Object s : objs) {
	         sb.append(s).append("=").append(map.get(s)!=null?map.get(s):"");
	         sb.append("&");
	     }
		return sb.substring(0,sb.lastIndexOf("&"));
	}
	
	 public static void main(String[] args) {
		 Map<String,Object> array = new HashMap<String,Object>();
		 array.put("spbill_create_ip","1");
		 array.put("out_trade_no","2");
		 array.put("fee_type","3");
		 array.put("spbill_create_ip","4");
		 array.put("attach","5");
		 String baseUrl = "https://gw.tenpay.com/gateway/verifynotifyid.xml?";//?out_trade_no="+out_trade_no;
		 String s = baseUrl += getUrlAscciParameter(array);
	     System.out.println(s);
	 }
}
