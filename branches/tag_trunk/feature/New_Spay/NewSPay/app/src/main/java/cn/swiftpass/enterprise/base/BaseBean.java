package cn.swiftpass.enterprise.base;

import android.text.TextUtils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.JsonAdapter;

import java.io.Serializable;
import java.lang.reflect.Type;

import cn.swiftpass.enterprise.util.GsonUtil;

public class BaseBean<T> implements Serializable {
    private String error;
    @JsonAdapter(JsonDeserializer.class)
    private T data;
    private String code;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static class JsonDeserializer implements com.google.gson.JsonDeserializer {

        @Override
        public Object deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            if (jsonElement.isJsonPrimitive()) {
                String json = jsonElement.getAsString();
                if (!TextUtils.isEmpty(json)) {
                    return GsonUtil.fromJson(json, type);
                }
            }
            return null;
        }
    }
}
