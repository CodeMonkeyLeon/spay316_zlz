package cn.swiftpass.enterprise.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

/**
 * 创建快捷方式
 * */
public class ShortcutUtil
{
    private static final String TAG = ShortcutUtil.class.getSimpleName();
    /**是否创建了快捷方式*/
    public static boolean hasShortcut(Context cx)
    {
        boolean result = false;
        //获取包名
        String title = getPackageName(cx);
        
        final String uriStr;
        //2.2以下的
        if (android.os.Build.VERSION.SDK_INT <= 8)
        {
            uriStr = "content://com.android.launcher.settings/favorites?notify=true";
        }
        else
        {
            uriStr = "content://com.android.launcher2.settings/favorites?notify=true";
        }
        final Uri CONTENT_URI = Uri.parse(uriStr);
        String selection = "title=?";
        final Cursor c = cx.getContentResolver().query(CONTENT_URI, null, selection, new String[] {title}, null);
        if (c != null && c.getCount() > 0)
        {
            result = true;
            c.close();
        }
        
        return result;
    }
    
    /**添加快捷方式*/
   /* public static void addShortcut(Context cx)
    {
        if (android.os.Build.VERSION.SDK_INT < 8)
        {
            delShortcut(cx);
        }
        Intent shortcut = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        Intent shortcutIntent = cx.getPackageManager().getLaunchIntentForPackage(cx.getPackageName());
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        // 快捷方式名称
        String title = getPackageName(cx);
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_NAME, title);
        // 不允许重复创建（不一定有效）
        shortcut.putExtra("duplicate", false);
        // 快捷方式的图标
        Parcelable iconResource = Intent.ShortcutIconResource.fromContext(cx, R.drawable.logo);
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, iconResource);
        cx.sendBroadcast(shortcut);
        
    }*/
    
    /**删除快捷方式*/
    /*public static void delShortcut(Context cx)
    {
        Intent shortcut = new Intent("com.android.launcher.action.UNINSTALL_SHORTCUT");
        // 快捷方式名称
        String title = getPackageName(cx);
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_NAME, title);
        Intent shortcutIntent = cx.getPackageManager().getLaunchIntentForPackage(cx.getPackageName());
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        cx.sendBroadcast(shortcut);
    }*/
    
    /**获取包名*/
    public static String getPackageName(Context cx)
    {
        String title = null;
        try
        {
            
            PackageManager pm = cx.getPackageManager();
            title =
                pm.getApplicationLabel(pm.getApplicationInfo(cx.getPackageName(), PackageManager.GET_META_DATA))
                    .toString();
            
        }
        catch (Exception e)
        {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return title;
    }
}
