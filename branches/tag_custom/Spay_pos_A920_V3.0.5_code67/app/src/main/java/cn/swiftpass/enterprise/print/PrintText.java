package cn.swiftpass.enterprise.print;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;

import com.pax.dal.IPrinter;
import com.pax.dal.exceptions.PrinterDevException;
import com.pax.gl.IGL;
import com.pax.gl.imgprocessing.IImgProcessing;
import com.pax.gl.imgprocessing.IImgProcessing.IPage;
import com.pax.gl.imgprocessing.IImgProcessing.IPage.EAlign;
import com.pax.gl.impl.GLProxy;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalInfo;
import cn.swiftpass.enterprise.bussiness.model.OrderTotalItemInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

public class PrintText {

	private static PrintText printText;
	private IPrinter printer;
	String line = "6";
	String space = "1";
	private static IGL gl;

	private PrintText() {

		printer = GetObj.getDal().getPrinter();
		
		
	}

	public static PrintText getInstance() {
		if (printText == null) {
			printText = new PrintText();
		}
		return printText;
	}

	public void init() {
		try {
			printer.init();

		} catch (PrinterDevException e) {
			e.printStackTrace();

		}
	}
	
	
	
	
	public void printSummary(Context context, OrderTotalInfo info){
		
		gl = new GLProxy(context).getGL();
		IImgProcessing ImgProcessing = gl.getImgProcessing();
		IPage page = ImgProcessing.createPage();

		page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_data_sum), 28, EAlign.CENTER);
		page.addLine().addUnit(" ", 20, EAlign.CENTER);


		page.addLine().addUnit(ToastHelper.toStr(R.string.shop_name) + "：", 24, EAlign.LEFT);
		if (!StringUtil.isEmptyOrNull(MainApplication.getMchName())) {
			page.addLine().addUnit(MainApplication.getMchName(), 24, EAlign.LEFT);
		}

		page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "："+ MainApplication.getMchId(), 24, EAlign.LEFT);
		/*if (!StringUtil.isEmptyOrNull(MainApplication.getMchId())) {
			page.addLine().addUnit(, 24, EAlign.LEFT);
		}*/

		if (StringUtil.isEmptyOrNull(info.getUserName())) {
			//            printBuffer.append("收银员：全部收银员\n");
		} else {
			page.addLine().addUnit(ToastHelper.toStr(R.string.tx_user) + "：", 24, EAlign.LEFT);
			page.addLine().addUnit(info.getUserName(), 24, EAlign.LEFT);
		}

		page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_start_time) + "：" + info.getStartTime(), 24, EAlign.LEFT);
		page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_end_time) + "：" + info.getEndTime(), 24, EAlign.LEFT);

		page.addLine().addUnit("==========================", 28, EAlign.CENTER);

		if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
			page.addLine().addUnit(ToastHelper.toStr(R.string.tx_bill_stream_pay_money) + "：" + DateUtil.formatMoneyUtils(info.getCountTotalFee()) + ToastHelper.toStr(R.string.pay_yuan) , 24, EAlign.LEFT);
		} else {
			page.addLine().addUnit(ToastHelper.toStr(R.string.tx_bill_stream_pay_money) + "：" + MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(info.getCountTotalFee()) , 24, EAlign.LEFT);

		}
		page.addLine().addUnit(ToastHelper.toStr(R.string.tx_bill_stream_pay_money_num) + "：" + info.getCountTotalCount(), 24, EAlign.LEFT);

		if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
			page.addLine().addUnit(ToastHelper.toStr(R.string.tv_refund_dialog_refund_money) + DateUtil.formatMoneyUtils(info.getCountTotalRefundFee()) + ToastHelper.toStr(R.string.pay_yuan), 24, EAlign.LEFT);

		} else {
			page.addLine().addUnit(ToastHelper.toStr(R.string.tv_refund_dialog_refund_money) + MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(info.getCountTotalRefundFee()), 24, EAlign.LEFT);

		}
		page.addLine().addUnit(ToastHelper.toStr(R.string.tx_bill_reufun_total_num) + "：" + info.getCountTotalRefundCount(), 24, EAlign.LEFT);


		if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
			page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_pay_total) + "：" + DateUtil.formatMoneyUtils((info.getCountTotalFee() - info.getCountTotalRefundFee())) + ToastHelper.toStr(R.string.pay_yuan), 24, EAlign.LEFT);

		} else {
			page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_pay_total) + "：" + MainApplication.getFeeFh() + DateUtil.formatMoneyUtils((info.getCountTotalFee() - info.getCountTotalRefundFee())) , 24, EAlign.LEFT);
		}
		page.addLine().addUnit("==========================", 28, EAlign.CENTER);

		/**
		 * 是否开通小费功能，打印小费金额、总计
		 */
		if (MainApplication.isTipOpenFlag()){
			// 小费金额
			page.addLine().addUnit(ToastHelper.toStr(R.string.tip_amount) + "：" + MainApplication.getFeeType() + DateUtil.formatMoneyUtils(info.getCountTotalTipFee()) , 24, EAlign.LEFT);
			//小费笔数
			page.addLine().addUnit(ToastHelper.toStr(R.string.tip_count) + "：" + info.getCountTotalTipFeeCount(), 24, EAlign.LEFT);
			page.addLine().addUnit("==========================", 28, EAlign.CENTER);
		}

		List<OrderTotalItemInfo> list = info.getOrderTotalItemInfo();

		Map<Integer, Object> map = new HashMap<Integer, Object>();

		for (OrderTotalItemInfo itemInfo : list) {

			switch (itemInfo.getPayTypeId()) {

				case 1:
					map.put(0, itemInfo);
					break;
				case 2:
					map.put(1, itemInfo);
					break;

				case 4:
					map.put(2, itemInfo);
					break;
				case 12:
					map.put(3, itemInfo);
					break;
				default:
					map.put(itemInfo.getPayTypeId(), itemInfo);
					break;
			}

		}

		for (Integer key : map.keySet()) {
			OrderTotalItemInfo itemInfo = (OrderTotalItemInfo) map.get(key);
			if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
				page.addLine().addUnit(MainApplication.getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId())) , 24, EAlign.LEFT);
				page.addLine().addUnit(ToastHelper.toStr(R.string.tx_money)+ DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()) + ToastHelper.toStr(R.string.pay_yuan), 24, EAlign.LEFT);
			} else {
				page.addLine().addUnit(MainApplication.getPayTypeMap().get(String.valueOf(itemInfo.getPayTypeId())), 24, EAlign.LEFT);
				page.addLine().addUnit(ToastHelper.toStr(R.string.tx_money)+ MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(itemInfo.getSuccessFee()), 24, EAlign.LEFT);

			}
			page.addLine().addUnit(ToastHelper.toStr(R.string.tv_settle_count)  + "："+ itemInfo.getSuccessCount(), 24, EAlign.LEFT);
		}

		page.addLine().addUnit("-------------------------------------------------------------", 24, EAlign.CENTER);

		page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()), 24, EAlign.LEFT);
		page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_sign) , 24, EAlign.LEFT);

		page.addLine().addUnit(" ", 20, EAlign.CENTER);
		page.addLine().addUnit(" ", 20, EAlign.CENTER);
		page.addLine().addUnit(" ", 20, EAlign.CENTER);

		PrinterTester.getInstance().printBitmap(ImgProcessing.pageToBitmap(page, 410));

		try {
			printer.printStr(" \n", null);
			printer.printStr(" \n", null);
			printer.printStr(" \n", null);
			printer.printStr(" \n", null);
			printer.start();
		} catch (PrinterDevException e) {

			e.printStackTrace();
		}
	}

	public void printStr(Context context, boolean isCashierShow , Order orderModel) {
		try {

			gl = new GLProxy(context).getGL();
			IImgProcessing ImgProcessing = gl.getImgProcessing();
			IPage page = ImgProcessing.createPage();

			if (!orderModel.isPay()) {
				page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_refund_note), 28, EAlign.CENTER);
			}else
			{
				page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_pay_note), 28, EAlign.CENTER);
			}
			page.addLine().addUnit(" ", 20, EAlign.CENTER);

			if(!orderModel.isPay()){//退款

			}else{

				if (MainApplication.isAdmin.equals("0") && !isCashierShow) { //收银员
					page.addLine().addUnit(ToastHelper.toStr(R.string.tx_user) + "：" + MainApplication.realName , 24, EAlign.LEFT);
				}
				if (isCashierShow){
					page.addLine().addUnit(ToastHelper.toStr(R.string.tx_user) + "：" + orderModel.getUserName(), 24, EAlign.LEFT);
				}
			}
			page.addLine().addUnit(orderModel.getPartner(), 24, EAlign.LEFT);
			page.addLine().addUnit(ToastHelper.toStr(R.string.tv_pay_client_save), 24, EAlign.LEFT);
			page.addLine().addUnit("==========================", 28, EAlign.CENTER);

			if (!StringUtil.isEmptyOrNull(MainApplication.getMchName())) {
				page.addLine().addUnit(ToastHelper.toStr(R.string.shop_name), 24, EAlign.LEFT);
				page.addLine().addUnit(MainApplication.getMchName(), 24, EAlign.LEFT);
			}
			if (!StringUtil.isEmptyOrNull(MainApplication.getMchId())) {
				page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "：" + MainApplication.getMchId(), 24, EAlign.LEFT);

			}
			if(!orderModel.isPay()){//退款
				page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_refund_time) + "：", 24, EAlign.LEFT);
				page.addLine().addUnit(orderModel.getAddTimeNew(), 24, EAlign.LEFT);
				page.addLine().addUnit(ToastHelper.toStr(R.string.refund_odd_numbers)+ "：", 24, EAlign.LEFT);
				page.addLine().addUnit(orderModel.getRefundNo(), 24, EAlign.LEFT);

			}else{
				page.addLine().addUnit(ToastHelper.toStr(R.string.tx_bill_stream_time) + "：", 24, EAlign.LEFT);
				page.addLine().addUnit(orderModel.getAddTimeNew(), 24, EAlign.LEFT);
			}

			page.addLine().addUnit(ToastHelper.toStr(R.string.tx_order_no) + ": ", 24, EAlign.LEFT);
			page.addLine().addUnit(orderModel.getOrderNoMch(), 24, EAlign.LEFT);

			if (orderModel.isPay()) {
				if (MainApplication.getPayTypeMap() != null && MainApplication.getPayTypeMap().size() > 0) {
					page.addLine().addUnit(MainApplication.getPayTypeMap().get(orderModel.getApiCode())+ " " + ToastHelper.toStr(R.string.tx_orderno), 24, EAlign.LEFT);
				}
				page.addLine().addUnit(orderModel.getTransactionId() , 24, EAlign.LEFT);


				page.addLine().addUnit(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：", 24, EAlign.LEFT);
				page.addLine().addUnit(orderModel.getTradeName(), 24, EAlign.LEFT);

				page.addLine().addUnit(ToastHelper.toStr(R.string.tx_bill_stream_statr) + "：", 24, EAlign.LEFT);
				page.addLine().addUnit(MainApplication.getTradeTypeMap().get(orderModel.getTradeState() + ""), 24, EAlign.LEFT);

				if (!StringUtil.isEmptyOrNull(orderModel.getAttach())) {
					page.addLine().addUnit(ToastHelper.toStr(R.string.tx_bill_stream_attach) + "：" + " " + orderModel.getAttach() , 24, EAlign.LEFT);
				}

				if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
					if (orderModel.getDaMoney() > 0) {
						long actualMoney = orderModel.getDaMoney() + orderModel.getOrderFee();
						page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_money) + "：", 24, EAlign.LEFT);
						page.addLine().addUnit(DateUtil.formatRMBMoneyUtils(actualMoney) + ToastHelper.toStr(R.string.pay_yuan), 24, EAlign.RIGHT);

					} else {
						page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_money) + "：" , 24, EAlign.LEFT);
						page.addLine().addUnit(DateUtil.formatRMBMoneyUtils(orderModel.getOrderFee()) + ToastHelper.toStr(R.string.pay_yuan), 24, EAlign.RIGHT);

					}
				} else {
					if (MainApplication.isSurchargeOpen()) {
						if(!orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") &&  !orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")){
							page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_money) + "：", 24, EAlign.LEFT);
							page.addLine().addUnit(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getOrderFee()), 24, EAlign.RIGHT);


							page.addLine().addUnit(ToastHelper.toStr(R.string.tx_surcharge) + "：", 24, EAlign.LEFT);
							page.addLine().addUnit(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getSurcharge()), 24, EAlign.RIGHT);
						}
					}

					//Uplan ---V3.0.5迭代新增

					//如果为预授权的支付类型，则不展示任何优惠相关的信息
                    /*因目前SPAY與網關支持的預授權接口僅為‘支付寶’預授權，
                    遂與銀聯Uplan不衝突，遂預授權頁面內，無需處理該優惠信息。*/
					if(!TextUtils.isEmpty(orderModel.getTradeType()) && !orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") &&  !orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")){
						// 优惠详情 --- 如果列表有数据，则展示，有多少条，展示多少条
						if(orderModel.getUplanDetailsBeans() != null && orderModel.getUplanDetailsBeans().size() > 0){
							for(int i =0 ; i<orderModel.getUplanDetailsBeans().size() ; i++){
								String string_Uplan_details;
								//如果是Uplan Discount或者Instant Discount，则采用本地词条，带不同语言的翻译
								if(orderModel.getUplanDetailsBeans().get(i).getDiscountNote().equalsIgnoreCase("Uplan discount")){
									string_Uplan_details = ToastHelper.toStr(R.string.uplan_Uplan_discount) + "：";
								}else if(orderModel.getUplanDetailsBeans().get(i).getDiscountNote().equalsIgnoreCase("Instant Discount")){
									string_Uplan_details = ToastHelper.toStr(R.string.uplan_Uplan_instant_discount) + "：";
								} else{//否则，后台传什么展示什么
									string_Uplan_details = orderModel.getUplanDetailsBeans().get(i).getDiscountNote()+ "：";
								}

								page.addLine().addUnit(string_Uplan_details, 24, EAlign.LEFT);
								page.addLine().addUnit(MainApplication.feeType+ " "+ "-" + orderModel.getUplanDetailsBeans().get(i).getDiscountAmt(), 24, EAlign.RIGHT);
							}
						}

						// 消费者实付金额 --- 如果不为0 ，就展示
						if(orderModel.getCostFee() != 0){
							page.addLine().addUnit(ToastHelper.toStr(R.string.uplan_Uplan_actual_paid_amount) + "：", 24, EAlign.LEFT);
							page.addLine().addUnit(MainApplication.feeType + " "+ DateUtil.formatMoneyUtils(orderModel.getCostFee()), 24, EAlign.RIGHT);
						}
					}


					page.addLine().addUnit(ToastHelper.toStr(R.string.tv_charge_total) + "：", 24, EAlign.LEFT);
					page.addLine().addUnit(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee()), 24, EAlign.RIGHT);

					if (orderModel.getCashFeel() > 0) {
						page.addLine().addUnit(ToastHelper.toStr(R.string.pay_yuan)+ DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()), 24, EAlign.RIGHT);
					}
				}
			}else {
				page.addLine().addUnit(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：", 24, EAlign.LEFT);
				page.addLine().addUnit(orderModel.getTradeName(), 24, EAlign.LEFT);

				if (!StringUtil.isEmptyOrNull(orderModel.getUserName())) {
					page.addLine().addUnit(ToastHelper.toStr(R.string.tv_refund_peop) + "：", 24, EAlign.LEFT);
					page.addLine().addUnit(orderModel.getUserName(), 24, EAlign.LEFT);
				}
				if (MainApplication.getRefundStateMap() != null && MainApplication.getRefundStateMap().size() > 0) {
					page.addLine().addUnit(ToastHelper.toStr(R.string.tv_refund_state) + "：", 24, EAlign.LEFT);
					page.addLine().addUnit(MainApplication.getRefundStateMap().get(orderModel.getRefundState() + ""), 24, EAlign.LEFT);
				}
				if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {

					page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_money) + "：", 24, EAlign.LEFT);
					page.addLine().addUnit(DateUtil.formatRMBMoneyUtils(orderModel.getTotalFee()) + ToastHelper.toStr(R.string.pay_yuan), 24, EAlign.RIGHT);

					page.addLine().addUnit(ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "：", 24, EAlign.LEFT);
					page.addLine().addUnit(DateUtil.formatRMBMoneyUtils(orderModel.getRefundMoney()) + ToastHelper.toStr(R.string.pay_yuan), 24, EAlign.RIGHT);

				} else {

					page.addLine().addUnit(ToastHelper.toStr(R.string.tv_charge_total) + "：", 24, EAlign.LEFT);
					page.addLine().addUnit(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee()), 24, EAlign.RIGHT);

					page.addLine().addUnit(ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "：", 24, EAlign.LEFT);
					page.addLine().addUnit(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getRefundMoney()), 24, EAlign.RIGHT);

					if(orderModel.getCashFeel() > 0){
						page.addLine().addUnit(ToastHelper.toStr(R.string.pay_yuan)+ DateUtil.formatMoneyUtils(orderModel.getCashFeel()), 24, EAlign.RIGHT);
					}
				}
				if (!StringUtil.isEmptyOrNull(orderModel.getPrintInfo())) {
					page.addLine().addUnit(orderModel.getPrintInfo(), 24, EAlign.LEFT);
				}
			}

			if(!StringUtil.isEmptyOrNull(orderModel.getOrderNoMch()) && orderModel.isPay()){
				//打印小票的代码屏蔽
				Bitmap c = CreateOneDiCodeUtilPrint.createCode(orderModel.getOrderNoMch(), 260, 260);
				page.addLine().addUnit(c, EAlign.CENTER);
				page.addLine().addUnit(ToastHelper.toStr(R.string.refound_QR_code), 24, EAlign.CENTER);
			}

			page.addLine().addUnit("-------------------------------------------------------------", 24, EAlign.CENTER);

			page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) , 24, EAlign.LEFT);

			if (orderModel.getPartner().equalsIgnoreCase(ToastHelper.toStr(R.string.tv_pay_mch_stub))
					|| orderModel.getPartner().equalsIgnoreCase(ToastHelper.toStr(R.string.tv_pay_user_stub))) {

				page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_sign) + "：", 24, EAlign.LEFT);
				page.addLine().addUnit(" ", 20, EAlign.CENTER);
				page.addLine().addUnit(" ", 20, EAlign.CENTER);
				page.addLine().addUnit(" ", 20, EAlign.CENTER);

			} else {
				page.addLine().addUnit(" ", 20, EAlign.CENTER);
				page.addLine().addUnit(" ", 20, EAlign.CENTER);
				page.addLine().addUnit(" ", 20, EAlign.CENTER);

			}

			PrinterTester.getInstance().printBitmap(ImgProcessing.pageToBitmap(page, 410));

			printer.printStr(" \n", null);
			printer.printStr(" \n", null);
			printer.printStr(" \n", null);
			printer.printStr(" \n", null);
			printer.start();

		} catch (PrinterDevException e) {
			e.printStackTrace();

		}

	}

	/**
	 * printType参数1：预授权成功---预授权小票(反扫)
	 *  * printType参数5：预授权成功---预授权小票(正扫)
	 * printType参数4：预授权订单详情---预授权小票
	 * printType参数2：预授权解冻详情小票
	 * printType参数3：预授权解冻完成小票
	 */
	public  void printPreAuthStr(Context context, int printType, Order orderModel) {
		int minus=1;
		//根据当前的小数点的位数来判断应该除以或者乘以多少
		for(int i = 0; i < MainApplication.numFixed ; i++ ){
			minus = minus * 10;
		}
		try {
			gl = new GLProxy(context).getGL();
			IImgProcessing ImgProcessing = gl.getImgProcessing();
			IPage page = ImgProcessing.createPage();

			switch (printType){
				case 1:
				case 4:
				case 5:
					page.addLine().addUnit(ToastHelper.toStr(R.string.pre_auth_receipt), 28, EAlign.CENTER);
					break;

				case 2:
				case 3:
					page.addLine().addUnit(ToastHelper.toStr(R.string.pre_auth_unfreezed_receipt), 28, EAlign.CENTER);
					break;
			}
			page.addLine().addUnit(" ", 20, EAlign.CENTER);

			page.addLine().addUnit(orderModel.getPartner(), 24, EAlign.LEFT);
			page.addLine().addUnit(ToastHelper.toStr(R.string.tv_pay_client_save), 24, EAlign.LEFT);
			page.addLine().addUnit("==========================", 28, EAlign.CENTER);

			if (!StringUtil.isEmptyOrNull(MainApplication.getMchName())) {
				page.addLine().addUnit(ToastHelper.toStr(R.string.shop_name), 24, EAlign.LEFT);
				page.addLine().addUnit(MainApplication.getMchName(), 24, EAlign.LEFT);
			}
			if (!StringUtil.isEmptyOrNull(MainApplication.getMchId())) {
				page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "：" + MainApplication.getMchId(), 24, EAlign.LEFT);
			}

			if (printType==1) {
				page.addLine().addUnit(ToastHelper.toStr(R.string.pre_auth_time)+ "：", 24, EAlign.LEFT);
				page.addLine().addUnit(orderModel.getTradeTime(), 24, EAlign.LEFT);
			}else if(printType==4){
				page.addLine().addUnit(ToastHelper.toStr(R.string.pre_auth_time)+ "：", 24, EAlign.LEFT);
				page.addLine().addUnit(orderModel.getTradeTimeNew(), 24, EAlign.LEFT);
			}else if(printType==5){
				page.addLine().addUnit(ToastHelper.toStr(R.string.pre_auth_time)+ "：", 24, EAlign.LEFT);
				page.addLine().addUnit(orderModel.getTimeEnd(), 24, EAlign.LEFT);
			} else if(printType==2){
				page.addLine().addUnit(ToastHelper.toStr(R.string.unfreezed_time) + "：", 24, EAlign.LEFT);
				page.addLine().addUnit(orderModel.getOperateTime(), 24, EAlign.LEFT);
			}else if(printType==3){
				page.addLine().addUnit(ToastHelper.toStr(R.string.unfreezed_time) + "：", 24, EAlign.LEFT);
				page.addLine().addUnit(orderModel.getUnFreezeTime(), 24, EAlign.LEFT);
			}

			if (printType==1 || printType == 4 || printType == 5){
				page.addLine().addUnit(ToastHelper.toStr(R.string.platform_pre_auth_order_id) + "：", 24, EAlign.LEFT);//平台预授权订单号
				page.addLine().addUnit(orderModel.getAuthNo(), 24, EAlign.LEFT);//平台预授权订单号

				String language = PreferenceUtil.getString("language","");
				Locale locale = MainApplication.getContext().getResources().getConfiguration().locale; //zh-rHK
				String lan = locale.getCountry();
				if (!TextUtils.isEmpty(language)) {
					if (language.equals(MainApplication.LANG_CODE_EN_US)) {
						page.addLine().addUnit(MainApplication.getPayTypeMap().get("2") + " "+ ToastHelper.toStr(R.string.tx_orderno) + "：", 24, EAlign.LEFT);//支付宝单号
					} else {
						page.addLine().addUnit(MainApplication.getPayTypeMap().get("2") + ToastHelper.toStr(R.string.tx_orderno) + "：", 24, EAlign.LEFT);//支付宝单号
					}
				} else {
					if (lan.equalsIgnoreCase("en")) {
						page.addLine().addUnit(MainApplication.getPayTypeMap().get("2") + " "+ ToastHelper.toStr(R.string.tx_orderno) + "：", 24, EAlign.LEFT);//支付宝单号
					} else {

						page.addLine().addUnit(MainApplication.getPayTypeMap().get("2") + ToastHelper.toStr(R.string.tx_orderno) + "：", 24, EAlign.LEFT);//支付宝单号
					}
				}

				if(printType==1 || printType == 5){
					page.addLine().addUnit(orderModel.getOutTransactionId(), 24, EAlign.LEFT);
				}

				if(printType== 4){
					page.addLine().addUnit(orderModel.getTransactionId(), 24, EAlign.LEFT);
				}

			}else if(printType == 2 || printType== 3){
				page.addLine().addUnit(ToastHelper.toStr(R.string.unfreezed_order_id) + "：", 24, EAlign.LEFT);//解冻订单号
				page.addLine().addUnit(orderModel.getOutRequestNo() , 24, EAlign.LEFT);
				page.addLine().addUnit(ToastHelper.toStr(R.string.platform_pre_auth_order_id) + "：", 24, EAlign.LEFT);//平台预授权订单号
				page.addLine().addUnit(orderModel.getAuthNo(), 24, EAlign.LEFT);
			}

			if(printType == 5){
				String tradename;
				if (!isAbsoluteNullStr(orderModel.getTradeName())){//先判断大写的Name，再判断小写name
					tradename=orderModel.getTradeName();
				}else {
					tradename=orderModel.getTradename();
				}
				page.addLine().addUnit(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：", 24, EAlign.LEFT);
				page.addLine().addUnit(tradename , 24, EAlign.LEFT);

			}else{
				page.addLine().addUnit(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：", 24, EAlign.LEFT);
				page.addLine().addUnit(orderModel.getTradeName(), 24, EAlign.LEFT);//支付方式
			}

			if (printType == 3) {
				page.addLine().addUnit((ToastHelper.toStr(R.string.tv_unfreezen_applicant) + "："), 24, EAlign.LEFT);//办理人
				page.addLine().addUnit(orderModel.getUserName(), 24, EAlign.LEFT);
			}

			page.addLine().addUnit(ToastHelper.toStr(R.string.tx_bill_stream_statr) + "：", 24, EAlign.LEFT);//订单状态
			String operationType = "";
			if(printType == 1 || printType == 4){
				operationType = MainApplication.getPreAuthtradeStateMap().get(orderModel.getTradeState() + "");

			}else if(printType == 2){
				switch (orderModel.getOperationType()){
					case 2:
						operationType = ToastHelper.toStr(R.string.freezen_success);
						break;
				}
			}else if(printType == 3){
				operationType = ToastHelper.toStr(R.string.unfreezing);
			}else if (printType == 5){
				operationType = ToastHelper.toStr(R.string.pre_auth_authorized);
			}
			page.addLine().addUnit(operationType, 24, EAlign.LEFT);

			switch (printType){
				case 1:
				case 4:
				case 5:
					if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))){
						page.addLine().addUnit(ToastHelper.toStr(R.string.pre_auth_amount)+":", 24, EAlign.LEFT);
						page.addLine().addUnit(DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan), 24, EAlign.RIGHT);
					}else{
						long preAuthorizationMoney=0;//用来接收printType判断授权金额来自反扫、正扫
						if (printType==1){
							preAuthorizationMoney=orderModel.getRestAmount();//反扫
						}else if (printType==5){
							preAuthorizationMoney=orderModel.getTotalFee();//正扫
						}else if(printType==4){
							preAuthorizationMoney=orderModel.getTotalFreezeAmount();//预授权详情
						}
						page.addLine().addUnit(ToastHelper.toStr(R.string.pre_auth_amount)+":", 24, EAlign.LEFT);//预授权金额
						page.addLine().addUnit( MainApplication.feeType + DateUtil.formatMoneyUtils(preAuthorizationMoney), 24, EAlign.RIGHT);//预授权金额

					}

					page.addLine().addUnit(" ", 20, EAlign.CENTER);
					page.addLine().addUnit(" ", 20, EAlign.CENTER);

					//二维码
					//打印小票的代码
					Bitmap c = CreateOneDiCodeUtilPrint.createCode(orderModel.getAuthNo(), 260, 260);
					page.addLine().addUnit(c, EAlign.CENTER);
					page.addLine().addUnit(orderModel.getPrintInfo(), 24, EAlign.LEFT);
					page.addLine().addUnit(ToastHelper.toStr(R.string.scan_to_check) , 24, EAlign.CENTER);//此二维码用于预授权收款、解冻操作

					break;
				case 2:
					if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))){
						page.addLine().addUnit(ToastHelper.toStr(R.string.unfreezing_amount)+":", 24, EAlign.LEFT);
						page.addLine().addUnit( DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan), 24, EAlign.RIGHT);

					}else{
						page.addLine().addUnit(ToastHelper.toStr(R.string.unfreezing_amount)+":", 24, EAlign.LEFT);
						page.addLine().addUnit( MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getMoney()), 24, EAlign.RIGHT);

					}
					page.addLine().addUnit(ToastHelper.toStr(R.string.unfreeze) , 24, EAlign.LEFT);//解冻将原路返回支付账户或银行卡，到账时间已第三方为准
					break;
				case 3:
					//授权金额
					if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))){
						page.addLine().addUnit(ToastHelper.toStr(R.string.pre_auth_amount)+":", 24, EAlign.LEFT);
						page.addLine().addUnit(DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan), 24, EAlign.RIGHT);

					}else{
						page.addLine().addUnit(ToastHelper.toStr(R.string.pre_auth_amount)+":", 24, EAlign.LEFT);
						page.addLine().addUnit(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFreezeAmount()), 24, EAlign.RIGHT);
					}
					//解冻金额
					if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))){
						page.addLine().addUnit(ToastHelper.toStr(R.string.unfreezing_amount)+":", 24, EAlign.LEFT);
						page.addLine().addUnit( DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan), 24, EAlign.RIGHT);

					}else{
						page.addLine().addUnit(ToastHelper.toStr(R.string.unfreezing_amount)+":", 24, EAlign.LEFT);
						page.addLine().addUnit(MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee()), 24, EAlign.RIGHT);

					}
					page.addLine().addUnit(ToastHelper.toStr(R.string.unfreeze), 24, EAlign.LEFT);//解冻将原路返回支付账户或银行卡，到账时间已第三方为准

					break;
				default:
					break;
			}
			page.addLine().addUnit("-------------------------------------------------------------", 24, EAlign.CENTER);

			page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) , 24, EAlign.LEFT);

			if (orderModel.getPartner().equalsIgnoreCase(ToastHelper.toStr(R.string.tv_pay_mch_stub))
					|| orderModel.getPartner().equalsIgnoreCase(ToastHelper.toStr(R.string.tv_pay_user_stub))) {

				page.addLine().addUnit(ToastHelper.toStr(R.string.tx_blue_print_sign) + "：", 24, EAlign.LEFT);
				page.addLine().addUnit(" ", 20, EAlign.CENTER);
				page.addLine().addUnit(" ", 20, EAlign.CENTER);
				page.addLine().addUnit(" ", 20, EAlign.CENTER);

			} else {
				page.addLine().addUnit(" ", 20, EAlign.CENTER);
				page.addLine().addUnit(" ", 20, EAlign.CENTER);
				page.addLine().addUnit(" ", 20, EAlign.CENTER);

			}

			PrinterTester.getInstance().printBitmap(ImgProcessing.pageToBitmap(page, 410));

			printer.printStr(" \n", null);
			printer.printStr(" \n", null);
			printer.printStr(" \n", null);
			printer.printStr(" \n", null);
			printer.start();


		} catch (PrinterDevException e) {
			e.printStackTrace();

		}
	}

	public static boolean isAbsoluteNullStr(String str) {
		str = deleteBlank(str);
		if (str == null || str.length() == 0 || "null".equalsIgnoreCase(str)) {
			return true;
		}

		return false;
	}

	/**
	 * 去前后空格和去换行符
	 *
	 * @param str
	 * @return
	 */
	public static String deleteBlank(String str) {
		if (null != str && 0 < str.trim().length()) {
			char[] array = str.toCharArray();
			int start = 0, end = array.length - 1;
			while (array[start] == ' ') start++;
			while (array[end] == ' ') end--;
			return str.substring(start, end + 1).replaceAll("\n", "");

		} else {
			return "";
		}

	}

	public String start() {

		try {
			printer.start();
		} catch (PrinterDevException e) {

			e.printStackTrace();
		}
		return "";

	}

	public String statusCode2Str(int status) {
		String res = "";
		switch (status) {
		case 0:
			res = "Success ";
			break;
		case 1:
			res = "Printer is busy ";
			break;
		case 2:
			res = "Out of paper ";
			break;
		case 3:
			res = "The format of print data packet error ";
			break;
		case 4:
			res = "Printer malfunctions ";
			break;
		case 8:
			res = "Printer over heats ";
			break;
		case 9:
			res = "Printer voltage is too low";
			break;
		case 240:
			res = "Printing is unfinished ";
			break;
		case 252:
			res = " The printer has not installed font library ";
			break;
		case 254:
			res = "Data package is too long ";
			break;
		default:
			break;
		}
		return res;
	}

}
