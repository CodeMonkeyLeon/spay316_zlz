package cn.swiftplus.enterprise.networksdk;

import cn.swiftplus.enterprise.networksdk.response.CommonJsonCallback;
import cn.swiftplus.enterprise.networksdk.response.ResposeDataHandle;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Created by aijingya on 2020/11/19.
 *
 * @Package cn.swiftplus.enterprise.networksdk
 * @Description:  网络请求工具类
 * @date 2020/11/19.14:03.
 */
public class OkHttpClientManager {
    private static final String TAG = "OkHttpClientManager";
    public static final long DEFAULT_MILLISECONDS = 10000L;
    private volatile static OkHttpClientManager mInstance;
    private static OkHttpClient mOkHttpClient;

    public OkHttpClientManager(OkHttpClient okHttpClient)
    {
        if (okHttpClient == null) {
            mOkHttpClient = new OkHttpClient();
        } else {
            mOkHttpClient = okHttpClient;
        }
    }


    public static OkHttpClientManager initClient(OkHttpClient okHttpClient)
    {
        if (mInstance == null)
        {
            synchronized (OkHttpClientManager.class)
            {
                if (mInstance == null)
                {
                    mInstance = new OkHttpClientManager(okHttpClient);
                }
            }
        }
        return mInstance;
    }

    public static OkHttpClientManager getInstance()
    {
        return initClient(null);
    }



    /**
     * GET请求
     */
    public static Call get(Request request, ResposeDataHandle handle) {
        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new CommonJsonCallback(handle));
        return call;
    }

    /**
     * POST请求
     */
    public static Call post(Request request, ResposeDataHandle handle) {
        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new CommonJsonCallback(handle));
        return call;
    }


}
