package cn.swiftpass.enterprise.ui.widget.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.common.sp.PreferenceUtil;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;

/**
 * Created by aijingya on 2018/11/30.
 *
 * @Package cn.swiftpass.enterprise.ui.widget.dialog
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/11/30.14:42.
 */

public class ChangePreauthDialog extends Dialog {
    private static final String TAG = ChangePreauthDialog.class.getSimpleName();

    private TextView tv_message;
    private LinearLayout ll_select_item1,ll_select_item2,ll_select_item3;
    private TextView tv_select_01,tv_select_02,tv_select_03;
    private ImageView iv_select_01,iv_select_02,iv_select_03;
    private TextView confirm,cancel;
    private View view_line2,view_line3;

    private Context mContext;
    private View.OnClickListener confirmButtonListener;
    private String selectType;
    private PageTypeEnum PageType; //是收款页面还是账单页面
    public enum PageTypeEnum {
        PAY,//收款页面
        BILL;//账单页面
    }

    public ChangePreauthDialog(Activity context ,PageTypeEnum showPage ,View.OnClickListener ButtonListener) {
        super(context, R.style.simpleDialog);
        confirmButtonListener = ButtonListener;
        mContext = context;
        PageType = showPage;
        init(context);
        setCancelable(true);
    }

    private void init(Context context) {
        View rootView = View.inflate(context, R.layout.dialog_choose_pre_auth_layout, null);
        setContentView(rootView);

        tv_message = (TextView)rootView.findViewById(R.id.tv_message);

        ll_select_item1 = (LinearLayout)rootView.findViewById(R.id.ll_select_item1);
        ll_select_item2 = (LinearLayout)rootView.findViewById(R.id.ll_select_item2);
        ll_select_item3 = (LinearLayout)rootView.findViewById(R.id.ll_select_item3);

        tv_select_01 = (TextView) rootView.findViewById(R.id.tv_select_01);
        tv_select_02 = (TextView) rootView.findViewById(R.id.tv_select_02);
        tv_select_03 = (TextView) rootView.findViewById(R.id.tv_select_03);

        iv_select_01 = (ImageView) rootView.findViewById(R.id.iv_select_01);
        iv_select_02 = (ImageView) rootView.findViewById(R.id.iv_select_02);
        iv_select_03 = (ImageView) rootView.findViewById(R.id.iv_select_03);

        view_line2 = rootView.findViewById(R.id.view_line2);
        view_line3 = rootView.findViewById(R.id.view_line3);

        confirm = rootView.findViewById(R.id.confirm);
        cancel = rootView.findViewById(R.id.cancel);

        //读取上一次选择的是什么状态，然后根据状态设置title，默认是sale
        if(PageType == PageTypeEnum.PAY){ //收款页面
            selectType = PreferenceUtil.getString("choose_pay_or_pre_auth", "sale");

            //收款页面隐藏这个选项
            ll_select_item3.setVisibility(View.GONE);
            view_line3.setVisibility(View.GONE);

        }else if(PageType == PageTypeEnum.BILL){ //账单页面
            selectType = PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth", "sale");

            //如果商户已开通预授权
            if (MainApplication.getInstance().getUserInfo().isAuthFreezeOpen == 1) {
                ll_select_item2.setVisibility(View.VISIBLE);
                view_line2.setVisibility(View.VISIBLE);
            } else {
                ll_select_item2.setVisibility(View.GONE);
                view_line2.setVisibility(View.GONE);
            }

            //账单页面显示这个选项
            if (MainApplication.getInstance().hasCardPayment()) {
                ll_select_item3.setVisibility(View.VISIBLE);
                view_line3.setVisibility(View.VISIBLE);
            } else {
                ll_select_item3.setVisibility(View.GONE);
                view_line3.setVisibility(View.GONE);
            }

        }

        if(TextUtils.equals(selectType,"sale")){//如果选择的是sale
            iv_select_01.setSelected(true);
            iv_select_02.setSelected(false);
            iv_select_03.setSelected(false);
        }else if(TextUtils.equals(selectType,"pre_auth")){//如果选择的是预授权pre_auth
            iv_select_01.setSelected(false);
            iv_select_02.setSelected(true);
            iv_select_03.setSelected(false);
        } else if(TextUtils.equals(selectType,"card_payment")){//如果选择的是卡交易通道
            iv_select_01.setSelected(false);
            iv_select_02.setSelected(false);
            iv_select_03.setSelected(true);
        }

        confirm.setOnClickListener(confirmButtonListener);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        ll_select_item1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_select_01.setSelected(true);
                iv_select_02.setSelected(false);
                iv_select_03.setSelected(false);
                selectType = "sale";
            }
        });

        ll_select_item2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_select_01.setSelected(false);
                iv_select_02.setSelected(true);
                iv_select_03.setSelected(false);
                selectType = "pre_auth";
            }
        });

        ll_select_item3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_select_01.setSelected(false);
                iv_select_02.setSelected(false);
                iv_select_03.setSelected(true);

                selectType = "card_payment";
            }
        });
    }

    public void  setMessage(String message){
        if(!TextUtils.isEmpty(message)){
            tv_message.setText(message);
        }else{
            Toast.makeText(mContext,"with no message",Toast.LENGTH_SHORT).show();
        }
    }

    public String getSelectType(){
        return selectType;
    }

}
