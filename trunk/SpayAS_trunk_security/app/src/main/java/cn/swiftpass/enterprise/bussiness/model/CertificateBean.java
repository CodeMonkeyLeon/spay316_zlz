package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by aijingya on 2020/4/1.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: 获取证书公钥接口，返回的证书信息(用一句话描述该文件做什么)
 * @date 2020/4/1.16:04.
 */
public class CertificateBean implements Serializable {
    public String publicKey; //证书公钥
    public String beginDate;//生效时间
    public String endDate;//失效时间

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
