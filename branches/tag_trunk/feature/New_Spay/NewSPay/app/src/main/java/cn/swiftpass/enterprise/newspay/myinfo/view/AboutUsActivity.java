package cn.swiftpass.enterprise.newspay.myinfo.view;


import android.os.Bundle;
import android.view.View;

import butterknife.BindView;
import cn.swiftpass.enterprise.R;
import cn.swiftpass.enterprise.base.BaseActivity;
import cn.swiftpass.enterprise.widget.Toolbar;

public class AboutUsActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_about_us;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        mToolbar.setCenterTitle(getString(R.string.title_aboutus), 0);
        mToolbar.setLeftTitle(null, R.mipmap.toolbar_back);
        mToolbar.setLeftTitleVisibility(View.VISIBLE);
        initListener();
    }

    private void initListener() {
        mToolbar.setOnToolBarClickListener(new Toolbar.OnToolBarClickListener() {
            @Override
            public void onLeftListener() {
                finish();
            }

            @Override
            public void onRightListener() {

            }
        });
    }
}
