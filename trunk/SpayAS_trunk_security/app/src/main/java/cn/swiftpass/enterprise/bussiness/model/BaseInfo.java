package cn.swiftpass.enterprise.bussiness.model;

import java.util.HashMap;

/**
 * Created by congwei.li on 2022/4/25.
 *
 * @Description:
 */
public class BaseInfo {
    //{
    //	"trade_state": {
    //		"1": "未支付",
    //		"2": "收款成功",
    //		"3": "已關閉",
    //		"4": "轉入退款",
    //		"7": "沖正中",
    //		"8": "已撤銷",
    //		"9": "已撤銷"
    //	},
    //	"pay_type": {
    //		"44": "NambaPay",
    //		"25": "銀聯二維碼",
    //		"47": "XNAP & Other e-Wallets",
    //		"26": "轉數快",
    //		"29": "GCash",
    //		"999": "未知",
    //		"51": "Tiqmo",
    //		"30": "Swallet",
    //		"52": "Bank Payment/Instapay",
    //		"32": "HomeCredit",
    //		"54": "QR Ph",
    //		"55": "Alipay+",
    //		"56": "微信支付",
    //		"35": "微信支付",
    //		"57": "ShopeePay",
    //		"36": "輕錢包",
    //		"58": "MasterCard",
    //		"37": "Equicom",
    //		"38": "GrabPay",
    //		"39": "Kabu",
    //		"18": "微信支付香港钱包",
    //		"1": "微信支付",
    //		"2": "支付寶",
    //		"40": "Santander",
    //		"62": "Instapay",
    //		"41": "HelloMoney",
    //		"63": "GCash",
    //		"20": "銀聯二維碼",
    //		"42": "Mpay",
    //		"65": "PayMe"
    //	},
    //	"refund_state": {
    //		"0": "審核中",
    //		"1": "退款成功",
    //		"2": "退款失敗",
    //		"3": "退款成功",
    //		"5": "退款成功"
    //	},
    //	"card_trade_type": {
    //		"11": "預授權撤銷沖正",
    //		"12": "預授權完成沖正",
    //		"13": "預授權完成撤銷沖正",
    //		"1": "消費",
    //		"2": "消費撤銷",
    //		"3": "預授權",
    //		"4": "預授權撤銷",
    //		"5": "退款",
    //		"6": "預授權完成",
    //		"7": "預授權完成撤銷",
    //		"8": "消費沖正",
    //		"9": "消費撤銷沖正",
    //		"10": "預授權沖正"
    //	}
    //}

    public HashMap<String, String> trade_state;
    public HashMap<String, String> pay_type;
    public HashMap<String, String> refund_state;
    public HashMap<String, String> card_trade_type;
}
