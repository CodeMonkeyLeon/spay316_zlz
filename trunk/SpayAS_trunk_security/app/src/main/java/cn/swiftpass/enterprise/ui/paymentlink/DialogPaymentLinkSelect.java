package cn.swiftpass.enterprise.ui.paymentlink;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.fmt.BaseDialogFragment;

/**
 * Created by congwei.li on 2021/9/14.
 *
 * @Description: payment link列表类型切换，包含order、customer、product
 */
public class DialogPaymentLinkSelect extends BaseDialogFragment {

    private static final String TAG = "PaymentLinkSelectFragment";
    private OnItemClickListener clickListener;

    public void setClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public static DialogPaymentLinkSelect newInstance() {
        DialogPaymentLinkSelect fragment = new DialogPaymentLinkSelect();
        return fragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.dialog_select_payment_link;
    }

    @Override
    protected void init(View view, Bundle savedInstanceState) {
        TextView selectOrder = view.findViewById(R.id.tv_select_order);
        TextView selectCustomer = view.findViewById(R.id.tv_select_customer);
        TextView selectProduct = view.findViewById(R.id.tv_select_product);
        TextView cancel = view.findViewById(R.id.tv_select_cancel);

        selectOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null) {
                    clickListener.onSelectOrder();
                }
                dismiss();
            }
        });

        selectCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null) {
                    clickListener.onSelectCustomer();
                }
                dismiss();
            }
        });

        selectProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null) {
                    clickListener.onSelectProduct();
                }
                dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    public interface OnItemClickListener {
        void onSelectOrder();

        void onSelectCustomer();

        void onSelectProduct();
    }

    public void show(FragmentManager manager) {
        show(manager, TAG);
    }
}
