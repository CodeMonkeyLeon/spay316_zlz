package cn.swiftpass.enterprise.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.common.sentry.SentryUtils;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;

/**
 * Created by congwei.li on 2021/9/17.
 *
 * @Description:
 */
public class DialogUtils {


    private static final String TAG = "DialogUtils";

    private static Dialog dialogNew = null;

    /**
     * 只带内容
     *
     * @param context
     * @param content
     * @param handleBtn
     */
    public static void toastDialog(final Activity context, final String content, final NewDialogInfo.HandleBtn handleBtn) {
        context.runOnUiThread(new Runnable() {

            @Override
            public void run() {

                try {
                    if (StringUtil.isEmptyOrNull(content)) {
                        return;
                    }

                    NewDialogInfo info = new NewDialogInfo(context, content, null, 2, null, handleBtn);
                    info.setOnKeyListener(new DialogInterface.OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                            if (keycode == KeyEvent.KEYCODE_BACK) {
                                return true;
                            }
                            return false;
                        }
                    });
                    DialogHelper.resize(context, info);

                    info.show();
                } catch (Exception e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
        });

    }

    public static void loadDialog(Activity mActivity, int res) {
        if (mActivity == null) {
            return;
        }
        // 加载样式
        try {

            if (null == dialogNew) {
                dialogNew = new Dialog(mActivity, R.style.my_dialog);
                dialogNew.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogNew.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialogNew.setCanceledOnTouchOutside(false);
                LayoutInflater inflater = LayoutInflater.from(mActivity);
                View v = inflater.inflate(R.layout.load_progress_info, null);// 得到加载view dialog_common
                TextView tv = (TextView) v.findViewById(R.id.tv_dialog);
                tv.setText(res);
                // dialog透明
                WindowManager.LayoutParams lp = dialogNew.getWindow().getAttributes();
                lp.alpha = 0.8f;
                dialogNew.getWindow().setAttributes(lp);
                dialogNew.setContentView(v);
                dialogNew.setOnKeyListener(new DialogInterface.OnKeyListener() {

                    @Override
                    public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                        if (keycode == KeyEvent.KEYCODE_BACK) {
                            return true;
                        }
                        return false;
                    }
                });
            }
            DialogHelper.resizeNew(mActivity, dialogNew);
            dialogNew.show();
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
    }

    public static void dismissDialog(Activity mActivity) {
        if (mActivity == null) {
            return;
        }
        if (null != dialogNew) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        dialogNew.dismiss();
                        dialogNew = null;
                    } catch (Exception e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                        Log.e(TAG, Log.getStackTraceString(e));
                    }
                }
            });
        }
    }
}
