package com.cxy.networklibrary;

import android.app.Application;

import com.cxy.networklibrary.cache.CacheConfig;
import com.cxy.networklibrary.cache.CacheManager;
import com.cxy.networklibrary.di.component.AppComponent;
import com.cxy.networklibrary.di.component.DaggerAppComponent;
import com.cxy.networklibrary.http.HttpConfig;
import com.cxy.networklibrary.http.HttpManager;
import com.cxy.networklibrary.other.ActivityListManager;
import com.cxy.networklibrary.other.LogUtil;
import com.cxy.networklibrary.other.OtherConfig;
import com.cxy.networklibrary.other.PermissionManager;
import com.cxy.networklibrary.util.ToastUtil;
import com.trello.rxlifecycle2.internal.Preconditions;

/**
 * 创建人：caoxiaoya
 * 时间：2018/6/13
 * 描述：核心模块（网络请求，数据库，图片加载，事件总线，缓存）的供应Module
 * 备注：
 */

public class CoreComponent {

    private static AppComponent mAppComponent;


    /**
     * 初始化操作
     */
    public static void init(Application application) {
        mAppComponent = DaggerAppComponent.builder().application(application).build();//如果提示找不到DaggerRingComponent类，请重新编译下项目。
        application.registerActivityLifecycleCallbacks(mAppComponent.activityLifeCallback());
    }

    /**
     * 开始构建
     */
    public static void create() {

        //其他模块的构建工作
        //崩溃日志
        if (mAppComponent.otherConfig().isUseCrashDiary()) {
            mAppComponent.crashDiary().init(mAppComponent.application(), mAppComponent.otherConfig().getCrashDiaryFolder());
        }
        //LogUtil
        LogUtil.init(mAppComponent.otherConfig().isShowLog());
        //ToastUtil
        ToastUtil.init(mAppComponent.application());
    }

    /**
     * 获取RingComponent，从而获取RingComponent中提供的各对象。
     */
    public static AppComponent ringComponent() {
        return Preconditions.checkNotNull(mAppComponent, "RingComponent为空，请先在Application中调用DevRing.init(Application)方法进行初始化");
    }


    /**
     * 配置缓存模块
     */
    public static CacheConfig configureCache() {
        return mAppComponent.cacheConfig();
    }

    /**
     * 获取缓存管理者
     */
    public static CacheManager cacheManager() {
        return mAppComponent.cacheManager();
    }

    /**
     * 配置网络请求模块
     */
    public static HttpConfig configureHttp() {
        return mAppComponent.httpConfig();
    }

    /**
     * 获取网络请求管理者
     */
    public static HttpManager httpManager() {
        return mAppComponent.httpManager();
    }

    /**
     * 配置其他模块
     */
    public static OtherConfig configureOther() {
        return mAppComponent.otherConfig();
    }

    public static ActivityListManager activityListManager() {
        return mAppComponent.activityListManager();
    }

    /**
     * 获取权限管理者
     */
//    public static PermissionManager permissionManager() {
//        return mAppComponent.permissionManager();
//    }

    /**
     * 获取Application
     */
    public static Application application() {
        return mAppComponent.application();
    }
}
