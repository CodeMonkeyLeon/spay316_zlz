package cn.swiftpass.enterprise.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.ShopModel;
import cn.swiftpass.enterprise.ui.widget.ItemShop;

/**
 * 店铺列表适配
 * 暂无用 目前不考虑多家店铺
 * User: Alan
 * Date: 13-12-23
 * Time: 下午4:29
 */
public class ShopAdapter extends ArrayListAdapter<ShopModel>
{
    
    public ShopAdapter(Context context)
    {
        super(context);
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ItemShop itemShop = null;
        if (convertView != null && convertView instanceof ItemShop)
        {
            itemShop = (ItemShop)convertView;
        }
        if (convertView == null)
        {
            itemShop = (ItemShop)LayoutInflater.from(mContext).inflate(R.layout.listitem_shop, null);
        }
        ShopModel model = (ShopModel)getItem(position);
        itemShop.setData(model);
        return itemShop;
    }
}
