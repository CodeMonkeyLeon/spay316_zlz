package cn.swiftpass.enterprise.base.mvp;

/**
 * 创建人：caoxiaoya
 * 时间：2019/4/28
 * 描述：本质是用来约束所有View
 * 备注：
 */
public interface IView extends BaseView{

    /**
     * 数据获取失败
     * @param fromRequest
     * @param code
     * @param errMsg
     */
    void onError(int fromRequest,String code,String errMsg);

}
