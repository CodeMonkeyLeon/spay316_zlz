package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Alan
 * Date: 13-9-21
 * Time: 下午9:08
 * To change this template use File | Settings | File Templates.
 */
public class UpgradeInfo implements Serializable
{
    public int version;
    
    public String versionName;
    
    public long dateTime;
    
    public String message;
    
    public boolean mustUpgrade;
    
    public long download;
    
    public String url;
    
    public boolean isMoreSetting = false;
    
}
