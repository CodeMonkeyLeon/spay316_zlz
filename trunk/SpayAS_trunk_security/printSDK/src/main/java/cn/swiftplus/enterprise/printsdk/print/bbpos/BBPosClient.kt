package cn.swiftplus.enterprise.printsdk.print.bbpos

import android.content.Context
import android.text.TextUtils
import android.util.Log
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import java.lang.ref.WeakReference
import java.net.Inet4Address
import java.net.InetAddress
import java.net.NetworkInterface
import java.nio.charset.Charset
import java.util.Collections
import kotlin.concurrent.thread

object BBPosClient {

    /**
     * 使用WeakReference防止内存泄漏
     */
    private var mContextRef: WeakReference<Context>? = null


    private var mLocalIp = ""

    const val TAG = "BBPOS_TAG"

    /**
     * 打印纸一行最大的字节
     */
    private const val LINE_BYTE_SIZE = 32


    const val PRINT_DATA =
        "http://@@@@:8080/pos?transactionType=PRINT&isExternal=true&apiVersion=21&printData="

    private var mPrintText = ""

    fun init(context: Context?) {
        mContextRef = WeakReference(context)
        thread {
            mLocalIp = getLocalIpAddress()
        }
    }

    private fun getContext(): Context? {
        return mContextRef?.get()
    }


    private fun getLocalIpAddress(): String {
        try {
            val interfaces: List<NetworkInterface> =
                Collections.list(NetworkInterface.getNetworkInterfaces())
            for (intf in interfaces) {
                val addrs: List<InetAddress> = Collections.list(intf.inetAddresses)
                for (addr in addrs) {
                    if (!addr.isLinkLocalAddress && !addr.isLoopbackAddress && addr is Inet4Address) {
                        return addr.getHostAddress()
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }

    fun printTitle(text: String? = "") {
        mPrintText += "<h3 align=\'center\'>${text}</h3><br/><br/>"
    }


    fun printLeft(text: String = "") {
        val array = text.split("：")
        if (array.isNotEmpty() && array.size > 1 && !TextUtils.isEmpty(array[1])) {
            val leftText = array[0].trim() + "："
            var rightText = array[1].trim()
            var ratio = calculateOffset(rightText)
            rightText.let {
                val regex = Regex("""^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$""")
                if (regex.matches(it)) {
                    ratio += 0.08f
                }
            }
            mPrintText += "<row><column>$leftText</column><column offset=$ratio>$rightText</column></row>"
        } else {

            val a2 = text.split("\n")
            if (a2.isNotEmpty() && a2.size > 1) {
                a2.forEach {
                    mPrintText += "<span align='left'>$it</span>"
                }
            } else {
                mPrintText += "<span align='left'>${text}</span>"
            }
        }
    }

    fun printRight(text: String? = "") {
        mPrintText += "<span align='right'>${text}</span>"
    }

    fun printCenter(text: String? = "") {
        if (!TextUtils.isEmpty(text)) {
            val array = text!!.split("\n")
            if (array.isNotEmpty() && array.size > 1) {
                array.forEach {
                    mPrintText += "<span align='center'>${it}</span>"
                }
            } else {
                mPrintText += "<span align='center'>${text}</span>"
            }
        }
    }

    fun printQrCode(mchId: String? = "") {
        if (!TextUtils.isEmpty(mchId)) {
//            val imgUrl = "https://admin.yo-coupon.com/WxApi/MakeQrCode?c_No=$mchId"
//            val encodeUrl = URLEncoder.encode(imgUrl, "UTF-8")
//            mPrintText += "<br/><br/><img src=\'${encodeUrl}\' ></img><br/><br/>"
//            mPrintText += "<br/><br/><qrcode>$imgUrl</qrcode><br/><br/>"
            mPrintText += "<br/><br/><qrcode scale=\'0.5\'>$mchId</qrcode><br/><br/>"
        }
    }


    fun printDoubleLines() {
        mPrintText += "<span align='center'>================================</span>"
    }


    fun printSingleLine() {
        mPrintText += "<span align='center'>-----------------------------------------------------------------</span>"
    }


    fun printEmptyLine() {
        mPrintText += "<br/>"
    }

//    private val mMyLock = ReentrantLock()


    fun startPrint() {
//        bbPosPrint()
        if (!TextUtils.isEmpty(mLocalIp)) {
            val st = "<html><body>"
            val en = "</body></html>"
            val printData = PRINT_DATA.replace("@@@@", mLocalIp) + st + mPrintText + en
            print(printData)
        }
    }


    private fun print(printData: String) {
        mPrintText = ""
        Log.i(TAG, "本地IP: $mLocalIp")
        Log.i(TAG, "打印数据: $printData")
        OkHttpClient().newCall(
            Request.Builder()
                .url(printData)
                .build()
        ).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.i(TAG, "打印异常: $e")
            }

            override fun onResponse(call: Call, response: Response) {
                if (response.isSuccessful) {
                    response.body?.run {
                        val res = this.string()
                        Log.i(TAG, "返回结果: $res")
//                    val array = res.split("<status>")
//                    val isPrintSuccess =
//                        array.isNotEmpty() && array.size > 1 && array[1].startsWith("SUCCESS")
//                    if (!isPrintSuccess) {
//                        //打印失败, 重新打印
//                        print(printData)
//                    }


                        if (res.contains("<resultCode>")) {
                            val array = res.split("<resultCode>")
                            val isBusy =
                                array.isNotEmpty() && array.size > 1 && array[1].startsWith("RESULT_BUSY")
                            Log.i(TAG, "打印机是否正在被占用:  $isBusy")
                            if (isBusy) {
                                //打印正在被占用, 故需重新打印
                                print(printData)
                            }
                        }
                    }
                }
            }
        })
    }

    private fun calculateOffset(rightText: String?): Float {
        var rightTextLength: Int = getBytesLength(rightText)
        return 1 - rightTextLength.toFloat() / LINE_BYTE_SIZE
    }


    private fun getBytesLength(msg: String?): Int {
        if (TextUtils.isEmpty(msg)) {
            return 0
        }
        return msg!!.toByteArray(Charset.forName("GBK")).size
    }

}