/*
 * 文 件 名:  PushMoneySuccActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-8-12
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.webView;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Blance;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PreferenceUtil;

/**
 * webView展示
 * 
 * @author  he_hui
 * @version  [版本号, 2016-8-12]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class WebViewActivityRegister extends TemplateActivity
{
    private TextView tv_bank_name, tv_money;
    
    private WebView wb;
    
    private boolean isTag = true;
    
    private ValueCallback<Uri> mUploadMessage;
    
    private final static int FILECHOOSER_RESULTCODE = 1;
    
    private ValueCallback<Uri[]> mUploadCallbackAboveL;
    
    final class InJavaScriptLocalObj
    {
        public void showSource(String html)
        {
            Logger.i("hehui", "showSource");
        }
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        //        titleBar.setTitle(R.string.register);
        titleBar.setRightButLayVisibleForTotal(false, getString(R.string.tv_push_record));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
            }
            
            @Override
            public void onLeftButtonClick()
            {
                //                finish();
                
                wb.loadUrl("javascript:goBack()");
            }
        });
    }
    
    @SuppressLint({"NewApi", "JavascriptInterface"})
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contenttext);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
            | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        wb = getViewById(R.id.webview);
//        wb.addJavascriptInterface(new InJavaScriptLocalObj(), "local_obj");
        wb.getSettings().setJavaScriptEnabled(true);
//        wb.addJavascriptInterface(new JsToJava(), "androidShare");
        wb.getSettings().setAllowFileAccessFromFileURLs(false);
        wb.getSettings().setAllowUniversalAccessFromFileURLs(false);
        
        String url = getIntent().getStringExtra("url");
        WebSettings settings = wb.getSettings();
        if (AppHelper.getAndroidSDKVersion() == 17)
        {
            settings.setDisplayZoomControls(false);
        }
        settings.setLoadWithOverviewMode(true);
        settings.setDomStorageEnabled(true);
        //settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        //各种分辨率适应
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int mDensity = metrics.densityDpi;
        if (mDensity == 120)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        }
        else if (mDensity == 160)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        }
        else if (mDensity == 240)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        }
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        
        settings.setDefaultTextEncodingName("utf-8");
        wb.getSettings().setDomStorageEnabled(true);
        wb.getSettings().setAppCacheMaxSize(1024 * 1024 * 8);
        String appCachePath = getApplicationContext().getCacheDir().getAbsolutePath();
        wb.getSettings().setAppCachePath(appCachePath);
        wb.getSettings().setAllowFileAccess(true);
        wb.getSettings().setAppCacheEnabled(true);
        
        wb.setWebChromeClient(new myWebChromeClien());
        wb.setWebViewClient(new MyWebViewClient());
        settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        settings.setSupportZoom(false);
        settings.setBuiltInZoomControls(false);
        
        String language = PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN);
        Map<String, String> heards = new HashMap<String, String>();
        Locale locale = getResources().getConfiguration().locale; //zh-rHK
        String lan = locale.getCountry();
        String langu = "zh_cn";
        if (!TextUtils.isEmpty(language))
        {
            heards.put("fp-lang", PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN));
            langu = PreferenceUtil.getString("language", MainApplication.LANG_CODE_ZH_CN);
        }
        else
        {
            if (lan.equalsIgnoreCase("CN"))
            {
                heards.put("fp-lang", MainApplication.LANG_CODE_ZH_CN);
                langu = MainApplication.LANG_CODE_ZH_CN;
            }
            else
            {
                heards.put("fp-lang", MainApplication.LANG_CODE_ZH_TW);
                langu = MainApplication.LANG_CODE_ZH_TW;
            }
            
        }
        
        String key = "sp-" + "Android" + "-" + AppHelper.getImei(MainApplication.getContext());
        
        String value =
            AppHelper.getVerCode(MainApplication.getContext()) + "," + ApiConstant.bankCode + "," + langu + ","
                + AppHelper.getAndroidSDKVersionName() + "," + DateUtil.formatTime(System.currentTimeMillis()) + ","
                + "Android";
        
        heards.put(key, value);
        
        wb.loadUrl(url);
        
    }
    
    private class JsToJava
    {
        // 这里需要加@JavascriptInterface，4.2之后提供给javascript调用的函数必须带有@JavascriptInterface
        @JavascriptInterface
        public void jsMethod(final String paramFromJS)
        {
            //
            WebViewActivityRegister.this.runOnUiThread(new Runnable()
            {
                
                @Override
                public void run()
                {
                    //                    tv_title.setText(paramFromJS);
                    Logger.i("hehui", "paramFromJS-->" + paramFromJS);
                    titleBar.setTitle(paramFromJS);
                }
            });
        }
    }
    
    class myWebChromeClien extends WebChromeClient
    {
        @Override
        public void onProgressChanged(WebView view, int newProgress)
        {
            super.onProgressChanged(view, newProgress);
            
        }
        
        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result)
        {
            Logger.i("hehui", "onJsAlert");
            return super.onJsAlert(view, url, message, result);
        }
        
        // For Android 3.0+
        public void openFileChooser(ValueCallback uploadMsg, String acceptType)
        {
            mUploadMessage = uploadMsg;
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("*/*");
            WebViewActivityRegister.this.startActivityForResult(Intent.createChooser(i, "File Browser"),
                FILECHOOSER_RESULTCODE);
        }
        
        //For Android 4.1
        public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture)
        {
            mUploadMessage = uploadMsg;
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("image/*");
            WebViewActivityRegister.this.startActivityForResult(Intent.createChooser(i, "File Chooser"),
                WebViewActivityRegister.FILECHOOSER_RESULTCODE);
        }
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILECHOOSER_RESULTCODE)
        {
            if (null == mUploadMessage && null == mUploadCallbackAboveL)
                return;
            Uri result = data == null || resultCode != RESULT_OK ? null : data.getData();
            if (mUploadCallbackAboveL != null)
            {
                onActivityResultAboveL(requestCode, resultCode, data);
            }
            else if (mUploadMessage != null)
            {
                mUploadMessage.onReceiveValue(result);
                mUploadMessage = null;
            }
        }
    }
    
    @SuppressLint("NewApi")
    private void onActivityResultAboveL(int requestCode, int resultCode, Intent data)
    {
        if (requestCode != FILECHOOSER_RESULTCODE || mUploadCallbackAboveL == null)
        {
            return;
        }
        
        Uri[] results = null;
        if (resultCode == Activity.RESULT_OK)
        {
            if (data == null)
            {
                
            }
            else
            {
                String dataString = data.getDataString();
                ClipData clipData = data.getClipData();
                
                if (clipData != null)
                {
                    results = new Uri[clipData.getItemCount()];
                    for (int i = 0; i < clipData.getItemCount(); i++)
                    {
                        ClipData.Item item = clipData.getItemAt(i);
                        results[i] = item.getUri();
                    }
                }
                
                if (dataString != null)
                    results = new Uri[] {Uri.parse(dataString)};
            }
        }
        mUploadCallbackAboveL.onReceiveValue(results);
        mUploadCallbackAboveL = null;
        return;
    }
    
    final class MyWebViewClient extends WebViewClient
    {
        public MyWebViewClient()
        {
            super();
        }
        
        /** {@inheritDoc} */
        
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            Logger.i("hehui", "onPageStarted");
            //            if (isTag)
            //            {
            //                isTag = false;
            //                showLoading(false, R.string.public_loading);
            //            }
            super.onPageStarted(view, url, favicon);
        }
        
        @SuppressLint("NewApi")
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url)
        {
            String ajaxUrl = url;
            Logger.i("hehui", "shouldInterceptRequest--" + url);
            return super.shouldInterceptRequest(view, ajaxUrl);
            
        }
        
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            Logger.i("hehui", "shouldOverrideUrlLoading-->" + url);
            //            if (isTag)
            //            {
            //                isTag = false;
            //                showLoading(false, R.string.public_loading);
            //            }
            
            //            if (!StringUtil.isEmptyOrNull(url))
            //            {
            //                if (url.contains("backApp"))
            //                {
            //                    WebViewActivityRegister.this.finish();
            //                }
            //            }
            
            if (null != url && url.contains("goBack.html"))
            {
                WebViewActivityRegister.this.finish();
            }
            
            //调用拨号程序  
            Logger.i("hehui", "url-->" + url);
            if (url.startsWith("mailto:") || url.startsWith("geo:") || url.startsWith("tel:"))
            {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                return true;
            }
            
            return super.shouldOverrideUrlLoading(view, url);
            
        }
        
        @Override
        public void doUpdateVisitedHistory(WebView view, String url, boolean isReload)
        {
            
            Logger.i("hehui", "doUpdateVisitedHistory");
            
            super.doUpdateVisitedHistory(view, url, isReload);
        }
        
        @Override
        public void onLoadResource(WebView view, String url)
        {
            // TODO Auto-generated method stub
            Logger.i("hehui", "onLoadResource");
            super.onLoadResource(view, url);
        }
        
        @Override
        public void onScaleChanged(WebView view, float oldScale, float newScale)
        {
            
            Logger.i("hehui", "onScaleChanged");
            super.onScaleChanged(view, oldScale, newScale);
        }
        
        @Override
        public void onPageFinished(WebView view, String url)
        {
            //            dismissLoading();
            wb.loadUrl("javascript:getTitle()");
            super.onPageFinished(view, url);
        }
        
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
        {
            super.onReceivedError(view, errorCode, description, failingUrl);
            //            dismissLoading();
            //            wb.loadUrl("file:///android_asset/error.html");
            
        }
        
    }
    
    //    private void initView()
    //    {
    //        tv_bank_name = getViewById(R.id.tv_bank_name);
    //        tv_money = getViewById(R.id.tv_money);
    //    }
    
    //    @Override
    //    protected void setupTitleBar()
    //    {
    //        super.setupTitleBar();
    //        titleBar.setLeftButtonVisible(true);
    //        titleBar.setTitle(R.string.tv_apply_push);
    //    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (event.getAction() == KeyEvent.ACTION_DOWN)
        {
            if (keyCode == KeyEvent.KEYCODE_BACK)
            { //表示按返回键    
                wb.loadUrl("javascript:goBack()");
                
                return true; //已处理      
            }
        }
        return super.onKeyDown(keyCode, event);
    }
    
    public static void startActivity(Context context, String url)
    {
        Intent it = new Intent();
        it.setClass(context, WebViewActivityRegister.class);
        it.putExtra("url", url);
        context.startActivity(it);
    }
    
    public static void startActivity(Context context, Blance blance)
    {
        Intent it = new Intent();
        it.setClass(context, WebViewActivityRegister.class);
        it.putExtra("blance", blance);
        context.startActivity(it);
    }
}
