package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.GoodsMode
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.GoodsNameSettingContract
import cn.swiftpass.enterprise.utils.JsonUtil
import com.example.common.entity.EmptyData
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @那些因为缘分而来的东西
 * @终有缘尽而别的时候
 */
class GoodsNameSettingPresenter : GoodsNameSettingContract.Presenter {

    private var mView: GoodsNameSettingContract.View? = null


    override fun updateOrAddBody(
        body: String?,
        count: String?
    ) {
        mView?.let { view ->

            view.showLoading(R.string.public_submitting, ParamsConstants.COMMON_LOADING)
            AppClient.updateOrAddBody(
                MainApplication.getInstance().getMchId(),
                body,
                count,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.updateOrAddBodyFailed(it.message)
                        }
                    }

                    override fun onResponse(response: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        view.updateOrAddBodySuccess(true, body)
                    }
                }
            )
        }
    }

    override fun queryBody() {
        mView?.let { view ->
            view.showLoading(R.string.public_loading, ParamsConstants.COMMON_LOADING)
            AppClient.queryBody(
                MainApplication.getInstance().getMchId(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<GoodsMode>(GoodsMode()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {

                            if (TextUtils.equals("405", it.result)) {
                                view.queryBodyFailed("405${it.message}")
                            } else {
                                view.queryBodyFailed(it.message)
                            }
                        }

                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val goodsMode = JsonUtil.jsonToBean(
                                response.message, GoodsMode::class.java
                            ) as GoodsMode
                            view.queryBodySuccess(goodsMode)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: GoodsNameSettingContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}