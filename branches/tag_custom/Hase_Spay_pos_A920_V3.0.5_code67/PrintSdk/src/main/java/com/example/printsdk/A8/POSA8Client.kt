package com.example.printsdk.A8

import android.content.Context

class POSA8Client {
    private var mContext: Context? = null

    companion object {
        const val DELAY_TIME = 3000L
        private var sPosA8Client: POSA8Client? = null

        fun initA8(context: Context) {
            if (sPosA8Client == null) {
                sPosA8Client = POSA8Client(context)
            }
        }

        fun getInstance(): POSA8Client? {
            if (sPosA8Client == null) {
                throw RuntimeException("A8Client is not initialized")
            }
            return sPosA8Client
        }
    }


    constructor(context: Context) {
        mContext = context
    }
}