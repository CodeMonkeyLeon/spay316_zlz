package cn.swiftpass.enterprise.ui.paymentlink;

import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.ADD_ORDER_SHARE_EMAIL;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.ADD_ORDER_SHARE_SMS;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.DATA_TAG;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.TYPE_TAG;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.ShareOrderContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.ShareOrderPresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.paymentlink.model.OrderConfig;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkOrder;
import cn.swiftpass.enterprise.ui.paymentlink.model.ShareInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.KeyBoardUtil;
import cn.swiftpass.enterprise.utils.RegexUtil;

/**
 * Created by congwei.li on 2021/9/17.
 *
 * @Description:
 */
public class ShareOrderActivity extends BaseActivity<ShareOrderContract.Presenter> implements ShareOrderContract.View {

    @BindView(R.id.tv_share_order_id)
    TextView tvOrderId;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.tv_share_payment_link)
    TextView tvPaymentLink;
    @BindView(R.id.tv_share_expire_time)
    TextView tvExpireTime;
    @BindView(R.id.ll_share_content1)
    LinearLayout llContent1;
    @BindView(R.id.tv_share_content1)
    TextView tvContent1;
    @BindView(R.id.et_share_content1)
    EditText etContent1;
    @BindView(R.id.ll_share_content2)
    LinearLayout llContent2;
    @BindView(R.id.tv_share_content2)
    TextView tvContent2;
    @BindView(R.id.et_share_content2)
    EditText etContent2;
    @BindView(R.id.ll_share_content3)
    LinearLayout llContent3;
    @BindView(R.id.tv_share_content3)
    TextView tvContent3;
    @BindView(R.id.et_share_content3)
    EditText etContent3;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;
    @BindView(R.id.view_share_line)
    View line;
    PaymentLinkOrder order;
    ShareInfo shareInfo;
    int type;

    public static void startActivity(Context mContext, Serializable order, int shareType) {
        Intent intent = new Intent(mContext, ShareOrderActivity.class);
        intent.putExtra(DATA_TAG, order);
        intent.putExtra(TYPE_TAG, shareType);
        mContext.startActivity(intent);
    }

    @Override
    protected ShareOrderContract.Presenter createPresenter() {
        return new ShareOrderPresenter();
    }

    public void checkInfo() {
        if (type == ADD_ORDER_SHARE_EMAIL) {
            tvConfirm.setEnabled(shareInfo.isEmailAllFull());
        } else if (type == ADD_ORDER_SHARE_SMS) {
            tvConfirm.setEnabled(shareInfo.isMsgAllFull());
        }
    }


    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_order);
        ButterKnife.bind(this);

        order = (PaymentLinkOrder) getIntent().getSerializableExtra(DATA_TAG);
        type = getIntent().getIntExtra(TYPE_TAG, ADD_ORDER_SHARE_EMAIL);
        titleBar.setLeftButton(R.drawable.icon_general_top_back_default);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButLayClick() {

            }
        });
        tvConfirm.setText(getStringById(R.string.payment_link_send_email_send));
        shareInfo = new ShareInfo();
        initView();
    }

    private void initView() {
        if (order == null) {
            return;
        }
        shareInfo.orderNo = order.orderNo;
        tvOrderId.setText(order.orderNo);
        tvPaymentLink.setText(order.payUrl);
        tvExpireTime.setText(order.effectiveDate);
        if (type == ADD_ORDER_SHARE_EMAIL) {
            initSendEmail();
        } else if (type == ADD_ORDER_SHARE_SMS) {
            initSendMessage();
        }

    }

    private void initSendEmail() {
        titleBar.setTitleText(R.string.payment_link_send_email);
        etContent1.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                shareInfo.custEmail = etContent1.getText().toString().trim();
                checkInfo();
            }
        });
        etContent2.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                shareInfo.emailSubject = etContent2.getText().toString().trim();
                checkInfo();
            }
        });
        etContent3.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                shareInfo.emailContent = etContent3.getText().toString().trim();
                checkInfo();
            }
        });
        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!RegexUtil.isEmail(shareInfo.custEmail)) {
                    toastDialog(ShareOrderActivity.this,
                            getStringById(R.string.payment_link_add_customer_email_error), null);
                    return;
                }
                sendEmail();
            }
        });

        etContent1.setText(order.custEmail);
        etContent1.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});
        etContent2.setFilters(new InputFilter[]{new InputFilter.LengthFilter(200)});
        etContent3.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1000)});
        requestOrderConfig();
    }

    private void initSendMessage() {
        titleBar.setTitleText(R.string.payment_link_share_message);
        tvContent1.setText(getStringById(R.string.payment_link_add_customer_phone_number));
        llContent2.setVisibility(View.GONE);
        llContent3.setVisibility(View.GONE);
        line.setVisibility(View.GONE);
        etContent1.setHint(getStringById(R.string.payment_link_share_phone_num_input));
        etContent1.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                shareInfo.custMobile = etContent1.getText().toString().trim();
                checkInfo();
            }
        });
        etContent1.setText(order.custMobile);
        etContent1.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});
        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });
    }

    @Override
    public void sendMessageSuccess(@Nullable String response) {
        Toast.makeText(ShareOrderActivity.this,
                getResources().getString(R.string.payment_link_share_send_success),
                Toast.LENGTH_SHORT).show();
        if (!ShareOrderActivity.this.isFinishing()) {
            ShareOrderActivity.this.finish();
        }
    }

    @Override
    public void sendMessageFailed(@Nullable Object error) {
        toastDialog(ShareOrderActivity.this, error.toString(), null);
    }

    private void sendMessage() {
        if (mPresenter != null) {
            mPresenter.sendMessage(shareInfo);
        }
    }

    @Override
    public void sendEmailSuccess(@Nullable String response) {
        Toast.makeText(ShareOrderActivity.this,
                getResources().getString(R.string.payment_link_share_send_success),
                Toast.LENGTH_SHORT).show();
        if (!ShareOrderActivity.this.isFinishing()) {
            ShareOrderActivity.this.finish();
        }
    }

    @Override
    public void sendEmailFailed(@Nullable Object error) {
        toastDialog(ShareOrderActivity.this, error.toString(), null);
    }

    private void sendEmail() {
        if (mPresenter != null) {
            mPresenter.sendEmail(shareInfo);
        }
    }

    @Override
    public void getOrderConfigSuccess(@Nullable OrderConfig response) {
        if (TextUtils.isEmpty(order.custName)) {
            order.custName = "";
        }
        etContent2.setText(response.getDisplayTitle(order.custName));
        etContent3.setText(response.getDisplayContent(order.custName));
    }

    @Override
    public void getOrderConfigFailed(@Nullable Object error) {

    }

    private void requestOrderConfig() {
        if (mPresenter != null) {
            mPresenter.getOrderConfig();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = ShareOrderActivity.this.getCurrentFocus();
            if (KeyBoardUtil.isShouldHideInput(v, ev)) {//点击editText控件外部
                InputMethodManager imm = (InputMethodManager) ShareOrderActivity.this
                        .getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    KeyBoardUtil.hideKeyboard(v);//软键盘工具类
                    if (llContent != null) {
                        llContent.clearFocus();
                    }
                }
            }
        }
        return getWindow().superDispatchTouchEvent(ev) || onTouchEvent(ev);
    }

}
