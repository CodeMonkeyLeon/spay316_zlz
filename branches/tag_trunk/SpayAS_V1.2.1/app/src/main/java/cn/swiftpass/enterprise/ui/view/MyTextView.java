package cn.swiftpass.enterprise.ui.view;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

public class MyTextView extends TextView {

    private Paint selfPaint;

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        selfPaint = new Paint();
        selfPaint.set(getPaint());
    }

    private void ajustWidth(String text, float width) {
//        if (width > 0) {
//            float availableWidth = width - getPaddingLeft() - getPaddingRight();
//            float testLen = getTextWidth(selfPaint, text);
//            setTextSize(TypedValue.COMPLEX_UNIT_PX, selfPaint.getTextSize());
//            if (testLen > availableWidth) {
//                setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) (getPaint().getTextSize() * (((int) (availableWidth * 100)) / 100d) / testLen * 0.99));
//            }
//        }
    }

    private float getTextWidth(Paint paint, String text) {
        float width = 0;
        if (!TextUtils.isEmpty(text)) {
            float[] widths = new float[text.length()];
            paint.getTextWidths(text, widths);
            for (int i = 0; i < widths.length; i++) {
                width += widths[i];
            }
        }
        return width;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        ajustWidth(getText().toString(), w);
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        ajustWidth(text.toString(), getWidth());
    }

}
