package cn.swiftplus.enterprise.printsdk.print.Sunmi.printUtils;

import android.content.Context;
import android.graphics.Bitmap;

import com.pax.dal.exceptions.PrinterDevException;
import com.pax.gl.imgprocessing.IImgProcessing;

import cn.swiftplus.enterprise.printsdk.print.A920.printUtils.CreateOneDiCodeUtilPrint;
import cn.swiftplus.enterprise.printsdk.print.A920.printUtils.PrinterTester;

/**
 * Created by aijingya on 2020/10/20.
 *
 * @Package cn.swiftplus.enterprise.printsdk.print.sunmi.printUtils
 * @Description:
 * @date 2020/10/20.21:19.
 */
public class PrintSunmiText {

    private static PrintSunmiText mPrintSunmiText;
    private Context mContext;

    public static PrintSunmiText  getInstance(){
        if(mPrintSunmiText == null){
            mPrintSunmiText = new PrintSunmiText();
        }
        return mPrintSunmiText;
    }


    public void init(Context context){
        this.mContext = context;
    }

    public void printTitle(String text){
        SunmiPrintHelper.getInstance().setAlign(1);
        SunmiPrintHelper.getInstance().printText(text+ "\n", 36, true, false);
        SunmiPrintHelper.getInstance().printText("\n", 28, false, false);
    }

    public void printTextLine(String text,int TextSize ,int align){
        SunmiPrintHelper.getInstance().setAlign(align);
        SunmiPrintHelper.getInstance().printText(text + "\n", TextSize, false, false);
    }

    public void printEmptyLine(){
        SunmiPrintHelper.getInstance().setAlign(1);
        SunmiPrintHelper.getInstance().printText("\n", 28, false, false);
    }

    public void printDoubleLine(){
        SunmiPrintHelper.getInstance().setAlign(1);
        SunmiPrintHelper.getInstance().printText("===========================\n", 28, false, false);
    }

    public void printSingleLine(){
        SunmiPrintHelper.getInstance().setAlign(1);
        SunmiPrintHelper.getInstance().printText("---------------------------\n", 28, false, false);
    }

    public void printQRCode(String orderNoMch){
        //打印小票的代码
        SunmiPrintHelper.getInstance().setAlign(1);
        SunmiPrintHelper.getInstance().printText("\n", 28, false, false);
        SunmiPrintHelper.getInstance().printQr(orderNoMch, 7, 2);
        SunmiPrintHelper.getInstance().printText("\n", 28, false, false);
    }

    public void printBitmap(Bitmap logoBitmap){
        if (logoBitmap != null){
            SunmiPrintHelper.getInstance().setAlign(1);
            SunmiPrintHelper.getInstance().printBitmap(logoBitmap);
            SunmiPrintHelper.getInstance().printText("\n", 28, false, false);
            SunmiPrintHelper.getInstance().printText("\n", 28, false, false);
        }
    }

}
