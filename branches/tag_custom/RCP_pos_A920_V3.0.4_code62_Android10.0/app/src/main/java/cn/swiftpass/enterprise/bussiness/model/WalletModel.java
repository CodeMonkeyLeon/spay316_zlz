/*
 * 文 件 名:  WalletModel.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-4
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * 电子钱包 
 * 
 * @author  he_hui
 * @version  [版本号, 2016-11-4]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class WalletModel implements Serializable
{
    
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    private String accountId;//结算表ID
    
    private Integer isHasEwallet;// 0不存在     1存在
    
    private String cardholder; // 持卡人
    
    private String identityCard;//身份证号码
    
    private long balance = 0;//余额
    
    private long availBlance;//可用余额
    
    private String bankName;//银行名称
    
    private String suffixIdCard;//银行卡4位
    
    private String tel;
    
    private String phone;
    
    private String accountCode;
    
    private String checkAuth; // 是否实名认证 是否有验证实名认证接口1 开通    2未开通
    
    private String order_no;//威富通订单
    
    private String money; //
    
    private String transactionId;
    
    private String transStatus;
    
    private String bankTransTime;
    
    private String amount;
    
    private String transaction_id; //银行订单
    
    private String trans_type;//1-汇款汇出 2-转帐 3-其他
    
    private String trans_status;// 0-未知状态 1-成功状态 2-失败状态
    
    private String bank_trans_time; // 交易时间
    
    private String isUpdateFlag;
    
    private String modifyidCards;
    
    /**
     * @return 返回 modifyidCards
     */
    public String getModifyidCards()
    {
        return modifyidCards;
    }
    
    /**
     * @param 对modifyidCards进行赋值
     */
    public void setModifyidCards(String modifyidCards)
    {
        this.modifyidCards = modifyidCards;
    }
    
    /**
     * @return 返回 isUpdateFlag
     */
    public String getIsUpdateFlag()
    {
        return isUpdateFlag;
    }
    
    /**
     * @param 对isUpdateFlag进行赋值
     */
    public void setIsUpdateFlag(String isUpdateFlag)
    {
        this.isUpdateFlag = isUpdateFlag;
    }
    
    /**
     * @return 返回 amount
     */
    public String getAmount()
    {
        return amount;
    }
    
    /**
     * @param 对amount进行赋值
     */
    public void setAmount(String amount)
    {
        this.amount = amount;
    }
    
    /**
     * @return 返回 transaction_id
     */
    public String getTransaction_id()
    {
        return transaction_id;
    }
    
    /**
     * @param 对transaction_id进行赋值
     */
    public void setTransaction_id(String transaction_id)
    {
        this.transaction_id = transaction_id;
    }
    
    /**
     * @return 返回 trans_type
     */
    public String getTrans_type()
    {
        return trans_type;
    }
    
    /**
     * @param 对trans_type进行赋值
     */
    public void setTrans_type(String trans_type)
    {
        this.trans_type = trans_type;
    }
    
    /**
     * @return 返回 trans_status
     */
    public String getTrans_status()
    {
        return trans_status;
    }
    
    /**
     * @param 对trans_status进行赋值
     */
    public void setTrans_status(String trans_status)
    {
        this.trans_status = trans_status;
    }
    
    /**
     * @return 返回 bank_trans_time
     */
    public String getBank_trans_time()
    {
        return bank_trans_time;
    }
    
    /**
     * @param 对bank_trans_time进行赋值
     */
    public void setBank_trans_time(String bank_trans_time)
    {
        this.bank_trans_time = bank_trans_time;
    }
    
    /**
     * @return 返回 isBanlance
     */
    public String getIsBanlance()
    {
        return isBanlance;
    }
    
    /**
     * @param 对isBanlance进行赋值
     */
    public void setIsBanlance(String isBanlance)
    {
        this.isBanlance = isBanlance;
    }
    
    private String isBanlance; //查询是否开通电子钱包的同时 查询余额是否失败，1成功，2失败
    
    /**
     * @return 返回 transactionId
     */
    public String getTransactionId()
    {
        return transactionId;
    }
    
    /**
     * @param 对transactionId进行赋值
     */
    public void setTransactionId(String transactionId)
    {
        this.transactionId = transactionId;
    }
    
    /**
     * @return 返回 transStatus
     */
    public String getTransStatus()
    {
        return transStatus;
    }
    
    /**
     * @param 对transStatus进行赋值
     */
    public void setTransStatus(String transStatus)
    {
        this.transStatus = transStatus;
    }
    
    /**
     * @return 返回 bankTransTime
     */
    public String getBankTransTime()
    {
        return bankTransTime;
    }
    
    /**
     * @param 对bankTransTime进行赋值
     */
    public void setBankTransTime(String bankTransTime)
    {
        this.bankTransTime = bankTransTime;
    }
    
    /**
     * @return 返回 money
     */
    public String getMoney()
    {
        return money;
    }
    
    /**
     * @param 对money进行赋值
     */
    public void setMoney(String money)
    {
        this.money = money;
    }
    
    /**
     * @return 返回 order_no
     */
    public String getOrder_no()
    {
        return order_no;
    }
    
    /**
     * @param 对order_no进行赋值
     */
    public void setOrder_no(String order_no)
    {
        this.order_no = order_no;
    }
    
    /**
     * @return 返回 checkAuth
     */
    public String getCheckAuth()
    {
        return checkAuth;
    }
    
    /**
     * @param 对checkAuth进行赋值
     */
    public void setCheckAuth(String checkAuth)
    {
        this.checkAuth = checkAuth;
    }
    
    /**
     * @return 返回 accountCode
     */
    public String getAccountCode()
    {
        return accountCode;
    }
    
    /**
     * @param 对accountCode进行赋值
     */
    public void setAccountCode(String accountCode)
    {
        this.accountCode = accountCode;
    }
    
    /**
     * @return 返回 phone
     */
    public String getPhone()
    {
        return phone;
    }
    
    /**
     * @param 对phone进行赋值
     */
    public void setPhone(String phone)
    {
        this.phone = phone;
    }
    
    /**
     * @return 返回 tel
     */
    public String getTel()
    {
        return tel;
    }
    
    /**
     * @param 对tel进行赋值
     */
    public void setTel(String tel)
    {
        this.tel = tel;
    }
    
    /**
     * @return 返回 accountId
     */
    public String getAccountId()
    {
        return accountId;
    }
    
    /**
     * @param 对accountId进行赋值
     */
    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }
    
    /**
     * @return 返回 isHasEwallet
     */
    public Integer getIsHasEwallet()
    {
        return isHasEwallet;
    }
    
    /**
     * @param 对isHasEwallet进行赋值
     */
    public void setIsHasEwallet(Integer isHasEwallet)
    {
        this.isHasEwallet = isHasEwallet;
    }
    
    /**
     * @return 返回 cardholder
     */
    public String getCardholder()
    {
        return cardholder;
    }
    
    /**
     * @param 对cardholder进行赋值
     */
    public void setCardholder(String cardholder)
    {
        this.cardholder = cardholder;
    }
    
    /**
     * @return 返回 identityCard
     */
    public String getIdentityCard()
    {
        return identityCard;
    }
    
    /**
     * @param 对identityCard进行赋值
     */
    public void setIdentityCard(String identityCard)
    {
        this.identityCard = identityCard;
    }
    
    /**
     * @return 返回 balance
     */
    public long getBalance()
    {
        return balance;
    }
    
    /**
     * @param 对balance进行赋值
     */
    public void setBalance(long balance)
    {
        this.balance = balance;
    }
    
    /**
     * @return 返回 availBlance
     */
    public long getAvailBlance()
    {
        return availBlance;
    }
    
    /**
     * @param 对availBlance进行赋值
     */
    public void setAvailBlance(long availBlance)
    {
        this.availBlance = availBlance;
    }
    
    /**
     * @return 返回 bankName
     */
    public String getBankName()
    {
        return bankName;
    }
    
    /**
     * @param 对bankName进行赋值
     */
    public void setBankName(String bankName)
    {
        this.bankName = bankName;
    }
    
    /**
     * @return 返回 suffixIdCard
     */
    public String getSuffixIdCard()
    {
        return suffixIdCard;
    }
    
    /**
     * @param 对suffixIdCard进行赋值
     */
    public void setSuffixIdCard(String suffixIdCard)
    {
        this.suffixIdCard = suffixIdCard;
    }
}
