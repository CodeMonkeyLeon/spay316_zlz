package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.OrderTotalInfo
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @愿你低声啜泣
 * @但不要彻夜不眠
 */
class ReportContract {

    interface View : BaseView {

        fun orderCountSuccess(
            response: OrderTotalInfo?,
            countMethod: Int
        )

        fun orderCountFailed(error: Any?)

    }

    interface Presenter : BasePresenter<View> {
        fun orderCount(
            startTime: String?,
            endTime: String?,
            countMethod: Int,
            userId: String?,
            countDayKind: String?,
            isTrade: Boolean
        )
    }

}