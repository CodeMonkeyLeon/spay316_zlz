package org.xclcharts.base;

import android.app.Application;
import android.content.Context;

/**
 * Created by aijingya on 2019/7/5.
 *
 * @Package org.xclcharts.base
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2019/7/5.20:16.
 */
public class BaseApplication extends Application {
    /**
     * 系统上下文
     */
    private static Context mAppContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mAppContext = getApplicationContext();

    }

    /**
     * 获取系统上下文：用于ToastUtil类
     */
    public static Context getAppContext() {
        return mAppContext;
    }
}
