package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.OrderSearchResult
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.OrderStreamContract
import cn.swiftpass.enterprise.utils.JsonUtil
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @如果你是条船
 * @漂泊就是你的命运
 * @可别靠岸
 */
class OrderStreamPresenter : OrderStreamContract.Presenter {

    private var mView: OrderStreamContract.View? = null


    override fun queryOrderData(
        orderNo: String?,
        currentPage: Int,
        state: Int,
        time: String?,
        orderNoMch: String?,
        isLoading: Boolean
    ) {
        mView?.let { view ->

            if (isLoading) {
                view.showLoading(
                    R.string.public_data_loading,
                    ParamsConstants.ORDER_STREAM_ACTIVITY_QUERY_ORDER_DATA
                )
            }

            var noMch = orderNo
            orderNoMch?.let {
                noMch = it
            }

            AppClient.queryOrderData(
                noMch,
                MainApplication.getInstance().userInfo.mchId,
                MainApplication.getInstance().getUserId().toString(),
                time,
                currentPage.toString(),
                state.toString(),
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object :
                    CallBackUtil.CallBackCommonResponse<OrderSearchResult>(OrderSearchResult()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.ORDER_STREAM_ACTIVITY_QUERY_ORDER_DATA)
                        commonResponse?.let {
                            view.queryOrderDataFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.ORDER_STREAM_ACTIVITY_QUERY_ORDER_DATA)
                        r?.let { response ->
                            val orderList = JsonUtil.jsonToBean(
                                response.message,
                                OrderSearchResult::class.java
                            ) as OrderSearchResult
                            for (order in orderList.data) {
                                order.notify_time =
                                    if (TextUtils.isEmpty(order.notifyTime)) 0 else order.notifyTime.toLong()
                                order.add_time =
                                    if (TextUtils.isEmpty(order.addTime)) 0 else order.addTime.toLong()
                                order.useId = order.userId.toString()
                            }
                            view.queryOrderDataSuccess(orderList)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: OrderStreamContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}