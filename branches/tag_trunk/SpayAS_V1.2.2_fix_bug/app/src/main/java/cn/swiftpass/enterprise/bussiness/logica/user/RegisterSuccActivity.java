/*
 * 文 件 名:  RegisterSuccActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2015-3-12
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.logica.user;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.MainActivity;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.shop.ShopkeeperActivity;

/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2015-3-12]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class RegisterSuccActivity extends TemplateActivity
{
    
    /** {@inheritDoc} */
    
    private Button mainBut, dataBut;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_succeed);
        
        mainBut = getViewById(R.id.mainBut);
        
        dataBut = getViewById(R.id.dataBut);
        
        mainBut.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                MainActivity.startActivity(RegisterSuccActivity.this, "WelcomeActivity");
                finish();
            }
        });
        
        dataBut.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                showPage(ShopkeeperActivity.class);
            }
        });
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(false);
        titleBar.setTitle("注册成功");
    }
    
}
