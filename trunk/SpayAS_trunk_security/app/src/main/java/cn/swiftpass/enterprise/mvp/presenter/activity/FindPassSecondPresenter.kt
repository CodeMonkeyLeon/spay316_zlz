package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.bussiness.model.ForgetPSWBean
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.FindPassSecondContract
import cn.swiftpass.enterprise.utils.JsonUtil
import com.example.common.entity.EmptyData
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @花谢的时候
 * @并不伤心
 * @生命要在死亡里休息
 * @变得干净
 */
class FindPassSecondPresenter : FindPassSecondContract.Presenter {

    private var mView: FindPassSecondContract.View? = null


    override fun checkPhoneCode(
        token: String?,
        code: String?
    ) {
        mView?.let { view ->
            view.showLoading(R.string.reg_next_step_loading, ParamsConstants.COMMON_LOADING)
            AppClient.checkPhoneCode(
                token,
                code,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.checkPhoneCodeFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        view.checkPhoneCodeSuccess(true)
                    }
                }
            )
        }
    }

    override fun getCode(input: String?) {
        mView?.let { view ->

            view.showLoading(R.string.dialog_message, ParamsConstants.COMMON_LOADING)
            AppClient.checkForgetPwdInputAndGetCode(
                input,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<ForgetPSWBean>(ForgetPSWBean()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.getCodeFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val pswBean = JsonUtil.jsonToBean(
                                response.message,
                                ForgetPSWBean::class.java
                            ) as ForgetPSWBean
                            view.getCodeSuccess(pswBean)
                        }
                    }
                }
            )

        }
    }

    override fun attachView(view: FindPassSecondContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}