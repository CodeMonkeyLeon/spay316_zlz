package cn.swiftpass.enterprise.utils;

import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * aes加密
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2015-12-25]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class AESHelper
{
    private static final String TAG = AESHelper.class.getSimpleName();
    /**
     * 将16进制转换为二进制
     * 
     * @param hexStr
     * @return
     */
    public static byte[] parseHexStr2Byte(String hexStr)
    {
        if (hexStr.length() < 1)
            return null;
        byte[] result = new byte[hexStr.length() / 2];
        for (int i = 0; i < hexStr.length() / 2; i++)
        {
            int high = Integer.parseInt(hexStr.substring(i * 2, i * 2 + 1), 16);
            int low = Integer.parseInt(hexStr.substring(i * 2 + 1, i * 2 + 2), 16);
            result[i] = (byte)(high * 16 + low);
        }
        return result;
    }
    
    public static String aesEncrypt(String content, String password) {
        try {

            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            int blockSize = cipher.getBlockSize();

            byte[] dataBytes = content.getBytes();
            int plaintextLength = dataBytes.length;
            if (plaintextLength % blockSize != 0) {
                plaintextLength = plaintextLength + (blockSize - (plaintextLength % blockSize));
            }

            byte[] plaintext = new byte[plaintextLength];
            System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);

            SecretKeySpec keyspec = new SecretKeySpec(password.getBytes(), "AES");
            IvParameterSpec ivspec = new IvParameterSpec(IV.getBytes());

            cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
            byte[] encrypted = cipher.doFinal(plaintext);

            return parseByte2HexStr(encrypted);

        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
            return null;
        }
    }
    
    private static final String IV = "a1b2c3d4e5f6g7h8";

    public static String aesDecrypt(String content, String password) {
        try {

            byte[] encrypted1 = parseHexStr2Byte(content);

            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            SecretKeySpec keyspec = new SecretKeySpec(password.getBytes(), "AES");
            IvParameterSpec ivspec = new IvParameterSpec(IV.getBytes());

            cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);

            byte[] original = cipher.doFinal(encrypted1);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            for (byte b : original) {
                if (b != 0) {
                    byteArrayOutputStream.write(b);
                }
            }
            String originalString = new String(byteArrayOutputStream.toByteArray(), "utf-8");
            return originalString;
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
            return null;
        }
    }
    
    /**
     * 将二进制转换成16进制
     * 
     * @param buf
     * @return
     */
    public static String parseByte2HexStr(byte buf[])
    {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < buf.length; i++)
        {
            String hex = Integer.toHexString(buf[i] & 0xFF);
            if (hex.length() == 1)
            {
                hex = '0' + hex;
            }
            sb.append(hex.toUpperCase());
        }
        return sb.toString();
    }
    
    /** 
     * 结合密钥生成加密后的密文 
     *  
     * @param raw 
     * @param input 
     * @return 
     * @throws Exception 
     */
   /* public static String encrypt(String seed, String cleartext)
        throws Exception
    {
        byte[] rawKey = getRawKey(seed.getBytes());
        byte[] result = encrypt(rawKey, cleartext.getBytes());
        return toHex(result);
    }*/
    
    /** 
     * 解密后的字符串 
     *  
     * @param seed 
     * @param encrypted 
     * @return 
     */
    /*public static String decrypt(String seed, String encrypted)
        throws Exception
    {
        byte[] rawKey = getRawKey(seed.getBytes());
        byte[] enc = toByte(encrypted);
        byte[] result = decrypt(rawKey, enc);
        return new String(result);
    }*/

//    private static byte[] getRawKey(byte[] seed)
//    {
//        try {
//            KeyGenerator kgen = KeyGenerator.getInstance("AES");
//            SecureRandom sr = null;
//            if (android.os.Build.VERSION.SDK_INT >= 17)
//            {
//                sr = SecureRandom.getInstance("SHA1PRNG", "Crypto");
//            }
//            else
//            {
//                sr = SecureRandom.getInstance("SHA1PRNG");
//            }
//            sr.setSeed(seed);
//            kgen.init(128, sr);
//            SecretKey skey = kgen.generateKey();
//            byte[] raw = skey.getEncoded();
//            return raw;
//        }catch (Exception e){
//            Log.e(TAG,Log.getStackTraceString(e));
//        }
//        return null;
//    }

    /*private static byte[] encrypt(byte[] raw, byte[] clear)
        throws Exception
    {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(clear);
        return encrypted;
    }*/
    
  /*  private static byte[] decrypt(byte[] raw, byte[] encrypted)
        throws Exception
    {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] decrypted = cipher.doFinal(encrypted);
        return decrypted;
    }*/
    
  /*  public static String toHex(String txt)
    {
        return toHex(txt.getBytes());
    }
    
    public static String fromHex(String hex)
    {
        return new String(toByte(hex));
    }*/
    
    public static byte[] toByte(String hexString)
    {
        int len = hexString.length() / 2;
        byte[] result = new byte[len];
        for (int i = 0; i < len; i++)
            result[i] = Integer.valueOf(hexString.substring(2 * i, 2 * i + 2), 16).byteValue();
        return result;
    }
    
    public static String toHex(byte[] buf)
    {
        if (buf == null)
            return "";
        StringBuffer result = new StringBuffer(2 * buf.length);
        for (int i = 0; i < buf.length; i++)
        {
            appendHex(result, buf[i]);
        }
        return result.toString();
    }
    
    private static void appendHex(StringBuffer sb, byte b)
    {
        final String HEX = "0123456789ABCDEF";
        sb.append(HEX.charAt((b >> 4) & 0x0f)).append(HEX.charAt(b & 0x0f));
    }
}
