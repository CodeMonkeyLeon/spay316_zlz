package cn.swiftpass.enterprise.ui.paymentlink

import android.os.Bundle
import android.view.View
import android.widget.TextView
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.ui.fmt.BaseDialogFragment
import cn.swiftpass.enterprise.ui.paymentlink.interfaces.OnOrderFilterClickListener

class PaymentLinkOrderFilterDialog : BaseDialogFragment(),
    View.OnClickListener {

    override fun getLayoutID() = R.layout.dialog_payment_link_order_filter

    private var mOnClickListener: OnOrderFilterClickListener? = null


    companion object {

        const val TYPE_ALL = ""
        const val TYPE_UNPAID = "0"
        const val TYPE_EXCEED_TIME = "1"
        const val TYPE_PAYING = "2"
        const val TYPE_PAID = "3"
        const val TYPE_REFUNDING = "4"
        const val TYPE_CANCEL_PAY = "5"

        fun getInstance() = PaymentLinkOrderFilterDialog()
    }


    fun setOnClickListener(listener: OnOrderFilterClickListener?) {
        mOnClickListener = listener
    }


    override fun init(view: View?, savedInstanceState: Bundle?) {
        view?.run {
            R.id.tv_select_all.setOnClickListener(this, this@PaymentLinkOrderFilterDialog)
            R.id.tv_select_unpaid.setOnClickListener(this, this@PaymentLinkOrderFilterDialog)
            R.id.tv_select_exceed.setOnClickListener(this, this@PaymentLinkOrderFilterDialog)
            R.id.tv_select_paying.setOnClickListener(this, this@PaymentLinkOrderFilterDialog)
            R.id.tv_select_paid.setOnClickListener(this, this@PaymentLinkOrderFilterDialog)
            R.id.tv_select_refunding.setOnClickListener(this, this@PaymentLinkOrderFilterDialog)
            R.id.tv_select_cancel_pay.setOnClickListener(this, this@PaymentLinkOrderFilterDialog)
            R.id.tv_select_cancel.setOnClickListener(this, this@PaymentLinkOrderFilterDialog)
        }
    }


    fun Int.setOnClickListener(view: View, listener: View.OnClickListener) {
        view.findViewById<TextView>(this).setOnClickListener(listener)
    }


    override fun onClick(view: View?) {
        view?.run {
            mOnClickListener?.run {
                when (id) {
                    R.id.tv_select_all -> {
                        onClick(TYPE_ALL)
                        dismiss()
                    }
                    R.id.tv_select_unpaid -> {
                        onClick(TYPE_UNPAID)
                        dismiss()
                    }
                    R.id.tv_select_exceed -> {
                        onClick(TYPE_EXCEED_TIME)
                        dismiss()
                    }
                    R.id.tv_select_paying -> {
                        onClick(TYPE_PAYING)
                        dismiss()
                    }
                    R.id.tv_select_paid -> {
                        onClick(TYPE_PAID)
                        dismiss()
                    }
                    R.id.tv_select_refunding -> {
                        onClick(TYPE_REFUNDING)
                        dismiss()
                    }
                    R.id.tv_select_cancel_pay -> {
                        onClick(TYPE_CANCEL_PAY)
                        dismiss()
                    }
                    R.id.tv_select_cancel -> {
                        dismiss()
                    }
                }
            }
        }
    }
}