package cn.swiftpass.enterprise.ui.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;

import cn.swiftpass.enterprise.bussiness.enums.CacheEnum;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.CashierReport;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.OrderReport;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.adapter.ReportAdapter;
import cn.swiftpass.enterprise.ui.widget.ChooseDateTimeDialog;
import cn.swiftpass.enterprise.ui.widget.OrderReportsumDialog;
import cn.swiftpass.enterprise.ui.widget.OrderReportsumDialog.ConfirmListener;
import cn.swiftpass.enterprise.ui.widget.PullDownListView;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.ToastHelper;

/** 订单统计
 * User: Alan
 * Date: 13-10-11
 * Time: 下午2:18
 */
public class OrderTotalActivity extends TemplateActivity implements View.OnClickListener,
    AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, PullDownListView.OnRefreshListioner
{
    private static final String TAG = OrderTotalActivity.class.getSimpleName();
    //private TextView tvName;
    private Context mContext;
    
    private String strTime;
    
    private int currentPage = 0;
    
    private long searchTime = System.currentTimeMillis();
    
    private PullDownListView mPullDownView;
    
    private ListView mListView;
    
    //private TextView tvInfo;
    private List<CashierReport> data;
    
    ReportAdapter adapter;
    
    private String endTime;
    
    private RelativeLayout btnThisWeek, btnLastWeek, btnThisMonth, btnLastMonth, btnDateCenter;
    
    private LinearLayout llDatetime;
    
    private TextView tvThisWeek, tvLastWeek, tvThisMonth, tvLastMonth, tvDateCenter;
    
    private ImageView ivThisWeek, ivLastWeek, ivThisMonth, ivLastMonth, ivDateCenter;
    
    private TextView tvTurnover, tvMoney, tvDatetime;
    
    private Handler handler = new Handler();
    
    private int maxAount = 100;//设置了最大数据值
    
    private LayoutInflater inflater;
    
    private View view;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_total);
        initViews(); //暂时屏蔽
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(false);
        titleBar.setTitle(R.string.title_search);
        
    }
    
    private void initViews()
    {
        mContext = this;
        
        tvTurnover = getViewById(R.id.tv_tradeNum);
        tvMoney = getViewById(R.id.tv_money);
        tvDatetime = getViewById(R.id.tv_datetime_value);
        llDatetime = getViewById(R.id.ll_datetime);
        llDatetime.setOnClickListener(this);
        
        // tvName = getViewById(R.id.tv_name);
        // tvName.setText(LocalAccountManager.getInstance().getUserName()+"");
        // tvInfo = getViewById(R.id.tv_info);
        mPullDownView = (PullDownListView)findViewById(R.id.list);
        mPullDownView.setRefreshListioner(this);
        
        adapter = new ReportAdapter(this);
        mListView = mPullDownView.mListView;
        mListView.setAdapter(adapter);
        adapter.setListView(mListView);
        mListView.setOnItemClickListener(this);
        
        btnThisWeek = getViewById(R.id.btn_thisWeek);
        btnThisWeek.setOnClickListener(this);
        btnLastWeek = getViewById(R.id.btn_lastWeek);
        btnLastWeek.setOnClickListener(this);
        btnThisMonth = getViewById(R.id.btn_thisMonth);
        btnThisMonth.setOnClickListener(this);
        btnLastMonth = getViewById(R.id.btn_lastMonth);
        btnLastMonth.setOnClickListener(this);
        
        tvThisWeek = getViewById(R.id.tv_thisWeek);
        tvThisMonth = getViewById(R.id.tv_thisMonth);
        tvLastWeek = getViewById(R.id.tv_lastWeek);
        tvLastMonth = getViewById(R.id.tv_lastMonth);
        
        ivThisWeek = getViewById(R.id.iv_thisWeek);
        ivLastWeek = getViewById(R.id.iv_lastWeek);
        ivThisMonth = getViewById(R.id.iv_thisMonth);
        ivLastMonth = getViewById(R.id.iv_lastMonth);
        
        btnDateCenter = getViewById(R.id.btn_date_center);
        btnDateCenter.setOnClickListener(this);
        ivDateCenter = getViewById(R.id.iv_date_center);
        tvDateCenter = getViewById(R.id.tv_date_center);
        
        strTime = DateUtil.getMondayOfThisWeek();
        endTime = DateUtil.getSundayOfThisWeek();
        value = CacheEnum.CASHIER_THIS_WEEK_TYPE;
        onSearch(CacheEnum.CASHIER_THIS_WEEK_TYPE);
        
    }
    
    /***
     * 查询当日订单
     */
    private void onSearch(CacheEnum cacheEnum)
    {
        
        OrderManager.getInstance().queryOrderReport(cacheEnum, strTime, endTime, new UINotifyListener<OrderReport>()
        {
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                if (object != null)
                {
                    ToastHelper.showInfo(object.toString());
                }
            }
            
            @Override
            public void onSucceed(OrderReport result)
            {
                super.onSucceed(result);
                titleBar.setRightLodingVisible(false, false);
                try
                {
                    if (result == null)
                    {
                        //tvInfo.setText(R.string.msg_no_order_info);
                        //tvInfo.setVisibility(View.VISIBLE);
                        showToastInfo("无统计数据");
                        mPullDownView.setVisibility(View.GONE);
                        tvTurnover.setText("0");
                        tvMoney.setText("¥ 0.00");
                        return;
                    }
                    if (data == null)
                    {
                        data = result.cashierReports;
                    }
                    else
                    {
                        //if (result != null) list.addAll(data);
                        data.clear();
                        data = result.cashierReports;
                    }
                    if (result != null && result.cashierReports.size() > 0)
                    {
                        //tvInfo.setVisibility(View.GONE);
                        adapter.setList(data);
                        mPullDownView.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        //tvInfo.setText(R.string.msg_no_order_info);
                        //tvInfo.setVisibility(View.VISIBLE);
                        mPullDownView.setVisibility(View.GONE);
                    }
                    //                    CashierReportSum cs = result.cashierReportSum;
                    if (result.cashierReports != null)
                    {
                        int numT = 0, numR = 0;
                        long moneyT = 0, moneyR = 0;
                        for (CashierReport s : result.cashierReports)
                        {
                            numT = numT + s.tradeNum;
                            numR = numR + s.refundNum;
                            
                            moneyT = moneyT + s.turnover;
                            
                            moneyR = moneyR + s.refundFee;
                        }
                        tvTurnover.setText((numT + numR) + "");
                        tvMoney.setText("¥ " + DateUtil.formatMoneyUtil((moneyR + moneyT) / 100d));
                    }
                    mPullDownView.post(new Runnable()
                    {
                        @Override
                        public void run()
                        { //自动滚动第一个
                            mPullDownView.mListView.setSelection(0);
                            mPullDownView.setMore(false);
                        }
                    });
                }
                catch (Exception e)
                {
                    Log.e(TAG,Log.getStackTraceString(e));
                    Logger.d(e.getMessage());
                }
                
            }
            
            @Override
            public void onPostExecute()
            {
                super.onPostExecute();
                // dismissLoading();
                
            }
            
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                // showLoading("请稍候");
                titleBar.setRightLodingVisible(true);
            }
        });
    }
    
    @Override
    public void onRefresh()
    {
        handler.postDelayed(new Runnable()
        {
            
            public void run()
            {
                currentPage = 0;
                mPullDownView.onRefreshComplete();//这里表示刷新处理完成后把上面的加载刷新界面隐藏
                mPullDownView.setMore(true);//这里设置true表示还有更多加载，设置为false底部将不显示更多
            }
        }, 1500);
    }
    
    @Override
    public void onLoadMore()
    {
        
        // searchOrder();
        handler.postDelayed(new Runnable()
        {
            public void run()
            {
                if (data != null)
                {
                    currentPage = currentPage + 1;
                }
                mPullDownView.onLoadMoreComplete();//这里表示加载更多处理完成后把下面的加载更多界面（隐藏或者设置字样更多）
                if (data.size() < maxAount)//判断当前list中已添加的数据是否小于最大值maxAount，是那么久显示更多否则不显示
                    mPullDownView.setMore(true);//这里设置true表示还有更多加载，设置为false底部将不显示更多
                else
                    mPullDownView.setMore(false);
                
            }
        }, 1500);
    }
    
    public static void startActivity(Context context)
    {
        Intent it = new Intent();
        it.setClass(context, OrderTotalActivity.class);
        context.startActivity(it);
    }
    
    public boolean onKeyDown(int keycode, KeyEvent event)
    {
        if (keycode == KeyEvent.KEYCODE_BACK)
        {
            //            if (LocalAccountManager.getInstance().getLoggedUser().role == UserRole.manager)
            //            {
            showExitDialog(this);
            //            }
        }
        return super.onKeyDown(keycode, event);
    }
    
    CacheEnum value = null;
    
    @Override
    public void onClick(View view)
    {
        int color = Color.parseColor("#007fff");
        int titleTextColor = Color.parseColor("#ff666666");
        switch (view.getId())
        {
        
            case R.id.btn_thisWeek:
            {
                strTime = DateUtil.getMondayOfThisWeek();
                endTime = DateUtil.getSundayOfThisWeek();
                value = CacheEnum.CASHIER_THIS_WEEK_TYPE;
                tvThisWeek.setTextColor(color);
                ivThisWeek.setVisibility(View.VISIBLE);
                tvLastWeek.setTextColor(titleTextColor);
                ivLastWeek.setVisibility(View.GONE);
                tvThisMonth.setTextColor(titleTextColor);
                ivThisMonth.setVisibility(View.GONE);
                tvLastMonth.setTextColor(titleTextColor);
                ivLastMonth.setVisibility(View.GONE);
                tvDateCenter.setTextColor(titleTextColor);
                ivDateCenter.setVisibility(View.GONE);
                //tvDatetime.setText("选择日期");
                // searchTime = System.currentTimeMillis();
                if (data != null)
                    data.clear();
                adapter.removeAll();
                onSearch(value);
                break;
            }
            case R.id.btn_lastWeek:
            {
                String[] str = DateUtil.getLastWeek();
                strTime = str[0];
                endTime = str[1];
                value = CacheEnum.CASHIER_LAST_WEEK_TYPE;
                tvThisWeek.setTextColor(titleTextColor);
                ivThisWeek.setVisibility(View.GONE);
                tvLastWeek.setTextColor(color);
                ivLastWeek.setVisibility(View.VISIBLE);
                tvThisMonth.setTextColor(titleTextColor);
                ivThisMonth.setVisibility(View.GONE);
                tvLastMonth.setTextColor(titleTextColor);
                ivLastMonth.setVisibility(View.GONE);
                tvDateCenter.setTextColor(titleTextColor);
                ivDateCenter.setVisibility(View.GONE);
                //tvDatetime.setText("选择日期");
                //searchTime = System.currentTimeMillis();
                if (data != null)
                    data.clear();
                adapter.removeAll();
                onSearch(value);
                break;
            }
            case R.id.btn_thisMonth:
            {
                String[] str = DateUtil.getThisMonth();
                strTime = str[0];
                endTime = str[1];
                value = CacheEnum.CASHIER_THIS_MONTH_TYPE;
                tvThisWeek.setTextColor(titleTextColor);
                ivThisWeek.setVisibility(View.GONE);
                tvLastWeek.setTextColor(titleTextColor);
                ivLastWeek.setVisibility(View.GONE);
                tvThisMonth.setTextColor(color);
                ivThisMonth.setVisibility(View.VISIBLE);
                tvLastMonth.setTextColor(titleTextColor);
                ivLastMonth.setVisibility(View.GONE);
                tvDateCenter.setTextColor(titleTextColor);
                ivDateCenter.setVisibility(View.GONE);
                //tvDatetime.setText("选择日期");
                // searchTime = System.currentTimeMillis();
                if (data != null)
                    data.clear();
                adapter.removeAll();
                onSearch(value);
                break;
            }
            case R.id.btn_lastMonth:
            {
                String[] str = DateUtil.getcLastMonth();
                strTime = str[0];
                endTime = str[1];
                value = CacheEnum.CASHIER_LAST_MONTH_TYPE;
                tvThisWeek.setTextColor(titleTextColor);
                ivThisWeek.setVisibility(View.GONE);
                tvLastWeek.setTextColor(titleTextColor);
                ivLastWeek.setVisibility(View.GONE);
                tvThisMonth.setTextColor(titleTextColor);
                ivThisMonth.setVisibility(View.GONE);
                tvLastMonth.setTextColor(color);
                ivLastMonth.setVisibility(View.VISIBLE);
                tvDateCenter.setTextColor(titleTextColor);
                ivDateCenter.setVisibility(View.GONE);
                // tvDatetime.setText("选择日期");
                // searchTime = System.currentTimeMillis();
                if (data != null)
                    data.clear();
                adapter.removeAll();
                onSearch(value);
                break;
            }
            
            case R.id.btn_date_center:
            {
                tvThisWeek.setTextColor(titleTextColor);
                ivThisWeek.setVisibility(View.GONE);
                tvLastWeek.setTextColor(titleTextColor);
                ivLastWeek.setVisibility(View.GONE);
                tvThisMonth.setTextColor(titleTextColor);
                ivThisMonth.setVisibility(View.GONE);
                tvLastMonth.setTextColor(titleTextColor);
                ivLastMonth.setVisibility(View.GONE);
                tvDateCenter.setTextColor(color);
                ivDateCenter.setVisibility(View.VISIBLE);
                //showDatetimeDialog();
                if (chooseDateDialog != null && chooseDateDialog.isShowing())
                {
                    chooseDateDialog.dismiss();
                }
                chooseDateDialog =
                    new ChooseDateTimeDialog(mContext, "请任选任区间日期", new ChooseDateTimeDialog.ConfirmListener()
                    {
                        @Override
                        public void ok(String strStartDate, String strEndDate)
                        {
                            strTime = strStartDate;
                            endTime = strEndDate;
                            if (data != null)
                                data.clear();
                            adapter.removeAll();
                            onSearch(value);
                        }
                        
                        @Override
                        public void cancel()
                        {
                            
                        }
                    });
                DialogHelper.resize(OrderTotalActivity.this, chooseDateDialog);
                chooseDateDialog.show();
                break;
            }
            
        }
        
    }
    
    ChooseDateTimeDialog chooseDateDialog;
    
    public List<Order> removeDuplicate(List<Order> list)
    {
        for (int i = 0; i < list.size() - 1; i++)
        {
            for (int j = list.size() - 1; j > i; j--)
            {
                if (list.get(j).id == (list.get(i)).id)
                {
                    list.remove(j);
                }
            }
        }
        return list;
    }
    
    OrderReportsumDialog mDialogOrderReportsum;
    
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
    {
        
        if (data.size() > 0)
        {
            CashierReport c = data.get(i - 1);
            mDialogOrderReportsum = new OrderReportsumDialog(mContext, "提示", new ConfirmListener()
            {
                
                @Override
                public void ok()
                {
                    
                }
                
                @Override
                public void cancel()
                {
                    
                }
            }, c);
            DialogHelper.resize(OrderTotalActivity.this, mDialogOrderReportsum);
            mDialogOrderReportsum.show();
        }
        
    }
    
    private Calendar c = null;
    
    public void showDatetimeDialog()
    {
        c = Calendar.getInstance();
        c.setTimeInMillis(searchTime);
        dialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener()
        {
            public void onDateSet(DatePicker dp, int year, int month, int dayOfMonth)
            {
                c.set(year, month, dayOfMonth);
                
                searchTime = c.getTimeInMillis();
                Log.i(TAG,DateUtil.formatYMD(c.getTime()));
                strTime = year + "-" + (month + 1) + "-" + dayOfMonth;
                endTime = year + "-" + (month + 1) + "-" + dayOfMonth;
                if (DateUtil.isSameDay(System.currentTimeMillis(), searchTime))
                {
                    tvDatetime.setText("今天");
                }
                else
                {
                    tvDatetime.setText(year + "年" + (month + 1) + "月" + dayOfMonth + "日");
                }
                value = CacheEnum.CASHIER_DATE_TIME_TYPE;
                onSearch(value);
                
            }
        }, c.get(Calendar.YEAR), // 传入年份
            c.get(Calendar.MONTH), // 传入月份
            c.get(Calendar.DAY_OF_MONTH) // 传入天数*/
            );
        dialog.show();
    }
    
    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l)
    {
        return false;
    }
}
