/*
 * 文 件 名:  ClientEnum.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2015-8-7
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.enums;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2015-8-7]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public enum ClientEnum
{
    SPAY_AND("SPAY_ANDROID"), SPAY_IOS("SPAY_IOS"), SPAY_PC("SPAY_PC"), MPOS_AND("MPOS_and"), ERP("ERP"), PLATFORM(
        "platform"), UNKNOW(ToastHelper.toStr(R.string.tv_clinet));
    
    public String clientType;
    
    private ClientEnum(String clientType)
    {
        this.clientType = clientType;
    }
    
    public static String getClienName(String clientType)
    {
        if (clientType.equals(SPAY_AND.name()))
        {
            return MainApplication.getInstance().getString(R.string.spay_android);
        }
        else if (clientType.equals(SPAY_IOS.name()))
        {
            return MainApplication.getInstance().getString(R.string.spay_ios);
        }
        else if (clientType.startsWith(ClientEnum.SPAY_PC.name()))
        {
            return MainApplication.getInstance().getString(R.string.spay_pc);
            
        }
        else if (clientType.equalsIgnoreCase(ClientEnum.MPOS_AND.name()))
        {
            return ClientEnum.MPOS_AND.clientType;
        }
        else if (clientType.equals(ClientEnum.ERP.name()))
        {
            return ClientEnum.ERP.clientType;
        }
        else if (clientType.equalsIgnoreCase(ClientEnum.PLATFORM.name()))
        {
            return "商户平台";
        }
        else
        {
            return ToastHelper.toStr(R.string.tv_clinet);
        }
    }
}
