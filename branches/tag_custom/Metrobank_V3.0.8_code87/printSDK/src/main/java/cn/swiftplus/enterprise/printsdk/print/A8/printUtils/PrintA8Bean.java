package cn.swiftplus.enterprise.printsdk.print.A8.printUtils;

/**
 * Created by aijingya on 2020/10/15.
 *
 * @Package cn.swiftplus.enterprise.printsdk.print.A8.printUtils
 * @Description:
 * @date 2020/10/15.17:41.
 */
public class PrintA8Bean {
    private String print_text;
    private PRINT_FUNCTION_NAME Function_name;

    public enum PRINT_FUNCTION_NAME {
        TITLE,
        TEXT_LINE_LEFT,
        TEXT_LINE_RIGHT,
        TEXT_LINE_CENTER,
        TEXT_MULTI_LINES,
        SINGLE_LINE,
        DOUBLE_LINE,
        EMPTY_LINE,
        QR_CODE;
    }

    public String getPrint_text() {
        return print_text;
    }

    public void setPrint_text(String print_text) {
        this.print_text = print_text;
    }

    public PRINT_FUNCTION_NAME getFunction_name() {
        return Function_name;
    }

    public void setFunction_name(PRINT_FUNCTION_NAME function_name) {
        Function_name = function_name;
    }
}
