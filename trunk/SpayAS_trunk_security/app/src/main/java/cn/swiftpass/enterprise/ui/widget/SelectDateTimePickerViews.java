package cn.swiftpass.enterprise.ui.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.interfaces.OnTimeLongSelectListener;
import com.bigkoo.pickerview.interfaces.OnWheelTimeViewScrolledListener;
import com.bigkoo.pickerview.lib.WheelView;
import com.bigkoo.pickerview.listener.CustomListener;
import com.bigkoo.pickerview.utils.ShapreUtils;
import com.bigkoo.pickerview.view.BasePickerView;
import com.example.common.sentry.SentryUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.KotlinUtils;


public class SelectDateTimePickerViews extends BasePickerView implements View.OnClickListener {

    private static final String TAG = SelectDateTimePickerViews.class.getSimpleName();
    private int layoutRes;
    private CustomListener customListener;

    WheelTime wheelTime; //自定义控件

    private Button btnConfirm, btnReset; //确定、取消按钮
    private TextView tvTitle;//标题
    //    private TextView tvStartTime, tvEndTime;
    private LinearLayout llTimeFilter;
    private OnTimeSelectListener timeSelectListener;//回调接口
    private OnTimeLongSelectListener timeLongSelectListener;
    private int gravity = Gravity.CENTER;//内容显示位置 默认居中
    private boolean[] type;// 显示类型

    private String Str_Submit;//确定按钮字符串
    private String Str_Cancel;//取消按钮字符串
    private String Str_Title;//标题字符串

    private int Color_Submit;//确定按钮颜色
    private int Color_Cancel;//取消按钮颜色
    private int Color_Title;//标题颜色

    private int Color_Background_Wheel;//滚轮背景颜色
    private int Color_Background_Title;//标题背景颜色

    private int Size_Submit_Cancel;//确定取消按钮大小
    private int Size_Title;//标题字体大小
    private int Size_Content;//内容字体大小

    private Calendar date;//当前选中时间
    //private Calendar fromdate;//当前选中之前时间

    //private Calendar sdate;//当前选中时间
    private Calendar startDate;//开始时间
    private Calendar endDate;//终止时间
    private int startYear;//开始年份
    private int endYear;//结尾年份

    private boolean cyclic;//是否循环
    private boolean cancelable;//是否能取消
    private boolean isCenterLabel;//是否只显示中间的label
    private boolean isLunarCalendar = false;//是否显示农历

    private int textColorOut; //分割线以外的文字颜色
    private int textColorCenter; //分割线之间的文字颜色
    private int dividerColor; //分割线的颜色
    private int backgroundId; //显示时的外部背景色颜色,默认是灰色

    // 条目间距倍数 默认1.6
    private float lineSpacingMultiplier = 1.6F;
    private boolean isDialog;//是否是对话框模式
    private String label_year, label_month, label_day;
    private WheelView.DividerType dividerType;//分隔线类型

    private static final String TAG_CONFIRM = "confirm";
    private static final String TAG_RESET = "reset";
    private static final String TAG_START_TIME = "start_time";
    private static final String TAG_END_TIME = "end_time";
    private static final String TAG_LL_TIME_FILTER = "ll_time_filter";
    //    private static final String TAG_TOTAL_BAR = "total_bar";
//    private static final String TAG_TIME_SHOW = "time_show";
    private boolean isEnlish;//是否循环
    private ShapreUtils mShapreUtils;

    //构造方法
    public SelectDateTimePickerViews(Builder builder) {
        super(builder.context);
        this.timeSelectListener = builder.timeSelectListener;
        this.timeLongSelectListener = builder.timeLongSelectListener;
        this.gravity = builder.gravity;
        this.type = builder.type;
        this.Str_Submit = builder.Str_Submit;
        this.Str_Cancel = builder.Str_Cancel;
        this.Str_Title = builder.Str_Title;
        this.Color_Submit = builder.Color_Submit;
        this.Color_Cancel = builder.Color_Cancel;
        this.Color_Title = builder.Color_Title;
        this.Color_Background_Wheel = builder.Color_Background_Wheel;
        this.Color_Background_Title = builder.Color_Background_Title;
        this.Size_Submit_Cancel = builder.Size_Submit_Cancel;
        this.Size_Title = builder.Size_Title;
        this.Size_Content = builder.Size_Content;
        this.startYear = builder.startYear;
        this.endYear = builder.endYear;
        this.startDate = builder.startDate;
        this.endDate = builder.endDate;
        //this.fromdate = builder.fromdate;
        this.date = builder.date;
        this.cyclic = builder.cyclic;
        this.isCenterLabel = builder.isCenterLabel;
        this.isLunarCalendar = builder.isLunarCalendar;
        this.cancelable = builder.cancelable;
        this.label_year = builder.label_year;
        this.label_month = builder.label_month;
        this.label_day = builder.label_day;
        this.textColorCenter = builder.textColorCenter;
        this.textColorOut = builder.textColorOut;
        this.dividerColor = builder.dividerColor;
        this.customListener = builder.customListener;
        this.layoutRes = builder.layoutRes;
        this.lineSpacingMultiplier = builder.lineSpacingMultiplier;
        this.isDialog = builder.isDialog;
        this.dividerType = builder.dividerType;
        this.backgroundId = builder.backgroundId;
        this.decorView = builder.decorView;
        //添加是否是英文判断
        this.isEnlish = builder.isEnglish;
        Log.e("JAMY", "isEnglish1: " + isEnlish);
        initView(builder.context);
    }


    //建造器
    public static class Builder {
        private int layoutRes = R.layout.select_date_pickerview_times;
        private CustomListener customListener;
        private Context context;
        private OnTimeSelectListener timeSelectListener;
        private OnTimeLongSelectListener timeLongSelectListener;
        private boolean[] type = new boolean[]{true, true, true, true, true, false};//显示类型 默认全部显示
        private int gravity = Gravity.CENTER;//内容显示位置 默认居中

        private String Str_Submit;//确定按钮文字
        private String Str_Cancel;//取消按钮文字
        private String Str_Title;//标题文字

        private int Color_Submit;//确定按钮颜色
        private int Color_Cancel;//取消按钮颜色
        private int Color_Title;//标题颜色

        private int Color_Background_Wheel;//滚轮背景颜色
        private int Color_Background_Title;//标题背景颜色

        private int Size_Submit_Cancel = 17;//确定取消按钮大小
        private int Size_Title = 18;//标题字体大小
        private int Size_Content = 18;//内容字体大小
        private Calendar date;//当前选中时间
        //private Calendar fromdate;//当前选中之前的时间
        private Calendar startDate;//开始时间
        private Calendar endDate;//终止时间
        private int startYear;//开始年份
        private int endYear;//结尾年份

        private boolean cyclic = false;//是否循环
        private boolean cancelable = true;//是否能取消

        private boolean isCenterLabel = true;//是否只显示中间的label
        private boolean isLunarCalendar = false;//是否显示农历
        public ViewGroup decorView;//显示pickerview的根View,默认是activity的根view

        private int textColorOut; //分割线以外的文字颜色
        private int textColorCenter; //分割线之间的文字颜色
        private int dividerColor; //分割线的颜色
        private int backgroundId; //显示时的外部背景色颜色,默认是灰色
        private WheelView.DividerType dividerType;//分隔线类型
        // 条目间距倍数 默认1.6
        private float lineSpacingMultiplier = 1.6F;

        private boolean isDialog;//是否是对话框模式

        private String label_year, label_month, label_day;
        private boolean isEnglish;

        //Required
        public Builder(Context context) {
            this.context = context;
        }

        public Builder(Context context, OnTimeSelectListener listener) {
            this.context = context;
            this.timeSelectListener = listener;
        }

        public Builder(Context context, OnTimeLongSelectListener listener) {
            this.context = context;
            this.timeLongSelectListener = listener;
        }

        //Option
        public Builder setType(boolean[] type) {
            this.type = type;
            return this;
        }

        public Builder setGravity(int gravity) {
            this.gravity = gravity;
            return this;
        }

        public Builder setConfirmText(String Str_Submit) {
            this.Str_Submit = Str_Submit;
            return this;
        }

        public Builder isDialog(boolean isDialog) {
            this.isDialog = isDialog;
            return this;
        }

        public Builder setResetText(String Str_Cancel) {
            this.Str_Cancel = Str_Cancel;
            return this;
        }

        public Builder setTitleText(String Str_Title) {
            this.Str_Title = Str_Title;
            return this;
        }

        public Builder setSubmitColor(int Color_Submit) {
            this.Color_Submit = Color_Submit;
            return this;
        }

        public Builder setCancelColor(int Color_Cancel) {
            this.Color_Cancel = Color_Cancel;
            return this;
        }

        /**
         * 必须是viewgroup
         * 设置要将pickerview显示到的容器id
         *
         * @param decorView
         * @return
         */
        public Builder setDecorView(ViewGroup decorView) {
            this.decorView = decorView;
            return this;
        }

        public Builder setBgColor(int Color_Background_Wheel) {
            this.Color_Background_Wheel = Color_Background_Wheel;
            return this;
        }

        public Builder setTitleBgColor(int Color_Background_Title) {
            this.Color_Background_Title = Color_Background_Title;
            return this;
        }

        public Builder setTitleColor(int Color_Title) {
            this.Color_Title = Color_Title;
            return this;
        }

        public Builder setSubCalSize(int Size_Submit_Cancel) {
            this.Size_Submit_Cancel = Size_Submit_Cancel;
            return this;
        }

        public Builder setTitleSize(int Size_Title) {
            this.Size_Title = Size_Title;
            return this;
        }

        public Builder setContentSize(int Size_Content) {
            this.Size_Content = Size_Content;
            return this;
        }

        /**
         * 因为系统Calendar的月份是从0-11的,所以如果是调用Calendar的set方法来设置时间,月份的范围也要是从0-11
         *
         * @param date
         * @return
         */
        public Builder setDate(Calendar date) {
            this.date = date;
            return this;
        }

        /**
         * 因为系统Calendar的月份是从0-11的,所以如果是调用Calendar的set方法来设置时间,月份的范围也要是从0-11
         *
         * @param fromdate
         * @return
         */
        public Builder setFromDate(Calendar fromdate) {
            //this.fromdate = fromdate;
            return this;
        }

        public Builder setLayoutRes(int res, CustomListener customListener) {
            this.layoutRes = res;
            this.customListener = customListener;
            return this;
        }

        public Builder setRange(int startYear, int endYear) {
            this.startYear = startYear;
            this.endYear = endYear;
            return this;
        }

        /**
         * 设置起始时间
         * 因为系统Calendar的月份是从0-11的,所以如果是调用Calendar的set方法来设置时间,月份的范围也要是从0-11
         *
         * @return
         */

        public Builder setRangDate(Calendar startDate, Calendar endDate) {
            this.startDate = startDate;
            this.endDate = endDate;
            return this;
        }


        /**
         * 设置间距倍数,但是只能在1.2-2.0f之间
         *
         * @param lineSpacingMultiplier
         */
        public Builder setLineSpacingMultiplier(float lineSpacingMultiplier) {
            this.lineSpacingMultiplier = lineSpacingMultiplier;
            return this;
        }

        /**
         * 设置分割线的颜色
         *
         * @param dividerColor
         */
        public Builder setDividerColor(int dividerColor) {
            this.dividerColor = dividerColor;
            return this;
        }

        /**
         * 设置分割线的类型
         *
         * @param dividerType
         */
        public Builder setDividerType(WheelView.DividerType dividerType) {
            this.dividerType = dividerType;
            return this;
        }

        /**
         * //显示时的外部背景色颜色,默认是灰色
         *
         * @param backgroundId
         */

        public Builder setBackgroundId(int backgroundId) {
            this.backgroundId = backgroundId;
            return this;
        }

        /**
         * 设置分割线之间的文字的颜色
         *
         * @param textColorCenter
         */
        public Builder setTextColorCenter(int textColorCenter) {
            this.textColorCenter = textColorCenter;
            return this;
        }

        /**
         * 设置分割线以外文字的颜色
         *
         * @param textColorOut
         */
        public Builder setTextColorOut(int textColorOut) {
            this.textColorOut = textColorOut;
            return this;
        }

        public Builder isCyclic(boolean cyclic) {
            this.cyclic = cyclic;
            return this;
        }

        public Builder setOutSideCancelable(boolean cancelable) {
            this.cancelable = cancelable;
            return this;
        }

        public Builder setLunarCalendar(boolean lunarCalendar) {
            isLunarCalendar = lunarCalendar;
            return this;
        }

        public Builder setLabel(String label_year, String label_month, String label_day) {
            this.label_year = label_year;
            this.label_month = label_month;
            this.label_day = label_day;
            return this;
        }

        public Builder isCenterLabel(boolean isCenterLabel) {
            this.isCenterLabel = isCenterLabel;
            return this;
        }

        public Builder setLanguage(boolean isEnglish) {
            this.isEnglish = isEnglish;
            Log.e("JAMY", "isEnglish0: " + isEnglish);
            return this;
        }

        public SelectDateTimePickerViews build() {
            return new SelectDateTimePickerViews(this);
        }
    }


    private void initView(Context context) {
        setDialogOutSideCancelable(cancelable);
        initViews(backgroundId);
        init();
        initEvents();
        //新增时间选择缓存
        mShapreUtils = new ShapreUtils(context);

        if (customListener == null) {
            LayoutInflater.from(context).inflate(R.layout.select_date_pickerview_times, contentContainer);

            //顶部标题
            tvTitle = (TextView) findViewById(R.id.tvTitle);

            //确定和取消按钮
            btnConfirm = (Button) findViewById(R.id.btnConfirm);
            btnReset = (Button) findViewById(R.id.btnReset);

//            //开始/结束时间
//            tvStartTime = (TextView) findViewById(R.id.id_tv_start_time);
//            tvEndTime = (TextView) findViewById(R.id.id_tv_end_time);
//            tvStartTime.setTag(TAG_START_TIME);
//            tvEndTime.setTag(TAG_END_TIME);
//            tvStartTime.setSelected(true);
//            tvStartTime.setOnClickListener(this);
//            tvEndTime.setOnClickListener(this);

            llTimeFilter = (LinearLayout) findViewById(R.id.id_ll_time_filter);
            llTimeFilter.setTag(TAG_LL_TIME_FILTER);
            llTimeFilter.setOnClickListener(this);


            btnConfirm.setTag(TAG_CONFIRM);
            btnReset.setTag(TAG_RESET);

            btnConfirm.setOnClickListener(this);
            btnReset.setOnClickListener(this);


            //设置文字
            btnConfirm.setText(TextUtils.isEmpty(Str_Submit) ? context.getResources().getString(R.string.pickerview_submit) : Str_Submit);
            btnReset.setText(TextUtils.isEmpty(Str_Cancel) ? context.getResources().getString(R.string.pickerview_cancel) : Str_Cancel);
            tvTitle.setText(TextUtils.isEmpty(Str_Title) ? "" : Str_Title);//默认为空

            //设置文字颜色
            btnConfirm.setTextColor(Color_Submit == 0 ? pickerview_timebtn_nor : Color_Submit);
            btnReset.setTextColor(Color_Cancel == 0 ? pickerview_timebtn_nor : Color_Cancel);
            tvTitle.setTextColor(Color_Title == 0 ? pickerview_topbar_title : Color_Title);

            //设置文字大小
            btnConfirm.setTextSize(Size_Submit_Cancel);
            btnReset.setTextSize(Size_Submit_Cancel);
            tvTitle.setTextSize(Size_Title);
            RelativeLayout rv_top_bar = (RelativeLayout) findViewById(R.id.rv_topbar);
            rv_top_bar.setBackgroundColor(Color_Background_Title == 0 ? pickerview_bg_topbar : Color_Background_Title);
        } else {
            customListener.customLayout(LayoutInflater.from(context).inflate(layoutRes, contentContainer));
        }
        // 时间转轮 自定义控件
        LinearLayout timePickerView = (LinearLayout) findViewById(R.id.timepicker);
        timePickerView.setBackgroundColor(Color_Background_Wheel == 0 ? bgColor_default : Color_Background_Wheel);
        wheelTime = new WheelTime(timePickerView, type, gravity, Size_Content);
        wheelTime.setLunarCalendar(isLunarCalendar);
        wheelTime.setLanguage(isEnlish);

//        LinearLayout timePickerViews = (LinearLayout) findViewById(R.id.timepickers);
//        wheelTimes = new WheelTime(timePickerViews, type, gravity, Size_Content);
//        wheelTimes.setLunarCalendar(isLunarCalendar);
//        wheelTimes.setLanguage(isEnlish);
        //添加语言判断
        if (startYear != 0 && endYear != 0 && startYear <= endYear) {
            setRange();
        }
        if (startDate != null && endDate != null) {
            if (startDate.getTimeInMillis() <= endDate.getTimeInMillis()) {
                setRangDate();
            }
        } else if (startDate != null && endDate == null) {
            setRangDate();
        } else if (startDate == null && endDate != null) {
            setRangDate();
        }

        setTime();
        setOutSideCancelable(cancelable);

        wheelTime.setLabels(label_year, label_month, label_day);
        wheelTime.setCyclic(cyclic);
        wheelTime.setDividerColor(dividerColor);
        wheelTime.setDividerType(dividerType);
        wheelTime.setLineSpacingMultiplier(lineSpacingMultiplier);
        wheelTime.setTextColorOut(textColorOut);
        wheelTime.setTextColorCenter(textColorCenter);
        wheelTime.isCenterLabel(isCenterLabel);

        wheelTime.setOnWheelTimeViewScrolledListener(new OnWheelTimeViewScrolledListener() {
            @Override
            public void onWheelTimeViewScrolled(int type) {

//                if (tvStartTime.isSelected()) {
//                    tvStartTime.setText(getWheelTimeSelectTime());
//                } else if (tvEndTime.isSelected()) {
//                    tvEndTime.setText(getWheelTimeSelectTime());
//                }
            }
        });


//        tvStartTime.setText(getWheelTimeSelectTime());
//        tvEndTime.setText(getSystemCurrentTime());
    }


    /**
     * 获取 wheelTime控件当前选中的时间
     *
     * @return String
     */
    private String getWheelTimeSelectTime() {
        try {
            Date startDate = WheelTime.formatDate(wheelTime.getTime());
            long startTime = startDate.getTime();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String time = format.format(new Date(startTime));
            return time;
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            return "";
        }
    }

    /**
     * 获取当天零点时间
     *
     * @return
     */
    private String getTodayZero() {
        Date date = new Date();
        long l = 24 * 60 * 60 * 1000; //每天的毫秒数
        //date.getTime()是现在的毫秒数，它 减去 当天零点到现在的毫秒数（ 现在的毫秒数%一天总的毫秒数，取余。），理论上等于零点的毫秒数，不过这个毫秒数是UTC+0时区的。
        //减8个小时的毫秒值是为了解决时区的问题。
        long zeroTime = (date.getTime() - (date.getTime() % l) - 8 * 60 * 60 * 1000);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String time = format.format(new Date(zeroTime));
        return time;
    }


    /**
     * 获取当前系统时间
     *
     * @return String
     */
    private String getSystemCurrentTime() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(new Date());
    }

    /**
     * 设置默认时间
     */
    public void setDate(Calendar date) {
        this.date = date;
        setTime();
    }

    /**
     * 设置可以选择的时间范围, 要在setTime之前调用才有效果
     */
    private void setRange() {
        wheelTime.setStartYear(startYear);
        wheelTime.setEndYear(endYear);
    }

    /**
     * 设置可以选择的时间范围, 要在setTime之前调用才有效果
     */
    private void setRangDate() {
        wheelTime.setRangDate(startDate, endDate);
        //如果设置了时间范围
        if (startDate != null && endDate != null) {
            //判断一下默认时间是否设置了，或者是否在起始终止时间范围内
            if (date == null || date.getTimeInMillis() < startDate.getTimeInMillis()
                    || date.getTimeInMillis() > endDate.getTimeInMillis()) {
                date = startDate;
            }
        } else if (startDate != null) {
            //没有设置默认选中时间,那就拿开始时间当默认时间
            date = startDate;
        } else if (endDate != null) {
            date = endDate;
        }
    }

    /**
     * 设置选中时间,默认选中当前时间
     */
    //private Date tempEDate,tempSDate;
    private void setTime() {
        int year, month, day, hours, minute;
        //int  seconds;
        int fromyear, frommonth, fromday, fromhours, fromminute;
        //int fromseconds;
        long tempetime = 0l, tempstime = 0l;
        Calendar tocalendar = Calendar.getInstance();
        Calendar fromcalendar = Calendar.getInstance();
        tocalendar.setTimeInMillis(System.currentTimeMillis());
        if (date == null) {
            tocalendar.setTimeInMillis(System.currentTimeMillis());
            year = tocalendar.get(Calendar.YEAR);
            month = tocalendar.get(Calendar.MONTH);
            day = tocalendar.get(Calendar.DAY_OF_MONTH);
            hours = tocalendar.get(Calendar.HOUR_OF_DAY);
            minute = tocalendar.get(Calendar.MINUTE);
            //seconds = 59;
            fromyear = tocalendar.get(Calendar.YEAR);
            frommonth = tocalendar.get(Calendar.MONTH);
            fromday = tocalendar.get(Calendar.DAY_OF_MONTH);
            //fromhours = 00;
            //fromminute = 00;
            //fromseconds = 00;
            Log.e("JAMY", "Month: " + frommonth + " day: " + fromday);
            wheelTime.setPicker(fromyear, frommonth, fromday);
        } else {
            Log.e("JAMY", "currentTime: " + System.currentTimeMillis() + " date: " + date.getTimeInMillis());
            tempetime = mShapreUtils.getTotime();
            tempstime = mShapreUtils.getFromtime();
            if (!mShapreUtils.isSelect()) {
                if (0l != tempstime && 0l != tempetime) {
                    fromcalendar.setTimeInMillis(tempstime);
                    tocalendar.setTimeInMillis(tempetime);

                    fromyear = fromcalendar.get(Calendar.YEAR);
                    frommonth = fromcalendar.get(Calendar.MONTH);
                    fromday = fromcalendar.get(Calendar.DAY_OF_MONTH);
                    fromhours = fromcalendar.get(Calendar.HOUR_OF_DAY);
                    fromminute = fromcalendar.get(Calendar.MINUTE);
                    //fromseconds = 00;

                    year = tocalendar.get(Calendar.YEAR);
                    month = tocalendar.get(Calendar.MONTH);
                    day = tocalendar.get(Calendar.DAY_OF_MONTH);
                    hours = tocalendar.get(Calendar.HOUR_OF_DAY);
                    minute = tocalendar.get(Calendar.MINUTE);
                    //seconds = 59;
                    wheelTime.setPicker(fromyear, frommonth, fromday);
                } else {
                    year = date.get(Calendar.YEAR);
                    month = date.get(Calendar.MONTH);
                    day = date.get(Calendar.DAY_OF_MONTH);
                    hours = date.get(Calendar.HOUR_OF_DAY);
                    minute = date.get(Calendar.MINUTE);
                    //seconds = 00;
                    fromyear = year;
                    frommonth = month;
                    fromday = day;
                    //fromhours = hours;
                    //fromminute = minute;
                    //fromseconds = seconds;
                    wheelTime.setPicker(fromyear, frommonth, fromday);
                }
            } else {
                year = date.get(Calendar.YEAR);
                month = date.get(Calendar.MONTH);
                day = date.get(Calendar.DAY_OF_MONTH);
                hours = date.get(Calendar.HOUR_OF_DAY);
                minute = date.get(Calendar.MINUTE);
                //seconds = 00;
                fromyear = year;
                frommonth = month;
                fromday = day;
                //fromhours = hours;
                //fromminute = minute;
                //fromseconds = seconds;
                wheelTime.setPicker(fromyear, frommonth, fromday);
            }
        }


    }

    @Override
    public void onClick(View v) {
        String tag = (String) v.getTag();
        switch (tag) {
            case TAG_LL_TIME_FILTER:
                //点击整个弹框布局
                break;
            case TAG_RESET:
                //点击 "重置"
//                tvStartTime.setText(getTodayZero());
//                tvEndTime.setText(getSystemCurrentTime());
                setDate(Calendar.getInstance());
                break;
//            case TAG_START_TIME:
//                //点击 "开始时间"
//                tvStartTime.setSelected(true);
//                tvEndTime.setSelected(false);
//                break;
//            case TAG_END_TIME:
//                //点击 "结束时间"
//                tvStartTime.setSelected(false);
//                tvEndTime.setSelected(true);
//                break;
            case TAG_CONFIRM:
                returnData();
                dismiss();
                break;
            default:
                break;
        }


//        if (tag.equals(TAG_SUBMIT)) {
//            returnData();
//        }
//        dismiss();
    }

    public void returnData() {
        if (timeSelectListener != null) {
            try {
                String date = wheelTime.getTime();
                Date d = stringToDate(date);
                long time = d.getTime();
                timeSelectListener.onTimeSelect(date, time, clickView);
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
            }
        }

        if (timeLongSelectListener != null) {
            try {
//                Date startDate = stringToDate(tvStartTime.getText().toString().trim());
//                Date endDate = stringToDate(tvEndTime.getText().toString().trim());
//                timeLongSelectListener.onTimeSelect(startDate, endDate, clickView);
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
            }
        }
    }


    private Date stringToDate(String strTime)
            throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        date = formatter.parse(strTime);
        return date;
    }


    public void setLunarCalendar(boolean lunar) {
        try {
            int year, month, day;
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(WheelTime.formatDate(wheelTime.getTime()));
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);
            wheelTime.setLunarCalendar(lunar);
            wheelTime.setLabels(label_year, label_month, label_day);
            wheelTime.setPicker(year, month, day);
        } catch (ParseException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
    }

    public boolean isLunarCalendar() {
        return wheelTime.isLunarCalendar();
    }

    public interface OnTimeSelectListener {
        void onTimeSelect(String date, long time, View v);
    }

    @Override
    public boolean isDialog() {
        return isDialog;
    }
}