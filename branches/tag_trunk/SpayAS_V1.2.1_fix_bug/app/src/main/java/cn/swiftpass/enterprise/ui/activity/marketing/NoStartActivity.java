/*
 * 文 件 名:  NoStartActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-1-15
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.marketing;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.order.HistoryRankingManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.AwardModel;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.PullDownListView;
import cn.swiftpass.enterprise.ui.widget.TitleBar;

/**
 * 未开始活动列表
 * 
 * @author  he_hui
 * @version  [版本号, 2016-1-15]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class NoStartActivity extends TemplateActivity implements OnItemClickListener,
    PullDownListView.OnRefreshListioner
{
    private PullDownListView marketing_list;
    
    private List<AwardModel> list;
    
    private NoStartAdapter noStartAdapter;
    
    private ListView listView;
    
    private Handler mHandler = new Handler();
    
    private int pageFulfil = 0;
    
    String activeStatus;
    
    private List<AwardModel> results = new ArrayList<AwardModel>();
    
    private LinearLayout lay_start;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nostart_marketing);
        initObject();
        
        initView();
        
        activeStatus = getIntent().getStringExtra("activeStatus");
        if (!"".equals(activeStatus) && null != activeStatus)
        {
            loadDate(0, false, "2");
            titleBar.setTitle(R.string.tx_stop);
        }
    }
    
    @Override
    protected void onResume()
    {
        // TODO Auto-generated method stub
        super.onResume();
        
        if ("".equals(activeStatus) || null == activeStatus)
        {
            titleBar.setTitle(R.string.tx_no_start);
            loadDate(0, false, "0");
        }
    }
    
    private void loadDate(int page, final boolean isLoadMore, String activeStatus)
    {
        HistoryRankingManager.getInstance().getActiveList("1",
            activeStatus,
            page,
            new UINotifyListener<List<AwardModel>>()
            {
                @Override
                public void onPreExecute()
                {
                    super.onPreExecute();
                    if (pageFulfil == 0)
                    {
                        showNewLoading(true, getString(R.string.public_data_loading));
                    }
                }
                
                @Override
                public void onError(final Object object)
                {
                    super.onError(object);
                    dismissLoading();
                    if (checkSession())
                    {
                        return;
                    }
                    if (object != null)
                    {
                        showToastInfo(object.toString());
                    }
                }
                
                @Override
                public void onSucceed(List<AwardModel> result)
                {
                    super.onSucceed(result);
                    dismissLoading();
                    if (result != null && result.size() > 0)
                    {
                        lay_start.setVisibility(View.GONE);
                        results = result;
                        mySetListData(marketing_list, list, noStartAdapter, result, isLoadMore);
                        if (isLoadMore)
                        {
                            pageFulfil += 1;
                        }
                        
                        marketing_list.setMore(true);
                    }
                    else
                    {
                        if (results != null && results.size() == 0)
                        {
                            
                            marketing_list.setVisibility(View.GONE);
                            lay_start.setVisibility(View.VISIBLE);
                        }
                    }
                    
                }
            });
        
    }
    
    public static void startActivity(Context context, String activeStatus)
    {
        Intent it = new Intent();
        it.setClass(context, NoStartActivity.class);
        it.putExtra("activeStatus", activeStatus);
        context.startActivity(it);
    }
    
    private void mySetListData(final PullDownListView pull, List<AwardModel> list, NoStartAdapter adapter,
        List<AwardModel> result, boolean isLoadMore)
    {
        if (!isLoadMore)
        {
            pull.onfinish(getStringById(R.string.refresh_successfully));// 刷新完成
            
            mHandler.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    // 这里表示刷新处理完成后把上面的加载刷新界面隐藏
                    pull.onRefreshComplete();
                }
            }, 800);
            
            list.clear();
        }
        else
        {
            mHandler.postDelayed(new Runnable()
            {
                
                @Override
                public void run()
                {
                    pull.onLoadMoreComplete();
                    // pull.setMore(true);
                }
            }, 1000);
        }
        list.addAll(result);
        adapter.notifyDataSetChanged();
        
        if (list.size() > 0)
        {
            pull.setVisibility(View.VISIBLE);
        }
        else
        {
            pull.setVisibility(View.GONE);
        }
    }
    
    private void initView()
    {
        lay_start = getViewById(R.id.lay_start);
        marketing_list = getViewById(R.id.marketing_list);
        marketing_list.setAutoLoadMore(true);
        marketing_list.setRefreshListioner(this);
        
        listView = marketing_list.mListView;
        listView.setAdapter(noStartAdapter);
        listView.setOnItemClickListener(this);
    }
    
    private void initObject()
    {
        list = new ArrayList<AwardModel>();
        noStartAdapter = new NoStartAdapter(NoStartActivity.this, list);
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tx_no_start);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
            }
            
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
        });
    }
    
    private class NoStartAdapter extends BaseAdapter
    {
        private Context context;
        
        private List<AwardModel> scanList;
        
        private ViewHolder holder;
        
        public NoStartAdapter(Context context, List<AwardModel> scanList)
        {
            this.context = context;
            
            this.scanList = scanList;
        }
        
        @Override
        public int getCount()
        {
            return scanList.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            return scanList.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(context, R.layout.activity_maketing_list_item, null);
                holder = new ViewHolder();
                holder.tv_title = (TextView)convertView.findViewById(R.id.tv_title);
                holder.tv_time = (TextView)convertView.findViewById(R.id.tv_time);
                
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            AwardModel awardModel = scanList.get(position);
            if (awardModel != null)
            {
                
                holder.tv_title.setText(awardModel.getActiveName());
                holder.tv_time.setText(awardModel.getStartDate() + "-" + awardModel.getEndDate());
            }
            
            return convertView;
        }
        
        private class ViewHolder
        {
            private TextView tv_title, tv_time;
        }
    }
    
    @Override
    public void onRefresh()
    {
        pageFulfil = 0;
        if (!TextUtils.isEmpty(activeStatus))
        {
            loadDate(pageFulfil, false, "2");
        }
        else
        {
            loadDate(pageFulfil, false, "0");
        }
    }
    
    @Override
    public void onLoadMore()
    {
        if (!TextUtils.isEmpty(activeStatus))
        {
            loadDate(pageFulfil + 1, true, "2");
        }
        else
        {
            loadDate(pageFulfil + 1, true, "0");
        }
    }
    
    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
    {
        AwardModel awardModel = list.get(position - 1);
        if (awardModel != null)
        {
            pageFulfil = 0;
            HistoryRankingManager.getInstance().queryActiveDetais(awardModel.getActiveId(),
                new UINotifyListener<AwardModel>()
                {
                    @Override
                    public void onPreExecute()
                    {
                        super.onPreExecute();
                        
                        showNewLoading(true, getString(R.string.public_data_loading));
                    }
                    
                    @Override
                    public void onError(final Object object)
                    {
                        super.onError(object);
                        dismissLoading();
                        if (checkSession())
                        {
                            return;
                        }
                        if (object != null)
                        {
                            
                            showToastInfo(object.toString());
                        }
                    }
                    
                    @Override
                    public void onSucceed(AwardModel result)
                    {
                        super.onSucceed(result);
                        dismissLoading();
                        
                        if (result != null)
                        {
                            MarketingAddActivity.startActivity(NoStartActivity.this, result);
                        }
                        
                    }
                });
        }
    }
    
}
