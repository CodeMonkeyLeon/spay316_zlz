package cn.swiftpass.enterprise.io.database.access;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.enums.DownloadStatus;
import cn.swiftpass.enterprise.bussiness.model.DownloadInfo;
import cn.swiftpass.enterprise.io.database.table.DownloadTable;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.util.List;

public class DownloadDB
{
	private static Dao<DownloadInfo,Integer> downloadDao;
	private DownloadDB()
	{
		try
		{
			downloadDao = MainApplication.getContext().getHelper().getDao(DownloadInfo.class);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	private static DownloadDB instance;
	public static DownloadDB getInstance()
	{
		if(instance == null)
		{
			instance = new DownloadDB();
		}
		return instance;
	}
	
	/**根据URL获取下载数据*/
	public  DownloadInfo quaryDownloadInfo1(String url) throws SQLException
	{
		QueryBuilder<DownloadInfo,Integer> builder = downloadDao.queryBuilder();
		Where<DownloadInfo,Integer> where =  builder.where();
		where.eq(DownloadTable.COLUMN_DOWN_URL, url);
		List<DownloadInfo> lists = downloadDao.query(builder.prepare());
		if(null != lists && lists.size() > 0)
		{
			return lists.get(0);
		}
		return null;
		
	}
	/**根据Did获取下载数据*/
	public  DownloadInfo quaryDownloadInfo(long dId) throws SQLException
	{
		QueryBuilder<DownloadInfo,Integer> builder = downloadDao.queryBuilder();
		Where<DownloadInfo,Integer> where =  builder.where();
		where.eq(DownloadTable.COLUMN_DOWNLOAD_ID, dId);
		List<DownloadInfo> lists = downloadDao.query(builder.prepare());
		if(null != lists && lists.size() > 0)
		{
			return lists.get(0);
		}
		return null;
		
	}
	/**根据径删除*/
	public int deleteADownLoad(long dID) throws SQLException
	{
		DeleteBuilder<DownloadInfo,Integer> builder = downloadDao.deleteBuilder();
		Where<DownloadInfo,Integer> where = builder.where();
		where.eq(DownloadTable.COLUMN_DOWNLOAD_ID, dID);
		return downloadDao.delete(builder.prepare());
	}
	/**创建下载记录 */
	public int addADownLoad(DownloadInfo downloadInfo) throws SQLException{
		return downloadDao.create(downloadInfo);
	}
	/**修改下载信息*/
	public int updateADDownload(DownloadInfo downloadInfo) throws SQLException
	{
		return downloadDao.update(downloadInfo);
	}
	/**修改下载信息*/
	public int updateADDownloadByDId(DownloadInfo downloadInfo) throws SQLException
	{
		UpdateBuilder<DownloadInfo,Integer> builder = downloadDao.updateBuilder();
		builder.updateColumnValue(DownloadTable.COLUMN_DOWN_STATUS, downloadInfo.mDownStatus);
		Where<DownloadInfo,Integer> where = builder.where();
		where.eq(DownloadTable.COLUMN_DOWNLOAD_ID, downloadInfo.downloadId);
		return builder.update();
	}
	
	/**程序启动的时候检查未下载失败的状态*/
	public int deleteByFaill() throws SQLException
	{
		DeleteBuilder<DownloadInfo,Integer> builder = downloadDao.deleteBuilder();
		Where<DownloadInfo,Integer> where = builder.where();
		where.eq(DownloadTable.COLUMN_DOWN_STATUS, DownloadStatus.STATUS_DOWN_DOWNING);
		return downloadDao.delete(builder.prepare());
	}
	/**程序启动的时候检查正在下载的将变成下载暂停的状态*/
	public int updateStateInitPause() throws SQLException
	{
		
		UpdateBuilder<DownloadInfo,Integer> builder = downloadDao.updateBuilder();
		builder.updateColumnValue(DownloadTable.COLUMN_DOWN_STATUS, DownloadStatus.STATUS_DOWN_PAUSE);
		Where<DownloadInfo,Integer> where = builder.where();
		where.eq(DownloadTable.COLUMN_DOWN_STATUS, DownloadStatus.STATUS_DOWN_DOWNING);
		return downloadDao.update(builder.prepare());
	}
	/**根据状态获取*/
	public List<DownloadInfo> getDownloadsByStatus(DownloadStatus status) throws SQLException
	{
		QueryBuilder<DownloadInfo,Integer> builder = downloadDao.queryBuilder();
		Where<DownloadInfo,Integer> where = builder.where();
		where.eq(DownloadTable.COLUMN_DOWN_STATUS, status);
		return downloadDao.query(builder.prepare());
		
	}
	/**获取必须下载的 */
	public List<DownloadInfo> getNeedDownloads() throws SQLException
	{
		QueryBuilder<DownloadInfo,Integer> builder = downloadDao.queryBuilder();
		Where<DownloadInfo,Integer> where = builder.where();
		where.eq(DownloadTable.COLUMN_DOWN_STATUS,DownloadStatus.STATUS_DOWN_DOWNING);
		where.or();
		where.eq(DownloadTable.COLUMN_DOWN_STATUS, DownloadStatus.STATUS_DOWN_READY);
		where.or();
		where.eq(DownloadTable.COLUMN_DOWN_STATUS, DownloadStatus.STATUS_DOWN_PAUSE);
		return downloadDao.query(where.prepare());
		
	}
}
