package cn.swiftpass.enterprise.ui.paymentlink.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkCustomer;
import cn.swiftpass.enterprise.ui.paymentlink.widget.SlideItemView1;

/**
 * Created by congwei.li on 2021/9/14.
 *
 * @Description: 顾客列表adapter
 */
public class CustomerListAdapter extends BaseRecyclerAdapter<PaymentLinkCustomer> {

    private Context mContext;
    private OnListItemClickListener clickListener;
    private SlideItemView1 openItem;

    private boolean isDeleteAble = true;

    public void setItemClickListener(OnListItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void cleanItemClickListener() {
        this.clickListener = null;
    }

    public CustomerListAdapter(Context context, List<PaymentLinkCustomer> data, boolean deleteAble) {
        super(R.layout.item_pl_main_list_item, data);
        mContext = context;
        isDeleteAble = deleteAble;
    }

    public CustomerListAdapter(Context context, List<PaymentLinkCustomer> data) {
        this(context, data, true);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder holder, PaymentLinkCustomer item, int position) {
        if (item == null) {
            return;
        }
        TextView desc1 = holder.getView(R.id.tv_desc1);
        TextView desc2 = holder.getView(R.id.tv_desc2);
        TextView desc3 = holder.getView(R.id.tv_desc3);
        TextView desc4 = holder.getView(R.id.tv_desc4);
        TextView desc5 = holder.getView(R.id.tv_desc5);

        desc1.setText(item.custName);
        if (!TextUtils.isEmpty(item.custMobile)) {
            desc2.setText(mContext.getResources().getString(R.string.pl_customer_item_tel) + " "
                    + item.custMobile);
        } else {
            desc2.setText("");
        }
        if (!TextUtils.isEmpty(item.custEmail)) {
            desc3.setText(mContext.getResources().getString(R.string.pl_customer_item_email) + " "
                    + item.custEmail);
        } else {
            desc3.setText("");
        }
        desc4.setVisibility(View.GONE);
        desc5.setVisibility(View.GONE);
        desc2.setEllipsize(TextUtils.TruncateAt.END);
        desc2.setMaxLines(1);
        desc3.setEllipsize(TextUtils.TruncateAt.END);
        desc3.setMaxLines(1);

        SlideItemView1 view = (SlideItemView1) holder.itemView;
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // 禁止滑动
                return !isDeleteAble;
            }
        });
        view.setOnStateChangeListener(new SlideItemView1.OnStateChangeListener() {
            @Override
            public void onFuncShow(SlideItemView1 itemView, boolean isShow) {
                if (isShow) {
                    if (null != openItem && itemView != openItem) {
                        openItem.reset();
                        openItem = itemView;
                    } else if (null == openItem) {
                        openItem = itemView;
                    }
                } else {
                    if (null != openItem && itemView == openItem) {
                        openItem = null;
                    }
                }
            }

            @Override
            public void onFuncClick() {
                if (null != openItem && openItem.isExpansion()) {
                    openItem.reset();
                    openItem = null;
                }
                if (clickListener != null) {
                    clickListener.onDeleteClick(position);
                }
            }

            @Override
            public void onContentClick() {
                if (clickListener != null) {
                    clickListener.onItemClick(position);
                }
            }
        });
    }
}
