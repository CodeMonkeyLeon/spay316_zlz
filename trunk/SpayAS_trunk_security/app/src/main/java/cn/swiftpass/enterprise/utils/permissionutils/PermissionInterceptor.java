package cn.swiftpass.enterprise.utils.permissionutils;

import androidx.fragment.app.FragmentActivity;

import java.util.List;

/**
 * Created by congwei.li on 2021/5/24.
 *
 * @Description:
 */
public class PermissionInterceptor implements IPermissionInterceptor {

    @Override
    public void grantedPermissions(FragmentActivity activity, OnPermissionCallback callback,
                                   List<String> permissions, boolean all) {
        // 回调授权结果的方法
        callback.onGranted(permissions, all);
    }

    @Override
    public void deniedPermissions(FragmentActivity activity, OnPermissionCallback callback,
                                  List<String> permissions, boolean never) {
        // 回调授权失败的方法
        callback.onDenied(permissions, never);
    }

}