package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created by aijingya on 2018/8/29.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/8/29.16:39.
 */

public class ForgetPSWBean implements Serializable {

    private String token;//忘记密码发送验证码接口返回
    private String ee;
    private String input; //输入的商户号或者邮箱
    private String EmailCode; //输入的验证码

    public String getEmailCode() {
        return EmailCode;
    }

    public void setEmailCode(String emailCode) {
        EmailCode = emailCode;
    }


    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEe() {
        return ee;
    }

    public void setEe(String ee) {
        this.ee = ee;
    }



}
