package cn.swiftpass.enterprise.ui.fmt;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tencent.stat.StatService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.bill.BillOrderManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.refund.RefundManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.bussiness.model.WxCard;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.OrderDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.PreAuthActivity;
import cn.swiftpass.enterprise.ui.activity.PreAuthDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.bill.ReportActivity;
import cn.swiftpass.enterprise.ui.activity.bill.UserListActivity;
import cn.swiftpass.enterprise.ui.activity.list.NewPullDownListView;
import cn.swiftpass.enterprise.ui.activity.list.PullToRefreshBase;
import cn.swiftpass.enterprise.ui.activity.list.PullToRefreshListView;
import cn.swiftpass.enterprise.ui.activity.spayMainTabActivity;
import cn.swiftpass.enterprise.ui.activity.user.RefundRecordOrderDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.user.VerificationDetails;
import cn.swiftpass.enterprise.ui.widget.PreAuthSelectPopupWindow;
import cn.swiftpass.enterprise.ui.widget.SelectDatePopupWindow;
import cn.swiftpass.enterprise.ui.widget.SelectPicPopupWindow;
import cn.swiftpass.enterprise.ui.widget.SpayTitleView;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * Created by aijingya on 2018/5/2.
 *
 * @Package cn.swiftpass.enterprise.ui.fmt
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/5/2.17:25.
 */

public class FragmentTabBill extends BaseFragment implements NewPullDownListView.OnRefreshListioner, NewPullDownListView.OnLoadDateRefreshListioner {
    private static final String TAG = FragmentTabBill.class.getSimpleName();

    private spayMainTabActivity mActivity;

    //private NewPullDownListView bill_list;

    private ViewHolder holder;

    private UserModel userModel;

    //    private PinnedHeaderListView listView;

    private ListView listView;

    private List<Order> orderList = new ArrayList<Order>();

    private Handler mHandler = new Handler();

    private BillStreamAdapter billStreamAdapter;


    private List<String> choiceTypeList = new ArrayList<String>();

    private List<String> payTypeList = new ArrayList<String>();

    private Map<String, String> dayCountMap = new HashMap<String, String>();

    private int pageFulfil = 0;

    private TextView tv_prompt;

    private int isRef = 0;//0流水，1，退款，2 卡券

    private int isUnFreezen = 0;//0预授权流水，1，预授权解冻

    //private Map<Integer, String> mapPostion = new HashMap<Integer, String>();

    private PullToRefreshListView mPullRefreshListView;

    private TextView tv_total;

    private static Map<String, String> dayTatal = new HashMap<String, String>();

    private List<String> titleList = new ArrayList<String>();

    private RelativeLayout ly_stream;

    //private Integer pageCount = 0;

    private String streamTime, refundTime;
    // 标记是否滑动
    //private boolean scrollFlag = false;

    List<Integer> tradeType = new ArrayList<Integer>();

    List<Integer> payType = new ArrayList<Integer>();

    //private boolean isRefresh = true;
    //刷新反正多次刷新
    private boolean isMoerRefresh = true;

    private LinearLayout ly_but_choice, ly_choice_date,ly_but_cachier;

    private TextView tv_cashier_name;

    private String time;

    private TextView tv_time;

    SelectDatePopupWindow selectDatePopupWindow;

    private boolean loadMore = true;

    private Integer reqFeqTime = 0;

    private boolean loadNext = false;

    private boolean isHadhandle;

    private boolean isPreAuth = false;//区分是否预授权

    private String bill_sale_or_Pre_auth; //订单列表页面，选择是收银还是预授权订单

    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            //流水
            if (msg.what == HandlerManager.STREAM_BILL) {
                if (!StringUtil.isEmptyOrNull(refundTime)) {
                    String value = dayTatal.get(refundTime);
                    if (!StringUtil.isEmptyOrNull(value)) {

                        tv_total.setText(getString(R.string.tx_bill_stream_total) + DateUtil.formatMoneyUtils(Long.parseLong(value)));
                    }
                }

            }
            //退款
            else if (msg.what == HandlerManager.STREAM_BILL_REFUND) {

                if (!StringUtil.isEmptyOrNull(streamTime)) {
                    String value = dayTatal.get(streamTime);
                    if (!StringUtil.isEmptyOrNull(value)) {
                        tv_total.setText(getString(R.string.tv_refund_title_info) + DateUtil.formatMoneyUtils(Long.parseLong(value)));
                    }
                }
            } else if (msg.what == HandlerManager.PAY_FINISH_REFUND) {
                tv_total.setText("");
            } else if (msg.what == HandlerManager.PAY_SWITCH_TAB) {//刷新账单
                if (choiceTypeList.size() > 0) {
                    choiceTypeList.clear();
                }
                payTypeList.clear();
                isRef = 0;
                isUnFreezen = 0;
                //                orderList.clear();
                tradeType.clear();
                payType.clear();
                isMoerRefresh = true;

                time = "";
                if (tv_time != null) {

                    tv_time.setText(R.string.tx_today);
                    tv_time.setTextSize(16);
                } else {
                    tv_time.setTextSize(16);
                    tv_time.setText(R.string.tx_today);
                }

                userModel = null;
                tv_cashier_name.setVisibility(View.VISIBLE);
                tv_cashier_name.setText(R.string.tx_bill_stream_cashier);

                loadDateTask(1, 1, true, false,null,0);

            } else if (msg.what == HandlerManager.PAY_DETAIL_TO_REFRESH) {
                //                orderList.clear();
                loadDateTask(1, 1, true, false,getSelectedUserId(),0);
            }else if (msg.what == HandlerManager.CHOICE_CASHIER) { //收银员
                try {
                    if (loadNext) { //
                        toastDialog(mActivity, R.string.tx_request_more, null);
                        return;
                    }
                    userModel = (UserModel) msg.obj;
                    if (userModel != null) {
                        tv_cashier_name.setVisibility(View.VISIBLE);
                        tv_cashier_name.setText(userModel.getRealname());

                        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth","sale"),"pre_auth")){
                            loadDateTask(1, 1, true, false,getSelectedUserId(),isUnFreezen);
                        }else {
                            //如果是退款
                            if(isRef == 1 ){
                                orderList.clear();
                                loadDateTask(1, 1, true, true,getSelectedUserId(),0);
                            }else{
                                orderList.clear();
                                loadDateTask(1, 1, true, false,getSelectedUserId(),0);
                            }
                        }

                    } else {
                        tv_cashier_name.setVisibility(View.VISIBLE);
                        tv_cashier_name.setText(R.string.tx_bill_stream_cashier);
                        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth","sale"),"pre_auth")) {
                            loadDateTask(1, 1, true, false,null,isUnFreezen);
                        }else{
                            if(isRef == 1){//如果是退款
                                orderList.clear();
                                loadDateTask(1, 1, true, true,null,0);
                            }else{
                                orderList.clear();
                                loadDateTask(1, 1, true, false,null,0);
                            }
                        }
                    }
                } catch (Exception e) {
                    tv_cashier_name.setVisibility(View.VISIBLE);
                    tv_cashier_name.setText(R.string.tx_bill_stream_cashier);
                    if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth","sale"),"pre_auth")) {
                        loadDateTask(1, 1, true, false,null,isUnFreezen);
                    }else{
                        if(isRef == 1){//如果是退款
                            orderList.clear();
                            loadDateTask(1, 1, true, true,null,0);
                        }else{
                            orderList.clear();
                            loadDateTask(1, 1, true, false,null,0);
                        }
                    }
                }
            }else if(msg.what == HandlerManager.BILL_CHOICE_SALE || msg.what == HandlerManager.BILL_CHOICE_PRE_AUTH){
                if (msg.what==HandlerManager.BILL_CHOICE_SALE){
                    isPreAuth=false;
                }else if ( msg.what == HandlerManager.BILL_CHOICE_PRE_AUTH){
                    isPreAuth=true;
                }

                if (choiceTypeList.size() > 0) {
                    choiceTypeList.clear();
                }
                payTypeList.clear();
                isRef = 0;
                isUnFreezen = 0;
                //                orderList.clear();
                tradeType.clear();
                payType.clear();
                isMoerRefresh = true;

                time = "";
                if (tv_time != null) {

                    tv_time.setText(R.string.tx_today);
                    tv_time.setTextSize(16);
                } else {
                    tv_time.setTextSize(16);
                    tv_time.setText(R.string.tx_today);
                }
                userModel = null;
                tv_cashier_name.setVisibility(View.VISIBLE);
                tv_cashier_name.setText(R.string.tx_bill_stream_cashier);

                loadDateTask(1, 1, true, false,null,0);
            }
        };
    };

    @Override
    protected int getLayoutId() {
        return 0;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_bill_stream, container, false);
        bill_sale_or_Pre_auth = PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth","sale");//用来区分预授权还是消费颜色标记
        if (TextUtils.equals(bill_sale_or_Pre_auth,"sale")){
            isPreAuth=false;
        }else  if (TextUtils.equals(bill_sale_or_Pre_auth,"pre_auth")){
            isPreAuth=true;
        }

        initView(view);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setLister();
        HandlerManager.registerHandler(HandlerManager.STREAM_BILL, handler);
        HandlerManager.registerHandler(HandlerManager.CHOICE_CASHIER, handler);
        HandlerManager.registerHandler(HandlerManager.BILL_CHOICE_SALE_OR_PRE_AUTH, handler);
        if (orderList.size() == 0) {
            orderList.clear();
            loadDateTask(1, 1, true, false,null,isUnFreezen);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof spayMainTabActivity){
            mActivity=(spayMainTabActivity) activity;
        }
    }

    void loadDateTask(int page, int pullType, final boolean isLoadMore, boolean isRefund,String userId,int isUnFreezen) {

        if (null != selectDatePopupWindow) {
            selectDatePopupWindow.dismiss();
        }
        switch (pullType) {
            case 1:
                loadMore = true;
                pageFulfil = 1;
                titleList.clear();
                loadDate(pageFulfil, isLoadMore, isRef, time ,isUnFreezen,userId);
                break;
            case 2:
                mPullRefreshListView.onRefreshComplete();
                break;
            case 3:
                pageFulfil = pageFulfil + 1;
                if (loadMore) {
                    loadDate(pageFulfil, isLoadMore, isRef, time,isUnFreezen,userId);
                } else {
                    mPullRefreshListView.onRefreshComplete();
                }
                break;
            default:
                break;
        }
    }

    /**
     * 暂停 后 才去请求
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void sleep(long time) {
        try {
            //            Thread.sleep(time * 1000);

            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Logger.i("hehui", "sleep-->");
                            loadNext = false;
                        }
                    });

                }

            };
            Timer timer = new Timer();
            timer.schedule(task, time * 1000);

        } catch (Exception e) {
            return;
        }
    }

    void loadDate(final int page, final boolean isLoadMore, int isRefund, String startDate,int isUnFreezen,String userId) {
        Logger.i("hehui", "loadNext-->" + loadNext);
        if (loadNext) {
            mPullRefreshListView.onRefreshComplete();
            toastDialog(mActivity, R.string.tx_request_more, null);
            return;
        }

        BillOrderManager.getInstance().querySpayOrderNew(reqFeqTime, choiceTypeList, payTypeList, isRefund, page, null,
                isUnFreezen , userId,startDate, new UINotifyListener<List<Order>>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                reqFeqTime = 0;
                if (page == 1) {
                    isMoerRefresh = false;
                }
                if (isLoadMore) {
                    loadDialog(mActivity, R.string.public_data_loading);
                }
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                dissDialog();
                mPullRefreshListView.onRefreshComplete();
                isMoerRefresh = true;
                if (checkSession()) {
                    return;
                }
                if (null != object) {
                    if (object.toString().startsWith("reqFeqTime")) { // 服务器返回有这个参数，说明服务器有压力，需要暂停reqFeqTime=对应的秒数才去请求返回
                        String time = object.toString().substring(object.toString().lastIndexOf("=") + 1);
                        if (!StringUtil.isEmptyOrNull(time)) {
                            Integer count = Integer.parseInt(time);
                            loadNext = true;
                            sleep(count);
                        } else {
                            dissDialog();
                        }
                    } else {
                        dissDialog();
                        mActivity.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                if (!StringUtil.isEmptyOrNull(object.toString())) {
                                    toastDialog(mActivity, object.toString(), null);
                                } else {
                                    toastDialog(mActivity, R.string.tx_load_fail, null);
                                }

                                if (orderList.size() == 0) {
                                    tv_prompt.setVisibility(View.VISIBLE);
                                    mPullRefreshListView.setVisibility(View.GONE);
                                    ly_stream.setVisibility(View.GONE);
                                }
                            }
                        });
                    }

                }
            }

            @Override
            public void onSucceed(List<Order> model) {
                dissDialog();
                isMoerRefresh = true;
                if (page == 1) {
                    orderList.clear();
                }
                if (!TextUtils.isEmpty(FragmentTabBill.this.time) && isToday(FragmentTabBill.this.time)){
                    orderList.clear();
                }

                if (null != model && model.size() > 0) {
                    tv_prompt.setVisibility(View.GONE);
                    //pageCount = model.get(0).getPageCount();

                    reqFeqTime = model.get(0).getReqFeqTime();

                    setListData(mPullRefreshListView, orderList, billStreamAdapter, model, isLoadMore, page);
                    mActivity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            mPullRefreshListView.setVisibility(View.VISIBLE);
                            ly_stream.setVisibility(View.VISIBLE);

                        }
                    });
                    //暂停
                    if (reqFeqTime > 0) {
                        loadNext = true;
                        sleep(reqFeqTime);
                    }
                    // mySetListData(bill_list, orderList, billStreamAdapter, model, isLoadMore);
                } else {
                    loadMore = false;
                    if (orderList.size() == 0) {
                        mActivity.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {

                                tv_prompt.setVisibility(View.VISIBLE);
                                mPullRefreshListView.setVisibility(View.GONE);
                                ly_stream.setVisibility(View.GONE);
                            }
                        });
                        //                            bill_list.setVisibility(View.GONE);
                    } else {
                        mPullRefreshListView.onRefreshComplete();
                    }
                }
            }
        });
    }

    /**
     * 判断之前选中的日期和当前的日期是否是同一天
     * 比较方法，分别比较年月日
     * @param selecttime
     * @return
     */
    private boolean isToday(String selecttime){
        String newDate = DateUtil.formatYYMD(System.currentTimeMillis());
        String date = selecttime;
        String[] arr1 = newDate.split("\\-");
        String[] arr2 = date.split("\\-");
        if (arr1[0].equals(arr2[0]) && arr1[1].equals(arr2[1]) && arr1[2].equals(arr2[2])) {
            return true;
        }else{
            return false;
        }
    }

    private void setTime(long time) {
        String newDate = DateUtil.formatYYMD(System.currentTimeMillis());
        String date = DateUtil.formatYYMD(time);

        String[] arr1 = newDate.split("\\-");

        String[] arr2 = date.split("\\-");

        if (arr1[0].equals(arr2[0]) && arr1[1].equals(arr2[1]) && arr1[2].equals(arr2[2])) {
            tv_time.setText(R.string.tx_today);
            tv_time.setTextSize(16);
        } else {
            tv_time.setTextSize(14);
            tv_time.setText(DateUtil.formatYYMD(time));
        }
    }

    private void setLister() {

        PullToRefreshBase.OnRefreshListener mOnrefreshListener = new PullToRefreshBase.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadDateTask(1, mPullRefreshListView.getRefreshType(), false, false,getSelectedUserId(),isUnFreezen);
            }
        };
        mPullRefreshListView.setOnRefreshListener(mOnrefreshListener);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int lastItemIndex;//当前ListView中最后一个Item的索引

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                lastItemIndex = firstVisibleItem + visibleItemCount - 1 - 1;
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (lastItemIndex >= billStreamAdapter.getCount() - 2) {
                    loadDateTask(1, 3, true, false,getSelectedUserId(),isUnFreezen);
                }

            }

        });

        tv_prompt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                loadDateTask(1, 1, false, false,getSelectedUserId(),isUnFreezen);
            }
        });

        ly_choice_date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    StatService.trackCustomEvent(mContext, "kMTASPayBillSelectDate", "日期选择");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
                selectDatePopupWindow = new SelectDatePopupWindow(mActivity,isPreAuth ,time, new SelectDatePopupWindow.HandleBtn() {

                    @Override
                    public void handleOkBtn(long time) {
                        if (time == 0) {

                            if (!StringUtil.isEmptyOrNull(FragmentTabBill.this.time)) {
                                if (isToday(FragmentTabBill.this.time)){
                                    tv_time.setText(R.string.tx_today);
                                    tv_time.setTextSize(16);
                                }else{
                                    tv_time.setTextSize(14);
                                    tv_time.setText(FragmentTabBill.this.time);
                                }
                                orderList.clear();
                                loadDateTask(1, 1, true, false,getSelectedUserId(),isUnFreezen);
                                return;
                            } else {
                                time = System.currentTimeMillis();
                            }

                        }
                        long nowTime = System.currentTimeMillis();
                        if (time > nowTime) {
                            toastDialog(mActivity, R.string.tx_date, null);
                            return;
                        }
                        Calendar ca = Calendar.getInstance();//得到一个Calendar的实例  90天
                        ca.add(Calendar.DAY_OF_MONTH, -90);

                        if (time <= ca.getTimeInMillis()) {
                            toastDialog(mActivity, R.string.tx_choice_date, null);
                            return;
                        }
                        setTime(time);
                        FragmentTabBill.this.time = DateUtil.formatYYMD(time);
                        orderList.clear();
                        loadDateTask(1, 1, true, false,getSelectedUserId(),isUnFreezen);
                    }

                });

                selectDatePopupWindow.showAtLocation(ly_choice_date, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0); //设置layout在PopupWindow中显示的位置
            }
        });

        ly_but_choice.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    StatService.trackCustomEvent(mContext, "kMTASPayBillFilter", "筛选入口");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth","sale"),"pre_auth")){
                    PreAuthSelectPopupWindow p = new PreAuthSelectPopupWindow(isPreAuth,isHadhandle,mActivity, choiceTypeList, payTypeList, new PreAuthSelectPopupWindow.HandleBtn() {

                        @Override
                        public void handleOkBtn(List<String> choiceType, List<String> payTypeList,boolean ishandled) {
                            payTypeList = payTypeList;
                            choiceTypeList = choiceType;
                            orderList.clear();
                            isHadhandle  = ishandled;
                            if (choiceType.contains("Unfreezed")) {//如果包含解冻
                                isUnFreezen = 1;
                                dayCountMap.clear();
                                dayTatal.clear();
                                loadDateTask(1, 1, true, true,getSelectedUserId(),isUnFreezen);
                            }else {
                                dayCountMap.clear();
                                dayTatal.clear();
                                isUnFreezen = 0;

                                loadDateTask(1, 1, true, false,getSelectedUserId(),isUnFreezen);
                            }
                        }
                    });

                    /**
                     * 对话框消失监听
                     */
                    p.setOnDismissListener(new PopupWindow.OnDismissListener() {
                        @Override
                        public void onDismiss() {

                        }
                    });
                    //设置layout在PopupWindow中显示的位置
                    p.showAtLocation(ly_but_choice, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                }else{
                    SelectPicPopupWindow p = new SelectPicPopupWindow(isHadhandle,mActivity, choiceTypeList, payTypeList, new SelectPicPopupWindow.HandleBtn() {

                        @Override
                        public void handleOkBtn(List<String> choiceType, List<String> payTypeList,boolean ishandled) {
                            payTypeList = payTypeList;
                            choiceTypeList = choiceType;
                            orderList.clear();
                            isHadhandle  = ishandled;
                            if (choiceType.contains("refund")) {
                                isRef = 1;
                                //                                    loadDate(1, true, true);
                                if (choiceTypeList.size() > 1) {
                                    HandlerManager.notifyMessage(HandlerManager.STREAM_BILL, HandlerManager.PAY_FINISH_REFUND);
                                } else {
                                    HandlerManager.notifyMessage(HandlerManager.STREAM_BILL, HandlerManager.STREAM_BILL_REFUND);
                                }
                                loadDateTask(1, 1, true, true,getSelectedUserId(),0);
                                dayCountMap.clear();
                                dayTatal.clear();
                            } else if (choiceType.contains("card")) {
                                isRef = 2;
                                loadDateTask(1, 1, true, false,getSelectedUserId(),0);
                                dayCountMap.clear();
                                dayTatal.clear();
                            } else {
                                dayCountMap.clear();
                                dayTatal.clear();
                                HandlerManager.notifyMessage(HandlerManager.STREAM_BILL, HandlerManager.STREAM_BILL);
                                isRef = 0;
                                //                                    loadDate(1, true, false);
                                tradeType.clear();
                                payType.clear();
                                BillOrderManager.getInstance().paseToJson(choiceType, tradeType, payType);

                                loadDateTask(1, 1, true, false,getSelectedUserId(),0);
                            }
                        }
                    });

                    /**
                     * 对话框消失监听
                     */
                    p.setOnDismissListener(new PopupWindow.OnDismissListener() {
                        @Override
                        public void onDismiss() {

                        }
                    });
                    //设置layout在PopupWindow中显示的位置
                    p.showAtLocation(ly_but_choice, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                }
            }
        });

        ly_but_cachier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    StatService.trackCustomEvent(mContext, "kMTASPayBillSelectCashier", "收银员选择");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                if (userModel != null) {
                    UserListActivity.startActivity(mActivity, userModel,true,false,isPreAuth);
                } else {
                    Intent intent = new Intent(mActivity, UserListActivity.class);
                    intent.putExtra("isFromBillEnter",true);
                    intent.putExtra("isFromCodeListEnter",false);
                    intent.putExtra("isPreAuth",isPreAuth);
                    startActivity(intent);
                }

            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                final Order order = orderList.get(position - 1);
                if (null != order) {
                    //isRefresh = false;
                    if (isRef == 1) {
                        RefundManager.getInstant().queryRefundDetail(order.getOutRefundNo(), MainApplication.getMchId(), new UINotifyListener<Order>() {

                            @Override
                            public void onError(Object object) {
                                super.onError(object);
                                dismissLoading();
                                if (checkSession()) {
                                    return;
                                }
                                if (object != null) {
                                    toastDialog(mActivity, object.toString(), null);
                                }
                            }

                            @Override
                            public void onPreExecute() {
                                super.onPreExecute();

                                loadDialog(mActivity, getStringById(R.string.public_data_loading));
                            }

                            @Override
                            public void onSucceed(Order result) {
                                super.onSucceed(result);
                                dismissLoading();
                                if (result != null) {
                                    RefundRecordOrderDetailsActivity.startActivity(mActivity, result);
                                }
                            }

                        });
                    } else if (isRef == 0) {
                        String orderNo = "";
                        //如果有预授权权限，同时账单页面选择的是预授权
                        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth","sale"),"pre_auth")){
                            orderNo = order.getAuthNo();
                        }else{
                            orderNo = order.getOutTradeNo();
                        }

                        if(isUnFreezen == 1){//如果是解冻，则把列表数据直接带过去
                           // PreAuthActivity.startActivity(mActivity,order,1);//预授权解冻详情
                            PreAuthDetailsActivity.startActivity(mContext, order, 2,2);//跳预授权转支付详情

                        }else{
                            OrderManager.getInstance().queryOrderDetail(orderNo, MainApplication.getMchId(), true, new UINotifyListener<Order>() {

                                @Override
                                public void onError(Object object) {
                                    super.onError(object);
                                    dismissLoading();
                                    if (checkSession()) {
                                        return;
                                    }
                                    if (object != null) {
                                        toastDialog(mActivity, object.toString(), null);
                                    }
                                }

                                @Override
                                public void onPreExecute() {
                                    super.onPreExecute();

                                    loadDialog(mActivity, getStringById(R.string.public_data_loading));

                                }

                                @Override
                                public void onSucceed(Order result) {

                                    super.onSucceed(result);
                                    dismissLoading();
                                    if (result != null) {
                                        //如果有预授权权限，同时账单页面选择的是预授权
                                        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth","sale"),"pre_auth")) {
                                            PreAuthActivity.startActivity(mActivity,result,2);//预授权支付详情
                                        }else{
                                            OrderDetailsActivity.startActivity(mActivity, result);
                                        }
                                    }
                                }

                            });
                        }
                    } else {
                        OrderManager.getInstance().queryVardDetail(order.getCardId(), order.getCardCode(), new UINotifyListener<WxCard>() {

                            @Override
                            public void onError(Object object) {
                                super.onError(object);
                                dismissLoading();
                                if (object != null) {
                                    //                                showToastInfo(object.toString());
                                    toastDialog(mActivity, object.toString(), null);
                                }
                            }

                            @Override
                            public void onPreExecute() {
                                super.onPreExecute();

                                loadDialog(mActivity, R.string.public_data_loading);
                            }

                            @Override
                            public void onSucceed(WxCard result) {
                                super.onSucceed(result);
                                dismissLoading();
                                if (result != null) {
                                    VerificationDetails.startActivity(mActivity, result);
                                }
                            }

                        });
                    }

                }
            }
        });

    }

    private void setListData(final PullToRefreshListView pull, List<Order> list, BillStreamAdapter adapter, List<Order> result, boolean isLoadMore, int page) {
        if (!isLoadMore) {
            list.clear();
        }
        list.addAll(result);
        adapter.notifyDataSetChanged();
        pull.onRefreshComplete();
        if (page == 1) {
            listView.setSelection(0);
        }
    }


    private void initView(View view) {

        //ly_but_choice,ly_choice_date;
        tv_time = view.findViewById(R.id.tv_time);
        ly_but_choice = view.findViewById(R.id.ly_but_choice);
        ly_choice_date = view.findViewById(R.id.ly_choice_date);
        ly_but_cachier = view.findViewById(R.id.ly_but_cachier);
        tv_cashier_name = view.findViewById(R.id.tv_cashier_name);
        ly_stream = view.findViewById(R.id.ly_stream);
        tv_prompt = view.findViewById(R.id.tv_prompt);
        tv_prompt.setText(R.string.tv_bill);
        tv_total = view.findViewById(R.id.tv_total);
        tv_time = view.findViewById(R.id.tv_time);

        mPullRefreshListView = view.findViewById(R.id.pullrefresh);
        listView = mPullRefreshListView.getRefreshableView();

        billStreamAdapter = new BillStreamAdapter(orderList);
        listView.setAdapter(billStreamAdapter);

        //判断是收银员还是商户，如果是收银员，且收银员没有账单权限，则不显示cashier这一列筛选项
        if(MainApplication.isAdmin.equals("0") && MainApplication.isOrderAuth.equals("0")){
            ly_but_cachier.setVisibility(View.GONE);
        }else{
            ly_but_cachier.setVisibility(View.VISIBLE);
        }
    }

    private class BillStreamAdapter extends BaseAdapter
    {

        private List<Order> orders;

        public BillStreamAdapter() {
        }

        public BillStreamAdapter(List<Order> orders) {
            this.orders = orders;
        }

        @Override
        public int getCount() {
            return orders.size();
        }

        @Override
        public Object getItem(int position) {
            return orders.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(mContext, R.layout.activity_bill_stream_list_item, null);
                holder = new ViewHolder();
                holder.tv_center = (TextView) convertView.findViewById(R.id.tv_center);
                holder.tv_time = (TextView) convertView.findViewById(R.id.tv_time);
                holder.tv_money = (TextView) convertView.findViewById(R.id.tv_money);
                holder.tv_state = (TextView) convertView.findViewById(R.id.tv_state);
                holder.iv_type = (ImageView) convertView.findViewById(R.id.iv_type);
                holder.v_iv = (View) convertView.findViewById(R.id.v_iv);
                holder.iv_cover = (ImageView) convertView.findViewById(R.id.iv_cover);
                holder.iv_cover.setVisibility(View.GONE);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            Order order = orderList.get(position);
            if (null != order) {
                //如果有预授权权限，同时账单页面选择的是预授权
                if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth","sale"),"pre_auth") ){
                    if (order.getMoney() > 0) {
                        holder.tv_money.setVisibility(View.VISIBLE);
                        holder.tv_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(order.getMoney()));
                    }
                    if(isUnFreezen == 1) {
                        holder.tv_center.setText(StringUtil.paseStrToMarkStr(order.getOutRequestNo()));
                    }else {
                        holder.tv_center.setText(StringUtil.paseStrToMarkStr(order.getAuthNo()));
                    }

                    //解冻
                    if(isUnFreezen == 1){

                        if (!StringUtil.isEmptyOrNull(order.getOperateTimeFormat())) {
                            try {
                                holder.tv_time.setText(order.getOperateTimeFormat());
                                if (isMove(position)) {
                                    holder.v_iv.setVisibility(View.GONE);
                                } else {
                                    holder.v_iv.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception e) {
                                Log.e(TAG,Log.getStackTraceString(e));
                            }
                        }

                        switch (order.getOperationStatus()){
                            case 1:
                                //解冻成功
                                holder.tv_state.setText(getContext().getResources().getString(R.string.freezen_success));
                                holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.pay_fail));
                                holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.black));
                                holder.iv_cover.setVisibility(View.GONE);
                                break;
                            case 0:
                            case 2:
                                //解冻失败
                                holder.tv_state.setText(getContext().getResources().getString(R.string.unfreezen_failed));
                                holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                holder.iv_cover.setVisibility(View.GONE);
                                break;
                        }
                    }else{
                        if (!StringUtil.isEmptyOrNull(order.getTradeTimeNew())) {
                            try {
                                holder.tv_time.setText(order.getTradeTimeNew());
                                if (isMove(position)) {
                                    holder.v_iv.setVisibility(View.GONE);
                                } else {
                                    holder.v_iv.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception e) {
                                Log.e(TAG,Log.getStackTraceString(e));
                            }
                        }

                        holder.tv_state.setText(MainApplication.getPreAuthtradeStateMap().get(order.getTradeState() + ""));
                        switch (order.getTradeState()) {
                            case 1:
                                //未授权
                                holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                holder.iv_cover.setVisibility(View.VISIBLE);
                                break;
                            case 2:
                                //已授权
                                holder.tv_state.setTextColor(Color.parseColor("#03C700"));
                                holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.black));
                                holder.iv_cover.setVisibility(View.GONE);
                                break;
                            case 3:
                                //已撤销
                                holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                holder.iv_cover.setVisibility(View.VISIBLE);
                                break;
                            default:
                                holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                holder.iv_cover.setVisibility(View.VISIBLE);
                                break;
                        }
                    }

                    Object object = SharedPreUtile.readProduct("payTypeIcon" + ApiConstant.bankCode + MainApplication.getMchId());
                    if (object != null) {
                        try {
                            Map<String, String> typePicMap = (Map<String, String>) object;
                            if (typePicMap != null && typePicMap.size() > 0) {
                                String picUrl = typePicMap.get("2");
                                if (!StringUtil.isEmptyOrNull(picUrl)) {
                                    Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.icon_general_receivables);
                                    if (bitmap != null) {
                                        MainApplication.finalBitmap.display(holder.iv_type, picUrl, bitmap);
                                    } else {
                                        MainApplication.finalBitmap.display(holder.iv_type, picUrl);
                                    }
                                } else {
                                    holder.iv_type.setImageResource(R.drawable.icon_general_receivables);
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG,Log.getStackTraceString(e));
                        }
                    } else {
                        //如果是预授权列表，则getApiProvider默认写死成2
                        //默认写死支付宝
                        holder.iv_type.setImageResource(R.drawable.icon_general_pay);
                    }

                }else{ //否则走原来的逻辑
                    if (order.getMoney() > 0) {
                        holder.tv_money.setVisibility(View.VISIBLE);
                        holder.tv_money.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtils(order.getMoney()));
                    }

                    holder.tv_center.setText(StringUtil.paseStrToMarkStr(order.getOrderNoMch()));

                    if (isRef == 1) {

                        holder.tv_center.setText(StringUtil.paseStrToMarkStr(order.getRefundNo() ));

                        String langeage = PreferenceUtil.getString("language", "");
//                    if (langeage.equals("en_us")) {
//                        holder.tv_pay_type.setText(MainApplication.getPayTypeMap().get(String.valueOf(order.getApiProvider())) + " " + getString(R.string.title_order_refund));
//                    } else {
//                        holder.tv_pay_type.setText(MainApplication.getPayTypeMap().get(String.valueOf(order.getApiProvider())) + getString(R.string.title_order_refund));
//                    }
                        try {
                            holder.tv_time.setText(order.getAddTimeNew());
                            if (isMove(position)) {
                                holder.v_iv.setVisibility(View.GONE);
                            } else {
                                holder.v_iv.setVisibility(View.VISIBLE);
                            }

                            holder.tv_state.setText(MainApplication.getRefundStateMap().get(order.getRefundState() + ""));
                            //0:审核中 1:退款成功 2:退款失败  3:退款成功 5:退款成功
                            switch (order.getRefundState()) {
                                case 0:
                                    //审核中
                                    holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.paytype_title_reset));
                                    holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.black));
                                    holder.iv_cover.setVisibility(View.GONE);
                                    break;
                                case 1:
                                case 3:
                                case 5:
                                    //退款成功
                                    holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.pay_fail));
                                    holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.black));
                                    holder.iv_cover.setVisibility(View.GONE);
                                    break;
                                case 2:
                                    //退款失败
                                    holder.tv_state.setText(R.string.refund_failure);
                                    holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                    holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                    holder.iv_cover.setVisibility(View.GONE);
                                    break;
                                default:
                                    holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                    holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                    holder.iv_cover.setVisibility(View.VISIBLE);
                                    break;
                            }
                        } catch (Exception e) {
                            Log.e(TAG, Log.getStackTraceString(e));
                        }
                    } else if (isRef == 0) {
                        //  holder.tv_pay_type.setText(MainApplication.getPayTypeMap().get(String.valueOf(order.getApiProvider())));

                        if (!StringUtil.isEmptyOrNull(order.getTradeTimeNew())) {
                            try {
                                holder.tv_time.setText(order.getTradeTimeNew());
                                if (isMove(position)) {
                                    holder.v_iv.setVisibility(View.GONE);
                                } else {
                                    holder.v_iv.setVisibility(View.VISIBLE);
                                }

                                holder.tv_state.setText(MainApplication.getTradeTypeMap().get(order.getTradeState() + ""));
                                switch (order.getTradeState()) {
                                    case 2:
                                        //支付成功
                                        holder.tv_state.setTextColor(Color.parseColor("#03C700"));
                                        holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.black));
                                        holder.iv_cover.setVisibility(View.GONE);
                                        break;
                                    case 1:
                                        //未支付
                                        holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                        holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                        holder.iv_cover.setVisibility(View.VISIBLE);
                                        break;
                                    case 3:
                                        //关闭
                                        holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                        holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                        holder.iv_cover.setVisibility(View.VISIBLE);
                                        break;
                                    case 4:
                                        //转入退款
                                        holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.pay_fail));
                                        holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.black));
                                        holder.iv_cover.setVisibility(View.GONE);
                                        break;
                                    case 8:
                                        //已撤销
                                        holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                        holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                        holder.iv_cover.setVisibility(View.VISIBLE);
                                        break;
                                    default:
                                        holder.tv_state.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                        holder.tv_money.setTextColor(mContext.getResources().getColor(R.color.bg_setting_right_text));
                                        holder.iv_cover.setVisibility(View.VISIBLE);
                                        break;
                                }

                            } catch (Exception e) {
                                Log.e(TAG,Log.getStackTraceString(e));
                            }
                        }
                    } else {
                        holder.tv_time.setText(order.getUseTimeNew());
                        //  holder.tv_pay_type.setText(order.getTitle());
                        holder.tv_money.setVisibility(View.INVISIBLE);
                        holder.iv_type.setImageResource(R.drawable.icon_pop_coupon);
                        //String value = dayCountMap.get(order.getFromatCard());
                        if (isMove(position)) {
                            holder.v_iv.setVisibility(View.GONE);
                        } else {
                            holder.v_iv.setVisibility(View.VISIBLE);
                        }
                        //卡券
                        holder.tv_state.setText(R.string.tx_affirm_succ);
                        holder.tv_state.setTextColor(Color.parseColor("#666666"));
                    }

                    if (isRef == 0 || isRef == 1) {

                        Object object = SharedPreUtile.readProduct("payTypeIcon" + ApiConstant.bankCode + MainApplication.getMchId());
                        if (object != null) {
                            try {

                                Map<String, String> typePicMap = (Map<String, String>) object;
                                if (typePicMap != null && typePicMap.size() > 0) {
                                    String picUrl = typePicMap.get(order.getApiProvider() + "");
                                    if (!StringUtil.isEmptyOrNull(picUrl)) {
                                        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.icon_general_receivables);
                                        if (bitmap != null) {
                                            MainApplication.finalBitmap.display(holder.iv_type, picUrl, bitmap);
                                        } else {
                                            MainApplication.finalBitmap.display(holder.iv_type, picUrl);
                                        }
                                    } else {
                                        holder.iv_type.setImageResource(R.drawable.icon_general_receivables);
                                    }
                                }
                            } catch (Exception e) {
                                Log.e(TAG,Log.getStackTraceString(e));
                            }
                        } else {

                            switch (order.getApiProvider()) {
                                case 1:
                                    //微信
                                    holder.iv_type.setImageResource(R.drawable.icon_general_wechat);
                                    break;
                                case 2:
                                    //支付宝
                                    holder.iv_type.setImageResource(R.drawable.icon_general_pay);
                                    break;
                                case 12:
                                    //京东
                                    holder.iv_type.setImageResource(R.drawable.icon_general_jingdong);
                                    break;
                                case 4:
                                    //qq
                                    holder.iv_type.setImageResource(R.drawable.icon_general_qq);
                                    break;
                                default:
                                    holder.iv_type.setImageResource(R.drawable.icon_general_receivables);
                                    break;
                            }
                        }
                    }
                }
            }
            return convertView;
        }
    }

    private boolean isMove(int position) {
        // 获取当前与下一项
        //        ItemEntity currentEntity = (ItemEntity)getItem(position);
        //          ItemEntity nextEntity = (ItemEntity)getItem(position + 1);
        try {
            Order order = orderList.get(position);
            Order order1 = orderList.get(position + 1);

            if (null == order || null == order1) {
                return true;
            }

            if (isRef == 0) {
                // 获取两项header内容
                //                String currentTitle = DateUtil.formartDateYYMMDD(order.getTradeTimeNew());
                //                String nextTitle = DateUtil.formartDateYYMMDD(order1.getTradeTimeNew());

                String currentTitle = order.getFormatTimePay();
                String nextTitle = order1.getFormatTimePay();
                if (null == currentTitle || null == nextTitle) {
                    return false;
                }

                // 当前不等于下一项header，当前项需要移动了
                if (!currentTitle.equals(nextTitle)) {
                    return true;
                }

            } else if (isRef == 1) { //退款
                String currentTitle = order.getFormatRefund();
                String nextTitle = order1.getFormatRefund();
                if (currentTitle == null || nextTitle == null) {
                    return false;
                }

                if (!currentTitle.equalsIgnoreCase(nextTitle)) {
                    return true;
                }
            } else {
                String currentTitle = order.getFromatCard();
                String nextTitle = order1.getFromatCard();
                if (currentTitle == null || nextTitle == null) {
                    return false;
                }

                if (!currentTitle.equalsIgnoreCase(nextTitle)) {
                    return false;
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
            return true;
        }

        return false;
    }

    private class ViewHolder {
        private TextView  tv_time, tv_center, tv_money, tv_state;

        private ImageView iv_type;

        private ImageView iv_cover;

        private View v_iv;
    }

    @Override
    public void onLoadMoreDate() {

    }

    @Override
    public void onRefresh() {
        Logger.i("hehui", "onRefresh()");
        pageFulfil = 1;
        if (isMoerRefresh) {

            loadDate(pageFulfil, false, isRef, time,isUnFreezen,getSelectedUserId());
        }
    }

    @Override
    public void onLoadMore() {
        Logger.i("hehui", "onLoadMore()");
        pageFulfil = pageFulfil + 1;

        loadDate(pageFulfil, true, isRef, time,isUnFreezen,getSelectedUserId());
    }

    public String getSelectedUserId(){
        String userId = null;
        if(tv_cashier_name != null && userModel != null){
            if(!StringUtil.isEmptyOrNull(tv_cashier_name.getText().toString())
                    && !tv_cashier_name.getText().equals(getStringById(R.string.tx_bill_stream_cashier))){
                userId = String.valueOf(userModel.getId());
            }
        }
        return userId;
    }

}
