package cn.swiftpass.enterprise.utils.json;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

/**
 * Created by congwei.li on 2022/5/6.
 *
 * @Description:
 */
public class LongTypeAdapter implements JsonDeserializer<Long>,
        JsonSerializer<Long> {
    public Long deserialize(JsonElement json, Type typeOfT,
                                           JsonDeserializationContext context)
            throws JsonParseException {
        long runtime;
        try {
            runtime = json.getAsLong();
        } catch (Exception e) {
            runtime = 0L;
        }
        return runtime;
    }

    public JsonElement serialize(Long src, Type typeOfSrc,
                                 JsonSerializationContext context) {
        return new JsonPrimitive(src);
    }

}
