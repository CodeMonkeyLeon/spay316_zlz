package com.example.printsdk.A920.printUtils

import android.content.Context
import com.pax.dal.IPrinter
import com.pax.dal.exceptions.PrinterDevException
import com.pax.gl.IGL
import com.pax.gl.imgprocessing.IImgProcessing
import com.pax.gl.imgprocessing.IImgProcessing.IPage
import com.pax.gl.imgprocessing.IImgProcessing.IPage.EAlign
import com.pax.gl.impl.GLProxy

class PrintA920Text {
    private var printer: IPrinter? = null
    var line = "6"
    var space = "1"
    private var gl: IGL? = null
    private var ImgProcessing: IImgProcessing? = null
    private var page: IPage? = null
    private var mContext: Context? = null


    init {
        GetObj.getDal()?.let {
            printer = it.printer
        }
    }


    companion object {
        private var printText: PrintA920Text? = null
        fun getInstance(): PrintA920Text? {
            if (printText == null) {
                printText = PrintA920Text()
            }
            return printText
        }
    }

    fun init(context: Context?) {
        try {
            printer?.let {
                it.init()
            }
            mContext = context
            gl = GLProxy(mContext).gl
            gl?.let { g ->
                ImgProcessing = g.imgProcessing
                ImgProcessing?.let { im ->
                    page = im.createPage()
                }
            }
        } catch (e: PrinterDevException) {
            e.printStackTrace()
        }
    }


    fun printTitle(text: String?) {
        page?.let {
            it.addLine().addUnit(text, 30, EAlign.CENTER)
        }
    }


    fun printTextLine(text: String?, TextSize: Int, align: EAlign?) {
        page?.let {
            it.addLine().addUnit(text, TextSize, align)
        }
    }

    fun printEmptyLine() {
        page?.let {
            it.addLine().addUnit(" ", 30, EAlign.CENTER)
        }
    }

    fun printDoubleLine() {
        page?.let {
            it.addLine().addUnit("==========================", 28, EAlign.CENTER)
        }
    }

    fun printSingleLine() {
        page?.let {
            it.addLine().addUnit(
                "-------------------------------------------------------------",
                24,
                EAlign.CENTER
            )
        }
    }

    fun printQRCode(orderNoMch: String?) {
        //打印小票的代码屏蔽
        val QR_Bitmap = CreateOneDiCodeUtilPrint.createCode(orderNoMch, 260, 260)
        page?.let {
            it.addLine().addUnit(QR_Bitmap, EAlign.CENTER)
        }
    }

    fun startPrint() {
        try {
            PrinterTester.getInstance()?.let { p ->
                ImgProcessing?.let { im ->
                    p.printBitmap(im.pageToBitmap(page, 410))
                }
            }

            printer?.let {
                it.printStr(" \n", null)
                it.printStr(" \n", null)
                it.printStr(" \n", null)
                it.printStr(" \n", null)
                it.start()
            }
        } catch (e: PrinterDevException) {
            e.printStackTrace()
        }
    }


    fun start(): String? {
        try {
            printer?.let {
                it.start()
            }
        } catch (e: PrinterDevException) {
            e.printStackTrace()
        }
        return ""
    }

    fun statusCode2Str(status: Int): String? {
        var res = ""
        when (status) {
            0 -> res = "Success "
            1 -> res = "Printer is busy "
            2 -> res = "Out of paper "
            3 -> res = "The format of print data packet error "
            4 -> res = "Printer malfunctions "
            8 -> res = "Printer over heats "
            9 -> res = "Printer voltage is too low"
            240 -> res = "Printing is unfinished "
            252 -> res = " The printer has not installed font library "
            254 -> res = "Data package is too long "
            else -> {}
        }
        return res
    }

}