/*
 * 文 件 名:  EmpManagerActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2015-3-12
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 *修改名字
 * 
 * @author  he_hui
 * @version  [版本号, 2015-3-12]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class CashierNameModifyActivity extends TemplateActivity
{
    
    private EditText cashierName;
    
    private ImageView iv_cashierName;

    private Button bt_changeName;
    
    private UserModel userModel;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.cashier_modify_name_view);
        userModel = (UserModel)getIntent().getSerializableExtra("userModel");
        
        initView();
        
        if (userModel != null)
        {
            iv_cashierName.setVisibility(View.VISIBLE);
            cashierName.setText(userModel.getRealname());
            if (!StringUtil.isEmptyOrNull(userModel.getRealname()))
            {
                cashierName.setSelection(userModel.getRealname().length());
            }

            if (cashierName.getText().toString().length() > 0)
            {
                iv_cashierName.setVisibility(View.VISIBLE);
                setButtonBg(bt_changeName, true, 0);
            }
            else
            {
                iv_cashierName.setVisibility(View.GONE);
                setButtonBg(bt_changeName, false, 0);
            }
        }
        
    }
    
    private void initView()
    {
        cashierName = getViewById(R.id.cashierName);
        iv_cashierName = getViewById(R.id.iv_cashierName);
        bt_changeName = getViewById(R.id.bt_changeName);
        
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        {
            
            @Override
            public void onExecute(CharSequence s, int start, int before, int count)
            {
            }
            
            @Override
            public void onAfterTextChanged(Editable s)
            {
                if (cashierName.isFocused() && cashierName.getText().toString().length() > 0)
                {
                    iv_cashierName.setVisibility(View.VISIBLE);
                    setButtonBg(bt_changeName, true, 0);
                }
                else
                {
                    iv_cashierName.setVisibility(View.GONE);
                    setButtonBg(bt_changeName, false, 0);
                }
            }
            
        });
        
        cashierName.addTextChangedListener(editTextWatcher);
        
        iv_cashierName.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                cashierName.setText("");
            }
        });

        bt_changeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StringUtil.isEmptyOrNull(cashierName.getText().toString()))
                {
                    toastDialog(CashierNameModifyActivity.this, R.string.tx_surname_notnull, null);
                    cashierName.setFocusable(true);
                    return;
                }

                userModel.setRealname(cashierName.getText().toString());
                submit(userModel);
            }
        });
    }

    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b, enable, res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_press_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }


    
    public static void startActivity(Context context, UserModel userModel)
    {
        Intent it = new Intent();
        it.setClass(context, CashierNameModifyActivity.class);
        it.putExtra("userModel", userModel);
        context.startActivity(it);
    }
    
    /** {@inheritDoc} */
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            CashierNameModifyActivity.this.finish();
        }
        
        return super.onKeyDown(keyCode, event);
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.et_name);
//        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.save));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
              /*  if (StringUtil.isEmptyOrNull(cashierName.getText().toString()))
                {
                    toastDialog(CashierNameModifyActivity.this, R.string.tx_surname_notnull, null);
                    cashierName.setFocusable(true);
                    return;
                }
                
                userModel.setRealname(cashierName.getText().toString());
                submit(userModel);*/
            }
            
            @Override
            public void onLeftButtonClick()
            {
                CashierNameModifyActivity.this.finish();
            }
        });
        
    }
    
    private void submit(final UserModel usemModel)
    {
        UserManager.cashierAdd(usemModel, new UINotifyListener<Boolean>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                
                showLoading(false, getString(R.string.show_save_loading));
            }
            
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dismissLoading();
                if (checkSession())
                {
                    return;
                }
                if (object != null)
                {
                    
                    toastDialog(CashierNameModifyActivity.this, object.toString(), null);
                }
            }
            
            @Override
            public void onSucceed(Boolean result)
            {
                super.onSucceed(result);
                dismissLoading();
                
                if (result)
                {
                    //                    toastDialog(CashierNameModifyActivity.this,
                    //                        R.string.tv_save_pay_method_prompt,
                    //                        new NewDialogInfo.HandleBtn()
                    //                        {
                    //                            
                    //                            @Override
                    //                            public void handleOkBtn()
                    //                            {
                    //                                
                    //                            }
                    //                        });
                    
                    HandlerManager.notifyMessage(HandlerManager.CASH_NAME,
                        HandlerManager.CASH_NAME,
                        usemModel.getRealname());
                    
                    finish();
                }
                
            }
        });
    }
}
