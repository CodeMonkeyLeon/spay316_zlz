package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @我来到这个世界
 * @为了看看太阳和蓝色的地平线
 */
class MySpeakerBindContract {


    interface View : BaseView {

        fun bindSpeakerSuccess(response: Boolean)

        fun bindSpeakerFailed(error: Any?)


        fun unbindSpeakerSuccess(response: Boolean)

        fun unbindSpeakerFailed(error: Any?)

    }


    interface Presenter : BasePresenter<View> {

        fun unbindSpeaker(
            userId: String?,
            deviceName: String?
        )

        fun bindSpeaker(deviceName: String?)

    }


}