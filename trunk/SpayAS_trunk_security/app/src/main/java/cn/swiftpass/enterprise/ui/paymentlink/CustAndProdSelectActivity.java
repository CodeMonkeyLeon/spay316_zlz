package cn.swiftpass.enterprise.ui.paymentlink;

import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.ADD_ORDER_CREATE_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.ADD_ORDER_CREATE_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.DATA_TAG;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.DEFAULT_PAGE_SIZE;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.REQUEST_CODE_SELECT_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.REQUEST_CODE_SELECT_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_CREATE_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_CREATE_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_SELECT_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_SELECT_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.SEARCH_TYPE_ADD_ORDER_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.SEARCH_TYPE_ADD_ORDER_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.TYPE_TAG;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.common.sentry.SentryUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.CustAndProdSelectContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.CustAndProdSelectPresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.paymentlink.adapter.BaseRecyclerAdapter;
import cn.swiftpass.enterprise.ui.paymentlink.adapter.CustomerSelectAdapter;
import cn.swiftpass.enterprise.ui.paymentlink.adapter.ProductSelectAdapter;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkCustomer;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkCustomerModel;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkProduct;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkProductModel;
import cn.swiftpass.enterprise.ui.widget.TitleBar;

/**
 * Created by congwei.li on 2021/9/9.
 *
 * @Description: payment Link 中 customer 和 product 的编辑和新增页面
 */
public class CustAndProdSelectActivity extends BaseActivity<CustAndProdSelectContract.Presenter> implements CustAndProdSelectContract.View {

    @BindView(R.id.ll_search_payment_link)
    LinearLayout search;
    @BindView(R.id.rv_product_customer_list)
    RecyclerView rvList;
    @BindView(R.id.tv_add_product)
    TextView addProduct;
    @BindView(R.id.tv_search_payment_link)
    TextView tvSearchPaymentLink;
    @BindView(R.id.tv_no_data)
    TextView tvNoData;
    @BindView(R.id.sw_payment_link_list)
    SwipeRefreshLayout swPaymentLinkList;
    ProductSelectAdapter productSelectAdapter;
    CustomerSelectAdapter customerSelectAdapter;
    private int requestCode;
    private PaymentLinkCustomer customerSelect;
    private ArrayList<PaymentLinkProduct> productsSelect;
    private ArrayList<PaymentLinkCustomer> dataCustomer;
    private ArrayList<PaymentLinkProduct> dataProduct;
    private int customerPage = 1;
    private int productPage = 1;

    public static void startActivityForResult(Fragment fragment, Serializable data, int requestCode) {
        Intent intent = new Intent(fragment.getContext(), CustAndProdSelectActivity.class);
        intent.putExtra(TYPE_TAG, requestCode);
        intent.putExtra(DATA_TAG, data);
        fragment.startActivityForResult(intent, requestCode);
    }

    @Override
    protected CustAndProdSelectContract.Presenter createPresenter() {
        return new CustAndProdSelectPresenter();
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cust_prod_select);
        ButterKnife.bind(this);

        requestCode = getIntent().getIntExtra(TYPE_TAG, REQUEST_CODE_SELECT_CUSTOMER);

        switch (requestCode) {
            case REQUEST_CODE_SELECT_CUSTOMER:
                tvSearchPaymentLink.setText(
                        getStringById(R.string.payment_link_manage_search_customer));
                break;
            case REQUEST_CODE_SELECT_PRODUCT:
                tvSearchPaymentLink.setText(
                        getStringById(R.string.payment_link_manage_search_product));
                break;
        }

        initView();
    }

    private void initView() {
        titleBar.setLeftButton(R.drawable.icon_general_top_back_default);
        titleBar.setRightButton(R.drawable.icon_add);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
                switch (requestCode) {
                    case REQUEST_CODE_SELECT_CUSTOMER:
                        CustAndProdEditActivity.startActivityForResult(CustAndProdSelectActivity.this,
                                ADD_ORDER_CREATE_CUSTOMER, null);
                        break;
                    case REQUEST_CODE_SELECT_PRODUCT:
                        CustAndProdEditActivity.startActivityForResult(CustAndProdSelectActivity.this,
                                ADD_ORDER_CREATE_PRODUCT, null);
                        break;
                }
            }

            @Override
            public void onRightLayClick() {
            }

            @Override
            public void onRightButLayClick() {
            }
        });

        rvList.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));

        if (requestCode == REQUEST_CODE_SELECT_CUSTOMER) {
            customerSelect = (PaymentLinkCustomer) getIntent().getSerializableExtra(DATA_TAG);
            if (customerSelect == null) {
                customerSelect = new PaymentLinkCustomer();
            }
            showCustomerEdit();
        } else if (requestCode == REQUEST_CODE_SELECT_PRODUCT) {
            productsSelect = (ArrayList<PaymentLinkProduct>) getIntent().getSerializableExtra(DATA_TAG);
            if (productsSelect == null) {
                productsSelect = new ArrayList<>();
            }
            showProductEdit();
        }

        swPaymentLinkList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                switch (requestCode) {
                    case REQUEST_CODE_SELECT_CUSTOMER:
                        customerPage = 1;
                        requestCustomerList(String.valueOf(customerPage));
                        break;
                    case REQUEST_CODE_SELECT_PRODUCT:
                        productPage = 1;
                        requestProductList(String.valueOf(productPage));
                        break;
                }

            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (requestCode) {
                    case REQUEST_CODE_SELECT_CUSTOMER:
                        SearchActivity.startActivityForResult(CustAndProdSelectActivity.this,
                                SEARCH_TYPE_ADD_ORDER_CUSTOMER,
                                getStringById(R.string.payment_link_manage_search_customer));
                        break;
                    case REQUEST_CODE_SELECT_PRODUCT:
                        SearchActivity.startActivityForResult(CustAndProdSelectActivity.this,
                                SEARCH_TYPE_ADD_ORDER_PRODUCT,
                                getStringById(R.string.payment_link_manage_search_product));
                        break;
                }
            }
        });
    }

    private void showProductEdit() {
        titleBar.setTitleText(R.string.payment_link_add_order_choose_products);
        addProduct.setVisibility(View.VISIBLE);
        addProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (productsSelect.size() > 20) {
                    toastDialog(CustAndProdSelectActivity.this,
                            getStringById(R.string.pl_create_order_product_exceed), null);
                    return;
                }
                returnSelectedProduct();
            }
        });

        dataProduct = new ArrayList<>();
        productSelectAdapter = new ProductSelectAdapter(this, dataProduct);
        productSelectAdapter.setOnProductNumChange(new ProductSelectAdapter.OnProductNumChange() {
            @Override
            public void onProductChange(PaymentLinkProduct data) {
                int selectProdNum = 0;
                String lastGoodsId = "";
                boolean isLast = false;
                for (int i = 0; i < dataProduct.size(); i++) {
                    if (Integer.parseInt(dataProduct.get(i).goodsNum) > 0 && !isLast
                            && !dataProduct.get(i).goodsId.equals(data.goodsId)) {
                        lastGoodsId = dataProduct.get(i).goodsId;
                    }
                    if (dataProduct.get(i).goodsId.equals(data.goodsId)) {
                        dataProduct.get(i).goodsNum = data.goodsNum;
                        isLast = true;
                    }
                    try {
                        selectProdNum += Integer.parseInt(dataProduct.get(i).goodsNum);
                    } catch (Exception e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                    }
                }
                boolean isHave = false;
                int position = -1;
                for (int i = 0; i < productsSelect.size(); i++) {
                    if (Integer.parseInt(data.goodsNum) <= 0) {
                        if (productsSelect.get(i).goodsId.equals(data.goodsId)) {
                            productsSelect.remove(i);
                            break;
                        }
                    } else {
                        if (lastGoodsId.equals(productsSelect.get(i).goodsId)) {
                            position = i;
                        }
                        if (productsSelect.get(i).goodsId.equals(data.goodsId)) {
                            isHave = true;
                            productsSelect.get(i).goodsNum = data.goodsNum;
                        }
                    }
                }
                if (!isHave && Integer.parseInt(data.goodsNum) > 0) {
                    if (position >= productsSelect.size() || position == -1) {
                        productsSelect.add(0, data);
                    } else {
                        productsSelect.add(position + 1, data);
                    }
                }
                addProduct.setEnabled(selectProdNum > 0);
            }
        });
        productSelectAdapter.setOnLoadMoreListener(new BaseRecyclerAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                requestProductList(String.valueOf(++productPage));
            }
        }, rvList);
        rvList.setAdapter(productSelectAdapter);

        productPage = 1;
        requestProductList(String.valueOf(productPage));
    }

    private void showCustomerEdit() {
        titleBar.setTitleText(R.string.payment_link_add_order_choose_customer);
        addProduct.setVisibility(View.GONE);

        dataCustomer = new ArrayList<>();
        customerSelectAdapter = new CustomerSelectAdapter(this, dataCustomer);
        customerSelectAdapter.setOnItemClickListener(new CustomerSelectAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                customerSelect = dataCustomer.get(position);
                returnSelectCustomer();
            }
        });
        customerSelectAdapter.setOnLoadMoreListener(new BaseRecyclerAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                requestCustomerList(String.valueOf(++customerPage));
            }
        }, rvList);
        rvList.setAdapter(customerSelectAdapter);

        customerPage = 1;
        requestCustomerList(String.valueOf(customerPage));
    }

    @Override
    public void getCustomerListSuccess(@Nullable PaymentLinkCustomerModel response) {
        requestCustomerListSuccess(response.data);
    }

    @Override
    public void getCustomerListFailed(@Nullable Object error) {
        showErrorMsgDialog(error);
    }

    private void requestCustomerList(String pageNum) {
        if (mPresenter != null) {
            swPaymentLinkList.setRefreshing(false);
            mPresenter.getCustomerList(pageNum, "", "", "");
        }
    }

    public void requestCustomerListSuccess(ArrayList<PaymentLinkCustomer> data) {
        if (isDataEmpty(customerPage, data)) {
            return;
        }
        if (customerPage != 1) {
            Iterator<PaymentLinkCustomer> it = data.iterator();
            while (it.hasNext()) {
                PaymentLinkCustomer customer = it.next();
                for (int i = 0; i < dataCustomer.size(); i++) {
                    if (customer.custId.equals(dataCustomer.get(i).custId)) {
                        it.remove();
                    }
                }
            }
        }
        if (customerPage == 1) {
            dataCustomer.clear();
            customerSelectAdapter.setDataList(dataCustomer);
        }
        dataCustomer.addAll(data);
        if (data.size() == 0 || data.size() < DEFAULT_PAGE_SIZE) {
            customerSelectAdapter.loadMoreEnd(false);
        } else {
            customerSelectAdapter.loadMoreComplete();
        }
        customerSelectAdapter.notifyDataSetChanged();
    }

    @Override
    public void getProductListSuccess(@Nullable PaymentLinkProductModel response) {
        requestProductListSuccess(response.data);
    }

    @Override
    public void getProductListFailed(@Nullable Object error) {
        showErrorMsgDialog(error);
    }

    private void requestProductList(String pageNum) {
        if (mPresenter != null) {
            swPaymentLinkList.setRefreshing(false);
            mPresenter.getProductList(pageNum, "", "", "");
        }
    }

    public void requestProductListSuccess(ArrayList<PaymentLinkProduct> data) {
        if (isDataEmpty(productPage, data)) {
            return;
        }
        if (productPage != 1) {
            Iterator<PaymentLinkProduct> it = data.iterator();
            while (it.hasNext()) {
                PaymentLinkProduct product = it.next();
                for (int i = 0; i < dataProduct.size(); i++) {
                    if (product.goodsId.equals(dataProduct.get(i).goodsId)) {
                        it.remove();
                    }
                }
            }
        }
        for (int i = 0; i < productsSelect.size(); i++) {
            for (int j = 0; j < data.size(); j++) {
                if (data.get(j).goodsId.equals(productsSelect.get(i).goodsId)) {
                    data.get(j).goodsNum = productsSelect.get(i).goodsNum;
                }
            }
        }
        if (productPage == 1) {
            dataProduct.clear();
            productSelectAdapter.setDataList(dataProduct);
        }
        dataProduct.addAll(data);
        if (data.size() == 0 || data.size() < DEFAULT_PAGE_SIZE) {
            productSelectAdapter.loadMoreEnd(false);
        } else {
            productSelectAdapter.loadMoreComplete();
        }
        productSelectAdapter.notifyDataSetChanged();
    }

    private void showErrorMsgDialog(Object object) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                toastDialog(CustAndProdSelectActivity.this, object.toString(), null);
                switch (requestCode) {
                    case REQUEST_CODE_SELECT_CUSTOMER:
                        requestCustomerListSuccess(new ArrayList<>());
                        break;
                    case REQUEST_CODE_SELECT_PRODUCT:
                        requestProductListSuccess(new ArrayList<>());
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == ADD_ORDER_CREATE_CUSTOMER || requestCode == SEARCH_TYPE_ADD_ORDER_CUSTOMER)
                && resultCode == RESULT_CODE_CREATE_CUSTOMER && null != data) {
            customerSelect = (PaymentLinkCustomer) data.getSerializableExtra("result");
            if (null == customerSelect) {
                return;
            }
            returnSelectCustomer();
        }
        if ((requestCode == ADD_ORDER_CREATE_PRODUCT || requestCode == SEARCH_TYPE_ADD_ORDER_PRODUCT)
                && resultCode == RESULT_CODE_CREATE_PRODUCT && null != data) {
            PaymentLinkProduct product = (PaymentLinkProduct) data.getSerializableExtra("result");
            if (null == product) {
                return;
            }
            product.goodsNum = "1";
            boolean isHave = false;
            for (int i = 0; i < productsSelect.size(); i++) {
                if (productsSelect.get(i).goodsId.equals(product.goodsId)) {
                    isHave = true;
                    int goodsNum = 0;
                    try {
                        goodsNum = Integer.parseInt(productsSelect.get(i).goodsNum);
                    } catch (Exception e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                    }
                    productsSelect.get(i).goodsNum = String.valueOf(++goodsNum);
                }
            }
            if (!isHave) {
                if (productsSelect.size() < 20) {
                    dataProduct.add(product);
                    productsSelect.add(product);
                } else {
                    toastDialog(CustAndProdSelectActivity.this,
                            getStringById(R.string.pl_create_order_product_exceed), null);
                    return;
                }
            }
            returnSelectedProduct();
        }
    }

    private void returnSelectedProduct() {
        Intent i = new Intent();
        i.putExtra("result", productsSelect);
        setResult(RESULT_CODE_SELECT_PRODUCT, i);
        finish();
    }

    private void returnSelectCustomer() {
        Intent i = new Intent();
        i.putExtra("result", customerSelect);
        setResult(RESULT_CODE_SELECT_CUSTOMER, i);
        finish();
    }

    public boolean isDataEmpty(int page, List data) {
        if ((page == 1) && (data == null || data.size() <= 0)) {
            rvList.setVisibility(View.GONE);
            tvNoData.setVisibility(View.VISIBLE);
            return true;
        } else {
            rvList.setVisibility(View.VISIBLE);
            tvNoData.setVisibility(View.GONE);
            return false;
        }
    }

}
