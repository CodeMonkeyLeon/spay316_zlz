package cn.swiftpass.enterprise.ui.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.City;
import cn.swiftpass.enterprise.io.database.CityDB;
import cn.swiftpass.enterprise.ui.adapter.CityAdapter;
import cn.swiftpass.enterprise.ui.widget.KeyBoardLinearLayout;
import cn.swiftpass.enterprise.ui.widget.KeyBoardLinearLayout.onKybdsChangeListener;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.ui.widget.plistview.BladeView;
import cn.swiftpass.enterprise.ui.widget.plistview.PinnedHeaderListView;
import cn.swiftpass.enterprise.utils.Logger;

/*import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;*/

/**
 * 城市选择
 * User: Alan
 * Date: 13-12-27
 * Time: 下午2:45
 */
public class SelectCityActivity extends TemplateActivity implements MainApplication.EventHandler
{
    private PinnedHeaderListView mCityListView;
    
    private BladeView mLetter;
    
    private List<City> mCities;
    
    private CityAdapter mCityAdapter;
    
    // 首字母集
    private List<String> mSections;
    
    // 根据首字母存放数据
    private Map<String, List<City>> mMap;
    
    // 首字母位置集
    private List<Integer> mPositions;
    
    // 首字母对应的位置
    private Map<String, Integer> mIndexer;
    
    private CityDB mCityDB;
    
    private AutoCompleteTextView mAutoCompleteTextView;
    
    private KeyBoardLinearLayout keyBoardLinearLayout;
    
    private TextView tvLocaCity;
    
    private Context mContext;
    
   /* public LocationClient mLocationClient = null;
    
    public BDLocationListener myListener;*/
    
    private String loc = null; // 保存定位信息
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_select_city);
        MainApplication.mListeners.add(this);
       /* mLocationClient = new LocationClient(getApplicationContext()); // 声明LocationClient类
        myListener = new MyLocationListener();
        mLocationClient.registerLocationListener(myListener); // 注册监听函数
        LocationClientOption option = new LocationClientOption();
        option.setOpenGps(true);// 打开GPS
        option.setAddrType("all");// 返回的定位结果包含地址信息
        option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度,默认值gcj02
        option.setScanSpan(3000);// 设置发起定位请求的间隔时间为3000ms
        option.disableCache(false);// 禁止启用缓存定位
        option.setPriority(LocationClientOption.NetWorkFirst);// 网络定位优先
        mLocationClient.setLocOption(option);// 使用设置
        mLocationClient.start();// 开启定位SDK
        mLocationClient.requestLocation();// 开始请求位置*/
        initView();
        initData();
        
    }
    
    private void initView()
    {
        mCityListView = (PinnedHeaderListView)findViewById(R.id.citys_list);
        mLetter = (BladeView)findViewById(R.id.citys_bladeview);
        tvLocaCity = (TextView)findViewById(R.id.tv_being_loca_city);
        tvLocaCity.setOnClickListener(new OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                City mCity = new City();
                mCity.setCity(tvLocaCity.getText().toString());
                startActivity(mCity);
            }
        });
        mLetter.setOnItemClickListener(new BladeView.OnItemClickListener()
        {
            
            @Override
            public void onItemClick(String s)
            {
                if (mIndexer.get(s) != null)
                {
                    mCityListView.setSelection(mIndexer.get(s));
                }
            }
        });
        mLetter.setVisibility(View.VISIBLE);
        
        mCityListView.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener()
        {
            
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                // TODO Auto-generated method stub
                Logger.i(mCityAdapter.getItem(position).toString());
                startActivity(mCityAdapter.getItem(position));
            }
        });
        mAutoCompleteTextView = (AutoCompleteTextView)findViewById(R.id.input_city_name_hint);
        //        mAutoCompleteTextView.setOnFocusChangeListener(new OnFocusChangeListener()
        //        {
        //            
        //            @Override
        //            public void onFocusChange(View v, boolean hasFocus)
        //            {
        //                if (hasFocus)
        //                    mLetter.setVisibility(View.GONE);
        //                else
        //                    mLetter.setVisibility(View.VISIBLE);
        //                
        //            }
        //        });
        
        keyBoardLinearLayout = (KeyBoardLinearLayout)findViewById(R.id.keyBoardLayout);
        keyBoardLinearLayout.setOnkbdStateListener(new onKybdsChangeListener()
        {
            
            @Override
            public void onKeyBoardStateChange(int state)
            {
                Log.v("clarence", "---->" + state);
                switch (state)
                {
                    case KeyBoardLinearLayout.KEYBOARD_STATE_HIDE:
                    case KeyBoardLinearLayout.KEYBOARD_STATE_INIT:
                        mLetter.setVisibility(View.VISIBLE);
                        break;
                    case KeyBoardLinearLayout.KEYBOARD_STATE_SHOW:
                        mLetter.setVisibility(View.INVISIBLE);
                        break;
                }
            }
        });
        
    }
    
    private void startActivity(City city)
    {
        Intent i = new Intent();
        i.putExtra("city", city);
        setResult(RESULT_OK, i);
        finish();
    }
    
    List<String> list = new ArrayList<String>();
    
    @SuppressWarnings("null")
    private void initData()
    {
        
        mCityDB = MainApplication.getContext().getCityDB();
        //mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        
        if (MainApplication.getContext().isCityListComplite())
        {
            mCities = MainApplication.getContext().getCityList();
            mSections = MainApplication.getContext().getSections();
            mMap = MainApplication.getContext().getMap();
            mPositions = MainApplication.getContext().getPositions();
            mIndexer = MainApplication.getContext().getIndexer();
            
            mCityAdapter = new CityAdapter(mContext, mCities, mMap, mSections, mPositions);
            mCityListView.setAdapter(mCityAdapter);
            mCityListView.setOnScrollListener(mCityAdapter);
            mCityListView.setPinnedHeaderView(LayoutInflater.from(mContext)
                .inflate(R.layout.biz_plugin_weather_list_group_item, mCityListView, false));
            //  mTitleProgressBar.setVisibility(View.GONE);
            for (City mCity : mCities)
            {
                list.add(mCity.getCity());
            }
            int size = list.size();
            String[] strs = (String[])list.toArray(new String[size]);
            final ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, strs);
            mAutoCompleteTextView.setAdapter(adapter);
            mAutoCompleteTextView.setOnItemClickListener(new OnItemClickListener()
            {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
                {
                    Adapter adapter = arg0.getAdapter();
                    String s = (String)adapter.getItem(arg2);
                    if ("定位失败".equals(s))
                    {
                        s = "深圳市";
                    }
                    City mCity = new City();
                    mCity.setCity(s);
                    startActivity(mCity);
                }
            });
            //            mLetter.setVisibility(View.VISIBLE);
        }
        
    }
    
    @Override
    protected void onDestroy()
    {
//        stopListener();//停止监听
        super.onDestroy();
        //Application.mListeners.remove(this);
    }
    
    @Override
    public void onCityComplite()
    {
        // 城市列表加载完的回调函数 
        
        mCities = MainApplication.getContext().getCityList();
        mSections = MainApplication.getContext().getSections();
        mMap = MainApplication.getContext().getMap();
        mPositions = MainApplication.getContext().getPositions();
        mIndexer = MainApplication.getContext().getIndexer();
        
        mCityAdapter = new CityAdapter(mContext, mCities, mMap, mSections, mPositions);
        mCityListView.setAdapter(mCityAdapter);
        mCityListView.setOnScrollListener(mCityAdapter);
        mCityListView.setPinnedHeaderView(LayoutInflater.from(mContext)
            .inflate(R.layout.biz_plugin_weather_list_group_item, mCityListView, false));
        //        mLetter.setVisibility(View.VISIBLE);
    }
    
    @Override
    public void onNetChange()
    {
    }
    
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle("选择城市");
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightLayClick()
            {
                // TODO Auto-generated method stub
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                // TODO Auto-generated method stub
                
            }
        });
    }
    
    /*public class MyLocationListener implements BDLocationListener
    {
        @Override
        public void onReceiveLocation(BDLocation location)
        {
            if (location != null)
            {
                StringBuffer sb = new StringBuffer(128);// 接受服务返回的缓冲区
                if (location.getCity() == null || "".equals(location.getCity()))
                {
                    tvLocaCity.setText("定位失败");
                }
                sb.append(location.getCity());// 获得城市
                loc = sb.toString().trim();
                tvLocaCity.setText(loc);
                tvLocaCity.setEnabled(true);
            }
            else
            {
                tvLocaCity.setText("无法定位");
                return;
            }
        }
        
        @Override
        public void onReceivePoi(BDLocation arg0)
        {
            
        }
        
    }
    
    *//**
     * 停止，减少资源消耗
     *//*
    public void stopListener()
    {
        if (mLocationClient != null && mLocationClient.isStarted())
        {
            mLocationClient.stop();// 关闭定位SDK
            mLocationClient = null;
        }
    }*/
}
