package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.MasterCardInfo
import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.bussiness.model.QRcodeInfo
import cn.swiftpass.enterprise.common.Constant
import cn.swiftpass.enterprise.intl.BuildConfig
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.ShowQrCodeContract
import cn.swiftpass.enterprise.mvp.utils.ParamsUtils
import cn.swiftpass.enterprise.utils.JsonUtil
import cn.swiftpass.enterprise.utils.SharedPreUtils
import cn.swiftpass.enterprise.utils.StringUtil
import com.example.common.sentry.SentryUtils.getClassNameAndMethodName
import com.example.common.sentry.SentryUtils.uploadTryCatchException
import com.example.common.sp.PreferenceUtil
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/2
 *
 * @锁上我的记忆
 * @锁上我的忧伤
 * @不再想你
 * @怎么可能再想你
 * @快乐是禁地
 * @生死之后
 * @找不到进去的钥匙
 */
class ShowQrCodePresenter : ShowQrCodeContract.Presenter {

    private var mView: ShowQrCodeContract.View? = null


    override fun queryOrderByOrderNo(orderNo: String?) {
        mView?.let { view ->
            AppClient.queryOrderByOrderNo(
                Constant.PAY_ZFB_QUERY,
                MainApplication.getInstance().getUserId().toString(),
                MainApplication.getInstance().getMchId(),
                orderNo,
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        commonResponse?.let {
                            view.queryOrderByOrderNoFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        r?.let { response ->
                            //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"",
                                "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                Order::class.java
                            ) as Order
                            order.tradeState =
                                if (TextUtils.isEmpty(order.state)) 0 else order.state.toInt()
                            order.tradeType = order.service
                            order.openid = order.subOpenID
                            order.cashFeel =
                                if (TextUtils.isEmpty(order.cashFee)) 0 else order.cashFee.toLong()
                            order.setUplanDetailsBeans(order.discountDetail)
                            view.queryOrderByOrderNoSuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun queryMasterCardOrderByOrderNo(service: String?, outTradeNo: String?) {
        mView?.let { view ->
            AppClient.queryMasterCardOrderByOrderNo(
                outTradeNo,
                service,
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        commonResponse?.let {
                            view.queryMasterCardOrderByOrderNoFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        r?.let { response ->
                            //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"",
                                "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                Order::class.java
                            ) as Order

                            view.queryMasterCardOrderByOrderNoSuccess(order)
                        }
                    }
                }
            )
        }
    }


    override fun masterCardNativePay(
        money: String?,
        apiCode: String?,
        attach: String?
    ) {
        mView?.let { view ->
            view.showLoading(R.string.tv_pay_prompt, ParamsConstants.COMMON_LOADING)
            val deskName = PreferenceUtil.getString(ParamsConstants.CASHIER_DESK_NAME, "")
            val mchId = MainApplication.getInstance().getMchId()
            val mCardPaymentPayTypeMap = SharedPreUtils.readProduct(
                ParamsConstants.CARD_PAYMENT_PAY_TYPE_MAP +
                        BuildConfig.bankCode + mchId
            )
            var service = ""
            val map = mCardPaymentPayTypeMap as Map<String, String>
            if (map.isNotEmpty()) {
                map[apiCode]?.let {
                    service = it
                }
            }
            val body =
                if (!TextUtils.isEmpty(MainApplication.getInstance().userInfo.tradeName)) MainApplication.getInstance().userInfo.tradeName else BuildConfig.body


            AppClient.masterCardNativePay(
                mchId,
                service,
                apiCode,
                body,
                Constant.CLIENT,
                money,
                deskName,
                attach,
                MainApplication.getInstance().userInfo.realname,
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<MasterCardInfo>(MasterCardInfo()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.masterCardNativePayFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                MasterCardInfo::class.java
                            ) as MasterCardInfo
                            view.masterCardNativePaySuccess(order, apiCode)
                        }
                    }
                }
            )
        }
    }


    override fun unifiedNativePay(
        money: String?,
        pt: String?,
        traType: String?,
        adm: Long,
        outTradeNo: String?,
        note: String?,
        isInstaPay: Boolean,
        isSecond: Boolean
    ) {
        mView?.let { view ->
            view.showLoading(R.string.tv_pay_prompt, ParamsConstants.COMMON_LOADING)
            val deskName = PreferenceUtil.getString(ParamsConstants.CASHIER_DESK_NAME, "")
            val payType = ParamsUtils.updateType(pt)
            val body =
                if (!TextUtils.isEmpty(MainApplication.getInstance().userInfo.tradeName)) MainApplication.getInstance().userInfo.tradeName else BuildConfig.body
            val mchId = MainApplication.getInstance().getMchId()
            var service: String? = ""
            try {
                //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
                val payTypeMap: Any =
                    if (MainApplication.getInstance().userInfo.isAuthFreezeOpen == 1 && TextUtils.equals(
                            PreferenceUtil.getString(
                                ParamsConstants.CHOOSE_PAY_OR_PRE_AUTH,
                                "sale"
                            ),
                            ParamsConstants.PRE_AUTH
                        )
                    ) {
                        SharedPreUtils.readProduct(ParamsConstants.PAY_TYPE_MAP_PRE_AUTH + BuildConfig.bankCode + mchId)
                    } else {
                        SharedPreUtils.readProduct(ParamsConstants.PAY_TYPE_MAP + BuildConfig.bankCode + mchId)
                    }
                val map = payTypeMap as Map<String, String>
                if (null != map && map.isNotEmpty()) {
                    service = map[payType]
                }
            } catch (e: Exception) {
                uploadTryCatchException(
                    e,
                    getClassNameAndMethodName()
                )
                payType?.let {
                    service = it
                }
            }

            AppClient.unifiedNativePay(
                money,
                note,
                deskName,
                outTradeNo,
                adm.toString(),
                MainApplication.getInstance().userInfo.realname,
                MainApplication.getInstance().getUserId().toString(),
                body,
                mchId,
                Constant.CLIENT,
                payType,
                traType,
                service,
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<QRcodeInfo>(QRcodeInfo()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.unifiedNativePayFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                QRcodeInfo::class.java
                            ) as QRcodeInfo
                            if (!StringUtil.isEmptyOrNull(order.service) && order.service == Constant.PAY_QQ_NATIVE1) {
                                val codeImg = order.codeImg
                                val code = codeImg.substring(0, codeImg.lastIndexOf("pay"))
                                val tokenId = codeImg.substring(codeImg.lastIndexOf("=") + 1)
                                order.uuId = code + "pay/qqpay?token_id=" + tokenId
                            } else {
                                val codeUlr = order.uuid
                                if (!TextUtils.isEmpty(codeUlr) && codeUlr != "null") {
                                    order.uuId = codeUlr
                                } else {
                                    // 截取code_img_url
                                    val code_img_url = order.codeImg
                                    if (!TextUtils.isEmpty(code_img_url)) {
                                        order.uuId =
                                            code_img_url.substring(code_img_url.lastIndexOf("=") + 1)
                                    }
                                }
                            }
                            order.totalMoney = order.money
                            order.payType = payType
                            order.orderNo = order.outTradeNo
                            order.apiCode = payType
                            view.unifiedNativePaySuccess(order, payType, isInstaPay, isSecond)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: ShowQrCodeContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}