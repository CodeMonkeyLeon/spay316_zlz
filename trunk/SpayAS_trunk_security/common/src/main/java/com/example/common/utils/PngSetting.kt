package com.example.common.utils

object PngSetting {

    private var mIsChangePng1 = false
//    private var mIsChangePng2 = false

    fun init1(isChangePng: Boolean) {
        mIsChangePng1 = isChangePng
    }

//    fun init2(isChangePng: Boolean) {
//        mIsChangePng2 = isChangePng
//    }

    fun isChangePng1() = mIsChangePng1
//    fun isChangePng2() = mIsChangePng2
}