package cn.swiftpass.enterprise.newmvp.config;

/**
 * 创建人：caoxiaoya
 * 时间：2019/3/18
 * 描述：管理SharedPreferences所有key
 * 备注：
 */
public class SPConfig {
    /**
     * SP存储名字
     */
    public final static String SP_NAME = "spay_data";
    public final static String LOGIN_SKEY = "login_skey";
    public final static String LOGIN_SAUTHID = "login_sauthid";
    /**
     * 语言
     */
    public final static String LANGUAGE = "language";
    public final static String SECRETKEY = "secretKey";

}
