package cn.swiftpass.enterprise.ui.activity;

import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.CashierReport;
import cn.swiftpass.enterprise.bussiness.model.OrderReport;
import cn.swiftpass.enterprise.bussiness.model.StreamTotabean;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.ToastHelper;

public class StreamActivity extends BaseActivity implements OnItemClickListener
{
    private ListView lv;
    
    private ImageView image_view;
    
    private RelativeLayout stream_id;
    
    private EditText searchContent;
    
    private ImageView iv_clear, iv_search;
    
    private ListView stream_list;
    
    private ViewHolder holder;
    
    private ViewHolderOrder holderOrder;
    
    private List<CashierReport> cashierReports;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stream);
        
        initView();
        setLister();
        
        //        stream_id = (RelativeLayout)findViewById(R.id.stream_trade_id);
        //        image_view = (ImageView)findViewById(R.id.stream_id);
        //        image_view.setImageResource(R.drawable.icon_arrow_triangle);
        
        //        lv = (ListView)findViewById(R.id.activity_stream_id);
        
        //        lv.setAdapter(new ContactAdapter());
        //        stream_id.setOnClickListener(new View.OnClickListener()
        //        {
        //            @Override
        //            public void onClick(View v)
        //            {
        //                Intent intent = new Intent(StreamActivity.this, MainActivity.class);
        //                startActivity(intent);
        //                finish();
        //                return;
        //                
        //            }
        //        });
    }
    
    boolean isflag = false;
    
    private ListView listView;
    
    @Override
    public void onItemClick(AdapterView<?> arg0, View v, int position, long id)
    {
        listView = (ListView)v.findViewById(R.id.list_stream);
        listView.setVisibility(View.VISIBLE);
        listView.setTag(position);
        //        if (!isflag)
        //        {
        CashierReport report = cashierReports.get(position);
        OrderAdapter orderAdapter = new OrderAdapter(report.getStreamTotabeans(), StreamActivity.this);
        listView.setAdapter(orderAdapter);
        isflag = true;
        //        }
        //        else
        //        {
        //            if (((Integer)listView.getTag() == position))
        //            {
        //                listView.setVisibility(View.GONE);
        //            }
        //            
        //            isflag = false;
        //        }
        
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void setLister()
    {
        
        iv_clear.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                searchContent.setText("");
                iv_clear.setVisibility(View.GONE);
                
            }
        });
        stream_list.setOnItemClickListener(this);
    }
    
    private void queryOrderReport()
    {
        OrderManager.getInstance().queryOrderCashierTotal(MainApplication.merchantId,
            String.valueOf(MainApplication.userId),
            new UINotifyListener<OrderReport>()
            {
                @Override
                public void onError(Object object)
                {
                    super.onError(object);
                    if (object != null)
                    {
                        ToastHelper.showInfo(object.toString());
                    }
                }
                
                @Override
                public void onSucceed(OrderReport result)
                {
                    super.onSucceed(result);
                    
                    if (result != null)
                    {
                        cashierReports = result.cashierReports;
                        ContactAdapter adapter = new ContactAdapter(StreamActivity.this, cashierReports);
                        stream_list.setAdapter(adapter);
                    }
                    
                }
                
                @Override
                public void onPostExecute()
                {
                    super.onPostExecute();
                    // dismissLoading();
                    
                }
                
                @Override
                public void onPreExecute()
                {
                    super.onPreExecute();
                    
                }
            });
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView()
    {
        searchContent = getViewById(R.id.searchContent);
        iv_clear = getViewById(R.id.iv_clear);
        iv_search = getViewById(R.id.iv_search);
        stream_list = getViewById(R.id.stream_list);
        
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged()
        {
            @Override
            public void onExecute(CharSequence s, int start, int before, int count)
            {
            }
            
            @Override
            public void onAfterTextChanged(Editable s)
            {
                
                if (s != null && s.length() >= 1)
                {
                    if (searchContent.hasFocus())
                    {
                        iv_clear.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        
        searchContent.addTextChangedListener(editTextWatcher);
        
        queryOrderReport();
        
    }
    
    private class OrderAdapter extends BaseAdapter
    {
        
        private List<StreamTotabean> streamTotabeans;
        
        private Context context;
        
        public OrderAdapter(List<StreamTotabean> streamTotabeans, Context context)
        {
            this.context = context;
            this.streamTotabeans = streamTotabeans;
        }
        
        @Override
        public int getCount()
        {
            return streamTotabeans.size();
        }
        
        /** {@inheritDoc} */
        
        @Override
        public Object getItem(int position)
        {
            return streamTotabeans.get(position);
        }
        
        /** {@inheritDoc} */
        
        @Override
        public long getItemId(int position)
        {
            // TODO Auto-generated method stub
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            
            if (convertView == null)
            {
                convertView = View.inflate(StreamActivity.this, R.layout.stream_list, null);
                holderOrder = new ViewHolderOrder();
                holderOrder.busines_time = (TextView)convertView.findViewById(R.id.busines_time);
                holderOrder.stream_img_id = (ImageView)convertView.findViewById(R.id.stream_img_id);
                holderOrder.sum_of_busines = (TextView)convertView.findViewById(R.id.sum_of_busines);
                
                convertView.setTag(holderOrder);
            }
            else
            {
                holderOrder = (ViewHolderOrder)convertView.getTag();
            }
            
            StreamTotabean bean = streamTotabeans.get(position);
            if (bean.getTradeTime() != null)
            {
                holderOrder.busines_time.setText(bean.getTradeTime());
            }
            holderOrder.sum_of_busines.setText("¥" + bean.getMoney());
            
            return convertView;
        }
        
    }
    
    private class ContactAdapter extends BaseAdapter
    {
        
        private List<CashierReport> cashierReports;
        
        private Context context;
        
        public ContactAdapter(Context context, List<CashierReport> cashierReports)
        {
            this.context = context;
            this.cashierReports = cashierReports;
        }
        
        @Override
        public int getCount()
        {
            return cashierReports.size();
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(StreamActivity.this, R.layout.stream_list_item, null);
                holder = new ViewHolder();
                holder.count = (TextView)convertView.findViewById(R.id.count);
                holder.gather_money_id = (TextView)convertView.findViewById(R.id.gather_money_id);
                holder.month_id = (TextView)convertView.findViewById(R.id.month_id);
                holder.list_stream = (ListView)convertView.findViewById(R.id.list_stream);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            
            CashierReport report = cashierReports.get(position);
            if (report.getTradeNum() != null)
            {
                holder.count.setText("收款：" + report.getTradeNum() + "笔");
            }
            if (report.getTurnover() != null)
            {
                holder.gather_money_id.setText("¥" + report.getTurnover());
            }
            holder.month_id.setText(report.getAddTime());
            
            return convertView;
        }
        
        @Override
        public Object getItem(int position)
        {
            // TODO Auto-generated method stub
            return cashierReports.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            // TODO Auto-generated method stub
            return position;
        }
        
    }
    
    private class ViewHolderOrder
    {
        private TextView busines_time, sum_of_busines;
        
        private ImageView stream_img_id;
        
    }
    
    private class ViewHolder
    {
        private TextView month_id, gather_money_id, count;
        
        private ListView list_stream;
    }
    
    /** {@inheritDoc} */
    
}
