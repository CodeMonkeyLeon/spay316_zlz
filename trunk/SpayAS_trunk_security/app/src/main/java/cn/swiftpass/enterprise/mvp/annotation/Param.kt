package cn.swiftpass.enterprise.mvp.annotation

/**
 * @author lizheng.zhao
 * @date 2022/12/14
 *
 * @一眨眼
 * @算不算少年
 * @一辈子
 * @算不算永远
 */
@MustBeDocumented
@Target(AnnotationTarget.VALUE_PARAMETER)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class Param(val value: String)
