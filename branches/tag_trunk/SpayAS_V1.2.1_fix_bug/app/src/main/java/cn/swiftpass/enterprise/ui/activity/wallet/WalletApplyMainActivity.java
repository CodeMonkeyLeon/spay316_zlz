/*
 * 文 件 名:  WalletMainActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-1
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.wallet;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.wallet.WalletManager;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.WalletModel;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.ContentTextActivity;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

import com.tencent.stat.StatService;

/**
 * 电子钱包
 * 
 * @author  he_hui
 * @version  [版本号, 2016-11-1]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class WalletApplyMainActivity extends TemplateActivity
{
    private CheckBox cb_box;
    
    private Button btn_open, btn_out;
    
    private LinearLayout ly_push, ly_bottom;
    
    private RelativeLayout rl_top;
    
    private TextView tv_balance, tv_refresh;
    
    private ProgressBar pro_bar;
    
    private WalletModel walletModel;
    
    private LinearLayout ly_pro;
    
    private LinearLayout ly_error;
    
    private TextView tv_error;
    
    private Button bt_reload;
    
    private Handler handler = new Handler()
    {
        public void handleMessage(android.os.Message msg)
        {
            if (msg.what == HandlerManager.WALLET_REFERSH)
            { //刷新余额界面
                loadeWalletQueryBalanc(false);
            }
            else if (msg.what == HandlerManager.WALLET_OPEN_SUCCESS)
            {
                loadisHaveEwallet(false);
            }
        };
    };
    
    @Override
    protected void onResume()
    {
        super.onResume();
        if (MainApplication.isHasEwallet == 1)
        {
            //加载余额显示
            setView();
            loadeWalletQueryBalanc(true);
        }
        else
        {
            loadisHaveEwallet(false);
        }
    };
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_main_layout);
        
        initView();
        setLister();
        
        HandlerManager.registerHandler(HandlerManager.WALLET_REFERSH, handler);
        
        HandlerManager.registerHandler(HandlerManager.WALLET_OPEN_SUCCESS, handler);
    }
    
    void setView()
    {
        ly_push.setVisibility(View.VISIBLE);
        rl_top.setVisibility(View.GONE);
        ly_bottom.setVisibility(View.GONE);
        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.tx_apply_balance_list));
    }
    
    /**
     * 加载余额
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void loadeWalletQueryBalanc(final boolean isTag)
    {
        WalletManager.getInstance().ewalletQueryBalanc(new UINotifyListener<WalletModel>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                if (isTag)
                {
                    loadDialog(WalletApplyMainActivity.this, R.string.public_data_loading);
                }
                else
                {
                    pro_bar.setVisibility(View.VISIBLE);
                }
            }
            
            @Override
            public void onPostExecute()
            {
                super.onPostExecute();
            }
            
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dissDialog();
                if (checkSession())
                {
                    return;
                }
                if (null != object)
                {
                    WalletApplyMainActivity.this.runOnUiThread(new Runnable()
                    {
                        
                        @Override
                        public void run()
                        {
                            //                            tv_refresh.setVisibility(View.VISIBLE);
                            pro_bar.setVisibility(View.GONE);
                            ly_error.setVisibility(View.VISIBLE);
                            ly_push.setVisibility(View.GONE);
                            rl_top.setVisibility(View.GONE);
                            ly_bottom.setVisibility(View.GONE);
                            //toastDialog(WalletApplyMainActivity.this, object.toString(), null);
                            tv_error.setText(object.toString());
                        }
                    });
                }
            }
            
            @Override
            public void onSucceed(WalletModel model)
            {
                dissDialog();
                pro_bar.setVisibility(View.GONE);
                if (null != model)
                {
                    walletModel = model;
                    tv_balance.setVisibility(View.VISIBLE);
                    if (model.getBalance() > 0)
                    {
                        tv_balance.setText(DateUtil.formatMoneyUtils(model.getBalance()));
                        //                        btn_out.setBackgroundResource(R.drawable.btn_register);
                        //                        btn_out.setEnabled(true);
                        tv_refresh.setVisibility(View.GONE);
                    }
                    //                    else
                    //                    {
                    //                        tv_refresh.setVisibility(View.VISIBLE);
                    //                    }
                    
                    ly_error.setVisibility(View.GONE);
                    ly_push.setVisibility(View.VISIBLE);
                    rl_top.setVisibility(View.GONE);
                    ly_bottom.setVisibility(View.GONE);
                    
                }
                else
                {
                    tv_refresh.setVisibility(View.VISIBLE);
                    tv_balance.setVisibility(View.GONE);
                }
            }
        });
    }
    
    /**
     * 加载是否开通电子钱包
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void loadisHaveEwallet(final boolean isTag)
    {
        WalletManager.getInstance().isHaveEwallet(new UINotifyListener<WalletModel>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                if (isTag)
                {
                    loadDialog(WalletApplyMainActivity.this, R.string.public_loading);
                    
                }
                else
                {
                    loadDialog(WalletApplyMainActivity.this, R.string.public_data_loading);
                }
            }
            
            @Override
            public void onPostExecute()
            {
                super.onPostExecute();
            }
            
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dissDialog();
                if (checkSession())
                {
                    return;
                }
                WalletApplyMainActivity.this.runOnUiThread(new Runnable()
                {
                    
                    @Override
                    public void run()
                    {
                        tv_refresh.setVisibility(View.VISIBLE);
                        tv_balance.setVisibility(View.GONE);
                        // toastDialog(WalletApplyMainActivity.this, object.toString(), null);
                        tv_error.setText(object.toString());
                        ly_error.setVisibility(View.VISIBLE);
                        ly_push.setVisibility(View.GONE);
                        rl_top.setVisibility(View.GONE);
                        ly_bottom.setVisibility(View.GONE);
                        
                    }
                });
            }
            
            @Override
            public void onSucceed(WalletModel model)
            {
                dissDialog();
                if (null != model)
                {
                    walletModel = model;
                    MainApplication.isHasEwallet = model.getIsHasEwallet();
                    MainApplication.accountId = model.getAccountId();
                    if (MainApplication.isHasEwallet == -1 || MainApplication.isHasEwallet == 0)
                    { //没有开通
                        if (isTag)
                        {
                            WalletOpenActivity.startActivity(WalletApplyMainActivity.this, model);
                        }
                        else
                        {
                            
                            ly_push.setVisibility(View.GONE);
                            rl_top.setVisibility(View.VISIBLE);
                            ly_bottom.setVisibility(View.VISIBLE);
                            ly_error.setVisibility(View.GONE);
                            titleBar.setRightButLayVisibleForTotal(false, getString(R.string.tx_apply_balance_list));
                        }
                    }
                    else
                    { //开通了电子钱包
                        setView();
                        if (model.getBalance() > 0)
                        {
                            tv_balance.setText(DateUtil.formatMoneyUtils(model.getBalance()));
                            tv_balance.setVisibility(View.VISIBLE);
                            //                            btn_out.setBackgroundResource(R.drawable.btn_register);
                            //                            btn_out.setEnabled(true);
                        }
                        //                        else
                        //                        {
                        //                            tv_refresh.setVisibility(View.VISIBLE);
                        //                        }
                        
                        if (model.getIsBanlance().equals("2"))
                        { //失败
                            tv_refresh.setVisibility(View.VISIBLE);
                            tv_balance.setVisibility(View.GONE);
                        }
                    }
                    
                }
            }
        });
    }
    
    private void setLister()
    {
        
        bt_reload.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (MainApplication.isHasEwallet == 1)
                {
                    //加载余额显示
                    setView();
                    loadeWalletQueryBalanc(true);
                }
                else
                {
                    loadisHaveEwallet(false);
                }
            }
        });
        
        tv_refresh.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                loadeWalletQueryBalanc(false);
            }
        });
        
        //立即开通
        btn_open.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                //                showPage(WalletOpenActivity.class);
                loadisHaveEwallet(true); //同意开通电子钱包
            }
        });
        
        //余额转出
        btn_out.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                if (walletModel != null)
                {
                    
                    if (walletModel.getBalance() == 0)
                    {//余额不足
                        toastDialog(WalletApplyMainActivity.this, R.string.tv_push_not_enough, null);
                        return;
                    }
                }
                
                showPage(PushMoneyActivity.class);
            }
        });
        
        ly_pro.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                
                DynModel dynModel = (DynModel)SharedPreUtile.readProduct("dynModel" + ApiConstant.bankCode);
                if (dynModel != null && !StringUtil.isEmptyOrNull(dynModel.getEleAccountLink()))
                {
                    ContentTextActivity.startActivity(WalletApplyMainActivity.this,
                        dynModel.getEleAccountLink(),
                        R.string.tx_wallet_protocol);
                    
                }
                else
                {
                    
                    ContentTextActivity.startActivity(WalletApplyMainActivity.this, ApiConstant.BASE_URL_PORT
                        + "spay/agreement", R.string.tx_wallet_protocol);
                }
                
            }
        });
    }
    
    private void initView()
    {
        tv_error = getViewById(R.id.tv_error);
        bt_reload = getViewById(R.id.bt_reload);
        ly_pro = getViewById(R.id.ly_pro);
        tv_refresh = getViewById(R.id.tv_refresh);
        cb_box = getViewById(R.id.cb_box);
        btn_open = getViewById(R.id.btn_open);
        btn_out = getViewById(R.id.btn_out);
        ly_push = getViewById(R.id.ly_push);
        rl_top = getViewById(R.id.rl_top);
        ly_bottom = getViewById(R.id.ly_bottom);
        tv_balance = getViewById(R.id.tv_balance);
        pro_bar = getViewById(R.id.pro_bar);
        ly_error = getViewById(R.id.ly_error);
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        
        titleBar.setTitle(R.string.tv_tab_wallet);
        titleBar.setLeftButtonIsVisible(false);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                try
                {
                    StatService.trackCustomEvent(WalletApplyMainActivity.this, "SPConstTapBalanceListButton", "钱包流水");
                }
                catch (Exception e)
                {
                }
                //流水
                showPage(WalletPushStreamActivity.class);
            }
        });
    }
}
