/*
 * 文 件 名:  BillMainActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-8
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.bill;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.viewpage.NewPage;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.StringUtil;

import com.fourmob.datetimepicker.date.AccessibleDateAnimator;
import com.fourmob.datetimepicker.date.DatePickerController;
import com.fourmob.datetimepicker.date.DatePickerDialog.OnDateChangedListener;
import com.fourmob.datetimepicker.date.DayPickerView;
import com.fourmob.datetimepicker.date.SimpleMonthAdapter;
import com.fourmob.datetimepicker.date.SimpleMonthAdapter.CalendarDay;

/**
 * 日期选择
 *
 * @author he_hui
 * @version [版本号, 2016-11-8]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class DateChoiceActivity extends TemplateActivity {
    private LinearLayout ly_start, ly_end;

    private NewPage page;

    private View firstView, secondView;

    private View[] views = new View[2];

    private AccessibleDateAnimator animator;

    private DayPickerView mDayPickerView;

    private AccessibleDateAnimator animator1;

    private final Calendar mCalendar = Calendar.getInstance();

    private final Calendar mCalendar1 = Calendar.getInstance();

    private int mWeekStart = mCalendar.getFirstDayOfWeek();

    //    private int mMaxYear = 2037;
    //    
    //    private int mMinYear = 1902;

    private int mMaxYear = 2021;

    private int mMinYear = 2016;

    private int currentYear = mCalendar.get(Calendar.YEAR);

    private int currentMonth = mCalendar.get(Calendar.MONTH);

    private TextView tv_choice_start_time, tv_choice_end_time, tv_right_end, tv_left_end, tv_right_start, tv_left_start;

    private TextView tv_end_info, tv_start_info;

    private View v_start, v_end;

    private String startTime, endTime;

    private long sTime, eTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_choice);
        page = getViewById(R.id.pager);
        initView();

        setLiseter();

        startTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 00:00:00";
        endTime = DateUtil.formatYYMD(System.currentTimeMillis()) + " 23:59:59";
    }

    void setLeftViewBg() {
        tv_right_start.setTextColor(getResources().getColor(R.color.title_bg_new));
        tv_left_start.setTextColor(getResources().getColor(R.color.title_bg_new));
        tv_start_info.setTextColor(getResources().getColor(R.color.title_bg_new));
        tv_choice_start_time.setTextColor(getResources().getColor(R.color.title_bg_new));
        tv_right_start.setText(")");
        tv_left_start.setText("(");
        v_start.setVisibility(View.VISIBLE);
        v_end.setVisibility(View.GONE);
    }

    void setRightViewBg() {
        tv_right_end.setTextColor(getResources().getColor(R.color.title_bg_new));
        tv_left_end.setTextColor(getResources().getColor(R.color.title_bg_new));
        tv_end_info.setTextColor(getResources().getColor(R.color.title_bg_new));
        tv_choice_end_time.setTextColor(getResources().getColor(R.color.title_bg_new));
        if (!StringUtil.isEmptyOrNull(tv_choice_end_time.getText().toString())) {
            tv_right_end.setText(")");
            tv_left_end.setText("(");
        } else {
            tv_choice_end_time.setText(DateUtil.formatMD(System.currentTimeMillis()));
        }
        v_start.setVisibility(View.GONE);
        v_end.setVisibility(View.VISIBLE);
    }

    private void setLiseter() {
        ly_start.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                page.setCurrentItem(0);
            }
        });

        ly_end.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!StringUtil.isEmptyOrNull(tv_choice_start_time.getText().toString())) {
                    //                    tv_right_end.setTextColor(getResources().getColor(R.color.user_text_color));
                    //                    tv_left_end.setTextColor(getResources().getColor(R.color.user_text_color));
                    //                    tv_end_info.setTextColor(getResources().getColor(R.color.user_text_color));
                    //                    tv_choice_end_time.setTextColor(getResources().getColor(R.color.title_bg_new));
                    page.setCurrentItem(1);
                }
            }
        });

        page.setOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0: //汇总
                        setLeftViewBg();
                        if (!StringUtil.isEmptyOrNull(tv_choice_end_time.getText().toString())) {
                            tv_right_end.setTextColor(getResources().getColor(R.color.user_text_color));
                            tv_left_end.setTextColor(getResources().getColor(R.color.user_text_color));
                            tv_end_info.setTextColor(getResources().getColor(R.color.user_text_color));
                            tv_choice_end_time.setTextColor(getResources().getColor(R.color.title_bg_new));
                        }
                        break;
                    case 1://走势
                        setRightViewBg();
                        tv_right_start.setTextColor(getResources().getColor(R.color.user_text_color));
                        tv_left_start.setTextColor(getResources().getColor(R.color.user_text_color));
                        tv_start_info.setTextColor(getResources().getColor(R.color.user_text_color));
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });
    }

    private void initView() {
        v_start = getViewById(R.id.v_start);
        v_end = getViewById(R.id.v_end);
        ly_start = getViewById(R.id.ly_start);
        ly_end = getViewById(R.id.ly_end);
        tv_choice_start_time = getViewById(R.id.tv_choice_start_time);

        tv_choice_end_time = getViewById(R.id.tv_choice_end_time);
        tv_left_end = getViewById(R.id.tv_left_end);
        tv_right_end = getViewById(R.id.tv_right_end);
        tv_right_start = getViewById(R.id.tv_right_start);
        tv_left_start = getViewById(R.id.tv_left_start);
        tv_end_info = getViewById(R.id.tv_end_info);
        tv_start_info = getViewById(R.id.tv_start_info);

        LayoutInflater inflater = LayoutInflater.from(this);
        firstView = inflater.inflate(R.layout.date_time_pick1, null);
        views[0] = firstView;
        animator = (AccessibleDateAnimator) firstView.findViewById(R.id.animator);
        mDayPickerView = new DayPickerView(DateChoiceActivity.this, new DatePickerControllerImp(), null);

        mCalendar.set(Calendar.YEAR, Calendar.YEAR);
        mCalendar.set(Calendar.MONTH, Calendar.MONTH);
        mCalendar.set(Calendar.DAY_OF_MONTH, Calendar.DAY_OF_MONTH);

        animator.addView(mDayPickerView);
        animator.setDateMillis(mCalendar.getTimeInMillis());
        secondView = inflater.inflate(R.layout.date_time_pick2, null);
        views[1] = secondView;
        animator1 = (AccessibleDateAnimator) secondView.findViewById(R.id.animator1);

        Calendar c = Calendar.getInstance();

        loadSeondDayPick(c);

        page.setAdapter(new MyAdapter());
        page.setCurrentItem(0);
        //        page.setScrollble(false);

        tv_choice_start_time.setText(DateUtil.formatMD(System.currentTimeMillis()));
        tv_choice_start_time.setTextColor(getResources().getColor(R.color.title_bg_new));
        tv_right_start.setTextColor(getResources().getColor(R.color.title_bg_new));
        tv_right_start.setText(")");
        tv_left_start.setText("(");
        tv_left_start.setTextColor(getResources().getColor(R.color.title_bg_new));
        tv_start_info.setTextColor(getResources().getColor(R.color.title_bg_new));

    }

    class DatePickerControllerImp implements DatePickerController {

        @Override
        public int getFirstDayOfWeek() {
            return mWeekStart;
        }

        @Override
        public int getMaxYear() {
            return mMaxYear;
        }

        @Override
        public int getMinYear() {
            return mMinYear;
        }

        @Override
        public CalendarDay getSelectedDay() {
            return new SimpleMonthAdapter.CalendarDay(mCalendar);
        }

        @Override
        public void onDayOfMonthSelected(int year, int month, int day) {
            if (day <= 9) {
                tv_choice_start_time.setText((month + 1) + "-0" + day);

            } else {
                tv_choice_start_time.setText((month + 1) + "-" + day);
            }
            tv_choice_start_time.setTextColor(getResources().getColor(R.color.title_bg_new));
            page.setScrollble(true);
            tv_right_start.setTextColor(getResources().getColor(R.color.title_bg_new));
            tv_right_start.setText(")");
            tv_left_start.setText("(");
            tv_left_start.setTextColor(getResources().getColor(R.color.title_bg_new));
            tv_start_info.setTextColor(getResources().getColor(R.color.title_bg_new));
            page.setCurrentItem(1);
            v_start.setVisibility(View.GONE);
            v_end.setVisibility(View.VISIBLE);

            Calendar c = Calendar.getInstance();
            c.set(year, month, day);
            try {
                startTime = DateUtil.formartDateToYYMMDD(year + "-" + (month + 1) + "-" + day + " 00:00:00") + " 00:00:00";
            } catch (ParseException e) {
                e.printStackTrace();

            }
            //            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //            Logger.i("hehui", "onDayOfMonthSelected-->" + df.format(c.getTimeInMillis()));
            loadSeondDayPick(c);
        }

        @Override
        public void onYearSelected(int year) {

        }

        @Override
        public void tryVibrate() {

        }

        @Override
        public void registerOnDateChangedListener(OnDateChangedListener onDateChangedListener) {

        }

        @Override
        public void registerOnDateChangedListener(com.fourmob.datetimepicker.test.MainTest.OnDateChangedListener onDateChangedListener) {

        }

        @Override
        public int getCurrentYear() {
            // TODO Auto-generated method stub
            return currentYear;
        }

        @Override
        public int getCurrentMonth() {
            // TODO Auto-generated method stub
            return currentMonth;
        }

    }

    class DatePickerControllerImp2 implements DatePickerController {
        Calendar calendar;

        public DatePickerControllerImp2(Calendar calendar) {
            this.calendar = calendar;
        }

        @Override
        public int getFirstDayOfWeek() {
            return mWeekStart;
        }

        @Override
        public int getMaxYear() {
            return mMaxYear;
        }

        @Override
        public int getMinYear() {
            return mMinYear;
        }

        @Override
        public CalendarDay getSelectedDay() {
            return new SimpleMonthAdapter.CalendarDay(calendar);
        }

        @Override
        public void onDayOfMonthSelected(int year, int month, int day) {
            if (day <= 9) {
                tv_choice_end_time.setText((month + 1) + "-0" + day);

            } else {
                tv_choice_end_time.setText((month + 1) + "-" + day);
            }
            try {
                //                endTime = year + "-" + (month + 1) + "-" + day + " 23:59:59";
                endTime = DateUtil.formartDateToYYMMDD(year + "-" + (month + 1) + "-" + day + " 23:59:59") + " 23:59:59";
            } catch (ParseException e) {
                e.printStackTrace();

            }
            //            Logger.i("hehui", ",y-->" + year + ",m-->" + month + ",d-->" + day);
            setRightViewBg();

        }

        @Override
        public void onYearSelected(int year) {

        }

        @Override
        public void tryVibrate() {

        }

        @Override
        public void registerOnDateChangedListener(OnDateChangedListener onDateChangedListener) {

        }

        @Override
        public void registerOnDateChangedListener(com.fourmob.datetimepicker.test.MainTest.OnDateChangedListener onDateChangedListener) {

        }

        @Override
        public int getCurrentYear() {
            // TODO Auto-generated method stub
            return currentYear;
        }

        @Override
        public int getCurrentMonth() {
            // TODO Auto-generated method stub
            return currentMonth;
        }

    }

    void loadSeondDayPick(Calendar c) {
        animator1.removeAllViews();
        DayPickerView mDayPickerView1 = new DayPickerView(DateChoiceActivity.this, new DatePickerControllerImp2(c), c);

        mCalendar1.set(Calendar.YEAR, Calendar.YEAR);
        mCalendar1.set(Calendar.MONTH, Calendar.MONTH);
        mCalendar1.set(Calendar.DAY_OF_MONTH, Calendar.DAY_OF_MONTH);
        animator1.addView(mDayPickerView1);
        animator1.setDateMillis(c.getTimeInMillis());

    }

    public class MyAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            if (views.length < 3) {
                return views.length;
            } else {
                return Integer.MAX_VALUE;
            }
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public void destroyItem(View container, int position, Object object) {
            //            ((ViewPager)container).removeView(mImageViews[position % mImageViews.length]);

        }

        /**
         * 载入图片进去，用当前的position 除以 图片数组长度取余数是关键
         */
        @Override
        public Object instantiateItem(View container, int position) {
            try {

                ((ViewPager) container).addView(views[position % views.length], 0);
            } catch (Exception e) {
            }
            return views[position % views.length];
        }

        @Override
        public void finishUpdate(View arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {
            // TODO Auto-generated method stub

        }

        @Override
        public Parcelable saveState() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void startUpdate(View arg0) {
            // TODO Auto-generated method stub

        }

    }

    @Override
    protected void setupTitleBar() {
        // TODO Auto-generated method stub
        super.setupTitleBar();

        titleBar.setTitle(R.string.tv_date_info);
        titleBar.setLeftButtonIsVisible(true);
        titleBar.setLeftButtonVisible(true);
        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.btnOk));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
                //                if (!StringUtil.isEmptyOrNull(startTime) && !StringUtil.isEmptyOrNull(endTime))
                //                {
                //                    String time = startTime + "|" + endTime;
                //                    HandlerManager.notifyMessage(HandlerManager.BILL_CHOICE_USER, HandlerManager.BILL_CHOICE_TIME, time);
                //                }
            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButLayClick() {
                //确定
                if (StringUtil.isEmptyOrNull(startTime)) {
                    toastDialog(DateChoiceActivity.this, R.string.tv_time_start, null);
                    return;
                }
                if (StringUtil.isEmptyOrNull(endTime)) {
                    toastDialog(DateChoiceActivity.this, R.string.tv_time_end, null);
                    return;
                }
                if (!StringUtil.isEmptyOrNull(startTime) && !StringUtil.isEmptyOrNull(endTime)) {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date startDate;
                    try {
                        startDate = df.parse(startTime);
                        Date endDate = df.parse(endTime);
                        if (startDate.getTime() >= endDate.getTime()) {
                            toastDialog(DateChoiceActivity.this, R.string.show_endtime_than_starttime, null);
                            return;
                        }
                        String time = startTime + "|" + endTime;
                        HandlerManager.notifyMessage(HandlerManager.BILL_CHOICE_USER, HandlerManager.BILL_CHOICE_TIME, time);
                        DateChoiceActivity.this.finish();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
    }
}
