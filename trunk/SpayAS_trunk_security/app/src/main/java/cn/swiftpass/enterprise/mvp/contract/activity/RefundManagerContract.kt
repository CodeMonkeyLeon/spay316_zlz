package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.bussiness.model.OrderSearchResult
import cn.swiftpass.enterprise.bussiness.model.UserModelList
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView


/**
 * @author lizheng.zhao
 * @date 2022/11/23
 *
 * @你没有如期归来
 * @而这正是离别的意义
 */
class RefundManagerContract {

    interface View : BaseView {

        fun cashierDeleteSuccess(
            response: Boolean,
            position: Int
        )

        fun cashierDeleteFailed(error: Any?)

        fun queryCashierSuccess(
            response: UserModelList?,
            isFirst: Boolean,
            isLoadCashier: Boolean
        )

        fun queryCashierFailed(error: Any?)


        fun queryUserRefundOrderSuccess(
            response: ArrayList<Order>?,
            orderNo: String?
        )

        fun queryUserRefundOrderFailed(error: Any?)

        fun queryRefundOrderSuccess(
            response: ArrayList<Order>?,
            orderNo: String?
        )

        fun queryRefundOrderFailed(error: Any?)


        fun queryOrderDetailSuccess(response: Order?)

        fun queryOrderDetailFailed(error: Any?)

        fun queryRefundDetailSuccess(response: Order?)

        fun queryRefundDetailFailed(error: Any?)


        fun queryOrderDataSuccess(
            response: OrderSearchResult?,
            orderNo: String?
        )

        fun queryOrderDataFailed(error: Any?)
    }

    interface Presenter : BasePresenter<View> {

        fun queryOrderData(
            orderNo: String?,
            currentPage: Int,
            state: Int,
            time: String?,
            orderNoMch: String?
        )

        fun queryRefundDetail(
            orderNo: String?,
            mchId: String?
        )


        fun queryOrderDetail(
            orderNo: String?,
            mchId: String?,
            isMark: Boolean
        )


        fun queryRefundOrder(
            orderNo: String?,
            mchId: String?,
            page: Int
        )


        fun queryUserRefundOrder(
            outTradeNo: String?,
            refundTime: String?,
            mchId: String?,
            refundState: String?,
            page: Int
        )


        fun queryCashier(
            page: Int,
            pageSize: Int,
            input: String?,
            isFirst: Boolean,
            isLoadCashier: Boolean
        )


        fun cashierDelete(
            userId: Long,
            position: Int
        )

    }


}