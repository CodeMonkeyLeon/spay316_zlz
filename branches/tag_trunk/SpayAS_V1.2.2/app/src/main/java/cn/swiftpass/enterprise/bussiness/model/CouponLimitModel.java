package cn.swiftpass.enterprise.bussiness.model;

/**
 * 优惠券限制实体
 * User: Alan
 * Date: 13-12-24
 * Time: 下午7:44
 * To change this template use File | Settings | File Templates.
 */
public class CouponLimitModel {
    public Integer limitId; //id
    public Integer oncePiece; //现金券（特殊限制）一人只限用一次 1：是2：否
    public Integer onceMaxPece; //一次消费只限用多少张
    public Integer useInShops; //是否可以跨分店使用 1：可以 2：不可以
    public String otherLimt; //其他限制条件 文本方式呈现，仅作登记和显示之用
    public String specialDate; //"可用"的特殊日期
    public String disabledDate; //"不可用"日期段
    public String startHour; //"可用"开始时间点
    public String endHour; //"可用"结束时间点
    public String weekDays; //周一、周二…周日 任选一个或多个
    public Integer weekUseType; //选定的星期是否可用1：可用；2：不可用
    public Integer minAmount; //最低消费金额，超过此金额的才可以使用此优惠券
}
