package cn.swiftpass.enterprise.ui.activity.diagnosis.interfaces

interface OnNetworkDiagnosisItemClickListener {

    fun onItemClick(position: Int)


}