package cn.swiftplus.enterprise.printsdk.print.A8.printUtils;

import android.content.Context;

import com.landicorp.android.eptapi.DeviceService;
import com.landicorp.android.eptapi.device.Printer;
import com.landicorp.android.eptapi.exception.RequestException;
import com.landicorp.android.eptapi.utils.QrCode;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by aijingya on 2020/8/25.
 *
 * @Package cn.swiftplus.enterprise.printsdk.print.A8.printUtils
 * @Description:
 * @date 2020/8/25.11:12.
 */
public class PrintA8Text {
    private static PrintA8Text printA8Text;
    private boolean isDeviceServiceLogined = false;
    private Context mContext;
    private Printer mPrinter;
    private Printer.Format format;
    private List<PrintA8Bean> mList_PrintBean = new ArrayList<PrintA8Bean>();

    public static PrintA8Text getInstance(){
        if(printA8Text == null){
            printA8Text = new PrintA8Text() ;
        }
        return printA8Text;
    }

    private Printer.Progress progress = new Printer.Progress() {

        @Override
        public void doPrint(Printer arg0) throws Exception {
        }

        @Override
        public void onFinish(int code) {
            if (code == Printer.ERROR_NONE) {
            }
            /**
             * Has some error. Here is display it, but you may want to hanle the
             * error such as ERROR_OVERHEAT、ERROR_BUSY、ERROR_PAPERENDED to start
             * again in the right time later.
             */
            else {
            }
        }

        @Override
        public void onCrash() {
            bindDeviceService();
        }
    };

    public void bindDeviceService() {
        try {
            isDeviceServiceLogined = false;
            DeviceService.login(mContext);
            isDeviceServiceLogined = true;
        } catch (Exception e) {
        }
    }

    public void unbindDeviceService() {
        DeviceService.logout();
        this.isDeviceServiceLogined = false;
    }

    /**
     * Search card and show all track info
     */
    public void startPrint() {
        try {
            progress.start();
        } catch (RequestException e) {
            bindDeviceService();
        }
    }


   public void init(Context context){
       this.mContext = context;
       progress.addStep(new Printer.Step() {
           @Override
           public void doPrint(Printer printer) throws Exception {
               mPrinter = printer;
               mPrinter.setAutoTrunc(true);

               mPrinter.setMode(Printer.MODE_VIRTUAL);

               //根据打印需求列表，去映射具体的打印信息
               for(int i = 0 ;i < mList_PrintBean.size() ; i++){
                    getPrintFunction(mList_PrintBean.get(i).getFunction_name(),mList_PrintBean.get(i).getPrint_text());
               }
               mPrinter.printText("\r\n");

               mList_PrintBean.clear();
           }
       });
   }

    private void getPrintFunction(PrintA8Bean.PRINT_FUNCTION_NAME function_name,String content){
        switch (function_name){
            case TITLE:
                printTitle(content);
                break;
            case TEXT_LINE_LEFT:
                printTextLine(Printer.Alignment.LEFT,content);
                break;
            case TEXT_LINE_RIGHT:
                printTextLine(Printer.Alignment.RIGHT,content);
                break;
            case TEXT_LINE_CENTER:
                printTextLine(Printer.Alignment.CENTER,content);
                break;
            case TEXT_MULTI_LINES:
                printMultiLines(content);
                break;
            case SINGLE_LINE:
                printSingleLine();
                break;
            case DOUBLE_LINE:
                printDoubleLine();
                break;
            case EMPTY_LINE:
                printEmptyLine();
                break;
            case QR_CODE:
                printQRCode(content);
                break;

        }
   }

   public void printInfo(PrintA8Bean.PRINT_FUNCTION_NAME function_name, String content){
        PrintA8Bean bean = new PrintA8Bean();

        bean.setFunction_name(function_name);
        bean.setPrint_text(content);

        mList_PrintBean.add(bean);
   }

    private void printTitle(String text){
        try {
            format = new Printer.Format();
            format.setAscSize(Printer.Format.ASC_DOT24x12);
            format.setAscScale(Printer.Format.ASC_SC1x2);
            format.setHzSize(Printer.Format.HZ_DOT24x24);
            format.setHzScale(Printer.Format.HZ_SC1x2);
            mPrinter.setFormat(format);
            // 标题
            mPrinter.printText(Printer.Alignment.CENTER,text+ "\n");
            mPrinter.printText("\r\n");

        }catch (Exception e){

        }
    }

    private void printTextLine(Printer.Alignment align,String text){
        try {
            format.setAscSize(Printer.Format.ASC_DOT24x12);
            format.setAscScale(Printer.Format.ASC_SC1x1);
            format.setHzSize(Printer.Format.HZ_DOT24x24);
            format.setHzScale(Printer.Format.HZ_SC1x1);
            mPrinter.setFormat(format);

            mPrinter.printText(align,text+"\n");

        }catch (Exception e){

        }
    }

    public void printMultiLines(String text){
        try {
            mPrinter.printText(text+"\n");

        }catch (Exception e){

        }
    }

    private void printSingleLine(){
        try {
            mPrinter.printText("-------------------------------\n");
        }catch (Exception e){

        }
    }

    private void printDoubleLine(){
        try {
            mPrinter.printText("===============================\n");
        }catch (Exception e){
        }
    }

    private void printQRCode(String orderNoMch){
        try {
            mPrinter.printQrCode(Printer.Alignment.CENTER, new QrCode(orderNoMch, QrCode.ECLEVEL_Q), 184);
        }catch (Exception e){
        }
    }

    private void printEmptyLine(){
        try {
            mPrinter.printText(" "+"\r\n");
        }catch (Exception e){

        }
    }

}
