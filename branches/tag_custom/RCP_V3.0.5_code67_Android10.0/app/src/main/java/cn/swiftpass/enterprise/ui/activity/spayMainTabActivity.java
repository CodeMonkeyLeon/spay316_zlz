package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.tencent.stat.MtaSDkException;
import com.tencent.stat.StatConfig;
import com.tencent.stat.StatReportStrategy;
import com.tencent.stat.StatService;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.Locale;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.bill.OrderSearchActivity;
import cn.swiftpass.enterprise.ui.activity.bill.ReportActivity;
import cn.swiftpass.enterprise.ui.activity.print.BluePrintUtil;
import cn.swiftpass.enterprise.ui.activity.scan.CaptureActivity;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabBill;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabPay;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabReport;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabSetting;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabSummary;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.MyRadioButton;
import cn.swiftpass.enterprise.ui.widget.MyToast;
import cn.swiftpass.enterprise.ui.widget.SpayTitleView;
import cn.swiftpass.enterprise.ui.widget.dialog.ChangePreauthDialog;
import cn.swiftpass.enterprise.utils.DataReportUtils;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

import static cn.swiftpass.enterprise.MainApplication.getContext;

/**
 * Created by aijingya on 2018/4/26.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(新的MainTab页面)
 * @date 2018/4/26.14:32.
 */

public class spayMainTabActivity extends FragmentActivity {
    private static final String TAG = spayMainTabActivity.class.getSimpleName();

    private Context mContext;

    SpayTitleView spay_title_view;
    private FrameLayout mContent_view;
    private RadioGroup mRadioGroup_bottom_view;
    private MyRadioButton mHomePay;
    private MyRadioButton mHomeBill;
    private MyRadioButton mHomeSummary;
    private MyRadioButton mHomeSetting;
    private Fragment FragmentPay;
    private Fragment FragmentBill;
    private Fragment FragmentSummary;
    private Fragment FragmentSetting;
    private Fragment FragmentReport;

    private ChangePreauthDialog changePreauthDialog;
    private ChangePreauthDialog changeBillPreauthDialog;
    private String saleOrPreauth;          //上一次选择的是sale 还是 pre_auth,默认是sale
    private String bill_sale_or_Pre_auth; //订单列表页面，选择是收银还是预授权订单
    private int tab_index = 0 ;           //tab_index ,默认是第0个tab

    private int tabIds[] = new int[]{
            R.id.id_tab_pay,
            R.id.id_tab_bill,
            R.id.id_tab_summary,
            R.id.id_tab_setting
    };

    boolean isOpen = true, isClose = true, isOpening = true, isCloseing = true;
    /**
     * 连接默认上一次
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void connentBlue() {
        boolean connState = PreferenceUtil.getBoolean("connState" + ApiConstant.bankCode, true);
        if (connState && !StringUtil.isEmptyOrNull(MainApplication.getBlueDeviceAddress())) {
            //异步去连接蓝牙
            new Thread(new Runnable() {
                @Override
                public void run() {
                    boolean isConnect = BluePrintUtil.blueConnent(MainApplication.getBlueDeviceAddress());
                    Logger.i(TAG, " connentBlue isConnect-->" + isConnect);
                    if (!isConnect) {
                        MainApplication.setBlueState(false);
                        //                        MainApplication.bluetoothSocket = null;
                        //                        MainApplication.setBlueDeviceName("");
                        //                        MainApplication.setBlueDeviceNameAddress("");
                        //                        PreferenceUtil.commitBoolean("connState" + MainApplication.getMchId(), false);
                        HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET, HandlerManager.BLUE_CONNET);
                    } else {
                        MainApplication.setBlueState(true);
                    }
                }
            }).start();
        }
    }

    protected void switchLanguage(String language) {
        //设置应用语言类型
        Resources resources = getResources();
        Configuration config = resources.getConfiguration();
        DisplayMetrics dm = resources.getDisplayMetrics();

        if (!TextUtils.isEmpty(language) && language.equals(MainApplication.LANG_CODE_ZH_TW)) // 繁体
        {
            config.locale = Locale.TRADITIONAL_CHINESE;
        } else if (!TextUtils.isEmpty(language) && language.equals(MainApplication.LANG_CODE_EN_US)) {
            config.locale = Locale.ENGLISH;
        } else if (!TextUtils.isEmpty(language) && language.equals(MainApplication.LANG_CODE_ZH_CN)) {
            config.locale = Locale.SIMPLIFIED_CHINESE;
        }else if(!TextUtils.isEmpty(language) && language.equals(MainApplication.LANG_CODE_JA_JP)){
            config.locale = Locale.JAPAN;
        }else { // 跟随系统
            config.locale = Locale.getDefault(); //默认英文
        }
        Logger.i("hehui", "Locale.getDefault()-->" + Locale.getDefault());
        if (!StringUtil.isEmptyOrNull(language)) {
            PreferenceUtil.commitString("language", language);
        } else {
            Locale locale = getResources().getConfiguration().locale;
            String lan = locale.getLanguage() + "-" + locale.getCountry();
//            String lan = getResources().getConfiguration().locale.getCountry();//英文 返回为空  中文 CN TW
//            String lan = Locale.getDefault().toString();
            Logger.i("hehui", "lan-->" + lan);//  zh-HK  en-HK   zh-CN   en-
            if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_NEW) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_HANS)) {
                //                PreferenceUtil.commitString("language", MainApplication.LANG_CODE_ZH_CN);
                config.locale = Locale.SIMPLIFIED_CHINESE;
            } else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_NEW) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO_NEW) ||
                    lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW_NEW)|| lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_HANT)) {
                Logger.i("hehui", "LANG_CODE_ZH_HK-->");
                config.locale = Locale.TRADITIONAL_CHINESE;
            }else if(lan.equalsIgnoreCase(MainApplication.LANG_CODE_JA_JP_NEW)){//日语
                config.locale = Locale.JAPAN;
            }  else {
                config.locale = Locale.ENGLISH;
            }
        }
        resources.updateConfiguration(config, dm);
    }

    /**
     * 根据不同的模式，建议设置的开关状态，可根据实际情况调整，仅供参考。
     *
     * @param isDebugMode 根据调试或发布条件，配置对应的MTA配置
     */
    private void initMTAConfig(boolean isDebugMode) {
        if (isDebugMode) { // 调试时建议设置的开关状态
            // 查看MTA日志及上报数据内容
            StatConfig.setDebugEnable(true);
            // 禁用MTA对app未处理异常的捕获，方便开发者调试时，及时获知详细错误信息。
            StatConfig.setAutoExceptionCaught(false);
            // StatConfig.setEnableSmartReporting(false);
            // Thread.setDefaultUncaughtExceptionHandler(new
            // UncaughtExceptionHandler() {
            //
            // @Override
            // public void uncaughtException(Thread thread, Throwable ex) {
            // logger.error("setDefaultUncaughtExceptionHandler");
            // }
            // });
            // 调试时，使用实时发送
            // StatConfig.setStatSendStrategy(StatReportStrategy.BATCH);
            // // 是否按顺序上报
            // StatConfig.setReportEventsByOrder(false);
            // // 缓存在内存的buffer日志数量,达到这个数量时会被写入db
            // StatConfig.setNumEventsCachedInMemory(30);
            // // 缓存在内存的buffer定期写入的周期
            // StatConfig.setFlushDBSpaceMS(10 * 1000);
            // // 如果用户退出后台，记得调用以下接口，将buffer写入db
            // StatService.flushDataToDB(getApplicationContext());

            // StatConfig.setEnableSmartReporting(false);
            // StatConfig.setSendPeriodMinutes(1);
            // StatConfig.setStatSendStrategy(StatReportStrategy.PERIOD);
        } else { // 发布时，建议设置的开关状态，请确保以下开关是否设置合理
            // 禁止MTA打印日志
            StatConfig.setDebugEnable(false);
            // 根据情况，决定是否开启MTA对app未处理异常的捕获
            StatConfig.setAutoExceptionCaught(true);
            StatConfig.setInstallChannel(ApiConstant.Channel);
            // 选择默认的上报策略
            StatConfig.setStatSendStrategy(StatReportStrategy.APP_LAUNCH);
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //初始化PreferenceUtil
        PreferenceUtil.init(this);
        //根据上次的语言设置，重新设置语言
        switchLanguage(PreferenceUtil.getString("language", ""));
        //group = this;

        mContext = getContext();
        getContext().setLastActivityRef(new WeakReference<Activity>(this));
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        StatConfig.setAutoExceptionCaught(true);
        // 腾讯云 统计
        initMTAConfig(false);

        MainApplication.allActivities.add(this);
        // 腾讯云 启动mta
        try {
            // 第三个参数必须为：com.tencent.stat.common.StatConstants.VERSION
            StatService.startStatService(this, BuildConfig.appKey, com.tencent.stat.common.StatConstants.VERSION);
        } catch (MtaSDkException e) {
            // MTA初始化失败
            Log.e(TAG, Log.getStackTraceString(e));
        }

        setContentView(R.layout.activity_main_tab);
//        initData();
        initViews();
        initEvents();

        registerBlueReceiver();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        //从intent对象中把封装好的数据取出来
        String billTab = intent.getStringExtra("tab_index");
        if(billTab != null && billTab.equals("bill")){//如果点击的是要进入账单页面
            setSelection(1);
            tab_index = 1;
        }else{//否则默认进入首页
            setSelection(0);
            tab_index = 0;
        }

    }

    /*private void initData() {
        mList.add(new DrawerContentModel(R.drawable.icon_fixedqrcode, getString(R.string.drawer_fixed_code), 1));
        mList.add(new DrawerContentModel(R.drawable.icon_management,  getString(R.string.drawer_managerment), 2));
        mList.add(new DrawerContentModel(R.drawable.icon_support,  getString(R.string.drawer_Support), 3));
        mList.add(new DrawerContentModel(R.drawable.icon_setting,  getString(R.string.drawer_setting), 4));
    }*/

    //初始化控件
    public void initViews(){
        spay_title_view = findViewById(R.id.spay_title_view);
        mContent_view = findViewById(R.id.fl_content);
        mRadioGroup_bottom_view = findViewById(R.id.rg_bottom_view);

        mHomePay = findViewById(R.id.id_tab_pay);
        mHomeBill = findViewById(R.id.id_tab_bill);
        mHomeSummary = findViewById(R.id.id_tab_summary);
        mHomeSetting = findViewById(R.id.id_tab_setting);

        if(MainApplication.isAdmin.equals("1")){//如果是商户登录，则显示报表的tab
            mHomeSummary.setText(R.string.report_new_report);
            Drawable drawable = getResources().getDrawable(R.drawable.icon_tab_report);
            float scale = getResources().getDisplayMetrics().density;// 屏幕密度
            drawable.setBounds(0, 0, (int) (30 * scale + 0.5f), (int) (30 * scale + 0.5f));//30,30为宽高
            mHomeSummary.setCompoundDrawables(null, drawable, null, null);
        }else if(MainApplication.isAdmin.equals("0")){//如果是收银员登录，则显示原来的汇总的tab
            mHomeSummary.setText(R.string.tab_sum_title);
            Drawable drawable = getResources().getDrawable(R.drawable.icon_tab_summary);
            float scale = getResources().getDisplayMetrics().density;// 屏幕密度
            drawable.setBounds(0, 0, (int) (30 * scale + 0.5f), (int) (30 * scale + 0.5f));//30,30为宽高
            mHomeSummary.setCompoundDrawables(null, drawable, null, null);
        }
    }

    @Override
    public void onAttachFragment(Fragment fragment){
        //当前的界面的保存状态，只是从新让新的Fragment指向了原本未被销毁的fragment，它就是onAttach方法对应的Fragment对象
        if(FragmentPay == null && fragment instanceof FragmentTabPay){
            FragmentPay = fragment;
        }else if(FragmentBill == null && fragment instanceof FragmentTabBill){
            FragmentBill = fragment;
        }else if(FragmentSummary == null && fragment instanceof FragmentTabSummary){
            FragmentSummary = fragment;
        }else if(FragmentSetting == null && fragment instanceof FragmentTabSetting){
            FragmentSetting = fragment;
        }else if(FragmentReport == null && fragment instanceof FragmentTabReport){
            FragmentReport =  fragment;
        }
    }

    private void registerBlueReceiver() {
        IntentFilter filterBlue = new IntentFilter();
        filterBlue.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filterBlue.addAction(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
        try {
            registerReceiver(blueReceiver, filterBlue);
        } catch (Exception e) {
            Logger.e("hehui", "" + e);
        }
    }

    public SpayTitleView getTitleView() {
        return spay_title_view;
    }

    public void cleanTitleAllView() {
        if (spay_title_view != null){
            spay_title_view.cleanAllView();
        }
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        Log.d(TAG,"onDestroy");
        if (blueReceiver != null) {
            unregisterReceiver(blueReceiver);
        }

        // 通知释放空间
        //System.gc();
        Runtime.getRuntime().gc();

        android.os.Debug.stopMethodTracing();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        Log.d(TAG,"onPause");
        StatService.onPause(this);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        Log.d(TAG,"onResume");

        StatService.onResume(this);
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        Log.d(TAG, "onStop");
    }


    public void initEvents(){
        mRadioGroup_bottom_view.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                try {
                    StatService.trackCustomEvent(mContext, "kMTASPayPayTabbar", "收款一级菜单切换");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                // 1.携带参数的打点
                Bundle values = new Bundle();
                values.putString("kGFASPayPayTabbar","收款一级菜单”收款“切换");
                DataReportUtils.getInstance().report("kGFASPayPayTabbar",values);

                for (int i = 0; i < tabIds.length; i++) {
                    if (tabIds[i] == checkedId) {
                        setSelection(i);
                        tab_index = i;
                        break;
                    }
                }
            }
        });

        Intent intent = getIntent();
        //从intent对象中把封装好的数据取出来
        String billTab = intent.getStringExtra("tab_index");
        if(billTab != null && billTab.equals("bill")){//如果点击的是要进入账单页面
            setSelection(1);
            tab_index = 1;
        }else{
            setSelection(0);
            tab_index = 0;
        }


    }

    private void setSelection(int position){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction mfragmentTransaction = fm.beginTransaction();
        hideAllFragments(mfragmentTransaction);

        switch (position){
            case 0:
                mHomePay.setSelected(true);
                mHomePay.setTextColor(getResources().getColor(R.color.bg_text_new));
                initPayTitle();

                if (FragmentPay == null) {
                    FragmentPay = new FragmentTabPay();
                    mfragmentTransaction.add(R.id.fl_content, FragmentPay);
                }else {
                    mfragmentTransaction.show(FragmentPay);
                }
                break;
            case 1:
                mHomeBill.setSelected(true);
                mHomeBill.setTextColor(getResources().getColor(R.color.bg_text_new));
                initBillTitle();

                try {
                    StatService.trackCustomEvent(getApplicationContext(), "kMTASPayBillTabbar", "收款一级菜单”流水“切换");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                // 1.携带参数的打点
                Bundle values = new Bundle();
                values.putString("kGFASPayBillTabbar","收款一级菜单”流水“切换");
                DataReportUtils.getInstance().report("kGFASPayBillTabbar",values);

                // 如果是收银登录进来
                if(MainApplication.isAdmin.equals("0")){
                    clickBillTab(mfragmentTransaction);
                }else{
                    if (!MainApplication.remark.equals("") && !MainApplication.remark.equals("1")) {
                        MyToast toast = new MyToast();
                        toast.showToast(getApplicationContext(), ToastHelper.toStr(R.string.show_mch_stream));
                    } else if (MainApplication.remark.equals("0")) {
                        if (!MainApplication.isOrderAuth.equals("") && MainApplication.isOrderAuth.equals("1")) {
                            clickBillTab(mfragmentTransaction);
                        }
                    } else {
                        clickBillTab(mfragmentTransaction);
                    }
                }
                break;
            case 2:
                mHomeSummary.setSelected(true);
                mHomeSummary.setTextColor(getResources().getColor(R.color.bg_text_new));

                try {
                    StatService.trackCustomEvent(mContext, "kMTASPayBillSummary", "汇总入口");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                // 1.携带参数的打点
                Bundle value = new Bundle();
                value.putString("kGFASPayBillSummary","汇总入口");
                DataReportUtils.getInstance().report("kGFASPayBillSummary",value);

                if(MainApplication.isAdmin.equals("1")){//如果是商户登录的，则把汇总的tab换成报表的tab
                    initReportTitle();
                    mHomeSummary.setText(R.string.report_new_report);
                    Drawable drawable = getResources().getDrawable(R.drawable.icon_tab_report);
                    float scale = getResources().getDisplayMetrics().density;// 屏幕密度
                    drawable.setBounds(0, 0, (int) (30 * scale + 0.5f), (int) (30 * scale + 0.5f));//30,30为宽高
                    mHomeSummary.setCompoundDrawables(null, drawable, null, null);
                    if (FragmentReport == null) {
                        FragmentReport = new FragmentTabReport();
                        mfragmentTransaction.add(R.id.fl_content, FragmentReport);
                    }else {
                        mfragmentTransaction.show(FragmentReport);
                    }
                }else if(MainApplication.isAdmin.equals("0")){//如果是收银员登录，则不展示报表的tab，只展示旧的汇总页面
                    initSummaryTitle();
                    mHomeSummary.setText(R.string.tab_sum_title);
                    Drawable drawable = getResources().getDrawable(R.drawable.icon_tab_summary);
                    float scale = getResources().getDisplayMetrics().density;// 屏幕密度
                    drawable.setBounds(0, 0, (int) (30 * scale + 0.5f), (int) (30 * scale + 0.5f));//30,30为宽高
                    mHomeSummary.setCompoundDrawables(null, drawable, null, null);
                    if (FragmentSummary == null) {
                        FragmentSummary = new FragmentTabSummary();
                        mfragmentTransaction.add(R.id.fl_content, FragmentSummary);
                    }else {
                        mfragmentTransaction.show(FragmentSummary);
                    }
                    HandlerManager.notifyMessage(HandlerManager.SWITCH_TAB, HandlerManager.SUMMARY_SWITCH_TAB);
                }
                break;
            case 3:
                try {
                    StatService.trackCustomEvent(getApplicationContext(), "kMTASPayMeTabbar", "我的一级菜单标签栏‘我’切换");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                // 1.携带参数的打点
                Bundle value1 = new Bundle();
                value1.putString("kGFASPayMeTabbar","我的一级菜单标签栏‘我’切换");
                DataReportUtils.getInstance().report("kGFASPayMeTabbar",value1);


                mHomeSetting.setSelected(true);
                mHomeSetting.setTextColor(getResources().getColor(R.color.bg_text_new));
                initSettingTitle();
                if (FragmentSetting == null) {
                    FragmentSetting = new FragmentTabSetting();
                    mfragmentTransaction.add(R.id.fl_content, FragmentSetting);
                }else {
                    mfragmentTransaction.show(FragmentSetting);
                }
                break;
            default:
                break;
        }
        mfragmentTransaction.commit();
    }

    public void initPayTitle(){
        cleanTitleAllView();

        if(MainApplication.isPre_authOpen == 1){ //如果商户已开通预授权才有下面的流程
            //读取上一次选择的是什么状态，然后根据状态设置title，默认是sale
            saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth","sale");
            if(TextUtils.equals(saleOrPreauth,"sale")){
                getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
                getTitleView().addMiddleTextView(getString(R.string.choose_sale));
                getTitleView().addMiddleImageView(R.drawable.icon_select);
                HandlerManager.notifyMessage(HandlerManager.CHOICE_SALE_OR_PRE_AUTH,HandlerManager.CHOICE_SALE);

            }else if(TextUtils.equals(saleOrPreauth,"pre_auth")) {
                getTitleView().setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
                getTitleView().addMiddleTextView(getString(R.string.choose_pre_auth));
                getTitleView().addMiddleImageView(R.drawable.icon_select);
                HandlerManager.notifyMessage(HandlerManager.CHOICE_SALE_OR_PRE_AUTH,HandlerManager.CHOICE_PRE_AUTH);
            }
            getTitleView().OnClickMiddleLayout(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(tab_index == 0){ //收款页面
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                changePreauthDialog = new ChangePreauthDialog(spayMainTabActivity.this, ChangePreauthDialog.PageTypeEnum.PAY,new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        changePreauthDialog.dismiss();
                                        //点击确定的时候保存当前的选择状态
                                        PreferenceUtil.commitString("choose_pay_or_pre_auth", changePreauthDialog.getSelectType());
                                        //更新tabpay的界面的颜色
                                        saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth", "sale");
                                        if(TextUtils.equals(saleOrPreauth,"sale")){
                                            getTitleView().replaceMiddleTextView(0,getString(R.string.choose_sale));
                                            getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
                                            HandlerManager.notifyMessage(HandlerManager.CHOICE_SALE_OR_PRE_AUTH,HandlerManager.CHOICE_SALE);
                                        }else if(TextUtils.equals(saleOrPreauth,"pre_auth")) {
                                            getTitleView().replaceMiddleTextView(0,getString(R.string.choose_pre_auth));
                                            getTitleView().setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
                                            HandlerManager.notifyMessage(HandlerManager.CHOICE_SALE_OR_PRE_AUTH,HandlerManager.CHOICE_PRE_AUTH);
                                        }
                                    }
                                });

                                if(changePreauthDialog != null && !changePreauthDialog.isShowing()){
                                    changePreauthDialog.show();
                                }
                            }
                        });
                    }
                }
            });
        }else { //如果没有开通预授权通道，则显示默认的颜色
            getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
        }
    }

    public void initBillTitle(){
        cleanTitleAllView();

        //如果当前商户开启预授权权限
        if(MainApplication.isPre_authOpen == 1){
            //读取上一次选择的是什么状态，然后根据状态设置title，默认是sale
            bill_sale_or_Pre_auth = PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth", "sale");
            if(TextUtils.equals(bill_sale_or_Pre_auth,"sale")){
                getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
                getTitleView().addMiddleTextView(getString(R.string.choose_sale));
                getTitleView().addMiddleImageView(R.drawable.icon_select);
            }else if(TextUtils.equals(bill_sale_or_Pre_auth,"pre_auth")) {
                getTitleView().setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
                getTitleView().addMiddleTextView(getString(R.string.choose_pre_auth));
                getTitleView().addMiddleImageView(R.drawable.icon_select);
            }
            getTitleView().OnClickMiddleLayout(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(tab_index == 1){//账单页面
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                changeBillPreauthDialog = new ChangePreauthDialog(spayMainTabActivity.this, ChangePreauthDialog.PageTypeEnum.BILL,new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        changeBillPreauthDialog.dismiss();
                                        //点击确定的时候保存当前的选择状态
                                        PreferenceUtil.commitString("bill_list_choose_pay_or_pre_auth", changeBillPreauthDialog.getSelectType());
                                        //更新title的界面的颜色
                                        bill_sale_or_Pre_auth = PreferenceUtil.getString("bill_list_choose_pay_or_pre_auth","sale");
                                        if(TextUtils.equals(bill_sale_or_Pre_auth,"sale")){
                                            getTitleView().replaceMiddleTextView(0,getString(R.string.choose_sale));
                                            getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
                                            HandlerManager.notifyMessage(HandlerManager.BILL_CHOICE_SALE_OR_PRE_AUTH,HandlerManager.BILL_CHOICE_SALE);
                                        }else if(TextUtils.equals(bill_sale_or_Pre_auth,"pre_auth")) {
                                            getTitleView().replaceMiddleTextView(0,getString(R.string.choose_pre_auth));
                                            getTitleView().setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
                                            HandlerManager.notifyMessage(HandlerManager.BILL_CHOICE_SALE_OR_PRE_AUTH,HandlerManager.BILL_CHOICE_PRE_AUTH);
                                        }
                                    }
                                });

                                if(changeBillPreauthDialog != null && !changeBillPreauthDialog.isShowing()){
                                    changeBillPreauthDialog.show();
                                }
                            }
                        });
                    }
                }
            });
        } else{ //如果当前商户没有开启预授权权限
            getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
            getTitleView().addMiddleTextView(getResources().getString(R.string.tab_bill_title));
        }

        getTitleView().addLeftImageView(R.drawable.icon_scan, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CaptureActivity.startActivity(mContext, "pay");
            }
        });

        getTitleView().addRightTextView(getResources().getString(R.string.search_order), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    StatService.trackCustomEvent(mContext, "kMTASPayBillSearch", "搜索入口");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                // 1.携带参数的打点
                Bundle values = new Bundle();
                values.putString("kGFASPayBillSearch","搜索入口");
                DataReportUtils.getInstance().report("kGFASPayBillSearch",values);

                showPage(OrderSearchActivity.class);
            }
        });
    }

    public void initSummaryTitle(){
        cleanTitleAllView();
        getTitleView().addMiddleTextView(getResources().getString(R.string.tab_sum_title));
        getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));

        getTitleView().addRightTextView(getResources().getString(R.string.print), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HandlerManager.notifyMessage(HandlerManager.CLICK_TITLE,HandlerManager.SUNMMARY_PRINT_DATA);

                try {
                    StatService.trackCustomEvent(spayMainTabActivity.this, "kMTASPayBillSummaryPrint", "汇总/打印");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                // 1.携带参数的打点
                Bundle values = new Bundle();
                values.putString("kGFASPayBillSummaryPrint","汇总/打印");
                DataReportUtils.getInstance().report("kGFASPayBillSummaryPrint",values);
            }
        });
    }

    public void initReportTitle(){
        cleanTitleAllView();
        getTitleView().addMiddleTextView(getResources().getString(R.string.report_new_report));
        getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));

        getTitleView().addRightTextView(getResources().getString(R.string.tab_sum_title), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //跳转到汇总页
                Intent intent = new Intent(getContext(),SummaryActivity.class);
                startActivity(intent);
            }
        });
    }


    public void initSettingTitle(){
        cleanTitleAllView();
        getTitleView().addMiddleTextView(getResources().getString(R.string.tab_setting_title));
        getTitleView().setBackgroundColor(getResources().getColor(R.color.app_drawer_title));
    }

    public void showPage(Class clazz) {
        Intent intent = new Intent(mContext, clazz);
        this.startActivity(intent);
    }

    private void clickBillTab(FragmentTransaction mfragmentTransaction){
        if (FragmentBill == null) {
            FragmentBill = new FragmentTabBill();
            mfragmentTransaction.add(R.id.fl_content, FragmentBill);
        }else {
            mfragmentTransaction.show(FragmentBill);
        }
        HandlerManager.notifyMessage(HandlerManager.STREAM_BILL, HandlerManager.PAY_SWITCH_TAB);
    }

    private void hideAllFragments(FragmentTransaction ft){
        if (FragmentPay != null) {
            ft.hide(FragmentPay);
            mHomePay.setSelected(false);
            mHomePay.setTextColor(getResources().getColor(R.color.tab_text_nor));
        }

        if (FragmentBill != null) {
            ft.hide(FragmentBill);
            mHomeBill.setSelected(false);
            mHomeBill.setTextColor(getResources().getColor(R.color.tab_text_nor));
        }

        if(FragmentSummary != null){
            ft.hide(FragmentSummary);
            mHomeSummary.setSelected(false);
            mHomeSummary.setTextColor(getResources().getColor(R.color.tab_text_nor));
        }

        if (FragmentSetting != null) {
            ft.hide(FragmentSetting);
            mHomeSetting.setSelected(false);
            mHomeSetting.setTextColor(getResources().getColor(R.color.tab_text_nor));
        }
        if (FragmentReport != null) {
            ft.hide(FragmentReport);
            mHomeSummary.setSelected(false);
            mHomeSummary.setTextColor(getResources().getColor(R.color.tab_text_nor));
        }
    }

    /**
     * 实时监听蓝牙广播
     */
    private final BroadcastReceiver blueReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
            switch (state) {
                case BluetoothAdapter.STATE_OFF:
                    MainApplication.setBlueState(false);
                    if (isClose) {
                        isClose = false;
                    }
                    MainApplication.bluetoothSocket = null;
                    HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET_STUTS, HandlerManager.BLUE_CONNET_STUTS_CLOSED);
                    break;
                case BluetoothAdapter.STATE_TURNING_OFF:
                    MainApplication.setBlueState(false);
                    MainApplication.bluetoothSocket = null;
                    if (isCloseing) {
                        isCloseing = false;

                    }
                    HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET_STUTS, HandlerManager.BLUE_CONNET_STUTS_CLOSED);
                    break;
                case BluetoothAdapter.STATE_ON:
                    MainApplication.setBlueState(true);
                    if (isOpen) {
                        isOpen = false;
                    }
                    try {
                        connentBlue();
                    } catch (Exception e) {
                        Log.e(TAG, Log.getStackTraceString(e));
                    }
                    HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET_STUTS, HandlerManager.BLUE_CONNET_STUTS);
                    break;
                case BluetoothAdapter.STATE_TURNING_ON:
                    if (isOpening) {
                        isOpening = false;

                    }
                    break;
            }

        }

    };

    @Override
    public boolean onKeyDown(int keycode, KeyEvent event) {
        if (keycode == KeyEvent.KEYCODE_BACK) {
            showExitDialog(this);
            return true;
        }
        return super.onKeyDown(keycode, event);
    }

    private DialogInfo dialogInfo;
    public void showExitDialog(final Activity context) {
        dialogInfo = new DialogInfo(context, getString(R.string.public_cozy_prompt), getString(R.string.show_sign_out), getString(R.string.btnOk), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                finish();
                getContext().exit();
//                                System.exit(0);
                MainApplication.isSessionOutTime = true;

            }

            @Override
            public void handleCancleBtn() {
                dialogInfo.cancel();
            }
        }, null);

        DialogHelper.resize(context, dialogInfo);
        dialogInfo.show();
    }



}
