package cn.swiftpass.enterprise.ui.paymentlink;

import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.CREATE_ORDER_SUCCESS;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.CREATE_ORDER_SUCCESS_TAG;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.PERMISSIONS_EXTERNAL_STORAGE;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.REQUEST_CODE_PICK_IMAGE;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.REQUEST_CODE_SELECT_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_CREATE_ORDER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_CREATE_ORDER_SUCCESS;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_SELECT_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.SUCCESS_TAG;
import static cn.swiftpass.enterprise.utils.DateUtil.getTime;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.bigkoo.pickerview.TimePickerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.common.sentry.SentryUtils;

import java.util.Date;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.fragment.PaymentLinkContract;
import cn.swiftpass.enterprise.mvp.presenter.fragment.PaymentLinkPresenter;
import cn.swiftpass.enterprise.ui.fmt.BaseFragment;
import cn.swiftpass.enterprise.ui.paymentlink.model.ImageUrl;
import cn.swiftpass.enterprise.ui.paymentlink.model.OrderConfig;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkCustomer;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkOrder;
import cn.swiftpass.enterprise.ui.paymentlink.widget.SlideItemView1;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.ImageUtil;
import cn.swiftpass.enterprise.utils.MoneyInputFilter;
import cn.swiftpass.enterprise.utils.SharedPreUtils;

/**
 * Created by congwei.li on 2021/9/10.
 *
 * @Description:
 */
public class FragmentAmountMode extends BaseFragment<PaymentLinkContract.Presenter> implements PaymentLinkContract.View {

    public OrderConfig orderConfig;
    LinearLayout detailsTitle;
    LinearLayout detailsContent;
    LinearLayout addCustomer;
    LinearLayout addOtherFee;
    SlideItemView1 customerInfo;
    TextView uploadPhoto;
    FrameLayout flPhotoPreview;
    ImageView ivPhotoPreview;
    ImageView ivPhotoPreviewDelete;
    EditText notes;
    EditText periodValidity;
    EditText etAmount;
    TextView customerName;
    TextView customerPhone;
    TextView customerEmail;
    TextView confirm;
    ImageView detailsTitleImage;
    PaymentLinkOrder orderData;
    OrderCreateActivity mActivity;
    boolean isCreate = true;
    private Bitmap selectImage;

    public FragmentAmountMode(PaymentLinkOrder orderData, boolean isCreate) {
        this.isCreate = isCreate;
        this.orderData = orderData;
    }

    public FragmentAmountMode(boolean isCreate) {
        this.isCreate = isCreate;
    }

    public void setOrderConfig(OrderConfig orderConfig) {
        this.orderConfig = orderConfig;
        if (!TextUtils.isEmpty(orderData.effectiveDate)) {
            return;
        }
        try {
            if (!TextUtils.isEmpty(orderConfig.orderExpireTime)
                    && !TextUtils.isEmpty(orderConfig.expireTimeUnit)) {
                long moreMill = 0;
                if (orderConfig.expireTimeUnit.equals("0")) { // 分钟
                    moreMill = Long.parseLong(orderConfig.orderExpireTime) * 60 * 1000;
                } else if (orderConfig.expireTimeUnit.equals("1")) {// 小时
                    moreMill = Long.parseLong(orderConfig.orderExpireTime) * 60 * 60 * 1000;
                } else if (orderConfig.expireTimeUnit.equals("2")) {// 天
                    moreMill = Long.parseLong(orderConfig.orderExpireTime) * 24 * 60 * 60 * 1000;
                }
                orderData.effectiveDate = DateUtil.formatTime(System.currentTimeMillis()
                        + moreMill);
            } else {
                orderData.effectiveDate = DateUtil.formatTime(System.currentTimeMillis()
                        + (24 * 60 * 60 * 1000));
            }
        } catch (Exception e) {
            orderData.effectiveDate = DateUtil.formatTime(System.currentTimeMillis()
                    + (24 * 60 * 60 * 1000));
        }
        periodValidity.setText(orderData.effectiveDate);
    }

    private void checkConfirm() {
        confirm.setEnabled(DateUtil.convertToDouble(orderData.totalFee, 0) > 0);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OrderCreateActivity) {
            mActivity = (OrderCreateActivity) activity;
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_amount_mode;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view == null) {
            return null;
        }

        if (null == orderData) {
            orderData = new PaymentLinkOrder();
        } else {
            if (null != orderData.orderRelateGoodsList && orderData.orderRelateGoodsList.size() > 0) {
                orderData.orderRelateGoodsList.clear();
            }
            if (null != orderData.orderCostList && orderData.orderCostList.size() > 0) {
                orderData.orderCostList.clear();
            }
        }
        initView(view);

        return view;
    }

    private void initView(View view) {
        detailsTitle = view.findViewById(R.id.ll_details_optional);
        detailsContent = view.findViewById(R.id.ll_details_content);
        addCustomer = view.findViewById(R.id.ll_add_customer);
        addOtherFee = view.findViewById(R.id.ll_add_other_fee);
        customerInfo = view.findViewById(R.id.ll_customer_item);
        uploadPhoto = view.findViewById(R.id.tv_upload_photo);
        flPhotoPreview = view.findViewById(R.id.fl_photo_preview);
        ivPhotoPreview = view.findViewById(R.id.im_photo_preview);
        ivPhotoPreviewDelete = view.findViewById(R.id.im_photo_preview_delete);
        notes = view.findViewById(R.id.et_notes);
        periodValidity = view.findViewById(R.id.et_validity);
        etAmount = view.findViewById(R.id.et_input_amount);
        customerName = view.findViewById(R.id.tv_customer_name);
        customerPhone = view.findViewById(R.id.tv_customer_phone);
        customerEmail = view.findViewById(R.id.tv_customer_email);
        confirm = view.findViewById(R.id.tv_confirm);
        detailsTitleImage = view.findViewById(R.id.iv_show_detail);


        periodValidity.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(periodValidity.getText().toString().trim())) {
                    orderData.effectiveDate = periodValidity.getText().toString().trim();
                }
            }
        });
        periodValidity.setText(orderData.effectiveDate);
        periodValidity.setFocusable(false);
        periodValidity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.showDateSelect(orderData.effectiveDate,
                        new TimePickerView.OnTimeSelectListener() {
                            @Override
                            public void onTimeSelect(Date date, View v) {
                                periodValidity.setText(
                                        DateUtil.getAllNormalTime(getTime(date)));
                            }
                        });
            }
        });

        customerInfo.setOnStateChangeListener(new SlideItemView1.OnStateChangeListener() {
            @Override
            public void onFuncShow(SlideItemView1 itemView, boolean isShow) {
            }

            @Override
            public void onFuncClick() {
                if (null != customerInfo && customerInfo.isExpansion()) {
                    customerInfo.reset();
                }
                orderData.customer = new PaymentLinkCustomer();
                orderData.custId = "";
                setCustomerView();
            }

            @Override
            public void onContentClick() {
            }
        });

        etAmount.setFilters(new InputFilter[]{new MoneyInputFilter()
                , new InputFilter.LengthFilter(20)});
        addOtherFee.setVisibility(View.GONE);
        detailsContent.setVisibility(orderData.isHaveData() ? View.VISIBLE : View.GONE);
        confirm.setText(isCreate ? getStringById(R.string.payment_link_add_order_confirm_order) :
                getStringById(R.string.pl_edit_order_save));
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mActivity != null) {
                    DialogOrderInfoConfirm dialogOrderInfoConfirm =
                            new DialogOrderInfoConfirm(mActivity, orderData);
                    dialogOrderInfoConfirm.setCurrentListener(
                            new DialogOrderInfoConfirm.DialogClickListener() {
                                @Override
                                public void onClickConfirm() {
                                    uploadImg();
                                }
                            });
                    if (!dialogOrderInfoConfirm.isShowing()) {
                        dialogOrderInfoConfirm.show();
                    }
                }
            }
        });
        // 设置可换行edittext，限制200字符
        notes.setSingleLine(false);
        notes.setMaxLines(10);
        notes.setHorizontallyScrolling(false);
        notes.setGravity(Gravity.TOP);
        notes.setFilters(new InputFilter[]{new InputFilter.LengthFilter(200)});

        notes.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(notes.getText().toString().trim())) {
                    return;
                }
                orderData.orderRemark = notes.getText().toString().trim();
            }
        });
        if (!TextUtils.isEmpty(orderData.orderRemark)) {
            notes.setText(orderData.orderRemark);
        }

        notes.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    //通知父控件不要干扰
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                    //通知父控件不要干扰
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    view.getParent().requestDisallowInterceptTouchEvent(false);
                }
                return false;
            }
        });

        if (!TextUtils.isEmpty(orderData.custId)) {
            orderData.customer = new PaymentLinkCustomer();
            orderData.customer.custId = orderData.custId;
            orderData.customer.custName = orderData.custName;
            orderData.customer.custMobile = orderData.custMobile;
            orderData.customer.custEmail = orderData.custEmail;
        }
        setCustomerView();

        detailsTitleImage.setRotation(detailsContent.getVisibility() == View.VISIBLE ? 270f : 90f);
        detailsTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detailsContent.setVisibility(detailsContent.getVisibility() == View.VISIBLE ?
                        View.GONE : View.VISIBLE);
                detailsTitleImage.setRotation(detailsContent.getVisibility() == View.VISIBLE ?
                        270f : 90f);
            }
        });
        addCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCustomerSelect();
            }
        });

        uploadPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() == null) {
                    return;
                }
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                PERMISSIONS_EXTERNAL_STORAGE);
                        return;
                    }
                }

                toSelectImage();
            }
        });
        ivPhotoPreviewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSelectPhoto(false);
                orderData.picUrl = "";
            }
        });
        if (!TextUtils.isEmpty(orderData.totalFee)) {
            etAmount.setText(DateUtil.formatMoneyUtils(orderData.totalFee));
            checkConfirm();
        }
        etAmount.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                try {
                    orderData.totalFee = DateUtil.formatMoneyInt(
                            etAmount.getText().toString().trim());
                    checkConfirm();
                } catch (Exception e) {
                    SentryUtils.INSTANCE.uploadTryCatchException(
                            e,
                            SentryUtils.INSTANCE.getClassNameAndMethodName()
                    );
                }
            }
        });
        showSelectPhoto(!TextUtils.isEmpty(orderData.picUrl));
        if (!TextUtils.isEmpty(orderData.picUrl)) {
            Glide.with(mActivity)
                    .load(orderData.picUrl)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(ivPhotoPreview);
        }
    }

    private void showSelectPhoto(boolean isShow) {
        uploadPhoto.setVisibility(isShow ? View.GONE : View.VISIBLE);
        flPhotoPreview.setVisibility(isShow ? View.VISIBLE : View.GONE);
        if (!isShow) {
            selectImage = null;
        }
    }

    private void toSelectImage() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CODE_PICK_IMAGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSIONS_EXTERNAL_STORAGE:
                toSelectImage();
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CREATE_ORDER_SUCCESS && resultCode == RESULT_CODE_CREATE_ORDER_SUCCESS) {
            Intent i = new Intent();
            mActivity.setResult(RESULT_CODE_CREATE_ORDER, i);
            if (!mActivity.isFinishing()) {
                mActivity.isFinishing();
            }
        }
        if (null == data) {
            return;
        }
        if (requestCode == REQUEST_CODE_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            Bitmap resultBimap = ImageUtil.getBitmapFromPath(getActivity(), uri);
            if (resultBimap != null) {
                selectImage = ImageUtil.compressImage(resultBimap);
                showSelectPhoto(true);
                ivPhotoPreview.setImageBitmap(selectImage);
            }
        }
        if (requestCode == REQUEST_CODE_SELECT_CUSTOMER && resultCode == RESULT_CODE_SELECT_CUSTOMER) {
            orderData.customer = (PaymentLinkCustomer) data.getSerializableExtra("result");
            if (null != orderData.customer) {
                orderData.custId = orderData.customer.custId;
                orderData.custName = orderData.customer.custName;
                orderData.custMobile = orderData.customer.custMobile;
                orderData.custEmail = orderData.customer.custEmail;
            }
            setCustomerView();
        }
    }

    private void initData() {

    }

    private void openCustomerSelect() {
        CustAndProdSelectActivity.startActivityForResult(this, orderData.customer,
                REQUEST_CODE_SELECT_CUSTOMER);
    }

    private void setCustomerView() {
        customerInfo.setVisibility(!TextUtils.isEmpty(orderData.custId) ? View.VISIBLE : View.GONE);
        if (!TextUtils.isEmpty(orderData.custId)) {
            customerName.setText(orderData.custName);
            if (!TextUtils.isEmpty(orderData.custMobile)) {
                customerPhone.setText(mContext.getResources().getString(R.string.pl_customer_item_tel)
                        + orderData.custMobile);
            } else {
                customerPhone.setText("");
            }
            if (!TextUtils.isEmpty(orderData.custEmail)) {
                customerEmail.setText(mContext.getResources().getString(R.string.pl_customer_item_email)
                        + orderData.custEmail);
            } else {
                customerEmail.setText("");
            }
        }
    }


    @Override
    public void uploadImgSuccess(@NonNull ImageUrl result) {
        orderData.picUrl = result.picUrl;
        requestCreateOrder(orderData);
    }

    @Override
    public void uploadImgFailed(@NonNull Object error) {
        if (null != getActivity()) {
            toastDialog(getActivity(), error.toString(), null);
        }
    }

    private void uploadImg() {
        if (selectImage != null) {
            if (mPresenter != null) {
                mPresenter.uploadImg(ImageUtil.bitmapToBase64(selectImage), "jpg");
            }
        } else {
            requestCreateOrder(orderData);
        }
    }


    @Override
    public void addNewOrderSuccess(@NonNull PaymentLinkOrder response) {
        SharedPreUtils.saveObject(SUCCESS_TAG, CREATE_ORDER_SUCCESS_TAG);
        OrderDetailsActivity.startActivityForResult(getActivity(), response,
                CREATE_ORDER_SUCCESS);
        if (!mActivity.isFinishing()) {
            mActivity.finish();
        }
    }

    @Override
    public void addNewOrderFailed(@NonNull Object error) {
        if (null != getActivity()) {
            toastDialog(getActivity(), error.toString(), null);
        }
    }


    @Override
    public void editOrderSuccess(@NonNull String response) {
        SharedPreUtils.saveObject(SUCCESS_TAG, CREATE_ORDER_SUCCESS_TAG);
        if (!mActivity.isFinishing()) {
            mActivity.finish();
        }
    }

    @Override
    public void editOrderFailed(@NonNull Object error) {
        if (null != getActivity()) {
            toastDialog(getActivity(), error.toString(), null);
        }
    }

    private void requestCreateOrder(PaymentLinkOrder orderData) {
        if (isCreate) {
            if (mPresenter != null) {
                mPresenter.addNewOrder(orderData);
            }
        } else {
            if (mPresenter != null) {
                mPresenter.editOrder(orderData);
            }
        }
    }

    @Override
    protected PaymentLinkContract.Presenter createPresenter() {
        return new PaymentLinkPresenter();
    }
}
