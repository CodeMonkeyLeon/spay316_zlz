package cn.swiftpass.enterprise.utils;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Collection;

public abstract class Dumper implements Serializable
{
	public void dump()
	{
		if(GlobalConstant.isDebug)
		{
			StringBuffer sb = new StringBuffer();
			dump(0, sb);
			Logger.d("Dump", sb.toString());
		}
	}
	public void dump(int depth, StringBuffer sb)
	{		
		Class c = this.getClass();
		if(sb==null)
		{
			sb = new StringBuffer();
		}
		
		int currentDepth = depth;
		addTab(currentDepth, sb);
		sb.append(c.getSimpleName());
		sb.append(" begin");
		sb.append("\n");
		
		dumpFields(++depth, c, sb);
		
		addTab(currentDepth, sb);
		sb.append(c.getSimpleName());
		sb.append(" end");
	}
	
	private void dumpFields(int depth, Class c, StringBuffer sb)
	{
		if(c == null)
		{
			return;
		}
		
		Class parent = c.getSuperclass();
		if(parent!=null)
		{
			dumpFields(depth, parent, sb);
		}
		
		Field[] fields = c.getDeclaredFields();
		
		for(Field field : fields)
		{
			boolean accessable = field.isAccessible();
			
			field.setAccessible(true);
			try 
			{
				Object o = field.get(this);
				addTab(depth, sb);
				sb.append(field.getName());
				sb.append(":");
				if(o!=null)
				{
					if(o instanceof Dumper)
					{
						sb.append("\n");
						((Dumper) o).dump(++depth, sb);
					}
					else
					{
						if(o instanceof Collection)
						{
							Collection list = (Collection)o;
							sb.append("\n-------------- "+field.getName()+" Begin --------------\n");
							depth++;
							int index=0;
							for(Object e:list)
							{
								if(e instanceof Dumper)
								{
									sb.append(++index+".\n");
									((Dumper) e).dump(depth, sb);
								}
								else
								{
									sb.append(e.toString()+"\n");
								}
							}
							sb.append("\n-------------- "+field.getName()+" End --------------\n");
						}
						else if(o instanceof short[])
						{
							short[] arr = (short[])o;
							StringBuffer temp = new StringBuffer();
							if(arr!=null)
							{
								for(short s:arr)
								{
									temp.append(s+" ");
								}
								
								sb.append(temp);
							}
						}
						else
						{
							sb.append(o.toString());
						}
					}
				}
				sb.append("\n");		
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
                Logger.i("Dump",e.getMessage());
			}
			
			field.setAccessible(accessable);
		}
	}
	
	private void addTab(int depth, StringBuffer sb)
	{
		for(int i=0;i<depth;i++)
		{
			sb.append("    ");
		}
	}
}
