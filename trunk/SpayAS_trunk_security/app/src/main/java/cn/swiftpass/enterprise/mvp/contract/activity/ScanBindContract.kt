package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bean.QRCodeBean
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/11/23
 *
 * @大家走进了夜海
 * @去打捞遗失的繁星
 */
class ScanBindContract {

    interface View : BaseView {

        fun bindCodeDetailsSuccess(response: QRCodeBean)

        fun bindCodeDetailsFailed(error: Any?)

        fun bindCodeSuccess(response: String)

        fun bindCodeFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {


        fun bindCode(
            qrId: String?,
            deskName: String?
        )


        fun bindCodeDetails(
            qrId: String?,
            deskName: String?
        )

    }

}