package cn.swiftpass.enterprise.ui.paymentlink.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;

import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkProduct;
import cn.swiftpass.enterprise.ui.widget.BaseRecycleAdapter;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.KotlinUtils;

public class OrderDetailProductListAdapter extends BaseRecycleAdapter<PaymentLinkProduct> {

    private Context mContext;

    public OrderDetailProductListAdapter(Context context, List<PaymentLinkProduct> data) {
        super(data);
        mContext = context;
    }

    @Override
    protected void bindData(BaseViewHolder holder, int position) {
        PaymentLinkProduct data = datas.get(position);

        TextView desc1 = (TextView) holder.getView(R.id.tv_desc1);
        TextView desc2 = (TextView) holder.getView(R.id.tv_desc2);
        TextView desc3 = (TextView) holder.getView(R.id.tv_desc3);
        TextView desc4 = (TextView) holder.getView(R.id.tv_desc4);
        TextView desc5 = (TextView) holder.getView(R.id.tv_desc5);
        ImageView imgIcon = (ImageView) holder.getView(R.id.id_img_icon);
        ConstraintLayout productInfo = (ConstraintLayout) holder.getView(R.id.cl_order_info);

        desc4.setTextColor(mContext.getResources().getColor(R.color.color_A3A3A3));


        Glide.with(mContext)
                .load(data.goodsPic)
                .error(R.drawable.icon_payment_link_default)
                .transform(new CenterCrop(), new RoundedCorners(KotlinUtils.INSTANCE.dp2px(4f, mContext)))
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imgIcon);

        desc1.setText(data.goodsName);
        if (!TextUtils.isEmpty(data.goodsCode)) {
            desc2.setText(mContext.getResources().getString(R.string.payment_link_add_product_phone_code)
                    + ": " + data.goodsCode);
        } else {
            desc2.setText("");
        }
        if (!TextUtils.isEmpty(data.goodsDesc)) {
            desc3.setText(mContext.getResources().getString(R.string.payment_link_add_product_description)
                    + ": " + data.goodsDesc);
        } else {
            desc3.setText("");
        }
        desc4.setText(MainApplication.getInstance().getFeeFh() + " " + DateUtil.formatMoneyUtils(data.goodsPrice));
        desc5.setText("x" + data.goodsNum);
        productInfo.setBackgroundColor(mContext.getResources().getColor(R.color.transparent));
    }

    @Override
    public int getLayoutId() {
        return R.layout.horizontal_sliding_item_view;
    }
}
