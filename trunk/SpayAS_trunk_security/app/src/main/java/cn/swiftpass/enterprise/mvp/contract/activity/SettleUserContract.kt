package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.SettleModel
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @多想爱
 * @走累了
 * @走进深秋
 * @寺院间泛滥的落叶
 * @把我覆盖
 */
class SettleUserContract {


    interface View : BaseView {

        fun getOrderTotalSuccess(response: SettleModel?)

        fun getOrderTotalFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {

        fun getOrderTotal(
            startTime: String?,
            endTime: String?,
            mobile: String?
        )


    }


}