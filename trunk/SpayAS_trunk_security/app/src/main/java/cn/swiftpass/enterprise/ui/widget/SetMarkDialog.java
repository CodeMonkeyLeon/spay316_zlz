/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.common.sentry.SentryUtils;

import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 弹出提示框
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class SetMarkDialog extends Dialog {
    private static final String TAG = SetMarkDialog.class.getSimpleName();
    private Activity context;

    private TextView content;

    private Button btnOk, btCancel;

    private ViewGroup mRootView;

    private SetMarkDialog.HandleBtn handleBtn;

    private SetMarkDialog.HandleBtnCancle cancleBtn;

    //private SetMarkDialog.HandleBtnrtw rTWBtn;

    //private View line_img, line_img_bt;

    // 更多
    public static final int FLAG = 0;

    // 最终提交
    public static final int SUBMIT = 1;

    // 注册成功
    public static final int REGISTFLAG = 2;

    // 微信支付 是否授权激活
    public static final int EXITAUTH = 3;

    // 微信支付 授权激活 重新登录
    public static final int EXITAUTHLOGIN = 4;

    //提交商户信息
    public static final int SUBMITSHOPINFO = 5;

    //判断网络连接状态
    public static final int NETWORKSTATUE = 8;

    //优惠卷列表，长按提示删除提示框
    public static final int SUBMIT_DEL_COUPON_INFO = 6;

    //优惠卷发布
    public static final int SUBMIT_COUPON_INFO = 7;

    public static final int SUBMIT_SHOP = 9;

    public static final int SCAN_PAY = 10;

    public static final int VARD = 11;

    public static final int UNIFED_DIALOG = 12;

    //private OnItemLongDelListener mDelListener;

    //private int position;

    //private OnSubmitCouponListener mOnSubmitCouponListener;

    //private ImageView radioButton1, radioButton2;

    //private RelativeLayout lay_zh, lay_rTw;

    //private boolean lang = true; // 默认是中文

    private EditText et_content;

    //private ImageView iv_clearTel;

    /**
     * EditText获取焦点并显示软键盘
     */
    private void showSoftInputFromWindow(Activity activity, EditText editText) {
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public SetMarkDialog(Activity context, String reMark, SetMarkDialog.HandleBtn handleBtn,
                         SetMarkDialog.HandleBtnCancle cancleBtn) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.set_mark_dialog, null);
        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);

        this.context = context;
        this.handleBtn = handleBtn;
        this.cancleBtn = cancleBtn;

        initView(reMark);

        setLinster();
        showSoftInputFromWindow(context, et_content);
        /**
         * 可配置设置
         */
        DynModel dynModel = (DynModel) SharedPreUtils.readProduct("dynModel" + BuildConfig.bankCode);
        if (null != dynModel) {
            try {
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonColor())) {//btCancel
                    btnOk.setBackgroundColor(Color.parseColor(dynModel.getButtonColor()));
                    //                    btCancel.setBackgroundColor(Color.parseColor(dynModel.getButtonColor()));
                }

                if (!StringUtil.isEmptyOrNull(dynModel.getButtonFontColor())) {
                    btnOk.setTextColor(Color.parseColor(dynModel.getButtonFontColor()));
                    //                    btCancel.setTextColor(Color.parseColor(dynModel.getButtonFontColor()));
                }
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                Log.e(TAG, Log.getStackTraceString(e));
            }
        }
    }

    public void setBtnOkText(String string) {
        if (btnOk != null) {
            btnOk.setText(string);
        }
    }

    public void setMessage(String msg) {
        this.content.setText(msg);
    }

    public void setmDelListener(OnItemLongDelListener mDelListener, int position) {
        //this.mDelListener = mDelListener;
        //this.position = position;
    }

    public void setmOnSubmitCouponListener(OnSubmitCouponListener mOnSubmitCouponListener) {
        //this.mOnSubmitCouponListener = mOnSubmitCouponListener;
    }

    /**
     * 设置监听
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void setLinster() {

        //        lay_zh.setOnClickListener(new View.OnClickListener()
        //        {
        //            
        //            @Override
        //            public void onClick(View v)
        //            {
        //                //                if (radioButton1.isChecked())
        //                //                {
        //                radioButton1.setBackgroundResource(R.drawable.select_selected);
        //                radioButton2.setBackgroundResource(R.drawable.select_default);
        //                lang = true;
        //                //                }
        //                //                else
        //                //                {
        //                //                    radioButton1.setChecked(false);
        //                //                }
        //            }
        //        });
        //        lay_rTw.setOnClickListener(new View.OnClickListener()
        //        {
        //            
        //            @Override
        //            public void onClick(View v)
        //            {
        //                //                if (radioButton2.isChecked())
        //                //                {
        //                
        //                radioButton1.setBackgroundResource(R.drawable.select_default);
        //                radioButton2.setBackgroundResource(R.drawable.select_selected);
        //                lang = false;
        //                //                }else {
        //                //                    
        //                //                }
        //                //                else
        //                //                {
        //                //                    radioButton1.setChecked(false);
        //                //                }
        //            }
        //        });

        btCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
                cancleBtn.handleCancleBtn();
                cancel(); //Intl
            }

        });
        btnOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (!TextUtils.isEmpty(et_content.getText().toString())) {
                    handleBtn.handleOkBtn(et_content.getText().toString());
                    dismiss();
                    cancel();
                } else {
                    MyToast myToast = new MyToast();
                    myToast.showToast(context, ToastHelper.toStr(R.string.tv_remark_input));
                }
            }
        });
    }

    /**
     * 初始化
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initView(String reMark) {

        btnOk = (Button) findViewById(R.id.btnOk);
        btCancel = (Button) findViewById(R.id.btCancel);
        et_content = (EditText) findViewById(R.id.et_content);
        if (!StringUtil.isEmptyOrNull(reMark)) {
            et_content.setText(reMark);
        }
        //iv_clearTel = (ImageView)findViewById(R.id.iv_clearTel);
    }

    public void showPage(Class clazz) {
        Intent intent = new Intent(context, clazz);
        context.startActivity(intent);
    }

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    public interface HandleBtn {
        void handleOkBtn(String lang);

        //        void handleCancleBtn();
    }

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    public interface HandleBtnCancle {
        void handleCancleBtn();
    }

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    public interface HandleBtnrtw {
        void handleCancleBtn();
    }

    /***
     *
     * 长按删除优惠
     *
     * @author wang_lin
     * @version [2.1.0, 2014-4-23]
     */
    public interface OnItemLongDelListener {
        public void onItemLongDelMessage(int position);
    }

    /***
     *
     * 发布和修改优惠价提示
     * @author wang_lin
     * @version [2.1.0, 2014-4-23]
     */
    public interface OnSubmitCouponListener {
        public void onSubmitCouponListenerOk();

        public void onSubmitCouponListenerCancel();
    }
}
