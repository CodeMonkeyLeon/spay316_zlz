package cn.swiftpass.enterprise.utils;

import android.text.TextUtils;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;

import java.io.StringReader;
import java.util.HashMap;

public class XmlUtil {
	private static String PARSE_ERROR = "Problem parsing API response";

	public static HashMap<String, String> parse(String result) throws ParseException {
		HashMap<String, String>  data = new HashMap<String, String>();
		
		final XmlPullParser parser = Xml.newPullParser();
		
		try {
			parser.setInput(new StringReader(result));
			int type;
			String tag = null;
			
			while ((type = parser.next()) != XmlPullParser.END_DOCUMENT) {
				switch (type) {
				case XmlPullParser.START_TAG:
					if (!TextUtils.equals(parser.getName(),"root")) {
						tag = parser.getName().trim();						
					}
					break;
				case XmlPullParser.TEXT:
					if (tag != null){						
						data.put(tag, parser.getText().trim());
					}
					break;
				case XmlPullParser.END_TAG:									
					tag = null;					
					break;
				default:
					break;
				}
			}
		} catch (Exception e) {
			throw new ParseException(PARSE_ERROR, e);
		}
		
		return data;
	}

//	@Override
//	public boolean equals(Object obj) {
//		return super.equals(obj);
//	}

	/**
	 * Thrown when there were problems parsing the response to an API call,
	 * either because the response was empty, or it was malformed.
	 */
	public static class ParseException extends Exception {
		private static final long serialVersionUID = 1L;

		public ParseException(String detailMessage, Throwable throwable) {
			super(detailMessage, throwable);
		}
	}

}
