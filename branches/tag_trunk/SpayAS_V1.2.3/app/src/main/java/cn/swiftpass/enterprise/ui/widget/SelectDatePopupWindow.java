package cn.swiftpass.enterprise.ui.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.List;

import cn.swiftpass.enterprise.bussiness.model.WalletModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.StringUtil;

public class SelectDatePopupWindow extends PopupWindow implements OnDateChangedListener
{
    private TextView tv_cancel, tv_ok, tv_wx_pay, tv_zfb_pay, tv_qq_pay, tv_jd_pay;
    
    private View mMenuView;
    
    private String type;
    
    private TextView tv_scan;
    
    private ListView listView;
    
    private TextView title;
    
    List<WalletModel> model;
    
    private String choice;
    
    private TextView tv_cel;
    
    private ImageView iv_bank;
    
    private LinearLayout ly_reset;
    
    private TextView ly_ok;
    
    private SelectDatePopupWindow.HandleBtn handleBtn;
    
    private TimePicker timePicker;
    
    private Activity activity;
    
    private DatePicker datePicker;
    
    Button btnOk;
    
    private long time;
    
    public interface HandleTv
    {
        void takePic();
        
        void choicePic();
    }
    
    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作  
     * @author Administrator  
     *
     */
    public interface HandleBtn
    {
        void handleOkBtn(long time);
    }
    
    public SelectDatePopupWindow(Activity context, String time, SelectDatePopupWindow.HandleBtn handleBtn)
    {
        super(context);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.date_time_pick_dialog, null);
        this.handleBtn = handleBtn;
        this.activity = context;
        
        initView(mMenuView, time);
        
        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.FILL_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new OnTouchListener()
        {
            
            public boolean onTouch(View v, MotionEvent event)
            {
                
                int height = mMenuView.findViewById(R.id.pop_layout).getTop();
                int y = (int)event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    if (y < height)
                    {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }
    
    private void initView(View view, String date)
    {
        datePicker = (DatePicker)view.findViewById(R.id.datepicker);
        timePicker = (TimePicker)view.findViewById(R.id.timepicker);
        timePicker.setVisibility(View.GONE);
        Calendar calendar = Calendar.getInstance();
        if (!StringUtil.isEmptyOrNull(date))
        {
            try
            {
                //                String d = DateUtil.formatYYMD(Long.parseLong(date));
                String[] arr = date.split("\\-");
                datePicker.init(Integer.parseInt(arr[0]),
                    (Integer.parseInt(arr[1]) - 1),
                    Integer.parseInt(arr[2]),
                    this);
            }
            catch (Exception e)
            {
                // TODO: handle exception
                datePicker.init(calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH),
                    this);
            }
        }
        else
        {
            
            datePicker.init(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH),
                this);
        }
        
        btnOk = (Button)view.findViewById(R.id.btnOk);
        
        btnOk.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                handleBtn.handleOkBtn(time);
            }
        });
    }
    
    @Override
    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);
        time = calendar.getTimeInMillis();

    }
    
}
