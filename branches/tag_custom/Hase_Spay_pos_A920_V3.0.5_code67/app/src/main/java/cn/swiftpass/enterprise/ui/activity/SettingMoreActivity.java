package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Locale;
import java.util.Map;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.upgrade.UpgradeManager;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.GoodsMode;
import cn.swiftpass.enterprise.bussiness.model.MerchantTempDataModel;
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.setting.SettingAboutActivity;
import cn.swiftpass.enterprise.ui.activity.setting.SettingSwitchLanActivity;
import cn.swiftpass.enterprise.ui.activity.shop.ShopkeeperActivity;
import cn.swiftpass.enterprise.ui.activity.user.GoodsNameSetting;
import cn.swiftpass.enterprise.ui.widget.CommonConfirmDialog;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DataReportUtils;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 设置 会自动检查版本 User: ALAN Date: 13-10-28 Time: 下午5:11 To change this template use
 * File | Settings | File Templates.
 */
public class SettingMoreActivity extends TemplateActivity implements View.OnClickListener {
    private static final String TAG = SettingMoreActivity.class.getSimpleName();

    public static final String ACTION_BLUETOOTH_CONNECT_IS_SUCCESS = "action_bluetooth_connect_is_success";
    public static final String ACTION_BLUETOOTH_CONNECT_IS_CANCEL = "action_bluetooth_connect_is_cancel";

    private Context mContext;

    private LinearLayout llChangePwd,ll_tip_settings,cashier_layout;

    private Button ll_logout;

    private SharedPreferences sp;

    private LinearLayout bodyLay;

    private LinearLayout lay_choise;

    private ImageView iv_voice, iv_point;

    //private View vi_pay_method;

    private LinearLayout device_lay;
    private LinearLayout ll_FAQ,ll_about_us;


    private TextView tv_body, tv_device; //tv_bluetool_device;

//    private LinearLayout bluetool_lay;

    private DynModel dynModel;

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_setting);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButLayClick() {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        initLanuage();
    }

    private void initLanuage(){
        String language = PreferenceUtil.getString("language", "");
        if (!StringUtil.isEmptyOrNull(language))
        {
            if (language.equals(MainApplication.LANG_CODE_ZH_TW)
                    || language.equals(MainApplication.LANG_CODE_ZH_HK)
                    || language.equals(MainApplication.LANG_CODE_ZH_MO)
                    || language.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_HANT)) {
                tv_device.setText("中文(繁體)");

            } else if (language.equals(MainApplication.LANG_CODE_ZH_CN) || language.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_HANS)) {
                tv_device.setText("中文(简体)");
            } else if (language.equals(MainApplication.LANG_CODE_JA_JP) ) {
                tv_device.setText("日本語");
            } else {
                tv_device.setText("English");
            }
        }
        else {
            String lan = Locale.getDefault().toString();
            if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK)
                    || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO)
                    ||lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW)
                    || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_HANT))
            {
                tv_device.setText("中文(繁體)");
            }
            else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_HANS)) {
                tv_device.setText("中文(简体)");
            } else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_JA_JP) ) {
                tv_device.setText("日本語");
            } else {
                tv_device.setText("English");
            }

        }
    }


    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                //蓝牙关闭
                case HandlerManager.BLUE_CONNET_STUTS_CLOSED:
//                    tv_bluetool_device.setText(R.string.tx_print_no_conn);
                    break;
                //蓝牙已连接上后
                case HandlerManager.BLUE_CONNET_STUTS:
                    String name = MainApplication.getBlueDeviceName();
                    if (!StringUtil.isEmptyOrNull(name)) {
//                        tv_bluetool_device.setText(name);
                    } else {
//                        tv_bluetool_device.setText(R.string.tx_print_no_conn);
                    }
                    break;
                case HandlerManager.BLUE_CONNET:
                    String deviceName = MainApplication.getBlueDeviceName();
                    if (!StringUtil.isEmptyOrNull(deviceName)) {
//                        tv_bluetool_device.setText(deviceName);
                    } else {
//                        tv_bluetool_device.setText(R.string.tx_print_no_conn);
                    }
                    break;

                case HandlerManager.NOTICE_TYPE:
                    isHidePoint(iv_point);
                    break;
                case HandlerManager.BODY_SWITCH:
                    String body = (String) msg.obj;
                    if (body != null) {
                        tv_body.setText(body);
                    }
                    break;
                case HandlerManager.DEVICE_SWITCH:
                    String device = (String) msg.obj;
                    if (device != null) {
                        tv_device.setText(device);
                    }
                    break;
                default:
                    MerchantTempDataModel dataModel = (MerchantTempDataModel) msg.obj;
                    Integer merchantType = dataModel.getMerchantType();
                    Intent intent = new Intent(SettingMoreActivity.this, ShopkeeperActivity.class);
                    intent.putExtra("merchantType", merchantType);
                    SettingMoreActivity.this.startActivity(intent);
                    break;
            }
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sp = this.getSharedPreferences("login", 0);
        dynModel = (DynModel) SharedPreUtile.readProduct("dynModel" + ApiConstant.bankCode);
        initViews();
        HandlerManager.registerHandler(HandlerManager.NOTICE_TYPE, handler);

        HandlerManager.registerHandler(HandlerManager.VICE_SWITCH, handler);

        SettingMoreActivity.this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                isShowPoint(iv_point);
            }
        });

        IntentFilter Filter_Success = new IntentFilter();
        Filter_Success.addAction("action_bluetooth_connect_is_success");
        registerReceiver(updateBluetoothReceiver, Filter_Success,MainApplication.permission,null);

        IntentFilter Filter_Cancel = new IntentFilter();
        Filter_Cancel.addAction("action_bluetooth_connect_is_cancel");
        registerReceiver(closeBluetoothReceiver, Filter_Cancel,MainApplication.permission,null);


        loadBody();

        setLister();
        initValue();
    }

    private void initValue() {
        String deviceName = MainApplication.getBlueDeviceName();
        if (!StringUtil.isEmptyOrNull(deviceName) && MainApplication.getBlueState()) {
//            tv_bluetool_device.setText(deviceName);
        }
    }

    private void setLister() {
      /*  bluetool_lay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showPage(BluetoothSettingActivity.class);
            }
        });*/
    }


    void loadBody() {
        UserManager.queryBody(new UINotifyListener<GoodsMode>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
                dismissLoading();
            }

            @Override
            public void onError(Object object) {
                super.onError(object);
            }

            @Override
            public void onSucceed(GoodsMode object) {
                super.onSucceed(object);
                if (object != null) {
                    tv_body.setText(object.getBody());
                }
            }
        });
    }

    /**
     * 公告小圆点
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void isShowPoint(ImageView iv) {
        String noticePre = PreferenceUtil.getString("noticePre", "");
        Logger.i("hehui", "isShowPoint-->" + noticePre);
        if (!StringUtil.isEmptyOrNull(noticePre)) {
            Map<String, String> mapNotice = JsonUtil.jsonToMap(noticePre);
            for (Map.Entry<String, String> entry : mapNotice.entrySet()) {
                String value = entry.getValue();
                if (value.equals("0")) { //代表还有公开没有被打开查看详情过
                    iv.setVisibility(View.VISIBLE);
                }
            }
        }

    }

    /**
     * 公告小圆点
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void isHidePoint(ImageView iv) {
        boolean tag = true;
        String noticePre = PreferenceUtil.getString("noticePre", "");
        if (!StringUtil.isEmptyOrNull(noticePre)) {
            Map<String, String> mapNotice = JsonUtil.jsonToMap(noticePre);
            for (Map.Entry<String, String> entry : mapNotice.entrySet()) {
                String value = entry.getValue();
                if (value.equals("0")) { //代表还有公开没有被打开查看详情过
                    iv.setVisibility(View.VISIBLE);
                    tag = false;
                }
            }
            if (tag) {
                iv.setVisibility(View.GONE);
            }
        }

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();


    }

    @Override
    protected void onStop() {
        super.onStop();
        HandlerManager.unregisterHandler(HandlerManager.GETSHOPINFODONE, handler);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(updateBluetoothReceiver);
        unregisterReceiver(closeBluetoothReceiver);
    }

    BroadcastReceiver updateBluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Handle the received local broadcast
            String action = intent.getAction();
            if ("action_bluetooth_connect_is_success".equals(action)) {
                String name = MainApplication.getBlueDeviceName();
                if (!StringUtil.isEmptyOrNull(name)) {
//                    tv_bluetool_device.setText(name);
                } else {
//                    tv_bluetool_device.setText(R.string.tx_print_no_conn);
                }
            }
        }
    };

    BroadcastReceiver closeBluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Handle the received local broadcast
            String action = intent.getAction();
            if ("action_bluetooth_connect_is_cancel".equals(action)) {
//                tv_bluetool_device.setText(R.string.tx_print_no_conn);
            }
        }
    };

    private void initViews() {
        setContentView(R.layout.activity_setting_more);
        mContext = this;
//        tv_bluetool_device = getViewById(R.id.tv_bluetool_device);
//        bluetool_lay = getViewById(R.id.bluetool_lay);
        tv_device = getViewById(R.id.tv_device);

        tv_body = getViewById(R.id.tv_body);
        device_lay = getViewById(R.id.device_lay);

        device_lay.setOnClickListener(this);

        iv_voice = getViewById(R.id.iv_voice);
        lay_choise = getViewById(R.id.lay_choise);
        lay_choise.setOnClickListener(this);

        ll_logout = getViewById(R.id.ll_logout);
        ll_logout.setOnClickListener(this);

        cashier_layout = getViewById(R.id.cashier_layout);
        cashier_layout.setOnClickListener(this);

        ll_FAQ = getViewById(R.id.ll_FAQ);
        ll_FAQ.setOnClickListener(this);

        ll_about_us = getViewById(R.id.ll_about_us);
        ll_about_us.setOnClickListener(this);

        bodyLay = getViewById(R.id.bodyLay);
        bodyLay.setOnClickListener(this);

        llChangePwd = getViewById(R.id.ll_changePwd);
        llChangePwd.setOnClickListener(this);

        ll_tip_settings = getViewById(R.id.ll_tip_settings);
        ll_tip_settings.setOnClickListener(this);

        //该入口的展示需根据平台的小费开关状态判断，若小费开关打开，则展示，反之隐藏
        if(MainApplication.isTipOpenFlag()){
            ll_tip_settings.setVisibility(View.VISIBLE);
        }else{
            ll_tip_settings.setVisibility(View.GONE);
        }

        checkVersion(true);

        if (MainApplication.isAdmin.equals("0")) {
            bodyLay.setVisibility(View.GONE);
        }

        String voice = PreferenceUtil.getString("voice", "open");
        if (voice.equals("open")) {
            iv_voice.setImageResource(R.drawable.button_configure_switch_default);
        } else {
            iv_voice.setImageResource(R.drawable.button_configure_switch_close);
        }

        /**
         * 可配置设置
         */
        if (null != dynModel) {
            try {
                if (!StringUtil.isEmptyOrNull(dynModel.getButtonColor())) {
                    ll_logout.setBackgroundColor(Color.parseColor(dynModel.getButtonColor()));
                }

                if (!StringUtil.isEmptyOrNull(dynModel.getButtonFontColor())) {
                    ll_logout.setTextColor(Color.parseColor(dynModel.getButtonFontColor()));
                }
            } catch (Exception e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }
        }

        if (MainApplication.IS_POS_VERSION) {
//            bluetool_lay.setVisibility(View.GONE);
            lay_choise.setVisibility(View.GONE);
        }
    }



    /**
     * 修改密码
     */
    public void onChangePwd() {
        ChanagePwdActivity.startActivity(mContext);
    }


    private UpgradeInfo upVerInfo;

    /**
     * 检查版本显示提示图标
     */
    public void checkVersion(final boolean isDisplayIcon) {
        // 不用太平凡的检测升级 所以使用时间间隔区分
        UpgradeManager.getInstance().getVersonCode(new UINotifyListener<UpgradeInfo>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                // titleBar.setRightLodingVisible(true);
                showLoading(false, R.string.show_new_version_loading);
            }

            @Override
            public void onError(Object object) {
                super.onError(object);
                dismissLoading();
            }

            @Override
            public void onSucceed(UpgradeInfo result) {
                super.onSucceed(result);
                dismissLoading();
                if (null != result) {
                    if (isDisplayIcon) {
                        upVerInfo = result;
                    } else {
                        result.isMoreSetting = true;
                        showUpgradeInfoDialog(result, new ComDialogListener(result));
                    }
                } else {
                    if (!isDisplayIcon) {
                        showToastInfo(R.string.show_no_version);
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.device_lay: //语言切换
                showPage(SettingSwitchLanActivity.class);
                break;
            //FAQ
            case R.id.ll_FAQ:

                // 1.携带参数的打点
                Bundle value1 = new Bundle();
                value1.putString("kGFASPayMeFAQ","常见问题的入口");
                DataReportUtils.getInstance().report("kGFASPayMeFAQ",value1);

                onHelpPage();
                break;
            //关于我们
            case R.id.ll_about_us:
                showPage(SettingAboutActivity.class);
                break;
            //语音播报
            case R.id.lay_choise:
                String voice = PreferenceUtil.getString("voice", "open");
                if (voice.equals("open")) {
                    PreferenceUtil.commitString("voice", "close");
                    iv_voice.setImageResource(R.drawable.button_configure_switch_close);
                } else {
                    iv_voice.setImageResource(R.drawable.button_configure_switch_default);
                    PreferenceUtil.commitString("voice", "open");
                }
                break;

            case R.id.bodyLay://修改商品名称
                if (MainApplication.isAdmin.equals("1")) {
                    if (null == MainApplication.body) {
                        showPage(GoodsNameSetting.class);
                        return;
                    }

                    UserManager.queryBody(new UINotifyListener<GoodsMode>() {
                        @Override
                        public void onPreExecute() {
                            super.onPreExecute();
                            showLoading(false, R.string.public_loading);
                        }

                        @Override
                        public void onPostExecute() {
                            super.onPostExecute();
                            dismissLoading();
                        }

                        @Override
                        public void onError(Object object) {
                            super.onError(object);
                            dismissLoading();
                            if (object != null) showPage(GoodsNameSetting.class);
                            //                                toastDialog(SettingMoreActivity.this, object.toString(), null);
                        }

                        @Override
                        public void onSucceed(GoodsMode object) {
                            super.onSucceed(object);
                            dismissLoading();

                            if (object != null) {
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("body", object);
                                showPage(GoodsNameSetting.class, bundle);
                            } else {
                                showPage(GoodsNameSetting.class);
                            }
                        }
                    });
                }


                break;
            case R.id.ll_logout:

                // 1.携带参数的打点
                Bundle values = new Bundle();
                values.putString("kGFASPayMeLogout","安全退出入口");
                DataReportUtils.getInstance().report("kGFASPayMeLogout",values);


                DialogInfo dialogInfo = new DialogInfo(SettingMoreActivity.this, getString(R.string.public_cozy_prompt), getString(R.string.show_sign_out), getString(R.string.btnOk), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

                    @Override
                    public void handleOkBtn() {
                        //                                finish();
                        //                                MainApplication.getContext().exit();
                        //                                System.exit(0);

                        //调用退出登录接口
                        logout();

                    }

                    @Override
                    public void handleCancleBtn() {
                        //                                dialogInfo.cancel();
                    }
                }, null);

                DialogHelper.resize(SettingMoreActivity.this, dialogInfo);
                dialogInfo.show();

                break;
            case R.id.ll_changePwd:
                onChangePwd();
                break;

            case R.id.ll_tip_settings:
                showPage(TipsSettingActivity.class);
                break;

            case R.id.cashier_layout:

                showPage(CashierDeskSettingActivity.class);

                break;

        }
    }

    //APP的本地退出操作，清掉本地缓存
    public void ClearAPPState(){
        //删掉本地缓存，清除登录状态
        deleteSharedPre();

        for (Activity a : MainApplication.allActivities) {
            a.finish();
        }

        Intent it = new Intent();
        it.setClass(getApplicationContext(), WelcomeActivity.class);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(it);

        LocalAccountManager.getInstance().onDestory();
        Editor editor = sp.edit().remove("userPwd");
        editor.commit();

        PreferenceUtil.removeKey("login_skey");
        PreferenceUtil.removeKey("login_sauthid");
        PreferenceUtil.removeKey("choose_pay_or_pre_auth");
        PreferenceUtil.removeKey("bill_list_choose_pay_or_pre_auth");

                     /*   ApiConstant.BASE_URL_PORT = MainApplication.config.getServerAddr();
                        ApiConstant.pushMoneyUrl = MainApplication.config.getPushMoneyUrl();
                        PreferenceUtil.removeKey("serverCifg");*/


        finish();
    }

    public void logout(){
        LocalAccountManager.getInstance().Logout(new UINotifyListener<Boolean>(){
            @Override
            public void onError(Object object) {
                super.onError(object);
                dismissLoading();
                if (null != object)
                {
                    toastDialog(SettingMoreActivity.this, object.toString(), null);
                    ClearAPPState();
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showLoading(false, R.string.public_loading);

            }

            @Override
            protected void onPostExecute() {
                super.onPostExecute();
                dismissLoading();
            }

            @Override
            public void onSucceed(Boolean result) {
                super.onSucceed(result);
                dismissLoading();
                ClearAPPState();
            }
        });
    }


    /**
     * 升级点击事件
     */
    class ComDialogListener implements CommonConfirmDialog.ConfirmListener {
        private UpgradeInfo result;

        public ComDialogListener(UpgradeInfo result) {
            this.result = result;
        }

        @Override
        public void ok() {
            new UpgradeDailog(SettingMoreActivity.this, result, new UpgradeDailog.UpdateListener() {
                @Override
                public void cancel() {

                }
            }).show();
        }

        @Override
        public void cancel() {
            PreferenceUtil.commitInt("update", result.version); // 保存更新，下次进来不更新
        }

    }

    MerchantTempDataModel merchantInfo = null;

    boolean isAuth = false;


    @Override
    protected boolean isLoginRequired() {
        return false;
    }

    /**
     * 进入帮助页
     */
    public void onHelpPage() {
        if (null != dynModel && !StringUtil.isEmptyOrNull(dynModel.getQuestionLink())) {
            ContentTextActivity.startActivity(mContext, dynModel.getQuestionLink(), R.string.title_common_question);
        } else {
            try {
                String language = PreferenceUtil.getString("language", "");
                if (!TextUtils.isEmpty(language)) {
                    if(language.equalsIgnoreCase(MainApplication.LANG_CODE_JA_JP)){
                        ContentTextActivity.startActivity(mContext, "https://haseapp.wepayez.com/web/faq/index.html?language=" + "ja_jp" + "&bank="+ApiConstant.bankName, R.string.title_common_question);
                    }else{
                        ContentTextActivity.startActivity(mContext, "https://haseapp.wepayez.com/web/faq/index.html?language=" + language + "&bank="+ApiConstant.bankName, R.string.title_common_question);
                    }
                } else {
                    String lan = Locale.getDefault().toString();
                    if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_HANS)) {
                        ContentTextActivity.startActivity(mContext, "https://haseapp.wepayez.com/web/faq/index.html?language=zh_cn&bank="+ApiConstant.bankName, R.string.title_common_question);
                    } else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO) ||
                            lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_HANT)) {
                        ContentTextActivity.startActivity(mContext, "https://haseapp.wepayez.com/web/faq/index.html?language=zh_tw&bank="+ApiConstant.bankName, R.string.title_common_question);
                    } else if(lan.equalsIgnoreCase(MainApplication.LANG_CODE_JA_JP) ){
                        ContentTextActivity.startActivity(mContext, "https://haseapp.wepayez.com/web/faq/index.html?language=ja_jp&bank="+ApiConstant.bankName, R.string.title_common_question);
                    }else {
                        ContentTextActivity.startActivity(mContext, "https://haseapp.wepayez.com/web/faq/index.html?language=en_us&bank="+ApiConstant.bankName, R.string.title_common_question);
                    }

                }

            } catch (Exception e) {
                Log.e(TAG,Log.getStackTraceString(e));
            }
        }

    }

}