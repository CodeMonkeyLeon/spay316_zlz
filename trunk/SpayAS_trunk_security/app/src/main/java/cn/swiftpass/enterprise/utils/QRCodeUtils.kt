package cn.swiftpass.enterprise.utils

import android.graphics.Bitmap
import com.example.common.sentry.SentryUtils
import com.example.common.sentry.SentryUtils.uploadTryCatchException
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.MultiFormatWriter
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import java.util.*

/**
 * @author lizheng.zhao
 * @date 2022/08/03
 */
object QRCodeUtils {

    //编码集
    private const val CHARSET = "utf-8"

    //二维码颜色
    private const val COLOR_BLACK = 0xff000000

    //二维码尺寸
    private const val QRCODE_SIZE = 100


    /**
     * 通过 String生成二维码
     */
    fun generateQRCodeByString(content: String): Bitmap? {

        var bitmap: Bitmap? = null
        val hashTable = Hashtable<EncodeHintType, Any>()
        hashTable[EncodeHintType.ERROR_CORRECTION] = ErrorCorrectionLevel.H
        hashTable[EncodeHintType.CHARACTER_SET] = CHARSET
        hashTable[EncodeHintType.MARGIN] = 1
        try {
            val bitMatrix = MultiFormatWriter().encode(
                content,
                BarcodeFormat.QR_CODE,
                QRCODE_SIZE,
                QRCODE_SIZE,
                hashTable
            )
            val width = bitMatrix.width
            val height = bitMatrix.height
            val pixels = IntArray(width * height)
            for (y in 0 until height) {
                for (x in 0 until width) {
                    if (bitMatrix[x, y]) {
                        pixels[y * width + x] = COLOR_BLACK.toInt()
                    }
                }
            }
            bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height)
        } catch (e: Exception) {
            uploadTryCatchException(
                e,
                SentryUtils.getClassNameAndMethodName()
            )
        }
        return bitmap
    }
}