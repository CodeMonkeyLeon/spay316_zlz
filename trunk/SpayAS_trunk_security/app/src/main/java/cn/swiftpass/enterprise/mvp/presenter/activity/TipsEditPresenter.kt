package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.TipsBean
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.TipsEditContract
import cn.swiftpass.enterprise.utils.*
import com.example.common.entity.EmptyData
import com.example.common.sentry.SentryUtils.getClassNameAndMethodName
import com.example.common.sentry.SentryUtils.uploadTryCatchException
import okhttp3.Call
import org.json.JSONArray
import org.json.JSONObject


/**
 * @author lizheng.zhao
 * @date 2022/12/2
 *
 * @真诚与热爱
 * @是我永不放弃的品质
 */
class TipsEditPresenter : TipsEditContract.Presenter {

    private var mView: TipsEditContract.View? = null

    override fun submitTipsSetting(tipsList: ArrayList<TipsBean>) {
        mView?.let { view ->

            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)

            val jsonMaps = JSONObject()
            val spayRs = System.currentTimeMillis()
            try {
                jsonMaps.put(ParamsConstants.MCH_ID, MainApplication.getInstance().userInfo.mchId)
                jsonMaps.put(ParamsConstants.USER_ID, MainApplication.getInstance().getUserId())

                //加签名
                val priKey = SharedPreUtils.readProduct(ParamsConstants.SECRET_KEY) as String?
                jsonMaps.put(ParamsConstants.SPAY_RS, spayRs.toString())
                if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getSKey())) {
                    jsonMaps.put(
                        ParamsConstants.NNS, SignUtil.getInstance().createSign(
                            JsonUtil.jsonToMap(jsonMaps.toString()),
                            MD5.md5s(priKey).substring(0, 16)
                        )
                    )
                } else {
                    jsonMaps.put(
                        ParamsConstants.NNS, SignUtil.getInstance().createSign(
                            JsonUtil.jsonToMap(jsonMaps.toString()), MD5.md5s(spayRs.toString())
                        )
                    )
                }
                val jsonArray = JSONArray()
                for (tip in tipsList) {
                    val jsonObject = JSONObject()
                    jsonObject.put(ParamsConstants.ID, tip.id)
                    jsonObject.put(ParamsConstants.TIPS_RATE, tip.tipsRate)
                    jsonObject.put(ParamsConstants.DEFAULT_TIP, tip.isDefaultTip)
                    jsonArray.put(jsonObject)
                }
                jsonMaps.put(ParamsConstants.TIPS_LIST, jsonArray)
            } catch (e: Exception) {
                uploadTryCatchException(
                    e,
                    getClassNameAndMethodName()
                )
            }


            AppClient.submitTipsSetting(
                spayRs.toString(),
                jsonMaps.toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {


                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.submitTipsSettingFailed(it.message)
                        }
                    }


                    override fun onResponse(response: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        view.submitTipsSettingSuccess(true)
                    }
                }
            )

        }
    }

    override fun attachView(view: TipsEditContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}