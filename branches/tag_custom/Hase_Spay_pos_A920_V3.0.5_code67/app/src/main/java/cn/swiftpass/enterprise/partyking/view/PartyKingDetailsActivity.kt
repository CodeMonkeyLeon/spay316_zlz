package cn.swiftpass.enterprise.partyking.view

import android.app.Activity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.partyking.adapter.PartyKingDetailsAdapter
import cn.swiftpass.enterprise.partyking.entity.VoucherInfoItemEntity
import cn.swiftpass.enterprise.partyking.interfaces.OnPktBottomLayoutListener
import cn.swiftpass.enterprise.ui.activity.TemplateActivity
import cn.swiftpass.enterprise.ui.widget.TitleBar
import cn.swiftpass.enterprise.utils.ActivitySkipUtil
import cn.swiftpass.enterprise.utils.Logger

class PartyKingDetailsActivity : TemplateActivity() {


    private fun getLayoutId() = R.layout.act_party_king_details


    private lateinit var mRecycleView: RecyclerView
    private lateinit var mAdapter: PartyKingDetailsAdapter
    private var mList = mutableListOf<VoucherInfoItemEntity>()
    private var mToolBarTitle: String = ""
    private var mToolBarRightText: String = ""


    companion object {
        const val TAG = "PartyKingDetailsActivity"
        const val PTK_DETAILS_DATA = "PTK_DETAILS_DATA"
        const val PTK_DETAILS_TOOLBAR_TITLE = "PTK_DETAILS_TOOLBAR_TITLE"
        const val PTK_DETAILS_TOOLBAR_RIGHT_TEXT = "PTK_DETAILS_TOOLBAR_RIGHT_TEXT"


        fun startPtkDetailsActivity(
            fromActivity: Activity,
            data: ArrayList<VoucherInfoItemEntity>,
            toolBarTitle: String,
            toolBarRightText: String = ""
        ) {
            val hashMap = HashMap<String, Any>()
            hashMap[PTK_DETAILS_DATA] = data
            hashMap[PTK_DETAILS_TOOLBAR_TITLE] = toolBarTitle
            hashMap[PTK_DETAILS_TOOLBAR_RIGHT_TEXT] = toolBarRightText
            ActivitySkipUtil.startAnotherActivity(
                fromActivity,
                PartyKingDetailsActivity::class.java,
                hashMap
            )
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        mList.clear()
        intent?.let {
            val list = it.getSerializableExtra(PTK_DETAILS_DATA) as ArrayList<VoucherInfoItemEntity>
            mToolBarTitle = it.getStringExtra(PTK_DETAILS_TOOLBAR_TITLE).toString()
            mToolBarRightText = it.getStringExtra(PTK_DETAILS_TOOLBAR_RIGHT_TEXT).toString()
            mList.addAll(list)
        }
        initToolBar()
        initView()
    }


    fun initView() {
        //初始化RecyclerView
        mRecycleView = findViewById(R.id.id_recycler_view_party_king)
        val layoutManager = LinearLayoutManager(activity)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        mRecycleView.layoutManager = layoutManager

        mAdapter = PartyKingDetailsAdapter(activity, mList)
        mAdapter.setOnPktButtonLayoutListener(object : OnPktBottomLayoutListener {
//            override fun onPktBottomLayoutSize(bottomLayout: LinearLayout) {
//                Logger.d(TAG, "---- ptk 底部已绘制完成")
//            }

            override fun onPktBottomGuideTextClick(data: VoucherInfoItemEntity?) {
                Logger.d(TAG, "---- ptk 点击底部引导文字")
            }

            override fun onPktBottomBlueButtonClick(data: VoucherInfoItemEntity?) {
                Logger.d(TAG, "---- ptk 点击底部蓝色按钮")
            }

            override fun onPktBottomWhiteButtonClick(data: VoucherInfoItemEntity?) {
                Logger.d(TAG, "---- ptk 点击底部白色按钮")
            }
        })

        mRecycleView.adapter = mAdapter
    }


    private fun isShowToolBarRightText() = !TextUtils.isEmpty(mToolBarRightText)


    private fun initToolBar() {
        titleBar.setLeftButtonVisible(true)

        if (isShowToolBarRightText()) {
            titleBar.setRightButLayVisibleForTotal(true, mToolBarRightText)
        } else {
            titleBar.setRightButLayVisibleForTotal(false, "")
        }
        titleBar.setTitle(mToolBarTitle)
        titleBar.setOnTitleBarClickListener(object : TitleBar.OnTitleBarClickListener {
            override fun onLeftButtonClick() {
                //返回
                finish()
            }

            override fun onRightButtonClick() {

            }

            override fun onRightLayClick() {

            }

            override fun onRightButLayClick() {
                if (isShowToolBarRightText()) {
                    Logger.d(TAG, "--- 点击了 $mToolBarRightText -- ToolBar Right Text")
                }
            }

        })
    }


}