package cn.swiftpass.enterprise.mvp.presenter.fragment

import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.fragment.TabPaymentLinkContract
import cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkCustomer
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkCustomerModel
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkOrderModel
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkProduct
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkProductModel
import cn.swiftpass.enterprise.utils.JsonUtil
import com.example.common.entity.EmptyData
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/11/17
 *
 * @你一会看我会看云我觉得你看我时很远你看云时很近
 */
class TabPaymentLinkPresenter : TabPaymentLinkContract.Presenter {


    private var mView: TabPaymentLinkContract.View? = null


    override fun getProductList(
        pageNumber: String?,
        goodsId: String?,
        goodsName: String?,
        goodsCode: String?
    ) {

        mView?.let {
            it.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
        }

        AppClient.getProductList(
            "1",
            goodsId,
            goodsName,
            goodsCode,
            pageNumber,
            PaymentLinkConstant.DEFAULT_PAGE_SIZE.toString(),
            System.currentTimeMillis().toString(),
            object :
                CallBackUtil.CallBackCommonResponse<PaymentLinkProductModel>(PaymentLinkProductModel()) {

                override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                    mView?.let { v ->
                        v.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            v.getProductListFailed(it.message)
                        }
                    }

                }

                override fun onResponse(res: CommonResponse?) {
                    mView?.let { v ->
                        v.dismissLoading(ParamsConstants.COMMON_LOADING)
                        res?.let { response ->
                            //如果orderRelateGoodsList值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"data\":\"\"",
                                "\"data\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                PaymentLinkProductModel::class.java
                            ) as PaymentLinkProductModel
                            v.getProductListSuccess(order)
                        }
                    }
                }
            }
        )

    }

    override fun getCustomerList(
        pageNumber: String?,
        custName: String?,
        custMobile: String?,
        custEmail: String?
    ) {
        mView?.let {
            it.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
        }
        AppClient.getCustomerList(
            "1",
            custName,
            custMobile,
            custEmail,
            pageNumber,
            PaymentLinkConstant.DEFAULT_PAGE_SIZE.toString(),
            System.currentTimeMillis().toString(),
            object : CallBackUtil.CallBackCommonResponse<PaymentLinkCustomerModel>(
                PaymentLinkCustomerModel()
            ) {
                override fun onFailure(call: Call?, commonResponse: CommonResponse?) {

                    mView?.let { view ->
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.getCustomerListFailed(it.message)
                        }
                    }
                }


                override fun onResponse(response: CommonResponse?) {
                    mView?.let { v ->
                        v.dismissLoading(ParamsConstants.COMMON_LOADING)
                        response?.let {
                            //如果orderRelateGoodsList值为空，替换""为[]，因为Gson无法解析""为数组
                            it.message = it.message.replace(
                                "\"data\":\"\"",
                                "\"data\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                it.message,
                                PaymentLinkCustomerModel::class.java
                            ) as PaymentLinkCustomerModel
                            v.getCustomerListSuccess(order)
                        }
                    }
                }
            }
        )

    }

    override fun getOrderList(
        orderNo: String?,
        realPlatOrderNo: String?,
        custName: String?,
        orderRemark: String?,
        pageNumber: String?,
        order_status: String?
    ) {
        mView?.let {
            it.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
        }

        AppClient.getOrderList(
            "1",
            orderNo,
            realPlatOrderNo,
            custName,
            orderRemark,
            pageNumber,
            PaymentLinkConstant.DEFAULT_PAGE_SIZE.toString(),
            order_status,
            System.currentTimeMillis().toString(),
            object :
                CallBackUtil.CallBackCommonResponse<PaymentLinkOrderModel>(PaymentLinkOrderModel()) {

                override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                    mView?.let { view ->
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.getOrderListFailed(it.message)
                        }
                    }
                }


                override fun onResponse(response: CommonResponse?) {
                    mView?.let { view ->
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        response?.let {
                            //如果dataList值为空，替换""为[]，因为Gson无法解析""为数组
                            it.message = it.message.replace(
                                "\"data\":\"\"",
                                "\"data\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                it.message,
                                PaymentLinkOrderModel::class.java
                            ) as PaymentLinkOrderModel
                            view.getOrderListSuccess(order)
                        }
                    }
                }
            }
        )


    }

    override fun deleteCustomer(c: PaymentLinkCustomer?, position: Int) {

        c?.let { customer ->
            mView?.let { view ->
                view.showLoading(
                    R.string.public_data_loading,
                    ParamsConstants.COMMON_LOADING
                )
            }

            AppClient.deleteOrQueryCustomer(
                "5",
                customer.custId,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        mView?.let { view ->
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            commonResponse?.let {
                                view.deleteCustomerFailed(it.message)
                            }
                        }
                    }

                    override fun onResponse(response: CommonResponse?) {
                        mView?.let { view ->
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            response?.let {
                                view.deleteCustomerSuccess(it.message, position)
                            }
                        }
                    }
                }
            )
        }
    }

    override fun deleteProduct(
        p: PaymentLinkProduct?,
        position: Int
    ) {
        p?.let { product ->
            mView?.let {
                it.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
            }

            AppClient.deleteOrQueryProduct(
                "5",
                product.goodsId,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        mView?.let { view ->
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            commonResponse?.let { error ->
                                view.deleteProductFailed(error.message)
                            }
                        }
                    }

                    override fun onResponse(res: CommonResponse?) {
                        mView?.let { view ->
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            res?.let { response ->
                                view.deleteProductSuccess(response.message, position)
                            }
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: TabPaymentLinkContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}