package cn.swiftpass.enterprise.ui.activity;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.enums.UserRole;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.refund.RefundManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.RefundModel;
import cn.swiftpass.enterprise.ui.adapter.RefundAdapter;
import cn.swiftpass.enterprise.utils.AppHelper;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 退款管理，进行审核
 * Date: 13-10-15
 * Time: 下午9:27
 */
public class RefundActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemClickListener,
    AdapterView.OnItemLongClickListener
{
    
    private EditText etDatetime, etOrderNo;
    
    private ListView listView;
    
    private RefundAdapter adapter;
    
    private TextView tvInfo;
    
    private List<RefundModel> data;
    
    private Context context;
    
    private boolean flag = true;
    
    private long searchTime = 0;
    
    private TextView tvState;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        context = this;
        initViews();
    }
    
    private void initViews()
    {
        setContentView(R.layout.activity_order_stream);
        tvState = getViewById(R.id.tv_state);
        tvState.setVisibility(View.VISIBLE);
        tvState.setText(R.string.public_operation);
        // etOrderNo = getViewById(R.id.et_orderNo);
        // etDatetime = getViewById(R.id.et_datetime);
        tvInfo = getViewById(R.id.tv_total);
        etDatetime.setOnClickListener(this);
        listView = getViewById(R.id.listView);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);
        adapter = new RefundAdapter(this);
        listView.setAdapter(adapter);
        flag = getIntent().getBooleanExtra("isAll", true);
        View llDatetime = getViewById(R.id.ll_datetime);
        if (flag)
        {
            llDatetime.setVisibility(View.VISIBLE);
            search();
        }
        else
        {
            llDatetime.setVisibility(View.GONE);
            searchTime = System.currentTimeMillis();
        }
    }
    
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
    {
        RefundModel o = data.get(i);
        OrderDetailsActivity.startActivity(context, o);
    }
    
    private void search()
    {
        String orderNo = etOrderNo.getText().toString();
        
        RefundManager.getInstant().queryRefund(orderNo, searchTime, new UINotifyListener<List<RefundModel>>()
        {
            @Override
            public void onError(Object object)
            {
                super.onError(object);
                if (object != null)
                {
                    showToastInfo(object.toString());
                }
            }
            
            @Override
            public void onPostExecute()
            {
                super.onPostExecute();
                dismissLoading();
            }
            
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                showLoading(true, getStringById(R.string.wait_a_moment));
            }
            
            @Override
            public void onSucceed(List<RefundModel> result)
            {
                super.onSucceed(result);
                data = result;
                if (result != null && result.size() > 0)
                {
                    adapter.setList(result);
                    tvInfo.setVisibility(View.GONE);
                    listView.setVisibility(View.VISIBLE);
                }
                else
                {
                    tvInfo.setText(R.string.no_refund_data);
                    tvInfo.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.GONE);
                }
            }
        });
    }
    
    @Override
    public void onClick(View view)
    {
        /*if(view.getId() == R.id.et_datetime)
        {
            SelectTimeDialog.createDatadialog(etDatetime.getText().toString(),RefundActivity.this,new SelectTimeDialog.OnMyClickListener() {
                @Override
                public void onOk(long datetime) {
                    etDatetime.setText(DateUtil.formatTime(datetime,"yyyy-MM-dd"));
                    searchTime = datetime;
                }

                @Override
                public void onCancel() {

                }
            });
        }*/
    }
    
    public void onSearch(View v)
    {
        search();
    }
    
    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l)
    {
        RefundModel o = data.get(i);
        AppHelper.copy(o.orderNo, context);
        ToastHelper.showInfo(getStringById(R.string.content_copy));
        return false;
    }
    
    public static void startActivity(Context context)
    {
        Intent it = new Intent();
        it.setClass(context, RefundActivity.class);
        context.startActivity(it);
    }
    
    public boolean onKeyDown(int keycode, KeyEvent event)
    {
        if (keycode == KeyEvent.KEYCODE_BACK)
        {
            if (LocalAccountManager.getInstance().getLoggedUser().role == UserRole.approve)
            {
                showExitDialog(this);
            }
        }
        return super.onKeyDown(keycode, event);
    }
}
