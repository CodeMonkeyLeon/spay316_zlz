package cn.swiftpass.enterprise.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.BindSpeakerBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.dialog.UnbinderSpeakerDialog;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;

import static cn.swiftpass.enterprise.MainApplication.getContext;

/**
 * Created by aijingya on 2019/8/19.
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: (绑定speaker的最终界面)
 * @date 2019/8/19.16:36.
 */
public class MySpeakerBindActivity extends TemplateActivity {

    @BindView(R.id.tv_merchant_name)
    TextView tvMerchantName;
    @BindView(R.id.tv_mch_name)
    TextView tvMchName;
    @BindView(R.id.ll_merchant_name)
    LinearLayout llMerchantName;
    @BindView(R.id.tv_merchant_id)
    TextView tvMerchantId;
    @BindView(R.id.tv_mch_id)
    TextView tvMchId;
    @BindView(R.id.ll_merchant_id)
    LinearLayout llMerchantId;
    @BindView(R.id.tv_speaker_id)
    TextView tvSpeakerId;
    @BindView(R.id.tv_spk_id)
    TextView tvSpkId;
    @BindView(R.id.ll_speaker_id)
    LinearLayout llSpeakerId;
    @BindView(R.id.ll_bind_info)
    LinearLayout llBindInfo;
    @BindView(R.id.btn_bind_speaker_next)
    Button btnBindSpeakerNext;
    @BindView(R.id.tv_unbind_speaker)
    TextView tvUnbindSpeaker;

    private UnbinderSpeakerDialog unbinderSpeakerDialog;
    private String speakerIDCode;
    private BindSpeakerBean SpeakerBean ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_speaker);
        ButterKnife.bind(this);

        initView();
    }

    public void initView(){
        speakerIDCode = getIntent().getStringExtra("speakerIDCode");
        SpeakerBean =  (BindSpeakerBean) getIntent().getSerializableExtra("SpeakerBean");
        btnBindSpeakerNext.setEnabled(true);

        if(!TextUtils.isEmpty(speakerIDCode) && SpeakerBean == null){
            //设置标题为绑定
            titleBar.setTitle(getStringById(R.string.terminal_bind_speaker));
            tvSpkId.setText(speakerIDCode);

            btnBindSpeakerNext.setText(getStringById(R.string.reg_next_step));

            tvUnbindSpeaker.setVisibility(View.GONE);

            if(MainApplication.isAdmin.equals("1")){//如果是商户登录，进行绑定
                tvMerchantName.setText(getStringById(R.string.terminal_speaker_merchant_name));
                tvMchName.setText(MainApplication.getMchName());

                tvMerchantId.setText(getStringById(R.string.terminall_speaker_merchant_id));
                String mch;
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    mch = MainApplication.merchantId;
                } else {
                    mch = MainApplication.getMchId();
                }
                tvMchId.setText(mch);

            }else if(MainApplication.isAdmin.equals("0")){//如果是收银员
                tvMerchantName.setText(getStringById(R.string.terminal_speaker_cashier_name));
                tvMchName.setText(MainApplication.realName);

                tvMerchantId.setText(getStringById(R.string.terminal_speaker_cashier_account));
                tvMchId.setText(PreferenceUtil.getString("user_name", MainApplication.userName));
            }

        }else if(TextUtils.isEmpty(speakerIDCode) && SpeakerBean != null){
            //设置标题为设备管理
            titleBar.setTitle(getStringById(R.string.terminal_speaker_speaker_management));
            tvSpkId.setText(String.valueOf(SpeakerBean.getDeviceName()));

            btnBindSpeakerNext.setText(getStringById(R.string.terminal_speaker_configure));

            tvUnbindSpeaker.setVisibility(View.VISIBLE);

            if(SpeakerBean.getUserType()== 3){//如果是商户
                tvMerchantName.setText(getStringById(R.string.terminal_speaker_merchant_name));
                tvMchName.setText(SpeakerBean.getUserName());

                tvMerchantId.setText(getStringById(R.string.terminall_speaker_merchant_id));
                tvMchId.setText(SpeakerBean.getAccount());

            }else if(SpeakerBean.getUserType() == 5){//如果是收银员
                tvMerchantName.setText(getStringById(R.string.terminal_speaker_cashier_name));
                tvMchName.setText(SpeakerBean.getUserName());

                tvMerchantId.setText(getStringById(R.string.terminal_speaker_cashier_account));
                tvMchId.setText(SpeakerBean.getAccount());
            }
        }
    }


    @OnClick({R.id.btn_bind_speaker_next, R.id.tv_unbind_speaker})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_bind_speaker_next:
                //点击 NEXT按钮，进行绑定操作，绑定成功跳转到配置WiFi界面
                if(!TextUtils.isEmpty(speakerIDCode) && SpeakerBean == null){
                    bindSpeaker(speakerIDCode);
                }else if(TextUtils.isEmpty(speakerIDCode) && SpeakerBean != null){//如果是configure按钮，则直接进入网络配置界面
                    checkWIFIState();
                }

                break;
            case R.id.tv_unbind_speaker:
                String mch_name= null;
                String mch_id = null;
                String speaker_id = null;
                if(SpeakerBean != null){
                    if(SpeakerBean.getUserType()== 3){//如果是商户
                        mch_name = getString(R.string.terminal_speaker_merchant_name)+": "+SpeakerBean.getUserName();
                        mch_id = getString(R.string.terminall_speaker_merchant_id)+": "+SpeakerBean.getAccount();
                        speaker_id = getString(R.string.terminal_speaker_id)+": "+SpeakerBean.getDeviceName();

                    }else if(SpeakerBean.getUserType() == 5){//如果是收银员
                        mch_name = getString(R.string.terminal_speaker_cashier_name)+": "+SpeakerBean.getUserName();
                        mch_id = getString(R.string.terminal_speaker_cashier_account)+": "+SpeakerBean.getAccount();
                        speaker_id = getString(R.string.terminal_speaker_id)+": "+SpeakerBean.getDeviceName();
                    }
                }
                unbinderSpeakerDialog = new UnbinderSpeakerDialog(MySpeakerBindActivity.this,mch_name,mch_id,speaker_id, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        unbinderSpeakerDialog.dismiss();
                        //此处做解绑点击确认按钮之后的解绑操作
                        unbindSpeaker(SpeakerBean.getUserId(),SpeakerBean.getDeviceName());
                    }
                });
                if(unbinderSpeakerDialog != null && !unbinderSpeakerDialog.isShowing()){
                    unbinderSpeakerDialog.show();
                }
                break;
        }
    }

    public void checkWIFIState(){
        //如果手机没连接WiFi
        if(!isWifiEnabled()){
            toastDialog(MySpeakerBindActivity.this, getStringById(R.string.terminal_speaker_wifi_disconnect), new NewDialogInfo.HandleBtn() {
                @Override
                public void handleOkBtn() {
                    finish();
                    showPage(MySpeakerListActivity.class);
                }
            });
        }else{
            showPage(MySpeakerConfigureNetworkActivity.class);
        }
    }

    public static boolean isWifiEnabled() {
        Context myContext = getContext();
        if (myContext == null) {
            throw new NullPointerException("Global context is null");
        }
        WifiManager wifiMgr = (WifiManager) myContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wifiMgr.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
            ConnectivityManager connManager = (ConnectivityManager) myContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo wifiInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            return wifiInfo.isConnected();
        } else {
            return false;
        }
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(getStringById(R.string.terminal_bind_speaker));
        titleBar.setLeftButtonVisible(true);
    }

    public void bindSpeaker(String deviceName){
        UserManager.BindSpeaker(deviceName,new UINotifyListener<Boolean>(){
            @Override
            public void onError(Object object) {
                super.onError(object);
                dismissLoading();
                if (checkSession()) {
                    return;
                }
                if(object != null ){
                    toastDialog(MySpeakerBindActivity.this, object.toString(), new NewDialogInfo.HandleBtn() {
                        @Override
                        public void handleOkBtn() {
                            finish();
                            showPage(MySpeakerListActivity.class);
                        }
                    });
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showLoading(false, R.string.public_loading);
            }

            @Override
            protected void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(Boolean result) {
                super.onSucceed(result);
                dismissLoading();
                if(result){
                    checkWIFIState();
                }
            }
        });
    }

    public void unbindSpeaker(String userId,String deviceName){
        UserManager.UnbindSpeaker(userId,deviceName,new UINotifyListener<Boolean>(){
            @Override
            public void onError(Object object) {
                super.onError(object);
                dismissLoading();
                if (checkSession()) {
                    return;
                }
                if(object != null ){
                    toastDialog(MySpeakerBindActivity.this, object.toString(), null);
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showLoading(false, R.string.public_loading);
            }

            @Override
            protected void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(Boolean result) {
                super.onSucceed(result);
                dismissLoading();
                if(result){
                    //解绑成功，则finish当前页面，跳转到云播报列表页面，同时更新数据
                    finish();

                    Intent it = new Intent();
                    it.setClass(MySpeakerBindActivity.this, MySpeakerListActivity.class);
                    startActivity(it);
                }

            }
        });
    }
}
