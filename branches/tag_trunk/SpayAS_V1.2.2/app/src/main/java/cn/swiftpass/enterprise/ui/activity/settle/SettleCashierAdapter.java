/*
 * 文 件 名:  SettleCashierAdapter.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-9-23
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.settle;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.UserModel;

/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2016-9-23]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class SettleCashierAdapter extends BaseAdapter
{
    private ViewHolder holder;
    
    private List<UserModel> userModels;
    
    private Context context;
    
    private long userId = 0;
    
    public SettleCashierAdapter()
    {
    }
    
    public SettleCashierAdapter(List<UserModel> userModels, Context context, long userId)
    {
        this.userModels = userModels;
        
        this.context = context;
        
        this.userId = userId;
    }
    
    @Override
    public int getCount()
    {
        return userModels.size();
    }
    
    @Override
    public Object getItem(int position)
    {
        return userModels.get(position);
    }
    
    @Override
    public long getItemId(int position)
    {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            convertView = View.inflate(context, R.layout.settl_cashier_list_item, null);
            holder = new ViewHolder();
            holder.cashier_state = (TextView)convertView.findViewById(R.id.cashier_state);
            holder.cashier_store = (TextView)convertView.findViewById(R.id.cashier_store);
            holder.userId = (TextView)convertView.findViewById(R.id.userId);
            holder.userName = (TextView)convertView.findViewById(R.id.userName);
            holder.usertel = (TextView)convertView.findViewById(R.id.usertel);
            holder.iv_choice = (ImageView)convertView.findViewById(R.id.iv_choice);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder)convertView.getTag();
        }
        
        UserModel userModel = userModels.get(position);
        if (userModel.getDeptname() != null)
        {
            holder.cashier_store.setText(userModel.getDeptname());
        }
        
        //        if (userModel.getEnabled() != null && userModel.getEnabled().equals("true"))
        //        {
        //            holder.cashier_state.setText("(" + context.getString(R.string.open) + ")");
        //        }
        //        else
        //        {
        //            holder.cashier_state.setText("(" + context.getString(R.string.tx_frozen) + ")");
        //        }
        holder.userId.setText(String.valueOf(userModel.getId()));
        if (userModel.getPhone() != null)
        {
            holder.usertel.setText(userModel.getPhone());
        }
        
        if (userModel.getRealname() != null)
        {
            holder.userName.setText(userModel.getRealname());
        }
        
        if (userId > 0 && userModel.getId() == userId)
        {
            holder.iv_choice.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.iv_choice.setVisibility(View.GONE);
        }
        
        return convertView;
    }
    
    private class ViewHolder
    {
        private ImageView iv_choice;
        
        private TextView userId, cashier_state, userName, usertel, cashier_store;
        
    }
}
