package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.bussiness.model.ForgetPSWBean
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.FindPassFirstContract
import cn.swiftpass.enterprise.utils.JsonUtil
import okhttp3.Call


/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @我想在大地上画满窗子
 * @让所有习惯黑暗的眼睛
 * @都习惯光明
 */
class FindPassFirstPresenter : FindPassFirstContract.Presenter {


    private var mView: FindPassFirstContract.View? = null


    override fun checkForgetPwdInput(input: String?) {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
            AppClient.checkForgetPwdInputAndGetCode(
                input,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<ForgetPSWBean>(ForgetPSWBean()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.checkForgetPwdInputFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val dynModels = JsonUtil.jsonToBean(
                                response.message,
                                ForgetPSWBean::class.java
                            ) as ForgetPSWBean
                            view.checkForgetPwdInputSuccess(dynModels)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: FindPassFirstContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}