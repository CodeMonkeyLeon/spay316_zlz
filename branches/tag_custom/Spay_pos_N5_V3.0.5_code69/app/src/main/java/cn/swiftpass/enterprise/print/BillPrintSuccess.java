/*
 * 文 件 名:  BillPrint.java
 * 描    述:  <描述>
 * 修 改 人:  admin
 * 修改时间:  2017-3-23
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.print;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;

import com.nexgo.oaf.apiv3.device.printer.AlignEnum;
import com.nexgo.oaf.apiv3.device.printer.OnPrintListener;
import com.nexgo.oaf.apiv3.device.printer.Printer;

import java.util.Locale;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * <一句话功能简述> <功能详细描述>
 * 
 * @author admin
 * @version [版本号, 2017-3-23]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class BillPrintSuccess
{

    private final int FONT_SIZE_SMALL = 15;

    private final static int FONT_SIZE_NORMAL = 21;

    private final static int FONT_SIZE_BIG = 30;
    /**
     * <一句话功能简述> <功能详细描述>

     * @param orderModel
     * @param printer
     * @see [类、类#方法、类#成员]
     */
    public static void print(boolean isCashierShow, Order orderModel, Printer printer, Bitmap bitmap)
    {
        Bitmap logoBitmap = null;

        if (!TextUtils.isEmpty(orderModel.getApiCode())){
            if (orderModel.getApiCode().equals("1")){//微信
                logoBitmap = BitmapFactory.decodeResource(MainApplication.getContext().getResources(), R.drawable.wechat_pint, null);
            }else if (orderModel.getApiCode().equals("2")){//支付宝
                logoBitmap = BitmapFactory.decodeResource(MainApplication.getContext().getResources(), R.drawable.alipay_pint, null);
            }
        }
        /**
         *  打印图片
         */
        try
        {
            if (logoBitmap != null)
            {
                printer.appendImage(logoBitmap, AlignEnum.CENTER);
                printer.appendPrnStr(" ", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
            }
        }
        catch (Exception e)
        {
        }


        if(orderModel.isPay()){
            printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_pay_note), FONT_SIZE_BIG, AlignEnum.CENTER, false);
    	}else {
    		 printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_refund_note), FONT_SIZE_BIG, AlignEnum.CENTER, false);
		}
        printer.appendPrnStr(" ", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);

        printer.appendPrnStr(orderModel.getPartner(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        printer.appendPrnStr(ToastHelper.toStr(R.string.tv_pay_client_save), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        printer.appendPrnStr("================================", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
        printer.appendPrnStr(ToastHelper.toStr(R.string.shop_name) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        printer.appendPrnStr(MainApplication.getMchName(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "：" + MainApplication.getMchId(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

        if(orderModel.isPay()){
            if (MainApplication.isAdmin.equals("0") && !isCashierShow) { //收银员
                printer.appendPrnStr(ToastHelper.toStr(R.string.tx_user) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                printer.appendPrnStr( MainApplication.realName , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            }

            if (isCashierShow){
                printer.appendPrnStr(ToastHelper.toStr(R.string.tx_user) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                printer.appendPrnStr(orderModel.getUserName(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            }

            try {
                printer.appendPrnStr(ToastHelper.toStr(R.string.tx_bill_stream_time) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                printer.appendPrnStr(orderModel.getAddTimeNew(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            } catch (Exception e) {
                printer.appendPrnStr(ToastHelper.toStr(R.string.tx_bill_stream_time) + "：" , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                printer.appendPrnStr(orderModel.getTradeTimeNew(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            }

        }else {//退款
            printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_refund_time) + "：" , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            printer.appendPrnStr(orderModel.getAddTimeNew(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            printer.appendPrnStr(ToastHelper.toStr(R.string.refund_odd_numbers) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            printer.appendPrnStr(orderModel.getRefundNo(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        }

        printer.appendPrnStr(ToastHelper.toStr(R.string.tx_order_no) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        printer.appendPrnStr(orderModel.getOrderNoMch(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

        if (orderModel.isPay()) {
            if (MainApplication.getPayTypeMap() != null && MainApplication.getPayTypeMap().size() > 0) {
                printer.appendPrnStr(MainApplication.getPayTypeMap().get(orderModel.getApiCode())+ " " + ToastHelper.toStr(R.string.tx_orderno) + ":", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            }
            printer.appendPrnStr(orderModel.getTransactionId() , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            printer.appendPrnStr(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "："  , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            printer.appendPrnStr(orderModel.getTradeName()  , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            printer.appendPrnStr(ToastHelper.toStr(R.string.tx_bill_stream_statr) + "："   , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            printer.appendPrnStr( MainApplication.getTradeTypeMap().get(orderModel.getTradeState() + ""), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

            if (!StringUtil.isEmptyOrNull(orderModel.getAttach())) {
                printer.appendPrnStr(ToastHelper.toStr(R.string.tx_bill_stream_attach) + "：" + orderModel.getAttach() , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            }

            if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
                if (orderModel.getDaMoney() > 0) {
                    long actualMoney = orderModel.getDaMoney() + orderModel.getOrderFee();
                    printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_money) + "：" , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                    printer.appendPrnStr(DateUtil.formatMoneyUtils(actualMoney) + ToastHelper.toStr(R.string.pay_yuan), FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);
                } else {
                    printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_money) + "：" , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                    printer.appendPrnStr(DateUtil.formatMoneyUtils(orderModel.getOrderFee()) + ToastHelper.toStr(R.string.pay_yuan), FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);

                }
            } else {
                if (MainApplication.isSurchargeOpen()) {
                    if(!orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") &&  !orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")){
                        printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_money) + "：" , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                        printer.appendPrnStr(MainApplication.feeType +" "+ DateUtil.formatMoneyUtils(orderModel.getOrderFee()), FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);

                        printer.appendPrnStr(ToastHelper.toStr(R.string.tx_surcharge) + "：" , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                        printer.appendPrnStr( MainApplication.feeType+" " + DateUtil.formatMoneyUtils(orderModel.getSurcharge()), FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);

                    }
                }

                //Uplan ---V3.0.5迭代新增

                //如果为预授权的支付类型，则不展示任何优惠相关的信息
                /*因目前SPAY與網關支持的預授權接口僅為‘支付寶’預授權，
                遂與銀聯Uplan不衝突，遂預授權頁面內，無需處理該優惠信息。*/
                if(!TextUtils.isEmpty(orderModel.getTradeType()) && !orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") &&  !orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")){
                    // 优惠详情 --- 如果列表有数据，则展示，有多少条，展示多少条
                    if(orderModel.getUplanDetailsBeans() != null && orderModel.getUplanDetailsBeans().size() > 0){
                        for(int i =0 ; i<orderModel.getUplanDetailsBeans().size() ; i++){
                            String string_Uplan_details;
                            //如果是Uplan Discount或者Instant Discount，则采用本地词条，带不同语言的翻译
                            if(orderModel.getUplanDetailsBeans().get(i).getDiscountNote().equalsIgnoreCase("Uplan discount")){
                                string_Uplan_details = ToastHelper.toStr(R.string.uplan_Uplan_discount) + "：";
                            }else if(orderModel.getUplanDetailsBeans().get(i).getDiscountNote().equalsIgnoreCase("Instant Discount")){
                                string_Uplan_details = ToastHelper.toStr(R.string.uplan_Uplan_instant_discount) + "：";
                            } else{//否则，后台传什么展示什么
                                string_Uplan_details = orderModel.getUplanDetailsBeans().get(i).getDiscountNote()+ "：";
                            }

                            printer.appendPrnStr(string_Uplan_details, FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                            printer.appendPrnStr( MainApplication.feeType+ " "+ "-" + orderModel.getUplanDetailsBeans().get(i).getDiscountAmt(), FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);
                        }
                    }

                    // 消费者实付金额 --- 如果不为0 ，就展示
                    if(orderModel.getCostFee() != 0){
                        printer.appendPrnStr(ToastHelper.toStr(R.string.uplan_Uplan_actual_paid_amount) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                        printer.appendPrnStr( MainApplication.feeType + " "+ DateUtil.formatMoneyUtils(orderModel.getCostFee()), FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);
                    }
                }

                printer.appendPrnStr(ToastHelper.toStr(R.string.tv_charge_total) + "：" , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                printer.appendPrnStr(MainApplication.feeType+" " + DateUtil.formatMoneyUtils(orderModel.getTotalFee()), FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);

                if (orderModel.getCashFeel() > 0) {
                    printer.appendPrnStr(ToastHelper.toStr(R.string.pay_yuan)+ DateUtil.formatMoneyUtils(orderModel.getCashFeel()) , FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);
                }
            }
        } else {
            printer.appendPrnStr(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "："+ "\n" + orderModel.getTradeName() , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            if (!StringUtil.isEmptyOrNull(orderModel.getUserName())) {
                printer.appendPrnStr(ToastHelper.toStr(R.string.tv_refund_peop) + "："+ "\n" + orderModel.getUserName() , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            }
            if (MainApplication.getRefundStateMap() != null && MainApplication.getRefundStateMap().size() > 0) {

                printer.appendPrnStr(ToastHelper.toStr(R.string.tv_refund_state) + "："  , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                printer.appendPrnStr(MainApplication.getRefundStateMap().get(orderModel.getRefundState() + "")  , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            }
            if (MainApplication.feeFh.equalsIgnoreCase("¥")&& MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
                printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_money) + "：" , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                printer.appendPrnStr(DateUtil.formatMoneyUtils(orderModel.getTotalFee()) + ToastHelper.toStr(R.string.pay_yuan) , FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);

                printer.appendPrnStr(ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "："  , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                printer.appendPrnStr( DateUtil.formatMoneyUtils(orderModel.getRefundMoney()) + ToastHelper.toStr(R.string.pay_yuan) , FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);

            } else {
//                    printBuffer.append(ToastHelper.toStr(R.string.tx_blue_print_money) + "：" + MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getOrderFee()) + "\n");


                    /*if (MainApplication.isSurchargeOpen()) {
                        printBuffer.append(ToastHelper.toStr(R.string.tx_surcharge) + "：" + MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getSurcharge()) + "\n");
                    }*/

                printer.appendPrnStr(ToastHelper.toStr(R.string.tv_charge_total) + "：" , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                printer.appendPrnStr(MainApplication.feeType+" " + DateUtil.formatMoneyUtils(orderModel.getTotalFee()), FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);

                printer.appendPrnStr(ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "：" , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                printer.appendPrnStr(MainApplication.feeType +" "+ DateUtil.formatMoneyUtils(orderModel.getRefundMoney()), FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);

                if(orderModel.getCashFeel() > 0){
                    printer.appendPrnStr(ToastHelper.toStr(R.string.pay_yuan)+ DateUtil.formatMoneyUtils(orderModel.getCashFeel()) , FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);
                }
            }
            if (!StringUtil.isEmptyOrNull(orderModel.getPrintInfo())) {
                printer.appendPrnStr(orderModel.getPrintInfo()  , FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            }
        }
        printer.appendPrnStr(" ", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);

        if(!StringUtil.isEmptyOrNull(orderModel.getOrderNoMch()) && orderModel.isPay()){
            try
            {
                if (bitmap != null)
                {
                    printer.appendImage(bitmap, AlignEnum.CENTER);
                    printer.appendPrnStr( "\n"+ ToastHelper.toStr(R.string.refound_QR_code), FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
                }
            }
            catch (Exception e)
            {
            }
        }

        printer.appendPrnStr("------------------------------------------", FONT_SIZE_BIG, AlignEnum.CENTER, false);
        printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        if (orderModel.getPartner().equalsIgnoreCase(ToastHelper.toStr(R.string.tv_pay_mch_stub))
                || orderModel.getPartner().equalsIgnoreCase(ToastHelper.toStr(R.string.tv_pay_user_stub))) {
            printer.appendPrnStr(" ", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
            printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_sign) + "：",FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            printer.appendPrnStr(" ", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
            //                printBuffer.append("备注：\n\n\n");
        } else {
            printer.appendPrnStr(" ", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
            printer.appendPrnStr(" ", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
            printer.appendPrnStr(" ", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
        }


        //		printer.appendPrnStr(" ", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        
        //Bitmap b = CreateOneDiCodeUtilPrint.createCode(orderModel.getOrderNoMch(), 260, 260);
        //        

        printer.startPrint(true, new OnPrintListener()
        {
            @Override
            public void onPrintResult(final int retCode)
            {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Log.i("hehui", "retCode-->" + retCode);
                    }
                });
            }
            
            private void runOnUiThread(Runnable runnable)
            {
                
            }
        });
        
    }

    /**
     * printType参数1：预授权成功---预授权小票(反扫)
     *  * printType参数5：预授权成功---预授权小票(正扫)
     * printType参数4：预授权订单详情---预授权小票
     * printType参数2：预授权解冻详情小票
     * printType参数3：预授权解冻完成小票
     */
    public static void printPreAuthStr(Printer printer, int printType, Order orderModel){
        int minus=1;
        //根据当前的小数点的位数来判断应该除以或者乘以多少
        for(int i = 0; i < MainApplication.numFixed ; i++ ){
            minus = minus * 10;
        }
        Bitmap logoBitmap = BitmapFactory.decodeResource(MainApplication.getContext().getResources(), R.drawable.alipay_pint, null);;

        /**
         *  打印图片
         */
        try
        {
            if (logoBitmap != null)
            {
                printer.appendImage(logoBitmap, AlignEnum.CENTER);
                printer.appendPrnStr(" ", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
            }
        } catch (Exception e) {
        }


        switch (printType){
            case 1:
            case 4:
            case 5:
                printer.appendPrnStr(ToastHelper.toStr(R.string.pre_auth_receipt), FONT_SIZE_BIG, AlignEnum.CENTER, false);
                break;

            case 2:
            case 3:
                printer.appendPrnStr(ToastHelper.toStr(R.string.pre_auth_unfreezed_receipt), FONT_SIZE_BIG, AlignEnum.CENTER, false);
                break;
        }

        printer.appendPrnStr(orderModel.getPartner(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        printer.appendPrnStr(ToastHelper.toStr(R.string.tv_pay_client_save), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        printer.appendPrnStr("================================", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);

        if (!StringUtil.isEmptyOrNull(MainApplication.getMchName())) {
            printer.appendPrnStr(ToastHelper.toStr(R.string.shop_name) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            printer.appendPrnStr(MainApplication.getMchName(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        }
        if (!StringUtil.isEmptyOrNull(MainApplication.getMchId())) {
            printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "：" + MainApplication.getMchId(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

        }

        if (printType==1) {
            printer.appendPrnStr(ToastHelper.toStr(R.string.pre_auth_time) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            printer.appendPrnStr(orderModel.getTradeTime(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        }else if(printType==4){
            printer.appendPrnStr(ToastHelper.toStr(R.string.pre_auth_time) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            printer.appendPrnStr(orderModel.getTradeTimeNew(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

        }else if(printType==5){
            printer.appendPrnStr(ToastHelper.toStr(R.string.pre_auth_time) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            printer.appendPrnStr(orderModel.getTimeEnd(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

        } else if(printType==2){
            printer.appendPrnStr(ToastHelper.toStr(R.string.unfreezed_time) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            printer.appendPrnStr(orderModel.getOperateTime(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

        }else if(printType==3){
            printer.appendPrnStr(ToastHelper.toStr(R.string.unfreezed_time) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            printer.appendPrnStr(orderModel.getUnFreezeTime(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        }

        if (printType==1 || printType == 4 || printType == 5){
            //平台预授权订单号
            printer.appendPrnStr(ToastHelper.toStr(R.string.platform_pre_auth_order_id) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            printer.appendPrnStr(orderModel.getAuthNo(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

            String language = PreferenceUtil.getString("language","");
            Locale locale = MainApplication.getContext().getResources().getConfiguration().locale; //zh-rHK
            String lan = Locale.getDefault().toString();
            if (!TextUtils.isEmpty(language)) {
                if (language.equals(MainApplication.LANG_CODE_EN_US)) {
                    //支付宝单号
                    if(printType==1 || printType == 5){
                        printer.appendPrnStr(MainApplication.getPayTypeMap().get("2") + " "+ ToastHelper.toStr(R.string.tx_orderno) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                        printer.appendPrnStr(orderModel.getOutTransactionId(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                    }

                    if(printType== 4){
                        printer.appendPrnStr(MainApplication.getPayTypeMap().get("2") + " "+ ToastHelper.toStr(R.string.tx_orderno) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                        printer.appendPrnStr(orderModel.getTransactionId(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

                    }

                } else {
                    //支付宝单号
                    if(printType==1 || printType == 5){
                        printer.appendPrnStr(MainApplication.getPayTypeMap().get("2")+ ToastHelper.toStr(R.string.tx_orderno) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                        printer.appendPrnStr(orderModel.getOutTransactionId(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

                    }

                    if(printType== 4){
                        printer.appendPrnStr(MainApplication.getPayTypeMap().get("2") + ToastHelper.toStr(R.string.tx_orderno) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                        printer.appendPrnStr(orderModel.getTransactionId(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

                    }
                }
            } else {
                if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_EN_US) ) {
                    //支付宝单号
                    if(printType==1 || printType == 5){
                        printer.appendPrnStr(MainApplication.getPayTypeMap().get("2") + " "+ ToastHelper.toStr(R.string.tx_orderno) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                        printer.appendPrnStr(orderModel.getOutTransactionId(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

                    }

                    if(printType== 4){
                        printer.appendPrnStr(MainApplication.getPayTypeMap().get("2") + " "+ ToastHelper.toStr(R.string.tx_orderno) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                        printer.appendPrnStr(orderModel.getTransactionId(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

                    }

                } else {
                    //支付宝单号
                    if(printType==1 || printType == 5){
                        printer.appendPrnStr(MainApplication.getPayTypeMap().get("2")+ ToastHelper.toStr(R.string.tx_orderno) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                        printer.appendPrnStr(orderModel.getOutTransactionId(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

                    }

                    if(printType== 4){
                        printer.appendPrnStr(MainApplication.getPayTypeMap().get("2") + ToastHelper.toStr(R.string.tx_orderno) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                        printer.appendPrnStr(orderModel.getTransactionId(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

                    }
                }
            }


        }else if(printType == 2 || printType== 3){
            //解冻订单号
            try {
                printer.appendPrnStr(ToastHelper.toStr(R.string.unfreezed_order_id) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                printer.appendPrnStr(orderModel.getOutRequestNo(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            } catch (Exception e) {
            }
            //平台预授权订单号
            try {
                printer.appendPrnStr(ToastHelper.toStr(R.string.platform_pre_auth_order_id) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                printer.appendPrnStr(orderModel.getAuthNo(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            } catch (Exception e) {
            }
        }

        if(printType == 5){
            String tradename;
            if (!isAbsoluteNullStr(orderModel.getTradeName())){//先判断大写的Name，再判断小写name
                tradename=orderModel.getTradeName();
            }else {
                tradename=orderModel.getTradename();
            }
            try {
                printer.appendPrnStr(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                printer.appendPrnStr(tradename, FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            } catch (Exception e) {
            }

        }else{
            try {
                printer.appendPrnStr(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                printer.appendPrnStr(orderModel.getTradeName(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            } catch (Exception e) {
            }
        }

        if (printType == 3) {
            try {
                printer.appendPrnStr(ToastHelper.toStr(R.string.tv_unfreezen_applicant) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                printer.appendPrnStr(orderModel.getUserName(), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            } catch (Exception e) {
            }
        }

        //订单状态
        String operationType = "";
        if(printType == 1 || printType == 4){
            operationType = MainApplication.getPreAuthtradeStateMap().get(orderModel.getTradeState() + "");

        }else if(printType == 2){
            switch (orderModel.getOperationType()){
                case 2:
                    operationType = ToastHelper.toStr(R.string.freezen_success);
                    break;
            }
        }else if(printType == 3){
            operationType = ToastHelper.toStr(R.string.unfreezing);
        }else if (printType == 5){
            operationType = ToastHelper.toStr(R.string.pre_auth_authorized);
        }

        try {
            printer.appendPrnStr(ToastHelper.toStr(R.string.tx_bill_stream_statr) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
            printer.appendPrnStr(operationType, FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        } catch (Exception e) {
        }

        switch (printType){
            case 1:
            case 4:
            case 5:
                if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))){
                    printer.appendPrnStr(ToastHelper.toStr(R.string.pre_auth_amount) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                    printer.appendPrnStr(DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan), FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);
                }else{
                    long preAuthorizationMoney=0;//用来接收printType判断授权金额来自反扫、正扫
                    if (printType==1){
                        preAuthorizationMoney=orderModel.getRestAmount();//反扫
                    }else if (printType==5){
                        preAuthorizationMoney=orderModel.getTotalFee();//正扫
                    }else if(printType==4){
                        preAuthorizationMoney=orderModel.getTotalFreezeAmount();//预授权详情
                    }
                    //预授权金额
                    printer.appendPrnStr(ToastHelper.toStr(R.string.pre_auth_amount) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                    printer.appendPrnStr(MainApplication.feeType +" "+ DateUtil.formatMoneyUtils(preAuthorizationMoney), FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);

                }
                printer.appendPrnStr(" ", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
                //二维码
                //打印小票的代码

                if(!StringUtil.isEmptyOrNull(orderModel.getAuthNo())){
                    try
                    {
                        final Bitmap bitmap = CreateOneDiCodeUtilPrint.createCode(orderModel.getAuthNo(), Integer.valueOf(260), Integer.valueOf(260));
                        if (bitmap != null)
                        {
                            printer.appendImage(bitmap, AlignEnum.CENTER);
                            //此二维码用于预授权收款、解冻操作
                            printer.appendPrnStr(ToastHelper.toStr(R.string.scan_to_check), FONT_SIZE_NORMAL, AlignEnum.CENTER, false);                        }
                    }
                    catch (Exception e)
                    {
                    }
                }
                break;
            case 2:
                if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))){
                    printer.appendPrnStr(ToastHelper.toStr(R.string.unfreezing_amount)+":", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                    printer.appendPrnStr(DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan), FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);

                }else{
                    printer.appendPrnStr(ToastHelper.toStr(R.string.unfreezing_amount)+":", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                    printer.appendPrnStr(MainApplication.feeType+" "  + DateUtil.formatMoneyUtils(orderModel.getMoney()), FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);

                }
                //解冻将原路返回支付账户或银行卡，到账时间已第三方为准
                printer.appendPrnStr(ToastHelper.toStr(R.string.unfreeze), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                break;
            case 3:
                //授权金额
                if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))){
                    printer.appendPrnStr(ToastHelper.toStr(R.string.pre_auth_amount)+":", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                    printer.appendPrnStr(DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan), FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);

                }else{
                    printer.appendPrnStr(ToastHelper.toStr(R.string.pre_auth_amount)+":", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                    printer.appendPrnStr(MainApplication.feeType +" " + DateUtil.formatMoneyUtils(orderModel.getTotalFreezeAmount()), FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);

                }
                //解冻金额
                if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))){
                    printer.appendPrnStr(ToastHelper.toStr(R.string.unfreezing_amount)+":", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                    printer.appendPrnStr(DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan), FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);

                }else{
                    printer.appendPrnStr(ToastHelper.toStr(R.string.unfreezing_amount)+":", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
                    printer.appendPrnStr(MainApplication.feeType +" "+ DateUtil.formatMoneyUtils(orderModel.getTotalFee()), FONT_SIZE_NORMAL, AlignEnum.RIGHT, false);

                }
                //解冻将原路返回支付账户或银行卡，到账时间已第三方为准
                printer.appendPrnStr(ToastHelper.toStr(R.string.unfreeze), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);

                break;
            default:
                break;
        }

        printer.appendPrnStr("------------------------------------------", FONT_SIZE_BIG, AlignEnum.CENTER, false);
        printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()), FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        printer.appendPrnStr(" ", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);
        printer.appendPrnStr(ToastHelper.toStr(R.string.tx_blue_print_sign) + "：", FONT_SIZE_NORMAL, AlignEnum.LEFT, false);
        printer.appendPrnStr(" ", FONT_SIZE_NORMAL, AlignEnum.CENTER, false);

        printer.startPrint(true, new OnPrintListener()
        {
            @Override
            public void onPrintResult(final int retCode)
            {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Log.i("hehui", "retCode-->" + retCode);
                    }
                });
            }

            private void runOnUiThread(Runnable runnable)
            {

            }
        });

    }

    public static boolean isAbsoluteNullStr(String str) {
        str = deleteBlank(str);
        if (str == null || str.length() == 0 || "null".equalsIgnoreCase(str)) {
            return true;
        }

        return false;
    }
    /**
     * 去前后空格和去换行符
     *
     * @param str
     * @return
     */
    public static String deleteBlank(String str) {
        if (null != str && 0 < str.trim().length()) {
            char[] array = str.toCharArray();
            int start = 0, end = array.length - 1;
            while (array[start] == ' ') start++;
            while (array[end] == ' ') end--;
            return str.substring(start, end + 1).replaceAll("\n", "");

        } else {
            return "";
        }

    }

}
