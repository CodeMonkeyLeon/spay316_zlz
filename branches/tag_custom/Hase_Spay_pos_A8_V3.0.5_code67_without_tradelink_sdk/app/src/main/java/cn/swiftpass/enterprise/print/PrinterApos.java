package cn.swiftpass.enterprise.print;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;

import com.landicorp.android.eptapi.device.Printer;
import com.landicorp.android.eptapi.device.Printer.Alignment;
import com.landicorp.android.eptapi.device.Printer.Format;
import com.landicorp.android.eptapi.exception.RequestException;
import com.landicorp.android.eptapi.utils.QrCode;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Locale;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * 新大陆 N900打印 <一句话功能简述> <功能详细描述>
 * 
 * @author he_hui
 * @version [版本号, 2016-2-4]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public abstract class PrinterApos {

	private Context context;

	private StaticLayout mLayout = null;

	private TextPaint mPaint;

	private Canvas mCanvas;

	private String content = null;

	private Printer.Progress progress = new Printer.Progress() {

		@Override
		public void doPrint(Printer arg0) throws Exception {

		}

		@Override
		public void onFinish(int code) {
			if (code == Printer.ERROR_NONE) {
				displayPrinterInfo("PRINT SUCCESS END ");
			}
			/**
			 * Has some error. Here is display it, but you may want to hanle the
			 * error such as ERROR_OVERHEAT、ERROR_BUSY、ERROR_PAPERENDED to start
			 * again in the right time later.
			 */
			else {
				displayPrinterInfo("PRINT ERR - " + (code));
			}
		}

		@Override
		public void onCrash() {
			onDeviceServiceCrash();
		}

	};

	/**
	 * Search card and show all track info
	 */
	public void startPrint() {
		try {
			progress.start();
			// DeviceService.logout();
		} catch (RequestException e) {
			onDeviceServiceCrash();
		}
	}

	protected abstract void displayPrinterInfo(String info);

	protected abstract void onDeviceServiceCrash();

	public PrinterApos(Context context, final boolean isCashierShow, final Bitmap bitmapLogo, final Order orderModel)
    {
        this.context = context;

        progress.addStep(new Printer.Step()
        {

            @Override
            public void doPrint(Printer printer)
                throws Exception
            {
              /*  printer.setAutoTrunc(true);

                // Default mode is real mode, now set it to virtual mode.
                printer.setMode(Printer.MODE_REAL);*/
                printer.setAutoTrunc(false);
                printer.setPageSpace(1500);

                /**
                 *  打印图片
                 */
            /*    if (null != bitmapLogo){
                    byte[] bytes = Bitmap2Bytes(bitmapLogo);
                    InputStream bitmapStream = BytesToInStream(bytes);
                    printer.printImage(0,300,40,bytes);
//                    printer.printImage(Alignment.CENTER,bitmapStream);
                }*/

                Format format = new Format();
                format.setAscSize(Format.ASC_DOT24x12);
                format.setAscScale(Format.ASC_SC1x2);
                format.setHzSize(Format.HZ_DOT24x24);
                format.setHzScale(Format.HZ_SC1x2);
                printer.setFormat(format);

                if (!orderModel.isPay()) {//退款
                    printer.printText(Alignment.CENTER,ToastHelper.toStr(R.string.tx_blue_print_refund_note) + "\n");
                }else {
                    printer.printText(Alignment.CENTER,ToastHelper.toStr(R.string.tx_blue_print_pay_note) + "\n");
                }

                format.setAscSize(Format.ASC_DOT24x12);
                format.setAscScale(Format.ASC_SC1x1);
                format.setHzSize(Format.HZ_DOT24x24);
                format.setHzScale(Format.HZ_SC1x1);
                printer.setFormat(format);

                printer.printText("\n" + orderModel.getPartner() +"\n" + ToastHelper.toStr(R.string.tv_pay_client_save));
                printer.printText("\n===============================\n");
                printer.printText(ToastHelper.toStr(R.string.shop_name) + "："+"\n"+ MainApplication.getMchName() + "\n");
                printer.printText(ToastHelper.toStr(R.string.tx_blue_print_merchant_no) + "：" + MainApplication.getMchId() + "\n");


                if (!orderModel.isPay()) {//退款
                    printer.printText(ToastHelper.toStr(R.string.tx_blue_print_refund_time) + "：" + "\n"+ orderModel.getAddTimeNew() + "\n");
                    printer.printText(ToastHelper.toStr(R.string.refund_odd_numbers) + "：\n");
                    printer.printText(orderModel.getRefundNo() + "\n");
                } else {

                    if (MainApplication.isAdmin.equals("0") && !isCashierShow) { //收银员
                        printer.printText(ToastHelper.toStr(R.string.tx_user) + "：" + MainApplication.realName + "\n");
                    }
                    if (isCashierShow){
                        printer.printText(ToastHelper.toStr(R.string.tx_user) + "：" + orderModel.getUserName() + "\n");
                    }
                    try {
                        printer.printText(ToastHelper.toStr(R.string.tx_bill_stream_time) + "："+ "\n" + orderModel.getAddTimeNew() + "\n");
                    } catch (Exception e) {
                        printer.printText(ToastHelper.toStr(R.string.tx_bill_stream_time) + "："+ "\n" + orderModel.getAddTimeNew() + "\n");
                    }
                }
                printer.printText(ToastHelper.toStr(R.string.tx_order_no) + "：\n");
                printer.printText(orderModel.getOrderNoMch() + "\n");

                if (orderModel.isPay()) {
                    if (MainApplication.getPayTypeMap() != null && MainApplication.getPayTypeMap().size() > 0) {
                        String language  = PreferenceUtil.getString("language", "");;
                        //zh-rHK
                        Locale locale = MainApplication.getContext().getResources().getConfiguration().locale;
                        String lan = locale.getCountry();
                        if (!TextUtils.isEmpty(language)) {
                            if (language.equals(MainApplication.LANG_CODE_EN_US)) {

                                printer.printText(MainApplication.getPayTypeMap().get(orderModel.getApiCode())+ " " +
                                        ToastHelper.toStr(R.string.tx_orderno) + ":\n");
                            } else {
                                printer.printText(MainApplication.getPayTypeMap().get(orderModel.getApiCode())+
                                        ToastHelper.toStr(R.string.tx_orderno) + ":\n");
                            }
                        } else {
                            if (lan.equalsIgnoreCase("en")) {
                                printer.printText(MainApplication.getPayTypeMap().get(orderModel.getApiCode())+ " " +
                                        ToastHelper.toStr(R.string.tx_orderno) + ":\n");
                            } else {
                                printer.printText(MainApplication.getPayTypeMap().get(orderModel.getApiCode())+
                                        ToastHelper.toStr(R.string.tx_orderno) + ":\n");
                            }

                        }

                    }


                    printer.printText(orderModel.getTransactionId() + "\n");

                    printer.printText(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "：" + "\n"+ orderModel.getTradeName() + "\n");
                    printer.printText(ToastHelper.toStr(R.string.tx_bill_stream_statr) + "：" + "\n"+ MainApplication.getTradeTypeMap().get(orderModel.getTradeState() + "") + "\n");

                    if (!StringUtil.isEmptyOrNull(orderModel.getAttach())) {
                        printer.printText(ToastHelper.toStr(R.string.tx_bill_stream_attach) + "：" + orderModel.getAttach() + "\n");
                    }

                    if (MainApplication.feeFh.equalsIgnoreCase("¥")  && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
                        if (orderModel.getDaMoney() > 0) {
                            long actualMoney = orderModel.getDaMoney() + orderModel.getOrderFee();
                            printer.printText(Alignment.LEFT,ToastHelper.toStr(R.string.tx_blue_print_money) + "："  + "\n");
                            printer.printText(Alignment.RIGHT, DateUtil.formatRMBMoneyUtils(actualMoney) + ToastHelper.toStr(R.string.pay_yuan) + "\n");

                        } else {
                          printer.printText(Alignment.LEFT,ToastHelper.toStr(R.string.tx_blue_print_money) + "：" + "\n");
                          printer.printText(Alignment.RIGHT,DateUtil.formatRMBMoneyUtils(orderModel.getOrderFee()) + ToastHelper.toStr(R.string.pay_yuan) + "\n");

                        }
                    }else {
                        if (MainApplication.isSurchargeOpen()) {
                            if(!orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") &&  !orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")){
                                printer.printText(Alignment.LEFT,ToastHelper.toStr(R.string.tx_blue_print_money) + "：" + "\n");
                                printer.printText(Alignment.RIGHT,MainApplication.feeType+ " " + DateUtil.formatMoneyUtils(orderModel.getOrderFee()) + "\n");

                                printer.printText(Alignment.LEFT,ToastHelper.toStr(R.string.tx_surcharge) + "："+ "\n");
                                printer.printText(Alignment.RIGHT,MainApplication.feeType+ " " + DateUtil.formatMoneyUtils(orderModel.getSurcharge())+ "\n");
                            }
                     }

                        //Uplan ---V3.0.5迭代新增

                        //如果为预授权的支付类型，则不展示任何优惠相关的信息
                    /*因目前SPAY與網關支持的預授權接口僅為‘支付寶’預授權，
                    遂與銀聯Uplan不衝突，遂預授權頁面內，無需處理該優惠信息。*/
                        if(!TextUtils.isEmpty(orderModel.getTradeType()) && !orderModel.getTradeType().equals("pay.alipay.auth.micropay.freeze") &&  !orderModel.getTradeType().equals("pay.alipay.auth.native.freeze")){
                            // 优惠详情 --- 如果列表有数据，则展示，有多少条，展示多少条
                            if(orderModel.getUplanDetailsBeans() != null && orderModel.getUplanDetailsBeans().size() > 0){
                                for(int i =0 ; i<orderModel.getUplanDetailsBeans().size() ; i++){
                                    String string_Uplan_details;
                                    //如果是Uplan Discount或者Instant Discount，则采用本地词条，带不同语言的翻译
                                    if(orderModel.getUplanDetailsBeans().get(i).getDiscountNote().equalsIgnoreCase("Uplan discount")){
                                        string_Uplan_details = ToastHelper.toStr(R.string.uplan_Uplan_discount) + "：";
                                    }else if(orderModel.getUplanDetailsBeans().get(i).getDiscountNote().equalsIgnoreCase("Instant Discount")){
                                        string_Uplan_details = ToastHelper.toStr(R.string.uplan_Uplan_instant_discount) + "：";
                                    } else{//否则，后台传什么展示什么
                                        string_Uplan_details = orderModel.getUplanDetailsBeans().get(i).getDiscountNote()+ "：";
                                    }
                                    printer.printText(Alignment.LEFT,string_Uplan_details+ "\n");
                                    printer.printText(Alignment.RIGHT, MainApplication.feeType+ " "+ "-" + orderModel.getUplanDetailsBeans().get(i).getDiscountAmt()+ "\n");
                                }
                            }

                            // 消费者实付金额 --- 如果不为0 ，就展示
                            if(orderModel.getCostFee() != 0){
                                printer.printText(Alignment.LEFT,ToastHelper.toStr(R.string.uplan_Uplan_actual_paid_amount) + "："+ "\n");
                                printer.printText(Alignment.RIGHT, MainApplication.feeType + " "+ DateUtil.formatMoneyUtils(orderModel.getCostFee())+ "\n");

                            }
                        }

                        printer.printText(Alignment.LEFT,ToastHelper.toStr(R.string.tv_charge_total) + "："+ "\n");
                        printer.printText(Alignment.RIGHT, MainApplication.feeType  + " "+ DateUtil.formatMoneyUtils(orderModel.getTotalFee())+ "\n");

                        if (orderModel.getCashFeel() > 0) {
                            printer.printText(Alignment.RIGHT,ToastHelper.toStr(R.string.pay_yuan)+ DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel())+ "\n");
                        }
                    }
                }else {
                    printer.printText(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "："+ "\n" + orderModel.getTradeName() + "\n");
                    if (!StringUtil.isEmptyOrNull(orderModel.getUserName())) {
                        printer.printText(ToastHelper.toStr(R.string.tv_refund_peop) + "："+ "\n" + orderModel.getUserName() + "\n");
                    }
                    if (MainApplication.getRefundStateMap() != null && MainApplication.getRefundStateMap().size() > 0) {

                        printer.printText(ToastHelper.toStr(R.string.tv_refund_state) + "："+ "\n" + MainApplication.getRefundStateMap().get(orderModel.getRefundState() + "") + "\n");
                    }
                    if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))) {
                        printer.printText(Alignment.LEFT,ToastHelper.toStr(R.string.tx_blue_print_money) + "："+ "\n");
                        printer.printText(Alignment.RIGHT, DateUtil.formatRMBMoneyUtils(orderModel.getTotalFee()) + ToastHelper.toStr(R.string.pay_yuan)+ "\n");


                        printer.printText(Alignment.LEFT,ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "："+ "\n");
                        printer.printText(Alignment.RIGHT, DateUtil.formatRMBMoneyUtils(orderModel.getRefundMoney()) + ToastHelper.toStr(R.string.pay_yuan)+ "\n");

                    } else {
                        printer.printText(Alignment.LEFT,ToastHelper.toStr(R.string.tv_charge_total) + "："+ "\n");
                        printer.printText(Alignment.RIGHT, MainApplication.feeType +" "+ DateUtil.formatMoneyUtils(orderModel.getTotalFee())+ "\n");

                        printer.printText(Alignment.LEFT,ToastHelper.toStr(R.string.tx_bill_stream_refund_money) + "："+ "\n");
                        printer.printText(Alignment.RIGHT, MainApplication.feeType+" " + DateUtil.formatMoneyUtils(orderModel.getRefundMoney())+ "\n");

                       if(orderModel.getCashFeel() > 0){
                            printer.printText(Alignment.RIGHT, ToastHelper.toStr(R.string.pay_yuan)+ DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel())+ "\n");
                        }
                    }
                    if (!StringUtil.isEmptyOrNull(orderModel.getPrintInfo())) {
                        printer.printText(orderModel.getPrintInfo() + "\n");
                    }

                }

                if(!StringUtil.isEmptyOrNull(orderModel.getOrderNoMch()) && orderModel.isPay()){

                    printer.printQrCode(Alignment.CENTER,
                            new QrCode(orderModel.getOrderNoMch(), QrCode.ECLEVEL_Q),
                            184);
                    printer.printText(Alignment.CENTER,"\n"+ ToastHelper.toStr(R.string.refound_QR_code)+ "\n");
                }
                printer.printText("-------------------------------\r\n");
                printer.printText(ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\r\n");

                if (orderModel.getPartner().equalsIgnoreCase(ToastHelper.toStr(R.string.tv_pay_mch_stub))
                        || orderModel.getPartner().equalsIgnoreCase(ToastHelper.toStr(R.string.tv_pay_user_stub))) {
//                    printer.printText("\n"+ ToastHelper.toStr(R.string.tx_blue_print_sign) + "：\r\n");
                    printer.printText("\r\n\n");
                } else {
                    printer.printText("\r\n\n");
                }
                printer.printText("\r\n\n");
                printer.printText("\r\n\n");

            }
        });
    }


    /************************新增获取图片方法*******************************/
    public  byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos =new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);

        return baos.toByteArray();
    }

    public InputStream BytesToInStream(byte[] bytes)
    {
        InputStream is = new ByteArrayInputStream(bytes);
        return is;
    }
}
