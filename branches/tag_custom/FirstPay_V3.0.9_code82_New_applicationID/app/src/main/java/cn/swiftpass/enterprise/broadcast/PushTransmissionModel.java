package cn.swiftpass.enterprise.broadcast;

/**
 * 透传的消息体
 *
 */
public class PushTransmissionModel
{
    
    /**
     * 收银员颁奖
     */
    public static final int MESSAGE_KIND_CASHIER_WIN = 90000;
    
    /**
     * 收银员发奖
     */
    public static final int MESSAGE_KIND_CASHIER_CASH_OUT = 90001;
    
    public static final int MESSAGE_ORDER = 90002;
    
    /**
     * 文本格式
     */
    public static final int CONTENT_FORMAT_TEXT = 1;
    
    /**
     * JSON格式
     */
    public static final int CONTENT_FORMAT_JSON = 2;
    
    /**
     * 消息类型
     */
    private Integer messageKind;
    
    /**
     * 消息内容
     */
    private String content;
    
    /**
     * 内容格式
     */
    private Integer contentFormat = CONTENT_FORMAT_TEXT;
    
    private String voiceContent;
    
    private String title;
    
    /**
     * 生成时间
     */
    private Long createDate;
    
    /**
     * @return 返回 voiceContent
     */
    public String getVoiceContent()
    {
        return voiceContent;
    }
    
    /**
     * @param 对voiceContent进行赋值
     */
    public void setVoiceContent(String voiceContent)
    {
        this.voiceContent = voiceContent;
    }
    
    /**
     * @return 返回 title
     */
    public String getTitle()
    {
        return title;
    }
    
    /**
     * @param 对title进行赋值
     */
    public void setTitle(String title)
    {
        this.title = title;
    }
    
    public Integer getMessageKind()
    {
        return messageKind;
    }
    
    public void setMessageKind(Integer messageKind)
    {
        this.messageKind = messageKind;
    }
    
    public String getContent()
    {
        return content;
    }
    
    public void setContent(String content)
    {
        this.content = content;
    }
    
    public Integer getContentFormat()
    {
        return contentFormat;
    }
    
    public void setContentFormat(Integer contentFormat)
    {
        this.contentFormat = contentFormat;
    }
    
    public Long getCreateDate()
    {
        return createDate;
    }
    
    public void setCreateDate(Long createDate)
    {
        this.createDate = createDate;
    }
    
}
