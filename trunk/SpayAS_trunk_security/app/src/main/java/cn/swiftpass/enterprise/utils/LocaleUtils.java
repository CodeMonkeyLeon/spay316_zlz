package cn.swiftpass.enterprise.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.DisplayMetrics;

import com.example.common.sp.PreferenceUtil;

import java.util.Locale;

import cn.swiftpass.enterprise.common.Constant;
import cn.swiftpass.enterprise.intl.R;


/**
 * Created by congwei.li on 2021/12/21.
 *
 * @Description:
 */
public class LocaleUtils {

    public static String getLocaleLanguage() {
        String language = PreferenceUtil.getString("language", "");
        if (StringUtil.isEmptyOrNull(language)) {
            String lan = Locale.getDefault().toString();
            if (lan.equalsIgnoreCase(Constant.LANG_CODE_ZH_CN)
                    || lan.equalsIgnoreCase(Constant.LANG_CODE_ZH_CN_HANS)) {
                language = Constant.LANG_CODE_ZH_CN;
            } else if (lan.equalsIgnoreCase(Constant.LANG_CODE_ZH_HK)
                    || lan.equalsIgnoreCase(Constant.LANG_CODE_ZH_MO)
                    || lan.equalsIgnoreCase(Constant.LANG_CODE_ZH_TW)
                    || lan.equalsIgnoreCase(Constant.LANG_CODE_ZH_HK_HANT)) {
                language = Constant.LANG_CODE_ZH_TW;
            } else if (lan.equalsIgnoreCase(Constant.LANG_CODE_JA_JP)) {
                language = Constant.LANG_CODE_JA_JP;
            } else {
                language = Constant.LANG_CODE_EN_US;
            }
        }
        return language;
    }

    public static String getShowLanguage(Context context) {
        String lang = getLocaleLanguage();
        String showLanguage;
        if (lang.equalsIgnoreCase(Constant.LANG_CODE_ZH_CN)) {
            showLanguage = context.getResources().getString(R.string.lang_chinese_simplified);
        } else if (lang.equalsIgnoreCase(Constant.LANG_CODE_ZH_TW)) {
            showLanguage = context.getResources().getString(R.string.lang_chinese_traditional);
        } else if (lang.equalsIgnoreCase(Constant.LANG_CODE_JA_JP)) {
            showLanguage = context.getResources().getString(R.string.lang_japan);
        } else {
            showLanguage = context.getResources().getString(R.string.lang_english);
        }
        return showLanguage;
    }

    public static void switchLanguage(Context context) {
        switchLanguage(context, getLocaleLanguage());
    }

    public static void switchLanguage(Context context, String language) {
        //设置应用语言类型
        Resources resources = context.getResources();
        Configuration config = resources.getConfiguration();
        DisplayMetrics dm = resources.getDisplayMetrics();

        if (!TextUtils.isEmpty(language) && language.equals(Constant.LANG_CODE_ZH_CN)) {
            config.locale = Locale.SIMPLIFIED_CHINESE;
        } else if (!TextUtils.isEmpty(language) && language.equals(Constant.LANG_CODE_ZH_TW)) {
            config.locale = Locale.TRADITIONAL_CHINESE;
        } else if (!TextUtils.isEmpty(language) && language.equals(Constant.LANG_CODE_JA_JP)) {
            config.locale = Locale.JAPAN;
        } else if (!TextUtils.isEmpty(language) && language.equals(Constant.LANG_CODE_ZH_CN)) {
            config.locale = Locale.SIMPLIFIED_CHINESE;
        } else {
            config.locale = Locale.ENGLISH; //默认英文
        }

        if (!StringUtil.isEmptyOrNull(language)) {
            PreferenceUtil.commitString("language", language);
        }
        resources.updateConfiguration(config, dm);
    }

    /**
     * 判断当前语言环境是否是英文环境
     *
     * @return
     */
    public static boolean isEnglish() {
        String language = getLocaleLanguage();
        return isEnglish(language);
    }

    /**
     * 判断当前语言环境是否是英文环境
     *
     * @return
     */
    public static boolean isEnglish(String lang) {
        return !TextUtils.isEmpty(lang) && lang.startsWith(Constant.LANG_CODE_EN_US);
    }
}
