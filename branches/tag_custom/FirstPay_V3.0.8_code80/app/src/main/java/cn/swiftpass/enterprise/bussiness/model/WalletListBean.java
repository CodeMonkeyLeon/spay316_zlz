package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created by aijingya on 2020/5/21.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description:
 * @date 2020/5/21.19:22.
 */
public class WalletListBean  implements Serializable {
    public String imageUrl ; //钱包icon地址
    public String name; //钱包名称
    public String payloadCode; //钱包类型

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPayloadCode() {
        return payloadCode;
    }

    public void setPayloadCode(String payloadCode) {
        this.payloadCode = payloadCode;
    }
}
