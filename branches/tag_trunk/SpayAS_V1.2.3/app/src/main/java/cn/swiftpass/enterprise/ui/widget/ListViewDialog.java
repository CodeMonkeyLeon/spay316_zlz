package cn.swiftpass.enterprise.ui.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.adapter.KeyValueArrayAdapter;

public class ListViewDialog extends Dialog
{
    
    private Context mContext;
    
    private IResultCallBack callBack;
    
    public ListViewDialog(Context context, IResultCallBack callBack, KeyValueArrayAdapter adapter)
    {
        super(context, R.style.BaseDialog);
        //super(context);
        this.mContext = context;
        this.callBack = callBack;
        this.adapter = adapter;
    }
    
    private LinearLayout view;
    
    KeyValueArrayAdapter adapter;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        view = (LinearLayout)getLayoutInflater().inflate(R.layout.view_listview, null);
        setContentView(view);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        this.setCanceledOnTouchOutside(false);
        
        // data = mContext.getResources().getStringArray(R.array.arr_representing);
        ListView listView = (ListView)view.findViewById(R.id.listView);//android.R.layout.simple_list_item_1
        
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new OnItemClickListener()
        {
            
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int idx, long arg3)
            {
                if (callBack != null)
                {
                    callBack.getData(adapter.getItemId(idx), adapter.getItem(idx), ListViewDialog.this, idx);
                }
                ListViewDialog.this.dismiss();
            }
            
        });
    }
    
    @Override
    public void show()
    {
        // TODO Auto-generated method stub
        super.show();
    }
    
    public interface IResultCallBack
    {
        public void getData(long id, String text, Dialog dialog, int idx);
    }
}
