package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

import cn.swiftpass.enterprise.bussiness.enums.UserRole;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 *
 * User: Alan
 * Date: 13-9-21
 * Time: 下午1:57
 *
 */

public class UserModel implements Serializable
{
    @DatabaseField(generatedId = true)
    public long id;
    
    /**
     * @return 返回 id
     */
    public long getId()
    {
        return id;
    }
    
    /**
     * @param 对id进行赋值
     */
    public void setId(long id)
    {
        this.id = id;
    }
    
    private int pageCount = 0;
    
    /**
     * @return 返回 pageCount
     */
    public int getPageCount()
    {
        return pageCount;
    }
    
    /**
     * @param 对pageCount进行赋值
     */
    public void setPageCount(int pageCount)
    {
        this.pageCount = pageCount;
    }
    

    public long uId;
    

    public String name;
    

    public String pwd;
    

    public String realname;
    
    private String mchId;
    
    private int isMch;
    
    private String tel7;
    
    private String tel;
    
    private String userName;
    
    private String password;
    
    private String username;
    
    private String ee;
    
    private String emailCode;

    private boolean isselect;

    public void setIsselect(boolean isselect) {
        this.isselect = isselect;
    }

    public boolean isIsselect() {
        return isselect;
    }

    /**
     * @return 返回 emailCode
     */
    public String getEmailCode()
    {
        return emailCode;
    }
    
    /**
     * @param 对emailCode进行赋值
     */
    public void setEmailCode(String emailCode)
    {
        this.emailCode = emailCode;
    }
    
    /**
     * @return 返回 ee
     */
    public String getEe()
    {
        return ee;
    }
    
    /**
     * @param 对ee进行赋值
     */
    public void setEe(String ee)
    {
        this.ee = ee;
    }
    
    /**
     * @return 返回 username
     */
    public String getUsername()
    {
        return username;
    }
    
    /**
     * @param 对username进行赋值
     */
    public void setUsername(String username)
    {
        this.username = username;
    }
    
    /**
     * @return 返回 userName
     */
    public String getUserName()
    {
        return userName;
    }
    
    /**
     * @param
     */
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    /**
     * @return 返回
     */
    public String getPassword()
    {
        return password;
    }
    
    /**
     * @param
     */
    public void setPassword(String password)
    {
        this.password = password;
    }
    
    /**
     * @return 返回 tel
     */
    public String getTel()
    {
        return tel;
    }
    
    /**
     * @param 对tel进行赋值
     */
    public void setTel(String tel)
    {
        this.tel = tel;
    }
    
    /**
     * @return 返回 isMch
     */
    public int getIsMch()
    {
        return isMch;
    }
    
    /**
     * @param 对isMch进行赋值
     */
    public void setIsMch(int isMch)
    {
        this.isMch = isMch;
    }
    
    /**
     * @return 返回 tel7
     */
    public String getTel7()
    {
        return tel7;
    }
    
    /**
     * @param 对tel7进行赋值
     */
    public void setTel7(String tel7)
    {
        this.tel7 = tel7;
    }
    
    /**
     * @return 返回 mchId
     */
    public String getMchId()
    {
        return mchId;
    }
    
    /**
     * @param 对mchId进行赋值
     */
    public void setMchId(String mchId)
    {
        this.mchId = mchId;
    }
    
    /**
     * @return 返回 realname
     */
    public String getRealname()
    {
        return realname;
    }
    
    /**
     * @param 对realname进行赋值
     */
    public void setRealname(String realname)
    {
        this.realname = realname;
    }
    

    public int existData = 0;
    
    private String code;
    
    private String deptname;
    
    private String isRefundAuth;
    
    private String isOrderAuth;
    
    private boolean enabled;
    
    private String isActivityAuth;

    private String isUnfreezeAuth;

    public String getIsUnfreezeAuth() {
        return isUnfreezeAuth;
    }

    public void setIsUnfreezeAuth(String isUnfreezeAuth) {
        this.isUnfreezeAuth = isUnfreezeAuth;
    }


    
    /**
     * @return 返回 isActivityAuth
     */
    public String getIsActivityAuth()
    {
        return isActivityAuth;
    }
    
    /**
     * @param 对isActivityAuth进行赋值
     */
    public void setIsActivityAuth(String isActivityAuth)
    {
        this.isActivityAuth = isActivityAuth;
    }
    
    /**
     * @return 返回 deptname
     */
    public String getDeptname()
    {
        return deptname;
    }
    
    /**
     * @param 对deptname进行赋值
     */
    public void setDeptname(String deptname)
    {
        this.deptname = deptname;
    }
    
    /**
     * @return 返回 isRefundAuth
     */
    public String getIsRefundAuth()
    {
        return isRefundAuth;
    }
    
    /**
     * @param 对isRefundAuth进行赋值
     */
    public void setIsRefundAuth(String isRefundAuth)
    {
        this.isRefundAuth = isRefundAuth;
    }
    
    /**
     * @return 返回 isOrderAuth
     */
    public String getIsOrderAuth()
    {
        return isOrderAuth;
    }
    
    /**
     * @param 对isOrderAuth进行赋值
     */
    public void setIsOrderAuth(String isOrderAuth)
    {
        this.isOrderAuth = isOrderAuth;
    }
    
    /**
     * @return 返回 enabled
     */
    public boolean getEnabled()
    {
        return enabled;
    }
    
    /**
     * @param 对enabled进行赋值
     */
    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }
    
    /**
     * @return 返回 code
     */
    public String getCode()
    {
        return code;
    }
    
    /**
     * @param 对code进行赋值
     */
    public void setCode(String code)
    {
        this.code = code;
    }
    
    /**
     * @return 返回 addTime
     */
    public long getAddTime()
    {
        return addTime;
    }
    
    /**
     * @param 对addTime进行赋值
     */
    public void setAddTime(long addTime)
    {
        this.addTime = addTime;
    }
    
    /**
     * @return 返回 name
     */
    public String getName()
    {
        return name;
    }
    
    /**
     * @param 对name进行赋值
     */
    public void setName(String name)
    {
        this.name = name;
    }
    
    /**
     * @return 返回 pwd
     */
    public String getPwd()
    {
        return pwd;
    }
    
    /**
     * @return 返回 existData
     */
    public int isExistData()
    {
        return existData;
    }
    
    /**
     * @param 对existData进行赋值
     */
    public void setExistData(int existData)
    {
        this.existData = existData;
    }
    
    /**
     * @param 对pwd进行赋值
     */
    public void setPwd(String pwd)
    {
        this.pwd = pwd;
    }
    

    public String merchantId;
    

    public String merchantName;
    

    public int userOptionType;
    

    public Integer userType;
    

    public long loginTime;
    

    public String derviceNo;

    public long addTime;
    
    public Integer merId;
    
    public String partent;
    
    public String key;
    
    // 省份证号码
    private String IDNumber;
    
    /**
     * @return 返回 iDNumber
     */
    public String getIDNumber()
    {
        return IDNumber;
    }
    
    /**
     * @param 对iDNumber进行赋值
     */
    public void setIDNumber(String iDNumber)
    {
        IDNumber = iDNumber;
    }
    
    /**
     * @return 返回 phone
     */
    public String getPhone()
    {
        return phone;
    }
    
    /**
     * @param 对phone进行赋值
     */
    public void setPhone(String phone)
    {
        this.phone = phone;
    }
    
    /**设备是否 授权*/
    public boolean isMake;
    
    /**用户类型 普通用户 商户管理员 商户审核员 三种角色*/
    public UserRole role;
    
    /**是否商户管理员*/
    // public boolean isMerchant;
    
    public String email;
    
    /**
     * @return 返回 email
     */
    public String getEmail()
    {
        return email;
    }
    
    /**
     * @param 对email进行赋值
     */
    public void setEmail(String email)
    {
        this.email = email;
    }
    
    public String phone;
    
    public String ipAddr;
    
    private String isTotalAuth;
    
    private String telephone;


    /**
     * @return 返回 telephone
     */
    public String getTelephone()
    {
        return telephone;
    }
    
    /**
     * @param 对telephone进行赋值
     */
    public void setTelephone(String telephone)
    {
        this.telephone = telephone;
    }
    
    /**
     * @return 返回 isTotalAuth
     */
    public String getIsTotalAuth()
    {
        return isTotalAuth;
    }
    
    /**
     * @param 对isTotalAuth进行赋值
     */
    public void setIsTotalAuth(String isTotalAuth)
    {
        this.isTotalAuth = isTotalAuth;
    }
    
    //店铺ID
    public Integer shopId;
    
    /*  public void initRole() //原来
      {
          if (UserOperateTypeEnum.manager.getEnumId() == userOptionType)
          {
              role = UserRole.manager;
          }
          else if (UserOperateTypeEnum.approve.getEnumId() == userOptionType)
          {
              role = UserRole.approve;
          }
          else if (userOptionType == 0)
          {
              role = UserRole.general;
          }
      }*/
    public void initRole()
    {
        if (userType == 101 || userType == 102)
        {
            role = UserRole.approve;
        }
        else if (userType == 103)
        {
            role = UserRole.general;
        }
    }
    
    public boolean isNomal()
    {
        if (role == UserRole.general)
        {
            return true;
        }
        else if (role == UserRole.approve)
        {
            return false;
        }
        else if (role == UserRole.manager)
        {
            return false;
        }
        return false;
    }
    
}
