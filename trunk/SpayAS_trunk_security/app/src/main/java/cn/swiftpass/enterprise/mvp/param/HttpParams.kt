package cn.swiftpass.enterprise.mvp.param

/**
 * @author lizheng.zhao
 * @date 2022/12/14
 *
 * @一眨眼
 * @算不算少年
 * @一辈子
 * @算不算永远
 */
data class HttpParams(
    var mParamsMap: HashMap<String?, String?> = HashMap(),
    var mUrl: String? = "",
    var mMethodType: String? = "",
    var mSpayRs: String? = "",
    var mJsonStr: String? = "",
    var mIsNeedSpayId: Boolean = true,
    var mIsNeedCook: Boolean = true,
    var mIsNeedVerifyCerts: Boolean = true
) : java.io.Serializable