package cn.swiftpass.enterprise.ui.adapter;


import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import cn.swiftpass.enterprise.bussiness.model.PreAuthDetailsBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.widget.BaseRecycleAdapter;

public class PreAuthDetailsAdapter extends BaseRecycleAdapter<PreAuthDetailsBean> {

     private List<PreAuthDetailsBean> mList;
     private int type;
     private Context mContext;

    public PreAuthDetailsAdapter(Context context,List<PreAuthDetailsBean> datas,int type) {
        super(datas);
        mList=datas;
        this.type=type;
        mContext=context;
    }

    @Override
    protected void bindData(BaseViewHolder holder, int position) {
        TextView tv_title=(TextView) holder.getView(R.id.tv_title);
        TextView tv_content=(TextView) holder.getView(R.id.tv_content);
        TextView tv_tips=(TextView) holder.getView(R.id.tv_tips);
        PreAuthDetailsBean detailsBean=mList.get(position);
        switch (type){
            case 1:
                if (position==0){
                    RelativeLayout.LayoutParams params =
                            new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                                    RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params.addRule(RelativeLayout.CENTER_VERTICAL);
                    tv_title.setLayoutParams(params);
                    tv_content.setVisibility(View.GONE);
                    tv_title.setText(detailsBean.getTitle());
                    tv_tips.setText(detailsBean.getMsg());

                    if (detailsBean.getMsg().equals(mContext.getString(R.string.tx_bill_stream_chioce_succ))){
                        tv_tips.setTextColor(mContext.getResources().getColor(R.color.clr_23bf3f));
                    }else {
                        tv_tips.setTextColor(mContext.getResources().getColor(R.color.clr_a3a3a3));
                    }

                }else {
                    tv_title.setText(detailsBean.getTitle());
                    tv_content.setText(detailsBean.getTitle_msg());
                    tv_tips.setVisibility(View.GONE);
                }
                break;
            case 2:
                if (position==0){
                    RelativeLayout.LayoutParams params =
                            new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                                    RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params.addRule(RelativeLayout.CENTER_VERTICAL);
                    tv_title.setLayoutParams(params);
                    tv_content.setVisibility(View.GONE);
                    tv_title.setText(detailsBean.getTitle());
                    tv_tips.setText(detailsBean.getMsg());
                    if (detailsBean.getMsg().equals(mContext.getString(R.string.freezen_success))){
                        tv_tips.setTextColor(mContext.getResources().getColor(R.color.clr_f99426));
                    }else {
                        tv_tips.setTextColor(mContext.getResources().getColor(R.color.clr_a3a3a3));
                    }
                }else {
                    tv_title.setText(detailsBean.getTitle());
                    tv_content.setText(detailsBean.getTitle_msg());
                    tv_tips.setVisibility(View.GONE);
                }
                break;
        }

    }

    @Override
    public int getLayoutId() {
        return R.layout.item_preauth_content;
    }

}
