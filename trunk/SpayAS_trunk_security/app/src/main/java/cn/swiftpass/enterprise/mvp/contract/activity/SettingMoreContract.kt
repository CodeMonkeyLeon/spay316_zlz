package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.GoodsMode
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView


/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @我们始终没有想出
 * @太阳却已悄悄安息
 */
class SettingMoreContract {

    interface View : BaseView {

        fun queryBodySuccess(response: GoodsMode)


        fun queryBodyFailed(error: Any?)


        fun getVersionCodeSuccess(
            response: UpgradeInfo?,
            isDisplayIcon: Boolean
        )

        fun getVersionCodeFailed(error: Any?)


        fun logoutSuccess(response: Boolean)

        fun logoutFailed(error: Any?)
    }

    interface Presenter : BasePresenter<View> {

        fun logout()

        fun getVersionCode(isDisplayIcon: Boolean)

        fun queryBody()
    }


}