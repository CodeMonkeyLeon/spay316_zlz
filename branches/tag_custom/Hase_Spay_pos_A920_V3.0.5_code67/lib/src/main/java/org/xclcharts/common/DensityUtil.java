package org.xclcharts.common;

import android.content.Context;
import android.graphics.Paint;
import android.view.View;

import java.util.List;

/**
 * @ClassName DensityUtil
 * @Description  用于手机适配的一些类
 * @author XiongChuanLiang<br/>(xcl_168@aliyun.com)
 */

public class DensityUtil
{
    
    //private static final String TAG = "DensityUtil";
    
    private DensityUtil()
    {
    }
    
    public static float getDensity(Context context)
    {
        return context.getResources().getDisplayMetrics().density;
    }
    
    /** 
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素) 
     */
    public static int dip2px(Context context, float dpValue)
    {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int)(dpValue * scale + 0.5f);
    }
    
    /** 
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素) 
     */
    public static double dip2pxDoub(Context context, Double dpValue)
    {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (double)(dpValue * scale + 0.5f);
    }
    
    /** 
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp 
     */
    public static int px2dip(Context context, float pxValue)
    {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int)(pxValue / scale + 0.5f);
    }

    public static int getMaxTextWidth(List<String> list, float textSize, String feeFh) {
        float max = 0f;
        Paint paint = new Paint();
        paint.setTextSize(textSize);
        for (String str : list) {
            if (str.contains("|")) {
                String[] split = str.split("\\|");
                for (int i = 0; i < split.length; i++) {
                    String dataStr = split[i];
                    if (i == 1) {
                        dataStr = feeFh + dataStr;
                    }
                    float text = paint.measureText(dataStr);
                    if (text > max) {
                        max = text;
                    }
                }

            } else {
                float text = paint.measureText(str);
                if (text > max)
                    max = text;
            }

        }
        return (int) max;
    }

    /*
     * 屏幕宽度
     */
    public static int getScreenWidth(Context context)
    {
        return context.getResources().getDisplayMetrics().widthPixels;
    }
    
    /**
     * 屏幕高度
     */
    public static int getScreenHeight(Context context)
    {
        return context.getResources().getDisplayMetrics().heightPixels;
    }
    
    /* 
    * 获取控件宽 
    */
    public static int getWidth(View view)
    {
        int w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        int h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(w, h);
        return (view.getMeasuredWidth());
    }
    
    /* 
    * 获取控件高 
    */
    public static int getHeight(View view)
    {
        int w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        int h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(w, h);
        return (view.getMeasuredHeight());
    }


}
