package cn.swiftpass.enterprise.ui.fmt;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;
import cn.swiftpass.enterprise.ui.activity.spayMainTabActivity;
import cn.swiftpass.enterprise.ui.widget.SlidingTabLayout;

/**
 * Created by aijingya on 2019/4/12.
 *
 * @Package cn.swiftpass.enterprise.ui.fmt
 * @Description: ${TODO}(报表tab)
 * @date 2019/4/12.15:37.
 */
public class FragmentTabReport extends BaseFragment implements ViewPager.OnPageChangeListener{
    private static final String TAG = FragmentTabReport.class.getSimpleName();
    private spayMainTabActivity mActivity;
    /*viewpager*/
    private ViewPager mViewPager ;
    /*自定义的 tabLayout*/
    private SlidingTabLayout mSlidingTabLayout ;
    /*每个 tab 的 item*/
    private List<BaseFragment> mFragments = new ArrayList<>() ;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof spayMainTabActivity){
            mActivity=(spayMainTabActivity) activity;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_report, container, false);
        initViews(view);
        initData();
        return view;

    }

    @Override
    protected int getLayoutId() {
        return 0;
    }

    //初始化控件
    private void initViews(View view) {
        mViewPager = view.findViewById(R.id.id_viewpager);
        mSlidingTabLayout = view.findViewById(R.id.id_SlidingTabLayout);

    }

    public void initData(){
        FragmentTransactionReport fragmentTransactionReport1 = new FragmentTransactionReport();
        fragmentTransactionReport1.setFragmentReportType("1");
        mFragments.add(fragmentTransactionReport1);

        FragmentTransactionReport fragmentTransactionReport2 = new FragmentTransactionReport();
        fragmentTransactionReport2.setFragmentReportType("2");
        mFragments.add(fragmentTransactionReport2);

        FragmentTransactionReport fragmentTransactionReport3 = new FragmentTransactionReport();
        fragmentTransactionReport3.setFragmentReportType("3");
        mFragments.add(fragmentTransactionReport3);

        mViewPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager()));

        ArrayList<String> stringArrayList = new ArrayList<String>();
        stringArrayList.add(getStringById(R.string.report_daily_report));
        stringArrayList.add(getStringById(R.string.report_weekly_report));
        stringArrayList.add(getStringById(R.string.report_monthly_report));

        /*需要先为 viewpager 设置 adapter*/
        mSlidingTabLayout.setTitleList(stringArrayList);
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setViewPagerOnChangeListener(this);

        mViewPager.setCurrentItem(0);
    }


    private class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public BaseFragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            return super.instantiateItem(container, position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        //设置position对应的集合中的Fragment
        mViewPager.setCurrentItem(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }
}
