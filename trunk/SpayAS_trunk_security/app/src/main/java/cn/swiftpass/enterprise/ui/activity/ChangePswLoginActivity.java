package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import androidx.annotation.Nullable;

import com.example.common.sentry.SentryUtils;
import com.example.common.sp.PreferenceUtil;
import com.ziyeyouhu.library.KeyboardTouchListener;
import com.ziyeyouhu.library.KeyboardUtil;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.ECDHInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants;
import cn.swiftpass.enterprise.mvp.contract.activity.ChangePswLoginContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.ChangePswLoginPresenter;
import cn.swiftpass.enterprise.ui.widget.dialog.CashierNoticeDialog;
import cn.swiftpass.enterprise.utils.ECDHUtils;
import cn.swiftpass.enterprise.utils.EditTextWatcher;
import cn.swiftpass.enterprise.utils.KotlinUtils;
import cn.swiftpass.enterprise.utils.SharedPreUtils;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * Created by aijingya on 2019/3/13.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2019/3/13.11:30.
 */
public class ChangePswLoginActivity extends BaseActivity<ChangePswLoginContract.Presenter> implements ChangePswLoginContract.View {

    private static final String TAG = ChangePswLoginActivity.class.getCanonicalName();
    private EditText et_id, et_pass;
    private ImageView iv_clean_input, iv_clean_input_pass;
    private Button btn_next_step;
    private String oldPsw;
    private LinearLayout rootView;
    private KeyboardUtil keyboardUtil;
    private ScrollView scrollView;
    private CashierNoticeDialog ChangePswSuccessNoticeDialog;

    public static void startActivity(Context context, String oldPsw) {
        Intent it = new Intent();
        it.setClass(context, ChangePswLoginActivity.class);
        it.putExtra("oldPsw", oldPsw);
        context.startActivity(it);
    }

    @Override
    protected ChangePswLoginContract.Presenter createPresenter() {
        return new ChangePswLoginPresenter();
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.change_psw);
    }

    @Override
    protected boolean isLoginRequired() {
        return false;
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_pass_submit);

        initView();
        setLister();

        btn_next_step.getBackground().setAlpha(102);
        btn_next_step.setEnabled(false);
        oldPsw = getIntent().getStringExtra("oldPsw");
        MainApplication.getInstance().getListActivities().add(this);
        initMoveKeyBoard();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                keyboardUtil.showKeyBoardLayout(et_id, KeyboardUtil.INPUT_TYPE_ABC, -1);
            }
        }, 200);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (keyboardUtil.isShow) {
                keyboardUtil.hideSystemKeyBoard();
                keyboardUtil.hideAllKeyBoard();
                keyboardUtil.hideKeyboardLayout();

                finish();
            } else {
                return super.onKeyDown(keyCode, event);
            }

            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    //安全键盘
    private void initMoveKeyBoard() {
        rootView = findViewById(R.id.rootview);
        scrollView = findViewById(R.id.scrollview);
        keyboardUtil = new KeyboardUtil(this, rootView, scrollView);
        // monitor the KeyBarod state
        keyboardUtil.setKeyBoardStateChangeListener(new ChangePswLoginActivity.KeyBoardStateListener());
        // monitor the finish or next Key
        keyboardUtil.setInputOverListener(new ChangePswLoginActivity.inputOverListener());
        et_id.setOnTouchListener(new KeyboardTouchListener(keyboardUtil, KeyboardUtil.INPUT_TYPE_ABC, -1));
        et_pass.setOnTouchListener(new KeyboardTouchListener(keyboardUtil, KeyboardUtil.INPUT_TYPE_ABC, -1));
    }

    @Override
    protected void onPause() {
        super.onPause();
        et_id.setText("");
        et_pass.setText("");
        et_id.requestFocus();
    }

    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b, enable, res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_press_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }

    private void initView() {
        et_pass = getViewById(R.id.et_pass);
        et_id = getViewById(R.id.et_id);
        iv_clean_input = getViewById(R.id.iv_clean_input);
        btn_next_step = getViewById(R.id.btn_next_step);
        iv_clean_input_pass = getViewById(R.id.iv_clean_input_pass);

        EditTextWatcher editTextWatcher = new EditTextWatcher();
        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (et_id.isFocused()) {
                    if (et_id.getText().toString().length() > 0) {
                        iv_clean_input.setVisibility(View.VISIBLE);

                    } else {
                        iv_clean_input.setVisibility(View.GONE);
                    }
                }

                if (et_pass.isFocused()) {
                    if (et_pass.getText().toString().length() > 0) {
                        iv_clean_input_pass.setVisibility(View.VISIBLE);
                    } else {
                        iv_clean_input_pass.setVisibility(View.GONE);
                    }
                }
                if (!StringUtil.isEmptyOrNull(et_id.getText().toString().trim())
                        && !StringUtil.isEmptyOrNull(et_pass.getText().toString().trim())) {
                    setButtonBg(btn_next_step, true, R.string.bt_confirm);
                } else {
                    setButtonBg(btn_next_step, false, R.string.bt_confirm);
                }
            }
        });
        et_pass.addTextChangedListener(editTextWatcher);
        et_id.addTextChangedListener(editTextWatcher);
    }

    private void setLister() {
        iv_clean_input.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                et_id.setText("");
                iv_clean_input.setVisibility(View.GONE);
            }
        });

        iv_clean_input_pass.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                et_pass.setText("");
                iv_clean_input_pass.setVisibility(View.GONE);
            }
        });

        btn_next_step.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkPasswordDate();
            }
        });
    }

    public void checkPasswordDate() {
        if (StringUtil.isEmptyOrNull(et_id.getText().toString().trim())) {
            toastDialog(ChangePswLoginActivity.this, R.string.tx_newpass_notnull, null);
            et_id.setFocusable(true);
            return;
        }
        if (StringUtil.isEmptyOrNull(et_pass.getText().toString().trim())) {
            toastDialog(ChangePswLoginActivity.this, R.string.tx_repeatpass_notnull, null);
            et_pass.setFocusable(true);
            return;
        }

        if (et_id.getText().toString().length() < 8) {
            et_id.setFocusable(true);
            toastDialog(ChangePswLoginActivity.this, R.string.show_pass_prompt, null);
            return;
        }

        //校验新密码是否符合8-16位字符
        if (!KotlinUtils.INSTANCE.isNewVersion() && !isContainAll(et_id.getText().toString().trim())) {
            et_id.setFocusable(true);
            toastDialog(ChangePswLoginActivity.this, R.string.et_new_pass, null);
            return;
        }


        if (!et_id.getText().toString().trim().equals(et_pass.getText().toString().trim())) {
            toastDialog(ChangePswLoginActivity.this, R.string.tx_pass_notdiff, null);
            et_pass.setFocusable(true);
            return;
        }

        //调用接口，修改密码，旧密码从登录页面带过来的
        changePsw();

    }


    @Override
    public void changePwdSuccess(boolean response) {
        if (response) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (ChangePswSuccessNoticeDialog == null) {
                        ChangePswSuccessNoticeDialog = new CashierNoticeDialog(ChangePswLoginActivity.this);
                        ChangePswSuccessNoticeDialog.setContentText(R.string.change_psw_success);
                        ChangePswSuccessNoticeDialog.setConfirmText(R.string.bt_dialog_ok);
                        ChangePswSuccessNoticeDialog.setCanceledOnTouchOutside(false);
                        //设置点击返回键不消失
                        ChangePswSuccessNoticeDialog.setCancelable(false);
                        ChangePswSuccessNoticeDialog.setCurrentListener(new CashierNoticeDialog.ButtonConfirmCallBack() {
                            @Override
                            public void onClickConfirmCallBack() {
                                if (ChangePswSuccessNoticeDialog != null && ChangePswSuccessNoticeDialog.isShowing()) {
                                    ChangePswSuccessNoticeDialog.dismiss();
                                    //跳转到登录的页面
                                    PreferenceUtil.removeKey("login_skey");
                                    PreferenceUtil.removeKey("login_sauthid");
                                    MainApplication.getInstance().setUserInfo(null);
                                    showPage(WelcomeActivity.class);
                                    for (Activity a : MainApplication.getInstance().getAllActivities()) {
                                        a.finish();
                                    }
                                    finish();
                                }
                            }
                        });
                    }
                    if (!ChangePswSuccessNoticeDialog.isShowing()) {
                        ChangePswSuccessNoticeDialog.show();
                    }
                }
            });
        }
    }

    @Override
    public void changePwdFailed(@Nullable Object error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (error.toString().startsWith("405")) {//Require to renegotiate ECDH key
                    ECDHKeyExchange();
                } else {
                    toastDialog(ChangePswLoginActivity.this, error.toString(), null);
                }

            }
        });
    }

    public void changePsw() {
        final String newPwd = et_id.getText().toString().trim();
        final String newPwd2 = et_pass.getText().toString().trim();

        if (mPresenter != null) {
            mPresenter.changePwd(
                    oldPsw,
                    MainApplication.getInstance().getUserInfo().phone,
                    newPwd,
                    newPwd2);
        }
    }


    @Override
    public void ecdhKeyExchangeSuccess(ECDHInfo response) {
        if (response != null) {
            try {
                final String privateKey = SharedPreUtils.readProduct(ParamsConstants.APP_PRIVATE_KEY).toString();
                String secretKey = ECDHUtils.getInstance().ecdhGetShareKey(MainApplication.getInstance().getSerPubKey(), privateKey);
                SharedPreUtils.saveObject(secretKey, ParamsConstants.SECRET_KEY);
                //再去请求一次修改密码接口
                changePsw();
            } catch (Exception e) {
                SentryUtils.INSTANCE.uploadTryCatchException(
                        e,
                        SentryUtils.INSTANCE.getClassNameAndMethodName()
                );
                Log.e(TAG, Log.getStackTraceString(e));
            }
        }
    }

    @Override
    public void ecdhKeyExchangeFailed(@Nullable Object error) {

    }

    private void ECDHKeyExchange() {
        try {
            ECDHUtils.getInstance().getAppPubKey();
        } catch (Exception e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
        }
        final String publicKey = SharedPreUtils.readProduct(ParamsConstants.APP_PUBLIC_KEY).toString();


        if (mPresenter != null) {
            mPresenter.ecdhKeyExchange(publicKey);
        }
    }

    //用来校验密码，是否包含数字和字母
    public boolean isContainAll(String str) {
        boolean isDigit = false;
                /*boolean isLowerCase = false;
                boolean isUpperCase = false;*/
        boolean isLetters = false;

        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))) {
                isDigit = true;
            } else if (Character.isLowerCase(str.charAt(i))) {
//                        isLowerCase = true;
                isLetters = true;
            } else if (Character.isUpperCase(str.charAt(i))) {
//                        isUpperCase = true;
                isLetters = true;
            }
        }
        String regex = "^[a-zA-Z0-9]+$";
//                boolean isCorrect = isDigit&&isLowerCase&&isUpperCase&&str.matches(regex);
        boolean isCorrect = isDigit && isLetters && str.matches(regex);
        return isCorrect;
    }

    class KeyBoardStateListener implements KeyboardUtil.KeyBoardStateChangeListener {

        @Override
        public void KeyBoardStateChange(int state, EditText editText) {

        }
    }

    class inputOverListener implements KeyboardUtil.InputFinishListener {

        @Override
        public void inputHasOver(int onclickType, EditText editText) {

        }
    }


}
