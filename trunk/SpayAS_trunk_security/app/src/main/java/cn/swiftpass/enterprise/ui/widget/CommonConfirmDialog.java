package cn.swiftpass.enterprise.ui.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.UpgradeInfo;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DialogHelper;

/***
 * 常用对话框
 */
public class CommonConfirmDialog extends Dialog implements View.OnClickListener {
    private ViewGroup mRootView;

    private TextView btnOk, btnCancel;

    private Button btn_cure, btnConfirm;

    private ConfirmListener btnListener;

    private LinearLayout rlConfirm;

    private RelativeLayout rlTowConfirm;

    //private UpgradeInfo result;

    private View app_line;

    public CommonConfirmDialog(Context context, String title, String msg, ConfirmListener btnListener,
                               boolean isVisibleConfirm, UpgradeInfo result) {
        super(context);
        this.btnListener = btnListener;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_common_confirm, null);
        setContentView(mRootView);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        this.setCanceledOnTouchOutside(false);
        //this.result = result;

        TextView tvTitle = (TextView) mRootView.findViewById(R.id.title);
        if (title != null) {
            tvTitle.setText(title);
        }
        TextView tvMsg = (TextView) mRootView.findViewById(R.id.tv_msg);
        if (msg != null) {
            tvMsg.setText(msg);
        }

        btnOk = (TextView) mRootView.findViewById(R.id.ok);
        btnCancel = (TextView) mRootView.findViewById(R.id.cancel);
        rlTowConfirm = (RelativeLayout) mRootView.findViewById(R.id.rl_tow_confirm);
        rlConfirm = (LinearLayout) mRootView.findViewById(R.id.rl_confirm);
        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnConfirm = (Button) mRootView.findViewById(R.id.btn_confirm);
        btnConfirm.setOnClickListener(this);
        btn_cure = (Button) mRootView.findViewById(R.id.btn_cure);
        btn_cure.setOnClickListener(this);

        if (!isVisibleConfirm) {
            rlTowConfirm.setVisibility(View.VISIBLE);
            rlConfirm.setVisibility(View.INVISIBLE);

        } else {
            rlConfirm.setVisibility(View.VISIBLE);
            rlTowConfirm.setVisibility(View.INVISIBLE);
        }

        if (result != null) {
            if (!result.mustUpgrade) {
                // 不是强制更新
                btn_cure.setVisibility(View.VISIBLE);
                app_line = (View) findViewById(R.id.app_line);
                app_line.setVisibility(View.VISIBLE);
            }

            if (result.isMoreSetting) {
                btn_cure.setText(R.string.btnCancel);
            }
        }

    }

    private static DialogInfo dialogInfo;

    public static void show(final Context context, String title, String msg, ConfirmListener btnListener,
                            boolean isVisibleConfirm, UpgradeInfo result) {

        CommonConfirmDialog dia = new CommonConfirmDialog(context, title, msg, btnListener, isVisibleConfirm, result);
        if (result != null && result.mustUpgrade) {

            dia.setOnKeyListener(new OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {
                    if (keycode == KeyEvent.KEYCODE_BACK && arg2.getAction() == KeyEvent.ACTION_DOWN) {
                        dialogInfo =
                                new DialogInfo(context, context.getString(R.string.public_cozy_prompt),
                                        context.getString(R.string.show_sign_out), context.getString(R.string.btnOk),
                                        context.getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG,
                                        new DialogInfo.HandleBtn() {

                                            @Override
                                            public void handleOkBtn() {
                                                MainApplication.getInstance().exit();
                                                System.exit(0);
                                            }

                                            @Override
                                            public void handleCancelBtn() {
                                                dialogInfo.dismiss();
                                            }
                                        }, null);
                        if (!dialogInfo.isShowing()) {
                            dialogInfo.show();
                        } else {
                            dialogInfo.dismiss();
                        }

                        return true;
                    }
                    return false;
                }
            });
        }
        dia.show();
    }

    public static void show(Context context, String title, String msg, ConfirmListener btnListener) {
        CommonConfirmDialog dia = new CommonConfirmDialog(context, title, msg, btnListener, true, null);
        DialogHelper.resize((Activity) context, dia);
        dia.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ok:
                if (btnListener != null) {
                    btnListener.ok();
                }
                break;
            case R.id.cancel:
                if (btnListener != null) {
                    btnListener.cancel();
                }
                break;
            case R.id.btn_confirm:
                if (btnListener != null) {
                    btnListener.ok();
                }
                break;

            case R.id.btn_cure:
                if (btnListener != null) {
                    this.cancel();
                    btnListener.cancel();
                }
                break;
            default:
                break;
        }
        dismiss();
    }

    public interface ConfirmListener {
        public void ok();

        public void cancel();
    }
}
