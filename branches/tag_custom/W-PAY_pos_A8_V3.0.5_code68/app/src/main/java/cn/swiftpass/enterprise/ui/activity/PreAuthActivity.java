package cn.swiftpass.enterprise.ui.activity;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.bill.BillOrderManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.PreAuthDetailsBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.print.PrinterPreAuth;
import cn.swiftpass.enterprise.ui.activity.bill.OrderRefundActivity;
import cn.swiftpass.enterprise.ui.activity.print.BluetoothSettingActivity;
import cn.swiftpass.enterprise.ui.adapter.PreAuthAdapter;
import cn.swiftpass.enterprise.ui.widget.CustomDialog;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.ApkUtil;
import cn.swiftpass.enterprise.utils.CreateOneDiCodeUtil;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;


/**
 * 创建人：caoxiaoya
 * 时间：2018/12/24
 * 描述：预授权解冻
 * 备注：进入预授权订单详情→点击“解冻”进入预授权解冻页面→输入解冻金额，确认解冻→输入登录密码验证身份→解冻成功
 */
public class PreAuthActivity extends TemplateActivity {
    private PreAuthAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private List<PreAuthDetailsBean> mList=new ArrayList<>();
    private Order mOrder;
    public static final int TYPE_ONE=1;//预授权订单详情(解冻)
    public static final int TYPE_TWO=2;//预授权订单详情(支付)
    private int activityType;//标识哪个Activity传过来的
    private String state="";
    private PrinterPreAuth printerPreAuth;

    public static void startActivity(Context mContext, Order order,int activityType) {
        Intent it = new Intent();
        it.setClass(mContext, PreAuthActivity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT); //W.l 解决弹出多个activity
        it.putExtra("order", order);
        it.putExtra("activityType",activityType);//标识哪个Activity传过来的
        mContext.startActivity(it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_auth);
        MainApplication.listActivities.add(this);
        initView();
        if (mOrder!=null) {
            initViewDate();

        }
        bindDeviceService();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.bindDeviceService();
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.unbindDeviceService();
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
        titleBar.setLeftButtonVisible(true);
    }

    private void initView() {
        activityType=getIntent().getIntExtra("activityType",0);
        mOrder=(Order)getIntent().getSerializableExtra("order");
        mRecyclerView=(RecyclerView)findViewById(R.id.rv_pre_auth);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));

        String title = "";
        if(activityType == 1){
            title = getStringById(R.string.pre_auth_unfreezen_details);
        }else{
            title = getStringById(R.string.pre_auth_details);

            if (MainApplication.isUnfreezeAuth.equals("1")){//收银员权限检查啊
                titleBar.setRightButLayVisibleForTotal(true,getString(R.string.unfreezed));//开启
            }else {
                titleBar.setRightButLayVisibleForTotal(false,getString(R.string.unfreezed));//关闭
            }

        }
        titleBar.setTitle(title);

        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButLayClick() {
                OrderRefundActivity.startActivity(PreAuthActivity.this, mOrder, 2);//预授权解冻
            }
        });
    }
    public void initViewDate(){
        switch (mOrder.getTradeState()){
            case 1:
                state=getString(R.string.unauthorized);//未授权
                break;
            case 2:
                state=getString(R.string.pre_auth_authorized);//已授权
                break;
            case 3:
                state=getString(R.string.pre_auth_closed);//已关闭
                break;
            default:
                break;
        }
        mList.add(new PreAuthDetailsBean(getString(R.string.captured_amount),
                MainApplication.getFeeFh()+DateUtil.formatMoneyUtils(mOrder.getTotalPayAmount()),getStringById(R.string.details)));//已支付金额
        mList.add(new PreAuthDetailsBean(getString(R.string.unfreezed_amount),
                MainApplication.getFeeFh()+DateUtil.formatMoneyUtils(mOrder.getTotalUnfreezeAmount()),getStringById(R.string.details)));//已解冻金额
        mList.add(new PreAuthDetailsBean(getString(R.string.remaining_amount),
                MainApplication.getFeeFh()+DateUtil.formatMoneyUtils(mOrder.getRestAmount()),null));//剩余授权金额

        mList.add(new PreAuthDetailsBean(getString(R.string.order_status),null,state));//订单状态
        mList.add(new PreAuthDetailsBean(getString(R.string.pre_auth_time),mOrder.getTradeTimeNew(),null));//授权时间
        mList.add(new PreAuthDetailsBean(getString(R.string.payment_method),mOrder.getTradeName(),null));//交易方式

        if (!(state.equals(getString(R.string.unauthorized))||state.equals(getString(R.string.pre_auth_closed)))) {
            if (!TextUtils.isEmpty(mOrder.getTransactionId())) {

                if (ApkUtil.isZhLanguage(PreAuthActivity.this)) {
                    mList.add(new PreAuthDetailsBean(MainApplication.getPayTypeMap().get("2") +
                            getString(R.string.tx_orderno), mOrder.getTransactionId(), null));//支付宝单号 中文显示
                } else {
                    mList.add(new PreAuthDetailsBean(MainApplication.getPayTypeMap().get("2") + " " +
                            getString(R.string.tx_orderno), mOrder.getTransactionId(), null));//支付宝单号 英文显示
                }
            }
        }

        mList.add(new PreAuthDetailsBean(getString(R.string.platform_pre_auth_order_id),mOrder.getAuthNo(),null));//平台预授权订单号
        if (!TextUtils.isEmpty(mOrder.getNote())&&isAbsoluteNullStr(mOrder.getNote())) {
            mList.add(new PreAuthDetailsBean(getString(R.string.note_mark), mOrder.getNote(), null));//备注
        }
        mAdapter=new PreAuthAdapter(PreAuthActivity.this,mOrder,mList,activityType);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setData(mList);
        mAdapter.setHeaderView(createHeadView());
        mAdapter.setFooterView(createFootView());
        mAdapter.setOnItemClickListener(new PreAuthAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position==0){
                    itemType=1;
                    loadList(itemType);
                }else if (position==1){
                    itemType=2;
                    loadList(itemType);
                }
            }
        });

    }


    private int  itemType;//记录list列表前两条

    private void loadList(int operationType) {
        loadDialog(PreAuthActivity.this,R.string.loading);
        BillOrderManager.getInstance().authOperateQuery(String.valueOf(operationType), mOrder.getAuthNo(),
                null, null, 1, 10, null, null,
                new UINotifyListener<List<Order>>() {
                    @Override
                    public void onError(Object object) {
                        super.onError(object);
                        dismissLoading();
                    }

                    @Override
                    public void onSucceed(List<Order> result) {
                        super.onSucceed(result);
                        if (result != null) {//操作类型：1-转支付,2-解冻
                            if (result.size() > 0){
                                if (result.size()==1){//当size等于1的时候直接跳转转支付详情
                                    if (itemType==1) {//再去请求一次接口
                                        queryOrderDetail(result.get(0).getOrderNo());
                                    }else if (itemType==2){//直接已解冻金额详情
                                        result.get(0).setTradeName(mOrder.getTradeName());
                                        PreAuthDetailsActivity.startActivity(PreAuthActivity.this,result.get(0),2,2);
                                        dismissLoading();
                                    }
                                }else {
                                    if (itemType==1){//跳转已支付金额List列表
                                        PreAuthThawActivity.startActivity(PreAuthActivity.this,mOrder
                                                ,result,1);
                                    }else if (itemType==2){//跳转已解冻金额List列表
                                        PreAuthThawActivity.startActivity(PreAuthActivity.this,mOrder,
                                                result,2);//itemType用于显示下级页面是已支付还是解冻金额页面
                                    }
                                    dismissLoading();
                                }
                            }else {
                                PreAuthThawActivity.startActivity(PreAuthActivity.this,mOrder,
                                        result,2);
                                    dismissLoading();
                            }

                        }else {
                            dismissLoading();
                        }
                    }
                });
    }


    private void  queryOrderDetail(String oderNo){
        OrderManager.getInstance().queryOrderDetail(oderNo, MainApplication.getMchId(), false, new UINotifyListener<Order>() {

            @Override
            public void onError(Object object) {
                super.onError(object);
                dismissLoading();
            }
            @Override
            public void onSucceed(Order result) {
                super.onSucceed(result);
                dismissLoading();
                if (result != null) {
                    OrderDetailsActivity.startActivity(PreAuthActivity.this, result);
                }
            }

        });
    }


    private View createHeadView(){
        LinearLayout.LayoutParams layoutParams=
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        LayoutInflater inflater = LayoutInflater.from(this);
        View viewHead=inflater.inflate(R.layout.item_preauth_head, null);
        viewHead.setLayoutParams(layoutParams);
        TextView tv_type_total=(TextView)viewHead.findViewById(R.id.tv_type_total);
        TextView tv_type_money=(TextView)viewHead.findViewById(R.id.tv_type_money);
        TextView tv_money=(TextView)viewHead.findViewById(R.id.tv_money);
        tv_type_money.setText(MainApplication.getFeeFh()+DateUtil.formatMoneyUtils(mOrder.getTotalFreezeAmount()));

        if (activityType==1||activityType==2){
            tv_type_total.setText(getString(R.string.pre_auth_amount));
            tv_money.setVisibility(View.GONE);
        }

        return viewHead;
    }

    private View createFootView(){
        LinearLayout.LayoutParams layoutParams=
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LayoutInflater inflater = LayoutInflater.from(this);
        View viewFoot=inflater.inflate(R.layout.item_preauth_foot, null);
        viewFoot.setLayoutParams(layoutParams);

        Button btn_finish=(Button)viewFoot.findViewById(R.id.btn_finish);//完成
        Button btn_blue_print=(Button)viewFoot.findViewById(R.id.btn_blue_print);//打印
       final ImageView iv_qr=(ImageView)viewFoot.findViewById(R.id.iv_qr);//一维码
        TextView tv_qr_numbertext=(TextView)viewFoot.findViewById(R.id.tv_qr_numbertext);//一维码数字
        TextView tv_description=(TextView)viewFoot.findViewById(R.id.tv_description);//说明
        switch (activityType){
            case TYPE_ONE:
                if (state.equals(getString(R.string.unauthorized))||state.equals(getString(R.string.pre_auth_closed))){
                    btn_finish.setVisibility(View.GONE);
                    btn_blue_print.setText(R.string.syn_order);//同步订单
                    btn_blue_print.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                          //同步接口
                            synchronizeOrder();
                        }
                    });
                }else {
                    btn_finish.setVisibility(View.VISIBLE);
                    btn_finish.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            OrderRefundActivity.startActivity(PreAuthActivity.this,mOrder,4);//预授权解冻 不需要验证密码
                        }
                    });
                    btn_blue_print.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                           bluePrint(2);
                        }
                    });
                }

                btn_blue_print.setVisibility(View.VISIBLE);

                // 生成一维码
                if (!isAbsoluteNullStr(mOrder.getAuthNo())) {
                    iv_qr.setVisibility(View.VISIBLE);
                    tv_qr_numbertext.setVisibility(View.VISIBLE);
                    WindowManager wm = this.getWindowManager();
                    int width = wm.getDefaultDisplay().getWidth();
                    final int w = (int) (width * 0.85);
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                iv_qr.setImageBitmap(CreateOneDiCodeUtil.createCode(mOrder.getAuthNo(), w, 180));
                            }
                        });
                    } catch (Exception e) {
                    }
                    tv_qr_numbertext.setText(mOrder.getAuthNo());
                    iv_qr.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            RotateCanvasViewActivity.startActivity(PreAuthActivity.this,mOrder.getAuthNo());
                        }
                    });
                }
                if (mOrder.getRestAmount()==0){
                    titleBar.setRightButLayVisible(false,getString(R.string.unfreezed));
                    btn_finish.setBackgroundResource(R.drawable.btn_press_pre_shape);
                    btn_finish.setTextColor(getResources().getColor(R.color.white));
                    btn_finish.setEnabled(false);
                }
                if (state.equals(getString(R.string.pre_auth_closed))){
                    btn_finish.setVisibility(View.GONE);
                    btn_blue_print.setVisibility(View.GONE);
                    tv_qr_numbertext.setPadding(0,0,0,60);
                }
                break;
            case TYPE_TWO:
                if (state.equals(getString(R.string.unauthorized))) {
                    btn_finish.setVisibility(View.GONE);
                    titleBar.setRightButtonVisible(false);
                    if (mOrder.getTradeState() == 3) {//已撤销的时候，需要隐藏打印按钮
                        btn_blue_print.setVisibility(View.GONE);
                        tv_qr_numbertext.setPadding(0,0,0,100);
                    } else {
                        btn_blue_print.setVisibility(View.VISIBLE);
                        btn_blue_print.setText(R.string.syn_order);//同步订单
                        btn_blue_print.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //同步接口
                                synchronizeOrder();
                            }
                        });
                    }
                }else {
                    btn_blue_print.setVisibility(View.VISIBLE);
                    btn_finish.setVisibility(View.VISIBLE);
                    btn_finish.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            OrderRefundActivity.startActivity(PreAuthActivity.this,mOrder,4);//预授权解冻 不需要验证密码
                        }
                    });
                    btn_blue_print.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                           bluePrint(4);
                        }
                    });
                }


                // 生成一维码
                if (!isAbsoluteNullStr(mOrder.getAuthNo())) {
                    iv_qr.setVisibility(View.VISIBLE);
                    tv_qr_numbertext.setVisibility(View.VISIBLE);
                    WindowManager wm = this.getWindowManager();
                    int width = wm.getDefaultDisplay().getWidth();
                    final int w = (int) (width * 0.85);
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                iv_qr.setImageBitmap(CreateOneDiCodeUtil.createCode(mOrder.getAuthNo(), w, 180));
                            }
                        });
                    } catch (Exception e) {
                    }
                    tv_qr_numbertext.setText(mOrder.getAuthNo());
                    iv_qr.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            RotateCanvasViewActivity.startActivity(PreAuthActivity.this,mOrder.getAuthNo());
                        }
                    });
                }
                if (mOrder.getRestAmount()==0){//如果剩余金额为0就设置不能收款点击
                    titleBar.setRightButLayVisible(false,getString(R.string.unfreezed));
                    btn_finish.setBackgroundResource(R.drawable.btn_press_pre_shape);
                    btn_finish.setTextColor(getResources().getColor(R.color.white));
                    btn_finish.setEnabled(false);
                }
                if (state.equals(getString(R.string.pre_auth_closed))){
                    btn_finish.setVisibility(View.GONE);
                    btn_blue_print.setVisibility(View.GONE);
                    tv_qr_numbertext.setPadding(0,0,0,60);
                }
                break;
                default:
                    break;
        }

        return viewFoot;
    }


    private void synchronizeOrder(){
        if (mOrder!=null){
            BillOrderManager.getInstance().authPayQuery("",mOrder.getAuthNo(),new UINotifyListener<Order>(){
                @Override
                public void onSucceed(Order result) {
                    super.onSucceed(result);
                    if (result!=null){
                        PreAutFinishActivity.startActivity(PreAuthActivity.this,result,3);//4改3
                    }
                }

                @Override
                public void onError(Object object) {
                    super.onError(object);
                    PreAuthActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showErrorDialog();
                        }
                    });
                }
            });
        }
    }

    private CustomDialog errorDialog;
    private void showErrorDialog(){
        errorDialog= new CustomDialog.Builder(PreAuthActivity.this)
                .gravity(Gravity.CENTER).widthdp(250)
                .cancelTouchout(true)
                .view(R.layout.dialog_error)
                .addViewOnclick(R.id.tv_confirm, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        errorDialog.dismiss();
                    }
                })
                .build();
        errorDialog.show();
        TextView tv_msg=(TextView)errorDialog.findViewById(R.id.tv_title);
        tv_msg.setText(getString(R.string.this_unauthorized));
    }


    public void bluePrint(final int printType){
        bindDeviceService();
        mOrder.setPartner(getString(R.string.tv_pay_user_stub));
        printerPreAuth = new PrinterPreAuth(this, printType,mOrder) {
            @Override
            protected void onDeviceServiceCrash() {
                PreAuthActivity.this.bindDeviceService();
            }

            @Override
            protected void displayPrinterInfo(String info) {
                Log.i("zhouwei", "打印完成" + info);
            }
        };
        printerPreAuth.startPrint();

        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                mOrder.setPartner(getString(R.string.tv_pay_mch_stub));
                printerPreAuth = new PrinterPreAuth(PreAuthActivity.this, printType,mOrder) {
                    @Override
                    protected void onDeviceServiceCrash() {
                        PreAuthActivity.this.bindDeviceService();
                    }

                    @Override
                    protected void displayPrinterInfo(String info) {
                        Log.i("zhouwei", "打印完成" + info);
                    }
                };
                printerPreAuth.startPrint();
            }
        }, 4000L);
    }

    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void showDialog() {
        dialog = new DialogInfo(PreAuthActivity.this, null, getString(R.string.tx_blue_set), getString(R.string.title_setting), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                showPage(BluetoothSettingActivity.class);
                dialog.cancel();
                dialog.dismiss();
            }

            @Override
            public void handleCancleBtn() {
                dialog.cancel();
            }
        }, null);

        DialogHelper.resize(PreAuthActivity.this, dialog);
        dialog.show();
    }

}
