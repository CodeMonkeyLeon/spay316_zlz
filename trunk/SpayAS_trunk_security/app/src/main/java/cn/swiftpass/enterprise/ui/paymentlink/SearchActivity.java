package cn.swiftpass.enterprise.ui.paymentlink;

import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.DATA_TAG;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.DEFAULT_PAGE_SIZE;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.MANAGE_EDIT_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.MANAGE_EDIT_ORDER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.MANAGE_EDIT_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_CREATE_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_CREATE_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.RESULT_CODE_SEARCH_CHANGE;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.SEARCH_TYPE_ADD_ORDER_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.SEARCH_TYPE_ADD_ORDER_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.SEARCH_TYPE_MANAGE_CUSTOMER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.SEARCH_TYPE_MANAGE_ORDER;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.SEARCH_TYPE_MANAGE_PRODUCT;
import static cn.swiftpass.enterprise.ui.paymentlink.constant.PaymentLinkConstant.TYPE_TAG;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.SearchContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.SearchPresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.paymentlink.adapter.BaseRecyclerAdapter;
import cn.swiftpass.enterprise.ui.paymentlink.adapter.CustomerListAdapter;
import cn.swiftpass.enterprise.ui.paymentlink.adapter.OnListItemClickListener;
import cn.swiftpass.enterprise.ui.paymentlink.adapter.OrderListAdapter;
import cn.swiftpass.enterprise.ui.paymentlink.adapter.ProductListAdapter;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkCustomer;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkCustomerModel;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkOrder;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkOrderModel;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkProduct;
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkProductModel;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.DialogUtils;
import cn.swiftpass.enterprise.utils.KotlinUtils;

/**
 * Created by congwei.li on 2021/9/27.
 *
 * @Description:
 */
public class SearchActivity extends BaseActivity<SearchContract.Presenter> implements SearchContract.View {

    protected Dialog dialog;
    @BindView(R.id.et_order_search_input)
    EditText etOrderSearchInput;
    @BindView(R.id.et_order_search_clear_btn)
    ImageView ivSearchClearBtn;
    @BindView(R.id.tv_search_cancel)
    TextView tvSearchCancel;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.ll_search_notices)
    LinearLayout llNotices;
    @BindView(R.id.ll_expand1)
    ConstraintLayout llExpand1;
    @BindView(R.id.ll_expand2)
    ConstraintLayout llExpand2;
    @BindView(R.id.ll_expand3)
    ConstraintLayout llExpand3;
    @BindView(R.id.ll_expand4)
    ConstraintLayout llExpand4;
    @BindView(R.id.tv_expand1_title)
    TextView tvExpand1Title;
    @BindView(R.id.tv_expand1_content)
    TextView tvExpand1Content;
    @BindView(R.id.tv_expand2_title)
    TextView tvExpand2Title;
    @BindView(R.id.tv_expand2_content)
    TextView tvExpand2Content;
    @BindView(R.id.tv_expand3_title)
    TextView tvExpand3Title;
    @BindView(R.id.tv_expand3_content)
    TextView tvExpand3Content;
    @BindView(R.id.tv_expand4_title)
    TextView tvExpand4Title;
    @BindView(R.id.tv_expand4_content)
    TextView tvExpand4Content;
    @BindView(R.id.sw_payment_link_list)
    SwipeRefreshLayout swSearchResult;
    @BindView(R.id.rv_payment_link_order)
    RecyclerView rvSearchResultOrder;
    @BindView(R.id.rv_payment_link_customer)
    RecyclerView rvSearchResultCustomer;
    @BindView(R.id.rv_payment_link_product)
    RecyclerView rvSearchResultProduct;
    @BindView(R.id.tv_no_data)
    TextView tvNoData;
    private int searchType;
    private int mSearchExpend;
    private OrderListAdapter orderListAdapter;
    private CustomerListAdapter customerListAdapter;
    private ProductListAdapter productListAdapter;
    private ArrayList<PaymentLinkOrder> orders = new ArrayList<>();
    private ArrayList<PaymentLinkCustomer> customers = new ArrayList<>();
    private ArrayList<PaymentLinkProduct> products = new ArrayList<>();
    private int orderPage = 1;
    private int customerPage = 1;
    private int productPage = 1;
    private boolean isChange = false;

    public static void startActivity(Activity mContext, int searchType, String hintText) {
        Intent intent = new Intent(mContext, SearchActivity.class);
        intent.putExtra(TYPE_TAG, searchType);
        intent.putExtra(DATA_TAG, hintText);
        mContext.startActivity(intent);
    }

    public static void startActivityForResult(Activity mContext, int searchType, String hintText) {
        Intent intent = new Intent(mContext, SearchActivity.class);
        intent.putExtra(TYPE_TAG, searchType);
        intent.putExtra(DATA_TAG, hintText);
        mContext.startActivityForResult(intent, searchType);
    }

    @Override
    protected SearchContract.Presenter createPresenter() {
        return new SearchPresenter();
    }

    @Override
    protected boolean isWindowFeatureNoTitle() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        KotlinUtils.INSTANCE.isReStartApp(savedInstanceState, this);
        setContentView(R.layout.activity_search_for_payment_link);
        ButterKnife.bind(this);

        searchType = getIntent().getIntExtra(TYPE_TAG, SEARCH_TYPE_MANAGE_ORDER);
        String hintText = getIntent().getStringExtra(DATA_TAG);
        etOrderSearchInput.setHint(hintText);
        initView(savedInstanceState);
    }

    private void initView(Bundle savedInstanceState) {

        initListParam();

        switch (searchType) {
            case SEARCH_TYPE_MANAGE_ORDER:
                tvExpand1Title.setText(getString(R.string.payment_link_order_result_order_id));
                tvExpand2Title.setText(getString(R.string.pl_order_platform_order_id));
                tvExpand3Title.setText(getString(R.string.payment_link_order_result_customer_name));
                tvExpand4Title.setText(getString(R.string.payment_link_order_result_remark));
                break;
            case SEARCH_TYPE_MANAGE_PRODUCT:
            case SEARCH_TYPE_ADD_ORDER_PRODUCT:
                llExpand3.setVisibility(View.GONE);
                llExpand4.setVisibility(View.GONE);
                tvExpand1Title.setText(getString(R.string.payment_link_add_product_name));
                tvExpand2Title.setText(getString(R.string.payment_link_add_product_phone_code));
                break;
            case SEARCH_TYPE_MANAGE_CUSTOMER:
            case SEARCH_TYPE_ADD_ORDER_CUSTOMER:
                llExpand2.setVisibility(View.GONE);
                llExpand3.setVisibility(View.GONE);
                llExpand4.setVisibility(View.GONE);
                tvExpand1Title.setText(getString(R.string.payment_link_order_result_customer_name));
                break;
        }
        etOrderSearchInput.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(etOrderSearchInput.getText().toString().trim())) {
                    llNotices.setVisibility(View.VISIBLE);
                    ivSearchClearBtn.setVisibility(View.VISIBLE);
                } else {
                    llNotices.setVisibility(View.GONE);
                    ivSearchClearBtn.setVisibility(View.GONE);
                }
                tvExpand1Content.setText(etOrderSearchInput.getText().toString().trim());
                tvExpand2Content.setText(etOrderSearchInput.getText().toString().trim());
                tvExpand3Content.setText(etOrderSearchInput.getText().toString().trim());
                tvExpand4Content.setText(etOrderSearchInput.getText().toString().trim());
            }
        });
        etOrderSearchInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus && !TextUtils.isEmpty(etOrderSearchInput.getText().toString().trim())) {
                    llNotices.setVisibility(View.VISIBLE);
                    ivSearchClearBtn.setVisibility(View.VISIBLE);
                } else {
                    llNotices.setVisibility(View.GONE);
                    ivSearchClearBtn.setVisibility(View.GONE);
                }
            }
        });
        ivSearchClearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etOrderSearchInput.setText("");
                ivSearchClearBtn.setVisibility(View.GONE);
            }
        });

        tvSearchCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isChange) {
                    returnMainList();
                } else {
                    if (!SearchActivity.this.isFinishing()) {
                        SearchActivity.this.finish();
                    }
                }
            }
        });
        llExpand1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSearchExpend = 1;
                requestPage1();
            }
        });
        llExpand2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSearchExpend = 2;
                requestPage1();
            }
        });
        llExpand3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSearchExpend = 3;
                requestPage1();
            }
        });
        llExpand4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSearchExpend = 4;
                requestPage1();
            }
        });

        swSearchResult.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestPage1();
            }
        });
    }

    private void requestPage1() {
        orderPage = 1;
        productPage = 1;
        customerPage = 1;
        searchExpend("1");
        llContent.clearFocus();
    }

    private void initListParam() {
        rvSearchResultOrder.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        rvSearchResultProduct.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        rvSearchResultCustomer.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));

        orderListAdapter = new OrderListAdapter(this, orders);
        orderListAdapter.setOnLoadMoreListener(new BaseRecyclerAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                searchExpend(String.valueOf(++orderPage));
            }
        }, rvSearchResultOrder);
        orderListAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                OrderDetailsActivity.startActivity(SearchActivity.this,
                        orders.get(position), MANAGE_EDIT_ORDER);
            }
        });
        rvSearchResultOrder.setAdapter(orderListAdapter);

        productListAdapter = new ProductListAdapter(this, products,
                searchType == SEARCH_TYPE_MANAGE_PRODUCT);
        productListAdapter.setOnLoadMoreListener(new BaseRecyclerAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                searchExpend(String.valueOf(++productPage));
            }
        }, rvSearchResultProduct);
        productListAdapter.setItemClickListener(new OnListItemClickListener() {
            @Override
            public void onDeleteClick(int position) {
                showDialog(new FragmentTabPaymentLink.DialogClick() {
                    @Override
                    public void onOkClick() {
                        deleteProduct(position);
                    }

                    @Override
                    public void onCancelClick() {
                    }
                });
            }

            @Override
            public void onItemClick(int position) {
                if (searchType == SEARCH_TYPE_MANAGE_PRODUCT) {
                    CustAndProdEditActivity.startActivityForResult(SearchActivity.this,
                            MANAGE_EDIT_PRODUCT, products.get(position));
                } else if (searchType == SEARCH_TYPE_ADD_ORDER_PRODUCT) {
                    returnSelectedProduct(products.get(position));
                }
            }

            @Override
            public void onDeleteShow(int position, boolean isShow) {

            }
        });
        rvSearchResultProduct.setAdapter(productListAdapter);

        customerListAdapter = new CustomerListAdapter(this, customers,
                searchType == SEARCH_TYPE_MANAGE_CUSTOMER);
        customerListAdapter.setOnLoadMoreListener(new BaseRecyclerAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                searchExpend(String.valueOf(++customerPage));
            }
        }, rvSearchResultCustomer);
        customerListAdapter.setItemClickListener(new OnListItemClickListener() {
            @Override
            public void onDeleteClick(int position) {
                showDialog(new FragmentTabPaymentLink.DialogClick() {
                    @Override
                    public void onOkClick() {
                        deleteCustomer(position);
                    }

                    @Override
                    public void onCancelClick() {
                    }
                });
            }

            @Override
            public void onItemClick(int position) {
                if (searchType == SEARCH_TYPE_MANAGE_CUSTOMER) {
                    CustAndProdEditActivity.startActivityForResult(SearchActivity.this,
                            MANAGE_EDIT_CUSTOMER, customers.get(position));
                } else {
                    returnSelectCustomer(customers.get(position));
                }
            }

            @Override
            public void onDeleteShow(int position, boolean isShow) {
            }
        });
        rvSearchResultCustomer.setAdapter(customerListAdapter);
    }


    @Override
    public void deleteProductSuccess(String response, int position) {
        isChange = true;
        products.remove(products.get(position));
        productListAdapter.notifyDataSetChanged();
    }

    @Override
    public void deleteProductFailed(Object error) {
        DialogUtils.toastDialog(SearchActivity.this, error.toString(), null);
    }

    private void deleteProduct(int position) {
        if (mPresenter != null) {
            mPresenter.deleteProduct(products.get(position), position);
        }
    }


    @Override
    public void deleteCustomerSuccess(String response, int position) {
        isChange = true;
        customers.remove(customers.get(position));
        customerListAdapter.notifyDataSetChanged();
    }

    @Override
    public void deleteCustomerFailed(Object error) {
        DialogUtils.toastDialog(SearchActivity.this, error.toString(), null);
    }

    private void deleteCustomer(int position) {
        if (mPresenter != null) {
            mPresenter.deleteCustomer(customers.get(position), position);
        }
    }

    private void searchExpend(String pageNum) {
        switch (mSearchExpend) {
            case 1:
                requestSearch(etOrderSearchInput.getText().toString().trim(),
                        "", "", "", pageNum);
                break;
            case 2:
                requestSearch("", etOrderSearchInput.getText().toString().trim(),
                        "", "", pageNum);
                break;
            case 3:
                requestSearch("", "", etOrderSearchInput.getText().toString().trim(),
                        "", pageNum);
                break;
            case 4:
                requestSearch("", "", "",
                        etOrderSearchInput.getText().toString().trim(), pageNum);
                break;
        }
    }

    private void requestSearch(String expand1, String expand2, String expand3, String expand4,
                               String pageNumber) {
        switch (searchType) {
            case SEARCH_TYPE_MANAGE_ORDER:
                requestOrderList(expand1, expand2, expand3, expand4, pageNumber);
                break;
            case SEARCH_TYPE_MANAGE_PRODUCT:
            case SEARCH_TYPE_ADD_ORDER_PRODUCT:
                requestProductList(pageNumber, expand1, expand2);
                break;
            case SEARCH_TYPE_ADD_ORDER_CUSTOMER:
            case SEARCH_TYPE_MANAGE_CUSTOMER:
                requestCustomerList(pageNumber, expand1);
                break;
        }

    }

    @Override
    public void getOrderListSuccess(PaymentLinkOrderModel response) {
        showListData(response);
    }

    @Override
    public void getOrderListFailed(Object error) {
        showErrorMsgDialog(error);
    }

    private void requestOrderList(String orderNo, String realPlatOrderNo, String custName,
                                  String orderRemark, String pageNumber) {
        if (mPresenter != null) {
            swSearchResult.setRefreshing(false);
            mPresenter.getOrderList(
                    orderNo,
                    realPlatOrderNo,
                    custName,
                    orderRemark,
                    pageNumber
            );
        }
    }


    @Override
    public void getProductListSuccess(PaymentLinkProductModel response) {
        showListData(response);
    }

    @Override
    public void getProductListFailed(Object error) {
        showErrorMsgDialog(error);
    }

    private void requestProductList(String pageNumber, String goodsName, String goodsCode) {

        if (mPresenter != null) {
            swSearchResult.setRefreshing(false);
            mPresenter.getProductList(
                    pageNumber,
                    "",
                    goodsName,
                    goodsCode
            );
        }
    }

    @Override
    public void getCustomerListSuccess(PaymentLinkCustomerModel response) {
        showListData(response);
    }

    @Override
    public void getCustomerListFailed(Object error) {
        showErrorMsgDialog(error);
    }

    private void requestCustomerList(String pageNumber, String custName) {

        if (mPresenter != null) {
            swSearchResult.setRefreshing(false);
            mPresenter.getCustomerList(pageNumber, custName, "", "");
        }
    }

    private void showListData(Serializable serializable) {
        SearchActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogUtils.dismissDialog(SearchActivity.this);
                if (serializable instanceof PaymentLinkOrderModel) {
                    requestOrderListSuccess(((PaymentLinkOrderModel) serializable).data);
                }
                if (serializable instanceof PaymentLinkCustomerModel) {
                    requestCustomerListSuccess(((PaymentLinkCustomerModel) serializable).data);
                }
                if (serializable instanceof PaymentLinkProductModel) {
                    requestProductListSuccess(((PaymentLinkProductModel) serializable).data);
                }
            }
        });
    }

    public void requestOrderListSuccess(final ArrayList<PaymentLinkOrder> data) {
        if (isDataEmpty(orderPage, data)) {
            return;
        }
        if (orderPage != 1) {
            Iterator<PaymentLinkOrder> it = data.iterator();
            while (it.hasNext()) {
                PaymentLinkOrder order = it.next();
                for (int i = 0; i < orders.size(); i++) {
                    if (order.orderNo.equals(orders.get(i).orderNo)) {
                        it.remove();
                    }
                }
            }
        }
        if (orderPage == 1) {
            orders.clear();
            orderListAdapter.setDataList(orders);
        }
        orders.addAll(data);
        if (data.size() == 0 || data.size() < DEFAULT_PAGE_SIZE) {
            orderListAdapter.loadMoreEnd(false);
        } else {
            orderListAdapter.loadMoreComplete();
        }
        orderListAdapter.notifyDataSetChanged();
    }

    public void requestCustomerListSuccess(ArrayList<PaymentLinkCustomer> data) {
        if (isDataEmpty(customerPage, data)) {
            return;
        }
        if (customerPage != 1) {
            Iterator<PaymentLinkCustomer> it = data.iterator();
            while (it.hasNext()) {
                PaymentLinkCustomer order = it.next();
                for (int i = 0; i < customers.size(); i++) {
                    if (order.custId.equals(customers.get(i).custId)) {
                        it.remove();
                    }
                }
            }
        }
        if (customerPage == 1) {
            customers.clear();
            customerListAdapter.setDataList(customers);
        }
        customers.addAll(data);
        if (data.size() == 0 || data.size() < DEFAULT_PAGE_SIZE) {
            customerListAdapter.loadMoreEnd(false);
        } else {
            customerListAdapter.loadMoreComplete();
        }
        customerListAdapter.notifyDataSetChanged();
    }

    public void requestProductListSuccess(ArrayList<PaymentLinkProduct> data) {
        if (isDataEmpty(productPage, data)) {
            return;
        }
        if (productPage != 1) {
            Iterator<PaymentLinkProduct> it = data.iterator();
            while (it.hasNext()) {
                PaymentLinkProduct product = it.next();
                for (int i = 0; i < products.size(); i++) {
                    if (product.goodsId.equals(products.get(i).goodsId)) {
                        it.remove();
                    }
                }
            }
        }
        if (productPage == 1) {
            products.clear();
            productListAdapter.setDataList(products);
        }
        products.addAll(data);
        if (data.size() == 0 || data.size() < DEFAULT_PAGE_SIZE) {
            productListAdapter.loadMoreEnd(false);
        } else {
            productListAdapter.loadMoreComplete();
        }
        productListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CODE_CREATE_CUSTOMER) {
            if (requestCode == MANAGE_EDIT_CUSTOMER && null != data) {
                PaymentLinkCustomer customerSelect = (PaymentLinkCustomer) data.getSerializableExtra("result");
                if (null == customerSelect) {
                    return;
                }
                for (int i = 0; i < customers.size(); i++) {
                    if (customers.get(i).custId.equals(customerSelect.custId)) {
                        isChange = true;
                        customers.get(i).custEmail = customerSelect.custEmail;
                        customers.get(i).custMobile = customerSelect.custMobile;
                        customers.get(i).custName = customerSelect.custName;
                        customerListAdapter.notifyItemChanged(i);
                        break;
                    }
                }
            }
        }

        if (resultCode == RESULT_CODE_CREATE_PRODUCT) {
            if (requestCode == MANAGE_EDIT_PRODUCT && null != data) {
                PaymentLinkProduct product = (PaymentLinkProduct) data.getSerializableExtra("result");
                if (null == product) {
                    return;
                }
                for (int i = 0; i < products.size(); i++) {
                    if (products.get(i).goodsId.equals(product.goodsId)) {
                        isChange = true;
                        products.get(i).goodsName = product.goodsName;
                        products.get(i).goodsCode = product.goodsCode;
                        products.get(i).goodsPrice = product.goodsPrice;
                        products.get(i).goodsDesc = product.goodsDesc;
                        products.get(i).goodsPic = product.goodsPic;
                        productListAdapter.notifyItemChanged(i);
                        break;
                    }
                }
            }
        }
    }

    private void showErrorMsgDialog(Object object) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogUtils.dismissDialog(SearchActivity.this);
                DialogUtils.toastDialog(SearchActivity.this, object.toString(), null);
                switch (searchType) {
                    case SEARCH_TYPE_MANAGE_ORDER:
                        requestOrderListSuccess(new ArrayList<>());
                        break;
                    case SEARCH_TYPE_MANAGE_PRODUCT:
                    case SEARCH_TYPE_ADD_ORDER_PRODUCT:
                        requestProductListSuccess(new ArrayList<>());
                        break;
                    case SEARCH_TYPE_ADD_ORDER_CUSTOMER:
                    case SEARCH_TYPE_MANAGE_CUSTOMER:
                        requestCustomerListSuccess(new ArrayList<>());
                        break;
                }
            }
        });
    }

    public void showDialog(FragmentTabPaymentLink.DialogClick dialogClick) {
        dialog = new DialogInfo(SearchActivity.this, null,
                getString(R.string.payment_link_delete_confirm),
                getString(R.string.bt_confirm), getString(R.string.payment_link_add_order_cancel),
                DialogInfo.UNIFED_DIALOG,
                new DialogInfo.HandleBtn() {
                    @Override
                    public void handleOkBtn() {
                        if (dialogClick != null) {
                            dialogClick.onOkClick();
                        }
                        dialog.cancel();
                        dialog.dismiss();
                    }

                    @Override
                    public void handleCancelBtn() {
                        if (dialogClick != null) {
                            dialogClick.onCancelClick();
                        }
                        dialog.cancel();
                    }
                }, null);
        DialogHelper.resize(SearchActivity.this, dialog);
        dialog.show();
    }

    public boolean isDataEmpty(int page, List data) {
        if ((page == 1) && (data == null || data.size() <= 0)) {
            rvSearchResultOrder.setVisibility(View.GONE);
            rvSearchResultProduct.setVisibility(View.GONE);
            rvSearchResultCustomer.setVisibility(View.GONE);
            tvNoData.setVisibility(View.VISIBLE);
            return true;
        } else {
            rvSearchResultOrder.setVisibility(searchType == SEARCH_TYPE_MANAGE_ORDER ?
                    View.VISIBLE : View.GONE);
            rvSearchResultProduct.setVisibility(searchType == SEARCH_TYPE_MANAGE_PRODUCT ||
                    searchType == SEARCH_TYPE_ADD_ORDER_PRODUCT ?
                    View.VISIBLE : View.GONE);
            rvSearchResultCustomer.setVisibility(searchType == SEARCH_TYPE_ADD_ORDER_CUSTOMER ||
                    searchType == SEARCH_TYPE_MANAGE_CUSTOMER ?
                    View.VISIBLE : View.GONE);
            tvNoData.setVisibility(View.GONE);
            return false;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            if (isChange) {
                returnMainList();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private void hideAllResult() {
        rvSearchResultOrder.setVisibility(View.GONE);
        rvSearchResultProduct.setVisibility(View.GONE);
        rvSearchResultCustomer.setVisibility(View.GONE);
        tvNoData.setVisibility(View.GONE);
    }

    private void returnSelectedProduct(PaymentLinkProduct product) {
        product.goodsNum = "1";
        Intent i = new Intent();
        i.putExtra("result", product);
        SearchActivity.this.setResult(RESULT_CODE_CREATE_PRODUCT, i);
        SearchActivity.this.finish();
    }

    private void returnSelectCustomer(PaymentLinkCustomer customer) {
        Intent i = new Intent();
        i.putExtra("result", customer);
        SearchActivity.this.setResult(RESULT_CODE_CREATE_CUSTOMER, i);
        SearchActivity.this.finish();
    }

    private void returnMainList() {
        Intent i = new Intent();
        SearchActivity.this.setResult(RESULT_CODE_SEARCH_CHANGE, i);
        SearchActivity.this.finish();
    }
}
