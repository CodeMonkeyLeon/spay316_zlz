package cn.swiftpass.enterprise.ui.view

import android.content.Context
import android.graphics.Typeface
import android.text.TextUtils
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.utils.KotlinUtils
import cn.swiftpass.enterprise.utils.QRCodeUtils

class ItemTicketView(
    var mLayoutType: String,
    var mContext: Context,
    var attrs: AttributeSet? = null,
    var defStyleAttr: Int = 0
) : LinearLayout(mContext, attrs, defStyleAttr) {

    companion object {
        const val LAYOUT_TYPE_HORIZONTAL = "Horizontal"
        const val LAYOUT_TYPE_VERTICAL = "Vertical"
        const val LAYOUT_TYPE_MULTI_LINES = "Multi_lines"
        const val LAYOUT_TYPE_OR_CODE = "Qr_code"
    }

    private fun getLayoutId() = when (mLayoutType) {
        LAYOUT_TYPE_MULTI_LINES -> R.layout.item_ticket_multi_lines
        LAYOUT_TYPE_VERTICAL -> R.layout.item_ticket_vertical
        LAYOUT_TYPE_OR_CODE -> R.layout.item_ticket_qr_code
        else -> R.layout.item_ticket_horizonal
    }


    private lateinit var mRlQrCodeRoot: RelativeLayout
    private lateinit var mImgQrcode: ImageView

    private lateinit var mLlRoot: LinearLayout
    private lateinit var mTvTitle: TextView
    private lateinit var mTvValue: TextView


    init {
        initView()
    }


    /**
     * 初始化布局
     */
    private fun initView() {
        //加载布局
        LayoutInflater.from(context).inflate(getLayoutId(), this)
        //绑定控件
        mTvTitle = findViewById(R.id.id_tv_title)
        if (TextUtils.equals(mLayoutType, LAYOUT_TYPE_OR_CODE)) {
            //二维码布局
            mRlQrCodeRoot = findViewById(R.id.id_rl_root)
            mImgQrcode = findViewById(R.id.id_img_or_code)
        } else {
            //其他布局
            mLlRoot = findViewById(R.id.id_ll_root)
            mTvValue = findViewById(R.id.id_tv_value)
        }
    }


    /**
     * 设置二维码
     */
    fun setQrCode(qrCode: String) {
        val bitmap = QRCodeUtils.generateQRCodeByString(qrCode)
        mImgQrcode.setImageBitmap(bitmap)
    }


    /**
     * 设置标题
     */
    fun setTitleText(title: String?) {
        mTvTitle.text = title
    }

    /**
     * 设置内容值
     */
    fun setValueText(value: String?) {
        mTvValue.text = value
//        if (TextUtils.equals(mLayoutType, LAYOUT_TYPE_HORIZONTAL)) {
//            mTvValue.post {
//                if (mTvValue.lineCount > 1) {
//                    mTvValue.gravity = Gravity.LEFT
//                }
//            }
//        }
    }


    /**
     * 设置内容粗体样式
     */
    fun setValueTextBold(isValueTextBold: Boolean) {
        if (isValueTextBold) {
            mTvValue.typeface = Typeface.DEFAULT_BOLD
        }
    }


    /**
     * 设置标题粗体样式
     */
    fun setTitleTextBold(isTitleTextBold: Boolean) {
        if (isTitleTextBold) {
            mTvTitle.typeface = Typeface.DEFAULT_BOLD
        }
    }


    /**
     * 设置value文字内容偏左偏右
     */
    fun setValueTextRightGravity(isTextRightGravity: Boolean) {
        if (isTextRightGravity) {
            //右对齐
            mTvValue.gravity = Gravity.RIGHT
        }
    }


    /**
     * 隐藏ValueTextView
     */
    fun hideValueTextView(isHideValueTextView: Boolean) {
        if (isHideValueTextView) {
            //隐藏
            mTvValue.visibility = View.GONE
        } else {
            //显示
            mTvValue.visibility = View.VISIBLE
        }
    }


    /**
     * 隐藏TitleTextView
     */
    fun hideTitleTextView(isHideTitleTextView: Boolean) {
        if (isHideTitleTextView) {
            //隐藏
            mTvTitle.visibility = View.GONE
        } else {
            //显示
            mTvTitle.visibility = View.VISIBLE
        }
    }


    /**
     * 色设置文字内容
     */
    fun setText(title: String, value: String) {
        mTvTitle.text = title
        mTvValue.text = value
    }


    /**
     * 设置布局参数
     */
    fun setLayoutParams(topMargin: Int) {
        val layoutParam = mLlRoot.layoutParams as LinearLayout.LayoutParams
        layoutParam.topMargin = KotlinUtils.dp2px(topMargin.toFloat(), mContext)
        mLlRoot.layoutParams = layoutParam
    }

    /**
     * 设置二维码布局参数
     */
    fun setQrLayoutParam(topMargin: Int) {
        val layoutParam = mRlQrCodeRoot.layoutParams as LinearLayout.LayoutParams
        layoutParam.topMargin = KotlinUtils.dp2px(topMargin.toFloat(), mContext)
        mRlQrCodeRoot.layoutParams = layoutParam
    }

}