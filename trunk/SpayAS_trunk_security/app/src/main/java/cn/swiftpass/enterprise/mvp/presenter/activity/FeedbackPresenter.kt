package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.intl.BuildConfig
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.FeedbackContract
import com.example.common.entity.EmptyData
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @如果大地早已冰封
 * @就让我们面对着暖流
 * @走向海
 */
class FeedbackPresenter : FeedbackContract.Presenter {

    private var mView: FeedbackContract.View? = null


    override fun sendFeedBack(content: String?) {
        mView?.let { view ->
            view.showLoading(R.string.loading, ParamsConstants.COMMON_LOADING)
            AppClient.sendFeedBack(
                MainApplication.getInstance().getUserId().toString(),
                content,
                BuildConfig.SPAY.toString(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.sendFeedBackFailed(it.message)
                        }
                    }


                    override fun onResponse(response: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        view.sendFeedBackSuccess(true)
                    }
                }
            )
        }
    }

    override fun attachView(view: FeedbackContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}