package cn.swiftpass.enterprise.ui.paymentlink

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.GestureDetector.OnDoubleTapListener
import android.view.MotionEvent
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.ui.activity.BaseActivity
import cn.swiftpass.enterprise.ui.widget.DialogInfo
import cn.swiftpass.enterprise.ui.widget.TitleBar.OnTitleBarClickListener
import cn.swiftpass.enterprise.utils.DialogHelper
import cn.swiftpass.enterprise.utils.KotlinUtils
import com.bumptech.glide.Glide
import com.github.chrisbanes.photoview.PhotoView

class BigImageDisplayActivity : BaseActivity<BasePresenter<*>>() {
    private fun getLayoutId() = R.layout.act_big_image_display

    private lateinit var mImgUri: Uri

    //    private lateinit var mImgView: ImageView
    private lateinit var mPhotoView: PhotoView


//    //缩放比例
//    private var scaleFactor = 1.0f
//
//    //记录上一次坐标
//    private var lastX = 0f
//    private var lastY = 0f
//
//
//    private lateinit var mGes: GestureDetector
//    private lateinit var mScaleGes: ScaleGestureDetector
//
//    // 处理手势事件
//    inner class MyGestureListener : GestureDetector.SimpleOnGestureListener() {
//        override fun onDown(e: MotionEvent): Boolean {
//            // 记录手指按下的位置
//            if (e.pointerCount <= 1) {
//                lastX = e.x
//                lastY = e.y
//                showLog.invoke("onDown  e:  x: ${e.x}      y: ${e.y}")
//            }
//            return true
//        }
//
//        override fun onScroll(
//            e1: MotionEvent,
//            e2: MotionEvent,
//            distanceX: Float,
//            distanceY: Float
//        ): Boolean {
//            if (e2.pointerCount <= 1) {
//
//                showLog.invoke("onScroll  e1:  x: ${e1.x}      y: ${e1.y}")
//                showLog.invoke("onScroll  e2:  x: ${e2.x}      y: ${e2.y}")
//
//
//                // 计算手指滑动的距离
//                val dx = e2.x - e1.x
//                val dy = e2.y - e1.y
//
//
//                // 将图片向滑动方向移动
//                mImgView.translationX += dx
//                mImgView.translationY += dy
//
//                // 记录上一次手指的位置
////                lastX = e2.x
////                lastY = e2.y
//            }
//            return false
//        }
//    }
//
//    // 处理缩放手势事件
//    inner class MyScaleGestureListener : ScaleGestureDetector.SimpleOnScaleGestureListener() {
//        override fun onScale(detector: ScaleGestureDetector): Boolean {
//
//
//            // 计算缩放比例
//            scaleFactor *= detector.scaleFactor
//
//            // 限制缩放比例在最小和最大值之间
//            scaleFactor = maxOf(1.0f, minOf(scaleFactor, 2.0f))
//
//            // 将图片缩放到指定比例
//            mImgView.scaleX = scaleFactor
//            mImgView.scaleY = scaleFactor
//
//            return false
//        }
//    }


    val showLog: (String) -> Unit = { msg ->
        Log.i("TAG_ZLZ", "$msg")
    }


    companion object {
        private const val PARAM_URI = "param_uri"
        const val REQUEST_CODE = 1001
        const val RESULT_CODE = 1002
        const val IS_DELETE_PHOTO = "is_delete_photo"


        fun startBigImageDisplayActivity(fromActivity: Activity, uri: Uri?) {
            val intent = Intent(fromActivity, BigImageDisplayActivity::class.java)
            intent.putExtra(PARAM_URI, uri)
            fromActivity.startActivityForResult(intent, REQUEST_CODE)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())



        intent?.let {
            val uri: Uri? = it.getParcelableExtra(PARAM_URI)
            uri?.let { u ->
                mImgUri = u
                initView()
            }
        }
    }


    @SuppressLint("ClickableViewAccessibility")
    private fun initView() {
        mPhotoView = findViewById(R.id.id_photo_view)


//        mPhotoView.setImageURI(mImgUri)

        mPhotoView.maximumScale = 2f
        mPhotoView.minimumScale = 1f

        Glide.with(this)
            .load(mImgUri)
            .into(mPhotoView)


        mPhotoView.setOnDoubleTapListener(object : OnDoubleTapListener {
            override fun onSingleTapConfirmed(e: MotionEvent) = true

            override fun onDoubleTap(e: MotionEvent): Boolean {
                if (mPhotoView.scale == 2f) {
                    mPhotoView.setScale(1f, true)
                } else {
                    mPhotoView.setScale(2f, true)
                }
                return true
            }

            override fun onDoubleTapEvent(e: MotionEvent) = true
        })


//        mGes = GestureDetector(this, MyGestureListener())
//        mScaleGes = ScaleGestureDetector(this, MyScaleGestureListener())
//
//
//        mImgView.setOnTouchListener { v, event ->
//            event?.let { e ->
//                mGes.onTouchEvent(e)
//                mScaleGes.onTouchEvent(e)
////                TouchController().onTouch(e)
//            }
//            true
//        }
    }


    override fun useToolBar() = true

    override fun setupTitleBar() {
        super.setupTitleBar()
        titleBar.setLeftButtonVisible(true)
        titleBar.setTitle(R.string.payment_link_add_order_image)
        titleBar.setRightButton(R.drawable.icon_toolbar_delete)
        titleBar.setRightButtonHeightWidth(
            KotlinUtils.dp2px(23f, this),
            KotlinUtils.dp2px(23f, this)
        )
        titleBar.setOnTitleBarClickListener(object : OnTitleBarClickListener {
            override fun onLeftButtonClick() {
                finish()
            }

            override fun onRightButtonClick() {
                deletePhoto()
            }

            override fun onRightLayClick() {

            }

            override fun onRightButLayClick() {

            }
        })
    }


    private fun deletePhoto() {

        val dialog = DialogInfo(
            this,
            getString(R.string.string_payment_link_delete_photo_dialog_title),
            getString(R.string.string_payment_link_delete_photo_dialog_content),
            getString(R.string.string_payment_link_delete_photo_dialog_delete),
            getString(R.string.string_payment_link_delete_photo_dialog_cancel),
            DialogInfo.UNIFED_DIALOG,
            object : DialogInfo.HandleBtn {
                override fun handleOkBtn() {
                    val result = Intent()
                    result.putExtra(IS_DELETE_PHOTO, true)
                    setResult(RESULT_CODE, result)
                    finish()
                }

                override fun handleCancelBtn() {

                }
            },
            null
        )
        dialog.setBtnOkTextColor(resources.getColor(R.color.cashier_add))
        dialog.setBtnCancelTextColor(resources.getColor(R.color.cashier_add))
        dialog.setBtnCancelTextBold()
        DialogHelper.resize(this@BigImageDisplayActivity, dialog)
        dialog.show()

    }


    override fun createPresenter() = null
}