package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.bussiness.model.OrderList
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.PreAuthContract
import cn.swiftpass.enterprise.utils.JsonUtil
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @阳光聚散
 * @我们不多说
 */
class PreAuthPresenter : PreAuthContract.Presenter {

    private var mView: PreAuthContract.View? = null


    override fun authPayQuery(orderNo: String?, outRequestNo: String?) {
        mView?.let { view ->

            AppClient.authPayQuery(
                MainApplication.getInstance().getMchId(),
                MainApplication.getInstance().getUserId().toString(),
                outRequestNo,
                orderNo,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        commonResponse?.let {
                            view.authPayQueryFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        r?.let { response ->
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                Order::class.java
                            ) as Order
                            view.authPayQuerySuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun queryOrderDetail(
        orderNo: String?,
        mchId: String?,
        isMark: Boolean
    ) {
        mView?.let { view ->

            AppClient.queryOrderDetail(
                orderNo,
                orderNo,
                orderNo,
                mchId,
                MainApplication.getInstance().getUserId().toString(),
                System.currentTimeMillis().toString(),
                isMark,
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        commonResponse?.let {
                            view.queryOrderDetailFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        r?.let { response ->
                            //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"",
                                "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                Order::class.java
                            ) as Order
                            order.add_time =
                                if (TextUtils.isEmpty(order.addTime)) 0 else order.addTime.toLong()
                            order.useId = order.userId.toString()
                            order.canAffim = order.canAffirm
                            order.setUplanDetailsBeans(order.discountDetail)
                            view.queryOrderDetailSuccess(order)
                        }
                    }
                }
            )

        }
    }

    override fun authOperateQuery(
        operationType: String?,
        authNo: String?,
        page: Int,
        pageSize: Int
    ) {
        mView?.let { view ->
            view.showLoading(R.string.loading, ParamsConstants.COMMON_LOADING)
            AppClient.authOperateQuery(
                MainApplication.getInstance().getMchId(),
                MainApplication.getInstance().getUserId().toString(),
                authNo,
                operationType,
                page.toString(),
                pageSize.toString(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<OrderList>(OrderList()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.authOperateQueryFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val orderList = JsonUtil
                                .jsonToBean(response.message, OrderList::class.java) as OrderList
                            for (order in orderList.data) {
                                order.setPageCount(orderList.pageCount)
                                order.setFinishTime(if (TextUtils.isEmpty(order.finishedTime)) 0 else order.finishedTime.toLong())
                            }
                            view.authOperateQuerySuccess(orderList.data)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: PreAuthContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}