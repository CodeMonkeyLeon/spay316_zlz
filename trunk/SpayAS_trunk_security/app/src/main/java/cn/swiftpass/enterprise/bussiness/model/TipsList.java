package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by congwei.li on 2022/4/27.
 *
 * @Description:
 */
public class TipsList implements Serializable {
    public ArrayList<TipsBean> data;
}
