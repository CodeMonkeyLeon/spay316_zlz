package cn.swiftpass.enterprise.demo;

/**
 * Created by YZX on 2019年04月08日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */
public class JsonResponse<T> {
    private String code;
    private String error;
    private T data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
