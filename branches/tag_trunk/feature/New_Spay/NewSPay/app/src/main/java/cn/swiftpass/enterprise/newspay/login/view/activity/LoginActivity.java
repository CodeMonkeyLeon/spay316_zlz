package cn.swiftpass.enterprise.newspay.login.view.activity;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import cn.swiftpass.enterprise.R;
import cn.swiftpass.enterprise.base.BaseActivity;
import cn.swiftpass.enterprise.config.RequestTypeConfig;
import cn.swiftpass.enterprise.config.RequestValueConfig;
import cn.swiftpass.enterprise.config.SPConfig;
import cn.swiftpass.enterprise.newspay.login.contract.LoginContract;
import cn.swiftpass.enterprise.newspay.login.model.bean.ExchangeKeyBean;
import cn.swiftpass.enterprise.newspay.login.model.bean.LoginBean;
import cn.swiftpass.enterprise.newspay.login.presenter.LoginPresenter;
import cn.swiftpass.enterprise.util.CodeUtils;
import cn.swiftpass.enterprise.util.ECDHUtils;
import cn.swiftpass.enterprise.util.LogUtil;
import cn.swiftpass.enterprise.util.SharedPreferencesUtil;
import cn.swiftpass.enterprise.widget.CustomDialog;


public class LoginActivity extends BaseActivity<LoginPresenter> implements LoginContract.View, View.OnClickListener {

    @BindView(R.id.tv_netset)
    TextView tv_netSet;
    @BindView(R.id.tv_forget_pwd)
    TextView tv_forgetPwd;
    @BindView(R.id.et_name)
    EditText et_name;
    @BindView(R.id.et_pwd)
    EditText et_pwd;
    @BindView(R.id.btn_login)
    Button btn_login;
    @BindView(R.id.iv_code)
    ImageView iv_code;
    @BindView(R.id.ll_code)
    LinearLayout ll_code;
    @BindView(R.id.et_code)
    EditText et_code;
    @BindView(R.id.tv_test)
    TextView tv_test;

    private CodeUtils mCodeUtils;
    private int mWrongPwdTag;
    private CustomDialog mErrorDialog;
    private String skey;
    private String sauthid;
    private boolean isExchangeKey;
    private boolean isDeviceLogin;


    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        initListener();
        initData();
    }

    private void initData() {
        initPermission();
        skey = SharedPreferencesUtil.getInstance().getString(SPConfig.LOGIN_SKEY, "");
        sauthid = SharedPreferencesUtil.getInstance().getString(SPConfig.LOGIN_SAUTHID, "");
        mPresenter = new LoginPresenter(this, this);
    }

    private void initPermission() {
        //8.0动态权限   临时获取
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int checkPermission = checkSelfPermission(Manifest.permission.READ_PHONE_STATE);
            if (checkPermission != PackageManager.PERMISSION_GRANTED) {
                //后面的1为请求码
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, 1);

            }
        }

    }

    private void initListener() {
        tv_netSet.setOnClickListener(this);
        tv_forgetPwd.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        iv_code.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_netset:
                startActivity(new Intent(LoginActivity.this, NetworkSettingActivity.class));
                break;
            case R.id.tv_forget_pwd:
                startActivity(new Intent(LoginActivity.this, ForgetPasswordActivity.class));
                break;
            case R.id.btn_login:
                login();
                break;
            case R.id.iv_code:
                if (mCodeUtils==null){
                    mCodeUtils = new CodeUtils();
                }
                iv_code.setImageBitmap(mCodeUtils.createBitmap());
                break;
            default:
                break;
        }
    }

    private void login() {
        if (verifyUserNamePwd()) {
            isExchangeKey=true;
            mPresenter.exchangeServiceKey();
            btn_login.setEnabled(false);
            btn_login.setText(R.string.bt_login_loading);
        }
    }

    /**
     * 私钥+秘钥  混合
     */

    @Override
    public void onExchangeKeySuccess(ExchangeKeyBean result) {
        mPresenter.login(et_name.getText().toString(), et_pwd.getText().toString());
    }

    @Override
    public void onLoginSuccess(LoginBean bean) {
        btn_login.setEnabled(true);
        btn_login.setText(R.string.login);
        if (bean.getCookieMap() != null && !TextUtils.isEmpty(bean.getCookieMap().getSKEY())) {
            SharedPreferencesUtil.getInstance().put(SPConfig.LOGIN_SKEY, bean.getCookieMap().getSKEY());
        }
        if (bean.getCookieMap() != null && !TextUtils.isEmpty(bean.getCookieMap().getSAUTHID())) {
            SharedPreferencesUtil.getInstance().put(SPConfig.LOGIN_SAUTHID, bean.getCookieMap().getSAUTHID());
        }
        tv_test.setText("登录成功"+bean.getUsername());
    }

    @Override
    public void onError(int fromRequest, String code, String errorMsg) {
        switch (fromRequest) {
            case RequestTypeConfig.LOGIN_TYPE:
                if (code.equals(400) ) {
                    mWrongPwdTag++;
                    if (mWrongPwdTag == 2) {
                        ll_code.setVisibility(View.VISIBLE);
                    }
                }
                break;
                case RequestTypeConfig.DEVICELOGIN_TYPE:
                   if (code.equals(401)){
                       //去调用获取skey
                      mPresenter.exchangeServiceKey();
                   }
                    break;
            default:
                break;
        }
        btn_login.setEnabled(true);
        btn_login.setText(R.string.login);
        LogUtil.e("onError------------" + code + "----" + errorMsg + "----------" + fromRequest);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCodeUtils != null) {
            mCodeUtils.recycle();
            mCodeUtils = null;
        }
    }

    /**
     * 验证账户密码输入是否正确
     *
     * @return
     */
    private boolean verifyUserNamePwd() {
        if (TextUtils.isEmpty(et_name.getText().toString())) {
            showLoginErrorDialog(getString(R.string.show_user_name));
            return false;
        }
        if (TextUtils.isEmpty(et_pwd.getText().toString())) {
            showLoginErrorDialog(getString(R.string.pay_login_pwd));
            return false;
        }

        if (mWrongPwdTag >= 2) {
            if (TextUtils.isEmpty(et_code.getText().toString()) && !mCodeUtils.getCode().equals(et_code.getText().toString())) {
                showLoginErrorDialog(getString(R.string.tx_ver_code));
                return false;
            }
        }
        return true;
    }

    private void showLoginErrorDialog(String title) {
        if (mErrorDialog == null) {
            mErrorDialog = new CustomDialog.Builder(mContext)
                    .gravity(Gravity.CENTER).widthdp(300)
                    .cancelTouchout(false)
                    .view(R.layout.dialog_login_error)
                    .addViewOnclick(R.id.tv_error_content, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mErrorDialog.dismiss();
                        }
                    }).build();
        }
        mErrorDialog.show();
        if (!TextUtils.isEmpty(title)) {
            TextView tvTitle = (TextView) mErrorDialog.findViewById(R.id.tv_error_title);
            tvTitle.setText(title);
        }
    }

}
