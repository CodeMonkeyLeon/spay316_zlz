package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.bussiness.model.WxCard
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @心的碎片
 * @又重新组合
 * @但再不是天真的我
 */
class RefundOrderListContract {


    interface View : BaseView {

        fun querySpayOrderSuccess(
            response: ArrayList<Order>?,
            isLoadMore: Boolean,
            page: Int
        )

        fun querySpayOrderFailed(error: Any?)


        fun queryRefundDetailSuccess(response: Order?)

        fun queryRefundDetailFailed(error: Any?)

        fun queryOrderDetailSuccess(response: Order?)

        fun queryOrderDetailFailed(error: Any?)


        fun queryCardDetailSuccess(response: WxCard?)


        fun queryCardDetailFailed(error: Any?)

    }


    interface Presenter : BasePresenter<View> {


        fun queryCardDetail(
            cardId: String?,
            cardCode: String?
        )


        fun queryOrderDetail(
            orderNo: String?,
            mchId: String?,
            isMark: Boolean
        )


        fun queryRefundDetail(
            orderNo: String?,
            mchId: String?
        )


        fun querySpayOrder(
            listStr: ArrayList<String?>?,
            payTypeList: ArrayList<String?>?,
            isRefund: Int,
            page: Int,
            order: String?,
            startDate: String?,
            isLoadMore: Boolean
        )
    }


}