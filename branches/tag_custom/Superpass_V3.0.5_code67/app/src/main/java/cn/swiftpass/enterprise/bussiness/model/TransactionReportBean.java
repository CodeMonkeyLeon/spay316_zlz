package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * Created by aijingya on 2019/4/15.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: ${TODO}(日报，周报，月报 数据列表的数据类型)
 * @date 2019/4/15.20:31.
 */
public class TransactionReportBean implements Serializable {
    private Long transactionAmount; //交易金额
    private int transactionCount; //交易笔数
    private double transactionRelativeRatio;   //环比
    private String startDate;        //开始时间
    private String endDate;          //结束时间

    public Long getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Long transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public int getTransactionCount() {
        return transactionCount;
    }

    public void setTransactionCount(int transactionCount) {
        this.transactionCount = transactionCount;
    }

    public double getTransactionRelativeRatio() {
        return transactionRelativeRatio;
    }

    public void setTransactionRelativeRatio(double transactionRelativeRatio) {
        this.transactionRelativeRatio = transactionRelativeRatio;
    }


    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }


}
