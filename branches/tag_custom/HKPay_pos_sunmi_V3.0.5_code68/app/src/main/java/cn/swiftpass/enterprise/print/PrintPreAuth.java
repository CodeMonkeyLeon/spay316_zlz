package cn.swiftpass.enterprise.print;

import android.text.TextUtils;

import java.util.Locale;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * Created by aijingya on 2020/8/18.
 *
 * @Package cn.swiftpass.enterprise.print
 * @Description:
 * @date 2020/8/18.20:20.
 */
public class PrintPreAuth {

    /**
     * printType参数1：预授权成功---预授权小票(反扫)
     *  * printType参数5：预授权成功---预授权小票(正扫)
     * printType参数4：预授权订单详情---预授权小票
     * printType参数2：预授权解冻详情小票
     * printType参数3：预授权解冻完成小票
     */
    public static void printPreAut(int printType, Order orderModel){
        int minus=1;
        //根据当前的小数点的位数来判断应该除以或者乘以多少
        for(int i = 0; i < MainApplication.numFixed ; i++ ){
            minus = minus * 10;
        }
        SunmiPrintHelper.getInstance().setAlign(1);
        switch (printType){
            case 1:
            case 4:
            case 5:
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.pre_auth_receipt) + "\n", 36, true, false);
                break;

            case 2:
            case 3:
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.pre_auth_unfreezed_receipt) + "\n", 36, true, false);
                break;
        }
        SunmiPrintHelper.getInstance().printText("\n", 28, false, false);
        SunmiPrintHelper.getInstance().setAlign(0);
        SunmiPrintHelper.getInstance().printText(orderModel.getPartner()+ "\n", 24, false, false);
        SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tv_pay_client_save)+ "\n", 24, false, false);

        SunmiPrintHelper.getInstance().printText("===========================\n", 28, false, false);

        if (!StringUtil.isEmptyOrNull(MainApplication.getMchName())) {
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.shop_name)+ "：" + "\n"+ MainApplication.getMchName() + "\n", 24, false, false);
        }
        if (!StringUtil.isEmptyOrNull(MainApplication.getMchId())) {
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_blue_print_merchant_no)+ "：" + MainApplication.getMchId() + "\n", 24, false, false);
        }

        if (printType==1) {
            try {
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.pre_auth_time) + "："+ "\n" + orderModel.getTradeTime() + "\n", 24, false, false);
            } catch (Exception e) {
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.pre_auth_time) + "：" + "\n"+ orderModel.getTradeTime() + "\n", 24, false, false);
            }
        }else if(printType==4){
            try {
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.pre_auth_time) + "："+ "\n" + orderModel.getTradeTimeNew() + "\n", 24, false, false);
            } catch (Exception e) {
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.pre_auth_time) + "：" + "\n"+ orderModel.getTradeTimeNew() + "\n", 24, false, false);
            }
        }else if(printType==5){
            try {
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.pre_auth_time) + "："+ "\n" + orderModel.getTimeEnd() + "\n", 24, false, false);
            } catch (Exception e) {
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.pre_auth_time) + "：" + "\n"+ orderModel.getTimeEnd() + "\n", 24, false, false);
            }
        } else if(printType==2){
            try {
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.unfreezed_time) + "："+ "\n" + orderModel.getOperateTime() + "\n", 24, false, false);
            } catch (Exception e) {
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.unfreezed_time) + "：" + "\n"+ orderModel.getOperateTime() + "\n", 24, false, false);
            }

        }else if(printType==3){
            try {
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.unfreezed_time) + "："+ "\n" + orderModel.getUnFreezeTime() + "\n", 24, false, false);
            } catch (Exception e) {
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.unfreezed_time) + "：" + "\n"+ orderModel.getUnFreezeTime() + "\n", 24, false, false);
            }
        }
        if (printType==1 || printType == 4 || printType == 5){
            //平台预授权订单号
            try {
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.platform_pre_auth_order_id) + "："+ "\n" + orderModel.getAuthNo() + "\n", 24, false, false);
            } catch (Exception e) {
            }

            String language = PreferenceUtil.getString("language","");
            Locale locale = MainApplication.getContext().getResources().getConfiguration().locale; //zh-rHK
            String lan = Locale.getDefault().toString();
            if (!TextUtils.isEmpty(language)) {
                if (language.equals(MainApplication.LANG_CODE_EN_US)) {
                    //支付宝单号
                    if(printType==1 || printType == 5){
                        SunmiPrintHelper.getInstance().printText(MainApplication.getPayTypeMap().get("2") + " "+ ToastHelper.toStr(R.string.tx_orderno) + "："+ "\n" + orderModel.getOutTransactionId() + "\n", 24, false, false);
                    }

                    if(printType== 4){
                        SunmiPrintHelper.getInstance().printText(MainApplication.getPayTypeMap().get("2") + " "+ ToastHelper.toStr(R.string.tx_orderno) + "："+ "\n" + orderModel.getTransactionId() + "\n", 24, false, false);
                    }

                } else {
                    //支付宝单号
                    if(printType==1 || printType == 5){
                        SunmiPrintHelper.getInstance().printText(MainApplication.getPayTypeMap().get("2") + ToastHelper.toStr(R.string.tx_orderno) + "："+ "\n" + orderModel.getOutTransactionId() + "\n", 24, false, false);
                    }

                    if(printType== 4){
                        SunmiPrintHelper.getInstance().printText(MainApplication.getPayTypeMap().get("2") + ToastHelper.toStr(R.string.tx_orderno) + "："+ "\n" + orderModel.getTransactionId() + "\n", 24, false, false);
                    }
                }
            }else {
                if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_EN_US) ) {
                    //支付宝单号
                    if(printType==1 || printType == 5){
                        SunmiPrintHelper.getInstance().printText(MainApplication.getPayTypeMap().get("2") + " "+ ToastHelper.toStr(R.string.tx_orderno) + "："+ "\n" + orderModel.getOutTransactionId() + "\n", 24, false, false);
                    }

                    if(printType== 4){
                        SunmiPrintHelper.getInstance().printText(MainApplication.getPayTypeMap().get("2") + " "+ ToastHelper.toStr(R.string.tx_orderno) + "："+ "\n" + orderModel.getTransactionId() + "\n", 24, false, false);
                    }

                } else {
                    //支付宝单号
                    if(printType==1 || printType == 5){
                        SunmiPrintHelper.getInstance().printText(MainApplication.getPayTypeMap().get("2")+ ToastHelper.toStr(R.string.tx_orderno) + "："+ "\n" + orderModel.getOutTransactionId() + "\n", 24, false, false);
                    }

                    if(printType== 4){
                        SunmiPrintHelper.getInstance().printText(MainApplication.getPayTypeMap().get("2") + ToastHelper.toStr(R.string.tx_orderno) + "："+ "\n" + orderModel.getTransactionId() + "\n", 24, false, false);
                    }
                }
            }


        }else if(printType == 2 || printType== 3){
            //解冻订单号
            try {
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.unfreezed_order_id) + "："+ "\n" + orderModel.getOutRequestNo() + "\n", 24, false, false);

            } catch (Exception e) {
            }
            //平台预授权订单号
            try {
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.platform_pre_auth_order_id) + "："+ "\n" + orderModel.getAuthNo() + "\n", 24, false, false);
            } catch (Exception e) {
            }
        }

        if(printType == 5){
            String tradename;
            if (!isAbsoluteNullStr(orderModel.getTradeName())){//先判断大写的Name，再判断小写name
                tradename=orderModel.getTradeName();
            }else {
                tradename=orderModel.getTradename();
            }
            try {
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "："+ "\n" + tradename + "\n", 24, false, false);
            } catch (Exception e) {
            }

        }else{
            try {
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_bill_stream_choice_title) + "："+ "\n" + orderModel.getTradeName() + "\n", 24, false, false);
            } catch (Exception e) {
            }
        }

        if (printType == 3) {
            try {
                SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tv_unfreezen_applicant) + "："+ "\n" + orderModel.getUserName() + "\n", 24, false, false);
            } catch (Exception e) {
            }
        }
        //订单状态
        String operationType = "";
        if(printType == 1 || printType == 4){
            operationType = MainApplication.getPreAuthtradeStateMap().get(orderModel.getTradeState() + "");

        }else if(printType == 2){
            switch (orderModel.getOperationType()){
                case 2:
                    operationType = ToastHelper.toStr(R.string.freezen_success);
                    break;
            }
        }else if(printType == 3){
            operationType = ToastHelper.toStr(R.string.unfreezing);
        }else if (printType == 5){
            operationType = ToastHelper.toStr(R.string.pre_auth_authorized);
        }

        try {
            SunmiPrintHelper.getInstance().printText(ToastHelper.toStr(R.string.tx_bill_stream_statr) + "："+ "\n" + operationType+ "\n", 24, false, false);
        } catch (Exception e) {
        }
        switch (printType){
            case 1:
            case 4:
            case 5:
                if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))){
                    SunmiPrintHelper.getInstance().printText( ToastHelper.toStr(R.string.pre_auth_amount) + "："+  DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan)+ "\n", 36, false, false);
                }else{
                    long preAuthorizationMoney=0;//用来接收printType判断授权金额来自反扫、正扫
                    if (printType==1){
                        preAuthorizationMoney=orderModel.getRestAmount();//反扫
                    }else if (printType==5){
                        preAuthorizationMoney=orderModel.getTotalFee();//正扫
                    }else if(printType==4){
                        preAuthorizationMoney=orderModel.getTotalFreezeAmount();//预授权详情
                    }
                    //预授权金额
                    SunmiPrintHelper.getInstance().setAlign(0);
                    SunmiPrintHelper.getInstance().printText( ToastHelper.toStr(R.string.pre_auth_amount) + "："+ "\n", 24, false, false);

                    SunmiPrintHelper.getInstance().setAlign(2);
                    SunmiPrintHelper.getInstance().printText( MainApplication.feeType + DateUtil.formatMoneyUtils(preAuthorizationMoney)+ "\n", 24, false, false);

                }
                SunmiPrintHelper.getInstance().printText("\n", 28, false, false);
                //二维码
                //打印小票的代码
                if(!StringUtil.isEmptyOrNull(orderModel.getAuthNo())){
                    SunmiPrintHelper.getInstance().setAlign(1);
                    SunmiPrintHelper.getInstance().printQr(orderModel.getAuthNo(), 7, 2);
                    SunmiPrintHelper.getInstance().printText( "\n"+ ToastHelper.toStr(R.string.scan_to_check)+ "\n", 24, false, false);//此二维码用于预授权收款、解冻操作
                }
                break;
            case 2:
                if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))){
                    SunmiPrintHelper.getInstance().setAlign(0);
                    SunmiPrintHelper.getInstance().printText( ToastHelper.toStr(R.string.unfreezing_amount)+":"+ "\n", 24, false, false);

                    SunmiPrintHelper.getInstance().setAlign(2);
                    SunmiPrintHelper.getInstance().printText( DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan), 24, false, false);

                }else{
                    SunmiPrintHelper.getInstance().setAlign(0);
                    SunmiPrintHelper.getInstance().printText( ToastHelper.toStr(R.string.unfreezing_amount)+":"+ "\n", 24, false, false);

                    SunmiPrintHelper.getInstance().setAlign(2);
                    SunmiPrintHelper.getInstance().printText( MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getMoney())+ "\n", 24, false, false);
                }
                //解冻将原路返回支付账户或银行卡，到账时间已第三方为准
                SunmiPrintHelper.getInstance().setAlign(0);
                SunmiPrintHelper.getInstance().printText( ToastHelper.toStr(R.string.unfreeze)+ "\n", 24, false, false);
                break;
            case 3:
                //授权金额
                if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))){
                    SunmiPrintHelper.getInstance().setAlign(0);
                    SunmiPrintHelper.getInstance().printText( ToastHelper.toStr(R.string.pre_auth_amount)+":"+ "\n", 24, false, false);

                    SunmiPrintHelper.getInstance().setAlign(2);
                    SunmiPrintHelper.getInstance().printText( DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan)+ "\n", 24, false, false);
                }else{
                    SunmiPrintHelper.getInstance().setAlign(0);
                    SunmiPrintHelper.getInstance().printText( ToastHelper.toStr(R.string.pre_auth_amount)+":"+ "\n", 24, false, false);

                    SunmiPrintHelper.getInstance().setAlign(2);
                    SunmiPrintHelper.getInstance().printText( MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFreezeAmount())+ "\n", 24, false, false);

                }
                //解冻金额
                if (MainApplication.feeFh.equalsIgnoreCase("¥") && MainApplication.getFeeType().equalsIgnoreCase(ToastHelper.toStr(R.string.pay_yuan))){
                    SunmiPrintHelper.getInstance().setAlign(0);
                    SunmiPrintHelper.getInstance().printText( ToastHelper.toStr(R.string.unfreezing_amount)+":"+ "\n", 24, false, false);

                    SunmiPrintHelper.getInstance().setAlign(2);
                    SunmiPrintHelper.getInstance().printText( DateUtil.formatRMBMoneyUtils(orderModel.getCashFeel()) + ToastHelper.toStr(R.string.pay_yuan)+ "\n", 24, false, false);
                }else{
                    SunmiPrintHelper.getInstance().setAlign(0);
                    SunmiPrintHelper.getInstance().printText( ToastHelper.toStr(R.string.unfreezing_amount)+":"+ "\n", 24, false, false);

                    SunmiPrintHelper.getInstance().setAlign(2);
                    SunmiPrintHelper.getInstance().printText( MainApplication.feeType + DateUtil.formatMoneyUtils(orderModel.getTotalFee())+ "\n", 24, false, false);
                }
                //解冻将原路返回支付账户或银行卡，到账时间已第三方为准
                SunmiPrintHelper.getInstance().setAlign(0);
                SunmiPrintHelper.getInstance().printText( ToastHelper.toStr(R.string.unfreeze)+ "\n", 24, false, false);
                break;
            default:
                break;
        }

        SunmiPrintHelper.getInstance().printText("---------------------------\n", 28, false, false);
        SunmiPrintHelper.getInstance().setAlign(0);
        SunmiPrintHelper.getInstance().printText( ToastHelper.toStr(R.string.tx_blue_print_time) + "：" + DateUtil.formatTime(System.currentTimeMillis()) + "\n", 24, false, false);
        SunmiPrintHelper.getInstance().print3Line();
    }


    public static boolean isAbsoluteNullStr(String str) {
        str = deleteBlank(str);
        if (str == null || str.length() == 0 || "null".equalsIgnoreCase(str)) {
            return true;
        }

        return false;
    }

    /**
     * 去前后空格和去换行符
     *
     * @param str
     * @return
     */
    public static String deleteBlank(String str) {
        if (null != str && 0 < str.trim().length()) {
            char[] array = str.toCharArray();
            int start = 0, end = array.length - 1;
            while (array[start] == ' ') start++;
            while (array[end] == ' ') end--;
            return str.substring(start, end + 1).replaceAll("\n", "");

        } else {
            return "";
        }

    }
}
