/*
 * 文 件 名:  MerchantTempDataModel.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2014-3-18
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 完善资料
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2014-3-18]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class MerchantTempDataModel implements Serializable
{
    
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    private Integer id;
    
    // 注册的手机号码
    private String phone;//电话
    
    private Date addTime;//添加时间
    
    private String userName;//用户名
    
    private String password;//密码
    
    private Integer status; //激活状态：1：已经激活；2：未激活
    
    //spay新版本注册 
    private Integer merchantType; //商户小类型   
    
    private String merchantTypeName; //商户类型名称
    
    private String merchantName; //商户名称
    
    private String regionNo; //区域
    
    private String regionName; //区域名称
    
    private String address; //详细地址  
    
    private String telephone;//联系电话 有格式规定 比如区号-电话号码 0755-26879999
    
    private String areaCode; //所在地区电话区号（4位数字）
    
    private Integer bankType; //商户银行类型
    
    private String bankTypeName; //银行类型
    
    private String branchBankName; //开户银行支行名称
    
    private String bankUserName;//收款姓名 *
    
    private String bank; //商户银行账号
    
    private String identityNo; //身份证号码
    
    private String idCardJustPic;//身份证正面照片名
    
    private String idCardTrunmPic;//身份证反面照片名
    
    private String identityPic; //运营者手持照片（路径）
    
    private String licensePic; // 营业执照 路径
    
    private String idCode; // 机构代码证
    
    private String principalPhone;
    
    private String principal;//负责人
    
    /**
     * @return 返回 principal
     */
    public String getPrincipal()
    {
        return principal;
    }
    
    /**
     * @param 对principal进行赋值
     */
    public void setPrincipal(String principal)
    {
        this.principal = principal;
    }
    
    /**
     * @return 返回 principalPhone
     */
    public String getPrincipalPhone()
    {
        return principalPhone;
    }
    
    /**
     * @param 对principalPhone进行赋值
     */
    public void setPrincipalPhone(String principalPhone)
    {
        this.principalPhone = principalPhone;
    }
    
    /**
     * @return 返回 idCode
     */
    public String getIdCode()
    {
        return idCode;
    }
    
    /**
     * @param 对idCode进行赋值
     */
    public void setIdCode(String idCode)
    {
        this.idCode = idCode;
    }
    
    private String emil;
    
    private String bankNum;
    
    private String province; //省
    
    private String city; //市
    
    /**
     * @return 返回 province
     */
    public String getProvince()
    {
        return province;
    }
    
    /**
     * @param 对province进行赋值
     */
    public void setProvince(String province)
    {
        this.province = province;
    }
    
    /**
     * @return 返回 city
     */
    public String getCity()
    {
        return city;
    }
    
    /**
     * @param 对city进行赋值
     */
    public void setCity(String city)
    {
        this.city = city;
    }
    
    /**
     * @return 返回 bankNum
     */
    public String getBankNum()
    {
        return bankNum;
    }
    
    /**
     * @param 对bankNum进行赋值
     */
    public void setBankNum(String bankNum)
    {
        this.bankNum = bankNum;
    }
    
    /**
     * @return 返回 emil
     */
    public String getEmil()
    {
        return emil;
    }
    
    /**
     * @param 对emil进行赋值
     */
    public void setEmil(String emil)
    {
        this.emil = emil;
    }
    
    /**
     * @return 返回 licensePic
     */
    public String getLicensePic()
    {
        return licensePic;
    }
    
    /**
     * @param 对licensePic进行赋值
     */
    public void setLicensePic(String licensePic)
    {
        this.licensePic = licensePic;
    }
    
    //Bill Xu 2014-01-07 新增字段
    private String taxRegCertPic; //税务登记表扫描件
    
    private String orgaCodeCertPic; // 组织机构代码扫描件
    
    private String mercInfoFormPic; // 商户信息登记表扫描件 //暂无
    
    //临时字段
    private String merchantId;//商户表的商户号
    
    /**
     * @return 返回 phone
     */
    public String getPhone()
    {
        return phone;
    }
    
    /**
     * @param 对phone进行赋值
     */
    public void setPhone(String phone)
    {
        this.phone = phone;
    }
    
    /**
     * @return 返回 addTime
     */
    public Date getAddTime()
    {
        return addTime;
    }
    
    /**
     * @param 对addTime进行赋值
     */
    public void setAddTime(Date addTime)
    {
        this.addTime = addTime;
    }
    
    /**
     * @return 返回 userName
     */
    public String getUserName()
    {
        return userName;
    }
    
    /**
     * @param
     */
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    /**
     * @return 返回 密码
     */
    public String getPassword()
    {
        return password;
    }
    
    /**
     * @param
     */
    public void setPassword(String password)
    {
        this.password = password;
    }
    
    /**
     * @return 返回 status
     */
    public Integer getStatus()
    {
        return status;
    }
    
    /**
     * @param
     */
    public void setStatus(Integer status)
    {
        this.status = status;
    }
    
    /**
     * @return 返回 merchantType
     */
    public Integer getMerchantType()
    {
        return merchantType;
    }
    
    /**
     * @param 对merchantType进行赋值
     */
    public void setMerchantType(Integer merchantType)
    {
        this.merchantType = merchantType;
    }
    
    /**
     * @return 返回 merchantTypeName
     */
    public String getMerchantTypeName()
    {
        return merchantTypeName;
    }
    
    /**
     * @param 对merchantTypeName进行赋值
     */
    public void setMerchantTypeName(String merchantTypeName)
    {
        this.merchantTypeName = merchantTypeName;
    }
    
    /**
     * @return 返回 merchantName
     */
    public String getMerchantName()
    {
        return merchantName;
    }
    
    /**
     * @param 对merchantName进行赋值
     */
    public void setMerchantName(String merchantName)
    {
        this.merchantName = merchantName;
    }
    
    /**
     * @return 返回 regionNo
     */
    public String getRegionNo()
    {
        return regionNo;
    }
    
    /**
     * @param 对regionNo进行赋值
     */
    public void setRegionNo(String regionNo)
    {
        this.regionNo = regionNo;
    }
    
    /**
     * @return 返回 regionName
     */
    public String getRegionName()
    {
        return regionName;
    }
    
    /**
     * @param 对regionName进行赋值
     */
    public void setRegionName(String regionName)
    {
        this.regionName = regionName;
    }
    
    /**
     * @return 返回 address
     */
    public String getAddress()
    {
        return address;
    }
    
    /**
     * @param 对address进行赋值
     */
    public void setAddress(String address)
    {
        this.address = address;
    }
    
    /**
     * @return 返回 telephone
     */
    public String getTelephone()
    {
        return telephone;
    }
    
    /**
     * @param 对telephone进行赋值
     */
    public void setTelephone(String telephone)
    {
        this.telephone = telephone;
    }
    
    /**
     * @return 返回 areaCode
     */
    public String getAreaCode()
    {
        return areaCode;
    }
    
    /**
     * @param 对areaCode进行赋值
     */
    public void setAreaCode(String areaCode)
    {
        this.areaCode = areaCode;
    }
    
    /**
     * @return 返回 bankType
     */
    public Integer getBankType()
    {
        return bankType;
    }
    
    /**
     * @param 对bankType进行赋值
     */
    public void setBankType(Integer bankType)
    {
        this.bankType = bankType;
    }
    
    /**
     * @return 返回 bankTypeName
     */
    public String getBankTypeName()
    {
        return bankTypeName;
    }
    
    /**
     * @param 对bankTypeName进行赋值
     */
    public void setBankTypeName(String bankTypeName)
    {
        this.bankTypeName = bankTypeName;
    }
    
    /**
     * @return 返回 branchBankName
     */
    public String getBranchBankName()
    {
        return branchBankName;
    }
    
    /**
     * @param 对branchBankName进行赋值
     */
    public void setBranchBankName(String branchBankName)
    {
        this.branchBankName = branchBankName;
    }
    
    /**
     * @return 返回 bankUserName
     */
    public String getBankUserName()
    {
        return bankUserName;
    }
    
    /**
     * @param 对bankUserName进行赋值
     */
    public void setBankUserName(String bankUserName)
    {
        this.bankUserName = bankUserName;
    }
    
    /**
     * @return 返回 bank
     */
    public String getBank()
    {
        return bank;
    }
    
    /**
     * @param 对bank进行赋值
     */
    public void setBank(String bank)
    {
        this.bank = bank;
    }
    
    /**
     * @return 返回 identityNo
     */
    public String getIdentityNo()
    {
        return identityNo;
    }
    
    /**
     * @param 对identityNo进行赋值
     */
    public void setIdentityNo(String identityNo)
    {
        this.identityNo = identityNo;
    }
    
    /**
     * @return 返回 idCardJustPic
     */
    public String getIdCardJustPic()
    {
        return idCardJustPic;
    }
    
    /**
     * @param 对idCardJustPic进行赋值
     */
    public void setIdCardJustPic(String idCardJustPic)
    {
        this.idCardJustPic = idCardJustPic;
    }
    
    /**
     * @return 返回 idCardTrunmPic
     */
    public String getIdCardTrunmPic()
    {
        return idCardTrunmPic;
    }
    
    /**
     * @param 对idCardTrunmPic进行赋值
     */
    public void setIdCardTrunmPic(String idCardTrunmPic)
    {
        this.idCardTrunmPic = idCardTrunmPic;
    }
    
    /**
     * @return 返回 identityPic
     */
    public String getIdentityPic()
    {
        return identityPic;
    }
    
    /**
     * @param 对identityPic进行赋值
     */
    public void setIdentityPic(String identityPic)
    {
        this.identityPic = identityPic;
    }
    
    /**
     * @return 返回 taxRegCertPic
     */
    public String getTaxRegCertPic()
    {
        return taxRegCertPic;
    }
    
    /**
     * @param 对taxRegCertPic进行赋值
     */
    public void setTaxRegCertPic(String taxRegCertPic)
    {
        this.taxRegCertPic = taxRegCertPic;
    }
    
    /**
     * @return 返回 orgaCodeCertPic
     */
    public String getOrgaCodeCertPic()
    {
        return orgaCodeCertPic;
    }
    
    /**
     * @param 对orgaCodeCertPic进行赋值
     */
    public void setOrgaCodeCertPic(String orgaCodeCertPic)
    {
        this.orgaCodeCertPic = orgaCodeCertPic;
    }
    
    /**
     * @return 返回 mercInfoFormPic
     */
    public String getMercInfoFormPic()
    {
        return mercInfoFormPic;
    }
    
    /**
     * @param 对mercInfoFormPic进行赋值
     */
    public void setMercInfoFormPic(String mercInfoFormPic)
    {
        this.mercInfoFormPic = mercInfoFormPic;
    }
    
    /**
     * @return 返回 merchantId
     */
    public String getMerchantId()
    {
        return merchantId;
    }
    
    /**
     * @param 对merchantId进行赋值
     */
    public void setMerchantId(String merchantId)
    {
        this.merchantId = merchantId;
    }
    
    @Override
    public String toString()
    {
        return "MerchantTempDataModel [id=" + id + ", phone=" + phone + ", addTime=" + addTime + ", userName="
            + userName + ", password=" + password + ", status=" + status + ", merchantType=" + merchantType
            + ", merchantTypeName=" + merchantTypeName + ", merchantName=" + merchantName + ", regionNo=" + regionNo
            + ", regionName=" + regionName + ", address=" + address + ", telephone=" + telephone + ", areaCode="
            + areaCode + ", bankType=" + bankType + ", bankTypeName=" + bankTypeName + ", branchBankName="
            + branchBankName + ", bankUserName=" + bankUserName + ", bank=" + bank + ", identityNo=" + identityNo
            + ", idCardJustPic=" + idCardJustPic + ", idCardTrunmPic=" + idCardTrunmPic + ", identityPic="
            + identityPic + ", licensePic=" + licensePic + ", taxRegCertPic=" + taxRegCertPic + ", orgaCodeCertPic="
            + orgaCodeCertPic + ", mercInfoFormPic=" + mercInfoFormPic + ", merchantId=" + merchantId + "]";
    }
    
}
