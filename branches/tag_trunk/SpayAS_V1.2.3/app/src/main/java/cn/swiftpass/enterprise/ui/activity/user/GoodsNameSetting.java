package cn.swiftpass.enterprise.ui.activity.user;


import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.GoodsMode;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.HandlerManager;

public class GoodsNameSetting extends TemplateActivity implements OnClickListener {
    private static final String TAG = GoodsNameSetting.class.getSimpleName();

    private EditText goods_name_edit, goods_name_count;

    private Button goods_name_submit_btn;

    GoodsMode goodsMode = null;

    private TextView body_info, good_info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goods_name_setting);

        //        Bundle bundle = this.getIntent().getBundleExtra("bundle");
        //        if (bundle != null)
        //        {
        //            goodsMode = (GoodsMode)bundle.get("body");
        //        }
        initView(goodsMode);
        initListener();
        goods_name_submit_btn.getBackground().setAlpha(102);
        load();

        setButBgAndFont(goods_name_submit_btn);
    }

    private void load() {
        UserManager.queryBody(new UINotifyListener<GoodsMode>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                showLoading(false, R.string.public_loading);

            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
                dismissLoading();
            }

            @Override
            public void onError(Object object) {
                super.onError(object);
                dismissLoading();
                //                if (object != null)
                //                    showToastInfo(object.toString());
            }

            @Override
            public void onSucceed(GoodsMode object) {
                super.onSucceed(object);
                dismissLoading();

                if (object != null) {
                    goods_name_edit.setText(object.getBody());
                    goods_name_count.setText(object.getCount());
                    goods_name_edit.setFocusable(true);
                    goods_name_edit.setFocusableInTouchMode(true);
                    goods_name_edit.requestFocus();
                    goods_name_edit.setSelection(goods_name_edit.getText().length());
                }
            }
        });
    }

    private void initView(GoodsMode goodsMode) {
        this.goods_name_edit = getViewById(R.id.goods_name_edit);
        this.goods_name_submit_btn = getViewById(R.id.goods_name_submit_btn);
        goods_name_count = getViewById(R.id.goods_name_count);
        body_info = getViewById(R.id.body_info);
        good_info = getViewById(R.id.good_info);
        if (null != ApiConstant.bankType && !"".equals(ApiConstant.bankType) && ApiConstant.bankType == 1) {
            good_info.setText(getResources().getString(R.string.goods_name_setting_des_xy));
        }

//        EditTextWatcher editTextWatcher = new EditTextWatcher();
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (goods_name_edit.isFocused() || goods_name_count.isFocused()) {
                    String countStr = goods_name_count.getText().toString();
                    int value = 0;
                    try {
                        value = Integer.valueOf(countStr);
                    } catch (NumberFormatException e) {
                        Log.e(TAG,Log.getStackTraceString(e));
                    }
                    if (goods_name_edit.getText().toString().length() > 0 && goods_name_count.getText().toString().length() > 0 && value > 0) {
                        setButtonBg(goods_name_submit_btn, true, 0);
                    } else {
                        setButtonBg(goods_name_submit_btn, false, 0);
                    }
                }

//                if (goods_name_count.isFocused()) {
//                    String text = s.toString();
//                    int len = s.toString().length();
//                    if (len == 1 && text.equals("0")) {
//                        s.clear();
//                    }
//                }

            }
        };
//        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {
//
//
//
//            @Override
//            public void onExecute(CharSequence s, int start, int before, int count) {
//            }
//
//            @Override
//            public void onAfterTextChanged(Editable s) {
//                if (goods_name_edit.isFocused()||goods_name_count.isFocused()) {
//                    if (goods_name_edit.getText().toString().length() > 0&&goods_name_count.getText().toString().length()>0) {
//                        setButtonBg(goods_name_submit_btn, true, 0);
//                    } else {
//                        setButtonBg(goods_name_submit_btn, false, 0);
//                    }
//                }
//            }
//        });
        goods_name_edit.addTextChangedListener(textWatcher);
        goods_name_count.addTextChangedListener(textWatcher);
    }

    private void initListener() {
        goods_name_submit_btn.setOnClickListener(this);
    }

    @Override
    protected void setupTitleBar() {

        super.setupTitleBar();
        titleBar.setTitle(getStringById(R.string.productName));
        titleBar.setLeftButtonVisible(true);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onRightButLayClick() {
            }

            @Override
            public void onLeftButtonClick() {
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.goods_name_submit_btn://提交
                String content = goods_name_edit.getText().toString().trim();
                String count = goods_name_count.getText().toString().trim();
                if (isAbsoluteNullStr(content)) {
                    toastDialog(GoodsNameSetting.this, R.string.goods_name_not_be_empty, null);
                    goods_name_edit.setFocusable(true);
                    return;
                }

//                String regEx = "[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
//                Pattern pp1 = Pattern.compile(regEx);
//                Matcher m = pp1.matcher(content);
//
//                if (m.find()) {
//                    toastDialog(GoodsNameSetting.this, R.string.not_contain_special, null);
//                    goods_name_edit.setFocusable(true);
//                    return;
//                }

                sumit(content, count);

                break;

            default:
                break;
        }
    }

    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void sumit(final String body, final String count) {
        UserManager.updateOrAddBody(body, count, new UINotifyListener<Boolean>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                showLoading(false, getStringById(R.string.public_submitting));
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
                dismissLoading();
            }

            @Override
            public void onError(Object object) {
                super.onError(object);
                dismissLoading();
                if (object != null) toastDialog(GoodsNameSetting.this, object.toString(), null);

            }

            @Override
            public void onSucceed(Boolean object) {
                super.onSucceed(object);
                dismissLoading();

                if (object) {
                    toastDialog(GoodsNameSetting.this, R.string.submit_success_auditing, new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            HandlerManager.notifyMessage(HandlerManager.VICE_SWITCH, HandlerManager.BODY_SWITCH, body);

                            finish();

                        }
                    });
                }
            }
        });
    }
}
