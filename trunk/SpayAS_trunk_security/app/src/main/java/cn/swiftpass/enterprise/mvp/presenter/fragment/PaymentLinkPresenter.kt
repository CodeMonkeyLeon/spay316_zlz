package cn.swiftpass.enterprise.mvp.presenter.fragment

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.fragment.PaymentLinkContract
import cn.swiftpass.enterprise.ui.paymentlink.model.ImageUrl
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkOrder
import cn.swiftpass.enterprise.utils.JsonUtil
import cn.swiftpass.enterprise.utils.SignUtil
import cn.swiftpass.enterprise.utils.StringUtil
import com.example.common.entity.EmptyData
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/11/16
 *
 * @你应该是一场梦我应该是一阵风
 */
class PaymentLinkPresenter : PaymentLinkContract.Presenter {

    private var mView: PaymentLinkContract.View? = null


    override fun editOrder(o: PaymentLinkOrder?) {
        o?.let { order ->
            mView?.let {
                it.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
            }

            val param: MutableMap<String, Any> = java.util.HashMap()
            val spayRs = System.currentTimeMillis().toString()
            param[ParamsConstants.ACTION_TYPE] = "3"
            param[ParamsConstants.ORDER_NO] = order.orderNo
            param[ParamsConstants.EFFECTIVE_DATE] = order.effectiveDate
            param[ParamsConstants.TOTAL_FEE] = order.totalFee
            if (!TextUtils.isEmpty(order.orderRemark)) {
                param[ParamsConstants.ORDER_REMARK] = order.orderRemark
            }
            if (!TextUtils.isEmpty(order.picUrl)) {
                param[ParamsConstants.PIC_URL] = order.picUrl
            }
            if (!TextUtils.isEmpty(order.custId)) {
                param[ParamsConstants.CUST_ID] = order.custId
            }
            if (!TextUtils.isEmpty(order.custName)) {
                param[ParamsConstants.CUST_NAME] = order.custName
            }
            if (!TextUtils.isEmpty(order.custMobile)) {
                param[ParamsConstants.CUST_MOBILE] = order.custMobile
            }
            if (!TextUtils.isEmpty(order.custEmail)) {
                param[ParamsConstants.CUST_EMAIL] = order.custEmail
            }
            if (!TextUtils.isEmpty(MainApplication.getInstance().getSignKey())) {
                param[ParamsConstants.SIGN] = SignUtil.getInstance().createSign(
                    JsonUtil.jsonToMap(JsonUtil.objectToJson(param)),
                    MainApplication.getInstance().getSignKey()
                )
            }
            //加签名
            if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getNewSignKey())) {
                param[ParamsConstants.SPAY_RS] = spayRs
                param[ParamsConstants.NNS] = SignUtil.getInstance().createSign(
                    JsonUtil.jsonToMap(JsonUtil.objectToJson(param)),
                    MainApplication.getInstance().getNewSignKey()
                )
            }
            if (null != order.orderCostList && order.orderCostList.size > 0) {
                param[ParamsConstants.ORDER_COST_LIST] = order.orderCostList
            }
            if (null != order.orderRelateGoodsList && order.orderRelateGoodsList.size > 0) {
                param[ParamsConstants.ORDER_RELATE_GOODS_LIST] = order.orderRelateGoodsList
            }

            val jsonStr = JsonUtil.mapToJsons(param)
            AppClient.paymentLinkOrder(
                spayRs,
                jsonStr,
                object : CallBackUtil.CallBackCommonResponse<EmptyData>(EmptyData()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        mView?.let { view ->
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            commonResponse?.let { error ->
                                view.editOrderFailed(error.message)
                            }
                        }
                    }

                    override fun onResponse(res: CommonResponse?) {
                        mView?.let { view ->
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            res?.let { response ->
                                view.editOrderSuccess(response.message)
                            }
                        }
                    }
                }
            )
        }


    }

    override fun addNewOrder(o: PaymentLinkOrder?) {

        o?.let { order ->
            mView?.let {
                it.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
            }

            val mapParam: MutableMap<String, Any> = java.util.HashMap()
            val spayRs = System.currentTimeMillis().toString()
            mapParam[ParamsConstants.ACTION_TYPE] = "4"
            mapParam[ParamsConstants.EFFECTIVE_DATE] = order.effectiveDate
            mapParam[ParamsConstants.TOTAL_FEE] = order.totalFee
            if (!TextUtils.isEmpty(order.orderRemark)) {
                mapParam[ParamsConstants.ORDER_REMARK] = order.orderRemark
            }
            if (!TextUtils.isEmpty(order.custId)) {
                mapParam[ParamsConstants.CUST_ID] = order.custId
            }
            if (!TextUtils.isEmpty(order.picUrl)) {
                mapParam[ParamsConstants.PIC_URL] = order.picUrl
            }

            if (!TextUtils.isEmpty(MainApplication.getInstance().getSignKey())) {
                mapParam[ParamsConstants.SIGN] = SignUtil.getInstance().createSign(
                    JsonUtil.jsonToMap(JsonUtil.objectToJson(mapParam)),
                    MainApplication.getInstance().getSignKey()
                )
            }
            //加签名
            if (!StringUtil.isEmptyOrNull(MainApplication.getInstance().getNewSignKey())) {
                mapParam[ParamsConstants.SPAY_RS] = spayRs
                mapParam[ParamsConstants.NNS] = SignUtil.getInstance().createSign(
                    JsonUtil.jsonToMap(JsonUtil.objectToJson(mapParam)),
                    MainApplication.getInstance().getNewSignKey()
                )
            }
            if (null != order.orderCostList && order.orderCostList.size > 0) {
                mapParam[ParamsConstants.ORDER_COST_LIST] = order.orderCostList
            }
            if (null != order.orderRelateGoodsList && order.orderRelateGoodsList.size > 0) {
                mapParam[ParamsConstants.ORDER_RELATE_GOODS_LIST] = order.orderRelateGoodsList
            }

            val jsonStr = JsonUtil.mapToJsons(mapParam)
            AppClient.paymentLinkOrder(
                spayRs,
                jsonStr,
                object : CallBackUtil.CallBackCommonResponse<PaymentLinkOrder>(PaymentLinkOrder()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {

                        mView?.let { view ->
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            commonResponse?.let { error ->
                                view.addNewOrderFailed(error.message)
                            }
                        }
                    }

                    override fun onResponse(res: CommonResponse?) {
                        mView?.let { view ->
                            view.dismissLoading(ParamsConstants.COMMON_LOADING)
                            res?.let { response ->

                                //如果orderRelateGoodsList值为空，替换""为[]，因为Gson无法解析""为数组
                                response.message = response.message.replace(
                                    "\"orderRelateGoodsList\":\"\"",
                                    "\"orderRelateGoodsList\":[]"
                                )
                                response.message = response.message.replace(
                                    "\"orderCostList\":\"\"",
                                    "\"orderCostList\":[]"
                                )
                                val order = JsonUtil.jsonToBean(
                                    response.message,
                                    PaymentLinkOrder::class.java
                                ) as PaymentLinkOrder
                                view.addNewOrderSuccess(order)

                            }
                        }
                    }
                }
            )
        }

    }

    override fun uploadImg(
        imgBase64: String?,
        imgType: String?
    ) {

        mView?.let {
            it.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
        }


        AppClient.uploadImg(
            imgType,
            imgBase64,
            System.currentTimeMillis().toString(),
            object : CallBackUtil.CallBackCommonResponse<ImageUrl>(ImageUrl()) {


                override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                    mView?.let { view ->
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let { error ->
                            view.uploadImgFailed(error.message)
                        }
                    }
                }


                override fun onResponse(res: CommonResponse?) {
                    mView?.let { view ->
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        res?.let { response ->
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                ImageUrl::class.java
                            ) as ImageUrl
                            view.uploadImgSuccess(order)
                        }
                    }
                }
            }
        )


    }


    override fun attachView(view: PaymentLinkContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}