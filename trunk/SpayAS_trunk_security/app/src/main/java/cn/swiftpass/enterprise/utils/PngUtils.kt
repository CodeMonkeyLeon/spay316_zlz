package cn.swiftpass.enterprise.utils

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.Drawable
import android.widget.ImageView
import cn.swiftpass.enterprise.intl.R

object PngUtils {

    /**
     * 方案1: 每次均调用
     *
     * @param pngId 需要修改的png图片id
     */
    fun setPng1(
        context: Context?,
        imageView: ImageView?,
        pngId: Int = 0
    ) {
        context?.let { c ->
            imageView?.let { i ->
                val originalDrawable = c.resources.getDrawable(pngId) // 加载原始图片
                val colorFilter =
                    PorterDuffColorFilter(
                        c.resources.getColor(R.color.color_app_theme),
                        PorterDuff.Mode.SRC_ATOP
                    ) // 创建颜色过滤器

                val modifiedDrawable = originalDrawable.mutate() // 创建一个可变的 Drawable
                modifiedDrawable.colorFilter = colorFilter // 应用颜色过滤器

                // 将修改后的图片设置为 ImageView 的 src 属性
                i.setImageResource(0) // 首先清除 ImageView 的现有图片
                i.setImageDrawable(modifiedDrawable)
            }
        }
    }


    fun getPngDrawable1(
        context: Context?,
        pngId: Int = 0
    ): Drawable? {
        context?.let { c ->
            val originalDrawable = c.resources.getDrawable(pngId) // 加载原始图片
            val colorFilter =
                PorterDuffColorFilter(
                    c.resources.getColor(R.color.color_app_theme),
                    PorterDuff.Mode.SRC_ATOP
                ) // 创建颜色过滤器

            val modifiedDrawable = originalDrawable.mutate() // 创建一个可变的 Drawable
            modifiedDrawable.colorFilter = colorFilter // 应用颜色过滤器
            return modifiedDrawable
        }
        return null
    }


    fun getPngDrawable1(
        context: Context?,
        originalDrawable: Drawable? = null
    ): Drawable? {
        context?.let { c ->
            originalDrawable?.let { d ->
                val colorFilter =
                    PorterDuffColorFilter(
                        c.resources.getColor(R.color.color_app_theme),
                        PorterDuff.Mode.SRC_ATOP
                    ) // 创建颜色过滤器

                val modifiedDrawable = d.mutate() // 创建一个可变的 Drawable
                modifiedDrawable.colorFilter = colorFilter // 应用颜色过滤器
                return modifiedDrawable
            }
        }
        return null
    }
}