package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.UserModelList
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.UserListContract
import cn.swiftpass.enterprise.utils.JsonUtil
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/11/22
 *
 * @阳光在天上一闪
 * @又被乌云埋掩
 * @暴雨冲洗着
 * @我灵魂的底片
 */
class UserListPresenter : UserListContract.Presenter {

    private var mView: UserListContract.View? = null


    override fun queryCashier(
        page: Int,
        pageSize: Int,
        input: String?,
        isLoadCashierData: Boolean,
        isLoadMore: Boolean,
        what: Int
    ) {

        mView?.let { view ->

            if (isLoadCashierData) {
                if (isLoadMore) {
                    view.showLoading(
                        R.string.public_data_loading,
                        ParamsConstants.COMMON_LOADING
                    )
                }
            } else {
                view.showLoading(
                    R.string.public_data_loading,
                    ParamsConstants.COMMON_LOADING
                )
            }

            AppClient.queryCashier(
                MainApplication.getInstance().getMchId(),
                page.toString(),
                input,
                pageSize.toString(),
                MainApplication.getInstance().getSignKey(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<UserModelList>(UserModelList()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.queryCashierFailed(it.message, isLoadCashierData)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val dynModels = JsonUtil.jsonToBean(
                                response.message,
                                UserModelList::class.java
                            ) as UserModelList
                            view.queryCashierSuccess(dynModels, input, isLoadCashierData, what)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: UserListContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}