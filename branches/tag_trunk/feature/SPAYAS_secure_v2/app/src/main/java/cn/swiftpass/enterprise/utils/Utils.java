package cn.swiftpass.enterprise.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-8-22
 * Time: 下午8:25
 * To change this template use File | Settings | File Templates.
 */
public class Utils
{
    private static final String TAG = Utils.class.getSimpleName();
    /** 格式化为yyyy年MM月 */
    public static String formatYM(long timestamp)
    {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddHHmmss");
        return sdf1.format(new Date(timestamp));
    }
    
    public static class Integer
    {
        /**
         * 尝试从字符串中解析一个int数值
         *
         * @param val
         *            字符串 
         * @param defaultValue
         *            默认值
         * @return 如果val是有效的数值，则返回该数 否则，返回默认值
         */
        public static int tryParse(String val, int defaultValue)
        {
            if (TextUtils.isEmpty(val))
            {
                return defaultValue;
            }
            try
            {
                return java.lang.Integer.parseInt(val);
            }
            catch (Exception e)
            {
                return defaultValue;
            }
        }
        
        /**
         * 判断一个字符串是否为整数
         * @param val
         *            输入字符串
         * @return true:是整数，false:不是整数
         */
        public static boolean isInteger(String val)
        {
            if (TextUtils.isEmpty(val))
            {
                return false;
            }
            try
            {
                java.lang.Integer.parseInt(val);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
    
    public static class Long
    {
        public static long tryParse(String val, long defaultValue)
        {
            if (TextUtils.isEmpty(val))
            {
                return defaultValue;
            }
            try
            {
                return java.lang.Long.parseLong(val);
            }
            catch (Exception e)
            {
                return defaultValue;
            }
        }
        
        public static boolean isLong(String val)
        {
            if (TextUtils.isEmpty(val))
            {
                return false;
            }
            try
            {
                java.lang.Long.parseLong(val);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
    
    public static class Double
    {
        /**
         * 尝试从字符串中解析一个double数值
         *
         * @param val
         *            字符串
         * @param defaultValue
         *            默认值
         * @return 如果val是有效的数值，则返回该数 否则，返回默认值
         */
        public static double tryParse(String val, double defaultValue)
        {
            if (TextUtils.isEmpty(val))
            {
                return defaultValue;
            }
            try
            {
                return java.lang.Double.parseDouble(val);
            }
            catch (Exception e)
            {
                return defaultValue;
            }
        }
        
        /**
         * 判断一个字符串是否为整数
         * @param val
         *            输入字符串
         * @return true:是double，false:不是double
         */
        public static boolean isDouble(String val)
        {
            if (TextUtils.isEmpty(val))
            {
                return false;
            }
            try
            {
                java.lang.Double.parseDouble(val);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
    
    public static class Strings
    {
        /**
         * 返回固定长度的文本
         *
         * @param str
         * @param len
         * @return
         */
        public static String getTextWithFixLength(String str, int len, String placeHolder)
        {
            return subStrLenth(str, 0, len, placeHolder);
        }
        
        private static boolean isChinese(char c)
        {
            return (int)c >= 0x4E00 && (int)c <= 0x9FA5;
        }
        
        private static int getStrByteLength(String str)
        {
            try
            {
                return str.getBytes().length;
            }
            catch (Exception e)
            {
                return 0;
            }
        }
        
        private static String subStrLenth(String str, int startIndex, int length, String placeHolder)
        {
            if (str == null)
            {
                return "";
            }
            int strlen = getStrByteLength(str);
            if (startIndex + 1 > strlen)
            {
                return "";
            }
            int j = 0;// 记录遍历的字节数
            int L = 0;// 记录每次截取开始，遍历到开始的字节位，才开始记字节数
            int strW = 0;// 字符宽度
            boolean b = false;// 当每次截取时，遍历到开始截取的位置才为true
            String restr = "";
            boolean isFinished = false;
            for (int i = 0; i < str.length(); i++)
            {
                char C = str.charAt(i);
                if (isChinese(C))
                {
                    strW = 2;
                }
                else
                {
                    strW = 1;
                }
                if ((L == length - 1) && (L + strW > length))
                {
                    b = false;
                    break;
                }
                if (!isFinished && j >= startIndex)
                {
                    restr = new StringBuffer(restr).append(C).toString();
//                    restr += C;
                    b = true;
                }
                j += strW;
                if (b)
                {
                    L += strW;
                    if (((L + 1) > length))
                    {
                        b = false;
                        isFinished = true;
                    }
                }
            }
            if (j > length)
            {
//                restr += placeHolder;
                restr = new StringBuffer(restr).append(placeHolder).toString();
            }
            return restr;
        }
        
        /**
         * 把json里的null转换成""
         *
         * @param str
         * @return
         */
        public static String verifyJsonString(String str)
        {
            if (str != null)
            {
                if (str.equals("null"))
                {
                    str = "";
                }
            }
            return str;
        }
    }
    
    public static class Time
    {
        /**
         * 返回一个相对时间的描述字符串 例如：2分钟前，1天前等等
         *
         * @param time
         * @return
         */
        public static CharSequence getDescription(long time)
        {
            return DateUtils.getRelativeTimeSpanString(time);
        }
    }
    
    /**
     * MD5相关
     *
     *
     */
    public static class MD5
    {
        /**
         * 获取MD5字符值
         *
         * @param bytes
         * @return
         */
       /* public static String toMd5(byte[] bytes)
        {
            try
            {
                MessageDigest algorithm = MessageDigest.getInstance("MD5");
                algorithm.reset();
                algorithm.update(bytes);
                return toHexString(algorithm.digest(), "");
            }
            catch (NoSuchAlgorithmException e)
            {
                throw new RuntimeException(e);
            }
        }*/
        
        /**
         * 将MD5格式化为16进制
         *
         * @param bytes
         * @param separator
         * @return
         */
        public static String toHexString(byte[] bytes, String separator)
        {
            StringBuilder hexString = new StringBuilder();
            for (byte b : bytes)
            {
                String value = java.lang.Integer.toHexString(0xFF & b);
                if (value.length() == 1)
                {
                    value = "0" + value;
                }
                hexString.append(value).append(separator);
            }
            return hexString.toString();
        }
        
        /*public static String getApiCode(Context context)
        {
            android.content.pm.Signature[] sigs;
            try
            {
                sigs = context.getPackageManager().getPackageInfo("com.baicai.job", 64).signatures;
                // GlobalConstant.apiCode =
                // Util.MD5.toMd5(sigs[0].toByteArray());
                return Utils.MD5.toMd5(sigs[0].toCharsString().getBytes());
            }
            catch (PackageManager.NameNotFoundException e)
            {
                Log.e(TAG,Log.getStackTraceString(e));
            }
            return null;
        }*/
    }
    
    public static class System
    {
        public static int getAppVersionCode(Context context)
        {
            try
            {
                PackageManager pm = context.getPackageManager();
                PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
                return pi.versionCode;
            }
            catch (Exception e)
            {
                Log.e(TAG,Log.getStackTraceString(e));
            }
            return 1;
        }
        
        public static boolean isIntentAvailable(Context context, String action)
        {
            final PackageManager packageManager = context.getPackageManager();
            final Intent intent = new Intent(action);
            List list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            return list.size() > 0;
        }

        
        /**
         * 获取联网方式
         *
         * @param context
         * @return
         */
        public static String getNetType(Context context)
        {
            ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = cm.getActiveNetworkInfo();
            try
            {
                String typeName = info.getTypeName(); // cmwap/cmnet/wifi/uniwap/uninet
                return typeName;
            }
            catch (Exception e)
            {
                return "";
            }
        }
        
        /*public static String exec(String[] args)
        {
            String result = "";
            ProcessBuilder processBuilder = new ProcessBuilder(args);
            Process process = null;
            InputStream errIs = null;
            InputStream inIs = null;
            try
            {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                int read = -1;
                process = processBuilder.start();
                errIs = process.getErrorStream();
                while ((read = errIs.read()) != -1)
                {
                    baos.write(read);
                }
                baos.write('\n');
                inIs = process.getInputStream();
                while ((read = inIs.read()) != -1)
                {
                    baos.write(read);
                }
                byte[] data = baos.toByteArray();
                result = new String(data);
            }
            catch (IOException e)
            {
                Log.e(TAG,Log.getStackTraceString(e));
            }
            catch (Exception e)
            {
                Log.e(TAG,Log.getStackTraceString(e));
            }
            finally
            {
                try
                {
                    if (errIs != null)
                    {
                        errIs.close();
                    }
                    if (inIs != null)
                    {
                        inIs.close();
                    }
                }
                catch (IOException e)
                {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
                if (process != null)
                {
                    process.destroy();
                }
            }
            return result;
        }*/
    }
    
    /** 电话号码 */
    public static boolean isMobileNO(String mobiles)
    {
        Pattern p = Pattern.compile("^1\\d{10}$");//"^1\\d{10}$"
        Matcher m = p.matcher(mobiles.trim());
        return m.matches();
    }
    
    /**验证QQ*/
    public static boolean isQQNumber(String qq)
    {
        String s = "^[1-9]{1}[0-9]{4,12}$";
        Pattern p = Pattern.compile(s);//"^1\\d{10}$"
        Matcher m = p.matcher(qq.trim());
        return m.matches();
    }

    /** 格式化文件的大小为MB，KB，B*/
    public static String formatByte(int bytes)
    {
        DecimalFormat floatFormater = new DecimalFormat("0.00");
        float r = (bytes + 0.0f) / (1 << 20);
        if (r > 1.0f)
        {
            // MB
            return floatFormater.format(r) + "MB";
        }
        else
        {
            r = (bytes + 0.0f) / (1 << 10);
            if (r > 1.0f)
            {
                // KB
                return floatFormater.format(r) + "KB";
            }
            else
            {
                // B
                return bytes + "B";
            }
        }
    }
    
    /**获取系统字体大小*/
    public static int adjustFontSize(int screenWidth, int screenHeight)
    {
        screenWidth = screenWidth > screenHeight ? screenWidth : screenHeight;
        /**
         * 1. 在视图的 onsizechanged里获取视图宽度，一般情况下默认宽度是320，所以计算一个缩放比率
         rate = (float) w/320   w是实际宽度
         2.然后在设置字体尺寸时 paint.setTextSize((int)(8*rate));   8是在分辨率宽为320 下需要设置的字体大小
         实际字体大小 = 默认字体大小 x  rate
         */
        int rate = (int)(5 * (float)screenWidth / 320); //我自己测试这个倍数比较适合，当然你可以测试后再修改
        return rate < 15 ? 15 : rate; //字体太小也不好看的
    }
    
    public static boolean isEmpty(String str)
    {
        return str == null || str.length() == 0;
    }
    
    private static final int PAD_LIMIT = 8192;
    
    public static String leftPad(String str, int size, String padStr)
    {
        if (str == null)
        {
            return null;
        }
        if (isEmpty(padStr))
        {
            padStr = " ";
        }
        int padLen = padStr.length();
        int strLen = str.length();
        int pads = size - strLen;
        if (pads <= 0)
        {
            return str; // returns original String when possible
        }
        if (padLen == 1 && pads <= PAD_LIMIT)
        {
            return leftPad(str, size, padStr.charAt(0));
        }
        
        if (pads == padLen)
        {
            return padStr.concat(str);
        }
        else if (pads < padLen)
        {
            return padStr.substring(0, pads).concat(str);
        }
        else
        {
            char[] padding = new char[pads];
            char[] padChars = padStr.toCharArray();
            for (int i = 0; i < pads; i++)
            {
                padding[i] = padChars[i % padLen];
            }
            return new String(padding).concat(str);
        }
    }
    
    private static String padding(int repeat, char padChar)
        throws IndexOutOfBoundsException
    {
        if (repeat < 0)
        {
            throw new IndexOutOfBoundsException("Cannot pad a negative amount: " + repeat);
        }
        final char[] buf = new char[repeat];
        for (int i = 0; i < buf.length; i++)
        {
            buf[i] = padChar;
        }
        return new String(buf);
    }
    
    public static String leftPad(String str, int size, char padChar)
    {
        if (str == null)
        {
            return null;
        }
        int pads = size - str.length();
        if (pads <= 0)
        {
            return str; // returns original String when possible
        }
        if (pads > PAD_LIMIT)
        {
            return leftPad(str, size, String.valueOf(padChar));
        }
        return padding(pads, padChar).concat(str);
    }
    
    /**
     * 相减
     * @param one
     * @param two
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static String subtract(String one, String two)
    {
        BigDecimal bignum1 = new BigDecimal(one);
        BigDecimal bignum2 = new BigDecimal(two);
        
        return bignum1.subtract(bignum2).toString();
    }
    
    public static boolean compareTo(String one, String two)
    {
        BigDecimal bignum1 = new BigDecimal(one);
        BigDecimal bignum2 = new BigDecimal(two);
        int a = bignum1.compareTo(bignum2);
        if (a == -1)
        {
            return false;
        }
        else if (a == 1)
        {
            return true;
        }
        
        return false;
    }
    
    /**
     * 相加
     * @param one
     * @param two
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static String subtractSum(String one, String two)
    {
        try
        {
            BigDecimal bignum1 = new BigDecimal(one);
            BigDecimal bignum2 = new BigDecimal(two);
            
            return bignum1.add(bignum2).toString();
        }
        catch (Exception e)
        {
            // TODO: handle exception
            return "";
        }
    }
}
