package cn.swiftpass.enterprise.config;

/**
 * 创建人：caoxiaoya
 * 时间：2019/3/18
 * 描述：仅配置全局信息
 * 备注：所有名字全部大写，值为小写
 */
public interface AppConfig {

    String BASE_URL = "https://app.wepayez.com/";
    /**
     * 请求超时时间
     */
    int TIMEOUT = 10;
    /**
     * 下载包名
     */
    String APK_NAME = "spay";
    /**
     * 来自bankcode，临时用intl_and
     */
    String BANKCODE = "intl_and";


}
