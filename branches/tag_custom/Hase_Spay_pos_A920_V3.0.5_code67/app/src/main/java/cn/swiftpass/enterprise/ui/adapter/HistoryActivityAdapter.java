/*
 * 文 件 名:  CashierDetailsAdapter.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  yan_shuo
 * 修改时间:  2015-4-29
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.AwardModel;
import cn.swiftpass.enterprise.utils.DateUtil;

/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  Administrator
 * @version  [版本号, 2015-4-29]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class HistoryActivityAdapter extends BaseAdapter
{
    
    private List<AwardModel> list;
    
    private Context context;
    
    private ViewHolder holder;
    
    public HistoryActivityAdapter(Context context, List<AwardModel> list)
    {
        this.context = context;
        this.list = list;
    }
    
    @Override
    public int getCount()
    {
        return list.size();
    }
    
    @Override
    public Object getItem(int position)
    {
        return list.get(position);
    }
    
    @Override
    public long getItemId(int position)
    {
        return position;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            
            convertView = View.inflate(context, R.layout.history_listview_item, null);
            holder = new ViewHolder();
            holder.cashire_image = (ImageView)convertView.findViewById(R.id.cashire_image);
            holder.ac_title = (TextView)convertView.findViewById(R.id.ac_title);
            holder.tx_num = (TextView)convertView.findViewById(R.id.tx_num);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder)convertView.getTag();
        }
        AwardModel val = list.get(position);
        holder.ac_title.setText(val.getActiveName());
        holder.tx_num.setText(R.string.tv_history_ranking_start + val.getActiveOrder()
            + R.string.tv_history_ranking_moeny + DateUtil.formatMoneyUtils(val.getMoney()));
        if (val.isHadWin())
        { //得奖
            holder.cashire_image.setImageResource(R.drawable.icon_reward_logo);
        }
        return convertView;
    }
    
    private class ViewHolder
    {
        private TextView ac_title, tx_num;
        
        @SuppressWarnings("unused")
        private ImageView cashire_image;
        
    }
    
}
