package cn.swiftpass.enterprise.ui.activity.user;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.refund.RefundManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.adapter.RefundHistoryAdapter;
import cn.swiftpass.enterprise.ui.adapter.RefundViewPagerAdapter;
import cn.swiftpass.enterprise.ui.widget.PullDownListView;
import cn.swiftpass.enterprise.ui.widget.TitleBar;

public class RefundHistoryActivity extends TemplateActivity implements OnClickListener, OnPageChangeListener,
    OnItemClickListener, PullDownListView.OnRefreshListioner
{
    private LinearLayout refund_history_refunding;
    
    private LinearLayout refund_history_refund_complete;
    
    private LinearLayout refund_history_refund_failure;
    
    private TextView refund_history_refunding_tv;
    
    private TextView refund_history_refund_complete_tv;
    
    private TextView refund_history_refund_failure_tv;
    
    private ViewPager refund_viewpager;
    
    private RefundViewPagerAdapter viewPagerAdapter;
    
    private List<View> pageList;
    
    private List<Order> listAuditing;//退款审核中 数据
    
    private List<Order> listFulfil;//退款已受理 数据
    
    private List<Order> listFailure;//退款失败 数据
    
    private RefundHistoryAdapter adapterAuditing;//退款审核中  适配器
    
    private RefundHistoryAdapter adapterFulfil;//退款已受理 适配器
    
    private RefundHistoryAdapter adapterFailure;//退款失败 适配器
    
    private ListView listViewAuditing;
    
    private ListView listViewFulfil;
    
    private ListView listViewFailure;
    
    private LinearLayout ll_Auditing, ll_Fulfil, ll_Failure;
    
    public static final String FULFIL = "1"; //退款已受理
    
    public static final String FAILURE = "2";//退款失败
    
    public static final String AUDITING = "0";//退款中
    
    private PullDownListView mPullDownViewAuditing;//退款审核中  下拉
    
    private PullDownListView mPullDownViewFulfil;//退款已受理 下拉
    
    private PullDownListView mPullDownViewFailure;//退款失败 下拉
    
    private int pageAuditing = 0;//退款审核中 当前页
    
    private int pageFulfil = 0;//退款已受理 当前页
    
    private int pageFailure = 0;//退款失败 当前页
    
    private String currentViewPage = AUDITING;//当前在哪个页面
    
    private Handler mHandler = new Handler();
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refund_history);
        
        initObject();
        initView();
        initData();
        initListener();
    }
    
    private void initObject()
    {
        listAuditing = new ArrayList<Order>();
        listFulfil = new ArrayList<Order>();
        listFailure = new ArrayList<Order>();
        
        adapterAuditing = new RefundHistoryAdapter(this, listAuditing, RefundHistoryAdapter.REFUND_HISTORY_RECORD);
        adapterFulfil = new RefundHistoryAdapter(this, listFulfil, RefundHistoryAdapter.REFUND_HISTORY_RECORD);
        adapterFailure = new RefundHistoryAdapter(this, listFailure, RefundHistoryAdapter.REFUND_HISTORY_RECORD);
        
    }
    
    private void initData()
    {
        pageList = new ArrayList<View>();
        View v1 = View.inflate(this, R.layout.view_pager_refund, null);
        pageList.add(v1);
        View v2 = View.inflate(this, R.layout.view_pager_refund, null);
        pageList.add(v2);
        View v3 = View.inflate(this, R.layout.view_pager_refund, null);
        pageList.add(v3);
        
        mPullDownViewAuditing = (PullDownListView)v1.findViewById(R.id.refund_pulldown);
        mPullDownViewFulfil = (PullDownListView)v2.findViewById(R.id.refund_pulldown);
        mPullDownViewFailure = (PullDownListView)v3.findViewById(R.id.refund_pulldown);
        
        mPullDownViewAuditing.setAutoLoadMore(true);
        mPullDownViewFulfil.setAutoLoadMore(true);
        mPullDownViewFailure.setAutoLoadMore(true);
        
        //listViewAuditing = (ListView)v1.findViewById(R.id.refund_history_list);
        //listViewFulfil = (ListView)v2.findViewById(R.id.refund_history_list);
        //listViewFailure = (ListView)v3.findViewById(R.id.refund_history_list);
        
        listViewAuditing = mPullDownViewAuditing.mListView;
        listViewFulfil = mPullDownViewFulfil.mListView;
        listViewFailure = mPullDownViewFailure.mListView;
        
        mPullDownViewAuditing.setRefreshListioner(this);
        mPullDownViewFulfil.setRefreshListioner(this);
        mPullDownViewFailure.setRefreshListioner(this);
        
        ll_Auditing = (LinearLayout)v1.findViewById(R.id.ll_default);
        ll_Fulfil = (LinearLayout)v2.findViewById(R.id.ll_default);
        ll_Failure = (LinearLayout)v3.findViewById(R.id.ll_default);
        
        listViewAuditing.setAdapter(adapterAuditing);
        listViewFulfil.setAdapter(adapterFulfil);
        listViewFailure.setAdapter(adapterFailure);
        
        viewPagerAdapter = new RefundViewPagerAdapter(pageList);
        refund_viewpager.setAdapter(viewPagerAdapter);
        refund_viewpager.setOnPageChangeListener(this);
        refund_viewpager.setCurrentItem(0);
        
        searchRefundRecord(AUDITING, 0, false, true);
    }
    
    private void initListener()
    {
        refund_history_refunding.setOnClickListener(this);
        refund_history_refund_complete.setOnClickListener(this);
        refund_history_refund_failure.setOnClickListener(this);
        
        listViewAuditing.setOnItemClickListener(this);
        listViewFulfil.setOnItemClickListener(this);
        listViewFailure.setOnItemClickListener(this);
    }
    
    private void initView()
    {
        refund_history_refunding = getViewById(R.id.refund_history_refunding);
        refund_history_refund_complete = getViewById(R.id.refund_history_refund_complete);
        refund_history_refund_failure = getViewById(R.id.refund_history_refund_failure);
        refund_viewpager = getViewById(R.id.refund_viewpager);
        
        refund_history_refunding_tv = getViewById(R.id.refund_history_refunding_tv);
        refund_history_refund_complete_tv = getViewById(R.id.refund_history_refund_complete_tv);
        refund_history_refund_failure_tv = getViewById(R.id.refund_history_refund_failure_tv);
        
    }
    
    @Override
    protected void setupTitleBar()
    {
        
        super.setupTitleBar();
        titleBar.setTitle(getStringById(R.string.refund_record));
        titleBar.setLeftButtonVisible(true);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
            }
            
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
        });
    }
    
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.refund_history_refunding:// 退款中
                refund_viewpager.setCurrentItem(0);
                changeTextColor(0);
                break;
            case R.id.refund_history_refund_complete:// 退款已受理
                refund_viewpager.setCurrentItem(1);
                changeTextColor(1);
                break;
            case R.id.refund_history_refund_failure:// 退款失败
                refund_viewpager.setCurrentItem(2);
                changeTextColor(2);
                break;
            
            default:
                break;
        }
    }
    
    private void changeTextColor(int tag)
    {
        switch (tag)
        {
            case 0:
                refund_history_refunding_tv.setTextColor(Color.parseColor("#ff7f00"));
                refund_history_refund_complete_tv.setTextColor(Color.parseColor("#888888"));
                refund_history_refund_failure_tv.setTextColor(Color.parseColor("#888888"));
                break;
            case 1:
                refund_history_refunding_tv.setTextColor(Color.parseColor("#888888"));
                refund_history_refund_complete_tv.setTextColor(Color.parseColor("#ff7f00"));
                refund_history_refund_failure_tv.setTextColor(Color.parseColor("#888888"));
                break;
            case 2:
                refund_history_refunding_tv.setTextColor(Color.parseColor("#888888"));
                refund_history_refund_complete_tv.setTextColor(Color.parseColor("#888888"));
                refund_history_refund_failure_tv.setTextColor(Color.parseColor("#ff7f00"));
                break;
            
            default:
                break;
        }
        
    }
    
    @Override
    public void onPageScrollStateChanged(int arg0)
    {
        
    }
    
    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2)
    {
    }
    
    /**
     * 查询退款记录
     * <功能详细描述>
     * @param refundState 请求类型
     * @param page 分页
     * @param isLoadMore 区别 下拉刷新和加载更多 false为下拉刷新 true为加载更多
     * @param showDialog 是否显示右上角的dialog
     * @see [类、类#方法、类#成员]
     */
    private void searchRefundRecord(final String refundState, int page, final boolean isLoadMore,
        final boolean showDialog)
    {
        OrderManager.getInstance().queryUserRefundOrder("",
            "",
            MainApplication.getMchId(),
            refundState,
            page,
            new UINotifyListener<List<Order>>()
            {
                
                @Override
                public void onError(Object object)
                {
                    super.onError(object);
                    titleBar.setRightLodingVisible(false, false);
                    if (object != null)
                    {
                        showToastInfo(object.toString());
                    }
                }
                
                @Override
                public void onPreExecute()
                {
                    super.onPreExecute();
                    if (showDialog)
                    {
                        titleBar.setRightLodingVisible(true);
                    }
                }
                
                @Override
                public void onSucceed(List<Order> result)
                {
                    super.onSucceed(result);
                    titleBar.setRightLodingVisible(false, false);
                    if (result == null)
                    {
                        return;
                    }
                    if (refundState.equals(FULFIL))//已受理
                    {
                        if (listFulfil != null)
                        {
                            mySetListData(mPullDownViewFulfil,
                                listFulfil,
                                adapterFulfil,
                                result,
                                listViewFulfil,
                                ll_Fulfil,
                                isLoadMore);
                            if (isLoadMore)
                            {
                                pageFulfil += 1;
                            }
                        }
                    }
                    else if (refundState.equals(FAILURE))//退款失败
                    {
                        if (listFailure != null)
                        {
                            mySetListData(mPullDownViewFailure,
                                listFailure,
                                adapterFailure,
                                result,
                                listViewFailure,
                                ll_Failure,
                                isLoadMore);
                            if (isLoadMore)
                            {
                                pageFailure += 1;
                            }
                        }
                    }
                    else if (refundState.equals(AUDITING))//退款中
                    {
                        if (listAuditing != null)
                        {
                            
                            mySetListData(mPullDownViewAuditing,
                                listAuditing,
                                adapterAuditing,
                                result,
                                listViewAuditing,
                                ll_Auditing,
                                isLoadMore);
                            if (isLoadMore)
                            {
                                pageAuditing += 1;
                            }
                        }
                    }
                }
                
            });
    }
    
    private void mySetListData(final PullDownListView pull, List<Order> list, RefundHistoryAdapter adapter,
        List<Order> result, ListView listView, LinearLayout ll, boolean isLoadMore)
    {
        
        if (!isLoadMore)
        {
            pull.onfinish(getStringById(R.string.refresh_successfully));// 刷新完成
            
            mHandler.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    // 这里表示刷新处理完成后把上面的加载刷新界面隐藏
                    pull.onRefreshComplete();
                }
            }, 800);
            
            list.clear();
        }
        else
        {
            mHandler.postDelayed(new Runnable()
            {
                
                @Override
                public void run()
                {
                    pull.onLoadMoreComplete();
                    // pull.setMore(true);
                }
            }, 1000);
        }
        list.addAll(result);
        adapter.notifyDataSetChanged();
        if (list.size() > 0)
        {
            pull.setVisibility(View.VISIBLE);
            ll.setVisibility(View.GONE);
        }
        else
        {
            pull.setVisibility(View.GONE);
            ll.setVisibility(View.VISIBLE);
        }
    }
    
    @Override
    public void onPageSelected(int page)
    {
        if (pageList == null || pageList.size() < 1)
        {
            return;
        }
        changeTextColor(page);
        switch (page)
        {
            case 0:
                pageAuditing = 0;
                searchRefundRecord(AUDITING, 0, false, true);
                currentViewPage = AUDITING;
                break;
            case 1:
                pageFulfil = 0;
                searchRefundRecord(FULFIL, 0, false, true);
                currentViewPage = FULFIL;
                break;
            case 2:
                pageFailure = 0;
                searchRefundRecord(FAILURE, 0, false, true);
                currentViewPage = FAILURE;
                break;
            default:
                break;
        }
    }
    
    private void queryDetail(final String outTradeNo)
    {
        RefundManager.getInstant().queryRefundDetail(outTradeNo,
            MainApplication.getMchId(),
            new UINotifyListener<Order>()
            {
                
                @Override
                public void onError(Object object)
                {
                    super.onError(object);
                    dismissLoading();
                    if (object != null)
                    {
                        showToastInfo(object.toString());
                    }
                }
                
                @Override
                public void onPreExecute()
                {
                    super.onPreExecute();
                    
                    showLoading(false, getStringById(R.string.public_data_loading));
                }
                
                @Override
                public void onSucceed(Order result)
                {
                    
                    super.onSucceed(result);
                    dismissLoading();
                    if (result != null)
                    {
                        RefundRecordOrderDetailsActivity.startActivity(RefundHistoryActivity.this, result);
                    }
                }
                
            });
    }
    
    /** {@inheritDoc} */
    
    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
    {
        Order order;
        int currentItem = refund_viewpager.getChildCount();
        switch (currentItem)
        {
            case 0://审核中
                order = listAuditing.get(position - 1);
                queryDetail(order.getOutRefundNo());
                break;
            case 1://已受理
                order = listFulfil.get(position - 1);
                queryDetail(order.getOutRefundNo());
                //                RefundRecordOrderDetailsActivity.startActivity(RefundHistoryActivity.this, order);
                break;
            case 2://失败
                order = listFailure.get(position - 1);
                queryDetail(order.getOutRefundNo());
                //                RefundRecordOrderDetailsActivity.startActivity(RefundHistoryActivity.this, order);
                break;
        //            default:
        //                break;
        }
    }
    
    /** {@inheritDoc} */
    
    @Override
    public void onRefresh()
    {
        //下拉刷新
        if (currentViewPage.equals(AUDITING))
        {
            //退款中
            pageAuditing = 0;
            searchRefundRecord(AUDITING, pageAuditing, false, false);
        }
        else if (currentViewPage.equals(FULFIL))
        {
            //退款已受理
            pageFulfil = 0;
            searchRefundRecord(FULFIL, pageFulfil, false, false);
        }
        else if (currentViewPage.equals(FAILURE))
        {
            //退款失败
            pageFailure = 0;
            searchRefundRecord(FAILURE, pageFailure, false, false);
        }
    }
    
    /** {@inheritDoc} */
    
    @Override
    public void onLoadMore()
    {
        
        //上拉 加载更多
        if (currentViewPage.equals(AUDITING))
        {
            //退款中
            mPullDownViewAuditing.setMore(true);
            searchRefundRecord(AUDITING, pageAuditing + 1, true, false);
        }
        else if (currentViewPage.equals(FULFIL))
        {
            //退款已受理
            mPullDownViewFulfil.setMore(true);
            searchRefundRecord(FULFIL, pageFulfil + 1, true, false);
        }
        else if (currentViewPage.equals(FAILURE))
        {
            //退款失败
            mPullDownViewFailure.setMore(true);
            searchRefundRecord(FAILURE, pageFailure + 1, true, false);
        }
    }
}
