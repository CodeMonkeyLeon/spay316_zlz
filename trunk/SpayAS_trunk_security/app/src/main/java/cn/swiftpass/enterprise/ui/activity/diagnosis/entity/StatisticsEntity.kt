package cn.swiftpass.enterprise.ui.activity.diagnosis.entity

import java.io.Serializable

data class StatisticsEntity(
    val sent: String,
    val receive: String
) : Serializable
