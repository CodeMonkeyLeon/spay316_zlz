/*
 * 文 件 名:  CashierFragment.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  yan_shuo
 * 修改时间:  2015-4-27
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.reward;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.AppHelper;

/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  Administrator
 * @version  [版本号, 2015-4-27]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class AwardDetiActivity extends TemplateActivity
{
    
    private WebView wb;
    
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //        initObject();
        setContentView(R.layout.activity_contenttext);
        String activeDetailUrl = getIntent().getStringExtra("activeDetailUrl");
        initViews(activeDetailUrl);
    }
    
    final class InJavaScriptLocalObj
    {
        public void showSource(String html)
        {
            Log.d("HTML", html);
        }
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.tx_active_detai);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }
            
            @Override
            public void onRightButtonClick()
            {
            }
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                
            }
        });
    }
    
    public static void startActivity(Context context, String url)
    {
        Intent it = new Intent();
        it.setClass(context, AwardDetiActivity.class);
        it.putExtra("activeDetailUrl", url);
        context.startActivity(it);
    }
    
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @SuppressLint("JavascriptInterface")
    private void initViews(String activeDetailUrl)
    {
        wb = (WebView)findViewById(R.id.webview);
//        wb.addJavascriptInterface(new InJavaScriptLocalObj(), "local_obj");
        wb.getSettings().setJavaScriptEnabled(true);
        wb.getSettings().setAllowFileAccessFromFileURLs(false);
        wb.getSettings().setAllowUniversalAccessFromFileURLs(false);
        
        WebSettings settings = wb.getSettings();
        if (AppHelper.getAndroidSDKVersion() == 17)
        {
            settings.setDisplayZoomControls(false);
        }
        settings.setLoadWithOverviewMode(true);
        //settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        //各种分辨率适应
        DisplayMetrics metrics = new DisplayMetrics();
        AwardDetiActivity.this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int mDensity = metrics.densityDpi;
        if (mDensity == 120)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        }
        else if (mDensity == 160)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        }
        else if (mDensity == 240)
        {
            settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        }
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        
        wb.setWebChromeClient(new myWebChromeClien());
        wb.setWebViewClient(new MyWebViewClient());
        //        int title = getIntent().getIntExtra("title", R.string.title_context_text);
        //        titleBar.setTitle(title);
        //        String url = getIntent().getStringExtra("url");
        try
        {
            wb.loadUrl(activeDetailUrl);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    class myWebChromeClien extends WebChromeClient
    {
        @Override
        public void onProgressChanged(WebView view, int newProgress)
        {
            super.onProgressChanged(view, newProgress);
            
        }
    }
    
    final class MyWebViewClient extends WebViewClient
    {
        public MyWebViewClient()
        {
            super();
        }
        
        /** {@inheritDoc} */
        
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            showNewLoading(true, getString(R.string.public_loading));
            super.onPageStarted(view, url, favicon);
        }
        
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            showNewLoading(true, getString(R.string.public_loading));
            return super.shouldOverrideUrlLoading(view, url);
            
        }
        
        @Override
        public void onPageFinished(WebView view, String url)
        {
            //            dialog.dismiss();
            dismissLoading();
            super.onPageFinished(view, url);
        }
        
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
        {
            super.onReceivedError(view, errorCode, description, failingUrl);
            dismissLoading();
            wb.loadUrl("file:///android_asset/error.html");
            
        }
    }
    
}
