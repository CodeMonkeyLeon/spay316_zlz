package cn.swiftpass.enterprise.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;
import cn.swiftpass.enterprise.utils.DisplayUtil;

public class Rotate90TextView extends TextView
{
    
    public Rotate90TextView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
    
    @Override
    protected void onDraw(Canvas canvas)
    {
        //canvas.rotate(90, getMeasuredWidth() / 2, getMeasuredHeight() / 2);
        canvas.rotate(90);
        canvas.translate(0, -DisplayUtil.dip2Px(getContext(), 60));
        
        super.onDraw(canvas);
    }
    
}
