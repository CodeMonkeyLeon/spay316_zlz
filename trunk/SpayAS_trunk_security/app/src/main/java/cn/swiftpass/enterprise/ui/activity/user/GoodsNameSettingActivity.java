package cn.swiftpass.enterprise.ui.activity.user;


import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.common.sentry.SentryUtils;

import cn.swiftpass.enterprise.bussiness.model.GoodsMode;
import cn.swiftpass.enterprise.intl.BuildConfig;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.contract.activity.GoodsNameSettingContract;
import cn.swiftpass.enterprise.mvp.presenter.activity.GoodsNameSettingPresenter;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.HandlerManager;

public class GoodsNameSettingActivity extends BaseActivity<GoodsNameSettingContract.Presenter> implements OnClickListener,
        GoodsNameSettingContract.View {

    private static final String TAG = GoodsNameSettingActivity.class.getSimpleName();
    GoodsMode goodsMode = null;
    private EditText goods_name_edit, goods_name_count;
    private Button goods_name_submit_btn;
    private TextView good_info;

    @Override
    protected GoodsNameSettingContract.Presenter createPresenter() {
        return new GoodsNameSettingPresenter();
    }

    @Override
    protected boolean useToolBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goods_name_setting);

        initView(goodsMode);
        initListener();
        goods_name_submit_btn.getBackground().setAlpha(102);
        load();

        setButBgAndFont(goods_name_submit_btn);
    }

    @Override
    public void queryBodySuccess(@Nullable GoodsMode response) {
        if (response != null) {
            goods_name_edit.setText(response.commodityName);
            goods_name_count.setText(response.commodityNum);
            goods_name_edit.setFocusable(true);
            goods_name_edit.setFocusableInTouchMode(true);
            goods_name_edit.requestFocus();
            goods_name_edit.setSelection(goods_name_edit.getText().length());
        }
    }

    @Override
    public void queryBodyFailed(@Nullable Object error) {

    }

    private void load() {
        if (mPresenter != null) {
            mPresenter.queryBody();
        }
    }

    private void initView(GoodsMode goodsMode) {
        this.goods_name_edit = findViewById(R.id.goods_name_edit);
        this.goods_name_submit_btn = findViewById(R.id.goods_name_submit_btn);
        goods_name_count = findViewById(R.id.goods_name_count);
        good_info = findViewById(R.id.good_info);
        if (!TextUtils.isEmpty(BuildConfig.bankType) && Integer.parseInt(BuildConfig.bankType) == 1) {
            good_info.setText(getResources().getString(R.string.goods_name_setting_des_xy));
        }

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (goods_name_edit.isFocused() || goods_name_count.isFocused()) {
                    String countStr = goods_name_count.getText().toString().trim();
                    int value = 0;
                    if (!TextUtils.isEmpty(countStr)) {
                        try {
                            value = Integer.valueOf(countStr);
                        } catch (NumberFormatException e) {
                            SentryUtils.INSTANCE.uploadTryCatchException(
                                    e,
                                    SentryUtils.INSTANCE.getClassNameAndMethodName()
                            );
                            Log.e(TAG, Log.getStackTraceString(e));
                        }
                    }

                    if (goods_name_edit.getText().toString().trim().length() > 0 && goods_name_count.getText().toString().trim().length() > 0 && value > 0) {
                        setButtonBg(goods_name_submit_btn, true, 0);
                    } else {
                        setButtonBg(goods_name_submit_btn, false, 0);
                    }
                }

            }
        };
        goods_name_edit.addTextChangedListener(textWatcher);
        goods_name_count.addTextChangedListener(textWatcher);
    }

    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b, enable, res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_press_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }

    private void initListener() {
        goods_name_submit_btn.setOnClickListener(this);
    }

    @Override
    protected void setupTitleBar() {

        super.setupTitleBar();
        titleBar.setTitle(getStringById(R.string.productName));
        titleBar.setLeftButtonVisible(true);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButtonClick() {

            }

            @Override
            public void onRightButLayClick() {
            }

            @Override
            public void onLeftButtonClick() {
                finish();
            }
        });
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.goods_name_submit_btn://提交
                String content = goods_name_edit.getText().toString().trim();
                String count = goods_name_count.getText().toString().trim();
                if (isAbsoluteNullStr(content)) {
                    toastDialog(GoodsNameSettingActivity.this, R.string.goods_name_not_be_empty, null);
                    goods_name_edit.setFocusable(true);
                    return;
                }
                sumit(content, count);

                break;

            default:
                break;
        }
    }


    @Override
    public void updateOrAddBodySuccess(boolean response, String body) {
        if (response) {
            toastDialog(GoodsNameSettingActivity.this, R.string.submit_success_auditing, new NewDialogInfo.HandleBtn() {

                @Override
                public void handleOkBtn() {
                    HandlerManager.notifyMessage(HandlerManager.VICE_SWITCH, HandlerManager.BODY_SWITCH, body);

                    finish();

                }
            });
        }
    }

    @Override
    public void updateOrAddBodyFailed(@Nullable Object error) {
        if (error != null) {
            toastDialog(GoodsNameSettingActivity.this, error.toString(), null);

        }
    }

    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void sumit(final String body, final String count) {
        if (mPresenter != null) {
            mPresenter.updateOrAddBody(body, count);
        }
    }
}
