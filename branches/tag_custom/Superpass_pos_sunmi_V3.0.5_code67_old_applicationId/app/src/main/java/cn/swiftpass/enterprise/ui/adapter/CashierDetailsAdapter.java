/*
 * 文 件 名:  CashierDetailsAdapter.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  yan_shuo
 * 修改时间:  2015-4-29
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.model.AwardModel;
import cn.swiftpass.enterprise.utils.DateUtil;

/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  Administrator
 * @version  [版本号, 2015-4-29]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class CashierDetailsAdapter extends BaseAdapter
{
    
    private List<AwardModel> list;
    
    private Context context;
    
    private ViewHolder holder;
    
    private String userId;
    
    public CashierDetailsAdapter(Context context, List<AwardModel> list)
    {
        this.context = context;
        this.list = list;
    }
    
    /**
     * @return 返回 userId
     */
    public String getUserId()
    {
        return userId;
    }
    
    /**
     * @param 对userId进行赋值
     */
    public void setUserId(String userId)
    {
        this.userId = userId;
    }
    
    @Override
    public int getCount()
    {
        return list.size();
    }
    
    @Override
    public Object getItem(int position)
    {
        return list.get(position);
    }
    
    @Override
    public long getItemId(int position)
    {
        return position;
    }
    
    /** {@inheritDoc} */
    @Override
    public boolean isEnabled(int position)
    {
        return false;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            
            convertView = View.inflate(context, R.layout.cashier_fragment_listview_item, null);
            holder = new ViewHolder();
            holder.tx_valid = (TextView)convertView.findViewById(R.id.tx_valid);
            holder.cashire_item_money_tv = (TextView)convertView.findViewById(R.id.cashire_item_money_tv);
            holder.tx_userId = (TextView)convertView.findViewById(R.id.tx_userId);
            holder.tx_userName = (TextView)convertView.findViewById(R.id.tx_userName);
            holder.cashire_image = (ImageView)convertView.findViewById(R.id.cashire_image);
            holder.tx_no_valid = (TextView)convertView.findViewById(R.id.tx_no_valid);
            holder.cashire_item_money_scale_tv = (TextView)convertView.findViewById(R.id.cashire_item_money_scale_tv);
            
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder)convertView.getTag();
        }
        AwardModel val = list.get(position);
        holder.tx_no_valid.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
            LayoutParams.MATCH_PARENT, Float.parseFloat(val.getPercent())));
        holder.tx_valid.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
            LayoutParams.MATCH_PARENT, (1 - Float.parseFloat(val.getPercent()))));
        if (val.getUserName() != null && !"".equals(val.getUserName()))
        {
            setUseName(holder.tx_userName, val.getUserName(), val.getUserId());
        }
        
        holder.tx_userId.setText("(" + val.getUserId() + ")");
        holder.cashire_item_money_tv.setText(DateUtil.formatMoneyUtils(val.getMoney()));
        holder.cashire_item_money_scale_tv.setText(val.getActiveOrder() + "");
        setImage(holder.cashire_image, val.getActiveOrder());
        return convertView;
    }
    
    private class ViewHolder
    {
        private TextView tx_valid, cashire_item_money_tv, tx_userId, tx_userName, tx_no_valid,
            cashire_item_money_scale_tv;
        
        private ImageView cashire_image;
        
    }
    
    boolean tag = false; //是否已经排名第一
    
    private void setImage(ImageView view, long activeOrder)
    {
        if (activeOrder == 1)
        {
            if (!tag)
            {
                tag = true;
                view.setImageResource(R.drawable.icon_reward_special);
            }
            else
            {
                view.setImageResource(R.drawable.icon_reward_special);
            }
        }
        else if (activeOrder == 2)
        {
            view.setImageResource(R.drawable.icon_reward_01);
        }
        else if (activeOrder == 3)
        {
            view.setImageResource(R.drawable.icon_reward_02);
        }
        else
        {
            view.setImageResource(R.drawable.icon_reward_default);
        }
    }
    
    private void setUseName(TextView view, String userName, String userId)
    {
        if (userName.length() == 1)
        {
            view.setText(userName);
        }
        else
        {
            if (null != getUserId() && !"".equals(getUserId()))
            {
                if (getUserId().equals(userId))
                {
                    view.setText(userName);
                    return;
                }
                String first = userName.substring(0, 1);
                //            int len = userName.length() - 1;
                for (int i = 0; i < 2; i++)
                {
                    first = new StringBuffer(first).append("*").toString();
//                    first += "*";
                }
                view.setText(first);
            }
            
            else
            {
                String first = userName.substring(0, 1);
                //            int len = userName.length() - 1;
                for (int i = 0; i < 2; i++)
                {
                    first = new StringBuffer(first).append("*").toString();
//                    first += "*";
                }
                view.setText(first);
                //                if (MainApplication.isAdmin.equals("1"))
                //                {
                //                    String first = userName.substring(0, 1);
                //                    //            int len = userName.length() - 1;
                //                    for (int i = 0; i < 2; i++)
                //                    {
                //                        first += "*";
                //                    }
                //                    view.setText(first);
                //                }
                //                else
                //                {
                //                    
                //                    view.setText(userName);
                //                }
                
            }
        }
        
    }
}
