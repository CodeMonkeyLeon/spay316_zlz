package cn.swiftpass.enterprise.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.bill.BillOrderManager;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.PreAuthDetailsBean;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.print.PrinterPreAuth;
import cn.swiftpass.enterprise.ui.activity.bill.OrderRefundActivity;
import cn.swiftpass.enterprise.ui.activity.print.BluetoothSettingActivity;
import cn.swiftpass.enterprise.ui.activity.user.RefundRecordOrderDetailsActivity;
import cn.swiftpass.enterprise.ui.adapter.PreAuthDetailsAdapter;
import cn.swiftpass.enterprise.ui.widget.CustomDialog;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.CreateOneDiCodeUtil;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 创建人：caoxiaoya
 * 时间：2018/12/24
 * 描述：预授权转支付
 * 备注：选择账单→查询账单→扫一扫小票二维码或输入平台预授权订单号→进入预授权订单详情→进入扣款页面进行扣款→收款成功
 */
public class PreAuthDetailsActivity extends TemplateActivity {

    private RecyclerView mRecyclerView;
    private PreAuthDetailsAdapter mAdapter;
    private Order mOrder;
    private  int itemType;
    private List<PreAuthDetailsBean> mList=new ArrayList<>();
    private int isUnfreezeOrPay;// 1支付 2解冻
    private String state="";
    private int isRefreshList;//如果同步成功，销毁时候去刷新列表
    private PrinterPreAuth printerPreAuthDetails;


    public static void startActivity(Context mContext, Order order, int itemType,int isUnfreezeOrPay) {
        Intent it = new Intent();
        it.setClass(mContext, PreAuthDetailsActivity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT); //W.l 解决弹出多个activity
        it.putExtra("itemType",itemType);
        it.putExtra("order", order);
        it.putExtra("isUnfreezeOrPay",isUnfreezeOrPay);
        mContext.startActivity(it);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_auth_details);
        initView();
        initData();

        bindDeviceService();
    }


    @Override
    protected void onResume() {
        super.onResume();
        this.bindDeviceService();
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.unbindDeviceService();
    }

    private void initView(){
        mRecyclerView= findViewById(R.id.rv_pre_auth);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    private void initData() {
        mOrder=(Order) getIntent().getSerializableExtra("order");
        itemType=getIntent().getIntExtra("itemType",0);
        isUnfreezeOrPay=getIntent().getIntExtra("isUnfreezeOrPay",0);
        switch (itemType){
            case 1://已支付金额{未支付、收款成功}
                titleBar.setTitle(R.string.title_order_detail);
                if (mOrder!=null){
                    mAdapter=new PreAuthDetailsAdapter(this,mList,1);
                    mRecyclerView.setAdapter(mAdapter);
                    type_one();
                    mAdapter.setHeaderView(createHeadView());
                    if (isUnfreezeOrPay==1) {// 1支付 2解冻
                        mAdapter.setFooterView(unfreezeFootView());
                    }
                }
                break;

            case 2://解冻金额{已解冻、解冻失败}
                titleBar.setTitle(R.string.unfreeze_details);
                if (mOrder!=null){
                    mAdapter=new PreAuthDetailsAdapter(this,mList,2);
                    mRecyclerView.setAdapter(mAdapter);
                    type_two();
                    mAdapter.setHeaderView(createHeadView());
                   if (isUnfreezeOrPay==2){
                        mAdapter.setFooterView(payFootView());
                    }
                }
                break;
                default:
                    break;
        }
    }

    private void type_one(){
        if (mOrder.getOperationStatus()!=null&&mOrder.getOperationStatus()==1){
            titleBar.setRightButLayVisibleForTotal(true,getString(R.string.title_order_refund));//退款
            mList.add(new PreAuthDetailsBean(getString(R.string.order_status), null,getString(R.string.tx_bill_stream_chioce_succ)));//收款成功
            mList.add(new PreAuthDetailsBean(getString(R.string.pre_auth_time),mOrder.getOperateTime()+"",null));//交易时间
            mList.add(new PreAuthDetailsBean(getString(R.string.payment_method),mOrder.getTradeName(),null));//交易方式
            mList.add(new PreAuthDetailsBean(getString(R.string.tv_pay_zfb_order),mOrder.getAuthNo(),null));//支付宝单号
            mList.add(new PreAuthDetailsBean(getString(R.string.platform_pre_auth_order_id),mOrder.getAuthNo(),null));//平台订单号
        }else {
            mList.add(new PreAuthDetailsBean(getString(R.string.order_status), null,getString(R.string.stream_receivable)));//未支付
            mList.add(new PreAuthDetailsBean(getString(R.string.unfreezed_time),mOrder.getOperateTime()+"",null));//交易时间
            mList.add(new PreAuthDetailsBean(getString(R.string.transaction_method),mOrder.getTradeName(),null));//交易方式
            mList.add(new PreAuthDetailsBean(getString(R.string.platform_pre_auth_order_id),mOrder.getOrderNo(),null));//平台订单号
        }
        state=getString(R.string.tv_charge_total);

        mAdapter.setData(mList);
    }

    private void type_two(){
        if (mOrder.getOperationStatus()!=null&&mOrder.getOperationStatus()==1){
            mList.add(new PreAuthDetailsBean(getString(R.string.order_status),
                    null,getString(R.string.freezen_success)));//已解冻
            state=getString(R.string.unfreezed_amount);
        }else{
            mList.add(new PreAuthDetailsBean(getString(R.string.order_status),
                    null,getString(R.string.unfreezen_failed)));//解冻失败
            state=getString(R.string.unfreezing_amount);
        }

        mList.add(new PreAuthDetailsBean(getString(R.string.unfreezed_order_id),mOrder.getOutRequestNo(),null));//解冻订单号
        mList.add(new PreAuthDetailsBean(getString(R.string.unfreezed_time),mOrder.getOperateTime()+"",null));//解冻时间
        mList.add(new PreAuthDetailsBean(getString(R.string.platform_pre_auth_order_id),mOrder.getAuthNo(),null));//平台授权订单号
        mAdapter.setData(mList);
    }


    private View createHeadView(){
        LinearLayout.LayoutParams layoutParams=
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        LayoutInflater inflater = LayoutInflater.from(this);
        View viewHead=inflater.inflate(R.layout.item_preauth_head, null);
        viewHead.setLayoutParams(layoutParams);

        TextView tv_type_money= viewHead.findViewById(R.id.tv_type_money);
        TextView tv_money= viewHead.findViewById(R.id.tv_money);
        TextView tv_type_total= viewHead.findViewById(R.id.tv_type_total);

        tv_type_total.setText(state);
        tv_type_money.setText(MainApplication.getFeeFh()+ DateUtil.formatMoneyUtils(mOrder.getMoney()));
        tv_money.setVisibility(View.GONE);
        return viewHead;
    }
    private TextView tv_description;
    private Button btn_blue_print;
    private View payFootView(){
        LinearLayout.LayoutParams layoutParams=
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LayoutInflater inflater = LayoutInflater.from(this);
        View viewFoot=inflater.inflate(R.layout.item_preauth_foot, null);
        viewFoot.setLayoutParams(layoutParams);
        Button btn_finish= viewFoot.findViewById(R.id.btn_finish);//完成
        btn_blue_print= viewFoot.findViewById(R.id.btn_blue_print);//打印
        final ImageView iv_qr= viewFoot.findViewById(R.id.iv_qr);//一维码
        TextView tv_qr_numbertext= viewFoot.findViewById(R.id.tv_qr_numbertext);//一维码数字
        tv_description= viewFoot.findViewById(R.id.tv_description);//说明
        TextView tv_refund_record= viewFoot.findViewById(R.id.tv_refund_record);
        Log.e("cxy","itemType"+itemType);
        switch (itemType){
            case 1:
                if (mOrder.getOperationStatus()!=null&&mOrder.getOperationStatus()==1) {
                    tv_description.setPadding(50, 30, 50, 30);
                    tv_description.setGravity(Gravity.LEFT);
                    tv_description.setTextColor(getResources().getColor(R.color.bg_color_text));
                    tv_description.setVisibility(View.VISIBLE);
                    tv_description.setText(getString(R.string.unfreeze));
                    btn_blue_print.setVisibility(View.VISIBLE);
                    btn_blue_print.setText(getString(R.string.bt_print));
                    btn_blue_print.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                          bluePrint(2);
                        }
                    });
                    // 生成一维码
                    if (!isAbsoluteNullStr(mOrder.getAuthNo())) {
                        iv_qr.setVisibility(View.VISIBLE);
                        tv_qr_numbertext.setText(mOrder.getAuthNo());
                        tv_qr_numbertext.setVisibility(View.VISIBLE);
                        WindowManager wm = this.getWindowManager();
                        int width = wm.getDefaultDisplay().getWidth();
                        final int w = (int) (width * 0.85);
                        try {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    iv_qr.setImageBitmap(CreateOneDiCodeUtil.createCode(mOrder.getAuthNo(), w, 180));
                                }
                            });
                        } catch (Exception e) {
                        }
                    }
                }
                break;
            case 2:
                if (mList!=null&&mList.size()>0&&mList.get(0).getMsg().equals(getString(R.string.freezen_success))) {
                    tv_description.setPadding(50, 30, 50, 30);
                    tv_description.setGravity(Gravity.LEFT);
                    tv_description.setTextColor(getResources().getColor(R.color.bg_color_text));
                    tv_description.setVisibility(View.VISIBLE);
                    tv_description.setText(getString(R.string.unfreeze));
                    btn_blue_print.setVisibility(View.VISIBLE);
                    btn_blue_print.setText(getString(R.string.bt_print));
                    btn_blue_print.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            bluePrint(2);
                        }
                    });
                }else {
                    //未支付
                    btn_blue_print.setText(R.string.syn_order);
                    btn_blue_print.setVisibility(View.VISIBLE);
                    btn_blue_print.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //订单同步
                            synchronizeOrder();
                        }
                    });
                }
                break;
                default:
                    break;
        }

        return viewFoot;
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
        titleBar.setLeftButtonVisible(true);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {

            }

            @Override
            public void onRightButLayClick() {
                OrderRefundActivity.startActivity(PreAuthDetailsActivity.this, mOrder,3);
            }
        });
    }
    private View unfreezeFootView() {
        LinearLayout.LayoutParams layoutParams =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LayoutInflater inflater = LayoutInflater.from(this);
        View viewFoot = inflater.inflate(R.layout.item_preauth_foot, null);
        viewFoot.setLayoutParams(layoutParams);

        Button btn_finish = viewFoot.findViewById(R.id.btn_finish);//完成
        Button btn_blue_print = viewFoot.findViewById(R.id.btn_blue_print);//打印
        final ImageView iv_qr = viewFoot.findViewById(R.id.iv_qr);//一维码
        TextView tv_qr_numbertext = viewFoot.findViewById(R.id.tv_qr_numbertext);//一维码数字
        TextView tv_description = viewFoot.findViewById(R.id.tv_description);//说明
        TextView tv_refund_record= viewFoot.findViewById(R.id.tv_refund_record);
        Log.e("cxy","itemType"+itemType);
        switch (itemType) {
            case 1:
                if (mOrder.getOperationStatus()!=null&&mOrder.getOperationStatus()==1){
                  //收款成功
                    tv_refund_record.setVisibility(View.VISIBLE);
                    tv_refund_record.setGravity(Gravity.CENTER);
                    Drawable drawable = getResources().getDrawable(
                            R.drawable.icon_check);
                    drawable.setBounds(3, 4, drawable.getMinimumWidth(),
                            drawable.getMinimumHeight());
                    tv_refund_record.setCompoundDrawables(null, null, drawable, null);
                    tv_refund_record.setText(getString(R.string.refund_record));
                    tv_refund_record.setTextColor(getResources().getColor(R.color.title_bg_new));
                    tv_refund_record.setPadding(0,20,0,0);

                    tv_refund_record.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            loadDate(1,false,0,null);//退款列表
                        }
                    });

                    btn_blue_print.setVisibility(View.VISIBLE);

                    if (MainApplication.bluetoothSocket != null && MainApplication.bluetoothSocket.isConnected()) {
                        btn_blue_print.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        bluePrint(1);
                                    }
                                });
                            }
                        });
                    }
                }else {
                   //未支付
                    btn_blue_print.setText(R.string.syn_order);
                    btn_blue_print.setVisibility(View.VISIBLE);
                    btn_blue_print.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //订单同步
                            synchronizeOrder();
                        }
                    });
                }
                // 生成一维码
                if (!isAbsoluteNullStr(mOrder.getOrderNo())) {
                    iv_qr.setVisibility(View.VISIBLE);
                    tv_qr_numbertext.setText(mOrder.getOrderNo());
                    tv_qr_numbertext.setPadding(0,10,0,0);
                    tv_qr_numbertext.setVisibility(View.VISIBLE);
                    WindowManager wm = this.getWindowManager();
                    int width = wm.getDefaultDisplay().getWidth();
                    final int w = (int) (width * 0.85);
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                iv_qr.setImageBitmap(CreateOneDiCodeUtil.createCode(mOrder.getOrderNo(), w, 180));
                            }
                        });
                    } catch (Exception e) {
                    }
                    iv_qr.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            PreAuthDetailsActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    OneDiCodeActivity.startActivity(PreAuthDetailsActivity.this,mOrder.getAuthNo());
                                }
                            });
                        }
                    });
                }
                break;
            default:
                break;
        }

        return viewFoot;
    }

    /**
     * *  以下字段区别使用。
     *  *   1、查询预授权订单时使用此字段
     *  *  @param outAuthNo  商户授权号
     *  *   2、查询同步解冻订单时使用此两个字段
     *  *  @param outRequestNo  商户请求号
     *  *  @param authNo  平台授权号
     */
    private void synchronizeOrder(){
        if (mOrder!=null){
            loadDialog(PreAuthDetailsActivity.this,R.string.loading);
            OrderManager.getInstance().synchronizeOrder(mOrder.getAuthNo(),mOrder.getOutRequestNo(), new UINotifyListener<Order>() {
                @Override
                public void onError(Object object) {
                    super.onError(object);
                    dismissLoading();
                    PreAuthDetailsActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showErrorDialog();
                        }
                    });
                }

                @Override
                public void onSucceed(Order result) {
                    super.onSucceed(result);
                    dismissLoading();
                    if (result != null) {
                       if (itemType==2&&!TextUtils.isEmpty(result.getState())&&result.getState().equals("2")){
                           isRefreshList=1;
                           mOrder.setOperationStatus(1);
                           mList.clear();
                           type_two();

                           Intent i = new Intent();
                           i.putExtra("isRefreshList", isRefreshList);
                           setResult(PreAuthThawActivity.REQUEST_REFRESH, i);

                           tv_description.setPadding(50, 30, 50, 30);
                           tv_description.setGravity(Gravity.LEFT);
                           tv_description.setTextColor(getResources().getColor(R.color.bg_color_text));
                           tv_description.setVisibility(View.VISIBLE);
                           tv_description.setText(getString(R.string.unfreeze));
                           btn_blue_print.setVisibility(View.VISIBLE);
                           btn_blue_print.setText(getString(R.string.bt_print));
                           btn_blue_print.setOnClickListener(new View.OnClickListener() {
                               @Override
                               public void onClick(View view) {
                                   bluePrint(2);
                               }
                           });
                        }else {
                           showErrorDialog();
                       }
                    }
                }
            });
        }
    }
    private CustomDialog errorDialog;
    private void showErrorDialog(){
       if (errorDialog==null) {
           errorDialog = new CustomDialog.Builder(PreAuthDetailsActivity.this)
                   .gravity(Gravity.CENTER).widthdp(250)
                   .cancelTouchout(true)
                   .view(R.layout.dialog_error)
                   .addViewOnclick(R.id.tv_confirm, new View.OnClickListener() {
                       @Override
                       public void onClick(View v) {
                           errorDialog.dismiss();
                       }
                   })
                   .build();
           errorDialog.show();
           TextView tv_msg= errorDialog.findViewById(R.id.tv_title);
           tv_msg.setText(getString(R.string.unfreezen_failed));
       }else {
           if (!errorDialog.isShowing()){
               errorDialog.show();
           }
       }
    }
   private void loadDate(final int page, final boolean isLoadMore, int isRefund, String startDate) {

        BillOrderManager.getInstance().querySpayOrder(null,0, null, null, isRefund, page, mOrder.getOrderNoMch(), startDate, new UINotifyListener<List<Order>>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                dissDialog();
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                dissDialog();

            }

            @Override
            public void onSucceed(List<Order> model) {
                dissDialog();
                Logger.i("JAMY","list: "+model.size());
                int size = model.size();
                if (size == 1){
                    Order order = model.get(0);
                    go2OrderDetailActivity(order);
                }else{
                    go2orderListActivity(size);
                }
            }
        });
    }

    /**
     * 进入退款记录列表
     * @param order
     */
    private final static int QUERY_REFUND = 0x111;
    private void go2OrderDetailActivity(Order order){
        Intent intent  = new Intent();
        intent.setClass(PreAuthDetailsActivity.this,RefundRecordOrderDetailsActivity.class);
        intent.putExtra("order",order);
        intent.putExtra("Tag",QUERY_REFUND);
        startActivity(intent);
    }
    /**
     * 进入退款记录列表
     */
    private void go2orderListActivity(int size){
        Intent intent  = new Intent();
        intent.setClass(PreAuthDetailsActivity.this,RefundOrderListActivity.class);
        intent.putExtra("orderNo", mOrder.getOrderNoMch());
        intent.putExtra("order_size",size+"");
        startActivity(intent);
    }

    public void bluePrint(final int printType){
        bindDeviceService();
        mOrder.setPartner(getString(R.string.tv_pay_user_stub));
        printerPreAuthDetails = new PrinterPreAuth(this, printType,mOrder) {
            @Override
            protected void onDeviceServiceCrash() {
                PreAuthDetailsActivity.this.bindDeviceService();
            }

            @Override
            protected void displayPrinterInfo(String info) {
                Log.i("zhouwei", "打印完成" + info);
            }
        };
        printerPreAuthDetails.startPrint();

        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                mOrder.setPartner(getString(R.string.tv_pay_mch_stub));
                printerPreAuthDetails = new PrinterPreAuth(PreAuthDetailsActivity.this, printType,mOrder) {
                    @Override
                    protected void onDeviceServiceCrash() {
                        PreAuthDetailsActivity.this.bindDeviceService();
                    }

                    @Override
                    protected void displayPrinterInfo(String info) {
                        Log.i("zhouwei", "打印完成" + info);
                    }
                };
                printerPreAuthDetails.startPrint();
            }
        }, 4000L);
    }


    /**
     * <一句话功能简述>
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void showDialog() {
        dialog = new DialogInfo(PreAuthDetailsActivity.this, null, getString(R.string.tx_blue_set), getString(R.string.title_setting), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                showPage(BluetoothSettingActivity.class);
                dialog.cancel();
                dialog.dismiss();
            }

            @Override
            public void handleCancleBtn() {
                dialog.cancel();
            }
        }, null);

        DialogHelper.resize(PreAuthDetailsActivity.this, dialog);
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

}
