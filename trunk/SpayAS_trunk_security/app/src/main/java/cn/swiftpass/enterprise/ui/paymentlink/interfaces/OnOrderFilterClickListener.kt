package cn.swiftpass.enterprise.ui.paymentlink.interfaces

interface OnOrderFilterClickListener {
    fun onClick(type: String)
}