package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.OrderDetailsBaseContract
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkOrder
import cn.swiftpass.enterprise.utils.JsonUtil
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/11/23
 *
 * @你在雾海中航行
 * @没有帆
 * @你在月夜下漂泊
 * @没有锚
 * @路从这里消失
 * @夜从这里消失
 */
class OrderDetailsBasePresenter : OrderDetailsBaseContract.Presenter {

    private var mView: OrderDetailsBaseContract.View? = null


    override fun getOrderDetail(order: PaymentLinkOrder?) {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)

            var orderNo = ""
            order?.let {
                orderNo = it.orderNo
            }

            AppClient.getUpdateOrderDetail(
                "2",
                orderNo,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<PaymentLinkOrder>(PaymentLinkOrder()) {
                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.getOrderDetailFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果orderRelateGoodsList值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"orderRelateGoodsList\":\"\"",
                                "\"orderRelateGoodsList\":[]"
                            )
                            response.message = response.message.replace(
                                "\"orderCostList\":\"\"",
                                "\"orderCostList\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                PaymentLinkOrder::class.java
                            ) as PaymentLinkOrder
                            view.getOrderDetailSuccess(order)
                        }
                    }
                }
            )

        }
    }

    override fun updateOrderStatus(order: PaymentLinkOrder?) {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
            var orderNo = ""
            order?.let {
                orderNo = it.orderNo
            }
            AppClient.getUpdateOrderDetail(
                "6",
                orderNo,
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<PaymentLinkOrder>(PaymentLinkOrder()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.updateOrderStatusFailed(it.message)
                        }
                    }


                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果orderRelateGoodsList值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"orderRelateGoodsList\":\"\"", "\"orderRelateGoodsList\":[]"
                            )
                            response.message = response.message.replace(
                                "\"orderCostList\":\"\"", "\"orderCostList\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message, PaymentLinkOrder::class.java
                            ) as PaymentLinkOrder
                            view.updateOrderStatusSuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: OrderDetailsBaseContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}