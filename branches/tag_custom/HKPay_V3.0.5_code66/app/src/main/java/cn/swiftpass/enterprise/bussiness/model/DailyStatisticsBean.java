package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;
import java.math.BigDecimal;

import cn.swiftpass.enterprise.utils.Logger;

/**
 * Created by aijingya on 2019/4/9.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: ${TODO}(订单列表页当天交易数据统计)
 * @date 2019/4/9.15:30.
 */
public class DailyStatisticsBean implements Serializable {

     private  Long transactionAmount;     //	交易金额
     private  int transactionsCount;     //	交易笔数
     private  Long refundAmount;         //	退款金额
     private  int refundCount;           //	退款笔数
     private  Long transactionsRetainedProfits;//	交易净额
     private  double yesterdayDoD;              //	昨日环比

    public Long getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Long transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public int getTransactionsCount() {
        return transactionsCount;
    }

    public void setTransactionsCount(int transactionsCount) {
        this.transactionsCount = transactionsCount;
    }

    public Long getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(Long refundAmount) {
        this.refundAmount = refundAmount;
    }

    public int getRefundCount() {
        return refundCount;
    }

    public void setRefundCount(int refundCount) {
        this.refundCount = refundCount;
    }

    public Long getTransactionsRetainedProfits() {
        return transactionsRetainedProfits;
    }

    public void setTransactionsRetainedProfits(Long transactionsRetainedProfits) {
        this.transactionsRetainedProfits = transactionsRetainedProfits;
    }

    public double getYesterdayDoD() {
        return yesterdayDoD;
    }

    public void setYesterdayDoD(double yesterdayDoD) {
        this.yesterdayDoD = yesterdayDoD;
    }
}
