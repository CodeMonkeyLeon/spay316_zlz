package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView
import cn.swiftpass.enterprise.ui.paymentlink.model.ImageUrl
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkCustomer
import cn.swiftpass.enterprise.ui.paymentlink.model.PaymentLinkProduct

/**
 * @author lizheng.zhao
 * @date 2022/12/6
 *
 * @月光还是少年的月光
 * @九州一色还是李白的霜
 */
class CustAndProdEditContract {

    interface View : BaseView {

        fun uploadImgSuccess(response: ImageUrl)

        fun uploadImgFailed(error: Any?)

        fun getProductDetailSuccess(response: PaymentLinkProduct?)

        fun getProductDetailFailed(error: Any?)


        fun getCustomerDetailSuccess(response: PaymentLinkCustomer?)

        fun getCustomerDetailFailed(error: Any?)

        fun addNewProductSuccess(response: PaymentLinkProduct?)

        fun addNewProductFailed(error: Any?)

        fun editProductDetailSuccess(response: String?)

        fun editProductDetailFailed(error: Any?)


        fun addNewCustomerSuccess(response: PaymentLinkCustomer?)

        fun addNewCustomerFailed(error: Any?)

        fun editCustomerDetailSuccess(response: String?)

        fun editCustomerDetailFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {

        fun uploadImg(
            imgBase64: String?,
            imgType: String?
        )

        fun editCustomerDetail(customer: PaymentLinkCustomer?)

        fun addNewCustomer(customer: PaymentLinkCustomer?)


        fun editProductDetail(product: PaymentLinkProduct?)

        fun addNewProduct(product: PaymentLinkProduct?)


        fun getCustomerDetail(customer: PaymentLinkCustomer?)


        fun getProductDetail(product: PaymentLinkProduct?)

    }


}