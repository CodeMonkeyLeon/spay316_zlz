package cn.swiftplus.enterprise.printsdk.print.A920.printUtils;

import android.content.Context;
import android.graphics.Bitmap;

import com.example.common.sentry.SentryUtils;
import com.pax.dal.IPrinter;
import com.pax.dal.exceptions.PrinterDevException;
import com.pax.gl.IGL;
import com.pax.gl.imgprocessing.IImgProcessing;
import com.pax.gl.imgprocessing.IImgProcessing.IPage;
import com.pax.gl.imgprocessing.IImgProcessing.IPage.EAlign;
import com.pax.gl.impl.GLProxy;

public class PrintA920Text {

    private static PrintA920Text printText;
    private IPrinter printer;
    String line = "6";
    String space = "1";
    private static IGL gl;
    private IImgProcessing ImgProcessing;
    private IPage page;
    private Context mContext;

    private PrintA920Text() {
        printer = GetObj.getDal().getPrinter();
    }

    public static PrintA920Text getInstance() {
        if (printText == null) {
            printText = new PrintA920Text();
        }
        return printText;
    }

    public void init(Context context) {
        try {
            printer.init();
            mContext = context;

            gl = new GLProxy(mContext).getGL();
            ImgProcessing = gl.getImgProcessing();
            page = ImgProcessing.createPage();

        } catch (PrinterDevException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
    }


    public void printTitle(String text) {
        page.addLine().addUnit(text, 30, EAlign.CENTER);
    }


    public void printTextLine(String text, int TextSize, IImgProcessing.IPage.EAlign align) {
        page.addLine().addUnit(text, TextSize, align);
    }

    public void printEmptyLine() {
        page.addLine().addUnit(" ", 30, EAlign.CENTER);
    }

    public void printDoubleLine() {
        page.addLine().addUnit("==========================", 28, EAlign.CENTER);
    }

    public void printSingleLine() {
        page.addLine().addUnit("-------------------------------------------------------------", 24, EAlign.CENTER);
    }

    public void printQRCode(String orderNoMch) {
        //打印小票的代码屏蔽
        Bitmap QR_Bitmap = CreateOneDiCodeUtilPrint.createCode(orderNoMch, 260, 260);
        page.addLine().addUnit(QR_Bitmap, EAlign.CENTER);
    }

    public void startPrint() {
        try {
            PrinterTester.getInstance().printBitmap(ImgProcessing.pageToBitmap(page, 410));
            printer.printStr(" \n", null);
            printer.printStr(" \n", null);
            printer.printStr(" \n", null);
            printer.printStr(" \n", null);
            printer.start();
        } catch (PrinterDevException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
    }


    public String start() {

        try {
            printer.start();
        } catch (PrinterDevException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
        }
        return "";

    }

    public String statusCode2Str(int status) {
        String res = "";
        switch (status) {
            case 0:
                res = "Success ";
                break;
            case 1:
                res = "Printer is busy ";
                break;
            case 2:
                res = "Out of paper ";
                break;
            case 3:
                res = "The format of print data packet error ";
                break;
            case 4:
                res = "Printer malfunctions ";
                break;
            case 8:
                res = "Printer over heats ";
                break;
            case 9:
                res = "Printer voltage is too low";
                break;
            case 240:
                res = "Printing is unfinished ";
                break;
            case 252:
                res = " The printer has not installed font library ";
                break;
            case 254:
                res = "Data package is too long ";
                break;
            default:
                break;
        }
        return res;
    }

}
