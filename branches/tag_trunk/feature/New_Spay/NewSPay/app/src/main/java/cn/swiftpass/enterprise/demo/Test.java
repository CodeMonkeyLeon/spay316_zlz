package cn.swiftpass.enterprise.demo;


import cn.swiftpass.enterprise.http.HttpManager;


/**
 * Created by YZX on 2019年04月08日.
 * 每一个不曾起舞的日子 都是对生命的辜负
 */
public class Test {

    public static void main(String[] args) {
        LoginApi loginApi = HttpManager.getInstance().create(LoginApi.class);
        loginApi.login("登录账号", "密码").enqueue(new ResponseHandler<LoginEntity>() {
            @Override
            public void onSuccess(LoginEntity result) {

            }

            @Override
            public void onFailure(String code, String error) {

            }
        });

        loginApi.register("注册账号", "密码").enqueue(new ResponseHandler<RegisterEntity>() {
            @Override
            public void onSuccess(RegisterEntity result) {

            }

            @Override
            public void onFailure(String code, String error) {

            }
        });
    }
}
