package cn.swiftpass.enterprise.utils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.UUID;

import org.apache.http.conn.util.InetAddressUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import cn.swiftpass.enterprise.MainApplication;

public class NetworkUtils
{
    
    public static boolean isNetworkAvailable(Context context)
    {
        ConnectivityManager connectivity = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null)
        {
            return false;
        }
        else
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
            {
                for (int i = 0; i < info.length; i++)
                {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED
                        || info[i].getState() == NetworkInfo.State.CONNECTING)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    public static boolean isNetWorkValid(Context context)
    {
        
        ConnectivityManager connectMgr = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo simNetInfo = connectMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifiNetInfo = connectMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        boolean simNet = false;
        if (simNetInfo != null)
        {
            simNet = simNetInfo.isConnectedOrConnecting();
        }
        boolean wifiNet = false;
        if (wifiNetInfo != null)
        {
            wifiNet = wifiNetInfo.isConnectedOrConnecting();
        }
        return simNet || wifiNet;
    }
    
    public static String getFileNameFromUrl(String url)
    {
        // 通过 ‘？’ 和 ‘/’ 判断文件名
        int index = url.lastIndexOf('?');
        String filename;
        if (index > 1)
        {
            filename = url.substring(url.lastIndexOf('/') + 1, index);
        }
        else
        {
            filename = url.substring(url.lastIndexOf('/') + 1);
        }
        
        if (filename == null || "".equals(filename.trim()))
        {// 如果获取不到文件名称
            filename = UUID.randomUUID() + ".apk";// 默认取一个文件名
        }
        return filename;
    }
    
    /**
     * 链接WIFI 获取MAC地址
     *
     * */
    public static String getLocalMacAddress()
    {
        ConnectivityManager cManager =
            (ConnectivityManager)MainApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo.State wifiState = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState();
        if (NetworkInfo.State.CONNECTED == wifiState)
        {
            WifiManager wifi = (WifiManager)MainApplication.getContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo info = wifi.getConnectionInfo();
            return info.getMacAddress();
        }
        else
        {
            //
            if (AppHelper.getAndroidSDKVersion() >= 9)
            {
                return getLocalMacAddressFromIp();
            }
        }
        return null;
    }
    
    /**
     * 根据IP获取本地Mac   API 9 以上
     * @param
     * @return
     */
    @SuppressLint("NewApi")
    public static String getLocalMacAddressFromIp()
    {
        String mac_s = "";
        try
        {
            byte[] mac;
            NetworkInterface ne = NetworkInterface.getByInetAddress(InetAddress.getByName(getLocalIpAddress()));
            mac = ne.getHardwareAddress();
            mac_s = byte2hex(mac);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        return mac_s;
    }
    
    public static String byte2hex(byte[] b)
    {
        StringBuffer hs = new StringBuffer(b.length);
        String stmp = "";
        int len = b.length;
        for (int n = 0; n < len; n++)
        {
            stmp = Integer.toHexString(b[n] & 0xFF);
            if (stmp.length() == 1)
                hs = hs.append("0").append(stmp);
            else
            {
                hs = hs.append(stmp);
            }
        }
        return String.valueOf(hs);
    }
    
    /**获取IP地址*/
    public static String getLocalIpAddress()
    {
        try
        {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();)
            {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();)
                {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()
                        && InetAddressUtils.isIPv4Address(inetAddress.getHostAddress()))
                    {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        }
        catch (SocketException ex)
        {
            Logger.e(ex);
        }
        return null;
    }
}
