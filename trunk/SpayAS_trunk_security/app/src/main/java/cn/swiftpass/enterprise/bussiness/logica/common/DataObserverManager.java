package cn.swiftpass.enterprise.bussiness.logica.common;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.example.common.sentry.SentryUtils;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Hashtable;
import java.util.Iterator;

import cn.swiftpass.enterprise.utils.KotlinUtils;

/**
 * 主线程注册/取消注册：registerObserver, unregisterObserver
 * <p>
 * 其它线程通知:notifyChange
 */
public class DataObserverManager {
    private static final String TAG = DataObserverManager.class.getSimpleName();
    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String tableName = (String) msg.obj;
            processChange(tableName);
        }
    };
    /**
     * 一个URI可以有多个Observer, 是不是同一个Observer，是由Uri和名字一起决定的
     */
    private Hashtable<String, Hashtable<String, DataObserver>> observers;

    public void registerObserver(DataObserver observer) {
        Hashtable<String, DataObserver> group = observers.get(observer
                .getEventName());
        if (group == null) {
            group = new Hashtable<String, DataObserver>();
            group.put(observer.getConsumerName(), observer);
            observers.put(observer.getEventName(), group);
        } else {
            group.put(observer.getConsumerName(), observer);
        }
    }

    /**
     * 取消注册观察者
     *
     * @param event        如果为NULL, 则移除consumerName注册的所有Observer, 例如：DataObserver.EVENT_RECEIVED_POS
     * @param consumerName 通常为调用registerObserver的类的class.getName();
     */
    public void unregisterObserver(String event, String consumerName) {
        Hashtable<String, DataObserver> group = null;
        if (!TextUtils.isEmpty(event)) {
            group = observers.get(event);
            unregisterOneObserver(group, consumerName);
        } else {
            for (Hashtable<String, DataObserver> g : observers.values()) {
                unregisterOneObserver(g, consumerName);
            }
        }
    }

    private void unregisterOneObserver(Hashtable<String, DataObserver> group, String consumerName) {
        if (group == null) {
            return;
        }
        group.remove(consumerName);
    }

    /**
     * 通知数据变更
     *
     */
    public void notifyChange(String event) {
        Message msg = handler.obtainMessage();
        msg.obj = event;
        handler.sendMessage(msg);
    }

    /**
     * 在UI线程中响应Observer的onChange事件
     *
     */
    private void processChange(String event) {
        Hashtable<String, DataObserver> group = observers.get(event);
        try {
            if (group != null) {
                Collection<DataObserver> values = group.values();
                synchronized (values) {
                    Iterator<DataObserver> it = values.iterator();
                    try {
                        while (it.hasNext()) {
                            it.next().onChange();
                        }
                    } catch (Exception e) {
                        SentryUtils.INSTANCE.uploadTryCatchException(
                                e,
                                SentryUtils.INSTANCE.getClassNameAndMethodName()
                        );
                        Log.e(TAG, Log.getStackTraceString(e));
                    }
                }
            }
        } catch (ConcurrentModificationException e) {
            SentryUtils.INSTANCE.uploadTryCatchException(
                    e,
                    SentryUtils.INSTANCE.getClassNameAndMethodName()
            );
            Log.e(TAG, Log.getStackTraceString(e));
        }
    }

    /**
     * 实现单例
     */
    private static class SingletonContainer {
        private static DataObserverManager instance = new DataObserverManager();
    }

    /**
     * 获取单例
     *
     * @return
     */
    public static DataObserverManager getInstance() {
        return SingletonContainer.instance;
    }

    private DataObserverManager() {
        observers = new Hashtable<String, Hashtable<String, DataObserver>>();
    }
}
