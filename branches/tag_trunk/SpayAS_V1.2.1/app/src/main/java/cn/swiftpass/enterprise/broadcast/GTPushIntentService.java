/**
 * Project Name:Spay_wftintl
 * File Name:GTIntentService.java
 * Package Name:cn.swiftpass.enterprise.broadcast
 * Date:2017-9-21上午10:07:35
 *
*/

package cn.swiftpass.enterprise.broadcast;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.WelcomeActivity;
import cn.swiftpass.enterprise.ui.activity.bill.BillMainActivity;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.TtsVoice;

import com.google.gson.Gson;
import com.igexin.sdk.GTIntentService;
import com.igexin.sdk.PushManager;
import com.igexin.sdk.message.FeedbackCmdMessage;
import com.igexin.sdk.message.GTCmdMessage;
import com.igexin.sdk.message.GTTransmitMessage;

import java.util.List;

/**
 * ClassName:GTIntentService
 * Function: TODO ADD FUNCTION.
 * Date:     2017-9-21 上午10:07:35 
 * @author   admin 
 */
public class GTPushIntentService extends GTIntentService
{
    private static final String TAG = "hehui";
    
    /**
     * 为了观察透传数据变化.
     */
    private static int cnt;
    
    public GTPushIntentService()
    {
        
    }
    
    @Override
    public void onReceiveServicePid(Context context, int pid)
    {
        Log.i(TAG, "onReceiveServicePid -> " + pid);
    }
    
    @Override
    public void onReceiveMessageData(Context context, GTTransmitMessage msg)
    {
        String appid = msg.getAppid();
        String taskid = msg.getTaskId();
        String messageid = msg.getMessageId();
        byte[] payload = msg.getPayload();
        String pkg = msg.getPkgName();
        String cid = msg.getClientId();
        
        // 第三方回执调用接口，actionid范围为90000-90999，可根据业务场景执行
        boolean result = PushManager.getInstance().sendFeedbackMessage(context, taskid, messageid, 90001);
        Log.i(TAG, "call sendFeedbackMessage = " + (result ? "success" : "failed"));
        
        Log.i(TAG, "onReceiveMessageData -> " + "appid = " + appid + "\ntaskid = " + taskid + "\nmessageid = "
            + messageid + "\npkg = " + pkg + "\ncid = " + cid);
        
        if (payload == null)
        {
            Log.e(TAG, "receiver payload = null");
        }
        else
        {
            String data = new String(payload);
            Log.i(TAG, "receiver payload = " + data);
            Gson gson = new Gson();
            PushTransmissionModel pushTransmissionModel = null;
            try
            {
                pushTransmissionModel = gson.fromJson(data, PushTransmissionModel.class);
            }
            catch (Exception e)
            {
                Log.e("hehui", "fromJson error", e);
            }
            if (pushTransmissionModel != null)
            {
                try
                {
                    String voice = PreferenceUtil.getString("voice", "open");
                    Log.i("hehui", "voice-->" + voice + ",data-->" + data + ",voice-->" + voice);
                    if (!StringUtil.isEmptyOrNull(data))
                    {
                        
                        if (voice.equalsIgnoreCase("open"))
                        {
                            if (pushTransmissionModel.getMessageKind() == PushTransmissionModel.MESSAGE_ORDER)
                            { //收款成功 语音播放
                                TtsVoice ttsVoice = new TtsVoice(context, pushTransmissionModel.getVoiceContent());
                                ttsVoice.startVoice();
                            }
                        }
                    }
                    
                }
                catch (Exception e)
                {
                    Log.e("hehui", "TtsVoice-->" + e);
                }
                
                showNotify(context, pushTransmissionModel);
            }
        }
        
        Log.d(TAG, "----------------------------------------------------------------------------------------------");
    }
    
    private void showNotify(Context context, PushTransmissionModel pushTransmissionModel)
    {
        Log.i("hehui", "showNotify-->");
        NotificationManager mNotificationManager =
            (NotificationManager)context.getSystemService(context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        //        Intent intent = new Intent(context, MainActivity.class);
        //        intent.putExtra("registPromptStr", "OrderDetailsActivity");
        //        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        //        mBuilder.setContentIntent(pendingIntent);

        if(!isBackground(context)){
            //如果当前APP在前台点击就跳转到账单页面、
            Intent intent = new Intent(context,BillMainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(context,0,intent,0);
            mBuilder.setContentIntent(pendingIntent);
        }else{
            //如果当前APP不在前台，点击消息通知就跳转到首页
            Intent intent = new Intent(context,WelcomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(context,0,intent,0);
            mBuilder.setContentIntent(pendingIntent);

        }


        mBuilder.setAutoCancel(true);
        mBuilder.setContentTitle(pushTransmissionModel.getTitle())// 设置通知栏标题
            .setContentText(pushTransmissionModel.getVoiceContent())
            .setOngoing(false)
            // ture，设置他为一个正在进行的通知。他们通常是用来表示一个后台任务,用户积极参与(如播放音乐)或以某种方式正在等待,因此占用设备(如一个文件下载,同步操作,主动网络连接)
            .setDefaults(Notification.DEFAULT_VIBRATE)
            // 向通知添加声音、闪灯和振动效果的最简单、最一致的方式是使用当前的用户默认设置，使用defaults属性，可以组合
            // Notification.DEFAULT_ALL Notification.DEFAULT_SOUND 添加声音 // requires VIBRATE
            // permission
            .setSmallIcon(R.drawable.icon_app_logo);// 设置通知小ICON
        
        mNotificationManager.notify(0, mBuilder.build());
    }

    //用来检测当前APP是否在前台运行
    public static boolean isBackground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.processName.equals(context.getPackageName())) {
                /*
                BACKGROUND=400 EMPTY=500 FOREGROUND=100
                GONE=1000 PERCEPTIBLE=130 SERVICE=300 ISIBLE=200
                 */
                Log.i(context.getPackageName(), "此appimportace ="
                        + appProcess.importance
                        + ",context.getClass().getName()="
                        + context.getClass().getName());
                if (appProcess.importance != ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    Log.i(context.getPackageName(), "处于后台" + appProcess.processName);
                    return true;
                } else {
                    Log.i(context.getPackageName(), "处于前台" + appProcess.processName);
                    return false;
                }
            }
        }
        return false;
    }
    
    @Override
    public void onReceiveClientId(Context context, String clientid)
    {
        Log.e(TAG, "onReceiveClientId -> " + "clientid = " + clientid);
        
    }
    
    @Override
    public void onReceiveOnlineState(Context context, boolean online)
    {
        Log.i(TAG, "onReceiveOnlineState -> " + (online ? "online" : "offline"));
    }
    
    @Override
    public void onReceiveCommandResult(Context context, GTCmdMessage cmdMessage)
    {
        Log.i(TAG, "onReceiveCommandResult -> " + cmdMessage);
        
        int action = cmdMessage.getAction();
        
    }
    
    private void feedbackResult(FeedbackCmdMessage feedbackCmdMsg)
    {
        String appid = feedbackCmdMsg.getAppid();
        String taskid = feedbackCmdMsg.getTaskId();
        String actionid = feedbackCmdMsg.getActionId();
        String result = feedbackCmdMsg.getResult();
        long timestamp = feedbackCmdMsg.getTimeStamp();
        String cid = feedbackCmdMsg.getClientId();
        
        Log.i(TAG, "onReceiveCommandResult -> " + "appid = " + appid + "\ntaskid = " + taskid + "\nactionid = "
            + actionid + "\nresult = " + result + "\ncid = " + cid + "\ntimestamp = " + timestamp);
    }
    
}
