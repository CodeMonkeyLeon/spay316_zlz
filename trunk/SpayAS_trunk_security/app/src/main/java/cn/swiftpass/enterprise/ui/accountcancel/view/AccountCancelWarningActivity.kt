package cn.swiftpass.enterprise.ui.accountcancel.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import cn.swiftpass.enterprise.bussiness.model.MerchantTempDataModel
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.mvp.contract.activity.AccountCancelWarningContract
import cn.swiftpass.enterprise.mvp.presenter.activity.AccountCancelWarningPresenter
import cn.swiftpass.enterprise.ui.activity.BaseActivity
import cn.swiftpass.enterprise.ui.widget.DialogInfo
import cn.swiftpass.enterprise.ui.widget.TitleBar
import cn.swiftpass.enterprise.utils.DialogHelper


/**
 * @author lizheng.zhao
 * @date 2022/06/16
 */
class AccountCancelWarningActivity : BaseActivity<AccountCancelWarningContract.Presenter>(),
    AccountCancelWarningContract.View {

    override fun createPresenter() = AccountCancelWarningPresenter()

    private fun getLayoutId() = R.layout.act_account_cancel_warning


    companion object {
        const val TAG = "AccountCancel"

        const val TEMP_STRING_EMAIL = "fa***@sample.com"

        fun startAccountCancelWarningActivity(fromActivity: Activity) {
            fromActivity.startActivity(
                Intent(
                    fromActivity,
                    AccountCancelWarningActivity::class.java
                )
            )
        }
    }


    private lateinit var mBtnNext: Button

    private var mEmail: String? = null


    override fun useToolBar() = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getEmailInfo()
    }


    override fun queryMerchantDataByTelSuccess(response: MerchantTempDataModel) {
        mEmail = response.email
        setContentView(getLayoutId())
        initView()
    }

    override fun queryMerchantDataByTelFailed(error: Any?) {
        toastDialog(activity, error.toString()) {
            finish()
        }
    }

    /**
     * 获取邮箱信息
     */
    private fun getEmailInfo() {
        mPresenter?.let { p ->
            p.queryMerchantDataByTel()
        }
    }


    private fun initView() {
        mBtnNext = findViewById(R.id.btn_next)
        mBtnNext.setOnClickListener {
            goToNextPage()
        }
    }

    private fun goToNextPage() {
        mEmail?.let {
            val msg = getString(R.string.string_account_cancel_warning_msg_dialog).replace(
                TEMP_STRING_EMAIL,
                it
            )

            val dialog = DialogInfo(
                activity,
                getString(R.string.public_cozy_prompt),
                msg,
                getString(R.string.string_account_cancel_warning_msg_dialog_ok),
                getString(R.string.btnCancel),
                DialogInfo.UNIFED_DIALOG,
                object : DialogInfo.HandleBtn {
                    override fun handleOkBtn() {
                        sendEmail()
                    }

                    override fun handleCancelBtn() {

                    }
                },
                null
            )
            DialogHelper.resize(activity, dialog)
            dialog.show()
        }
    }


    override fun sendEmailForAccountCancelSuccess(response: Boolean) {
        AccountCancelConfirmActivity.startAccountCancelConfirmActivity(activity)
    }

    override fun sendEmailForAccountCancelFailed(error: Any?) {
        toastDialog(activity, error.toString()) {
//                                //TODO --- 测试代码
//                                AccountCancelConfirmActivity.startAccountCancelConfirmActivity(
//                                    activity
//                                )
        }
    }

    private fun sendEmail() {
        mEmail?.let { email ->
            mPresenter?.let { p ->
                p.sendEmailForAccountCancel(email)
            }
        }
    }


    override fun setupTitleBar() {
        super.setupTitleBar()
        titleBar.setTitle(getString(R.string.string_cancel_account))
        titleBar.setLeftButtonVisible(true)
        titleBar.setOnTitleBarClickListener(object : TitleBar.OnTitleBarClickListener {
            override fun onLeftButtonClick() {
                finish()
            }

            override fun onRightButtonClick() {

            }

            override fun onRightLayClick() {

            }

            override fun onRightButLayClick() {

            }
        })
    }

}