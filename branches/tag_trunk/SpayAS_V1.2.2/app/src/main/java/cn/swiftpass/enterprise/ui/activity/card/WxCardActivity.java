/*
 * 文 件 名:  FindPassWordActivity.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-14
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.card;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.WxCard;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.user.VerificationDetails;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 微信卡券列表
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-14]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class WxCardActivity extends TemplateActivity
{
    private List<WxCard> vardList = new ArrayList<WxCard>();
    
    private ListView list1;
    
    public static void startActivity(Context context, List<WxCard> vardOrders)
    {
        Intent it = new Intent();
        it.setClass(context, WxCardActivity.class);
        it.putExtra("vardOrders", (Serializable)vardOrders);
        context.startActivity(it);
    }
    
    private ViewHolder holder;
    
    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wxcard_list);
        vardList = ((List<WxCard>)getIntent().getSerializableExtra("vardOrders"));
        initView();
        
    }
    
    private void initView()
    {
        list1 = getViewById(R.id.list1);
        list1.setAdapter(new WxCardActivityAdapter(vardList));
        
        list1.setOnItemClickListener(new OnItemClickListener()
        {
            
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
            {
                WxCard order = vardList.get(position);
                if (order == null)
                {
                    return;
                }
                OrderManager.getInstance().queryVardDetail(order.getCardId(),
                    order.getCardCode(),
                    new UINotifyListener<WxCard>()
                    {
                        
                        @Override
                        public void onError(Object object)
                        {
                            super.onError(object);
                            dismissLoading();
                            if (object != null)
                            {
                                //                                showToastInfo(object.toString());
                                toastDialog(WxCardActivity.this, object.toString(), null);
                            }
                        }
                        
                        @Override
                        public void onPreExecute()
                        {
                            super.onPreExecute();
                            
                            loadDialog(WxCardActivity.this, R.string.public_data_loading);
                        }
                        
                        @Override
                        public void onSucceed(WxCard result)
                        {
                            super.onSucceed(result);
                            dismissLoading();
                            if (result != null)
                            {
                                //                                result.setUseTime(order.getUseTime());
                                //                                result.setCardCode(order.getCardCode());
                                //                            result.setDealDetail(order.getDealDetail());
                                VerificationDetails.startActivity(WxCardActivity.this, result);
                            }
                        }
                        
                    });
            }
        });
    }
    
    private class WxCardActivityAdapter extends BaseAdapter
    {
        private List<WxCard> vardList;
        
        public WxCardActivityAdapter(List<WxCard> vardList)
        {
            this.vardList = vardList;
        }
        
        @Override
        public int getCount()
        {
            // TODO Auto-generated method stub
            return vardList.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            // TODO Auto-generated method stub
            return vardList.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            // TODO Auto-generated method stub
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(WxCardActivity.this, R.layout.activity_wxcard_list_item, null);
                holder = new ViewHolder();
                holder.tv_card_title = (TextView)convertView.findViewById(R.id.tv_card_title);
                holder.tv_time = (TextView)convertView.findViewById(R.id.tv_time);
                holder.iv_state = (ImageView)convertView.findViewById(R.id.iv_state);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            
            WxCard wxCard = vardList.get(position);
            if (wxCard != null)
            {
                holder.tv_card_title.setText(wxCard.getTitle());
                if (!StringUtil.isEmptyOrNull(wxCard.getBeginTimestamp())
                    && !StringUtil.isEmptyOrNull(wxCard.getEndTimestamp()))
                {
                    
                    holder.tv_time.setText(getString(R.string.bt_card_time) + ":"
                        + DateUtil.formatTime(Long.parseLong(wxCard.getBeginTimestamp())) + " "
                        + getString(R.string.tv_least) + DateUtil.formatTime(Long.parseLong(wxCard.getEndTimestamp())));
                }
                
                if (wxCard.getCardType().equalsIgnoreCase("groupon"))
                {
                    holder.iv_state.setImageResource(R.drawable.coupon_grouppurchasing);
                }
                else if (wxCard.getCardType().equalsIgnoreCase("gift"))
                {
                    holder.iv_state.setImageResource(R.drawable.coupon_gift);
                }
                else if (wxCard.getCardType().equalsIgnoreCase("cash"))
                {
                    holder.iv_state.setImageResource(R.drawable.coupon_voucher);
                }
                else
                {
                    holder.iv_state.setImageResource(R.drawable.coupon_discount);
                }
            }
            
            return convertView;
        }
        
    }
    
    private class ViewHolder
    {
        private TextView tv_card_title, tv_time;
        
        private ImageView iv_state;
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_coupon);
    }
    
}
