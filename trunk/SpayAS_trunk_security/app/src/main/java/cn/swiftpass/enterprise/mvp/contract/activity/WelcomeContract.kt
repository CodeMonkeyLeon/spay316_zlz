package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.*
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/11/17
 *
 * @沉默
 * @像一朵傍晚的云
 */
class WelcomeContract {


    interface View : BaseView {

        fun ecdhKeyExchangeSuccess(
            response: ECDHInfo?,
            isInIt: Boolean,
            privateKey: String?
        )

        fun ecdhKeyExchangeFailed(
            error: Any?,
            isInIt: Boolean
        )

        fun getCodeSuccess(response: String?)

        fun getCodeFailed(error: Any?)

        fun loginAsyncSuccess(
            response: Boolean,
            password: String?
        )

        fun loginAsyncFailed(error: Any?)

        fun queryWalletListSuccess(response: List<WalletListBean>?)

        fun queryWalletListFailed(error: Any?)

        fun getCertificateStringSuccess(
            response: CertificateBean?,
            isDeviceLogin: Boolean
        )

        fun getCertificateStringFailed(
            error: Any?,
            isDeviceLogin: Boolean
        )

        fun deviceLoginSuccess(response: Boolean)

        fun deviceLoginFailed(error: Any?)

        fun apiShowListSuccess(response: ArrayList<DynModel>?)

        fun apiShowListFailed(error: Any?)

        fun getVersionCodeSuccess(response: UpgradeInfo?)

        fun getVersionCodeFailed(error: Any?)

    }


    interface Presenter : BasePresenter<View> {

        fun getVersionCode()


        fun apiShowList()


        fun deviceLogin()


        /**
         *  CA证书公钥获取接口
         * 请求CA证书公钥获取接口得到最新CA证书公钥
         * 获取证书，使用旧的签名方式（签名Key为时间戳spayRs）
         */
        fun getCertificateString(isDeviceLogin: Boolean)


        /**
         * 聚合通道支持钱包列表
         * 查询聚合通道支持的钱包类型
         */
        fun queryWalletList(serviceType: String?)


        /**
         *  0.登录成功
         *  1.商户待审核
         *  2.商户初审通过
         *  3.商户初审不通过
         *  5.商户终审不通过
         *  6.商户失效或过期
         *  7.商户不存在
         *  8.密码错误
         */
        fun loginAsync(
            code: String?,
            username: String?,
            password: String?
        )


        fun getCode()


        /**
         * 客户端和服务器公钥交换的接口
         */
        fun ecdhKeyExchange(
            publicKey: String?,
            isNeedRequestECDHKey: Boolean,
            isInIt: Boolean,
            privateKey: String?
        )
    }


}