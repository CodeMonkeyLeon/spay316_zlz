package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.TipsBean
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView


/**
 * @author lizheng.zhao
 * @date 2022/12/2
 *
 * @真诚与热爱
 * @是我永不放弃的品质
 */
class TipsEditContract {

    interface View : BaseView {


        fun submitTipsSettingSuccess(response: Boolean)

        fun submitTipsSettingFailed(error: Any?)
    }


    interface Presenter : BasePresenter<View> {

        fun submitTipsSetting(tipsList: ArrayList<TipsBean>)

    }
}