package cn.swiftpass.enterprise.mvp.presenter.activity

import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.OrderCreateContract
import cn.swiftpass.enterprise.ui.paymentlink.model.OrderConfig
import cn.swiftpass.enterprise.utils.JsonUtil
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/11/23
 *
 * @一生中
 * @我多次撒谎
 * @却始终诚实地遵守着
 * @一个儿时的诺言
 * @因此
 * @那与孩子的心
 * @不能相容的世界
 * @再也没有饶恕过我
 */
class OrderCreatePresenter : OrderCreateContract.Presenter {

    private var mView: OrderCreateContract.View? = null


    override fun getOrderConfig() {
        mView?.let { view ->
            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)
            AppClient.getOrderConfig(
                "2",
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<OrderConfig>(OrderConfig()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.getOrderConfigFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                OrderConfig::class.java
                            ) as OrderConfig
                            view.getOrderConfigSuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: OrderCreateContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}