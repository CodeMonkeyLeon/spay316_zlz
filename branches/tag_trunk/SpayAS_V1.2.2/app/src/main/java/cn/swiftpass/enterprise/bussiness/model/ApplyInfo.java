/*
 * 文 件 名:  ApplyInfo.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2014-8-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.model;

/**
 * 我要申请
 * 
 * @author  he_hui
 * @version  [版本号, 2014-8-11]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class ApplyInfo
{
    private String Name;
    
    private String phone;
    
    private String city;
    
    private String Company;
    
    /**
     * @return 返回 name
     */
    public String getName()
    {
        return Name;
    }
    
    /**
     * @param 对name进行赋值
     */
    public void setName(String name)
    {
        Name = name;
    }
    
    /**
     * @return 返回 phone
     */
    public String getPhone()
    {
        return phone;
    }
    
    /**
     * @param 对phone进行赋值
     */
    public void setPhone(String phone)
    {
        this.phone = phone;
    }
    
    /**
     * @return 返回 city
     */
    public String getCity()
    {
        return city;
    }
    
    /**
     * @param 对city进行赋值
     */
    public void setCity(String city)
    {
        this.city = city;
    }
    
    /**
     * @return 返回 company
     */
    public String getCompany()
    {
        return Company;
    }
    
    /**
     * @param 对company进行赋值
     */
    public void setCompany(String company)
    {
        Company = company;
    }
}
