package cn.swiftpass.enterprise.ui.activity.scan;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.tencent.stat.StatService;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.Timer;
import java.util.Vector;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.broadcast.GTPushIntentService;
import cn.swiftpass.enterprise.broadcast.PushTransmissionModel;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.Camera.CameraManager;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.Decoding.CaptureActivityHandler;
import cn.swiftpass.enterprise.bussiness.logica.Zxing.Decoding.InactivityTimer;
import cn.swiftpass.enterprise.bussiness.logica.order.OrderManager;
import cn.swiftpass.enterprise.bussiness.logica.refund.RefundManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.DynModel;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.QRcodeInfo;
import cn.swiftpass.enterprise.bussiness.model.WxCard;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.activity.NoteMarkActivity;
import cn.swiftpass.enterprise.ui.activity.OrderDetailsActivity;
import cn.swiftpass.enterprise.ui.activity.PayResultActivity;
import cn.swiftpass.enterprise.ui.activity.PreAutFinishActivity;
import cn.swiftpass.enterprise.ui.activity.PreAuthActivity;
import cn.swiftpass.enterprise.ui.activity.ShowQRcodeActivity;
import cn.swiftpass.enterprise.ui.widget.ButtonM;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.NewDialogInfo;
import cn.swiftpass.enterprise.ui.widget.PayTypeDialog;
import cn.swiftpass.enterprise.ui.widget.ProgressInfoDialog;
import cn.swiftpass.enterprise.ui.widget.RefundCheckDialog;
import cn.swiftpass.enterprise.ui.widget.ScanCodeCheckDialog;
import cn.swiftpass.enterprise.ui.widget.WxCardDialog;
import cn.swiftpass.enterprise.ui.widget.Zxing.ViewfinderView;
import cn.swiftpass.enterprise.ui.widget.dialog.ScanImputDialog;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.DisplayUtil;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;
import cn.swiftpass.enterprise.utils.Utils;

import static cn.swiftpass.enterprise.MainApplication.IS_POS_VERSION;

/**
 * 扫描2code
 *
 * @author Alan
 */
public class CaptureActivity extends BaseActivity implements Callback, View.OnClickListener {

    private static final String TAG = CaptureActivity.class.getSimpleName();

    private CaptureActivityHandler handler;

    private ViewfinderView viewfinderView;

    private boolean hasSurface;

    private Vector<BarcodeFormat> decodeFormats;

    private String characterSet;

    private TextView txtResult, tv_money,tv_surcharge;


    private InactivityTimer inactivityTimer;

    private MediaPlayer mediaPlayer;

    private boolean playBeep;

    private ScanCodeCheckDialog dialog;

    private static final float BEEP_VOLUME = 0.10f;


    private boolean vibrate;

    private String money, payType, strSurchargelMoney;


    private Context mContext;

    private Timer timer;

    private String orderNo;

    private String outTradeNo;

    private Handler mHandler;

    private DialogInfo showDialog;


    private LinearLayout money_lay, ly_back;

    long totalFee;

    List<String> vard = new ArrayList<String>();

    private List<String> payMeths = new ArrayList<String>();

    List<WxCard> vardOrders = new ArrayList<WxCard>();

    private List<WxCard> vardList = new ArrayList<WxCard>();

    long discountAmount; // 优惠金额

    public static final String FLAG = "NATIVE.SHOWQRCODE.ACTIVITY";

    private String fTag = "";

    private String tradeType = null, payOutTradeNo;

    private Integer isMark = 0;

    private DialogInfo dialogs;

    private TextView tv_code_info;

    private LinearLayout  ly_switch;

    private TextView tv_switch_code, tv_input_code, tv_pase;

    private SurfaceHolder surfaceHolder;

    SurfaceView surfaceView;

    static final String[] PERMISSIONS = new String[]{Manifest.permission.CAMERA};
    //    /**
//     * 判断是否需要检测，防止不停的弹框
//     */
    private boolean isNeedCheck = true;
    private LinearLayout has_fee_layout;
    private String mark = "";
    private PushTransmissionModel pushTransmissionModel;
    double minus = 1;
    private boolean isPreAuth =false;
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_capture_new);
        ly_back = getViewById(R.id.ly_back);
        MainApplication.listActivities.add(this);
        payType = getIntent().getStringExtra("payType");
        mark = getIntent().getStringExtra("mark");
        //popUtils = new MyPopupWindowUtils(this, null);
        timer = new Timer();
        CameraManager.init(this);

        viewfinderView = (ViewfinderView) findViewById(R.id.viewfinder_view);
        //preview_top_view = getViewById(R.id.preview_top_view);
        hasSurface = false;
        inactivityTimer = new InactivityTimer(this);

        //根据当前的小数点的位数来判断应该除以多少
        for(int i = 0 ; i < MainApplication.numFixed ; i++ ){
            minus = minus * 10;
        }

        pushTransmissionModel = new PushTransmissionModel();
        tv_pase = getViewById(R.id.tv_pase);
        ly_switch = getViewById(R.id.ly_switch);

        tv_code_info = getViewById(R.id.tv_code_info);
        tv_code_info.setPadding(0, 0, 0, DisplayUtil.dip2Px(CaptureActivity.this, 20));

        mHandler = new Handler();

        money_lay = getViewById(R.id.money_lay);
        tv_switch_code = getViewById(R.id.tv_switch_code);
        tv_input_code = getViewById(R.id.tv_input_code);
        setLister();
        tv_input_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    StatService.trackCustomEvent(mContext, "kMTASPayPayInputCode", "输入付款码");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
                ScanImputDialog scanImputDialog = new ScanImputDialog(CaptureActivity.this, new ScanImputDialog.ConfirmListener() {

                    @Override
                    public void ok(String code) {
                        submitData(code, false);
                    }
                });
                DialogHelper.resize(CaptureActivity.this, scanImputDialog);
                scanImputDialog.show();
            }
        });

        tv_switch_code.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //判断是否开通一种支付类型
                try {
                    StatService.trackCustomEvent(mContext, "kMTASPayPaySwitchQRCode", "切换扫码按钮");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }
                String payType = isOpenFisrtpayType();
                if (!StringUtil.isEmptyOrNull(payType)) {
                    toNewPay(money, payType);
                    return;
                }

                PayTypeDialog dialogPay = new PayTypeDialog(CaptureActivity.this, money,new PayTypeDialog.HandleItemBtn() {

                    @Override
                    public void toPay(String money, String payType) {
                        toNewPay(money, payType);
                    }
                });
                DialogHelper.resize(CaptureActivity.this, dialogPay);
                dialogPay.show();
            }
        });
    }

    private void setLister() {
        ly_back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * 生成二维码
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void toNewPay(String money, final String payType) {
        OrderManager.getInstance().unifiedNativePay(money, payType, discountAmount, vardList, null,mark, new UINotifyListener<QRcodeInfo>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
                loadDialog(CaptureActivity.this, R.string.tv_pay_prompt);
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onError(final Object object) {
                super.onError(object);
                dismissLoading();
                if (checkSession()) {
                    return;
                }

                if (object != null && !TextUtils.isEmpty(object.toString())) {
                    CaptureActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toastDialog(CaptureActivity.this, object.toString(), null);
                        }
                    });
                }else{
                    toastDialog(CaptureActivity.this, getStringById(R.string.generate_code_failed), null);
                }

            }

            @Override
            public void onSucceed(QRcodeInfo result) {
                super.onSucceed(result);
                dismissLoading();
                if (result != null) {
                    result.setSurcharge(strSurchargelMoney);
                    result.setPayType(payType);
//                    ShowQRcodeActivity.startActivity(result, CaptureActivity.this,mark);

                    ShowQRcodeActivity.startActivity(result, CaptureActivity.this);
                }
            }
        });
    }


    public static void startActivity(Context context, String payType,String mark) {
        Intent it = new Intent();
        it.setClass(context, CaptureActivity.class);
        it.putExtra("payType", payType);
        it.putExtra("mark", mark);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
        context.startActivity(it);
    }

    public static void startActivity(Context context, String payType) {
        Intent it = new Intent();
        it.setClass(context, CaptureActivity.class);
        it.putExtra("payType", payType);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
        context.startActivity(it);
    }

    public static void startActivity(Context context, String payType, String flag, String money) {
        Intent it = new Intent();
        it.setClass(context, CaptureActivity.class);
        it.putExtra("payType", payType);
        it.putExtra("flag", flag);
        it.putExtra("money", money);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
        context.startActivity(it);
    }

    public static void startActivitys(Context context, String outTradeNo, String money, String tradeType, Integer isMark) {
        Intent it = new Intent();
        it.setClass(context, CaptureActivity.class);
        it.putExtra("outTradeNo", outTradeNo);
        it.putExtra("money", money);
        it.putExtra("tradeType", tradeType);
        it.putExtra("isMark", isMark);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
        context.startActivity(it);
    }

    /**
     * 申请权限结果的回调方法
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] paramArrayOfInt) {
        if (requestCode == PERMISSON_REQUESTCODE) {
            if (!verifyPermissions(paramArrayOfInt)) {
                showMissingPermissionDialog(CaptureActivity.this);
                isNeedCheck = false;
            }
        }
    }

    /**
     * 显示提示信息
     *
     * @since 2.5.0
     */
    protected void showMissingPermissionDialog(Context context) {
        DialogInfo dialogInfo = new DialogInfo(context, null, getString(R.string.setting_permisson_camera), getStringById(R.string.title_setting), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                startAppSettings();
            }

            @Override
            public void handleCancleBtn() {
                finish();
            }
        }, null);
        dialogInfo.setOnKeyListener(new DialogInterface.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                if (keycode == KeyEvent.KEYCODE_BACK) {
                    return true;
                }
                return false;
            }
        });
        DialogHelper.resize(context, dialogInfo);
        dialogInfo.show();
    }

    String diaglTitle = ToastHelper.toStr(R.string.tx_other_code);

    @SuppressWarnings("unchecked")
    @Override
    protected void onResume() {
        super.onResume();
        if (isNeedCheck && Build.VERSION.SDK_INT >= 23) {
            checkPermissions(PERMISSIONS);
        }
        txtResult = (TextView) findViewById(R.id.txtResult);
        txtResult.setText(MainApplication.getFeeFh());
        tv_money = getViewById(R.id.tv_money);
        //额外费用
        tv_surcharge = getViewById(R.id.surcharge);
        has_fee_layout = getViewById(R.id.has_fee_layout);
        fTag = getIntent().getStringExtra("flag");

        payOutTradeNo = getIntent().getStringExtra("outTradeNo");
        isMark = getIntent().getIntExtra("isMark", 0);
        if (null != getIntent().getSerializableExtra("vardOrders")) {
            vardList = ((List<WxCard>) getIntent().getSerializableExtra("vardOrders"));
        }

        if (!isAbsoluteNullStr(payType) && payType.equals(MainApplication.PAY_TYPE_REFUND)) {
            txtResult.setVisibility(View.GONE);
            tv_money.setVisibility(View.GONE);
            //新增
            has_fee_layout.setVisibility(View.INVISIBLE);

        } else if (!isAbsoluteNullStr(payType) && payType.equals(MainApplication.PAY_TYPE_MICROPAY_VCARD)) { //

            vardOrders = vardList;

            money_lay.setVisibility(View.VISIBLE);
            money = getIntent().getStringExtra("money");
            double d = Double.parseDouble(money);
            money = (long) d + "";
            discountAmount = getIntent().getLongExtra("discountAmount", 0);

            tv_money.setText(getString(R.string.tx_real_money) + "：");
            //新增
            has_fee_layout.setVisibility(View.VISIBLE);
//            tv_surcharge.setText();
            ly_switch.setVisibility(View.VISIBLE);
//            if (totalvalues.length() > 10){
//                tv_total.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
//            }else{
//                tv_total.setTextSize(TypedValue.COMPLEX_UNIT_SP, 48);
//            }
            if (!TextUtils.isEmpty(money)) {
                changeTextSize(money);
            }
        } else if (!isAbsoluteNullStr(fTag) && fTag.equalsIgnoreCase(FLAG)) { // 从二维码界面调转进来
            money = getIntent().getStringExtra("money");
            tag = false;
            money_lay.setVisibility(View.VISIBLE);
            ly_switch.setVisibility(View.VISIBLE);
            changeTextSize(money);
            BigDecimal bigDecimal = new BigDecimal(Long.parseLong(money) / minus);
            paseRMB(bigDecimal, tv_pase);

        } else if (!isAbsoluteNullStr(payType) && payType.equalsIgnoreCase(MainApplication.PAY_TYPE_SCAN_OPNE)) { // 从固定二维码，扫码激活
            //            titleBar.setRightButLayVisible(false, R.string.tx_mic_help);
            txtResult.setVisibility(View.GONE);
            tv_money.setVisibility(View.GONE);
            //新增
            has_fee_layout.setVisibility(View.INVISIBLE);
        } else {
            diaglTitle = getString(R.string.tx_other_code);
            money = getIntent().getStringExtra("money");
            strSurchargelMoney = getIntent().getStringExtra("surcharge");
            if (!StringUtil.isEmptyOrNull(money)) {
                if (MainApplication.isSurchargeOpen()) {
                    if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth","sale"),"pre_auth")){
                        tv_money.setVisibility(View.GONE);
                        has_fee_layout.setVisibility(View.INVISIBLE);

                        //如果是预授权。则手动不计算surcharge
                        strSurchargelMoney = "0";
                    }else{
                        has_fee_layout.setVisibility(View.VISIBLE);
                        tv_money.setVisibility(View.VISIBLE);
                    }
                } else {
                    tv_money.setVisibility(View.GONE);
                    has_fee_layout.setVisibility(View.INVISIBLE);
                }

                money_lay.setVisibility(View.VISIBLE);
                ly_switch.setVisibility(View.VISIBLE);
                BigDecimal bigDecimal = null;
                BigDecimal bigSurcharge = null;
                if (TextUtils.isEmpty(strSurchargelMoney)) {
                    changeTextSize(money);
                    bigSurcharge = new BigDecimal(0);
                } else {
                    String mon = MainApplication.getFeeFh() + DateUtil.formatMoneyUtil((Long.parseLong(money) + Long.parseLong(strSurchargelMoney)) / minus);
                    changeTextSize(mon);
                    bigSurcharge = new BigDecimal(Long.parseLong(strSurchargelMoney) / minus);
                }

                bigDecimal = new BigDecimal(Long.parseLong(money) / minus);
                paseRMBWithSurcharge(bigDecimal, bigSurcharge);
            } else {

                ly_switch.setVisibility(View.INVISIBLE);
                money_lay.setVisibility(View.GONE);
                tv_code_info.setText(R.string.tv_scan_prompt);
            }
        }

        if (isMark == 1) { //不能再使用卡券
            tradeType = getIntent().getStringExtra("tradeType");
        }

        surfaceView = (SurfaceView) findViewById(R.id.preview_view);
        surfaceHolder = surfaceView.getHolder();

        if (hasSurface) {
            initCamera(surfaceHolder, false);
        } else {
            surfaceHolder.addCallback(this);
            surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }
        decodeFormats = null;
        characterSet = null;

        playBeep = true;
        AudioManager audioService = (AudioManager) getSystemService(AUDIO_SERVICE);
        if (audioService.getRingerMode() != AudioManager.RINGER_MODE_NORMAL) {
            playBeep = false;
        }
        initBeepSound();
        vibrate = true;
    }

    private void paseRMBWithSurcharge(BigDecimal bigDecimal, BigDecimal bigSurcharge) {
        try {

            if (!MainApplication.getFeeType().equalsIgnoreCase("CNY")) {
                tv_pase.setVisibility(View.GONE);
                BigDecimal totalCharge = null;
                if (MainApplication.getSourceToUsdExchangeRate() > 0 && MainApplication.getUsdToRmbExchangeRate() > 0) {
                    BigDecimal sourceToUsd = new BigDecimal(MainApplication.getSourceToUsdExchangeRate());
                    BigDecimal usdRate = new BigDecimal(MainApplication.getUsdToRmbExchangeRate());
                    totalCharge = bigSurcharge.add(bigDecimal).multiply(sourceToUsd).setScale(MainApplication.numFixed, BigDecimal.ROUND_HALF_UP).
                            multiply(usdRate).setScale(MainApplication.numFixed, BigDecimal.ROUND_DOWN);
                } else {
                    BigDecimal rate = new BigDecimal(MainApplication.getExchangeRate());
                    totalCharge = bigSurcharge.add(bigDecimal).multiply(rate).setScale(MainApplication.numFixed, BigDecimal.ROUND_FLOOR);
                }
//                String totalStr = "(" + getString(R.string.tx_bill_stream_pay_money) + ":" + DateUtil.formatPaseMoney(bigDecimal) + "," + getString(R.string.tx_surcharge) + ":" + DateUtil.formatPaseMoney(bigSurcharge) + ")";
                String totalStr =  DateUtil.formatPaseMoney(bigDecimal);
                tv_money.setText(MainApplication.feeFh+totalStr);
                tv_surcharge.setText(MainApplication.feeFh+DateUtil.formatPaseMoney(bigSurcharge));
                tv_pase.setText(getString(R.string.tx_about) + getString(R.string.tx_mark) + DateUtil.formatPaseRMBMoney(totalCharge));
            } else {
                tv_pase.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Logger.e("hehui", "paseRMB-->" + e);
        }
    }

    private void changeTextSize(String mt){
        String totalvalues = mt;
        if (totalvalues.length() > 12){
            txtResult.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
        }else{
            txtResult.setTextSize(TypedValue.COMPLEX_UNIT_SP, 40);
        }
        txtResult.setText(totalvalues);

    }

    @Override
    public boolean onKeyDown(int keycode, KeyEvent event) {
        if (keycode == KeyEvent.KEYCODE_BACK) {
            if (null != payType && !"".equals(payType)) {
                if (payType.equals(MainApplication.PAY_TYPE_WRITE_OFF) || payType.equals(MainApplication.PAY_TYPE_REFUND) || (!isAbsoluteNullStr(fTag)) || payType.equals(MainApplication.PAY_TYPE_SCAN_OPNE)) {

                    if (!isAbsoluteNullStr(fTag) && vardOrders.size() > 0) {

                        finish();

                    } else {
                        finish();
                    }
                } else {

                    finish();
                }
                return false;
            } else {
                finish();

                return true;
            }

        } else {
            return false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
        CameraManager.get().closeDriver();
        if (!hasSurface) {
            SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
            SurfaceHolder surfaceHolder = surfaceView.getHolder();
            surfaceHolder.removeCallback(this);
        }
    }

    void closeCamera() {
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
    }

    @Override
    protected void onDestroy() {
        inactivityTimer.shutdown();
        super.onDestroy();
    }

    void restartCamera() {
        closeCamera();
        viewfinderView.setVisibility(View.VISIBLE);
        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
        SurfaceHolder surfaceHolder = surfaceView.getHolder();
        initCamera(surfaceHolder, false);
    }

    private void initCamera(SurfaceHolder surfaceHolder, boolean isFirst) {
        try {
            CameraManager.get().openDriver(surfaceHolder, surfaceView);
        } catch (IOException ioe) {
            return;
        } catch (RuntimeException e) {
            return;
        }
        if (handler == null) {
            handler = new CaptureActivityHandler(this, decodeFormats, characterSet);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (!hasSurface) {
            hasSurface = true;
            initCamera(holder, false);
        }

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        hasSurface = false;

    }

    public ViewfinderView getViewfinderView() {
        return viewfinderView;
    }

    public Handler getHandler() {
        return handler;
    }

    public void drawViewfinder() {
        viewfinderView.drawViewfinder();
    }

    public void handleDecode(Result obj, Bitmap barcode) {
        inactivityTimer.onActivity();
        //        viewfinderView.drawResultBitmap(barcode);
        playBeepSoundAndVibrate();

        AlertDialog.Builder builder = new AlertDialog.Builder(CaptureActivity.this);
        builder.setTitle(getString(R.string.public_cozy_prompt));
        builder.setPositiveButton(getString(R.string.btnOk), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                restartCamera();
            }
        });

        builder.setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.show();
        //txtResult.setText(obj.getBarcodeFormat().toString() + ":" + obj.getText());
    }

    private void initBeepSound() {
        if (playBeep && mediaPlayer == null) {
            // The volume on STREAM_SYSTEM is not adjustable, and users found it
            // too loud,
            // so we now play on the music stream.
            setVolumeControlStream(AudioManager.STREAM_MUSIC);
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setOnCompletionListener(beepListener);

            AssetFileDescriptor file = getResources().openRawResourceFd(R.raw.beep);
            try {
                mediaPlayer.setDataSource(file.getFileDescriptor(), file.getStartOffset(), file.getLength());
                file.close();
                mediaPlayer.setVolume(BEEP_VOLUME, BEEP_VOLUME);
                mediaPlayer.prepare();
            } catch (IOException e) {
                mediaPlayer = null;
            }
        }
    }

    private static final long VIBRATE_DURATION = 200L;

    private void playBeepSoundAndVibrate() {
        if (playBeep && mediaPlayer != null) {
            mediaPlayer.start();
        }
        if (vibrate) {
            Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
            vibrator.vibrate(VIBRATE_DURATION);
        }
    }

    /**
     * When the beep has finished playing, rewind to queue up another one.
     */
    private final OnCompletionListener beepListener = new OnCompletionListener() {
        public void onCompletion(MediaPlayer mediaPlayer) {
            mediaPlayer.seekTo(0);
        }
    };

    @Override
    protected boolean isLoginRequired() {
        // TODO Auto-generated method stub
        return true;
    }


    String title = ToastHelper.toStr(R.string.tx_pay_loading);

    protected Dialog dialog3;

    // 到spay 服务端查询订单
    private void queryFormServer(final String orderNo) {
        OrderManager.getInstance().queryOrderDetail(orderNo, MainApplication.getMchId(), false, new UINotifyListener<Order>() {

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(final Order result) {
                dismissLoading();
                if (result != null) {
                    outTradeNo = result.getOutTradeNo();
                    totalFee = result.money;
                    OrderDetailsActivity.startActivity(CaptureActivity.this, result);
                    finish();
                } else {
                    toastDialog(CaptureActivity.this, R.string.show_order_no_find, new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            finish();
                        }
                    });
                }
                super.onSucceed(result);
            }

            @Override
            public void onPreExecute() {
                super.onPreExecute();
                //                    DialogHelper.resize(CaptureActivity.this, dialog1);
                //                    dialog1.show();
                loadDialog(CaptureActivity.this, R.string.tx_query_loading);
            }

            @Override
            public void onError(Object object) {
                super.onError(object);
                dismissLoading();
                if (checkSession()) {
                    return;
                }

                if (object != null && !TextUtils.isEmpty(object.toString())) {

                    toastDialog(CaptureActivity.this, object.toString(), new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            finish();
                        }
                    });
                }else{
                    toastDialog(CaptureActivity.this, R.string.show_order_no_find, new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            finish();
                        }
                    });
                }
            }
        });
    }

    // 到spay 服务端查询订单===包含预授权的情形
    private void queryFormServerWithPreAuth(final String orderNo) {

        final int scanType = 1;//先查询订单列表
        OrderManager.getInstance().ScanQueryOrderDetail(orderNo, MainApplication.getMchId(), scanType ,new UINotifyListener<Order>() {

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(final Order result) {
                dismissLoading();
                if (result != null) {
                    outTradeNo = result.getOutTradeNo();
                    totalFee = result.money;
                    OrderDetailsActivity.startActivity(CaptureActivity.this, result);
                    finish();
                }
                super.onSucceed(result);
            }

            @Override
            public void onPreExecute() {
                super.onPreExecute();
                loadDialog(CaptureActivity.this, R.string.tx_query_loading);
            }

            @Override
            public void onError(Object object) {
                super.onError(object);
                if (checkSession()) {
                    dismissLoading();
                    return;
                }

                if(scanType == 1 ){
                    scanPreAuthOrder(orderNo);
                }

            }
        });
    }

    public void scanPreAuthOrder(final String orderNo){
        OrderManager.getInstance().ScanQueryOrderDetail(orderNo, MainApplication.getMchId(), 2 ,new UINotifyListener<Order>() {

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(final Order result) {
                dismissLoading();
                if (result != null) {
                    PreAuthActivity.startActivity(CaptureActivity.this,result,2);//预授权解冻 参数1
                    finish();
                } else {
                    toastDialog(CaptureActivity.this, R.string.show_order_no_find, new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            finish();
                        }
                    });
                }
                super.onSucceed(result);
            }

            @Override
            public void onPreExecute() {
                super.onPreExecute();
                loadDialog(CaptureActivity.this, R.string.tx_query_loading);
            }

            @Override
            public void onError(Object object) {
                super.onError(object);
                dismissLoading();
                if (checkSession()) {
                    return;
                }
                if (object != null && !TextUtils.isEmpty(object.toString())) {

                    toastDialog(CaptureActivity.this, object.toString(), new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            finish();
                        }
                    });
                }else{
                    toastDialog(CaptureActivity.this, R.string.show_order_no_find, new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            finish();
                        }
                    });
                }
            }
        });
    }


    public void submitData(final String code, boolean vibration) {

        if (vibration) {
            playBeepSoundAndVibrate();
        }

        //扫码查单
        if(payType.equals("pay")){
            // 查询该笔订单是否存在
            //如果开通预授权，则走新的逻辑
            if(MainApplication.isPre_authOpen == 1 ){
                queryFormServerWithPreAuth(code);
            }else{
                queryFormServer(code);
            }
            return;
        }else{
            if (code.startsWith(MainApplication.getMchId())) {
                //如果开通预授权，则走新的逻辑
                if(MainApplication.isPre_authOpen == 1 ){
                    queryFormServerWithPreAuth(code);
                }else{
                    queryFormServer(code);
                }
                return;
            } else if (code.startsWith("http") && code.contains("qrId")) {
                //扫码激活
                ActivateScanOneActivity.startActivity(CaptureActivity.this, code);
                return;
            } else if (code.startsWith("spay://")) {
                //扫码登录
                String c = code.substring(code.lastIndexOf("=") + 1);
//            ScanCodeLoginActivity.startActivity(CaptureActivity.this, c);
                return;
            } else {
                OrderManager.getInstance().doCoverOrder(code, money, tradeType, vardList, discountAmount, payOutTradeNo, mark,new UINotifyListener<QRcodeInfo>() {
                    @Override
                    public void onError(final Object object) {
                        super.onError(object);
                        dismissLoading();
                        if (checkSession()) {
                            return;
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //String mmsg = null;
                                if (object != null && object.toString().contains("SPAY")) {
                                    String out_order_no = object.toString().substring(0, object.toString().length() - 4);
                                    queryOrderGetStuts(out_order_no, getString(R.string.confirming_waiting), false, false);
                                } else if ("20".equals(object.toString())) {
                                    //mmsg = "单笔限额已经超出";
                                    //                                    ToastHelper.showError(getString(R.string.single_limit_exceeded));
                                    toastDialog(CaptureActivity.this, R.string.single_limit_exceeded, new NewDialogInfo.HandleBtn() {

                                        @Override
                                        public void handleOkBtn() {
                                            restartCamera();
                                        }

                                    });

                                } else {
                                    toastDialog(CaptureActivity.this, object.toString(), new NewDialogInfo.HandleBtn() {

                                        @Override
                                        public void handleOkBtn() {
                                            finish();
                                        }

                                    });
                                }
                            }
                        });

                    }

                    @Override
                    public void onPreExecute() {
                        super.onPreExecute();
                        loadDialog(CaptureActivity.this, title);

                    }

                    @Override
                    public void onPostExecute() {
                        super.onPostExecute();
                    }

                    @Override
                    public void onSucceed(QRcodeInfo result) {
                        super.onSucceed(result);
                        if (result != null) {
                            if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth","sale"),"pre_auth")){
                                orderNo = result.outAuthNo;
                            }else{
                                orderNo = result.orderNo;
                            }

                            //查询订单获取状态 定时任务查询订单状态,状态不为2 才轮询
                            if (result.order.state != null && !TextUtils.isEmpty(result.order.state) && !result.order.state.equals("2") ){
                                // 查询订单获取状态 定时任务查询订单状态
                                queryOrderGetStuts(orderNo, getString(R.string.confirming_waiting), false, false);

                            } else {
                                //否则下单成功成功
                                dismissLoading();
                                result.order.money = Utils.Integer.tryParse(money, 0);
                                //交易成功清除Note
                                NoteMarkActivity.setNoteMark("");

                                if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth","sale"),"pre_auth")){
                                    PreAutFinishActivity.startActivity(CaptureActivity.this, result.order,5);
                                }else{
                                    //交易成功进行语音播报
                                    if(!IS_POS_VERSION){
                                        voiceplay(result.order,pushTransmissionModel);
                                    }
                                    PayResultActivity.startActivity(mContext, result.order);
                                }
                                finish();
                            }
                        }
                    }
                });
            }
        }



    }

    /**
     * 语音播报
     */
    private void voiceplay(Order order,PushTransmissionModel pushTransmissionModel){
        StringBuffer sb = new StringBuffer();
//        sb.append(getResources().getString(R.string.successful_collection)).append(",");
        double totalfee = 0.0;
        if (null != order){
            //如果是不包含小数的情况，则后台传多少的值就直接读多少的值
            if(minus == 1){
                sb.append(order.totalFee).append(MainApplication.feeType);
            }else{
                totalfee = ((double)(order.totalFee)/minus);
                sb.append(totalfee).append(MainApplication.feeType);
            }
        }

        pushTransmissionModel.setVoiceContent(sb.toString());
        GTPushIntentService.startVoicePlay(mContext,pushTransmissionModel);
    }


    boolean flag = true;

    int count = 1;

    /**
     * 冲正接口
     * <功能详细描述>
     *
     * @param orderNo
     * @see [类、类#方法、类#成员]
     */
    public void payReverse(final String orderNo) {
        OrderManager.getInstance().unifiedPayReverse(orderNo, new UINotifyListener<Order>() {
            @Override
            public void onError(final Object object) {
                super.onError(object);
                dismissLoading();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (object != null) {
                            toastDialog(CaptureActivity.this, object.toString(), new NewDialogInfo.HandleBtn() {

                                @Override
                                public void handleOkBtn() {
                                    CaptureActivity.this.finish();
                                }
                            });
                        }
                    }
                });
            }

            @Override
            public void onPreExecute() {
                super.onPreExecute();
                loadDialog(CaptureActivity.this, R.string.order_czing_waiting);

            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(Order result) {
                super.onSucceed(result);

                dismissLoading();
                if (result != null) {
                    toastDialog(CaptureActivity.this, R.string.order_cz_success_1, new NewDialogInfo.HandleBtn() {

                        @Override
                        public void handleOkBtn() {
                            CaptureActivity.this.finish();
                        }
                    });
                }
            }
        });

    }

    RefundCheckDialog dialogRever = null;

    /*
     * 冲正弹窗口
     */
    private void showWindowRever() {
        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth","sale"),"pre_auth")){
            isPreAuth = true;
        }else{
            isPreAuth = false;
        }
        dialogRever = new RefundCheckDialog(CaptureActivity.this, isPreAuth,RefundCheckDialog.REVERS, getString(R.string.public_cozy_prompt), getString(R.string.tx_dialog_money), outTradeNo, String.valueOf(money), null, new RefundCheckDialog.ConfirmListener() {
            @Override
            public void ok(String code) {

                dialogs = new DialogInfo(CaptureActivity.this, getString(R.string.public_cozy_prompt), getString(R.string.tx_dialog_reverse), getString(R.string.btnOk), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

                    @Override
                    public void handleOkBtn() {
                        String msg = ToastHelper.toStr(R.string.confirming_waiting);
                        queryOrderGetStuts(outTradeNo, msg, true, true);
                        //   payReverse(outTradeNo);
                        if (dialogRever != null) {
                            dialogRever.dismiss();
                        }
                    }

                    @Override
                    public void handleCancleBtn() {
                        dialogs.cancel();
                    }
                }, null);

                DialogHelper.resize(CaptureActivity.this, dialogs);
                dialogs.show();

            }

            @Override
            public void cancel() {
                queryOrderGetStuts(outTradeNo, ToastHelper.toStr(R.string.confirming_waiting), true, false);
            }

        });
        dialogRever.show();

        dialogRever.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                if (keycode == KeyEvent.KEYCODE_BACK) {
                    return true;
                }
                return false;
            }
        });
    }

    /**
     * 查询订单获取状态
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void queryOrderGetStuts(final String orderNo, final String title, final boolean isRevers, final boolean isMoreQuery) {
        OrderManager.getInstance().queryOrderByOrderNo(orderNo, payType, new UINotifyListener<Order>() {
            @Override
            public void onError(final Object object) {
                super.onError(object);
                dismissLoading();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (object != null) {

                            if (count < 4) {
                                timeCount = 5;
                                count++;
                                showDialog = new DialogInfo(CaptureActivity.this, getStringById(R.string.public_cozy_prompt), "", getStringById(R.string.btnOk), DialogInfo.SCAN_PAY, null, null);
                                DialogHelper.resize(CaptureActivity.this, showDialog);
                                showDialog.setOnKeyListener(new OnKeyListener() {

                                    @Override
                                    public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                                        if (keycode == KeyEvent.KEYCODE_BACK) {
                                            return true;
                                        }
                                        return false;
                                    }
                                });
                                showDialog.show();
                                outTradeNo = orderNo;
                                mHandler.post(myRunnable);

                            } // 3次5秒查询后，如果还未返回成功，调用冲正接口
                            else {
                                outTradeNo = orderNo;
                                //                                dialog1.dismiss();
                                showWindowRever();
                            }

                            if (isMoreQuery) { // 再次查询后去冲正
                                payReverse(outTradeNo);
                            }
                        }
                    }
                });
            }

            @Override
            public void onPreExecute() {
                super.onPreExecute();
                loadDialog(CaptureActivity.this, title);
            }

            @Override
            public void onPostExecute() {
                super.onPostExecute();
            }

            @Override
            public void onSucceed(Order result) {
                super.onSucceed(result);

                if (result != null) {
                     //如果交易成功则将备注清空
                     if (result.state.equals("2")){
                         NoteMarkActivity.setNoteMark("");
                     }
                    if (result.state.equals("2") && flag) {

                        // 支付成功
                        flag = false;
                        dismissLoading();


//                        PayResultActivity.startActivity(mContext, result);
                        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth","sale"),"pre_auth")){
                            PreAutFinishActivity.startActivity(CaptureActivity.this, result,1);
                            Log.e("cxy","B");
                        }else{
                            if(!IS_POS_VERSION){
                                voiceplay(result,pushTransmissionModel);
                            }
                            PayResultActivity.startActivity(mContext, result);
                        }
                        finish();
                    } else {
                        if (isRevers) {

                            if (result.state.equals("2")) // 支付成功
                            {
                                if (isMoreQuery) {
                                    showToastInfo(R.string.show_order_revers);
                                }

//                                PayResultActivity.startActivity(mContext, result);
                                if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(PreferenceUtil.getString("choose_pay_or_pre_auth","sale"),"pre_auth")){
                                    PreAutFinishActivity.startActivity(CaptureActivity.this, result,1);
                                    Log.e("cxy","C");
                                }else{
                                    if(!IS_POS_VERSION){
                                        voiceplay(result,pushTransmissionModel);
                                    }

                                    PayResultActivity.startActivity(mContext, result);
                                }
                                finish();
                            } else {
                                if (isMoreQuery) { // 再次查询后去冲正
                                    payReverse(outTradeNo);
                                } else {
                                    showWindowRever();
                                }
                            }
                            return;
                        }

                        if (count < 4) {
                            dismissLoading();
                            timeCount = 5;
                            count++;
                            showDialog = new DialogInfo(CaptureActivity.this, getStringById(R.string.public_cozy_prompt), "", getStringById(R.string.btnOk), DialogInfo.SCAN_PAY, null, null);
                            DialogHelper.resize(CaptureActivity.this, showDialog);
                            showDialog.setOnKeyListener(new OnKeyListener() {

                                @Override
                                public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {

                                    if (keycode == KeyEvent.KEYCODE_BACK) {
                                        return true;
                                    }
                                    return false;
                                }
                            });
                            showDialog.show();
                            outTradeNo = orderNo;
                            mHandler.post(myRunnable);

                        } else {
                            // 3次5秒查询后，如果还未返回成功，调用冲正接口

                            if (!result.state.equals("2")) {
                                outTradeNo = orderNo;
                                dismissLoading();
                                showWindowRever();
                            } else {
                                dismissLoading();
                            }
                        }

                    }
                }
            }
        });
    }

    private long timeCount = 5;

    private Runnable myRunnable = new Runnable() {

        @Override
        public void run() {
            if (timeCount > 0 && showDialog != null) {
                //                dialogInfo.setBtnOkText("确定(" + timeCount + "秒后关闭)");
                showDialog.setMessage(getString(R.string.dialog_order_stuts) + "，(" + timeCount + getString(R.string.dialog_start) + (count - 1) + getString(R.string.dialog_end));
                timeCount -= 1;
                mHandler.postDelayed(this, 1000);
            } else {
                mHandler.removeCallbacks(this);
                if (showDialog != null && showDialog.isShowing()) {

                    showDialog.dismiss();
                }
                queryOrderGetStuts(outTradeNo, ToastHelper.toStr(R.string.confirming_waiting), false, false);
            }

        }
    };

    boolean tag = true;

    private void showDiagle() {
        CameraManager.get().stopPreview();
        HandlerManager.notifyMessage(HandlerManager.SCANCODE, 1);
        dialog = new ScanCodeCheckDialog(CaptureActivity.this, ScanCodeCheckDialog.PAY, diaglTitle, payType, null, null, new ScanCodeCheckDialog.ConfirmListener() {
            @Override
            public void ok(String code) {
                String tvInfo = getString(R.string.tx_other_code_notnull);

                if (isAbsoluteNullStr(code)) {
                    if (!isAbsoluteNullStr(payType) && payType.equals(MainApplication.PAY_TYPE_WRITE_OFF)) {
                        tvInfo = getString(R.string.tx_other_card_notnull);
                    } else if (payType.equals(MainApplication.PAY_TYPE_REFUND)) {
                        tvInfo = getString(R.string.tx_order_notnull);
                    }

                    showToastInfo(tvInfo);
                } else {
                    submitData(code, false);
                    dialog.dismiss();
                }

            }

            @Override
            public void cancel() {
                CameraManager.get().startPreview();
                HandlerManager.notifyMessage(HandlerManager.SCANCODE, 0);
                restartCamera();
            }
        });
        dialog.setInputType(InputType.TYPE_CLASS_NUMBER);
        DialogHelper.resize(CaptureActivity.this, dialog);
        dialog.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface arg0, int keycode, KeyEvent arg2) {
                if (keycode == KeyEvent.KEYCODE_BACK) {
                    CameraManager.get().startPreview();
                    HandlerManager.notifyMessage(HandlerManager.SCANCODE, 0);
                    restartCamera();
                    return false;
                }
                return false;
            }
        });
        dialog.show();
    }


    public String isOpenFisrtpayType() {
        //先判断预授权是否开通,只有开通预授权通道,同时选择是预授权模式，才会走下面的逻辑
        String saleOrPreauth = PreferenceUtil.getString("choose_pay_or_pre_auth","sale");
        if(MainApplication.isPre_authOpen == 1 && TextUtils.equals(saleOrPreauth,"pre_auth")){
            Object object = SharedPreUtile.readProduct("dynPayType_pre_auth" + ApiConstant.bankCode + MainApplication.getMchId());
            if (object != null) {
                List<DynModel> list = (List<DynModel>) object;
                if (null != list && list.size() > 0 && list.size() == 1) {
                    return list.get(0).getApiCode();
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }else{
            Object object = SharedPreUtile.readProduct("dynPayType" + ApiConstant.bankCode + MainApplication.getMchId());

            if (object != null) {
                List<DynModel> list = (List<DynModel>) object;
                if (null != list && list.size() > 0 && list.size() == 1) {
                    return list.get(0).getApiCode();
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
    }

    private String setType() {
        if (!StringUtil.isEmptyOrNull(MainApplication.serviceType)) {
            String[] arrPays = MainApplication.serviceType.split("\\|");
            payMeths.clear();
            for (int s = 0; s < arrPays.length; s++) {
                if (arrPays[s].equalsIgnoreCase(MainApplication.PAY_QQ_NATIVE) || arrPays[s].equalsIgnoreCase(MainApplication.PAY_QQ_NATIVE1)) {
                    payMeths.add(arrPays[s]);
                    if (payMeths.contains(MainApplication.PAY_QQ_NATIVE) && payMeths.contains(MainApplication.PAY_QQ_NATIVE1)) {
                        payMeths.remove(MainApplication.PAY_QQ_NATIVE1);
                    }

                } else if (arrPays[s].startsWith(MainApplication.PAY_WX_NATIVE)) {
                    //微信支付
                    payMeths.add(arrPays[s]);
                } else if (arrPays[s].equals(MainApplication.PAY_ZFB_NATIVE) || arrPays[s].equals(MainApplication.PAY_ZFB_NATIVE1)) {
                    //支付宝支付
                    payMeths.add(arrPays[s]);
                    if (payMeths.contains(MainApplication.PAY_ZFB_NATIVE) && payMeths.contains(MainApplication.PAY_ZFB_NATIVE1)) {
                        payMeths.remove(MainApplication.PAY_ZFB_NATIVE);
                    }
                } else if (arrPays[s].equalsIgnoreCase(MainApplication.PAY_JINGDONG_NATIVE)) {
                    payMeths.add(arrPays[s]);
                }
            }

            if (payMeths.size() > 1) {
                return null;
            } else {
                if (payMeths.size() > 0) {
                    return payMeths.get(0);
                } else {
                    return null;
                }
            }
        } else {
            return null;
        }
    }

    @Override
    public void onClick(View v) {

    }
}
