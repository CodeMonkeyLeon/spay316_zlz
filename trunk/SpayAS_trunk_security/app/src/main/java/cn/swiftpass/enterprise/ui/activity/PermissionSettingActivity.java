package cn.swiftpass.enterprise.ui.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.common.utils.PngSetting;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.mvp.base.BasePresenter;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.PngUtils;
import cn.swiftpass.enterprise.utils.permissionutils.Permission;
import cn.swiftpass.enterprise.utils.permissionutils.PermissionsManage;

/**
 * Created by congwei.li on 2022/5/20.
 *
 * @Description:
 */
public class PermissionSettingActivity extends BaseActivity implements View.OnClickListener {

    private TextView tv_status_storage, tv_status_bluetooth, tv_status_camera,
            tv_status_location;
    private LinearLayout ll_permission_storage, ll_permission_bluetooth,
            ll_permission_camera, ll_permission_location;


    private String[] mBlueToothPermissions;
    private String[] mStoragePermissions;


    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.setting_permission_management);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                finish();
            }

            @Override
            public void onRightButtonClick() {
            }

            @Override
            public void onRightLayClick() {
            }

            @Override
            public void onRightButLayClick() {
            }
        });
    }


    @Override
    protected boolean useToolBar() {
        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            //android 13
            mBlueToothPermissions = Permission.Group.BLUETOOTH_API_31;
            mStoragePermissions = Permission.Group.STORAGE_API_33;
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            //android 12
            mBlueToothPermissions = Permission.Group.BLUETOOTH_API_31;
            mStoragePermissions = Permission.Group.STORAGE;
        } else {
            mBlueToothPermissions = Permission.Group.BLUETOOTH;
            mStoragePermissions = Permission.Group.STORAGE;
        }


        initViews();


        if (PngSetting.INSTANCE.isChangePng1()) {
            initPng();
        }
    }

    private void initPng() {
        ImageView imgCamera = findViewById(R.id.id_img_camera);
        ImageView imgStorage = findViewById(R.id.id_img_storage);
        ImageView imgBluetooth = findViewById(R.id.id_img_bluetooth);
        ImageView imgPosition = findViewById(R.id.id_img_position);


        PngUtils.INSTANCE.setPng1(this, imgCamera, R.drawable.icon_access_camera);
        PngUtils.INSTANCE.setPng1(this, imgStorage, R.drawable.icon_access_file);
        PngUtils.INSTANCE.setPng1(this, imgBluetooth, R.drawable.icon_access_bluetooth);
        PngUtils.INSTANCE.setPng1(this, imgPosition, R.drawable.icon_access_navigation);
    }


    @Override
    protected void onStop() {
        super.onStop();
    }

    private void initViews() {
        setContentView(R.layout.activity_setting_permission);
        ll_permission_storage = findViewById(R.id.ll_permission_storage);
        ll_permission_bluetooth = findViewById(R.id.ll_permission_bluetooth);
        ll_permission_camera = findViewById(R.id.ll_permission_camera);
        ll_permission_location = findViewById(R.id.ll_permission_location);
        tv_status_storage = findViewById(R.id.tv_status_storage);
        tv_status_bluetooth = findViewById(R.id.tv_status_bluetooth);
        tv_status_camera = findViewById(R.id.tv_status_camera);
        tv_status_location = findViewById(R.id.tv_status_location);

        ll_permission_storage.setOnClickListener(this);
        ll_permission_bluetooth.setOnClickListener(this);
        ll_permission_camera.setOnClickListener(this);
        ll_permission_location.setOnClickListener(this);

        checkStatus();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 从系统设置权限完成后返回app的处理
        if (requestCode == PermissionsManage.REQUEST_CODE) {
            checkStatus();
        }
    }

    private void checkStatus() {
        tv_status_storage.setText(checkPermission(mStoragePermissions) ?
                getString(R.string.setting_permission_activated) :
                getString(R.string.setting_permission_inactivated));
        tv_status_bluetooth.setText(checkPermission(mBlueToothPermissions) ?
                getString(R.string.setting_permission_activated) :
                getString(R.string.setting_permission_inactivated));
        tv_status_camera.setText(checkPermission(Permission.CAMERA) ?
                getString(R.string.setting_permission_activated) :
                getString(R.string.setting_permission_inactivated));
        tv_status_location.setText(checkPermission(Permission.Group.ACCESS) ?
                getString(R.string.setting_permission_activated) :
                getString(R.string.setting_permission_inactivated));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_permission_storage:
                toSystemSetPermission(mStoragePermissions);
                break;
            case R.id.ll_permission_bluetooth:
                toSystemSetPermission(mBlueToothPermissions);
                break;
            case R.id.ll_permission_camera:
                toSystemSetPermission(Permission.CAMERA);
                break;
            case R.id.ll_permission_location:
                toSystemSetPermission(Permission.Group.ACCESS);
                break;
        }
    }

    private boolean checkPermission(String[] permission) {
        return PermissionsManage.isGranted(PermissionSettingActivity.this, permission);
    }

    private boolean checkPermission(String permission) {
        return checkPermission(new String[]{permission});
    }

    private void toSystemSetPermission(String[] permission) {
        PermissionsManage.startPermissionActivity(PermissionSettingActivity.this,
                permission);
    }

    private void toSystemSetPermission(String permission) {
        toSystemSetPermission(new String[]{permission});
    }
}