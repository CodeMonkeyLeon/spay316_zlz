package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 优惠券
 * User: Alan
 * Date: 13-12-24
 * Time: 下午5:49
 */
public class CouponModel implements  Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Integer couponId; //id
    public String couponNo; //优惠券编号
    public Integer shopId; //所属店铺id
    public Integer userId; //添加该优惠券的userId
    public String couponName; //标题
    public String couponDetail; //优惠详情

    public String couponPic; //优惠券图片地址

    public Integer couponCount; //发放数量
    public Integer collectCount;//消费数量
    public String issueLimt;//发放限制条件满X元送Y张
    public Date startDate; //有效期开始日期
   // public Date endDate; //有效期结束日期//
    public String strEndDate;// W.l
    public Date endDate;

    public String otherShop; //其他门面

    public String strStartDate; //有效期开始日期
    //public String strEndDate; //有效期结束日期

    public Integer limitId; //使用限制id
    public Integer couponType; //优惠券类型
    public Integer color; //背景颜色

    public Integer issueX = 0;//满 xx 送 xx
    public Integer issueY;


    public Integer discount = 0; //如果优惠券类型为1，则该参数必须填写，最终金额为总金额乘以该折扣
    public Double commonTitle; //如果优惠券类型为2，则该参数须填写，最终价格为该价格，适合类似KFC套餐优惠价类的商品
    public Double sectionPriceFirst; //一阶优惠价位：如果优惠券类型为3，则该参数须填写，如果用户消费大于该金额，则按照一阶优惠减免价格减免
    public Double sectionDiscountFirst; //一阶优惠减免价格：如果优惠券类型为3，则该参数须填写，用户消费超过SECTION_PRICE_FIRST时减免SECTION_DISCOUNT_FIRST这个价格
    public Double sectionPriceSec; //二阶优惠价位：如果优惠券类型为3，则该参数须填写，如果用户消费大于该金额，则按照一阶优惠减免价格减免
    public Double sectionDiscountSec; //二阶优惠减免价格：如果优惠券类型为3，则该参数须填写，用户消费超过SECTION_PRICE_SEC时减免SECTION_DISCOUNT_SEC这个价格
    public Double sectionPriceThr; //三阶优惠价位：如果优惠券类型为3，则该参数须填写，如果用户消费大于该金额，则按照一阶优惠减免价格减免
    public Double sectionDiscountThr; //三阶优惠减免价格：如果优惠券类型为3，则该参数须填写，用户消费超过SECTION_PRICE_THR时减免SECTION_DISCOUNT_THR这个价格

    public String weekDays;//选择星期
    //店铺的信息 新增时候可以改店铺的
    public String address;//店铺地址
    public String linkPhone;//  店铺联系电话

    public String useDays;
    public String useDaysDefined;   //自定义可用日期
    public String useHours;
    public String useHoursDefined; //自定义可用时段
    public String otherLimt;
    public String otherLimtDefined;  //自定义使用说明

    public Integer issueReceive;//1 赠送券 2领用券
    public Integer minAmount = 0;//最低消费金额，超过此金额的才可以使用此优惠券
    public Integer validDays ; // 有效天数;
    public Integer couponStatus; //CouponStatusEnum 优惠券状态
    public Integer oncePiece;//赠送券是否重复使用
	@Override
	public String toString() {
		return "CouponModel [couponId=" + couponId + ", couponNo=" + couponNo
				+ ", shopId=" + shopId + ", userId=" + userId + ", couponName="
				+ couponName + ", couponDetail=" + couponDetail
				+ ", couponPic=" + couponPic + ", couponCount=" + couponCount
				+ ", collectCount=" + collectCount + ", issueLimt=" + issueLimt
				+ ", startDate=" + startDate + ", endDate=" + endDate
				+ ", otherShop=" + otherShop + ", strStartDate=" + strStartDate
				+ ", strEndDate=" + strEndDate + ", limitId=" + limitId
				+ ", couponType=" + couponType + ", color=" + color
				+ ", issueX=" + issueX + ", issueY=" + issueY + ", discount="
				+ discount + ", commonTitle=" + commonTitle
				+ ", sectionPriceFirst=" + sectionPriceFirst
				+ ", sectionDiscountFirst=" + sectionDiscountFirst
				+ ", sectionPriceSec=" + sectionPriceSec
				+ ", sectionDiscountSec=" + sectionDiscountSec
				+ ", sectionPriceThr=" + sectionPriceThr
				+ ", sectionDiscountThr=" + sectionDiscountThr + ", weekDays="
				+ weekDays + ", address=" + address + ", linkPhone="
				+ linkPhone + ", useDays=" + useDays + ", useDaysDefined="
				+ useDaysDefined + ", useHours=" + useHours
				+ ", useHoursDefined=" + useHoursDefined + ", otherLimt="
				+ otherLimt + ", otherLimtDefined=" + otherLimtDefined
				+ ", issueReceive=" + issueReceive + ", minAmount=" + minAmount
				+ ", validDays=" + validDays + ", couponStatus=" + couponStatus
				+ ", oncePiece=" + oncePiece + "]";
	}
    
    

}
