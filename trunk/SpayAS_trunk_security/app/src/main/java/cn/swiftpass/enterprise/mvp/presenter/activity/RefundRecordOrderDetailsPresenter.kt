package cn.swiftpass.enterprise.mvp.presenter.activity

import android.text.TextUtils
import cn.swiftpass.enterprise.MainApplication
import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.io.okhttp.CallBackUtil
import cn.swiftpass.enterprise.io.okhttp.CommonResponse
import cn.swiftpass.enterprise.mvp.api.AppClient
import cn.swiftpass.enterprise.mvp.constants.ParamsConstants
import cn.swiftpass.enterprise.mvp.contract.activity.RefundRecordOrderDetailsContract
import cn.swiftpass.enterprise.utils.JsonUtil
import okhttp3.Call

/**
 * @author lizheng.zhao
 * @date 2022/12/5
 *
 * @当你的情人已改名玛丽
 * @你怎能送她一首菩萨蛮
 */
class RefundRecordOrderDetailsPresenter : RefundRecordOrderDetailsContract.Presenter {

    private var mView: RefundRecordOrderDetailsContract.View? = null


    override fun queryRefundDetail(
        orderNo: String?,
        mchId: String?
    ) {
        mView?.let { view ->

            view.showLoading(R.string.public_data_loading, ParamsConstants.COMMON_LOADING)

            AppClient.queryRefundDetail(
                orderNo,
                mchId,
                MainApplication.getInstance().getUserId().toString(),
                System.currentTimeMillis().toString(),
                object : CallBackUtil.CallBackCommonResponse<Order>(Order()) {

                    override fun onFailure(call: Call?, commonResponse: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        commonResponse?.let {
                            view.queryRefundDetailFailed(it.message)
                        }
                    }

                    override fun onResponse(r: CommonResponse?) {
                        view.dismissLoading(ParamsConstants.COMMON_LOADING)
                        r?.let { response ->
                            //如果discountDetail值为空，替换""为[]，因为Gson无法解析""为数组
                            response.message = response.message.replace(
                                "\"discountDetail\":\"\"",
                                "\"discountDetail\":[]"
                            )
                            val order = JsonUtil.jsonToBean(
                                response.message,
                                Order::class.java
                            ) as Order
                            order.useId = order.userId.toString()
                            order.orderNoMch = order.orderNo
                            order.cashFeel =
                                if (TextUtils.isEmpty(order.cashFee)) 0 else order.cashFee.toLong()
                            view.queryRefundDetailSuccess(order)
                        }
                    }
                }
            )
        }
    }

    override fun attachView(view: RefundRecordOrderDetailsContract.View?) {
        view?.let {
            mView = it
        }
    }

    override fun detachView() {
        mView = null
    }
}