package cn.swiftpass.enterprise.ui.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.StringUtil;

public class RefundHistoryAdapter extends BaseAdapter
{
    
    private Context context;
    
    public static final int REFUND_HISTORY_RECORD = 0;// 查看退款记录
    
    public static final int REFUND_SEARCH = 1;// 搜索退款订单
    
    public static final int ORDER_STREAM = 2;//订单流水
    
    public static final int ORDER_STREAM_REFUND_RECORD = 3;//从订单流水页面查看退款记录
    
    public static final int ORDER_HEXIAO = 4;//核销
    
    private int state;
    
    private List<Order> list;
    
    private String seachContent = "";
    
    private boolean isFristInit = true;
    
    private int height = 0;
    
    public RefundHistoryAdapter(Context context, List<Order> list, int state)
    {
        this.context = context;
        this.list = list;
        this.state = state;
    }
    
    @Override
    public int getCount()
    {
        return list.size();
    }
    
    @Override
    public Object getItem(int arg0)
    {
        return list.get(arg0);
    }
    
    @Override
    public long getItemId(int arg0)
    {
        return arg0;
    }
    
    @Override
    public View getView(int position, View view, ViewGroup arg2)
    {
        Order order = list.get(position);
        MyHolder myHolder;
        if (view == null)
        {
            int resId = 0;
            if (state == ORDER_STREAM)
            {
                resId = R.layout.refund_query_search_order;
            }
            else if (state == REFUND_HISTORY_RECORD || state == ORDER_STREAM_REFUND_RECORD || state == REFUND_SEARCH)
            {
                resId = R.layout.refund_query_search_tuikuan;
            }
            else if (state == ORDER_HEXIAO)
            {
                resId = R.layout.refund_query_search_hexiao;
            }
            view = View.inflate(context, resId, null);
            myHolder = new MyHolder(view);
            view.setTag(myHolder);
        }
        else
        {
            myHolder = (MyHolder)view.getTag();
        }
        
        mySetVisibility(position, myHolder);
        String tradeType = order.getTradeType();
        if (null != tradeType)
        {
            if (tradeType.startsWith(MainApplication.PAY_WX_MICROPAY)
                || tradeType.startsWith(MainApplication.PAY_WX_NATIVE))
            {// 微信
                myHolder.refund_img_id.setImageResource(R.drawable.icon_list_wechat);
            }
            else if (tradeType.equals(MainApplication.PAY_ZFB_MICROPAY)
                || tradeType.contains(MainApplication.PAY_ZFB_NATIVE)
                || tradeType.startsWith(MainApplication.PAY_ALIPAY_WAP)
                || tradeType.contains(MainApplication.PAY_ALIPAY_TAG))
            {// 支付宝
                myHolder.refund_img_id.setImageResource(R.drawable.icon_list_pay);
            }
            else if (tradeType.equals(MainApplication.PAY_QQ_NATIVE)
                || tradeType.equals(MainApplication.PAY_QQ_MICROPAY)
                || tradeType.equalsIgnoreCase(MainApplication.PAY_QQ_WAP)
                || tradeType.equals(MainApplication.PAY_QQ_NATIVE1) || tradeType.contains(MainApplication.PAY_QQ_TAG))
            {// 手Q
                myHolder.refund_img_id.setImageResource(R.drawable.icon_list_qq);
            }
            else if (tradeType.equals(MainApplication.PAY_WX_SJPAY))
            {
                myHolder.refund_img_id.setImageResource(R.drawable.icon_list_wechat_public);
            }
            else if (tradeType.equals(MainApplication.PAY_JINGDONG)
                || tradeType.equals(MainApplication.PAY_JINGDONG_NATIVE)
                || tradeType.contains(MainApplication.PAY_JD_TAG))
            {
                myHolder.refund_img_id.setImageResource(R.drawable.icon_list_jd);
            }
        }
        //        if (state != ORDER_HEXIAO)
        //        {
        //            myHolder.card_add_time_tv.setVisibility(View.GONE);
        //            
        //        }
        if (state == ORDER_STREAM)
        {
            initView_SearchOrder(myHolder, order);
        }
        else if (state == REFUND_HISTORY_RECORD || state == ORDER_STREAM_REFUND_RECORD || state == REFUND_SEARCH)
        {
            initView_SearchTuiKuan(myHolder, order);
        }
        else if (state == ORDER_HEXIAO)
        {
            initView_SearchHeXiao(myHolder, order);
        }
        if (!StringUtil.isEmptyOrNull(order.getTradeType()))
        {
            if (order.getTradeType().startsWith(MainApplication.PAY_ALIPAY_WAP)
                || order.getTradeType().equals(MainApplication.PAY_QQ_WAP)
                || order.getTradeType().equals(MainApplication.PAY_WX_SJPAY))
            {
                myHolder.img_id.setVisibility(View.VISIBLE);
                myHolder.img_id.setImageResource(R.drawable.icon_mark_code);
            }
            else
            {
                myHolder.img_id.setVisibility(View.GONE);
            }
        }
        
        //        if (state == REFUND_HISTORY_RECORD || state == ORDER_STREAM_REFUND_RECORD)
        //        {
        //            if (state == ORDER_STREAM_REFUND_RECORD)
        //            {
        //                if (isFristInit)
        //                {
        //                    height = linearParams.height - 15;
        //                    isFristInit = false;
        //                }
        //                myHolder.ll_refund_big.setBackgroundColor(Color.parseColor("#f0f0f0"));
        //                linearParams.height = height;
        //                myHolder.ll_refund_back.setLayoutParams(linearParams);
        //            }
        //            // 查看退款记录
        //            myHolder.refund_time_title.setVisibility(View.VISIBLE);
        //            //myHolder.refund_state_or_time_tv.setText("退款中");
        //            
        //            myHolder.refund_order_id_tv.setText(transactionId);
        //            try
        //            {
        //                //long time = Long.parseLong(order.add_time);
        //                String converTime = DateUtil.converTime(order.add_time);
        //                myHolder.refund_time_tv.setText(converTime);// 退款时间
        //            }
        //            catch (Exception e)
        //            {
        //                myHolder.refund_time_tv.setText("");// 退款时间
        //            }
        //            switch (order.getRefundState())
        //            {
        //                case 3:
        //                    myHolder.refund_state_or_time_tv.setTextColor(Color.parseColor("#ff7f00"));
        //                    myHolder.refund_state_or_time_tv.setText("审核中");
        //                    break;
        //                case 2:
        //                    myHolder.refund_state_or_time_tv.setText("退款失败");
        //                    break;
        //                case 1:
        //                    myHolder.refund_state_or_time_tv.setTextColor(Color.parseColor("#000000"));
        //                    myHolder.refund_state_or_time_tv.setText("退款已受理");
        //                    break;
        //                default:
        //                    break;
        //            }
        //            
        //            double moneyText = (double)((double)order.getRefundMoney() / 100);
        //            myHolder.sum_of_busines.setText("¥ " + DateUtil.formatMoneyUtil(moneyText));
        //            
        //        }
        //        else if (state == REFUND_SEARCH)
        //        {
        //            // 搜索退款订单
        //            
        //            myHolder.refund_time_title.setVisibility(View.GONE);
        //            
        //            ForegroundColorSpan nameColor = new ForegroundColorSpan(context.getResources().getColor(R.color.refund_1));
        //            SpannableStringBuilder builder = new SpannableStringBuilder(transactionId);
        //            if (!TextUtils.isEmpty(seachContent) && transactionId.contains(seachContent))
        //            {
        //                builder.setSpan(nameColor, transactionId.indexOf(seachContent), transactionId.indexOf(seachContent)
        //                    + seachContent.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        //            }
        //            
        //            myHolder.refund_time_tv.setTextColor(context.getResources().getColor(R.color.refund_2));
        //            //myHolder.refund_time_tv.setText(builder);
        //            myHolder.refund_order_id_tv.setText(builder);
        //            myHolder.refund_state_or_time_tv.setVisibility(View.GONE);
        //            //myHolder.refund_state_or_time_tv.setTextColor(context.getResources().getColor(R.color.refund_2));
        //            try
        //            {
        //                long time = Long.parseLong(order.getTradeTime());
        //                String converTime = DateUtil.converTime(time);
        //                myHolder.refund_time_tv.setText(converTime);// 退款时间
        //            }
        //            catch (Exception e)
        //            {
        //                myHolder.refund_time_tv.setText("");// 退款时间
        //            }
        //            
        //            double moneyText = (double)((double)order.money / 100);
        //            myHolder.sum_of_busines.setText("¥ " + DateUtil.formatMoneyUtil(moneyText));
        //        }
        //        else if (state == ORDER_STREAM)
        //        {
        //            if (isFristInit)
        //            {
        //                //height = linearParams.height - 20;
        //                isFristInit = false;
        //            }
        //            myHolder.ll_refund_big.setBackgroundColor(Color.parseColor("#f0f0f0"));
        //            //linearParams.height = height;
        //            //myHolder.ll_refund_back.setLayoutParams(linearParams);
        //            
        //            //订单流水
        //            myHolder.refund_time_title.setVisibility(View.GONE);
        //            //myHolder.refund_state_or_time_tv.setVisibility(View.GONE);
        //            myHolder.refund_order_id_tv.setText(transactionId);
        //            myHolder.refund_state_or_time_tv.setText("收款成功");
        //            double money = order.money / 100d;
        //            myHolder.sum_of_busines.setText("¥ " + DateUtil.formatMoney(money));
        //            
        //            try
        //            {
        //                String converTime = DateUtil.formatTimeUtil(Long.parseLong(order.notifyTime));
        //                //String converTime = DateUtil.formatTimeUtil(Long.parseLong(order.notifyTime));
        //                myHolder.refund_time_tv.setText(converTime);// 退款时间
        //            }
        //            catch (Exception e)
        //            {
        //                myHolder.refund_time_tv.setText("");// 退款时间
        //            }
        //        }
        //        else if (state == ORDER_HEXIAO)//核销
        //        {
        //            if (isFristInit)
        //            {
        //                //height = linearParams.height - 15;
        //                isFristInit = false;
        //            }
        //            myHolder.ll_refund_big.setBackgroundColor(Color.parseColor("#f0f0f0"));
        //            //linearParams.height = height;
        //            //myHolder.card_add_time_tv.setVisibility(View.VISIBLE);
        //            //myHolder.ll_refund_back.setLayoutParams(linearParams);
        //            myHolder.refund_time_title.setVisibility(View.GONE);
        //            myHolder.refund_time_tv.setText(order.getAddTime());
        //            myHolder.sum_of_busines.setText(order.getTitle());
        //            myHolder.refund_state_or_time_tv.setText("核销成功");
        //            myHolder.refund_order_id_tv.setText(order.getCode());
        //            
        //        }
        
        return view;
    }
    
    /** 初始化退款订单item
     * <功能详细描述>
     * @param myHolder
     * @param order
     * @see [类、类#方法、类#成员]
     */
    private void initView_SearchTuiKuan(MyHolder myHolder, Order order)
    {
        // 查看退款记录
        
        String transactionId = "";
        if (!TextUtils.isEmpty(order.getOutRefundNo()) && order.getOutRefundNo().length() > 15)
        {
            transactionId = order.getOutRefundNo().substring(10);// 单号
            
            transactionId = order.getOutRefundNo().substring(order.getOutRefundNo().length() - 15);// 单号
        }
        else
        {
            transactionId = order.getOutRefundNo();
        }
        double moneyText = 0;
        if (state == REFUND_SEARCH)
        {
            ForegroundColorSpan nameColor = new ForegroundColorSpan(context.getResources().getColor(R.color.refund_1));
            SpannableStringBuilder builder = new SpannableStringBuilder(transactionId);
            if (!TextUtils.isEmpty(seachContent) && transactionId.contains(seachContent))
            {
                builder.setSpan(nameColor, transactionId.indexOf(seachContent), transactionId.indexOf(seachContent)
                    + seachContent.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            myHolder.tuikuan_time_title.setText("");
            myHolder.refund_order_id_tv.setText(builder);
            moneyText = (double)((double)order.money / 100);
            
        }
        else
        {
            myHolder.refund_order_id_tv.setText(transactionId);
            moneyText = (double)((double)order.getRefundMoney() / 100);
        }
        
        myHolder.sum_of_busines.setText(MainApplication.getFeeFh() + DateUtil.formatMoneyUtil(moneyText));
        
        try
        {
            //long time = Long.parseLong(order.add_time);
            String converTime = DateUtil.getEasyTime(order.add_time);
            myHolder.refund_time_tv.setText(converTime);// 退款时间
        }
        catch (Exception e)
        {
            myHolder.refund_time_tv.setText("");// 退款时间
        }
        switch (order.getRefundState())
        {
            case 0:
                myHolder.refund_state_tv.setTextColor(Color.parseColor("#ff7f00"));
                myHolder.tuikuan_time_title.setText(R.string.tuikuan_time_accept);
                myHolder.refund_state_tv.setText("退款中");
                break;
            case 2:
                myHolder.tuikuan_time_title.setText("");
                myHolder.refund_state_tv.setText("退款失败");
                break;
            case 1:
                myHolder.tuikuan_time_title.setText(R.string.tuikuan_time_ok);
                myHolder.refund_state_tv.setTextColor(Color.parseColor("#000000"));
                myHolder.refund_state_tv.setText("退款已受理");
                break;
            default:
                break;
        }
        
    }
    
    /** 初始化订单item
     * <功能详细描述>
     * @param myHolder
     * @param order
     * @see [类、类#方法、类#成员]
     */
    @SuppressLint("ResourceAsColor")
    private void initView_SearchOrder(MyHolder myHolder, Order order)
    {
        if (isFristInit)
        {
            isFristInit = false;
        }
        String transactionId = "";
        if (!TextUtils.isEmpty(order.getOrderNoMch()) && order.getOrderNoMch().length() > 15)
        {
            String orderNo = order.getOrderNoMch();
            
            transactionId = orderNo.substring(orderNo.length() - 15);// 单号
        }
        else
        {
            transactionId = order.getOutTradeNo();
        }
        //        myHolder.ll_refund_big.setBackgroundColor(Color.parseColor("#f0f0f0"));
        myHolder.refund_order_id_tv.setText(transactionId);
        
        String orderStatusDesc = order.getTradeStateText();
        if (!TextUtils.isEmpty(orderStatusDesc))
        {
            myHolder.refund_state_tv.setText(orderStatusDesc);
        }
        else
        {
            myHolder.refund_state_tv.setText("未知");
            
        }
        switch (order.getTradeState())
        {
            case 3:
                // 已关闭状态
                myHolder.refund_order_id_tv.setTextColor(context.getResources().getColor(R.color.bg_color_text));
                myHolder.sum_of_busines.setTextColor(context.getResources().getColor(R.color.bg_color_text));
                myHolder.refund_state_tv.setTextColor(context.getResources().getColor(R.color.setting_tx_color));
                break;
            case 1:
                myHolder.sum_of_busines.setTextColor(context.getResources().getColor(R.color.bg_color_text));
                myHolder.refund_state_tv.setTextColor(context.getResources().getColor(R.color.stram_nopay_state));
                break;
            case 8:
                myHolder.sum_of_busines.setTextColor(context.getResources().getColor(R.color.setting_tx_color));
                myHolder.refund_state_tv.setTextColor(context.getResources().getColor(R.color.stram_nopay_state));
                break;
            case 2:
                myHolder.sum_of_busines.setTextColor(context.getResources().getColor(R.color.setting_tx_color));
                myHolder.refund_state_tv.setTextColor(context.getResources().getColor(R.color.setting_tx_color));
                break;
            default:
                myHolder.refund_state_tv.setTextColor(context.getResources().getColor(R.color.setting_tx_color));
                break;
        }
        
        //myHolder.refund_state_tv.setText("收款成功");
        
        double money = order.money / 100d;
        myHolder.sum_of_busines.setText(MainApplication.getFeeFh() + DateUtil.formatMoney(money));
        
        try
        {
            //String converTime = DateUtil.formatTimeUtil(Long.parseLong(order.notifyTime));
            String converTime = null;
            if (!TextUtils.isEmpty(order.notifyTime))
            {
                converTime = DateUtil.getEasyTime(Long.parseLong(order.notifyTime));
            }
            else
            {
                converTime = DateUtil.getEasyTime(order.notify_time);
            }
            myHolder.refund_time_tv.setText(converTime);
            //            if (order.getAffirm() == 1)
            //            {
            //                myHolder.img_id.setVisibility(View.VISIBLE);
            //                myHolder.img_id.setImageResource(R.drawable.icon_mark_verification);
            //            }
            if (order.getAffirm() == 2)
            {
                myHolder.img_id.setVisibility(View.VISIBLE);
                myHolder.img_id.setImageResource(R.drawable.icon_mark_card);
            }
            else
            {
                if (order.getTradeType().startsWith(MainApplication.PAY_ALIPAY_WAP)
                    || order.getTradeType().equals(MainApplication.PAY_QQ_WAP)
                    || order.getTradeType().equals(MainApplication.PAY_WX_SJPAY))
                {
                    myHolder.img_id.setVisibility(View.VISIBLE);
                    myHolder.img_id.setImageResource(R.drawable.icon_mark_code);
                }
                else
                {
                    myHolder.img_id.setVisibility(View.GONE);
                }
            }
        }
        catch (Exception e)
        {
            if (order.notify_time > 0)
            {
                myHolder.refund_time_tv.setText(DateUtil.getEasyTime(order.notify_time));// 退款时间
            }
        }
    }
    
    /** 初始化核销订单item
     * <功能详细描述>
     * @param myHolder
     * @param order
     * @see [类、类#方法、类#成员]
     */
    private void initView_SearchHeXiao(MyHolder myHolder, Order order)
    {
        
        myHolder.refund_order_id_tv.setText(order.getCardCode()); // 卡券编号
        myHolder.sum_of_busines.setText(order.getTitle()); // 卡券主题
        myHolder.refund_state_tv.setText(R.string.tx_affirm_succ);
        
        try
        {
            String converTime = "";
            if (order.getUseTime() > 0) // 核销时间
            {
                converTime = DateUtil.getEasyTime(order.getUseTime());
            }
            myHolder.refund_time_tv.setText(converTime);
            
        }
        catch (Exception e)
        {
            myHolder.refund_time_tv.setText("");
        }
        
        //        myHolder.refund_state_tv.setText("核销成功");
        //        myHolder.refund_title_tv.setText(order.getTitle());
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @param myHolder 
     * @param position 
     * @see [类、类#方法、类#成员]
     */
    private void mySetVisibility(int position, MyHolder myHolder)
    {
        //        if (position == 0)
        //        {
        //            myHolder.line_top.setVisibility(View.VISIBLE);
        //            myHolder.line_top2.setVisibility(View.GONE);
        //        }
        //        else
        //        {
        //            myHolder.line_top.setVisibility(View.GONE);
        //            myHolder.line_top2.setVisibility(View.VISIBLE);
        //            
        //        }
        //        if (position == list.size() - 1)
        //        {
        //            myHolder.line_bottm.setVisibility(View.VISIBLE);
        //        }
        //        else
        //        {
        //            myHolder.line_bottm.setVisibility(View.GONE);
        //        }
        
    }
    
    private class MyHolder
    {
        public ImageView refund_img_id;
        
        public TextView sum_of_busines;
        
        public TextView refund_title_tv;
        
        public TextView refund_time_tv;
        
        public TextView tuikuan_time_title;
        
        public TextView refund_state_tv, card_add_time_tv;
        
        public TextView refund_order_id_tv;
        
        public View line_bottm;
        
        public LinearLayout ll_refund_big, ll_refund_back;
        
        private ImageView img_id;
        
        public MyHolder(View v)
        {
            this.ll_refund_big = (LinearLayout)v.findViewById(R.id.ll_refund_big);
            this.ll_refund_back = (LinearLayout)v.findViewById(R.id.ll_refund_back);
            this.refund_img_id = (ImageView)v.findViewById(R.id.refund_img_id);
            
            this.sum_of_busines = (TextView)v.findViewById(R.id.sum_of_busines);
            //            this.refund_title_tv = (TextView)v.findViewById(R.id.refund_title);
            this.refund_time_tv = (TextView)v.findViewById(R.id.refund_time_tv);
            
            this.refund_state_tv = (TextView)v.findViewById(R.id.refund_state_tv);
            
            this.line_bottm = v.findViewById(R.id.refund_query_search_line_bottm);
            this.refund_order_id_tv = (TextView)v.findViewById(R.id.order_id_tv);
            this.tuikuan_time_title = (TextView)v.findViewById(R.id.tuikuan_time_title);
            this.img_id = (ImageView)v.findViewById(R.id.img_id);
            //            this.ll_card_add_time = (LinearLayout)v.findViewById(R.id.ll_card_add_time);
        }
    }
    
    public void setSeachContent(String seachContent)
    {
        this.seachContent = seachContent;
    }
    
}
