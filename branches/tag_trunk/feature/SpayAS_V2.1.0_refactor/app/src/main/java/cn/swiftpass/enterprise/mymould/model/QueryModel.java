package cn.swiftpass.enterprise.mymould.model;

import com.cxy.networklibrary.CoreComponent;

import cn.swiftpass.enterprise.mymould.api.MyMouldApi;
import cn.swiftpass.enterprise.mymould.contract.QueryContract;
import io.reactivex.Observable;

/**
 * 这里不去做数据处理，弱化M层，P层处理业务逻辑关系
 * 为什么弱化M层，我是参考谷歌MVP-Demo,谷歌也是没有在M层作处理，是在P层作处理
 */
public class QueryModel implements QueryContract.Model {
    @Override
    public Observable queryData(String data, String spayRs) {
        return CoreComponent.httpManager().getService(MyMouldApi.class).queryCashier(data,spayRs);
    }
}
//传统MVP中Model起着处理具体逻辑的功能，Presenter起着隔离和解耦的作用。
//这种方式实现的MVP中Presenter看上去更像一个代理类，仅仅是不让View直接访问Model。
//虽然这样做解耦的程度更高，但实际项目中，一个页面逻辑的修改是非常少的，
//仅仅在产品修改需求是才会发生，大部分情况下仅仅是修复bug之类的小修改，
//并需要这样彻底的解耦。从另一个方面来说，一个页面的视图逻辑和业务逻辑本就是一体的。
//因此，Google的MVP弱化的Model的存在，让Presenter代替传统意义上的Model，
// 减少了因使用MVP而剧增的类。这种方式相比不适用MVP仅从1个activity，
// 增加到了1个activity，1个fragment（view），1个presenter，
// 1个contract（如果有传统意义上的Model，还会有1个Model的接口和1个实现类）。
