package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.Order
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/12/1
 *
 * @我来到这个世界
 * @为了看看太阳和蓝色的地平线
 */
class OrderDetailsContract {


    interface View : BaseView {

        fun authPayQuerySuccess(response: Order?)


        fun authPayQueryFailed(error: Any?)


        fun queryOrderByOrderNoSuccess(response: Order?)

        fun queryOrderByOrderNoFailed(error: Any?)


        fun unifiedPayReverseSuccess(response: Order?)

        fun unifiedPayReverseFailed(error: Any?)


        fun querySpayOrderSuccess(response: List<Order>?)

        fun querySpayOrderFailed(error: Any?)
    }

    interface Presenter : BasePresenter<View> {


        fun querySpayOrder(
            listStr: ArrayList<String?>?,
            payTypeList: List<String?>?,
            isRefund: Int,
            page: Int,
            order: String?,
            startDate: String?
        )


        fun unifiedPayReverse(orderNo: String?)


        fun queryOrderByOrderNo(orderNo: String?)


        fun authPayQuery(
            orderNo: String?,
            outRequestNo: String?
        )
    }
}