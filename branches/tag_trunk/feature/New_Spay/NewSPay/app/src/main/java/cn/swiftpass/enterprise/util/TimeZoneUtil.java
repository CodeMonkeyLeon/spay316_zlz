package cn.swiftpass.enterprise.util;

import java.util.TimeZone;

/**
 * 
 * <一句话功能简述>
 * 得到系统时区
 * 
 * @author  he_hui
 * @version  [版本号, 2017-3-23]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class TimeZoneUtil
{
    public static String getCurrentTimeZone()
    {
        TimeZone tz = TimeZone.getDefault();
        return createGmtOffsetString(true, true, tz.getRawOffset());
    }
    
    public static String createGmtOffsetString(boolean includeGmt, boolean includeMinuteSeparator, int offsetMillis)
    {
        int offsetMinutes = offsetMillis / 60000;
        char sign = '+';
        if (offsetMinutes < 0)
        {
            sign = '-';
            offsetMinutes = -offsetMinutes;
        }
        StringBuilder builder = new StringBuilder(9);
        builder.append(sign);
        appendNumber(builder, 2, offsetMinutes / 60);
        return builder.toString();
    }
    
    private static void appendNumber(StringBuilder builder, int count, int value)
    {
        String string = Integer.toString(value);
        builder.append(string);
    }
}
