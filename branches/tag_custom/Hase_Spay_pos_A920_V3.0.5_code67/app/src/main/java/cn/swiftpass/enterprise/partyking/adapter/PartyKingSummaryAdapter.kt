package cn.swiftpass.enterprise.partyking.adapter

import android.content.Context
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import cn.swiftpass.enterprise.intl.R
import cn.swiftpass.enterprise.partyking.entity.PtkSummaryItemEntity
import cn.swiftpass.enterprise.partyking.interfaces.OnPtkSummaryItemClickListener
import cn.swiftpass.enterprise.utils.Logger
import cn.swiftpass.enterprise.utils.OnProhibitFastClickListener
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder

class PartyKingSummaryAdapter(val mContext: Context, val mList: List<PtkSummaryItemEntity>) :
    BaseMultiItemQuickAdapter<PtkSummaryItemEntity, BaseViewHolder>(mList) {


    companion object {
        const val TAG = "PartyKingSummaryAdapter"
    }


    private var mOnPtkSummaryItemClickListener: OnPtkSummaryItemClickListener? = null

    fun setOnPtkSummaryItemClickListener(listener: OnPtkSummaryItemClickListener) {
        mOnPtkSummaryItemClickListener = listener
    }


    init {
        addItemType(PtkSummaryItemEntity.TYPE_PTK_SUMMARY_DEFAULT, R.layout.item_pk_summary)
    }


    override fun convert(helper: BaseViewHolder?, item: PtkSummaryItemEntity?) {
        helper?.let { h ->
            Logger.d(
                TAG,
                "--- itemViewType : ${h.itemViewType}    position : ${h.position}    adapterPosition: ${h.adapterPosition}"
            )
            if (h.itemViewType == PtkSummaryItemEntity.TYPE_PTK_SUMMARY_DEFAULT) {
                item?.let {
                    setLayout(h, it, h.adapterPosition)
                }
            }
        }
    }


    private fun setLayout(helper: BaseViewHolder, item: PtkSummaryItemEntity, position: Int) {

        //设置内容
        helper.setText(R.id.id_tv_pk_summary_item_title, item.title)
        helper.setText(R.id.id_tv_pk_summary_item_time, item.time)
        helper.setText(R.id.id_tv_pk_summary_item_status, item.status)

        //设置状态字体颜色
        val statusTextView =
            helper.itemView.findViewById<TextView>(R.id.id_tv_pk_summary_item_status)
        when (item.statusType) {
            //成功
            PtkSummaryItemEntity.STATUS_TYPE_SUCCEED -> statusTextView.setTextColor(
                mContext.resources.getColor(
                    R.color.pk_summary_status_succeed
                )
            )
            //失败
            PtkSummaryItemEntity.STATUS_TYPE_FAILED -> statusTextView.setTextColor(
                mContext.resources.getColor(
                    R.color.pk_summary_status_failed
                )
            )
            //超时
            PtkSummaryItemEntity.STATUS_TYPE_TIMEOUT -> statusTextView.setTextColor(
                mContext.resources.getColor(
                    R.color.pk_summary_status_timeout
                )
            )
            //撤回
            PtkSummaryItemEntity.STATUS_TYPE_ROLLBACK -> statusTextView.setTextColor(
                mContext.resources.getColor(
                    R.color.pk_summary_status_rollback
                )
            )
        }


        //item点击事件
        val itemRelativeLayout =
            helper.itemView.findViewById<RelativeLayout>(R.id.id_rl_pk_summary_item_layout)
        itemRelativeLayout.setOnClickListener(object : OnProhibitFastClickListener() {
            override fun onFilterClick(v: View?) {
                mOnPtkSummaryItemClickListener?.let {
                    it.onPtkSummaryItemClick(v, position)
                }
            }
        })


    }
}