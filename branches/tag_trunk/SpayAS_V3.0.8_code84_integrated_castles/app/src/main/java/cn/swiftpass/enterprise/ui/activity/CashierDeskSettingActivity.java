package cn.swiftpass.enterprise.ui.activity;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.tencent.stat.StatService;

import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DataReportUtils;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * Created by aijingya on 2018/5/30.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(用一句话描述该文件做什么)
 * @date 2018/5/30.14:23.
 */

public class CashierDeskSettingActivity extends TemplateActivity{
    private static final String TAG = CashierDeskSettingActivity.class.getSimpleName();
    private EditText et_cashierDeskName;

    private ImageView iv_cashierDeskName;

    private Button bt_changeDeskName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cashier_desk_setting);
        String name = PreferenceUtil.getString("CashierDeskName", "");
        initView();
        if(!StringUtil.isEmptyOrNull(name)){
            et_cashierDeskName.setText(name);
            et_cashierDeskName.setSelection(name.length());
            iv_cashierDeskName.setVisibility(View.VISIBLE);
            setButtonBg(bt_changeDeskName, true, 0);

        }else{
            iv_cashierDeskName.setVisibility(View.GONE);
            setButtonBg(bt_changeDeskName, false, 0);
        }
    }


    private void initView(){
        et_cashierDeskName = getViewById(R.id.et_cashierDeskName);
        iv_cashierDeskName = getViewById(R.id.iv_cashierDeskName);
        bt_changeDeskName = getViewById(R.id.bt_changeDeskName);

        et_cashierDeskName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (et_cashierDeskName.getText().toString().trim().length() > 0)
                {
                    iv_cashierDeskName.setVisibility(View.VISIBLE);
                    setButtonBg(bt_changeDeskName, true, 0);
                    if (et_cashierDeskName.getText().toString().trim().length() >= 18){
                        closeKeybord(et_cashierDeskName,CashierDeskSettingActivity.this);
                    }
                    if (et_cashierDeskName.getText().toString().trim().length() >= 14){
                        et_cashierDeskName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                    }else{
                        et_cashierDeskName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                    }
                } else {
                    iv_cashierDeskName.setVisibility(View.GONE);
                    setButtonBg(bt_changeDeskName, false, 0);
                    et_cashierDeskName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                }
            }
        });

        iv_cashierDeskName.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                et_cashierDeskName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                et_cashierDeskName.setText("");
                PreferenceUtil.commitString("CashierDeskName", "");
            }
        });
        bt_changeDeskName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    StatService.trackCustomEvent(CashierDeskSettingActivity.this, "kMTASPayMeCashierDesk", "设置收银台”确定“按钮");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                // 1.携带参数的打点
                Bundle value1 = new Bundle();
                value1.putString("kGFASPayMeCashierDesk","设置收银台”确定“按钮");
                DataReportUtils.getInstance().report("kGFASPayMeCashierDesk",value1);

                if (StringUtil.isEmptyOrNull(et_cashierDeskName.getText().toString().trim()))
                {
                    toastDialog(CashierDeskSettingActivity.this, R.string.tx_surname_notnull, null);
                    return;
                }
                submitData(et_cashierDeskName.getText().toString().trim());
            }
        });

    }

    /**
     * 关闭软键盘
     *
     */
    public static void closeKeybord(EditText mEditText, Context mContext) {
        InputMethodManager imm = (InputMethodManager) mContext
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEditText.getWindowToken(), 0);
    }

    @Override
    public void setButtonBg(Button b, boolean enable, int res) {
        super.setButtonBg(b, enable, res);
        if (enable) {
            b.setEnabled(true);
            b.setTextColor(getResources().getColor(R.color.white));
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_nor_shape);
        } else {
            b.setEnabled(false);
            if (res > 0) {
                b.setText(res);
            }
            b.setBackgroundResource(R.drawable.btn_press_shape);
            b.setTextColor(getResources().getColor(R.color.bt_enable));
            b.getBackground().setAlpha(102);
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            CashierDeskSettingActivity.this.finish();
        }

        return super.onKeyDown(keyCode, event);

    }

    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.cashier_desk_settine);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {

            @Override
            public void onRightLayClick()
            {

            }

            @Override
            public void onRightButtonClick()
            {

            }

            @Override
            public void onRightButLayClick()
            {

            }

            @Override
            public void onLeftButtonClick()
            {
                CashierDeskSettingActivity.this.finish();
            }
        });
    }

    private void submitData(String DeskName){
        PreferenceUtil.commitString("CashierDeskName", DeskName);
        Toast.makeText(CashierDeskSettingActivity.this,getResources().getString(R.string.save_success),Toast.LENGTH_SHORT).show();
        finish();
    }
}
