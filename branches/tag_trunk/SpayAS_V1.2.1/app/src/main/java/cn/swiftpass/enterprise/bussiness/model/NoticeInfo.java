/*
 * 文 件 名:  ShopBaseDataInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2014-3-20
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

import cn.swiftpass.enterprise.io.database.table.NoticeActiveDataTab;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2014-3-20]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
@DatabaseTable(tableName = NoticeActiveDataTab.TABLE_NAME_ACTIVE)
public class NoticeInfo implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    @DatabaseField(generatedId = true)
    public long id;
    
    @DatabaseField(columnName = NoticeActiveDataTab.title)
    public String title;
    
    @DatabaseField(columnName = NoticeActiveDataTab.state)
    public Integer state;
    
    /**
     * @return 返回 startTime
     */
    public String getStartTime()
    {
        return startTime;
    }
    
    /**
     * @param 对startTime进行赋值
     */
    public void setStartTime(String startTime)
    {
        this.startTime = startTime;
    }
    
    @DatabaseField(columnName = NoticeActiveDataTab.START_TIME)
    public String startTime;
    
    @DatabaseField(columnName = NoticeActiveDataTab.END_TIME)
    public String endTime;
    
    /**
     * @return 返回 title
     */
    public String getTitle()
    {
        return title;
    }
    
    /**
     * @param 对title进行赋值
     */
    public void setTitle(String title)
    {
        this.title = title;
    }
    
    /**
     * @return 返回 endTime
     */
    public String getEndTime()
    {
        return endTime;
    }
    
    /**
     * @param 对endTime进行赋值
     */
    public void setEndTime(String endTime)
    {
        this.endTime = endTime;
    }
    
    /**
     * @return 返回 noticeContentUrl
     */
    public String getNoticeContentUrl()
    {
        return noticeContentUrl;
    }
    
    /**
     * @param 对noticeContentUrl进行赋值
     */
    public void setNoticeContentUrl(String noticeContentUrl)
    {
        this.noticeContentUrl = noticeContentUrl;
    }
    
    /**
     * @return 返回 noticeId
     */
    public String getNoticeId()
    {
        return noticeId;
    }
    
    /**
     * @param 对noticeId进行赋值
     */
    public void setNoticeId(String noticeId)
    {
        this.noticeId = noticeId;
    }
    
    @DatabaseField(columnName = NoticeActiveDataTab.noticeContentUrl)
    public String noticeContentUrl;
    
    @DatabaseField(columnName = NoticeActiveDataTab.noticeId)
    public String noticeId;
    //    @DatabaseField(columnName = ActiveDataTab.END_TIME)
    //    public String endTime;
    
}
