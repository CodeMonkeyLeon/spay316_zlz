package cn.swiftpass.enterprise.mvp.contract.activity

import cn.swiftpass.enterprise.bussiness.model.ECDHInfo
import cn.swiftpass.enterprise.mvp.base.BasePresenter
import cn.swiftpass.enterprise.mvp.base.BaseView

/**
 * @author lizheng.zhao
 * @date 2022/11/17
 *
 * @你一会看我
 * @一会看云
 * @我觉得你看我时很远
 * @你看云时很近
 */
class ChangePwdContract {

    interface View : BaseView {

        fun changePwdSuccess(response: Boolean)

        fun changePwdFailed(error: Any?)


        fun ecdhKeyExchangeSuccess(response: ECDHInfo?)

        fun ecdhKeyExchangeFailed(error: Any?)

    }


    interface Presenter : BasePresenter<View> {


        /**
         * 客户端和服务器公钥交换的接口
         */
        fun ecdhKeyExchange(publicKey: String?)


        /**
         * 修改密码
         */
        fun changePwd(
            oldPwd: String?,
            phone: String?,
            newPwd: String?,
            repPassword: String?
        )

    }

}