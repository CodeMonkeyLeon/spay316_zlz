package cn.swiftpass.enterprise.ui.activity.settle;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.logica.user.RefundManagerActitvity;
import cn.swiftpass.enterprise.bussiness.logica.user.UserManager;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.BaseActivity;
import cn.swiftpass.enterprise.ui.activity.user.CashierAddActivity;
import cn.swiftpass.enterprise.ui.widget.PullDownListView;

public class SettleCashierManager extends BaseActivity
{
    //private PullDownListView cashier_list;
    
    private ViewHolder holder;
    
    private ListView listView;
    
    //private List<UserModel> list = new ArrayList<UserModel>();
    
    private List<UserModel> listUser;
    
    private Handler mHandler = new Handler();
    
    //private CashierAdapter cashierAdapter;
    
    //private int pageFulfil = 0;
    
    //private int pageCount = 0;
    
    //private EditText et_input;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settle_cashier_manager);
        
        listUser = new ArrayList<UserModel>();
        //cashierAdapter = new CashierAdapter(listUser);
        listView = getViewById(R.id.cashier_manager_id);
        //et_input = getViewById(R.id.et_input);
        
        listView.setOnItemClickListener(new OnItemClickListener()
        {
            
            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3)
            {
                try
                {
                    UserModel userModel = listUser.get(position - 1);
                    if (userModel != null)
                    {
                        
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("userModel", userModel);
                        showPage(CashierAddActivity.class, bundle);
                    }
                    
                }
                catch (Exception e)
                {
                    UserModel userModel = listUser.get(position);
                    if (userModel != null)
                    {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("userModel", userModel);
                        showPage(RefundManagerActitvity.class, bundle);
                    }
                }
                //                finish();
            }
        });
        
        loadData(0, false);
        
    }
    
    /** {@inheritDoc} */
    
    @Override
    protected void onResume()
    {
        // TODO Auto-generated method stub
        super.onResume();
        
    }
    
    /** <一句话功能简述>
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void loadData(final int page, final boolean isLoadMore)
    {
        
        UserManager.queryCashier(page, 10, null, new UINotifyListener<List<UserModel>>()
        {
            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                if (!isLoadMore)
                {
                    showLoading(false, R.string.public_data_loading);
                }
            }
            
            @Override
            public void onError(final Object object)
            {
                super.onError(object);
                dismissLoading();
                if (checkSession())
                {
                    return;
                }
                if (object != null)
                {
                    showToastInfo(object.toString());
                }
            }
            
            @Override
            public void onSucceed(List<UserModel> result)
            {
                super.onSucceed(result);
                dismissLoading();
                
                if (result != null && result.size() > 0)
                {
                    listView.setAdapter(new CashierAdapter(result));
                    
                }
                
            }
        });
        
    }
    
//    private void mySetListData(final PullDownListView pull, List<UserModel> list, CashierAdapter adapter,
//        List<UserModel> result, boolean isLoadMore)
//    {
//        if (!isLoadMore)
//        {
//            pull.onfinish(getStringById(R.string.refresh_successfully));// 刷新完成
//
//            mHandler.postDelayed(new Runnable()
//            {
//                @Override
//                public void run()
//                {
//                    // 这里表示刷新处理完成后把上面的加载刷新界面隐藏
//                    pull.onRefreshComplete();
//                }
//            }, 800);
//
//            list.clear();
//        }
//        else
//        {
//            mHandler.postDelayed(new Runnable()
//            {
//
//                @Override
//                public void run()
//                {
//                    pull.onLoadMoreComplete();
//                    // pull.setMore(true);
//                }
//            }, 1000);
//        }
//        list.addAll(result);
//        adapter.notifyDataSetChanged();
//
//    }
//
    private class CashierAdapter extends BaseAdapter
    {
        
        private List<UserModel> userModels;
        
        public CashierAdapter()
        {
        }
        
        public CashierAdapter(List<UserModel> userModels)
        {
            this.userModels = userModels;
        }
        
        @Override
        public int getCount()
        {
            return userModels.size();
        }
        
        @Override
        public Object getItem(int position)
        {
            return userModels.get(position);
        }
        
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = View.inflate(SettleCashierManager.this, R.layout.settl_cashier_list_item, null);
                holder = new ViewHolder();
                holder.cashier_state = (TextView)convertView.findViewById(R.id.cashier_state);
                holder.cashier_store = (TextView)convertView.findViewById(R.id.cashier_store);
                holder.userId = (TextView)convertView.findViewById(R.id.userId);
                holder.userName = (TextView)convertView.findViewById(R.id.userName);
                holder.usertel = (TextView)convertView.findViewById(R.id.usertel);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder)convertView.getTag();
            }
            
            UserModel userModel = userModels.get(position);
            if (userModel.getDeptname() != null)
            {
                holder.cashier_store.setText(userModel.getDeptname());
            }
            
            //            if (userModel.getEnabled() != null && userModel.getEnabled().equals("true"))
            //            {
            //                holder.cashier_state.setText("(" + getString(R.string.open) + ")");
            //            }
            //            else
            //            {
            //                holder.cashier_state.setText("(" + getString(R.string.tx_frozen) + ")");
            //            }
            holder.userId.setText(String.valueOf(userModel.getId()));
            if (userModel.getPhone() != null)
            {
                holder.usertel.setText(userModel.getPhone());
            }
            
            if (userModel.getRealname() != null)
            {
                holder.userName.setText(userModel.getRealname());
            }
            
            return convertView;
        }
    }
    
    private class ViewHolder
    {
        private TextView userId, cashier_state, userName, usertel, cashier_store;
        
    }
    
    //    @Override
    //    public void onRefresh()
    //    {
    //        pageFulfil = 0;
    //        loadData(pageFulfil, false);
    //    }
    //    
    //    @Override
    //    public void onLoadMore()
    //    {
    //        pageFulfil = pageFulfil + 1;
    //        Log.i("hehui", "pageFulfil-pageCount->" + pageFulfil + ",pageCount-->" + pageCount);
    //        if (pageFulfil <= pageCount)
    //        {
    //            loadData(pageFulfil, true);
    //        }
    //    }
    
}
