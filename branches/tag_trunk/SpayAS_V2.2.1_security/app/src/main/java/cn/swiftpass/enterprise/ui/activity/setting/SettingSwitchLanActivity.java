package cn.swiftpass.enterprise.ui.activity.setting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Locale;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.SettingMoreActivity;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.spayMainTabActivity;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 设置 会自动检查版本 User: ALAN Date: 13-10-28 Time: 下午5:11 To change this template use
 * File | Settings | File Templates.
 */
public class SettingSwitchLanActivity extends TemplateActivity
{

    //private Context mContext;

    private LinearLayout ly_close, ly_open, ly_china,ll_japanesee;

    private ImageView iv_open, iv_close, iv_china,iv_japan;

    private TextView tv_en, tv_tw, tv_ch,tv_japan;

    void getBase()
    {
        LocalAccountManager.getInstance().getBase(new UINotifyListener<Boolean>()
        {
            @Override
            public void onError(Object object)
            {
                // TODO Auto-generated method stub
                super.onError(object);
                dismissLoading();
                if (checkSession())
                {
                    return;
                }
                finish();

                Intent it = new Intent();
                it.setClass(SettingSwitchLanActivity.this, spayMainTabActivity.class);
                startActivity(it);
            }

            @Override
            public void onPreExecute()
            {
                super.onPreExecute();
                showLoading(false, R.string.public_loading);
            }

            @Override
            public void onSucceed(Boolean result)
            {
                // TODO Auto-generated method stub
                super.onSucceed(result);
                dismissLoading();
                Intent it = new Intent();
                it.setClass(SettingSwitchLanActivity.this, spayMainTabActivity.class);
                startActivity(it);
                for (Activity a : MainApplication.allActivities)
                {
                    a.finish();
                }

            }

        });

    }

    private void initViews()
    {
        setContentView(R.layout.activity_setting_switch_lan);
        //mContext = this;
        tv_tw = getViewById(R.id.tv_tw);
        tv_en = getViewById(R.id.tv_en);
        tv_ch = getViewById(R.id.tv_ch);
        tv_japan = getViewById(R.id.tv_japan);

        tv_ch.setText("中文(简体)");
        tv_tw.setText("中文(繁體)");
        tv_en.setText("English");
        tv_japan.setText("日本語");

        iv_china = getViewById(R.id.iv_china);
        ly_china = getViewById(R.id.ly_china);
        iv_open = getViewById(R.id.iv_open);
        iv_close = getViewById(R.id.iv_close);
        ly_close = getViewById(R.id.ly_close);
        ly_open = getViewById(R.id.ly_open);
        iv_japan = getViewById(R.id.iv_japan);
        ll_japanesee = getViewById(R.id.ll_japanese);

        String language = PreferenceUtil.getString("language", "");

        //        Locale locale = MainApplication.getContext().getResources().getConfiguration().locale; //zh-rHK
        //        String lan = locale.getCountry();

        if (!StringUtil.isEmptyOrNull(language))
        {
            if (language.equals(MainApplication.LANG_CODE_ZH_TW))
            {
                iv_open.setVisibility(View.GONE);
                iv_china.setVisibility(View.GONE);
                iv_close.setVisibility(View.VISIBLE);
                iv_japan.setVisibility(View.GONE);

            } else if (language.equals(MainApplication.LANG_CODE_ZH_CN)) {
                iv_open.setVisibility(View.GONE);
                iv_close.setVisibility(View.GONE);
                iv_china.setVisibility(View.VISIBLE);
                iv_japan.setVisibility(View.GONE);

            }else if(language.equals(MainApplication.LANG_CODE_JA_JP)){
                iv_open.setVisibility(View.GONE);
                iv_close.setVisibility(View.GONE);
                iv_china.setVisibility(View.GONE);
                iv_japan.setVisibility(View.VISIBLE);
            } else {
                iv_open.setVisibility(View.VISIBLE);
                iv_close.setVisibility(View.GONE);
                iv_china.setVisibility(View.GONE);
                iv_japan.setVisibility(View.GONE);
            }
        } else {
            String lan = Locale.getDefault().toString();
            if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK)
                    || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO)
                    || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW)
                    ||lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_HANT)) {

                iv_open.setVisibility(View.GONE);
                iv_china.setVisibility(View.GONE);
                iv_close.setVisibility(View.VISIBLE);
                iv_japan.setVisibility(View.GONE);
            } else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_HANS)) {

                iv_open.setVisibility(View.GONE);
                iv_close.setVisibility(View.GONE);
                iv_china.setVisibility(View.VISIBLE);
                iv_japan.setVisibility(View.GONE);
            }else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_JA_JP)) {
                iv_open.setVisibility(View.GONE);
                iv_close.setVisibility(View.GONE);
                iv_china.setVisibility(View.GONE);
                iv_japan.setVisibility(View.VISIBLE);
            } else {
                iv_open.setVisibility(View.VISIBLE);
                iv_close.setVisibility(View.GONE);
                iv_china.setVisibility(View.GONE);
                iv_japan.setVisibility(View.GONE);
            }

        }

        ly_china.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                iv_china.setVisibility(View.VISIBLE);
                iv_open.setVisibility(View.GONE);
                iv_close.setVisibility(View.GONE);
                iv_japan.setVisibility(View.GONE);

                switchLanguage(MainApplication.LANG_CODE_ZH_CN);

                getBase();
            }
        });

        ly_open.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                iv_open.setVisibility(View.VISIBLE);
                iv_close.setVisibility(View.GONE);
                iv_china.setVisibility(View.GONE);
                iv_japan.setVisibility(View.GONE);

                switchLanguage(MainApplication.LANG_CODE_EN_US);

                getBase();


            }
        });

        ly_close.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                iv_open.setVisibility(View.GONE);
                iv_close.setVisibility(View.VISIBLE);
                iv_china.setVisibility(View.GONE);
                iv_japan.setVisibility(View.GONE);

                switchLanguage(MainApplication.LANG_CODE_ZH_TW);

                getBase();
            }
        });

        ll_japanesee.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                iv_open.setVisibility(View.GONE);
                iv_close.setVisibility(View.GONE);
                iv_china.setVisibility(View.GONE);
                iv_japan.setVisibility(View.VISIBLE);

                switchLanguage(MainApplication.LANG_CODE_JA_JP);

                getBase();
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        initViews();

    }

    @Override
    protected void onResume()
    {
        super.onResume();

    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }

    @Override
    protected boolean isLoginRequired()
    {
        return false;
    }

    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        titleBar.setLeftButtonVisible(true);
        titleBar.setTitle(R.string.title_common_yuyan);
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
            }

            @Override
            public void onRightButtonClick()
            {
            }

            @Override
            public void onRightLayClick()
            {

            }

            @Override
            public void onRightButLayClick()
            {

            }
        });
    }
}