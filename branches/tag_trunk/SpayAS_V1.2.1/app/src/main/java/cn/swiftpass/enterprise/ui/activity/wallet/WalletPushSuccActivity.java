/*
 * 文 件 名:  WalletMainActivity.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2016-11-1
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity.wallet;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.WalletModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.ui.activity.TemplateActivity;
import cn.swiftpass.enterprise.ui.activity.total.PayDistributionActivity;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 钱包 提现成功界面
 * 
 * @author  he_hui
 * @version  [版本号, 2016-11-1]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class WalletPushSuccActivity extends TemplateActivity
{
    
    private Button btn_next_step;
    
    private TextView tx_peop, tv_moeny, tv_bank_name, tv_fail, tv_push_succ, tv_order;
    
    private EditText et_id;
    
    private ImageView iv_prompt;
    
    private LinearLayout lay_stream;
    
    private TextView tv_query;
    
    private LinearLayout ly_fail;
    
    private String errorCode;// 2 需要跳转到流水界面
    
    public static void startActivity(Context context, WalletModel walletSucc, String fail)
    {
        Intent it = new Intent();
        it.setClass(context, WalletPushSuccActivity.class);
        it.putExtra("walletSucc", walletSucc);
        it.putExtra("walletFail", fail);
        context.startActivity(it);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.push_money_succeed);
        
        initView();
        setLister();
        
        WalletModel walletModel = (WalletModel)getIntent().getSerializableExtra("walletSucc");
        String walletFail = getIntent().getStringExtra("walletFail");
        if (walletModel != null)
        {
            tv_bank_name.setText(walletModel.getBankName());
            tv_moeny.setText(MainApplication.getFeeFh() + " "
                + DateUtil.formatMoneyUtils(Double.parseDouble(walletModel.getMoney())));
            if (!StringUtil.isEmptyOrNull(walletModel.getOrder_no()))
            {
                lay_stream.setVisibility(View.VISIBLE);
                tv_order.setText(walletModel.getOrder_no());
            }
        }
        if (!StringUtil.isEmptyOrNull(walletFail))
        {
            errorCode = walletFail;
            if (walletFail.equals("1"))
            {//转出失败
            
                iv_prompt.setImageResource(R.drawable.icon_prompt_warning);
                tv_push_succ.setText(R.string.tv_push_fail);
                ly_fail.setVisibility(View.GONE);
            }
            else if (walletFail.equalsIgnoreCase("timeout"))
            {//超时
                errorCode = "2";
                ly_fail.setVisibility(View.VISIBLE);
                iv_prompt.setImageResource(R.drawable.icon_prompt_timeout);
                tv_push_succ.setText(R.string.tv_push_outtime);
                btn_next_step.setText(R.string.tv_push_query);
            }
            
        }
        else
        {
            ly_fail.setVisibility(View.GONE);
        }
    }
    
    private void setLister()
    {
        btn_next_step.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                WalletPushSuccActivity.this.finish();
                HandlerManager.notifyMessage(HandlerManager.WALLET_REFERSH, HandlerManager.WALLET_REFERSH);
                if (MainApplication.listActivities.size() > 0)
                {
                    for (Activity activity : MainApplication.listActivities)
                    {
                        activity.finish();
                    }
                }
                
                if (!StringUtil.isEmptyOrNull(errorCode) && errorCode.equals("2"))
                {
                    //调转到提现流水界面
                    showPage(WalletPushStreamActivity.class);
                }
            }
        });
    }
    
    private void initView()
    {
        tv_query = getViewById(R.id.tv_query);
        tv_query.setText("\"" + getString(R.string.tv_push_query) + "\"");
        tv_fail = getViewById(R.id.tv_fail);
        btn_next_step = getViewById(R.id.dataBut);
        tx_peop = getViewById(R.id.tx_peop);
        et_id = getViewById(R.id.et_id);
        tv_bank_name = getViewById(R.id.tv_bank_name);
        tv_moeny = getViewById(R.id.tv_moeny);
        iv_prompt = getViewById(R.id.iv_prompt);
        tv_push_succ = getViewById(R.id.tv_push_promt);
        lay_stream = getViewById(R.id.lay_stream);
        tv_order = getViewById(R.id.tv_order);
        ly_fail = getViewById(R.id.ly_fail);
    }
    
    @Override
    protected void setupTitleBar()
    {
        super.setupTitleBar();
        
        titleBar.setTitle(R.string.tx_apply_detail);
        titleBar.setLeftButtonIsVisible(true);
        titleBar.setLeftButtonVisible(true);
        titleBar.setRightButLayVisibleForTotal(false, getString(R.string.tx_apply_balance_list));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener()
        {
            @Override
            public void onLeftButtonClick()
            {
                finish();
                
                HandlerManager.notifyMessage(HandlerManager.WALLET_REFERSH, HandlerManager.WALLET_REFERSH);
                if (MainApplication.listActivities.size() > 0)
                {
                    for (Activity activity : MainApplication.listActivities)
                    {
                        activity.finish();
                    }
                }
            }
            
            @Override
            public void onRightButtonClick()
            {
                
            }
            
            @Override
            public void onRightLayClick()
            {
                
            }
            
            @Override
            public void onRightButLayClick()
            {
                showPage(PayDistributionActivity.class);
            }
        });
    }
}
