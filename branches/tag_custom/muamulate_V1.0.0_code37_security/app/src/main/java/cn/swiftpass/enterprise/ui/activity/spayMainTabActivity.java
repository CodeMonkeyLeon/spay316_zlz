package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;



import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.DrawerContentModel;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.ui.activity.bill.OrderSearchActivity;
import cn.swiftpass.enterprise.ui.activity.bill.ReportActivity;
import cn.swiftpass.enterprise.ui.activity.print.BluePrintUtil;
import cn.swiftpass.enterprise.ui.adapter.DrawerContentAdapter;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabBill;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabPay;
import cn.swiftpass.enterprise.ui.fmt.FragmentTabSetting;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.MyToast;
import cn.swiftpass.enterprise.ui.widget.SpayTitleView;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;

/**
 * Created by aijingya on 2018/4/26.
 *
 * @Package cn.swiftpass.enterprise.ui.activity
 * @Description: ${TODO}(新的MainTab页面)
 * @date 2018/4/26.14:32.
 */

public class spayMainTabActivity extends FragmentActivity {
    private static final String TAG = spayMainTabActivity.class.getSimpleName();

    private Context mContext;

    SpayTitleView spay_title_view;
    private FrameLayout mContent_view;
    private RadioGroup mRadioGroup_bottom_view;
    private RadioButton mHomePay;
    private RadioButton mHomeBill;
    private RadioButton mHomeSetting;
    private Fragment FragmentPay;
    private Fragment FragmentBill;
    private Fragment FragmentSetting;

    private int tabIds[] = new int[]{
            R.id.id_tab_pay,
            R.id.id_tab_bill,
            R.id.id_tab_setting
    };

    boolean isOpen = true, isClose = true, isOpening = true, isCloseing = true;
    /**
     * 连接默认上一次
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    void connentBlue() {
        boolean connState = PreferenceUtil.getBoolean("connState" + ApiConstant.bankCode, true);
        if (connState && !StringUtil.isEmptyOrNull(MainApplication.getBlueDeviceAddress())) {
            //异步去连接蓝牙
            new Thread(new Runnable() {
                @Override
                public void run() {
                    boolean isConnect = BluePrintUtil.blueConnent(MainApplication.getBlueDeviceAddress());
                    Logger.i(TAG, " connentBlue isConnect-->" + isConnect);
                    if (!isConnect) {
                        MainApplication.setBlueState(false);
                        //                        MainApplication.bluetoothSocket = null;
                        //                        MainApplication.setBlueDeviceName("");
                        //                        MainApplication.setBlueDeviceNameAddress("");
                        //                        PreferenceUtil.commitBoolean("connState" + MainApplication.getMchId(), false);
                        HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET, HandlerManager.BLUE_CONNET);
                    } else {
                        MainApplication.setBlueState(true);
                    }
                }
            }).start();
        }
    }

    protected void switchLanguage(String language) {
        //设置应用语言类型
        Resources resources = getResources();
        Configuration config = resources.getConfiguration();
        DisplayMetrics dm = resources.getDisplayMetrics();

        if (!TextUtils.isEmpty(language) && language.equals(MainApplication.LANG_CODE_ZH_TW)) // 繁体
        {
            config.locale = Locale.TRADITIONAL_CHINESE;
        } else if (!TextUtils.isEmpty(language) && language.equals(MainApplication.LANG_CODE_EN_US)) {
            config.locale = Locale.ENGLISH;
        } else if (!TextUtils.isEmpty(language) && language.equals(MainApplication.LANG_CODE_ZH_CN)) {
            config.locale = Locale.SIMPLIFIED_CHINESE;
        } else { // 跟随系统
            config.locale = Locale.getDefault(); //默认英文
        }
        Logger.i("hehui", "Locale.getDefault()-->" + Locale.getDefault());
        if (!StringUtil.isEmptyOrNull(language)) {
            PreferenceUtil.commitString("language", language);
        } else {
            Locale locale = getResources().getConfiguration().locale;
            String lan = locale.getLanguage() + "-" + locale.getCountry();
//            String lan = getResources().getConfiguration().locale.getCountry();//英文 返回为空  中文 CN TW
//            String lan = Locale.getDefault().toString();
            Logger.i("hehui", "lan-->" + lan);//  zh-HK  en-HK   zh-CN   en-
            if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_NEW) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_CN_HANS)) {
                //                PreferenceUtil.commitString("language", MainApplication.LANG_CODE_ZH_CN);
                config.locale = Locale.SIMPLIFIED_CHINESE;
            } else if (lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_NEW) || lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_MO_NEW) ||
                    lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_TW_NEW)|| lan.equalsIgnoreCase(MainApplication.LANG_CODE_ZH_HK_HANT)) {
                Logger.i("hehui", "LANG_CODE_ZH_HK-->");
                config.locale = Locale.TRADITIONAL_CHINESE;
            } else {
                config.locale = Locale.ENGLISH;
            }
        }
        resources.updateConfiguration(config, dm);
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //初始化PreferenceUtil
        PreferenceUtil.init(this);
        //根据上次的语言设置，重新设置语言
        switchLanguage(PreferenceUtil.getString("language",
                ""));
        //group = this;

        mContext = MainApplication.getContext();
        MainApplication.getContext().setLastActivityRef(new WeakReference<Activity>((Activity) this));
        requestWindowFeature(Window.FEATURE_NO_TITLE);



        MainApplication.allActivities.add(this);

        setContentView(R.layout.activity_main_tab);
//        initData();
        initViews();
        initEvents();

        registerBlueReceiver();

        Intent intent = getIntent();
        //从intent对象中把封装好的数据取出来
        String billTab = intent.getStringExtra("tab_index");
        if(billTab != null && billTab.equals("bill")){//如果点击的是要进入账单页面
            setSelection(1);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);


    }

    /*private void initData() {
        mList.add(new DrawerContentModel(R.drawable.icon_fixedqrcode, getString(R.string.drawer_fixed_code), 1));
        mList.add(new DrawerContentModel(R.drawable.icon_management,  getString(R.string.drawer_managerment), 2));
        mList.add(new DrawerContentModel(R.drawable.icon_support,  getString(R.string.drawer_Support), 3));
        mList.add(new DrawerContentModel(R.drawable.icon_setting,  getString(R.string.drawer_setting), 4));
    }*/

    //初始化控件
    public void initViews(){
        spay_title_view = (SpayTitleView) findViewById(R.id.spay_title_view);
        mContent_view = (FrameLayout)findViewById(R.id.fl_content);
        mRadioGroup_bottom_view = (RadioGroup)findViewById(R.id.rg_bottom_view);

        mHomePay = (RadioButton)findViewById(R.id.id_tab_pay);
        mHomeBill = (RadioButton)findViewById(R.id.id_tab_bill);
        mHomeSetting = (RadioButton)findViewById(R.id.id_tab_setting);
    }

    @Override
    public void onAttachFragment(Fragment fragment){
        //当前的界面的保存状态，只是从新让新的Fragment指向了原本未被销毁的fragment，它就是onAttach方法对应的Fragment对象
        if(FragmentPay == null && fragment instanceof FragmentTabPay){
            FragmentPay = (FragmentTabPay)fragment;
        }else if(FragmentBill == null && fragment instanceof FragmentTabBill){
            FragmentBill = (FragmentTabBill)fragment;
        }else if(FragmentSetting == null && fragment instanceof FragmentTabSetting){
            FragmentSetting = (FragmentTabSetting)fragment;
        }
    }

    private void registerBlueReceiver() {
        IntentFilter filterBlue = new IntentFilter();
        filterBlue.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filterBlue.addAction(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
        try {
            registerReceiver(blueReceiver, filterBlue);
        } catch (Exception e) {
            Logger.e("hehui", "" + e);
        }
    }

    public SpayTitleView getTitleView() {
        return spay_title_view;
    }

    public void cleanTitleAllView() {
        if (spay_title_view != null){
            spay_title_view.cleanAllView();
        }
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        Log.d(TAG,"onDestroy");
        if (blueReceiver != null) {
            unregisterReceiver(blueReceiver);
        }

        // 通知释放空间
        //System.gc();
        Runtime.getRuntime().gc();

        android.os.Debug.stopMethodTracing();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        Log.d(TAG,"onPause");

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        Log.d(TAG,"onResume");

    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        Log.d(TAG, "onStop");
    }



    public void initEvents(){
        mRadioGroup_bottom_view.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {


                for (int i = 0; i < tabIds.length; i++) {
                    if (tabIds[i] == checkedId) {
                        setSelection(i);
                        break;
                    }
                }
            }
        });
        setSelection(0);

    }

    private void setSelection(int position){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction mfragmentTransaction = fm.beginTransaction();
        hideAllFragments(mfragmentTransaction);

        switch (position){
            case 0:
                mHomePay.setSelected(true);
                mHomePay.setTextColor(getResources().getColor(R.color.bg_text_new));
                initPayTitle();
                if (FragmentPay == null) {
                    FragmentPay = new FragmentTabPay();
                    mfragmentTransaction.add(R.id.fl_content, FragmentPay);
                }else {
                    mfragmentTransaction.show(FragmentPay);
                }
                break;
            case 1:
                mHomeBill.setSelected(true);
                mHomeBill.setTextColor(getResources().getColor(R.color.bg_text_new));
                initBillTitle();



                // 如果是收银登录进来
                if(MainApplication.isAdmin.equals("0")){
                    clickBillTab(mfragmentTransaction);
                }else{
                    if (!MainApplication.remark.equals("") && !MainApplication.remark.equals("1")) {
                        MyToast toast = new MyToast();
                        toast.showToast(getApplicationContext(), ToastHelper.toStr(R.string.show_mch_stream));
                    } else if (MainApplication.remark.equals("0")) {
                        if (!MainApplication.isOrderAuth.equals("") && MainApplication.isOrderAuth.equals("1")) {
                            clickBillTab(mfragmentTransaction);
                        }
                    } else {
                        clickBillTab(mfragmentTransaction);
                    }
                }
                break;
            case 2:

                mHomeSetting.setSelected(true);
                mHomeSetting.setTextColor(getResources().getColor(R.color.bg_text_new));
                initSettingTitle();
                if (FragmentSetting == null) {
                    FragmentSetting = new FragmentTabSetting();
                    mfragmentTransaction.add(R.id.fl_content, FragmentSetting);
                }else {
                    mfragmentTransaction.show(FragmentSetting);
                }
                break;
            default:
                break;
        }
        mfragmentTransaction.commit();
    }

    public void initPayTitle(){
        cleanTitleAllView();
    }

    public void initBillTitle(){
        cleanTitleAllView();

        getTitleView().addLeftImageView(R.drawable.icon_general_bill_search, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPage(OrderSearchActivity.class);
            }
        });

        getTitleView().addMiddleTextView(getResources().getString(R.string.tab_bill_title));

        TextView textView = getTitleView().addRightTextView(getResources().getString(R.string.tx_sum), new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //流水
                showPage(ReportActivity.class);

            }
        });
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
    }

    public void initSettingTitle(){
        cleanTitleAllView();
        getTitleView().addMiddleTextView(getResources().getString(R.string.tab_setting_title));
    }

    public void showPage(Class clazz) {
        Intent intent = new Intent(mContext, clazz);
        this.startActivity(intent);
    }

    private void clickBillTab(FragmentTransaction mfragmentTransaction){
        HandlerManager.notifyMessage(HandlerManager.STREAM_BILL, HandlerManager.PAY_SWITCH_TAB);
        if (FragmentBill == null) {
            FragmentBill = new FragmentTabBill();
            mfragmentTransaction.add(R.id.fl_content, FragmentBill);
        }else {
            mfragmentTransaction.show(FragmentBill);
        }
    }

    private void hideAllFragments(FragmentTransaction ft){
        if (FragmentPay != null) {
            ft.hide(FragmentPay);
            mHomePay.setSelected(false);
            mHomePay.setTextColor(getResources().getColor(R.color.tab_text_nor));
        }

        if (FragmentBill != null) {
            ft.hide(FragmentBill);
            mHomeBill.setSelected(false);
            mHomeBill.setTextColor(getResources().getColor(R.color.tab_text_nor));
        }

        if (FragmentSetting != null) {
            ft.hide(FragmentSetting);
            mHomeSetting.setSelected(false);
            mHomeSetting.setTextColor(getResources().getColor(R.color.tab_text_nor));
        }
    }

    /**
     * 实时监听蓝牙广播
     */
    private final BroadcastReceiver blueReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
            switch (state) {
                case BluetoothAdapter.STATE_OFF:
                    MainApplication.setBlueState(false);
                    if (isClose) {
                        isClose = false;
                    }
                    MainApplication.bluetoothSocket = null;
                    HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET_STUTS, HandlerManager.BLUE_CONNET_STUTS_CLOSED);
                    break;
                case BluetoothAdapter.STATE_TURNING_OFF:
                    MainApplication.setBlueState(false);
                    MainApplication.bluetoothSocket = null;
                    if (isCloseing) {
                        isCloseing = false;

                    }
                    HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET_STUTS, HandlerManager.BLUE_CONNET_STUTS_CLOSED);
                    break;
                case BluetoothAdapter.STATE_ON:
                    MainApplication.setBlueState(true);
                    if (isOpen) {
                        isOpen = false;
                    }
                    try {
                        connentBlue();
                    } catch (Exception e) {
                        Log.e(TAG, Log.getStackTraceString(e));
                    }
                    HandlerManager.notifyMessage(HandlerManager.BLUE_CONNET_STUTS, HandlerManager.BLUE_CONNET_STUTS);
                    break;
                case BluetoothAdapter.STATE_TURNING_ON:
                    if (isOpening) {
                        isOpening = false;

                    }
                    break;
            }

        }

    };

    @Override
    public boolean onKeyDown(int keycode, KeyEvent event) {
        if (keycode == KeyEvent.KEYCODE_BACK) {
            showExitDialog(this);
        }
        return super.onKeyDown(keycode, event);
    }

    private DialogInfo dialogInfo;
    public void showExitDialog(final Activity context) {
        dialogInfo = new DialogInfo(context, getString(R.string.public_cozy_prompt), getString(R.string.show_sign_out), getString(R.string.btnOk), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                finish();
                MainApplication.getContext().exit();
//                                System.exit(0);
                MainApplication.isSessionOutTime = true;

            }

            @Override
            public void handleCancleBtn() {
                dialogInfo.cancel();
            }
        }, null);

        DialogHelper.resize(context, dialogInfo);
        dialogInfo.show();
    }



}
