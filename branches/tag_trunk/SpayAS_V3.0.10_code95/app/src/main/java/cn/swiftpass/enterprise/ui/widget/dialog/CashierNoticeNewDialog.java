package cn.swiftpass.enterprise.ui.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import cn.swiftpass.enterprise.intl.R;

/**
 * Created by aijingya on 2021/2/7.
 *
 * @Package cn.swiftpass.enterprise.ui.widget.dialog
 * @Description:
 * @date 2021/2/7.13:52.
 */
public class CashierNoticeNewDialog extends Dialog {
    private static final String TAG = "CashierNoticeNewDialog";
    Context mContext;
    TextView tv_content;
    TextView tv_cancel,tv_copy;

    private ButtonCopyCallBack mCopyListener;

    public CashierNoticeNewDialog(@NonNull Context context) {
        super(context, R.style.simpleDialog);
        mContext = context;
        init();
    }

    public void init(){
        View rootView = View.inflate(mContext, R.layout.dialog_cashier_new_pop, null);
        setContentView(rootView);

        tv_content = (TextView)rootView.findViewById(R.id.tv_content);
        tv_cancel = (TextView) rootView.findViewById(R.id.tv_cancel);
        tv_copy = (TextView) rootView.findViewById(R.id.tv_copy);

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        tv_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCopyListener == null){
                    Log.i(TAG, "mCopyListener == null");
                }else{
                    mCopyListener.onClickCopyCallBack();
                }
            }
        });

    }

    public void setContentText(String  text){
        tv_content.setText(text);
    }
    public void setContentTextSize(int size){
        tv_content.setTextSize(size);
    }

    public void setContentText(int text){
        tv_content.setText(text);
    }

    public void setCopyText(int text){
        tv_copy.setText(text);
    }

    /**
     * 定义接口用来响应button的监听
     */
    public interface ButtonCopyCallBack{
        void onClickCopyCallBack();
    }

    public void setCurrentListener(ButtonCopyCallBack listener){
        mCopyListener = listener;
    }



}
