package cn.swiftpass.enterprise.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SelectItemView extends LinearLayout implements Checkable, OnClickListener
{
	public TextView tvText;
	public TextView tvText2;
	private CheckBox cbSelected;
	private boolean checked;

	public SelectItemView(Context context)
	{
		super(context);
	}
	//必须写这个构造函数
	public SelectItemView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
	@Override
	protected void onFinishInflate()
	{
		super.onFinishInflate();
		cbSelected.setVisibility(View.GONE);
	}
		
	@Override
	public void onClick(View v)
	{
	}

	@Override
	public void setChecked(boolean checked)
	{
		this.checked = checked;
		cbSelected.setChecked(checked);
	}
	@Override
	public boolean isChecked()
	{
		return checked;
	}

	@Override
	public void toggle()
	{
		setChecked(!isChecked());
	}
	/**
	 * 是否隐藏checkbox
	 * @param flag
	 */
	public void isShowCheckBox(boolean flag)
	{
		if(flag){
			cbSelected.setVisibility(View.VISIBLE);
		}else{
			cbSelected.setVisibility(View.GONE);
		}
	}
	


	
}
