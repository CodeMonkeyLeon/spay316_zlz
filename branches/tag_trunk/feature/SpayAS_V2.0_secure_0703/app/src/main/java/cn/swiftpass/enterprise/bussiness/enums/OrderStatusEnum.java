package cn.swiftpass.enterprise.bussiness.enums;

/**
 * 订单状态
 * User: Administrator
 * Date: 13-9-23
 * Time: 下午4:27
 * To change this template use File | Settings | File Templates.
 */
public enum OrderStatusEnum
{
    PAY(0, "查询所有订单记录"),
    /**默认是未支付*/
    NO_PAY(1, "未支付"),
    /**微信财付通返回支付结果是成功，才是正常成功  支付成功*/
    PAY_SUCCESS(2, "支付成功"),
    /**其他出现问题必须改成支付失败  失效交易*/
    PAY_FAILL(1, "待收款"),
    /**用户需要退货物，那么就是退款状态*/
    PAY_CLOSE(3, "已关闭"), RETURN_PRODUCT(4, "转入退款"),
    /**其他失败异常 异常定位不到*/
    ORTHER_FIAIL(5, "退款中"),
    /**收到通知 还未查询到通知*/
    RECEVIE_NOTIFY(6, "收到通知"),
    //核销
    HEXIAO(7, "核销"), REVERSE(8, "已冲正");
    private final Integer value;
    
    private String displayName = "";
    
    private OrderStatusEnum(Integer v, String displayName)
    {
        this.value = v;
        this.displayName = displayName;
        
    }
    
    public static String getDisplayNameByVaue(Integer v)
    {
        if (v != null)
        {
            for (OrderStatusEnum ose : values())
            {
                if (ose.value.equals(v))
                {
                    return ose.getDisplayName();
                }
            }
        }
        return "";
    }
    
    public String getDisplayName()
    {
        return displayName;
    }
    
    public Integer getValue()
    {
        return value;
    }
    
    public static String getName(int state)
    {
        if (state == OrderStatusEnum.NO_PAY.getValue())
        {
            return OrderStatusEnum.NO_PAY.getDisplayName();
        }
        else if (state == OrderStatusEnum.PAY_FAILL.getValue())
        {
            return OrderStatusEnum.ORTHER_FIAIL.getDisplayName();
        }
        else if (state == OrderStatusEnum.ORTHER_FIAIL.getValue())
        {
            return OrderStatusEnum.NO_PAY.getDisplayName();
        }
        else if (state == OrderStatusEnum.PAY_SUCCESS.getValue())
        {
            return OrderStatusEnum.PAY_SUCCESS.getDisplayName();
        }
        else if (state == OrderStatusEnum.RETURN_PRODUCT.getValue())
        {
            return OrderStatusEnum.RETURN_PRODUCT.getDisplayName();
        }
        else if (state == OrderStatusEnum.PAY_CLOSE.getValue())
        {
            return OrderStatusEnum.PAY_CLOSE.getDisplayName();
        }
        else if (state == OrderStatusEnum.REVERSE.getValue())
        {
            return OrderStatusEnum.REVERSE.getDisplayName();
        }
        return "";
    }
}
