package cn.swiftplus.enterprise.networksdk.response;

/**
 * Created by aijingya on 2020/11/25.
 *
 * @Package cn.swiftplus.enterprise.networksdk.response
 * @Description: 自定义异常类，返回ecode,emsg到业务层
 * @date 2020/11/25.11:25.
 */
public class OkHttpException extends Exception{

    private static final long serialVersionUID = 1L;

    private int error_code; //错误码
    private String error_msg; //错误消息

    public OkHttpException(int ecode, String emsg) {
        this.error_code = ecode;
        this.error_msg = emsg;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }
}
