package cn.swiftpass.enterprise.partyking.interfaces

import android.view.View

interface OnPtkSummaryItemClickListener {
    fun onPtkSummaryItemClick(view: View?, position: Int = 0)
}