package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by aijingya on 2019/4/22.
 *
 * @Package cn.swiftpass.enterprise.bussiness.model
 * @Description: ${TODO}(报表详情页的数据)
 * @date 2019/4/22.16:21.
 */
public class ReportTransactionDetailsBean implements Serializable {

    public ArrayList<DailyTransactionDataBean> dayStatistics; // 折线图数据
    public ArrayList<PaymentChannelBean> payChannelStatistical; //饼状图交易通道数据
    public ArrayList<CashierTop5Bean> statisticsByAmount; //收银员柱状图--按金额
    public ArrayList<CashierTop5Bean> statisticsByCount;//收银员柱状图--按笔数
    public TransactionTotalBean total;//汇总的数据


    public ArrayList<DailyTransactionDataBean> getDayStatistics() {
        return dayStatistics;
    }

    public void setDayStatistics(ArrayList<DailyTransactionDataBean> dayStatistics) {
        this.dayStatistics = dayStatistics;
    }

    public ArrayList<PaymentChannelBean> getPayChannelStatistical() {
        return payChannelStatistical;
    }

    public void setPayChannelStatistical(ArrayList<PaymentChannelBean> payChannelStatistical) {
        this.payChannelStatistical = payChannelStatistical;
    }

    public ArrayList<CashierTop5Bean> getStatisticsByAmount() {
        return statisticsByAmount;
    }

    public void setStatisticsByAmount(ArrayList<CashierTop5Bean> statisticsByAmount) {
        this.statisticsByAmount = statisticsByAmount;
    }

    public ArrayList<CashierTop5Bean> getStatisticsByCount() {
        return statisticsByCount;
    }

    public void setStatisticsByCount(ArrayList<CashierTop5Bean> statisticsByCount) {
        this.statisticsByCount = statisticsByCount;
    }

    public TransactionTotalBean getTotal() {
        return total;
    }

    public void setTotal(TransactionTotalBean total) {
        this.total = total;
    }
}
