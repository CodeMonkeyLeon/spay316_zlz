package cn.swiftpass.enterprise.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexgo.oaf.apiv3.device.printer.Printer;
import com.tencent.stat.StatService;

import net.tsz.afinal.FinalBitmap;

import java.util.Date;
import java.util.Locale;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.print.BillPrintSuccess;
import cn.swiftpass.enterprise.print.CreateOneDiCodeUtilPrint;
import cn.swiftpass.enterprise.ui.activity.print.BluePrintUtil;
import cn.swiftpass.enterprise.ui.activity.print.BluetoothSettingActivity;
import cn.swiftpass.enterprise.ui.widget.DialogInfo;
import cn.swiftpass.enterprise.ui.widget.TitleBar;
import cn.swiftpass.enterprise.ui.widget.WxCardDialog;
import cn.swiftpass.enterprise.utils.CreateOneDiCodeUtil;
import cn.swiftpass.enterprise.utils.DataReportUtils;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.DialogHelper;
import cn.swiftpass.enterprise.utils.FileUtils;
import cn.swiftpass.enterprise.utils.HandlerManager;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.PreferenceUtil;
import cn.swiftpass.enterprise.utils.StringUtil;

/**
 * 交易订单结果状态.
 * User: Alan
 * Date: 14-3-11
 * Time: 下午4:42
 * To change this template use File | Settings | File Templates.
 */
public class PayResultActivity extends TemplateActivity {

    private static final String TAG = PayResultActivity.class.getSimpleName();

    //private TextView tvMoney, tvInfo;

    private TextView tvOrderNo, tvOrderTime, mch_name, wx_tvOrderCode, tvOrderState, tvBank,
            tvWXUserName,tv_order_complete,tv_transaction;

    //private ImageView ivPayType;

    private ImageView ivPayImg;

    private Order order;

    //private LinearLayout mLayout;

    private LinearLayout llPayImg, llWxusername, logo_lay;

    private LinearLayout ll_uplan_list;

    //private TextView tv_transNo;

    private TextView body_info, wx_title_info,  tv_mch_transNo;

    private ImageView logo_title, iv_code;

    private FinalBitmap finalBitmap;

    private LinearLayout lay_mch, money_lay, discount_lay, card_lay, id_lin_surcharge, id_lin_withholding;

    //private TextView x_receiv, tx_vcard;

    private TextView tv_pay_order, tv_code, pay_mchId, tx_discount,  tx_receivable;

    private Button confirm, blue_print;

    private LinearLayout ly_card_len, id_total_line;

    private TextView id_total, tx_surcharge, tx_withholding;


    private TextView tx_rmb;
    private View id_first_line, id_second_line;

    private boolean isCashierVisiable = false;

    private Printer printer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        finalBitmap = FinalBitmap.create(PayResultActivity.this);
        try {
            finalBitmap.configDiskCachePath(FileUtils.getAppCache());
            printer = MainApplication.deviceEngine.getPrinter();
        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }
        MainApplication.listActivities.add(this);
        initView();
        setLister();
        try {
            order.setAddTimeNew(order.getNotifyTime());
        } catch (Exception e) {
            Log.e(TAG,Log.getStackTraceString(e));
        }
        //设置自动打印
        if(order != null && printer != null){
            print(order);
        }
    }

    private void print(final Order orderModel) {
        //调用打印机打印
        orderModel.setTradeState(2);
        orderModel.setAddTimeNew(tvOrderTime.getText().toString());
        orderModel.setPay(true);
        orderModel.setPartner(getString(R.string.tv_pay_user_stub));

        printer.initPrinter();
        final Bitmap bitmap = CreateOneDiCodeUtilPrint.createCode(order.getOrderNoMch(), Integer.valueOf(260), Integer.valueOf(260));
        BillPrintSuccess.print(isCashierVisiable, order, printer, bitmap);
        //打印第二联
        try {
            (new Handler()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    order.setPay(true);
                    orderModel.setTradeState(2);
                    orderModel.setPartner(getString(R.string.tv_pay_mch_stub));
                    Printer printerPic = MainApplication.deviceEngine.getPrinter();
                    printerPic.initPrinter();
                    BillPrintSuccess.print(isCashierVisiable, order, printerPic, bitmap);
                }
            }, 5000L);
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
    }

    void showDialog() {
        dialog = new DialogInfo(PayResultActivity.this, null, getString(R.string.tx_blue_set), getString(R.string.title_setting), getString(R.string.btnCancel), DialogInfo.UNIFED_DIALOG, new DialogInfo.HandleBtn() {

            @Override
            public void handleOkBtn() {
                dialog.cancel();
                dialog.dismiss();
                showPage(BluetoothSettingActivity.class);
            }

            @Override
            public void handleCancleBtn() {
                dialog.cancel();
            }
        }, null);

        DialogHelper.resize(PayResultActivity.this, dialog);
        dialog.show();
    }

    private void setLister() {
        blue_print.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    StatService.trackCustomEvent(PayResultActivity.this, "kMTASPayPayOrderFinish", "收款成功完成按钮");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                // 1.携带参数的打点
                Bundle values = new Bundle();
                values.putString("kGFASPayPayOrderFinish","收款成功完成按钮");
                DataReportUtils.getInstance().report("kGFASPayPayOrderFinish",values);


                if (MainApplication.listActivities.size() > 0) {
                    for (Activity a : MainApplication.listActivities) {
                        a.finish();
                    }
                }
                HandlerManager.notifyMessage(HandlerManager.PAY_FINISH, HandlerManager.PAY_FINISH,null);
            }
        });

        card_lay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                WxCardDialog wxCardDialgo = new WxCardDialog(PayResultActivity.this, order.getWxCardList(), new WxCardDialog.ConfirmListener() {

                    @Override
                    public void cancel() {

                    }
                });
                DialogHelper.resize(PayResultActivity.this, wxCardDialgo);
                wxCardDialgo.show();
            }
        });
    }

    private void initView() {
        setContentView(R.layout.activity_pay_result_new);
        tv_transaction=getViewById(R.id.tv_transaction);
        tv_order_complete=getViewById(R.id.tv_order_complete);
        blue_print = getViewById(R.id.blue_print);
        id_first_line = getViewById(R.id.id_first_line);
        id_second_line = getViewById(R.id.id_second_line);
        tx_rmb = getViewById(R.id.tx_rmb);
        id_total = getViewById(R.id.id_total);
        tx_surcharge = getViewById(R.id.tx_surcharge);
        tx_withholding = getViewById(R.id.tx_withholding);
        id_lin_surcharge = getViewById(R.id.id_lin_surcharge);
        id_lin_withholding = getViewById(R.id.id_lin_withholding);
        ly_card_len = getViewById(R.id.ly_card_len);
        discount_lay = getViewById(R.id.discount_lay);
        //tx_vcard = getViewById(R.id.tx_vcard);
        card_lay = getViewById(R.id.card_lay);
        //tx_receiv = getViewById(R.id.tx_receiv);
        money_lay = getViewById(R.id.money_lay);
        discount_lay = getViewById(R.id.discount_lay);
        tx_receivable = getViewById(R.id.tx_receivable);
        tx_discount = getViewById(R.id.tx_discount);
        pay_mchId = getViewById(R.id.pay_mchId);
        confirm = getViewById(R.id.confirm);
        iv_code = getViewById(R.id.iv_code);
        tv_code = getViewById(R.id.tv_code);
        tvOrderNo = getViewById(R.id.tv_orderNo);
        tv_mch_transNo = getViewById(R.id.tv_mch_transNo);
        body_info = getViewById(R.id.body_info);
        tvOrderTime = getViewById(R.id.tv_order_time);
        tvOrderState = getViewById(R.id.tv_order_state);
        tvBank = getViewById(R.id.tv_bank);
        id_total_line = getViewById(R.id.id_total_line);
        //tvMoney = getViewById(R.id.tv_money);
        //ivPayType = getViewById(R.id.iv_payType);
        ivPayImg = getViewById(R.id.iv_pay_img);
        //tvInfo = getViewById(R.id.tv_info);
        tvWXUserName = getViewById(R.id.tv_wxusername);
        order = (Order) getIntent().getSerializableExtra("order");
        //mLayout = getViewById(R.id.dialog);
        llPayImg = getViewById(R.id.ll_pay_img);
        llWxusername = getViewById(R.id.ll_wxusername);
        wx_tvOrderCode = getViewById(R.id.wx_tvOrderCode);
        mch_name = getViewById(R.id.mch_name);

        tv_pay_order = getViewById(R.id.tv_pay_order);
        lay_mch = getViewById(R.id.lay_mch);

        logo_lay = getViewById(R.id.logo_lay);
        logo_title = getViewById(R.id.logo_title);

        wx_title_info = getViewById(R.id.wx_title_info);

        ll_uplan_list = getViewById(R.id.ll_uplan_list);

        initData(order);

        confirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
                HandlerManager.notifyMessage(HandlerManager.PAY_SET_PAY_METHOD, HandlerManager.SHOPKEEPID);
            }
        });

    }

    private void initData(final Order order) {
        if (order.getCashFeel() > 0)
        {
            tx_rmb.setVisibility(View.VISIBLE);
            tx_rmb.setText(getString(R.string.tx_mark) + DateUtil.formatRMBMoneyUtils(order.getCashFeel()));
        }else{
            tx_rmb.setVisibility(View.GONE);
        }

        pay_mchId.setText(MainApplication.getMchId());

        // 生成一维码
        if (!isAbsoluteNullStr(order.getOrderNoMch())) {
            WindowManager wm = this.getWindowManager();
            int width = wm.getDefaultDisplay().getWidth();
            final int w = (int) (width * 0.85);
            PayResultActivity.this.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    iv_code.setImageBitmap(CreateOneDiCodeUtil.createCode(order.getOrderNoMch(), w, 180));
                }
            });
            tv_code.setText(order.getOrderNoMch());
        }

        tv_mch_transNo.setText(order.getOrderNoMch());
        wx_tvOrderCode.setText(order.getTransactionId());
        tvOrderNo.setText(order.outTradeNo + "");
        tvOrderState.setText(R.string.order_success);//OrderStatusEnum.getDisplayNameByVaue(order.state);
        mch_name.setText(MainApplication.getMchName());

        if (!MainApplication.mchLogo.equals("") && !MainApplication.mchLogo.equals("null") && MainApplication.mchLogo != null) {
            logo_lay.setVisibility(View.VISIBLE);
            llPayImg.setVisibility(View.GONE);
            finalBitmap.display(logo_title, MainApplication.mchLogo);
        }
        if (!"".equals(MainApplication.body) && null != MainApplication.body && !MainApplication.body.equals("null")) {
            body_info.setText(MainApplication.body);
        } else {
            body_info.setText(ApiConstant.body);
        }
        if (order!=null&&!TextUtils.isEmpty(order.getTradeType())) {
            if (order.getTradeType().equals("pay.weixin.native")) {

                tvBank.setText(getString(R.string.weChat_pay_type));
                // ivPayType.setImageResource(R.drawable.n_pay_weixin);
            }
        }
        if (MainApplication.isAdmin.equals("0")) {
            tv_pay_order.setText(MainApplication.realName + "(" + MainApplication.userId + ")");
        } else {
            lay_mch.setVisibility(View.GONE);
            id_second_line.setVisibility(View.GONE);
        }

        if (order!=null&&!TextUtils.isEmpty(order.getTradeType())) {

            if (order.getTradeType().equals(MainApplication.PAY_WX_MICROPAY) || order.getTradeType().equals("unified.trade.micropay")) {
                tvBank.setText(getStringById(R.string.wechat_car_pay));
                //.setImageResource(R.drawable.picture_pay_barcode);
            } else if (order.getTradeType().equals(MainApplication.PAY_ZFB_MICROPAY)) {
                ivPayImg.setImageResource(R.drawable.icon_alipay_color);
                tvBank.setText(getStringById(R.string.zfb_code_pay));
                wx_title_info.setText(getStringById(R.string.tv_pay_zfb_order));
            } else if (order.getTradeType().startsWith(MainApplication.PAY_ZFB_NATIVE)) {
                ivPayImg.setImageResource(R.drawable.icon_alipay_color);
                wx_title_info.setText(getStringById(R.string.tv_pay_zfb_order));
                tvBank.setText(getStringById(R.string.zfb_scan_code_pay));
            } else if (order.getTradeType().equals(MainApplication.PAY_QQ_MICROPAY) || order.getTradeType().equals(MainApplication.PAY_QQ_PROXY_MICROPAY)) {
                ivPayImg.setImageResource(R.drawable.icon_qq_color);
                tvBank.setText(getStringById(R.string.qq_code_pay));
                wx_title_info.setText(getStringById(R.string.tv_pay_qq_order));
            } else if (order.getTradeType().equalsIgnoreCase("pay.qq.jspay") || order.getTradeType().equals(MainApplication.PAY_QQ_NATIVE)) {
                wx_title_info.setText(R.string.tv_pay_qq_order);
                ivPayImg.setImageResource(R.drawable.icon_qq_color);
                tvBank.setText(R.string.qq_scan_pay);
            } else if (order.getTradeType().equalsIgnoreCase(MainApplication.PAY_JINGDONG) || order.getTradeType().equalsIgnoreCase(MainApplication.PAY_JINGDONG_NATIVE)) {
                wx_title_info.setText(R.string.tx_pay_jd_order);
                ivPayImg.setImageResource(R.drawable.icon_list_jd);
            }
        }

        if (!TextUtils.isEmpty(order.getTradeName())) {
            tvBank.setText(order.getTradeName());
        } else {
            tvBank.setText(Order.TradeTypetoStr.getTradeNameForType(order.getTradeType()));
        }

        if (!TextUtils.isEmpty(order.wxUserName)) {
            llWxusername.setVisibility(View.GONE);
            tvWXUserName.setText(order.wxUserName);
        } else {
            llWxusername.setVisibility(View.GONE);
        }
        if (!StringUtil.isEmptyOrNull(order.getNotifyTime())) {
            tvOrderTime.setText(order.getNotifyTime());
        } else {
            tvOrderTime.setText(DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
        }
//        double money = order.totalFee / 100d;

//        if (order.getDaMoney() > 0) { //优惠金额大于零
//            //            discount_lay.setVisibility(View.VISIBLE);
//            //            money_lay.setVisibility(View.VISIBLE);
//            tx_discount.setVisibility(View.VISIBLE);
//            //            tx_receiv.setText(R.string.tx_real_money);
//            discount_lay.setVisibility(View.VISIBLE);
//            double discount = (double) order.getDaMoney();
//
//            tx_discount.setText(MainApplication.getFeeFh() + " " + DateUtil.formatMoneyUtils(discount));
//            tx_receivable.setText(MainApplication.getFeeFh() + " " + DateUtil.formatMoneyUtils(order.totalFee));
//            // 实际金额 = 应收金额+优惠金额
//
//            if (order.getWxCardList() != null && order.getWxCardList().size() > 0) {
//                ly_card_len.setVisibility(View.VISIBLE);
//
//            } else {
//                ly_card_len.setVisibility(View.GONE);
//                card_lay.setVisibility(View.GONE);
//            }
//        } else {
        ly_card_len.setVisibility(View.GONE);
        discount_lay.setVisibility(View.GONE);
        id_first_line.setVisibility(View.GONE);
        tx_discount.setVisibility(View.GONE);
        //            discount_lay.setVisibility(View.GONE);
        money_lay.setVisibility(View.GONE);
        card_lay.setVisibility(View.GONE);

        //根据当前的小数点的位数来判断应该除以多少
        double min = 1;
        for(int i = 0 ; i < MainApplication.numFixed ; i++ ){
            min = min * 10;
        }
        tx_receivable.setText(MainApplication.getFeeFh() + " " + DateUtil.formatMoneyUtil(order.totalFee / min));

        //Uplan --- V3.0.5迭代新增
        if(order != null){
            //如果为预授权的支付类型，则不展示任何优惠相关的信息
            /*因目前SPAY與網關支持的預授權接口僅為‘支付寶’預授權，
            遂與銀聯Uplan不衝突，遂預授權頁面內，無需處理該優惠信息。*/
            if(!TextUtils.isEmpty(order.getTradeType())&&order.getTradeType().equals("pay.alipay.auth.micropay.freeze") || order.getTradeType().equals("pay.alipay.auth.native.freeze")){
                ll_uplan_list.removeAllViews();
            }else{
                ll_uplan_list.removeAllViews();

                // 优惠详情 --- 如果列表有数据，则展示，有多少条，展示多少条
                if(order.getUplanDetailsBeans() != null && order.getUplanDetailsBeans().size() > 0){
                    for(int i =0 ; i<order.getUplanDetailsBeans().size() ; i++){
                        LayoutInflater inflater = LayoutInflater.from(this);
                        View view = inflater.inflate(R.layout.item_uplan_discount_pay_result, null);// 得到加载view item_uplan_discount

                        TextView tv_title = (TextView) view.findViewById(R.id.tv_uplan_discount_title);
                        TextView tv_content = (TextView) view.findViewById(R.id.tv_uplan_discount_content);

                        //如果是Uplan Discount或者Instant Discount，则采用本地词条，带不同语言的翻译
                        if(order.getUplanDetailsBeans().get(i).getDiscountNote().equalsIgnoreCase("Uplan discount")){
                            tv_title.setText(getStringById(R.string.uplan_Uplan_discount));
                        }else if(order.getUplanDetailsBeans().get(i).getDiscountNote().equalsIgnoreCase("Instant Discount")){
                            tv_title.setText(getStringById(R.string.uplan_Uplan_instant_discount));
                        } else{//否则，后台传什么展示什么
                            tv_title.setText(order.getUplanDetailsBeans().get(i).getDiscountNote());
                        }
                        tv_content.setText(MainApplication.feeFh + " -"+order.getUplanDetailsBeans().get(i).getDiscountAmt());

                        ll_uplan_list.addView(view);
                    }
                }

                // 消费者实付金额 --- 如果不为0 ，就展示
                if(order.getCostFee() != 0){
                    LayoutInflater inflater = LayoutInflater.from(this);
                    View view = inflater.inflate(R.layout.item_uplan_discount_pay_result, null);// 得到加载view item_uplan_discount

                    TextView tv_title = (TextView) view.findViewById(R.id.tv_uplan_discount_title);
                    TextView tv_content = (TextView) view.findViewById(R.id.tv_uplan_discount_content);

                    tv_title.setText(getStringById(R.string.uplan_Uplan_actual_paid_amount));
                    tv_content.setText(MainApplication.feeFh +DateUtil.formatMoneyUtils(order.getCostFee()));

                    ll_uplan_list.addView(view);
                }
            }
        }

        String language = PreferenceUtil.getString("language","");
        Locale locale = MainApplication.getContext().getResources().getConfiguration().locale; //zh-rHK
        String lan = locale.getCountry();
        if (!StringUtil.isEmptyOrNull(MainApplication.getPayTypeMap().get(order.getApiCode()))) {
            if (!TextUtils.isEmpty(language)) {
                if (language.equals(MainApplication.LANG_CODE_EN_US)) {
                    wx_title_info.setText(MainApplication.getPayTypeMap().get(order.getApiCode()) + " " + getString(R.string.tx_orderno));
                } else {
                    wx_title_info.setText(MainApplication.getPayTypeMap().get(order.getApiCode()) + getString(R.string.tx_orderno));
                }
            } else {
                if (lan.equalsIgnoreCase("en")) {
                    wx_title_info.setText(MainApplication.getPayTypeMap().get(order.getApiCode()) + " " + getString(R.string.tx_orderno));
                } else {
                    wx_title_info.setText(MainApplication.getPayTypeMap().get(order.getApiCode()) + getString(R.string.tx_orderno));
                }
            }
        }

        //预授权
        if (order.getTradeType().equals("pay.alipay.auth.micropay.freeze") || order.getTradeType().equals("pay.alipay.auth.native.freeze")){
            tv_order_complete.setText(R.string.tx_bill_stream_chioce_succ);
            titleBar.setBackgroundColor(getResources().getColor(R.color.title_bg_pre_auth));
            blue_print.setBackgroundResource(R.drawable.btn_pre_finish);
            tv_transaction.setText(R.string.payment_method);
        }else{
            blue_print.setBackgroundResource(R.drawable.btn_finish);
        }

        if (!MainApplication.isSurchargeOpen()) {
            id_lin_surcharge.setVisibility(View.GONE);
            id_total_line.setVisibility(View.GONE);
        } else {
            if (order != null) {
                if(order.getTradeType().equals("pay.alipay.auth.micropay.freeze") || order.getTradeType().equals("pay.alipay.auth.native.freeze")){
                    id_lin_surcharge.setVisibility(View.GONE);
                    id_total_line.setVisibility(View.GONE);
                }else{
                    tx_surcharge.setText(MainApplication.feeFh + DateUtil.formatMoneyUtils(order.getSurcharge()));
                }
            }
        }

       /* if (!MainApplication.isTaxRateOpen()) {
            id_lin_withholding.setVisibility(View.GONE);
        } else {
            tx_withholding.setText(MainApplication.feeFh + DateUtil.formatMoneyUtils(order.getWithholdingTax()));
        }*/
        id_lin_withholding.setVisibility(View.GONE);

        id_total.setText(MainApplication.feeFh + DateUtil.formatMoneyUtils(order.getOrderFee()));

        if (null != order.getUserName() && !"".equals(order.getUserName()) && !"null".equals(order.getUserName()) && !StringUtil.isEmptyOrNull(order.getUseId())) {
            isCashierVisiable = true;
        } else {
            isCashierVisiable = false;
        }
    }


    @Override
    protected void setupTitleBar() {
        super.setupTitleBar();
        titleBar.setTitle(getStringById(R.string.title_order_detail));
        titleBar.setLeftButtonVisible(true);
        titleBar.setRightButtonVisible(false);
        titleBar.setRightButLayVisibleForTotal(true, getString(R.string.print));
        titleBar.setOnTitleBarClickListener(new TitleBar.OnTitleBarClickListener() {
            @Override
            public void onLeftButtonClick() {
                if (MainApplication.listActivities.size() > 0) {
                    for (Activity a : MainApplication.listActivities) {
                        a.finish();
                    }
                }
//                HandlerManager.notifyMessage(HandlerManager.PAY_SET_PAY_METHOD, HandlerManager.PAY_SET_PAY_METHOD, null);

                HandlerManager.notifyMessage(HandlerManager.PAY_SET_PAY_METHOD, HandlerManager.SHOPKEEPID);
                PayResultActivity.this.finish();
            }

            @Override
            public void onRightButtonClick() {
                try {
                    StatService.trackCustomEvent(PayResultActivity.this, "kMTASPayPayOrderPrint", "收款成功打印");
                } catch (Exception e) {
                    Log.e(TAG,Log.getStackTraceString(e));
                }

                // 1.携带参数的打点
                Bundle values = new Bundle();
                values.putString("kGFASPayPayOrderPrint","收款成功“打印”按钮");
                DataReportUtils.getInstance().report("kGFASPayPayOrderPrint",values);

                print(order);
            }

            @Override
            public void onRightLayClick() {
                // TODO Auto-generated method stub
                   finish();
            }

            @Override
            public void onRightButLayClick() {
                // TODO Auto-generated method stub
                //                MainApplication.listActivities.add(PayResultActivity.this);
                //                OrderRefundActivity.startActivity(PayResultActivity.this, order);\
                print(order);


            }
        });
    }

    @Override
    public boolean onKeyDown(int keycode, KeyEvent event) {
        if (keycode == KeyEvent.KEYCODE_BACK) {
            if (MainApplication.listActivities.size() > 0) {
                for (Activity a : MainApplication.listActivities) {
                    a.finish();
                }
            }
            //            HandlerManager.notifyMessage(HandlerManager.PAY_SET_PAY_METHOD, HandlerManager.SHOPKEEPID);
            HandlerManager.notifyMessage(HandlerManager.PAY_FINISH, HandlerManager.SHOPKEEPID);
        }
        return super.onKeyDown(keycode, event);
    }

    public static void startActivity(Context mContext, Order order) {
        Intent it = new Intent();
        it.setClass(mContext, PayResultActivity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT); //W.l 解决弹出多个activity
        it.putExtra("order", order);
        mContext.startActivity(it);
    }

    public void showBigOneCode(View view) {
        if (order != null) {
//            OneDiCodeActivity.startActivity(PayResultActivity.this, order.getOrderNoMch());
            RotateCanvasViewActivity.startActivity(PayResultActivity.this, order.getOrderNoMch());
        }
    }

}
