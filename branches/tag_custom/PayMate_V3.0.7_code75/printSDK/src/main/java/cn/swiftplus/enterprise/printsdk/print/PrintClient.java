package cn.swiftplus.enterprise.printsdk.print;
import android.content.Context;
import com.pax.gl.imgprocessing.IImgProcessing;

import cn.swiftplus.enterprise.printsdk.print.A8.POSA8Client;
import cn.swiftplus.enterprise.printsdk.print.A8.printUtils.PrintA8Text;
import cn.swiftplus.enterprise.printsdk.print.A920.POSA920Client;
import cn.swiftplus.enterprise.printsdk.print.A920.printUtils.PrintA920Text;


/**
 * Created by aijingya on 2020/8/7.
 *
 * @Package cn.swiftplus.enterprise.printsdk.print
 * @Description:
 * @date 2020/8/7.10:50.
 */
public class PrintClient {
    private static PrintClient sPrintClient;
    private static String sPosTerminal;
    private Context mAppContext;

    public static void initClient(Context context){
        if (sPrintClient != null) {
            throw new RuntimeException("PrintClient Already initialized");
        }
        sPrintClient = new PrintClient(context.getApplicationContext());
    }

    public static PrintClient getInstance() {
        if (sPrintClient == null) {
            throw new RuntimeException("PrintClient is not initialized");
        }
        return sPrintClient;
    }

    private PrintClient(Context context ){
        mAppContext = context;
    }

    public void initPrintSDK(String posTerminal){
        sPosTerminal = posTerminal;
        //根据不同的POS机型号，初始化不同的打印机SDK
        if(sPosTerminal.equalsIgnoreCase("A920")){
            POSA920Client.initA920(mAppContext);
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            POSA8Client.initA8(mAppContext);
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

    public void printTitle(String text){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().init(mAppContext);
            PrintA920Text.getInstance().printTitle(text);
            PrintA920Text.getInstance().printEmptyLine();

        }else if(sPosTerminal.equalsIgnoreCase("A8")){

        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

    public void printTextLeft(String text){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().printTextLine(text,24, IImgProcessing.IPage.EAlign.LEFT);
        }else if(sPosTerminal.equalsIgnoreCase("A8")){

        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

    public void  printTextRight(String text){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().printTextLine(text,24, IImgProcessing.IPage.EAlign.RIGHT);
        }else if(sPosTerminal.equalsIgnoreCase("A8")){

        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

    public void  printTextCenter(String text){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().printTextLine(text,24, IImgProcessing.IPage.EAlign.CENTER);
        }else if(sPosTerminal.equalsIgnoreCase("A8")){

        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

    public void  printQRCode(String orderNoMch){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().printQRCode(orderNoMch);
        }else if(sPosTerminal.equalsIgnoreCase("A8")){

        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

    public void printDoubleLine(){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().printDoubleLine();
        }else if(sPosTerminal.equalsIgnoreCase("A8")){

        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

    public void pintSingleLine(){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().pintSingleLine();
        }else if(sPosTerminal.equalsIgnoreCase("A8")){

        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

    public void printEmptyLine(){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().printEmptyLine();
        }else if(sPosTerminal.equalsIgnoreCase("A8")){

        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

    public void startPrint(){
        if(sPosTerminal.equalsIgnoreCase("A920")){
            PrintA920Text.getInstance().startPrint();
        }else if(sPosTerminal.equalsIgnoreCase("A8")){
            PrintA8Text.getInstance().startPrint();
        }else if(sPosTerminal.equalsIgnoreCase("sunmi")){

        }
    }

}
