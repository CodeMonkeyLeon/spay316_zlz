/*
 * 文 件 名:  BaseFragmentActivity.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  yan_shuo
 * 修改时间:  2015-4-27
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.ui.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import androidx.fragment.app.FragmentActivity;

/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  Administrator
 * @version  [版本号, 2015-4-27]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class BaseFragmentActivity extends FragmentActivity
{
    /** {@inheritDoc} */
    
    @Override
    protected void onCreate(Bundle b)
    {
        super.onCreate(b);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }
    
    protected <T extends View> T getViewById(int id)
    {
        View view = findViewById(id);
        return (T)view;
    }
    
    protected <T extends View> T inflate(int res)
    {
        return (T)LayoutInflater.from(this).inflate(res, null);
    }
    
}
