/*
 * 文 件 名:  ShopBaseDataDB.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2014-3-20
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.io.database.access;

import java.sql.SQLException;
import java.util.List;

import android.util.Log;
import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.model.NoticeInfo;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;

/**
 * 公告
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2014-3-20]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class NoticeDataDB
{
    
    private static Dao<NoticeInfo, Integer> userDao;
    
    private static NoticeDataDB instance = null;
    
    public static NoticeDataDB getInstance()
    {
        if (instance == null)
        {
            instance = new NoticeDataDB();
        }
        
        return instance;
    }
    
    public NoticeDataDB()
    {
        try
        {
            userDao = MainApplication.getContext().getHelper().getDao(NoticeInfo.class);
        }
        catch (SQLException e)
        {
            Log.e("hehui", "NoticeDataDB-->" + e.getMessage());
        }
    }
    
    /**
     * 保存 商户行业数据
     * <功能详细描述>
     * @param userModel
     * @throws SQLException
     * @see [类、类#方法、类#成员]
     */
    public void save(NoticeInfo info)
        throws SQLException
    {
        //        AwaInfo result = queryByID(info.userName);
        //        if (result == null)
        //        {
        userDao.create(info);
        //        }
        //        else
        //        {
        //            userDao.update(result);
        //        }
    }
    
    /**
     * 根据id查询
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public NoticeInfo queryByID(String bankID)
    {
        String sql = "select * from t_notice where noticeId = '" + bankID + "'";
        NoticeInfo list = null;
        try
        {
            GenericRawResults<String[]> s = userDao.queryRaw(sql);
            List<String[]> listStr = s.getResults();
            for (int i = 0; i < listStr.size(); i++)
            {
                list = new NoticeInfo();
                String[] str = listStr.get(i);
                list.setNoticeId(str[0]);
            }
            
        }
        catch (SQLException e)
        {
            Log.e("hehui", "query failed " + e.getMessage());
            return null;
        }
        
        return list;
    }
    
    /**
     * 根据id查询
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public void deleteByID(String bankID)
    {
        String sql = "delete from t_notice where noticeId = '" + bankID + "'";
        NoticeInfo list = null;
        try
        {
            GenericRawResults<String[]> s = userDao.queryRaw(sql);
            List<String[]> listStr = s.getResults();
            for (int i = 0; i < listStr.size(); i++)
            {
                list = new NoticeInfo();
                String[] str = listStr.get(i);
                list.setNoticeId(str[0]);
            }
            
        }
        catch (SQLException e)
        {
            Log.e("hehui", "query failed " + e.getMessage());
        }
        
    }
    
}
