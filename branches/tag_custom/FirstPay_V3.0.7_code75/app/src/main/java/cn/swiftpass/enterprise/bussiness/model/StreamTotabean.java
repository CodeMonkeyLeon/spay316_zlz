/*
 * 文 件 名:  StreamTotabean.java
 * 版    权:   Ltd. Copyright swiftpass,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2015-3-14
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package cn.swiftpass.enterprise.bussiness.model;

import java.io.Serializable;

/**
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2015-3-14]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class StreamTotabean implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1L;
    
    private String tradeTime;
    
    private String outTradeNo;
    
    private String mchName;
    
    private String transactionId;
    
    private String tradeName;
    
    private String tradeState;
    
    private String tradeStateText;
    
    private String money;
    
    private String refundMoney;
    
    private String orderNoMch;
    
    /**
     * @return 返回 tradeTime
     */
    public String getTradeTime()
    {
        return tradeTime;
    }
    
    /**
     * @param 对tradeTime进行赋值
     */
    public void setTradeTime(String tradeTime)
    {
        this.tradeTime = tradeTime;
    }
    
    /**
     * @return 返回 outTradeNo
     */
    public String getOutTradeNo()
    {
        return outTradeNo;
    }
    
    /**
     * @param 对outTradeNo进行赋值
     */
    public void setOutTradeNo(String outTradeNo)
    {
        this.outTradeNo = outTradeNo;
    }
    
    /**
     * @return 返回 mchName
     */
    public String getMchName()
    {
        return mchName;
    }
    
    /**
     * @param 对mchName进行赋值
     */
    public void setMchName(String mchName)
    {
        this.mchName = mchName;
    }
    
    /**
     * @return 返回 transactionId
     */
    public String getTransactionId()
    {
        return transactionId;
    }
    
    /**
     * @param 对transactionId进行赋值
     */
    public void setTransactionId(String transactionId)
    {
        this.transactionId = transactionId;
    }
    
    /**
     * @return 返回 tradeName
     */
    public String getTradeName()
    {
        return tradeName;
    }
    
    /**
     * @param 对tradeName进行赋值
     */
    public void setTradeName(String tradeName)
    {
        this.tradeName = tradeName;
    }
    
    /**
     * @return 返回 tradeState
     */
    public String getTradeState()
    {
        return tradeState;
    }
    
    /**
     * @param 对tradeState进行赋值
     */
    public void setTradeState(String tradeState)
    {
        this.tradeState = tradeState;
    }
    
    /**
     * @return 返回 tradeStateText
     */
    public String getTradeStateText()
    {
        return tradeStateText;
    }
    
    /**
     * @param 对tradeStateText进行赋值
     */
    public void setTradeStateText(String tradeStateText)
    {
        this.tradeStateText = tradeStateText;
    }
    
    /**
     * @return 返回 money
     */
    public String getMoney()
    {
        return money;
    }
    
    /**
     * @param 对money进行赋值
     */
    public void setMoney(String money)
    {
        this.money = money;
    }
    
    /**
     * @return 返回 refundMoney
     */
    public String getRefundMoney()
    {
        return refundMoney;
    }
    
    /**
     * @param 对refundMoney进行赋值
     */
    public void setRefundMoney(String refundMoney)
    {
        this.refundMoney = refundMoney;
    }
    
    /**
     * @return 返回 orderNoMch
     */
    public String getOrderNoMch()
    {
        return orderNoMch;
    }
    
    /**
     * @param 对orderNoMch进行赋值
     */
    public void setOrderNoMch(String orderNoMch)
    {
        this.orderNoMch = orderNoMch;
    }
}
