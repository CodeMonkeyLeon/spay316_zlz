package cn.swiftpass.enterprise.bussiness.logica.order;

import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xclcharts.test.OrderTotalModel;

import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import cn.swiftpass.enterprise.MainApplication;
import cn.swiftpass.enterprise.bussiness.enums.CacheEnum;
import cn.swiftpass.enterprise.bussiness.enums.OrderStatusEnum;
import cn.swiftpass.enterprise.bussiness.enums.OrderTypeEnum;
import cn.swiftpass.enterprise.bussiness.enums.QRCodeState;
import cn.swiftpass.enterprise.bussiness.logica.BaseManager;
import cn.swiftpass.enterprise.bussiness.logica.account.LocalAccountManager;
import cn.swiftpass.enterprise.bussiness.logica.coupon.CouponManager;
import cn.swiftpass.enterprise.bussiness.logica.threading.Executable;
import cn.swiftpass.enterprise.bussiness.logica.threading.ThreadHelper;
import cn.swiftpass.enterprise.bussiness.logica.threading.UINotifyListener;
import cn.swiftpass.enterprise.bussiness.model.CacheModel;
import cn.swiftpass.enterprise.bussiness.model.CashierReport;
import cn.swiftpass.enterprise.bussiness.model.CodeModel;
import cn.swiftpass.enterprise.bussiness.model.DailyStatement;
import cn.swiftpass.enterprise.bussiness.model.Order;
import cn.swiftpass.enterprise.bussiness.model.OrderReport;
import cn.swiftpass.enterprise.bussiness.model.OrderSearchResult;
import cn.swiftpass.enterprise.bussiness.model.PaySuccessInfo;
import cn.swiftpass.enterprise.bussiness.model.QRcodeInfo;
import cn.swiftpass.enterprise.bussiness.model.RefundTotal;
import cn.swiftpass.enterprise.bussiness.model.RequestResult;
import cn.swiftpass.enterprise.bussiness.model.SettleModel;
import cn.swiftpass.enterprise.bussiness.model.StreamTotabean;
import cn.swiftpass.enterprise.bussiness.model.UserModel;
import cn.swiftpass.enterprise.bussiness.model.WxCard;
import cn.swiftpass.enterprise.intl.R;
import cn.swiftpass.enterprise.io.database.access.CodeDB;
import cn.swiftpass.enterprise.io.database.access.OrderDB;
import cn.swiftpass.enterprise.io.net.ApiConstant;
import cn.swiftpass.enterprise.io.net.HttpRequester;
import cn.swiftpass.enterprise.io.net.HttpRespons;
import cn.swiftpass.enterprise.io.net.NetHelper;
import cn.swiftpass.enterprise.ui.activity.WxPayActivity;
import cn.swiftpass.enterprise.utils.CycledThread;
import cn.swiftpass.enterprise.utils.DateTimeUtil;
import cn.swiftpass.enterprise.utils.DateUtil;
import cn.swiftpass.enterprise.utils.JsonUtil;
import cn.swiftpass.enterprise.utils.Logger;
import cn.swiftpass.enterprise.utils.MessageBus;
import cn.swiftpass.enterprise.utils.PacteraHttp;
import cn.swiftpass.enterprise.utils.SharedPreUtile;
import cn.swiftpass.enterprise.utils.SignUtil;
import cn.swiftpass.enterprise.utils.StringUtil;
import cn.swiftpass.enterprise.utils.ToastHelper;
import cn.swiftpass.enterprise.utils.Utils;
import cn.swiftpass.enterprise.utils.XmlUtil;
import cn.swiftpass.enterprise.utils.XmlUtil.ParseException;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * Created with IntelliJ IDEA. User: Alan Date: 13-9-21 Time: 下午2:33 To change
 * this template use File | Settings | File Templates.
 */
public class OrderManager extends BaseManager {

    public static final String TAG = "LocalAccountManager";

    // public static final String GET_QUERY_ORDER="queryOrder";
    public static OrderManager getInstance() {
        return Container.instance;
    }

    private static class Container {
        public static OrderManager instance = new OrderManager();
    }

    @Override
    public void init() {
    }

    private String check_qrcode_state_url = "https://login.weixin.qq.com/cgi-bin/mmwebwx-bin/login";

    private String codeUUUID = "";

    /**
     * 新版本 订单统计
     *
     * @param countMethod 1=30天 ，4=5分钟统计，5=5分钟收银员,2=30支付类型,3=5分钟支付类型
     * @author Administrator
     */
    public void getNewOrderTotal(final String startTime, final String endTime, final String countMethod, final String userName, final UINotifyListener<List<OrderTotalModel>> listener) {
        ThreadHelper.executeWithCallback(new Executable<List<OrderTotalModel>>() {
            @Override
            public List<OrderTotalModel> execute() throws Exception {
                JSONObject jsonObject = new JSONObject();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    jsonObject.put("mchId", MainApplication.merchantId);// todo
                } else {
                    jsonObject.put("mchId", MainApplication.getMchId());// todo
                }
                jsonObject.put("startTime", startTime);
                jsonObject.put("endTime", endTime);
                jsonObject.put("countMethod", countMethod);
                if (!StringUtil.isEmptyOrNull(userName)) {
                    jsonObject.put("userName", userName);
                }
                Logger.i("hehui", "req->" + jsonObject.toString());
                List<OrderTotalModel> list = new ArrayList<OrderTotalModel>();
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/orderCount", jsonObject);
                result.setNotifyListener(listener);
                if (!result.hasError()) {
                    int code = result.data.getInt("result");
                    if (code == 200) {
                        Logger.i("hehui", "result.data-->" + result.data.getString("message"));
                        try {
                            JSONObject jsonObjectList = new JSONObject(result.data.getString("message"));
                            long totalMoney = jsonObjectList.optLong("countTotalFee", 0);
                            JSONArray jArray = new JSONArray(jsonObjectList.getString("dataList"));
                            List<String> tList = DateTimeUtil.getListTime();
                            Collections.reverse(tList);
                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject json = jArray.getJSONObject(i);
                                OrderTotalModel orderTotalModel = new OrderTotalModel();
                                orderTotalModel.setCheckTime(json.optString("checkTime", ""));
                                orderTotalModel.setDate(json.optString("date", ""));
                                orderTotalModel.setPayTypeName(json.optString("payTypeName", ""));
                                orderTotalModel.setSuccessCount(json.optLong("successCount", 0));
                                orderTotalModel.setSuccessFee(json.optLong("successFee", 0));
                                orderTotalModel.setUserName(json.optString("userName", ""));
                                if (countMethod.equals("4")) {
                                    orderTotalModel.setTime(tList.get(i));
                                }
                                orderTotalModel.setMoneyText(ToastHelper.toStr(R.string.tx_money));
                                orderTotalModel.setNumText(ToastHelper.toStr(R.string.tv_settle_count));
                                orderTotalModel.setDate(json.optString("date", ""));
                                orderTotalModel.setWeek(json.optString("week", ""));
                                if (totalMoney > 0) {
                                    orderTotalModel.setTotalScale(DateTimeUtil.formatMonetToStr(orderTotalModel.getSuccessFee() * 100, totalMoney));
                                }
                                if (countMethod.equals("4")) { //每隔5分钟
                                    orderTotalModel.setTag(true);
                                } else {
                                    orderTotalModel.setTag(false);
                                }
                                list.add(orderTotalModel);
                            }
                            if (countMethod.equals("4")) {//5分钟，要补一个小时的数据
                                OrderTotalModel orderTotalModel = new OrderTotalModel();
                                orderTotalModel.setTime("0:00");
                                orderTotalModel.setSuccessCount(0);
                                orderTotalModel.setSuccessFee(0);
                                list.add(orderTotalModel);
                            }
                            return list;
                        } catch (Exception e) {
                            Log.e("hehui", "" + e);
                            listener.onError("获取数据失败");
                            return null;
                        }
                    } else {
                        listener.onError(result.data.getString("message"));
                    }
                }

                return null;
            }
        }, listener);
    }

    /**
     * 交易汇总数据
     *
     * @param listener
     */
    public void getOrderTotal(final String startTime, final String endTime, final String mobile, final UINotifyListener<SettleModel> listener) {
        ThreadHelper.executeWithCallback(new Executable<SettleModel>() {
            @Override
            public SettleModel execute() throws Exception {
                JSONObject jsonObject = new JSONObject();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    jsonObject.put("mchId", MainApplication.merchantId);// todo
                } else {
                    jsonObject.put("mchId", MainApplication.getMchId());// todo
                }
                jsonObject.put("startTime", startTime);
                jsonObject.put("endTime", endTime);
                if (!StringUtil.isEmptyOrNull(mobile)) {
                    jsonObject.put("mobile", mobile);
                }
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/settlementToTal", jsonObject);
                result.setNotifyListener(listener);
                if (!result.hasError()) {
                    int code = result.data.getInt("result");
                    if (code == 200) {
                        return (SettleModel) JsonUtil.jsonToBean(result.data.getString("message"), SettleModel.class);
                    } else {
                        listener.onError(result.data.getString("message"));
                    }
                }

                return null;
            }
        }, listener);
    }

    /**
     * 获取固定二维码
     * <功能详细描述>
     *
     * @param orderCode
     * @param strTotalMoney
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void getOrderNo(final UINotifyListener<String> listener) {
        ThreadHelper.executeWithCallback(new Executable<String>() {
            @Override
            public String execute() throws Exception {
                JSONObject jsonObject = new JSONObject();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    jsonObject.put("mchId", MainApplication.merchantId);// todo
                } else {
                    jsonObject.put("mchId", MainApplication.getMchId());// todo
                }
                jsonObject.put("uId", MainApplication.userId + "");
                jsonObject.put("client", MainApplication.CLIENT);
                String result = PacteraHttp.getInstance().post(ApiConstant.BASE_URL_PORT + "spay/produceCode", jsonObject.toString());
                try {

                    JSONObject json = new JSONObject(result);
                    int status = json.getInt("result");
                    if (status == 200) {
                        return json.getString("message");
                    } else {
                        listener.onError(json.getString("message"));
                    }
                } catch (Exception e) {
                    listener.onError("生成二维码失败");
                }
                return null;
            }
        }, listener);
    }

    /**
     * 生成订单ID
     *
     * @param listener
     */
    public void getOrderNum(final String orderCode, final String strTotalMoney, final UINotifyListener<String> listener) {
        ThreadHelper.executeWithCallback(new Executable<String>() {
            @Override
            public String execute() throws Exception {
                UserModel userModel = LocalAccountManager.getInstance().getLoggedUser();
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("mId", userModel.merchantId + "");
                jsonObject.put("uId", userModel.uId + "");
                jsonObject.put("money", strTotalMoney + "");
                jsonObject.put("orderCode", orderCode); // 如果之前有订单号查询下是否有效
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.ORDER_GET_ORDER_NUM, jsonObject);
                result.setNotifyListener(listener);
                if (!result.hasError()) {
                    String code = result.data.getString("code");
                    if (code != null) {
                        // callback(MSG_NOTIFY_ON_SUCCEED,listener,code);
                        return code;
                    }
                }

                return null;
            }
        }, listener);
    }

    /**
     * 订单核销
     *
     * @param listener
     */
    public void orderAffirm(final String outTradeNo, final UINotifyListener<Order> listener) {
        ThreadHelper.executeWithCallback(new Executable<Order>() {
            @Override
            public Order execute() throws Exception {
                JSONObject jsonObject = new JSONObject();

                //                jsonObject.put("uId", MainApplication.userId + "");
                jsonObject.put("orderNoMch", outTradeNo);
                Map<String, String> params = new HashMap<String, String>();
                params.put("orderNoMch", outTradeNo);//code=177500566780, method=consume, organNo=7551000001, channel=swiftpass
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    params.put("mchId", MainApplication.merchantId + "");
                    jsonObject.put("mchId", MainApplication.merchantId);// todo
                } else {
                    jsonObject.put("mchId", MainApplication.getMchId());// todo
                    params.put("mchId", MainApplication.getMchId());

                }
                //                params.put("uId", MainApplication.userId + "");

                if (MainApplication.isAdmin.equals("0")) // 收银员
                {
                    jsonObject.put("uId", MainApplication.userId);
                    if (null != MainApplication.realName && !"".equals(MainApplication.realName) && !MainApplication.realName.equals("null")) {
                        jsonObject.put("userName", MainApplication.realName); // 收银员名称
                        params.put("userName", MainApplication.realName);

                        jsonObject.put("affirmUser", MainApplication.realName);
                        params.put("affirmUser", MainApplication.realName);
                    }
                    params.put("uId", MainApplication.userId + "");
                } else {
                    jsonObject.put("affirmUser", MainApplication.mchName);
                    params.put("affirmUser", MainApplication.mchName);
                }

                jsonObject.put("client", MainApplication.CLIENT);
                params.put("client", MainApplication.CLIENT);
                jsonObject.put("sign", SignUtil.getInstance().createSign(params, MainApplication.getSignKey()));
                try {
                    //                    RequestResult resualt =
                    //                        NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/orderAffirm", jsonObject);
                    Logger.i("hehui", "orderAffirm param-->" + jsonObject);
                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/orderAffirm", jsonObject, null, null);

                    result.setNotifyListener(listener);
                    if (!result.hasError()) {
                        int code = result.data.getInt("result");
                        if (code == 200) {
                            Order order = new Order();
                            try {

                                JSONObject jsObject = new JSONObject(result.data.getString("message"));
                                WxCard wxCard = new WxCard();
                                wxCard.setTitle(jsObject.optString("title", ""));
                                wxCard.setDealDetail(jsObject.optString("dealDetail", ""));
                                if (StringUtil.isEmptyOrNull(wxCard.getDealDetail())) {
                                    wxCard.setDealDetail(jsObject.optString("description", null));
                                }
                                wxCard.setCardId(jsObject.optString("cardId", ""));
                                wxCard.setCardCode(outTradeNo);
                                wxCard.setCardType(jsObject.optString("cardType", ""));
                                wxCard.setGift(jsObject.optString("gift", ""));
                                wxCard.setDiscount(jsObject.optInt("discount", 0));
                                wxCard.setReduceCost(jsObject.optInt("reduceCost", 0));
                                wxCard.setUseTime(jsObject.optLong("useTime", 0));
                                wxCard.setVcardUseTime(jsObject.optString("vcardUseTime", ""));
                                wxCard.setType(jsObject.optString("type", ""));
                                wxCard.setFixedTerm(jsObject.optInt("fixedTerm", 0));
                                wxCard.setBeginTimeStr(jsObject.optString("beginTimeStr", ""));
                                wxCard.setEndTimeStr(jsObject.optString("endTimeStr", ""));
                                order.setWxCard(wxCard);
                            } catch (Exception e) {
                                return order;
                            }
                            return order;
                        } else {
                            listener.onError(result.data.getString("message"));
                        }
                    }
                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.tx_affirm_fail));
                }

                return null;
            }
        }, listener);
    }

    /**
     * 本地创建订单号
     */
    public String createOrderNumber() {
        CodeModel code = CodeDB.getInstance().getCodeModel();
        String derviceNo = "";
        try {
            derviceNo = LocalAccountManager.getInstance().getLoggedUser().derviceNo;
        } catch (Exception e) {
            derviceNo = LocalAccountManager.getInstance().getDerviceNo();
        }
        if (code != null) {
            code.value = code.id;
        } else {
            code = new CodeModel();
            code.value = 1;
        }
        CodeDB.getInstance().save(code);
        String codeStr = "";
        String dateTime = "";
        if (derviceNo.length() > 12) {
            dateTime = DateUtil.formatByYYMD(System.currentTimeMillis());
            codeStr = derviceNo + dateTime + Utils.leftPad(code.value + "", 10, "0");
        } else {
            dateTime = DateUtil.formatYYYYMD(System.currentTimeMillis());
            codeStr = derviceNo + dateTime + Utils.leftPad(code.value + "", 10, "0");
        }
        return codeStr;
    }

    /**
     * 组织本地订单号
     */
    public void createOrderNum1(final int money, final long time, final UINotifyListener<String> listener) {
        ThreadHelper.executeWithCallback(new Executable<String>() {
            @Override
            public String execute() {
                String derviceNo = LocalAccountManager.getInstance().getDerviceNo();
                String partent = "";// LocalAccountManager.getInstance().getPartent();
                String key = LocalAccountManager.getInstance().getPatentKey();

                if ("".equals(derviceNo) || "".equals(partent) || "".equals(key)) {
                    //                    listener.onError(cn.swiftpass.enterprise.R.string.msg_pay_offline_error_1);
                } else {
                    ApiConstant.KEY = key;
                    ApiConstant.PARTNET = partent;

                    String codeStr = createOrderNumber();
                    // save order
                    try {
                        Order order = new Order();
                        order.orderNo = codeStr;
                        order.money = money;
                        order.employeeId = LocalAccountManager.getInstance().getUID();
                        order.add_time = time;
                        order.createOrderType = OrderTypeEnum.OFFLINE.getValue();
                        order.merchantId = LocalAccountManager.getInstance().getLoggedUser().merchantId;
                        OrderDB.getInstance().saveOrder(order);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return codeStr;
                }
                return null;
            }

        }, listener);

    }

    /**
     * 监听订单状态
     */
    public void listenerOrderState() {
        // 30s执行一次
        /*
         * CycledThread thread = new CycledThread(orderListenerThread,3000);
         * thread.setShortSleepTime(200); thread.start();
         */

    }

    private Handler handler;

    /**
     * 开始检查，只要发现状态为支付成功继续请求
     */
    public void checkOrderState(Handler handler) {
        this.handler = handler;
        handler.sendEmptyMessage(WxPayActivity.MSG_START_CHECKIN);
    }

    private MessageBus mBus;

    private int updateTime = 1000 * 30;

    /**
     * 请求服务器订单状态，如果成功或失败不在检查
     */
    public void findOrderState(final String orderId) {

        getOrderState(new UINotifyListener<String>() {
            @Override
            public void onError(Object object) {
                super.onError(object);
                // handler.sendEmptyMessageDelayed(WxPayActivity.MSG_START_CHECKIN,
                // 1000 * 30);
            }

            @Override
            public void onSucceed(String result) {
                super.onSucceed(result);
                try {
                    OrderStatusEnum statusEnum = getEnumState(Utils.Integer.tryParse(result, 0));
                    mBus = MessageBus.getBusFactory();
                    if (statusEnum == OrderStatusEnum.NO_PAY) {
                        // 如果订单状态不是成功或失败继续 如果是未支付继续请求，时间30s更新一次
                        handler.sendEmptyMessageDelayed(WxPayActivity.MSG_START_CHECKIN, updateTime);
                    } else if (statusEnum == OrderStatusEnum.PAY_SUCCESS) {
                        // 通知UI
                        MessageBus.MMessage msg = mBus.createMessage(WxPayActivity.MSG_SUCCESS);
                        MessageBus.getBusFactory().send(msg);

                    } else if (statusEnum == OrderStatusEnum.PAY_FAILL) {
                        // thread._stop();
                        // 通知UI
                        MessageBus.MMessage msg = mBus.createMessage(WxPayActivity.MSG_FAIL);
                        MessageBus.getBusFactory().send(msg);

                    } else if (statusEnum == OrderStatusEnum.ORTHER_FIAIL) {
                        // 通知UI
                        MessageBus.MMessage msg = mBus.createMessage(WxPayActivity.MSG_FAIL);
                        MessageBus.getBusFactory().send(msg);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, orderId);

    }

    /**
     * 解析状态
     */
    private OrderStatusEnum getEnumState(Integer state) {
        if (OrderStatusEnum.NO_PAY.getValue() == state) {
            return OrderStatusEnum.NO_PAY;
        } else if (OrderStatusEnum.PAY_SUCCESS.getValue() == state) {
            return OrderStatusEnum.PAY_SUCCESS;
        } else if (OrderStatusEnum.PAY_FAILL.getValue() == state) {
            return OrderStatusEnum.PAY_FAILL;
        } else if (OrderStatusEnum.ORTHER_FIAIL.getValue() == state) {
            return OrderStatusEnum.ORTHER_FIAIL;
        } else if (OrderStatusEnum.RETURN_PRODUCT.getValue() == state) {
            return OrderStatusEnum.RETURN_PRODUCT;
        }
        return null;
    }

    /**
     * 根据订单ID 获取状态
     *
     * @param listener
     */
    public void getOrderState(final UINotifyListener<String> listener, final String orderId) {
        ThreadHelper.executeWithCallback(new Executable<String>() {
            @Override
            public String execute() throws Exception {
                return getOrderState(orderId);
            }
        }, listener);
    }

    /**
     * 获取订单状态
     */
    private String getOrderState(String orderCode) throws Exception {
        JSONObject jsonObject = new JSONObject();
        // jsonObject.put("mId", userModel.merchantId + "");
        // jsonObject.put("uId", userModel.uId + "");
        // jsonObject.put("ounrderId", orderCode); //这里注意不是id,是code

        jsonObject.put("outTradeNo", orderCode);
        jsonObject.put("userId", MainApplication.userId);// 普通用户才需要查自己的数据，管理员可以查询全部
        jsonObject.put("mId", MainApplication.merchantId);// todo

        RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.ORDER_GETLAST_TTIME, jsonObject);
        if (!result.hasError()) {

            Gson gson2 = new Gson();
            List<Order> orders = gson2.fromJson(result.data.getString("message"), new TypeToken<List<Order>>() {
            }.getType());

            return String.valueOf(orders.get(0).getState());
            // String state = result.data.getString("state");
            // if (state != null && !"".equals(state))
            // {
            // return state;
            // }
        } else {
            return null;
        }
        // return null;

    }

    /**
     * 点击支付的时候提交数据并保存订单数据 0.成功 1.参数错误 2.用户已经失效 3.订单重复 4.创建订单失败
     */
    public void submitOrderData(final UINotifyListener<String[]> listener, final Order order, final String pathSignatureUrl) {
        ThreadHelper.executeWithCallback(new Executable<String[]>() {
            @Override
            public String[] execute() throws Exception {
                UserModel userModel = LocalAccountManager.getInstance().getLoggedUser();
                order.merchantId = userModel.merchantId;
                order.employeeId = userModel.uId;
                order.clientType = ApiConstant.SPAY;
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("uId", order.employeeId + "");
                if (!TextUtils.isEmpty(order.useId)) {
                    jsonObject.put("useId", order.useId + "");// 1.3.5版本 优惠券标识
                }
                // 注意 需要传入订单号的是提前申请好的
                if (!TextUtils.isEmpty(order.orderNo)) {
                    jsonObject.put("orderId", order.orderNo);
                }

                jsonObject.put("mId", order.merchantId + "");
                jsonObject.put("remark", order.remark);
                jsonObject.put("money", order.money + "");
                jsonObject.put("clientType", order.clientType + "");
                jsonObject.put("money", order.money + "");
                jsonObject.put("transactionType", order.transactionType + ""); // 1.3.6
                // 版本交易类型
                String signatureNo;
                if (order.transactionType == 1) {
                    signatureNo = order.orderNo + ".bmp";
                } else {
                    signatureNo = null;
                }
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.ORDER_CREATE_ORDER, jsonObject, signatureNo, pathSignatureUrl);
                result.setNotifyListener(listener);
                if (!result.hasError()) {
                    String state = result.data.getString("result");
                    int ret = Utils.Integer.tryParse(state, -1);
                    switch (ret) {
                        case 0:
                            String orderTime = result.data.getString("dateTime");
                            String orderCode = result.data.getString("orderCode");
                            order.orderNo = orderCode;
                            // 保存订单信息
                            if (null == OrderDB.getInstance().getOrderByOrderNo(order.orderNo, order.employeeId)) {
                                // order.state = OrderStatusEnum.NO_PAY.getValue();
                                // 服务器时间为准
                                order.add_time = Utils.Long.tryParse(orderTime, System.currentTimeMillis());
                                order.userName = userModel.name;
                                OrderDB.getInstance().saveOrder(order);
                            }
                            String[] strs = new String[2];
                            strs[0] = orderCode;
                            strs[1] = orderTime;

                            return strs;
                        case 1:
                            listener.onError("参数错误！");
                            break;
                        case 2:
                            listener.onError("用户已经失效！");
                            break;
                        case 3:
                            listener.onError("订单重复了！");
                            break;
                        case 4:
                            listener.onError("创建订单失败！");
                            break;
                        case -1:
                            listener.onError("订单提交失败，通讯不良！");
                            break;
                        case 20:
                            listener.onError("单笔限额已经超出，请联系管理员！");
                            break;
                    }

                }

                return null;
            }
        }, listener);
    }

    /***
     * pos订单提交wft服务器，保存，并返回
     *
     * @param listener
     * @param order
     * @param pathSignatureUrl
     * @param desCode
     * @see [类、类#方法、类#成员]
     */

    public void sendOrderData(final UINotifyListener<String[]> listener, final Order order, final String pathSignatureUrl, final String desCode) {
        ThreadHelper.executeWithCallback(new Executable<String[]>() {
            @Override
            public String[] execute() throws Exception {
                UserModel userModel = LocalAccountManager.getInstance().getLoggedUser();
                order.merchantId = userModel.merchantId;
                order.employeeId = userModel.uId;
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("uId", order.employeeId + "");
                if (!TextUtils.isEmpty(order.useId)) {
                    jsonObject.put("useId", order.useId + "");// 1.3.5版本 优惠券标识
                }
                // 注意 需要传入订单号的是提前申请好的
                if (!TextUtils.isEmpty(order.orderNo)) {
                    jsonObject.put("orderId", order.orderNo);
                }

                jsonObject.put("mId", order.merchantId + "");
                jsonObject.put("remark", order.remark);
                jsonObject.put("money", order.money + "");
                jsonObject.put("clientType", order.clientType + "");
                jsonObject.put("money", order.money + "");
                jsonObject.put("imeiNo", "1397112900744");// AppHelper.getIMEI()
                jsonObject.put("posId", CouponManager.getInstance().getISNnumber());
                jsonObject.put("clientPadKey", desCode);
                jsonObject.put("transactionType", order.transactionType + ""); // 1.3.6
                // 版本交易类型
                String signatureNo;
                if (order.transactionType == 1) {
                    signatureNo = order.orderNo + ".jpg";
                } else {
                    signatureNo = null;
                }
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.ORDER_CREATE_SEND, jsonObject, signatureNo, pathSignatureUrl);
                result.setNotifyListener(listener);
                if (!result.hasError()) {
                    String state = result.data.getString("result");
                    int ret = Utils.Integer.tryParse(state, -1);
                    switch (ret) {
                        case 0:
                            String orderTime = result.data.getString("dateTime");
                            String cardNo = result.data.getString("cardNo");
                            // 保存订单信息
                            if (null == OrderDB.getInstance().getOrderByOrderNo(order.orderNo, order.employeeId)) {
                                // order.state = OrderStatusEnum.NO_PAY.getValue();
                                // 服务器时间为准
                                order.add_time = Utils.Long.tryParse(orderTime, System.currentTimeMillis());
                                order.userName = userModel.name;
                                OrderDB.getInstance().saveOrder(order);
                            }
                            String[] strs = new String[2];
                            strs[0] = cardNo;
                            strs[1] = orderTime;

                            return strs;
                        case 1:
                            listener.onError("参数错误！");
                            break;
                        case 2:
                            listener.onError("用户已经失效！");
                            break;
                        case 3:
                            listener.onError("订单重复了！");
                            break;
                        case 4:
                            listener.onError("创建订单失败！");
                            break;
                        case -1:
                            listener.onError("订单提交失败，通讯不良！");
                            break;
                        case 20:
                            listener.onError("单笔限额已经超出，请联系管理员！");
                            break;
                    }

                }

                return null;
            }
        }, listener);
    }

    /**
     * 修改订单状态
     */
    public void updateOrderState(String orderNo, OrderStatusEnum statusEnum) {
        try {
            Order orderModel = new Order();
            orderModel.orderNo = orderNo;
            // orderModel.state = statusEnum.getValue();
            orderModel.employeeId = LocalAccountManager.getInstance().getUID();
            orderModel.finishTime = System.currentTimeMillis();
            OrderDB.getInstance().updateOrderState(orderModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 查询获取订单
     *
     * @param orderNo
     * @param time
     * @param listener
     */
    public void queryLocalOrder(final String orderNo, final long time, UINotifyListener<List<Order>> listener) {
        ThreadHelper.executeWithCallback(new Executable<List<Order>>() {
            @Override
            public List<Order> execute() throws Exception {
                long uid = LocalAccountManager.getInstance().getUID();
                // System.currentTimeMillis() - day * 24 * 60 * 60 * 1000;
                return OrderDB.getInstance().queryOrder(orderNo, uid, time);
            }
        }, listener);
    }

    /**
     * 查询用户的退款记录
     */
    public void queryUserRefundOrder(final String outTradeNo, final String refundTime, final String mchId, final String refundState, final int page, final UINotifyListener<List<Order>> listener) {
        ThreadHelper.executeWithCallback(new Executable<List<Order>>() {

            @Override
            public List<Order> execute() throws Exception {
                List<Order> result = new ArrayList<Order>();
                JSONObject param = new JSONObject();
                Map<String, String> map = new HashMap<String, String>();
                if (null != outTradeNo && !"".equals(outTradeNo)) { //退款单号
                    param.put("outRefundNo", outTradeNo);
                    map.put("outRefundNo", outTradeNo);
                }
                param.put("mchId", mchId);
                if (null != refundState && !"".equals(refundState) && !refundState.equals("-1")) {
                    param.put("tradeState", refundState);
                    map.put("tradeState", refundState);
                }
                param.put("page", page + "");
                param.put("pageSize", "10");
                if (null != refundTime && !"".equals(refundTime)) {
                    param.put("addTime", refundTime);
                    map.put("addTime", refundTime);
                }

                map.put("mchId", mchId);
                map.put("page", page + "");
                map.put("pageSize", "10");

                if (MainApplication.isAdmin.equals("0") && MainApplication.isOrderAuth.equals("0")) //如果是收银员 
                {
                    param.put("userId", MainApplication.userId);//添加收银员的uid
                    map.put("userId", MainApplication.userId + "");//添加收银员的uid
                }

                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey())) {
                    param.put("sign", SignUtil.getInstance().createSign(map, MainApplication.getSignKey()));

                    Logger.i("hehui", "sign-->" + SignUtil.getInstance().createSign(map, MainApplication.getSignKey()));
                }

                RequestResult post = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/searchRefundOrder", param);
                if (!post.hasError()) {
                    int resCode = Integer.parseInt(post.data.getString("result"));
                    if (resCode == 200) {

                        JSONObject json = new JSONObject(post.data.getString("message"));
                        JSONArray jsonArray = json.getJSONArray("data");
                        if (jsonArray != null && jsonArray.length() > 0) {
                            for (int a = 0; a < jsonArray.length(); a++) {
                                JSONObject subJson = jsonArray.getJSONObject(a);
                                Order order = new Order();
                                order.setTransactionId(subJson.optString("transactionId"));
                                order.setMchName(subJson.optString("mchName"));
                                order.setTradeType(subJson.optString("tradeType", ""));
                                order.setOutTradeNo(subJson.optString("outTradeNo"));
                                order.setRefundMoney(subJson.optLong("refundFee"));
                                order.money = subJson.optInt("totalFee");
                                order.setTradeName(subJson.optString("tradeName"));
                                order.add_time = subJson.optLong("addTime");
                                order.setRefundState(subJson.optInt("refundState"));
                                order.setUseId(subJson.optString("userId", ""));
                                order.setUserName(subJson.optString("userName", ""));
                                order.setTradeTime(subJson.optString("tradeTime"));
                                order.setTradeState(subJson.optInt("tradeState", -1));
                                order.setTradeStateText(subJson.optString("tradeStateText"));
                                order.notifyTime = subJson.optString("notifyTime", "");
                                order.setRefundNo(subJson.optString("refundNo", ""));
                                order.setTradeTime(subJson.optString("tradeTime", ""));
                                order.setBody(subJson.optString("body", ""));
                                order.setOrderNoMch(subJson.optString("orderNoMch", ""));
                                order.setClient(subJson.optString("clien", ""));
                                order.setOutRefundNo(subJson.optString("outRefundNo", ""));
                                result.add(order);
                            }
                        }
                        return result;

                    } else {
                        listener.onError(post.data.getString("message"));
                    }

                } else {

                    switch (post.resultCode) {
                        case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                            listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                            return null;
                        case RequestResult.RESULT_TIMEOUT_ERROR:
                            listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            return null;
                        case RequestResult.RESULT_READING_ERROR:
                            listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            return null;
                    }
                    //                    listener.onError("对不起，没有查询到订单信息！");
                    return null;
                }
                return null;
            }

        }, listener);
    }

    /**
     * 根据单号查询退款流水
     *
     * @param orderNo
     * @param time
     * @param listener
     */
    public void queryRefundOrder(final String orderNo, final String mchId, final int page, final UINotifyListener<List<Order>> listener) {
        ThreadHelper.executeWithCallback(new Executable<List<Order>>() {
            @Override
            public List<Order> execute() throws Exception {
                Map<String, String> params = new HashMap<String, String>();
                List<Order> list = new ArrayList<Order>();
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("mchId", mchId);
                jsonObject.put("page", page);
                jsonObject.put("tradeState", "2");

                if (!TextUtils.isEmpty(orderNo)) {
                    jsonObject.put("orderNoMch", orderNo);
                    params.put("orderNoMch", orderNo);
                }
                params.put("mchId", mchId);
                params.put("page", page + "");
                params.put("tradeState", "2");
                jsonObject.put("isRefundStream", 1);
                params.put("isRefundStream", "1");
                if (MainApplication.isAdmin.equals("0") && MainApplication.isOrderAuth.equals("0")) // 
                {
                    jsonObject.put("userId", MainApplication.userId);
                    params.put("userId", String.valueOf(MainApplication.userId));
                }
                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey())) {
                    jsonObject.put("sign", SignUtil.getInstance().createSign(params, MainApplication.getSignKey()));

                }

                // jsonObject.put("pageSize", value)
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/order/querySpayOrder", jsonObject);
                result.setNotifyListener(listener);
                if (!result.hasError()) {
                    int resCode = Integer.parseInt(result.data.getString("result"));
                    if (resCode == 200) {
                        // Logger.i("hehui", "result.data-->" + result.data);
                        JSONObject json = new JSONObject(result.data.getString("message"));
                        JSONArray jsonArray = json.getJSONArray("data");
                        if (jsonArray != null && jsonArray.length() > 0) {
                            for (int a = 0; a < jsonArray.length(); a++) {

                                JSONObject subJson = jsonArray.getJSONObject(a);
                                Order order = new Order();
                                order.setTransactionId(subJson.optString("transactionId"));
                                order.setMchName(subJson.optString("mchName"));
                                order.setTradeType(subJson.optString("tradeType", ""));
                                order.setOutTradeNo(subJson.optString("outTradeNo"));
                                order.money = subJson.optInt("money");
                                order.setTradeTime(subJson.optString("tradeTime"));
                                order.setTradeName(subJson.optString("tradeName"));
                                order.add_time = subJson.optLong("addTime");
                                order.setTradeState(subJson.optInt("tradeState", -1));
                                order.notifyTime = subJson.optString("notifyTime", "");
                                order.setBody(subJson.optString("body", ""));
                                order.setOrderNoMch(subJson.optString("orderNoMch", ""));
                                order.setRufundMark(subJson.optInt("rufundMark", 0));
                                order.setOutRefundNo(subJson.optString("outRefundNo", ""));
                                order.setClient(subJson.optString("client", ""));
                                order.setTradeStateText(subJson.optString("tradeStateText", ""));
                                list.add(order);
                            }
                            return list;
                        } else {
                            listener.onError(MainApplication.getContext().getString(R.string.not_found_more_data));
                        }
                    } else {
                        listener.onError(result.data.getString("message"));
                    }

                }
                return null;
                // return OrderDB.getInstance().queryOrder(orderNo, uid, time);
            }
        }, listener);
    }

    /**
     * 根据流水统计
     *
     * @param orderNo
     * @param time
     * @param listener
     */
    public void queryOrderTotal(final String mchId, final String state, final String addTime, final String tradeFlag, final UINotifyListener<RefundTotal> listener) {
        ThreadHelper.executeWithCallback(new Executable<RefundTotal>() {
            @Override
            public RefundTotal execute() throws Exception {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("mchId", mchId);
                jsonObject.put("addTime", addTime);
                if (MainApplication.isAdmin.equals("0") && MainApplication.isOrderAuth.equals("0")) // 
                {
                    jsonObject.put("userId", MainApplication.userId);
                }

                if (tradeFlag.equals("0")) { // 流水
                    jsonObject.put("tradeState", state);
                } else {//退款
                    if (state.equals("0")) { //统计退款中的订单
                        jsonObject.put("tradeState", "4");
                    } else if (state.equals("1")) { // 退款已受理
                        jsonObject.put("tradeState", "5");
                    }
                }

                jsonObject.put("tradeFlag", tradeFlag);
                //                jsonObject.put("", value)
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/order/getOrderTotal", jsonObject);
                result.setNotifyListener(listener);
                if (!result.hasError()) {
                    int resCode = Integer.parseInt(result.data.getString("result"));
                    if (resCode == 200) {
                        try {
                            JSONObject json = new JSONObject(result.data.getString("message"));
                            RefundTotal order = new RefundTotal();
                            order.setTradeNum(json.optInt("tradeNum", 0));
                            order.setTurnover(json.optLong("turnover", 0));
                            return order;
                        } catch (Exception e) {
                            //                            listener.onError("查询订单详情失败，请稍候在试");
                            return null;
                        }
                    } else {
                        return null;
                    }
                }
                return null;

            }
        }, listener);
    }

    /**
     * 根据单号查询订单详情
     *
     * @param orderNo
     * @param time
     * @param isMark   true用outTradeNo查询，false用威富通订单号查询
     * @param listener
     */
    public void queryOrderDetail(final String orderNo, final String mchId, final boolean isMark, final UINotifyListener<Order> listener) {
        ThreadHelper.executeWithCallback(new Executable<Order>() {
            @Override
            public Order execute() throws Exception {
                JSONObject jsonObject = new JSONObject();
                if (isMark) {
                    jsonObject.put("outTradeNo", orderNo);
                } else {
                    jsonObject.put("orderNoMch", orderNo);
                }
                jsonObject.put("mchId", mchId);
                if (MainApplication.isAdmin.equals("0") && MainApplication.isOrderAuth.equals("0")) // 
                {
                    jsonObject.put("userId", MainApplication.userId);

                }

                long spayRs = System.currentTimeMillis();
                //                    //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                    jsonObject.put("spayRs", spayRs);
                    jsonObject.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(jsonObject.toString()), MainApplication.getNewSignKey()));
                }

                Logger.i("hehui", "queryOrderDetail param-->" + jsonObject);
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/queryOrderDetail", jsonObject, String.valueOf(spayRs), null);
                result.setNotifyListener(listener);
                if (!result.hasError()) {
                    int resCode = Integer.parseInt(result.data.getString("result"));
                    if (resCode == 200) {
                        try {
                            JSONObject json = new JSONObject(result.data.getString("message"));
                            Order order = new Order();
                            order.setTransactionId(json.optString("transactionId"));
                            order.setMchName(json.optString("mchName"));
                            order.setTradeType(json.optString("tradeType", ""));
                            order.setOutTradeNo(json.optString("outTradeNo"));
                            order.money = json.optInt("money");
                            order.setTradeTime(json.optString("tradeTime"));
                            order.setTradeName(json.optString("tradeName"));
                            order.add_time = json.optLong("addTime");
                            order.setTradeState(json.optInt("tradeState", -1));
                            order.setTradeStateText(json.optString("tradeStateText"));
                            order.notifyTime = json.optString("notifyTime", "");
                            order.setBody(json.optString("body", ""));
                            order.setOrderNoMch(json.optString("orderNoMch", ""));
                            order.setRufundMark(json.optInt("rufundMark", 0));
                            order.setClient(json.optString("client", ""));
                            order.setAddTime(json.optString("addTime", ""));
                            order.setUserName(json.optString("userName", ""));
                            order.setUseId(json.optString("userId", ""));
                            order.setRefundMoney(json.optLong("refundMoney", 0));
                            order.setRfMoneyIng(json.optLong("rfMoneyIng", 0));
                            order.setAffirm(json.optInt("affirm", 0));
                            order.setCanAffim(json.optInt("canAffirm", 0));
                            order.setDaMoney(json.optLong("daMoney", 0));
                            order.setIsAgainPay(json.optInt("isAgainPay", 0));
                            order.setAttach(json.optString("attach", ""));
                            order.setTradeTimeNew(json.optString("tradeTimeNew", ""));
                            order.setApiCode(json.optString("apiCode", ""));
                            order.setCashFeel(json.optLong("cashFee", 0));
                            order.setTipFee(json.optLong("tipFee", 0));
                            order.setSurcharge(json.optLong("surcharge", 0));
                            order.setWithholdingTax(json.optLong("withholdingTax", 0));
                            order.setTotalFee(json.optLong("totalFee", 0));
                            order.setOrderFee(json.optLong("orderFee", 0));

                            try {

                                if (null != json.getJSONArray("wxVcardsList")) {

                                    JSONArray jsonArrs = json.getJSONArray("wxVcardsList");
                                    List<WxCard> list = new ArrayList<WxCard>();
                                    int len = jsonArrs.length();
                                    for (int i = 0; i < len; i++) {
                                        WxCard wxCard = new WxCard();
                                        JSONObject object = jsonArrs.getJSONObject(i);
                                        wxCard.setCardId(object.optString("cardId", ""));
                                        wxCard.setDealDetail(object.optString("dealDetail", ""));
                                        wxCard.setTitle(object.optString("title", ""));
                                        wxCard.setCardCode(object.optString("cardCode", ""));
                                        wxCard.setCardType(object.optString("cardType", ""));
                                        wxCard.setGift(object.optString("gift", ""));
                                        wxCard.setDiscount(object.optInt("discount", 0));
                                        wxCard.setReduceCost(object.optInt("reduceCost", 0));
                                        list.add(wxCard);
                                    }
                                    order.setWxCardList(list);
                                }
                            } catch (Exception e) {
                            }
                            return order;

                        } catch (Exception e) {
                            listener.onError(ToastHelper.toStr(R.string.show_order_fail));
                            return null;
                        }
                        // Logger.i("hehui", "result.data-->" + result.data);
                    } else {
                        listener.onError(result.data.getString("message"));
                    }
                }

                return null;
                // return OrderDB.getInstance().queryOrder(orderNo, uid, time);
            }
        }, listener);
    }

    static final int pageSize = 10;

    /**
     * 查询服务器订单流水
     */
    public void queryOrderData(final String orderNo, final int currentPage, final int state, final String time, final int transactionType, final String orderNoMch, final UINotifyListener<OrderSearchResult> listener) {
        ThreadHelper.executeWithCallback(new Executable<OrderSearchResult>() {
            @Override
            public OrderSearchResult execute() throws Exception {
                // UserModel userModel =
                // LocalAccountManager.getInstance().getLoggedUser();
                // Order order = new Order();
                // order.employeeId = userModel.uId;
                JSONObject jsonObject = new JSONObject();
                Map<String, String> params = new HashMap<String, String>();
                if (null != orderNo && !"".equals(orderNo)) // 修改成商户单号
                {
                    jsonObject.put("orderNoMch", orderNo);
                    params.put("orderNoMch", orderNo);
                }
                jsonObject.put("mchId", MainApplication.merchantId);// todo
                params.put("mchId", MainApplication.merchantId);
                if (MainApplication.isAdmin.equals("0") && MainApplication.isOrderAuth.equals("0")) // 默认收银员只能看自己流水，开通流水权限，看到所有
                {
                    jsonObject.put("userId", MainApplication.userId);// 普通用户才需要查自己的数据，管理员可以查询全部
                    params.put("userId", MainApplication.userId + "");
                }

                if (null != orderNoMch && !"".equals(orderNoMch)) {
                    jsonObject.put("orderNoMch", orderNoMch);
                    params.put("orderNoMch", orderNoMch);
                }
                if (time != null) {
                    jsonObject.put("addTime", time);
                    params.put("addTime", time);
                }
                //                jsonObject.put("currentPage", currentPage + "");
                jsonObject.put("page", currentPage + "");
                params.put("page", currentPage + "");
                if (state != 0) {
                    jsonObject.put("tradeState", state); // OrderStatusEnum.PAY_SUCCESS.getValue()
                    params.put("tradeState", state + "");
                }
                //                jsonObject.put("pageSize", pageSize + "");
                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey())) {
                    jsonObject.put("sign", SignUtil.getInstance().createSign(params, MainApplication.getSignKey()));
                }
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.ORDER_GET_ORDER, jsonObject);
                result.setNotifyListener(listener);
                if (!result.hasError()) {
                    String state = result.data.getString("result");
                    // boolean ret = Boolean.parseBoolean(state);
                    if (Integer.parseInt(state) == 200) {
                        OrderSearchResult restults = new OrderSearchResult();
                        List<Order> list = new ArrayList<Order>();
                        restults.orders = list;
                        if (result.data.getString("message").equalsIgnoreCase("[]")) {
                            return null;
                        }
                        try {
                            String strList = result.data.getString("message");
                            JSONObject jObject = new JSONObject(strList);
                            String jsData = jObject.getString("data");
                            JSONArray jArray = new JSONArray(jsData);
                            if (jArray != null && jArray.length() > 0) {
                                int len = jArray.length();

                                for (int i = 0; i < len; i++) {
                                    JSONObject o = jArray.getJSONObject(i);
                                    Order order = new Order();
                                    order.setBody(o.optString("bode", ""));
                                    order.setMchName(o.optString("mchName", ""));
                                    order.setOutTradeNo(o.optString("outTradeNo", ""));
                                    order.setClient(o.optString("client", ""));
                                    order.setOrderNoMch(o.optString("orderNoMch", ""));
                                    order.setTransactionId(o.optString("transactionId", ""));
                                    order.setAdd_time(o.optLong("addTime", 0));
                                    order.setNotify_time(o.optLong("notifyTime", 0));
                                    order.setMoney(o.optLong("money", 0));
                                    order.setTradeType(o.optString("tradeType", ""));
                                    order.setTradeStateText(o.optString("tradeStateText"));
                                    order.setTradeState(o.optInt("tradeState", -1));
                                    order.setAffirm(o.optInt("affirm", 0));
                                    order.setTradeName(o.optString("tradeName", "UNKOWN"));
                                    list.add(order);

                                }

                            }

                            return restults;

                        } catch (Exception e) {
                            listener.onError(ToastHelper.toStr(R.string.show_order_stream));
                            return null;
                        }

                    } else {
                        listener.onError(ToastHelper.toStr(R.string.show_order_stream));
                        return null;
                    }
                }
                return null;

            }
        }, listener);
    }

    public Order getOrderByTime() {
        UserModel userModel = LocalAccountManager.getInstance().getLoggedUser();
        try {
            return OrderDB.getInstance().queryOrderByLastTime(userModel.uId);
        } catch (Exception e) {
            Logger.i(e.getMessage());
            return null;
        }
    }

    private List<Order> paseJsonToList(JSONObject json, OrderSearchResult restults) throws JSONException {
        JSONArray jsonArray = json.getJSONArray("message");
        Integer totle = 0;

        List<Order> list = new ArrayList<Order>();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject j = jsonArray.getJSONObject(i);

            Order order = new Order();

            order.setAddTime(j.optString("addTime"));
            order.setNotifyTime(j.optString("notifyTime"));
            order.setOrderNo(j.optString("outTradeNo", ""));
            order.setBankName(j.optString("tradeName", ""));
            order.setMoney(j.optLong("money"));
            totle = totle + j.getInt("money");
            // order.setState(j.optInt("tradeState"));

            list.add(order);
        }
        if (restults != null) {
            restults.total = totle;
        }
        return list;
    }

    /**
     * 普通，管理用户可以查询 管理用户查询是所有用户中的最后一笔订单
     */
    public void queryOrderByOrderNo(final String orderNo, final String payType, final UINotifyListener<Order> listener) {
        ThreadHelper.executeWithCallback(new Executable<Order>() {
            @Override
            public Order execute() throws Exception {
                Map<String, String> params = new HashMap<String, String>();
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("outTradeNo", orderNo);
                // 普通用户才需要查自己的数据，管理员可以查询全部
                jsonObject.put("money", "1");// todo
                //                if (null != payType && !"".equals(payType) && payType.equals(MainApplication.PAY_WX_MICROPAY))
                //                {
                //                    jsonObject.put("service", MainApplication.PAY_WX_QUERY);
                //                    params.put("service", MainApplication.PAY_WX_QUERY);
                //                }
                //                else if (null != payType && !"".equals(payType) && payType.equals(MainApplication.PAY_ZFB_MICROPAY))
                //                {
                //                    jsonObject.put("service", MainApplication.PAY_ZFB_QUERY);
                //                    params.put("service", MainApplication.PAY_ZFB_QUERY);
                //                }
                //                else
                //                {
                //                }
                jsonObject.put("service", MainApplication.PAY_ZFB_QUERY);
                params.put("service", MainApplication.PAY_ZFB_QUERY);

                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    jsonObject.put("mchId", MainApplication.merchantId);
                    jsonObject.put("userId", MainApplication.userId);
                    params.put("mchId", MainApplication.merchantId);
                    params.put("userId", MainApplication.userId + "");
                } else {
                    params.put("mchId", MainApplication.getMchId());
                    jsonObject.put("mchId", MainApplication.getMchId());
                    jsonObject.put("userId", MainApplication.getUserId());
                    params.put("userId", MainApplication.getUserId() + "");
                }

                params.put("money", "1");
                params.put("outTradeNo", orderNo);

                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey())) {
                    jsonObject.put("sign", SignUtil.getInstance().createSign(params, MainApplication.getSignKey()));
                    Logger.i("hehui", "jsonObject-->" + jsonObject.toString());
                }

                long spayRs = System.currentTimeMillis();
                //                    //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                    jsonObject.put("spayRs", spayRs);
                    jsonObject.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(jsonObject.toString()), MainApplication.getNewSignKey()));
                }

                try {
                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.ORDER_GETLAST_TTIME, jsonObject, String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    Logger.i("hehui", "result.data-->" + result.data);
                    if (!result.hasError()) {

                        if (Integer.parseInt(result.data.getString("result")) == 200) {
                            JSONObject jObject = new JSONObject(result.data.getString("message"));

                            Order order = new Order();
                            String money = jObject.optString("money", "");
                            if (null != money && !"".equals(money) && !money.equals("null")) {
                                order.setMoney(Long.parseLong(money));
                            }
                            String tradeState = jObject.optString("tradeState", "");
                            Integer state = jObject.optInt("state", 1);
                            order.setState(String.valueOf(state));
                            Logger.i("hehui", "result.data.getString-->" + result.data.getString("message"));
                            order.setOutTradeNo(jObject.optString("outTradeNo", ""));
                            order.setTradeType(jObject.optString("service", ""));
                            order.setTradeName(jObject.optString("tradeName", ""));
                            order.setTradeTime(jObject.optString("tradeTime", ""));
                            order.setTransactionId(jObject.optString("transactionId", ""));
                            order.setBody(jObject.optString("body", ""));
                            order.setOrderNoMch(jObject.optString("orderNoMch", ""));
                            order.setOpenid(jObject.optString("subOpenID", ""));
                            order.setAddTime(jObject.optString("addTime", ""));
                            order.setNotifyTime(jObject.optString("notifyTime", ""));
                            order.setClient(jObject.optString("client", ""));
                            order.setDaMoney(jObject.optLong("daMoney", 0));
                            order.setApiCode(jObject.optString("apiCode", ""));
                            order.setSurcharge(jObject.optLong("surcharge", 0));
                            order.setWithholdingTax(jObject.optLong("withholdingTax", 0));
                            order.setCashFeel(jObject.optLong("cashFee", 0));
                            order.setOrderFee(jObject.optLong("orderFee", 0));
                            order.setTotalFee(jObject.optLong("totalFee", 0));
                            try {

                                if (null != jObject.getJSONArray("wxVcardsList")) {

                                    JSONArray jsonArrs = jObject.getJSONArray("wxVcardsList");
                                    List<WxCard> list = new ArrayList<WxCard>();
                                    int len = jsonArrs.length();
                                    for (int i = 0; i < len; i++) {
                                        WxCard wxCard = new WxCard();
                                        JSONObject object = jsonArrs.getJSONObject(i);
                                        wxCard.setCardId(object.optString("cardId", ""));
                                        wxCard.setDealDetail(object.optString("dealDetail", ""));
                                        wxCard.setTitle(object.optString("title", ""));
                                        wxCard.setCardType(object.optString("cardType", ""));
                                        wxCard.setGift(object.optString("gift", ""));
                                        wxCard.setDiscount(object.optInt("discount", 0));
                                        wxCard.setReduceCost(object.optInt("reduceCost", 0));
                                        list.add(wxCard);
                                    }
                                    order.setWxCardList(list);
                                }
                            } catch (Exception e) {
                            }
                            order.tradeState = state;

                            Logger.i("hehui", "orderNoMch-->" + order.getOrderNoMch());
                            // JSONObject mchObject = jObject.getJSONObject("mch");
                            //
                            // order.setMchName(mchObject.optString("name", ""));

                            return order;
                        } else {
                            Logger.i("hehui", "queryOrderByOrderNo " + result.data.getString("message"));
                            listener.onError(result.data.getString("message"));
                        }
                    }

                    return null;
                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                    return null;
                }

            }
        }, listener);

    }

    /**
     * 统一冲正接口 <功能详细描述>
     *
     * @param orderNo
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void unifiedPayReverse(final String orderNo, final UINotifyListener<Order> listener) {
        ThreadHelper.executeWithCallback(new Executable<Order>() {
            @Override
            public Order execute() throws Exception {

                JSONObject jsonObject = new JSONObject();
                Map<String, String> params = new HashMap<String, String>();
                //                jsonObject.put("outTradeNo", orderNo);

                //                jsonObject.put("money", "1");// todo

                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    //                    jsonObject.put("mchId", MainApplication.merchantId);// todo
                    //                    jsonObject.put("userId", MainApplication.userId);// 普通用户才需要查自己的数据，管理员可以查询全部
                    params.put("mchId", MainApplication.merchantId);
                    params.put("userId", MainApplication.userId + "");
                } else {
                    //                    jsonObject.put("mchId", MainApplication.getMchId());// todo
                    //                    jsonObject.put("userId", MainApplication.getUserId());// 普通用户才需要查

                    params.put("mchId", MainApplication.getMchId());
                    params.put("userId", MainApplication.getUserId() + "");
                }

                params.put("outTradeNo", orderNo);
                //                params.put("money", "1");

                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey())) {
                    jsonObject.put("sign", SignUtil.getInstance().createSign(params, MainApplication.getSignKey()));

                    Logger.i("hehui", "sign-->" + SignUtil.getInstance().createSign(params, MainApplication.getSignKey()));
                }

                long spayRs = System.currentTimeMillis();
                //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {

                    params.put("spayRs", String.valueOf(spayRs));
                    params.put("nns", SignUtil.getInstance().createSign(params, MainApplication.getNewSignKey()));
                }

                // }
                Logger.i("hehui", "开始调用冲正接口完成参数-->" + jsonObject.toString());
                try {

                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/unifiedPayReverse", JsonUtil.mapToJson(params), String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    Logger.i("hehui", "调用冲正接口完成-->" + result.data);
                    if (!result.hasError()) {

                        if (result.data != null) {
                            int code = Integer.parseInt(result.data.getString("result"));
                            if (code == 200) {

                                JSONObject jObject = new JSONObject(result.data.getString("message"));

                                Order order = new Order();
                                order.setOutTradeNo(jObject.optString("outTradeNo", ""));
                                order.setState(jObject.optString("tradeState", ""));
                                order.setTradeType(jObject.optString("tradeType", ""));
                                order.setTradeName(jObject.optString("tradeName", ""));
                                order.setTradeTime(jObject.optString("tradeTime", ""));
                                order.setTransactionId(jObject.optString("transactionId", ""));
                                order.setMchName(jObject.optString("mchName", ""));

                                return order;

                            } else {
                                listener.onError(result.data.getString("message"));
                            }
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                        }
                    }

                    return null;
                } catch (Exception e) {

                    listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                    return null;
                }
            }
        }, listener);

    }

    /**
     * 冲正接口 <功能详细描述>
     *
     * @param orderNo
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void payReverse(final String orderNo, final String payType, final UINotifyListener<Order> listener) {
        ThreadHelper.executeWithCallback(new Executable<Order>() {
            @Override
            public Order execute() throws Exception {

                JSONObject jsonObject = new JSONObject();
                Map<String, String> params = new HashMap<String, String>();
                jsonObject.put("outTradeNo", orderNo);
                jsonObject.put("money", "1");// todo
                if (payType.equals(MainApplication.PAY_WX_MICROPAY)) {
                    Logger.i(TAG, "开始调用微信冲正接口");
                    jsonObject.put("service", "pay.weixin.micropay.reverse");
                    params.put("service", "pay.weixin.micropay.reverse");
                } else if (payType.equals(MainApplication.PAY_ZFB_MICROPAY)) {
                    jsonObject.put("service", "pay.alipay.micropay.reverse");
                    params.put("service", "pay.alipay.micropay.reverse");
                    Logger.i(TAG, "开始调用支付宝冲正接口");
                }

                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    jsonObject.put("mchId", MainApplication.merchantId);
                    params.put("mchId", MainApplication.merchantId);
                    jsonObject.put("userId", MainApplication.userId);
                    params.put("userId", MainApplication.userId + "");
                } else {
                    params.put("mchId", MainApplication.getMchId());
                    jsonObject.put("mchId", MainApplication.getMchId());
                    jsonObject.put("userId", MainApplication.getUserId());
                    params.put("userId", MainApplication.getUserId() + "");
                }

                params.put("outTradeNo", orderNo);
                params.put("money", "1");

                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey())) {
                    jsonObject.put("sign", SignUtil.getInstance().createSign(params, MainApplication.getSignKey()));

                    Logger.i("hehui", "sign-->" + SignUtil.getInstance().createSign(params, MainApplication.getSignKey()));
                }

                // }
                Logger.i(TAG, "开始调用冲正接口完成参数-->" + jsonObject.toString());
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/payReverse", jsonObject, null, null);
                result.setNotifyListener(listener);
                Logger.i(TAG, "调用冲正接口完成-->" + result.data);
                if (!result.hasError()) {

                    if (result.data != null) {
                        int code = Integer.parseInt(result.data.getString("result"));
                        if (code == 200) {

                            JSONObject jObject = new JSONObject(result.data.getString("message"));

                            Order order = new Order();
                            order.setOutTradeNo(jObject.optString("outTradeNo", ""));
                            order.setState(jObject.optString("tradeState", ""));
                            order.setTradeType(jObject.optString("tradeType", ""));
                            order.setTradeName(jObject.optString("tradeName", ""));
                            order.setTradeTime(jObject.optString("tradeTime", ""));
                            order.setTransactionId(jObject.optString("transactionId", ""));
                            order.setMchName(jObject.optString("mchName", ""));

                            return order;

                        } else {
                            listener.onError(ToastHelper.toStr(R.string.show_rever_fail));
                        }
                    }
                }

                return null;
            }
        }, listener);

    }

    /**
     * 查询财付通订单信息
     */
    /*public Order queryOrderByCFTService(String orderNo) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(ApiConstant.P_PARTNER, ApiConstant.PARTNET);
        params.put(ApiConstant.P_OUT_TRADE_NO, orderNo);
        params.put(ApiConstant.P_INPUT_CHARSET, ApiConstant.INPUT_CHARTE);

        String urlParams = AsciiOrderUtils.getUrlAscciParameter(params);
        String md5urlParams = MD5.md5s(urlParams + "&" + ApiConstant.P_KEY + "=" + ApiConstant.KEY).toUpperCase();

        String url = ApiConstant.CFT_QUERY_ORDER_URL + urlParams + "&sign=" + md5urlParams;

        HttpRequester request = new HttpRequester();
        try {
            HttpRespons response = request.sendGet(url);
            String xmlContent = response.getContent();
            Order order = parseXML(xmlContent);
            return order;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }*/

    /*private Order parseXML(String xmlContent) throws Exception {
        String[] field = new String[]{"trade_state", "trade_mode", "partner", "bank_type", "bank_billno", "total_fee", "fee_type", "transaction_id", "out_trade_no", "attach", "time_end", "transport_fee", "product_fee", "discount", "buyer_alias"};
        String[] methods = new String[]{"setTradeState|Integer", "setTradeMode|Integer", "setPartner|String", "setBankType|String", "setBankBillno|String", "setTotalFee|Integer", "setFeeType|Integer", "setTransactionId|String", "setOutTradeNo|String", "setAttach|String", "setTimeEnd|String", "setTransportFee|Integer", "setProductFee|Integer", "setDiscount|Integer", "setBuyerAlias|String"};
        SAXReader reader = new SAXReader();
        Document doc = reader.read(new ByteArrayInputStream(xmlContent.getBytes()));
        Element root = doc.getRootElement();
        Element retcodeElement = root.element("retcode");
        String retcode = retcodeElement.getText();
        if (retcode != null && retcode.equals("0")) {
            Class<?> clazz = Class.forName(Order.class.getName());
            Order order = (Order) clazz.newInstance();
            for (int i = 0; i < field.length; i++) {
                Element element = root.element(field[i]);
                if (element != null && !element.getText().equals("")) {
                    String[] temp = methods[i].split("\\|");
                    if (temp[1].equals("Integer")) {
                        Method method = clazz.getMethod(temp[0], Integer.class);
                        method.invoke(order, Integer.parseInt(element.getText()));
                    } else if (temp[1].equals("String")) {
                        Method method = clazz.getMethod(temp[0], String.class);
                        method.invoke(order, element.getText());
                    }
                }
            }
            return order;
        }
        return null;
    }*/

    //    public void updateTradeState(final OrderBena orderBena, final UINotifyListener<Boolean> listener)
    //    {
    //        ThreadHelper.executeWithCallback(new Executable<Boolean>()
    //        {
    //            @Override
    //            public Boolean execute()
    //                throws Exception
    //            {
    //                JSONObject json = new JSONObject();
    //                json.put("mchId", MainApplication.merchantId);
    //                json.put("outTradeNo", orderBena.getOutTradeNo());
    //                json.put("servicesType", orderBena.getService());
    //                json.put("money", orderBena.getMoeny());
    //                RequestResult result =
    //                    NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/updateTradeState", json, null, null);
    //                result.setNotifyListener(listener);
    //                if (!result.hasError())
    //                {
    //                    String res = result.data.getString("result");
    //                    if (res.equals("200"))
    //                    {
    //                        return true;
    //                        
    //                    }
    //                    else
    //                    {
    //                        
    //                        return false;
    //                    }
    //                }
    //                return false;
    //            }
    //        },
    //            listener);
    //    }

    /**
     * 微信发红包
     */
    public void wxRedpack(final String openid, final UINotifyListener<Order> listener) {
        ThreadHelper.executeWithCallback(new Executable<Order>() {
            @Override
            public Order execute() throws Exception {

                JSONObject json = new JSONObject();
                //                Map<String, String> params = new HashMap<String, String>();
                Order order = new Order();
                //                json.put("appkey", "000001");
                //                json.put("customid", "1");
                //                String userKey = "405eabe576778167f839f91717ba5902";
                json.put("openid", openid);
                //                params.put("appkey", "000001");
                //                params.put("customid", "1");
                //                params.put("openid", openid + userKey);

                //                json.put("sign", SignUtil.getInstance().createRedpackSign(params));

                //                String reslut =
                //                    PacteraHttp.getInstance().post(ApiConstant.wxCardUrl + "wxredpack/sendWxRedpack", json.toString());

                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/sendRedPack", json, null, null);
                result.setNotifyListener(listener);
                Logger.i("hehui", "wxRedpack-->" + result.data);
                try {

                    if (!result.hasError()) {
                        String res = result.data.getString("result");
                        if (res != null) {
                            //                            boolean ret = Boolean.parseBoolean(res);
                            if (res.equals("200")) {
                                return order;
                            } else {
                                return null;
                            }

                        }
                    }
                } catch (Exception e) {
                    listener.onError("红包发送失败，请稍后再试..");
                }

                return null;

            }
        }, listener);
    }

    /**
     * 微信卡券核销
     */
    public void wxCard(final String card, final UINotifyListener<Order> listener) {
        ThreadHelper.executeWithCallback(new Executable<Order>() {
            @Override
            public Order execute() throws Exception {

                JSONObject json = new JSONObject();
                Order order = new Order();
                json.put("code", card);
                //                json.put("cardId", "psMSkt7LXGRykr-N3Kk3dH_1QJo8");
                //                RequestResult result = NetHelper.httpsPost(ApiConstant.wxCardUrl + "wxcard/consume", json, null, null);
                String reslut = PacteraHttp.getInstance().post(ApiConstant.wxCardUrl + "wxcard/consume", json.toString());

                Logger.i("hehui", "result-->" + reslut);
                if (null == reslut || "".equals(reslut)) {
                    return null;
                }
                try {

                    JSONObject jsonObject = new JSONObject(reslut);

                    Boolean status = Boolean.parseBoolean(jsonObject.optString("status", ""));
                    if (status) {
                        JSONObject j = jsonObject.getJSONObject("data");
                        JSONObject js = j.getJSONObject("card").getJSONObject("cash").getJSONObject("base_info");
                        order.setTitle(js.optString("title", ""));
                        order.setCode(card);
                        order.setAddTime(j.optString("useTime", ""));
                        return order;
                    } else {
                        listener.onError(jsonObject.optString("errmsg", ""));
                    }
                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.show_affirm_fail));
                }

                return null;

            }
        }, listener);
    }

    /**
     * 查询服务器订单
     */
    public void queryDailyerReport(final String time, final UINotifyListener<DailyStatement> listener) {
        ThreadHelper.executeWithCallback(new Executable<DailyStatement>() {
            @Override
            public DailyStatement execute() throws Exception {

                UserModel userModel = LocalAccountManager.getInstance().getLoggedUser();
                JSONObject json = new JSONObject();
                json.put("employeeId", userModel.uId + "");
                json.put("addTime", time + "");
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.ORDER_GET_ORDER_DAY_TRADE, json, null, null);
                result.setNotifyListener(listener);
                if (!result.hasError()) {
                    String res = result.data.getString("result");
                    if (res != null) {
                        boolean ret = Boolean.parseBoolean(res);
                        if (ret) {
                            String o = result.data.getString("dailyStatement");
                            Gson gson = new Gson();
                            DailyStatement d = gson.fromJson(o, DailyStatement.class);
                            return d;
                        } else {
                            return null;
                        }

                    }
                }
                return null;
            }
        }, listener);
    }

    /**
     * 收银员流水统计 <功能详细描述>
     *
     * @param query
     * @param startTime
     * @param endTime
     * @param listener
     * @see [类、类#方法、类#成员]
     */

    public void queryOrderCashierTotal(final String mchId, final String userId, final UINotifyListener<OrderReport> listener) {
        ThreadHelper.executeWithCallback(new Executable<OrderReport>() {
            @Override
            public OrderReport execute() throws Exception {
                OrderReport orderReport = new OrderReport();
                JSONObject json = new JSONObject();
                json.put("mchId", mchId);
                // json.put("userId", userId);
                json.put("tradeTime", "2015-3-17");

                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/order/searchUserCount", json, null, null);
                result.setNotifyListener(listener);
                if (!result.hasError()) {
                    switch (result.resultCode) {
                        case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                            listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                            return null;
                        case RequestResult.RESULT_TIMEOUT_ERROR:
                            listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            return null;
                        case RequestResult.RESULT_READING_ERROR:
                            listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                            return null;
                    }
                    String res = result.data.getString("result");
                    if (res != null) {
                        if (Integer.parseInt(res) == 200) {
                            List<CashierReport> cashierReports = new ArrayList<CashierReport>();

                            String strList = result.data.getString("message");

                            JSONArray jsonArray = new JSONArray(strList);

                            for (int i = 0; i < jsonArray.length(); i++) {
                                CashierReport report = new CashierReport();
                                JSONObject object = jsonArray.getJSONObject(i);
                                report.setAddTime(object.optString("addTime", ""));
                                report.setRefundFee(object.optLong("refundFee", 0));
                                report.setRefundNum(object.optInt("refundNum", 0));
                                report.setTurnover(object.optLong("turnover", 0));
                                Gson gson = new Gson();
                                List<StreamTotabean> streamTotabean = gson.fromJson(object.getString("childs"), new TypeToken<List<StreamTotabean>>() {
                                }.getType());

                                report.setStreamTotabeans(streamTotabean);

                                cashierReports.add(report);

                            }

                            orderReport.cashierReports = cashierReports;
                            return orderReport;
                        } else {
                            return null;
                        }

                    }
                }
                return null;
            }
        }, listener);
    }

    /**
     * 根据时间范围统计成功笔数金额
     */
    public void queryOrderReport(final CacheEnum query, final String startTime, final String endTime, final UINotifyListener<OrderReport> listener) {
        ThreadHelper.executeWithCallback(new Executable<OrderReport>() {
            @Override
            public OrderReport execute() throws Exception {
                OrderReport orderReport = new OrderReport();
                UserModel userModel = LocalAccountManager.getInstance().getLoggedUser();
                CacheModel cacheModel = new CacheModel();
                cacheModel.addTime = System.currentTimeMillis();
                cacheModel.type = query.getValue();
                cacheModel.uId = LocalAccountManager.getInstance().getUID();
                // 先去取本地数据
                // boolean isCache =
                // CacheInfoManager.getInstence().isUpdate(cacheModel);
                // if (!isCache)
                // {
                // List<CashierReport> lists =
                // CacheInfoManager.getInstence()
                // .readTextInputStream(userModel.uId + "-" +
                // query.getDisplayName());
                // if (lists != null && lists.size() > 0)
                // {
                // orderReport.cashierReports = lists;
                // CashierReportSum cs =
                // ReportSumDB.getInstance().queryByUId(query.getValue().intValue(),
                // userModel.uId);
                // orderReport.cashierReportSum = cs;
                // return orderReport;
                // }
                //
                // }

                JSONObject json = new JSONObject();
                json.put("mchId", MainApplication.merchantId);
                json.put("startTime", startTime + "");
                json.put("endTime", endTime + "");
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.ORDER_GET_ORDER_DAY_TRADE_By, json, null, null);
                result.setNotifyListener(listener);

                if (!result.hasError()) {

                    String res = result.data.getString("result");
                    if (res != null) {
                        if (Integer.parseInt(res) == 200) {
                            String strList = result.data.getString("message");
                            // String total = result.data.getString("total");
                            Gson gson = new Gson();
                            List<CashierReport> cashierReports = gson.fromJson(strList, new TypeToken<List<CashierReport>>() {
                            }.getType());
                            // CashierReportSum cashierReportSum =
                            // gson.fromJson(total, new
                            // TypeToken<CashierReportSum>()
                            // {
                            // }.getType());
                            // 缓存下
                            // CacheInfoManager.getInstence().writeCashierReportFile(userModel.uId
                            // + "-"
                            // + query.getDisplayName(),
                            // cashierReports);
                            orderReport.cashierReports = cashierReports;
                            // orderReport.cashierReportSum = cashierReportSum;

                            // 保存统计数据
                            // ReportSumDB.getInstance().delete(query.getValue().intValue(),
                            // userModel.uId);
                            // cashierReportSum.flag =
                            // query.getValue().intValue();
                            // cashierReportSum.uId = userModel.uId;
                            // ReportSumDB.getInstance().save(cashierReportSum);

                            return orderReport;
                        } else {
                            return null;
                        }

                    }
                }
                return null;
            }
        }, listener);
    }

    // //////////////////////////////////////////////////////////////////////////////

    /**
     * 提交离线订单 金额，订单号，时间 1.本地
     */
    public List<Order> getOfflineOrder() {
        long uid = LocalAccountManager.getInstance().getUID();
        try {
            return OrderDB.getInstance().queryOrderByType(OrderTypeEnum.OFFLINE.getValue(), uid);
        } catch (SQLException e) {
            Logger.i(e);
            return null;
        }
    }

    /**
     * 离线订单状态修改
     */
    public void updateOfflineOrder(List<Order> orders) {
        long uid = LocalAccountManager.getInstance().getUID();
        OrderDB.getInstance().updateLocalOrderType(orders, OrderTypeEnum.OFFLINE_FINISH.getValue(), uid);
    }

    // //////////////////////////////////////获取服务器UUID信息///////////////////////////////////////////////
    public void doCFTGetUUID(final String money, final UINotifyListener<QRcodeInfo> listener) {
        // doCreateOrderThree
        ThreadHelper.executeWithCallback(new Executable<QRcodeInfo>() {
            @Override
            public QRcodeInfo execute() throws Exception {
                String orderNo = createOrderNumber();
                String mId = LocalAccountManager.getInstance().getLoggedUser().merchantId;
                UserModel userModel = LocalAccountManager.getInstance().getLoggedUser();
                JSONObject json = new JSONObject();
                json.put("uId", userModel.uId + "");
                json.put("orderId", orderNo);
                json.put("mId", mId + "");
                json.put("money", money);
                json.put("clientType", ApiConstant.pad + "");
                json.put("uuidIpUrl", ApiConstant.BASE_URL_PORT + "/swiftUuidPro/uuid/findUuid.action?url=");// http://test.swiftpass.cn:8081
                long t = System.currentTimeMillis();
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.TEST_CFT_ORDER, json, null, null);
                result.setNotifyListener(listener);
                if (!result.hasError()) {
                    long tt = System.currentTimeMillis() - t;
                    Logger.d("time", "连接服务器获取UUID时间:" + tt);
                    String res = result.data.getString("result");
                    if (res != null) {
                        int ret = Utils.Integer.tryParse(res, -1);
                        switch (ret) {
                            case 0:
                                QRcodeInfo qRcodeInfo = new QRcodeInfo();
                                qRcodeInfo.appId = result.data.getString("appid");
                                qRcodeInfo.reqKey = result.data.getString("req_key");
                                qRcodeInfo.uuId = result.data.getString("uuid");
                                qRcodeInfo.orderNo = orderNo;

                                setqRcodeInfo(qRcodeInfo);
                                // save db
                                Order order = new Order();
                                order.orderNo = orderNo;
                                order.employeeId = userModel.uId;
                                // 保存订单信息
                                if (null == OrderDB.getInstance().getOrderByOrderNo(order.orderNo, order.employeeId)) {
                                    // order.state =
                                    // OrderStatusEnum.NO_PAY.getValue();
                                    order.add_time = System.currentTimeMillis();
                                    order.userName = userModel.name;
                                    OrderDB.getInstance().saveOrder(order);
                                }

                                return qRcodeInfo;
                            case 1:
                                listener.onError("参数错误！");
                                break;
                            case 2:
                                listener.onError("无效用户！");
                                break;
                            case 3:
                                listener.onError("订单号重复");
                                break;
                            case 4:
                                listener.onError("订单提交存入服务器出错");
                                break;

                        }

                    }
                }
                //listener.onError("系统繁忙，请稍候再试");
                return null;
            }
        }, listener);

    }

    void updateType(String paytype) {

        if (paytype.startsWith(MainApplication.PAY_ZFB_NATIVE)) {
            paytype = MainApplication.PAY_ZFB_NATIVE1;
        }
    }

    /**
     * 统一扫码单 <功能详细描述>
     *
     * @param money
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void unifiedNativePay(final String money, final String payType, final long adm, final List<WxCard> vardOrders, final String outTradeNo, final UINotifyListener<QRcodeInfo> listener) {
        ThreadHelper.executeWithCallback(new Executable<QRcodeInfo>() {
            @Override
            public QRcodeInfo execute() throws Exception {
                JSONArray jsonArray = new JSONArray();
                Map<String, String> params = new HashMap<String, String>();
                JSONObject json = new JSONObject();
                updateType(payType);
                json.put("money", money);


                if (outTradeNo != null) {
                    json.put("outTradeNo", outTradeNo);
                    params.put("outTradeNo", outTradeNo);
                }

                if (adm > 0) {
                    json.put("daMoney", adm); // 优惠金额
                    params.put("daMoney", String.valueOf(adm));
                }
                if (MainApplication.isAdmin.equals("0")) // 收银员
                {
                    json.put("uId", MainApplication.userId);
                    if (null != MainApplication.realName && !"".equals(MainApplication.realName) && !MainApplication.realName.equals("null")) {
                        json.put("userName", MainApplication.realName); // 收银员名称
                        params.put("userName", MainApplication.realName);
                    }
                    params.put("uId", MainApplication.userId + "");
                }
                if (null != MainApplication.body && !"".equals(MainApplication.body) && !MainApplication.body.equals("null")) {
                    json.put("body", MainApplication.body);
                    params.put("body", MainApplication.body);
                } else {
                    json.put("body", ApiConstant.body);
                    params.put("body", ApiConstant.body);
                }

                json.put("client", MainApplication.CLIENT);
                params.put("client", MainApplication.CLIENT);

                try {
                    String mchId = MainApplication.merchantId;
                    if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                        json.put("mchId", MainApplication.merchantId);
                        params.put("mchId", MainApplication.merchantId);
                    } else {
                        params.put("mchId", MainApplication.getMchId());
                        json.put("mchId", MainApplication.getMchId());
                        mchId = MainApplication.getMchId();
                    }
                    params.put("money", money);
                    Integer apiCode = Integer.parseInt(payType);
                    params.put("apiCode", apiCode + "");
                    json.put("apiCode", apiCode + "");
                    Object payTypeMap = SharedPreUtile.readProduct("payTypeMap" + ApiConstant.bankCode + mchId);

                    Map<String, String> map = (Map<String, String>) payTypeMap;
                    if (map.size() > 0) {
                        params.put("service", map.get(payType));
                        json.put("service", map.get(payType));
                    }

                } catch (Exception e) {
                    params.put("service", payType);
                    json.put("service", payType);
                }

                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey())) {
                    json.put("sign", SignUtil.getInstance().createSign(params, MainApplication.getSignKey()));

                }

                long spayRs = System.currentTimeMillis();
                //                    //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                    json.put("spayRs", spayRs);
                    json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                    json.remove("spayRs");
                }

                String url = ApiConstant.BASE_URL_PORT + "spay/unifiedNativePay";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;

                try {
                    Logger.i("hehui", "unifiedNativePay req params -->" + json.toString());
                    RequestResult result = NetHelper.httpsPost(url, json, String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    Logger.i("hehui", "unifiedNativePay result data-->" + result.data.getString("message"));
                    if (!result.hasError()) {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200) {
                            JSONObject jsObject = new JSONObject(result.data.getString("message"));
                            QRcodeInfo bean = new QRcodeInfo();
                            String type = jsObject.optString("service", "");
                            if (!StringUtil.isEmptyOrNull(type) && type.equals(MainApplication.PAY_QQ_NATIVE1)) {
                                String codeImg = jsObject.optString("codeImg", "");
                                String code = codeImg.substring(0, codeImg.lastIndexOf("pay"));
                                String tokenId = codeImg.substring(codeImg.lastIndexOf("=") + 1);
                                bean.uuId = code + "pay/qqpay?token_id=" + tokenId;
                                Logger.i("hehui", "unifiedNativePay code-->" + bean.uuId);
                            } else {
                                String codeUlr = jsObject.optString("uuid", "");
                                if (!TextUtils.isEmpty(codeUlr) && !codeUlr.equals("null")) {
                                    bean.uuId = codeUlr;
                                } else {
                                    // 截取code_img_url
                                    String code_img_url = jsObject.optString("codeImg", "");
                                    if (!TextUtils.isEmpty(code_img_url)) {
                                        bean.uuId = code_img_url.substring(code_img_url.lastIndexOf("=") + 1);
                                    }

                                }
                            }
                            //                            bean.uuId = jsObject.optString("uuid", "");
                            //                            }
                            bean.totalMoney = jsObject.optString("money", "");
                            bean.payType = payType;
                            bean.orderNo = jsObject.optString("outTradeNo", "");
                            try {
                                Integer apiCode = Integer.parseInt(payType);
                                bean.setApiCode(payType);
                            } catch (Exception e) {
                            }
                            return bean;
                        } else {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }

                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                        }
                    }

                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.show_order_submit_fail));
                }

                return null;
            }
        }, listener);

    }

    private String paramsToJson(List<WxCard> vardOrders, JSONArray jsonArray) throws JSONException {
        for (int i = 0; i < vardOrders.size(); i++) {
            JSONObject jObject = new JSONObject();
            WxCard order = vardOrders.get(i);
            jObject.put("cardId", order.getCardId());
            jObject.put("title", order.getTitle());
            jObject.put("dealDetail", order.getDealDetail());
            jObject.put("cardCode", order.getCardCode());
            jsonArray.put(jObject);
        }

        return jsonArray.toString();
    }

    /**
     * 提交被扫支付 <功能详细描述>
     *
     * @param money
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    public void doCoverOrder(final String code, final String money, final String payType, final List<WxCard> vardOrders, final long discountAmount, final String outTradeNo, final UINotifyListener<QRcodeInfo> listener) {
        ThreadHelper.executeWithCallback(new Executable<QRcodeInfo>() {
            @Override
            public QRcodeInfo execute() throws Exception {
                JSONArray jsonArray = new JSONArray();
                Map<String, String> params = new HashMap<String, String>();
                JSONObject json = new JSONObject();


                if (payType != null) {
                    json.put("service", payType);
                    params.put("service", payType);
                }

                if (outTradeNo != null) {
                    json.put("outTradeNo", outTradeNo);
                    params.put("outTradeNo", outTradeNo);
                }

                if (discountAmount > 0) {
                    json.put("daMoney", discountAmount); // 优惠金额
                    params.put("daMoney", String.valueOf(discountAmount));
                }
                // json.put("orderId",orderNo);

                json.put("money", money);
                // json.put("clientType", ApiConstant.SPAY_SUB + "");
                json.put("authCode", code);
                json.put("client", MainApplication.CLIENT); // 终端类型

                params.put("client", MainApplication.CLIENT);
                params.put("authCode", code);
                params.put("money", money);

                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    params.put("mchId", MainApplication.merchantId);
                    json.put("mchId", MainApplication.merchantId);
                } else {
                    params.put("mchId", MainApplication.getMchId());
                    json.put("mchId", MainApplication.getMchId());
                }

                if (MainApplication.isAdmin.equals("0")) {

                    if (null != MainApplication.realName && !"".equals(MainApplication.realName) && !MainApplication.realName.equals("null")) {
                        json.put("userName", MainApplication.realName); // 收银员名称
                        params.put("userName", MainApplication.realName);
                    }
                    json.put("uId", MainApplication.userId);
                    params.put("uId", MainApplication.userId + "");
                }

                if (null != MainApplication.body && !"".equals(MainApplication.body) && !MainApplication.body.equals("null")) {
                    json.put("body", MainApplication.body);
                    params.put("body", MainApplication.body);
                } else {
                    json.put("body", ApiConstant.body);
                    params.put("body", ApiConstant.body);
                }
                // json.put("signKey", "SPAY");
                String url = "";
                if (ApiConstant.ISOVERSEASY) {
                    url = ApiConstant.BASE_URL_PORT + "spay/inflMicroAliPay";
                } else {
                    url = ApiConstant.BASE_URL_PORT + "spay/unifiedMicroPay";
                }

                if (null != MainApplication.getSignKey() && !"".equals(MainApplication.getSignKey())) {
                    json.put("sign", SignUtil.getInstance().createSign(params, MainApplication.getSignKey()));

                    Logger.i("hehui", "sign-->" + SignUtil.getInstance().createSign(params, MainApplication.getSignKey()));
                }

                long spayRs = System.currentTimeMillis();
                //                    //加签名
                if (!StringUtil.isEmptyOrNull(MainApplication.getNewSignKey())) {
                    json.put("spayRs", spayRs);
                    json.put("nns", SignUtil.getInstance().createSign(JsonUtil.jsonToMap(json.toString()), MainApplication.getNewSignKey()));
                }

                try {
                    Logger.i("hehui", "调用反扫接->" + json);
                    RequestResult result = NetHelper.httpsPost(url, json, String.valueOf(spayRs), null);
                    result.setNotifyListener(listener);
                    Logger.i("hehui", "调用反扫接口返回接口-->" + result.data);
                    if (!result.hasError()) {
                        Integer res = Integer.parseInt(result.data.getString("result"));
                        if (res == 200) {
                            return executePaseRest(result.data, listener, false, vardOrders);
                        } else {
                            listener.onError(result.data.getString("message"));
                            return null;
                        }
                    } else {
                        switch (result.resultCode) {
                            case RequestResult.RESULT_BAD_NETWORK: // 网络不可用
                                listener.onError(ToastHelper.toStr(R.string.show_net_bad));
                            case RequestResult.RESULT_TIMEOUT_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_timeout));
                            case RequestResult.RESULT_READING_ERROR:
                                listener.onError(ToastHelper.toStr(R.string.show_net_server_fail));
                        }
                    }
                } catch (Exception e) {
                    listener.onError(ToastHelper.toStr(R.string.show_order_submit_fail));
                }

                return null;
            }
        }, listener);

    }

    /**
     * Native 2种提交支付请求
     *
     * @param money
     * @param isMoveNative true为动态二维码 false为静态二维码
     * @param listener
     * @see [类、类#方法、类#成员]
     */
    // //////////////////////////////////////获取服务器UUID信息///////////////////////////////////////////////
    public void getServiceUUID(final String money, final Boolean isMoveNative, final UINotifyListener<QRcodeInfo> listener) {
        ThreadHelper.executeWithCallback(new Executable<QRcodeInfo>() {
            @Override
            public QRcodeInfo execute() throws Exception {
                // String orderNo = createOrderNumber();暂不考虑本地生产订单号
                // String mId =
                // LocalAccountManager.getInstance().getLoggedUser().merchantId;
                // UserModel userModel =
                // LocalAccountManager.getInstance().getLoggedUser();
                JSONObject json = new JSONObject();
                json.put("uId", MainApplication.userId);
                // json.put("orderId",orderNo);
                json.put("mchId", MainApplication.merchantId);
                json.put("money", money);
                json.put("notifyUrl", "http://zhifu.dev.swiftpass.cn/spay/payClintNotify");
                // json.put("clientType", ApiConstant.SPAY_SUB + "");
                // json.put("cityPhone", MainApplication.cityStr == null ? "深圳"
                // : MainApplication.cityStr);
                String url = null;

                if (isMoveNative) {
                    // 动态二维码请求
                    url = ApiConstant.BASE_URL_PORT + "spay/nativePay";// ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                    MainApplication.IsNative = true;
                } else {
                    url = ApiConstant.BASE_URL_PORT + "spay/qqpay";// 第二种 二维码 静态
                    // ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                    // url = ApiConstant.BASE_URL_PORT +
                    // "employee/doCreateCodeUrl";// 第二种 二维码 静态
                    // ApiConstant.ORDER_QR_CODE_GET_UUIDINFO;
                }
                try {
                    Map<String, String> data = new HashMap<String, String>();
                    data.put("data", json.toString());
                    HttpRequester request = new HttpRequester();
                    request.setDefaultContentEncoding("UTF-8");
                    HttpRespons response = request.sendRequest(url, "POST", data, null, null, 1000 * 30); //
                    int code = response.getCode();

                    if (code >= 500 && code <= 599) {
                        listener.onError("请求服务器连接失败,请稍后再试!");
                        return null;
                    }
                    String r = response.getContent();
                    JSONObject jsonObject = new JSONObject(r);
                    return executePaseRest(jsonObject, listener, isMoveNative, null);
                } catch (SocketTimeoutException e1) // 超时
                {
                    listener.onError("请求生成二维码超时，请稍后在试!");
                } catch (UnknownHostException e1) // 连接服务器超时
                {
                    listener.onError("连接服务器失败，请检查你的网络!");
                } catch (Exception e) {
                    listener.onError("订单提交失败,请稍后再试!");
                }

                return null;
            }
        }, listener);

    }

    private QRcodeInfo executePaseRest(JSONObject data, final UINotifyListener<QRcodeInfo> listener, boolean isNative, List<WxCard> vardOrders) throws Exception {

        if (data != null) {
            int ret = Utils.Integer.tryParse(data.optString("result"), 1);
            if (ret == 200) {
                Logger.i("hehui", "data.getString-->" + data.getString("message"));
                QRcodeInfo qRcodeInfo = new QRcodeInfo();
                // qRcodeInfo.appId = data.optString("appid", "");
                // qRcodeInfo.reqKey = data.optString("req_key", "");
                JSONObject jsonObject = new JSONObject(data.getString("message"));
                // if (isNative)
                // {
                qRcodeInfo.uuId = jsonObject.optString("uuid", "");
                // }
                // qRcodeInfo.service_uuid = data.optString("service_uuid", "");
                qRcodeInfo.orderNo = jsonObject.optString("outTradeNo", "");
                qRcodeInfo.isQpay = isNative;
                setqRcodeInfo(qRcodeInfo);
                // save db
                Order order = new Order();
                order.outTradeNo = qRcodeInfo.orderNo;
                // order.state = jsonObject.optInt("status", 0);
                order.money = jsonObject.optInt("money", 0);
                order.setTradeType(jsonObject.optString("tradeType"));
                //                order.setTradeType(jsonObject.optString("service"));
                order.setTradeName(jsonObject.optString("tradeName"));
                order.state = jsonObject.optString("tradeState", "");
                order.transactionId = jsonObject.optString("transactionId", "");
                order.setOrderNoMch(jsonObject.optString("orderNoMch", ""));
                order.setDaMoney(jsonObject.optLong("daMoney", 0));
                order.setApiCode(jsonObject.optString("apiCode", ""));
                order.setCashFeel(jsonObject.optLong("cashFee", 0));
                order.setNotifyTime(jsonObject.optString("notifyTime", ""));
                order.setOrderFee(jsonObject.optLong("orderFee", 0));
                order.setSurcharge(jsonObject.optLong("surcharge", 0));
                order.setWithholdingTax(jsonObject.optLong("withholdingTax", 0));
                order.setTotalFee(jsonObject.optLong("totalFee", 0));
                order.setOrderFee(jsonObject.optLong("orderFee", 0));
                if (null != vardOrders && vardOrders.size() > 0) {
                    order.setWxCardList(vardOrders);
                }
                order.setOpenid(jsonObject.optString("openid", ""));

                qRcodeInfo.order = order;
                return qRcodeInfo;
            } else if (ret == 400) {
                Logger.i("hehui", "data.getString-->" + data.getString("message"));
                listener.onError(data.getString("message"));// 订单提交存入服务器出错
                return null;
            } else {
                listener.onError(data.getString("message"));// 订单提交存入服务器出错
                return null;
            }
            // switch (ret)
            // {
            // case 0:
            // QRcodeInfo qRcodeInfo = new QRcodeInfo();
            // // qRcodeInfo.appId = data.optString("appid", "");
            // // qRcodeInfo.reqKey = data.optString("req_key", "");
            // qRcodeInfo.uuId = data.optString("uuid", "");
            // // qRcodeInfo.service_uuid = data.optString("service_uuid", "");
            // qRcodeInfo.orderNo = null;
            // if (data.optString("orderNo", "") != null &&
            // !data.optString("orderNo", "").equals(""))
            // {
            // qRcodeInfo.orderNo = data.optString("orderNo", "");
            // }
            // setqRcodeInfo(qRcodeInfo);
            // //save db
            // Order order = new Order();
            // order.orderNo = qRcodeInfo.orderNo;
            // order.state = data.optInt("trade_state", 0);
            // order.money = data.optInt("total_fee", 0);
            // order.bankCardName = data.optString("bank_name", "");
            // order.notifyTime = new Date();
            // // UserModel userModel =
            // LocalAccountManager.getInstance().getLoggedUser();
            //
            // // order.employeeId = userModel.uId;
            // //优惠券信息
            // if (qRcodeInfo.isCoupon)
            // {
            // order.qrCodeImgUrl = data.optString("imgUrl", "");
            // order.issueyNum = data.getString("issueY");
            // order.coupMoney = data.getString("coupMoney");
            // }

            // 保存订单信息
            // if (null ==
            // OrderDB.getInstance().getOrderByOrderNo(order.orderNo,
            // order.employeeId))
            // {
            // order.state = OrderStatusEnum.NO_PAY.getValue();
            // order.add_time = System.currentTimeMillis();
            // order.userName = userModel.name;
            // OrderDB.getInstance().saveOrder(order);
            // }
            // qRcodeInfo.order = order;
            // return qRcodeInfo;
            // case 1:
            // listener.onError("提交订单参数失败!");//参数错误！
            // break;
            // case 2:
            // listener.onError("2");//无效用户！
            // break;
            // case 3:
            // listener.onError("订单号提交重复!");//订单号重复
            // break;
            // case 4:
            // listener.onError("请求生成二维码失败，订单提交失败!");//订单提交存入服务器出错
            // break;
            // case 5:
            // listener.onError("服务器出错，请稍后在试!");//服务器出错
            // break;
            // case 20:
            // listener.onError("20");
            // break;
            // case 21: // 联系代理商
            // // listener.onError("21");
            // // 2014624 新增获取对应代理商城市
            // String city = data.optString("cityPhone");
            // if (!"".equals(city) && city != null)
            // {
            // QRcodeInfo Info = new QRcodeInfo();
            // Info.setCity(city);
            // return Info;
            // }
            // }
        }
        return null;
    }

    /**
     * 扫一扫 <功能详细描述>
     *
     * @param data
     * @param listener
     * @return
     * @throws Exception
     * @see [类、类#方法、类#成员]
     */
    private QRcodeInfo executeRest(JSONObject data, final UINotifyListener<QRcodeInfo> listener) throws Exception {

        if (data != null) {

            int ret = Utils.Integer.tryParse(data.getString("result"), -1);
            switch (ret) {
                case 0:
                    QRcodeInfo qRcodeInfo = new QRcodeInfo();
                    // qRcodeInfo.appId = data.optString("appid", "");
                    // qRcodeInfo.reqKey = data.optString("req_key", "");
                    // qRcodeInfo.uuId = data.optString("uuid", "");
                    qRcodeInfo.orderNo = data.optString("orderNo", ""); // 订单好
                    // qRcodeInfo.isCoupon = data.getBoolean("isCoupon");

                    // qRcodeInfo.couponUrl = data.getString("couponUrl");
                    // cache UUID
                    setqRcodeInfo(qRcodeInfo);
                    // save db
                    Order order = new Order();
                    order.orderNo = qRcodeInfo.orderNo;
                    order.bankCardName = data.optString("bank_name", "");
                    // order.add_time = System.currentTimeMillis();
                    // order.notifyTime = new Date();
                    // UserModel userModel =
                    // LocalAccountManager.getInstance().getLoggedUser();
                    // order.employeeId = userModel.uId;
                    // 优惠券信息
                    // if (qRcodeInfo.isCoupon)
                    // {
                    // order.qrCodeImgUrl = data.optString("imgUrl", "");
                    // order.issueyNum = data.getString("issueY");
                    // order.coupMoney = data.getString("coupMoney");
                    // }

                    // 保存订单信息
                    // if (null ==
                    // OrderDB.getInstance().getOrderByOrderNo(order.orderNo,
                    // order.employeeId))
                    // {
                    // order.state = OrderStatusEnum.NO_PAY.getValue();
                    // order.add_time = System.currentTimeMillis();
                    // order.userName = userModel.name;
                    // OrderDB.getInstance().saveOrder(order);
                    // }
                    qRcodeInfo.order = order;
                    return qRcodeInfo;
                case 1:
                    listener.onError("1");// 参数错误！
                    break;
                case 2:
                    listener.onError("2");// 无效用户！
                    break;
                case 3:
                    listener.onError("3");// 订单号重复
                    break;
                case 4:
                    listener.onError("提交支付失败，请稍候再试!");// 订单提交存入服务器出错
                    break;
                case 5:
                    listener.onError("提交订单失败，请稍候再试!");// 订单提交存入服务器出错
                    break;
                case 6:
                    listener.onError("余额不足，支付失败!");
                    break;
                case 20:
                    listener.onError("20");
                    break;
                case 21: // 联系代理商
                    // listener.onError("21");
                    // 2014624 新增获取对应代理商城市
                    String city = data.optString("cityPhone");
                    if (!"".equals(city) && city != null) {
                        QRcodeInfo Info = new QRcodeInfo();
                        Info.setCity(city);
                        return Info;
                    }
            }
        }
        return null;
    }

    /**
     * 获取加密后的密钥
     *
     * @param listener
     */
    public void getOrderSignaturekey(final String orderCode, final String encOrderData, final UINotifyListener<String> listener) {
        ThreadHelper.executeWithCallback(new Executable<String>() {
            @Override
            public String execute() throws Exception {
                UserModel userModel = LocalAccountManager.getInstance().getLoggedUser();
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("mId", userModel.merchantId + "");
                jsonObject.put("imeiNo", "1397112900744");// AppHelper.getIMEI()
                jsonObject.put("posId", orderCode);
                if (encOrderData != null) {
                    jsonObject.put("clientPadKey", encOrderData);
                }
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.ORDER_GET_ORDER_POS_KEY, jsonObject);

                if (!result.hasError()) {
                    String code = result.data.getString("code");
                    if (code != null) {
                        // callback(MSG_NOTIFY_ON_SUCCEED,listener,code);
                        return code;
                    }
                } else {
                    // callback(MSG_NOTIFY_ON_ERROR,listener,"获取失败");
                    return null;
                }
                return null;
            }
        }, listener);
    }

    /**
     * 扫描状态 code 408
     */
    public void checkQRCodeState(final String uuId, final int code, UINotifyListener<QRCodeState> listener, final HttpURLConnection[] connections) {

        ThreadHelper.executeWithCallback(new Executable<QRCodeState>() {
            @Override
            public QRCodeState execute() throws Exception {
                HttpRequester request = new HttpRequester();
                try {
                    request.setDefaultContentEncoding("UTF-8");
                    Map<String, String> data = new HashMap<String, String>();
                    data.put("uuid", uuId.toString());
                    Logger.i("hehui", "checkQRCodeState--uuid-->" + uuId);
                    String url = ApiConstant.PAYGATEWAY + "/" + "pay/check?uuid=" + uuId;
                    String result = NetHelper.httpsGet(url);
                    Logger.i("hehui", "checkQRCodeState--result->" + result);
                    // String r = response.getContent();
                    return executeWXPayState(result);
                } catch (Exception e) {
                    e.printStackTrace();
                    Logger.i(e);
                }
                return QRCodeState.DEFAULT;
            }
        }, listener);
    }

    private QRCodeState executeWXPayState(String text) throws ParseException {
        if (text != null) {
            HashMap<String, String> mapRes = XmlUtil.parse(text);

            String status = mapRes.get("status");
            int ret = Utils.Integer.tryParse(status, 0);

            switch (ret) {
                case 200:
                    return QRCodeState.PAY_N;
                case 202:
                    return QRCodeState.PAY_FAIL;
                case 203:
                    return QRCodeState.SCAN_SUCCESS;
                case 201:
                    return QRCodeState.PAY_SUCCESS;
                case 204:
                    return QRCodeState.PAY;
                case 205:
                    return QRCodeState.SCAN_SUCCESS_CANCEL;
                case 400:
                    return QRCodeState.FAIL_UUID;
                case 408:
                    return QRCodeState.NO_PAY;
            }
        }
        return QRCodeState.DEFAULT;
    }

    private String qrCodeUrl = "https://login.weixin.qq.com/jspay";

    private String paysuccDetaillUrl = "https://wx.tenpay.com/cgi-bin/mmpayweb-bin/paysuccdetail";

    // https://login.weixin.qq.com/jspay?_=1383635697211&appid=wx6964eb0b10aa369b&prepayid=&lang=zh_CN&req_key=30762735a4e4da9705";

    /**
     * 获取二维码UUID
     */
    public void getQRCodeInfo(UINotifyListener<String> listener) {

        ThreadHelper.executeWithCallback(new Executable<String>() {
            @Override
            public String execute() throws Exception {
                JSONObject data = new JSONObject();
                // data.put("uuid", qRcodeInfo.uuId);
                data.put("appid", qRcodeInfo.appId);
                data.put("req_key", qRcodeInfo.reqKey);
                data.put("_", System.currentTimeMillis());
                data.put("lang", "zh_CN");
                RequestResult result = NetHelper.httpsGet(qrCodeUrl, data, false);
                if (!result.hasError()) {
                    String text = result.tag.toString();
                    Pattern pattern = Pattern.compile("(?s)(?i)uuid\\s*\\={1}\\s*\"{1}([^\"]*)");
                    Matcher matcher = pattern.matcher(text);
                    if (matcher.find()) {
                        codeUUUID = matcher.group(1);
                        qRcodeInfo.uuId = codeUUUID;
                    }
                }
                return codeUUUID;
            }
        }, listener);
    }

    public void searchCFTOrderPayInfo(UINotifyListener<PaySuccessInfo> listener) {
        ThreadHelper.executeWithCallback(new Executable<PaySuccessInfo>() {
            @Override
            public PaySuccessInfo execute() throws Exception {
                JSONObject data = new JSONObject();
                data.put("appid", qRcodeInfo.appId);
                data.put("req_key", qRcodeInfo.reqKey);
                data.put("uuid", qRcodeInfo.uuId);
                data.put("lang", "zh_CN");
                RequestResult result = NetHelper.httpsGet(paysuccDetaillUrl, data, false);
                if (!result.hasError()) {
                    String text = result.tag.toString();
                    System.out.println("text:" + text);
                    Pattern pattern = Pattern.compile("<script>(.*?)</script>");
                    String ret = java.net.URLDecoder.decode(text, "utf-8");
                    Matcher matcher = pattern.matcher(ret);
                    if (matcher.find()) {
                        String s = matcher.group(1);
                        Gson g = new Gson();
                        PaySuccessInfo p = g.fromJson(s, PaySuccessInfo.class);
                        sendOrderPayInfo(p);
                        return p;
                    }
                }
                return null;
            }
        }, listener);
    }

    /***
     * 本地二维码交易完成后报告服务器 用户名和银行名称
     */
    private void sendOrderPayInfo(final PaySuccessInfo p) {
        ThreadHelper.executeWithCallback(new Executable<Boolean>() {
            @Override
            public Boolean execute() throws Exception {
                JSONObject data = new JSONObject();
                data.put("orderNo", qRcodeInfo.order.orderNo);
                data.put("bankCard", p.bank_card);
                data.put("wxusername", p.username);
                RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + ApiConstant.TEST_CFT_ORDER, data, null, null);
                if (!result.hasError()) {
                    String res = result.data.getString("result");
                    int ret = Utils.Integer.tryParse(res, -1);
                    Logger.i("upload pay info ret:" + ret);

                }
                return false;
            }
        }, null);
    }

    public QRcodeInfo qRcodeInfo;

    public void setqRcodeInfo(QRcodeInfo qRcodeInfo) {
        this.qRcodeInfo = qRcodeInfo;
    }

    /**
     * 监听订单状态
     */
    public void listenerOrderQRCodeState() {
        // 30s执行一次
        CycledThread thread = new CycledThread(orderListenerThread, 1000 * 60 * 4);
        thread.setShortSleepTime(200);
        thread.start();
    }

    private Runnable orderListenerThread = new Runnable() {
        @Override
        public void run() {

        }
    };

    @Override
    public void destory() {

        qRcodeInfo = null;
        codeUUUID = null;
    }

    /**
     * 将金额格式分
     */
    public String getMoney(String strMoney) {
        double money = Double.parseDouble(strMoney);
        double t = money * 100;
        DecimalFormat df = new DecimalFormat("0");
        return df.format(t);
    }

    public String paseMoney(String strMoney) {
        double money = Double.parseDouble(strMoney);
        DecimalFormat df = new DecimalFormat("0");
        return df.format(money);
    }

    /**
     * 获取核销详情
     * <功能详细描述>
     *
     * @param uiNotifyListener
     * @see [类、类#方法、类#成员]
     */
    public void queryVardDetail(final String cardId, final String cardCode, final UINotifyListener<WxCard> uiNotifyListener) {
        ThreadHelper.executeWithCallback(new Executable<WxCard>() {

            @Override
            public WxCard execute() throws Exception {
                JSONObject json = new JSONObject();
                if (!StringUtil.isEmptyOrNull(MainApplication.merchantId)) {
                    json.put("mchId", MainApplication.merchantId);
                } else {
                    json.put("mchId", MainApplication.getMchId());
                }
                json.put("uId", MainApplication.userId + "");
                json.put("cardId", cardId);
                json.put("cardCode", cardCode);
                try {
                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/queryCardDetail", json, null, null);

                    result.setNotifyListener(uiNotifyListener);
                    if (!result.hasError()) {
                        int code = result.data.getInt("result");
                        if (code == 200) {
                            try {
                                return (WxCard) JsonUtil.jsonToBean(result.data.getString("message"), WxCard.class);
                            } catch (Exception e) {
                                return new WxCard();
                            }
                        } else {
                            uiNotifyListener.onError(result.data.getString("message"));
                        }
                    }
                } catch (Exception e) {
                    uiNotifyListener.onError(ToastHelper.toStr(R.string.show_affirm_fail));
                }
                return null;
            }
        }, uiNotifyListener);
    }

    /**
     * 获取核销列表
     * <功能详细描述>
     *
     * @param uiNotifyListener
     * @see [类、类#方法、类#成员]
     */
    public void queryHeXiaoRecord(final String cardTime, final String page, final UINotifyListener<List<Order>> uiNotifyListener) {
        ThreadHelper.executeWithCallback(new Executable<List<Order>>() {

            @Override
            public List<Order> execute() throws Exception {
                JSONObject json = new JSONObject();
                json.put("mchId", MainApplication.merchantId + "");

                if (MainApplication.isAdmin.equals("0")) // 收银员
                {
                    json.put("uId", MainApplication.userId);
                }
                json.put("cardTime", cardTime);
                json.put("page", page);
                json.put("pageSize", "10");
                try {
                    RequestResult result = NetHelper.httpsPost(ApiConstant.BASE_URL_PORT + "spay/wxCardGetUserList", json, null, null);

                    result.setNotifyListener(uiNotifyListener);
                    if (!result.hasError()) {
                        int code = result.data.getInt("result");
                        if (code == 200) {
                            List<Order> list = new ArrayList<Order>();
                            if (result.data.getString("message").equals("[[]]")) {
                                return list;
                            }
                            JSONArray jsonArray = new JSONArray(result.data.getString("message"));
                            int len = jsonArray.length();
                            for (int i = 0; i < len; i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                Order order = new Order();
                                order.setBrandName(object.optString("brandName", ""));
                                order.setCardCode(object.optString("cardCode", ""));
                                order.setTitle(object.optString("cardTitle", ""));
                                order.setCardId(object.optString("cardId", ""));
                                order.setNickName(object.optString("nickName", ""));
                                order.setUseTime(object.optLong("useTime", 0));
                                order.setTotalRows(object.optLong("totalRows", 0));
                                order.setDescription(object.optString("description", ""));
                                list.add(order);
                            }

                            return list;
                        } else {
                            uiNotifyListener.onError(result.data.getString("message"));
                        }
                    }
                } catch (Exception e) {
                    uiNotifyListener.onError(ToastHelper.toStr(R.string.see_order_submit_fail));
                }
                return null;
            }
        }, uiNotifyListener);
    }
}
